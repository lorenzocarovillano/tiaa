/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:51 PM
**        * FROM NATURAL PROGRAM : Twrp4610
************************************************************
**        * FILE NAME            : Twrp4610.java
**        * CLASS NAME           : Twrp4610
**        * INSTANCE NAME        : Twrp4610
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4610
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : EDITS AND VALIDATION PROGRAM.
* CREATED  : 07 / 24 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM EDITS, AND VALIDATES "IRA" CONTRIBUTIONS DATA
*            RECEIVED FROM VITALY IN ACTUARIAL.
*
* HISTORY
* 04/30/04D. APRIL RESTRUCTURED CODE TO HANDLE COMBINING MULTIPLE
*            CONTRIBUTIONS FOR SAME CONTRACT PAYEE.  SOME CONTRACTS
*            HAVE A FINANCIAL TRANSACTION WITH A TCII COMPANY
*            CODE, AND A TRANSACTION WITH EITHER A TIAA OR CREF
*            CODE.
* 08/25/05   J.ROTHOLZ - MODIFIED CODE TO HANDLE SEP IRAS FOR TOPS 6.5
* 02/01/07   J.ROTHOLZ - ADDED GRAND TOTALS TO REPORT #4 AND LOGIC
*                        TO BYPASS FMV CHANGES LESS THAN $10
*
* 04/27/2017  WEBBJ - PIN EXPANSION SCAN JW0427
* 09/25/2020  VIKRAM - 2 FIELDS ADDED IN TWR-IRA  TWRC-RMD-REQUIRED-FLAG
*                  AND  TWRC-RMD-DECEDENT-NAME : TAG - SECURE-ACT
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4610 extends BLNatBase
{
    // Data Areas
    private LdaTwrl462a ldaTwrl462a;
    private LdaTwrl462b ldaTwrl462b;
    private LdaTwrl462c ldaTwrl462c;
    private LdaTwrl462d ldaTwrl462d;
    private LdaTwrl462e ldaTwrl462e;
    private PdaTwrapart pdaTwrapart;
    private PdaTwracor pdaTwracor;
    private LdaTwrlcor1 ldaTwrlcor1;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;
    private DbsField pnd_Twrc_S2_Forms;

    private DbsGroup pnd_Twrc_S2_Forms__R_Field_1;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Status;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Contract;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Payee;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Fmv_Bypass_Ctr;
    private DbsField pnd_Dob_Ctr;
    private DbsField pnd_Combined_Ctr;
    private DbsField pnd_Combined_Eoj_Cnt;
    private DbsField pnd_No_Of_Errors;
    private DbsField pnd_No_Of_Warnings;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Warning_Msg;
    private DbsField pnd_Hdr_Tax_Year;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_Hdr_Simulation_Date;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;

    private DbsGroup pnd_G_Accum;
    private DbsField pnd_G_Accum_Pnd_G_Control;
    private DbsField pnd_G_Accum_Pnd_G_Trans_Count;
    private DbsField pnd_G_Accum_Pnd_G_Classic_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Roth_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Rollover_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Rechar_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Sep_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Fmv_Amt;
    private DbsField pnd_G_Accum_Pnd_G_Roth_Conv_Amt;
    private DbsField pnd_Rejected;
    private DbsField pnd_Dup_Found;
    private DbsField pnd_Naa_Found;
    private DbsField pnd_Naa_Found_Save;
    private DbsField pnd_New_Key;
    private DbsField pnd_Fmv_Plus_Or_Minus_10;
    private DbsField pnd_Company_Code1;
    private DbsField pnd_Company_Code2;

    private DbsGroup pnd_Combined_Codes;
    private DbsField pnd_Combined_Codes_Pnd_Company_Codes_Ss;
    private DbsField pnd_Combined_Codes_Pnd_Company_Codes_St;
    private DbsField pnd_Combined_Codes_Pnd_Company_Codes_Sc;
    private DbsField pnd_Combined_Codes_Pnd_Company_Codes_Tc;
    private DbsField pnd_Combined_Codes_Pnd_Company_Codes_Other;

    private DbsGroup pnd_Save_Old_Ira_Key;
    private DbsField pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id;
    private DbsField pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract;
    private DbsField pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee;

    private DbsGroup pnd_Save_Old_Ira_Key__R_Field_2;
    private DbsField pnd_Save_Old_Ira_Key_Pnd_Save_Old_Key;

    private DbsGroup pnd_Save_New_Ira_Key;
    private DbsField pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id;
    private DbsField pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract;
    private DbsField pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee;

    private DbsGroup pnd_Save_New_Ira_Key__R_Field_3;
    private DbsField pnd_Save_New_Ira_Key_Pnd_Save_New_Key;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i4;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;
    private DbsField x;
    private DbsField pnd_Ndx;
    private DbsField pnd_Diff1;
    private DbsField pnd_Diff2;
    private DbsField pnd_Tot;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl462a = new LdaTwrl462a();
        registerRecord(ldaTwrl462a);
        ldaTwrl462b = new LdaTwrl462b();
        registerRecord(ldaTwrl462b);
        ldaTwrl462c = new LdaTwrl462c();
        registerRecord(ldaTwrl462c);
        registerRecord(ldaTwrl462c.getVw_ira());
        ldaTwrl462d = new LdaTwrl462d();
        registerRecord(ldaTwrl462d);
        ldaTwrl462e = new LdaTwrl462e();
        registerRecord(ldaTwrl462e);
        localVariables = new DbsRecord();
        pdaTwrapart = new PdaTwrapart(localVariables);
        pdaTwracor = new PdaTwracor(localVariables);
        ldaTwrlcor1 = new LdaTwrlcor1();
        registerRecord(ldaTwrlcor1);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Twrc_S2_Forms = localVariables.newFieldInRecord("pnd_Twrc_S2_Forms", "#TWRC-S2-FORMS", FieldType.STRING, 25);

        pnd_Twrc_S2_Forms__R_Field_1 = localVariables.newGroupInRecord("pnd_Twrc_S2_Forms__R_Field_1", "REDEFINE", pnd_Twrc_S2_Forms);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year = pnd_Twrc_S2_Forms__R_Field_1.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Twrc_S2_Forms_Pnd_S2_Status = pnd_Twrc_S2_Forms__R_Field_1.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Status", "#S2-STATUS", FieldType.STRING, 
            1);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id = pnd_Twrc_S2_Forms__R_Field_1.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id", "#S2-TAX-ID", FieldType.STRING, 
            10);
        pnd_Twrc_S2_Forms_Pnd_S2_Contract = pnd_Twrc_S2_Forms__R_Field_1.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Contract", "#S2-CONTRACT", FieldType.STRING, 
            8);
        pnd_Twrc_S2_Forms_Pnd_S2_Payee = pnd_Twrc_S2_Forms__R_Field_1.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Payee", "#S2-PAYEE", FieldType.STRING, 
            2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Fmv_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Fmv_Bypass_Ctr", "#FMV-BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Dob_Ctr = localVariables.newFieldInRecord("pnd_Dob_Ctr", "#DOB-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Combined_Ctr = localVariables.newFieldInRecord("pnd_Combined_Ctr", "#COMBINED-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Combined_Eoj_Cnt = localVariables.newFieldInRecord("pnd_Combined_Eoj_Cnt", "#COMBINED-EOJ-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Of_Errors = localVariables.newFieldInRecord("pnd_No_Of_Errors", "#NO-OF-ERRORS", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Of_Warnings = localVariables.newFieldInRecord("pnd_No_Of_Warnings", "#NO-OF-WARNINGS", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Msg = localVariables.newFieldArrayInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Warning_Msg = localVariables.newFieldArrayInRecord("pnd_Warning_Msg", "#WARNING-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Hdr_Tax_Year = localVariables.newFieldInRecord("pnd_Hdr_Tax_Year", "#HDR-TAX-YEAR", FieldType.STRING, 4);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);
        pnd_Hdr_Simulation_Date = localVariables.newFieldInRecord("pnd_Hdr_Simulation_Date", "#HDR-SIMULATION-DATE", FieldType.DATE);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));

        pnd_G_Accum = localVariables.newGroupArrayInRecord("pnd_G_Accum", "#G-ACCUM", new DbsArrayController(1, 5));
        pnd_G_Accum_Pnd_G_Control = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Control", "#G-CONTROL", FieldType.PACKED_DECIMAL, 10);
        pnd_G_Accum_Pnd_G_Trans_Count = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Trans_Count", "#G-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Accum_Pnd_G_Classic_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Classic_Amt", "#G-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_G_Accum_Pnd_G_Roth_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Roth_Amt", "#G-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_G_Accum_Pnd_G_Rollover_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Rollover_Amt", "#G-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 
            2);
        pnd_G_Accum_Pnd_G_Rechar_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Rechar_Amt", "#G-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_G_Accum_Pnd_G_Sep_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Sep_Amt", "#G-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_G_Accum_Pnd_G_Fmv_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Fmv_Amt", "#G-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt = pnd_G_Accum.newFieldInGroup("pnd_G_Accum_Pnd_G_Roth_Conv_Amt", "#G-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Rejected = localVariables.newFieldInRecord("pnd_Rejected", "#REJECTED", FieldType.BOOLEAN, 1);
        pnd_Dup_Found = localVariables.newFieldInRecord("pnd_Dup_Found", "#DUP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found = localVariables.newFieldInRecord("pnd_Naa_Found", "#NAA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found_Save = localVariables.newFieldInRecord("pnd_Naa_Found_Save", "#NAA-FOUND-SAVE", FieldType.BOOLEAN, 1);
        pnd_New_Key = localVariables.newFieldInRecord("pnd_New_Key", "#NEW-KEY", FieldType.BOOLEAN, 1);
        pnd_Fmv_Plus_Or_Minus_10 = localVariables.newFieldInRecord("pnd_Fmv_Plus_Or_Minus_10", "#FMV-PLUS-OR-MINUS-10", FieldType.BOOLEAN, 1);
        pnd_Company_Code1 = localVariables.newFieldInRecord("pnd_Company_Code1", "#COMPANY-CODE1", FieldType.STRING, 1);
        pnd_Company_Code2 = localVariables.newFieldInRecord("pnd_Company_Code2", "#COMPANY-CODE2", FieldType.STRING, 1);

        pnd_Combined_Codes = localVariables.newGroupInRecord("pnd_Combined_Codes", "#COMBINED-CODES");
        pnd_Combined_Codes_Pnd_Company_Codes_Ss = pnd_Combined_Codes.newFieldInGroup("pnd_Combined_Codes_Pnd_Company_Codes_Ss", "#COMPANY-CODES-SS", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Combined_Codes_Pnd_Company_Codes_St = pnd_Combined_Codes.newFieldInGroup("pnd_Combined_Codes_Pnd_Company_Codes_St", "#COMPANY-CODES-ST", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Combined_Codes_Pnd_Company_Codes_Sc = pnd_Combined_Codes.newFieldInGroup("pnd_Combined_Codes_Pnd_Company_Codes_Sc", "#COMPANY-CODES-SC", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Combined_Codes_Pnd_Company_Codes_Tc = pnd_Combined_Codes.newFieldInGroup("pnd_Combined_Codes_Pnd_Company_Codes_Tc", "#COMPANY-CODES-TC", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Combined_Codes_Pnd_Company_Codes_Other = pnd_Combined_Codes.newFieldInGroup("pnd_Combined_Codes_Pnd_Company_Codes_Other", "#COMPANY-CODES-OTHER", 
            FieldType.PACKED_DECIMAL, 8);

        pnd_Save_Old_Ira_Key = localVariables.newGroupInRecord("pnd_Save_Old_Ira_Key", "#SAVE-OLD-IRA-KEY");
        pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id = pnd_Save_Old_Ira_Key.newFieldInGroup("pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id", "#OLD-IRA-TAX-ID", FieldType.STRING, 
            10);
        pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract = pnd_Save_Old_Ira_Key.newFieldInGroup("pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract", "#OLD-IRA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee = pnd_Save_Old_Ira_Key.newFieldInGroup("pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee", "#OLD-IRA-PAYEE", FieldType.STRING, 
            2);

        pnd_Save_Old_Ira_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Save_Old_Ira_Key__R_Field_2", "REDEFINE", pnd_Save_Old_Ira_Key);
        pnd_Save_Old_Ira_Key_Pnd_Save_Old_Key = pnd_Save_Old_Ira_Key__R_Field_2.newFieldInGroup("pnd_Save_Old_Ira_Key_Pnd_Save_Old_Key", "#SAVE-OLD-KEY", 
            FieldType.STRING, 20);

        pnd_Save_New_Ira_Key = localVariables.newGroupInRecord("pnd_Save_New_Ira_Key", "#SAVE-NEW-IRA-KEY");
        pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id = pnd_Save_New_Ira_Key.newFieldInGroup("pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id", "#NEW-IRA-TAX-ID", FieldType.STRING, 
            10);
        pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract = pnd_Save_New_Ira_Key.newFieldInGroup("pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract", "#NEW-IRA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee = pnd_Save_New_Ira_Key.newFieldInGroup("pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee", "#NEW-IRA-PAYEE", FieldType.STRING, 
            2);

        pnd_Save_New_Ira_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Save_New_Ira_Key__R_Field_3", "REDEFINE", pnd_Save_New_Ira_Key);
        pnd_Save_New_Ira_Key_Pnd_Save_New_Key = pnd_Save_New_Ira_Key__R_Field_3.newFieldInGroup("pnd_Save_New_Ira_Key_Pnd_Save_New_Key", "#SAVE-NEW-KEY", 
            FieldType.STRING, 20);
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 4);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 4);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 4);
        i4 = localVariables.newFieldInRecord("i4", "I4", FieldType.PACKED_DECIMAL, 4);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        x = localVariables.newFieldInRecord("x", "X", FieldType.PACKED_DECIMAL, 4);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Diff1 = localVariables.newFieldInRecord("pnd_Diff1", "#DIFF1", FieldType.PACKED_DECIMAL, 7);
        pnd_Diff2 = localVariables.newFieldInRecord("pnd_Diff2", "#DIFF2", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot = localVariables.newFieldInRecord("pnd_Tot", "#TOT", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl462a.initializeValues();
        ldaTwrl462b.initializeValues();
        ldaTwrl462c.initializeValues();
        ldaTwrl462d.initializeValues();
        ldaTwrl462e.initializeValues();
        ldaTwrlcor1.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
        pnd_New_Key.setInitialValue(false);
        pnd_Fmv_Plus_Or_Minus_10.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4610() throws Exception
    {
        super("Twrp4610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4610", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt9, 9);
        getReports().atTopOfPage(atTopEventRpt10, 10);
        setupReports();
        //* *--------
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 00 ) PS = 60 LS = 133
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 01 ) PS = 60 LS = 138
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 02 ) PS = 60 LS = 138
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 03 ) PS = 60 LS = 138;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 05 ) PS = 60 LS = 138
        //*  PIN EXPANSION JW0427                                                                                                                                         //Natural: FORMAT ( 06 ) PS = 60 LS = 138;//Natural: FORMAT ( 07 ) PS = 60 LS = 133;//Natural: FORMAT ( 08 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: FORMAT ( 09 ) PS = 60 LS = 138;//Natural: FORMAT ( 10 ) PS = 60 LS = 133
        pnd_Interface_Date.setValue(Global.getDATX());                                                                                                                    //Natural: ASSIGN #INTERFACE-DATE := *DATX
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-TOTALS
        sub_Read_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  FROM IRA-RECORD DAPRIL 4/22/04
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD RD-IRA-REC
        while (condition(getWorkFiles().read(1, ldaTwrl462e.getRd_Ira_Rec())))
        {
            //*  IF #READ-CTR > 84000 ESCAPE BOTTOM END-IF
            //*  $$$$
            //*  ACCEPT IF RD-IRA-CONTRACT = 'NE471155' OR = 'NE471395'
            //*  $$$$
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                pnd_Hdr_Tax_Year.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                                      //Natural: ASSIGN #HDR-TAX-YEAR := IRA-TAX-YEAR
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id());                                                              //Natural: MOVE RD-IRA-TAX-ID TO #OLD-IRA-TAX-ID #NEW-IRA-TAX-ID
                pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id());
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());                                                          //Natural: MOVE RD-IRA-CONTRACT TO #OLD-IRA-CONTRACT #NEW-IRA-CONTRACT
                pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());                                                                //Natural: MOVE RD-IRA-PAYEE TO #OLD-IRA-PAYEE #NEW-IRA-PAYEE
                pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());
                getReports().write(0, Global.getPROGRAM(),"=",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Simulation_Date());                                                        //Natural: WRITE ( 0 ) *PROGRAM '=' RD-IRA-SIMULATION-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Header_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Simulation_Date());                                        //Natural: MOVE EDITED RD-IRA-SIMULATION-DATE TO #HEADER-DATE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            j.setValue(1);                                                                                                                                                //Natural: ASSIGN J := 1
            if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("T")))                                                                                   //Natural: IF RD-IRA-COMPANY-CODE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-INPUT-CONTROL-TOTALS
                sub_Update_T_Input_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("C")))                                                                               //Natural: IF RD-IRA-COMPANY-CODE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-INPUT-CONTROL-TOTALS
                    sub_Update_C_Input_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("L")))                                                                           //Natural: IF RD-IRA-COMPANY-CODE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-INPUT-CONTROL-TOTALS
                        sub_Update_L_Input_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("S")))                                                                       //Natural: IF RD-IRA-COMPANY-CODE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-INPUT-CONTROL-TOTALS
                            sub_Update_S_Input_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  GET RESIDENCY CODE FROM COR/CNAD
            pdaTwrapart.getPnd_Twrapart_Pnd_Function().setValue("R");                                                                                                     //Natural: ASSIGN #TWRAPART.#FUNCTION := 'R'
            pdaTwrapart.getPnd_Twrapart_Pnd_Tin().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id());                                                                    //Natural: ASSIGN #TWRAPART.#TIN := RD-IRA-TAX-ID
            pdaTwrapart.getPnd_Twrapart_Pnd_Contract_Nbr().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());                                                         //Natural: ASSIGN #TWRAPART.#CONTRACT-NBR := RD-IRA-CONTRACT
            pdaTwrapart.getPnd_Twrapart_Pnd_Payee_Cde().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());                                                               //Natural: ASSIGN #TWRAPART.#PAYEE-CDE := RD-IRA-PAYEE
            pdaTwrapart.getPnd_Twrapart_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Year());                            //Natural: MOVE EDITED RD-IRA-TAX-YEAR TO #TWRAPART.#TAX-YEAR ( EM = 9999 )
            DbsUtil.callnat(Twrnpart.class , getCurrentProcessState(), pdaTwrapart.getPnd_Twrapart());                                                                    //Natural: CALLNAT 'TWRNPART' USING #TWRAPART
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwrapart.getPnd_Twrapart_Pnd_On_Cnad().getBoolean()))                                                                                        //Natural: IF #TWRAPART.#ON-CNAD
            {
                pnd_Naa_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #NAA-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Naa_Found.reset();                                                                                                                                    //Natural: RESET #NAA-FOUND
            }                                                                                                                                                             //Natural: END-IF
            //* *WRITE(02) 'RAHUL RD-IRA-RESIDENCY-CODE' RD-IRA-RESIDENCY-CODE
            if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Code().equals(" ")))                                                                                 //Natural: IF RD-IRA-RESIDENCY-CODE = ' '
            {
                //*  APO
                if (condition(pdaTwrapart.getPnd_Twrapart_Pnd_On_Cnad().getBoolean() || pdaTwrapart.getPnd_Twrapart_Geo().equals("00")))                                  //Natural: IF #TWRAPART.#ON-CNAD OR #TWRAPART.GEO = '00'
                {
                    //* *WRITE(02) 'RAHUL #TWRAPART.GEO' #TWRAPART.GEO
                    ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Code().setValue(pdaTwrapart.getPnd_Twrapart_Geo());                                                        //Natural: ASSIGN RD-IRA-RESIDENCY-CODE := #TWRAPART.GEO
                    if (condition(pdaTwrapart.getPnd_Twrapart_Geo().greaterOrEqual("00") && pdaTwrapart.getPnd_Twrapart_Geo().lessOrEqual("57")))                         //Natural: IF #TWRAPART.GEO = '00' THRU '57'
                    {
                        ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Type().setValue("1");                                                                                  //Natural: ASSIGN RD-IRA-RESIDENCY-TYPE := '1'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwrapart.getPnd_Twrapart_Geo().greaterOrEqual("75") && pdaTwrapart.getPnd_Twrapart_Geo().lessOrEqual("86")))                         //Natural: IF #TWRAPART.GEO = '75' THRU '86'
                    {
                        ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Type().setValue("5");                                                                                  //Natural: ASSIGN RD-IRA-RESIDENCY-TYPE := '5'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(0, "TIN",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id(),"CONTRACT",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract(),"PAYEE",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee(),"NEW RES",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Code(),"RES TYPE",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Type(),new  //Natural: WRITE 'TIN' RD-IRA-TAX-ID 'CONTRACT' RD-IRA-CONTRACT 'PAYEE' RD-IRA-PAYEE 'NEW RES' RD-IRA-RESIDENCY-CODE 'RES TYPE' RD-IRA-RESIDENCY-TYPE 67T 'FROM COR/CNAD'
                        TabSetting(67),"FROM COR/CNAD");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract_Xref().equals(" ")))                                                                                  //Natural: IF RD-IRA-CONTRACT-XREF = ' '
            {
                pdaTwracor.getPnd_Twracor_Pnd_Request_Type().setValue("2");                                                                                               //Natural: ASSIGN #TWRACOR.#REQUEST-TYPE := '2'
                pdaTwracor.getPnd_Twracor_Pnd_Ppcn().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());                                                               //Natural: ASSIGN #TWRACOR.#PPCN := RD-IRA-CONTRACT
                pdaTwracor.getPnd_Twracor_Pnd_Payee().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());                                                                 //Natural: ASSIGN #TWRACOR.#PAYEE := RD-IRA-PAYEE
                pdaTwracor.getPnd_Twracor_Pnd_Ppcn_Idx_Max().setValue(1);                                                                                                 //Natural: ASSIGN #TWRACOR.#PPCN-IDX-MAX := 1
                pdaTwracor.getPnd_Twracor_Pnd_Ppcn_Lookup_Only().setValue(true);                                                                                          //Natural: ASSIGN #TWRACOR.#PPCN-LOOKUP-ONLY := TRUE
                DbsUtil.callnat(Twrncor.class , getCurrentProcessState(), pdaTwracor.getPnd_Twracor(), ldaTwrlcor1.getPnd_Twrlcor1().getValue(1));                        //Natural: CALLNAT 'TWRNCOR' USING #TWRACOR #TWRLCOR1 ( 1 )
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwracor.getPnd_Twracor_Pnd_Ret_Cde().equals(true)))                                                                                      //Natural: IF #TWRACOR.#RET-CDE = TRUE
                {
                    if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract().equals(ldaTwrlcor1.getPnd_Twrlcor1_Cntrct_Nbr().getValue(1))))                              //Natural: IF RD-IRA-CONTRACT = #TWRLCOR1.CNTRCT-NBR ( 1 )
                    {
                        ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract_Xref().setValue(ldaTwrlcor1.getPnd_Twrlcor1_Cntrct_Cref_Nbr().getValue(1));                             //Natural: ASSIGN RD-IRA-CONTRACT-XREF := #TWRLCOR1.CNTRCT-CREF-NBR ( 1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract_Xref().setValue(ldaTwrlcor1.getPnd_Twrlcor1_Cntrct_Nbr().getValue(1));                                  //Natural: ASSIGN RD-IRA-CONTRACT-XREF := #TWRLCOR1.CNTRCT-NBR ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(0, "TIN",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id(),"Contract",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract(),"Payee",                 //Natural: WRITE 'TIN' RD-IRA-TAX-ID 'Contract' RD-IRA-CONTRACT 'Payee' RD-IRA-PAYEE 'Xref Contract' RD-IRA-CONTRACT-XREF
                        ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee(),"Xref Contract",ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract_Xref());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  NEW KEY FLAG SET TO FALSE.
            pnd_New_Key.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #NEW-KEY
            pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id());                                                                  //Natural: MOVE RD-IRA-TAX-ID TO #NEW-IRA-TAX-ID
            pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());                                                              //Natural: MOVE RD-IRA-CONTRACT TO #NEW-IRA-CONTRACT
            pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());                                                                    //Natural: MOVE RD-IRA-PAYEE TO #NEW-IRA-PAYEE
            if (condition(pnd_Save_Old_Ira_Key_Pnd_Save_Old_Key.equals(pnd_Save_New_Ira_Key_Pnd_Save_New_Key)))                                                           //Natural: IF #SAVE-OLD-KEY EQ #SAVE-NEW-KEY
            {
                                                                                                                                                                          //Natural: PERFORM SAVE-IRA-TRAN-DATA
                sub_Save_Ira_Tran_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  READ NEXT TO SEE IF SAME KEY.
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  WE HAVE A NEW KEY.
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_New_Key.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #NEW-KEY
            }                                                                                                                                                             //Natural: END-IF
            //*  PROCESS SINGLE TRANSACTIONS AND COMBINED TRANSACTIONS NOW READY
            //*  FOR OUTPUT TO A WORK FILE.
            //*  - READ IRA CONTRIBUTION FILE.  LOOK FOR RECORDS THAT MATCH INPUT.
                                                                                                                                                                          //Natural: PERFORM READ-IRA-CHECK-FOR-DUP
            sub_Read_Ira_Check_For_Dup();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Dup_Found.equals(true)))                                                                                                                    //Natural: IF #DUP-FOUND = TRUE
            {
                if (condition(pnd_New_Key.getBoolean()))                                                                                                                  //Natural: IF #NEW-KEY
                {
                    //*          ON CHANGE OF KEY, COMPLETE OLD KEY:
                    if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                           //Natural: IF #COMBINED-CTR GT 1
                    {
                                                                                                                                                                          //Natural: PERFORM REPORT-COMBINED
                        sub_Report_Combined();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Combined_Eoj_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #COMBINED-EOJ-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    //*          NOW PROCESS THE NEW KEY:
                    pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id);                                                            //Natural: MOVE #NEW-IRA-TAX-ID TO #OLD-IRA-TAX-ID
                    pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract);                                                        //Natural: MOVE #NEW-IRA-CONTRACT TO #OLD-IRA-CONTRACT
                    pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee);                                                              //Natural: MOVE #NEW-IRA-PAYEE TO #OLD-IRA-PAYEE
                                                                                                                                                                          //Natural: PERFORM SAVE-IRA-TRAN-DATA
                    sub_Save_Ira_Tran_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM PROCESS-NON-DUPS
            sub_Process_Non_Dups();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------------------------
            if (condition(pnd_New_Key.getBoolean()))                                                                                                                      //Natural: IF #NEW-KEY
            {
                //*     ON CHANGE OF KEY, COMPLETE THE OLD KEY:
                if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                               //Natural: IF #COMBINED-CTR GT 1
                {
                                                                                                                                                                          //Natural: PERFORM REPORT-COMBINED
                    sub_Report_Combined();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Combined_Eoj_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COMBINED-EOJ-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*     NOW PROCESS THE NEW KEY:
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Tax_Id.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Tax_Id);                                                                //Natural: MOVE #NEW-IRA-TAX-ID TO #OLD-IRA-TAX-ID
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Contract.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Contract);                                                            //Natural: MOVE #NEW-IRA-CONTRACT TO #OLD-IRA-CONTRACT
                pnd_Save_Old_Ira_Key_Pnd_Old_Ira_Payee.setValue(pnd_Save_New_Ira_Key_Pnd_New_Ira_Payee);                                                                  //Natural: MOVE #NEW-IRA-PAYEE TO #OLD-IRA-PAYEE
                                                                                                                                                                          //Natural: PERFORM SAVE-IRA-TRAN-DATA
                sub_Save_Ira_Tran_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_New_Key.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #NEW-KEY
            //*  WRITE ' SAURAV1 IN END OF DATA'                      /* SAURAV TESTING
                                                                                                                                                                          //Natural: PERFORM READ-IRA-CHECK-FOR-DUP
            sub_Read_Ira_Check_For_Dup();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Dup_Found.equals(true)))                                                                                                                    //Natural: IF #DUP-FOUND = TRUE
            {
                //*      COMPLETE TRANSACTION.
                if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                               //Natural: IF #COMBINED-CTR GT 1
                {
                                                                                                                                                                          //Natural: PERFORM REPORT-COMBINED
                    sub_Report_Combined();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Combined_Eoj_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COMBINED-EOJ-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ESCAPE BOTTOM;//Natural: END-IF
            //*  IF NOT DUPLICATE:
                                                                                                                                                                          //Natural: PERFORM PROCESS-NON-DUPS
            sub_Process_Non_Dups();
            if (condition(Global.isEscape())) {return;}
            //*     COMPLETE THE TRANSACTION.
            if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                                   //Natural: IF #COMBINED-CTR GT 1
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-COMBINED
                sub_Report_Combined();
                if (condition(Global.isEscape())) {return;}
                pnd_Combined_Eoj_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #COMBINED-EOJ-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *******
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System: IR"));                                                                                          //Natural: COMPRESS 'Source System: IR' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-COMBINATION-TOTALS
        sub_Display_Combination_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM  RESET-CONTROL-TOTALS
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //* *****************************************
        //* *****************************************
        //*  PROCESS CONTRIBUTION COMPOSED OF A SINGLE OR COMBINED RECORD.
        //*   AND TWRC-SIMULATION-DATE      =  IRA-SIMULATION-DATE
        //*   AND TWRC-TAX-ID-TYPE          =  IRA-TAX-ID-TYPE
        //*   AND TWRC-CITIZEN-CODE         =  IRA-CITIZENSHIP-CODE
        //*   AND TWRC-RESIDENCY-CODE       =  IRA-RESIDENCY-CODE
        //*   AND TWRC-RESIDENCY-TYPE       =  IRA-RESIDENCY-TYPE
        //*   AND TWRC-DOB                  =  IRA-DOB
        //*   AND TWRC-DOD                  =  IRA-DOD
        //*   AND TWRC-IRA-CONTRACT-TYPE    =  IRA-FORM-TYPE
        //*   AND TWRC-NAME                 =  IRA-NAME
        //*   AND TWRC-ADDR-1               =  IRA-ADDR-1
        //*   AND TWRC-ADDR-2               =  IRA-ADDR-2
        //*   AND TWRC-ADDR-3               =  IRA-ADDR-3
        //*   AND TWRC-ADDR-4               =  IRA-ADDR-4
        //*   AND TWRC-ADDR-5               =  IRA-ADDR-5
        //*   AND TWRC-ADDR-6               =  IRA-ADDR-6
        //*   AND TWRC-CITY                 =  IRA-CITY
        //*   AND TWRC-STATE                =  IRA-STATE
        //*   AND TWRC-ZIP-CODE             =  IRA-ZIP-CODE
        //*   AND TWRC-ADDR-FOREIGN         =  IRA-FOREIGN-ADDRESS
        //* ***********************************
        //* ************************
        //* ******************************************
        //* ******************************************
        //* ******************************************
        //* ******************************************
        //* ************************************************
        //* ************************************************
        //* ************************************************
        //* ************************************************
        //* ***************************************************
        //* *****************************************
        //* *****************************************
        //* *********************************************
        //* ****************************************
        //* ***************************************
        //* **********************************
        //* *********************************
        //* **********************************
        //* **************************************************
        //* *************************************
        //* **********************************
        //* ********************************************
        //* ********************************************
        //* ******************************************
        //* ******************************************
        //* ******************************************
        //* ******************************************
        //* **************************************
        //* *************************************
        //* ******************
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 02 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 03 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 04 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 05 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 06 )
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 09 )
        //* ***************                                                                                                                                               //Natural: AT TOP OF PAGE ( 10 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //* ***************
        //* ********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* **                                                                                                                                                            //Natural: ON ERROR
    }
    private void sub_Read_Ira_Check_For_Dup() throws Exception                                                                                                            //Natural: READ-IRA-CHECK-FOR-DUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Dup_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #DUP-FOUND := FALSE
        pnd_Fmv_Plus_Or_Minus_10.setValue(false);                                                                                                                         //Natural: ASSIGN #FMV-PLUS-OR-MINUS-10 := FALSE
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                             //Natural: ASSIGN #S2-TAX-YEAR := IRA-TAX-YEAR
        pnd_Twrc_S2_Forms_Pnd_S2_Status.setValue(" ");                                                                                                                    //Natural: ASSIGN #S2-STATUS := ' '
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                                 //Natural: ASSIGN #S2-TAX-ID := IRA-TAX-ID
        pnd_Twrc_S2_Forms_Pnd_S2_Contract.setValue(ldaTwrl462a.getIra_Record_Ira_Contract());                                                                             //Natural: ASSIGN #S2-CONTRACT := IRA-CONTRACT
        pnd_Twrc_S2_Forms_Pnd_S2_Payee.setValue(ldaTwrl462a.getIra_Record_Ira_Payee());                                                                                   //Natural: ASSIGN #S2-PAYEE := IRA-PAYEE
        ldaTwrl462c.getVw_ira().startDatabaseRead                                                                                                                         //Natural: READ IRA WITH TWRC-S2-FORMS = #TWRC-S2-FORMS
        (
        "READ01",
        new Wc[] { new Wc("TWRC_S2_FORMS", ">=", pnd_Twrc_S2_Forms, WcType.BY) },
        new Oc[] { new Oc("TWRC_S2_FORMS", "ASC") }
        );
        READ01:
        while (condition(ldaTwrl462c.getVw_ira().readNextRow("READ01")))
        {
            if (condition(ldaTwrl462c.getIra_Twrc_Tax_Year().equals(ldaTwrl462a.getIra_Record_Ira_Tax_Year()) && ldaTwrl462c.getIra_Twrc_Status().equals(" ")             //Natural: IF TWRC-TAX-YEAR = IRA-TAX-YEAR AND TWRC-STATUS = ' ' AND TWRC-TAX-ID = IRA-TAX-ID AND TWRC-CONTRACT = IRA-CONTRACT AND TWRC-PAYEE = IRA-PAYEE
                && ldaTwrl462c.getIra_Twrc_Tax_Id().equals(ldaTwrl462a.getIra_Record_Ira_Tax_Id()) && ldaTwrl462c.getIra_Twrc_Contract().equals(ldaTwrl462a.getIra_Record_Ira_Contract()) 
                && ldaTwrl462c.getIra_Twrc_Payee().equals(ldaTwrl462a.getIra_Record_Ira_Payee())))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl462c.getIra_Twrc_Company_Cde().equals(ldaTwrl462a.getIra_Record_Ira_Company_Code())))                                                    //Natural: IF TWRC-COMPANY-CDE = IRA-COMPANY-CODE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl462c.getIra_Twrc_Classic_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Classic_Amt()) && ldaTwrl462c.getIra_Twrc_Sep_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Sep_Amt())  //Natural: IF TWRC-CLASSIC-AMT = IRA-CLASSIC-AMT AND TWRC-SEP-AMT = IRA-SEP-AMT AND TWRC-ROTH-AMT = IRA-ROTH-AMT AND TWRC-ROLLOVER-AMT = IRA-ROLLOVER-AMT AND TWRC-RECHAR-AMT = IRA-RECHARACTER-AMT AND TWRC-FAIR-MKT-VAL-AMT = ( IRA-FMV-AMT - 10 ) THRU ( IRA-FMV-AMT + 10 ) AND TWRC-ROTH-CONVERSION-AMT = IRA-ROTH-CONV-AMT
                && ldaTwrl462c.getIra_Twrc_Roth_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Roth_Amt()) && ldaTwrl462c.getIra_Twrc_Rollover_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt()) 
                && ldaTwrl462c.getIra_Twrc_Rechar_Amt().equals(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt())))
            {
                pnd_Dup_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #DUP-FOUND := TRUE
                if (condition(ldaTwrl462c.getIra_Twrc_Fair_Mkt_Val_Amt().notEquals(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt())))                                             //Natural: IF TWRC-FAIR-MKT-VAL-AMT NE IRA-FMV-AMT
                {
                    pnd_Fmv_Plus_Or_Minus_10.setValue(true);                                                                                                              //Natural: ASSIGN #FMV-PLUS-OR-MINUS-10 := TRUE
                    pnd_Fmv_Bypass_Ctr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #FMV-BYPASS-CTR
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #DEBUG
                //*        AND TWRC-FAIR-MKT-VAL-AMT NE IRA-FMV-AMT
                //*      WRITE (0) /
                //*        'TIN             :' IRA-TAX-ID /
                //*        'TIN TYPE        :' IRA-CONTRACT /
                //*        'CONTRACT/PAYEE  :' IRA-PAYEE /
                //*    END-IF
                                                                                                                                                                          //Natural: PERFORM EDITS
                sub_Edits();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Rejected.equals(false)))                                                                                                                //Natural: IF #REJECTED = FALSE
                {
                    j.setValue(2);                                                                                                                                        //Natural: ASSIGN J := 2
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'T'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                        sub_Update_T_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'C'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                            sub_Update_C_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'L'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                                sub_Update_L_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  09-24-03 FRANK
                                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                  //Natural: IF IRA-COMPANY-CODE = 'S'
                                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                    sub_Update_S_Control_Totals();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                    getWorkFiles().write(5, false, ldaTwrl462a.getIra_Record());                                                                                          //Natural: WRITE WORK FILE 05 IRA-RECORD
                                                                                                                                                                          //Natural: PERFORM REPORT-BYPASSED-ALREADY-ON-FILE
                    sub_Report_Bypassed_Already_On_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    j.setValue(3);                                                                                                                                        //Natural: ASSIGN J := 3
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'T'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                        sub_Update_T_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'C'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                            sub_Update_C_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                      //Natural: IF IRA-COMPANY-CODE = 'L'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                                sub_Update_L_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  09-24-03 FRANK
                                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                  //Natural: IF IRA-COMPANY-CODE = 'S'
                                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                                    sub_Update_S_Control_Totals();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Reject_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #REJECT-CTR
                    getWorkFiles().write(4, false, ldaTwrl462a.getIra_Record());                                                                                          //Natural: WRITE WORK FILE 04 IRA-RECORD
                                                                                                                                                                          //Natural: PERFORM REPORT-REJECTED
                    sub_Report_Rejected();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Non_Dups() throws Exception                                                                                                                  //Natural: PROCESS-NON-DUPS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
                                                                                                                                                                          //Natural: PERFORM EDITS
        sub_Edits();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Rejected.equals(false)))                                                                                                                        //Natural: IF #REJECTED = FALSE
        {
            j.setValue(4);                                                                                                                                                //Natural: ASSIGN J := 4
            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                      //Natural: IF IRA-COMPANY-CODE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                    sub_Update_C_Control_Totals();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                        sub_Update_L_Control_Totals();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                            sub_Update_S_Control_Totals();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accept_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ACCEPT-CTR
            getWorkFiles().write(2, false, ldaTwrl462a.getIra_Record());                                                                                                  //Natural: WRITE WORK FILE 02 IRA-RECORD
                                                                                                                                                                          //Natural: PERFORM REPORT-ACCEPTED
            sub_Report_Accepted();
            if (condition(Global.isEscape())) {return;}
            if (condition(l.greater(getZero())))                                                                                                                          //Natural: IF L > 0
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-WARNING
                sub_Report_Warning();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Naa_Found_Save.getBoolean()))                                                                                                               //Natural: IF #NAA-FOUND-SAVE
            {
                ldaTwrl462a.getIra_Record_Ira_Addr_1().reset();                                                                                                           //Natural: RESET IRA-ADDR-1 IRA-ADDR-2 IRA-ADDR-3 IRA-ADDR-4 IRA-ADDR-5 IRA-ADDR-6 IRA-CITY IRA-STATE IRA-ZIP-CODE
                ldaTwrl462a.getIra_Record_Ira_Addr_2().reset();
                ldaTwrl462a.getIra_Record_Ira_Addr_3().reset();
                ldaTwrl462a.getIra_Record_Ira_Addr_4().reset();
                ldaTwrl462a.getIra_Record_Ira_Addr_5().reset();
                ldaTwrl462a.getIra_Record_Ira_Addr_6().reset();
                ldaTwrl462a.getIra_Record_Ira_City().reset();
                ldaTwrl462a.getIra_Record_Ira_State().reset();
                ldaTwrl462a.getIra_Record_Ira_Zip_Code().reset();
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl462a.getIra_Record_Ira_Dod().equals(" ")))                                                                                               //Natural: IF IRA-DOD = ' '
            {
                ldaTwrl462a.getIra_Record_Ira_Dod().setValue("00000000");                                                                                                 //Natural: ASSIGN IRA-DOD := '00000000'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(3, false, ldaTwrl462a.getIra_Record());                                                                                                  //Natural: WRITE WORK FILE 03 IRA-RECORD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            j.setValue(3);                                                                                                                                                //Natural: ASSIGN J := 3
            if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T")))                                                                                      //Natural: IF IRA-COMPANY-CODE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C")))                                                                                  //Natural: IF IRA-COMPANY-CODE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                    sub_Update_C_Control_Totals();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")))                                                                              //Natural: IF IRA-COMPANY-CODE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                        sub_Update_L_Control_Totals();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))                                                                          //Natural: IF IRA-COMPANY-CODE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                            sub_Update_S_Control_Totals();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Reject_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REJECT-CTR
            getWorkFiles().write(4, false, ldaTwrl462a.getIra_Record());                                                                                                  //Natural: WRITE WORK FILE 04 IRA-RECORD
                                                                                                                                                                          //Natural: PERFORM REPORT-REJECTED
            sub_Report_Rejected();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Edits() throws Exception                                                                                                                             //Natural: EDITS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        i.reset();                                                                                                                                                        //Natural: RESET I L #NO-OF-ERRORS #ERROR-MSG ( * ) #WARNING-MSG ( * )
        l.reset();
        pnd_No_Of_Errors.reset();
        pnd_Error_Msg.getValue("*").reset();
        pnd_Warning_Msg.getValue("*").reset();
        pnd_Rejected.setValue(false);                                                                                                                                     //Natural: ASSIGN #REJECTED := FALSE
        //*  EDIT TAX YEAR.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Tax_Year(),"NNNN") && ldaTwrl462a.getIra_Record_Ira_Tax_Year().greaterOrEqual("1998")))           //Natural: IF IRA-TAX-YEAR = MASK ( NNNN ) AND IRA-TAX-YEAR >= '1998'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Tax Year..............................", ldaTwrl462a.getIra_Record_Ira_Tax_Year()));     //Natural: COMPRESS 'ERROR - Invalid Tax Year..............................' IRA-TAX-YEAR INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT COMPANY CODE.
        //* *IF IRA-COMPANY-CODE  =  'C'  OR =  'T'  OR = 'L'
        //*  09-24-03 FC
        if (condition(ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("C") || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("T") || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("L")  //Natural: IF IRA-COMPANY-CODE = 'C' OR = 'T' OR = 'L' OR = 'S'
            || ldaTwrl462a.getIra_Record_Ira_Company_Code().equals("S")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Company Code..........................", ldaTwrl462a.getIra_Record_Ira_Company_Code())); //Natural: COMPRESS 'ERROR - Invalid Company Code..........................' IRA-COMPANY-CODE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT SIMULATION DATE.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Simulation_Date(),"YYYYMMDD")))                                                                   //Natural: IF IRA-SIMULATION-DATE = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Simulation (As Of) Date...............", ldaTwrl462a.getIra_Record_Ira_Simulation_Date())); //Natural: COMPRESS 'ERROR - Invalid Simulation (As Of) Date...............' IRA-SIMULATION-DATE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT TAX ID.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Tax_Id().equals(" ")))                                                                                                //Natural: IF IRA-TAX-ID = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Missing Social Security Number................", ldaTwrl462a.getIra_Record_Ira_Tax_Id()));       //Natural: COMPRESS 'ERROR - Missing Social Security Number................' IRA-TAX-ID INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT TAX ID TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("1") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("2") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("3")  //Natural: IF IRA-TAX-ID-TYPE = '1' OR = '2' OR = '3' OR = '4' OR = '5'
            || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("4") || ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().equals("5")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Tax ID Type...........................", ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type()));  //Natural: COMPRESS 'ERROR - Invalid Tax ID Type...........................' IRA-TAX-ID-TYPE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRACT NUMBER.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Contract().equals(" ")))                                                                                              //Natural: IF IRA-CONTRACT = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Missing Contract Number.......................", ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type()));  //Natural: COMPRESS 'ERROR - Missing Contract Number.......................' IRA-TAX-ID-TYPE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT PAYEE CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Payee().equals("01")))                                                                                                //Natural: IF IRA-PAYEE = '01'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Invalid Payee Code (Defaulted To 01)........", ldaTwrl462a.getIra_Record_Ira_Payee()));      //Natural: COMPRESS 'WARNING - Invalid Payee Code (Defaulted To 01)........' IRA-PAYEE INTO #WARNING-MSG ( L )
            ldaTwrl462a.getIra_Record_Ira_Payee().setValue("01");                                                                                                         //Natural: ASSIGN IRA-PAYEE := '01'
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CITIZENSHIP CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("01") || ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("02") ||                   //Natural: IF IRA-CITIZENSHIP-CODE = '01' OR = '02' OR = '03'
            ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().equals("03")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Citizenship Code......................", ldaTwrl462a.getIra_Record_Ira_Citizenship_Code())); //Natural: COMPRESS 'ERROR - Invalid Citizenship Code......................' IRA-CITIZENSHIP-CODE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RESIDENCY CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Residency_Code().equals(" ")))                                                                                        //Natural: IF IRA-RESIDENCY-CODE = ' '
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Missing Residency Code........................", ldaTwrl462a.getIra_Record_Ira_Residency_Code())); //Natural: COMPRESS 'ERROR - Missing Residency Code........................' IRA-RESIDENCY-CODE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RESIDENCY TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("1") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("2") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("3")  //Natural: IF IRA-RESIDENCY-TYPE = '1' OR = '2' OR = '3' OR = '4' OR = '5'
            || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("4") || ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("5")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Residency Type........................", ldaTwrl462a.getIra_Record_Ira_Residency_Type())); //Natural: COMPRESS 'ERROR - Invalid Residency Type........................' IRA-RESIDENCY-TYPE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT DATE OF BIRTH.
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dob(),"YYYYMMDD")))                                                                               //Natural: IF IRA-DOB = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Missing Date Of Birth.........................", ldaTwrl462a.getIra_Record_Ira_Dob()));          //Natural: COMPRESS 'ERROR - Missing Date Of Birth.........................' IRA-DOB INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FORM 5498.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form().equals("5498      ")))                                                                                         //Natural: IF IRA-FORM = '5498      '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Invalid Form Type (Defaulted To 5498).......", ldaTwrl462a.getIra_Record_Ira_Form()));       //Natural: COMPRESS 'WARNING - Invalid Form Type (Defaulted To 5498).......' IRA-FORM INTO #WARNING-MSG ( L )
            ldaTwrl462a.getIra_Record_Ira_Form().setValue("5498      ");                                                                                                  //Natural: ASSIGN IRA-FORM := '5498      '
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FORM TYPE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("10") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("20") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("30")  //Natural: IF IRA-FORM-TYPE = '10' OR = '20' OR = '30' OR = '40' OR = '50'
            || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("40") || ldaTwrl462a.getIra_Record_Ira_Form_Type().equals("50")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid IRA Form Type.........................", ldaTwrl462a.getIra_Record_Ira_Form_Type()));    //Natural: COMPRESS 'ERROR - Invalid IRA Form Type.........................' IRA-FORM-TYPE INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ORIGINAL, CORRECTION, OR VOID.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Ocv().equals("O") || ldaTwrl462a.getIra_Record_Ira_Form_Ocv().equals("C") || ldaTwrl462a.getIra_Record_Ira_Form_Ocv().equals("V"))) //Natural: IF IRA-FORM-OCV = 'O' OR = 'C' OR = 'V'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Invalid Form OCV Type.......................", ldaTwrl462a.getIra_Record_Ira_Form_Ocv()));   //Natural: COMPRESS 'WARNING - Invalid Form OCV Type.......................' IRA-FORM-OCV INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROLLOVER INDICATOR.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Rollover().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Rollover().equals("Y")))                                      //Natural: IF IRA-ROLLOVER = ' ' OR = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid IRA Rollover Indicator................", ldaTwrl462a.getIra_Record_Ira_Rollover()));     //Natural: COMPRESS 'ERROR - Invalid IRA Rollover Indicator................' IRA-ROLLOVER INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RECHARACTERIZATION INDICATOR.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Recharacter().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Recharacter().equals("Y")))                                //Natural: IF IRA-RECHARACTER = ' ' OR = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid IRA Recharacterization Indicator......", ldaTwrl462a.getIra_Record_Ira_Recharacter()));  //Natural: COMPRESS 'ERROR - Invalid IRA Recharacterization Indicator......' IRA-RECHARACTER INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FORM PERIOD.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P1 ") || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("PP1") || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("P2")  //Natural: IF IRA-FORM-PERIOD = 'P1 ' OR = 'PP1' OR = 'P2' OR = 'PP2'
            || ldaTwrl462a.getIra_Record_Ira_Form_Period().equals("PP2")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid IRA Form Period.......................", ldaTwrl462a.getIra_Record_Ira_Recharacter()));  //Natural: COMPRESS 'ERROR - Invalid IRA Form Period.......................' IRA-RECHARACTER INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CLASSIC AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Classic_Amt().less(new DbsDecimal("0.00"))))                                                                          //Natural: IF IRA-CLASSIC-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Classic Contribution.................", ldaTwrl462a.getIra_Record_Ira_Classic_Amt()));  //Natural: COMPRESS 'ERROR - Negative Classic Contribution.................' IRA-CLASSIC-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROTH AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Roth_Amt().less(new DbsDecimal("0.00"))))                                                                             //Natural: IF IRA-ROTH-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Roth Contribution....................", ldaTwrl462a.getIra_Record_Ira_Roth_Amt()));     //Natural: COMPRESS 'ERROR - Negative Roth Contribution....................' IRA-ROTH-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROLLOVER AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt().less(new DbsDecimal("0.00"))))                                                                         //Natural: IF IRA-ROLLOVER-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Rollover Amount......................", ldaTwrl462a.getIra_Record_Ira_Rollover_Amt())); //Natural: COMPRESS 'ERROR - Negative Rollover Amount......................' IRA-ROLLOVER-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT RECHARACTERIZATION AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt().less(new DbsDecimal("0.00"))))                                                                      //Natural: IF IRA-RECHARACTER-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Recharacterization Amount............", ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt())); //Natural: COMPRESS 'ERROR - Negative Recharacterization Amount............' IRA-RECHARACTER-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT SEP AMOUNT
        if (condition(ldaTwrl462a.getIra_Record_Ira_Sep_Amt().less(new DbsDecimal("0.00"))))                                                                              //Natural: IF IRA-SEP-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative SEP Contribution.....................", ldaTwrl462a.getIra_Record_Ira_Sep_Amt()));      //Natural: COMPRESS 'ERROR - Negative SEP Contribution.....................' IRA-SEP-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT ROTH CONVERSION AMOUNT.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt().less(new DbsDecimal("0.00"))))                                                                        //Natural: IF IRA-ROTH-CONV-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Roth Conversion Amount...............", ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt())); //Natural: COMPRESS 'ERROR - Negative Roth Conversion Amount...............' IRA-ROTH-CONV-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FAIR MARKET VALUE AMOUNT
        if (condition(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt().less(new DbsDecimal("0.00"))))                                                                              //Natural: IF IRA-FMV-AMT < 0.00
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Negative Fair Market Value Amount.............", ldaTwrl462a.getIra_Record_Ira_Fmv_Amt()));      //Natural: COMPRESS 'ERROR - Negative Fair Market Value Amount.............' IRA-FMV-AMT INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRIBUTOR NAME.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Name().equals(" ")))                                                                                                  //Natural: IF IRA-NAME = ' '
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Missing Contributors Name...................", ldaTwrl462a.getIra_Record_Ira_Name()));       //Natural: COMPRESS 'WARNING - Missing Contributors Name...................' IRA-NAME INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRIBUTOR ADDRESS.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Addr_1().equals(" ") && ldaTwrl462a.getIra_Record_Ira_Addr_2().equals(" ") && ldaTwrl462a.getIra_Record_Ira_Addr_3().equals(" ")  //Natural: IF IRA-ADDR-1 = ' ' AND IRA-ADDR-2 = ' ' AND IRA-ADDR-3 = ' ' AND IRA-ADDR-4 = ' ' AND IRA-ADDR-5 = ' ' AND IRA-ADDR-6 = ' '
            && ldaTwrl462a.getIra_Record_Ira_Addr_4().equals(" ") && ldaTwrl462a.getIra_Record_Ira_Addr_5().equals(" ") && ldaTwrl462a.getIra_Record_Ira_Addr_6().equals(" ")))
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Missing Contributors Address................", ldaTwrl462a.getIra_Record_Ira_Addr_1()));     //Natural: COMPRESS 'WARNING - Missing Contributors Address................' IRA-ADDR-1 INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRIBUTORS CITY.
        if (condition(ldaTwrl462a.getIra_Record_Ira_City().equals(" ") && DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Residency_Code(),"NN") &&                     //Natural: IF IRA-CITY = ' ' AND IRA-RESIDENCY-CODE = MASK ( NN ) AND IRA-RESIDENCY-CODE NE '00' AND IRA-RESIDENCY-TYPE = '1'
            ldaTwrl462a.getIra_Record_Ira_Residency_Code().notEquals("00") && ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("1")))
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Missing Contributors City...................", ldaTwrl462a.getIra_Record_Ira_City()));       //Natural: COMPRESS 'WARNING - Missing Contributors City...................' IRA-CITY INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRIBUTORS STATE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_State().equals(" ") && DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Residency_Code(),"NN") &&                    //Natural: IF IRA-STATE = ' ' AND IRA-RESIDENCY-CODE = MASK ( NN ) AND IRA-RESIDENCY-CODE NE '00' AND IRA-RESIDENCY-TYPE = '1'
            ldaTwrl462a.getIra_Record_Ira_Residency_Code().notEquals("00") && ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("1")))
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Missing Contributors State..................", ldaTwrl462a.getIra_Record_Ira_State()));      //Natural: COMPRESS 'WARNING - Missing Contributors State..................' IRA-STATE INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT CONTRIBUTORS ZIP CODE.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Zip_Code().equals(" ") && DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Residency_Code(),"NN")                    //Natural: IF IRA-ZIP-CODE = ' ' AND IRA-RESIDENCY-CODE = MASK ( NN ) AND IRA-RESIDENCY-TYPE = '1'
            && ldaTwrl462a.getIra_Record_Ira_Residency_Type().equals("1")))
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Missing Contributors Zip Code...............", ldaTwrl462a.getIra_Record_Ira_Zip_Code()));   //Natural: COMPRESS 'WARNING - Missing Contributors Zip Code...............' IRA-ZIP-CODE INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT FOREIGN ADDRESS INDICATOR.
        if (condition(ldaTwrl462a.getIra_Record_Ira_Foreign_Address().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Foreign_Address().equals("Y")))                        //Natural: IF IRA-FOREIGN-ADDRESS = ' ' OR = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Of_Warnings.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #NO-OF-WARNINGS
            l.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO L
            pnd_Warning_Msg.getValue(l).setValue(DbsUtil.compress("WARNING - Invalid Foreign Address Indicator...........", ldaTwrl462a.getIra_Record_Ira_Foreign_Address())); //Natural: COMPRESS 'WARNING - Invalid Foreign Address Indicator...........' IRA-FOREIGN-ADDRESS INTO #WARNING-MSG ( L )
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT DATE OF DEATH
        if (condition(ldaTwrl462a.getIra_Record_Ira_Dod().equals(" ") || DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dod(),"YYYYMMDD") || DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Dod(), //Natural: IF IRA-DOD = ' ' OR IRA-DOD = MASK ( YYYYMMDD ) OR IRA-DOD = MASK ( YYYYMM00 )
            "YYYYMM00")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected.setValue(true);                                                                                                                                  //Natural: ASSIGN #REJECTED := TRUE
            pnd_No_Of_Errors.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NO-OF-ERRORS
            i.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO I
            pnd_Error_Msg.getValue(i).setValue(DbsUtil.compress("ERROR - Invalid Date Of Death 'YYYYMMDD'..............", ldaTwrl462a.getIra_Record_Ira_Dod()));          //Natural: COMPRESS 'ERROR - Invalid Date Of Death "YYYYMMDD"..............' IRA-DOD INTO #ERROR-MSG ( I )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pnd_T_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_T_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #T-CLASSIC-AMT ( J )
        pnd_T_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #T-SEP-AMT ( J )
        pnd_T_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #T-ROTH-AMT ( J )
        pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #T-ROLLOVER-AMT ( J )
        pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #T-RECHAR-AMT ( J )
        pnd_T_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #T-FMV-AMT ( J )
        pnd_T_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #T-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                         //Natural: ADD IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                 //Natural: ADD IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                               //Natural: ADD IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                       //Natural: ADD IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                      //Natural: ADD IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                 //Natural: ADD IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                     //Natural: ADD IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_C_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #C-CLASSIC-AMT ( J )
        pnd_C_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #C-SEP-AMT ( J )
        pnd_C_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #C-ROTH-AMT ( J )
        pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #C-ROLLOVER-AMT ( J )
        pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #C-RECHAR-AMT ( J )
        pnd_C_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #C-FMV-AMT ( J )
        pnd_C_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #C-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                         //Natural: ADD IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                 //Natural: ADD IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                               //Natural: ADD IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                       //Natural: ADD IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                      //Natural: ADD IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                 //Natural: ADD IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                     //Natural: ADD IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pnd_L_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_L_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                                  //Natural: ADD IRA-CLASSIC-AMT TO #L-CLASSIC-AMT ( J )
        pnd_L_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                          //Natural: ADD IRA-SEP-AMT TO #L-SEP-AMT ( J )
        pnd_L_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                        //Natural: ADD IRA-ROTH-AMT TO #L-ROTH-AMT ( J )
        pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                                //Natural: ADD IRA-ROLLOVER-AMT TO #L-ROLLOVER-AMT ( J )
        pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                               //Natural: ADD IRA-RECHARACTER-AMT TO #L-RECHAR-AMT ( J )
        pnd_L_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                          //Natural: ADD IRA-FMV-AMT TO #L-FMV-AMT ( J )
        pnd_L_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                              //Natural: ADD IRA-ROTH-CONV-AMT TO #L-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                         //Natural: ADD IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                 //Natural: ADD IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                               //Natural: ADD IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                       //Natural: ADD IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                      //Natural: ADD IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                 //Natural: ADD IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                     //Natural: ADD IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        //*  NORMAL PROCESSING IF NON-COMBINED TRANSACTION.
        //*  TRANSACTION AMOUNT WILL BE ADDED TO THE ACCEPTED OR BYPASSED TOTAL.
        //*  NOT COMBINED.
        if (condition(pnd_Combined_Ctr.equals(1)))                                                                                                                        //Natural: IF #COMBINED-CTR EQ 1
        {
            pnd_S_Trans_Count.getValue(j).nadd(1);                                                                                                                        //Natural: ADD 1 TO #S-TRANS-COUNT ( J )
            pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                               //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
            pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                              //Natural: ADD IRA-CLASSIC-AMT TO #S-CLASSIC-AMT ( J )
            pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                                                      //Natural: ADD IRA-SEP-AMT TO #S-SEP-AMT ( J )
            pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                                    //Natural: ADD IRA-ROTH-AMT TO #S-ROTH-AMT ( J )
            pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                            //Natural: ADD IRA-ROLLOVER-AMT TO #S-ROLLOVER-AMT ( J )
            pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                           //Natural: ADD IRA-RECHARACTER-AMT TO #S-RECHAR-AMT ( J )
            pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                                      //Natural: ADD IRA-FMV-AMT TO #S-FMV-AMT ( J )
            pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                          //Natural: ADD IRA-ROTH-CONV-AMT TO #S-ROTH-CONV-AMT ( J )
            pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                     //Natural: ADD IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
            pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Sep_Amt());                                                             //Natural: ADD IRA-SEP-AMT TO #G-SEP-AMT ( J )
            pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                           //Natural: ADD IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
            pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                   //Natural: ADD IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
            pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                  //Natural: ADD IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
            pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                             //Natural: ADD IRA-FMV-AMT TO #G-FMV-AMT ( J )
            pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                 //Natural: ADD IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
            //*  EVERY COMBINED TRAN HAS A T OR C COMPONENT AND A TCII (S) COMPONENT.
            //*  ADD AMOUNTS WITH COMPANY CODES 'T' OR 'C'TO COMBINED COLUMNS.
            //*  ADD AMOUNTS WITH CODE 'S' TO ACCEPTED, BYPASSED, OR REJECTED COLS.
            //*  ROUTES TOTALS TO THE COMBINED COLUMN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                                   //Natural: IF #COMBINED-CTR GT 1
            {
                x.setValue(3);                                                                                                                                            //Natural: ASSIGN X := 3
                pnd_T_Trans_Count.getValue(x).nadd(ldaTwrl462e.getSvt_Trans_Count());                                                                                     //Natural: ADD SVT-TRANS-COUNT TO #T-TRANS-COUNT ( X )
                pnd_T_Classic_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Classic_Amt());                                                                                     //Natural: ADD SVT-CLASSIC-AMT TO #T-CLASSIC-AMT ( X )
                pnd_T_Roth_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Roth_Amt());                                                                                           //Natural: ADD SVT-ROTH-AMT TO #T-ROTH-AMT ( X )
                pnd_T_Rollover_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Rollover_Amt());                                                                                   //Natural: ADD SVT-ROLLOVER-AMT TO #T-ROLLOVER-AMT ( X )
                pnd_T_Rechar_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Rechar_Amt());                                                                                       //Natural: ADD SVT-RECHAR-AMT TO #T-RECHAR-AMT ( X )
                pnd_T_Sep_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Sep_Amt());                                                                                             //Natural: ADD SVT-SEP-AMT TO #T-SEP-AMT ( X )
                pnd_T_Fmv_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Fmv_Amt());                                                                                             //Natural: ADD SVT-FMV-AMT TO #T-FMV-AMT ( X )
                pnd_T_Roth_Conv_Amt.getValue(x).nadd(ldaTwrl462e.getSvt_Roth_Conv_Amt());                                                                                 //Natural: ADD SVT-ROTH-CONV-AMT TO #T-ROTH-CONV-AMT ( X )
                pnd_G_Accum_Pnd_G_Trans_Count.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Trans_Count());                                                            //Natural: ADD SVT-TRANS-COUNT TO #G-TRANS-COUNT ( X )
                pnd_G_Accum_Pnd_G_Classic_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Classic_Amt());                                                            //Natural: ADD SVT-CLASSIC-AMT TO #G-CLASSIC-AMT ( X )
                pnd_G_Accum_Pnd_G_Roth_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Roth_Amt());                                                                  //Natural: ADD SVT-ROTH-AMT TO #G-ROTH-AMT ( X )
                pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Rollover_Amt());                                                          //Natural: ADD SVT-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( X )
                pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Rechar_Amt());                                                              //Natural: ADD SVT-RECHAR-AMT TO #G-RECHAR-AMT ( X )
                pnd_G_Accum_Pnd_G_Sep_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Sep_Amt());                                                                    //Natural: ADD SVT-SEP-AMT TO #G-SEP-AMT ( X )
                pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Fmv_Amt());                                                                    //Natural: ADD SVT-FMV-AMT TO #G-FMV-AMT ( X )
                pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvt_Roth_Conv_Amt());                                                        //Natural: ADD SVT-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( X )
                pnd_C_Trans_Count.getValue(x).nadd(ldaTwrl462e.getSvc_Trans_Count());                                                                                     //Natural: ADD SVC-TRANS-COUNT TO #C-TRANS-COUNT ( X )
                pnd_C_Classic_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Classic_Amt());                                                                                     //Natural: ADD SVC-CLASSIC-AMT TO #C-CLASSIC-AMT ( X )
                pnd_C_Roth_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Roth_Amt());                                                                                           //Natural: ADD SVC-ROTH-AMT TO #C-ROTH-AMT ( X )
                pnd_C_Rollover_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Rollover_Amt());                                                                                   //Natural: ADD SVC-ROLLOVER-AMT TO #C-ROLLOVER-AMT ( X )
                pnd_C_Rechar_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Rechar_Amt());                                                                                       //Natural: ADD SVC-RECHAR-AMT TO #C-RECHAR-AMT ( X )
                pnd_C_Sep_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Sep_Amt());                                                                                             //Natural: ADD SVC-SEP-AMT TO #C-SEP-AMT ( X )
                pnd_C_Fmv_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Fmv_Amt());                                                                                             //Natural: ADD SVC-FMV-AMT TO #C-FMV-AMT ( X )
                pnd_C_Roth_Conv_Amt.getValue(x).nadd(ldaTwrl462e.getSvc_Roth_Conv_Amt());                                                                                 //Natural: ADD SVC-ROTH-CONV-AMT TO #C-ROTH-CONV-AMT ( X )
                pnd_G_Accum_Pnd_G_Trans_Count.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Trans_Count());                                                            //Natural: ADD SVC-TRANS-COUNT TO #G-TRANS-COUNT ( X )
                pnd_G_Accum_Pnd_G_Classic_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Classic_Amt());                                                            //Natural: ADD SVC-CLASSIC-AMT TO #G-CLASSIC-AMT ( X )
                pnd_G_Accum_Pnd_G_Roth_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Roth_Amt());                                                                  //Natural: ADD SVC-ROTH-AMT TO #G-ROTH-AMT ( X )
                pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Rollover_Amt());                                                          //Natural: ADD SVC-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( X )
                pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Rechar_Amt());                                                              //Natural: ADD SVC-RECHAR-AMT TO #G-RECHAR-AMT ( X )
                pnd_G_Accum_Pnd_G_Sep_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Sep_Amt());                                                                    //Natural: ADD SVC-SEP-AMT TO #G-SEP-AMT ( X )
                pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Fmv_Amt());                                                                    //Natural: ADD SVC-FMV-AMT TO #G-FMV-AMT ( X )
                pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(x.getInt() + 1).nadd(ldaTwrl462e.getSvc_Roth_Conv_Amt());                                                        //Natural: ADD SVC-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( X )
                //*  VALUE OF J WILL ROUTE AMOUNTS TO BYPASSED, ACCEPTED, REJECTED.
                pnd_S_Trans_Count.getValue(j).nadd(ldaTwrl462e.getSvs_Trans_Count());                                                                                     //Natural: ADD SVS-TRANS-COUNT TO #S-TRANS-COUNT ( J )
                pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Classic_Amt());                                                                                     //Natural: ADD SVS-CLASSIC-AMT TO #S-CLASSIC-AMT ( J )
                pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Roth_Amt());                                                                                           //Natural: ADD SVS-ROTH-AMT TO #S-ROTH-AMT ( J )
                pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Rollover_Amt());                                                                                   //Natural: ADD SVS-ROLLOVER-AMT TO #S-ROLLOVER-AMT ( J )
                pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Rechar_Amt());                                                                                       //Natural: ADD SVS-RECHAR-AMT TO #S-RECHAR-AMT ( J )
                pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Sep_Amt());                                                                                             //Natural: ADD SVS-SEP-AMT TO #S-SEP-AMT ( J )
                pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Fmv_Amt());                                                                                             //Natural: ADD SVS-FMV-AMT TO #S-FMV-AMT ( J )
                pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462e.getSvs_Roth_Conv_Amt());                                                                                 //Natural: ADD SVS-ROTH-CONV-AMT TO #S-ROTH-CONV-AMT ( J )
                pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Trans_Count());                                                            //Natural: ADD SVS-TRANS-COUNT TO #G-TRANS-COUNT ( J )
                pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Classic_Amt());                                                            //Natural: ADD SVS-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
                pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Roth_Amt());                                                                  //Natural: ADD SVS-ROTH-AMT TO #G-ROTH-AMT ( J )
                pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Rollover_Amt());                                                          //Natural: ADD SVS-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
                pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Rechar_Amt());                                                              //Natural: ADD SVS-RECHAR-AMT TO #G-RECHAR-AMT ( J )
                pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Sep_Amt());                                                                    //Natural: ADD SVS-SEP-AMT TO #G-SEP-AMT ( J )
                pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Fmv_Amt());                                                                    //Natural: ADD SVS-FMV-AMT TO #G-FMV-AMT ( J )
                pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getSvs_Roth_Conv_Amt());                                                        //Natural: ADD SVS-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
                //*  ADD TO REPORT 10 TIAA, CREF, TCII TOTALS.
                //*  TIAA
                ldaTwrl462e.getCmb_Trans_Count().getValue(1).nadd(ldaTwrl462e.getSvt_Trans_Count());                                                                      //Natural: ADD SVT-TRANS-COUNT TO CMB-TRANS-COUNT ( 1 )
                ldaTwrl462e.getCmb_Classic_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Classic_Amt());                                                                      //Natural: ADD SVT-CLASSIC-AMT TO CMB-CLASSIC-AMT ( 1 )
                ldaTwrl462e.getCmb_Roth_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Roth_Amt());                                                                            //Natural: ADD SVT-ROTH-AMT TO CMB-ROTH-AMT ( 1 )
                ldaTwrl462e.getCmb_Rollover_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Rollover_Amt());                                                                    //Natural: ADD SVT-ROLLOVER-AMT TO CMB-ROLLOVER-AMT ( 1 )
                ldaTwrl462e.getCmb_Rechar_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Rechar_Amt());                                                                        //Natural: ADD SVT-RECHAR-AMT TO CMB-RECHAR-AMT ( 1 )
                ldaTwrl462e.getCmb_Sep_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Sep_Amt());                                                                              //Natural: ADD SVT-SEP-AMT TO CMB-SEP-AMT ( 1 )
                ldaTwrl462e.getCmb_Fmv_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Fmv_Amt());                                                                              //Natural: ADD SVT-FMV-AMT TO CMB-FMV-AMT ( 1 )
                ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(1).nadd(ldaTwrl462e.getSvt_Roth_Conv_Amt());                                                                  //Natural: ADD SVT-ROTH-CONV-AMT TO CMB-ROTH-CONV-AMT ( 1 )
                //*  CREF
                ldaTwrl462e.getCmb_Trans_Count().getValue(2).nadd(ldaTwrl462e.getSvc_Trans_Count());                                                                      //Natural: ADD SVC-TRANS-COUNT TO CMB-TRANS-COUNT ( 2 )
                ldaTwrl462e.getCmb_Classic_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Classic_Amt());                                                                      //Natural: ADD SVC-CLASSIC-AMT TO CMB-CLASSIC-AMT ( 2 )
                ldaTwrl462e.getCmb_Roth_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Roth_Amt());                                                                            //Natural: ADD SVC-ROTH-AMT TO CMB-ROTH-AMT ( 2 )
                ldaTwrl462e.getCmb_Rollover_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Rollover_Amt());                                                                    //Natural: ADD SVC-ROLLOVER-AMT TO CMB-ROLLOVER-AMT ( 2 )
                ldaTwrl462e.getCmb_Rechar_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Rechar_Amt());                                                                        //Natural: ADD SVC-RECHAR-AMT TO CMB-RECHAR-AMT ( 2 )
                ldaTwrl462e.getCmb_Sep_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Sep_Amt());                                                                              //Natural: ADD SVC-SEP-AMT TO CMB-SEP-AMT ( 2 )
                ldaTwrl462e.getCmb_Fmv_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Fmv_Amt());                                                                              //Natural: ADD SVC-FMV-AMT TO CMB-FMV-AMT ( 2 )
                ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(2).nadd(ldaTwrl462e.getSvc_Roth_Conv_Amt());                                                                  //Natural: ADD SVC-ROTH-CONV-AMT TO CMB-ROTH-CONV-AMT ( 2 )
                //*  TCII
                ldaTwrl462e.getCmb_Trans_Count().getValue(3).nadd(ldaTwrl462e.getSvs_Trans_Count());                                                                      //Natural: ADD SVS-TRANS-COUNT TO CMB-TRANS-COUNT ( 3 )
                ldaTwrl462e.getCmb_Classic_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Classic_Amt());                                                                      //Natural: ADD SVS-CLASSIC-AMT TO CMB-CLASSIC-AMT ( 3 )
                ldaTwrl462e.getCmb_Roth_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Roth_Amt());                                                                            //Natural: ADD SVS-ROTH-AMT TO CMB-ROTH-AMT ( 3 )
                ldaTwrl462e.getCmb_Rollover_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Rollover_Amt());                                                                    //Natural: ADD SVS-ROLLOVER-AMT TO CMB-ROLLOVER-AMT ( 3 )
                ldaTwrl462e.getCmb_Rechar_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Rechar_Amt());                                                                        //Natural: ADD SVS-RECHAR-AMT TO CMB-RECHAR-AMT ( 3 )
                ldaTwrl462e.getCmb_Sep_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Sep_Amt());                                                                              //Natural: ADD SVS-SEP-AMT TO CMB-SEP-AMT ( 3 )
                ldaTwrl462e.getCmb_Fmv_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Fmv_Amt());                                                                              //Natural: ADD SVS-FMV-AMT TO CMB-FMV-AMT ( 3 )
                ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(3).nadd(ldaTwrl462e.getSvs_Roth_Conv_Amt());                                                                  //Natural: ADD SVS-ROTH-CONV-AMT TO CMB-ROTH-CONV-AMT ( 3 )
                short decideConditionsMet2079 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #COMPANY-CODE1;//Natural: VALUE 'T'
                if (condition((pnd_Company_Code1.equals("T"))))
                {
                    decideConditionsMet2079++;
                    short decideConditionsMet2081 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #COMPANY-CODE2;//Natural: VALUE 'S'
                    if (condition((pnd_Company_Code2.equals("S"))))
                    {
                        decideConditionsMet2081++;
                        pnd_Combined_Codes_Pnd_Company_Codes_St.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-ST
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((pnd_Company_Code2.equals("C"))))
                    {
                        decideConditionsMet2081++;
                        pnd_Combined_Codes_Pnd_Company_Codes_Tc.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-TC
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Combined_Codes_Pnd_Company_Codes_Other.nadd(1);                                                                                               //Natural: ADD 1 TO #COMPANY-CODES-OTHER
                        getReports().write(0, "=",pnd_Company_Code1);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE1
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Company_Code2);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE2
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                                //Natural: WRITE ( 0 ) '=' IRA-TAX-ID
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Contract());                                                                              //Natural: WRITE ( 0 ) '=' IRA-CONTRACT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Classic_Amt());                                                                           //Natural: WRITE ( 0 ) '=' IRA-CLASSIC-AMT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Roth_Amt());                                                                              //Natural: WRITE ( 0 ) '=' IRA-ROTH-AMT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Rollover_Amt());                                                                          //Natural: WRITE ( 0 ) '=' IRA-ROLLOVER-AMT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt());                                                                       //Natural: WRITE ( 0 ) '=' IRA-RECHARACTER-AMT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Fmv_Amt());                                                                               //Natural: WRITE ( 0 ) '=' IRA-FMV-AMT
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt());                                                                         //Natural: WRITE ( 0 ) '=' IRA-ROTH-CONV-AMT
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'C'
                else if (condition((pnd_Company_Code1.equals("C"))))
                {
                    decideConditionsMet2079++;
                    short decideConditionsMet2100 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #COMPANY-CODE2;//Natural: VALUE 'S'
                    if (condition((pnd_Company_Code2.equals("S"))))
                    {
                        decideConditionsMet2100++;
                        pnd_Combined_Codes_Pnd_Company_Codes_Sc.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-SC
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((pnd_Company_Code2.equals("T"))))
                    {
                        decideConditionsMet2100++;
                        pnd_Combined_Codes_Pnd_Company_Codes_Tc.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-TC
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Combined_Codes_Pnd_Company_Codes_Other.nadd(1);                                                                                               //Natural: ADD 1 TO #COMPANY-CODES-OTHER
                        getReports().write(0, "=",pnd_Company_Code1);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE1
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Company_Code2);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE2
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                                //Natural: WRITE ( 0 ) '=' IRA-TAX-ID
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'S'
                else if (condition((pnd_Company_Code1.equals("S"))))
                {
                    decideConditionsMet2079++;
                    short decideConditionsMet2112 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #COMPANY-CODE2;//Natural: VALUE 'S'
                    if (condition((pnd_Company_Code2.equals("S"))))
                    {
                        decideConditionsMet2112++;
                        pnd_Combined_Codes_Pnd_Company_Codes_Ss.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-SS
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((pnd_Company_Code2.equals("C"))))
                    {
                        decideConditionsMet2112++;
                        pnd_Combined_Codes_Pnd_Company_Codes_Sc.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-SC
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((pnd_Company_Code2.equals("T"))))
                    {
                        decideConditionsMet2112++;
                        pnd_Combined_Codes_Pnd_Company_Codes_St.nadd(1);                                                                                                  //Natural: ADD 1 TO #COMPANY-CODES-ST
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Combined_Codes_Pnd_Company_Codes_Other.nadd(1);                                                                                               //Natural: ADD 1 TO #COMPANY-CODES-OTHER
                        getReports().write(0, "=",pnd_Company_Code1);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE1
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",pnd_Company_Code2);                                                                                                     //Natural: WRITE ( 0 ) '=' #COMPANY-CODE2
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                                //Natural: WRITE ( 0 ) '=' IRA-TAX-ID
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_T_Input_Control_Totals() throws Exception                                                                                                     //Natural: UPDATE-T-INPUT-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        //*  ADD TO INPUT TOTALS WHICH SHOULD MATCH FEEDER CONTROL TOTALS.
        pnd_T_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_T_Classic_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                               //Natural: ADD RD-IRA-CLASSIC-AMT TO #T-CLASSIC-AMT ( J )
        pnd_T_Roth_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                                     //Natural: ADD RD-IRA-ROTH-AMT TO #T-ROTH-AMT ( J )
        pnd_T_Sep_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                                       //Natural: ADD RD-IRA-SEP-AMT TO #T-SEP-AMT ( J )
        pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                             //Natural: ADD RD-IRA-ROLLOVER-AMT TO #T-ROLLOVER-AMT ( J )
        pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                            //Natural: ADD RD-IRA-RECHARACTER-AMT TO #T-RECHAR-AMT ( J )
        pnd_T_Fmv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                                       //Natural: ADD RD-IRA-FMV-AMT TO #T-FMV-AMT ( J )
        pnd_T_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                           //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #T-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                      //Natural: ADD RD-IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                            //Natural: ADD RD-IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                              //Natural: ADD RD-IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                    //Natural: ADD RD-IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                   //Natural: ADD RD-IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                              //Natural: ADD RD-IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                  //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_C_Input_Control_Totals() throws Exception                                                                                                     //Natural: UPDATE-C-INPUT-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        //*  ADD TO INPUT TOTALS WHICH SHOULD MATCH FEEDER CONTROL TOTALS.
        pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_C_Classic_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                               //Natural: ADD RD-IRA-CLASSIC-AMT TO #C-CLASSIC-AMT ( J )
        pnd_C_Roth_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                                     //Natural: ADD RD-IRA-ROTH-AMT TO #C-ROTH-AMT ( J )
        pnd_C_Sep_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                                       //Natural: ADD RD-IRA-SEP-AMT TO #C-SEP-AMT ( J )
        pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                             //Natural: ADD RD-IRA-ROLLOVER-AMT TO #C-ROLLOVER-AMT ( J )
        pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                            //Natural: ADD RD-IRA-RECHARACTER-AMT TO #C-RECHAR-AMT ( J )
        pnd_C_Fmv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                                       //Natural: ADD RD-IRA-FMV-AMT TO #C-FMV-AMT ( J )
        pnd_C_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                           //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #C-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                      //Natural: ADD RD-IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                            //Natural: ADD RD-IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                              //Natural: ADD RD-IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                    //Natural: ADD RD-IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                   //Natural: ADD RD-IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                              //Natural: ADD RD-IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                  //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_L_Input_Control_Totals() throws Exception                                                                                                     //Natural: UPDATE-L-INPUT-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        //*  ADD TO INPUT TOTALS WHICH SHOULD MATCH FEEDER CONTROL TOTALS.
        pnd_L_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_L_Classic_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                               //Natural: ADD RD-IRA-CLASSIC-AMT TO #L-CLASSIC-AMT ( J )
        pnd_L_Roth_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                                     //Natural: ADD RD-IRA-ROTH-AMT TO #L-ROTH-AMT ( J )
        pnd_L_Sep_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                                       //Natural: ADD RD-IRA-SEP-AMT TO #L-SEP-AMT ( J )
        pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                             //Natural: ADD RD-IRA-ROLLOVER-AMT TO #L-ROLLOVER-AMT ( J )
        pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                            //Natural: ADD RD-IRA-RECHARACTER-AMT TO #L-RECHAR-AMT ( J )
        pnd_L_Fmv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                                       //Natural: ADD RD-IRA-FMV-AMT TO #L-FMV-AMT ( J )
        pnd_L_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                           //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #L-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                      //Natural: ADD RD-IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                            //Natural: ADD RD-IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                              //Natural: ADD RD-IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                    //Natural: ADD RD-IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                   //Natural: ADD RD-IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                              //Natural: ADD RD-IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                  //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_S_Input_Control_Totals() throws Exception                                                                                                     //Natural: UPDATE-S-INPUT-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        //*  ADD TO INPUT TOTALS WHICH SHOULD MATCH FEEDER CONTROL TOTALS.
        pnd_S_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #S-TRANS-COUNT ( J )
        pnd_G_Accum_Pnd_G_Trans_Count.getValue(j.getInt() + 1).nadd(1);                                                                                                   //Natural: ADD 1 TO #G-TRANS-COUNT ( J )
        pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                               //Natural: ADD RD-IRA-CLASSIC-AMT TO #S-CLASSIC-AMT ( J )
        pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                                     //Natural: ADD RD-IRA-ROTH-AMT TO #S-ROTH-AMT ( J )
        pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                                       //Natural: ADD RD-IRA-SEP-AMT TO #S-SEP-AMT ( J )
        pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                             //Natural: ADD RD-IRA-ROLLOVER-AMT TO #S-ROLLOVER-AMT ( J )
        pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                            //Natural: ADD RD-IRA-RECHARACTER-AMT TO #S-RECHAR-AMT ( J )
        pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                                       //Natural: ADD RD-IRA-FMV-AMT TO #S-FMV-AMT ( J )
        pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                           //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #S-ROTH-CONV-AMT ( J )
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                      //Natural: ADD RD-IRA-CLASSIC-AMT TO #G-CLASSIC-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                            //Natural: ADD RD-IRA-ROTH-AMT TO #G-ROTH-AMT ( J )
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                              //Natural: ADD RD-IRA-SEP-AMT TO #G-SEP-AMT ( J )
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                    //Natural: ADD RD-IRA-ROLLOVER-AMT TO #G-ROLLOVER-AMT ( J )
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                   //Natural: ADD RD-IRA-RECHARACTER-AMT TO #G-RECHAR-AMT ( J )
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                              //Natural: ADD RD-IRA-FMV-AMT TO #G-FMV-AMT ( J )
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(j.getInt() + 1).nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                  //Natural: ADD RD-IRA-ROTH-CONV-AMT TO #G-ROTH-CONV-AMT ( J )
    }
    private void sub_Update_Control_Totals_New_Record() throws Exception                                                                                                  //Natural: UPDATE-CONTROL-TOTALS-NEW-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************************
        if (condition(pnd_New_Key.getBoolean()))                                                                                                                          //Natural: IF #NEW-KEY
        {
            ldaTwrl462e.getSvt_Trans_Count().reset();                                                                                                                     //Natural: RESET SVT-TRANS-COUNT SVT-CLASSIC-AMT SVT-ROTH-AMT SVT-ROLLOVER-AMT SVT-RECHAR-AMT SVT-SEP-AMT SVT-FMV-AMT SVT-ROTH-CONV-AMT SVC-TRANS-COUNT SVC-CLASSIC-AMT SVC-ROTH-AMT SVC-ROLLOVER-AMT SVC-RECHAR-AMT SVC-SEP-AMT SVC-FMV-AMT SVC-ROTH-CONV-AMT SVS-TRANS-COUNT SVS-CLASSIC-AMT SVS-ROTH-AMT SVS-ROLLOVER-AMT SVS-RECHAR-AMT SVS-SEP-AMT SVS-FMV-AMT SVS-ROTH-CONV-AMT
            ldaTwrl462e.getSvt_Classic_Amt().reset();
            ldaTwrl462e.getSvt_Roth_Amt().reset();
            ldaTwrl462e.getSvt_Rollover_Amt().reset();
            ldaTwrl462e.getSvt_Rechar_Amt().reset();
            ldaTwrl462e.getSvt_Sep_Amt().reset();
            ldaTwrl462e.getSvt_Fmv_Amt().reset();
            ldaTwrl462e.getSvt_Roth_Conv_Amt().reset();
            ldaTwrl462e.getSvc_Trans_Count().reset();
            ldaTwrl462e.getSvc_Classic_Amt().reset();
            ldaTwrl462e.getSvc_Roth_Amt().reset();
            ldaTwrl462e.getSvc_Rollover_Amt().reset();
            ldaTwrl462e.getSvc_Rechar_Amt().reset();
            ldaTwrl462e.getSvc_Sep_Amt().reset();
            ldaTwrl462e.getSvc_Fmv_Amt().reset();
            ldaTwrl462e.getSvc_Roth_Conv_Amt().reset();
            ldaTwrl462e.getSvs_Trans_Count().reset();
            ldaTwrl462e.getSvs_Classic_Amt().reset();
            ldaTwrl462e.getSvs_Roth_Amt().reset();
            ldaTwrl462e.getSvs_Rollover_Amt().reset();
            ldaTwrl462e.getSvs_Rechar_Amt().reset();
            ldaTwrl462e.getSvs_Sep_Amt().reset();
            ldaTwrl462e.getSvs_Fmv_Amt().reset();
            ldaTwrl462e.getSvs_Roth_Conv_Amt().reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("T")))                                                                                       //Natural: IF RD-IRA-COMPANY-CODE EQ 'T'
        {
            ldaTwrl462e.getSvt_Trans_Count().nadd(1);                                                                                                                     //Natural: ADD 1 TO SVT-TRANS-COUNT
            ldaTwrl462e.getSvt_Classic_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                        //Natural: ADD RD-IRA-CLASSIC-AMT TO SVT-CLASSIC-AMT
            ldaTwrl462e.getSvt_Roth_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                              //Natural: ADD RD-IRA-ROTH-AMT TO SVT-ROTH-AMT
            ldaTwrl462e.getSvt_Sep_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                                //Natural: ADD RD-IRA-SEP-AMT TO SVT-SEP-AMT
            ldaTwrl462e.getSvt_Rollover_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                      //Natural: ADD RD-IRA-ROLLOVER-AMT TO SVT-ROLLOVER-AMT
            ldaTwrl462e.getSvt_Rechar_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                     //Natural: ADD RD-IRA-RECHARACTER-AMT TO SVT-RECHAR-AMT
            ldaTwrl462e.getSvt_Fmv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                                //Natural: ADD RD-IRA-FMV-AMT TO SVT-FMV-AMT
            ldaTwrl462e.getSvt_Roth_Conv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                    //Natural: ADD RD-IRA-ROTH-CONV-AMT TO SVT-ROTH-CONV-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("C")))                                                                                   //Natural: IF RD-IRA-COMPANY-CODE EQ 'C'
            {
                ldaTwrl462e.getSvc_Trans_Count().nadd(1);                                                                                                                 //Natural: ADD 1 TO SVC-TRANS-COUNT
                ldaTwrl462e.getSvc_Classic_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                    //Natural: ADD RD-IRA-CLASSIC-AMT TO SVC-CLASSIC-AMT
                ldaTwrl462e.getSvc_Roth_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                          //Natural: ADD RD-IRA-ROTH-AMT TO SVC-ROTH-AMT
                ldaTwrl462e.getSvc_Sep_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                            //Natural: ADD RD-IRA-SEP-AMT TO SVC-SEP-AMT
                ldaTwrl462e.getSvc_Rollover_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                                  //Natural: ADD RD-IRA-ROLLOVER-AMT TO SVC-ROLLOVER-AMT
                ldaTwrl462e.getSvc_Rechar_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                                 //Natural: ADD RD-IRA-RECHARACTER-AMT TO SVC-RECHAR-AMT
                ldaTwrl462e.getSvc_Fmv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                            //Natural: ADD RD-IRA-FMV-AMT TO SVC-FMV-AMT
                ldaTwrl462e.getSvc_Roth_Conv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                                //Natural: ADD RD-IRA-ROTH-CONV-AMT TO SVC-ROTH-CONV-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code().equals("S")))                                                                               //Natural: IF RD-IRA-COMPANY-CODE EQ 'S'
                {
                    ldaTwrl462e.getSvs_Trans_Count().nadd(1);                                                                                                             //Natural: ADD 1 TO SVS-TRANS-COUNT
                    ldaTwrl462e.getSvs_Classic_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                //Natural: ADD RD-IRA-CLASSIC-AMT TO SVS-CLASSIC-AMT
                    ldaTwrl462e.getSvs_Sep_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                        //Natural: ADD RD-IRA-SEP-AMT TO SVS-SEP-AMT
                    ldaTwrl462e.getSvs_Roth_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                      //Natural: ADD RD-IRA-ROTH-AMT TO SVS-ROTH-AMT
                    ldaTwrl462e.getSvs_Rollover_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                              //Natural: ADD RD-IRA-ROLLOVER-AMT TO SVS-ROLLOVER-AMT
                    ldaTwrl462e.getSvs_Rechar_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                             //Natural: ADD RD-IRA-RECHARACTER-AMT TO SVS-RECHAR-AMT
                    ldaTwrl462e.getSvs_Fmv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                        //Natural: ADD RD-IRA-FMV-AMT TO SVS-FMV-AMT
                    ldaTwrl462e.getSvs_Roth_Conv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                            //Natural: ADD RD-IRA-ROTH-CONV-AMT TO SVS-ROTH-CONV-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_G_Accum_Pnd_G_Control.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Control.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Records_Tot().add(ldaTwrl462d.getPnd_C_Irat_Records_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Records_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Records_Tot())); //Natural: ASSIGN #G-CONTROL ( 0 ) := #T.IRAT-RECORDS-TOT + #C.IRAT-RECORDS-TOT + #L.IRAT-RECORDS-TOT + #S.IRAT-RECORDS-TOT
        pnd_G_Accum_Pnd_G_Classic_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Classic_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Classic_Tot().add(ldaTwrl462d.getPnd_C_Irat_Classic_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Classic_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Classic_Tot())); //Natural: ASSIGN #G-CLASSIC-AMT ( 0 ) := #T.IRAT-CLASSIC-TOT + #C.IRAT-CLASSIC-TOT + #L.IRAT-CLASSIC-TOT + #S.IRAT-CLASSIC-TOT
        pnd_G_Accum_Pnd_G_Roth_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Roth_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Roth_Tot().add(ldaTwrl462d.getPnd_C_Irat_Roth_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Roth_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Roth_Tot())); //Natural: ASSIGN #G-ROTH-AMT ( 0 ) := #T.IRAT-ROTH-TOT + #C.IRAT-ROTH-TOT + #L.IRAT-ROTH-TOT + #S.IRAT-ROTH-TOT
        pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Rollover_Tot().add(ldaTwrl462d.getPnd_C_Irat_Rollover_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Rollover_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Rollover_Tot())); //Natural: ASSIGN #G-ROLLOVER-AMT ( 0 ) := #T.IRAT-ROLLOVER-TOT + #C.IRAT-ROLLOVER-TOT + #L.IRAT-ROLLOVER-TOT + #S.IRAT-ROLLOVER-TOT
        pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Rechar_Tot().add(ldaTwrl462d.getPnd_C_Irat_Rechar_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Rechar_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Rechar_Tot())); //Natural: ASSIGN #G-RECHAR-AMT ( 0 ) := #T.IRAT-RECHAR-TOT + #C.IRAT-RECHAR-TOT + #L.IRAT-RECHAR-TOT + #S.IRAT-RECHAR-TOT
        pnd_G_Accum_Pnd_G_Sep_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Sep_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Sep_Tot().add(ldaTwrl462d.getPnd_C_Irat_Sep_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Sep_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Sep_Tot())); //Natural: ASSIGN #G-SEP-AMT ( 0 ) := #T.IRAT-SEP-TOT + #C.IRAT-SEP-TOT + #L.IRAT-SEP-TOT + #S.IRAT-SEP-TOT
        pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Fmv_Tot().add(ldaTwrl462d.getPnd_C_Irat_Fmv_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Fmv_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Fmv_Tot())); //Natural: ASSIGN #G-FMV-AMT ( 0 ) := #T.IRAT-FMV-TOT + #C.IRAT-FMV-TOT + #L.IRAT-FMV-TOT + #S.IRAT-FMV-TOT
        pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(0 + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(0 + 1)), ldaTwrl462d.getPnd_T_Irat_Classic_To_Roth_Tot().add(ldaTwrl462d.getPnd_C_Irat_Classic_To_Roth_Tot()).add(ldaTwrl462d.getPnd_L_Irat_Classic_To_Roth_Tot()).add(ldaTwrl462d.getPnd_S_Irat_Classic_To_Roth_Tot())); //Natural: ASSIGN #G-ROTH-CONV-AMT ( 0 ) := #T.IRAT-CLASSIC-TO-ROTH-TOT + #C.IRAT-CLASSIC-TO-ROTH-TOT + #L.IRAT-CLASSIC-TO-ROTH-TOT + #S.IRAT-CLASSIC-TO-ROTH-TOT
        FOR01:                                                                                                                                                            //Natural: FOR #NDX 1 4
        for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(4)); pnd_Ndx.nadd(1))
        {
            pnd_G_Accum_Pnd_G_Control.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Control.getValue(pnd_Ndx.getInt()             //Natural: ASSIGN #G-CONTROL ( #NDX ) := #T-TRANS-COUNT ( #NDX ) + #C-TRANS-COUNT ( #NDX ) + #L-TRANS-COUNT ( #NDX ) + #S-TRANS-COUNT ( #NDX )
                + 1)), pnd_T_Trans_Count.getValue(pnd_Ndx).add(pnd_C_Trans_Count.getValue(pnd_Ndx)).add(pnd_L_Trans_Count.getValue(pnd_Ndx)).add(pnd_S_Trans_Count.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Classic_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Classic_Amt.getValue(pnd_Ndx.getInt()     //Natural: ASSIGN #G-CLASSIC-AMT ( #NDX ) := #T-CLASSIC-AMT ( #NDX ) + #C-CLASSIC-AMT ( #NDX ) + #L-CLASSIC-AMT ( #NDX ) + #S-CLASSIC-AMT ( #NDX )
                + 1)), pnd_T_Classic_Amt.getValue(pnd_Ndx).add(pnd_C_Classic_Amt.getValue(pnd_Ndx)).add(pnd_L_Classic_Amt.getValue(pnd_Ndx)).add(pnd_S_Classic_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Roth_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Roth_Amt.getValue(pnd_Ndx.getInt()           //Natural: ASSIGN #G-ROTH-AMT ( #NDX ) := #T-ROTH-AMT ( #NDX ) + #C-ROTH-AMT ( #NDX ) + #L-ROTH-AMT ( #NDX ) + #S-ROTH-AMT ( #NDX )
                + 1)), pnd_T_Roth_Amt.getValue(pnd_Ndx).add(pnd_C_Roth_Amt.getValue(pnd_Ndx)).add(pnd_L_Roth_Amt.getValue(pnd_Ndx)).add(pnd_S_Roth_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(pnd_Ndx.getInt()   //Natural: ASSIGN #G-ROLLOVER-AMT ( #NDX ) := #T-ROLLOVER-AMT ( #NDX ) + #C-ROLLOVER-AMT ( #NDX ) + #L-ROLLOVER-AMT ( #NDX ) + #S-ROLLOVER-AMT ( #NDX )
                + 1)), pnd_T_Rollover_Amt.getValue(pnd_Ndx).add(pnd_C_Rollover_Amt.getValue(pnd_Ndx)).add(pnd_L_Rollover_Amt.getValue(pnd_Ndx)).add(pnd_S_Rollover_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(pnd_Ndx.getInt()       //Natural: ASSIGN #G-RECHAR-AMT ( #NDX ) := #T-RECHAR-AMT ( #NDX ) + #C-RECHAR-AMT ( #NDX ) + #L-RECHAR-AMT ( #NDX ) + #S-RECHAR-AMT ( #NDX )
                + 1)), pnd_T_Rechar_Amt.getValue(pnd_Ndx).add(pnd_C_Rechar_Amt.getValue(pnd_Ndx)).add(pnd_L_Rechar_Amt.getValue(pnd_Ndx)).add(pnd_S_Rechar_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Sep_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Sep_Amt.getValue(pnd_Ndx.getInt()             //Natural: ASSIGN #G-SEP-AMT ( #NDX ) := #T-SEP-AMT ( #NDX ) + #C-SEP-AMT ( #NDX ) + #L-SEP-AMT ( #NDX ) + #S-SEP-AMT ( #NDX )
                + 1)), pnd_T_Sep_Amt.getValue(pnd_Ndx).add(pnd_C_Sep_Amt.getValue(pnd_Ndx)).add(pnd_L_Sep_Amt.getValue(pnd_Ndx)).add(pnd_S_Sep_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(pnd_Ndx.getInt()             //Natural: ASSIGN #G-FMV-AMT ( #NDX ) := #T-FMV-AMT ( #NDX ) + #C-FMV-AMT ( #NDX ) + #L-FMV-AMT ( #NDX ) + #S-FMV-AMT ( #NDX )
                + 1)), pnd_T_Fmv_Amt.getValue(pnd_Ndx).add(pnd_C_Fmv_Amt.getValue(pnd_Ndx)).add(pnd_L_Fmv_Amt.getValue(pnd_Ndx)).add(pnd_S_Fmv_Amt.getValue(pnd_Ndx)));
            pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(pnd_Ndx.getInt() + 1).compute(new ComputeParameters(false, pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(pnd_Ndx.getInt() //Natural: ASSIGN #G-ROTH-CONV-AMT ( #NDX ) := #T-ROTH-CONV-AMT ( #NDX ) + #C-ROTH-CONV-AMT ( #NDX ) + #L-ROTH-CONV-AMT ( #NDX ) + #S-ROTH-CONV-AMT ( #NDX )
                + 1)), pnd_T_Roth_Conv_Amt.getValue(pnd_Ndx).add(pnd_C_Roth_Conv_Amt.getValue(pnd_Ndx)).add(pnd_L_Roth_Conv_Amt.getValue(pnd_Ndx)).add(pnd_S_Roth_Conv_Amt.getValue(pnd_Ndx)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 04 )
        if (condition(Global.isEscape())){return;}
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 04 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(29),ldaTwrl462d.getPnd_T_Irat_Records_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Transaction Count...' 29T #T.IRAT-RECORDS-TOT 54T #T-TRANS-COUNT ( I1 ) 75T #T-TRANS-COUNT ( I2 ) 96T #T-TRANS-COUNT ( I3 ) 117T #T-TRANS-COUNT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(54),pnd_T_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_T_Trans_Count.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_T_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(117),pnd_T_Trans_Count.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Classic_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Classic Contrib.....' 26T #T.IRAT-CLASSIC-TOT 47T #T-CLASSIC-AMT ( I1 ) 68T #T-CLASSIC-AMT ( I2 ) 89T #T-CLASSIC-AMT ( I3 ) 110T #T-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_T_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Classic_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Classic_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Roth_Tot(), new ReportEditMask                     //Natural: WRITE ( 04 ) NOTITLE 'Roth Contrib........' 26T #T.IRAT-ROTH-TOT 47T #T-ROTH-AMT ( I1 ) 68T #T-ROTH-AMT ( I2 ) 89T #T-ROTH-AMT ( I3 ) 110T #T-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_T_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Roth_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Rollover_Tot(), new ReportEditMask                 //Natural: WRITE ( 04 ) NOTITLE 'Rollover............' 26T #T.IRAT-ROLLOVER-TOT 47T #T-ROLLOVER-AMT ( I1 ) 68T #T-ROLLOVER-AMT ( I2 ) 89T #T-ROLLOVER-AMT ( I3 ) 110T #T-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_T_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rollover_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Rollover_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Classic_To_Roth_Tot(), new ReportEditMask          //Natural: WRITE ( 04 ) NOTITLE 'Roth Conversion.....' 26T #T.IRAT-CLASSIC-TO-ROTH-TOT 47T #T-ROTH-CONV-AMT ( I1 ) 68T #T-ROTH-CONV-AMT ( I2 ) 89T #T-ROTH-CONV-AMT ( I3 ) 110T #T-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_T_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Roth_Conv_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Roth_Conv_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Rechar_Tot(), new ReportEditMask                   //Natural: WRITE ( 04 ) NOTITLE 'Recharacter.........' 26T #T.IRAT-RECHAR-TOT 47T #T-RECHAR-AMT ( I1 ) 68T #T-RECHAR-AMT ( I2 ) 89T #T-RECHAR-AMT ( I3 ) 110T #T-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_T_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Rechar_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Rechar_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Sep_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'SEP.................' 26T #T.IRAT-SEP-TOT 47T #T-SEP-AMT ( I1 ) 68T #T-SEP-AMT ( I2 ) 89T #T-SEP-AMT ( I3 ) 110T #T-SEP-AMT ( I4 )
            TabSetting(47),pnd_T_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_T_Sep_Amt.getValue(i2), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_T_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_T_Sep_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),ldaTwrl462d.getPnd_T_Irat_Fmv_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'Fair Market.........' 26T #T.IRAT-FMV-TOT 46T #T-FMV-AMT ( I1 ) 67T #T-FMV-AMT ( I2 ) 88T #T-FMV-AMT ( I3 ) 109T #T-FMV-AMT ( I4 )
            TabSetting(46),pnd_T_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_T_Fmv_Amt.getValue(i2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_T_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(109),pnd_T_Fmv_Amt.getValue(i4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 04 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(29),ldaTwrl462d.getPnd_C_Irat_Records_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Transaction Count...' 29T #C.IRAT-RECORDS-TOT 54T #C-TRANS-COUNT ( I1 ) 75T #C-TRANS-COUNT ( I2 ) 96T #C-TRANS-COUNT ( I3 ) 117T #C-TRANS-COUNT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(54),pnd_C_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_C_Trans_Count.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_C_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(117),pnd_C_Trans_Count.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Classic_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Classic Contrib.....' 26T #C.IRAT-CLASSIC-TOT 47T #C-CLASSIC-AMT ( I1 ) 68T #C-CLASSIC-AMT ( I2 ) 89T #C-CLASSIC-AMT ( I3 ) 110T #C-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_C_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Classic_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Classic_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Roth_Tot(), new ReportEditMask                     //Natural: WRITE ( 04 ) NOTITLE 'Roth Contrib........' 26T #C.IRAT-ROTH-TOT 47T #C-ROTH-AMT ( I1 ) 68T #C-ROTH-AMT ( I2 ) 89T #C-ROTH-AMT ( I3 ) 110T #C-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_C_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Roth_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Rollover_Tot(), new ReportEditMask                 //Natural: WRITE ( 04 ) NOTITLE 'Rollover............' 26T #C.IRAT-ROLLOVER-TOT 47T #C-ROLLOVER-AMT ( I1 ) 68T #C-ROLLOVER-AMT ( I2 ) 89T #C-ROLLOVER-AMT ( I3 ) 110T #C-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_C_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rollover_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Rollover_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Classic_To_Roth_Tot(), new ReportEditMask          //Natural: WRITE ( 04 ) NOTITLE 'Roth Conversion.....' 26T #C.IRAT-CLASSIC-TO-ROTH-TOT 47T #C-ROTH-CONV-AMT ( I1 ) 68T #C-ROTH-CONV-AMT ( I2 ) 89T #C-ROTH-CONV-AMT ( I3 ) 110T #C-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_C_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Roth_Conv_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Roth_Conv_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Rechar_Tot(), new ReportEditMask                   //Natural: WRITE ( 04 ) NOTITLE 'Recharacter.........' 26T #C.IRAT-RECHAR-TOT 47T #C-RECHAR-AMT ( I1 ) 68T #C-RECHAR-AMT ( I2 ) 89T #C-RECHAR-AMT ( I3 ) 110T #C-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_C_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Rechar_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Rechar_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Sep_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'SEP.................' 26T #C.IRAT-SEP-TOT 47T #C-SEP-AMT ( I1 ) 68T #C-SEP-AMT ( I2 ) 89T #C-SEP-AMT ( I3 ) 110T #C-SEP-AMT ( I4 )
            TabSetting(47),pnd_C_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_C_Sep_Amt.getValue(i2), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_C_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_C_Sep_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),ldaTwrl462d.getPnd_C_Irat_Fmv_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'Fair Market.........' 26T #C.IRAT-FMV-TOT 46T #C-FMV-AMT ( I1 ) 67T #C-FMV-AMT ( I2 ) 88T #C-FMV-AMT ( I3 ) 109T #C-FMV-AMT ( I4 )
            TabSetting(46),pnd_C_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_C_Fmv_Amt.getValue(i2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_C_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(109),pnd_C_Fmv_Amt.getValue(i4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 04 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(29),ldaTwrl462d.getPnd_L_Irat_Records_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Transaction Count...' 29T #L.IRAT-RECORDS-TOT 54T #L-TRANS-COUNT ( I1 ) 75T #L-TRANS-COUNT ( I2 ) 96T #L-TRANS-COUNT ( I3 ) 117T #L-TRANS-COUNT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(54),pnd_L_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_L_Trans_Count.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_L_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(117),pnd_L_Trans_Count.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Classic_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Classic Contrib.....' 26T #L.IRAT-CLASSIC-TOT 47T #L-CLASSIC-AMT ( I1 ) 68T #L-CLASSIC-AMT ( I2 ) 89T #L-CLASSIC-AMT ( I3 ) 110T #L-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_L_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Classic_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Classic_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Roth_Tot(), new ReportEditMask                     //Natural: WRITE ( 04 ) NOTITLE 'Roth Contrib........' 26T #L.IRAT-ROTH-TOT 47T #L-ROTH-AMT ( I1 ) 68T #L-ROTH-AMT ( I2 ) 89T #L-ROTH-AMT ( I3 ) 110T #L-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_L_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Roth_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Rollover_Tot(), new ReportEditMask                 //Natural: WRITE ( 04 ) NOTITLE 'Rollover............' 26T #L.IRAT-ROLLOVER-TOT 47T #L-ROLLOVER-AMT ( I1 ) 68T #L-ROLLOVER-AMT ( I2 ) 89T #L-ROLLOVER-AMT ( I3 ) 110T #L-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_L_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rollover_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Rollover_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Classic_To_Roth_Tot(), new ReportEditMask          //Natural: WRITE ( 04 ) NOTITLE 'Roth Conversion.....' 26T #L.IRAT-CLASSIC-TO-ROTH-TOT 47T #L-ROTH-CONV-AMT ( I1 ) 68T #L-ROTH-CONV-AMT ( I2 ) 89T #L-ROTH-CONV-AMT ( I3 ) 110T #L-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_L_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Roth_Conv_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Roth_Conv_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Rechar_Tot(), new ReportEditMask                   //Natural: WRITE ( 04 ) NOTITLE 'Recharacter.........' 26T #L.IRAT-RECHAR-TOT 47T #L-RECHAR-AMT ( I1 ) 68T #L-RECHAR-AMT ( I2 ) 89T #L-RECHAR-AMT ( I3 ) 110T #L-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_L_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Rechar_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Rechar_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Sep_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'SEP.................' 26T #L.IRAT-SEP-TOT 47T #L-SEP-AMT ( I1 ) 68T #L-SEP-AMT ( I2 ) 89T #L-SEP-AMT ( I3 ) 110T #L-SEP-AMT ( I4 )
            TabSetting(47),pnd_L_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_L_Sep_Amt.getValue(i2), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_L_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_L_Sep_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),ldaTwrl462d.getPnd_L_Irat_Fmv_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'Fair Market.........' 26T #L.IRAT-FMV-TOT 46T #L-FMV-AMT ( I1 ) 67T #L-FMV-AMT ( I2 ) 88T #L-FMV-AMT ( I3 ) 109T #L-FMV-AMT ( I4 )
            TabSetting(46),pnd_L_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_L_Fmv_Amt.getValue(i2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_L_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(109),pnd_L_Fmv_Amt.getValue(i4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 04 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(29),ldaTwrl462d.getPnd_S_Irat_Records_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Transaction Count...' 29T #S.IRAT-RECORDS-TOT 54T #S-TRANS-COUNT ( I1 ) 75T #S-TRANS-COUNT ( I2 ) 96T #S-TRANS-COUNT ( I3 ) 117T #S-TRANS-COUNT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(54),pnd_S_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_S_Trans_Count.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_S_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(117),pnd_S_Trans_Count.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Classic_Tot(), new ReportEditMask                  //Natural: WRITE ( 04 ) NOTITLE 'Classic Contrib.....' 26T #S.IRAT-CLASSIC-TOT 47T #S-CLASSIC-AMT ( I1 ) 68T #S-CLASSIC-AMT ( I2 ) 89T #S-CLASSIC-AMT ( I3 ) 110T #S-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_S_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Classic_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Classic_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Roth_Tot(), new ReportEditMask                     //Natural: WRITE ( 04 ) NOTITLE 'Roth Contrib........' 26T #S.IRAT-ROTH-TOT 47T #S-ROTH-AMT ( I1 ) 68T #S-ROTH-AMT ( I2 ) 89T #S-ROTH-AMT ( I3 ) 110T #S-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_S_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Roth_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Rollover_Tot(), new ReportEditMask                 //Natural: WRITE ( 04 ) NOTITLE 'Rollover............' 26T #S.IRAT-ROLLOVER-TOT 47T #S-ROLLOVER-AMT ( I1 ) 68T #S-ROLLOVER-AMT ( I2 ) 89T #S-ROLLOVER-AMT ( I3 ) 110T #S-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_S_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rollover_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Rollover_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Classic_To_Roth_Tot(), new ReportEditMask          //Natural: WRITE ( 04 ) NOTITLE 'Roth Conversion.....' 26T #S.IRAT-CLASSIC-TO-ROTH-TOT 47T #S-ROTH-CONV-AMT ( I1 ) 68T #S-ROTH-CONV-AMT ( I2 ) 89T #S-ROTH-CONV-AMT ( I3 ) 110T #S-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_S_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Roth_Conv_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Roth_Conv_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Rechar_Tot(), new ReportEditMask                   //Natural: WRITE ( 04 ) NOTITLE 'Recharacter.........' 26T #S.IRAT-RECHAR-TOT 47T #S-RECHAR-AMT ( I1 ) 68T #S-RECHAR-AMT ( I2 ) 89T #S-RECHAR-AMT ( I3 ) 110T #S-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(47),pnd_S_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Rechar_Amt.getValue(i2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Rechar_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Sep_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'SEP.................' 26T #S.IRAT-SEP-TOT 47T #S-SEP-AMT ( I1 ) 68T #S-SEP-AMT ( I2 ) 89T #S-SEP-AMT ( I3 ) 110T #S-SEP-AMT ( I4 )
            TabSetting(47),pnd_S_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_S_Sep_Amt.getValue(i2), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_S_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_S_Sep_Amt.getValue(i4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),ldaTwrl462d.getPnd_S_Irat_Fmv_Tot(), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"),new  //Natural: WRITE ( 04 ) NOTITLE 'Fair Market.........' 26T #S.IRAT-FMV-TOT 46T #S-FMV-AMT ( I1 ) 67T #S-FMV-AMT ( I2 ) 88T #S-FMV-AMT ( I3 ) 109T #S-FMV-AMT ( I4 )
            TabSetting(46),pnd_S_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_S_Fmv_Amt.getValue(i2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_S_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(109),pnd_S_Fmv_Amt.getValue(i4), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"   (Grand Total) ",NEWLINE);                                                                        //Natural: WRITE ( 04 ) NOTITLE 01T '   (Grand Total) ' /
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(33),pnd_G_Accum_Pnd_G_Control.getValue(0 + 1), new ReportEditMask                //Natural: WRITE ( 04 ) NOTITLE 'Transaction Count...' 33T #G-CONTROL ( 0 ) 54T #G-TRANS-COUNT ( I1 ) 75T #G-TRANS-COUNT ( I2 ) 96T #G-TRANS-COUNT ( I3 ) 117T #G-TRANS-COUNT ( I4 )
            ("Z,ZZZ,ZZ9"),new TabSetting(54),pnd_G_Accum_Pnd_G_Trans_Count.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(75),pnd_G_Accum_Pnd_G_Trans_Count.getValue(i2.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(96),pnd_G_Accum_Pnd_G_Trans_Count.getValue(i3.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            TabSetting(117),pnd_G_Accum_Pnd_G_Trans_Count.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_G_Accum_Pnd_G_Classic_Amt.getValue(0 + 1), new ReportEditMask            //Natural: WRITE ( 04 ) NOTITLE 'Classic Contrib.....' 26T #G-CLASSIC-AMT ( 0 ) 47T #G-CLASSIC-AMT ( I1 ) 68T #G-CLASSIC-AMT ( I2 ) 89T #G-CLASSIC-AMT ( I3 ) 110T #G-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Classic_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(68),pnd_G_Accum_Pnd_G_Classic_Amt.getValue(i2.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Classic_Amt.getValue(i3.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_G_Accum_Pnd_G_Classic_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_G_Accum_Pnd_G_Roth_Amt.getValue(0 + 1), new ReportEditMask               //Natural: WRITE ( 04 ) NOTITLE 'Roth Contrib........' 26T #G-ROTH-AMT ( 0 ) 47T #G-ROTH-AMT ( I1 ) 68T #G-ROTH-AMT ( I2 ) 89T #G-ROTH-AMT ( I3 ) 110T #G-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Roth_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_G_Accum_Pnd_G_Roth_Amt.getValue(i2.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Roth_Amt.getValue(i3.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(110),pnd_G_Accum_Pnd_G_Roth_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(0 + 1), new ReportEditMask           //Natural: WRITE ( 04 ) NOTITLE 'Rollover............' 26T #G-ROLLOVER-AMT ( 0 ) 47T #G-ROLLOVER-AMT ( I1 ) 68T #G-ROLLOVER-AMT ( I2 ) 89T #G-ROLLOVER-AMT ( I3 ) 110T #G-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(68),pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(i2.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(i3.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_G_Accum_Pnd_G_Rollover_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(0 + 1), new ReportEditMask          //Natural: WRITE ( 04 ) NOTITLE 'Roth Conversion.....' 26T #G-ROTH-CONV-AMT ( 0 ) 47T #G-ROTH-CONV-AMT ( I1 ) 68T #G-ROTH-CONV-AMT ( I2 ) 89T #G-ROTH-CONV-AMT ( I3 ) 110T #G-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(68),pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(i2.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(i3.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_G_Accum_Pnd_G_Roth_Conv_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(0 + 1), new ReportEditMask             //Natural: WRITE ( 04 ) NOTITLE 'Recharacter.........' 26T #G-RECHAR-AMT ( 0 ) 47T #G-RECHAR-AMT ( I1 ) 68T #G-RECHAR-AMT ( I2 ) 89T #G-RECHAR-AMT ( I3 ) 110T #G-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(68),pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(i2.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(i3.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_G_Accum_Pnd_G_Rechar_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_G_Accum_Pnd_G_Sep_Amt.getValue(0 + 1), new ReportEditMask                //Natural: WRITE ( 04 ) NOTITLE 'SEP.................' 26T #G-SEP-AMT ( 0 ) 47T #G-SEP-AMT ( I1 ) 68T #G-SEP-AMT ( I2 ) 89T #G-SEP-AMT ( I3 ) 110T #G-SEP-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),pnd_G_Accum_Pnd_G_Sep_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(68),pnd_G_Accum_Pnd_G_Sep_Amt.getValue(i2.getInt() + 1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_G_Accum_Pnd_G_Sep_Amt.getValue(i3.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(110),pnd_G_Accum_Pnd_G_Sep_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        getReports().write(4, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(0 + 1), new ReportEditMask                //Natural: WRITE ( 04 ) NOTITLE 'Fair Market.........' 25T #G-FMV-AMT ( 0 ) 46T #G-FMV-AMT ( I1 ) 67T #G-FMV-AMT ( I2 ) 88T #G-FMV-AMT ( I3 ) 109T #G-FMV-AMT ( I4 )
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(46),pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(i1.getInt() + 1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(67),pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(i2.getInt() + 1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(88),pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(i3.getInt() + 1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(109),pnd_G_Accum_Pnd_G_Fmv_Amt.getValue(i4.getInt() + 1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        pnd_Diff1.compute(new ComputeParameters(false, pnd_Diff1), pnd_G_Accum_Pnd_G_Control.getValue(0 + 1).subtract(pnd_G_Accum_Pnd_G_Control.getValue(1                //Natural: ASSIGN #DIFF1 := #G-CONTROL ( 0 ) - #G-CONTROL ( 1 )
            + 1)));
        pnd_Tot.compute(new ComputeParameters(false, pnd_Tot), pnd_G_Accum_Pnd_G_Control.getValue(2 + 1).add(pnd_G_Accum_Pnd_G_Control.getValue(3 + 1)).add(pnd_G_Accum_Pnd_G_Control.getValue(4  //Natural: ASSIGN #TOT := #G-CONTROL ( 2 ) + #G-CONTROL ( 3 ) + #G-CONTROL ( 4 )
            + 1)));
        pnd_Diff2.compute(new ComputeParameters(false, pnd_Diff2), pnd_G_Accum_Pnd_G_Control.getValue(0 + 1).subtract(pnd_Tot));                                          //Natural: ASSIGN #DIFF2 := #G-CONTROL ( 0 ) - #TOT
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"  (Auto Reconcile)",NEWLINE,"Control.............           ",pnd_G_Accum_Pnd_G_Control.getValue(0 + 1),  //Natural: WRITE ( 04 ) NOTITLE 01T '  (Auto Reconcile)' / 'Control.............           ' #G-CONTROL ( 0 ) / 'Input...............           ' #G-CONTROL ( 1 ) / 'Difference..........           ' #DIFF1 // 'Bypassed............           ' #G-CONTROL ( 2 ) / 'Combined/Rejected...           ' #G-CONTROL ( 3 ) / 'Accepted............           ' #G-CONTROL ( 4 ) / 'Total...............           ' #TOT / 'Difference..........           ' #DIFF2 /
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Input...............           ",pnd_G_Accum_Pnd_G_Control.getValue(1 + 1), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Difference..........           ",pnd_Diff1, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Bypassed............           ",pnd_G_Accum_Pnd_G_Control.getValue(2 + 1), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),NEWLINE,"Combined/Rejected...           ",pnd_G_Accum_Pnd_G_Control.getValue(3 + 1), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Accepted............           ",pnd_G_Accum_Pnd_G_Control.getValue(4 + 1), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Total...............           ",pnd_Tot, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Difference..........           ",pnd_Diff2, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
    }
    private void sub_Display_Combination_Totals() throws Exception                                                                                                        //Natural: DISPLAY-COMBINATION-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  PREPARE TO PRINT GRAND TOTALS FOR COMBINATION TRANSACTIONS
        ldaTwrl462e.getCmb_Trans_Count().getValue(4).nadd(ldaTwrl462e.getCmb_Trans_Count().getValue(3));                                                                  //Natural: ADD CMB-TRANS-COUNT ( 3 ) TO CMB-TRANS-COUNT ( 4 )
        ldaTwrl462e.getCmb_Classic_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Classic_Amt().getValue(1,":",3));                                                            //Natural: ADD CMB-CLASSIC-AMT ( 1:3 ) TO CMB-CLASSIC-AMT ( 4 )
        ldaTwrl462e.getCmb_Roth_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Roth_Amt().getValue(1,":",3));                                                                  //Natural: ADD CMB-ROTH-AMT ( 1:3 ) TO CMB-ROTH-AMT ( 4 )
        ldaTwrl462e.getCmb_Rollover_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Rollover_Amt().getValue(1,":",3));                                                          //Natural: ADD CMB-ROLLOVER-AMT ( 1:3 ) TO CMB-ROLLOVER-AMT ( 4 )
        ldaTwrl462e.getCmb_Rechar_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Rechar_Amt().getValue(1,":",3));                                                              //Natural: ADD CMB-RECHAR-AMT ( 1:3 ) TO CMB-RECHAR-AMT ( 4 )
        ldaTwrl462e.getCmb_Sep_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Sep_Amt().getValue(1,":",3));                                                                    //Natural: ADD CMB-SEP-AMT ( 1:3 ) TO CMB-SEP-AMT ( 4 )
        ldaTwrl462e.getCmb_Fmv_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Fmv_Amt().getValue(1,":",3));                                                                    //Natural: ADD CMB-FMV-AMT ( 1:3 ) TO CMB-FMV-AMT ( 4 )
        ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(4).nadd(ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(1,":",3));                                                        //Natural: ADD CMB-ROTH-CONV-AMT ( 1:3 ) TO CMB-ROTH-CONV-AMT ( 4 )
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),"              ",NEWLINE);                                                                          //Natural: WRITE ( 10 ) NOTITLE 01T '              ' /
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(28),ldaTwrl462e.getCmb_Trans_Count().getValue(i1),new TabSetting(47),ldaTwrl462e.getCmb_Trans_Count().getValue(i2),new  //Natural: WRITE ( 10 ) NOTITLE 'Transaction Count...' 28T CMB-TRANS-COUNT ( I1 ) 47T CMB-TRANS-COUNT ( I2 ) 70T CMB-TRANS-COUNT ( I3 ) 100T CMB-TRANS-COUNT ( I4 )
            TabSetting(70),ldaTwrl462e.getCmb_Trans_Count().getValue(i3),new TabSetting(100),ldaTwrl462e.getCmb_Trans_Count().getValue(i4));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(28),ldaTwrl462e.getCmb_Classic_Amt().getValue(i1), new ReportEditMask           //Natural: WRITE ( 10 ) NOTITLE 'Classic Contrib.....' 28T CMB-CLASSIC-AMT ( I1 ) 47T CMB-CLASSIC-AMT ( I2 ) 70T CMB-CLASSIC-AMT ( I3 ) 100T CMB-CLASSIC-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Classic_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Classic_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Classic_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(28),ldaTwrl462e.getCmb_Roth_Amt().getValue(i1), new ReportEditMask              //Natural: WRITE ( 10 ) NOTITLE 'Roth Contrib........' 28T CMB-ROTH-AMT ( I1 ) 47T CMB-ROTH-AMT ( I2 ) 70T CMB-ROTH-AMT ( I3 ) 100T CMB-ROTH-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Roth_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Roth_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Roth_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Rollover............",new TabSetting(28),ldaTwrl462e.getCmb_Rollover_Amt().getValue(i1), new ReportEditMask          //Natural: WRITE ( 10 ) NOTITLE 'Rollover............' 28T CMB-ROLLOVER-AMT ( I1 ) 47T CMB-ROLLOVER-AMT ( I2 ) 70T CMB-ROLLOVER-AMT ( I3 ) 100T CMB-ROLLOVER-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Rollover_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Rollover_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Rollover_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(28),ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(i1), new ReportEditMask         //Natural: WRITE ( 10 ) NOTITLE 'Roth Conversion.....' 28T CMB-ROTH-CONV-AMT ( I1 ) 47T CMB-ROTH-CONV-AMT ( I2 ) 70T CMB-ROTH-CONV-AMT ( I3 ) 100T CMB-ROTH-CONV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Roth_Conv_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(28),ldaTwrl462e.getCmb_Rechar_Amt().getValue(i1), new ReportEditMask            //Natural: WRITE ( 10 ) NOTITLE 'Recharacter.........' 28T CMB-RECHAR-AMT ( I1 ) 47T CMB-RECHAR-AMT ( I2 ) 70T CMB-RECHAR-AMT ( I3 ) 100T CMB-RECHAR-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Rechar_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Rechar_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Rechar_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"SEP.................",new TabSetting(28),ldaTwrl462e.getCmb_Sep_Amt().getValue(i1), new ReportEditMask               //Natural: WRITE ( 10 ) NOTITLE 'SEP.................' 28T CMB-SEP-AMT ( I1 ) 47T CMB-SEP-AMT ( I2 ) 70T CMB-SEP-AMT ( I3 ) 100T CMB-SEP-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Sep_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Sep_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Sep_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 1);                                                                                                                                         //Natural: SKIP ( 10 ) 1
        getReports().write(10, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(28),ldaTwrl462e.getCmb_Fmv_Amt().getValue(i1), new ReportEditMask               //Natural: WRITE ( 10 ) NOTITLE 'Fair Market.........' 28T CMB-FMV-AMT ( I1 ) 47T CMB-FMV-AMT ( I2 ) 70T CMB-FMV-AMT ( I3 ) 100T CMB-FMV-AMT ( I4 )
            ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(47),ldaTwrl462e.getCmb_Fmv_Amt().getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),ldaTwrl462e.getCmb_Fmv_Amt().getValue(i3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(100),ldaTwrl462e.getCmb_Fmv_Amt().getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(10, 3);                                                                                                                                         //Natural: SKIP ( 10 ) 3
        getReports().write(10, ReportOption.NOTITLE,"TOTAL COMBINATION COL includes transaction amounts written to the","Contribution file. These amounts are composed of the sum of ", //Natural: WRITE ( 10 ) NOTITLE 'TOTAL COMBINATION COL includes transaction amounts written to the' 'Contribution file. These amounts are composed of the sum of ' 'TCII and TIAA or TCII and CREF' //
            "TCII and TIAA or TCII and CREF",NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_T_Trans_Count.getValue(5,":",8).nadd(pnd_T_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #T-TRANS-COUNT ( 1:4 ) TO #T-TRANS-COUNT ( 5:8 )
        pnd_T_Classic_Amt.getValue(5,":",8).nadd(pnd_T_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( 1:4 ) TO #T-CLASSIC-AMT ( 5:8 )
        pnd_T_Roth_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #T-ROTH-AMT ( 1:4 ) TO #T-ROTH-AMT ( 5:8 )
        pnd_T_Rollover_Amt.getValue(5,":",8).nadd(pnd_T_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( 1:4 ) TO #T-ROLLOVER-AMT ( 5:8 )
        pnd_T_Rechar_Amt.getValue(5,":",8).nadd(pnd_T_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #T-RECHAR-AMT ( 1:4 ) TO #T-RECHAR-AMT ( 5:8 )
        pnd_T_Sep_Amt.getValue(5,":",8).nadd(pnd_T_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-SEP-AMT ( 1:4 ) TO #T-SEP-AMT ( 5:8 )
        pnd_T_Fmv_Amt.getValue(5,":",8).nadd(pnd_T_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-FMV-AMT ( 1:4 ) TO #T-FMV-AMT ( 5:8 )
        pnd_T_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #T-ROTH-CONV-AMT ( 1:4 ) TO #T-ROTH-CONV-AMT ( 5:8 )
        pnd_C_Trans_Count.getValue(5,":",8).nadd(pnd_C_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #C-TRANS-COUNT ( 1:4 ) TO #C-TRANS-COUNT ( 5:8 )
        pnd_C_Classic_Amt.getValue(5,":",8).nadd(pnd_C_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #C-CLASSIC-AMT ( 1:4 ) TO #C-CLASSIC-AMT ( 5:8 )
        pnd_C_Roth_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #C-ROTH-AMT ( 1:4 ) TO #C-ROTH-AMT ( 5:8 )
        pnd_C_Rollover_Amt.getValue(5,":",8).nadd(pnd_C_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #C-ROLLOVER-AMT ( 1:4 ) TO #C-ROLLOVER-AMT ( 5:8 )
        pnd_C_Rechar_Amt.getValue(5,":",8).nadd(pnd_C_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #C-RECHAR-AMT ( 1:4 ) TO #C-RECHAR-AMT ( 5:8 )
        pnd_C_Sep_Amt.getValue(5,":",8).nadd(pnd_C_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-SEP-AMT ( 1:4 ) TO #C-SEP-AMT ( 5:8 )
        pnd_C_Fmv_Amt.getValue(5,":",8).nadd(pnd_C_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-FMV-AMT ( 1:4 ) TO #C-FMV-AMT ( 5:8 )
        pnd_C_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #C-ROTH-CONV-AMT ( 1:4 ) TO #C-ROTH-CONV-AMT ( 5:8 )
        pnd_L_Trans_Count.getValue(5,":",8).nadd(pnd_L_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #L-TRANS-COUNT ( 1:4 ) TO #L-TRANS-COUNT ( 5:8 )
        pnd_L_Classic_Amt.getValue(5,":",8).nadd(pnd_L_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #L-CLASSIC-AMT ( 1:4 ) TO #L-CLASSIC-AMT ( 5:8 )
        pnd_L_Roth_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #L-ROTH-AMT ( 1:4 ) TO #L-ROTH-AMT ( 5:8 )
        pnd_L_Rollover_Amt.getValue(5,":",8).nadd(pnd_L_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #L-ROLLOVER-AMT ( 1:4 ) TO #L-ROLLOVER-AMT ( 5:8 )
        pnd_L_Rechar_Amt.getValue(5,":",8).nadd(pnd_L_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #L-RECHAR-AMT ( 1:4 ) TO #L-RECHAR-AMT ( 5:8 )
        pnd_L_Sep_Amt.getValue(5,":",8).nadd(pnd_L_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-SEP-AMT ( 1:4 ) TO #L-SEP-AMT ( 5:8 )
        pnd_L_Fmv_Amt.getValue(5,":",8).nadd(pnd_L_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-FMV-AMT ( 1:4 ) TO #L-FMV-AMT ( 5:8 )
        pnd_L_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #L-ROTH-CONV-AMT ( 1:4 ) TO #L-ROTH-CONV-AMT ( 5:8 )
        pnd_S_Trans_Count.getValue(5,":",8).nadd(pnd_S_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #S-TRANS-COUNT ( 1:4 ) TO #S-TRANS-COUNT ( 5:8 )
        pnd_S_Classic_Amt.getValue(5,":",8).nadd(pnd_S_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #S-CLASSIC-AMT ( 1:4 ) TO #S-CLASSIC-AMT ( 5:8 )
        pnd_S_Roth_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #S-ROTH-AMT ( 1:4 ) TO #S-ROTH-AMT ( 5:8 )
        pnd_S_Rollover_Amt.getValue(5,":",8).nadd(pnd_S_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #S-ROLLOVER-AMT ( 1:4 ) TO #S-ROLLOVER-AMT ( 5:8 )
        pnd_S_Rechar_Amt.getValue(5,":",8).nadd(pnd_S_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #S-RECHAR-AMT ( 1:4 ) TO #S-RECHAR-AMT ( 5:8 )
        pnd_S_Sep_Amt.getValue(5,":",8).nadd(pnd_S_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-SEP-AMT ( 1:4 ) TO #S-SEP-AMT ( 5:8 )
        pnd_S_Fmv_Amt.getValue(5,":",8).nadd(pnd_S_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-FMV-AMT ( 1:4 ) TO #S-FMV-AMT ( 5:8 )
        pnd_S_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #S-ROTH-CONV-AMT ( 1:4 ) TO #S-ROTH-CONV-AMT ( 5:8 )
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_T_Trans_Count.getValue(1,":",4).reset();                                                                                                                      //Natural: RESET #T-TRANS-COUNT ( 1:4 ) #C-TRANS-COUNT ( 1:4 ) #L-TRANS-COUNT ( 1:4 ) #T-CLASSIC-AMT ( 1:4 ) #C-CLASSIC-AMT ( 1:4 ) #L-CLASSIC-AMT ( 1:4 ) #T-ROTH-AMT ( 1:4 ) #C-ROTH-AMT ( 1:4 ) #L-ROTH-AMT ( 1:4 ) #T-ROLLOVER-AMT ( 1:4 ) #C-ROLLOVER-AMT ( 1:4 ) #L-ROLLOVER-AMT ( 1:4 ) #T-RECHAR-AMT ( 1:4 ) #C-RECHAR-AMT ( 1:4 ) #L-RECHAR-AMT ( 1:4 ) #T-SEP-AMT ( 1:4 ) #C-SEP-AMT ( 1:4 ) #L-SEP-AMT ( 1:4 ) #T-FMV-AMT ( 1:4 ) #C-FMV-AMT ( 1:4 ) #L-FMV-AMT ( 1:4 ) #T-ROTH-CONV-AMT ( 1:4 ) #C-ROTH-CONV-AMT ( 1:4 ) #L-ROTH-CONV-AMT ( 1:4 ) #S-TRANS-COUNT ( 1:4 ) #S-CLASSIC-AMT ( 1:4 ) #S-ROTH-AMT ( 1:4 ) #S-ROLLOVER-AMT ( 1:4 ) #S-RECHAR-AMT ( 1:4 ) #S-SEP-AMT ( 1:4 ) #S-FMV-AMT ( 1:4 ) #S-ROTH-CONV-AMT ( 1:4 )
        pnd_C_Trans_Count.getValue(1,":",4).reset();
        pnd_L_Trans_Count.getValue(1,":",4).reset();
        pnd_T_Classic_Amt.getValue(1,":",4).reset();
        pnd_C_Classic_Amt.getValue(1,":",4).reset();
        pnd_L_Classic_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Amt.getValue(1,":",4).reset();
        pnd_T_Rollover_Amt.getValue(1,":",4).reset();
        pnd_C_Rollover_Amt.getValue(1,":",4).reset();
        pnd_L_Rollover_Amt.getValue(1,":",4).reset();
        pnd_T_Rechar_Amt.getValue(1,":",4).reset();
        pnd_C_Rechar_Amt.getValue(1,":",4).reset();
        pnd_L_Rechar_Amt.getValue(1,":",4).reset();
        pnd_T_Sep_Amt.getValue(1,":",4).reset();
        pnd_C_Sep_Amt.getValue(1,":",4).reset();
        pnd_L_Sep_Amt.getValue(1,":",4).reset();
        pnd_T_Fmv_Amt.getValue(1,":",4).reset();
        pnd_C_Fmv_Amt.getValue(1,":",4).reset();
        pnd_L_Fmv_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_S_Trans_Count.getValue(1,":",4).reset();
        pnd_S_Classic_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Amt.getValue(1,":",4).reset();
        pnd_S_Rollover_Amt.getValue(1,":",4).reset();
        pnd_S_Rechar_Amt.getValue(1,":",4).reset();
        pnd_S_Sep_Amt.getValue(1,":",4).reset();
        pnd_S_Fmv_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Conv_Amt.getValue(1,":",4).reset();
    }
    private void sub_Report_Accepted() throws Exception                                                                                                                   //Natural: REPORT-ACCEPTED
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 01 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR K = 1 TO L
        for (k.setValue(1); condition(k.lessOrEqual(l)); k.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Warning_Msg.getValue(k));                                                                   //Natural: WRITE ( 01 ) NOTITLE NOHDR #WARNING-MSG ( K )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Report_Warning() throws Exception                                                                                                                    //Natural: REPORT-WARNING
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 06 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR K = 1 TO L
        for (k.setValue(1); condition(k.lessOrEqual(l)); k.nadd(1))
        {
            getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Warning_Msg.getValue(k));                                                                   //Natural: WRITE ( 06 ) NOTITLE NOHDR #WARNING-MSG ( K )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Report_Rejected() throws Exception                                                                                                                   //Natural: REPORT-REJECTED
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 02 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR K = 1 TO I
        for (k.setValue(1); condition(k.lessOrEqual(i)); k.nadd(1))
        {
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Error_Msg.getValue(k));                                                                     //Natural: WRITE ( 02 ) NOTITLE NOHDR #ERROR-MSG ( K )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Report_Bypassed_Already_On_File() throws Exception                                                                                                   //Natural: REPORT-BYPASSED-ALREADY-ON-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************
        getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 03 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99 ) IRA-ROTH-AMT ( EM = Z,ZZ9.99 ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99 ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 ) #FMV-PLUS-OR-MINUS-10 ( EM = ' '/'*' )
            new ReportEditMask ("Z,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),pnd_Fmv_Plus_Or_Minus_10, new ReportEditMask ("' '/'*'"));
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR K = 1 TO L
        for (k.setValue(1); condition(k.lessOrEqual(l)); k.nadd(1))
        {
            getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,pnd_Warning_Msg.getValue(k));                                                                   //Natural: WRITE ( 03 ) NOTITLE NOHDR #WARNING-MSG ( K )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Report_Missing_Dob() throws Exception                                                                                                                //Natural: REPORT-MISSING-DOB
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 05 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Report_Combined() throws Exception                                                                                                                   //Natural: REPORT-COMBINED
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(9, ReportOption.NOTITLE,ReportOption.NOHDR,ldaTwrl462a.getIra_Record_Ira_Company_Code(),ldaTwrl462a.getIra_Record_Ira_Tax_Id(),ldaTwrl462a.getIra_Record_Ira_Contract(),ldaTwrl462a.getIra_Record_Ira_Payee(),ldaTwrl462a.getIra_Record_Ira_Citizenship_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Code(),ldaTwrl462a.getIra_Record_Ira_Residency_Type(),ldaTwrl462a.getIra_Record_Ira_Pin(),ldaTwrl462a.getIra_Record_Ira_Form_Type(),ldaTwrl462a.getIra_Record_Ira_Form_Period(),ldaTwrl462a.getIra_Record_Ira_Classic_Amt(),  //Natural: WRITE ( 09 ) NOTITLE NOHDR IRA-COMPANY-CODE IRA-TAX-ID IRA-CONTRACT IRA-PAYEE IRA-CITIZENSHIP-CODE IRA-RESIDENCY-CODE IRA-RESIDENCY-TYPE IRA-PIN IRA-FORM-TYPE IRA-FORM-PERIOD IRA-CLASSIC-AMT ( EM = Z,ZZ9.99- ) IRA-ROTH-AMT ( EM = Z,ZZ9.99- ) IRA-SEP-AMT ( EM = ZZ,ZZ9.99- ) IRA-ROLLOVER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-RECHARACTER-AMT ( EM = Z,ZZZ,ZZ9.99- ) IRA-ROTH-CONV-AMT ( EM = Z,ZZZ,ZZ9.99 ) IRA-FMV-AMT ( EM = Z,ZZZ,ZZ9.99 )
            new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Amt(), new ReportEditMask ("Z,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Sep_Amt(), 
            new ReportEditMask ("ZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Rollover_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),ldaTwrl462a.getIra_Record_Ira_Fmv_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  FOR  K  =  1  TO  L
        //*  WRITE (09) NOTITLE NOHDR
        //*    #WARNING-MSG (K)
        //*  END-FOR
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(5);                                                                                                                                                   //Natural: ASSIGN I1 := 5
        i2.setValue(6);                                                                                                                                                   //Natural: ASSIGN I2 := 6
        i3.setValue(7);                                                                                                                                                   //Natural: ASSIGN I3 := 7
        i4.setValue(8);                                                                                                                                                   //Natural: ASSIGN I4 := 8
        //*  PERFORM  DISPLAY-CONTROL-TOTALS   /* <=== REDUNDANT - REMOVED JR 02/07
        if (condition(pnd_Accept_Ctr.greater(getZero())))                                                                                                                 //Natural: IF #ACCEPT-CTR > 0
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ACCEPTED-TOTALS
            sub_Display_Accepted_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Reject_Ctr.greater(getZero())))                                                                                                                 //Natural: IF #REJECT-CTR > 0
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REJECTED-TOTALS
            sub_Display_Rejected_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Bypass_Ctr.greater(getZero())))                                                                                                                 //Natural: IF #BYPASS-CTR > 0
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-BYPASSED-TOTALS
            sub_Display_Bypassed_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Combined_Eoj_Cnt.greater(getZero())))                                                                                                           //Natural: IF #COMBINED-EOJ-CNT > 0
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-COMBINED-TOTALS
            sub_Display_Combined_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                  //Natural: WRITE ( 00 ) 01T 'Records Read...............................' #READ-CTR / 01T 'Records Accepted...........................' #ACCEPT-CTR / 01T 'Records Rejected...........................' #REJECT-CTR / 01T 'Records Bypassed (Already On File).........' #BYPASS-CTR / 01T 'Records Bypassed w/FMV +/- $10.............' #FMV-BYPASS-CTR / 01T 'Records Missing Date Of Birth..............' #DOB-CTR
            TabSetting(1),"Records Accepted...........................",pnd_Accept_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Rejected...........................",pnd_Reject_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Bypassed (Already On File).........",pnd_Bypass_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Bypassed w/FMV +/- $10.............",pnd_Fmv_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),"Records Missing Date Of Birth..............",pnd_Dob_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(3, 3);                                                                                                                                          //Natural: SKIP ( 03 ) 3
        getReports().write(3, ReportOption.NOTITLE,"NOTE:  An asterisk next to FMV amount on this report indicates",NEWLINE,"       that the record has been bypassed because only FMV has ", //Natural: WRITE ( 03 ) 'NOTE:  An asterisk next to FMV amount on this report indicates' / '       that the record has been bypassed because only FMV has ' / '       changed and by less than $10.' /
            NEWLINE,"       changed and by less than $10.",NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new  //Natural: WRITE ( 04 ) 01T 'Records Read...............................' #READ-CTR / 01T 'Records Accepted...........................' #ACCEPT-CTR / 01T 'Records Rejected...........................' #REJECT-CTR / 01T 'Records Bypassed (Already On File).........' #BYPASS-CTR / 01T 'Records Missing Date Of Birth..............' #DOB-CTR
            TabSetting(1),"Records Accepted...........................",pnd_Accept_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Rejected...........................",pnd_Reject_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Bypassed (Already On File).........",pnd_Bypass_Ctr, new ReportEditMask 
            ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"Records Missing Date Of Birth..............",pnd_Dob_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(4, 3);                                                                                                                                          //Natural: SKIP ( 04 ) 3
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"How to balance this report to that output by TWRP4620.",NEWLINE,NEWLINE,new TabSetting(5),"Balance the FMV amount, for example, by adding amounts below:",NEWLINE,new  //Natural: WRITE ( 04 ) 01T 'How to balance this report to that output by TWRP4620.'// 05T 'Balance the FMV amount, for example, by adding amounts below:'/ 05T '    TIAA-FMV-Combined ' / 05T '    CREF-FMV-Combined ' / 05T '    TCII FMV-Bypassed ' / 05T '    TCII FMV-Accepted from the TWRP4610 report. ' / / 05T 'Compare result with the total of the following amounts:' / 05T '   TCII-FMV-Bypassed ' / 05T '   TCII-FMV-Accepted from the TWRP4620 report. ' / / 05T 'NOTE: TIAA AND CREF amounts are folded-into (combined-with) '/ 05T 'TCII bypassed and accepted amounts in the TWRP4620 report.' /
            TabSetting(5),"    TIAA-FMV-Combined ",NEWLINE,new TabSetting(5),"    CREF-FMV-Combined ",NEWLINE,new TabSetting(5),"    TCII FMV-Bypassed ",NEWLINE,new 
            TabSetting(5),"    TCII FMV-Accepted from the TWRP4610 report. ",NEWLINE,NEWLINE,new TabSetting(5),"Compare result with the total of the following amounts:",NEWLINE,new 
            TabSetting(5),"   TCII-FMV-Bypassed ",NEWLINE,new TabSetting(5),"   TCII-FMV-Accepted from the TWRP4620 report. ",NEWLINE,NEWLINE,new TabSetting(5),"NOTE: TIAA AND CREF amounts are folded-into (combined-with) ",NEWLINE,new 
            TabSetting(5),"TCII bypassed and accepted amounts in the TWRP4620 report.",NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(5, 3);                                                                                                                                          //Natural: SKIP ( 05 ) 3
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"Records Missing Date Of Birth..............",pnd_Dob_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));        //Natural: WRITE ( 05 ) 01T 'Records Missing Date Of Birth..............' #DOB-CTR
        if (Global.isEscape()) return;
        getReports().skip(6, 3);                                                                                                                                          //Natural: SKIP ( 06 ) 3
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"Number Of Warning Messages.................",pnd_No_Of_Warnings, new ReportEditMask                 //Natural: WRITE ( 06 ) 01T 'Number Of Warning Messages.................' #NO-OF-WARNINGS
            ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Display_Accepted_Totals() throws Exception                                                                                                           //Natural: DISPLAY-ACCEPTED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"  Accepted Totals   ");                                                                            //Natural: WRITE ( 01 ) NOTITLE 22T '  Accepted Totals   '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"====================");                                                                            //Natural: WRITE ( 01 ) NOTITLE 22T '===================='
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #T-FMV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 01 ) 3
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_C_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #C-FMV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 01 ) 3
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_L_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #L-FMV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_S_Fmv_Amt.getValue(i4), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 25T #S-FMV-AMT ( I4 )
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
    }
    private void sub_Display_Rejected_Totals() throws Exception                                                                                                           //Natural: DISPLAY-REJECTED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"  Rejected Totals   ");                                                                            //Natural: WRITE ( 02 ) NOTITLE 22T '  Rejected Totals   '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(22),"====================");                                                                            //Natural: WRITE ( 02 ) NOTITLE 22T '===================='
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 02 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #T-FMV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 02 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_C_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #C-FMV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 02 ) 3
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 02 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_L_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #L-FMV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 02 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 02 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 02 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 02 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 02 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 02 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 02 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
        getReports().write(2, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_S_Fmv_Amt.getValue(i3), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 02 ) NOTITLE 'Fair Market.........' 25T #S-FMV-AMT ( I3 )
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
    }
    private void sub_Display_Bypassed_Totals() throws Exception                                                                                                           //Natural: DISPLAY-BYPASSED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(22),"  Bypassed Totals   ");                                                                            //Natural: WRITE ( 03 ) NOTITLE 22T '  Bypassed Totals   '
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(22),"====================");                                                                            //Natural: WRITE ( 03 ) NOTITLE 22T '===================='
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 03 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 03 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 03 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 03 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 03 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 03 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 03 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Fair Market.........' 25T #T-FMV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 3);                                                                                                                                          //Natural: SKIP ( 03 ) 3
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 03 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 03 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 03 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 03 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 03 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 03 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 03 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_C_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Fair Market.........' 25T #C-FMV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 3);                                                                                                                                          //Natural: SKIP ( 03 ) 3
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 03 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 03 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 03 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 03 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 03 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 03 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 03 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_L_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Fair Market.........' 25T #L-FMV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        //* * 09-24-03 ADDED TCII BY FRANK
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 03 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 03 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 03 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 03 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 03 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 03 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 03 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
        getReports().write(3, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(25),pnd_S_Fmv_Amt.getValue(i2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 03 ) NOTITLE 'Fair Market.........' 25T #S-FMV-AMT ( I2 )
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 03 ) 1
    }
    private void sub_Display_Combined_Totals() throws Exception                                                                                                           //Natural: DISPLAY-COMBINED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().skip(9, 1);                                                                                                                                          //Natural: SKIP ( 09 ) 1
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(22),"  Combined Totals   ");                                                                            //Natural: WRITE ( 09 ) NOTITLE 22T '  Combined Totals   '
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(22),"====================");                                                                            //Natural: WRITE ( 03 ) NOTITLE 22T '===================='
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"   Count (TCII + TIAA)   ",pnd_Combined_Codes_Pnd_Company_Codes_St, new ReportEditMask              //Natural: WRITE ( 09 ) NOTITLE 01T '   Count (TCII + TIAA)   ' #COMPANY-CODES-ST /
            ("ZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"   Count (TCII + CREF)   ",pnd_Combined_Codes_Pnd_Company_Codes_Sc, new ReportEditMask              //Natural: WRITE ( 09 ) NOTITLE 01T '   Count (TCII + CREF)   ' #COMPANY-CODES-SC /
            ("ZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"   Count (TCII + TCII)   ",pnd_Combined_Codes_Pnd_Company_Codes_Ss, new ReportEditMask              //Natural: WRITE ( 09 ) NOTITLE 01T '   Count (TCII + TCII)   ' #COMPANY-CODES-SS /
            ("ZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"   Count (TIAA + CREF)   ",pnd_Combined_Codes_Pnd_Company_Codes_Tc, new ReportEditMask              //Natural: WRITE ( 09 ) NOTITLE 01T '   Count (TIAA + CREF)   ' #COMPANY-CODES-TC /
            ("ZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"   Count (   Other   )   ",pnd_Combined_Codes_Pnd_Company_Codes_Other, new ReportEditMask           //Natural: WRITE ( 09 ) NOTITLE 01T '   Count (   Other   )   ' #COMPANY-CODES-OTHER /
            ("ZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"Combined Transactions Count",pnd_Combined_Eoj_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"),               //Natural: WRITE ( 09 ) NOTITLE 01T 'Combined Transactions Count' #COMBINED-EOJ-CNT /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(9, 1);                                                                                                                                          //Natural: SKIP ( 09 ) 1
    }
    private void sub_Read_Control_Totals() throws Exception                                                                                                               //Natural: READ-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 06 RECORD IRAT-RECORD
        while (condition(getWorkFiles().read(6, ldaTwrl462b.getIrat_Record())))
        {
            if (condition(ldaTwrl462b.getIrat_Record_Irat_Company_Code().equals("T")))                                                                                    //Natural: IF IRAT-RECORD.IRAT-COMPANY-CODE = 'T'
            {
                ldaTwrl462d.getPnd_T().setValuesByName(ldaTwrl462b.getIrat_Record());                                                                                     //Natural: MOVE BY NAME IRAT-RECORD TO #T
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl462b.getIrat_Record_Irat_Company_Code().equals("C")))                                                                                //Natural: IF IRAT-RECORD.IRAT-COMPANY-CODE = 'C'
                {
                    ldaTwrl462d.getPnd_C().setValuesByName(ldaTwrl462b.getIrat_Record());                                                                                 //Natural: MOVE BY NAME IRAT-RECORD TO #C
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl462b.getIrat_Record_Irat_Company_Code().equals("L")))                                                                            //Natural: IF IRAT-RECORD.IRAT-COMPANY-CODE = 'L'
                    {
                        ldaTwrl462d.getPnd_L().setValuesByName(ldaTwrl462b.getIrat_Record());                                                                             //Natural: MOVE BY NAME IRAT-RECORD TO #L
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  09-24-03 FRANK
                        if (condition(ldaTwrl462b.getIrat_Record_Irat_Company_Code().equals("S")))                                                                        //Natural: IF IRAT-RECORD.IRAT-COMPANY-CODE = 'S'
                        {
                            ldaTwrl462d.getPnd_S().setValuesByName(ldaTwrl462b.getIrat_Record());                                                                         //Natural: MOVE BY NAME IRAT-RECORD TO #S
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Save_Ira_Tran_Data() throws Exception                                                                                                                //Natural: SAVE-IRA-TRAN-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pnd_New_Key.getBoolean()))                                                                                                                          //Natural: IF #NEW-KEY
        {
            ldaTwrl462a.getIra_Record_Ira_Classic_Amt().reset();                                                                                                          //Natural: RESET IRA-CLASSIC-AMT IRA-ROTH-AMT IRA-ROLLOVER-AMT IRA-RECHARACTER-AMT IRA-SEP-AMT IRA-FMV-AMT IRA-ROTH-CONV-AMT #COMBINED-CTR #COMPANY-CODE1 #COMPANY-CODE2
            ldaTwrl462a.getIra_Record_Ira_Roth_Amt().reset();
            ldaTwrl462a.getIra_Record_Ira_Rollover_Amt().reset();
            ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt().reset();
            ldaTwrl462a.getIra_Record_Ira_Sep_Amt().reset();
            ldaTwrl462a.getIra_Record_Ira_Fmv_Amt().reset();
            ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt().reset();
            pnd_Combined_Ctr.reset();
            pnd_Company_Code1.reset();
            pnd_Company_Code2.reset();
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Naa_Found_Save.setValue(pnd_Naa_Found.getBoolean());                                                                                                          //Natural: ASSIGN #NAA-FOUND-SAVE := #NAA-FOUND
        ldaTwrl462a.getIra_Record_Ira_Tax_Year().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Year());                                                                   //Natural: MOVE RD-IRA-TAX-YEAR TO IRA-TAX-YEAR
        ldaTwrl462a.getIra_Record_Ira_Company_Code().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code());                                                           //Natural: MOVE RD-IRA-COMPANY-CODE TO IRA-COMPANY-CODE
        ldaTwrl462a.getIra_Record_Ira_Simulation_Date().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Simulation_Date());                                                     //Natural: MOVE RD-IRA-SIMULATION-DATE TO IRA-SIMULATION-DATE
        ldaTwrl462a.getIra_Record_Ira_Tax_Id().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id());                                                                       //Natural: MOVE RD-IRA-TAX-ID TO IRA-TAX-ID
        ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Tax_Id_Type());                                                             //Natural: MOVE RD-IRA-TAX-ID-TYPE TO IRA-TAX-ID-TYPE
        ldaTwrl462a.getIra_Record_Ira_Contract().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract());                                                                   //Natural: MOVE RD-IRA-CONTRACT TO IRA-CONTRACT
        ldaTwrl462a.getIra_Record_Ira_Payee().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee());                                                                         //Natural: MOVE RD-IRA-PAYEE TO IRA-PAYEE
        ldaTwrl462a.getIra_Record_Ira_Contract_Xref().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Contract_Xref());                                                         //Natural: MOVE RD-IRA-CONTRACT-XREF TO IRA-CONTRACT-XREF
        ldaTwrl462a.getIra_Record_Ira_Payee_Xref().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Payee_Xref());                                                               //Natural: MOVE RD-IRA-PAYEE-XREF TO IRA-PAYEE-XREF
        ldaTwrl462a.getIra_Record_Ira_Pin().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Pin());                                                                             //Natural: MOVE RD-IRA-PIN TO IRA-PIN
        ldaTwrl462a.getIra_Record_Ira_Citizenship_Code().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Citizenship_Code());                                                   //Natural: MOVE RD-IRA-CITIZENSHIP-CODE TO IRA-CITIZENSHIP-CODE
        ldaTwrl462a.getIra_Record_Ira_Residency_Code().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Code());                                                       //Natural: MOVE RD-IRA-RESIDENCY-CODE TO IRA-RESIDENCY-CODE
        ldaTwrl462a.getIra_Record_Ira_Residency_Type().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Residency_Type());                                                       //Natural: MOVE RD-IRA-RESIDENCY-TYPE TO IRA-RESIDENCY-TYPE
        ldaTwrl462a.getIra_Record_Ira_Locality_Code().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Locality_Code());                                                         //Natural: MOVE RD-IRA-LOCALITY-CODE TO IRA-LOCALITY-CODE
        ldaTwrl462a.getIra_Record_Ira_Dob().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Dob());                                                                             //Natural: MOVE RD-IRA-DOB TO IRA-DOB
        ldaTwrl462a.getIra_Record_Ira_Dod().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Dod());                                                                             //Natural: MOVE RD-IRA-DOD TO IRA-DOD
        ldaTwrl462a.getIra_Record_Ira_Form().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form());                                                                           //Natural: MOVE RD-IRA-FORM TO IRA-FORM
        ldaTwrl462a.getIra_Record_Ira_Form_Type().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_Type());                                                                 //Natural: MOVE RD-IRA-FORM-TYPE TO IRA-FORM-TYPE
        ldaTwrl462a.getIra_Record_Ira_Form_Ocv().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_Ocv());                                                                   //Natural: MOVE RD-IRA-FORM-OCV TO IRA-FORM-OCV
        ldaTwrl462a.getIra_Record_Ira_Rollover().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover());                                                                   //Natural: MOVE RD-IRA-ROLLOVER TO IRA-ROLLOVER
        ldaTwrl462a.getIra_Record_Ira_Recharacter().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter());                                                             //Natural: MOVE RD-IRA-RECHARACTER TO IRA-RECHARACTER
        ldaTwrl462a.getIra_Record_Ira_Form_Period().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_Period());                                                             //Natural: MOVE RD-IRA-FORM-PERIOD TO IRA-FORM-PERIOD
        //*  MOVE 'P2'                     TO IRA-FORM-PERIOD
        ldaTwrl462a.getIra_Record_Ira_Form_1078().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_1078());                                                                 //Natural: MOVE RD-IRA-FORM-1078 TO IRA-FORM-1078
        ldaTwrl462a.getIra_Record_Ira_Form_1078_Eff_Date().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date());                                               //Natural: MOVE RD-IRA-FORM-1078-EFF-DATE TO IRA-FORM-1078-EFF-DATE
        ldaTwrl462a.getIra_Record_Ira_Form_1001().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_1001());                                                                 //Natural: MOVE RD-IRA-FORM-1001 TO IRA-FORM-1001
        ldaTwrl462a.getIra_Record_Ira_Form_1001_Eff_Date().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date());                                               //Natural: MOVE RD-IRA-FORM-1001-EFF-DATE TO IRA-FORM-1001-EFF-DATE
        ldaTwrl462a.getIra_Record_Ira_Classic_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Classic_Amt());                                                                 //Natural: ADD RD-IRA-CLASSIC-AMT TO IRA-CLASSIC-AMT
        ldaTwrl462a.getIra_Record_Ira_Roth_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Amt());                                                                       //Natural: ADD RD-IRA-ROTH-AMT TO IRA-ROTH-AMT
        ldaTwrl462a.getIra_Record_Ira_Rollover_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rollover_Amt());                                                               //Natural: ADD RD-IRA-ROLLOVER-AMT TO IRA-ROLLOVER-AMT
        ldaTwrl462a.getIra_Record_Ira_Recharacter_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Recharacter_Amt());                                                         //Natural: ADD RD-IRA-RECHARACTER-AMT TO IRA-RECHARACTER-AMT
        ldaTwrl462a.getIra_Record_Ira_Sep_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Sep_Amt());                                                                         //Natural: ADD RD-IRA-SEP-AMT TO IRA-SEP-AMT
        ldaTwrl462a.getIra_Record_Ira_Fmv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Fmv_Amt());                                                                         //Natural: ADD RD-IRA-FMV-AMT TO IRA-FMV-AMT
        ldaTwrl462a.getIra_Record_Ira_Roth_Conv_Amt().nadd(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt());                                                             //Natural: ADD RD-IRA-ROTH-CONV-AMT TO IRA-ROTH-CONV-AMT
        ldaTwrl462a.getIra_Record_Ira_Name().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Name());                                                                           //Natural: MOVE RD-IRA-NAME TO IRA-NAME
        ldaTwrl462a.getIra_Record_Ira_Addr_1().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_1());                                                                       //Natural: MOVE RD-IRA-ADDR-1 TO IRA-ADDR-1
        ldaTwrl462a.getIra_Record_Ira_Addr_2().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_2());                                                                       //Natural: MOVE RD-IRA-ADDR-2 TO IRA-ADDR-2
        ldaTwrl462a.getIra_Record_Ira_Addr_3().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_3());                                                                       //Natural: MOVE RD-IRA-ADDR-3 TO IRA-ADDR-3
        ldaTwrl462a.getIra_Record_Ira_Addr_4().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_4());                                                                       //Natural: MOVE RD-IRA-ADDR-4 TO IRA-ADDR-4
        ldaTwrl462a.getIra_Record_Ira_Addr_5().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_5());                                                                       //Natural: MOVE RD-IRA-ADDR-5 TO IRA-ADDR-5
        ldaTwrl462a.getIra_Record_Ira_Addr_6().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Addr_6());                                                                       //Natural: MOVE RD-IRA-ADDR-6 TO IRA-ADDR-6
        ldaTwrl462a.getIra_Record_Ira_City().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_City());                                                                           //Natural: MOVE RD-IRA-CITY TO IRA-CITY
        ldaTwrl462a.getIra_Record_Ira_State().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_State());                                                                         //Natural: MOVE RD-IRA-STATE TO IRA-STATE
        ldaTwrl462a.getIra_Record_Ira_Zip_Code().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Zip_Code());                                                                   //Natural: MOVE RD-IRA-ZIP-CODE TO IRA-ZIP-CODE
        ldaTwrl462a.getIra_Record_Ira_Foreign_Address().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Foreign_Address());                                                     //Natural: MOVE RD-IRA-FOREIGN-ADDRESS TO IRA-FOREIGN-ADDRESS
        ldaTwrl462a.getIra_Record_Ira_Foreign_Country().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Foreign_Country());                                                     //Natural: MOVE RD-IRA-FOREIGN-COUNTRY TO IRA-FOREIGN-COUNTRY
        ldaTwrl462a.getIra_Record_Ira_Unique_Id().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Unique_Id());                                                                 //Natural: MOVE RD-IRA-UNIQUE-ID TO IRA-UNIQUE-ID
        //*  SECURE-ACT
        ldaTwrl462a.getIra_Record_Ira_Rmd_Required_Flag().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rmd_Required_Flag());                                                 //Natural: MOVE RD-IRA-RMD-REQUIRED-FLAG TO IRA-RMD-REQUIRED-FLAG
        //*  SECURE-ACT
        ldaTwrl462a.getIra_Record_Ira_Rmd_Decedent_Name().setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name());                                                 //Natural: MOVE RD-IRA-RMD-DECEDENT-NAME TO IRA-RMD-DECEDENT-NAME
        if (condition(pnd_Company_Code1.equals(" ")))                                                                                                                     //Natural: IF #COMPANY-CODE1 EQ ' '
        {
            pnd_Company_Code1.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code());                                                                                  //Natural: MOVE RD-IRA-COMPANY-CODE TO #COMPANY-CODE1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Company_Code2.setValue(ldaTwrl462e.getRd_Ira_Rec_Rd_Ira_Company_Code());                                                                                  //Natural: MOVE RD-IRA-COMPANY-CODE TO #COMPANY-CODE2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Combined_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #COMBINED-CTR
        if (condition(pnd_Combined_Ctr.greater(1)))                                                                                                                       //Natural: IF #COMBINED-CTR GT 1
        {
            ldaTwrl462a.getIra_Record_Ira_Company_Code().setValue("S");                                                                                                   //Natural: ASSIGN IRA-COMPANY-CODE := 'S'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS-NEW-RECORD
        sub_Update_Control_Totals_New_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 01 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '        Accepted IRA Contributions        ' 120T 'REPORT: RPT1' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"        Accepted IRA Contributions        ",new 
                        TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 02 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '        Rejected IRA Contributions        ' 120T 'REPORT: RPT2' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"        Rejected IRA Contributions        ",new 
                        TabSetting(120),"REPORT: RPT2",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 03 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Bypassed IRA Contributions Already On File' 120T 'REPORT: RPT3' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 67T 'ROTH' 78T 'SEP' 86T 'ROLLOVER' 96T 'RECHARACTER' 111T 'ROTH CONV.' 126T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"Bypassed IRA Contributions Already On File",new 
                        TabSetting(120),"REPORT: RPT3",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(67),"ROTH",new TabSetting(78),"SEP",new TabSetting(86),"ROLLOVER",new TabSetting(96),"RECHARACTER",new TabSetting(111),"ROTH CONV.",new 
                        TabSetting(126),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 04 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(4, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRA Contributions Edit Control Report",new  //Natural: WRITE ( 04 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRA Contributions Edit Control Report' 120T 'REPORT: RPT4'
                        TabSetting(120),"REPORT: RPT4");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 04 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #INTERFACE-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(4, 2);                                                                                                                              //Natural: SKIP ( 04 ) 2 LINES
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(22),"      Control       ",new TabSetting(43),"       Input        ",new                    //Natural: WRITE ( 04 ) NOTITLE 22T '      Control       ' 43T '       Input        ' 64T '      Bypassed      ' 85T '  Combined / Reject ' 106T '      Accepted      '
                        TabSetting(64),"      Bypassed      ",new TabSetting(85),"  Combined / Reject ",new TabSetting(106),"      Accepted      ");
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(22),"====================",new                     //Natural: WRITE ( 04 ) NOTITLE 01T '                    ' 22T '====================' 43T '====================' 64T '====================' 85T '====================' 106T '===================='
                        TabSetting(43),"====================",new TabSetting(64),"====================",new TabSetting(85),"====================",new TabSetting(106),
                        "====================");
                    getReports().skip(4, 1);                                                                                                                              //Natural: SKIP ( 04 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 05 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '    IRA Contributions - Missing D.O.B.    ' 120T 'REPORT: RPT5' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"    IRA Contributions - Missing D.O.B.    ",new 
                        TabSetting(120),"REPORT: RPT5",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 06 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '   IRA Contributions - Warning Messages   ' 120T 'REPORT: RPT6' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"   IRA Contributions - Warning Messages   ",new 
                        TabSetting(120),"REPORT: RPT6",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt9 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    //*  PIN EXPANSION JW0427
                    getReports().write(9, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 09 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '    Tax Withholding & Reporting System    ' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T '   IRA Combined Transactions Report       ' 120T 'REPORT: RPT9' / 01T 'Tax Year :' #HDR-TAX-YEAR 20T 'Simulation :' #HEADER-DATE ( EM = MM/DD/YYYY ) 47T 'Interface :' #INTERFACE-DATE ( EM = MM/DD/YYYY ) // 01T 'C' 03T '  Tax ID  ' 14T 'Contract' 23T 'PY' 26T 'CZ' 29T 'RE' 32T 'RT' 35T ' PIN ' 46T 'IRA' 50T 'Per' 55T 'Classic' 68T 'Roth' 80T 'SEP' 89T 'Rollover' 100T 'Recharacter' 115T 'Roth Conv.' 132T 'F.M.V.'
                        TabSetting(45),"    Tax Withholding & Reporting System    ",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"   IRA Combined Transactions Report       ",new 
                        TabSetting(120),"REPORT: RPT9",NEWLINE,new TabSetting(1),"Tax Year :",pnd_Hdr_Tax_Year,new TabSetting(20),"Simulation :",pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(47),"Interface :",pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new 
                        TabSetting(1),"C",new TabSetting(3),"  Tax ID  ",new TabSetting(14),"Contract",new TabSetting(23),"PY",new TabSetting(26),"CZ",new 
                        TabSetting(29),"RE",new TabSetting(32),"RT",new TabSetting(35)," PIN ",new TabSetting(46),"IRA",new TabSetting(50),"Per",new TabSetting(55),"Classic",new 
                        TabSetting(68),"Roth",new TabSetting(80),"SEP",new TabSetting(89),"Rollover",new TabSetting(100),"Recharacter",new TabSetting(115),"Roth Conv.",new 
                        TabSetting(132),"F.M.V.");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt10 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(10, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 10 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(10, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(38),"IRA CONTRIBUTIONS COMBINATION CONTROL REPORT",new  //Natural: WRITE ( 10 ) NOTITLE *INIT-USER '-' *PROGRAM 38T 'IRA CONTRIBUTIONS COMBINATION CONTROL REPORT' 120T 'REPORT: RPT10'
                        TabSetting(120),"REPORT: RPT10");
                    getReports().write(10, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 10 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #INTERFACE-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Interface_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(10, 2);                                                                                                                             //Natural: SKIP ( 10 ) 2 LINES
                    getReports().write(10, ReportOption.NOTITLE,new TabSetting(28)," TIAA in Combination ",new TabSetting(50)," CREF in Combination ",new                 //Natural: WRITE ( 10 ) NOTITLE 28T ' TIAA in Combination ' 50T ' CREF in Combination ' 75T ' TCII in Combination ' 100T ' Total Combination '
                        TabSetting(75)," TCII in Combination ",new TabSetting(100)," Total Combination ");
                    getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(28),"====================",new                    //Natural: WRITE ( 10 ) NOTITLE 01T '                    ' 28T '====================' 50T '====================' 75T '====================' 100T '===================='
                        TabSetting(50),"====================",new TabSetting(75),"====================",new TabSetting(100),"====================");
                    getReports().skip(10, 1);                                                                                                                             //Natural: SKIP ( 10 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* ********
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System: IR"));                                                                                          //Natural: COMPRESS 'Source System: IR' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=138");
        Global.format(2, "PS=60 LS=138");
        Global.format(3, "PS=60 LS=138");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=138");
        Global.format(6, "PS=60 LS=138");
        Global.format(7, "PS=60 LS=133");
        Global.format(8, "PS=60 LS=133");
        Global.format(9, "PS=60 LS=138");
        Global.format(10, "PS=60 LS=133");
    }
}
