/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:43:51 PM
**        * FROM NATURAL PROGRAM : Twrp6890
************************************************************
**        * FILE NAME            : Twrp6890.java
**        * CLASS NAME           : Twrp6890
**        * INSTANCE NAME        : Twrp6890
************************************************************
************************************************************************
** PROGRAM.: TWRP6890
** SYSTEM..: TAXWARS
** AUTHOR..: SNEHA SINHA
** FUNCTION: MOBIUS MASS MIGRATION FOR TAX REFACTOR PROJECT
** DATE....: MAY 18, 2018
** DESC....: MOBIUS MASS MIGRATION ACCOMPANIES MOORE MASS MAILING JOBS
**           FOR TAX FORMS 1099-R, 1099-INT, 5498 AND 1042-S.  THIS
**           PROGRAM IDENTIFIES BY FORM TYPE WHICH FORMS WILL BE
**           MIGRATED TO MOBIUS. THIS JOB PROCESSES ALL PARTICIPANTS
*            WITH EMAIL AND NON-EMAIL PREFERENCES IN SINGLE FILE
**           FOR ALL FORM TYPE.
**
** HISTORY.:
** 07/05/19 SAIK CHANGES FOR RECONCILIATION OF TAX FILE WITH OM REPORTS
** 11/11/20 SAIK1 TAX COMPLIANCE CHANGES FOR TAX YEAR 2020(5498)
** 11/25/20 VIKRAM  MMM REPORTING 480.7C FORM -  DISASTER EXEMPT AMOUNT
**      PROCESSING ADDED FOR YEAR 2020.  TAG - 480.7C DISASTER CHANGES
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp6890 extends BLNatBase
{
    // Data Areas
    private PdaTwrafrmn pdaTwrafrmn;
    private PdaTwra0214 pdaTwra0214;
    private PdaPsta9610 pdaPsta9610;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl2001 ldaTwrl2001;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrl9610 ldaTwrl9610;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Tax_Year;
    private DbsField form_Tirf_Form_Type;
    private DbsField form_Tirf_Company_Cde;
    private DbsField form_Tirf_Tin;
    private DbsField form_Tirf_Tax_Id_Type;
    private DbsField form_Tirf_Contract_Nbr;
    private DbsField form_Tirf_Payee_Cde;
    private DbsField form_Tirf_Key;
    private DbsField form_Tirf_Active_Ind;
    private DbsField form_Tirf_Deceased_Ind;
    private DbsField form_Tirf_Empty_Form;
    private DbsField form_Tirf_Moore_Hold_Ind;
    private DbsField form_Tirf_Paper_Print_Hold_Ind;
    private DbsField form_Tirf_Mobius_Ind;
    private DbsField form_Tirf_Pin;
    private DbsField form_Tirf_Prev_Rpt_Ind;
    private DbsField form_Tirf_Ira_Distr;
    private DbsField form_Tirf_Pct_Of_Tot;
    private DbsField form_Tirf_Tot_Distr;
    private DbsField form_Tirf_Foreign_Addr;
    private DbsField form_Tirf_Moore_Test_Ind;
    private DbsField form_Tirf_Zip;
    private DbsField form_Tirf_Participant_Name;
    private DbsField form_Tirf_Addr_Ln1;
    private DbsField form_Tirf_Addr_Ln2;
    private DbsField form_Tirf_Addr_Ln3;
    private DbsField form_Tirf_Addr_Ln4;
    private DbsField form_Tirf_Addr_Ln5;
    private DbsField form_Tirf_Addr_Ln6;
    private DbsField form_Tirf_Walk_Rte;
    private DbsField form_Tirf_Geo_Cde;
    private DbsField form_Tirf_Country_Name;
    private DbsField form_Tirf_Gross_Amt;
    private DbsField form_Tirf_Int_Amt;
    private DbsField form_Tirf_Taxable_Amt;
    private DbsField form_Tirf_Fed_Tax_Wthld;
    private DbsField form_Tirf_Ivc_Amt;
    private DbsField form_Tirf_Tot_Contractual_Ivc;
    private DbsField form_Tirf_Trad_Ira_Rollover;

    private DbsGroup form__R_Field_1;
    private DbsField form_Tirf_Irr_Amt;
    private DbsField form_Count_Casttirf_1099_R_State_Grp;

    private DbsGroup form_Tirf_1099_R_State_Grp;
    private DbsField form_Tirf_State_Rpt_Ind;
    private DbsField form_Tirf_State_Hardcopy_Ind;
    private DbsField form_Tirf_State_Irs_Rpt_Ind;
    private DbsField form_Tirf_State_Code;
    private DbsField form_Tirf_State_Distr;
    private DbsField form_Tirf_State_Tax_Wthld;
    private DbsField form_Tirf_Loc_Code;
    private DbsField form_Tirf_Loc_Distr;
    private DbsField form_Tirf_Loc_Tax_Wthld;
    private DbsField form_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_Tirf_State_Auth_Rpt_Date;
    private DbsField form_Tirf_Roth_Year;
    private DbsField form_Tirf_Distribution_Cde;
    private DbsField form_Tirf_Taxable_Not_Det;
    private DbsField form_Tirf_Pr_Cntl_Num;
    private DbsField form_Tirf_Trad_Ira_Contrib;

    private DbsGroup form__R_Field_2;
    private DbsField form_Tirf_Pr_Gross_Amt_Roll;

    private DbsGroup form__R_Field_3;
    private DbsField form_Tirf_Summ_State_Tax_Wthld;
    private DbsField form_Tirf_Roth_Ira_Contrib;

    private DbsGroup form__R_Field_4;
    private DbsField form_Tirf_Pr_Ivc_Amt_Roll;
    private DbsField form_Tirf_Ira_Rechar_Amt;
    private DbsField form_Tirf_Postpn_Amt;
    private DbsField form_Tirf_Repayments_Amt;
    private DbsField form_Tirf_Roth_Ira_Conversion;
    private DbsField form_Tirf_Account_Fmv;
    private DbsField form_Tirf_Sep_Amt;
    private DbsField form_Tirf_Superde_3;
    private DbsField form_Tirf_Superde_10;
    private DbsField form_Tirf_Superde_12;
    private DbsField form_Tirf_Future_Use;
    private DbsField form_Count_Casttirf_1042_S_Line_Grp;

    private DbsGroup form_Tirf_1042_S_Line_Grp;
    private DbsField form_Tirf_1042_S_Income_Code;
    private DbsField form_Tirf_Gross_Income;
    private DbsField form_Tirf_Nra_Tax_Wthld;

    private DbsGroup form_Tirf_1042_S_Line_Grp1;
    private DbsField form_Tirf_1042_S_Refund_Amt;
    private DbsField form_Count_Casttirf_1042s_Chptr4;

    private DbsGroup form_Tirf_1042s_Chptr4;
    private DbsField form_Tirf_Chptr4_Income_Cde;

    private DataAccessProgramView vw_form_10;
    private DbsField form_10_Tirf_Tax_Year;
    private DbsField form_10_Tirf_Form_Type;
    private DbsField form_10_Tirf_Company_Cde;
    private DbsField form_10_Tirf_Pin;
    private DbsField form_10_Tirf_Tin;
    private DbsField form_10_Tirf_Contract_Nbr;
    private DbsField form_10_Tirf_Payee_Cde;
    private DbsField form_10_Tirf_Superde_3;
    private DbsField form_10_Tirf_Superde_10;
    private DbsField form_10_Tirf_Active_Ind;

    private DataAccessProgramView vw_form_Upd;
    private DbsField form_Upd_Tirf_Tax_Year;
    private DbsField form_Upd_Tirf_Form_Type;
    private DbsField form_Upd_Tirf_Tin;
    private DbsField form_Upd_Tirf_Payee_Cde;
    private DbsField form_Upd_Tirf_Mobius_Stat;
    private DbsField form_Upd_Tirf_Mobius_Ind;
    private DbsField form_Upd_Tirf_Mobius_Stat_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Date;
    private DbsField form_Upd_Tirf_Part_Rpt_Ind;
    private DbsField form_Upd_Tirf_Lu_User;
    private DbsField form_Upd_Tirf_Lu_Ts;
    private DbsField form_Upd_Tirf_Company_Cde;
    private DbsField pnd_Ira_Start;

    private DbsGroup pnd_Ira_Start__R_Field_5;
    private DbsField pnd_Ira_Start_Twrc_Tax_Year;
    private DbsField pnd_Ira_Start_Twrc_Status;
    private DbsField pnd_Ira_Start_Twrc_Tax_Id;
    private DbsField pnd_Ira_Start_Twrc_Contract;
    private DbsField pnd_Ira_Start_Twrc_Payee;
    private DbsField pnd_Ira_End;

    private DbsGroup pnd_Ws1;
    private DbsField pnd_Ws1_Pnd_Read_Cnt1;
    private DbsField pnd_Ws1_Pnd_Update_Cnt;
    private DbsField pnd_Ws1_Pnd_Et_Cnt;
    private DbsField pnd_Ws1_Pnd_Message;
    private DbsField et_Limit;
    private DbsField pnd_Contract_Txt1;
    private DbsField pnd_Contract_Hdr_Lit1;

    private DbsGroup pnd_Tiaa_Totals;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax;
    private DbsField pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont;
    private DbsField pnd_Dist_Exempt_Amt;

    private DbsGroup pnd_Trust_Totals;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Fed;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_State;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Local;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Irr;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_R_Int;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Fed;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_State;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Local;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Irr;
    private DbsField pnd_Trust_Totals_Pnd_Trust_1099_I_Int;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Taxable;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Int;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Fed;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax;
    private DbsField pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont;

    private DbsGroup pnd_Tiaa_Email_Bypassed;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B;

    private DbsGroup pnd_Tiaa_Email_Accepted;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A;

    private DbsGroup pnd_Trust_Email_Bypassed;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B;

    private DbsGroup pnd_Trust_Email_Accepted;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A;
    private DbsField pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A;

    private DbsGroup pnd_Trust_Paper_Bypassed;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_State_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Gross_Amt_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Taxable_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Ivc_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Fed_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Local_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Int_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_State_P_B;
    private DbsField pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Irr_P_B;

    private DbsGroup pnd_Tiaa_Trust_1099_R_Total;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A;
    private DbsField pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A;

    private DbsGroup pnd_Tiaa_Trust_1099_I_Total;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_B;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_A;
    private DbsField pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_A;

    private DbsGroup pnd_Tiaa_Totals_1042s;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca;
    private DbsField pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca;

    private DbsGroup pnd_Trust_Totals_1042s;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca;
    private DbsField pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca;

    private DbsGroup pnd_Tiaa_Email_Bypassed_1042s;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B;

    private DbsGroup pnd_Tiaa_Email_Accepted_1042s;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A;

    private DbsGroup pnd_Trust_Email_Bypassed_1042s;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B;
    private DbsField pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B;

    private DbsGroup pnd_Trust_Email_Accepted_1042s;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A;
    private DbsField pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A;

    private DbsGroup pnd_Tiaa_Trust_1042s_Total;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A;
    private DbsField pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A;

    private DbsGroup pnd_Tiaa_Trust_4807c_Total;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea;
    private DbsField pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea;

    private DbsGroup pnd_Tiaa_Totals_Nr4;
    private DbsField pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4;
    private DbsField pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt;
    private DbsField pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int;
    private DbsField pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax;

    private DbsGroup pnd_Trust_Totals_Nr4;
    private DbsField pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4;
    private DbsField pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt;
    private DbsField pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int;
    private DbsField pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax;

    private DbsGroup pnd_Tiaa_Email_Bypassed_Nr4;
    private DbsField pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B;
    private DbsField pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B;

    private DbsGroup pnd_Tiaa_Email_Accepted_Nr4;
    private DbsField pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A;
    private DbsField pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A;

    private DbsGroup pnd_Tiaa_Paper_Bypassed_Nr4;
    private DbsField pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_P_B;
    private DbsField pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_P_B;
    private DbsField pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_P_B;
    private DbsField pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_P_B;

    private DbsGroup pnd_Trust_Email_Bypassed_Nr4;
    private DbsField pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B;
    private DbsField pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B;

    private DbsGroup pnd_Trust_Email_Accepted_Nr4;
    private DbsField pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A;
    private DbsField pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A;
    private DbsField pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A;
    private DbsField pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A;

    private DbsGroup pnd_Tiaa_Trust_Nr4_Total;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A;
    private DbsField pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A;

    private DbsGroup pnd_Db_Read_Fields;
    private DbsField pnd_Db_Read_Fields_Pnd_Cnt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Sep_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Account_Fmv_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db;

    private DbsGroup bypassed_Fields;
    private DbsField bypassed_Fields_Pnd_Cnt_B;
    private DbsField bypassed_Fields_Pnd_Trad_Ira_Contrib_B;
    private DbsField bypassed_Fields_Pnd_Sep_Amt_B;
    private DbsField bypassed_Fields_Pnd_Trad_Ira_Rollover_B;
    private DbsField bypassed_Fields_Pnd_Ira_Rechar_Amt_B;
    private DbsField bypassed_Fields_Pnd_Account_Fmv_B;
    private DbsField bypassed_Fields_Pnd_Roth_Ira_Contrib_B;
    private DbsField bypassed_Fields_Pnd_Roth_Ira_Conversion_B;
    private DbsField bypassed_Fields_Pnd_Postpn_Amt_B;
    private DbsField bypassed_Fields_Pnd_Repayments_Amt_B;

    private DbsGroup accepted_Fields;
    private DbsField accepted_Fields_Pnd_Cnt_A;
    private DbsField accepted_Fields_Pnd_Trad_Ira_Contrib_A;
    private DbsField accepted_Fields_Pnd_Sep_Amt_A;
    private DbsField accepted_Fields_Pnd_Trad_Ira_Rollover_A;
    private DbsField accepted_Fields_Pnd_Ira_Rechar_Amt_A;
    private DbsField accepted_Fields_Pnd_Account_Fmv_A;
    private DbsField accepted_Fields_Pnd_Roth_Ira_Contrib_A;
    private DbsField accepted_Fields_Pnd_Roth_Ira_Conversion_A;
    private DbsField accepted_Fields_Pnd_Postpn_Amt_A;
    private DbsField accepted_Fields_Pnd_Repayments_Amt_A;

    private DbsGroup pnd_P1_Db_Read_Fields;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db;
    private DbsField pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db;

    private DbsGroup pnd_P1_Bypassed_Fields;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B;
    private DbsField pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B;

    private DbsGroup pnd_P1_Accepted_Fields;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Cnt_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Contrib_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Sep_Amt_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Rollover_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Ira_Rechar_Amt_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Account_Fmv_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Contrib_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Conversion_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Postpn_Amt_A;
    private DbsField pnd_P1_Accepted_Fields_Pnd_P1_Repayments_Amt_A;

    private DbsGroup pnd_P2_Db_Read_Fields;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db;
    private DbsField pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db;

    private DbsGroup pnd_P2_Bypassed_Fields;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B;
    private DbsField pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B;

    private DbsGroup pnd_P2_Accepted_Fields;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Cnt_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Contrib_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Sep_Amt_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Rollover_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Ira_Rechar_Amt_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Account_Fmv_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Contrib_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Conversion_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Postpn_Amt_A;
    private DbsField pnd_P2_Accepted_Fields_Pnd_P2_Repayments_Amt_A;
    private DbsField pnd_Total_Unq_Tin;
    private DbsField pnd_Unq_Tin_B;
    private DbsField pnd_Unq_Tin_A;
    private DbsField pnd_P1_Total_Unq_Tin;
    private DbsField pnd_P1_Unq_Tin_B;
    private DbsField pnd_P1_Unq_Tin_A;
    private DbsField pnd_P2_Total_Unq_Tin;
    private DbsField pnd_P2_Unq_Tin_B;
    private DbsField pnd_P2_Unq_Tin_A;
    private DbsField pnd_P2_Indicator;
    private DbsField pnd_Hold_Rpt_Tin;
    private DbsField pnd_C_1099_R;
    private DbsField pnd_Wkfle_Recd_Cnt;
    private DbsField pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Year__R_Field_6;
    private DbsField pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A;
    private DbsField pnd_Ws_Form_Type;
    private DbsField pnd_I;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_In_Form_Type;
    private DbsField pnd_Bypass_Reason;
    private DbsField pnd_Tin_Hold;
    private DbsField pnd_Test_Ind_Cnt;
    private DbsField pnd_Cnt;
    private DbsField pnd_Ws_Progname;
    private DbsField pnd_Coupon_Cnt;
    private DbsField pnd_Total_Gross_Amt;
    private DbsField pnd_Total_Int;
    private DbsField pnd_Total_Refund;
    private DbsField pnd_Total_Tax_Non_Fatca;
    private DbsField pnd_Total_Tax_Fatca;
    private DbsField pnd_Message1;
    private DbsField pnd_Type;
    private DbsField pnd_Total_Type;
    private DbsField pnd_Non_Fatca_Tax;
    private DbsField pnd_Fatca_Tax;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Stored_Tin;
    private DbsField pnd_Ws_Pnd_Restart_Text;
    private DbsField pnd_Ws_Pnd_Mobius_Status;
    private DbsField pnd_Ws_Pnd_Customer_Id;

    private DbsGroup pnd_Ws__R_Field_7;
    private DbsField pnd_Ws_Pnd_Pin;
    private DbsField pnd_Ws_Pnd_Hold_Customer_Id;
    private DbsField pnd_Ws_Pnd_Link_Let_Type;
    private DbsField pnd_Ws_Pnd_Form_Page_Cnt;
    private DbsField pnd_Ws_Pnd_Zero_Isn;
    private DbsField pnd_Ws_Pnd_Sys_Date;
    private DbsField pnd_Ws_Pnd_Sys_Time;
    private DbsField pnd_Ws_Pnd_Message;

    private DbsGroup pnd_Ws_Counters;
    private DbsField pnd_Ws_Pnd_Read_Cnt;
    private DbsField pnd_Ws_Pnd_Form01_Suny_Skip_Cnt;
    private DbsField pnd_Ws_Pnd_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Part_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Cpm_Lookup_Cnt;
    private DbsField pnd_Ws_Pnd_Tax_Email_Cnt;
    private DbsField pnd_Ws_Pnd_No_Tax_Email_Cnt;
    private DbsField pnd_Ws_Pnd_Email_Non_Email_Bypassed;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt;
    private DbsField pnd_Ws_Pnd_Missing_Pin_Cnt_Good;
    private DbsField pnd_Ws_Pnd_Missing_Ssn_Cnt;
    private DbsField pnd_Ws_Pnd_Paper_Print_Cnt;
    private DbsField pnd_Ws_Pnd_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_5498_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_Letr_Bypass_Cnt;
    private DbsField pnd_Ws_Pnd_Form_Pages_Cnt;
    private DbsField pnd_Ws_Pnd_Multi_Page_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Total_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Total_Form_Cnt_Xics;
    private DbsField pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Tirf_Moore_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Pin_A;
    private DbsField pnd_Ws_Pnd_Debug;
    private DbsField pnd_Superde_12;

    private DbsGroup pnd_Superde_12__R_Field_8;
    private DbsField pnd_Superde_12_Pnd_Sd12_Tax_Year;
    private DbsField pnd_Superde_12_Pnd_Sd12_Form_Type;
    private DbsField pnd_Superde_12_Pnd_Sd12_Active_Ind;
    private DbsField pnd_Superde_12_Pnd_Sd12_Mobius_Ind;
    private DbsField pnd_Superde_12_Pnd_Sd12_Tin;
    private DbsField pnd_Superde_12_End;
    private DbsField pnd_Superde_01;

    private DbsGroup pnd_Superde_01__R_Field_9;
    private DbsField pnd_Superde_01_Pnd_Sd1_Tax_Year;
    private DbsField pnd_Superde_01_Pnd_Sd1_Tirf_Tin;
    private DbsField pnd_Superde_01_Pnd_Sd1_Form_Type;
    private DbsField pnd_Form_Type_1_10_Found;
    private DbsField pnd_Contract_Txt;
    private DbsField pnd_Contract_Hdr_Lit;
    private DbsField pnd_I2;
    private DbsField pnd_C;
    private DbsField pnd_I10;
    private DbsField pnd_Y_Cnt;
    private DbsField pnd_Z;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Coupon_Ind;

    private DbsGroup pnd_Ltr_Array;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Tin;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Cntrct;
    private DbsField pnd_Ltr_Array_Pnd_Ltr_Data_Innd;
    private DbsField pnd_Additional_Forms;
    private DbsField pnd_Ws_Save_Date_N;
    private DbsField pls_Email_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        pdaTwra0214 = new PdaTwra0214(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl2001 = new LdaTwrl2001();
        registerRecord(ldaTwrl2001);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrl9610 = new LdaTwrl9610();
        registerRecord(ldaTwrl9610);
        registerRecord(ldaTwrl9610.getVw_ira());

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_Tirf_Tax_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Tirf_Form_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Tirf_Form_Type.setDdmHeader("FORM");
        form_Tirf_Company_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_Tirf_Tin = vw_form.getRecord().newFieldInGroup("form_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Tirf_Tax_Id_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        form_Tirf_Contract_Nbr = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_Tirf_Payee_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Tirf_Key = vw_form.getRecord().newFieldInGroup("form_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, "TIRF_KEY");
        form_Tirf_Key.setDdmHeader("KEY");
        form_Tirf_Active_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_Tirf_Deceased_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Deceased_Ind", "TIRF-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_DECEASED_IND");
        form_Tirf_Deceased_Ind.setDdmHeader("DECE-/ASED/IND");
        form_Tirf_Empty_Form = vw_form.getRecord().newFieldInGroup("form_Tirf_Empty_Form", "TIRF-EMPTY-FORM", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_EMPTY_FORM");
        form_Tirf_Empty_Form.setDdmHeader("EMPTY/FORM");
        form_Tirf_Moore_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Moore_Hold_Ind", "TIRF-MOORE-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_HOLD_IND");
        form_Tirf_Moore_Hold_Ind.setDdmHeader("MOORE/HOLD/IND");
        form_Tirf_Paper_Print_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Paper_Print_Hold_Ind", "TIRF-PAPER-PRINT-HOLD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRF_PAPER_PRINT_HOLD_IND");
        form_Tirf_Paper_Print_Hold_Ind.setDdmHeader("PAPER/PRINT/HOLD");
        form_Tirf_Mobius_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Tirf_Pin = vw_form.getRecord().newFieldInGroup("form_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "TIRF_PIN");
        form_Tirf_Pin.setDdmHeader("PIN");
        form_Tirf_Prev_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Prev_Rpt_Ind", "TIRF-PREV-RPT-IND", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_PREV_RPT_IND");
        form_Tirf_Prev_Rpt_Ind.setDdmHeader("PREV/RPT/IND");
        form_Tirf_Ira_Distr = vw_form.getRecord().newFieldInGroup("form_Tirf_Ira_Distr", "TIRF-IRA-DISTR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRA_DISTR");
        form_Tirf_Ira_Distr.setDdmHeader("IRA/DISTRI-/BUTION");
        form_Tirf_Pct_Of_Tot = vw_form.getRecord().newFieldInGroup("form_Tirf_Pct_Of_Tot", "TIRF-PCT-OF-TOT", FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, 
            "TIRF_PCT_OF_TOT");
        form_Tirf_Pct_Of_Tot.setDdmHeader("PERCENT/OF TOTAL/DISTRIB.");
        form_Tirf_Tot_Distr = vw_form.getRecord().newFieldInGroup("form_Tirf_Tot_Distr", "TIRF-TOT-DISTR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TOT_DISTR");
        form_Tirf_Tot_Distr.setDdmHeader("TOTAL/DISTRI-/BUTION");
        form_Tirf_Foreign_Addr = vw_form.getRecord().newFieldInGroup("form_Tirf_Foreign_Addr", "TIRF-FOREIGN-ADDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_FOREIGN_ADDR");
        form_Tirf_Foreign_Addr.setDdmHeader("FORE-/IGN/ADDR");
        form_Tirf_Moore_Test_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Moore_Test_Ind", "TIRF-MOORE-TEST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_TEST_IND");
        form_Tirf_Moore_Test_Ind.setDdmHeader("MOORE/TEST/IND");
        form_Tirf_Zip = vw_form.getRecord().newFieldInGroup("form_Tirf_Zip", "TIRF-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "TIRF_ZIP");
        form_Tirf_Zip.setDdmHeader("ZIP");
        form_Tirf_Participant_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Participant_Name", "TIRF-PARTICIPANT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "TIRF_PARTICIPANT_NAME");
        form_Tirf_Participant_Name.setDdmHeader("PARTICIPANT/NAME");
        form_Tirf_Addr_Ln1 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln1", "TIRF-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN1");
        form_Tirf_Addr_Ln1.setDdmHeader("ADDRESS/LINE 1");
        form_Tirf_Addr_Ln2 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln2", "TIRF-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN2");
        form_Tirf_Addr_Ln2.setDdmHeader("ADDRESS/LINE 2");
        form_Tirf_Addr_Ln3 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln3", "TIRF-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN3");
        form_Tirf_Addr_Ln3.setDdmHeader("ADDRESS/LINE 3");
        form_Tirf_Addr_Ln4 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln4", "TIRF-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN4");
        form_Tirf_Addr_Ln4.setDdmHeader("ADDRESS/LINE 4");
        form_Tirf_Addr_Ln5 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln5", "TIRF-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN5");
        form_Tirf_Addr_Ln5.setDdmHeader("ADDRESS/LINE 5");
        form_Tirf_Addr_Ln6 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln6", "TIRF-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN6");
        form_Tirf_Addr_Ln6.setDdmHeader("ADDRESS/LINE 6");
        form_Tirf_Walk_Rte = vw_form.getRecord().newFieldInGroup("form_Tirf_Walk_Rte", "TIRF-WALK-RTE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_WALK_RTE");
        form_Tirf_Walk_Rte.setDdmHeader("WALK/ROUTE");
        form_Tirf_Geo_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Geo_Cde", "TIRF-GEO-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_GEO_CDE");
        form_Tirf_Geo_Cde.setDdmHeader("GEO/CODE");
        form_Tirf_Country_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Country_Name", "TIRF-COUNTRY-NAME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_COUNTRY_NAME");
        form_Tirf_Country_Name.setDdmHeader("COUNTRY NAME");
        form_Tirf_Gross_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Gross_Amt", "TIRF-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_GROSS_AMT");
        form_Tirf_Gross_Amt.setDdmHeader("GROSS/DISTRI-/BUTION");
        form_Tirf_Int_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Int_Amt", "TIRF-INT-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIRF_INT_AMT");
        form_Tirf_Int_Amt.setDdmHeader("INTEREST/AMOUNT");
        form_Tirf_Taxable_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Taxable_Amt", "TIRF-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_TAXABLE_AMT");
        form_Tirf_Taxable_Amt.setDdmHeader("TAXABLE/AMOUNT");
        form_Tirf_Fed_Tax_Wthld = vw_form.getRecord().newFieldInGroup("form_Tirf_Fed_Tax_Wthld", "TIRF-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "TIRF_FED_TAX_WTHLD");
        form_Tirf_Fed_Tax_Wthld.setDdmHeader("FEDERAL/TAX/WITHHELD");
        form_Tirf_Ivc_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Ivc_Amt", "TIRF-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "TIRF_IVC_AMT");
        form_Tirf_Ivc_Amt.setDdmHeader("IVC/AMOUNT");
        form_Tirf_Tot_Contractual_Ivc = vw_form.getRecord().newFieldInGroup("form_Tirf_Tot_Contractual_Ivc", "TIRF-TOT-CONTRACTUAL-IVC", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIRF_TOT_CONTRACTUAL_IVC");
        form_Tirf_Tot_Contractual_Ivc.setDdmHeader("TOTAL/CONTRACTUAL/IVC");
        form_Tirf_Trad_Ira_Rollover = vw_form.getRecord().newFieldInGroup("form_Tirf_Trad_Ira_Rollover", "TIRF-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_ROLLOVER");
        form_Tirf_Trad_Ira_Rollover.setDdmHeader("TRADITIONAL/IRA/ROLLOVERS");

        form__R_Field_1 = vw_form.getRecord().newGroupInGroup("form__R_Field_1", "REDEFINE", form_Tirf_Trad_Ira_Rollover);
        form_Tirf_Irr_Amt = form__R_Field_1.newFieldInGroup("form_Tirf_Irr_Amt", "TIRF-IRR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        form_Count_Casttirf_1099_R_State_Grp = vw_form.getRecord().newFieldInGroup("form_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");

        form_Tirf_1099_R_State_Grp = vw_form.getRecord().newGroupInGroup("form_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_Tirf_State_Rpt_Ind = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Rpt_Ind", "TIRF-STATE-RPT-IND", FieldType.STRING, 1, 
            new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_Tirf_State_Hardcopy_Ind = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Hardcopy_Ind", "TIRF-STATE-HARDCOPY-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_HARDCOPY_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Hardcopy_Ind.setDdmHeader("HARD-/COPY/IND");
        form_Tirf_State_Irs_Rpt_Ind = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Irs_Rpt_Ind", "TIRF-STATE-IRS-RPT-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_IRS_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_Tirf_State_Code = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_Tirf_State_Distr = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Distr", "TIRF-STATE-DISTR", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Distr.setDdmHeader("STATE/DISTRI-/BUTION");
        form_Tirf_State_Tax_Wthld = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Tax_Wthld", "TIRF-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Tax_Wthld.setDdmHeader("STATE/TAX/WITHHELD");
        form_Tirf_Loc_Code = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_Loc_Code", "TIRF-LOC-CODE", FieldType.STRING, 5, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_Loc_Code.setDdmHeader("LOCA-/LITY/CODE");
        form_Tirf_Loc_Distr = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_Loc_Distr", "TIRF-LOC-DISTR", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_DISTR", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_Loc_Distr.setDdmHeader("LOCALITY/DISTRI-/BUTION");
        form_Tirf_Loc_Tax_Wthld = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_Loc_Tax_Wthld", "TIRF-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_Loc_Tax_Wthld.setDdmHeader("LOCAL/TAX/WITHHELD");
        form_Tirf_State_Auth_Rpt_Ind = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_Tirf_State_Auth_Rpt_Date = form_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", FieldType.DATE, 
            new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        form_Tirf_Roth_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Year", "TIRF-ROTH-YEAR", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "TIRF_ROTH_YEAR");
        form_Tirf_Roth_Year.setDdmHeader("1 YEAR/DESIGNATED/ROTH CONTR.");
        form_Tirf_Distribution_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Distribution_Cde", "TIRF-DISTRIBUTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_DISTRIBUTION_CDE");
        form_Tirf_Distribution_Cde.setDdmHeader("DISTRI-/BUTION/CODE");
        form_Tirf_Taxable_Not_Det = vw_form.getRecord().newFieldInGroup("form_Tirf_Taxable_Not_Det", "TIRF-TAXABLE-NOT-DET", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAXABLE_NOT_DET");
        form_Tirf_Taxable_Not_Det.setDdmHeader("TAXABLE/AMOUNT NOT/DETERMINED");
        form_Tirf_Pr_Cntl_Num = vw_form.getRecord().newFieldInGroup("form_Tirf_Pr_Cntl_Num", "TIRF-PR-CNTL-NUM", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_PR_CNTL_NUM");
        form_Tirf_Pr_Cntl_Num.setDdmHeader("PUERTO RICO/CONTROL/NUMBER");
        form_Tirf_Trad_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Trad_Ira_Contrib", "TIRF-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_CONTRIB");
        form_Tirf_Trad_Ira_Contrib.setDdmHeader("TRADITIONAL/IRA/CONTRIBUTION");

        form__R_Field_2 = vw_form.getRecord().newGroupInGroup("form__R_Field_2", "REDEFINE", form_Tirf_Trad_Ira_Contrib);
        form_Tirf_Pr_Gross_Amt_Roll = form__R_Field_2.newFieldInGroup("form_Tirf_Pr_Gross_Amt_Roll", "TIRF-PR-GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 
            11, 2);

        form__R_Field_3 = vw_form.getRecord().newGroupInGroup("form__R_Field_3", "REDEFINE", form_Tirf_Trad_Ira_Contrib);
        form_Tirf_Summ_State_Tax_Wthld = form__R_Field_3.newFieldInGroup("form_Tirf_Summ_State_Tax_Wthld", "TIRF-SUMM-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            2, 2);
        form_Tirf_Roth_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Ira_Contrib", "TIRF-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONTRIB");
        form_Tirf_Roth_Ira_Contrib.setDdmHeader("ROTH/IRA/CONTRIBUTION");

        form__R_Field_4 = vw_form.getRecord().newGroupInGroup("form__R_Field_4", "REDEFINE", form_Tirf_Roth_Ira_Contrib);
        form_Tirf_Pr_Ivc_Amt_Roll = form__R_Field_4.newFieldInGroup("form_Tirf_Pr_Ivc_Amt_Roll", "TIRF-PR-IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 11, 
            2);
        form_Tirf_Ira_Rechar_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Ira_Rechar_Amt", "TIRF-IRA-RECHAR-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TIRF_IRA_RECHAR_AMT");
        form_Tirf_Ira_Rechar_Amt.setDdmHeader("IRA/RECHAR./AMOUNT");
        form_Tirf_Postpn_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Postpn_Amt", "TIRF-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_AMT");
        form_Tirf_Repayments_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Repayments_Amt", "TIRF-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TIRF_REPAYMENTS_AMT");
        form_Tirf_Roth_Ira_Conversion = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Ira_Conversion", "TIRF-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONVERSION");
        form_Tirf_Roth_Ira_Conversion.setDdmHeader("ROTH/IRA/CONVERSION");
        form_Tirf_Account_Fmv = vw_form.getRecord().newFieldInGroup("form_Tirf_Account_Fmv", "TIRF-ACCOUNT-FMV", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_ACCOUNT_FMV");
        form_Tirf_Account_Fmv.setDdmHeader("FAIR/MARKET/VALUE");
        form_Tirf_Sep_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Sep_Amt", "TIRF-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_SEP_AMT");
        form_Tirf_Sep_Amt.setDdmHeader("SEP IRA/AMT");
        form_Tirf_Superde_3 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_3", "TIRF-SUPERDE-3", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_3");
        form_Tirf_Superde_3.setSuperDescriptor(true);
        form_Tirf_Superde_10 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_10", "TIRF-SUPERDE-10", FieldType.STRING, 31, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_10");
        form_Tirf_Superde_10.setSuperDescriptor(true);
        form_Tirf_Superde_12 = vw_form.getRecord().newFieldInGroup("form_Tirf_Superde_12", "TIRF-SUPERDE-12", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_12");
        form_Tirf_Superde_12.setSuperDescriptor(true);
        form_Tirf_Future_Use = vw_form.getRecord().newFieldInGroup("form_Tirf_Future_Use", "TIRF-FUTURE-USE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_FUTURE_USE");
        form_Count_Casttirf_1042_S_Line_Grp = vw_form.getRecord().newFieldInGroup("form_Count_Casttirf_1042_S_Line_Grp", "C*TIRF-1042-S-LINE-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP");

        form_Tirf_1042_S_Line_Grp = vw_form.getRecord().newGroupInGroup("form_Tirf_1042_S_Line_Grp", "TIRF-1042-S-LINE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP");
        form_Tirf_1042_S_Line_Grp.setDdmHeader("1042-S/LINE/GROUP");
        form_Tirf_1042_S_Income_Code = form_Tirf_1042_S_Line_Grp.newFieldArrayInGroup("form_Tirf_1042_S_Income_Code", "TIRF-1042-S-INCOME-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 6) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_1042_S_INCOME_CODE", "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP");
        form_Tirf_1042_S_Income_Code.setDdmHeader("1042-S/INCOME/CODE");
        form_Tirf_Gross_Income = form_Tirf_1042_S_Line_Grp.newFieldArrayInGroup("form_Tirf_Gross_Income", "TIRF-GROSS-INCOME", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 6) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_GROSS_INCOME", "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP");
        form_Tirf_Gross_Income.setDdmHeader("GROSS/INCOME/PAID");
        form_Tirf_Nra_Tax_Wthld = form_Tirf_1042_S_Line_Grp.newFieldArrayInGroup("form_Tirf_Nra_Tax_Wthld", "TIRF-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 6) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_NRA_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP");
        form_Tirf_Nra_Tax_Wthld.setDdmHeader("NRA/TAX/WITHHELD");

        form_Tirf_1042_S_Line_Grp1 = vw_form.getRecord().newGroupInGroup("form_Tirf_1042_S_Line_Grp1", "TIRF-1042-S-LINE-GRP1", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP1");
        form_Tirf_1042_S_Refund_Amt = form_Tirf_1042_S_Line_Grp1.newFieldArrayInGroup("form_Tirf_1042_S_Refund_Amt", "TIRF-1042-S-REFUND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 6) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_1042_S_REFUND_AMT", "TWRFRM_FORM_FILE_TIRF_1042_S_LINE_GRP1");
        form_Tirf_1042_S_Refund_Amt.setDdmHeader("1042-S/REFUND/AMOUNT");
        form_Count_Casttirf_1042s_Chptr4 = vw_form.getRecord().newFieldInGroup("form_Count_Casttirf_1042s_Chptr4", "C*TIRF-1042S-CHPTR4", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_1042S_CHPTR4");

        form_Tirf_1042s_Chptr4 = vw_form.getRecord().newGroupInGroup("form_Tirf_1042s_Chptr4", "TIRF-1042S-CHPTR4", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1042S_CHPTR4");
        form_Tirf_Chptr4_Income_Cde = form_Tirf_1042s_Chptr4.newFieldArrayInGroup("form_Tirf_Chptr4_Income_Cde", "TIRF-CHPTR4-INCOME-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 6) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_CHPTR4_INCOME_CDE", "TWRFRM_FORM_FILE_TIRF_1042S_CHPTR4");
        registerRecord(vw_form);

        vw_form_10 = new DataAccessProgramView(new NameInfo("vw_form_10", "FORM-10"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_10_Tirf_Tax_Year = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_10_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_10_Tirf_Form_Type = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_10_Tirf_Form_Type.setDdmHeader("FORM");
        form_10_Tirf_Company_Cde = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_10_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_10_Tirf_Pin = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Pin", "TIRF-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "TIRF_PIN");
        form_10_Tirf_Pin.setDdmHeader("PIN");
        form_10_Tirf_Tin = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_10_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_10_Tirf_Contract_Nbr = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_10_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_10_Tirf_Payee_Cde = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_10_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_10_Tirf_Superde_3 = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Superde_3", "TIRF-SUPERDE-3", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_3");
        form_10_Tirf_Superde_3.setSuperDescriptor(true);
        form_10_Tirf_Superde_10 = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Superde_10", "TIRF-SUPERDE-10", FieldType.STRING, 31, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_10");
        form_10_Tirf_Superde_10.setSuperDescriptor(true);
        form_10_Tirf_Active_Ind = vw_form_10.getRecord().newFieldInGroup("form_10_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_10_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        registerRecord(vw_form_10);

        vw_form_Upd = new DataAccessProgramView(new NameInfo("vw_form_Upd", "FORM-UPD"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Upd_Tirf_Tax_Year = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Upd_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Upd_Tirf_Form_Type = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Upd_Tirf_Form_Type.setDdmHeader("FORM");
        form_Upd_Tirf_Tin = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        form_Upd_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Upd_Tirf_Payee_Cde = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Upd_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Upd_Tirf_Mobius_Stat = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat", "TIRF-MOBIUS-STAT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_STAT");
        form_Upd_Tirf_Mobius_Stat.setDdmHeader("MOBIUS/STATUS");
        form_Upd_Tirf_Mobius_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Ind", "TIRF-MOBIUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOBIUS_IND");
        form_Upd_Tirf_Mobius_Ind.setDdmHeader("INCLUDE/IN/MOBIUS");
        form_Upd_Tirf_Mobius_Stat_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Mobius_Stat_Date", "TIRF-MOBIUS-STAT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIRF_MOBIUS_STAT_DATE");
        form_Upd_Tirf_Mobius_Stat_Date.setDdmHeader("MOBIUS/STATUS/DATE");
        form_Upd_Tirf_Part_Rpt_Date = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Date", "TIRF-PART-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_DATE");
        form_Upd_Tirf_Part_Rpt_Date.setDdmHeader("PART/RPT/DATE");
        form_Upd_Tirf_Part_Rpt_Ind = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Upd_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Upd_Tirf_Lu_User = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Upd_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Upd_Tirf_Lu_Ts = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_LU_TS");
        form_Upd_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Upd_Tirf_Company_Cde = vw_form_Upd.getRecord().newFieldInGroup("form_Upd_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Upd_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        registerRecord(vw_form_Upd);

        pnd_Ira_Start = localVariables.newFieldInRecord("pnd_Ira_Start", "#IRA-START", FieldType.STRING, 26);

        pnd_Ira_Start__R_Field_5 = localVariables.newGroupInRecord("pnd_Ira_Start__R_Field_5", "REDEFINE", pnd_Ira_Start);
        pnd_Ira_Start_Twrc_Tax_Year = pnd_Ira_Start__R_Field_5.newFieldInGroup("pnd_Ira_Start_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4);
        pnd_Ira_Start_Twrc_Status = pnd_Ira_Start__R_Field_5.newFieldInGroup("pnd_Ira_Start_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1);
        pnd_Ira_Start_Twrc_Tax_Id = pnd_Ira_Start__R_Field_5.newFieldInGroup("pnd_Ira_Start_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10);
        pnd_Ira_Start_Twrc_Contract = pnd_Ira_Start__R_Field_5.newFieldInGroup("pnd_Ira_Start_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8);
        pnd_Ira_Start_Twrc_Payee = pnd_Ira_Start__R_Field_5.newFieldInGroup("pnd_Ira_Start_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2);
        pnd_Ira_End = localVariables.newFieldInRecord("pnd_Ira_End", "#IRA-END", FieldType.STRING, 26);

        pnd_Ws1 = localVariables.newGroupInRecord("pnd_Ws1", "#WS1");
        pnd_Ws1_Pnd_Read_Cnt1 = pnd_Ws1.newFieldInGroup("pnd_Ws1_Pnd_Read_Cnt1", "#READ-CNT1", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws1_Pnd_Update_Cnt = pnd_Ws1.newFieldInGroup("pnd_Ws1_Pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws1_Pnd_Et_Cnt = pnd_Ws1.newFieldInGroup("pnd_Ws1_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws1_Pnd_Message = pnd_Ws1.newFieldInGroup("pnd_Ws1_Pnd_Message", "#MESSAGE", FieldType.STRING, 30);
        et_Limit = localVariables.newFieldInRecord("et_Limit", "ET-LIMIT", FieldType.PACKED_DECIMAL, 3);
        pnd_Contract_Txt1 = localVariables.newFieldInRecord("pnd_Contract_Txt1", "#CONTRACT-TXT1", FieldType.STRING, 9);
        pnd_Contract_Hdr_Lit1 = localVariables.newFieldInRecord("pnd_Contract_Hdr_Lit1", "#CONTRACT-HDR-LIT1", FieldType.STRING, 9);

        pnd_Tiaa_Totals = localVariables.newGroupInRecord("pnd_Tiaa_Totals", "#TIAA-TOTALS");
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R", "#TIAA-1099-R", FieldType.NUMERIC, 10);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt", "#TIAA-1099-R-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable", "#TIAA-1099-R-TAXABLE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed", "#TIAA-1099-R-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State", "#TIAA-1099-R-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local", "#TIAA-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc", "#TIAA-1099-R-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr", "#TIAA-1099-R-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int", "#TIAA-1099-R-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I", "#TIAA-1099-I", FieldType.NUMERIC, 10);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt", "#TIAA-1099-I-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable", "#TIAA-1099-I-TAXABLE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed", "#TIAA-1099-I-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State", "#TIAA-1099-I-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local", "#TIAA-1099-I-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc", "#TIAA-1099-I-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr", "#TIAA-1099-I-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int", "#TIAA-1099-I-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c", "#TIAA-4807C", FieldType.NUMERIC, 10);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt", "#TIAA-4807C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable", "#TIAA-4807C-TAXABLE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int", "#TIAA-4807C-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed", "#TIAA-4807C-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist", "#TIAA-4807C-ROLL-DIST", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont", "#TIAA-4807C-ROLL-CONT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt", "#TIAA-4807C-EXEMPT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax", "#TIAA-4807C-DISASTER-TAX", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont = pnd_Tiaa_Totals.newFieldInGroup("pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont", "#TIAA-4807C-AFTRTAX-CONT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Dist_Exempt_Amt = localVariables.newFieldInRecord("pnd_Dist_Exempt_Amt", "#DIST-EXEMPT-AMT", FieldType.NUMERIC, 5);

        pnd_Trust_Totals = localVariables.newGroupInRecord("pnd_Trust_Totals", "#TRUST-TOTALS");
        pnd_Trust_Totals_Pnd_Trust_1099_R = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R", "#TRUST-1099-R", FieldType.NUMERIC, 
            10);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt", "#TRUST-1099-R-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable", "#TRUST-1099-R-TAXABLE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Fed = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Fed", "#TRUST-1099-R-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_State = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_State", "#TRUST-1099-R-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Local = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Local", "#TRUST-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc", "#TRUST-1099-R-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Irr = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Irr", "#TRUST-1099-R-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_R_Int = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_R_Int", "#TRUST-1099-R-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I", "#TRUST-1099-I", FieldType.NUMERIC, 
            10);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt", "#TRUST-1099-I-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable", "#TRUST-1099-I-TAXABLE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Fed = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Fed", "#TRUST-1099-I-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_State = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_State", "#TRUST-1099-I-STATE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Local = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Local", "#TRUST-1099-I-LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc", "#TRUST-1099-I-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Irr = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Irr", "#TRUST-1099-I-IRR", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_1099_I_Int = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_1099_I_Int", "#TRUST-1099-I-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c", "#TRUST-4807C", FieldType.NUMERIC, 10);
        pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt", "#TRUST-4807C-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Taxable = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Taxable", "#TRUST-4807C-TAXABLE", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Int = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Int", "#TRUST-4807C-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Fed = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Fed", "#TRUST-4807C-FED", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist", "#TRUST-4807C-ROLL-DIST", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont", "#TRUST-4807C-ROLL-CONT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt", "#TRUST-4807C-EXEMPT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax", "#TRUST-4807C-DISASTER-TAX", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont = pnd_Trust_Totals.newFieldInGroup("pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont", "#TRUST-4807C-AFTRTAX-CONT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Bypassed = localVariables.newGroupInRecord("pnd_Tiaa_Email_Bypassed", "#TIAA-EMAIL-BYPASSED");
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B", "#TIAA-1099-R-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B", 
            "#TIAA-1099-R-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B", 
            "#TIAA-1099-R-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B", "#TIAA-1099-R-IVC-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B", "#TIAA-1099-R-FED-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B", 
            "#TIAA-1099-R-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B", "#TIAA-1099-R-INT-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B", 
            "#TIAA-1099-R-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B", "#TIAA-1099-R-IRR-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B", "#TIAA-1099-I-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B", 
            "#TIAA-1099-I-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B", 
            "#TIAA-1099-I-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B", "#TIAA-1099-I-IVC-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B", "#TIAA-1099-I-FED-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B", 
            "#TIAA-1099-I-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B", "#TIAA-1099-I-INT-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B", 
            "#TIAA-1099-I-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B", "#TIAA-1099-I-IRR-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B", "#TIAA-4807C-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B", 
            "#TIAA-4807C-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B", 
            "#TIAA-4807C-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B", "#TIAA-4807C-INT-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B", "#TIAA-4807C-FED-E-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B", 
            "#TIAA-4807C-ROLL-DIST-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B", 
            "#TIAA-4807C-ROLL-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B", 
            "#TIAA-4807C-EXEMPT-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B", 
            "#TIAA-4807C-DISASTER-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B = pnd_Tiaa_Email_Bypassed.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B", 
            "#TIAA-4807C-AFTRTAX-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Accepted = localVariables.newGroupInRecord("pnd_Tiaa_Email_Accepted", "#TIAA-EMAIL-ACCEPTED");
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A", "#TIAA-1099-R-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A", 
            "#TIAA-1099-R-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A", 
            "#TIAA-1099-R-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A", "#TIAA-1099-R-IVC-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A", "#TIAA-1099-R-FED-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A", 
            "#TIAA-1099-R-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A", "#TIAA-1099-R-INT-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A", 
            "#TIAA-1099-R-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A", "#TIAA-1099-R-IRR-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A", "#TIAA-1099-I-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A", 
            "#TIAA-1099-I-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A", 
            "#TIAA-1099-I-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A", "#TIAA-1099-I-IVC-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A", "#TIAA-1099-I-FED-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A", 
            "#TIAA-1099-I-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A", "#TIAA-1099-I-INT-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A", 
            "#TIAA-1099-I-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A", "#TIAA-1099-I-IRR-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A", "#TIAA-4807C-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A", 
            "#TIAA-4807C-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A", 
            "#TIAA-4807C-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A", "#TIAA-4807C-INT-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A", "#TIAA-4807C-FED-E-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A", 
            "#TIAA-4807C-ROLL-DIST-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A", 
            "#TIAA-4807C-ROLL-CONT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A", 
            "#TIAA-4807C-EXEMPT-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A", 
            "#TIAA-4807C-DISASTER-TAX-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A = pnd_Tiaa_Email_Accepted.newFieldInGroup("pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A", 
            "#TIAA-4807C-AFTRTAX-CONT-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Bypassed = localVariables.newGroupInRecord("pnd_Trust_Email_Bypassed", "#TRUST-EMAIL-BYPASSED");
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B", "#TRUST-1099-R-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B", 
            "#TRUST-1099-R-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B", 
            "#TRUST-1099-R-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B", 
            "#TRUST-1099-R-IVC-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B", 
            "#TRUST-1099-R-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B", 
            "#TRUST-1099-R-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B", 
            "#TRUST-1099-R-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B", 
            "#TRUST-1099-R-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B", 
            "#TRUST-1099-R-IRR-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B", "#TRUST-1099-I-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B", 
            "#TRUST-1099-I-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B", 
            "#TRUST-1099-I-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B", 
            "#TRUST-1099-I-IVC-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B", 
            "#TRUST-1099-I-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B", 
            "#TRUST-1099-I-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B", 
            "#TRUST-1099-I-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B", 
            "#TRUST-1099-I-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B", 
            "#TRUST-1099-I-IRR-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B", "#TRUST-4807C-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B", 
            "#TRUST-4807C-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B", 
            "#TRUST-4807C-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B", 
            "#TRUST-4807C-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B", 
            "#TRUST-4807C-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B", 
            "#TRUST-4807C-ROLL-DIST-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B", 
            "#TRUST-4807C-ROLL-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B", 
            "#TRUST-4807C-EXEMPT-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B", 
            "#TRUST-4807C-DISASTER-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B = pnd_Trust_Email_Bypassed.newFieldInGroup("pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B", 
            "#TRUST-4807C-AFTRTAX-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Accepted = localVariables.newGroupInRecord("pnd_Trust_Email_Accepted", "#TRUST-EMAIL-ACCEPTED");
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_E_A", "#TRUST-1099-R-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A", 
            "#TRUST-1099-R-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A", 
            "#TRUST-1099-R-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A", 
            "#TRUST-1099-R-IVC-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A", 
            "#TRUST-1099-R-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A", 
            "#TRUST-1099-R-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A", 
            "#TRUST-1099-R-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_E_A", 
            "#TRUST-1099-R-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_E_A", 
            "#TRUST-1099-R-IRR-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A", "#TRUST-1099-I-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A", 
            "#TRUST-1099-I-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A", 
            "#TRUST-1099-I-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A", 
            "#TRUST-1099-I-IVC-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A", 
            "#TRUST-1099-I-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A", 
            "#TRUST-1099-I-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A", 
            "#TRUST-1099-I-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A", 
            "#TRUST-1099-I-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A", 
            "#TRUST-1099-I-IRR-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A", "#TRUST-4807C-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A", 
            "#TRUST-4807C-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A", 
            "#TRUST-4807C-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A", 
            "#TRUST-4807C-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A", 
            "#TRUST-4807C-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A", 
            "#TRUST-4807C-ROLL-DIST-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A", 
            "#TRUST-4807C-ROLL-CONT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A", 
            "#TRUST-4807C-EXEMPT-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A", 
            "#TRUST-4807C-DISASTER-TAX-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A = pnd_Trust_Email_Accepted.newFieldInGroup("pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A", 
            "#TRUST-4807C-AFTRTAX-CONT-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Paper_Bypassed = localVariables.newGroupInRecord("pnd_Trust_Paper_Bypassed", "#TRUST-PAPER-BYPASSED");
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_P_B", "#TRUST-1099-R-P-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Gross_Amt_P_B", 
            "#TRUST-1099-R-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Taxable_P_B", 
            "#TRUST-1099-R-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Ivc_P_B", 
            "#TRUST-1099-R-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Fed_P_B", 
            "#TRUST-1099-R-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Local_P_B", 
            "#TRUST-1099-R-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Int_P_B", 
            "#TRUST-1099-R-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_State_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_State_P_B", 
            "#TRUST-1099-R-STATE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_R_Irr_P_B", 
            "#TRUST-1099-R-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_P_B", "#TRUST-1099-I-P-B", 
            FieldType.NUMERIC, 10);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Gross_Amt_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Gross_Amt_P_B", 
            "#TRUST-1099-I-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Taxable_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Taxable_P_B", 
            "#TRUST-1099-I-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Ivc_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Ivc_P_B", 
            "#TRUST-1099-I-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Fed_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Fed_P_B", 
            "#TRUST-1099-I-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Local_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Local_P_B", 
            "#TRUST-1099-I-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Int_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Int_P_B", 
            "#TRUST-1099-I-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_State_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_State_P_B", 
            "#TRUST-1099-I-STATE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Irr_P_B = pnd_Trust_Paper_Bypassed.newFieldInGroup("pnd_Trust_Paper_Bypassed_Pnd_Trust_1099_I_Irr_P_B", 
            "#TRUST-1099-I-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_1099_R_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_1099_R_Total", "#TIAA-TRUST-1099-R-TOTAL");
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R", 
            "#TIAA-TRUST-1099-R", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt", 
            "#TIAA-TRUST-1099-R-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable", 
            "#TIAA-TRUST-1099-R-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed", 
            "#TIAA-TRUST-1099-R-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State", 
            "#TIAA-TRUST-1099-R-STATE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local", 
            "#TIAA-TRUST-1099-R-LOCAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc", 
            "#TIAA-TRUST-1099-R-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr", 
            "#TIAA-TRUST-1099-R-IRR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int", 
            "#TIAA-TRUST-1099-R-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B", 
            "#TIAA-TRUST-1099-R-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B", 
            "#TIAA-TRUST-1099-R-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B", 
            "#TIAA-TRUST-1099-R-IVC-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B", 
            "#TIAA-TRUST-1099-R-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B", 
            "#TIAA-TRUST-1099-R-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B", 
            "#TIAA-TRUST-1099-R-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B", 
            "#TIAA-TRUST-1099-R-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_B", 
            "#TIAA-TRUST-1099-R-IRR-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A", 
            "#TIAA-TRUST-1099-R-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A", 
            "#TIAA-TRUST-1099-R-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A", 
            "#TIAA-TRUST-1099-R-IVC-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A", 
            "#TIAA-TRUST-1099-R-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A", 
            "#TIAA-TRUST-1099-R-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A", 
            "#TIAA-TRUST-1099-R-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A", 
            "#TIAA-TRUST-1099-R-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_A", 
            "#TIAA-TRUST-1099-R-IRR-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_B", 
            "#TIAA-TRUST-1099-R-P-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_B", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_B", 
            "#TIAA-TRUST-1099-R-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_B", 
            "#TIAA-TRUST-1099-R-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_B", 
            "#TIAA-TRUST-1099-R-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_B", 
            "#TIAA-TRUST-1099-R-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_B", 
            "#TIAA-TRUST-1099-R-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_B", 
            "#TIAA-TRUST-1099-R-STATE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_B", 
            "#TIAA-TRUST-1099-R-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_P_A", 
            "#TIAA-TRUST-1099-R-P-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_P_A", 
            "#TIAA-TRUST-1099-R-GROSS-AMT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_P_A", 
            "#TIAA-TRUST-1099-R-TAXABLE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_P_A", 
            "#TIAA-TRUST-1099-R-IVC-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_P_A", 
            "#TIAA-TRUST-1099-R-FED-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_P_A", 
            "#TIAA-TRUST-1099-R-LOCAL-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_P_A", 
            "#TIAA-TRUST-1099-R-INT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_P_A", 
            "#TIAA-TRUST-1099-R-STATE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A = pnd_Tiaa_Trust_1099_R_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_P_A", 
            "#TIAA-TRUST-1099-R-IRR-P-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_1099_I_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_1099_I_Total", "#TIAA-TRUST-1099-I-TOTAL");
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I", 
            "#TIAA-TRUST-1099-I", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt", 
            "#TIAA-TRUST-1099-I-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable", 
            "#TIAA-TRUST-1099-I-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed", 
            "#TIAA-TRUST-1099-I-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State", 
            "#TIAA-TRUST-1099-I-STATE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local", 
            "#TIAA-TRUST-1099-I-LOCAL", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc", 
            "#TIAA-TRUST-1099-I-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr", 
            "#TIAA-TRUST-1099-I-IRR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int", 
            "#TIAA-TRUST-1099-I-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B", 
            "#TIAA-TRUST-1099-I-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B", 
            "#TIAA-TRUST-1099-I-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B", 
            "#TIAA-TRUST-1099-I-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B", 
            "#TIAA-TRUST-1099-I-IVC-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B", 
            "#TIAA-TRUST-1099-I-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B", 
            "#TIAA-TRUST-1099-I-LOCAL-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B", 
            "#TIAA-TRUST-1099-I-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B", 
            "#TIAA-TRUST-1099-I-STATE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_B", 
            "#TIAA-TRUST-1099-I-IRR-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A", 
            "#TIAA-TRUST-1099-I-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A", 
            "#TIAA-TRUST-1099-I-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A", 
            "#TIAA-TRUST-1099-I-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A", 
            "#TIAA-TRUST-1099-I-IVC-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A", 
            "#TIAA-TRUST-1099-I-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A", 
            "#TIAA-TRUST-1099-I-LOCAL-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A", 
            "#TIAA-TRUST-1099-I-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A", 
            "#TIAA-TRUST-1099-I-STATE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_A", 
            "#TIAA-TRUST-1099-I-IRR-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_B", 
            "#TIAA-TRUST-1099-I-P-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_B", 
            "#TIAA-TRUST-1099-I-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_B", 
            "#TIAA-TRUST-1099-I-TAXABLE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_B", 
            "#TIAA-TRUST-1099-I-IVC-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_B", 
            "#TIAA-TRUST-1099-I-FED-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_B", 
            "#TIAA-TRUST-1099-I-LOCAL-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_B", 
            "#TIAA-TRUST-1099-I-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_B", 
            "#TIAA-TRUST-1099-I-STATE-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_B = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_B", 
            "#TIAA-TRUST-1099-I-IRR-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_P_A", 
            "#TIAA-TRUST-1099-I-P-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_P_A", 
            "#TIAA-TRUST-1099-I-GROSS-AMT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_P_A", 
            "#TIAA-TRUST-1099-I-TAXABLE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_P_A", 
            "#TIAA-TRUST-1099-I-IVC-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_P_A", 
            "#TIAA-TRUST-1099-I-FED-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_P_A", 
            "#TIAA-TRUST-1099-I-LOCAL-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_P_A", 
            "#TIAA-TRUST-1099-I-INT-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_P_A", 
            "#TIAA-TRUST-1099-I-STATE-P-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_A = pnd_Tiaa_Trust_1099_I_Total.newFieldInGroup("pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_P_A", 
            "#TIAA-TRUST-1099-I-IRR-P-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Totals_1042s = localVariables.newGroupInRecord("pnd_Tiaa_Totals_1042s", "#TIAA-TOTALS-1042S");
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s", "#TIAA-1042S", FieldType.NUMERIC, 
            10);
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt", "#TIAA-1042S-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund", "#TIAA-1042S-REFUND", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int", "#TIAA-1042S-INT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca", 
            "#TIAA-1042S-TAX-NON-FATCA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca = pnd_Tiaa_Totals_1042s.newFieldInGroup("pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca", "#TIAA-1042S-TAX-FATCA", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Totals_1042s = localVariables.newGroupInRecord("pnd_Trust_Totals_1042s", "#TRUST-TOTALS-1042S");
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s", "#TRUST-1042S", FieldType.NUMERIC, 
            10);
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt", 
            "#TRUST-1042S-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund", "#TRUST-1042S-REFUND", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int", "#TRUST-1042S-INT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca", 
            "#TRUST-1042S-TAX-NON-FATCA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca = pnd_Trust_Totals_1042s.newFieldInGroup("pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca", 
            "#TRUST-1042S-TAX-FATCA", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Bypassed_1042s = localVariables.newGroupInRecord("pnd_Tiaa_Email_Bypassed_1042s", "#TIAA-EMAIL-BYPASSED-1042S");
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B", 
            "#TIAA-1042S-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B", 
            "#TIAA-1042S-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B", 
            "#TIAA-1042S-REFUND-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B", 
            "#TIAA-1042S-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B", 
            "#TIAA-1042S-TAX-NON-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B = pnd_Tiaa_Email_Bypassed_1042s.newFieldInGroup("pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B", 
            "#TIAA-1042S-TAX-FATCA-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Accepted_1042s = localVariables.newGroupInRecord("pnd_Tiaa_Email_Accepted_1042s", "#TIAA-EMAIL-ACCEPTED-1042S");
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A", 
            "#TIAA-1042S-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A", 
            "#TIAA-1042S-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A", 
            "#TIAA-1042S-REFUND-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A", 
            "#TIAA-1042S-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A", 
            "#TIAA-1042S-TAX-NON-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A = pnd_Tiaa_Email_Accepted_1042s.newFieldInGroup("pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A", 
            "#TIAA-1042S-TAX-FATCA-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Bypassed_1042s = localVariables.newGroupInRecord("pnd_Trust_Email_Bypassed_1042s", "#TRUST-EMAIL-BYPASSED-1042S");
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B", 
            "#TRUST-1042S-E-B", FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B", 
            "#TRUST-1042S-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B", 
            "#TRUST-1042S-REFUND-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B", 
            "#TRUST-1042S-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B", 
            "#TRUST-1042S-TAX-NON-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B = pnd_Trust_Email_Bypassed_1042s.newFieldInGroup("pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B", 
            "#TRUST-1042S-TAX-FATCA-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Accepted_1042s = localVariables.newGroupInRecord("pnd_Trust_Email_Accepted_1042s", "#TRUST-EMAIL-ACCEPTED-1042S");
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A", 
            "#TRUST-1042S-E-A", FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A", 
            "#TRUST-1042S-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A", 
            "#TRUST-1042S-REFUND-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A", 
            "#TRUST-1042S-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A", 
            "#TRUST-1042S-TAX-NON-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A = pnd_Trust_Email_Accepted_1042s.newFieldInGroup("pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A", 
            "#TRUST-1042S-TAX-FATCA-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_1042s_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_1042s_Total", "#TIAA-TRUST-1042S-TOTAL");
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s", 
            "#TIAA-TRUST-1042S", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt", 
            "#TIAA-TRUST-1042S-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund", 
            "#TIAA-TRUST-1042S-REFUND", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int", 
            "#TIAA-TRUST-1042S-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non", 
            "#TIAA-TRUST-1042S-TAX-NON", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca", 
            "#TIAA-TRUST-1042S-TAX-FATCA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B", 
            "#TIAA-TRUST-1042S-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B", 
            "#TIAA-TRUST-1042S-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B", 
            "#TIAA-TRUST-1042S-REFUND-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B", 
            "#TIAA-TRUST-1042S-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B", 
            "#TIAA-TRUST-1042S-TAX-NON-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B", 
            "#TIAA-TRUST-1042S-TAX-FATCA-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A", 
            "#TIAA-TRUST-1042S-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A", 
            "#TIAA-TRUST-1042S-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A", 
            "#TIAA-TRUST-1042S-REFUND-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A", 
            "#TIAA-TRUST-1042S-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A", 
            "#TIAA-TRUST-1042S-TAX-NON-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A = pnd_Tiaa_Trust_1042s_Total.newFieldInGroup("pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A", 
            "#TIAA-TRUST-1042S-TAX-FATCA-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_4807c_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_4807c_Total", "#TIAA-TRUST-4807C-TOTAL");
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c", 
            "#TIAA-TRUST-4807C", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt", 
            "#TIAA-TRUST-4807C-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable", 
            "#TIAA-TRUST-4807C-TAXABLE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int", 
            "#TIAA-TRUST-4807C-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed", 
            "#TIAA-TRUST-4807C-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist", 
            "#TIAA-TRUST-4807C-ROLL-DIST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont", 
            "#TIAA-TRUST-4807C-ROLL-CONT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt", 
            "#TIAA-TRUST-4807C-EXEMPT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax", 
            "#TIAA-TRUST-4807C-DISASTER-TAX", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont", 
            "#TIAA-TRUST-4807C-AFTRTAX-CONT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B", 
            "#TIAA-TRUST-4807C-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B", 
            "#TIAA-TRUST-4807C-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B", 
            "#TIAA-TRUST-4807C-TAXABLE-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B", 
            "#TIAA-TRUST-4807C-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B", 
            "#TIAA-TRUST-4807C-FED-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B", 
            "#TIAA-TRUST-4807C-ROLL-DIST-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B", 
            "#TIAA-TRUST-4807C-ROLL-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B", 
            "#TIAA-TRUST-4807C-EXEMPT-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B", 
            "#TIAA-TRUST-4807C-DISASTER-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B", 
            "#TIAA-TRUST-4807C-AFTRTAX-CONT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A", 
            "#TIAA-TRUST-4807C-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A", 
            "#TIAA-TRUST-4807C-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A", 
            "#TIAA-TRUST-4807C-TAXABLE-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A", 
            "#TIAA-TRUST-4807C-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A", 
            "#TIAA-TRUST-4807C-FED-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A", 
            "#TIAA-TRUST-4807C-ROLL-DIST-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A", 
            "#TIAA-TRUST-4807C-ROLL-CONT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A", 
            "#TIAA-TRUST-4807C-EXEMPT-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea", 
            "#TIAA-TRUST-4807C-DISASTER-TAXEA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea = pnd_Tiaa_Trust_4807c_Total.newFieldInGroup("pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea", 
            "#TIAA-TRUST-4807C-AFTRTAX-CONTEA", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Totals_Nr4 = localVariables.newGroupInRecord("pnd_Tiaa_Totals_Nr4", "#TIAA-TOTALS-NR4");
        pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4 = pnd_Tiaa_Totals_Nr4.newFieldInGroup("pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4", "#TIAA-NR4", FieldType.NUMERIC, 10);
        pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt = pnd_Tiaa_Totals_Nr4.newFieldInGroup("pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt", "#TIAA-NR4-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int = pnd_Tiaa_Totals_Nr4.newFieldInGroup("pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int", "#TIAA-NR4-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax = pnd_Tiaa_Totals_Nr4.newFieldInGroup("pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax", "#TIAA-NR4-FED-TAX", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Totals_Nr4 = localVariables.newGroupInRecord("pnd_Trust_Totals_Nr4", "#TRUST-TOTALS-NR4");
        pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4 = pnd_Trust_Totals_Nr4.newFieldInGroup("pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4", "#TRUST-NR4", FieldType.NUMERIC, 
            10);
        pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt = pnd_Trust_Totals_Nr4.newFieldInGroup("pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt", "#TRUST-NR4-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int = pnd_Trust_Totals_Nr4.newFieldInGroup("pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int", "#TRUST-NR4-INT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax = pnd_Trust_Totals_Nr4.newFieldInGroup("pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax", "#TRUST-NR4-FED-TAX", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Bypassed_Nr4 = localVariables.newGroupInRecord("pnd_Tiaa_Email_Bypassed_Nr4", "#TIAA-EMAIL-BYPASSED-NR4");
        pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B = pnd_Tiaa_Email_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B", "#TIAA-NR4-E-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B = pnd_Tiaa_Email_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B", 
            "#TIAA-NR4-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B = pnd_Tiaa_Email_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B", 
            "#TIAA-NR4-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B = pnd_Tiaa_Email_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B", 
            "#TIAA-NR4-FED-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Email_Accepted_Nr4 = localVariables.newGroupInRecord("pnd_Tiaa_Email_Accepted_Nr4", "#TIAA-EMAIL-ACCEPTED-NR4");
        pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A = pnd_Tiaa_Email_Accepted_Nr4.newFieldInGroup("pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A", "#TIAA-NR4-E-A", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A = pnd_Tiaa_Email_Accepted_Nr4.newFieldInGroup("pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A", 
            "#TIAA-NR4-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A = pnd_Tiaa_Email_Accepted_Nr4.newFieldInGroup("pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A", 
            "#TIAA-NR4-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A = pnd_Tiaa_Email_Accepted_Nr4.newFieldInGroup("pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A", 
            "#TIAA-NR4-FED-TAX-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Paper_Bypassed_Nr4 = localVariables.newGroupInRecord("pnd_Tiaa_Paper_Bypassed_Nr4", "#TIAA-PAPER-BYPASSED-NR4");
        pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_P_B = pnd_Tiaa_Paper_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_P_B", "#TIAA-NR4-P-B", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_P_B = pnd_Tiaa_Paper_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_P_B", 
            "#TIAA-NR4-GROSS-AMT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_P_B = pnd_Tiaa_Paper_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_P_B", 
            "#TIAA-NR4-INT-P-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_P_B = pnd_Tiaa_Paper_Bypassed_Nr4.newFieldInGroup("pnd_Tiaa_Paper_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_P_B", 
            "#TIAA-NR4-FED-TAX-P-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Bypassed_Nr4 = localVariables.newGroupInRecord("pnd_Trust_Email_Bypassed_Nr4", "#TRUST-EMAIL-BYPASSED-NR4");
        pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B = pnd_Trust_Email_Bypassed_Nr4.newFieldInGroup("pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B", 
            "#TRUST-NR4-E-B", FieldType.NUMERIC, 10);
        pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B = pnd_Trust_Email_Bypassed_Nr4.newFieldInGroup("pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B", 
            "#TRUST-NR4-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B = pnd_Trust_Email_Bypassed_Nr4.newFieldInGroup("pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B", 
            "#TRUST-NR4-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B = pnd_Trust_Email_Bypassed_Nr4.newFieldInGroup("pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B", 
            "#TRUST-NR4-FED-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Trust_Email_Accepted_Nr4 = localVariables.newGroupInRecord("pnd_Trust_Email_Accepted_Nr4", "#TRUST-EMAIL-ACCEPTED-NR4");
        pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A = pnd_Trust_Email_Accepted_Nr4.newFieldInGroup("pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A", 
            "#TRUST-NR4-E-A", FieldType.NUMERIC, 10);
        pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A = pnd_Trust_Email_Accepted_Nr4.newFieldInGroup("pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A", 
            "#TRUST-NR4-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A = pnd_Trust_Email_Accepted_Nr4.newFieldInGroup("pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A", 
            "#TRUST-NR4-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A = pnd_Trust_Email_Accepted_Nr4.newFieldInGroup("pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A", 
            "#TRUST-NR4-FED-TAX-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Tiaa_Trust_Nr4_Total = localVariables.newGroupInRecord("pnd_Tiaa_Trust_Nr4_Total", "#TIAA-TRUST-NR4-TOTAL");
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4 = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4", "#TIAA-TRUST-NR4", 
            FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt", 
            "#TIAA-TRUST-NR4-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int", 
            "#TIAA-TRUST-NR4-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax", 
            "#TIAA-TRUST-NR4-FED-TAX", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B", 
            "#TIAA-TRUST-NR4-E-B", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B", 
            "#TIAA-TRUST-NR4-GROSS-AMT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B", 
            "#TIAA-TRUST-NR4-INT-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B", 
            "#TIAA-TRUST-NR4-FED-TAX-E-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A", 
            "#TIAA-TRUST-NR4-E-A", FieldType.NUMERIC, 10);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A", 
            "#TIAA-TRUST-NR4-GROSS-AMT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A", 
            "#TIAA-TRUST-NR4-INT-E-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A = pnd_Tiaa_Trust_Nr4_Total.newFieldInGroup("pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A", 
            "#TIAA-TRUST-NR4-FED-TAX-E-A", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Db_Read_Fields = localVariables.newGroupInRecord("pnd_Db_Read_Fields", "#DB-READ-FIELDS");
        pnd_Db_Read_Fields_Pnd_Cnt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Cnt_Db", "#CNT-DB", FieldType.NUMERIC, 8);
        pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db", "#TRAD-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Sep_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Sep_Amt_Db", "#SEP-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db", "#TRAD-IRA-ROLLOVER-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db", "#IRA-RECHAR-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Account_Fmv_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Account_Fmv_Db", "#ACCOUNT-FMV-DB", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db", "#ROTH-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db", "#ROTH-IRA-CONVERSION-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db", "#POSTPN-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db", "#REPAYMENTS-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);

        bypassed_Fields = localVariables.newGroupInRecord("bypassed_Fields", "BYPASSED-FIELDS");
        bypassed_Fields_Pnd_Cnt_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Cnt_B", "#CNT-B", FieldType.NUMERIC, 8);
        bypassed_Fields_Pnd_Trad_Ira_Contrib_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Trad_Ira_Contrib_B", "#TRAD-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        bypassed_Fields_Pnd_Sep_Amt_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Sep_Amt_B", "#SEP-AMT-B", FieldType.PACKED_DECIMAL, 11, 2);
        bypassed_Fields_Pnd_Trad_Ira_Rollover_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Trad_Ira_Rollover_B", "#TRAD-IRA-ROLLOVER-B", FieldType.PACKED_DECIMAL, 
            13, 2);
        bypassed_Fields_Pnd_Ira_Rechar_Amt_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Ira_Rechar_Amt_B", "#IRA-RECHAR-AMT-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        bypassed_Fields_Pnd_Account_Fmv_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Account_Fmv_B", "#ACCOUNT-FMV-B", FieldType.PACKED_DECIMAL, 
            13, 2);
        bypassed_Fields_Pnd_Roth_Ira_Contrib_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Roth_Ira_Contrib_B", "#ROTH-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        bypassed_Fields_Pnd_Roth_Ira_Conversion_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Roth_Ira_Conversion_B", "#ROTH-IRA-CONVERSION-B", 
            FieldType.PACKED_DECIMAL, 11, 2);
        bypassed_Fields_Pnd_Postpn_Amt_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Postpn_Amt_B", "#POSTPN-AMT-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        bypassed_Fields_Pnd_Repayments_Amt_B = bypassed_Fields.newFieldInGroup("bypassed_Fields_Pnd_Repayments_Amt_B", "#REPAYMENTS-AMT-B", FieldType.PACKED_DECIMAL, 
            11, 2);

        accepted_Fields = localVariables.newGroupInRecord("accepted_Fields", "ACCEPTED-FIELDS");
        accepted_Fields_Pnd_Cnt_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Cnt_A", "#CNT-A", FieldType.NUMERIC, 8);
        accepted_Fields_Pnd_Trad_Ira_Contrib_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Trad_Ira_Contrib_A", "#TRAD-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        accepted_Fields_Pnd_Sep_Amt_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Sep_Amt_A", "#SEP-AMT-A", FieldType.PACKED_DECIMAL, 11, 2);
        accepted_Fields_Pnd_Trad_Ira_Rollover_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Trad_Ira_Rollover_A", "#TRAD-IRA-ROLLOVER-A", FieldType.PACKED_DECIMAL, 
            13, 2);
        accepted_Fields_Pnd_Ira_Rechar_Amt_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Ira_Rechar_Amt_A", "#IRA-RECHAR-AMT-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        accepted_Fields_Pnd_Account_Fmv_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Account_Fmv_A", "#ACCOUNT-FMV-A", FieldType.PACKED_DECIMAL, 
            13, 2);
        accepted_Fields_Pnd_Roth_Ira_Contrib_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Roth_Ira_Contrib_A", "#ROTH-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        accepted_Fields_Pnd_Roth_Ira_Conversion_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Roth_Ira_Conversion_A", "#ROTH-IRA-CONVERSION-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        accepted_Fields_Pnd_Postpn_Amt_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Postpn_Amt_A", "#POSTPN-AMT-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        accepted_Fields_Pnd_Repayments_Amt_A = accepted_Fields.newFieldInGroup("accepted_Fields_Pnd_Repayments_Amt_A", "#REPAYMENTS-AMT-A", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_P1_Db_Read_Fields = localVariables.newGroupInRecord("pnd_P1_Db_Read_Fields", "#P1-DB-READ-FIELDS");
        pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db", "#P1-CNT-DB", FieldType.NUMERIC, 
            8);
        pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db", "#P1-TRAD-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db", "#P1-SEP-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db", 
            "#P1-TRAD-IRA-ROLLOVER-DB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db", "#P1-IRA-RECHAR-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db", "#P1-ACCOUNT-FMV-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db", "#P1-ROTH-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db", 
            "#P1-ROTH-IRA-CONVERSION-DB", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db", "#P1-POSTPN-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db = pnd_P1_Db_Read_Fields.newFieldInGroup("pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db", "#P1-REPAYMENTS-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_P1_Bypassed_Fields = localVariables.newGroupInRecord("pnd_P1_Bypassed_Fields", "#P1-BYPASSED-FIELDS");
        pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B", "#P1-CNT-B", FieldType.NUMERIC, 
            8);
        pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B", 
            "#P1-TRAD-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B", "#P1-SEP-AMT-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B", 
            "#P1-TRAD-IRA-ROLLOVER-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B", "#P1-IRA-RECHAR-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B", "#P1-ACCOUNT-FMV-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B", 
            "#P1-ROTH-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B", 
            "#P1-ROTH-IRA-CONVERSION-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B", "#P1-POSTPN-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B = pnd_P1_Bypassed_Fields.newFieldInGroup("pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B", "#P1-REPAYMENTS-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_P1_Accepted_Fields = localVariables.newGroupInRecord("pnd_P1_Accepted_Fields", "#P1-ACCEPTED-FIELDS");
        pnd_P1_Accepted_Fields_Pnd_P1_Cnt_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Cnt_A", "#P1-CNT-A", FieldType.NUMERIC, 
            8);
        pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Contrib_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Contrib_A", 
            "#P1-TRAD-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Sep_Amt_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Sep_Amt_A", "#P1-SEP-AMT-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Rollover_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Rollover_A", 
            "#P1-TRAD-IRA-ROLLOVER-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Ira_Rechar_Amt_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Ira_Rechar_Amt_A", "#P1-IRA-RECHAR-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Account_Fmv_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Account_Fmv_A", "#P1-ACCOUNT-FMV-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Contrib_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Contrib_A", 
            "#P1-ROTH-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Conversion_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Conversion_A", 
            "#P1-ROTH-IRA-CONVERSION-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Postpn_Amt_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Postpn_Amt_A", "#P1-POSTPN-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P1_Accepted_Fields_Pnd_P1_Repayments_Amt_A = pnd_P1_Accepted_Fields.newFieldInGroup("pnd_P1_Accepted_Fields_Pnd_P1_Repayments_Amt_A", "#P1-REPAYMENTS-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_P2_Db_Read_Fields = localVariables.newGroupInRecord("pnd_P2_Db_Read_Fields", "#P2-DB-READ-FIELDS");
        pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db", "#P2-CNT-DB", FieldType.NUMERIC, 
            8);
        pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db", "#P2-TRAD-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db", "#P2-SEP-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db", 
            "#P2-TRAD-IRA-ROLLOVER-DB", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db", "#P2-IRA-RECHAR-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db", "#P2-ACCOUNT-FMV-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db", "#P2-ROTH-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db", 
            "#P2-ROTH-IRA-CONVERSION-DB", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db", "#P2-POSTPN-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db = pnd_P2_Db_Read_Fields.newFieldInGroup("pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db", "#P2-REPAYMENTS-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_P2_Bypassed_Fields = localVariables.newGroupInRecord("pnd_P2_Bypassed_Fields", "#P2-BYPASSED-FIELDS");
        pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B", "#P2-CNT-B", FieldType.NUMERIC, 
            8);
        pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B", 
            "#P2-TRAD-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B", "#P2-SEP-AMT-B", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B", 
            "#P2-TRAD-IRA-ROLLOVER-B", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B", "#P2-IRA-RECHAR-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B", "#P2-ACCOUNT-FMV-B", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B", 
            "#P2-ROTH-IRA-CONTRIB-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B", 
            "#P2-ROTH-IRA-CONVERSION-B", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B", "#P2-POSTPN-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B = pnd_P2_Bypassed_Fields.newFieldInGroup("pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B", "#P2-REPAYMENTS-AMT-B", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_P2_Accepted_Fields = localVariables.newGroupInRecord("pnd_P2_Accepted_Fields", "#P2-ACCEPTED-FIELDS");
        pnd_P2_Accepted_Fields_Pnd_P2_Cnt_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Cnt_A", "#P2-CNT-A", FieldType.NUMERIC, 
            8);
        pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Contrib_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Contrib_A", 
            "#P2-TRAD-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Sep_Amt_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Sep_Amt_A", "#P2-SEP-AMT-A", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Rollover_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Rollover_A", 
            "#P2-TRAD-IRA-ROLLOVER-A", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Ira_Rechar_Amt_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Ira_Rechar_Amt_A", "#P2-IRA-RECHAR-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Account_Fmv_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Account_Fmv_A", "#P2-ACCOUNT-FMV-A", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Contrib_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Contrib_A", 
            "#P2-ROTH-IRA-CONTRIB-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Conversion_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Conversion_A", 
            "#P2-ROTH-IRA-CONVERSION-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Postpn_Amt_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Postpn_Amt_A", "#P2-POSTPN-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2_Accepted_Fields_Pnd_P2_Repayments_Amt_A = pnd_P2_Accepted_Fields.newFieldInGroup("pnd_P2_Accepted_Fields_Pnd_P2_Repayments_Amt_A", "#P2-REPAYMENTS-AMT-A", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Unq_Tin = localVariables.newFieldInRecord("pnd_Total_Unq_Tin", "#TOTAL-UNQ-TIN", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_B = localVariables.newFieldInRecord("pnd_Unq_Tin_B", "#UNQ-TIN-B", FieldType.NUMERIC, 8);
        pnd_Unq_Tin_A = localVariables.newFieldInRecord("pnd_Unq_Tin_A", "#UNQ-TIN-A", FieldType.NUMERIC, 8);
        pnd_P1_Total_Unq_Tin = localVariables.newFieldInRecord("pnd_P1_Total_Unq_Tin", "#P1-TOTAL-UNQ-TIN", FieldType.NUMERIC, 8);
        pnd_P1_Unq_Tin_B = localVariables.newFieldInRecord("pnd_P1_Unq_Tin_B", "#P1-UNQ-TIN-B", FieldType.NUMERIC, 8);
        pnd_P1_Unq_Tin_A = localVariables.newFieldInRecord("pnd_P1_Unq_Tin_A", "#P1-UNQ-TIN-A", FieldType.NUMERIC, 8);
        pnd_P2_Total_Unq_Tin = localVariables.newFieldInRecord("pnd_P2_Total_Unq_Tin", "#P2-TOTAL-UNQ-TIN", FieldType.NUMERIC, 8);
        pnd_P2_Unq_Tin_B = localVariables.newFieldInRecord("pnd_P2_Unq_Tin_B", "#P2-UNQ-TIN-B", FieldType.NUMERIC, 8);
        pnd_P2_Unq_Tin_A = localVariables.newFieldInRecord("pnd_P2_Unq_Tin_A", "#P2-UNQ-TIN-A", FieldType.NUMERIC, 8);
        pnd_P2_Indicator = localVariables.newFieldInRecord("pnd_P2_Indicator", "#P2-INDICATOR", FieldType.STRING, 1);
        pnd_Hold_Rpt_Tin = localVariables.newFieldInRecord("pnd_Hold_Rpt_Tin", "#HOLD-RPT-TIN", FieldType.STRING, 10);
        pnd_C_1099_R = localVariables.newFieldInRecord("pnd_C_1099_R", "#C-1099-R", FieldType.PACKED_DECIMAL, 3);
        pnd_Wkfle_Recd_Cnt = localVariables.newFieldInRecord("pnd_Wkfle_Recd_Cnt", "#WKFLE-RECD-CNT", FieldType.NUMERIC, 20);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws_Tax_Year__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Tax_Year__R_Field_6", "REDEFINE", pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A = pnd_Ws_Tax_Year__R_Field_6.newFieldInGroup("pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A", "#WS-TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Ws_Form_Type = localVariables.newFieldInRecord("pnd_Ws_Form_Type", "#WS-FORM-TYPE", FieldType.STRING, 20);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_In_Form_Type = localVariables.newFieldInRecord("pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Bypass_Reason = localVariables.newFieldInRecord("pnd_Bypass_Reason", "#BYPASS-REASON", FieldType.STRING, 50);
        pnd_Tin_Hold = localVariables.newFieldInRecord("pnd_Tin_Hold", "#TIN-HOLD", FieldType.STRING, 10);
        pnd_Test_Ind_Cnt = localVariables.newFieldInRecord("pnd_Test_Ind_Cnt", "#TEST-IND-CNT", FieldType.NUMERIC, 10);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 10);
        pnd_Ws_Progname = localVariables.newFieldInRecord("pnd_Ws_Progname", "#WS-PROGNAME", FieldType.STRING, 11);
        pnd_Coupon_Cnt = localVariables.newFieldInRecord("pnd_Coupon_Cnt", "#COUPON-CNT", FieldType.NUMERIC, 10);
        pnd_Total_Gross_Amt = localVariables.newFieldInRecord("pnd_Total_Gross_Amt", "#TOTAL-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Int = localVariables.newFieldInRecord("pnd_Total_Int", "#TOTAL-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Refund = localVariables.newFieldInRecord("pnd_Total_Refund", "#TOTAL-REFUND", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Tax_Non_Fatca = localVariables.newFieldInRecord("pnd_Total_Tax_Non_Fatca", "#TOTAL-TAX-NON-FATCA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Tax_Fatca = localVariables.newFieldInRecord("pnd_Total_Tax_Fatca", "#TOTAL-TAX-FATCA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Message1 = localVariables.newFieldInRecord("pnd_Message1", "#MESSAGE1", FieldType.STRING, 17);
        pnd_Type = localVariables.newFieldInRecord("pnd_Type", "#TYPE", FieldType.STRING, 6);
        pnd_Total_Type = localVariables.newFieldInRecord("pnd_Total_Type", "#TOTAL-TYPE", FieldType.PACKED_DECIMAL, 9);
        pnd_Non_Fatca_Tax = localVariables.newFieldInRecord("pnd_Non_Fatca_Tax", "#NON-FATCA-TAX", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Fatca_Tax = localVariables.newFieldInRecord("pnd_Fatca_Tax", "#FATCA-TAX", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input", "#INPUT");
        pnd_Ws_Pnd_Form_Type = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Stored_Tin = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Stored_Tin", "#STORED-TIN", FieldType.STRING, 10);
        pnd_Ws_Pnd_Restart_Text = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Restart_Text", "#RESTART-TEXT", FieldType.STRING, 1);
        pnd_Ws_Pnd_Mobius_Status = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mobius_Status", "#MOBIUS-STATUS", FieldType.STRING, 2);
        pnd_Ws_Pnd_Customer_Id = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Customer_Id", "#CUSTOMER-ID", FieldType.STRING, 15);

        pnd_Ws__R_Field_7 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_7", "REDEFINE", pnd_Ws_Pnd_Customer_Id);
        pnd_Ws_Pnd_Pin = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Ws_Pnd_Hold_Customer_Id = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Customer_Id", "#HOLD-CUSTOMER-ID", FieldType.STRING, 15);
        pnd_Ws_Pnd_Link_Let_Type = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Link_Let_Type", "#LINK-LET-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            7));
        pnd_Ws_Pnd_Form_Page_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Page_Cnt", "#FORM-PAGE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Ws_Pnd_Zero_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Zero_Isn", "#ZERO-ISN", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Sys_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Sys_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Ws_Pnd_Message = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Message", "#MESSAGE", FieldType.STRING, 30);

        pnd_Ws_Counters = pnd_Ws.newGroupInGroup("pnd_Ws_Counters", "COUNTERS");
        pnd_Ws_Pnd_Read_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Form01_Suny_Skip_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Form01_Suny_Skip_Cnt", "#FORM01-SUNY-SKIP-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Part_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Part_Cnt", "#PART-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Empty_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cpm_Lookup_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Cpm_Lookup_Cnt", "#CPM-LOOKUP-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Tax_Email_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Tax_Email_Cnt", "#TAX-EMAIL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_No_Tax_Email_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_No_Tax_Email_Cnt", "#NO-TAX-EMAIL-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Email_Non_Email_Bypassed = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Email_Non_Email_Bypassed", "#EMAIL-NON-EMAIL-BYPASSED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Pin_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt", "#MISSING-PIN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Pin_Cnt_Good = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Pin_Cnt_Good", "#MISSING-PIN-CNT-GOOD", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Missing_Ssn_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Missing_Ssn_Cnt", "#MISSING-SSN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Paper_Print_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Paper_Print_Cnt", "#PAPER-PRINT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Bypass_Cnt", "#BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_5498_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_5498_Bypass_Cnt", "#5498-BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Letr_Bypass_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Letr_Bypass_Cnt", "#LETR-BYPASS-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Form_Pages_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Form_Pages_Cnt", "#FORM-PAGES-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Multi_Page_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Multi_Page_Form_Cnt", "#MULTI-PAGE-FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Total_Form_Cnt = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Total_Form_Cnt", "#TOTAL-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Total_Form_Cnt_Xics = pnd_Ws_Counters.newFieldInGroup("pnd_Ws_Pnd_Total_Form_Cnt_Xics", "#TOTAL-FORM-CNT-XICS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tirf_Paper_Print_Hold_Ind", "#TIRF-PAPER-PRINT-HOLD-IND", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Tirf_Moore_Hold_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tirf_Moore_Hold_Ind", "#TIRF-MOORE-HOLD-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_Pin_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pin_A", "#PIN-A", FieldType.STRING, 12);
        pnd_Ws_Pnd_Debug = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Superde_12 = localVariables.newFieldInRecord("pnd_Superde_12", "#SUPERDE-12", FieldType.STRING, 18);

        pnd_Superde_12__R_Field_8 = localVariables.newGroupInRecord("pnd_Superde_12__R_Field_8", "REDEFINE", pnd_Superde_12);
        pnd_Superde_12_Pnd_Sd12_Tax_Year = pnd_Superde_12__R_Field_8.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Tax_Year", "#SD12-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Superde_12_Pnd_Sd12_Form_Type = pnd_Superde_12__R_Field_8.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Form_Type", "#SD12-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Superde_12_Pnd_Sd12_Active_Ind = pnd_Superde_12__R_Field_8.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Active_Ind", "#SD12-ACTIVE-IND", FieldType.STRING, 
            1);
        pnd_Superde_12_Pnd_Sd12_Mobius_Ind = pnd_Superde_12__R_Field_8.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Mobius_Ind", "#SD12-MOBIUS-IND", FieldType.STRING, 
            1);
        pnd_Superde_12_Pnd_Sd12_Tin = pnd_Superde_12__R_Field_8.newFieldInGroup("pnd_Superde_12_Pnd_Sd12_Tin", "#SD12-TIN", FieldType.STRING, 10);
        pnd_Superde_12_End = localVariables.newFieldInRecord("pnd_Superde_12_End", "#SUPERDE-12-END", FieldType.STRING, 18);
        pnd_Superde_01 = localVariables.newFieldInRecord("pnd_Superde_01", "#SUPERDE-01", FieldType.STRING, 17);

        pnd_Superde_01__R_Field_9 = localVariables.newGroupInRecord("pnd_Superde_01__R_Field_9", "REDEFINE", pnd_Superde_01);
        pnd_Superde_01_Pnd_Sd1_Tax_Year = pnd_Superde_01__R_Field_9.newFieldInGroup("pnd_Superde_01_Pnd_Sd1_Tax_Year", "#SD1-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Superde_01_Pnd_Sd1_Tirf_Tin = pnd_Superde_01__R_Field_9.newFieldInGroup("pnd_Superde_01_Pnd_Sd1_Tirf_Tin", "#SD1-TIRF-TIN", FieldType.STRING, 
            10);
        pnd_Superde_01_Pnd_Sd1_Form_Type = pnd_Superde_01__R_Field_9.newFieldInGroup("pnd_Superde_01_Pnd_Sd1_Form_Type", "#SD1-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Form_Type_1_10_Found = localVariables.newFieldInRecord("pnd_Form_Type_1_10_Found", "#FORM-TYPE-1-10-FOUND", FieldType.BOOLEAN, 1);
        pnd_Contract_Txt = localVariables.newFieldInRecord("pnd_Contract_Txt", "#CONTRACT-TXT", FieldType.STRING, 9);
        pnd_Contract_Hdr_Lit = localVariables.newFieldInRecord("pnd_Contract_Hdr_Lit", "#CONTRACT-HDR-LIT", FieldType.STRING, 9);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_I10 = localVariables.newFieldInRecord("pnd_I10", "#I10", FieldType.PACKED_DECIMAL, 3);
        pnd_Y_Cnt = localVariables.newFieldInRecord("pnd_Y_Cnt", "#Y-CNT", FieldType.NUMERIC, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 2);
        pnd_Test_Ind = localVariables.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 1);
        pnd_Coupon_Ind = localVariables.newFieldInRecord("pnd_Coupon_Ind", "#COUPON-IND", FieldType.STRING, 1);

        pnd_Ltr_Array = localVariables.newGroupArrayInRecord("pnd_Ltr_Array", "#LTR-ARRAY", new DbsArrayController(1, 999));
        pnd_Ltr_Array_Pnd_Ltr_Tin = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Tin", "#LTR-TIN", FieldType.STRING, 10);
        pnd_Ltr_Array_Pnd_Ltr_Cntrct = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Cntrct", "#LTR-CNTRCT", FieldType.STRING, 9);
        pnd_Ltr_Array_Pnd_Ltr_Data_Innd = pnd_Ltr_Array.newFieldInGroup("pnd_Ltr_Array_Pnd_Ltr_Data_Innd", "#LTR-DATA-INND", FieldType.STRING, 1);
        pnd_Additional_Forms = localVariables.newFieldInRecord("pnd_Additional_Forms", "#ADDITIONAL-FORMS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Save_Date_N = localVariables.newFieldInRecord("pnd_Ws_Save_Date_N", "#WS-SAVE-DATE-N", FieldType.NUMERIC, 8);
        pls_Email_Address = WsIndependent.getInstance().newFieldInRecord("pls_Email_Address", "+EMAIL-ADDRESS", FieldType.STRING, 70);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_form_10.reset();
        vw_form_Upd.reset();

        ldaTwrl0600.initializeValues();
        ldaTwrl2001.initializeValues();
        ldaTwrl9610.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        et_Limit.setInitialValue(100);
        pnd_Contract_Hdr_Lit1.setInitialValue("Ltr Hdr");
        pnd_Dist_Exempt_Amt.setInitialValue(10000);
        pnd_P2_Indicator.setInitialValue("N");
        pnd_Ws_Form_Type.setInitialValue(" ");
        pnd_I.setInitialValue(0);
        pnd_Read_Ctr.setInitialValue(0);
        pnd_Ws_Pnd_Sys_Date.setInitialValue(Global.getDATX());
        pnd_Ws_Pnd_Sys_Time.setInitialValue(Global.getTIMX());
        pnd_Ws_Pnd_Form01_Suny_Skip_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Part_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Empty_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Cpm_Lookup_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Tax_Email_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_No_Tax_Email_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Email_Non_Email_Bypassed.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Pin_Cnt_Good.setInitialValue(0);
        pnd_Ws_Pnd_Missing_Ssn_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Paper_Print_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_5498_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Letr_Bypass_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Form_Pages_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Multi_Page_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Total_Form_Cnt.setInitialValue(0);
        pnd_Ws_Pnd_Total_Form_Cnt_Xics.setInitialValue(0);
        pnd_Contract_Hdr_Lit.setInitialValue("Ltr Hdr");
        pnd_Additional_Forms.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp6890() throws Exception
    {
        super("Twrp6890");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*   /* SNEHA CHANGES STARTED - SINHSN                                                                                                                           //Natural: FORMAT ( 0 ) LS = 80 PS = 60 ZP = ON
        //*  SINHSN
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 5 ) LS = 133 PS = 100 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 2 ) LS = 132 PS = 100 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 3 ) LS = 132 PS = 60 ZP = ON
        //* *FORMAT (1) LS=132 PS=80 ZP=ON     /* SINHSN                                                                                                                  //Natural: FORMAT ( 4 ) LS = 133 PS = 58 ZP = ON
        //*  480.7C DISASTER
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 170 PS = 80 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 6 ) LS = 132 PS = 80 ZP = ON
        //*  SINHSN                                                                                                                                                       //Natural: FORMAT ( 7 ) LS = 132 PS = 60 ZP = ON
        //*                                                                                                                                                               //Natural: FORMAT ( 8 ) LS = 132 PS = 60 ZP = ON
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        //*  GET THE FORM NAMES BASED ON THE YEAR
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //* *********************** BYPASSED REPORTS **************************
        if (condition(pnd_Ws_Pnd_Form_Type.equals(1) || pnd_Ws_Pnd_Form_Type.equals(2) || pnd_Ws_Pnd_Form_Type.equals(10) || pnd_Ws_Pnd_Form_Type.equals(4)))             //Natural: IF #WS.#FORM-TYPE = 1 OR = 2 OR = 10 OR = 4
        {
            getReports().write(2, ReportOption.NOTITLE,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, new ReportEditMask                 //Natural: WRITE ( 2 ) #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 24T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 25T 'Mass Mailing Bypassed form Details' #WS.#TAX-YEAR /68T 'Report: RPT2' / 'Form Type:' #FORM-NAME ( #FORM-TYPE ) // / 05T 'TIN NO.' 20T 'CONTRACT NO.' 35T 'PART-NAME' 75T 'BYPASS-REASON' / '-' ( 131 ) //
                ("HH:IIAP"),new TabSetting(24),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask 
                ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(25),"Mass Mailing Bypassed form Details",pnd_Ws_Pnd_Tax_Year,NEWLINE,new 
                TabSetting(68),"Report: RPT2",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_Form_Type.getInt() + 1),NEWLINE,NEWLINE,NEWLINE,new 
                TabSetting(5),"TIN NO.",new TabSetting(20),"CONTRACT NO.",new TabSetting(35),"PART-NAME",new TabSetting(75),"BYPASS-REASON",NEWLINE,"-",new 
                RepeatItem(131),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(3)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 3
        {
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(40),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 40T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Mobius Mass Migration' 120T 'Report: RPT2' / 45T ' Bypassed form Details' #WS.#TAX-YEAR / 50T 'Form Type: 1042S' #TYPE // 'Tax' 37T 'Pay' 80T 'Interest Amount' 96T 'Non-FATCA Tax' 110T 'Mobius' / 'Year' 6T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'ee' 41T 'Pin' 60T 'Gross Amount' 80T 'Refund Amount' 96T 'FATCA Tax' 110T 'Status' 118T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(45),"Mobius Mass Migration",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(45)," Bypassed form Details",pnd_Ws_Pnd_Tax_Year,NEWLINE,new 
                TabSetting(50),"Form Type: 1042S",pnd_Type,NEWLINE,NEWLINE,"Tax",new TabSetting(37),"Pay",new TabSetting(80),"Interest Amount",new TabSetting(96),"Non-FATCA Tax",new 
                TabSetting(110),"Mobius",NEWLINE,"Year",new TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new TabSetting(27),"Contract",new 
                TabSetting(37),"ee",new TabSetting(41),"Pin",new TabSetting(60),"Gross Amount",new TabSetting(80),"Refund Amount",new TabSetting(96),"FATCA Tax",new 
                TabSetting(110),"Status",new TabSetting(118),"Message",NEWLINE,"-",new RepeatItem(131));
            if (Global.isEscape()) return;
            //*    112T 'Bypass Reason'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 5
        {
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(40),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 40T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Mobius Mass Migration' 120T 'Report: RPT2' / 45T ' Bypassed form Details' #WS.#TAX-YEAR / 50T 'Form Type: 480.7C' #TYPE // 'Tax' 37T 'Pay' 60T 'Gross Amount' 80T 'Interest Amount' 96T 'Rllvr Dstrbtn' 110T 'Mobius' / 'Year' 6T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'ee' 41T 'Pin' 60T 'Taxable Amount' 80T 'Federal Tax' 96T 'Rllvr Cntrbtn' 110T 'Status' 120T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(45),"Mobius Mass Migration",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(45)," Bypassed form Details",pnd_Ws_Pnd_Tax_Year,NEWLINE,new 
                TabSetting(50),"Form Type: 480.7C",pnd_Type,NEWLINE,NEWLINE,"Tax",new TabSetting(37),"Pay",new TabSetting(60),"Gross Amount",new TabSetting(80),"Interest Amount",new 
                TabSetting(96),"Rllvr Dstrbtn",new TabSetting(110),"Mobius",NEWLINE,"Year",new TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new 
                TabSetting(27),"Contract",new TabSetting(37),"ee",new TabSetting(41),"Pin",new TabSetting(60),"Taxable Amount",new TabSetting(80),"Federal Tax",new 
                TabSetting(96),"Rllvr Cntrbtn",new TabSetting(110),"Status",new TabSetting(120),"Message",NEWLINE,"-",new RepeatItem(131));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(7)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 7
        {
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(40),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 40T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Mobius Mass Migration' 120T 'Report: RPT2' / 45T ' Bypassed form Details' #WS.#TAX-YEAR / 50T 'Form Type: NR4' #TYPE // / 'Year' 06T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'Payee' 43T 'Pin' 60T 'Gross Amt' 80T 'Int Amt' 92T 'Canadian Tax' 106T 'Status' 114T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(45),"Mobius Mass Migration",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(45)," Bypassed form Details",pnd_Ws_Pnd_Tax_Year,NEWLINE,new 
                TabSetting(50),"Form Type: NR4",pnd_Type,NEWLINE,NEWLINE,NEWLINE,"Year",new TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new 
                TabSetting(27),"Contract",new TabSetting(37),"Payee",new TabSetting(43),"Pin",new TabSetting(60),"Gross Amt",new TabSetting(80),"Int Amt",new 
                TabSetting(92),"Canadian Tax",new TabSetting(106),"Status",new TabSetting(114),"Message",NEWLINE,"-",new RepeatItem(131));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** BYPASSED REPORTS ENDS**********************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 24T 'Tax Withholding and Reporting System' 68T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 29T 'Test Indicator Report' #WS.#TAX-YEAR /68T 'Report: RPT3' / 32T 'Participant Mass Mailing' 'Form Type:' #FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 4 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Mobius Mass Migration' 120T 'Report: RPT4' / 'Form Type:' #FORM-NAME ( #FORM-TYPE ) // 57T 'Form file Update' #WS.#TAX-YEAR //
        if (condition(pnd_Ws_Pnd_Form_Type.equals(4)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE EQ 04
        {
            getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 6 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 68T 'Report: RPT6' / 50T '5498 Coupon Counts Report' #WS.#TAX-YEAR /49T 'PARTICIPANT MASS MAILING' // /// 12T 'Tin Number' 24T 'Participant Name' / 12T '-' ( 10 ) 24T '-' ( 16 ) //
                TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(68),"Report: RPT6",NEWLINE,new TabSetting(50),"5498 Coupon Counts Report",pnd_Ws_Pnd_Tax_Year,NEWLINE,new TabSetting(49),"PARTICIPANT MASS MAILING",NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
                TabSetting(12),"Tin Number",new TabSetting(24),"Participant Name",NEWLINE,new TabSetting(12),"-",new RepeatItem(10),new TabSetting(24),"-",new 
                RepeatItem(16),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(7, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 7 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 68T 'Report: RPT7' / 40T '5498 Period 1 as a result of period 2' #WS.#TAX-YEAR /49T 'PARTICIPANT MASS MAILING' //
                TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(68),"Report: RPT7",NEWLINE,new TabSetting(40),"5498 Period 1 as a result of period 2",pnd_Ws_Pnd_Tax_Year,NEWLINE,new TabSetting(49),
                "PARTICIPANT MASS MAILING",NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* **
            getReports().write(8, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 8 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 68T 'Report: RPT8' / 50T '5498 True Period 2 Report' #WS.#TAX-YEAR /52T 'PARTICIPANT MASS MAILING' //
                TabSetting(120),"PAGE:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(68),"Report: RPT8",NEWLINE,new TabSetting(50),"5498 True Period 2 Report",pnd_Ws_Pnd_Tax_Year,NEWLINE,new TabSetting(52),"PARTICIPANT MASS MAILING",
                NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //* *********************** PROCESSED FORMS **************************
        //*  IF #WS.#FORM-TYPE = 2
        //*   WRITE (5) NOTITLE NOHDR
        //*     #SYS-DATE (EM=MM/DD/YYYY) '-' #SYS-TIME (EM=HH:IIAP)
        //*     24T  'Tax Withholding and Reporting System'
        //*     68T  'Page:' *PAGE-NUMBER(05)(EM=ZZ,ZZ9) /
        //*     *INIT-USER '-' *PROGRAM
        //*     24T  'Mass Mailing Form Extract Details' #WS.#TAX-YEAR
        //*     68T  'Report: RPT5' //
        //*     'Form Type:'   #FORM-NAME (#FORM-TYPE) //
        //*  END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(3)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 3
        {
            getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding & Payment System",new  //Natural: WRITE ( 5 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding & Payment System' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Mobius Mass Migration' 120T 'Report: RPT5' / 55T 'Processed Forms' 'Form Type: 1042S' #TYPE // 'Tax' 37T 'Pay' 80T 'Interest Amount' 96T 'Non-FATCA Tax' 110T 'Mobius' / 'Year' 6T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'ee' 41T 'Pin' 60T 'Gross Amount' 80T 'Refund Amount' 96T 'FATCA Tax' 110T 'Status' 120T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(55),"Mobius Mass Migration",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(55),"Processed Forms","Form Type: 1042S",pnd_Type,NEWLINE,NEWLINE,"Tax",new 
                TabSetting(37),"Pay",new TabSetting(80),"Interest Amount",new TabSetting(96),"Non-FATCA Tax",new TabSetting(110),"Mobius",NEWLINE,"Year",new 
                TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new TabSetting(27),"Contract",new TabSetting(37),"ee",new TabSetting(41),"Pin",new 
                TabSetting(60),"Gross Amount",new TabSetting(80),"Refund Amount",new TabSetting(96),"FATCA Tax",new TabSetting(110),"Status",new TabSetting(120),"Message",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 5
        {
            getReports().write(5, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 5 ) *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Mobius Mass Migration' 120T 'Report: RPT5' / 51T 'Processed Forms' 'Form Type: 480.7C' #TYPE // 'Tax' 37T 'Pay' 60T 'Gross Amount' 80T 'Interest Amount' 96T 'Rllvr Dstrbtn' 110T 'Mobius' / 'Year' 6T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'ee' 41T 'Pin' 60T 'Taxable Amount' 80T 'Federal Tax' 96T 'Rllvr Cntrbtn' 110T 'Status' 120T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(55),"Mobius Mass Migration",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(51),"Processed Forms","Form Type: 480.7C",pnd_Type,NEWLINE,NEWLINE,"Tax",new 
                TabSetting(37),"Pay",new TabSetting(60),"Gross Amount",new TabSetting(80),"Interest Amount",new TabSetting(96),"Rllvr Dstrbtn",new TabSetting(110),"Mobius",NEWLINE,"Year",new 
                TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new TabSetting(27),"Contract",new TabSetting(37),"ee",new TabSetting(41),"Pin",new 
                TabSetting(60),"Taxable Amount",new TabSetting(80),"Federal Tax",new TabSetting(96),"Rllvr Cntrbtn",new TabSetting(110),"Status",new TabSetting(120),"Message",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(7)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 7
        {
            getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new  //Natural: WRITE ( 5 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Mobius Mass Migration' 120T 'Report: RPT5' / 51T 'Processed Forms' 'Form Type: NR4' #TYPE // / 'Year' 06T 'Form' 11T 'Comp' 16T 'Tin' 27T 'Contract' 37T 'Payee' 43T 'Pin' 60T 'Gross Amt' 80T 'Int Amt' 92T 'Canadian Tax' 106T 'Status' 114T 'Message' / '-' ( 131 )
                TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                TabSetting(55),"Mobius Mass Migration",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(51),"Processed Forms","Form Type: NR4",pnd_Type,NEWLINE,NEWLINE,NEWLINE,"Year",new 
                TabSetting(6),"Form",new TabSetting(11),"Comp",new TabSetting(16),"Tin",new TabSetting(27),"Contract",new TabSetting(37),"Payee",new TabSetting(43),"Pin",new 
                TabSetting(60),"Gross Amt",new TabSetting(80),"Int Amt",new TabSetting(92),"Canadian Tax",new TabSetting(106),"Status",new TabSetting(114),"Message",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** PROCESSED FORMS ENDS *********************
        //*  /* SNEHA CHANGES ENDED - SINHSN
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  PERFORM PROCESS-INPUT-PARMS         /* SINHSN
        //*  01/30/2008 AY
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        pnd_Superde_12_Pnd_Sd12_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                   //Natural: ASSIGN #SUPERDE-12.#SD12-TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
        pnd_Superde_12_Pnd_Sd12_Form_Type.setValue(pnd_Ws_Pnd_Form_Type);                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-FORM-TYPE := #WS.#FORM-TYPE
        pnd_Superde_12_Pnd_Sd12_Active_Ind.setValue("A");                                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-ACTIVE-IND := 'A'
        pnd_Superde_12_Pnd_Sd12_Mobius_Ind.setValue("Y");                                                                                                                 //Natural: ASSIGN #SUPERDE-12.#SD12-MOBIUS-IND := 'Y'
        pnd_Superde_12_Pnd_Sd12_Tin.setValue(pnd_Ws_Const_Low_Values);                                                                                                    //Natural: MOVE LOW-VALUES TO #SD12-TIN
        pnd_Superde_12_End.setValue(pnd_Superde_12);                                                                                                                      //Natural: ASSIGN #SUPERDE-12-END := #SUPERDE-12
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Superde_12_End,9,10);                                                                                            //Natural: MOVE HIGH-VALUES TO SUBSTR ( #SUPERDE-12-END,9,10 )
        //*                                      /* MUKHERR 010515  >>
        //* CTS1
        ldaTwrl2001.getPnd_Rpt_Twrl2001().reset();                                                                                                                        //Natural: RESET #RPT-TWRL2001
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-TYPE
        sub_Process_Form_Type();
        if (condition(Global.isEscape())) {return;}
        //* *
        //* *  CHECK RECORD LIMIT
        //* *
        if (condition(pnd_Ws_Pnd_Form_Cnt.equals(getZero())))                                                                                                             //Natural: IF #FORM-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Empty file. Terminating program",new TabSetting(77),"***",NEWLINE,"***",new              //Natural: WRITE ( 0 ) '***' 25T 'Empty file. Terminating program' 77T '***' / '***' 25T 'No forms available for MMM process' 77T '***'
                TabSetting(25),"No forms available for MMM process",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************
        //*  S U B R O U T I N E S *
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FORM-TYPE
        //* *
        //* *
        //* * REPORT CASES WITH ADDITIONAL PAGES
        //* *
        //* *  CALL TAX PRINT MODULE
        //* *
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FORM-RECORD
        //* *
        //* * INTRODUCED NEW WORK FILE FOR GENERATING RECON REPORT INPUT FILE
        //* *
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-FILE
        //* **********************************************************************
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-FIELDS
        //* **************************************************************
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-REPORT-FIELD-MOVE
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-5498-REPORT-FIELDS
        //* *
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCESS-IRA-FILE
        //* ********************************
        //*  NEW REPORT FILE FIELD MOVEMENT ENDS ********
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PAPER-PRINT-HOLD-REPORT
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-1042S-REPORT-FIELDS
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-4807C-REPORT-FIELDS
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-NR4-REPORT-FIELDS
        //* ************************************          /* MUKHERR 010515  <<
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FORM-TYPE-10
        //* ************************************
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-RECORD
        //*   /* SNEHA CHANGES STARTED - SINHSN
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FORM-UPDATE
        //* ************************************
        getReports().write(4, NEWLINE,NEWLINE,NEWLINE,"Records Read...:",pnd_Ws1_Pnd_Read_Cnt1, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"Records Updated:",pnd_Ws1_Pnd_Update_Cnt,  //Natural: WRITE ( 4 ) // / 'Records Read...:' #READ-CNT1 / 'Records Updated:' #UPDATE-CNT
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                                            //Natural: WRITE ( 4 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-CONTROL-REPORT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRUST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS-TIAA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASSED-PROCESS-TIAA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCEPTED-PROCESS-TIAA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS-TRUST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BYPASSED-PROCESS-TRUST
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCEPTED-PROCESS-TRUST
        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM TITLE-REPORT1
        sub_Title_Report1();
        if (condition(Global.isEscape())) {return;}
        //*  1099R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1) || ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                    //Natural: IF #RPT-FORM-TYPE EQ 1 OR EQ 2
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TIAA",NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new  //Natural: WRITE ( 1 ) NOTITLE // 51X '     COMPANY :  TIAA' /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' IVC AMOUNT' 94T '    FEDERAL TAX' 117T '    LOCAL TAX ' / 77T ' INT AMOUNT' 94T '    STATE TAX' 117T '    IRR AMT   ' / '-' ( 131 ) //
                TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," IVC AMOUNT",new TabSetting(94),"    FEDERAL TAX",new 
                TabSetting(117),"    LOCAL TAX ",NEWLINE,new TabSetting(77)," INT AMOUNT",new TabSetting(94),"    STATE TAX",new TabSetting(117),"    IRR AMT   ",NEWLINE,"-",new 
                RepeatItem(131),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            getReports().write(1, ReportOption.NOTITLE,"1099-R",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R, new                   //Natural: WRITE ( 1 ) NOTITLE '1099-R' // 'DATABASE READ' 18T #TIAA-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-1099-R-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-1099-R-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-R-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-R-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-R-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-R-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-R-STATE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-R-IRR-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),
                NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            getReports().write(1, ReportOption.NOTITLE,"1099-INT",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I,                     //Natural: WRITE ( 1 ) NOTITLE '1099-INT' // 'DATABASE READ' 18T #TIAA-1099-I ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-I-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-I-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-I-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-I-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-1099-I-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-I-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-I-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-I-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-I-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-1099-I-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1099-I-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-1099-I-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1099-I-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-1099-I-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-1099-I-STATE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-1099-I-IRR-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),
                NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TIAA",NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new  //Natural: WRITE ( 1 ) NOTITLE // 51X '     COMPANY :  TIAA' /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' REFUND AMOUNT' 77T ' INT AMOUNT' 94T ' NON-FATCA TAX ' 117T '  FATCA TAX   ' / '-' ( 131 ) // '1042-S' // 'DATABASE READ' 18T #TIAA-1042S ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1042S-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-1042S-REFUND ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1042S-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-1042S-TAX-NON-FATCA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-1042S-TAX-FATCA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-1042S-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1042S-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-1042S-REFUND-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1042S-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-1042S-TAX-NON-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-1042S-TAX-FATCA-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-1042S-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-1042S-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-1042S-REFUND-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-1042S-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-1042S-TAX-NON-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-1042S-TAX-FATCA-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) // //
                TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," REFUND AMOUNT",new TabSetting(77)," INT AMOUNT",new TabSetting(94)," NON-FATCA TAX ",new 
                TabSetting(117),"  FATCA TAX   ",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,"1042-S",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(110),pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new 
                TabSetting(18),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(49),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        //*  SAIK1
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(18),"REC COUNT",new TabSetting(29),"IRA CONTRIBUTION",new TabSetting(53),"  SEP AMOUNT",new         //Natural: WRITE ( 1 ) NOTITLE 18T 'REC COUNT' 29T 'IRA CONTRIBUTION' 53T '  SEP AMOUNT' 70T '      IRA ROLLOVER ' 91T 'RECHARACTERIZATION' 112T '        F M V' / 11T 'UNIQUE TIN COUNT' 32T '  TRAD/ROTH   ' 53T 'LATE R/O AMT ' 70T '    IRA CONVERSION  ' 91T '        REPAYMENTS' / '-' ( 131 )
                TabSetting(70),"      IRA ROLLOVER ",new TabSetting(91),"RECHARACTERIZATION",new TabSetting(112),"        F M V",NEWLINE,new TabSetting(11),"UNIQUE TIN COUNT",new 
                TabSetting(32),"  TRAD/ROTH   ",new TabSetting(53),"LATE R/O AMT ",new TabSetting(70),"    IRA CONVERSION  ",new TabSetting(91),"        REPAYMENTS",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"5498",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(19),pnd_Db_Read_Fields_Pnd_Cnt_Db, new ReportEditMask        //Natural: WRITE ( 1 ) NOTITLE '5498' // 'DATABASE READ' 19T #CNT-DB ( EM = Z,ZZZ,ZZ9 ) 29T #TRAD-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #SEP-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRAD-IRA-ROLLOVER-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #IRA-RECHAR-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #ACCOUNT-FMV-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #TOTAL-UNQ-TIN ( EM = Z,ZZZ,ZZ9 ) 29T #ROTH-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #POSTPN-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #ROTH-IRA-CONVERSION-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #REPAYMENTS-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 19T #CNT-B ( EM = Z,ZZZ,ZZ9 ) 29T #TRAD-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #SEP-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRAD-IRA-ROLLOVER-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #IRA-RECHAR-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #ACCOUNT-FMV-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #UNQ-TIN-B ( EM = Z,ZZZ,ZZ9 ) 29T #ROTH-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #POSTPN-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #ROTH-IRA-CONVERSION-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #REPAYMENTS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 19T #CNT-A ( EM = Z,ZZZ,ZZ9 ) 29T #TRAD-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #SEP-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRAD-IRA-ROLLOVER-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #IRA-RECHAR-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #ACCOUNT-FMV-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #UNQ-TIN-A ( EM = Z,ZZZ,ZZ9 ) 29T #ROTH-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #POSTPN-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #ROTH-IRA-CONVERSION-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #REPAYMENTS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Db_Read_Fields_Pnd_Sep_Amt_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Db_Read_Fields_Pnd_Account_Fmv_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_Total_Unq_Tin, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(19),bypassed_Fields_Pnd_Cnt_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                TabSetting(29),bypassed_Fields_Pnd_Trad_Ira_Contrib_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),bypassed_Fields_Pnd_Sep_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),bypassed_Fields_Pnd_Trad_Ira_Rollover_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),bypassed_Fields_Pnd_Ira_Rechar_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),bypassed_Fields_Pnd_Account_Fmv_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_Unq_Tin_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),bypassed_Fields_Pnd_Roth_Ira_Contrib_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),bypassed_Fields_Pnd_Postpn_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),bypassed_Fields_Pnd_Roth_Ira_Conversion_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),bypassed_Fields_Pnd_Repayments_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(19),accepted_Fields_Pnd_Cnt_A, new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                TabSetting(29),accepted_Fields_Pnd_Trad_Ira_Contrib_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),accepted_Fields_Pnd_Sep_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),accepted_Fields_Pnd_Trad_Ira_Rollover_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),accepted_Fields_Pnd_Ira_Rechar_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),accepted_Fields_Pnd_Account_Fmv_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_Unq_Tin_A, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),accepted_Fields_Pnd_Roth_Ira_Contrib_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),accepted_Fields_Pnd_Postpn_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),accepted_Fields_Pnd_Roth_Ira_Conversion_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),accepted_Fields_Pnd_Repayments_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* ****************************************
            //* ** PERIOD 2 RELATED REPORTS ************
            //* ****************************************
            getReports().write(7, ReportOption.NOTITLE,new TabSetting(18),"REC COUNT",new TabSetting(29),"IRA CONTRIBUTION",new TabSetting(53),"  SEP AMOUNT",new         //Natural: WRITE ( 7 ) NOTITLE 18T 'REC COUNT' 29T 'IRA CONTRIBUTION' 53T '  SEP AMOUNT' 70T '      IRA ROLLOVER ' 91T 'RECHARACTERIZATION' 112T '        F M V' / 11T 'UNIQUE TIN COUNT' 32T '  TRAD/ROTH   ' 53T 'LATE R/O AMT ' 70T '    IRA CONVERSION  ' 91T '        REPAYMENTS' / '-' ( 131 )
                TabSetting(70),"      IRA ROLLOVER ",new TabSetting(91),"RECHARACTERIZATION",new TabSetting(112),"        F M V",NEWLINE,new TabSetting(11),"UNIQUE TIN COUNT",new 
                TabSetting(32),"  TRAD/ROTH   ",new TabSetting(53),"LATE R/O AMT ",new TabSetting(70),"    IRA CONVERSION  ",new TabSetting(91),"        REPAYMENTS",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
            getReports().write(7, ReportOption.NOTITLE,"5498",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(19),pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db,                     //Natural: WRITE ( 7 ) NOTITLE '5498' // 'DATABASE READ' 19T #P1-CNT-DB ( EM = Z,ZZZ,ZZ9 ) 29T #P1-TRAD-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-SEP-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P1-TRAD-IRA-ROLLOVER-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-IRA-RECHAR-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P1-ACCOUNT-FMV-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P1-TOTAL-UNQ-TIN ( EM = Z,ZZZ,ZZ9 ) 29T #P1-ROTH-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-POSTPN-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P1-ROTH-IRA-CONVERSION-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-REPAYMENTS-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 19T #P1-CNT-B ( EM = Z,ZZZ,ZZ9 ) 29T #P1-TRAD-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-SEP-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P1-TRAD-IRA-ROLLOVER-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-IRA-RECHAR-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P1-ACCOUNT-FMV-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P1-UNQ-TIN-B ( EM = Z,ZZZ,ZZ9 ) 29T #P1-ROTH-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-POSTPN-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P1-ROTH-IRA-CONVERSION-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-REPAYMENTS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 19T #P1-CNT-A ( EM = Z,ZZZ,ZZ9 ) 29T #P1-TRAD-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-SEP-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P1-TRAD-IRA-ROLLOVER-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-IRA-RECHAR-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P1-ACCOUNT-FMV-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P1-UNQ-TIN-A ( EM = Z,ZZZ,ZZ9 ) 29T #P1-ROTH-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P1-POSTPN-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P1-ROTH-IRA-CONVERSION-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P1-REPAYMENTS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_P1_Total_Unq_Tin, 
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(73),pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(19),pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(70),pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                TabSetting(19),pnd_P1_Unq_Tin_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B, new 
                ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(19),pnd_P1_Accepted_Fields_Pnd_P1_Cnt_A, new ReportEditMask 
                ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Contrib_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P1_Accepted_Fields_Pnd_P1_Sep_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Rollover_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_P1_Accepted_Fields_Pnd_P1_Ira_Rechar_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_P1_Accepted_Fields_Pnd_P1_Account_Fmv_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_P1_Unq_Tin_A, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Contrib_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P1_Accepted_Fields_Pnd_P1_Postpn_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Conversion_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P1_Accepted_Fields_Pnd_P1_Repayments_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* ***************************
            //* ***TRUE P2 REPORT *********
            //* ***************************
            getReports().write(8, ReportOption.NOTITLE,new TabSetting(18),"REC COUNT",new TabSetting(29),"IRA CONTRIBUTION",new TabSetting(53),"  SEP AMOUNT",new         //Natural: WRITE ( 8 ) NOTITLE 18T 'REC COUNT' 29T 'IRA CONTRIBUTION' 53T '  SEP AMOUNT' 70T '      IRA ROLLOVER ' 91T 'RECHARACTERIZATION' 112T '        F M V' / 11T 'UNIQUE TIN COUNT' 32T '  TRAD/ROTH   ' 53T 'LATE R/O AMT ' 70T '    IRA CONVERSION  ' 91T '        REPAYMENTS' / '-' ( 131 )
                TabSetting(70),"      IRA ROLLOVER ",new TabSetting(91),"RECHARACTERIZATION",new TabSetting(112),"        F M V",NEWLINE,new TabSetting(11),"UNIQUE TIN COUNT",new 
                TabSetting(32),"  TRAD/ROTH   ",new TabSetting(53),"LATE R/O AMT ",new TabSetting(70),"    IRA CONVERSION  ",new TabSetting(91),"        REPAYMENTS",NEWLINE,"-",new 
                RepeatItem(131));
            if (Global.isEscape()) return;
            getReports().write(8, ReportOption.NOTITLE,"5498",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(19),pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db,                     //Natural: WRITE ( 8 ) NOTITLE '5498' // 'DATABASE READ' 19T #P2-CNT-DB ( EM = Z,ZZZ,ZZ9 ) 29T #P2-TRAD-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-SEP-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P2-TRAD-IRA-ROLLOVER-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-IRA-RECHAR-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P2-ACCOUNT-FMV-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P2-TOTAL-UNQ-TIN ( EM = Z,ZZZ,ZZ9 ) 29T #P2-ROTH-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-POSTPN-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P2-ROTH-IRA-CONVERSION-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-REPAYMENTS-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 19T #P2-CNT-B ( EM = Z,ZZZ,ZZ9 ) 29T #P2-TRAD-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-SEP-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P2-TRAD-IRA-ROLLOVER-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-IRA-RECHAR-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P2-ACCOUNT-FMV-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P2-UNQ-TIN-B ( EM = Z,ZZZ,ZZ9 ) 29T #P2-ROTH-IRA-CONTRIB-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-POSTPN-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P2-ROTH-IRA-CONVERSION-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-REPAYMENTS-AMT-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 19T #P2-CNT-A ( EM = Z,ZZZ,ZZ9 ) 29T #P2-TRAD-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-SEP-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #P2-TRAD-IRA-ROLLOVER-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-IRA-RECHAR-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #P2-ACCOUNT-FMV-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 19T #P2-UNQ-TIN-A ( EM = Z,ZZZ,ZZ9 ) 29T #P2-ROTH-IRA-CONTRIB-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #P2-POSTPN-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #P2-ROTH-IRA-CONVERSION-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #P2-REPAYMENTS-AMT-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_P2_Total_Unq_Tin, 
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(73),pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(19),pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(70),pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                TabSetting(19),pnd_P2_Unq_Tin_B, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B, new 
                ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(19),pnd_P2_Accepted_Fields_Pnd_P2_Cnt_A, new ReportEditMask 
                ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Contrib_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P2_Accepted_Fields_Pnd_P2_Sep_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Rollover_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_P2_Accepted_Fields_Pnd_P2_Ira_Rechar_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_P2_Accepted_Fields_Pnd_P2_Account_Fmv_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(19),pnd_P2_Unq_Tin_A, new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(29),pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Contrib_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_P2_Accepted_Fields_Pnd_P2_Postpn_Amt_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(73),pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Conversion_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_P2_Accepted_Fields_Pnd_P2_Repayments_Amt_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER
        //*  480.7C
        //*  480.7C DISASTER CHANGES
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TIAA",NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new  //Natural: WRITE ( 1 ) NOTITLE // 51X '     COMPANY :  TIAA' /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' INT AMOUNT' 94T 'RLLVR DISTRBTN ' 115T 'TAXABLE AMT-DISASTER' 140T 'AFTR TAX DISTRBTN' / 77T ' FEDERAL TAX' 94T 'RLLVR CONTRBN' 115T 'EXEMPT AMT-DISASTER' / '-' ( 169 ) // '480.7C' // 'DATABASE READ' 18T #TIAA-4807C ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-4807C-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-4807C-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-4807C-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-DIST ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-DISASTER-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-4807C-AFTRTAX-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-4807C-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-EXEMPT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-4807C-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-4807C-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-4807C-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-4807C-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-DIST-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-DISASTER-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-4807C-AFTRTAX-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-4807C-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-EXEMPT-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-4807C-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-4807C-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-4807C-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-4807C-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-DIST-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-DISASTER-TAX-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-4807C-AFTRTAX-CONT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-4807C-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-4807C-ROLL-CONT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-4807C-EXEMPT-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 169 ) / //
                TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," INT AMOUNT",new TabSetting(94),"RLLVR DISTRBTN ",new 
                TabSetting(115),"TAXABLE AMT-DISASTER",new TabSetting(140),"AFTR TAX DISTRBTN",NEWLINE,new TabSetting(77)," FEDERAL TAX",new TabSetting(94),"RLLVR CONTRBN",new 
                TabSetting(115),"EXEMPT AMT-DISASTER",NEWLINE,"-",new RepeatItem(169),NEWLINE,NEWLINE,"480.7C",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Tiaa_Totals_Pnd_Tiaa_4807c, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(169),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* *  /  '-' (131)
            //* **
            //* **
            //* **
            //* **
            //* **
            //* **
            //* *  /  '-' (131)
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TIAA",NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new  //Natural: WRITE ( 1 ) NOTITLE // 51X '     COMPANY :  TIAA' /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' INT AMOUNT' 77T 'CANADIAN TAX' / '-' ( 131 ) // 'NR4   ' // 'DATABASE READ' 18T #TIAA-NR4 ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-NR4-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-NR4-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-NR4-FED-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-NR4-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-NR4-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-NR4-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-NR4-FED-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-NR4-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-NR4-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-NR4-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-NR4-FED-TAX-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                TabSetting(34),"  GROSS AMOUNT",new TabSetting(53)," INT AMOUNT",new TabSetting(77),"CANADIAN TAX",NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,"NR4   ",NEWLINE,NEWLINE,"DATABASE READ",new 
                TabSetting(18),pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(49),pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(70),pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  NOT 5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().notEquals(4)))                                                                                  //Natural: IF #RPT-FORM-TYPE NE 4
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(51),"     COMPANY :  TRUST",NEWLINE,NEWLINE);                                    //Natural: WRITE ( 1 ) NOTITLE //51X '     COMPANY :  TRUST' //
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            getReports().write(1, ReportOption.NOTITLE,"1099-R",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_Pnd_Trust_1099_R,                     //Natural: WRITE ( 1 ) NOTITLE '1099-R' // 'DATABASE READ' 18T #TRUST-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-1099-R-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-1099-R-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-R-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-R-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-R-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-R-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-R-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-R-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_R_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_R_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_R_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_R_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_E_A, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            getReports().write(1, ReportOption.NOTITLE,"1099-INT",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_Pnd_Trust_1099_I,                   //Natural: WRITE ( 1 ) NOTITLE '1099-INT' // 'DATABASE READ' 18T #TRUST-1099-I ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-I-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-I-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-I-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-I-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-1099-I-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-I-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-I-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-I-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-I-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-1099-I-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1099-I-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-1099-I-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1099-I-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-1099-I-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TRUST-1099-I-STATE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-1099-I-IRR-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_I_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_I_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_1099_I_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Totals_Pnd_Trust_1099_I_State, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Totals_Pnd_Trust_1099_I_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            getReports().write(1, ReportOption.NOTITLE,"1042-S",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_1042s_Pnd_Trust_1042s,                //Natural: WRITE ( 1 ) NOTITLE '1042-S' // 'DATABASE READ' 18T #TRUST-1042S ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1042S-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-1042S-REFUND ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1042S-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-1042S-TAX-NON-FATCA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TRUST-1042S-TAX-FATCA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-1042S-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1042S-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-1042S-REFUND-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1042S-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-1042S-TAX-NON-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TRUST-1042S-TAX-FATCA-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-1042S-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-1042S-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-1042S-REFUND-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-1042S-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-1042S-TAX-NON-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TRUST-1042S-TAX-FATCA-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(110),pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B, 
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new 
                TabSetting(18),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(49),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        //*  480.7C CHANGES
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            getReports().write(1, ReportOption.NOTITLE,"480.7C",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_Pnd_Trust_4807c, new                  //Natural: WRITE ( 1 ) NOTITLE '480.7C' // 'DATABASE READ' 18T #TRUST-4807C ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-4807C-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-4807C-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-4807C-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-DIST ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-DISASTER-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TRUST-4807C-AFTRTAX-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-4807C-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-EXEMPT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-4807C-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-4807C-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-4807C-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-4807C-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-DIST-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-DISASTER-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TRUST-4807C-AFTRTAX-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-4807C-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-EXEMPT-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-4807C-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-4807C-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TRUST-4807C-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-4807C-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-DIST-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-DISASTER-TAX-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TRUST-4807C-AFTRTAX-CONT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TRUST-4807C-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TRUST-4807C-ROLL-CONT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TRUST-4807C-EXEMPT-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
                ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Totals_Pnd_Trust_4807c_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_4807c_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Totals_Pnd_Trust_4807c_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(135),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new 
                TabSetting(18),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                TabSetting(70),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* **
            //* **
            //* **
            //* **
            //* **
            //* **
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            getReports().write(1, ReportOption.NOTITLE,"NR4   ",NEWLINE,NEWLINE,"DATABASE READ",new TabSetting(18),pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4,                    //Natural: WRITE ( 1 ) NOTITLE 'NR4   ' // 'DATABASE READ' 18T #TRUST-NR4 ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-NR4-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-NR4-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-NR4-FED-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TRUST-NR4-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-NR4-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-NR4-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-NR4-FED-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TRUST-NR4-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TRUST-NR4-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TRUST-NR4-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRUST-NR4-FED-TAX-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATION FOR TIAA & TRUST TOTALS FOR 1099-R
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R),                    //Natural: ASSIGN #TIAA-TRUST-1099-R := #TIAA-1099-R + #TRUST-1099-R
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R.add(pnd_Trust_Totals_Pnd_Trust_1099_R));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt),  //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT := #TIAA-1099-R-GROSS-AMT + #TRUST-1099-R-GROSS-AMT
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable),    //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE := #TIAA-1099-R-TAXABLE + #TRUST-1099-R-TAXABLE
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed),            //Natural: ASSIGN #TIAA-TRUST-1099-R-FED := #TIAA-1099-R-FED + #TRUST-1099-R-FED
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Fed));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State),        //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE := #TIAA-1099-R-STATE + #TRUST-1099-R-STATE
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State.add(pnd_Trust_Totals_Pnd_Trust_1099_R_State));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local),        //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL := #TIAA-1099-R-LOCAL + #TRUST-1099-R-LOCAL
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Local));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc),            //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC := #TIAA-1099-R-IVC + #TRUST-1099-R-IVC
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr),            //Natural: ASSIGN #TIAA-TRUST-1099-R-IRR := #TIAA-1099-R-IRR + #TRUST-1099-R-IRR
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Irr));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int),            //Natural: ASSIGN #TIAA-TRUST-1099-R-INT := #TIAA-1099-R-INT + #TRUST-1099-R-INT
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int.add(pnd_Trust_Totals_Pnd_Trust_1099_R_Int));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B),            //Natural: ASSIGN #TIAA-TRUST-1099-R-E-B := #TIAA-1099-R-E-B + #TRUST-1099-R-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT-E-B := #TIAA-1099-R-GROSS-AMT-E-B + #TRUST-1099-R-GROSS-AMT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE-E-B := #TIAA-1099-R-TAXABLE-E-B + #TRUST-1099-R-TAXABLE-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC-E-B := #TIAA-1099-R-IVC-E-B + #TRUST-1099-R-IVC-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-R-FED-E-B := #TIAA-1099-R-FED-E-B + #TRUST-1099-R-FED-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL-E-B := #TIAA-1099-R-LOCAL-E-B + #TRUST-1099-R-LOCAL-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-R-INT-E-B := #TIAA-1099-R-INT-E-B + #TRUST-1099-R-INT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE-E-B := #TIAA-1099-R-STATE-E-B + #TRUST-1099-R-STATE-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B));
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B.nadd(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B);                                                      //Natural: ASSIGN #TIAA-1099-R-IRR-E-B := #TIAA-1099-R-IRR-E-B + #TRUST-1099-R-IRR-E-B
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A),            //Natural: ASSIGN #TIAA-TRUST-1099-R-E-A := #TIAA-1099-R-E-A + #TRUST-1099-R-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-R-GROSS-AMT-E-A := #TIAA-1099-R-GROSS-AMT-E-A + #TRUST-1099-R-GROSS-AMT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-R-TAXABLE-E-A := #TIAA-1099-R-TAXABLE-E-A + #TRUST-1099-R-TAXABLE-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-R-IVC-E-A := #TIAA-1099-R-IVC-E-A + #TRUST-1099-R-IVC-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-R-FED-E-A := #TIAA-1099-R-FED-E-A + #TRUST-1099-R-FED-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-R-LOCAL-E-A := #TIAA-1099-R-LOCAL-E-A + #TRUST-1099-R-LOCAL-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-R-INT-E-A := #TIAA-1099-R-INT-E-A + #TRUST-1099-R-INT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A));
            pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-R-STATE-E-A := #TIAA-1099-R-STATE-E-A + #TRUST-1099-R-STATE-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_E_A));
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A.nadd(pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_E_A);                                                      //Natural: ASSIGN #TIAA-1099-R-IRR-E-A := #TIAA-1099-R-IRR-E-A + #TRUST-1099-R-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATION FOR TIAA & TRUST TOTALS FOR 1099-I
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I),                    //Natural: ASSIGN #TIAA-TRUST-1099-I := #TIAA-1099-I + #TRUST-1099-I
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I.add(pnd_Trust_Totals_Pnd_Trust_1099_I));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt),  //Natural: ASSIGN #TIAA-TRUST-1099-I-GROSS-AMT := #TIAA-1099-I-GROSS-AMT + #TRUST-1099-I-GROSS-AMT
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable),    //Natural: ASSIGN #TIAA-TRUST-1099-I-TAXABLE := #TIAA-1099-I-TAXABLE + #TRUST-1099-I-TAXABLE
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed),            //Natural: ASSIGN #TIAA-TRUST-1099-I-FED := #TIAA-1099-I-FED + #TRUST-1099-I-FED
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Fed));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State),        //Natural: ASSIGN #TIAA-TRUST-1099-I-STATE := #TIAA-1099-I-STATE + #TRUST-1099-I-STATE
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State.add(pnd_Trust_Totals_Pnd_Trust_1099_I_State));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local),        //Natural: ASSIGN #TIAA-TRUST-1099-I-LOCAL := #TIAA-1099-I-LOCAL + #TRUST-1099-I-LOCAL
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Local));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc),            //Natural: ASSIGN #TIAA-TRUST-1099-I-IVC := #TIAA-1099-I-IVC + #TRUST-1099-I-IVC
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr),            //Natural: ASSIGN #TIAA-TRUST-1099-I-IRR := #TIAA-1099-I-IRR + #TRUST-1099-I-IRR
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Irr));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int),            //Natural: ASSIGN #TIAA-TRUST-1099-I-INT := #TIAA-1099-I-INT + #TRUST-1099-I-INT
                pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int.add(pnd_Trust_Totals_Pnd_Trust_1099_I_Int));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B),            //Natural: ASSIGN #TIAA-TRUST-1099-I-E-B := #TIAA-1099-I-E-B + #TRUST-1099-I-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-I-GROSS-AMT-E-B := #TIAA-1099-I-GROSS-AMT-E-B + #TRUST-1099-I-GROSS-AMT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-I-TAXABLE-E-B := #TIAA-1099-I-TAXABLE-E-B + #TRUST-1099-I-TAXABLE-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-I-IVC-E-B := #TIAA-1099-I-IVC-E-B + #TRUST-1099-I-IVC-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-I-FED-E-B := #TIAA-1099-I-FED-E-B + #TRUST-1099-I-FED-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-I-LOCAL-E-B := #TIAA-1099-I-LOCAL-E-B + #TRUST-1099-I-LOCAL-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B),    //Natural: ASSIGN #TIAA-TRUST-1099-I-INT-E-B := #TIAA-1099-I-INT-E-B + #TRUST-1099-I-INT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B),  //Natural: ASSIGN #TIAA-TRUST-1099-I-STATE-E-B := #TIAA-1099-I-STATE-E-B + #TRUST-1099-I-STATE-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B));
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B.nadd(pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B);                                                      //Natural: ASSIGN #TIAA-1099-I-IRR-E-B := #TIAA-1099-I-IRR-E-B + #TRUST-1099-I-IRR-E-B
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A),            //Natural: ASSIGN #TIAA-TRUST-1099-I-E-A := #TIAA-1099-I-E-A + #TRUST-1099-I-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-I-GROSS-AMT-E-A := #TIAA-1099-I-GROSS-AMT-E-A + #TRUST-1099-I-GROSS-AMT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-I-TAXABLE-E-A := #TIAA-1099-I-TAXABLE-E-A + #TRUST-1099-I-TAXABLE-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-I-IVC-E-A := #TIAA-1099-I-IVC-E-A + #TRUST-1099-I-IVC-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-I-FED-E-A := #TIAA-1099-I-FED-E-A + #TRUST-1099-I-FED-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-I-LOCAL-E-A := #TIAA-1099-I-LOCAL-E-A + #TRUST-1099-I-LOCAL-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A),    //Natural: ASSIGN #TIAA-TRUST-1099-I-INT-E-A := #TIAA-1099-I-INT-E-A + #TRUST-1099-I-INT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A));
            pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A),  //Natural: ASSIGN #TIAA-TRUST-1099-I-STATE-E-A := #TIAA-1099-I-STATE-E-A + #TRUST-1099-I-STATE-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A));
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A.nadd(pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A);                                                      //Natural: ASSIGN #TIAA-1099-I-IRR-E-A := #TIAA-1099-I-IRR-E-A + #TRUST-1099-I-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATION FOR TIAA & TRUST TOTALS FOR 1042S
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s), pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s)); //Natural: ASSIGN #TIAA-TRUST-1042S := #TIAA-1042S + #TRUST-1042S
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt),    //Natural: ASSIGN #TIAA-TRUST-1042S-GROSS-AMT := #TIAA-1042S-GROSS-AMT + #TRUST-1042S-GROSS-AMT
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund),          //Natural: ASSIGN #TIAA-TRUST-1042S-REFUND := #TIAA-1042S-REFUND + #TRUST-1042S-REFUND
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int),                //Natural: ASSIGN #TIAA-TRUST-1042S-INT := #TIAA-1042S-INT + #TRUST-1042S-INT
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non),        //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-NON := #TIAA-1042S-TAX-NON-FATCA + #TRUST-1042S-TAX-NON-FATCA
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca),    //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-FATCA := #TIAA-1042S-TAX-FATCA + #TRUST-1042S-TAX-FATCA
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca.add(pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B),                //Natural: ASSIGN #TIAA-TRUST-1042S-E-B := #TIAA-1042S-E-B + #TRUST-1042S-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-1042S-GROSS-AMT-E-B := #TIAA-1042S-GROSS-AMT-E-B + #TRUST-1042S-GROSS-AMT-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-1042S-REFUND-AMT-E-B := #TIAA-1042S-REFUND-E-B + #TRUST-1042S-REFUND-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B),        //Natural: ASSIGN #TIAA-TRUST-1042S-INT-E-B := #TIAA-1042S-INT-E-B + #TRUST-1042S-INT-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B),  //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-NON-E-B := #TIAA-1042S-TAX-NON-E-B + #TRUST-1042S-TAX-NON-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B),  //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-FATCA-E-B := #TIAA-1042S-TAX-FATCA-E-B + #TRUST-1042S-TAX-FATCA-E-B
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B.add(pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A),                //Natural: ASSIGN #TIAA-TRUST-1042S-E-A := #TIAA-1042S-E-A + #TRUST-1042S-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-1042S-GROSS-AMT-E-A := #TIAA-1042S-GROSS-AMT-E-A + #TRUST-1042S-GROSS-AMT-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-1042S-REFUND-AMT-E-A := #TIAA-1042S-REFUND-E-A + #TRUST-1042S-REFUND-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A),        //Natural: ASSIGN #TIAA-TRUST-1042S-INT-E-A := #TIAA-1042S-INT-E-A + #TRUST-1042S-INT-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A),  //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-NON-E-A := #TIAA-1042S-TAX-NON-E-A + #TRUST-1042S-TAX-NON-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A));
            pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A),  //Natural: ASSIGN #TIAA-TRUST-1042S-TAX-FATCA-E-A := #TIAA-1042S-TAX-FATCA-E-A + #TRUST-1042S-TAX-FATCA-E-A
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A.add(pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A));
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATION FOR TIAA & TRUST TOTALS FOR 480.7C
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c), pnd_Tiaa_Totals_Pnd_Tiaa_4807c.add(pnd_Trust_Totals_Pnd_Trust_4807c)); //Natural: ASSIGN #TIAA-TRUST-4807C := #TIAA-4807C + #TRUST-4807C
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt),    //Natural: ASSIGN #TIAA-TRUST-4807C-GROSS-AMT := #TIAA-4807C-GROSS-AMT + #TRUST-4807C-GROSS-AMT
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt.add(pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable),        //Natural: ASSIGN #TIAA-TRUST-4807C-TAXABLE := #TIAA-4807C-TAXABLE + #TRUST-4807C-TAXABLE
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable.add(pnd_Trust_Totals_Pnd_Trust_4807c_Taxable));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int),                //Natural: ASSIGN #TIAA-TRUST-4807C-INT := #TIAA-4807C-INT + #TRUST-4807C-INT
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int.add(pnd_Trust_Totals_Pnd_Trust_4807c_Int));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed),                //Natural: ASSIGN #TIAA-TRUST-4807C-FED := #TIAA-4807C-FED + #TRUST-4807C-FED
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed.add(pnd_Trust_Totals_Pnd_Trust_4807c_Fed));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist),    //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-DIST := #TIAA-4807C-ROLL-DIST + #TRUST-4807C-ROLL-DIST
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist.add(pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont),    //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-CONT := #TIAA-4807C-ROLL-CONT + #TRUST-4807C-ROLL-CONT
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont.add(pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont));
            //* **
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt),  //Natural: ASSIGN #TIAA-TRUST-4807C-EXEMPT-AMT := #TIAA-4807C-EXEMPT-AMT + #TRUST-4807C-EXEMPT-AMT
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt.add(pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax),  //Natural: ASSIGN #TIAA-TRUST-4807C-DISASTER-TAX := #TIAA-4807C-DISASTER-TAX + #TRUST-4807C-DISASTER-TAX
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax.add(pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont),  //Natural: ASSIGN #TIAA-TRUST-4807C-AFTRTAX-CONT := #TIAA-4807C-AFTRTAX-CONT + #TRUST-4807C-AFTRTAX-CONT
                pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont.add(pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont));
            //* **
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B),                //Natural: ASSIGN #TIAA-TRUST-4807C-E-B := #TIAA-4807C-E-B + #TRUST-4807C-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-GROSS-AMT-E-B := #TIAA-4807C-GROSS-AMT-E-B + #TRUST-4807C-GROSS-AMT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-TAXABLE-E-B := #TIAA-4807C-TAXABLE-E-B + #TRUST-4807C-TAXABLE-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B),        //Natural: ASSIGN #TIAA-TRUST-4807C-INT-E-B := #TIAA-4807C-INT-E-B + #TRUST-4807C-INT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B),        //Natural: ASSIGN #TIAA-TRUST-4807C-FED-E-B := #TIAA-4807C-FED-E-B + #TRUST-4807C-FED-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-DIST-E-B := #TIAA-4807C-ROLL-DIST-E-B + #TRUST-4807C-ROLL-DIST-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-CONT-E-B := #TIAA-4807C-ROLL-CONT-E-B + #TRUST-4807C-ROLL-CONT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B));
            //* **
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-EXEMPT-AMT-E-B := #TIAA-4807C-EXEMPT-AMT-E-B + #TRUST-4807C-EXEMPT-AMT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-DISASTER-TAX-E-B := #TIAA-4807C-DISASTER-TAX-E-B + #TRUST-4807C-DISASTER-TAX-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B),  //Natural: ASSIGN #TIAA-TRUST-4807C-AFTRTAX-CONT-E-B := #TIAA-4807C-AFTRTAX-CONT-E-B + #TRUST-4807C-AFTRTAX-CONT-E-B
                pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B.add(pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B));
            //* **
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A),                //Natural: ASSIGN #TIAA-TRUST-4807C-E-A := #TIAA-4807C-E-A + #TRUST-4807C-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-4807C-GROSS-AMT-E-A := #TIAA-4807C-GROSS-AMT-E-A + #TRUST-4807C-GROSS-AMT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A),  //Natural: ASSIGN #TIAA-TRUST-4807C-TAXABLE-E-A := #TIAA-4807C-TAXABLE-E-A + #TRUST-4807C-TAXABLE-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A),        //Natural: ASSIGN #TIAA-TRUST-4807C-INT-E-A := #TIAA-4807C-INT-E-A + #TRUST-4807C-INT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A),        //Natural: ASSIGN #TIAA-TRUST-4807C-FED-E-A := #TIAA-4807C-FED-E-A + #TRUST-4807C-FED-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A),  //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-DIST-E-A := #TIAA-4807C-ROLL-DIST-E-A + #TRUST-4807C-ROLL-DIST-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A),  //Natural: ASSIGN #TIAA-TRUST-4807C-ROLL-CONT-E-A := #TIAA-4807C-ROLL-CONT-E-A + #TRUST-4807C-ROLL-CONT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A));
            //* **
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A),  //Natural: ASSIGN #TIAA-TRUST-4807C-EXEMPT-AMT-E-A := #TIAA-4807C-EXEMPT-AMT-E-A + #TRUST-4807C-EXEMPT-AMT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea),  //Natural: ASSIGN #TIAA-TRUST-4807C-DISASTER-TAXEA := #TIAA-4807C-DISASTER-TAX-E-A + #TRUST-4807C-DISASTER-TAX-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A));
            pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea.compute(new ComputeParameters(false, pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea),  //Natural: ASSIGN #TIAA-TRUST-4807C-AFTRTAX-CONTEA := #TIAA-4807C-AFTRTAX-CONT-E-A + #TRUST-4807C-AFTRTAX-CONT-E-A
                pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A.add(pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A));
            //* **                              /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATION OF TIAA + TRUST FOR NR4
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4), pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4.add(pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4)); //Natural: ASSIGN #TIAA-TRUST-NR4 := #TIAA-NR4 + #TRUST-NR4
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt),            //Natural: ASSIGN #TIAA-TRUST-NR4-GROSS-AMT := #TIAA-NR4-GROSS-AMT + #TRUST-NR4-GROSS-AMT
                pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt.add(pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int), pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int.add(pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int)); //Natural: ASSIGN #TIAA-TRUST-NR4-INT := #TIAA-NR4-INT + #TRUST-NR4-INT
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax),                //Natural: ASSIGN #TIAA-TRUST-NR4-FED-TAX := #TIAA-NR4-FED-TAX + #TRUST-NR4-FED-TAX
                pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax.add(pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B), pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B.add(pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B)); //Natural: ASSIGN #TIAA-TRUST-NR4-E-B := #TIAA-NR4-E-B + #TRUST-NR4-E-B
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B),    //Natural: ASSIGN #TIAA-TRUST-NR4-GROSS-AMT-E-B := #TIAA-NR4-GROSS-AMT-E-B + #TRUST-NR4-GROSS-AMT-E-B
                pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B.add(pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B),                //Natural: ASSIGN #TIAA-TRUST-NR4-INT-E-B := #TIAA-NR4-INT-E-B + #TRUST-NR4-INT-E-B
                pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B.add(pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B),        //Natural: ASSIGN #TIAA-TRUST-NR4-FED-TAX-E-B := #TIAA-NR4-FED-TAX-E-B + #TRUST-NR4-FED-TAX-E-B
                pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B.add(pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A), pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A.add(pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A)); //Natural: ASSIGN #TIAA-TRUST-NR4-E-A := #TIAA-NR4-E-A + #TRUST-NR4-E-A
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A),    //Natural: ASSIGN #TIAA-TRUST-NR4-GROSS-AMT-E-A := #TIAA-NR4-GROSS-AMT-E-A + #TRUST-NR4-GROSS-AMT-E-A
                pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A.add(pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A),                //Natural: ASSIGN #TIAA-TRUST-NR4-INT-E-A := #TIAA-NR4-INT-E-A + #TRUST-NR4-INT-E-A
                pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A.add(pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A));
            pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A.compute(new ComputeParameters(false, pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A),        //Natural: ASSIGN #TIAA-TRUST-NR4-FED-TAX-E-A := #TIAA-NR4-FED-TAX-E-A + #TRUST-NR4-FED-TAX-E-A
                pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A.add(pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A));
        }                                                                                                                                                                 //Natural: END-IF
        //* *****************
                                                                                                                                                                          //Natural: PERFORM TITLE-REPORT1
        sub_Title_Report1();
        if (condition(Global.isEscape())) {return;}
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new                    //Natural: WRITE ( 1 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' IVC AMOUNT' 94T '    FEDERAL TAX' 117T '    LOCAL TAX ' / 77T ' INT AMOUNT' 94T '    STATE TAX' 117T '    IRR AMT   ' / '-' ( 131 ) //
                TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," IVC AMOUNT",new TabSetting(94),"    FEDERAL TAX",new TabSetting(117),"    LOCAL TAX ",NEWLINE,new 
                TabSetting(77)," INT AMOUNT",new TabSetting(94),"    STATE TAX",new TabSetting(117),"    IRR AMT   ",NEWLINE,"-",new RepeatItem(131),NEWLINE,
                NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL 1099-R",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R,  //Natural: WRITE ( 1 ) NOTITLE // 'GRAND TOTAL 1099-R' // 'TOTAL FORMS' 18T #TIAA-TRUST-1099-R ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-1099-R-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-1099-R-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-R-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-R-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-R-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-R-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-R-STATE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-R-IRR-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_B, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new 
                TabSetting(18),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_E_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Gross_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Taxable_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Ivc_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Local_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Int_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_State_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_R_Total_Pnd_Tiaa_Trust_1099_R_Irr_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),
                NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new                    //Natural: WRITE ( 1 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' IVC AMOUNT' 94T '    FEDERAL TAX' 117T '    LOCAL TAX ' / 77T ' INT AMOUNT' 94T '    STATE TAX' 117T '    IRR AMT   ' / '-' ( 131 ) //
                TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," IVC AMOUNT",new TabSetting(94),"    FEDERAL TAX",new TabSetting(117),"    LOCAL TAX ",NEWLINE,new 
                TabSetting(77)," INT AMOUNT",new TabSetting(94),"    STATE TAX",new TabSetting(117),"    IRR AMT   ",NEWLINE,"-",new RepeatItem(131),NEWLINE,
                NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL 1099-INT",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I,  //Natural: WRITE ( 1 ) NOTITLE 'GRAND TOTAL 1099-INT' // 'TOTAL FORMS' 18T #TIAA-TRUST-1099-I ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-I-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-I-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-I-IVC ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-LOCAL ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-I-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-STATE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-IRR ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-1099-I-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-I-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-I-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-I-IVC-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-LOCAL-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-I-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-STATE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-IRR-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-1099-I-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1099-I-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-1099-I-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1099-I-IVC-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-LOCAL-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-1099-I-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #TIAA-TRUST-1099-I-STATE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-1099-I-IRR-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) // //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(18),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_B, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new 
                TabSetting(18),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_E_A, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Gross_Amt_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Taxable_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Ivc_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Fed_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Local_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Int_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(91),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_State_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(112),pnd_Tiaa_Trust_1099_I_Total_Pnd_Tiaa_Trust_1099_I_Irr_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),
                NEWLINE,NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new                    //Natural: WRITE ( 1 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' REFUND AMOUNT' 77T ' INT AMOUNT' 94T ' NON-FATCA TAX ' 117T '  FATCA TAX   ' / '-' ( 131 ) //
                TabSetting(53)," REFUND AMOUNT",new TabSetting(77)," INT AMOUNT",new TabSetting(94)," NON-FATCA TAX ",new TabSetting(117),"  FATCA TAX   ",NEWLINE,"-",new 
                RepeatItem(131),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL 1042-S",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s,  //Natural: WRITE ( 1 ) NOTITLE // 'GRAND TOTAL 1042-S' // 'TOTAL FORMS' 18T #TIAA-TRUST-1042S ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1042S-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-1042S-REFUND ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1042S-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-1042S-TAX-NON ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-TRUST-1042S-TAX-FATCA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-1042S-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1042S-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-1042S-REFUND-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1042S-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-1042S-TAX-NON-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-TRUST-1042S-TAX-FATCA-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-1042S-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-1042S-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-1042S-REFUND-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-1042S-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-1042S-TAX-NON-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #TIAA-TRUST-1042S-TAX-FATCA-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) // //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(18),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_B, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(49),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_E_A, new 
                ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Refund_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Non_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),pnd_Tiaa_Trust_1042s_Total_Pnd_Tiaa_Trust_1042s_Tax_Fatca_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new 
                RepeatItem(131),NEWLINE,NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        //*  480.7C DISASTER CHANGES
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new                    //Natural: WRITE ( 1 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' TAXABLE AMOUNT' 77T ' INT AMOUNT' 94T 'RLLVR DISTRBTN ' 115T 'TAXABLE AMT-DISASTER' 140T 'AFTR TAX DISTRBTN' / 77T ' FEDERAL TAX' 94T 'RLLVR CONTRBN' 115T 'EXEMPT AMT-DISASTER' / '-' ( 169 ) //
                TabSetting(53)," TAXABLE AMOUNT",new TabSetting(77)," INT AMOUNT",new TabSetting(94),"RLLVR DISTRBTN ",new TabSetting(115),"TAXABLE AMT-DISASTER",new 
                TabSetting(140),"AFTR TAX DISTRBTN",NEWLINE,new TabSetting(77)," FEDERAL TAX",new TabSetting(94),"RLLVR CONTRBN",new TabSetting(115),"EXEMPT AMT-DISASTER",NEWLINE,"-",new 
                RepeatItem(169),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* *  /  '-' (131)
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL 480.7C",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c,  //Natural: WRITE ( 1 ) NOTITLE // 'GRAND TOTAL 480.7C' // 'TOTAL FORMS' 18T #TIAA-TRUST-4807C ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-4807C-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-4807C-TAXABLE ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-4807C-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-DIST ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-DISASTER-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-TRUST-4807C-AFTRTAX-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-4807C-FED ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-CONT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-EXEMPT-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-4807C-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-4807C-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-4807C-TAXABLE-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-4807C-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-DIST-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-DISASTER-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-TRUST-4807C-AFTRTAX-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-4807C-FED-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-CONT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-EXEMPT-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-4807C-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-4807C-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #TIAA-TRUST-4807C-TAXABLE-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-4807C-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-DIST-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-DISASTER-TAXEA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 135T #TIAA-TRUST-4807C-AFTRTAX-CONTEA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 70T #TIAA-TRUST-4807C-FED-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #TIAA-TRUST-4807C-ROLL-CONT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #TIAA-TRUST-4807C-EXEMPT-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 169 ) // //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(135),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new 
                TabSetting(18),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_B, new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Tax_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(135),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Cont_E_B, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_E_A, new 
                ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(50),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Taxable_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Int_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Dist_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Disaster_Taxea, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(135),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Aftrtax_Contea, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(70),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Fed_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Roll_Cont_E_A, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Tiaa_Trust_4807c_Total_Pnd_Tiaa_Trust_4807c_Exempt_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new 
                RepeatItem(169),NEWLINE,NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //* **
            //* **
            //* **
            //* **
            //* **
            //* **
            //* *  /  '-' (131)
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(19),"FORM COUNT",new TabSetting(34),"  GROSS AMOUNT",new                    //Natural: WRITE ( 1 ) NOTITLE /// 19T 'FORM COUNT' 34T '  GROSS AMOUNT' 53T ' INT AMOUNT' 77T 'CANADIAN TAX' / '-' ( 131 )
                TabSetting(53)," INT AMOUNT",new TabSetting(77),"CANADIAN TAX",NEWLINE,"-",new RepeatItem(131));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"GRAND TOTAL NR4   ",NEWLINE,NEWLINE,"TOTAL FORMS",new TabSetting(18),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4,  //Natural: WRITE ( 1 ) NOTITLE // 'GRAND TOTAL NR4   ' // 'TOTAL FORMS' 18T #TIAA-TRUST-NR4 ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-NR4-GROSS-AMT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-NR4-INT ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-NR4-FED-TAX ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BYPASSED' 18T #TIAA-TRUST-NR4-E-B ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-NR4-GROSS-AMT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-NR4-INT-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-NR4-FED-TAX-E-B ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'ACCEPTED' 18T #TIAA-TRUST-NR4-E-A ( EM = ZZZ,ZZZ,Z99 ) 30T #TIAA-TRUST-NR4-GROSS-AMT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 49T #TIAA-TRUST-NR4-INT-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TIAA-TRUST-NR4-FED-TAX-E-A ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / '-' ( 131 ) / //
                new ReportEditMask ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BYPASSED",new TabSetting(18),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_B, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_B, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_B, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"ACCEPTED",new TabSetting(18),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_E_A, new ReportEditMask 
                ("ZZZ,ZZZ,Z99"),new TabSetting(30),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Gross_Amt_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(49),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Int_E_A, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Tiaa_Trust_Nr4_Total_Pnd_Tiaa_Trust_Nr4_Fed_Tax_E_A, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(131),NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SINHSN
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                       //Natural: WRITE ( 1 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TITLE-REPORT1
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BYPASS-REPORT
        //* ***********************************************************************
        //*    110T #BYPASS-REASON
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TEST-INDICATOR-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: F5498-COUPON-FORM-PROCESS
        //* ***********************************************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Form_Type() throws Exception                                                                                                                 //Natural: PROCESS-FORM-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  READING THE FORM FILE WITH SUPERDE-12 INSTEAD OF SUPERDE-10. THIS
        //*  CHANGES THE SORT ORDER FROM PIN TO TIN.
        //*  << CTS - STARTS
        getReports().write(0, ReportOption.NOTITLE,"READING FORM FILE FROM:",pnd_Superde_12,NEWLINE,"Tax Year..............:",pnd_Superde_12_Pnd_Sd12_Tax_Year,           //Natural: WRITE 'READING FORM FILE FROM:' #SUPERDE-12 / 'Tax Year..............:' #SUPERDE-12.#SD12-TAX-YEAR / 'Form Type.............:' #SUPERDE-12.#SD12-FORM-TYPE / 'Active Indicator......:' #SUPERDE-12.#SD12-ACTIVE-IND / 'Mobius indicator......:' #SUPERDE-12.#SD12-MOBIUS-IND / 'Tin...................:' #SUPERDE-12.#SD12-TIN
            NEWLINE,"Form Type.............:",pnd_Superde_12_Pnd_Sd12_Form_Type,NEWLINE,"Active Indicator......:",pnd_Superde_12_Pnd_Sd12_Active_Ind,NEWLINE,
            "Mobius indicator......:",pnd_Superde_12_Pnd_Sd12_Mobius_Ind,NEWLINE,"Tin...................:",pnd_Superde_12_Pnd_Sd12_Tin);
        if (Global.isEscape()) return;
        //*                             /* MUKHERR 010515 <<
        vw_form.startDatabaseRead                                                                                                                                         //Natural: READ FORM BY TIRF-SUPERDE-12 = #SUPERDE-12 THRU #SUPERDE-12-END
        (
        "READ_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_12", ">=", pnd_Superde_12, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_12", "<=", pnd_Superde_12_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_12", "ASC") }
        );
        READ_FORM:
        while (condition(vw_form.readNextRow("READ_FORM")))
        {
            pnd_Ws_Pnd_Read_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #READ-CNT
            //*  DO NOT BREAK AT SAME TIN
            if (condition(form_Tirf_Tin.notEquals(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin())))                                                             //Natural: IF FORM.TIRF-TIN NE #TWRA0214-TIN
            {
                pnd_Ws_Pnd_Part_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #PART-CNT
            }                                                                                                                                                             //Natural: END-IF
            //* *
            //* * SEGRGATING THE 1099R ALONE INTO SEPEARATE FILE NOT KEEPING ANY SUNY
            //* * PARTICIPANTS ALONG WITH REGULAR 1099R PARTICIPANT FORMS
            if (condition(form_Tirf_Form_Type.equals(1)))                                                                                                                 //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-FORM-TYPE-10
                sub_Check_Form_Type_10();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Form_Type_1_10_Found.getBoolean()))                                                                                                     //Natural: IF #FORM-TYPE-1-10-FOUND
                {
                    pnd_Form_Type_1_10_Found.reset();                                                                                                                     //Natural: RESET #FORM-TYPE-1-10-FOUND
                    pnd_Ws_Pnd_Form01_Suny_Skip_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #FORM01-SUNY-SKIP-CNT
                    //*  FORM TYPE 1 & 10 FOUND - SKIP PROCESS
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                              /* MUKHERR 010515  >>
            //*  DO NOT BYPASS PREVIOUSLY REPORTED EMPTY FORMS.
            short decideConditionsMet1980 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-EMPTY-FORM AND NOT FORM.TIRF-PREV-RPT-IND
            if (condition(form_Tirf_Empty_Form.getBoolean() && ! (form_Tirf_Prev_Rpt_Ind.getBoolean())))
            {
                decideConditionsMet1980++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1980 > 0))
            {
                pnd_Ws_Pnd_Empty_Form_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EMPTY-FORM-CNT
                pnd_Ws_Pnd_Mobius_Status.reset();                                                                                                                         //Natural: RESET #MOBIUS-STATUS
                pnd_Ws_Pnd_Message.setValue("Previously reported Empty Form");                                                                                            //Natural: ASSIGN #WS.#MESSAGE := 'Previously reported Empty Form'
                //* SK
                                                                                                                                                                          //Natural: PERFORM WRITE-PAPER-PRINT-HOLD-REPORT
                sub_Write_Paper_Print_Hold_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-RECORD
                sub_Process_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-BYPASS-REPORT
                sub_Write_Bypass_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *  BYPASS CASES
            //* *                                     /* MUKHERR 010515  <<
            //* * NOT BYPASSING IF PIN IS ZERO, STILL NEED TO GET IT PRINTED
            if (condition(form_Tirf_Pin.equals(getZero())))                                                                                                               //Natural: IF FORM.TIRF-PIN = 0
            {
                pnd_Ws_Pnd_Missing_Pin_Cnt_Good.nadd(1);                                                                                                                  //Natural: ADD 1 TO #MISSING-PIN-CNT-GOOD
            }                                                                                                                                                             //Natural: END-IF
            //* *
            short decideConditionsMet2003 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FORM.TIRF-PIN = 9999999 OR FORM.TIRF-PIN = 999999999999
            if (condition(form_Tirf_Pin.equals(9999999) || form_Tirf_Pin.equals(new DbsDecimal("999999999999"))))
            {
                decideConditionsMet2003++;
                pnd_Ws_Pnd_Mobius_Status.setValue("B");                                                                                                                   //Natural: ASSIGN #MOBIUS-STATUS := 'B'
                pnd_Ws_Pnd_Message.setValue("Missing PIN");                                                                                                               //Natural: ASSIGN #WS.#MESSAGE := 'Missing PIN'
                //*  MUKHERR 010515
                pnd_Ws_Pnd_Missing_Pin_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #MISSING-PIN-CNT
            }                                                                                                                                                             //Natural: WHEN FORM.TIRF-PAPER-PRINT-HOLD-IND = 'Y'
            else if (condition(form_Tirf_Paper_Print_Hold_Ind.equals("Y")))
            {
                decideConditionsMet2003++;
                pnd_Ws_Pnd_Mobius_Status.setValue("C");                                                                                                                   //Natural: ASSIGN #MOBIUS-STATUS := 'C'
                pnd_Ws_Pnd_Message.setValue("Paper Print Hold");                                                                                                          //Natural: ASSIGN #WS.#MESSAGE := 'Paper Print Hold'
                //*  MUKHERR 010515
                //*  MUKHERR 010515
                pnd_Ws_Pnd_Paper_Print_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PAPER-PRINT-CNT
            }                                                                                                                                                             //Natural: WHEN FORM.TIRF-MOORE-HOLD-IND NE ' '
            else if (condition(form_Tirf_Moore_Hold_Ind.notEquals(" ")))
            {
                decideConditionsMet2003++;
                pnd_Ws_Pnd_Mobius_Status.setValue("D");                                                                                                                   //Natural: ASSIGN #MOBIUS-STATUS := 'D'
                pnd_Ws_Pnd_Message.setValue("MOORE Hold");                                                                                                                //Natural: ASSIGN #WS.#MESSAGE := 'MOORE Hold'
                pnd_Ws_Pnd_Paper_Print_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PAPER-PRINT-CNT
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet2003 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-PAPER-PRINT-HOLD-REPORT
                sub_Write_Paper_Print_Hold_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-RECORD
                sub_Process_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM WRITE-BYPASS-REPORT
                sub_Write_Bypass_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Bypass_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BYPASS-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *  5498 PARTICIPANT LEVEL REPORTING LOGIC (CALL TAX PRINT LOGIC ONCE
            //* *                                          PER TIN FOR 5498.  TAX PRINT
            //* *                                          WILL FIND AND PRINT ALL 5498
            //* *                                          FORMS ON ONE STATEMENT.)
            short decideConditionsMet2034 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WS.#FORM-TYPE = 4 AND #WS.#TAX-YEAR >= 2002 AND FORM.TIRF-TIN = #TWRA0214-TIN
            if (condition(pnd_Ws_Pnd_Form_Type.equals(4) && pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2002) && form_Tirf_Tin.equals(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin())))
            {
                decideConditionsMet2034++;
                pnd_Ws_Pnd_Message.setValue("Multiple 5498");                                                                                                             //Natural: ASSIGN #WS.#MESSAGE := 'Multiple 5498'
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet2034 > 0))
            {
                pnd_Ws_Pnd_5498_Bypass_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #5498-BYPASS-CNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-RECORD
                sub_Process_Form_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
                //*  MOBIUS MASS MIGRATION
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form.getAstISN("READ_FORM"));                                                            //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM. )
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year().compute(new ComputeParameters(false, pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year()),  //Natural: ASSIGN #TWRA0214-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
                form_Tirf_Tax_Year.val());
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin().setValue(form_Tirf_Tin);                                                                             //Natural: ASSIGN #TWRA0214-TIN := FORM.TIRF-TIN
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type().setValue(form_Tirf_Form_Type);                                                                 //Natural: ASSIGN #TWRA0214-FORM-TYPE := FORM.TIRF-FORM-TYPE
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind().setValue("M");                                                                           //Natural: ASSIGN #TWRA0214-MOBIUS-MIGR-IND := 'M'
            pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key().setValue(" ");                                                                                //Natural: ASSIGN #TWRA0214-MOBIUS-KEY := ' '
            //*  SINHSN
            //*  DUTTAD 010815
            //*  SINHSN
            DbsUtil.callnat(Twrn0214.class , getCurrentProcessState(), pnd_Ws_Pnd_Form_Page_Cnt, pnd_Ws_Pnd_Zero_Isn, pdaTwra0214.getPnd_Twra0214_Link_Area(),            //Natural: CALLNAT 'TWRN0214' #FORM-PAGE-CNT #ZERO-ISN #TWRA0214-LINK-AREA PSTA9610 #LINK-LET-TYPE ( * ) #TEST-IND #COUPON-IND #LTR-ARRAY ( * ) #ADDITIONAL-FORMS
                pdaPsta9610.getPsta9610(), pnd_Ws_Pnd_Link_Let_Type.getValue("*"), pnd_Test_Ind, pnd_Coupon_Ind, pnd_Ltr_Array.getValue("*"), pnd_Additional_Forms);
            if (condition(Global.isEscape())) return;
            //*  ERROR OCCURED
            if (condition(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code().notEquals(getZero())))                                                            //Natural: IF #TWRA0214-RET-CODE NE 0
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Problem in Tax Print Module TWRN0214",new TabSetting(77),"***",NEWLINE,"***",new     //Natural: WRITE ( 0 ) '***' 25T 'Problem in Tax Print Module TWRN0214' 77T '***' / '***' 25T 'CODE :' #TWRA0214-RET-CODE 77T '***' / '***' 25T 'MSG  :' #TWRA0214-RET-MSG 77T '***'
                    TabSetting(25),"CODE :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"MSG  :",pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg(),new 
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(101);  if (true) return;                                                                                                                //Natural: TERMINATE 101
                //*  MOBIUS MASS MIGRATION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Mobius_Status.setValue("A1");                                                                                                                      //Natural: ASSIGN #MOBIUS-STATUS := 'A1'
            pnd_Ws_Pnd_Message.setValue("Normal Completion");                                                                                                             //Natural: ASSIGN #WS.#MESSAGE := 'Normal Completion'
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-RECORD
            sub_Process_Form_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Pnd_Form_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FORM-CNT
            //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM WRITE-TEST-INDICATOR-REPORT
            sub_Write_Test_Indicator_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Form_Type.equals(3) || pnd_Ws_Pnd_Form_Type.equals(5) || pnd_Ws_Pnd_Form_Type.equals(7)))                                                //Natural: IF #WS.#FORM-TYPE = 3 OR = 5 OR = 7
        {
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                   //Natural: WRITE ( 5 ) // 26T '***** END OF REPORT *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                       //Natural: WRITE ( 2 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        getReports().write(3, new TabSetting(2),"-",new RepeatItem(131),NEWLINE,new TabSetting(2),"Test Indicator Count:",pnd_Test_Ind_Cnt, new ReportEditMask            //Natural: WRITE ( 3 ) 2T '-' ( 131 ) / 2T 'Test Indicator Count:' #TEST-IND-CNT ( EM = ZZZZ,ZZZ,Z99 )
            ("ZZZZ,ZZZ,Z99"));
        if (Global.isEscape()) return;
        //*  SINHSN
        getReports().write(3, NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                                            //Natural: WRITE ( 3 ) // 26T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE EQ 04
        {
            getReports().write(6, ReportOption.NOTITLE,new TabSetting(2),"-",new RepeatItem(131),NEWLINE,new TabSetting(2),"Total Coupon Count: ",pnd_Coupon_Cnt,         //Natural: WRITE ( 6 ) 2T '-' ( 131 ) / 2T 'Total Coupon Count: ' #COUPON-CNT ( EM = ZZZZ,ZZZ,Z99 )
                new ReportEditMask ("ZZZZ,ZZZ,Z99"));
            if (Global.isEscape()) return;
            //*  SINHSN
            getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(26),"***** END OF REPORT *****");                                                   //Natural: WRITE ( 6 ) // 26T '***** END OF REPORT *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Form_Record() throws Exception                                                                                                               //Natural: PROCESS-FORM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                             //Natural: IF FORM.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                    //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #WS.#FORM-TYPE = 2
        //*   IF #MOBIUS-STATUS = 'A1'          /* ACCEPTED REPORT
        //*     DISPLAY(5)(HC=L)
        //*       'Tax/Year'       FORM.TIRF-TAX-YEAR
        //*       '/Form'          FORM.TIRF-FORM-TYPE
        //*       '/Comp'          FORM.TIRF-COMPANY-CDE     (LC=�)
        //*       '/TIN'           FORM.TIRF-TIN
        //*       '/Contract'      #CONTRACT-TXT
        //*       'Pay/ee'         FORM.TIRF-PAYEE-CDE
        //*       'Form/Key'       FORM.TIRF-KEY
        //*       '/PIN'           FORM.TIRF-PIN             (EM=999999999999)
        //*       'Mobius/Status'  #MOBIUS-STATUS            (LC=��)
        //*       '/Message'       #WS.#MESSAGE
        //*   END-IF
        //*  END-IF
        //*  WRITE WORK FILE 2            /* SINHSN
        //*   FORM.TIRF-SUPERDE-3
        //*   #MOBIUS-STATUS
        //*  #WS.#MESSAGE
        //*  MUKHERR 010515 << STARTS
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(0);                                                                                                //Natural: MOVE 0 TO #RPT-TAXABLE-AMT #RPT-IVC-AMT #RPT-FED-TAX-WTHLD #RPT-FED-GROSS-AMT #RPT-IRR-AMT #RPT-PCT-OF-TOT #RPT-FED-IVC-AMT #RPT-ROTH-YEAR #RPT-TOT-CONTRACTUAL-IVC #RPT-TRAD-IRA-CONTRIB #RPT-SEP-AMT #RPT-TRAD-IRA-ROLLOVER #RPT-IRA-RECHAR-AMT #RPT-ACCOUNT-FMV #RPT-ROTH-IRA-CONTRIB #RPT-ROTH-IRA-CONVERSION
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Gross_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pct_Of_Tot().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion().setValue(0);
        FOR01:                                                                                                                                                            //Natural: FOR #Z 1 TO 12
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(12)); pnd_Z.nadd(1))
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Distr().getValue(pnd_Z).setValue(0);                                                                            //Natural: MOVE 0 TO #RPT-STATE-DISTR ( #Z ) #RPT-STATE-TAX-WTHLD ( #Z ) #RPT-LOC-DISTR ( #Z ) #RPT-LOC-TAX-WTHLD ( #Z )
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Tax_Wthld().getValue(pnd_Z).setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Distr().getValue(pnd_Z).setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Tax_Wthld().getValue(pnd_Z).setValue(0);
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-FILE
        sub_Write_Report_File();
        if (condition(Global.isEscape())) {return;}
        //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM PROCESS-FORM-UPDATE
        sub_Process_Form_Update();
        if (condition(Global.isEscape())) {return;}
        //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-CONTROL-REPORT
        sub_Write_Extract_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4) && ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Coupon_Ind().equals("Y")))                 //Natural: IF #RPT-FORM-TYPE EQ 04 AND #RPT-COUPON-IND EQ 'Y'
        {
            //*  SINHSN
                                                                                                                                                                          //Natural: PERFORM F5498-COUPON-FORM-PROCESS
            sub_F5498_Coupon_Form_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report_File() throws Exception                                                                                                                 //Natural: WRITE-REPORT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year().setValue(form_Tirf_Tax_Year);                                                                                  //Natural: ASSIGN #RPT-TAX-YEAR := FORM.TIRF-TAX-YEAR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().setValue(form_Tirf_Tin);                                                                                            //Natural: ASSIGN #RPT-TIN := FORM.TIRF-TIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr().setValue(pnd_Contract_Txt);                                                                                //Natural: ASSIGN #RPT-CONTRACT-NBR := #CONTRACT-TXT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde().setValue(form_Tirf_Payee_Cde);                                                                                //Natural: ASSIGN #RPT-PAYEE-CDE := FORM.TIRF-PAYEE-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().setValue(form_Tirf_Pin);                                                                                            //Natural: ASSIGN #RPT-PIN := FORM.TIRF-PIN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Test_Ind().setValue(pnd_Test_Ind);                                                                                        //Natural: ASSIGN #RPT-TEST-IND := #TEST-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name().setValue(form_Tirf_Participant_Name);                                                                         //Natural: ASSIGN #RPT-PART-NAME := FORM.TIRF-PARTICIPANT-NAME
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().setValue(pnd_Ws_Pnd_Mobius_Status);                                                                    //Natural: MOVE #MOBIUS-STATUS TO #RPT-BYPASS-ACCPT-IND
        //*  /* SNEHA CHANGES ENDED - SINHSN
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().setValue(form_Tirf_Form_Type);                                                                                //Natural: MOVE FORM.TIRF-FORM-TYPE TO #RPT-FORM-TYPE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt().setValue(0);                                                                                            //Natural: MOVE 0 TO #RPT-1099R-GROSS-AMT #RPT-1099R-TAXABLE-AMT #RPT-1099R-IVC-AMT #RPT-1099R-INT-AMT #RPT-1099R-FED-TAX #RPT-1099R-STATE-TAX #RPT-1099R-LCL-TAX #RPT-1099R-IRR-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt().setValue(0);
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 01
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt().setValue(form_Tirf_Gross_Amt);                                                                      //Natural: MOVE FORM.TIRF-GROSS-AMT TO #RPT-1099R-GROSS-AMT
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt().setValue(form_Tirf_Taxable_Amt);                                                                  //Natural: MOVE FORM.TIRF-TAXABLE-AMT TO #RPT-1099R-TAXABLE-AMT
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax().setValue(form_Tirf_Fed_Tax_Wthld);                                                                    //Natural: MOVE FORM.TIRF-FED-TAX-WTHLD TO #RPT-1099R-FED-TAX
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt().setValue(form_Tirf_Ivc_Amt);                                                                          //Natural: MOVE FORM.TIRF-IVC-AMT TO #RPT-1099R-IVC-AMT
            if (condition(form_Count_Casttirf_1099_R_State_Grp.notEquals(getZero())))                                                                                     //Natural: IF C*FORM.TIRF-1099-R-STATE-GRP NE 0
            {
                pnd_C.setValue(form_Count_Casttirf_1099_R_State_Grp);                                                                                                     //Natural: ASSIGN #C := C*FORM.TIRF-1099-R-STATE-GRP
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax().nadd(form_Tirf_State_Tax_Wthld.getValue(1,":",pnd_C));                                          //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( 1:#C ) TO #RPT-1099R-STATE-TAX
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax().nadd(form_Tirf_Loc_Tax_Wthld.getValue(1,":",pnd_C));                                              //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( 1:#C ) TO #RPT-1099R-LCL-TAX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 02
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt().setValue(0);                                                                                        //Natural: MOVE 0 TO #RPT-1099I-GROSS-AMT #RPT-1099I-TAXABLE-AMT #RPT-1099I-IVC-AMT #RPT-1099I-INT-AMT #RPT-1099I-FED-TAX #RPT-1099I-STATE-TAX #RPT-1099I-LCL-TAX #RPT-1099I-IRR-AMT
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt().setValue(0);
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt().setValue(form_Tirf_Int_Amt);                                                                          //Natural: MOVE FORM.TIRF-INT-AMT TO #RPT-1099I-INT-AMT
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax().setValue(form_Tirf_Fed_Tax_Wthld);                                                                    //Natural: MOVE FORM.TIRF-FED-TAX-WTHLD TO #RPT-1099I-FED-TAX
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF FORM.TIRF-COMPANY-CDE = 'T'                           /* EINCHG
        //*  EINCHG
        if (condition(form_Tirf_Company_Cde.equals("T") || form_Tirf_Company_Cde.equals("A")))                                                                            //Natural: IF FORM.TIRF-COMPANY-CDE = 'T' OR = 'A'
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().setValue("T");                                                                                             //Natural: MOVE 'T' TO #RPT-COMP-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(form_Tirf_Company_Cde.equals("X")))                                                                                                             //Natural: IF FORM.TIRF-COMPANY-CDE = 'X'
            {
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().setValue("X");                                                                                         //Natural: MOVE 'X' TO #RPT-COMP-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STATE-REPORT-FIELD-MOVE
        sub_State_Report_Field_Move();
        if (condition(Global.isEscape())) {return;}
        pnd_Y_Cnt.setValue(0);                                                                                                                                            //Natural: ASSIGN #Y-CNT := 0
                                                                                                                                                                          //Natural: PERFORM STATE-FIELDS
        sub_State_Fields();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Y_Cnt.equals(getZero())))                                                                                                                       //Natural: IF #Y-CNT = 0
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(form_Tirf_Roth_Year);                                                                            //Natural: MOVE FORM.TIRF-ROTH-YEAR TO #RPT-ROTH-YEAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 04
        {
                                                                                                                                                                          //Natural: PERFORM POPULATE-5498-REPORT-FIELDS
            sub_Populate_5498_Report_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  SAIK
            getWorkFiles().write(4, false, ldaTwrl2001.getPnd_Rpt_Twrl2001());                                                                                            //Natural: WRITE WORK FILE 4 #RPT-TWRL2001
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S  &&&
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 03
        {
                                                                                                                                                                          //Natural: PERFORM POPULATE-1042S-REPORT-FIELDS
            sub_Populate_1042s_Report_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 05
        {
                                                                                                                                                                          //Natural: PERFORM POPULATE-4807C-REPORT-FIELDS
            sub_Populate_4807c_Report_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 07
        {
                                                                                                                                                                          //Natural: PERFORM POPULATE-NR4-REPORT-FIELDS
            sub_Populate_Nr4_Report_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  &&&
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_State_Fields() throws Exception                                                                                                                      //Natural: STATE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I2 1 TO C*TIRF-1099-R-STATE-GRP
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(form_Count_Casttirf_1099_R_State_Grp)); pnd_I2.nadd(1))
        {
            if (condition(form_Tirf_State_Rpt_Ind.getValue(pnd_I2).equals("Y")))                                                                                          //Natural: IF TIRF-STATE-RPT-IND ( #I2 ) = 'Y'
            {
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Rpt_Ind().getValue(pnd_I2).setValue("Y");                                                                   //Natural: MOVE 'Y' TO #RPT-STATE-RPT-IND ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Code().getValue(pnd_I2).setValue(form_Tirf_State_Code.getValue(pnd_I2));                                    //Natural: MOVE TIRF-STATE-CODE ( #I2 ) TO #RPT-STATE-CODE ( #I2 )
                pnd_Y_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y-CNT
                if (condition(pnd_Y_Cnt.notEquals(1)))                                                                                                                    //Natural: IF #Y-CNT NE 1
                {
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Gross_Amt().setValue(0);                                                                                  //Natural: MOVE 0 TO #RPT-FED-GROSS-AMT #RPT-FED-IVC-AMT #RPT-FED-TAX-WTHLD #RPT-TAXABLE-AMT #RPT-TOT-CONTRACTUAL-IVC #RPT-IRR-AMT
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Ivc_Amt().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(0);
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(0);
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Distr().getValue(pnd_I2).setValue(form_Tirf_State_Distr.getValue(pnd_I2));                                  //Natural: MOVE FORM.TIRF-STATE-DISTR ( #I2 ) TO #RPT-STATE-DISTR ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Tax_Wthld().getValue(pnd_I2).setValue(form_Tirf_State_Tax_Wthld.getValue(pnd_I2));                          //Natural: MOVE FORM.TIRF-STATE-TAX-WTHLD ( #I2 ) TO #RPT-STATE-TAX-WTHLD ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Distr().getValue(pnd_I2).setValue(form_Tirf_Loc_Distr.getValue(pnd_I2));                                      //Natural: MOVE FORM.TIRF-LOC-DISTR ( #I2 ) TO #RPT-LOC-DISTR ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Loc_Tax_Wthld().getValue(pnd_I2).setValue(form_Tirf_Loc_Tax_Wthld.getValue(pnd_I2));                              //Natural: MOVE FORM.TIRF-LOC-TAX-WTHLD ( #I2 ) TO #RPT-LOC-TAX-WTHLD ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_State_Hardcopy_Ind().getValue(pnd_I2).setValue(form_Tirf_State_Hardcopy_Ind.getValue(pnd_I2));                    //Natural: MOVE FORM.TIRF-STATE-HARDCOPY-IND ( #I2 ) TO #RPT-STATE-HARDCOPY-IND ( #I2 )
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Year().setValue(form_Tirf_Roth_Year);                                                                        //Natural: MOVE FORM.TIRF-ROTH-YEAR TO #RPT-ROTH-YEAR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_State_Report_Field_Move() throws Exception                                                                                                           //Natural: STATE-REPORT-FIELD-MOVE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Id_Ind().setValue(form_Tirf_Tax_Id_Type);                                                                                 //Natural: MOVE FORM.TIRF-TAX-ID-TYPE TO #RPT-ID-IND
        if (condition(form_Tirf_Tax_Id_Type.equals("1") || form_Tirf_Tax_Id_Type.equals("2") || form_Tirf_Tax_Id_Type.equals("3")))                                       //Natural: IF FORM.TIRF-TAX-ID-TYPE = '1' OR = '2' OR = '3'
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin2().setValue(form_Tirf_Tin);                                                                                       //Natural: MOVE FORM.TIRF-TIN TO #RPT-TIN2
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Distribution_Cde().setValue(form_Tirf_Distribution_Cde);                                                                  //Natural: MOVE FORM.TIRF-DISTRIBUTION-CDE TO #RPT-DISTRIBUTION-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Not_Det().setValue(form_Tirf_Taxable_Not_Det);                                                                    //Natural: MOVE FORM.TIRF-TAXABLE-NOT-DET TO #RPT-TAXABLE-NOT-DET
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Taxable_Amt().setValue(form_Tirf_Taxable_Amt);                                                                            //Natural: MOVE FORM.TIRF-TAXABLE-AMT TO #RPT-TAXABLE-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ivc_Amt().setValue(form_Tirf_Ivc_Amt);                                                                                    //Natural: MOVE FORM.TIRF-IVC-AMT TO #RPT-IVC-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Fed_Tax_Wthld().setValue(form_Tirf_Fed_Tax_Wthld);                                                                        //Natural: MOVE FORM.TIRF-FED-TAX-WTHLD TO #RPT-FED-TAX-WTHLD
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Contractual_Ivc().setValue(form_Tirf_Tot_Contractual_Ivc);                                                            //Natural: MOVE FORM.TIRF-TOT-CONTRACTUAL-IVC TO #RPT-TOT-CONTRACTUAL-IVC
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Distr().setValue(form_Tirf_Ira_Distr);                                                                                //Natural: MOVE FORM.TIRF-IRA-DISTR TO #RPT-IRA-DISTR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pct_Of_Tot().setValue(form_Tirf_Pct_Of_Tot);                                                                              //Natural: MOVE FORM.TIRF-PCT-OF-TOT TO #RPT-PCT-OF-TOT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tot_Distr().setValue(form_Tirf_Tot_Distr);                                                                                //Natural: MOVE FORM.TIRF-TOT-DISTR TO #RPT-TOT-DISTR
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Moore_Test_Ind().setValue(form_Tirf_Moore_Test_Ind);                                                                      //Natural: MOVE FORM.TIRF-MOORE-TEST-IND TO #RPT-MOORE-TEST-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Moore_Hold_Ind().setValue(form_Tirf_Moore_Hold_Ind);                                                                      //Natural: MOVE FORM.TIRF-MOORE-HOLD-IND TO #RPT-MOORE-HOLD-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Participant_Name().setValue(form_Tirf_Participant_Name);                                                                  //Natural: MOVE FORM.TIRF-PARTICIPANT-NAME TO #RPT-PARTICIPANT-NAME
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln1().setValue(form_Tirf_Addr_Ln1);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN1 TO #RPT-ADDR-LN1
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln2().setValue(form_Tirf_Addr_Ln2);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN2 TO #RPT-ADDR-LN2
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln3().setValue(form_Tirf_Addr_Ln3);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN3 TO #RPT-ADDR-LN3
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln4().setValue(form_Tirf_Addr_Ln4);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN4 TO #RPT-ADDR-LN4
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln5().setValue(form_Tirf_Addr_Ln5);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN5 TO #RPT-ADDR-LN5
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Addr_Ln6().setValue(form_Tirf_Addr_Ln6);                                                                                  //Natural: MOVE FORM.TIRF-ADDR-LN6 TO #RPT-ADDR-LN6
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Zip1().setValue(form_Tirf_Zip.getSubstring(1,5));                                                                         //Natural: ASSIGN #RPT-ZIP1 := SUBSTR ( TIRF-ZIP,1,5 )
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Zip4().setValue(form_Tirf_Zip.getSubstring(6,4));                                                                         //Natural: ASSIGN #RPT-ZIP4 := SUBSTR ( TIRF-ZIP,6,4 )
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Walk_Rte().setValue(form_Tirf_Walk_Rte);                                                                                  //Natural: MOVE FORM.TIRF-WALK-RTE TO #RPT-WALK-RTE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Geo_Cde().setValue(form_Tirf_Geo_Cde);                                                                                    //Natural: MOVE FORM.TIRF-GEO-CDE TO #RPT-GEO-CDE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Country_Name().setValue(form_Tirf_Country_Name);                                                                          //Natural: MOVE FORM.TIRF-COUNTRY-NAME TO #RPT-COUNTRY-NAME
        if (condition(form_Tirf_Foreign_Addr.equals("Y")))                                                                                                                //Natural: IF FORM.TIRF-FOREIGN-ADDR EQ 'Y'
        {
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Foreign_Addr().setValue(form_Tirf_Foreign_Addr);                                                                      //Natural: MOVE FORM.TIRF-FOREIGN-ADDR TO #RPT-FOREIGN-ADDR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Y_Cnt.setValue(0);                                                                                                                                            //Natural: ASSIGN #Y-CNT := 0
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Irr_Amt().setValue(form_Tirf_Irr_Amt);                                                                                    //Natural: MOVE FORM.TIRF-IRR-AMT TO #RPT-IRR-AMT
    }
    private void sub_Populate_5498_Report_Fields() throws Exception                                                                                                       //Natural: POPULATE-5498-REPORT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Coupon_Ind().setValue(pnd_Coupon_Ind);                                                                                    //Natural: MOVE #COUPON-IND TO #RPT-COUPON-IND
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib().setValue(form_Tirf_Trad_Ira_Contrib);                                                                  //Natural: MOVE TIRF-TRAD-IRA-CONTRIB TO #RPT-TRAD-IRA-CONTRIB
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt().setValue(form_Tirf_Sep_Amt);                                                                                    //Natural: MOVE TIRF-SEP-AMT TO #RPT-SEP-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover().setValue(form_Tirf_Trad_Ira_Rollover);                                                                //Natural: MOVE TIRF-TRAD-IRA-ROLLOVER TO #RPT-TRAD-IRA-ROLLOVER
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt().setValue(form_Tirf_Ira_Rechar_Amt);                                                                      //Natural: MOVE TIRF-IRA-RECHAR-AMT TO #RPT-IRA-RECHAR-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv().setValue(form_Tirf_Account_Fmv);                                                                            //Natural: MOVE TIRF-ACCOUNT-FMV TO #RPT-ACCOUNT-FMV
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib().setValue(form_Tirf_Roth_Ira_Contrib);                                                                  //Natural: MOVE TIRF-ROTH-IRA-CONTRIB TO #RPT-ROTH-IRA-CONTRIB
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion().setValue(form_Tirf_Roth_Ira_Conversion);                                                            //Natural: MOVE TIRF-ROTH-IRA-CONVERSION TO #RPT-ROTH-IRA-CONVERSION
        //*  DASDH
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt().setValue(form_Tirf_Postpn_Amt);                                                                              //Natural: MOVE TIRF-POSTPN-AMT TO #RPT-POSTPN-AMT
        //*  SAIK
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt().setValue(form_Tirf_Repayments_Amt);                                                                      //Natural: MOVE TIRF-REPAYMENTS-AMT TO #RPT-REPAYMENTS-AMT
        pnd_Coupon_Ind.reset();                                                                                                                                           //Natural: RESET #COUPON-IND
        //* ***********************************************************
        //* *CHECK IRA FILE TO SEE IF THE DETAILS ARE RELATED TO
        //* *PERIOD 1 OR PERIOD 2
        //* ***********************************************************
                                                                                                                                                                          //Natural: PERFORM ACCESS-IRA-FILE
        sub_Access_Ira_File();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Access_Ira_File() throws Exception                                                                                                                   //Natural: ACCESS-IRA-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ira_Start_Twrc_Tax_Year.setValue(form_Tirf_Tax_Year);                                                                                                         //Natural: ASSIGN #IRA-START.TWRC-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Ira_Start_Twrc_Status.setValue(" ");                                                                                                                          //Natural: ASSIGN #IRA-START.TWRC-STATUS := ' '
        pnd_Ira_Start_Twrc_Tax_Id.setValue(form_Tirf_Tin);                                                                                                                //Natural: ASSIGN #IRA-START.TWRC-TAX-ID := FORM.TIRF-TIN
        pnd_Ira_Start_Twrc_Contract.setValue(form_Tirf_Contract_Nbr);                                                                                                     //Natural: ASSIGN #IRA-START.TWRC-CONTRACT := FORM.TIRF-CONTRACT-NBR
        pnd_Ira_Start_Twrc_Payee.setValue(form_Tirf_Payee_Cde);                                                                                                           //Natural: ASSIGN #IRA-START.TWRC-PAYEE := FORM.TIRF-PAYEE-CDE
        pnd_Ira_End.setValue(pnd_Ira_Start);                                                                                                                              //Natural: ASSIGN #IRA-END := #IRA-START
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ira_Start,26,1);                                                                                                  //Natural: MOVE LOW-VALUES TO SUBSTR ( #IRA-START,26,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ira_End,26,1);                                                                                                   //Natural: MOVE HIGH-VALUES TO SUBSTR ( #IRA-END ,26,1 )
        ldaTwrl9610.getVw_ira().startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) IRA BY TWRC-S2-FORMS = #IRA-START THRU #IRA-END
        (
        "R_IRA",
        new Wc[] { new Wc("TWRC_S2_FORMS", ">=", pnd_Ira_Start, "And", WcType.BY) ,
        new Wc("TWRC_S2_FORMS", "<=", pnd_Ira_End, WcType.BY) },
        new Oc[] { new Oc("TWRC_S2_FORMS", "ASC") },
        1
        );
        R_IRA:
        while (condition(ldaTwrl9610.getVw_ira().readNextRow("R_IRA")))
        {
            if (condition(ldaTwrl9610.getIra_Twrc_Source().equals("P2") || ldaTwrl9610.getIra_Twrc_Update_Source().equals("P2")))                                         //Natural: IF IRA.TWRC-SOURCE = 'P2' OR IRA.TWRC-UPDATE-SOURCE = 'P2'
            {
                pnd_P2_Indicator.setValue("Y");                                                                                                                           //Natural: ASSIGN #P2-INDICATOR := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P2_Indicator.setValue("N");                                                                                                                           //Natural: ASSIGN #P2-INDICATOR := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Paper_Print_Hold_Report() throws Exception                                                                                                     //Natural: WRITE-PAPER-PRINT-HOLD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        pnd_Contract_Txt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-TXT
        if (condition(form_Tirf_Contract_Nbr.notEquals(" ")))                                                                                                             //Natural: IF FORM.TIRF-CONTRACT-NBR NE ' '
        {
            pnd_Contract_Txt.setValueEdited(form_Tirf_Contract_Nbr,new ReportEditMask("XXXXXXX'-'X"));                                                                    //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX'-'X ) TO #CONTRACT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Txt.setValue(pnd_Contract_Hdr_Lit);                                                                                                              //Natural: ASSIGN #CONTRACT-TXT := #CONTRACT-HDR-LIT
        }                                                                                                                                                                 //Natural: END-IF
        //*   /* SNEHA CHANGES STARTED - SINHSN
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP6890|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input);                                                                                     //Natural: INPUT #WS.#INPUT
                if (condition(! (pnd_Ws_Pnd_Form_Type.greaterOrEqual(1) && pnd_Ws_Pnd_Form_Type.lessOrEqual(10))))                                                        //Natural: IF NOT #WS.#FORM-TYPE = 1 THRU 10
                {
                    //*  JRR 07/2012
                    if (condition(pnd_Ws_Pnd_Form_Type.equals(8) || pnd_Ws_Pnd_Form_Type.equals(9)))                                                                      //Natural: IF #WS.#FORM-TYPE = 8 OR = 9
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing or Invalid Form Type:",pnd_Ws_Pnd_Form_Type,new TabSetting(77),      //Natural: WRITE ( 0 ) '***' 25T 'Missing or Invalid Form Type:' #WS.#FORM-TYPE 77T '***'
                            "***");
                        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(100);  if (true) return;                                                                                                        //Natural: TERMINATE 100
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*                                                  /* MUKHERR 010515<<
                //*  FOR SUNY PARTICIPANTS WE ARE PASSING THE PROGRAM CONTROL TO TWRP6892
                if (condition(pnd_Ws_Pnd_Form_Type.equals(10)))                                                                                                           //Natural: IF #WS.#FORM-TYPE = 10
                {
                    getReports().write(0, ReportOption.NOTITLE,"FETCH TWRP6892");                                                                                         //Natural: WRITE 'FETCH TWRP6892'
                    if (Global.isEscape()) return;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_Input);                                                                                        //Natural: FETCH 'TWRP6892' #INPUT
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP6892"));
                    if (condition(Global.isEscape())) return;
                    //*  /* SNEHA CHANGES ENDED - SINHSN
                }                                                                                                                                                         //Natural: END-IF
                //*                                                  /* MUKHERR 010515  >>
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                //*                                               /* &&&
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
                sub_Update_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*                                               /*
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE RECORD #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing Control Record",new TabSetting(77),"***");                                       //Natural: WRITE ( 0 ) '***' 25T 'Missing Control Record' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Letter_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd()); //Natural: MOVE EDITED #TWRP0600-INTERFACE-CCYYMMDD TO #LETTER-DTE ( EM = YYYYMMDD )
    }
    //*  FATCA &&&
    private void sub_Populate_1042s_Report_Fields() throws Exception                                                                                                      //Natural: POPULATE-1042S-REPORT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income().setValue(0);                                                                                         //Natural: MOVE 0 TO #RPT-1042S-GROSS-INCOME #RPT-1042S-INTEREST #RPT-1042S-NRA-TAX-WTHLD #RPT-1042S-S-REFUND-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt().setValue(0);
        FOR03:                                                                                                                                                            //Natural: FOR #C 1 C*FORM.TIRF-1042-S-LINE-GRP
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(form_Count_Casttirf_1042_S_Line_Grp)); pnd_C.nadd(1))
        {
            if (condition(form_Tirf_1042_S_Income_Code.getValue(pnd_C).equals("01") || form_Tirf_Chptr4_Income_Cde.getValue(pnd_C).equals("01")))                         //Natural: IF TIRF-1042-S-INCOME-CODE ( #C ) = '01' OR TIRF-CHPTR4-INCOME-CDE ( #C ) = '01'
            {
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest().nadd(form_Tirf_Gross_Income.getValue(pnd_C));                                                    //Natural: ADD FORM.TIRF-GROSS-INCOME ( #C ) TO #RPT-1042S-INTEREST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income().nadd(form_Tirf_Gross_Income.getValue(pnd_C));                                                //Natural: ADD FORM.TIRF-GROSS-INCOME ( #C ) TO #RPT-1042S-GROSS-INCOME
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld().nadd(form_Tirf_Nra_Tax_Wthld.getValue(pnd_C));                                                  //Natural: ADD FORM.TIRF-NRA-TAX-WTHLD ( #C ) TO #RPT-1042S-NRA-TAX-WTHLD
            ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt().nadd(form_Tirf_1042_S_Refund_Amt.getValue(pnd_C));                                               //Natural: ADD FORM.TIRF-1042-S-REFUND-AMT ( #C ) TO #RPT-1042S-S-REFUND-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().setValue(form_Tirf_Future_Use.getSubstring(20,1));                                                      //Natural: ASSIGN #RPT-1042S-FATCA-IND := SUBSTR ( TIRF-FUTURE-USE,20,1 )
    }
    //*  DST   &&&
    private void sub_Populate_4807c_Report_Fields() throws Exception                                                                                                      //Natural: POPULATE-4807C-REPORT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        //*  480.7C DISASTER CHANG
        //*  480.7C DISASTER CHANGES
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross().setValue(0);                                                                                                //Natural: MOVE 0 TO #RPT-4807C-GROSS #RPT-4807C-INT #RPT-4807C-TAXABLE #RPT-4807C-PR-GROSS-AMT-ROLL #RPT-4807C-PR-IVC-AMT-ROLL #RPT-4807C-FED-TAX-WTHLD #RPT-4807C-EXEMPT-AMT #RPT-4807C-AFTRTAX-CONT #RPT-4807C-DISASTER-TAX
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross().setValue(form_Tirf_Gross_Amt);                                                                              //Natural: MOVE FORM.TIRF-GROSS-AMT TO #RPT-4807C-GROSS
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int().setValue(form_Tirf_Int_Amt);                                                                                  //Natural: MOVE FORM.TIRF-INT-AMT TO #RPT-4807C-INT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable().setValue(form_Tirf_Taxable_Amt);                                                                          //Natural: MOVE FORM.TIRF-TAXABLE-AMT TO #RPT-4807C-TAXABLE
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll().setValue(form_Tirf_Pr_Gross_Amt_Roll);                                                          //Natural: MOVE FORM.TIRF-PR-GROSS-AMT-ROLL TO #RPT-4807C-PR-GROSS-AMT-ROLL
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll().setValue(form_Tirf_Pr_Ivc_Amt_Roll);                                                              //Natural: MOVE FORM.TIRF-PR-IVC-AMT-ROLL TO #RPT-4807C-PR-IVC-AMT-ROLL
        //* **                              /* 480.7C DISASTER CHANGES STARTS
        //* *MOVE FORM.TIRF-FED-TAX-WTHLD     TO #RPT-4807C-FED-TAX-WTHLD
        if (condition(form_Tirf_Tax_Year.greaterOrEqual("2019")))                                                                                                         //Natural: IF FORM.TIRF-TAX-YEAR GE '2019'
        {
            if (condition(form_Tirf_Distribution_Cde.equals("N")))                                                                                                        //Natural: IF FORM.TIRF-DISTRIBUTION-CDE EQ 'N'
            {
                if (condition(form_Tirf_Taxable_Amt.greater(getZero())))                                                                                                  //Natural: IF FORM.TIRF-TAXABLE-AMT > 0
                {
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable().reset();                                                                                      //Natural: RESET #RPT-4807C-TAXABLE #RPT-4807C-EXEMPT-AMT #RPT-4807C-DISASTER-TAX #RPT-4807C-AFTRTAX-CONT
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt().reset();
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax().reset();
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont().reset();
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax().compute(new ComputeParameters(false, ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax()),  //Natural: ASSIGN #RPT-4807C-DISASTER-TAX := FORM.TIRF-TAXABLE-AMT - #DIST-EXEMPT-AMT
                        form_Tirf_Taxable_Amt.subtract(pnd_Dist_Exempt_Amt));
                    if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax().lessOrEqual(getZero())))                                                   //Natural: IF #RPT-4807C-DISASTER-TAX LE 0
                    {
                        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt().setValue(form_Tirf_Taxable_Amt);                                                       //Natural: ASSIGN #RPT-4807C-EXEMPT-AMT := FORM.TIRF-TAXABLE-AMT
                        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax().setValue(0);                                                                         //Natural: ASSIGN #RPT-4807C-DISASTER-TAX := 0
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt().setValue(pnd_Dist_Exempt_Amt);                                                         //Natural: ASSIGN #RPT-4807C-EXEMPT-AMT := #DIST-EXEMPT-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont().setValue(form_Tirf_Ivc_Amt);                                                             //Natural: MOVE FORM.TIRF-IVC-AMT TO #RPT-4807C-AFTRTAX-CONT
                    ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld().setValue(form_Tirf_Fed_Tax_Wthld);                                                      //Natural: MOVE FORM.TIRF-FED-TAX-WTHLD TO #RPT-4807C-FED-TAX-WTHLD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **                              /* 480.7C DISASTER CHANGES ENDS
    }
    //*  DST   &&&
    private void sub_Populate_Nr4_Report_Fields() throws Exception                                                                                                        //Natural: POPULATE-NR4-REPORT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt().setValue(0);                                                                                              //Natural: MOVE 0 TO #RPT-NR4-GROSS-AMT #RPT-NR4-INTEREST-AMT #RPT-NR4-FED-TAX-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt().setValue(0);
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt().setValue(form_Tirf_Gross_Amt);                                                                            //Natural: MOVE TIRF-GROSS-AMT TO #RPT-NR4-GROSS-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt().setValue(form_Tirf_Int_Amt);                                                                           //Natural: MOVE TIRF-INT-AMT TO #RPT-NR4-INTEREST-AMT
        ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt().setValue(form_Tirf_Fed_Tax_Wthld);                                                                      //Natural: MOVE TIRF-FED-TAX-WTHLD TO #RPT-NR4-FED-TAX-AMT
    }
    private void sub_Check_Form_Type_10() throws Exception                                                                                                                //Natural: CHECK-FORM-TYPE-10
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Superde_01_Pnd_Sd1_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                    //Natural: ASSIGN #SUPERDE-01.#SD1-TAX-YEAR := #WS.#TAX-YEAR
        pnd_Superde_01_Pnd_Sd1_Tirf_Tin.setValue(form_Tirf_Tin);                                                                                                          //Natural: ASSIGN #SUPERDE-01.#SD1-TIRF-TIN := FORM.TIRF-TIN
        pnd_Superde_01_Pnd_Sd1_Form_Type.setValue(10);                                                                                                                    //Natural: ASSIGN #SUPERDE-01.#SD1-FORM-TYPE := 10
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Superde_01,17,1);                                                                                                 //Natural: MOVE LOW-VALUES TO SUBSTR ( #SUPERDE-01,17,1 )
        //*  THIS READ LOOP CHECKS THE PRESENCE OF AN ACTIVE SUNY PARTICIPANT. IF
        //*  FOUND THEN WE WILL SET THE FLAG AS TRUE AND SKIP THE 1099R PROCESSING.
        vw_form_10.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) FORM-10 BY TIRF-SUPERDE-1 FROM #SUPERDE-01
        (
        "READ_FORM_10",
        new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Superde_01.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") },
        1
        );
        READ_FORM_10:
        while (condition(vw_form_10.readNextRow("READ_FORM_10")))
        {
            if (condition(form_10_Tirf_Tin.equals(form_Tirf_Tin) && form_10_Tirf_Form_Type.equals(10) && form_10_Tirf_Active_Ind.equals("A")))                            //Natural: IF FORM-10.TIRF-TIN = FORM.TIRF-TIN AND FORM-10.TIRF-FORM-TYPE = 10 AND FORM-10.TIRF-ACTIVE-IND = 'A'
            {
                pnd_Form_Type_1_10_Found.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #FORM-TYPE-1-10-FOUND
                pnd_Superde_01.reset();                                                                                                                                   //Natural: RESET #SUPERDE-01
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   CHECK-FORM-TYPE-10      /* MUKHERR 010515  >>
    }
    //*  &&&
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Save_Date_N.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #WS-SAVE-DATE-N := *DATN
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                         //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_Form_Type);                                                                                        //Natural: ASSIGN #TWRATBL4.#FORM-IND := #WS.#FORM-TYPE
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Mass_Mail_Dte().setValue(pnd_Ws_Save_Date_N);                                                                             //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-MASS-MAIL-DTE := #WS-SAVE-DATE-N
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Process_Form_Update() throws Exception                                                                                                               //Natural: PROCESS-FORM-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().setValue(vw_form.getAstISN("READ_FORM"));                                                                //Natural: ASSIGN #TWRA0214-ISN := *ISN ( READ-FORM. )
        GET:                                                                                                                                                              //Natural: GET FORM-UPD #TWRA0214-ISN
        vw_form_Upd.readByID(pdaTwra0214.getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn().getLong(), "GET");
        pnd_Ws1_Pnd_Read_Cnt1.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #READ-CNT1
        form_Upd_Tirf_Mobius_Stat.setValue(pnd_Ws_Pnd_Mobius_Status);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT := #MOBIUS-STATUS
        form_Upd_Tirf_Mobius_Stat_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                     //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-STAT-DATE := #SYS-DATE
        if (condition(pnd_Ws_Pnd_Mobius_Status.equals("A1")))                                                                                                             //Natural: IF #MOBIUS-STATUS EQ 'A1'
        {
            form_Upd_Tirf_Part_Rpt_Date.setValue(pnd_Ws_Pnd_Sys_Date);                                                                                                    //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-DATE := #SYS-DATE
        }                                                                                                                                                                 //Natural: END-IF
        form_Upd_Tirf_Mobius_Ind.setValue(" ");                                                                                                                           //Natural: ASSIGN FORM-UPD.TIRF-MOBIUS-IND := ' '
        form_Upd_Tirf_Part_Rpt_Ind.setValue("M");                                                                                                                         //Natural: ASSIGN FORM-UPD.TIRF-PART-RPT-IND := 'M'
        vw_form_Upd.updateDBRow("GET");                                                                                                                                   //Natural: UPDATE ( GET. )
        pnd_Ws1_Pnd_Et_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Ws1_Pnd_Et_Cnt.greaterOrEqual(et_Limit)))                                                                                                       //Natural: IF #ET-CNT GE ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Ws1_Pnd_Et_Cnt.reset();                                                                                                                                   //Natural: RESET #ET-CNT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws1_Pnd_Update_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #UPDATE-CNT
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Extract_Control_Report() throws Exception                                                                                                      //Natural: WRITE-EXTRACT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wkfle_Recd_Cnt.nadd(1);                                                                                                                                       //Natural: ASSIGN #WKFLE-RECD-CNT := #WKFLE-RECD-CNT + 1
        short decideConditionsMet2456 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-COMP-CDE = 'T'
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().equals("T")))
        {
            decideConditionsMet2456++;
                                                                                                                                                                          //Natural: PERFORM TIAA
            sub_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #RPT-COMP-CDE = 'X'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde().equals("X")))
        {
            decideConditionsMet2456++;
                                                                                                                                                                          //Natural: PERFORM TRUST
            sub_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"INVALID COMPANY CODE AT ROW NUMBER:",pnd_Wkfle_Recd_Cnt);                                                         //Natural: WRITE 'INVALID COMPANY CODE AT ROW NUMBER:' #WKFLE-RECD-CNT
            if (Global.isEscape()) return;
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year());                                                                   //Natural: ASSIGN #WS-TAX-YEAR-A := #RPT-TAX-YEAR
        pnd_Hold_Rpt_Tin.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin());                                                                                         //Natural: ASSIGN #HOLD-RPT-TIN := #RPT-TIN
        pnd_Total_Unq_Tin.compute(new ComputeParameters(false, pnd_Total_Unq_Tin), pnd_Unq_Tin_B.add(pnd_Unq_Tin_A));                                                     //Natural: ASSIGN #TOTAL-UNQ-TIN := #UNQ-TIN-B + #UNQ-TIN-A
        pnd_P1_Total_Unq_Tin.compute(new ComputeParameters(false, pnd_P1_Total_Unq_Tin), pnd_P1_Unq_Tin_B.add(pnd_P1_Unq_Tin_A));                                         //Natural: ASSIGN #P1-TOTAL-UNQ-TIN := #P1-UNQ-TIN-B + #P1-UNQ-TIN-A
        pnd_P2_Total_Unq_Tin.compute(new ComputeParameters(false, pnd_P2_Total_Unq_Tin), pnd_P2_Unq_Tin_B.add(pnd_P2_Unq_Tin_A));                                         //Natural: ASSIGN #P2-TOTAL-UNQ-TIN := #P2-UNQ-TIN-B + #P2-UNQ-TIN-A
    }
    private void sub_Tiaa() throws Exception                                                                                                                              //Natural: TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS-TIAA
        sub_Db_Read_Process_Tiaa();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))                                                                        //Natural: IF #RPT-BYPASS-ACCPT-IND NE 'A1'
        {
                                                                                                                                                                          //Natural: PERFORM BYPASSED-PROCESS-TIAA
            sub_Bypassed_Process_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Message1.setValue("Bypassed");                                                                                                                            //Natural: ASSIGN #MESSAGE1 := 'Bypassed'
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-REPORT
            sub_Print_Detail_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCEPTED-PROCESS-TIAA
            sub_Accepted_Process_Tiaa();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Message1.setValue("Normal Completion");                                                                                                                   //Natural: ASSIGN #MESSAGE1 := 'Normal Completion'
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-REPORT
            sub_Print_Detail_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Trust() throws Exception                                                                                                                             //Natural: TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS-TRUST
        sub_Db_Read_Process_Trust();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().notEquals("A1")))                                                                        //Natural: IF #RPT-BYPASS-ACCPT-IND NE 'A1'
        {
                                                                                                                                                                          //Natural: PERFORM BYPASSED-PROCESS-TRUST
            sub_Bypassed_Process_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Message1.setValue("Bypassed");                                                                                                                            //Natural: ASSIGN #MESSAGE1 := 'Bypassed'
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-REPORT
            sub_Print_Detail_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCEPTED-PROCESS-TRUST
            sub_Accepted_Process_Trust();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Message1.setValue("Normal Completion");                                                                                                                   //Natural: ASSIGN #MESSAGE1 := 'Normal Completion'
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-REPORT
            sub_Print_Detail_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Db_Read_Process_Tiaa() throws Exception                                                                                                              //Natural: DB-READ-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TIAA-1099-R
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                                    //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                                    //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                            //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                            //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                          //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                            //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                                        //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_R_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                            //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TIAA-1099-I
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                                    //Natural: ADD #RPT-1099I-GROSS-AMT TO #TIAA-1099-I-GROSS-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                                    //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TIAA-1099-I-TAXABLE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                                            //Natural: ADD #RPT-1099I-IVC-AMT TO #TIAA-1099-I-IVC
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                                            //Natural: ADD #RPT-1099I-FED-TAX TO #TIAA-1099-I-FED
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                                          //Natural: ADD #RPT-1099I-LCL-TAX TO #TIAA-1099-I-LOCAL
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                                            //Natural: ADD #RPT-1099I-INT-AMT TO #TIAA-1099-I-INT
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                                        //Natural: ADD #RPT-1099I-STATE-TAX TO #TIAA-1099-I-STATE
            pnd_Tiaa_Totals_Pnd_Tiaa_1099_I_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                                            //Natural: ADD #RPT-1099I-IRR-AMT TO #TIAA-1099-I-IRR
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TIAA-1042S
            pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                                            //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TIAA-1042S-GROSS-AMT
            pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Refund.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                               //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TIAA-1042S-REFUND
            pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                                      //Natural: ADD #RPT-1042S-INTEREST TO #TIAA-1042S-INT
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Fatca.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                       //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-FATCA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Totals_1042s_Pnd_Tiaa_1042s_Tax_Non_Fatca.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                   //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-NON-FATCA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            pnd_Db_Read_Fields_Pnd_Cnt_Db.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CNT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                  //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                    //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-DB
            pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                      //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-DB
            pnd_Db_Read_Fields_Pnd_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                            //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                  //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                            //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-DB
            pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                              //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-DB
            //*  SAIK1
            pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                      //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-DB
            if (condition(pnd_P2_Indicator.equals("Y")))                                                                                                                  //Natural: IF #P2-INDICATOR = 'Y'
            {
                pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db.nadd(1);                                                                                                              //Natural: ADD 1 TO #P2-CNT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P2-TRAD-IRA-CONTRIB-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P2-SEP-AMT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P2-TRAD-IRA-ROLLOVER-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P2-IRA-RECHAR-AMT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P2-ACCOUNT-FMV-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P2-ROTH-IRA-CONTRIB-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P2-ROTH-IRA-CONVERSION-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P2-POSTPN-AMT-DB
                //*  SAIK1
                pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P2-REPAYMENTS-AMT-DB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db.nadd(1);                                                                                                              //Natural: ADD 1 TO #P1-CNT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P1-TRAD-IRA-CONTRIB-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P1-SEP-AMT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P1-TRAD-IRA-ROLLOVER-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P1-IRA-RECHAR-AMT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P1-ACCOUNT-FMV-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P1-ROTH-IRA-CONTRIB-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P1-ROTH-IRA-CONVERSION-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P1-POSTPN-AMT-DB
                //*  SAIK1
                pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P1-REPAYMENTS-AMT-DB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-4807C
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                                         //Natural: ADD #RPT-4807C-GROSS TO #TIAA-4807C-GROSS-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                                 //Natural: ADD #RPT-4807C-INT TO #TIAA-4807C-INT
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                                         //Natural: ADD #RPT-4807C-TAXABLE TO #TIAA-4807C-TAXABLE
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                                       //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TIAA-4807C-FED
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Dist.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                                             //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TIAA-4807C-ROLL-DIST
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Roll_Cont.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                               //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TIAA-4807C-ROLL-CONT
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Exempt_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                                   //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TIAA-4807C-EXEMPT-AMT
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Disaster_Tax.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                               //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TIAA-4807C-DISASTER-TAX
            pnd_Tiaa_Totals_Pnd_Tiaa_4807c_Aftrtax_Cont.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                               //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TIAA-4807C-AFTRTAX-CONT
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-NR4
            pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                                     //Natural: ADD #RPT-NR4-GROSS-AMT TO #TIAA-NR4-GROSS-AMT
            pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                                        //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TIAA-NR4-INT
            pnd_Tiaa_Totals_Nr4_Pnd_Tiaa_Nr4_Fed_Tax.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                                     //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TIAA-NR4-FED-TAX
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypassed_Process_Tiaa() throws Exception                                                                                                             //Natural: BYPASSED-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_E_B.nadd(1);                                                                                                          //Natural: ADD 1 TO #TIAA-1099-R-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                        //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                        //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Ivc_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Local_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                              //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_State_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                            //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_R_Irr_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR-E-B
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_E_B.nadd(1);                                                                                                          //Natural: ADD 1 TO #TIAA-1099-I-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                        //Natural: ADD #RPT-1099I-GROSS-AMT TO #TIAA-1099-I-GROSS-AMT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                        //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TIAA-1099-I-TAXABLE-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Ivc_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                                //Natural: ADD #RPT-1099I-IVC-AMT TO #TIAA-1099-I-IVC-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                                //Natural: ADD #RPT-1099I-FED-TAX TO #TIAA-1099-I-FED-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Local_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                              //Natural: ADD #RPT-1099I-LCL-TAX TO #TIAA-1099-I-LOCAL-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                                //Natural: ADD #RPT-1099I-INT-AMT TO #TIAA-1099-I-INT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_State_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                            //Natural: ADD #RPT-1099I-STATE-TAX TO #TIAA-1099-I-STATE-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_1099_I_Irr_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                                //Natural: ADD #RPT-1099I-IRR-AMT TO #TIAA-1099-I-IRR-E-B
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_E_B.nadd(1);                                                                                                     //Natural: ADD 1 TO #TIAA-1042S-E-B
            pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                                //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TIAA-1042S-GROSS-AMT-E-B
            pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Refund_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                   //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TIAA-1042S-REFUND-E-B
            pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                          //Natural: ADD #RPT-1042S-INTEREST TO #TIAA-1042S-INT-E-B
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                           //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-FATCA-E-B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Email_Bypassed_1042s_Pnd_Tiaa_1042s_Tax_Non_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                             //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-NON-E-B
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                     //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
            {
                pnd_Unq_Tin_B.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #UNQ-TIN-B
            }                                                                                                                                                             //Natural: END-IF
            bypassed_Fields_Pnd_Cnt_B.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-B
            bypassed_Fields_Pnd_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                      //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-B
            bypassed_Fields_Pnd_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                        //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-B
            bypassed_Fields_Pnd_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                    //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-B
            bypassed_Fields_Pnd_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                          //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-B
            bypassed_Fields_Pnd_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                                //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-B
            bypassed_Fields_Pnd_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                      //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-B
            bypassed_Fields_Pnd_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                                //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-B
            bypassed_Fields_Pnd_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                                  //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-B
            //*  SAIK1
            bypassed_Fields_Pnd_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                          //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-B
            if (condition(pnd_P2_Indicator.equals("Y")))                                                                                                                  //Natural: IF #P2-INDICATOR = 'Y'
            {
                if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                 //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
                {
                    pnd_P2_Unq_Tin_B.nadd(1);                                                                                                                             //Natural: ADD 1 TO #P2-UNQ-TIN-B
                }                                                                                                                                                         //Natural: END-IF
                pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B.nadd(1);                                                                                                              //Natural: ADD 1 TO #P2-CNT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P2-TRAD-IRA-CONTRIB-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P2-SEP-AMT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P2-TRAD-IRA-ROLLOVER-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P2-IRA-RECHAR-AMT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P2-ACCOUNT-FMV-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P2-ROTH-IRA-CONTRIB-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P2-ROTH-IRA-CONVERSION-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P2-POSTPN-AMT-B
                //*  SAIK1
                pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P2-REPAYMENTS-AMT-B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                 //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
                {
                    pnd_P1_Unq_Tin_B.nadd(1);                                                                                                                             //Natural: ADD 1 TO #P1-UNQ-TIN-B
                }                                                                                                                                                         //Natural: END-IF
                pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B.nadd(1);                                                                                                              //Natural: ADD 1 TO #P1-CNT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P1-TRAD-IRA-CONTRIB-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P1-SEP-AMT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P1-TRAD-IRA-ROLLOVER-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P1-IRA-RECHAR-AMT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P1-ACCOUNT-FMV-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P1-ROTH-IRA-CONTRIB-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P1-ROTH-IRA-CONVERSION-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P1-POSTPN-AMT-B
                //*  SAIK1
                pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P1-REPAYMENTS-AMT-B
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_E_B.nadd(1);                                                                                                           //Natural: ADD 1 TO #TIAA-4807C-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                             //Natural: ADD #RPT-4807C-GROSS TO #TIAA-4807C-GROSS-AMT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                             //Natural: ADD #RPT-4807C-TAXABLE TO #TIAA-4807C-TAXABLE-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                     //Natural: ADD #RPT-4807C-INT TO #TIAA-4807C-INT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                           //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TIAA-4807C-FED-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Dist_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                                 //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TIAA-4807C-ROLL-DIST-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Roll_Cont_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                   //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TIAA-4807C-ROLL-CONT-E-B
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Exempt_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                       //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TIAA-4807C-EXEMPT-AMT-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Disaster_Tax_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                   //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TIAA-4807C-DISASTER-TAX-E-B
            pnd_Tiaa_Email_Bypassed_Pnd_Tiaa_4807c_Aftrtax_Cont_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                   //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TIAA-4807C-AFTRTAX-CONT-E-B
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_E_B.nadd(1);                                                                                                         //Natural: ADD 1 TO #TIAA-NR4-E-B
            pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                         //Natural: ADD #RPT-NR4-GROSS-AMT TO #TIAA-NR4-GROSS-AMT-E-B
            pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                            //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TIAA-NR4-INT-E-B
            pnd_Tiaa_Email_Bypassed_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                         //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TIAA-NR4-FED-TAX-E-B
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accepted_Process_Tiaa() throws Exception                                                                                                             //Natural: ACCEPTED-PROCESS-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_E_A.nadd(1);                                                                                                          //Natural: ADD 1 TO #TIAA-1099-R-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                        //Natural: ADD #RPT-1099R-GROSS-AMT TO #TIAA-1099-R-GROSS-AMT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                        //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TIAA-1099-R-TAXABLE-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Ivc_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                //Natural: ADD #RPT-1099R-IVC-AMT TO #TIAA-1099-R-IVC-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                //Natural: ADD #RPT-1099R-FED-TAX TO #TIAA-1099-R-FED-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Local_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                              //Natural: ADD #RPT-1099R-LCL-TAX TO #TIAA-1099-R-LOCAL-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                //Natural: ADD #RPT-1099R-INT-AMT TO #TIAA-1099-R-INT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_State_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                            //Natural: ADD #RPT-1099R-STATE-TAX TO #TIAA-1099-R-STATE-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_R_Irr_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                //Natural: ADD #RPT-1099R-IRR-AMT TO #TIAA-1099-R-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_E_A.nadd(1);                                                                                                          //Natural: ADD 1 TO #TIAA-1099-I-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                        //Natural: ADD #RPT-1099I-GROSS-AMT TO #TIAA-1099-I-GROSS-AMT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                        //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TIAA-1099-I-TAXABLE-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Ivc_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                                //Natural: ADD #RPT-1099I-IVC-AMT TO #TIAA-1099-I-IVC-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                                //Natural: ADD #RPT-1099I-FED-TAX TO #TIAA-1099-I-FED-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                                //Natural: ADD #RPT-1099I-FED-TAX TO #TIAA-1099-I-FED-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Local_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                              //Natural: ADD #RPT-1099I-LCL-TAX TO #TIAA-1099-I-LOCAL-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                                //Natural: ADD #RPT-1099I-INT-AMT TO #TIAA-1099-I-INT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_State_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                            //Natural: ADD #RPT-1099I-STATE-TAX TO #TIAA-1099-I-STATE-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_1099_I_Irr_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                                //Natural: ADD #RPT-1099I-IRR-AMT TO #TIAA-1099-I-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_E_A.nadd(1);                                                                                                     //Natural: ADD 1 TO #TIAA-1042S-E-A
            pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                                //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TIAA-1042S-GROSS-AMT-E-A
            pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Refund_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                   //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TIAA-1042S-REFUND-E-A
            pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                          //Natural: ADD #RPT-1042S-INTEREST TO #TIAA-1042S-INT-E-A
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Fatca_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                           //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-FATCA-E-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Email_Accepted_1042s_Pnd_Tiaa_1042s_Tax_Non_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                             //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TIAA-1042S-TAX-NON-E-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                     //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
            {
                pnd_Unq_Tin_A.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #UNQ-TIN-A
            }                                                                                                                                                             //Natural: END-IF
            accepted_Fields_Pnd_Cnt_A.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-A
            accepted_Fields_Pnd_Trad_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                      //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-A
            accepted_Fields_Pnd_Sep_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                        //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-A
            accepted_Fields_Pnd_Trad_Ira_Rollover_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                    //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-A
            accepted_Fields_Pnd_Ira_Rechar_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                          //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-A
            accepted_Fields_Pnd_Account_Fmv_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                                //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-A
            accepted_Fields_Pnd_Roth_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                      //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-A
            accepted_Fields_Pnd_Roth_Ira_Conversion_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                                //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-A
            accepted_Fields_Pnd_Postpn_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                                  //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-A
            //*  SAIK1
            accepted_Fields_Pnd_Repayments_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                          //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-A
            if (condition(pnd_P2_Indicator.equals("Y")))                                                                                                                  //Natural: IF #P2-INDICATOR = 'Y'
            {
                if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                 //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
                {
                    pnd_P2_Unq_Tin_A.nadd(1);                                                                                                                             //Natural: ADD 1 TO #P2-UNQ-TIN-A
                }                                                                                                                                                         //Natural: END-IF
                pnd_P2_Accepted_Fields_Pnd_P2_Cnt_A.nadd(1);                                                                                                              //Natural: ADD 1 TO #P2-CNT-A
                pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P2-TRAD-IRA-CONTRIB-A
                pnd_P2_Accepted_Fields_Pnd_P2_Sep_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P2-SEP-AMT-A
                pnd_P2_Accepted_Fields_Pnd_P2_Trad_Ira_Rollover_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P2-TRAD-IRA-ROLLOVER-A
                pnd_P2_Accepted_Fields_Pnd_P2_Ira_Rechar_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P2-IRA-RECHAR-AMT-A
                pnd_P2_Accepted_Fields_Pnd_P2_Account_Fmv_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P2-ACCOUNT-FMV-A
                pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P2-ROTH-IRA-CONTRIB-A
                pnd_P2_Accepted_Fields_Pnd_P2_Roth_Ira_Conversion_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P2-ROTH-IRA-CONVERSION-A
                pnd_P2_Accepted_Fields_Pnd_P2_Postpn_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P2-POSTPN-AMT-A
                //*  SAIK1
                pnd_P2_Accepted_Fields_Pnd_P2_Repayments_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P2-REPAYMENTS-AMT-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin().notEquals(pnd_Hold_Rpt_Tin)))                                                                 //Natural: IF #RPT-TIN NE #HOLD-RPT-TIN
                {
                    pnd_P1_Unq_Tin_A.nadd(1);                                                                                                                             //Natural: ADD 1 TO #P1-UNQ-TIN-A
                }                                                                                                                                                         //Natural: END-IF
                pnd_P1_Accepted_Fields_Pnd_P1_Cnt_A.nadd(1);                                                                                                              //Natural: ADD 1 TO #P1-CNT-A
                pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P1-TRAD-IRA-CONTRIB-A
                pnd_P1_Accepted_Fields_Pnd_P1_Sep_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P1-SEP-AMT-A
                pnd_P1_Accepted_Fields_Pnd_P1_Trad_Ira_Rollover_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P1-TRAD-IRA-ROLLOVER-A
                pnd_P1_Accepted_Fields_Pnd_P1_Ira_Rechar_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P1-IRA-RECHAR-AMT-A
                pnd_P1_Accepted_Fields_Pnd_P1_Account_Fmv_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P1-ACCOUNT-FMV-A
                pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P1-ROTH-IRA-CONTRIB-A
                pnd_P1_Accepted_Fields_Pnd_P1_Roth_Ira_Conversion_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P1-ROTH-IRA-CONVERSION-A
                pnd_P1_Accepted_Fields_Pnd_P1_Postpn_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P1-POSTPN-AMT-A
                //*  SAIK1
                pnd_P1_Accepted_Fields_Pnd_P1_Repayments_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P1-REPAYMENTS-AMT-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_E_A.nadd(1);                                                                                                           //Natural: ADD 1 TO #TIAA-4807C-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                             //Natural: ADD #RPT-4807C-GROSS TO #TIAA-4807C-GROSS-AMT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                             //Natural: ADD #RPT-4807C-TAXABLE TO #TIAA-4807C-TAXABLE-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                     //Natural: ADD #RPT-4807C-INT TO #TIAA-4807C-INT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                           //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TIAA-4807C-FED-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Dist_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                                 //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TIAA-4807C-ROLL-DIST-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Roll_Cont_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                   //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TIAA-4807C-ROLL-CONT-E-A
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Exempt_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                       //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TIAA-4807C-EXEMPT-AMT-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Disaster_Tax_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                   //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TIAA-4807C-DISASTER-TAX-E-A
            pnd_Tiaa_Email_Accepted_Pnd_Tiaa_4807c_Aftrtax_Cont_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                   //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TIAA-4807C-AFTRTAX-CONT-E-A
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_E_A.nadd(1);                                                                                                         //Natural: ADD 1 TO #TIAA-NR4-E-A
            pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                         //Natural: ADD #RPT-NR4-GROSS-AMT TO #TIAA-NR4-GROSS-AMT-E-A
            pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                            //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TIAA-NR4-INT-E-A
            pnd_Tiaa_Email_Accepted_Nr4_Pnd_Tiaa_Nr4_Fed_Tax_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                         //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TIAA-NR4-FED-TAX-E-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Db_Read_Process_Trust() throws Exception                                                                                                             //Natural: DB-READ-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Totals_Pnd_Trust_1099_R.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TRUST-1099-R
            pnd_Trust_Totals_Pnd_Trust_1099_R_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                                  //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT
            pnd_Trust_Totals_Pnd_Trust_1099_R_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                                  //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE
            pnd_Trust_Totals_Pnd_Trust_1099_R_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                                          //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC
            pnd_Trust_Totals_Pnd_Trust_1099_R_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                                          //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED
            pnd_Trust_Totals_Pnd_Trust_1099_R_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                                        //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL
            pnd_Trust_Totals_Pnd_Trust_1099_R_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                                          //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT
            pnd_Trust_Totals_Pnd_Trust_1099_R_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                                      //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE
            pnd_Trust_Totals_Pnd_Trust_1099_R_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                                          //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Trust_Totals_Pnd_Trust_1099_I.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TRUST-1099-I
            pnd_Trust_Totals_Pnd_Trust_1099_I_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                                  //Natural: ADD #RPT-1099I-GROSS-AMT TO #TRUST-1099-I-GROSS-AMT
            pnd_Trust_Totals_Pnd_Trust_1099_I_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                                  //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TRUST-1099-I-TAXABLE
            pnd_Trust_Totals_Pnd_Trust_1099_I_Ivc.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                                          //Natural: ADD #RPT-1099I-IVC-AMT TO #TRUST-1099-I-IVC
            pnd_Trust_Totals_Pnd_Trust_1099_I_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                                          //Natural: ADD #RPT-1099I-FED-TAX TO #TRUST-1099-I-FED
            pnd_Trust_Totals_Pnd_Trust_1099_I_Local.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                                        //Natural: ADD #RPT-1099I-LCL-TAX TO #TRUST-1099-I-LOCAL
            pnd_Trust_Totals_Pnd_Trust_1099_I_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                                          //Natural: ADD #RPT-1099I-INT-AMT TO #TRUST-1099-I-INT
            pnd_Trust_Totals_Pnd_Trust_1099_I_State.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                                      //Natural: ADD #RPT-1099I-STATE-TAX TO #TRUST-1099-I-STATE
            pnd_Trust_Totals_Pnd_Trust_1099_I_Irr.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                                          //Natural: ADD #RPT-1099I-IRR-AMT TO #TRUST-1099-I-IRR
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Trust_Totals_1042s_Pnd_Trust_1042s.nadd(1);                                                                                                               //Natural: ADD 1 TO #TRUST-1042S
            pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                                          //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TRUST-1042S-GROSS-AMT
            pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Refund.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                             //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TRUST-1042S-REFUND
            pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                                    //Natural: ADD #RPT-1042S-INTEREST TO #TRUST-1042S-INT
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Fatca.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                     //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-FATCA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trust_Totals_1042s_Pnd_Trust_1042s_Tax_Non_Fatca.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                 //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-NON-FATCA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            pnd_Db_Read_Fields_Pnd_Cnt_Db.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CNT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                  //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                    //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-DB
            pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                      //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-DB
            pnd_Db_Read_Fields_Pnd_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                            //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                  //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                            //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-DB
            pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                              //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-DB
            //*  SAIK1
            pnd_Db_Read_Fields_Pnd_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                      //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-DB
            if (condition(pnd_P2_Indicator.equals("Y")))                                                                                                                  //Natural: IF #P2-INDICATOR = 'Y'
            {
                pnd_P2_Db_Read_Fields_Pnd_P2_Cnt_Db.nadd(1);                                                                                                              //Natural: ADD 1 TO #P2-CNT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P2-TRAD-IRA-CONTRIB-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P2-SEP-AMT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P2-TRAD-IRA-ROLLOVER-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P2-IRA-RECHAR-AMT-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P2-ACCOUNT-FMV-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P2-ROTH-IRA-CONTRIB-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P2-ROTH-IRA-CONVERSION-DB
                pnd_P2_Db_Read_Fields_Pnd_P2_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P2-POSTPN-AMT-DB
                //*  SAIK1
                pnd_P2_Db_Read_Fields_Pnd_P2_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P2-REPAYMENTS-AMT-DB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P1_Db_Read_Fields_Pnd_P1_Cnt_Db.nadd(1);                                                                                                              //Natural: ADD 1 TO #P1-CNT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P1-TRAD-IRA-CONTRIB-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Sep_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P1-SEP-AMT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Trad_Ira_Rollover_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P1-TRAD-IRA-ROLLOVER-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Ira_Rechar_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P1-IRA-RECHAR-AMT-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Account_Fmv_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P1-ACCOUNT-FMV-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Contrib_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P1-ROTH-IRA-CONTRIB-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Roth_Ira_Conversion_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P1-ROTH-IRA-CONVERSION-DB
                pnd_P1_Db_Read_Fields_Pnd_P1_Postpn_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P1-POSTPN-AMT-DB
                //*  SAIK1
                pnd_P1_Db_Read_Fields_Pnd_P1_Repayments_Amt_Db.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P1-REPAYMENTS-AMT-DB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Trust_Totals_Pnd_Trust_4807c.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TRUST-4807C
            pnd_Trust_Totals_Pnd_Trust_4807c_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                                       //Natural: ADD #RPT-4807C-GROSS TO #TRUST-4807C-GROSS-AMT
            pnd_Trust_Totals_Pnd_Trust_4807c_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                               //Natural: ADD #RPT-4807C-INT TO #TRUST-4807C-INT
            pnd_Trust_Totals_Pnd_Trust_4807c_Taxable.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                                       //Natural: ADD #RPT-4807C-TAXABLE TO #TRUST-4807C-TAXABLE
            pnd_Trust_Totals_Pnd_Trust_4807c_Fed.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                                     //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TRUST-4807C-FED
            pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Dist.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                                           //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TRUST-4807C-ROLL-DIST
            pnd_Trust_Totals_Pnd_Trust_4807c_Roll_Cont.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                             //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TRUST-4807C-ROLL-CONT
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Trust_Totals_Pnd_Trust_4807c_Exempt_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                                 //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TRUST-4807C-EXEMPT-AMT
            pnd_Trust_Totals_Pnd_Trust_4807c_Disaster_Tax.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                             //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TRUST-4807C-DISASTER-TAX
            pnd_Trust_Totals_Pnd_Trust_4807c_Aftrtax_Cont.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                             //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TRUST-4807C-AFTRTAX-CONT
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TRUST-NR4
            pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Gross_Amt.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                                   //Natural: ADD #RPT-NR4-GROSS-AMT TO #TRUST-NR4-GROSS-AMT
            pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Int.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                                      //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TRUST-NR4-INT
            pnd_Trust_Totals_Nr4_Pnd_Trust_Nr4_Fed_Tax.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                                   //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TRUST-NR4-FED-TAX
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bypassed_Process_Trust() throws Exception                                                                                                            //Natural: BYPASSED-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_E_B.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRUST-1099-R-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                      //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                      //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Ivc_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                              //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                              //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Local_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                            //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                              //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_State_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                          //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_R_Irr_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                              //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR-E-B
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_E_B.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRUST-1099-I-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                      //Natural: ADD #RPT-1099I-GROSS-AMT TO #TRUST-1099-I-GROSS-AMT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                      //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TRUST-1099-I-TAXABLE-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Ivc_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                              //Natural: ADD #RPT-1099I-IVC-AMT TO #TRUST-1099-I-IVC-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                              //Natural: ADD #RPT-1099I-FED-TAX TO #TRUST-1099-I-FED-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Local_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                            //Natural: ADD #RPT-1099I-LCL-TAX TO #TRUST-1099-I-LOCAL-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                              //Natural: ADD #RPT-1099I-INT-AMT TO #TRUST-1099-I-INT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_State_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                          //Natural: ADD #RPT-1099I-STATE-TAX TO #TRUST-1099-I-STATE-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_1099_I_Irr_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                              //Natural: ADD #RPT-1099I-IRR-AMT TO #TRUST-1099-I-IRR-E-B
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_E_B.nadd(1);                                                                                                   //Natural: ADD 1 TO #TRUST-1042S-E-B
            pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                              //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TRUST-1042S-GROSS-AMT-E-B
            pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Refund_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                 //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TRUST-1042S-REFUND-E-B
            pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                        //Natural: ADD #RPT-1042S-INTEREST TO #TRUST-1042S-INT-E-B
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Fatca_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                         //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-FATCA-E-B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trust_Email_Bypassed_1042s_Pnd_Trust_1042s_Tax_Non_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                           //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-NON-E-B
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            bypassed_Fields_Pnd_Cnt_B.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-B
            bypassed_Fields_Pnd_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                      //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-B
            bypassed_Fields_Pnd_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                        //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-B
            bypassed_Fields_Pnd_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                    //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-B
            bypassed_Fields_Pnd_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                          //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-B
            bypassed_Fields_Pnd_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                                //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-B
            bypassed_Fields_Pnd_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                      //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-B
            bypassed_Fields_Pnd_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                                //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-B
            bypassed_Fields_Pnd_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                                  //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-B
            //*  SAIK1
            bypassed_Fields_Pnd_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                          //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-B
            //* *
            if (condition(pnd_P2_Indicator.equals("Y")))                                                                                                                  //Natural: IF #P2-INDICATOR = 'Y'
            {
                pnd_P2_Bypassed_Fields_Pnd_P2_Cnt_B.nadd(1);                                                                                                              //Natural: ADD 1 TO #P2-CNT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P2-TRAD-IRA-CONTRIB-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P2-SEP-AMT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P2-TRAD-IRA-ROLLOVER-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P2-IRA-RECHAR-AMT-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P2-ACCOUNT-FMV-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P2-ROTH-IRA-CONTRIB-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P2-ROTH-IRA-CONVERSION-B
                pnd_P2_Bypassed_Fields_Pnd_P2_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P2-POSTPN-AMT-B
                //*  SAIK1
                pnd_P2_Bypassed_Fields_Pnd_P2_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P2-REPAYMENTS-AMT-B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P1_Bypassed_Fields_Pnd_P1_Cnt_B.nadd(1);                                                                                                              //Natural: ADD 1 TO #P1-CNT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                        //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #P1-TRAD-IRA-CONTRIB-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Sep_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                          //Natural: ADD #RPT-SEP-AMT TO #P1-SEP-AMT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Trad_Ira_Rollover_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                      //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #P1-TRAD-IRA-ROLLOVER-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Ira_Rechar_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                            //Natural: ADD #RPT-IRA-RECHAR-AMT TO #P1-IRA-RECHAR-AMT-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Account_Fmv_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                  //Natural: ADD #RPT-ACCOUNT-FMV TO #P1-ACCOUNT-FMV-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Contrib_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                        //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #P1-ROTH-IRA-CONTRIB-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Roth_Ira_Conversion_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                  //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #P1-ROTH-IRA-CONVERSION-B
                pnd_P1_Bypassed_Fields_Pnd_P1_Postpn_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                    //Natural: ADD #RPT-POSTPN-AMT TO #P1-POSTPN-AMT-B
                //*  SAIK1
                pnd_P1_Bypassed_Fields_Pnd_P1_Repayments_Amt_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                            //Natural: ADD #RPT-REPAYMENTS-AMT TO #P1-REPAYMENTS-AMT-B
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_E_B.nadd(1);                                                                                                         //Natural: ADD 1 TO #TRUST-4807C-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                           //Natural: ADD #RPT-4807C-GROSS TO #TRUST-4807C-GROSS-AMT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Taxable_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                           //Natural: ADD #RPT-4807C-TAXABLE TO #TRUST-4807C-TAXABLE-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                   //Natural: ADD #RPT-4807C-INT TO #TRUST-4807C-INT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Fed_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                         //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TRUST-4807C-FED-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Dist_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                               //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TRUST-4807C-ROLL-DIST-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Roll_Cont_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                 //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TRUST-4807C-ROLL-CONT-E-B
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Exempt_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                     //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TRUST-4807C-EXEMPT-AMT-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Disaster_Tax_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                 //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TRUST-4807C-DISASTER-TAX-E-B
            pnd_Trust_Email_Bypassed_Pnd_Trust_4807c_Aftrtax_Cont_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                 //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TRUST-4807C-AFTRTAX-CONT-E-B
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_E_B.nadd(1);                                                                                                       //Natural: ADD 1 TO #TRUST-NR4-E-B
            pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                       //Natural: ADD #RPT-NR4-GROSS-AMT TO #TRUST-NR4-GROSS-AMT-E-B
            pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Int_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                          //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TRUST-NR4-INT-E-B
            pnd_Trust_Email_Bypassed_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_B.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                       //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TRUST-NR4-FED-TAX-E-B
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accepted_Process_Trust() throws Exception                                                                                                            //Natural: ACCEPTED-PROCESS-TRUST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  1099-R
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 1
        {
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Gross_Amt());                                      //Natural: ADD #RPT-1099R-GROSS-AMT TO #TRUST-1099-R-GROSS-AMT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Taxable_Amt());                                      //Natural: ADD #RPT-1099R-TAXABLE-AMT TO #TRUST-1099-R-TAXABLE-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Ivc_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Ivc_Amt());                                              //Natural: ADD #RPT-1099R-IVC-AMT TO #TRUST-1099-R-IVC-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Fed_Tax());                                              //Natural: ADD #RPT-1099R-FED-TAX TO #TRUST-1099-R-FED-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Local_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Lcl_Tax());                                            //Natural: ADD #RPT-1099R-LCL-TAX TO #TRUST-1099-R-LOCAL-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Int_Amt());                                              //Natural: ADD #RPT-1099R-INT-AMT TO #TRUST-1099-R-INT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_State_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_State_Tax());                                          //Natural: ADD #RPT-1099R-STATE-TAX TO #TRUST-1099-R-STATE-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_R_Irr_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099r_Irr_Amt());                                              //Natural: ADD #RPT-1099R-IRR-AMT TO #TRUST-1099-R-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-I
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 2
        {
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_E_A.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRUST-1099-I-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Gross_Amt());                                      //Natural: ADD #RPT-1099I-GROSS-AMT TO #TRUST-1099-I-GROSS-AMT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Taxable_Amt());                                      //Natural: ADD #RPT-1099I-TAXABLE-AMT TO #TRUST-1099-I-TAXABLE-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Ivc_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Ivc_Amt());                                              //Natural: ADD #RPT-1099I-IVC-AMT TO #TRUST-1099-I-IVC-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Fed_Tax());                                              //Natural: ADD #RPT-1099I-FED-TAX TO #TRUST-1099-I-FED-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Local_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Lcl_Tax());                                            //Natural: ADD #RPT-1099I-LCL-TAX TO #TRUST-1099-I-LOCAL-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Int_Amt());                                              //Natural: ADD #RPT-1099I-INT-AMT TO #TRUST-1099-I-INT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_State_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_State_Tax());                                          //Natural: ADD #RPT-1099I-STATE-TAX TO #TRUST-1099-I-STATE-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_1099_I_Irr_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1099i_Irr_Amt());                                              //Natural: ADD #RPT-1099I-IRR-AMT TO #TRUST-1099-I-IRR-E-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  1042S
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 3
        {
            pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_E_A.nadd(1);                                                                                                   //Natural: ADD 1 TO #TRUST-1042S-E-A
            pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income());                              //Natural: ADD #RPT-1042S-GROSS-INCOME TO #TRUST-1042S-GROSS-AMT-E-A
            pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Refund_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt());                                 //Natural: ADD #RPT-1042S-S-REFUND-AMT TO #TRUST-1042S-REFUND-E-A
            pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest());                                        //Natural: ADD #RPT-1042S-INTEREST TO #TRUST-1042S-INT-E-A
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Fatca_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                         //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-FATCA-E-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trust_Email_Accepted_1042s_Pnd_Trust_1042s_Tax_Non_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                           //Natural: ADD #RPT-1042S-NRA-TAX-WTHLD TO #TRUST-1042S-TAX-NON-E-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  5498
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 4
        {
            accepted_Fields_Pnd_Cnt_A.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-A
            accepted_Fields_Pnd_Trad_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Contrib());                                                      //Natural: ADD #RPT-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-A
            accepted_Fields_Pnd_Sep_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Sep_Amt());                                                                        //Natural: ADD #RPT-SEP-AMT TO #SEP-AMT-A
            accepted_Fields_Pnd_Trad_Ira_Rollover_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Trad_Ira_Rollover());                                                    //Natural: ADD #RPT-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-A
            accepted_Fields_Pnd_Ira_Rechar_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Ira_Rechar_Amt());                                                          //Natural: ADD #RPT-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-A
            accepted_Fields_Pnd_Account_Fmv_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Account_Fmv());                                                                //Natural: ADD #RPT-ACCOUNT-FMV TO #ACCOUNT-FMV-A
            accepted_Fields_Pnd_Roth_Ira_Contrib_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Contrib());                                                      //Natural: ADD #RPT-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-A
            accepted_Fields_Pnd_Roth_Ira_Conversion_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Roth_Ira_Conversion());                                                //Natural: ADD #RPT-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-A
            accepted_Fields_Pnd_Postpn_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Postpn_Amt());                                                                  //Natural: ADD #RPT-POSTPN-AMT TO #POSTPN-AMT-A
            //*  SAIK1
            accepted_Fields_Pnd_Repayments_Amt_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Repayments_Amt());                                                          //Natural: ADD #RPT-REPAYMENTS-AMT TO #REPAYMENTS-AMT-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  480.7C
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 5
        {
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_E_A.nadd(1);                                                                                                         //Natural: ADD 1 TO #TRUST-4807C-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross());                                           //Natural: ADD #RPT-4807C-GROSS TO #TRUST-4807C-GROSS-AMT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Taxable_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable());                                           //Natural: ADD #RPT-4807C-TAXABLE TO #TRUST-4807C-TAXABLE-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int());                                                   //Natural: ADD #RPT-4807C-INT TO #TRUST-4807C-INT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Fed_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld());                                         //Natural: ADD #RPT-4807C-FED-TAX-WTHLD TO #TRUST-4807C-FED-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Dist_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll());                               //Natural: ADD #RPT-4807C-PR-GROSS-AMT-ROLL TO #TRUST-4807C-ROLL-DIST-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Roll_Cont_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll());                                 //Natural: ADD #RPT-4807C-PR-IVC-AMT-ROLL TO #TRUST-4807C-ROLL-CONT-E-A
            //*                                     /* 480.7C DISASTER CHANGES STARTS
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Exempt_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Exempt_Amt());                                     //Natural: ADD #RPT-4807C-EXEMPT-AMT TO #TRUST-4807C-EXEMPT-AMT-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Disaster_Tax_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Disaster_Tax());                                 //Natural: ADD #RPT-4807C-DISASTER-TAX TO #TRUST-4807C-DISASTER-TAX-E-A
            pnd_Trust_Email_Accepted_Pnd_Trust_4807c_Aftrtax_Cont_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Aftrtax_Cont());                                 //Natural: ADD #RPT-4807C-AFTRTAX-CONT TO #TRUST-4807C-AFTRTAX-CONT-E-A
            //*                                     /* 480.7C DISASTER CHANGES ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NR4
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))                                                                                     //Natural: IF #RPT-FORM-TYPE = 7
        {
            pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_E_A.nadd(1);                                                                                                       //Natural: ADD 1 TO #TRUST-NR4-E-A
            pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Gross_Amt_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt());                                       //Natural: ADD #RPT-NR4-GROSS-AMT TO #TRUST-NR4-GROSS-AMT-E-A
            pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Int_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt());                                          //Natural: ADD #RPT-NR4-INTEREST-AMT TO #TRUST-NR4-INT-E-A
            pnd_Trust_Email_Accepted_Nr4_Pnd_Trust_Nr4_Fed_Tax_E_A.nadd(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt());                                       //Natural: ADD #RPT-NR4-FED-TAX-AMT TO #TRUST-NR4-FED-TAX-E-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Title_Report1() throws Exception                                                                                                                     //Natural: TITLE-REPORT1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(24),"Tax Withholding & Payment System",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 24T 'Tax Withholding & Payment System' 68T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM /24X 'Extract Control Report' #WS-TAX-YEAR ( EM = 9999 ) // //25X 'Participant Mass Mailing' 68T 'Report: RPT1' // 'Form Type:' #FORM-NAME ( #FORM-TYPE ) // //
            TabSetting(68),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),NEWLINE,new 
            ColumnSpacing(24),"Extract Control Report",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(25),"Participant Mass Mailing",new 
            TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_Form_Type.getInt() + 1),
            NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Print_Detail_Report() throws Exception                                                                                                               //Natural: PRINT-DETAIL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Ws_Pnd_Form_Type.equals(3)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 3
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Fatca_Tax.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                                                    //Natural: ASSIGN #FATCA-TAX := #RPT-1042S-NRA-TAX-WTHLD
                pnd_Non_Fatca_Tax.setValue(0);                                                                                                                            //Natural: ASSIGN #NON-FATCA-TAX := 0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Non_Fatca_Tax.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                                                //Natural: ASSIGN #NON-FATCA-TAX := #RPT-1042S-NRA-TAX-WTHLD
                pnd_Fatca_Tax.setValue(0);                                                                                                                                //Natural: ASSIGN #FATCA-TAX := 0
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN-NBR EXPANSION
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 5 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 11T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-1042S-GROSS-INCOME ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-1042S-INTEREST ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #NON-FATCA-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 115T #MESSAGE1 / 72T #RPT-1042S-S-REFUND-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #FATCA-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                TabSetting(11),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Non_Fatca_Tax, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(115),pnd_Message1,NEWLINE,new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Fatca_Tax, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 5
        {
            //*  PIN-BR EXPANSION
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 5 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 12T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-4807C-GROSS ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-4807C-INT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #RPT-4807C-PR-GROSS-AMT-ROLL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 115T #MESSAGE1 / 54T #RPT-4807C-TAXABLE ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 75T #RPT-4807C-FED-TAX-WTHLD ( EM = ZZZ,ZZZ,ZZ9.99 ) 93T #RPT-4807C-PR-IVC-AMT-ROLL ( EM = ZZZ,ZZZ,ZZ9.99 )
                TabSetting(12),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll(), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(115),pnd_Message1,NEWLINE,new 
                TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(75),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(93),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll(), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(7)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 7
        {
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 5 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 11T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-NR4-GROSS-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-NR4-INTEREST-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 90T #RPT-NR4-FED-TAX-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 115T #MESSAGE1
                TabSetting(11),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(115),
                pnd_Message1);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Bypass_Report() throws Exception                                                                                                               //Natural: WRITE-BYPASS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_In_Form_Type.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type());                                                                                   //Natural: ASSIGN #IN-FORM-TYPE := #RPT-FORM-TYPE
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().notEquals(pnd_In_Form_Type)))                                                                   //Natural: IF #RPT-FORM-TYPE NE #IN-FORM-TYPE
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1) && pnd_In_Form_Type.equals(10)))                                                  //Natural: IF #RPT-FORM-TYPE = 01 AND #IN-FORM-TYPE = 10
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Input Form Type  and Input file conflict",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 0 ) '***' 25T 'Input Form Type  and Input file conflict' 77T '***' / '***' 25T 'Input Form Type' #IN-FORM-TYPE 77T '***' / '***' 25T 'Input File Form' #RPT-FORM-TYPE 77T '***'
                    TabSetting(25),"Input Form Type",pnd_In_Form_Type,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Input File Form",ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new 
                    TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("A1")))                                                                           //Natural: IF #RPT-BYPASS-ACCPT-IND EQ 'A1'
        {
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet3137 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'B'
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("B")))
        {
            decideConditionsMet3137++;
            //*      IF #RPT-PIN EQ 9999999       /* PIN-NBR EXPANSION
            //*  PIN-NBR EXPANSION
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().equals(new DbsDecimal("999999999999"))))                                                          //Natural: IF #RPT-PIN EQ 999999999999
            {
                pnd_Bypass_Reason.setValue(" MISSING PIN");                                                                                                               //Natural: ASSIGN #BYPASS-REASON := ' MISSING PIN'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bypass_Reason.setValue(" MISSING SSN");                                                                                                               //Natural: ASSIGN #BYPASS-REASON := ' MISSING SSN'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'B1'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("B1")))
        {
            decideConditionsMet3137++;
            //*      IF #RPT-PIN EQ 9999999       /* PIN-NBR EXPANSION
            //*  PIN-NBR EXPANSION
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().equals(new DbsDecimal("999999999999"))))                                                          //Natural: IF #RPT-PIN EQ 999999999999
            {
                pnd_Bypass_Reason.setValue(" MISSING PIN FOR SUNY");                                                                                                      //Natural: ASSIGN #BYPASS-REASON := ' MISSING PIN FOR SUNY'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bypass_Reason.setValue(" MISSING SSN FOR SUNY");                                                                                                      //Natural: ASSIGN #BYPASS-REASON := ' MISSING SSN FOR SUNY'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'B2'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("B2")))
        {
            decideConditionsMet3137++;
            //*      IF #RPT-PIN EQ 9999999       /* PIN-NBR EXPANSION
            //*  PIN-NBR EXPANSION
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin().equals(new DbsDecimal("999999999999"))))                                                          //Natural: IF #RPT-PIN EQ 999999999999
            {
                pnd_Bypass_Reason.setValue(" MISSING PIN FOR 1099R");                                                                                                     //Natural: ASSIGN #BYPASS-REASON := ' MISSING PIN FOR 1099R'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Bypass_Reason.setValue(" MISSING SSN FOR 1099R");                                                                                                     //Natural: ASSIGN #BYPASS-REASON := ' MISSING SSN FOR 1099R'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'C'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("C")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" PAPER PRINT HOLD");                                                                                                              //Natural: ASSIGN #BYPASS-REASON := ' PAPER PRINT HOLD'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'C1'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("C1")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" PAPER PRINT HOLD FOR SUNY");                                                                                                     //Natural: ASSIGN #BYPASS-REASON := ' PAPER PRINT HOLD FOR SUNY'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'C2'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("C2")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" PAPER PRINT HOLD FOR 1009R");                                                                                                    //Natural: ASSIGN #BYPASS-REASON := ' PAPER PRINT HOLD FOR 1009R'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'D'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("D")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" MOORE HOLD");                                                                                                                    //Natural: ASSIGN #BYPASS-REASON := ' MOORE HOLD'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'D1'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("D1")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" MOORE HOLD FOR SUNY");                                                                                                           //Natural: ASSIGN #BYPASS-REASON := ' MOORE HOLD FOR SUNY'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'D2'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("D2")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" MOORE HOLD FOR 1099R");                                                                                                          //Natural: ASSIGN #BYPASS-REASON := ' MOORE HOLD FOR 1099R'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ 'E'
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals("E")))
        {
            decideConditionsMet3137++;
            pnd_Bypass_Reason.setValue(" ONE OF NY LTR ERROR");                                                                                                           //Natural: ASSIGN #BYPASS-REASON := ' ONE OF NY LTR ERROR'
        }                                                                                                                                                                 //Natural: WHEN #RPT-BYPASS-ACCPT-IND EQ ' '
        else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind().equals(" ")))
        {
            decideConditionsMet3137++;
            short decideConditionsMet3177 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-FORM-TYPE EQ 01
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(1)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR 1099R");                                                                                  //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR 1099R'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 02
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(2)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR 1099I");                                                                                  //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR 1099I'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 03
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(3)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR 1042S");                                                                                  //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR 1042S'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 04
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(4)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR 5498");                                                                                   //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR 5498'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 05
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(5)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR 4807C");                                                                                  //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR 4807C'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 07
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(7)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR NR4");                                                                                    //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR NR4'
            }                                                                                                                                                             //Natural: WHEN #RPT-FORM-TYPE EQ 10
            else if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type().equals(10)))
            {
                decideConditionsMet3177++;
                pnd_Bypass_Reason.setValue(" PREVIOUSLY REPORTED EMPTY FORM FOR SUNY");                                                                                   //Natural: ASSIGN #BYPASS-REASON := ' PREVIOUSLY REPORTED EMPTY FORM FOR SUNY'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Form_Type.equals(1) || pnd_Ws_Pnd_Form_Type.equals(2) || pnd_Ws_Pnd_Form_Type.equals(10) || pnd_Ws_Pnd_Form_Type.equals(4)))             //Natural: IF #WS.#FORM-TYPE = 1 OR = 2 OR = 10 OR = 4
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(5),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(20),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new  //Natural: WRITE ( 2 ) 05T #RPT-TIN 20T #RPT-CONTRACT-NBR 35T #RPT-PARTICIPANT-NAME 75T #BYPASS-REASON /
                TabSetting(35),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Participant_Name(),new TabSetting(75),pnd_Bypass_Reason,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(3)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 3
        {
            if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Fatca_Ind().equals("Y")))                                                                         //Natural: IF #RPT-1042S-FATCA-IND = 'Y'
            {
                pnd_Fatca_Tax.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                                                    //Natural: ASSIGN #FATCA-TAX := #RPT-1042S-NRA-TAX-WTHLD
                pnd_Non_Fatca_Tax.setValue(0);                                                                                                                            //Natural: ASSIGN #NON-FATCA-TAX := 0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Non_Fatca_Tax.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Nra_Tax_Wthld());                                                                //Natural: ASSIGN #NON-FATCA-TAX := #RPT-1042S-NRA-TAX-WTHLD
                pnd_Fatca_Tax.setValue(0);                                                                                                                                //Natural: ASSIGN #FATCA-TAX := 0
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN-NBR EXPANSION
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 2 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 11T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-1042S-GROSS-INCOME ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-1042S-INTEREST ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #NON-FATCA-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 114T #MESSAGE1 / 72T #RPT-1042S-S-REFUND-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #FATCA-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                TabSetting(11),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Gross_Income(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_Interest(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Non_Fatca_Tax, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(114),pnd_Message1,NEWLINE,new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_1042s_S_Refund_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),pnd_Fatca_Tax, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(5)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 5
        {
            //*  PIN-BR EXPANSION
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 2 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 12T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-4807C-GROSS ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-4807C-INT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 90T #RPT-4807C-PR-GROSS-AMT-ROLL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 115T #MESSAGE1 / 54T #RPT-4807C-TAXABLE ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 75T #RPT-4807C-FED-TAX-WTHLD ( EM = ZZZ,ZZZ,ZZ9.99 ) 93T #RPT-4807C-PR-IVC-AMT-ROLL ( EM = ZZZ,ZZZ,ZZ9.99 )
                TabSetting(12),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Gross(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Int(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Gross_Amt_Roll(), 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(115),pnd_Message1,NEWLINE,new 
                TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Taxable(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(75),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Fed_Tax_Wthld(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(93),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_4807c_Pr_Ivc_Amt_Roll(), new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Form_Type.equals(7)))                                                                                                                    //Natural: IF #WS.#FORM-TYPE = 7
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year(),new TabSetting(6),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type(),new  //Natural: WRITE ( 2 ) / #RPT-TAX-YEAR 6T #RPT-FORM-TYPE 11T #RPT-COMP-CDE 16T #RPT-TIN 27T #RPT-CONTRACT-NBR 37T #RPT-PAYEE-CDE 41T #RPT-PIN ( EM = 999999999999 ) 54T #RPT-NR4-GROSS-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 72T #RPT-NR4-INTEREST-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 90T #RPT-NR4-FED-TAX-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 110T #RPT-BYPASS-ACCPT-IND 115T #MESSAGE1
                TabSetting(11),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Comp_Cde(),new TabSetting(16),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(27),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Contract_Nbr(),new 
                TabSetting(37),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Payee_Cde(),new TabSetting(41),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Pin(), new 
                ReportEditMask ("999999999999"),new TabSetting(54),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Gross_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(72),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Interest_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(90),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Nr4_Fed_Tax_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(110),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Bypass_Accpt_Ind(),new TabSetting(115),
                pnd_Message1);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Test_Indicator_Report() throws Exception                                                                                                       //Natural: WRITE-TEST-INDICATOR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Test_Ind().equals("Y")))                                                                                    //Natural: IF #RPT-TEST-IND = 'Y'
        {
            pnd_Test_Ind_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TEST-IND-CNT
            getReports().display(3, "Tin Number    ",                                                                                                                     //Natural: DISPLAY ( 03 ) 'Tin Number    ' #RPT-TIN 'Participant Name' #RPT-PART-NAME /
            		ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),"Participant Name",
            		ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name(),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_F5498_Coupon_Form_Process() throws Exception                                                                                                         //Natural: F5498-COUPON-FORM-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(12),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),new TabSetting(24),ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name()); //Natural: WRITE ( 6 ) 12T #RPT-TIN 24T #RPT-PART-NAME
        if (Global.isEscape()) return;
        if (condition(pnd_Tin_Hold.notEquals(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin())))                                                                             //Natural: IF #TIN-HOLD NE #RPT-TIN
        {
            pnd_Tin_Hold.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin());                                                                                         //Natural: ASSIGN #TIN-HOLD := #RPT-TIN
            pnd_Coupon_Cnt.nadd(1);                                                                                                                                       //Natural: ASSIGN #COUPON-CNT := #COUPON-CNT + 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60 ZP=ON");
        Global.format(5, "LS=133 PS=100 ZP=ON");
        Global.format(2, "LS=132 PS=100 ZP=ON");
        Global.format(3, "LS=132 PS=60 ZP=ON");
        Global.format(4, "LS=133 PS=58 ZP=ON");
        Global.format(1, "LS=170 PS=80 ZP=ON");
        Global.format(6, "LS=132 PS=80 ZP=ON");
        Global.format(7, "LS=132 PS=60 ZP=ON");
        Global.format(8, "LS=132 PS=60 ZP=ON");

        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(24),"Tax Withholding and Reporting System",new TabSetting(68),"Page:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(29),"Test Indicator Report",pnd_Ws_Pnd_Tax_Year,NEWLINE,new 
            TabSetting(68),"Report: RPT3",NEWLINE,new TabSetting(32),"Participant Mass Mailing","Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Ws_Pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Ws_Pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(55),"Mobius Mass Migration",new TabSetting(120),"Report: RPT4",NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE,new 
            TabSetting(57),"Form file Update",pnd_Ws_Pnd_Tax_Year,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(3, "Tin Number    ",
        		ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin(),"Participant Name",
        		ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Part_Name(),NEWLINE);
    }
}
