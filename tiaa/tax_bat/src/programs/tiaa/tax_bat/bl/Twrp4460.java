/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:21 PM
**        * FROM NATURAL PROGRAM : Twrp4460
************************************************************
**        * FILE NAME            : Twrp4460.java
**        * CLASS NAME           : Twrp4460
**        * INSTANCE NAME        : Twrp4460
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4460
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : "IRS" FORMS EXTRACT UPDATE THE FORM DATA BASE FILE.
* CREATED  : 11 / 03 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS 5498 "IRS" FORM RECORDS, AND UPDATES THEM AS
*            PROCESSED.  UPDATES THE MASS MAILING DATE ON THE CONTROL
*            DATA BASE FILE.
* 11/29/05  BK RESTOWED WITH UPDATED LDA
* 12/02/17  DASDH  RESTOW FOR 5498 CHANGES
* 18-11-20 : PALDE RESTOW FOR IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4460 extends BLNatBase
{
    // Data Areas
    private LdaTwrl442a ldaTwrl442a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Lu_User;
    private DbsField form_Tirf_Irs_Rpt_Ind;
    private DbsField form_Tirf_Lu_Ts;
    private DbsField form_Tirf_Irs_Rpt_Date;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_Tbl_Nbr;
    private DbsField cntl_Tircntl_Tax_Year;
    private DbsField cntl_Tircntl_Rpt_Form_Name;

    private DataAccessProgramView vw_cntlu;
    private DbsField cntlu_Count_Casttircntl_Rpt_Tbl_Pe;

    private DbsGroup cntlu_Tircntl_Rpt_Tbl_Pe;
    private DbsField cntlu_Tircntl_Rpt_Dte;
    private DbsField pnd_Super_Cntl;

    private DbsGroup pnd_Super_Cntl__R_Field_1;
    private DbsField pnd_Super_Cntl_Pnd_S_Tbl;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year;

    private DbsGroup pnd_Super_Cntl__R_Field_2;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year_N;
    private DbsField pnd_Super_Cntl_Pnd_S_Form;
    private DbsField pnd_Todays_Yyyymmdd;

    private DbsGroup pnd_Todays_Yyyymmdd__R_Field_3;
    private DbsField pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N;

    private DbsGroup pnd_Todays_Yyyymmdd__R_Field_4;
    private DbsField pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Et_Counter;
    private DbsField pnd_Datn;
    private DbsField pnd_Datx;
    private DbsField pnd_Timx;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Ws_Form;
    private DbsField pnd_Ws_Pnd_Ws_Tax_Year;
    private DbsField i;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl442a = new LdaTwrl442a();
        registerRecord(ldaTwrl442a);

        // Local Variables
        localVariables = new DbsRecord();

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Tirf_Lu_User = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Tirf_Irs_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_Tirf_Lu_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Tirf_Irs_Rpt_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        registerRecord(vw_form);

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        cntl_Tircntl_Tbl_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_Tircntl_Tax_Year = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_Tircntl_Rpt_Form_Name = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIRCNTL_RPT_FORM_NAME");
        registerRecord(vw_cntl);

        vw_cntlu = new DataAccessProgramView(new NameInfo("vw_cntlu", "CNTLU"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        cntlu_Count_Casttircntl_Rpt_Tbl_Pe = vw_cntlu.getRecord().newFieldInGroup("cntlu_Count_Casttircntl_Rpt_Tbl_Pe", "C*TIRCNTL-RPT-TBL-PE", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");

        cntlu_Tircntl_Rpt_Tbl_Pe = vw_cntlu.getRecord().newGroupInGroup("cntlu_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntlu_Tircntl_Rpt_Dte = cntlu_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("cntlu_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntlu_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        registerRecord(vw_cntlu);

        pnd_Super_Cntl = localVariables.newFieldInRecord("pnd_Super_Cntl", "#SUPER-CNTL", FieldType.STRING, 12);

        pnd_Super_Cntl__R_Field_1 = localVariables.newGroupInRecord("pnd_Super_Cntl__R_Field_1", "REDEFINE", pnd_Super_Cntl);
        pnd_Super_Cntl_Pnd_S_Tbl = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tbl", "#S-TBL", FieldType.NUMERIC, 1);
        pnd_Super_Cntl_Pnd_S_Tax_Year = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.STRING, 4);

        pnd_Super_Cntl__R_Field_2 = pnd_Super_Cntl__R_Field_1.newGroupInGroup("pnd_Super_Cntl__R_Field_2", "REDEFINE", pnd_Super_Cntl_Pnd_S_Tax_Year);
        pnd_Super_Cntl_Pnd_S_Tax_Year_N = pnd_Super_Cntl__R_Field_2.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year_N", "#S-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_Cntl_Pnd_S_Form = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Form", "#S-FORM", FieldType.STRING, 7);
        pnd_Todays_Yyyymmdd = localVariables.newFieldInRecord("pnd_Todays_Yyyymmdd", "#TODAYS-YYYYMMDD", FieldType.STRING, 8);

        pnd_Todays_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Todays_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Todays_Yyyymmdd);
        pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N = pnd_Todays_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N", "#TODAYS-YYYYMMDD-N", 
            FieldType.NUMERIC, 8);

        pnd_Todays_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Todays_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Todays_Yyyymmdd);
        pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N = pnd_Todays_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyy_N", "#TODAYS-YYYY-N", 
            FieldType.NUMERIC, 4);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Counter = localVariables.newFieldInRecord("pnd_Et_Counter", "#ET-COUNTER", FieldType.PACKED_DECIMAL, 4);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Ws_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Ws_Form", "#WS-FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Ws_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_cntl.reset();
        vw_cntlu.reset();

        ldaTwrl442a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4460() throws Exception
    {
        super("Twrp4460");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4460", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Todays_Yyyymmdd_Pnd_Todays_Yyyymmdd_N.setValue(Global.getDATN());                                                                                             //Natural: ASSIGN #TODAYS-YYYYMMDD-N := *DATN
        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        pnd_Datx.setValue(Global.getDATX());                                                                                                                              //Natural: ASSIGN #DATX := *DATX
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: ASSIGN #TIMX := *TIMX
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, ldaTwrl442a.getPnd_Form())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            G1:                                                                                                                                                           //Natural: GET FORM #F-ISN
            vw_form.readByID(ldaTwrl442a.getPnd_Form_Pnd_F_Isn().getLong(), "G1");
            form_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                            //Natural: ASSIGN TIRF-LU-USER := *INIT-USER
            form_Tirf_Irs_Rpt_Ind.setValue("O");                                                                                                                          //Natural: ASSIGN TIRF-IRS-RPT-IND := 'O'
            form_Tirf_Lu_Ts.setValue(pnd_Timx);                                                                                                                           //Natural: ASSIGN TIRF-LU-TS := #TIMX
            form_Tirf_Irs_Rpt_Date.setValue(pnd_Datx);                                                                                                                    //Natural: ASSIGN TIRF-IRS-RPT-DATE := #DATX
            vw_form.updateDBRow("G1");                                                                                                                                    //Natural: UPDATE ( G1. )
            pnd_Et_Counter.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ET-COUNTER
            if (condition(pnd_Et_Counter.greater(100)))                                                                                                                   //Natural: IF #ET-COUNTER > 100
            {
                pnd_Et_Counter.setValue(0);                                                                                                                               //Natural: ASSIGN #ET-COUNTER := 0
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form (5498) Extract File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new                   //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) Extract File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
        sub_Update_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *--------------------------------------
        //* *------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Cntl_Pnd_S_Tbl.setValue(4);                                                                                                                             //Natural: ASSIGN #S-TBL := 4
        pnd_Super_Cntl_Pnd_S_Form.setValue("5498");                                                                                                                       //Natural: ASSIGN #S-FORM := '5498'
        vw_cntl.startDatabaseRead                                                                                                                                         //Natural: READ ( 1 ) CNTL WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER-CNTL
        (
        "RL1",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super_Cntl, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") },
        1
        );
        RL1:
        while (condition(vw_cntl.readNextRow("RL1")))
        {
            if (condition(cntl_Tircntl_Tbl_Nbr.equals(pnd_Super_Cntl_Pnd_S_Tbl) && cntl_Tircntl_Tax_Year.equals(pnd_Super_Cntl_Pnd_S_Tax_Year_N) && cntl_Tircntl_Rpt_Form_Name.equals(pnd_Super_Cntl_Pnd_S_Form))) //Natural: IF TIRCNTL-TBL-NBR = #S-TBL AND TIRCNTL-TAX-YEAR = #S-TAX-YEAR-N AND TIRCNTL-RPT-FORM-NAME = #S-FORM
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            G2:                                                                                                                                                           //Natural: GET CNTLU *ISN ( RL1. )
            vw_cntlu.readByID(vw_cntl.getAstISN("RL1"), "G2");
            i.compute(new ComputeParameters(false, i), cntlu_Count_Casttircntl_Rpt_Tbl_Pe.add(1));                                                                        //Natural: ASSIGN I := C*TIRCNTL-RPT-TBL-PE + 1
            if (condition(i.greater(12)))                                                                                                                                 //Natural: IF I > 12
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cntlu_Tircntl_Rpt_Dte.getValue(i).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Datn);                                                                //Natural: MOVE EDITED #DATN TO CNTLU.TIRCNTL-RPT-DTE ( I ) ( EM = YYYYMMDD )
                vw_cntlu.updateDBRow("G2");                                                                                                                               //Natural: UPDATE ( G2. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(vw_cntl.getAstCOUNTER().equals(getZero())))                                                                                                         //Natural: IF *COUNTER ( RL1. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Report Form (5498) Control File Record Is Missing",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'Report Form (5498) Control File Record Is Missing' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP4460|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Ws_Tax_Year.equals(getZero())))                                                                                                  //Natural: IF #WS-TAX-YEAR = 0
                {
                    pnd_Super_Cntl_Pnd_S_Tax_Year.compute(new ComputeParameters(false, pnd_Super_Cntl_Pnd_S_Tax_Year), Global.getDATN().divide(10000).subtract(1));       //Natural: ASSIGN #S-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Super_Cntl_Pnd_S_Tax_Year.setValue(pnd_Ws_Pnd_Ws_Tax_Year);                                                                                       //Natural: ASSIGN #S-TAX-YEAR := #WS-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, new TabSetting(1),"Records Read & Updated.....................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'Records Read & Updated.....................' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Records Read & Updated.....................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));       //Natural: WRITE ( 01 ) 01T 'Records Read & Updated.....................' #READ-CTR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(43),"Form (5498) Data Base, And Control File Update",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 43T 'Form (5498) Data Base, And Control File Update' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
