/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:41 PM
**        * FROM NATURAL PROGRAM : Twrp0603
************************************************************
**        * FILE NAME            : Twrp0603.java
**        * CLASS NAME           : Twrp0603
**        * INSTANCE NAME        : Twrp0603
************************************************************
************************************************************************
*
* PROGRAM  : TWRP0603
* SYSTEM   : TAXWARS - TAX WITHHOLDING & REPORTING SYSTEM
* TITLE    : UPDATES DATE CONTROL FILES FOR PRIOR YEAR DAILY FEED
* FUNCTION : TO CREATE UPDATED CONTROL FILES FOR USE IN LOADING PAYMENT
*            DATA INTO THE TAXWARS DATABASE.
*
* HISTORY  :
* 06/01/06   J. ROTHOLZ
*            CLONED TWRP0600 FOR USE UPDATING CONTROL RECORDS FOR
*            PRIOR TAX YEAR.  (NEEDED FOR SOURCE CODES "OP" AND "NV".)
* 09/15/11   M BERLIN
*            ADDED SOURCE CODE "AM" FOR MCCAMISH FEED.
*            MODELLED AFTER "NV"                          /* 09/15/11
* 01/17/12   RSALGADO
*            ALLOW WEEK-END PROCESS IF IN ANY TEST ENVIRONMENT
* 05/31/12   RCARREON
*            ADDED SOURCE CODE "VL" FOR VUL              /* RC05
*            MODELLED AFTER "AM"
* 03/11/13   RCARREON
*            ADDED SOURCE CODE 'EW' FOR GENERIC WARRANTS /* RC06
* 04/02/13   COMMENT OUT VL
* 04/11/18   'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0603 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;
    private PdaTwra0001 pdaTwra0001;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Alpha_Date;

    private DbsGroup pnd_Alpha_Date__R_Field_1;
    private DbsField pnd_Alpha_Date_Pnd_Current_Year;
    private DbsField pnd_Alpha_Date_Pnd_Curent_Month;
    private DbsField pnd_Alpha_Date_Pnd_First_Day;
    private DbsField pnd_End_Of_Last_Month;
    private DbsField pnd_Work_Year;
    private DbsField pnd_Ws_Remainder;
    private DbsField pnd_Work_Ccyymmdd;

    private DbsGroup pnd_Work_Ccyymmdd__R_Field_2;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A;

    private DbsGroup pnd_Work_Ccyymmdd__R_Field_3;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Ccyy;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Mm;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Dd;
    private DbsField pnd_Datn;

    private DbsGroup pnd_Datn__R_Field_4;
    private DbsField pnd_Datn_Pnd_Datn_Year_Ccyy;

    private DbsGroup pnd_Datn__R_Field_5;
    private DbsField pnd_Datn_Pnd_Datn_Year_N4;
    private DbsField pnd_Tax_Year;

    private DbsGroup pnd_Tax_Year__R_Field_6;
    private DbsField pnd_Tax_Year_Pnd_Tax_Year_N4;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Day_Number;
    private DbsField pnd_System_Date;
    private DbsField i;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        localVariables = new DbsRecord();
        pdaTwra0001 = new PdaTwra0001(localVariables);

        // Local Variables
        pnd_Alpha_Date = localVariables.newFieldInRecord("pnd_Alpha_Date", "#ALPHA-DATE", FieldType.STRING, 8);

        pnd_Alpha_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Alpha_Date__R_Field_1", "REDEFINE", pnd_Alpha_Date);
        pnd_Alpha_Date_Pnd_Current_Year = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_Current_Year", "#CURRENT-YEAR", FieldType.STRING, 
            4);
        pnd_Alpha_Date_Pnd_Curent_Month = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_Curent_Month", "#CURENT-MONTH", FieldType.STRING, 
            2);
        pnd_Alpha_Date_Pnd_First_Day = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_First_Day", "#FIRST-DAY", FieldType.STRING, 2);
        pnd_End_Of_Last_Month = localVariables.newFieldInRecord("pnd_End_Of_Last_Month", "#END-OF-LAST-MONTH", FieldType.DATE);
        pnd_Work_Year = localVariables.newFieldInRecord("pnd_Work_Year", "#WORK-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Remainder = localVariables.newFieldInRecord("pnd_Ws_Remainder", "#WS-REMAINDER", FieldType.NUMERIC, 2);
        pnd_Work_Ccyymmdd = localVariables.newFieldInRecord("pnd_Work_Ccyymmdd", "#WORK-CCYYMMDD", FieldType.STRING, 8);

        pnd_Work_Ccyymmdd__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Ccyymmdd__R_Field_2", "REDEFINE", pnd_Work_Ccyymmdd);
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A", "#WORK-CCYY-A", FieldType.STRING, 
            4);

        pnd_Work_Ccyymmdd__R_Field_3 = pnd_Work_Ccyymmdd__R_Field_2.newGroupInGroup("pnd_Work_Ccyymmdd__R_Field_3", "REDEFINE", pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A);
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy = pnd_Work_Ccyymmdd__R_Field_3.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Ccyy", "#WORK-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Work_Ccyymmdd_Pnd_Work_Mm = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Mm", "#WORK-MM", FieldType.NUMERIC, 2);
        pnd_Work_Ccyymmdd_Pnd_Work_Dd = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);

        pnd_Datn__R_Field_4 = localVariables.newGroupInRecord("pnd_Datn__R_Field_4", "REDEFINE", pnd_Datn);
        pnd_Datn_Pnd_Datn_Year_Ccyy = pnd_Datn__R_Field_4.newFieldInGroup("pnd_Datn_Pnd_Datn_Year_Ccyy", "#DATN-YEAR-CCYY", FieldType.STRING, 4);

        pnd_Datn__R_Field_5 = pnd_Datn__R_Field_4.newGroupInGroup("pnd_Datn__R_Field_5", "REDEFINE", pnd_Datn_Pnd_Datn_Year_Ccyy);
        pnd_Datn_Pnd_Datn_Year_N4 = pnd_Datn__R_Field_5.newFieldInGroup("pnd_Datn_Pnd_Datn_Year_N4", "#DATN-YEAR-N4", FieldType.NUMERIC, 4);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_Tax_Year__R_Field_6 = localVariables.newGroupInRecord("pnd_Tax_Year__R_Field_6", "REDEFINE", pnd_Tax_Year);
        pnd_Tax_Year_Pnd_Tax_Year_N4 = pnd_Tax_Year__R_Field_6.newFieldInGroup("pnd_Tax_Year_Pnd_Tax_Year_N4", "#TAX-YEAR-N4", FieldType.NUMERIC, 4);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Day_Number = localVariables.newFieldInRecord("pnd_Day_Number", "#DAY-NUMBER", FieldType.STRING, 1);
        pnd_System_Date = localVariables.newFieldInRecord("pnd_System_Date", "#SYSTEM-DATE", FieldType.DATE);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_Work_Year.setInitialValue(0);
        pnd_Ws_Remainder.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0603() throws Exception
    {
        super("Twrp0603");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* **
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #TWRP0600-CONTROL-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            getReports().print(0, "BEFORE",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                 //Natural: PRINT 'BEFORE' #TWRP0600-CONTROL-RECORD
            //*  RC06
            //*  VIKRAM2
            if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("OP") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NV")  //Natural: IF #TWRP0600-ORIGIN-CODE = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL'
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AM") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("EW") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL")))
            {
                //* *                      OR = 'VL'
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  RC05 VR01 RC06
                getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Source Code",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid Source Codes","(NV/OP/AM/EW)",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Source Code' 77T '***' / '***' 06T 'Valid Source Codes' '(NV/OP/AM/EW)' #TWRP0600-ORIGIN-CODE 77T '***'
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(90);  if (true) return;                                                                                                                 //Natural: TERMINATE 90
            }                                                                                                                                                             //Natural: END-IF
            //*  VIKRAM2
            //* *  VALUE  'NV', 'OP', 'AM', 'EW'                                                                                                                          //Natural: DECIDE ON FIRST #TWRP0600-ORIGIN-CODE
            short decideConditionsMet109 = 0;                                                                                                                             //Natural: VALUE 'NV', 'OP', 'AM', 'EW','VL'
            if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NV") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("OP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AM") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("EW") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL"))))
            {
                decideConditionsMet109++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-DAILY
                sub_Process_Daily();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  09/15/11 VIKRAM2/* RC05
                //*  09/15/11       /* RC05
                getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Source Code",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid Source Codes","(NV/OP/AM/VL)","(NV/OP/AM)",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Source Code' 77T '***' / '***' 06T 'Valid Source Codes' '(NV/OP/AM/VL)' '(NV/OP/AM)' #TWRP0600-ORIGIN-CODE 77T '***'
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(91);  if (true) return;                                                                                                                 //Natural: TERMINATE 91
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(2, false, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                 //Natural: WRITE WORK FILE 02 #TWRP0600-CONTROL-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().print(0, "=",pnd_Read_Ctr);                                                                                                                          //Natural: PRINT '=' #READ-CTR
        //* *******************************
        //* *******************************
        //*  IF #TWRP0600-TAX-YEAR-CCYY   =  #DATN-YEAR-CCYY
        //*    IGNORE
        //*  ELSE
        //*    #TWRP0600-TAX-YEAR-CCYY  :=  #DATN-YEAR-CCYY
        //*  END-IF
        //* *******************************
        //* *******************
        //* *****************                                                                                                                                             //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //* *****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Process_Daily() throws Exception                                                                                                                     //Natural: PROCESS-DAILY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        if (condition(Global.getINIT_USER().equals("P1029TWD")))                                                                                                          //Natural: IF *INIT-USER = 'P1029TWD'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WEEKEND-TEST
            sub_Weekend_Test();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Datn);                                                                       //Natural: ASSIGN #TWRP0600-FROM-CCYYMMDD := #DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Datn);                                                                         //Natural: ASSIGN #TWRP0600-TO-CCYYMMDD := #DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Datn);                                                                  //Natural: ASSIGN #TWRP0600-INTERFACE-CCYYMMDD := #DATN
        pnd_Tax_Year_Pnd_Tax_Year_N4.compute(new ComputeParameters(false, pnd_Tax_Year_Pnd_Tax_Year_N4), pnd_Datn_Pnd_Datn_Year_N4.subtract(1));                          //Natural: ASSIGN #TAX-YEAR-N4 := #DATN-YEAR-N4 - 1
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValue(pnd_Tax_Year);                                                                   //Natural: ASSIGN #TWRP0600-TAX-YEAR-CCYY := #TAX-YEAR
        getReports().print(0, "AFTER-",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: PRINT 'AFTER-' #TWRP0600-CONTROL-RECORD
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( 00 ) 1
    }
    private void sub_Weekend_Test() throws Exception                                                                                                                      //Natural: WEEKEND-TEST
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  DETERMINE IF 'PROD' OR 'TEST' ENVIRONMENT      /* RS012012 START
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"- Calling Sub-Program 'TWRN0001'...");                                                                                 //Natural: WRITE ( 00 ) *PROGRAM '- Calling Sub-Program "TWRN0001"...'
        if (Global.isEscape()) return;
        pdaTwra0001.getTwra0001().reset();                                                                                                                                //Natural: RESET TWRA0001
        DbsUtil.callnat(Twrn0001.class , getCurrentProcessState(), pdaTwra0001.getTwra0001());                                                                            //Natural: CALLNAT 'TWRN0001' TWRA0001
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Prod_Test());                                                                                               //Natural: WRITE ( 00 ) '=' TWRA0001.#PROD-TEST
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return());                                                                                         //Natural: WRITE ( 00 ) '=' TWRA0001.#TWRN0001-RETURN
        if (Global.isEscape()) return;
        if (condition(pdaTwra0001.getTwra0001_Pnd_Prod_Test().equals("TEST")))                                                                                            //Natural: IF TWRA0001.#PROD-TEST = 'TEST'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_System_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Datn);                                                                                          //Natural: MOVE EDITED #DATN TO #SYSTEM-DATE ( EM = YYYYMMDD )
        pnd_Day_Number.setValueEdited(pnd_System_Date,new ReportEditMask("O"));                                                                                           //Natural: MOVE EDITED #SYSTEM-DATE ( EM = O ) TO #DAY-NUMBER
        //*   SUNDAY  OR  SATURDAY
        if (condition(pnd_Day_Number.equals("1") || pnd_Day_Number.equals("7")))                                                                                          //Natural: IF #DAY-NUMBER = '1' OR = '7'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Weekend Execution",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Job Is Valid To Run Monday Thru Friday Only",pnd_Datn,  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Weekend Execution' 77T '***' / '***' 06T 'Job Is Valid To Run Monday Thru Friday Only' #DATN ( EM = XXXX/XX/XX ) 77T '***'
                new ReportEditMask ("XXXX/XX/XX"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *******************
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(128),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 01 ) NOTITLE *PROGRAM 49T 'Tax Withholding & Reporting System' 128T *PAGE-NUMBER ( 01 ) ( EM = ZZZ9 ) / *DATX ( EM = LLL' 'DD','YY ) 49T '    Date Control File - Update    ' 124T *TIMX ( EM = HH':'II' 'AP ) / 49T '       Batch Control Report       ' ///
                        new ReportEditMask ("ZZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(49),"    Date Control File - Update    ",new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(49),"       Batch Control Report       ",
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
