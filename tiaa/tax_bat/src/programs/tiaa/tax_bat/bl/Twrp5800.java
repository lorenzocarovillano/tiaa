/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:52 PM
**        * FROM NATURAL PROGRAM : Twrp5800
************************************************************
**        * FILE NAME            : Twrp5800.java
**        * CLASS NAME           : Twrp5800
**        * INSTANCE NAME        : Twrp5800
************************************************************
**----------------------------------------------------------------------
** PROGRAM    :  TWRP5800
** APPLICATION:  TAXWARS
** DATE       :  2017/05/05
** DESCRIPTION:  PROGRAM TO RESET MOORE-HOLD-IND
** AUTHOR     :  SAURAV VIKRAM
** 10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
**----------------------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5800 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9710 ldaTwrl9710;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Moore_Hold_Ind;

    private DbsGroup pnd_Ws_Input;
    private DbsField pnd_Ws_Input_Pnd_In_Tax_Year;
    private DbsField pnd_Ws_Input_Pnd_In_Active_Ind;
    private DbsField pnd_Ws_Input_Pnd_In_Form_Type;
    private DbsField pnd_Superde_3;

    private DbsGroup pnd_Superde_3__R_Field_1;
    private DbsField pnd_Superde_3_Pnd_Sd3_Tax_Year;
    private DbsField pnd_Superde_3_Pnd_Sd3_Active_Ind;
    private DbsField pnd_Superde_3_Pnd_Sd3_Form_Type;
    private DbsField pnd_Superde_3_Pnd_Sd3_Company_Cde;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Isn;
    private DbsField pnd_Total_Input_Read;
    private DbsField pnd_Upd_Ctr;
    private DbsField pnd_Upd_Ctr_Before;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());

        // Local Variables
        localVariables = new DbsRecord();

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Moore_Hold_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Moore_Hold_Ind", "TIRF-MOORE-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_HOLD_IND");
        form_U_Tirf_Moore_Hold_Ind.setDdmHeader("MOORE/HOLD/IND");
        registerRecord(vw_form_U);

        pnd_Ws_Input = localVariables.newGroupInRecord("pnd_Ws_Input", "#WS-INPUT");
        pnd_Ws_Input_Pnd_In_Tax_Year = pnd_Ws_Input.newFieldInGroup("pnd_Ws_Input_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.STRING, 4);
        pnd_Ws_Input_Pnd_In_Active_Ind = pnd_Ws_Input.newFieldInGroup("pnd_Ws_Input_Pnd_In_Active_Ind", "#IN-ACTIVE-IND", FieldType.STRING, 1);
        pnd_Ws_Input_Pnd_In_Form_Type = pnd_Ws_Input.newFieldInGroup("pnd_Ws_Input_Pnd_In_Form_Type", "#IN-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Superde_3 = localVariables.newFieldInRecord("pnd_Superde_3", "#SUPERDE-3", FieldType.STRING, 8);

        pnd_Superde_3__R_Field_1 = localVariables.newGroupInRecord("pnd_Superde_3__R_Field_1", "REDEFINE", pnd_Superde_3);
        pnd_Superde_3_Pnd_Sd3_Tax_Year = pnd_Superde_3__R_Field_1.newFieldInGroup("pnd_Superde_3_Pnd_Sd3_Tax_Year", "#SD3-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Superde_3_Pnd_Sd3_Active_Ind = pnd_Superde_3__R_Field_1.newFieldInGroup("pnd_Superde_3_Pnd_Sd3_Active_Ind", "#SD3-ACTIVE-IND", FieldType.STRING, 
            1);
        pnd_Superde_3_Pnd_Sd3_Form_Type = pnd_Superde_3__R_Field_1.newFieldInGroup("pnd_Superde_3_Pnd_Sd3_Form_Type", "#SD3-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Superde_3_Pnd_Sd3_Company_Cde = pnd_Superde_3__R_Field_1.newFieldInGroup("pnd_Superde_3_Pnd_Sd3_Company_Cde", "#SD3-COMPANY-CDE", FieldType.STRING, 
            1);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 11);
        pnd_Total_Input_Read = localVariables.newFieldInRecord("pnd_Total_Input_Read", "#TOTAL-INPUT-READ", FieldType.NUMERIC, 8);
        pnd_Upd_Ctr = localVariables.newFieldInRecord("pnd_Upd_Ctr", "#UPD-CTR", FieldType.NUMERIC, 8);
        pnd_Upd_Ctr_Before = localVariables.newFieldInRecord("pnd_Upd_Ctr_Before", "#UPD-CTR-BEFORE", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();

        ldaTwrl9710.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5800() throws Exception
    {
        super("Twrp5800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp5800|Main");
        while(true)
        {
            try
            {
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *************************************************
                //*                MAIN PARA                        *
                //* *************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Input);                                                                                         //Natural: INPUT #WS-INPUT
                pnd_Superde_3_Pnd_Sd3_Tax_Year.setValue(pnd_Ws_Input_Pnd_In_Tax_Year);                                                                                    //Natural: MOVE #IN-TAX-YEAR TO #SD3-TAX-YEAR
                pnd_Superde_3_Pnd_Sd3_Active_Ind.setValue(pnd_Ws_Input_Pnd_In_Active_Ind);                                                                                //Natural: MOVE #IN-ACTIVE-IND TO #SD3-ACTIVE-IND
                pnd_Superde_3_Pnd_Sd3_Form_Type.setValue(pnd_Ws_Input_Pnd_In_Form_Type);                                                                                  //Natural: MOVE #IN-FORM-TYPE TO #SD3-FORM-TYPE
                pnd_Superde_3_Pnd_Sd3_Company_Cde.setValue(pnd_Ws_Const_Low_Values);                                                                                      //Natural: MOVE LOW-VALUES TO #SD3-COMPANY-CDE
                getReports().write(1, new TabSetting(12),"MOORE INDICATOR RESET REPORT - ",pnd_Ws_Input_Pnd_In_Tax_Year,NEWLINE,new TabSetting(12),"------------------------------------",NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 12T 'MOORE INDICATOR RESET REPORT - ' #IN-TAX-YEAR / 12T '------------------------------------' // 12T 'Participant Detail  ' 38T ' Contract Detail ' / 12T '------------------  ' 38T ' --------------- ' //
                    TabSetting(12),"Participant Detail  ",new TabSetting(38)," Contract Detail ",NEWLINE,new TabSetting(12),"------------------  ",new TabSetting(38),
                    " --------------- ",NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
                pnd_Upd_Ctr.reset();                                                                                                                                      //Natural: RESET #UPD-CTR #TOTAL-INPUT-READ #UPD-CTR-BEFORE #I
                pnd_Total_Input_Read.reset();
                pnd_Upd_Ctr_Before.reset();
                pnd_I.reset();
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-3 FROM #SUPERDE-3
                (
                "SV1",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Superde_3, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                SV1:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("SV1")))
                {
                    if (condition((ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Superde_3_Pnd_Sd3_Form_Type)) || (ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Ws_Input_Pnd_In_Tax_Year))  //Natural: IF ( TIRF-FORM-TYPE NE #SD3-FORM-TYPE ) OR ( TIRF-TAX-YEAR NE #IN-TAX-YEAR ) OR ( TIRF-ACTIVE-IND NE #SD3-ACTIVE-IND )
                        || (ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Superde_3_Pnd_Sd3_Active_Ind))))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Total_Input_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-INPUT-READ
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO FORM.C*TIRF-SYS-ERR
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(1).equals(33) && ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(2).equals(getZero())))     //Natural: IF TIRF-SYS-ERR ( 1 ) = 33 AND TIRF-SYS-ERR ( 2 ) = 0
                        {
                            pnd_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("SV1"));                                                                                  //Natural: ASSIGN #ISN := *ISN ( SV1. )
                            pnd_Upd_Ctr_Before.nadd(1);                                                                                                                   //Natural: ASSIGN #UPD-CTR-BEFORE := #UPD-CTR-BEFORE+ 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-MOORE-HOLD-IND
                            sub_Update_Moore_Hold_Ind();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("SV1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("SV1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* ******************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-MOORE-HOLD-IND
                //* ******************************************************************
                getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF FORMS READ    >> ",pnd_Total_Input_Read);                     //Natural: WRITE //// 'TOTAL NUMBER OF FORMS READ    >> ' #TOTAL-INPUT-READ
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"TOTAL NUMBER OF FORMS UPDATED >> ",pnd_Upd_Ctr);                                                              //Natural: WRITE 'TOTAL NUMBER OF FORMS UPDATED >> ' #UPD-CTR
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"TOTAL NUMBER OF FORMS BEFORE UPDATED >> ",pnd_Upd_Ctr_Before);                                                //Natural: WRITE 'TOTAL NUMBER OF FORMS BEFORE UPDATED >> ' #UPD-CTR-BEFORE
                if (Global.isEscape()) return;
                if (condition(pnd_Upd_Ctr.notEquals(pnd_Upd_Ctr_Before)))                                                                                                 //Natural: IF #UPD-CTR NE #UPD-CTR-BEFORE
                {
                    getReports().write(0, ReportOption.NOTITLE,"UPDATE NUMBER MISMATCH");                                                                                 //Natural: WRITE "UPDATE NUMBER MISMATCH"
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Update_Moore_Hold_Ind() throws Exception                                                                                                             //Natural: UPDATE-MOORE-HOLD-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************************
        getReports().write(0, ReportOption.NOTITLE,"Before Change ISN:",ldaTwrl9710.getVw_form().getAstISN("SV1"),"SSN1:",ldaTwrl9710.getForm_Tirf_Tin(),                 //Natural: WRITE NOTITLE 'Before Change ISN:' *ISN ( SV1. ) 'SSN1:' FORM.TIRF-TIN 'MOORE-HOLD-IND1:' FORM.TIRF-MOORE-HOLD-IND
            "MOORE-HOLD-IND1:",ldaTwrl9710.getForm_Tirf_Moore_Hold_Ind());
        if (Global.isEscape()) return;
        GET:                                                                                                                                                              //Natural: GET FORM-U #ISN
        vw_form_U.readByID(pnd_Isn.getLong(), "GET");
        form_U_Tirf_Moore_Hold_Ind.setValue(" ");                                                                                                                         //Natural: MOVE ' ' TO FORM-U.TIRF-MOORE-HOLD-IND
        pnd_Upd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #UPD-CTR
        getReports().write(0, ReportOption.NOTITLE,"After Change ISN :",ldaTwrl9710.getVw_form().getAstISN("SV1"),"SSN2:",ldaTwrl9710.getForm_Tirf_Tin(),                 //Natural: WRITE NOTITLE 'After Change ISN :' *ISN ( SV1. ) 'SSN2:' FORM.TIRF-TIN 'MOORE-HOLD-IND2:' FORM-U.TIRF-MOORE-HOLD-IND
            "MOORE-HOLD-IND2:",form_U_Tirf_Moore_Hold_Ind);
        if (Global.isEscape()) return;
        vw_form_U.updateDBRow("GET");                                                                                                                                     //Natural: UPDATE ( GET. )
        getReports().write(1, new TabSetting(12),ldaTwrl9710.getForm_Tirf_Tin(),new TabSetting(39),ldaTwrl9710.getForm_Tirf_Contract_Nbr());                              //Natural: WRITE ( 1 ) 12T FORM.TIRF-TIN 39T TIRF-CONTRACT-NBR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //
}
