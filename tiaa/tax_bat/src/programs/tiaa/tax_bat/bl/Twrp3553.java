/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:37:41 PM
**        * FROM NATURAL PROGRAM : Twrp3553
************************************************************
**        * FILE NAME            : Twrp3553.java
**        * CLASS NAME           : Twrp3553
**        * INSTANCE NAME        : Twrp3553
************************************************************
************************************************************************
** PROGRAM : TWRP3553
** SYSTEM  : TAXWARS
** FUNCTION: 480.7C CORRECTION REPORTING
** HISTORY.....:
**
** 05/22/09 - MS - USED FOR TAX YEARS STARTING WITH 2008
**                 TAXWARS DOES NOT SUPPORT TAX WITHHOLDING FOR 480.7C
**                 RIGHT NOW. RECONCILIATION AND GENERAL LEDGER REPORTS
**                 ARE COMMENTED OUT. DETAIL REPORTS MAY HAVE TO BE
**                 ENHANCED ONCE NEEDED.
**
** 07/10/14 - CTS - OUTPUT WORK FILE INTRODUCED FOR SU, PA, DETAIL
**                  AND SUMMARY RECORDS. CHANGES ARE TAGGED WITH CTS.
**                  BACKUP OF THE PROGRAM WITH UNUSED CODE HAS BEEN
**                  KEPT IN THE PROJBTAX LIBRARY.
**
** 07/14/15-RAHUL - 480.7C PHASE 1 CHANGES - INTRODUCE 2ND WORK FILE
**                  TO DISTINGUISH BETWEEN ORIGINAL AND CORRECTED
**                  RECORDS                   - TAG DASRAH
**                  FILE 1 - ORIGINAL RECORD
**                  FILE 2 - CORRECTED RECORD
** 08/12/15-RAHUL - 480.7C PHASE 2 CHANGES - PA,DTL AND SM RECORD
**                  CHANGES FOR TAXYEAR 2014 ONWARDS - TAG DASRAH2
** 06/01/16-DEBASISH - IRS CORRECTION FILE LAYOUT CHANGES DUE TO 480.7C
**                     CORRECTION REPORTING CHANGES - TAG DUTTAD
** 08/29/16-SNEHA - ADD SOME NEW FIELDS IN LDA TWRL3553 AND UPDATE THEM
**                  WITH ZEROES INSTEAD OF SPACES IF THERE ARE NO VALUES
**              DUE TO 480.7C CORRECTION REPORTING CHANGES - TAG SINAHSN
**
* 4/7/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
** 7/10/17-RUPOLEENA - ADDED LOGIC FOR PARTIAL PAYMENTS AND PLAN-TYPE
**                     EVALUATION FOR IRS FILE - TAG MUKH
** 7/10/17-RUPOLEENA - ADDED LOGIC FOR PARTIAL PAYMENTS AND PLAN-TYPE
**                     EVALUATION FOR IRS FILE - TAG MUKH
** 5/14/18-SAI K     - CONTACT NAME AND E-MAIL ADDRESS CHANGES
** 04/02/19 - VIKRAM - 480.7C.1 : NEW LAYOUT ADDED AND
**     CORRESPONDING  CHANGES IN PA ,SU , INFORMATIVE DETAIL AND SUMMARY
**     RECORD FIELDS (LDA TWRL117C) ADDED FOR FEIN=1 . TAG : VIKRAM
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3553 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private PdaTwratin pdaTwratin;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;
    private LdaTwrl3553 ldaTwrl3553;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Tirf_Irs_Rpt_Ind;
    private DbsField form_U_Tirf_Irs_Rpt_Date;

    private DataAccessProgramView vw_state_Tbl;
    private DbsField state_Tbl_Tircntl_Tbl_Nbr;
    private DbsField state_Tbl_Tircntl_Tax_Year;
    private DbsField state_Tbl_Tircntl_Seq_Nbr;
    private DbsField state_Tbl_Tircntl_State_Old_Code;
    private DbsField state_Tbl_Tircntl_State_Alpha_Code;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_Irsr_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Irsw_Max_Lines;
    private DbsField pnd_Ws_Const_Pnd_Irsg_Max_Lines;
    private DbsField pnd_State_De;

    private DbsGroup pnd_State_De__R_Field_1;
    private DbsField pnd_State_De_Pnd_Tircntl_Tbl_Nbr;
    private DbsField pnd_State_De_Pnd_Tircntl_Tax_Year;
    private DbsField pnd_State_De_Pnd_Tircntl_State_Old_Code;
    private DbsField pnd_Res_Code;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Amended_Date;
    private DbsField pnd_Ws_Pnd_Resubmit_Ind;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Err;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_S3_Start;
    private DbsField pnd_Ws_Pnd_S3_End;
    private DbsField pnd_Ws_Pnd_S4_Start;
    private DbsField pnd_Ws_Pnd_S4_End;
    private DbsField pnd_Ws_Pnd_Company_Line;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Active_Isn;
    private DbsField pnd_Case_Fields_Pnd_Prior_Held;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported;
    private DbsField pnd_Case_Fields_Pnd_Prior_Record;
    private DbsField pnd_Case_Fields_Pnd_No_Change;
    private DbsField pnd_Case_Fields_Pnd_Recon_Summary;
    private DbsField pnd_Case_Fields_Pnd_Recon_Detail;
    private DbsField pnd_Case_Fields_Pnd_Update_Ind;

    private DbsGroup pnd_Report_Indexes;
    private DbsField pnd_Report_Indexes_Pnd_Irsr1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw2;
    private DbsField pnd_Report_Indexes_Pnd_Irsg1;

    private DbsGroup pnd_Irsr;
    private DbsField pnd_Irsr_Pnd_Header1;
    private DbsField pnd_Irsr_Pnd_Header2;

    private DbsGroup pnd_Irsr_Pnd_Totals;
    private DbsField pnd_Irsr_Pnd_Form_Cnt;
    private DbsField pnd_Irsr_Pnd_Gross_Amt;
    private DbsField pnd_Irsr_Pnd_Gross_Amt_Roll;
    private DbsField pnd_Irsr_Pnd_Int_Amt;
    private DbsField pnd_Irsr_Pnd_Ivc_Amt_Roll;
    private DbsField pnd_Irsr_Pnd_Tax_Wthld;

    private DbsGroup pnd_Irsw;
    private DbsField pnd_Irsw_Pnd_Header;

    private DbsGroup pnd_Irsw_Pnd_Totals;
    private DbsField pnd_Irsw_Pnd_Form_Cnt;
    private DbsField pnd_Irsw_Pnd_Gross_Amt;
    private DbsField pnd_Irsw_Pnd_Gross_Amt_Roll;
    private DbsField pnd_Irsw_Pnd_Int_Amt;
    private DbsField pnd_Irsw_Pnd_Ivc_Amt_Roll;
    private DbsField pnd_Irsw_Pnd_Tax_Wthld;

    private DbsGroup pnd_Irsg;
    private DbsField pnd_Irsg_Pnd_Header;

    private DbsGroup pnd_Irsg_Pnd_Totals;
    private DbsField pnd_Irsg_Pnd_Form_Cnt;
    private DbsField pnd_Irsg_Pnd_Gross_Amt;
    private DbsField pnd_Irsg_Pnd_Gross_Amt_Roll;
    private DbsField pnd_Irsg_Pnd_Int_Amt;
    private DbsField pnd_Irsg_Pnd_Ivc_Amt_Roll;
    private DbsField pnd_Irsg_Pnd_Tax_Wthld;

    private DbsGroup pnd_Recon_Case;
    private DbsField pnd_Recon_Case_Pnd_Tin;
    private DbsField pnd_Recon_Case_Pnd_Cntrct_Py;

    private DbsGroup pnd_Recon_Case_Pnd_Recon_Case_Detail;
    private DbsField pnd_Recon_Case_Pnd_Header;
    private DbsField pnd_Recon_Case_Pnd_Form_Cnt;
    private DbsField pnd_Recon_Case_Pnd_Gross_Amt;
    private DbsField pnd_Recon_Case_Pnd_Gross_Amt_Roll;
    private DbsField pnd_Recon_Case_Pnd_Taxable_Amt;
    private DbsField pnd_Recon_Case_Pnd_Int_Amt;
    private DbsField pnd_Recon_Case_Pnd_Ivc_Amt;
    private DbsField pnd_Recon_Case_Pnd_Ivc_Amt_Roll;
    private DbsField pnd_Recon_Case_Pnd_Tax_Wthld;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num;

    private DbsGroup pnd_Work_Area__R_Field_2;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num_N1;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num_N9;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num1;

    private DbsGroup pnd_Work_Area__R_Field_3;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num1_N1;
    private DbsField pnd_Work_Area_Pnd_Temp_Cntl_Num1_N9;
    private DbsField pnd_Work_Area_Pnd_Temp_Zip_Code;

    private DbsGroup pnd_Work_Area__R_Field_4;
    private DbsField pnd_Work_Area_Pnd_Temp_Zip_Code_A5;
    private DbsField pnd_Work_Area_Pnd_Temp_Zip_Code_A4;
    private DbsField pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde;

    private DbsGroup pnd_Work_Area__R_Field_5;
    private DbsField pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_A1;
    private DbsField pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_Filler;
    private DbsField pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig;
    private DbsField pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr;
    private DbsField pnd_Work_Area_Pnd_Ws_Amt_Withld;
    private DbsField pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig;
    private DbsField pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr;
    private DbsField pnd_Work_Area_Pnd_Date;

    private DbsGroup pnd_Work_Area__R_Field_6;
    private DbsField pnd_Work_Area_Pnd_Date_Cc;
    private DbsField pnd_Work_Area_Pnd_Date_Yy;
    private DbsField pnd_Work_Area_Pnd_Date_Mm;
    private DbsField pnd_Work_Area_Pnd_Date_Dd;
    private DbsField pnd_Work_Area_Pnd_4807c_Amt_Dist;
    private DbsField pnd_Work_Area_Pnd_Other;
    private DbsField pnd_Work_Area_Pnd_Yyyymmdd_A;
    private DbsField pnd_Work_Area_Pnd_Ddmmyy_A;

    private DbsGroup pnd_Work_Area__R_Field_7;
    private DbsField pnd_Work_Area_Pnd_Ddmmyy_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());
        ldaTwrl3553 = new LdaTwrl3553();
        registerRecord(ldaTwrl3553);

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Tirf_Irs_Rpt_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_U_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_U_Tirf_Irs_Rpt_Date = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_U_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        registerRecord(vw_form_U);

        vw_state_Tbl = new DataAccessProgramView(new NameInfo("vw_state_Tbl", "STATE-TBL"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        state_Tbl_Tircntl_Tbl_Nbr = vw_state_Tbl.getRecord().newFieldInGroup("state_Tbl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        state_Tbl_Tircntl_Tax_Year = vw_state_Tbl.getRecord().newFieldInGroup("state_Tbl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, 
            RepeatingFieldStrategy.None, "TIRCNTL_TAX_YEAR");
        state_Tbl_Tircntl_Seq_Nbr = vw_state_Tbl.getRecord().newFieldInGroup("state_Tbl_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");
        state_Tbl_Tircntl_State_Old_Code = vw_state_Tbl.getRecord().newFieldInGroup("state_Tbl_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");
        state_Tbl_Tircntl_State_Alpha_Code = vw_state_Tbl.getRecord().newFieldInGroup("state_Tbl_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ALPHA_CODE");
        registerRecord(vw_state_Tbl);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_Irsr_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsr_Max_Lines", "#IRSR-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Const_Pnd_Irsw_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsw_Max_Lines", "#IRSW-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Const_Pnd_Irsg_Max_Lines = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Irsg_Max_Lines", "#IRSG-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_State_De = localVariables.newFieldInRecord("pnd_State_De", "#STATE-DE", FieldType.STRING, 8);

        pnd_State_De__R_Field_1 = localVariables.newGroupInRecord("pnd_State_De__R_Field_1", "REDEFINE", pnd_State_De);
        pnd_State_De_Pnd_Tircntl_Tbl_Nbr = pnd_State_De__R_Field_1.newFieldInGroup("pnd_State_De_Pnd_Tircntl_Tbl_Nbr", "#TIRCNTL-TBL-NBR", FieldType.STRING, 
            1);
        pnd_State_De_Pnd_Tircntl_Tax_Year = pnd_State_De__R_Field_1.newFieldInGroup("pnd_State_De_Pnd_Tircntl_Tax_Year", "#TIRCNTL-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_State_De_Pnd_Tircntl_State_Old_Code = pnd_State_De__R_Field_1.newFieldInGroup("pnd_State_De_Pnd_Tircntl_State_Old_Code", "#TIRCNTL-STATE-OLD-CODE", 
            FieldType.STRING, 3);
        pnd_Res_Code = localVariables.newFieldInRecord("pnd_Res_Code", "#RES-CODE", FieldType.STRING, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Amended_Date = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Amended_Date", "#AMENDED-DATE", FieldType.NUMERIC, 6);
        pnd_Ws_Pnd_Resubmit_Ind = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Resubmit_Ind", "#RESUBMIT-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE, new DbsArrayController(5, 
            6));
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Form_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Err = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Err", "#ERR", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S3_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Start", "#S3-START", FieldType.STRING, 8);
        pnd_Ws_Pnd_S3_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_End", "#S3-END", FieldType.STRING, 8);
        pnd_Ws_Pnd_S4_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_Start", "#S4-START", FieldType.STRING, 32);
        pnd_Ws_Pnd_S4_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_End", "#S4-END", FieldType.STRING, 32);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Active_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Active_Isn", "#ACTIVE-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Case_Fields_Pnd_Prior_Held = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Held", "#PRIOR-HELD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported", "#PRIOR-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Record = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Record", "#PRIOR-RECORD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_No_Change = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Recon_Summary = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Summary", "#RECON-SUMMARY", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Recon_Detail = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Detail", "#RECON-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Update_Ind = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_Ind", "#UPDATE-IND", FieldType.STRING, 1);

        pnd_Report_Indexes = localVariables.newGroupInRecord("pnd_Report_Indexes", "#REPORT-INDEXES");
        pnd_Report_Indexes_Pnd_Irsr1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsr1", "#IRSR1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw1", "#IRSW1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw2 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw2", "#IRSW2", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsg1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsg1", "#IRSG1", FieldType.PACKED_DECIMAL, 7);

        pnd_Irsr = localVariables.newGroupArrayInRecord("pnd_Irsr", "#IRSR", new DbsArrayController(1, 7));
        pnd_Irsr_Pnd_Header1 = pnd_Irsr.newFieldInGroup("pnd_Irsr_Pnd_Header1", "#HEADER1", FieldType.STRING, 19);
        pnd_Irsr_Pnd_Header2 = pnd_Irsr.newFieldInGroup("pnd_Irsr_Pnd_Header2", "#HEADER2", FieldType.STRING, 19);

        pnd_Irsr_Pnd_Totals = pnd_Irsr.newGroupArrayInGroup("pnd_Irsr_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsr_Pnd_Form_Cnt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsr_Pnd_Gross_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsr_Pnd_Gross_Amt_Roll = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Gross_Amt_Roll", "#GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 14, 
            2);
        pnd_Irsr_Pnd_Int_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsr_Pnd_Ivc_Amt_Roll = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Ivc_Amt_Roll", "#IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsr_Pnd_Tax_Wthld = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Irsw = localVariables.newGroupArrayInRecord("pnd_Irsw", "#IRSW", new DbsArrayController(1, 6));
        pnd_Irsw_Pnd_Header = pnd_Irsw.newFieldInGroup("pnd_Irsw_Pnd_Header", "#HEADER", FieldType.STRING, 14);

        pnd_Irsw_Pnd_Totals = pnd_Irsw.newGroupArrayInGroup("pnd_Irsw_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsw_Pnd_Form_Cnt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsw_Pnd_Gross_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsw_Pnd_Gross_Amt_Roll = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Gross_Amt_Roll", "#GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 14, 
            2);
        pnd_Irsw_Pnd_Int_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsw_Pnd_Ivc_Amt_Roll = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Ivc_Amt_Roll", "#IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsw_Pnd_Tax_Wthld = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Irsg = localVariables.newGroupArrayInRecord("pnd_Irsg", "#IRSG", new DbsArrayController(1, 3));
        pnd_Irsg_Pnd_Header = pnd_Irsg.newFieldInGroup("pnd_Irsg_Pnd_Header", "#HEADER", FieldType.STRING, 19);

        pnd_Irsg_Pnd_Totals = pnd_Irsg.newGroupArrayInGroup("pnd_Irsg_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Irsg_Pnd_Form_Cnt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsg_Pnd_Gross_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Irsg_Pnd_Gross_Amt_Roll = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Gross_Amt_Roll", "#GROSS-AMT-ROLL", FieldType.PACKED_DECIMAL, 14, 
            2);
        pnd_Irsg_Pnd_Int_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsg_Pnd_Ivc_Amt_Roll = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Ivc_Amt_Roll", "#IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irsg_Pnd_Tax_Wthld = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Recon_Case = localVariables.newGroupInRecord("pnd_Recon_Case", "#RECON-CASE");
        pnd_Recon_Case_Pnd_Tin = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Tin", "#TIN", FieldType.STRING, 11);
        pnd_Recon_Case_Pnd_Cntrct_Py = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 12);

        pnd_Recon_Case_Pnd_Recon_Case_Detail = pnd_Recon_Case.newGroupArrayInGroup("pnd_Recon_Case_Pnd_Recon_Case_Detail", "#RECON-CASE-DETAIL", new DbsArrayController(1, 
            3));
        pnd_Recon_Case_Pnd_Header = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Header", "#HEADER", FieldType.STRING, 10);
        pnd_Recon_Case_Pnd_Form_Cnt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Recon_Case_Pnd_Gross_Amt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_Gross_Amt_Roll = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Gross_Amt_Roll", "#GROSS-AMT-ROLL", 
            FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Recon_Case_Pnd_Taxable_Amt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Recon_Case_Pnd_Int_Amt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Recon_Case_Pnd_Ivc_Amt = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Recon_Case_Pnd_Ivc_Amt_Roll = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Ivc_Amt_Roll", "#IVC-AMT-ROLL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Recon_Case_Pnd_Tax_Wthld = pnd_Recon_Case_Pnd_Recon_Case_Detail.newFieldInGroup("pnd_Recon_Case_Pnd_Tax_Wthld", "#TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Temp_Cntl_Num = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num", "#TEMP-CNTL-NUM", FieldType.STRING, 10);

        pnd_Work_Area__R_Field_2 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_2", "REDEFINE", pnd_Work_Area_Pnd_Temp_Cntl_Num);
        pnd_Work_Area_Pnd_Temp_Cntl_Num_N1 = pnd_Work_Area__R_Field_2.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num_N1", "#TEMP-CNTL-NUM-N1", FieldType.NUMERIC, 
            1);
        pnd_Work_Area_Pnd_Temp_Cntl_Num_N9 = pnd_Work_Area__R_Field_2.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num_N9", "#TEMP-CNTL-NUM-N9", FieldType.NUMERIC, 
            9);
        pnd_Work_Area_Pnd_Temp_Cntl_Num1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num1", "#TEMP-CNTL-NUM1", FieldType.STRING, 10);

        pnd_Work_Area__R_Field_3 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_3", "REDEFINE", pnd_Work_Area_Pnd_Temp_Cntl_Num1);
        pnd_Work_Area_Pnd_Temp_Cntl_Num1_N1 = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num1_N1", "#TEMP-CNTL-NUM1-N1", FieldType.NUMERIC, 
            1);
        pnd_Work_Area_Pnd_Temp_Cntl_Num1_N9 = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Cntl_Num1_N9", "#TEMP-CNTL-NUM1-N9", FieldType.NUMERIC, 
            9);
        pnd_Work_Area_Pnd_Temp_Zip_Code = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Zip_Code", "#TEMP-ZIP-CODE", FieldType.STRING, 9);

        pnd_Work_Area__R_Field_4 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_4", "REDEFINE", pnd_Work_Area_Pnd_Temp_Zip_Code);
        pnd_Work_Area_Pnd_Temp_Zip_Code_A5 = pnd_Work_Area__R_Field_4.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Zip_Code_A5", "#TEMP-ZIP-CODE-A5", FieldType.STRING, 
            5);
        pnd_Work_Area_Pnd_Temp_Zip_Code_A4 = pnd_Work_Area__R_Field_4.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Zip_Code_A4", "#TEMP-ZIP-CODE-A4", FieldType.STRING, 
            4);
        pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde", "#TEMP-DTL-DIS-CDE", FieldType.STRING, 
            2);

        pnd_Work_Area__R_Field_5 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_5", "REDEFINE", pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde);
        pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_A1 = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_A1", "#TEMP-DTL-DIS-CDE-A1", 
            FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_Filler = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_Filler", "#TEMP-DTL-DIS-CDE-FILLER", 
            FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig", "#WS-NUM-OF-DOC-ORIG", FieldType.NUMERIC, 
            10);
        pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr", "#WS-NUM-OF-DOC-CORR", FieldType.NUMERIC, 
            10);
        pnd_Work_Area_Pnd_Ws_Amt_Withld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amt_Withld", "#WS-AMT-WITHLD", FieldType.NUMERIC, 15, 2);
        pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig", "#WS-AMOUT-PAID-ORIG", FieldType.NUMERIC, 
            15, 2);
        pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr", "#WS-AMOUT-PAID-CORR", FieldType.NUMERIC, 
            15, 2);
        pnd_Work_Area_Pnd_Date = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Work_Area__R_Field_6 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_6", "REDEFINE", pnd_Work_Area_Pnd_Date);
        pnd_Work_Area_Pnd_Date_Cc = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Date_Cc", "#DATE-CC", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Date_Yy = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Date_Yy", "#DATE-YY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Date_Mm = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Date_Mm", "#DATE-MM", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Date_Dd = pnd_Work_Area__R_Field_6.newFieldInGroup("pnd_Work_Area_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_4807c_Amt_Dist = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_4807c_Amt_Dist", "#4807C-AMT-DIST", FieldType.NUMERIC, 11, 
            2);
        pnd_Work_Area_Pnd_Other = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Other", "#OTHER", FieldType.NUMERIC, 11, 2);
        pnd_Work_Area_Pnd_Yyyymmdd_A = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Yyyymmdd_A", "#YYYYMMDD-A", FieldType.STRING, 8);
        pnd_Work_Area_Pnd_Ddmmyy_A = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ddmmyy_A", "#DDMMYY-A", FieldType.STRING, 6);

        pnd_Work_Area__R_Field_7 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_7", "REDEFINE", pnd_Work_Area_Pnd_Ddmmyy_A);
        pnd_Work_Area_Pnd_Ddmmyy_N = pnd_Work_Area__R_Field_7.newFieldInGroup("pnd_Work_Area_Pnd_Ddmmyy_N", "#DDMMYY-N", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();
        vw_state_Tbl.reset();

        ldaTwrl5001.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();
        ldaTwrl3553.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_Irsr_Max_Lines.setInitialValue(7);
        pnd_Ws_Const_Pnd_Irsw_Max_Lines.setInitialValue(6);
        pnd_Ws_Const_Pnd_Irsg_Max_Lines.setInitialValue(3);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Irsr_Pnd_Header1.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsr_Pnd_Header1.getValue(2).setInitialValue("Actve Records");
        pnd_Irsr_Pnd_Header1.getValue(3).setInitialValue("No Change");
        pnd_Irsr_Pnd_Header1.getValue(4).setInitialValue("Non Rept Zero Forms");
        pnd_Irsr_Pnd_Header1.getValue(5).setInitialValue("Prev Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(6).setInitialValue("New  Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(7).setInitialValue("Accepted Forms");
        pnd_Irsr_Pnd_Header2.getValue(2).setInitialValue(" (Prev. Reported)");
        pnd_Irsw_Pnd_Header.getValue(1).setInitialValue("Accepted");
        pnd_Irsw_Pnd_Header.getValue(2).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Irsw_Pnd_Header.getValue(4).setInitialValue("Rejected");
        pnd_Irsw_Pnd_Header.getValue(5).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(6).setInitialValue("Net Change");
        pnd_Irsg_Pnd_Header.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsg_Pnd_Header.getValue(2).setInitialValue("Prev. Reconciled");
        pnd_Irsg_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Recon_Case_Pnd_Header.getValue(1).setInitialValue("New Form");
        pnd_Recon_Case_Pnd_Header.getValue(2).setInitialValue("Original");
        pnd_Recon_Case_Pnd_Header.getValue(3).setInitialValue("Net Change");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3553() throws Exception
    {
        super("Twrp3553");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3553|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  IN MOBIUS, BUT NOT USED. SAME AS 02
                //*  IN MOBIUS, BUT NOT USED. SAME AS 04
                //*  IN MOBIUS, BUT NOT USED. SAME AS 06
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 58 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  CTS
                    //*  DASRAH
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Puerto Rico Correction Reporting Summary' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Rejected Detail' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Puerto Rico Correction Reporting Accepted Detail' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'Puerto Rico Correction Withholding Reconciliation Summary' 120T 'Report: RPT8' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Puerto Rico Correction Withholding Reconciliation' 'Accepted Detail' 120T 'Report: RPT9' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Puerto Rico Correction Withholding Reconciliation' 'Rejected Detail' 118T 'Report:  RPT10' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 42T 'Purto Rico General Ledger Reconciliation Summary' 118T 'Report:  RPT11' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 12 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 12 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T 'Purto Rico General Ledger Reconciliation Detail' 118T 'Report:  RPT12' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYPE ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) / #WS.#COMPANY-LINE //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Form,pnd_Ws_Pnd_Tax_Year,pnd_Ws_Pnd_Amended_Date,pnd_Ws_Pnd_Resubmit_Ind,                   //Natural: INPUT #WS.#FORM #WS.#TAX-YEAR #WS.#AMENDED-DATE #WS.#RESUBMIT-IND #WS.#DATX #WS.#TIMX #WS.#PREV-RPT-DATE ( * )
                    pnd_Ws_Pnd_Datx,pnd_Ws_Pnd_Timx,pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
                //*  CTS
                                                                                                                                                                          //Natural: PERFORM WRITE-SU-RECORD
                sub_Write_Su_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  CTS
                                                                                                                                                                          //Natural: PERFORM WRITE-PA-RECORD
                sub_Write_Pa_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year_A().setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                              //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #TWRAFRMN.#TAX-YEAR-A
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                pnd_Ws_Pnd_S3_Start.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                       //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #WS.#S3-START
                setValueToSubstring("A05",pnd_Ws_Pnd_S3_Start,5,3);                                                                                                       //Natural: MOVE 'A05' TO SUBSTR ( #WS.#S3-START,5,3 )
                pnd_Ws_Pnd_S3_End.setValue(pnd_Ws_Pnd_S3_Start);                                                                                                          //Natural: ASSIGN #WS.#S3-END := #WS.#S3-START
                setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S3_Start,8,1);                                                                                     //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S3-START,8,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S3_End,8,1);                                                                                      //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S3-END,8,1 )
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-3 = #WS.#S3-START THRU #WS.#S3-END
                (
                "RD_FORM",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Ws_Pnd_S3_Start, "And", WcType.BY) ,
                new Wc("TIRF_SUPERDE_3", "<=", pnd_Ws_Pnd_S3_End, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                boolean endOfDataRdForm = true;
                boolean firstRdForm = true;
                RD_FORM:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
                {
                    CheckAtStartofData1266();

                    if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRdForm = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))                                                                                                 //Natural: AT START OF DATA;//Natural: AT BREAK OF FORM.TIRF-COMPANY-CDE;//Natural: AT BREAK OF FORM.TIRF-FORM-TYPE;//Natural: IF #WS.#COMPANY-BREAK
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                        sub_Process_Company();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Report_Indexes_Pnd_Irsr1.setValue(1);                                                                                                             //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                    sub_Accum_Irsr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  ALREADY RECONCILED
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")))                                                                                 //Natural: IF FORM.TIRF-IRS-RPT-IND NE ' '
                    {
                        //*  FOR GENERAL LEDGER
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG
                        sub_Accum_Irsg();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ALREADY REPORTED
                    if (condition(! (ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals(" ") || ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H"))))                          //Natural: IF NOT FORM.TIRF-IRS-RPT-IND = ' ' OR = 'H'
                    {
                        pnd_Report_Indexes_Pnd_Irsr1.setValue(2);                                                                                                         //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 2
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields.resetInitial();                                                                                                                       //Natural: RESET INITIAL #CASE-FIELDS
                    pnd_Case_Fields_Pnd_Active_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                               //Natural: ASSIGN #CASE-FIELDS.#ACTIVE-ISN := *ISN ( RD-FORM. )
                    pnd_Ws_Pnd_S4_End.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());                                                                                     //Natural: ASSIGN #WS.#S4-END := #WS.#S4-START := FORM.TIRF-SUPERDE-4
                    pnd_Ws_Pnd_S4_Start.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());
                                                                                                                                                                          //Natural: PERFORM PRIOR-REPORTED
                    sub_Prior_Reported();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean() && ! (pnd_Case_Fields_Pnd_Prior_Reported.getBoolean())))                             //Natural: IF FORM.TIRF-EMPTY-FORM AND NOT #CASE-FIELDS.#PRIOR-REPORTED
                    {
                        pnd_Report_Indexes_Pnd_Irsr1.setValue(4);                                                                                                         //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 4
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields_Pnd_Prior_Record.setValue(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean());                                                           //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := #CASE-FIELDS.#PRIOR-REPORTED
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                    sub_Setup_Recon_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                       //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                    {
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RECORDS IN ERROR
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                 //Natural: IF FORM.TIRF-IRS-REJECT-IND = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-REJECT
                        sub_Irsr_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-REJECT
                        sub_Irsw_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  OLD REJECTS
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                //Natural: IF FORM.TIRF-IRS-RPT-IND = 'H'
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(5);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  FIRST TIME REJECT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(6);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Case_Fields_Pnd_Update_Ind.setValue("H");                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
                            sub_Update_Form();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))                                                                                       //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED
                    {
                                                                                                                                                                          //Natural: PERFORM COMPARE-IRSR-RECORDS
                        sub_Compare_Irsr_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Case_Fields_Pnd_No_Change.getBoolean()))                                                                                        //Natural: IF #CASE-FIELDS.#NO-CHANGE
                        {
                            pnd_Report_Indexes_Pnd_Irsr1.setValue(3);                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Report_Indexes_Pnd_Irsr1.setValue(7);                                                                                                             //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 7
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                    sub_Accum_Irsr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-ACCEPT
                    sub_Irsr_Detail_Accept();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Case_Fields_Pnd_Update_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
                    sub_Update_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-ACCEPT
                    sub_Irsw_Detail_Accept();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  CTS
                                                                                                                                                                          //Natural: PERFORM WRITE-480-7C-DETAIL-RECORD
                    sub_Write_480_7c_Detail_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd_Form(endOfDataRdForm);
                }
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Ws_Pnd_Accepted_Form_Cnt.equals(getZero())))                                                                                            //Natural: IF #WS.#ACCEPTED-FORM-CNT = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new       //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'Correction Reporting' 77T '***'
                        TabSetting(25),"Correction Reporting",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  VIKRAM
                                                                                                                                                                          //Natural: PERFORM WRITE-TRAILER-RECORD
                sub_Write_Trailer_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  CTS
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-RECORD
                sub_Write_Summary_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ************************
                //*  S U B R O U T I N E S
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSR
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSW
                //* ***************************
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG1
                //* ****************************
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRIOR-REPORTED
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-IRSR-RECORDS
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM
                //* ****************************
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-RECON-CASE
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-C
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-W
                //* **********************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS-G
                //* **********************************
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-REJECT
                //*  '//Taxes/Withheld'         FORM.TIRF-FED-TAX-WTHLD   (HC=R)
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-ACCEPT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-REJECT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-ACCEPT
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSG-DETAIL
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIN
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
                //* ********************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
                //* *************************************                /* CTS
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SU-RECORD
                //* *#TWRL3553-SU-RESUB-IND     := 1
                //*  #TWRL3553-SU-CNTCT-NME     := 'AMALIA FRIAS'
                //*  #TWRL3553-SU-CNTCT-PH-NUM  := '7049884654'
                //*  #TWRL3553-SU-CNTCT-EMAIL     := 'AFRIAS@TIAA-CREF.ORG'
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PA-RECORD
                //*  #TWRL3553-PA-CNTCT-EMAIL   := 'AFRIAS@TIAA-CREF.ORG'
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-480-7C-DETAIL-RECORD
                //* *#TWRL3553-DTL-CNTRL-NUM    := #TEMP-CNTL-NUM-N9
                //* *#TWRL3553-DTL-DOC-TYPE     := 'A'
                //* *IF #WS.#TAX-YEAR GE 2016 AND #TWRL3553-DTL-DIS-FORM= 'A'
                //* *  #TWRL3553-DTL-PL-TYPE      := 'A'
                //* *ELSE
                //*                                                 AMOUNT DISTRIBUTED
                //* *#TWRL3553-DTL-AMND-DATE := ' '
                //*      #TWRL3553-DTL-RSN-FOR-CHG  := FORM.TIRF-PR-ADJ-REASON
                //*      #TEMP-CNTL-NUM1            := FORM.TIRF-PR-ORG-CNTL-NUM
                //*      MOVE RIGHT #TEMP-CNTL-NUM1 TO #TEMP-CNTL-NUM1
                //* *  COMPRESS #DATE-DD #DATE-MM #DATE-YY INTO #TWRL3553-DTL-AMND-DATE
                //* *    LEAVING NO
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENCY-CODE
                //* ************************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-RECORD
                //* *#TWRL3553-SM-DOCUMENT-TYPE := 'A'
                //* *#TWRL3553-SM-NBR-OF-DOCS     := #WS-NUM-OF-DOC
                //* *#TWRL3553-SM-TYPE-OF-TAXPAYER := 'I'
                //* *MOVE *DATN TO #YYYYMMDD-A
                //* *MOVE SUBSTR(#YYYYMMDD-A,7,2) TO SUBSTR(#DDMMYY-A,1,2)
                //* *MOVE SUBSTR(#YYYYMMDD-A,5,2) TO SUBSTR(#DDMMYY-A,3,2)
                //* *MOVE SUBSTR(#YYYYMMDD-A,3,2) TO SUBSTR(#DDMMYY-A,5,2)
                //* *#TWRL3553-SM-AMND-DATE  := #DDMMYY-N
                //* *************************************     /* CTS
                //*  ----------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRAILER-RECORD
                //* **********************************************************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Accum_Irsr() throws Exception                                                                                                                        //Natural: ACCUM-IRSR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pnd_Irsr_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(1);                                                                                           //Natural: ADD 1 TO #IRSR.#FORM-CNT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                       //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSR.#GROSS-AMT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                          //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #IRSR.#GROSS-AMT-ROLL ( #IRSR1,1 )
        pnd_Irsr_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                           //Natural: ADD FORM.TIRF-INT-AMT TO #IRSR.#INT-AMT ( #IRSR1,1 )
        pnd_Irsr_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                              //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #IRSR.#IVC-AMT-ROLL ( #IRSR1,1 )
        pnd_Irsr_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsr1,1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                   //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSR.#TAX-WTHLD ( #IRSR1,1 )
    }
    private void sub_Accum_Irsw() throws Exception                                                                                                                        //Natural: ACCUM-IRSW
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            pnd_Report_Indexes_Pnd_Irsw2.compute(new ComputeParameters(false, pnd_Report_Indexes_Pnd_Irsw2), pnd_Report_Indexes_Pnd_Irsw1.add(pnd_Ws_Pnd_K));             //Natural: ASSIGN #REPORT-INDEXES.#IRSW2 := #REPORT-INDEXES.#IRSW1 + #WS.#K
            pnd_Irsw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K));                                      //Natural: ADD #RECON-CASE.#FORM-CNT ( #K ) TO #IRSW.#FORM-CNT ( #IRSW2,1 )
            pnd_Irsw_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K));                                    //Natural: ADD #RECON-CASE.#GROSS-AMT ( #K ) TO #IRSW.#GROSS-AMT ( #IRSW2,1 )
            pnd_Irsw_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(pnd_Ws_Pnd_K));                          //Natural: ADD #RECON-CASE.#GROSS-AMT-ROLL ( #K ) TO #IRSW.#GROSS-AMT-ROLL ( #IRSW2,1 )
            pnd_Irsw_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_K));                                        //Natural: ADD #RECON-CASE.#INT-AMT ( #K ) TO #IRSW.#INT-AMT ( #IRSW2,1 )
            pnd_Irsw_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(pnd_Ws_Pnd_K));                              //Natural: ADD #RECON-CASE.#IVC-AMT-ROLL ( #K ) TO #IRSW.#IVC-AMT-ROLL ( #IRSW2,1 )
            pnd_Irsw_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsw2,1).nadd(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K));                                    //Natural: ADD #RECON-CASE.#TAX-WTHLD ( #K ) TO #IRSW.#TAX-WTHLD ( #IRSW2,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Irsg() throws Exception                                                                                                                        //Natural: ACCUM-IRSG
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                                                                           //Natural: IF FORM.TIRF-FED-TAX-WTHLD = 0.00
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Irsg_Pnd_Form_Cnt.getValue(1,":",2,1).nadd(1);                                                                                                                //Natural: ADD 1 TO #IRSG.#FORM-CNT ( 1:2,1 )
        pnd_Irsg_Pnd_Gross_Amt.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                            //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSG.#GROSS-AMT ( 1:2,1 )
        pnd_Irsg_Pnd_Gross_Amt_Roll.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                               //Natural: ADD FORM.TIRF-PR-GROSS-AMT-ROLL TO #IRSG.#GROSS-AMT-ROLL ( 1:2,1 )
        pnd_Irsg_Pnd_Int_Amt.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #IRSG.#INT-AMT ( 1:2,1 )
        pnd_Irsg_Pnd_Ivc_Amt_Roll.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                   //Natural: ADD FORM.TIRF-PR-IVC-AMT-ROLL TO #IRSG.#IVC-AMT-ROLL ( 1:2,1 )
        pnd_Irsg_Pnd_Tax_Wthld.getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSG.#TAX-WTHLD ( 1:2,1 )
    }
    private void sub_Accum_Irsg1() throws Exception                                                                                                                       //Natural: ACCUM-IRSG1
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #REPORT-INDEXES.#IRSG1 = 1 TO 3
        for (pnd_Report_Indexes_Pnd_Irsg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsg1.lessOrEqual(3)); pnd_Report_Indexes_Pnd_Irsg1.nadd(1))
        {
            pnd_Irsg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1));                      //Natural: ADD #RECON-CASE.#FORM-CNT ( #IRSG1 ) TO #IRSG.#FORM-CNT ( #IRSG1,1 )
            pnd_Irsg_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1));                    //Natural: ADD #RECON-CASE.#GROSS-AMT ( #IRSG1 ) TO #IRSG.#GROSS-AMT ( #IRSG1,1 )
            pnd_Irsg_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1));          //Natural: ADD #RECON-CASE.#GROSS-AMT-ROLL ( #IRSG1 ) TO #IRSG.#GROSS-AMT-ROLL ( #IRSG1,1 )
            pnd_Irsg_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1));                        //Natural: ADD #RECON-CASE.#INT-AMT ( #IRSG1 ) TO #IRSG.#INT-AMT ( #IRSG1,1 )
            pnd_Irsg_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1));              //Natural: ADD #RECON-CASE.#IVC-AMT-ROLL ( #IRSG1 ) TO #IRSG.#IVC-AMT-ROLL ( #IRSG1,1 )
            pnd_Irsg_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsg1,1).nadd(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Report_Indexes_Pnd_Irsg1));                    //Natural: ADD #RECON-CASE.#TAX-WTHLD ( #IRSG1 ) TO #IRSG.#TAX-WTHLD ( #IRSG1,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Prior_Reported() throws Exception                                                                                                                    //Natural: PRIOR-REPORTED
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S4_Start,32,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S4-START,32,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S4_End,32,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S4-END,32,1 )
        ldaTwrl9715.getVw_form_R().startDatabaseRead                                                                                                                      //Natural: READ FORM-R BY FORM-R.TIRF-SUPERDE-4 = #WS.#S4-START THRU #WS.#S4-END WHERE FORM-R.TIRF-COMPANY-CDE = FORM.TIRF-COMPANY-CDE
        (
        "RD1_FORM",
        new Wc[] { new Wc("TIRF_COMPANY_CDE", "=", ldaTwrl9710.getForm_Tirf_Company_Cde(), WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_4", ">=", pnd_Ws_Pnd_S4_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_4", "<=", pnd_Ws_Pnd_S4_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") }
        );
        RD1_FORM:
        while (condition(ldaTwrl9715.getVw_form_R().readNextRow("RD1_FORM")))
        {
            if (condition(ldaTwrl9715.getForm_R_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                          //Natural: IF FORM-R.TIRF-IRS-RPT-IND = 'H'
            {
                if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                           //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                {
                    pnd_Case_Fields_Pnd_Prior_Held.setValue(true);                                                                                                        //Natural: ASSIGN #CASE-FIELDS.#PRIOR-HELD := TRUE
                    //*  INACTIVE
                    if (condition(ldaTwrl9715.getForm_R_Tirf_Active_Ind().equals(" ")))                                                                                   //Natural: IF FORM-R.TIRF-ACTIVE-IND = ' '
                    {
                        pnd_Case_Fields_Pnd_Prior_Record.setValue(true);                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                        sub_Setup_Recon_Case();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Case_Fields_Pnd_Prior_Reported.getBoolean())))                                                                                       //Natural: IF NOT #CASE-FIELDS.#PRIOR-REPORTED
                {
                    pnd_Case_Fields_Pnd_Prior_Reported.setValue(true);                                                                                                    //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Compare_Irsr_Records() throws Exception                                                                                                              //Natural: COMPARE-IRSR-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(ldaTwrl9715.getForm_R_Tirf_Company_Cde())  //Natural: IF FORM.TIRF-TAX-ID-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR FORM.TIRF-COMPANY-CDE NE FORM-R.TIRF-COMPANY-CDE OR FORM.TIRF-PARTICIPANT-NAME NE FORM-R.TIRF-PARTICIPANT-NAME OR FORM.TIRF-ADDR-LN1 NE FORM-R.TIRF-ADDR-LN1 OR FORM.TIRF-ADDR-LN2 NE FORM-R.TIRF-ADDR-LN2 OR FORM.TIRF-ADDR-LN3 NE FORM-R.TIRF-ADDR-LN3 OR FORM.TIRF-ADDR-LN4 NE FORM-R.TIRF-ADDR-LN4 OR FORM.TIRF-ADDR-LN5 NE FORM-R.TIRF-ADDR-LN5 OR FORM.TIRF-ADDR-LN6 NE FORM-R.TIRF-ADDR-LN6 OR FORM.TIRF-GROSS-AMT NE FORM-R.TIRF-GROSS-AMT OR FORM.TIRF-PR-GROSS-AMT-ROLL NE FORM-R.TIRF-PR-GROSS-AMT-ROLL OR FORM.TIRF-TAXABLE-AMT NE FORM-R.TIRF-TAXABLE-AMT OR FORM.TIRF-INT-AMT NE FORM-R.TIRF-INT-AMT OR FORM.TIRF-IVC-AMT NE FORM-R.TIRF-IVC-AMT OR FORM.TIRF-PR-IVC-AMT-ROLL NE FORM-R.TIRF-PR-IVC-AMT-ROLL OR FORM.TIRF-FED-TAX-WTHLD NE FORM-R.TIRF-FED-TAX-WTHLD
            || ldaTwrl9710.getForm_Tirf_Participant_Name().notEquals(ldaTwrl9715.getForm_R_Tirf_Participant_Name()) || ldaTwrl9710.getForm_Tirf_Addr_Ln1().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln1()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln2().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln2()) || ldaTwrl9710.getForm_Tirf_Addr_Ln3().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln3()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln4().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln4()) || ldaTwrl9710.getForm_Tirf_Addr_Ln5().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln5()) 
            || ldaTwrl9710.getForm_Tirf_Addr_Ln6().notEquals(ldaTwrl9715.getForm_R_Tirf_Addr_Ln6()) || ldaTwrl9710.getForm_Tirf_Gross_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Gross_Amt()) 
            || ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll().notEquals(ldaTwrl9715.getForm_R_Tirf_Pr_Gross_Amt_Roll()) || ldaTwrl9710.getForm_Tirf_Taxable_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Taxable_Amt()) 
            || ldaTwrl9710.getForm_Tirf_Int_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Int_Amt()) || ldaTwrl9710.getForm_Tirf_Ivc_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Ivc_Amt()) 
            || ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll().notEquals(ldaTwrl9715.getForm_R_Tirf_Pr_Ivc_Amt_Roll()) || ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld())))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_No_Change.setValue(true);                                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#NO-CHANGE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Form() throws Exception                                                                                                                       //Natural: UPDATE-FORM
    {
        if (BLNatReinput.isReinput()) return;

        G_FORM:                                                                                                                                                           //Natural: GET FORM-U #CASE-FIELDS.#ACTIVE-ISN
        vw_form_U.readByID(pnd_Case_Fields_Pnd_Active_Isn.getLong(), "G_FORM");
        form_U_Tirf_Irs_Rpt_Ind.setValue(pnd_Case_Fields_Pnd_Update_Ind);                                                                                                 //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := #CASE-FIELDS.#UPDATE-IND
        form_U_Tirf_Irs_Rpt_Date.setValue(pnd_Ws_Pnd_Datx);                                                                                                               //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #WS.#DATX
        form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                              //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
        form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
        vw_form_U.updateDBRow("G_FORM");                                                                                                                                  //Natural: UPDATE ( G-FORM. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Setup_Recon_Case() throws Exception                                                                                                                  //Natural: SETUP-RECON-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Case_Fields_Pnd_Recon_Summary.reset();                                                                                                                        //Natural: RESET #CASE-FIELDS.#RECON-SUMMARY #CASE-FIELDS.#RECON-DETAIL
        pnd_Case_Fields_Pnd_Recon_Detail.reset();
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                        //Natural: IF #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD = 0.00
        {
            pnd_Case_Fields_Pnd_Prior_Record.reset();                                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(new DbsDecimal("0.00")) || pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().notEquals(new  //Natural: IF FORM.TIRF-FED-TAX-WTHLD NE 0.00 OR #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Case_Fields_Pnd_Recon_Summary.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#RECON-SUMMARY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case.resetInitial();                                                                                                                                    //Natural: RESET INITIAL #RECON-CASE
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Tin.setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                                               //Natural: ASSIGN #RECON-CASE.#TIN := TWRATIN.#O-TIN
        pnd_Recon_Case_Pnd_Cntrct_Py.setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));                                            //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY
        setValueToSubstring(ldaTwrl9710.getForm_Tirf_Payee_Cde(),pnd_Recon_Case_Pnd_Cntrct_Py,11,2);                                                                      //Natural: MOVE FORM.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY,11,2 )
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).nadd(1);                                                                                                                  //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 1 )
        pnd_Recon_Case_Pnd_Gross_Amt.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                          //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 1 ) := FORM.TIRF-GROSS-AMT
        pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                                             //Natural: ASSIGN #RECON-CASE.#GROSS-AMT-ROLL ( 1 ) := FORM.TIRF-PR-GROSS-AMT-ROLL
        pnd_Recon_Case_Pnd_Taxable_Amt.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                                      //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 1 ) := FORM.TIRF-TAXABLE-AMT
        pnd_Recon_Case_Pnd_Int_Amt.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                              //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 1 ) := FORM.TIRF-INT-AMT
        pnd_Recon_Case_Pnd_Ivc_Amt.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                                              //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 1 ) := FORM.TIRF-IVC-AMT
        pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                                                 //Natural: ASSIGN #RECON-CASE.#IVC-AMT-ROLL ( 1 ) := FORM.TIRF-PR-IVC-AMT-ROLL
        pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                                      //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 1 ) := FORM.TIRF-FED-TAX-WTHLD
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#PRIOR-RECORD
        {
            pnd_Recon_Case_Pnd_Form_Cnt.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 2 )
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Gross_Amt());                                                                    //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 2 ) := FORM-R.TIRF-GROSS-AMT
            pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Pr_Gross_Amt_Roll());                                                       //Natural: ASSIGN #RECON-CASE.#GROSS-AMT-ROLL ( 2 ) := FORM-R.TIRF-PR-GROSS-AMT-ROLL
            pnd_Recon_Case_Pnd_Taxable_Amt.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Taxable_Amt());                                                                //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 2 ) := FORM-R.TIRF-TAXABLE-AMT
            pnd_Recon_Case_Pnd_Int_Amt.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Int_Amt());                                                                        //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 2 ) := FORM-R.TIRF-INT-AMT
            pnd_Recon_Case_Pnd_Ivc_Amt.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Ivc_Amt());                                                                        //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 2 ) := FORM-R.TIRF-IVC-AMT
            pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Pr_Ivc_Amt_Roll());                                                           //Natural: ASSIGN #RECON-CASE.#IVC-AMT-ROLL ( 2 ) := FORM-R.TIRF-PR-IVC-AMT-ROLL
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld());                                                                //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 2 ) := FORM-R.TIRF-FED-TAX-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Form_Cnt.getValue(3)), pnd_Recon_Case_Pnd_Form_Cnt.getValue(1).subtract(pnd_Recon_Case_Pnd_Form_Cnt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#FORM-CNT ( 3 ) := #RECON-CASE.#FORM-CNT ( 1 ) - #RECON-CASE.#FORM-CNT ( 2 )
        if (condition(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).equals(new DbsDecimal("0.00"))))                                                                           //Natural: IF #RECON-CASE.#TAX-WTHLD ( 1 ) = 0.00
        {
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 2 ) * -1
            pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT-ROLL ( 3 ) := #RECON-CASE.#GROSS-AMT-ROLL ( 2 ) * -1
            pnd_Recon_Case_Pnd_Taxable_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Taxable_Amt.getValue(3)), pnd_Recon_Case_Pnd_Taxable_Amt.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 3 ) := #RECON-CASE.#TAXABLE-AMT ( 2 ) * -1
            pnd_Recon_Case_Pnd_Int_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Int_Amt.getValue(3)), pnd_Recon_Case_Pnd_Int_Amt.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 3 ) := #RECON-CASE.#INT-AMT ( 2 ) * -1
            pnd_Recon_Case_Pnd_Ivc_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Ivc_Amt.getValue(3)), pnd_Recon_Case_Pnd_Ivc_Amt.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 3 ) := #RECON-CASE.#IVC-AMT ( 2 ) * -1
            pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(3)), pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#IVC-AMT-ROLL ( 3 ) := #RECON-CASE.#IVC-AMT-ROLL ( 2 ) * -1
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3)), pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2).multiply(-1)); //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 3 ) := #RECON-CASE.#TAX-WTHLD ( 2 ) * -1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt.getValue(1).subtract(pnd_Recon_Case_Pnd_Gross_Amt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 1 ) - #RECON-CASE.#GROSS-AMT ( 2 )
            pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(3)), pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(1).subtract(pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(2))); //Natural: ASSIGN #RECON-CASE.#GROSS-AMT-ROLL ( 3 ) := #RECON-CASE.#GROSS-AMT-ROLL ( 1 ) - #RECON-CASE.#GROSS-AMT-ROLL ( 2 )
            pnd_Recon_Case_Pnd_Taxable_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Taxable_Amt.getValue(3)), pnd_Recon_Case_Pnd_Taxable_Amt.getValue(1).subtract(pnd_Recon_Case_Pnd_Taxable_Amt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 3 ) := #RECON-CASE.#TAXABLE-AMT ( 1 ) - #RECON-CASE.#TAXABLE-AMT ( 2 )
            pnd_Recon_Case_Pnd_Int_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Int_Amt.getValue(3)), pnd_Recon_Case_Pnd_Int_Amt.getValue(1).subtract(pnd_Recon_Case_Pnd_Int_Amt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 3 ) := #RECON-CASE.#INT-AMT ( 1 ) - #RECON-CASE.#INT-AMT ( 2 )
            pnd_Recon_Case_Pnd_Ivc_Amt.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Ivc_Amt.getValue(3)), pnd_Recon_Case_Pnd_Ivc_Amt.getValue(1).subtract(pnd_Recon_Case_Pnd_Ivc_Amt.getValue(2))); //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 3 ) := #RECON-CASE.#IVC-AMT ( 1 ) - #RECON-CASE.#IVC-AMT ( 2 )
            pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(3)), pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(1).subtract(pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(2))); //Natural: ASSIGN #RECON-CASE.#IVC-AMT-ROLL ( 3 ) := #RECON-CASE.#IVC-AMT-ROLL ( 1 ) - #RECON-CASE.#IVC-AMT-ROLL ( 2 )
            pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).compute(new ComputeParameters(false, pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3)), pnd_Recon_Case_Pnd_Tax_Wthld.getValue(1).subtract(pnd_Recon_Case_Pnd_Tax_Wthld.getValue(2))); //Natural: ASSIGN #RECON-CASE.#TAX-WTHLD ( 3 ) := #RECON-CASE.#TAX-WTHLD ( 1 ) - #RECON-CASE.#TAX-WTHLD ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Recon_Case_Pnd_Form_Cnt.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Gross_Amt.getValue(3).equals(new DbsDecimal("0.00"))   //Natural: IF #RECON-CASE.#FORM-CNT ( 3 ) = 0.00 AND #RECON-CASE.#GROSS-AMT ( 3 ) = 0.00 AND #RECON-CASE.#GROSS-AMT-ROLL ( 3 ) = 0.00 AND #RECON-CASE.#TAXABLE-AMT ( 3 ) = 0.00 AND #RECON-CASE.#INT-AMT ( 3 ) = 0.00 AND #RECON-CASE.#IVC-AMT ( 3 ) = 0.00 AND #RECON-CASE.#IVC-AMT-ROLL ( 3 ) = 0.00 AND #RECON-CASE.#TAX-WTHLD ( 3 ) = 0.00
            && pnd_Recon_Case_Pnd_Gross_Amt_Roll.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Taxable_Amt.getValue(3).equals(new DbsDecimal("0.00")) 
            && pnd_Recon_Case_Pnd_Int_Amt.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Ivc_Amt.getValue(3).equals(new DbsDecimal("0.00")) 
            && pnd_Recon_Case_Pnd_Ivc_Amt_Roll.getValue(3).equals(new DbsDecimal("0.00")) && pnd_Recon_Case_Pnd_Tax_Wthld.getValue(3).equals(new DbsDecimal("0.00"))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_Recon_Detail.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#RECON-DETAIL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Summary_Reports() throws Exception                                                                                                                   //Natural: SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS-C
        sub_Summary_Reports_C();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //* *PERFORM SUMMARY-REPORTS-W
        //* *PERFORM SUMMARY-REPORTS-G
    }
    private void sub_Summary_Reports_C() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-C
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 4 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #IRSR1 = 1 TO #IRSR-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsr1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsr1.lessOrEqual(pnd_Ws_Const_Pnd_Irsr_Max_Lines)); pnd_Report_Indexes_Pnd_Irsr1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Irsr1.equals(7)))                                                                                                        //Natural: IF #IRSR1 = 7
            {
                getReports().write(2, "=",new RepeatItem(117));                                                                                                           //Natural: WRITE ( 2 ) '=' ( 117 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 2 ) ( ES = ON HC = R ) '/' #IRSR.#HEADER1 ( #IRSR1 ) / '/' #IRSR.#HEADER2 ( #IRSR1 ) 'Form/Count' #IRSR.#FORM-CNT ( #IRSR1,#S2 ) 'Amount/Distributed' #IRSR.#GROSS-AMT ( #IRSR1,#S2 ) 'Interest/Amount' #IRSR.#INT-AMT ( #IRSR1,#S2 ) 'Rollover/Distribution' #IRSR.#GROSS-AMT-ROLL ( #IRSR1,#S2 ) 'Rollover/Contribution' #IRSR.#IVC-AMT-ROLL ( #IRSR1,#S2 )
            		pnd_Irsr_Pnd_Header1.getValue(pnd_Report_Indexes_Pnd_Irsr1),NEWLINE,"/",
            		pnd_Irsr_Pnd_Header2.getValue(pnd_Report_Indexes_Pnd_Irsr1),"Form/Count",
            		pnd_Irsr_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"Amount/Distributed",
            		pnd_Irsr_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"Interest/Amount",
            		pnd_Irsr_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"Rollover/Distribution",
            		pnd_Irsr_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2),"Rollover/Contribution",
            		pnd_Irsr_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsr1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '/Taxes Withheld'       #IRSR.#TAX-WTHLD     (#IRSR1,#S2)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_W() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-W
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(8));                                                                                                                 //Natural: NEWPAGE ( 8 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(9));                                                                                                                 //Natural: NEWPAGE ( 9 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(10));                                                                                                                //Natural: NEWPAGE ( 10 )
        if (condition(Global.isEscape())){return;}
        FOR04:                                                                                                                                                            //Natural: FOR #IRSW1 = 1 TO #IRSW-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsw1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsw1.lessOrEqual(pnd_Ws_Const_Pnd_Irsw_Max_Lines)); pnd_Report_Indexes_Pnd_Irsw1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Irsw1.equals(3) || pnd_Report_Indexes_Pnd_Irsw1.equals(6)))                                                              //Natural: IF #IRSW1 = 3 OR = 6
            {
                getReports().write(8, "=",new RepeatItem(112));                                                                                                           //Natural: WRITE ( 8 ) '=' ( 112 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                         //Natural: DISPLAY ( 8 ) ( ES = ON HC = R ) '/' #IRSW.#HEADER ( #IRSW1 ) 'Form/Count' #IRSW.#FORM-CNT ( #IRSW1,#S2 ) 'Amount/Distributed' #IRSW.#GROSS-AMT ( #IRSW1,#S2 ) 'Interest/Amount' #IRSW.#INT-AMT ( #IRSW1,#S2 ) 'Rollover/Distribution' #IRSW.#GROSS-AMT-ROLL ( #IRSW1,#S2 ) 'Rollover/Contribution' #IRSW.#IVC-AMT-ROLL ( #IRSW1,#S2 )
            		pnd_Irsw_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Irsw1),"Form/Count",
            		pnd_Irsw_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"Amount/Distributed",
            		pnd_Irsw_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"Interest/Amount",
            		pnd_Irsw_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"Rollover/Distribution",
            		pnd_Irsw_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2),"Rollover/Contribution",
            		pnd_Irsw_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsw1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '/Taxes Withheld'       #IRSW.#TAX-WTHLD     (#IRSW1,#S2)
            if (condition(pnd_Report_Indexes_Pnd_Irsw1.equals(3)))                                                                                                        //Natural: IF #IRSW1 = 3
            {
                getReports().skip(8, 4);                                                                                                                                  //Natural: SKIP ( 8 ) 4 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports_G() throws Exception                                                                                                                 //Natural: SUMMARY-REPORTS-G
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(11));                                                                                                                //Natural: NEWPAGE ( 11 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(12));                                                                                                                //Natural: NEWPAGE ( 12 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #IRSG1 = 1 TO #IRSG-MAX-LINES
        for (pnd_Report_Indexes_Pnd_Irsg1.setValue(1); condition(pnd_Report_Indexes_Pnd_Irsg1.lessOrEqual(pnd_Ws_Const_Pnd_Irsg_Max_Lines)); pnd_Report_Indexes_Pnd_Irsg1.nadd(1))
        {
            if (condition(pnd_Report_Indexes_Pnd_Irsg1.equals(3)))                                                                                                        //Natural: IF #IRSG1 = 3
            {
                getReports().write(11, "=",new RepeatItem(117));                                                                                                          //Natural: WRITE ( 11 ) '=' ( 117 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(11, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                        //Natural: DISPLAY ( 11 ) ( ES = ON HC = R ) '/' #IRSG.#HEADER ( #IRSG1 ) 'Form/Count' #IRSG.#FORM-CNT ( #IRSG1,#S2 ) 'Amount/Distributed' #IRSG.#GROSS-AMT ( #IRSG1,#S2 ) 'Interest/Amount' #IRSG.#INT-AMT ( #IRSG1,#S2 ) 'Rollover/Distribution' #IRSG.#GROSS-AMT-ROLL ( #IRSG1,#S2 ) 'Rollover/Contribution' #IRSG.#IVC-AMT-ROLL ( #IRSG1,#S2 )
            		pnd_Irsg_Pnd_Header.getValue(pnd_Report_Indexes_Pnd_Irsg1),"Form/Count",
            		pnd_Irsg_Pnd_Form_Cnt.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"Amount/Distributed",
            		pnd_Irsg_Pnd_Gross_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"Interest/Amount",
            		pnd_Irsg_Pnd_Int_Amt.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"Rollover/Distribution",
            		pnd_Irsg_Pnd_Gross_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2),"Rollover/Contribution",
            		pnd_Irsg_Pnd_Ivc_Amt_Roll.getValue(pnd_Report_Indexes_Pnd_Irsg1,pnd_Ws_Pnd_S2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    '/Taxes Withheld'       #IRSG.#TAX-WTHLD     (#IRSG1,#S2)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Irsr_Detail_Reject() throws Exception                                                                                                                //Natural: IRSR-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(4, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 4 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '/Dist/Code' FORM.TIRF-DISTRIBUTION-CDE ( HC = R LC = � ) '/Amount Distributed' FORM.TIRF-GROSS-AMT ( HC = R ) / 'IVC Amount' FORM.TIRF-IVC-AMT ( HC = R LC = �� ) / 'Taxable Amount' FORM.TIRF-TAXABLE-AMT ( HC = R LC = �� ) '/Interest Amount' FORM.TIRF-INT-AMT ( HC = R LC = �� ) / 'Rollover Distribution' FORM.TIRF-PR-GROSS-AMT-ROLL ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) / 'Rollover Contribution' FORM.TIRF-PR-IVC-AMT-ROLL ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
        		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=�"),
            "/Amount Distributed",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"IVC Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Taxable Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),"/Interest Amount",
            
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Rollover Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,"Rollover Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO C*FORM.TIRF-SYS-ERR
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Err.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Ws_Pnd_I));                                                                           //Natural: ASSIGN #WS.#ERR := FORM.TIRF-SYS-ERR ( #I )
            getReports().write(4, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Participant_Name()),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ws_Pnd_Err.getInt() + 1)); //Natural: WRITE ( 4 ) T*FORM.TIRF-PARTICIPANT-NAME #TWRL5001.#ERR-DESC ( #ERR )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 4 ) 1 LINES
    }
    private void sub_Irsr_Detail_Accept() throws Exception                                                                                                                //Natural: IRSR-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(6, new ReportEmptyLineSuppression(true),"///TIN",                                                                                            //Natural: DISPLAY ( 6 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN / '/' TWRATIN.#O-TIN-TYPE-DESC '///Contract' FORM.TIRF-CONTRACT-NBR 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L ) '/Dist/Code' FORM.TIRF-DISTRIBUTION-CDE ( HC = R LC = � ) '/Amount Distributed' FORM.TIRF-GROSS-AMT ( HC = R ) / 'IVC Amount' FORM.TIRF-IVC-AMT ( HC = R LC = �� ) / 'Taxable Amount' FORM.TIRF-TAXABLE-AMT ( HC = R LC = �� ) '/Interest Amount' FORM.TIRF-INT-AMT ( HC = R LC = �� ) / 'Rollover Distribution' FORM.TIRF-PR-GROSS-AMT-ROLL ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 ) / 'Rollover Contribution' FORM.TIRF-PR-IVC-AMT-ROLL ( HC = R EM = -ZZZ,ZZZ,ZZ9.99 )
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
        		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=�"),
            "/Amount Distributed",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"IVC Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Taxable Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),"/Interest Amount",
            
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Rollover Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,"Rollover Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  '//Taxes/Withheld'         FORM.TIRF-FED-TAX-WTHLD   (HC=R)
        getReports().skip(6, 1);                                                                                                                                          //Natural: SKIP ( 6 ) 1 LINES
    }
    private void sub_Irsw_Detail_Reject() throws Exception                                                                                                                //Natural: IRSW-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Irsw1.setValue(3);                                                                                                                     //Natural: ASSIGN #REPORT-INDEXES.#IRSW1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 10 )
            FOR07:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(10, "/TIN",                                                                                                                          //Natural: DISPLAY ( 10 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(10, 1);                                                                                                                             //Natural: SKIP ( 10 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsw_Detail_Accept() throws Exception                                                                                                                //Natural: IRSW-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            pnd_Report_Indexes_Pnd_Irsw1.reset();                                                                                                                         //Natural: RESET #REPORT-INDEXES.#IRSW1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 09 )
            FOR08:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(9, "/TIN",                                                                                                                           //Natural: DISPLAY ( 09 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(9, 1);                                                                                                                              //Natural: SKIP ( 09 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsg_Detail() throws Exception                                                                                                                       //Natural: IRSG-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG1
            sub_Accum_Irsg1();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Recon_Detail.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#RECON-DETAIL
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 12 )
            FOR09:                                                                                                                                                        //Natural: FOR #WS.#K = 1 TO 3
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                               //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(12, "/TIN",                                                                                                                          //Natural: DISPLAY ( 12 ) '/TIN' #RECON-CASE.#TIN ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Amount Paid' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) 'Amount/Withheld' #RECON-CASE.#TAX-WTHLD ( #K ) ( HC = R )
                		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
                		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
                		pnd_Recon_Case_Pnd_Header.getValue(pnd_Ws_Pnd_K),"/Form Count",
                		pnd_Recon_Case_Pnd_Form_Cnt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
                    
                		pnd_Recon_Case_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
                    
                		pnd_Recon_Case_Pnd_Tax_Wthld.getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(12, 1);                                                                                                                             //Natural: SKIP ( 12 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Tin() throws Exception                                                                                                                       //Natural: PROCESS-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    //*  BK
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                     //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM.TIRF-COMPANY-CDE
        pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year()), ldaTwrl9710.getForm_Tirf_Tax_Year().val()); //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                     //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                              //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
        setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                            //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
        //*  EB
        setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                                   //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
        pnd_Ws_Pnd_Company_Break.reset();                                                                                                                                 //Natural: RESET #WS.#COMPANY-BREAK
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                       //Natural: TERMINATE #WS.#TERMINATE
    }
    private void sub_Write_Su_Record() throws Exception                                                                                                                   //Natural: WRITE-SU-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //* DASRAH
        //*  SK
        //*  SK
        //*  SK
        //*   FOR US POSTAL SERVICE
        //*  VIKRAM
        ldaTwrl3553.getPnd_Twrl3553c().reset();                                                                                                                           //Natural: RESET #TWRL3553C
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In().setValue("SU");                                                                                             //Natural: ASSIGN #TWRL3553-SU-REC-IN := 'SU'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein().setValue(131624203);                                                                                       //Natural: ASSIGN #TWRL3553-SU-SUB-EIN := 131624203
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind().setValue(pnd_Ws_Pnd_Resubmit_Ind);                                                                       //Natural: ASSIGN #TWRL3553-SU-RESUB-IND := #WS.#RESUBMIT-IND
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde().setValue(98);                                                                                               //Natural: ASSIGN #TWRL3553-SU-SW-CDE := 98
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                          //Natural: ASSIGN #TWRL3553-SU-CMP-NME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add().setValue("8500 ANDREW CARNEGIE BLVD");                                                                     //Natural: ASSIGN #TWRL3553-SU-LOC-ADD := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add().setValue("8500 ANDREW CARNEGIE BLVD");                                                                     //Natural: ASSIGN #TWRL3553-SU-DEL-ADD := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_City().setValue("CHARLOTTE");                                                                                        //Natural: ASSIGN #TWRL3553-SU-CITY := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr().setValue("NC");                                                                                            //Natural: ASSIGN #TWRL3553-SU-ST-ABBR := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip().setValue(28262);                                                                                               //Natural: ASSIGN #TWRL3553-SU-ZIP := 28262
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn().setValue(8500);                                                                                           //Natural: ASSIGN #TWRL3553-SU-ZIP-EXTN := 8500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                          //Natural: ASSIGN #TWRL3553-SU-SUB-NME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr().setValue("8500 ANDREW CARNEGIE BLVD");                                                                    //Natural: ASSIGN #TWRL3553-SU-LOC-ADDR := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr().setValue("8500 ANDREW CARNEGIE BLVD");                                                                    //Natural: ASSIGN #TWRL3553-SU-DEL-ADDR := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_City2().setValue("CHARLOTTE");                                                                                       //Natural: ASSIGN #TWRL3553-SU-CITY2 := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_State().setValue("NC");                                                                                              //Natural: ASSIGN #TWRL3553-SU-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2().setValue(28262);                                                                                              //Natural: ASSIGN #TWRL3553-SU-ZIP2 := 28262
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2().setValue(8500);                                                                                          //Natural: ASSIGN #TWRL3553-SU-ZIP-EXTN2 := 8500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme().setValue("KRISTINE L LEVINE");                                                                           //Natural: ASSIGN #TWRL3553-SU-CNTCT-NME := 'KRISTINE L LEVINE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num().setValue("7049882344");                                                                               //Natural: ASSIGN #TWRL3553-SU-CNTCT-PH-NUM := '7049882344'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email().setValue("Kristine.Levine@tiaa.org");                                                                  //Natural: ASSIGN #TWRL3553-SU-CNTCT-EMAIL := 'Kristine.Levine@tiaa.org'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde().setValue(2);                                                                                       //Natural: ASSIGN #TWRL3553-SU-PRF-PRB-NTF-CDE := 2
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde().setValue("P");                                                                                            //Natural: ASSIGN #TWRL3553-SU-PREP-CDE := 'P'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ().setValue("1");                                                                                   //Natural: ASSIGN #TWRL3553-SU-SUBMITR-IDENT-TYP := '1'
        getWorkFiles().write(1, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 1 #TWRL3553C
        //*  DASRAH
        getWorkFiles().write(2, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 2 #TWRL3553C
        //*  WRITE-SU-RECORD
    }
    private void sub_Write_Pa_Record() throws Exception                                                                                                                   //Natural: WRITE-PA-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  FORM 480.7C
        //*  SK
        //*  VIKRAM
        ldaTwrl3553.getPnd_Twrl3553c().reset();                                                                                                                           //Natural: RESET #TWRL3553C
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id().setValue("PA");                                                                                             //Natural: ASSIGN #TWRL3553-PA-REC-ID := 'PA'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                            //Natural: ASSIGN #TWRL3553-PA-TAX-YEAR := #WS.#TAX-YEAR
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde().setValue(1);                                                                                           //Natural: ASSIGN #TWRL3553-PA-AGT-IND-CDE := 1
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein().setValue(131624203);                                                                                           //Natural: ASSIGN #TWRL3553-PA-EIN := 131624203
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type().setValue("Y");                                                                                            //Natural: ASSIGN #TWRL3553-PA-FRM-TYPE := 'Y'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                         //Natural: ASSIGN #TWRL3553-PA-EMP-NAME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr().setValue("8500 ANDREW CARNEGIE BLVD");                                                                    //Natural: ASSIGN #TWRL3553-PA-LOC-ADDR := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr().setValue("8500 ANDREW CARNEGIE BLVD");                                                                    //Natural: ASSIGN #TWRL3553-PA-DEL-ADDR := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_City().setValue("CHARLOTTE");                                                                                        //Natural: ASSIGN #TWRL3553-PA-CITY := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_State().setValue("NC");                                                                                              //Natural: ASSIGN #TWRL3553-PA-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip().setValue(28262);                                                                                               //Natural: ASSIGN #TWRL3553-PA-ZIP := 28262
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn().setValue(8500);                                                                                       //Natural: ASSIGN #TWRL3553-PA-ZIP-CDE-EXTN := 8500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email().setValue("Kristine.Levine@tiaa.org");                                                                  //Natural: ASSIGN #TWRL3553-PA-CNTCT-EMAIL := 'Kristine.Levine@tiaa.org'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id().setValue("1");                                                                                        //Natural: ASSIGN #TWRL3553-PA-AGENT-TYP-ID := '1'
        //*  DASRAH
        if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2014)))                                                                                                          //Natural: IF #WS.#TAX-YEAR GE 2014
        {
            //*  DASRAH
            setValueToSubstring("A",ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1(),5,1);                                                                           //Natural: MOVE 'A' TO SUBSTR ( #TWRL3553-PA-BLANK1,5,1 )
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(1, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 1 #TWRL3553C
        //*  DASRAH
        if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2014)))                                                                                                          //Natural: IF #WS.#TAX-YEAR GE 2014
        {
            //*  DASRAH
            setValueToSubstring("E",ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1(),5,1);                                                                           //Natural: MOVE 'E' TO SUBSTR ( #TWRL3553-PA-BLANK1,5,1 )
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                   /* DASRAH
        //*  DASRAH
        getWorkFiles().write(2, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 2 #TWRL3553C
        //*  WRITE-PA-RECORD
    }
    private void sub_Write_480_7c_Detail_Record() throws Exception                                                                                                        //Natural: WRITE-480-7C-DETAIL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        ldaTwrl3553.getPnd_Twrl3553c().reset();                                                                                                                           //Natural: RESET #TWRL3553C #DATE
        pnd_Work_Area_Pnd_Date.reset();
        pnd_Work_Area_Pnd_Temp_Cntl_Num.setValue(ldaTwrl9710.getForm_Tirf_Pr_Cntl_Num());                                                                                 //Natural: ASSIGN #TEMP-CNTL-NUM := FORM.TIRF-PR-CNTL-NUM
        //*  DASRAH2
        pnd_Work_Area_Pnd_Temp_Cntl_Num.setValue(pnd_Work_Area_Pnd_Temp_Cntl_Num, MoveOption.RightJustified);                                                             //Natural: MOVE RIGHT #TEMP-CNTL-NUM TO #TEMP-CNTL-NUM
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig().setValue(pnd_Work_Area_Pnd_Temp_Cntl_Num_N9);                                                          //Natural: ASSIGN #TWRL3553-DTL-CNTRL-ORIG := #TEMP-CNTL-NUM-N9
        //*  FOR FORM 480.7C
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1().reset();                                                                                                  //Natural: RESET #TWRL3553-DTL-FILLER1
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type().setValue("Y");                                                                                          //Natural: ASSIGN #TWRL3553-DTL-FORM-TYPE := 'Y'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type().setValue(1);                                                                                             //Natural: ASSIGN #TWRL3553-DTL-REC-TYPE := 1
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year().compute(new ComputeParameters(false, ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year()),          //Natural: ASSIGN #TWRL3553-DTL-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type().setValue("1");                                                                                      //Natural: ASSIGN #TWRL3553-DTL-PAYER-ID-TYPE := '1'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr().setValue(131624203);                                                                                       //Natural: ASSIGN #TWRL3553-DTL-ID-NBR := 131624203
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                            //Natural: ASSIGN #TWRL3553-DTL-NAME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1().setValue("8500 ANDREW CARNEGIE BLVD");                                                                 //Natural: ASSIGN #TWRL3553-DTL-ADDR-LINE1 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2().setValue(" ");                                                                                         //Natural: ASSIGN #TWRL3553-DTL-ADDR-LINE2 := ' '
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town().setValue("CHARLOTTE");                                                                                       //Natural: ASSIGN #TWRL3553-DTL-TOWN := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_State().setValue("NC");                                                                                             //Natural: ASSIGN #TWRL3553-DTL-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip().setValue(28262);                                                                                              //Natural: ASSIGN #TWRL3553-DTL-ZIP := 28262
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn().setValue(8500);                                                                                          //Natural: ASSIGN #TWRL3553-DTL-ZIP-EXTN := 8500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn().compute(new ComputeParameters(false, ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn()),                    //Natural: ASSIGN #TWRL3553-DTL-SSN := VAL ( FORM.TIRF-TIN )
            ldaTwrl9710.getForm_Tirf_Tin().val());
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),         //Natural: COMPRESS FORM.TIRF-CONTRACT-NBR FORM.TIRF-PAYEE-CDE INTO #TWRL3553-DTL-ACT-NBR LEAVING NO
            ldaTwrl9710.getForm_Tirf_Payee_Cde()));
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name().setValue(DbsUtil.compress(ldaTwrl9710.getForm_Tirf_Part_First_Nme(), ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme(),  //Natural: COMPRESS FORM.TIRF-PART-FIRST-NME FORM.TIRF-PART-MDDLE-NME FORM.TIRF-PART-LAST-NME INTO #TWRL3553-DTL-PT-NAME
            ldaTwrl9710.getForm_Tirf_Part_Last_Nme()));
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr());                                                     //Natural: ASSIGN #TWRL3553-DTL-ADDR2-LINE1 := FORM.TIRF-STREET-ADDR
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2().setValue(ldaTwrl9710.getForm_Tirf_Street_Addr_Cont_1());                                              //Natural: ASSIGN #TWRL3553-DTL-ADDR2-LINE2 := FORM.TIRF-STREET-ADDR-CONT-1
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn().setValue(ldaTwrl9710.getForm_Tirf_City());                                                                 //Natural: ASSIGN #TWRL3553-DTL-PT-TWN := FORM.TIRF-CITY
        if (condition(DbsUtil.maskMatches(ldaTwrl9710.getForm_Tirf_Geo_Cde(),"99")))                                                                                      //Natural: IF FORM.TIRF-GEO-CDE EQ MASK ( 99 )
        {
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCY-CODE
            sub_Get_Residency_Code();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State().setValue(ldaTwrl9710.getForm_Tirf_Geo_Cde());                                                        //Natural: ASSIGN #TWRL3553-DTL-PT-STATE := FORM.TIRF-GEO-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Temp_Zip_Code.setValue(ldaTwrl9710.getForm_Tirf_Zip());                                                                                         //Natural: ASSIGN #TEMP-ZIP-CODE := FORM.TIRF-ZIP
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip().compute(new ComputeParameters(false, ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip()),              //Natural: ASSIGN #TWRL3553-DTL-PT-ZIP := VAL ( #TEMP-ZIP-CODE-A5 )
            pnd_Work_Area_Pnd_Temp_Zip_Code_A5.val());
        if (condition(pnd_Work_Area_Pnd_Temp_Zip_Code_A4.notEquals(" ")))                                                                                                 //Natural: IF #TEMP-ZIP-CODE-A4 NE ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn().compute(new ComputeParameters(false, ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn()),  //Natural: ASSIGN #TWRL3553-DTL-PT-ZIP-EXTN := VAL ( #TEMP-ZIP-CODE-A4 )
                pnd_Work_Area_Pnd_Temp_Zip_Code_A4.val());
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn().setValue(0);                                                                                      //Natural: ASSIGN #TWRL3553-DTL-PT-ZIP-EXTN := 0000
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUITY
        //*  MUKH
        //*  PARTIAL  /* MUKH
        //*  LUMP SUM
        short decideConditionsMet1923 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-DISTR-CATEGORY;//Natural: VALUE '1'
        if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("1"))))
        {
            decideConditionsMet1923++;
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form().setValue("A");                                                                                       //Natural: ASSIGN #TWRL3553-DTL-DIS-FORM := 'A'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("2"))))
        {
            decideConditionsMet1923++;
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form().setValue("P");                                                                                       //Natural: ASSIGN #TWRL3553-DTL-DIS-FORM := 'P'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((ldaTwrl9710.getForm_Tirf_Distr_Category().equals("3"))))
        {
            decideConditionsMet1923++;
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form().setValue("L");                                                                                       //Natural: ASSIGN #TWRL3553-DTL-DIS-FORM := 'L'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  PRIVATE, VIKRAM
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type().setValue("P");                                                                                            //Natural: ASSIGN #TWRL3553-DTL-PL-TYPE := 'P'
        //* *END-IF                                                   /* MUKH
        if (condition(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll().notEquals(new DbsDecimal("0.00"))))                                                                      //Natural: IF FORM.TIRF-PR-IVC-AMT-ROLL NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib().setValue(ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll());                                             //Natural: MOVE FORM.TIRF-PR-IVC-AMT-ROLL TO #TWRL3553-DTL-ROLL-CNTRIB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-ROLL-CNTRIB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll().notEquals(new DbsDecimal("0.00"))))                                                                    //Natural: IF FORM.TIRF-PR-GROSS-AMT-ROLL NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib().setValue(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                          //Natural: MOVE FORM.TIRF-PR-GROSS-AMT-ROLL TO #TWRL3553-DTL-ROLL-DISTRIB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-ROLL-DISTRIB
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost().setValue(0);                                                                                             //Natural: MOVE 0.00 TO #TWRL3553-DTL-ANN-COST
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-LS-TAX-WTHLD-20
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-LS-TAX-WTHLD-10
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-DR-TAX-WTHLD-10
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-RR-TAX-WTHLD-10
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-NR-TAX-WTHLD-DIS
        if (condition(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll().notEquals(new DbsDecimal("0.00"))))                                                                    //Natural: IF FORM.TIRF-PR-GROSS-AMT-ROLL NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib().setValue(ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll());                                          //Natural: MOVE FORM.TIRF-PR-GROSS-AMT-ROLL TO #TWRL3553-DTL-ROLL-DISTRIB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-ROLL-DISTRIB
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_4807c_Amt_Dist.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_4807c_Amt_Dist), ldaTwrl9710.getForm_Tirf_Gross_Amt().add(ldaTwrl9710.getForm_Tirf_Int_Amt())); //Natural: ASSIGN #4807C-AMT-DIST := FORM.TIRF-GROSS-AMT + FORM.TIRF-INT-AMT
        if (condition(pnd_Work_Area_Pnd_4807c_Amt_Dist.notEquals(new DbsDecimal("0.00"))))                                                                                //Natural: IF #4807C-AMT-DIST NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis().setValue(pnd_Work_Area_Pnd_4807c_Amt_Dist);                                                           //Natural: MOVE #4807C-AMT-DIST TO #TWRL3553-DTL-AMT-DIS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis().setValue(0);                                                                                          //Natural: MOVE 0.00 TO #TWRL3553-DTL-AMT-DIS
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay().setValue(0);                                                                                              //Natural: MOVE 0.00 TO #TWRL3553-DTL-PRE-PAY
        //*                                                 TAXABLE AMOUNT
        if (condition(ldaTwrl9710.getForm_Tirf_Taxable_Amt().notEquals(new DbsDecimal("0.00"))))                                                                          //Natural: IF FORM.TIRF-TAXABLE-AMT NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt().setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                 //Natural: MOVE FORM.TIRF-TAXABLE-AMT TO #TWRL3553-DTL-TAXABLE-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-TAXABLE-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib().setValue(0);                                                                                           //Natural: MOVE 0.00 TO #TWRL3553-DTL-DEF-CNTRIB
        //*                                               AFTER-TAX CONTRIBUTIONS
        if (condition(ldaTwrl9710.getForm_Tirf_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))                                                                              //Natural: IF FORM.TIRF-IVC-AMT NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib().setValue(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                  //Natural: MOVE FORM.TIRF-IVC-AMT TO #TWRL3553-DTL-AFT-TAX-CNTRIB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib().setValue(0);                                                                                   //Natural: MOVE 0.00 TO #TWRL3553-DTL-AFT-TAX-CNTRIB
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion().setValue(0);                                                                                        //Natural: MOVE 0.00 TO #TWRL3553-DTL-INC-ACCRETION
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total().setValue(ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis());                                          //Natural: MOVE #TWRL3553-DTL-AMT-DIS TO #TWRL3553-DTL-TOTAL
        pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde.setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                                         //Natural: MOVE FORM.TIRF-DISTRIBUTION-CDE TO #TEMP-DTL-DIS-CDE
        pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde.setValue(pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde, MoveOption.LeftJustified);                                                        //Natural: MOVE LEFT #TEMP-DTL-DIS-CDE TO #TEMP-DTL-DIS-CDE
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde().setValue(pnd_Work_Area_Pnd_Temp_Dtl_Dis_Cde_A1);                                                          //Natural: MOVE #TEMP-DTL-DIS-CDE-A1 TO #TWRL3553-DTL-DIS-CDE
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld().setValue(0);                                                                                         //Natural: MOVE 0.00 TO #TWRL3553-DTL-QP-TAX-WTHLD
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld().setValue(0);                                                                                         //Natural: MOVE 0.00 TO #TWRL3553-DTL-OD-TAX-WTHLD
        pnd_Work_Area_Pnd_Other.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Other), pnd_Work_Area_Pnd_4807c_Amt_Dist.subtract(ldaTwrl9710.getForm_Tirf_Ivc_Amt())); //Natural: ASSIGN #OTHER := #4807C-AMT-DIST - FORM.TIRF-IVC-AMT
        if (condition(pnd_Work_Area_Pnd_Other.notEquals(new DbsDecimal("0.00"))))                                                                                         //Natural: IF #OTHER NE 0.00
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others().setValue(pnd_Work_Area_Pnd_Other);                                                                     //Natural: MOVE #OTHER TO #TWRL3553-DTL-OTHERS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others().setValue(0);                                                                                           //Natural: MOVE 0.00 TO #TWRL3553-DTL-OTHERS
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld().setValue(0);                                                                                      //Natural: MOVE 0.00 TO #TWRL3553-DTL-OD-QP-TAX-WTHLD
        //*                                                     << SINHASN START
        if (condition(ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9().getSubstring(1,12).equals(" ")))                                                            //Natural: IF SUBSTR ( #TWRL3553-DTL-FILLER9,1,12 ) = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08().setValue(0);                                                                                  //Natural: MOVE 0.00 TO #TWRL3553-DTL-LS-TAX-WTHLD-08
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9().getSubstring(13,12).equals(" ")))                                                           //Natural: IF SUBSTR ( #TWRL3553-DTL-FILLER9,13,12 ) = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis().setValue(0);                                                                                 //Natural: MOVE 0.00 TO #TWRL3553-DTL-NQ-TAX-WTHLD-DIS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9().getSubstring(1839,12).equals(" ")))                                                         //Natural: IF SUBSTR ( #TWRL3553-DTL-FILLER9,1839,12 ) = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd().setValue(0);                                                                                       //Natural: MOVE 0.00 TO #TWRL3553-DTL-GOV-RT-FND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9().getSubstring(1851,12).equals(" ")))                                                         //Natural: IF SUBSTR ( #TWRL3553-DTL-FILLER9,1851,12 ) = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-AP-TAX-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                  /* VIKRAM STARTS
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt().setValue(0);                                                                                             //Natural: MOVE 0.00 TO #TWRL3553-DTL-A-EXEMPT
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable().setValue(0);                                                                                            //Natural: MOVE 0.00 TO #TWRL3553-DTL-B-TAXABLE
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep().setValue(0);                                                                                           //Natural: MOVE 0.00 TO #TWRL3553-DTL-C-AMT-PREP
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr().setValue(0);                                                                                       //Natural: MOVE 0.00 TO #TWRL3553-DTL-D-AFT-TAX-CNTR
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total().setValue(0);                                                                                              //Natural: MOVE 0.00 TO #TWRL3553-DTL-E-TOTAL
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-TAX-WTH-ELG-DIST
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm().setValue(0);                                                                                     //Natural: MOVE 0.00 TO #TWRL3553-DTL-DIST-EXEMPT-INCM
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn().reset();                                                                                             //Natural: RESET #TWRL3553-DTE-RECEIVE-PNSN #TWRL3553-DTL-OTHR-DISTR-CDE #TWRL3553-DTL-FILL6 #TWRL3553-DTL-PYE-MM-LAST-NAME
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name().reset();
        //*  INDIVIDUAL
        if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().equals("1")))                                                                                                //Natural: IF FORM.TIRF-TAX-ID-TYPE = '1'
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_First_Nme());                                               //Natural: ASSIGN #TWRL3553-DTL-FIRST-NAME := FORM.TIRF-PART-FIRST-NME
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme());                                          //Natural: ASSIGN #TWRL3553-DTL-PAYEEE-MID-NAME := FORM.TIRF-PART-MDDLE-NME
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name().setValue(ldaTwrl9710.getForm_Tirf_Part_Last_Nme());                                          //Natural: ASSIGN #TWRL3553-DTL-PAYEEE-LAST-NAME := FORM.TIRF-PART-LAST-NME
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein().setValue("000000000");                                                                                        //Natural: ASSIGN #TWRL3553-DTL-EIN := '000000000'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm().setValue(ldaTwrl9710.getForm_Tirf_Plan_Name());                                                           //Natural: ASSIGN #TWRL3553-DTL-PLAN-NM := TIRF-PLAN-NAME
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr().setValue(ldaTwrl9710.getForm_Tirf_Plan_Spnsr_Name());                                                  //Natural: ASSIGN #TWRL3553-DTL-PLAN-SPNSR := TIRF-PLAN-SPNSR-NAME
        if (condition(ldaTwrl9710.getForm_Tirf_Plan_Name().equals(" ")))                                                                                                  //Natural: IF FORM.TIRF-PLAN-NAME = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm().setValue("TIAA CLIENT PLAN");                                                                         //Natural: ASSIGN #TWRL3553-DTL-PLAN-NM := 'TIAA CLIENT PLAN'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Plan().getValue(1).equals("MLTPLN")))                                                                                      //Natural: IF FORM.TIRF-PLAN ( 1 ) = 'MLTPLN'
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm().setValue("TIAA CLIENT SPONSOR PLAN");                                                                 //Natural: ASSIGN #TWRL3553-DTL-PLAN-NM := 'TIAA CLIENT SPONSOR PLAN'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Plan_Spnsr_Name().equals(" ")))                                                                                            //Natural: IF FORM.TIRF-PLAN-SPNSR-NAME = ' '
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr().setValue("TIAA CLIENT PLAN SPONSOR");                                                              //Natural: ASSIGN #TWRL3553-DTL-PLAN-SPNSR := 'TIAA CLIENT PLAN SPONSOR'
            //*  VIKRAM ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                     <<DASRAH START
        //*  MOVE EDITED FORM.TIRF-CREATE-TS (EM=YYYYMMDD) TO #DATE
        //*  COMPRESS #DATE-DD #DATE-MM #DATE-YY INTO #TWRL3553-DTL-AMND-DATE
        //*   LEAVING NO
        //*  IF FORM.TIRF-FORM-ISSUE-TYPE   = '1'  /* ORIGINAL
        //*  ORIGINAL /* MUKH
        //*  VIKRAM
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("1") || ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("2")))                                  //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '1' OR = '2'
        {
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type().setValue("O");                                                                                       //Natural: ASSIGN #TWRL3553-DTL-DOC-TYPE := 'O'
            ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date().setValue(0);                                                                                        //Natural: ASSIGN #TWRL3553-DTL-AMND-DATE := 0
            getWorkFiles().write(1, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                               //Natural: WRITE WORK FILE 1 #TWRL3553C
            pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig.nadd(1);                                                                                                                 //Natural: ADD 1 TO #WS-NUM-OF-DOC-ORIG
            pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                              //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-AMOUT-PAID-ORIG
            pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                                //Natural: ADD FORM.TIRF-INT-AMT TO #WS-AMOUT-PAID-ORIG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CORRECTION
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Issue_Type().equals("3")))                                                                                        //Natural: IF FORM.TIRF-FORM-ISSUE-TYPE = '3'
            {
                //*  DASRAH2 STARTS
                if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2014)))                                                                                                  //Natural: IF #WS.#TAX-YEAR GE 2014
                {
                    //*  DUTTAD
                    ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg().reset();                                                                                  //Natural: RESET #TWRL3553-DTL-RSN-FOR-CHG
                    ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg().setValue(ldaTwrl9710.getForm_Tirf_Pr_Adj_Reason());                                       //Natural: ASSIGN #TWRL3553-DTL-RSN-FOR-CHG := FORM.TIRF-PR-ADJ-REASON
                    //*      #TWRL3553-DTL-CNTRL-AMND   := #TWRL3553-DTL-CNTRL-ORIG /* DUTTAD
                    //*  DUTTAD
                    pnd_Work_Area_Pnd_Temp_Cntl_Num1.reset();                                                                                                             //Natural: RESET #TEMP-CNTL-NUM1
                    pnd_Work_Area_Pnd_Temp_Cntl_Num1.setValue(ldaTwrl9710.getForm_Tirf_Pr_Org_Cntl_Num());                                                                //Natural: ASSIGN #TEMP-CNTL-NUM1 := FORM.TIRF-PR-ORG-CNTL-NUM
                    //*  DUTTAD
                    //*  DUTTAD
                    pnd_Work_Area_Pnd_Temp_Cntl_Num1.setValue(pnd_Work_Area_Pnd_Temp_Cntl_Num1, MoveOption.RightJustified);                                               //Natural: MOVE RIGHT #TEMP-CNTL-NUM1 TO #TEMP-CNTL-NUM1
                    ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd().setValue(pnd_Work_Area_Pnd_Temp_Cntl_Num1_N9);                                             //Natural: ASSIGN #TWRL3553-DTL-CNTRL-AMND := #TEMP-CNTL-NUM1-N9
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type().setValue("A");                                                                                   //Natural: ASSIGN #TWRL3553-DTL-DOC-TYPE := 'A'
                //*  VIKRAM
                pnd_Work_Area_Pnd_Date.setValueEdited(ldaTwrl9710.getForm_Tirf_Create_Ts(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED FORM.TIRF-CREATE-TS ( EM = YYYYMMDD ) TO #DATE
                ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date().setValue(0);                                                                                    //Natural: ASSIGN #TWRL3553-DTL-AMND-DATE := 0
                getWorkFiles().write(2, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                           //Natural: WRITE WORK FILE 2 #TWRL3553C
                pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr.nadd(1);                                                                                                             //Natural: ADD 1 TO #WS-NUM-OF-DOC-CORR
                pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr.nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                                          //Natural: ADD FORM.TIRF-GROSS-AMT TO #WS-AMOUT-PAID-CORR
                pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr.nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                                            //Natural: ADD FORM.TIRF-INT-AMT TO #WS-AMOUT-PAID-CORR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Not Corrected or Original record",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 1 ) '***' 25T 'Not Corrected or Original record' 77T '***' / '***' 25T 'Correction Reporting' 77T '***'
                    TabSetting(25),"Correction Reporting",new TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *ADD   1                   TO #WS-NUM-OF-DOC  /* DASRAH  END >>
        //*  NO WITHLD FOR TIAA
        pnd_Work_Area_Pnd_Ws_Amt_Withld.reset();                                                                                                                          //Natural: RESET #WS-AMT-WITHLD
        //* *ADD   FORM.TIRF-GROSS-AMT TO #WS-AMOUT-PAID     DASRAH
        //* *ADD   FORM.TIRF-INT-AMT   TO #WS-AMOUT-PAID     DASRAH
        //*  WRITE-480-7C-DETAIL-RECORD
    }
    //* DHI
    private void sub_Get_Residency_Code() throws Exception                                                                                                                //Natural: GET-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_State_De_Pnd_Tircntl_Tax_Year.setValue(ldaTwrl9710.getForm_Tirf_Tax_Year());                                                                                  //Natural: ASSIGN #TIRCNTL-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_State_De_Pnd_Tircntl_Tbl_Nbr.setValue(2);                                                                                                                     //Natural: ASSIGN #TIRCNTL-TBL-NBR := 2
        pnd_Res_Code.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9710.getForm_Tirf_Geo_Cde()));                                                  //Natural: COMPRESS '0' FORM.TIRF-GEO-CDE INTO #RES-CODE LEAVING NO
        pnd_State_De_Pnd_Tircntl_State_Old_Code.setValue(pnd_Res_Code);                                                                                                   //Natural: ASSIGN #TIRCNTL-STATE-OLD-CODE := #RES-CODE
        vw_state_Tbl.startDatabaseRead                                                                                                                                    //Natural: READ STATE-TBL BY TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #STATE-DE
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_State_De, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_state_Tbl.readNextRow("READ01")))
        {
            if (condition(state_Tbl_Tircntl_Tax_Year.notEquals(ldaTwrl9710.getForm_Tirf_Tax_Year().val())))                                                               //Natural: IF STATE-TBL.TIRCNTL-TAX-YEAR NE VAL ( FORM.TIRF-TAX-YEAR )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_State_De_Pnd_Tircntl_State_Old_Code.equals(pnd_Res_Code)))                                                                              //Natural: IF #TIRCNTL-STATE-OLD-CODE EQ #RES-CODE
                {
                    ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State().setValue(state_Tbl_Tircntl_State_Alpha_Code);                                                //Natural: MOVE STATE-TBL.TIRCNTL-STATE-ALPHA-CODE TO #TWRL3553-DTL-PT-STATE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Summary_Record() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        ldaTwrl3553.getPnd_Twrl3553c().reset();                                                                                                                           //Natural: RESET #TWRL3553C #DATE
        pnd_Work_Area_Pnd_Date.reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number().setValue(0);                                                                                        //Natural: ASSIGN #TWRL3553-SM-CONTROL-NUMBER := 000000000
        //*  DASRAH2
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6().reset();                                                                                                   //Natural: RESET #TWRL3553-SM-FILLER6
        //*  DASRAH2
        //*  FOR FORM 480.7C
        //*  FOR SUMMARY RECORD
        //*  FOR ORIGINAL DASRAH
        //*  VIKRAM
        //*  DASRAH
        setValueToSubstring("000000000",ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6(),2247,9);                                                                   //Natural: MOVE '000000000' TO SUBSTR ( #TWRL3553-SM-FILLER6,2247,9 )
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type().setValue("Y");                                                                                           //Natural: ASSIGN #TWRL3553-SM-FORM-TYPE := 'Y'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type().setValue(2);                                                                                           //Natural: ASSIGN #TWRL3553-SM-RECORD-TYPE := 2
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type().setValue("O");                                                                                       //Natural: ASSIGN #TWRL3553-SM-DOCUMENT-TYPE := 'O'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                        //Natural: ASSIGN #TWRL3553-SM-TAXABLE-YEAR := #WS.#TAX-YEAR
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ().setValue("1");                                                                                        //Natural: ASSIGN #TWRL3553-SM-PAYER-ID-TYP := '1'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr().setValue(131624203);                                                                                       //Natural: ASSIGN #TWRL3553-SM-IDN-NBR := 131624203
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                          //Natural: ASSIGN #TWRL3553-SM-NAMEER3 := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1().setValue("8500 ANDREW CARNEGIE BLVD");                                                                    //Natural: ASSIGN #TWRL3553-SM-ADD-LN-1 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Town().setValue("CHARLOTTE");                                                                                        //Natural: ASSIGN #TWRL3553-SM-TOWN := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_State().setValue("NC");                                                                                              //Natural: ASSIGN #TWRL3553-SM-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code().setValue(28262);                                                                                          //Natural: ASSIGN #TWRL3553-SM-ZIP-CODE := 28262
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn().setValue(8500);                                                                                      //Natural: ASSIGN #TWRL3553-SM-ZIP-CODE-EXTN := 8500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig);                                                        //Natural: ASSIGN #TWRL3553-SM-NBR-OF-DOCS := #WS-NUM-OF-DOC-ORIG
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld().setValue(pnd_Work_Area_Pnd_Ws_Amt_Withld);                                                           //Natural: MOVE #WS-AMT-WITHLD TO #TWRL3553-SM-TOT-AMT-WTHLD
        //* *MOVE #WS-AMOUT-PAID TO #TWRL3553-SM-TOT-AMT-PAID          /* DASRAH
        //*  DASRAH
        //*  CORPORATION = C , VIKRAM
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid().setValue(pnd_Work_Area_Pnd_Ws_Amout_Paid_Orig);                                                       //Natural: MOVE #WS-AMOUT-PAID-ORIG TO #TWRL3553-SM-TOT-AMT-PAID
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer().setValue("C");                                                                                    //Natural: ASSIGN #TWRL3553-SM-TYPE-OF-TAXPAYER := 'C'
        //* *RESET #TWRL3553-SM-FILLER6                                /* DASRAH2
        //*  #TWRL3553-SM-AMND-DATE  := #WS.#AMENDED-DATE              /* DASRAH
        getWorkFiles().write(1, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 1 #TWRL3553C
        //*                           /* << DASRAH START
        //*  FOR AMENDED
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type().reset();                                                                                             //Natural: RESET #TWRL3553-SM-DOCUMENT-TYPE #TWRL3553-SM-NBR-OF-DOCS #TWRL3553-SM-TOT-AMT-PAID
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type().setValue("A");                                                                                       //Natural: ASSIGN #TWRL3553-SM-DOCUMENT-TYPE := 'A'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr);                                                        //Natural: ASSIGN #TWRL3553-SM-NBR-OF-DOCS := #WS-NUM-OF-DOC-CORR
        //*  VIKRAM
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid().setValue(pnd_Work_Area_Pnd_Ws_Amout_Paid_Corr);                                                       //Natural: MOVE #WS-AMOUT-PAID-CORR TO #TWRL3553-SM-TOT-AMT-PAID
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date().setValue(0);                                                                                             //Natural: ASSIGN #TWRL3553-SM-AMND-DATE := 0
        //*   DASRAH END>>
        getWorkFiles().write(2, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 2 #TWRL3553C
        //*  WRITE-SUMMARY-RECORD
    }
    //*  VIKRAM ADDED
    private void sub_Write_Trailer_Record() throws Exception                                                                                                              //Natural: WRITE-TRAILER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------
        //*  FILLER+CONTROL N0
        //*  480.7C.1
        //*  1 = DETAIL RECORD
        //*  O = ORIGINAL
        //*  1 = FEIN
        //*  BLANK
        ldaTwrl3553.getPnd_Twrl3553c().reset();                                                                                                                           //Natural: RESET #TWRL3553C
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr().setValue(0);                                                                                              //Natural: ASSIGN #TWRL3553-CONTROL-NBR := 0
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Form_Type().setValue("R");                                                                                              //Natural: ASSIGN #TWRL3553-FORM-TYPE := 'R'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Rec_Type().setValue("1");                                                                                               //Natural: ASSIGN #TWRL3553-REC-TYPE := '1'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Doc_Type().setValue("O");                                                                                               //Natural: ASSIGN #TWRL3553-DOC-TYPE := 'O'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Tax_Year().compute(new ComputeParameters(false, ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Tax_Year()),                  //Natural: ASSIGN #TWRL3553-TAX-YEAR := VAL ( FORM.TIRF-TAX-YEAR )
            ldaTwrl9710.getForm_Tirf_Tax_Year().val());
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type().setValue("1");                                                                                         //Natural: ASSIGN #TWRL3553-PAYERS-ID-TYPE := '1'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins().setValue(" ");                                                                                    //Natural: ASSIGN #TWRL3553-TYP-INDUSTRY-BUSINS := ' '
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Indentification_No().setValue(131624203);                                                                               //Natural: ASSIGN #TWRL3553-INDENTIFICATION-NO := 131624203
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Busins_Name().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                         //Natural: ASSIGN #TWRL3553-BUSINS-NAME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme().setValue("TEACHERS INSURANCE AND ANNUITY ASSOC");                                                   //Natural: ASSIGN #TWRL3553-WITHHLD-AGENT-NME := 'TEACHERS INSURANCE AND ANNUITY ASSOC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A().setValue("7049882344");                                                                             //Natural: ASSIGN #TWRL3553-AGENT-TELEPHONE-A := '7049882344'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1().setValue("8500 ANDREW CARNEGIE BLVD");                                                                //Natural: ASSIGN #TWRL3553-AGENT-ADDRESS-1 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2().setValue("8500 ANDREW CARNEGIE BLVD");                                                                //Natural: ASSIGN #TWRL3553-AGENT-ADDRESS-2 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Town().setValue("CHARLOTTE");                                                                                     //Natural: ASSIGN #TWRL3553-AGENT-TOWN := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_State().setValue("NC");                                                                                           //Natural: ASSIGN #TWRL3553-AGENT-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full().setValue(282628500);                                                                                   //Natural: ASSIGN #TWRL3553-AGENT-ZIP-FULL := 282628500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1().setValue("8500 ANDREW CARNEGIE BLVD");                                                             //Natural: ASSIGN #TWRL3553-PHYSICAL-ADDRESS-1 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2().setValue("8500 ANDREW CARNEGIE BLVD");                                                             //Natural: ASSIGN #TWRL3553-PHYSICAL-ADDRESS-2 := '8500 ANDREW CARNEGIE BLVD'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_A_Town().setValue("CHARLOTTE");                                                                                         //Natural: ASSIGN #TWRL3553-A-TOWN := 'CHARLOTTE'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_A_State().setValue("NC");                                                                                               //Natural: ASSIGN #TWRL3553-A-STATE := 'NC'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full().setValue(282628500);                                                                                       //Natural: ASSIGN #TWRL3553-A-ZIP-FULL := 282628500
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address().setValue("N");                                                                                        //Natural: ASSIGN #TWRL3553-CHNG-OF-ADDRESS := 'N'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Email().setValue("Kristine.Levine@tiaa.org");                                                                     //Natural: ASSIGN #TWRL3553-AGENT-EMAIL := 'Kristine.Levine@tiaa.org'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov().setValue(0);                                                                                     //Natural: ASSIGN #TWRL3553-PRDC-PYMNT-QUALI-GOV := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20().setValue(0);                                                                                         //Natural: ASSIGN #TWRL3553-LUMPSUM-DISTRI20 := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10().setValue(0);                                                                                         //Natural: ASSIGN #TWRL3553-LUMPSUM-DISTRI10 := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified().setValue(0);                                                                                      //Natural: ASSIGN #TWRL3553-DISTRI-NONQUALIFIED := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10().setValue(0);                                                                                      //Natural: ASSIGN #TWRL3553-OTHR-INCM-QUALIFD10 := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost().setValue(0);                                                                                             //Natural: ASSIGN #TWRL3553-ANNUITY-COST := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira().setValue(0);                                                                                     //Natural: ASSIGN #TWRL3553-ROLOVR-QUAL-NONDTIRA := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10().setValue(0);                                                                                      //Natural: ASSIGN #TWRL3553-DISTRI-RTRMNT-SAV10 := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10().setValue(0);                                                                                         //Natural: ASSIGN #TWRL3553-RLVR-NON-DTIRA10 := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri().setValue(0);                                                                                            //Natural: ASSIGN #TWRL3553-NONRES-DSITRI := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution().setValue(0);                                                                                       //Natural: ASSIGN #TWRL3553-OTHER-DISTRIBUTION := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria().setValue(0);                                                                                       //Natural: ASSIGN #TWRL3553-ECONO-ERGNCY-MARIA := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Total().setValue(0);                                                                                                    //Natural: ASSIGN #TWRL3553-TOTAL := 0
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri().setValue(0);                                                                                            //Natural: ASSIGN #TWRL3553-NONRES-DSITRI := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution().setValue(0);                                                                                       //Natural: ASSIGN #TWRL3553-OTHER-DISTRIBUTION := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria().setValue(0);                                                                                       //Natural: ASSIGN #TWRL3553-ECONO-ERGNCY-MARIA := 0.00
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Total().setValue(0);                                                                                                    //Natural: ASSIGN #TWRL3553-TOTAL := 0
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Total_Forms().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Orig);                                                           //Natural: ASSIGN #TWRL3553-TOTAL-FORMS := #WS-NUM-OF-DOC-ORIG
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Fill1().reset();                                                                                                        //Natural: RESET #TWRL3553-FILL1 #TWRL3553-REASON-FOR-CHG #TWRL3553-FILLR1A
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Fillr1a().reset();
        getWorkFiles().write(1, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 1 #TWRL3553C
        //*  AMENDED
        //*  CORRECTIONAL
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Doc_Type().reset();                                                                                                     //Natural: RESET #TWRL3553-DOC-TYPE #TWRL3553-TOTAL-FORMS #TWRL3553-REASON-FOR-CHG
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Total_Forms().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg().reset();
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Doc_Type().setValue("A");                                                                                               //Natural: ASSIGN #TWRL3553-DOC-TYPE := 'A'
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Total_Forms().setValue(pnd_Work_Area_Pnd_Ws_Num_Of_Doc_Corr);                                                           //Natural: ASSIGN #TWRL3553-TOTAL-FORMS := #WS-NUM-OF-DOC-CORR
        ldaTwrl3553.getPnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg().setValue(ldaTwrl9710.getForm_Tirf_Pr_Adj_Reason());                                                    //Natural: ASSIGN #TWRL3553-REASON-FOR-CHG := FORM.TIRF-PR-ADJ-REASON
        getWorkFiles().write(2, false, ldaTwrl3553.getPnd_Twrl3553c());                                                                                                   //Natural: WRITE WORK FILE 2 #TWRL3553C
        //*  WRITE-TRAILER-RECORD  ENDS
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRd_Form() throws Exception {atBreakEventRd_Form(false);}
    private void atBreakEventRd_Form(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak = ldaTwrl9710.getForm_Tirf_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak = ldaTwrl9710.getForm_Tirf_Form_Type().isBreak(endOfData);
        if (condition(ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak || ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Irsr_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsr_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSR.#FORM-CNT ( *,1 ) TO #IRSR.#FORM-CNT ( *,2 )
            pnd_Irsr_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsr_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSR.#GROSS-AMT ( *,1 ) TO #IRSR.#GROSS-AMT ( *,2 )
            pnd_Irsr_Pnd_Gross_Amt_Roll.getValue("*",2).nadd(pnd_Irsr_Pnd_Gross_Amt_Roll.getValue("*",1));                                                                //Natural: ADD #IRSR.#GROSS-AMT-ROLL ( *,1 ) TO #IRSR.#GROSS-AMT-ROLL ( *,2 )
            pnd_Irsr_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Irsr_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #IRSR.#INT-AMT ( *,1 ) TO #IRSR.#INT-AMT ( *,2 )
            pnd_Irsr_Pnd_Ivc_Amt_Roll.getValue("*",2).nadd(pnd_Irsr_Pnd_Ivc_Amt_Roll.getValue("*",1));                                                                    //Natural: ADD #IRSR.#IVC-AMT-ROLL ( *,1 ) TO #IRSR.#IVC-AMT-ROLL ( *,2 )
            pnd_Irsr_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsr_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSR.#TAX-WTHLD ( *,1 ) TO #IRSR.#TAX-WTHLD ( *,2 )
            pnd_Irsr_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSR.#TOTALS ( *,1 )
            pnd_Irsw_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsw_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSW.#FORM-CNT ( *,1 ) TO #IRSW.#FORM-CNT ( *,2 )
            pnd_Irsw_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsw_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSW.#GROSS-AMT ( *,1 ) TO #IRSW.#GROSS-AMT ( *,2 )
            pnd_Irsw_Pnd_Gross_Amt_Roll.getValue("*",2).nadd(pnd_Irsw_Pnd_Gross_Amt_Roll.getValue("*",1));                                                                //Natural: ADD #IRSW.#GROSS-AMT-ROLL ( *,1 ) TO #IRSW.#GROSS-AMT-ROLL ( *,2 )
            pnd_Irsw_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Irsw_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #IRSW.#INT-AMT ( *,1 ) TO #IRSW.#INT-AMT ( *,2 )
            pnd_Irsw_Pnd_Ivc_Amt_Roll.getValue("*",2).nadd(pnd_Irsw_Pnd_Ivc_Amt_Roll.getValue("*",1));                                                                    //Natural: ADD #IRSW.#IVC-AMT-ROLL ( *,1 ) TO #IRSW.#IVC-AMT-ROLL ( *,2 )
            pnd_Irsw_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsw_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSW.#TAX-WTHLD ( *,1 ) TO #IRSW.#TAX-WTHLD ( *,2 )
            pnd_Irsw_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSW.#TOTALS ( *,1 )
            pnd_Irsg_Pnd_Form_Cnt.getValue("*",2).nadd(pnd_Irsg_Pnd_Form_Cnt.getValue("*",1));                                                                            //Natural: ADD #IRSG.#FORM-CNT ( *,1 ) TO #IRSG.#FORM-CNT ( *,2 )
            pnd_Irsg_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Irsg_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #IRSG.#GROSS-AMT ( *,1 ) TO #IRSG.#GROSS-AMT ( *,2 )
            pnd_Irsg_Pnd_Gross_Amt_Roll.getValue("*",2).nadd(pnd_Irsg_Pnd_Gross_Amt_Roll.getValue("*",1));                                                                //Natural: ADD #IRSG.#GROSS-AMT-ROLL ( *,1 ) TO #IRSG.#GROSS-AMT-ROLL ( *,2 )
            pnd_Irsg_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Irsg_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #IRSG.#INT-AMT ( *,1 ) TO #IRSG.#INT-AMT ( *,2 )
            pnd_Irsg_Pnd_Ivc_Amt_Roll.getValue("*",2).nadd(pnd_Irsg_Pnd_Ivc_Amt_Roll.getValue("*",1));                                                                    //Natural: ADD #IRSG.#IVC-AMT-ROLL ( *,1 ) TO #IRSG.#IVC-AMT-ROLL ( *,2 )
            pnd_Irsg_Pnd_Tax_Wthld.getValue("*",2).nadd(pnd_Irsg_Pnd_Tax_Wthld.getValue("*",1));                                                                          //Natural: ADD #IRSG.#TAX-WTHLD ( *,1 ) TO #IRSG.#TAX-WTHLD ( *,2 )
            pnd_Irsg_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #IRSG.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Accepted_Form_Cnt.nadd(pnd_Irsr_Pnd_Form_Cnt.getValue(7,2));                                                                                       //Natural: ADD #IRSR.#FORM-CNT ( 7,2 ) TO #WS.#ACCEPTED-FORM-CNT
            pnd_Irsr_Pnd_Totals.getValue("*",2).reset();                                                                                                                  //Natural: RESET #IRSR.#TOTALS ( *,2 ) #IRSW.#TOTALS ( *,2 ) #IRSG.#TOTALS ( *,2 )
            pnd_Irsw_Pnd_Totals.getValue("*",2).reset();
            pnd_Irsg_Pnd_Totals.getValue("*",2).reset();
            pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                          //Natural: ASSIGN #WS.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");
        Global.format(12, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Puerto Rico Correction Reporting Summary",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Puerto Rico Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"Puerto Rico Correction Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Puerto Rico Correction Withholding Reconciliation","Accepted Detail",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Puerto Rico Correction Withholding Reconciliation","Rejected Detail",new TabSetting(118),"Report:  RPT10",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(42),"Purto Rico General Ledger Reconciliation Summary",new TabSetting(118),"Report:  RPT11",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(43),"Purto Rico General Ledger Reconciliation Detail",new TabSetting(118),"Report:  RPT12",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsr_Pnd_Header1,NEWLINE,"/",
        		pnd_Irsr_Pnd_Header2,"Form/Count",
        		pnd_Irsr_Pnd_Form_Cnt,"Amount/Distributed",
        		pnd_Irsr_Pnd_Gross_Amt,"Interest/Amount",
        		pnd_Irsr_Pnd_Int_Amt,"Rollover/Distribution",
        		pnd_Irsr_Pnd_Gross_Amt_Roll,"Rollover/Contribution",
        		pnd_Irsr_Pnd_Ivc_Amt_Roll);
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsw_Pnd_Header,"Form/Count",
        		pnd_Irsw_Pnd_Form_Cnt,"Amount/Distributed",
        		pnd_Irsw_Pnd_Gross_Amt,"Interest/Amount",
        		pnd_Irsw_Pnd_Int_Amt,"Rollover/Distribution",
        		pnd_Irsw_Pnd_Gross_Amt_Roll,"Rollover/Contribution",
        		pnd_Irsw_Pnd_Ivc_Amt_Roll);
        getReports().setDisplayColumns(11, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		pnd_Irsg_Pnd_Header,"Form/Count",
        		pnd_Irsg_Pnd_Form_Cnt,"Amount/Distributed",
        		pnd_Irsg_Pnd_Gross_Amt,"Interest/Amount",
        		pnd_Irsg_Pnd_Int_Amt,"Rollover/Distribution",
        		pnd_Irsg_Pnd_Gross_Amt_Roll,"Rollover/Contribution",
        		pnd_Irsg_Pnd_Ivc_Amt_Roll);
        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
        		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=�"),
            "/Amount Distributed",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"IVC Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Taxable Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),"/Interest Amount",
            
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Rollover Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,"Rollover Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(6, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Dist/Code",
        		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=�"),
            "/Amount Distributed",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"IVC Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Taxable Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),"/Interest Amount",
            
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new FieldAttributes("LC=��"),NEWLINE,
            "Rollover Distribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Gross_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,"Rollover Contribution",
        		ldaTwrl9710.getForm_Tirf_Pr_Ivc_Amt_Roll(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(10, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(9, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(12, "/TIN",
        		pnd_Recon_Case_Pnd_Tin, new IdenticalSuppress(true),"Contract/Payee",
        		pnd_Recon_Case_Pnd_Cntrct_Py, new IdenticalSuppress(true),"/",
        		pnd_Recon_Case_Pnd_Header,"/Form Count",
        		pnd_Recon_Case_Pnd_Form_Cnt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Amount Paid",
        		pnd_Recon_Case_Pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Amount/Withheld",
        		pnd_Recon_Case_Pnd_Tax_Wthld, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData1266() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                          //Natural: ASSIGN #WS.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
}
