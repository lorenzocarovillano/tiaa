/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:15 PM
**        * FROM NATURAL PROGRAM : Twrp3330
************************************************************
**        * FILE NAME            : Twrp3330.java
**        * CLASS NAME           : Twrp3330
**        * INSTANCE NAME        : Twrp3330
************************************************************
************************************************************************
** PROGRAM : TWRP3330
** SYSTEM  : TAXWARS
** AUTHOR  :
** FUNCTION:
** HISTORY.....:
**    11/11/2005 MS RECOMPILED - CHANGE IN TWRL5001
**    10/20/2006 RM RECOMPILED - CHANGE IN TWRL3300
**    11/27/2013 RC RECOMPILED - CHANGE IN TWRL3300
**    02/20/2015 DUTTAD RECOMPILED - CHANGES IN TWRL3300
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3330 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3300 ldaTwrl3300;
    private LdaTwrl5001 ldaTwrl5001;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rej_Gross_Unkn;
    private DbsField pnd_Rej_Gross_Roll;
    private DbsField pnd_Rej_Gross;
    private DbsField pnd_Rej_Ivc;
    private DbsField pnd_Rej_Inter;
    private DbsField pnd_Rej_Fed;
    private DbsField pnd_Rej_Can;
    private DbsField pnd_Rej_Nra;
    private DbsField pnd_Rej_Pr;
    private DbsField pnd_Rej_State;
    private DbsField pnd_Rej_Local;
    private DbsField pnd_I;
    private DbsField pnd_Err_Ndx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3300 = new LdaTwrl3300();
        registerRecord(ldaTwrl3300);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Rej_Gross_Unkn = localVariables.newFieldArrayInRecord("pnd_Rej_Gross_Unkn", "#REJ-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Rej_Gross_Roll = localVariables.newFieldArrayInRecord("pnd_Rej_Gross_Roll", "#REJ-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Rej_Gross = localVariables.newFieldArrayInRecord("pnd_Rej_Gross", "#REJ-GROSS", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            2));
        pnd_Rej_Ivc = localVariables.newFieldArrayInRecord("pnd_Rej_Ivc", "#REJ-IVC", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 2));
        pnd_Rej_Inter = localVariables.newFieldArrayInRecord("pnd_Rej_Inter", "#REJ-INTER", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_Rej_Fed = localVariables.newFieldArrayInRecord("pnd_Rej_Fed", "#REJ-FED", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 2));
        pnd_Rej_Can = localVariables.newFieldArrayInRecord("pnd_Rej_Can", "#REJ-CAN", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_Rej_Nra = localVariables.newFieldArrayInRecord("pnd_Rej_Nra", "#REJ-NRA", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_Rej_Pr = localVariables.newFieldArrayInRecord("pnd_Rej_Pr", "#REJ-PR", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_Rej_State = localVariables.newFieldArrayInRecord("pnd_Rej_State", "#REJ-STATE", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_Rej_Local = localVariables.newFieldArrayInRecord("pnd_Rej_Local", "#REJ-LOCAL", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 2));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Err_Ndx = localVariables.newFieldInRecord("pnd_Err_Ndx", "#ERR-NDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3300.initializeValues();
        ldaTwrl5001.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3330() throws Exception
    {
        super("Twrp3330");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP3330", onError);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //* *--------
        //*  REJECTED RECORDS REPORT
        //*  ERROR LINE                                                                                                                                                   //Natural: FORMAT ( 03 ) LS = 133 PS = 61
        //*  07-16-99 FRANK
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) LS = 133 PS = 61
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *   READ TRANSACTION FILE
        //* *
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD EXTRACT-NPD-FILE-0
        while (condition(getWorkFiles().read(1, ldaTwrl3300.getExtract_Npd_File_0())))
        {
            //* **IF TIRF-IRS-REJECT-IND = 'Y'
            //* **    ADD 1 TO #IRS-REJECT-CNT
            //* **    ADD 1 TO #TOTAL-IRS-REJECT-CNT
            //* **#NDX := 1
            //* ** PERFORM INCREMENT-TOTALS
            //* ** FOR #SYS-ERR-NDX = 1 TO C*TIRF-SYS-ERR
            if (condition(getReports().getAstLineCount(3).greater(59)))                                                                                                   //Natural: IF *LINE-COUNT ( 03 ) > 59
            {
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 03 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  CREATE A LINE ON THE REJECTED RECS REPORT !!!
            getReports().write(3, new ReportEmptyLineSuppression(true),ReportOption.NOTITLE,NEWLINE,ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr(),           //Natural: WRITE ( 03 ) ( ES = ON ) / EXTRACT-CNTRCT-PY-NMBR ( IS = ON ) 5X EXTRACT-ID-NMBR ( IS = ON ) 2X EXTRACT-FED-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X EXTRACT-FED-IVC-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X EXTRACT-FED-WTHHLD-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X EXTRACT-ST-GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X EXTRACT-LCL-WTHHLD-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X EXTRACT-GEO-CDE
                new IdenticalSuppress(true),new ColumnSpacing(5),ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr(), new IdenticalSuppress(true),new ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt(), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Ivc_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Wthhld_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Gross_Amt(), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Wthhld_Amt(), new ReportEditMask 
                ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),ldaTwrl3300.getExtract_Npd_File_0_Extract_Geo_Cde());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *    1X #DATE2
            //* *    TWRT-INTEREST-AMT
            //* *    TWRT-CITIZENSHIP
            //*  IF EXTRACT-ID-NMBR EQ '127502859'
            //*   WRITE '=' EXTRACT-ID-NMBR
            //*       / '=' EXTRACT-CNTR-SYS-ERR
            //*       / '=' EXTRACT-SYS-ERR (1)
            //*       / '=' EXTRACT-SYS-ERR (2)
            //*       / '=' EXTRACT-SYS-ERR (3)
            //*       / '=' #TWRL5001.#ERR-DESC (32)
            //*  END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO EXTRACT-CNTR-SYS-ERR
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntr_Sys_Err())); pnd_I.nadd(1))
            {
                pnd_Err_Ndx.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Sys_Err().getValue(pnd_I));                                                                //Natural: ASSIGN #ERR-NDX := EXTRACT-SYS-ERR ( #I )
                getReports().write(3, new ReportEmptyLineSuppression(true),ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(20),"Error #",pnd_Err_Ndx,              //Natural: WRITE ( 03 ) ( ES = ON ) / / 20X 'Error #' #ERR-NDX ( ZP = OFF ) 2X #TWRL5001.#ERR-DESC ( #ERR-NDX )
                    new ReportZeroPrint (false),new ColumnSpacing(2),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Err_Ndx.getInt() + 1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *  IF #PURI > 0 OR #CAN-AMT > 0
            //* *    WRITE (03)  99X #PURI #CAN-AMT
            //* *  END-IF
            //* *WRITE (03) 33X 'Error: ' TIRCNTL-ERROR-DESCR ' - Rejected'
            //*  END OF EXTRACT PROCESSING
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*   CREATE EDITS DETAIL REPORT (REJECTED RECORDS) **
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 03 )
        //* *---------
        //* *
        //* * ........ERROR PROCESSING ROUTINE ........
        //* *
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP3330-03",new                    //Natural: WRITE ( 03 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP3330-03' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 03 ) / 38X 'Rejected Tax Transactions (EDITS)' 31X 'Date: '*DATU / // '               Taxpayer           Gross      ' '    IVC       Fed Tax         State         Local   ' / 'Contrct-Py     Ind.ID No         Amount        Amount' '       Amount        Amount        Amount   ' / '                                                                ' '                                                                 '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(3),NEWLINE,new 
                        ColumnSpacing(38),"Rejected Tax Transactions (EDITS)",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,NEWLINE,NEWLINE,"               Taxpayer           Gross      ",
                        "    IVC       Fed Tax         State         Local   ",NEWLINE,"Contrct-Py     Ind.ID No         Amount        Amount","       Amount        Amount        Amount   ",
                        NEWLINE,"                                                                ","                                                                 ");
                    //* *  'System -'#FEEDER-DESCR
                    //* *  54X #COMP-NAME
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        getReports().write(0, NEWLINE,"********************************************************",NEWLINE,"Program Name:",Global.getPROGRAM()," Error:",                   //Natural: WRITE ( 0 ) / '********************************************************' / 'Program Name:'*PROGRAM ' Error:' *ERROR-NR 'Line:' *ERROR-LINE / '********************************************************'
            Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"********************************************************");
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(3, "LS=133 PS=61");
        Global.format(0, "LS=133 PS=61");
    }
}
