/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:53 PM
**        * FROM NATURAL PROGRAM : Twrp8161
************************************************************
**        * FILE NAME            : Twrp8161.java
**        * CLASS NAME           : Twrp8161
**        * INSTANCE NAME        : Twrp8161
************************************************************
************************************************************************
** PROGRAM : TWRP8161
** SYSTEM  : TAXWARS
** AUTHOR  : J.ROTHOLZ
** FUNCTION: INITIALIZE TBL4 RECORDS W/PUERTO RICO SERIAL NUMBER RANGES
** HISTORY.....:
**    02/03/2011 - J.ROTHOLZ  -  CLONED FROM SUPPCNTL
**
**                   PARM WITH NEW CONTROL NUMBER RANGE PROVIDED BY
**                   PUERTO RICO WILL HAVE TO BE MIGRATED EACH YEAR
**                   BEFORE RUNNING THE JOB (PTW1426R).
**
**                   NOTE:  WHEN ENTERING NEW RANGE IN PARM, SET "CURR"
**                   NUMBER TO "START" VALUE -1 (MINUS 1).
**
**    08/26/2015 - D.DUTTA    -  CORRECTION REPORTING CHANGES
**
**                   ADDED ONE MORE PARAMETER TO BE PASSED FROM JCL FOR
**                   IDENTIFYING IF IT IS ORIGINAL (ORIG) OR CORRECTION
**                   (CORR) REPORTING. TAG DUTTAD.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8161 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_tircntl_Rpt_Corr_U;
    private DbsField tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Start;
    private DbsField tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_End;
    private DbsField tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Curr;
    private DbsField pnd_I;

    private DbsGroup pnd_Input_Parameters;
    private DbsField pnd_Input_Parameters_Pnd_In_Pr_Year;
    private DbsField pnd_Input_Parameters_Pnd_In_Pr_Start;
    private DbsField pnd_Input_Parameters_Pnd_In_Pr_End;
    private DbsField pnd_Input_Parameters_Pnd_In_Pr_Curr;
    private DbsField pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);

        // Local Variables

        vw_tircntl_Rpt_Corr_U = new DataAccessProgramView(new NameInfo("vw_tircntl_Rpt_Corr_U", "TIRCNTL-RPT-CORR-U"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Start = vw_tircntl_Rpt_Corr_U.getRecord().newFieldInGroup("tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Start", 
            "TIRCNTL-PR-AMND-CNTL-START", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRCNTL_PR_AMND_CNTL_START");
        tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_End = vw_tircntl_Rpt_Corr_U.getRecord().newFieldInGroup("tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_End", 
            "TIRCNTL-PR-AMND-CNTL-END", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRCNTL_PR_AMND_CNTL_END");
        tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Curr = vw_tircntl_Rpt_Corr_U.getRecord().newFieldInGroup("tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Curr", 
            "TIRCNTL-PR-AMND-CNTL-CURR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRCNTL_PR_AMND_CNTL_CURR");
        registerRecord(vw_tircntl_Rpt_Corr_U);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);

        pnd_Input_Parameters = localVariables.newGroupInRecord("pnd_Input_Parameters", "#INPUT-PARAMETERS");
        pnd_Input_Parameters_Pnd_In_Pr_Year = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Pr_Year", "#IN-PR-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Input_Parameters_Pnd_In_Pr_Start = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Pr_Start", "#IN-PR-START", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_Pr_End = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Pr_End", "#IN-PR-END", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_Pr_Curr = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Pr_Curr", "#IN-PR-CURR", FieldType.STRING, 
            10);
        pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind", "#IN-ORIG-CORR-IND", 
            FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_tircntl_Rpt_Corr_U.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8161() throws Exception
    {
        super("Twrp8161");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8161|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parameters_Pnd_In_Pr_Year,pnd_Input_Parameters_Pnd_In_Pr_Start,pnd_Input_Parameters_Pnd_In_Pr_End, //Natural: INPUT #IN-PR-YEAR #IN-PR-START #IN-PR-END #IN-PR-CURR #IN-ORIG-CORR-IND
                    pnd_Input_Parameters_Pnd_In_Pr_Curr,pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind);
                //*                                                              /* DUTTAD
                //*  DUTTAD
                //*  480.6B
                getReports().write(0, ReportOption.NOTITLE,"=",pnd_Input_Parameters_Pnd_In_Pr_Year,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_Pr_Start,NEWLINE,              //Natural: WRITE '=' #IN-PR-YEAR / '=' #IN-PR-START / '=' #IN-PR-END / '=' #IN-PR-CURR / '=' #IN-ORIG-CORR-IND /
                    "=",pnd_Input_Parameters_Pnd_In_Pr_End,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_Pr_Curr,NEWLINE,"=",pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind,
                    NEWLINE);
                if (Global.isEscape()) return;
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 00 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 23T '480.6b Control Records - Ser.#s Initialized ' 68T 'Report: RPT1' / //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                FOR01:                                                                                                                                                    //Natural: FOR #I = 5 TO 6
                for (pnd_I.setValue(5); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_I);                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Input_Parameters_Pnd_In_Pr_Year);                                                             //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #IN-PR-YEAR
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                         //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                           //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                    DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                        pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                    if (condition(Global.isEscape())) return;
                    //*  DUTTAD
                    if (condition(pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind.equals("ORIG")))                                                                              //Natural: IF #IN-ORIG-CORR-IND = 'ORIG'
                    {
                        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_Start().setValue(pnd_Input_Parameters_Pnd_In_Pr_Start);                                               //Natural: ASSIGN TIRCNTL-PR-CNTL-START := #IN-PR-START
                        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_End().setValue(pnd_Input_Parameters_Pnd_In_Pr_End);                                                   //Natural: ASSIGN TIRCNTL-PR-CNTL-END := #IN-PR-END
                        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_Curr().setValue(pnd_Input_Parameters_Pnd_In_Pr_Curr);                                                 //Natural: ASSIGN TIRCNTL-PR-CNTL-CURR := #IN-PR-CURR
                        getReports().write(0, ReportOption.NOTITLE," before update ");                                                                                    //Natural: WRITE ' before update '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                            pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  << DUTTAD
                        if (condition(pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind.equals("CORR")))                                                                          //Natural: IF #IN-ORIG-CORR-IND = 'CORR'
                        {
                            G0:                                                                                                                                           //Natural: GET TIRCNTL-RPT-CORR-U #TWRATBL4.#ISN
                            vw_tircntl_Rpt_Corr_U.readByID(pdaTwratbl4.getPnd_Twratbl4_Pnd_Isn().getLong(), "G0");
                            tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Start.setValue(pnd_Input_Parameters_Pnd_In_Pr_Start);                                                 //Natural: ASSIGN TIRCNTL-RPT-CORR-U.TIRCNTL-PR-AMND-CNTL-START := #IN-PR-START
                            tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_End.setValue(pnd_Input_Parameters_Pnd_In_Pr_End);                                                     //Natural: ASSIGN TIRCNTL-RPT-CORR-U.TIRCNTL-PR-AMND-CNTL-END := #IN-PR-END
                            tircntl_Rpt_Corr_U_Tircntl_Pr_Amnd_Cntl_Curr.setValue(pnd_Input_Parameters_Pnd_In_Pr_Curr);                                                   //Natural: ASSIGN TIRCNTL-RPT-CORR-U.TIRCNTL-PR-AMND-CNTL-CURR := #IN-PR-CURR
                            vw_tircntl_Rpt_Corr_U.updateDBRow("G0");                                                                                                      //Natural: UPDATE ( G0. )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  >> DUTTAD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  DUTTAD
                if (condition(pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind.equals("ORIG")))                                                                                  //Natural: IF #IN-ORIG-CORR-IND = 'ORIG'
                {
                    getReports().write(0, ReportOption.NOTITLE,"*** Puerto Rico Form Serial Numbers Initialized ***",NEWLINE,"in",pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), //Natural: WRITE '*** Puerto Rico Form Serial Numbers Initialized ***' / 'in' #TWRATBL4.#INPUT-PARMS / 'out' #TWRATBL4.TIRCNTL-PR-CNTL-START #TWRATBL4.TIRCNTL-PR-CNTL-END #TWRATBL4.TIRCNTL-PR-CNTL-CURR
                        NEWLINE,"out",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_Start(),pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_End(),pdaTwratbl4.getPnd_Twratbl4_Tircntl_Pr_Cntl_Curr());
                    if (Global.isEscape()) return;
                    //*  << DUTTAD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Parameters_Pnd_In_Orig_Corr_Ind.equals("CORR")))                                                                              //Natural: IF #IN-ORIG-CORR-IND = 'CORR'
                    {
                        getReports().write(0, ReportOption.NOTITLE,"*** Puerto Rico Form Serial Numbers Initialized ***",NEWLINE,"in",pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), //Natural: WRITE '*** Puerto Rico Form Serial Numbers Initialized ***' / 'in' #TWRATBL4.#INPUT-PARMS / 'out' #IN-PR-START #IN-PR-END #IN-PR-CURR
                            NEWLINE,"out",pnd_Input_Parameters_Pnd_In_Pr_Start,pnd_Input_Parameters_Pnd_In_Pr_End,pnd_Input_Parameters_Pnd_In_Pr_Curr);
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    //*  >> DUTTAD
                }                                                                                                                                                         //Natural: END-IF
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");

        getReports().write(0, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(23),"480.6b Control Records - Ser.#s Initialized ",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
