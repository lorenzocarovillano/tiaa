/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:17 PM
**        * FROM NATURAL PROGRAM : Twrp4440
************************************************************
**        * FILE NAME            : Twrp4440.java
**        * CLASS NAME           : Twrp4440
**        * INSTANCE NAME        : Twrp4440
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4440  (IRS - 5498  ORIGINAL REPORTING).
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCE FORM 5498 IRS TAPE FILE.
* CREATED  : 09 / 30 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM CREATES THE IRS TAPE FOR ALL FORM 5498 RECORDS.
*          : (ORIGINAL REPORTING).
* HISTORY  :
* 11-17-09 : A. YOUNG        - RE-COMPILED TO ACCOMMODATE 2009 IRS
*          :                   CHANGES TO 'B', 'C', AND 'K' RECS.
* 10-01-08 : A. YOUNG        - TWRL443A REVISED PER IRS PUB 1220 SPECS.
*          :                   #T-MAGNETIC-TAPE-FILE-IND DELETED,
*          :                   #T-MEDIA-NUMBER DELETED, #T-FILLER-3
*          :                   INCREASED FROM A83 - A91.
*          :                 - RE-COMPILED DUE TO TWRL443A.
* 10/02/07 : R. MA  LDA TWRL443B UPDATED PER IRS 1220 SPEC.
*          :        TAKE OUT LINE FOR POPULATING #A-ORIGINAL-FILE-IND.
*          :        UPDATE LOGIC TO INCLUDE #F-IRA-RECHAR NE 0 WHEN
*          :        POPULATING IRA, SEP AND ROTH IRA INDICATORS IN'B'
*          :        RECORD.
* 05-01-07 : A. YOUNG   - REVISED TO CALL NAME CONTROL SUB-ROUTINE
* 03-06-07 :              'TWRN5052' TO POPULATE IRS FIELD
*          :              IRS-B-NAME-CONTROL.
* 10-31-06 : J.ROTHOLZ - RE-STOWED FOR 2006 CHANGES IN LDA (TWRL443A)
* 07/07/06 : R. MA  (TO FIX PRODUCTION FILE WHICH WAS REJECTED BY IRS)
*          :        POPULATE 'A' RECORD AMOUNT-CODES PER IRS 1220 SPEC.
*          :        TAKE OUT OBSOLETE CODING TO FIX INDICATOR PROBLEM.
* 11-29-05 : BK              - ADDED SEP AMT (8) AND LOGIC
*          :                 - ADDED LOGIC FOR TEST INDICATOR
* 12-31-04 : A. YOUNG        - REVISED TO ADD 1 PREFIX TO
*          :                   #T-CONTACT-PHONE-N-EXTENSION (N15). WHEN
*          :                   POPULATED WITH #TWRACOM2.#PHONE (A14), A
*          :                   LEADING 0 WAS IN THE FIRST BYTE.
* 12-03-04 : A. YOUNG        - REVISED TO READ COMPANY TABLE AFTER FIRST
*          :                   FORM RECORD WAS READ IN ORDER TO POPULATE
*          :                   #TWRACOM2.#TAX-YEAR.
*          :                 - REVISED TO POPULATE #TWRACOM2.#COMP-CODE
*          :                   AND #TWRACOM2.#TAX-YEAR PARAMETERS BEFORE
*          :                   CALLING COMPANY TABLE FOR TIAA.
*          :                 - POPULATE COMPANY DATA FROM COMPANY TABLE
*          :                   FOR 'T'AND 'A' RECORDS.
*          :                 - REVISED TO POPULATE UNIQUE ACCOUNT NUMBER
*          :                   FROM #F-TIN (LAST 4 DIGITS)
*          :                        #F-CONTRACT-NBR
*          :                        #F-PAYEE-CDE
*          :                        #F-FORM-SEQ
*          :                   PER IRS AND TAX COMPLIANCE USER
*          :                   REQUIREMENTS.
* 10-13-10 : J. ROTHOLZ      - 2010 CHANGES IN LDA TWRL443B
* 12-07-16 : SNEHA SINHA     - ADDED LOGIC FOR TEST INDICATOR - SINHASN
* 12-03-17 : DASDH           - 5498 BOX CHANGES
* 10-05-18 : ARIVU           - EIN CHANGE - TAG: EINCHG
* 31-10-18 : VIKRAM          - 5498 YEAR OF POSTPONED CONTRIBUTION
* 18-11-20 : PALDE           - IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4440 extends BLNatBase
{
    // Data Areas
    private PdaTwra5052 pdaTwra5052;
    private LdaTwrl442a ldaTwrl442a;
    private LdaTwrl443a ldaTwrl443a;
    private LdaTwrl443b ldaTwrl443b;
    private LdaTwrl443c ldaTwrl443c;
    private LdaTwrl443d ldaTwrl443d;
    private LdaTwrl443e ldaTwrl443e;
    private LdaTwrl443f ldaTwrl443f;
    private PdaTwracom2 pdaTwracom2;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_state;
    private DbsField state_Tircntl_Tbl_Nbr;
    private DbsField state_Tircntl_Tax_Year;

    private DbsGroup state__R_Field_1;
    private DbsField state_Tircntl_Tax_Year_A;
    private DbsField state_Tircntl_Seq_Nbr;

    private DbsGroup state__R_Field_2;
    private DbsField state_Pnd_Filler_1;
    private DbsField state_Pnd_State_Irs_Code;
    private DbsField state_Tircntl_State_Old_Code;

    private DbsGroup state__R_Field_3;
    private DbsField state_Pnd_Filler_2;
    private DbsField state_Pnd_State_Num_Code;
    private DbsField state_Tircntl_State_Alpha_Code;

    private DbsGroup state__R_Field_4;
    private DbsField state_Pnd_State_Alpha_Code;
    private DbsField state_Pnd_Filler_3;
    private DbsField state_Tircntl_State_Full_Name;
    private DbsField state_Tircntl_Comb_Fed_Ind;
    private DbsField pnd_Super_Nbr_Year;

    private DbsGroup pnd_Super_Nbr_Year__R_Field_5;
    private DbsField pnd_Super_Nbr_Year_Pnd_Super_Nbr;
    private DbsField pnd_Super_Nbr_Year_Pnd_Super_Year;
    private DbsField pnd_T_State_Cnt;
    private DbsField pnd_T_Num_Code;
    private DbsField pnd_T_Alpha_Code;
    private DbsField pnd_T_Province;
    private DbsField pnd_T_Combined;
    private DbsField pnd_T_Irs_Code;
    private DbsField pnd_K_Irs_Code;
    private DbsField pnd_K_Payees;
    private DbsField pnd_K_Tot_1;
    private DbsField pnd_K_Tot_2;
    private DbsField pnd_K_Tot_3;
    private DbsField pnd_K_Tot_4;
    private DbsField pnd_K_Tot_5;
    private DbsField pnd_K_Tot_A;
    private DbsField pnd_K_Tot_8;
    private DbsField pnd_K_Tot_C;
    private DbsField pnd_K_Tot_D;
    private DbsField pnd_W_Province;
    private DbsField pnd_W_Province_3bytes;

    private DbsGroup pnd_W_Province_3bytes__R_Field_6;
    private DbsField pnd_W_Province_3bytes_Pnd_W_Province_Zero;
    private DbsField pnd_W_Province_3bytes_Pnd_W_Province_Code;
    private DbsField pnd_Total_B_Recs_Record;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Tin_Last_4_A;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Number_Of_A_Recs;
    private DbsField pnd_T_Ctr;
    private DbsField pnd_A_Ctr;
    private DbsField pnd_B_Ctr;
    private DbsField pnd_C_Ctr;
    private DbsField pnd_K_Ctr;
    private DbsField pnd_F_Ctr;
    private DbsField pnd_N_Ctr;
    private DbsField pnd_X_Ctr;
    private DbsField pnd_Bad_Count;
    private DbsField pnd_Sequence_Number;
    private DbsField pnd_Combined_Found;
    private DbsField i;
    private DbsField k;
    private DbsField t;
    private DbsField u;
    private DbsField pnd_Job_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwra5052 = new PdaTwra5052(localVariables);
        ldaTwrl442a = new LdaTwrl442a();
        registerRecord(ldaTwrl442a);
        ldaTwrl443a = new LdaTwrl443a();
        registerRecord(ldaTwrl443a);
        ldaTwrl443b = new LdaTwrl443b();
        registerRecord(ldaTwrl443b);
        ldaTwrl443c = new LdaTwrl443c();
        registerRecord(ldaTwrl443c);
        ldaTwrl443d = new LdaTwrl443d();
        registerRecord(ldaTwrl443d);
        ldaTwrl443e = new LdaTwrl443e();
        registerRecord(ldaTwrl443e);
        ldaTwrl443f = new LdaTwrl443f();
        registerRecord(ldaTwrl443f);
        pdaTwracom2 = new PdaTwracom2(localVariables);

        // Local Variables

        vw_state = new DataAccessProgramView(new NameInfo("vw_state", "STATE"), "TIRCNTL_STATE_CODE_TBL_VIEW", "TIR_CONTROL");
        state_Tircntl_Tbl_Nbr = vw_state.getRecord().newFieldInGroup("state_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        state_Tircntl_Tax_Year = vw_state.getRecord().newFieldInGroup("state_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");

        state__R_Field_1 = vw_state.getRecord().newGroupInGroup("state__R_Field_1", "REDEFINE", state_Tircntl_Tax_Year);
        state_Tircntl_Tax_Year_A = state__R_Field_1.newFieldInGroup("state_Tircntl_Tax_Year_A", "TIRCNTL-TAX-YEAR-A", FieldType.STRING, 4);
        state_Tircntl_Seq_Nbr = vw_state.getRecord().newFieldInGroup("state_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        state__R_Field_2 = vw_state.getRecord().newGroupInGroup("state__R_Field_2", "REDEFINE", state_Tircntl_Seq_Nbr);
        state_Pnd_Filler_1 = state__R_Field_2.newFieldInGroup("state_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        state_Pnd_State_Irs_Code = state__R_Field_2.newFieldInGroup("state_Pnd_State_Irs_Code", "#STATE-IRS-CODE", FieldType.STRING, 2);
        state_Tircntl_State_Old_Code = vw_state.getRecord().newFieldInGroup("state_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_OLD_CODE");

        state__R_Field_3 = vw_state.getRecord().newGroupInGroup("state__R_Field_3", "REDEFINE", state_Tircntl_State_Old_Code);
        state_Pnd_Filler_2 = state__R_Field_3.newFieldInGroup("state_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        state_Pnd_State_Num_Code = state__R_Field_3.newFieldInGroup("state_Pnd_State_Num_Code", "#STATE-NUM-CODE", FieldType.STRING, 2);
        state_Tircntl_State_Alpha_Code = vw_state.getRecord().newFieldInGroup("state_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_STATE_ALPHA_CODE");

        state__R_Field_4 = vw_state.getRecord().newGroupInGroup("state__R_Field_4", "REDEFINE", state_Tircntl_State_Alpha_Code);
        state_Pnd_State_Alpha_Code = state__R_Field_4.newFieldInGroup("state_Pnd_State_Alpha_Code", "#STATE-ALPHA-CODE", FieldType.STRING, 2);
        state_Pnd_Filler_3 = state__R_Field_4.newFieldInGroup("state_Pnd_Filler_3", "#FILLER-3", FieldType.STRING, 1);
        state_Tircntl_State_Full_Name = vw_state.getRecord().newFieldInGroup("state_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", FieldType.STRING, 
            19, RepeatingFieldStrategy.None, "TIRCNTL_STATE_FULL_NAME");
        state_Tircntl_Comb_Fed_Ind = vw_state.getRecord().newFieldInGroup("state_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_COMB_FED_IND");
        registerRecord(vw_state);

        pnd_Super_Nbr_Year = localVariables.newFieldInRecord("pnd_Super_Nbr_Year", "#SUPER-NBR-YEAR", FieldType.STRING, 5);

        pnd_Super_Nbr_Year__R_Field_5 = localVariables.newGroupInRecord("pnd_Super_Nbr_Year__R_Field_5", "REDEFINE", pnd_Super_Nbr_Year);
        pnd_Super_Nbr_Year_Pnd_Super_Nbr = pnd_Super_Nbr_Year__R_Field_5.newFieldInGroup("pnd_Super_Nbr_Year_Pnd_Super_Nbr", "#SUPER-NBR", FieldType.NUMERIC, 
            1);
        pnd_Super_Nbr_Year_Pnd_Super_Year = pnd_Super_Nbr_Year__R_Field_5.newFieldInGroup("pnd_Super_Nbr_Year_Pnd_Super_Year", "#SUPER-YEAR", FieldType.STRING, 
            4);
        pnd_T_State_Cnt = localVariables.newFieldInRecord("pnd_T_State_Cnt", "#T-STATE-CNT", FieldType.PACKED_DECIMAL, 4);
        pnd_T_Num_Code = localVariables.newFieldArrayInRecord("pnd_T_Num_Code", "#T-NUM-CODE", FieldType.STRING, 2, new DbsArrayController(1, 100));
        pnd_T_Alpha_Code = localVariables.newFieldArrayInRecord("pnd_T_Alpha_Code", "#T-ALPHA-CODE", FieldType.STRING, 2, new DbsArrayController(1, 100));
        pnd_T_Province = localVariables.newFieldArrayInRecord("pnd_T_Province", "#T-PROVINCE", FieldType.STRING, 19, new DbsArrayController(1, 100));
        pnd_T_Combined = localVariables.newFieldArrayInRecord("pnd_T_Combined", "#T-COMBINED", FieldType.STRING, 1, new DbsArrayController(1, 100));
        pnd_T_Irs_Code = localVariables.newFieldArrayInRecord("pnd_T_Irs_Code", "#T-IRS-CODE", FieldType.STRING, 2, new DbsArrayController(1, 100));
        pnd_K_Irs_Code = localVariables.newFieldArrayInRecord("pnd_K_Irs_Code", "#K-IRS-CODE", FieldType.STRING, 2, new DbsArrayController(1, 60));
        pnd_K_Payees = localVariables.newFieldArrayInRecord("pnd_K_Payees", "#K-PAYEES", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 60));
        pnd_K_Tot_1 = localVariables.newFieldArrayInRecord("pnd_K_Tot_1", "#K-TOT-1", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_2 = localVariables.newFieldArrayInRecord("pnd_K_Tot_2", "#K-TOT-2", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_3 = localVariables.newFieldArrayInRecord("pnd_K_Tot_3", "#K-TOT-3", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_4 = localVariables.newFieldArrayInRecord("pnd_K_Tot_4", "#K-TOT-4", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_5 = localVariables.newFieldArrayInRecord("pnd_K_Tot_5", "#K-TOT-5", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_A = localVariables.newFieldArrayInRecord("pnd_K_Tot_A", "#K-TOT-A", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_8 = localVariables.newFieldArrayInRecord("pnd_K_Tot_8", "#K-TOT-8", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_C = localVariables.newFieldArrayInRecord("pnd_K_Tot_C", "#K-TOT-C", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_K_Tot_D = localVariables.newFieldArrayInRecord("pnd_K_Tot_D", "#K-TOT-D", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 60));
        pnd_W_Province = localVariables.newFieldInRecord("pnd_W_Province", "#W-PROVINCE", FieldType.STRING, 19);
        pnd_W_Province_3bytes = localVariables.newFieldInRecord("pnd_W_Province_3bytes", "#W-PROVINCE-3BYTES", FieldType.STRING, 3);

        pnd_W_Province_3bytes__R_Field_6 = localVariables.newGroupInRecord("pnd_W_Province_3bytes__R_Field_6", "REDEFINE", pnd_W_Province_3bytes);
        pnd_W_Province_3bytes_Pnd_W_Province_Zero = pnd_W_Province_3bytes__R_Field_6.newFieldInGroup("pnd_W_Province_3bytes_Pnd_W_Province_Zero", "#W-PROVINCE-ZERO", 
            FieldType.STRING, 1);
        pnd_W_Province_3bytes_Pnd_W_Province_Code = pnd_W_Province_3bytes__R_Field_6.newFieldInGroup("pnd_W_Province_3bytes_Pnd_W_Province_Code", "#W-PROVINCE-CODE", 
            FieldType.STRING, 2);
        pnd_Total_B_Recs_Record = localVariables.newFieldInRecord("pnd_Total_B_Recs_Record", "#TOTAL-B-RECS-RECORD", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Tin_Last_4_A = localVariables.newFieldInRecord("pnd_Tin_Last_4_A", "#TIN-LAST-4-A", FieldType.STRING, 4);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Number_Of_A_Recs = localVariables.newFieldInRecord("pnd_Number_Of_A_Recs", "#NUMBER-OF-A-RECS", FieldType.NUMERIC, 8);
        pnd_T_Ctr = localVariables.newFieldInRecord("pnd_T_Ctr", "#T-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_A_Ctr = localVariables.newFieldInRecord("pnd_A_Ctr", "#A-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_B_Ctr = localVariables.newFieldInRecord("pnd_B_Ctr", "#B-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_C_Ctr = localVariables.newFieldInRecord("pnd_C_Ctr", "#C-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_K_Ctr = localVariables.newFieldInRecord("pnd_K_Ctr", "#K-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_F_Ctr = localVariables.newFieldInRecord("pnd_F_Ctr", "#F-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_N_Ctr = localVariables.newFieldInRecord("pnd_N_Ctr", "#N-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_X_Ctr = localVariables.newFieldInRecord("pnd_X_Ctr", "#X-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Bad_Count = localVariables.newFieldInRecord("pnd_Bad_Count", "#BAD-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Sequence_Number = localVariables.newFieldInRecord("pnd_Sequence_Number", "#SEQUENCE-NUMBER", FieldType.PACKED_DECIMAL, 8);
        pnd_Combined_Found = localVariables.newFieldInRecord("pnd_Combined_Found", "#COMBINED-FOUND", FieldType.BOOLEAN, 1);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        t = localVariables.newFieldInRecord("t", "T", FieldType.PACKED_DECIMAL, 4);
        u = localVariables.newFieldInRecord("u", "U", FieldType.PACKED_DECIMAL, 4);
        pnd_Job_Name = localVariables.newFieldInRecord("pnd_Job_Name", "#JOB-NAME", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_state.reset();

        ldaTwrl442a.initializeValues();
        ldaTwrl443a.initializeValues();
        ldaTwrl443b.initializeValues();
        ldaTwrl443c.initializeValues();
        ldaTwrl443d.initializeValues();
        ldaTwrl443e.initializeValues();
        ldaTwrl443f.initializeValues();

        localVariables.reset();
        pnd_Tin_Last_4_A.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4440() throws Exception
    {
        super("Twrp4440");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4440", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  << SINHASN
        //*  IF *APPLIC-ID = 'PROJNTAX'                   /* BK
        //*  #JOB-NAME  := *INIT-USER
        //*  IF  SUBSTR(#JOB-NAME,1,5) =  'F1610' OR = 'T1610'
        if (condition(Global.getAPPLIC_ID().equals("PROJNTAX") || Global.getAPPLIC_ID().equals("PROJPTAX") || Global.getAPPLIC_ID().equals("PROJCTAX")))                  //Natural: IF *APPLIC-ID = 'PROJNTAX' OR = 'PROJPTAX' OR = 'PROJCTAX'
        {
            //*  >> SINHASN
            ldaTwrl443a.getPnd_T_Tape_Pnd_T_Test_File_Indicator().setValue("T");                                                                                          //Natural: ASSIGN #T-TEST-FILE-INDICATOR := 'T'
            //*  BK
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-TOTAL-B-RECORDS
        sub_Read_Total_B_Records();
        if (condition(Global.isEscape())) {return;}
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, ldaTwrl442a.getPnd_Form())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                //*  12-03-2004
                                                                                                                                                                          //Natural: PERFORM EXTRACT-COMPANY-DATA
                sub_Extract_Company_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
                sub_Load_State_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-T-RECORD
                sub_Create_T_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
                sub_Create_A_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_A_Rec_Company.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde());                                                                             //Natural: ASSIGN #PREV-A-REC-COMPANY := #F-COMPANY-CDE
                pnd_Prev_C_Rec_Company.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde());                                                                             //Natural: ASSIGN #PREV-C-REC-COMPANY := #F-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_C_Rec_Company.equals(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde())))                                                                    //Natural: IF #PREV-C-REC-COMPANY = #F-COMPANY-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
                sub_Create_C_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_C_Rec_Company.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde());                                                                             //Natural: ASSIGN #PREV-C-REC-COMPANY := #F-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_A_Rec_Company.equals(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde())))                                                                    //Natural: IF #PREV-A-REC-COMPANY = #F-COMPANY-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
                sub_Create_A_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_A_Rec_Company.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde());                                                                             //Natural: ASSIGN #PREV-A-REC-COMPANY := #F-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-B-RECORD
            sub_Create_B_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-RECORD
            sub_Update_C_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form (5498) Extract File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new                   //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) Extract File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
        sub_Create_C_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-K-RECORDS
        sub_Create_K_Records();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-F-RECORD
        sub_Create_F_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  #B-NAME-CONTROL  :=   #F-4-CHAR-LAST-NAME
        //*  IF SUBSTR(#B-TAXPAYER-ID-NUMB,1,2) = '99'
        //*    #TIN-LAST-4-A := SUBSTR(#B-CONTRACT-PAYEE,7,4)
        //*  ELSE
        //*  END-IF
        //*                             TRADITIONAL IRA = '01', ROLLOVER IRA ='03'
        //* *------------
        //* *------------
        //* *--------------------------------
        //* *------------
        //*   #A-ORIGINAL-FILE-IND     :=  '1'   FIELD TAKEN OUT    10/02/07    RM
        //*  #A-AMOUNT-INDICATORS  :=  '123458A'
        //*  #A-AMOUNT-INDICATORS  :=  '123458AC'
        //* *------------
        //* *------------
        //* *---------------------------------
        //* *------------
        //*  #T-CONTACT-PHONE-N-EXTENSION   :=  100000000000000
        //*    +  VAL(#TWRACOM2.#PHONE)
        //* *------------
        //* *------------
        //* *------------
        //* *---------------------------------
        //* **  LOAD STATE CODE TABLE 2 INTO MEMORY  ***
        //* *------------
        //* *-------------------------------------
        //* *------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Create_B_Record() throws Exception                                                                                                                   //Natural: CREATE-B-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //*  BK
        //*  DASDH
        //*  DASDH
        //*  PALDE
        //*  03-06-07
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_1().reset();                                                                                                       //Natural: RESET #B-PAYMENT-AMOUNT-1 #B-PAYMENT-AMOUNT-2 #B-PAYMENT-AMOUNT-3 #B-PAYMENT-AMOUNT-4 #B-PAYMENT-AMOUNT-5 #B-PAYMENT-AMOUNT-A #B-PAYMENT-AMOUNT-8 #B-PAYMENT-AMOUNT-C #B-POSTPN-CODE #B-PAYMENT-AMOUNT-D #B-REP-CODE
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_2().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_3().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_4().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_5().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_A().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_8().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Postpn_Code().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_D().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Rep_Code().reset();
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Year().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year());                                                                //Natural: ASSIGN #B-PAYMENT-YEAR := #F-TAX-YEAR
        pdaTwra5052.getPnd_Twra5052_Pnd_Lname().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Part_Last_Nme());                                                                  //Natural: ASSIGN #TWRA5052.#LNAME := #F-PART-LAST-NME
        //*  CALLNAT    'TWRN5052' #TWRA5052.#INPUT-PARMS               /* 03-06-07
        //*  04-10-07
        DbsUtil.callnat(Twrn5052.class , getCurrentProcessState(), pdaTwra5052.getPnd_Twra5052());                                                                        //Natural: CALLNAT 'TWRN5052' #TWRA5052
        if (condition(Global.isEscape())) return;
        //*  04-10-07
        if (condition(pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #TWRA5052.#RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"IRS 5498 Original Abend In Name Control 'TWRN5052'",new TabSetting(77),"***",NEWLINE,"***",new                 //Natural: WRITE ( 00 ) '***' 06T 'IRS 5498 Original Abend In Name Control "TWRN5052"' 77T '***' / '***' 06T 'Return Message...:' #TWRA5052.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(6),"Return Message...:",pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(93);  if (true) return;                                                                                                                     //Natural: TERMINATE 93
            //*  03-06-07
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Name_Control().setValue(pdaTwra5052.getPnd_Twra5052_Pnd_Lname_Control());                                                         //Natural: ASSIGN #B-NAME-CONTROL := #TWRA5052.#LNAME-CONTROL
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Type_Of_Tin().setValue(" ");                                                                                                      //Natural: ASSIGN #B-TYPE-OF-TIN := ' '
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb().setValue(" ");                                                                                                 //Natural: ASSIGN #B-TAXPAYER-ID-NUMB := ' '
        //*  12-03-2004
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Contract_Payee().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl442a.getPnd_Form_Pnd_F_Contract_Nbr(),           //Natural: COMPRESS #F-CONTRACT-NBR #F-PAYEE-CDE INTO #B-CONTRACT-PAYEE LEAVING NO SPACE
            ldaTwrl442a.getPnd_Form_Pnd_F_Payee_Cde()));
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Id_Type().equals("1") || ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Id_Type().equals("3")))                                //Natural: IF #F-TAX-ID-TYPE = '1' OR = '3'
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Type_Of_Tin().setValue("2");                                                                                                  //Natural: ASSIGN #B-TYPE-OF-TIN := '2'
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tin());                                                             //Natural: ASSIGN #B-TAXPAYER-ID-NUMB := #F-TIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Id_Type().equals("2")))                                                                                       //Natural: IF #F-TAX-ID-TYPE = '2'
            {
                ldaTwrl443c.getPnd_B_Tape_Pnd_B_Type_Of_Tin().setValue("1");                                                                                              //Natural: ASSIGN #B-TYPE-OF-TIN := '1'
                ldaTwrl443c.getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tin());                                                         //Natural: ASSIGN #B-TAXPAYER-ID-NUMB := #F-TIN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '=' #B-TAXPAYER-ID-NUMB
        if (condition(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb().equals(" ")))                                                                                    //Natural: IF #B-TAXPAYER-ID-NUMB = ' '
        {
            pnd_N_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #N-CTR
            //*  12-03-2004
            pnd_Tin_Last_4_A.reset();                                                                                                                                     //Natural: RESET #TIN-LAST-4-A #B-TIN-LAST-4
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Tin_Last_4().reset();
            //*  12-03-2004
            //*  12-03-2004
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tin_Last_4_A.setValue(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb().getSubstring(6,4));                                                              //Natural: ASSIGN #TIN-LAST-4-A := SUBSTR ( #B-TAXPAYER-ID-NUMB,6,4 )
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Tin_Last_4().compute(new ComputeParameters(false, ldaTwrl443c.getPnd_B_Tape_Pnd_B_Tin_Last_4()), pnd_Tin_Last_4_A.val());     //Natural: ASSIGN #B-TIN-LAST-4 := VAL ( #TIN-LAST-4-A )
            //*  12-03-2004
            //*  BK
            //*  DASDH
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Unique_Acct_Seq().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Form_Seq());                                                             //Natural: ASSIGN #B-UNIQUE-ACCT-SEQ := #F-FORM-SEQ
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_1().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                    //Natural: ASSIGN #B-PAYMENT-AMOUNT-1 := #F-TRAD-IRA-CONTRIB
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_2().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                   //Natural: ASSIGN #B-PAYMENT-AMOUNT-2 := #F-TRAD-IRA-ROLLOVER
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_3().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                 //Natural: ASSIGN #B-PAYMENT-AMOUNT-3 := #F-ROTH-IRA-CONVERSION
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_4().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar());                                                          //Natural: ASSIGN #B-PAYMENT-AMOUNT-4 := #F-IRA-RECHAR
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_5().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv());                                                         //Natural: ASSIGN #B-PAYMENT-AMOUNT-5 := #F-ACCOUNT-FMV
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_A().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                    //Natural: ASSIGN #B-PAYMENT-AMOUNT-A := #F-ROTH-IRA-CONTRIB
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_8().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Sep_Contrib());                                                         //Natural: ASSIGN #B-PAYMENT-AMOUNT-8 := #F-SEP-CONTRIB
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                     //Natural: ASSIGN #B-PAYMENT-AMOUNT-C := #F-TIRF-POSTPN-AMT
        //*  #B-YR-PST-CNT         :=  '2017'                            /* DASDH
        //*  DASDH
        //*  DASDH
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt().greater(getZero())))                                                                                //Natural: IF #F-TIRF-POSTPN-AMT GT 0
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Postpn_Code().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Code());                                                     //Natural: ASSIGN #B-POSTPN-CODE := #F-TIRF-POSTPN-CODE
            //*  DASDH
            //*  PALDE STARTS
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_D().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                 //Natural: ASSIGN #B-PAYMENT-AMOUNT-D := #F-TIRF-REPAYMENTS-AMT
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt().greater(getZero())))                                                                            //Natural: IF #F-TIRF-REPAYMENTS-AMT GT 0
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Rep_Code().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Code());                                                    //Natural: ASSIGN #B-REP-CODE := #F-TIRF-REPAYMENTS-CODE
            //*  PALDE ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  VIKRAM STARTS
        if (condition(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C().greater(getZero())))                                                                             //Natural: IF #B-PAYMENT-AMOUNT-C GT 0
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Yr_Pst_Cnt().setValue(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Year());                                                        //Natural: ASSIGN #B-YR-PST-CNT := #B-PAYMENT-YEAR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Yr_Pst_Cnt().setValue("    ");                                                                                                //Natural: ASSIGN #B-YR-PST-CNT := '    '
            //*  VIKRAM ENDS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Foreign_Addr().equals(" ")))                                                                                          //Natural: IF #F-FOREIGN-ADDR = ' '
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Indicator().setValue(" ");                                                                                            //Natural: ASSIGN #B-FOREIGN-INDICATOR := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Indicator().setValue("1");                                                                                            //Natural: ASSIGN #B-FOREIGN-INDICATOR := '1'
            pnd_X_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #X-CTR
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Name_Line_1().setValue(DbsUtil.compress(ldaTwrl442a.getPnd_Form_Pnd_F_Part_Last_Nme(), ldaTwrl442a.getPnd_Form_Pnd_F_Part_First_Nme(),  //Natural: COMPRESS #F-PART-LAST-NME #F-PART-FIRST-NME #F-PART-MDDLE-NME INTO #B-NAME-LINE-1
            ldaTwrl442a.getPnd_Form_Pnd_F_Part_Mddle_Nme()));
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Name_Line_2().setValue(" ");                                                                                                      //Natural: ASSIGN #B-NAME-LINE-2 := ' '
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Address_Line().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Street_Addr());                                                             //Natural: ASSIGN #B-ADDRESS-LINE := #F-STREET-ADDR
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Address_Line().setValue(" ");                                                                                             //Natural: ASSIGN #B-FOREIGN-ADDRESS-LINE := ' '
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_City().setValue("  ");                                                                                                            //Natural: ASSIGN #B-CITY := #B-STATE := #B-COMBINED-STATE-CODE := '  '
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_State().setValue("  ");
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Combined_State_Code().setValue("  ");
        if (condition(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Indicator().equals("1")))                                                                                   //Natural: IF #B-FOREIGN-INDICATOR = '1'
        {
            pnd_W_Province.setValue(" ");                                                                                                                                 //Natural: ASSIGN #W-PROVINCE := ' '
            if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Province_Code().equals(" ")))                                                                                     //Natural: IF #F-PROVINCE-CODE = ' '
            {
                ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Address_Line().setValue(DbsUtil.compress(ldaTwrl442a.getPnd_Form_Pnd_F_City(), ldaTwrl442a.getPnd_Form_Pnd_F_Zip(),  //Natural: COMPRESS #F-CITY #F-ZIP #F-COUNTRY-NAME INTO #B-FOREIGN-ADDRESS-LINE
                    ldaTwrl442a.getPnd_Form_Pnd_F_Country_Name()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_W_Province_3bytes.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Province_Code());                                                                            //Natural: ASSIGN #W-PROVINCE-3BYTES := #F-PROVINCE-CODE
                FOR01:                                                                                                                                                    //Natural: FOR T = 1 TO #T-STATE-CNT
                for (t.setValue(1); condition(t.lessOrEqual(pnd_T_State_Cnt)); t.nadd(1))
                {
                    if (condition(pnd_W_Province_3bytes_Pnd_W_Province_Code.equals(pnd_T_Num_Code.getValue(t))))                                                          //Natural: IF #W-PROVINCE-CODE = #T-NUM-CODE ( T )
                    {
                        pnd_W_Province.setValue(pnd_T_Province.getValue(t));                                                                                              //Natural: ASSIGN #W-PROVINCE := #T-PROVINCE ( T )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                DbsUtil.examine(new ExamineSource(pnd_W_Province), new ExamineTranslate(TranslateOption.Upper));                                                          //Natural: EXAMINE #W-PROVINCE AND TRANSLATE INTO UPPER CASE
                ldaTwrl443c.getPnd_B_Tape_Pnd_B_Foreign_Address_Line().setValue(DbsUtil.compress(ldaTwrl442a.getPnd_Form_Pnd_F_City(), pnd_W_Province,                    //Natural: COMPRESS #F-CITY #W-PROVINCE #F-ZIP #F-COUNTRY-NAME INTO #B-FOREIGN-ADDRESS-LINE
                    ldaTwrl442a.getPnd_Form_Pnd_F_Zip(), ldaTwrl442a.getPnd_Form_Pnd_F_Country_Name()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_City().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_City());                                                                        //Natural: ASSIGN #B-CITY := #F-CITY
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_State().setValue("  ");                                                                                                       //Natural: ASSIGN #B-STATE := #B-COMBINED-STATE-CODE := '  '
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Combined_State_Code().setValue("  ");
            if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Geo_Cde().equals("00")))                                                                                          //Natural: IF #F-GEO-CDE = '00'
            {
                ldaTwrl443c.getPnd_B_Tape_Pnd_B_State().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Apo_Geo_Code());                                                           //Natural: ASSIGN #B-STATE := #F-APO-GEO-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR02:                                                                                                                                                    //Natural: FOR U = 1 TO #T-STATE-CNT
                for (u.setValue(1); condition(u.lessOrEqual(pnd_T_State_Cnt)); u.nadd(1))
                {
                    if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Geo_Cde().equals(pnd_T_Num_Code.getValue(u))))                                                            //Natural: IF #F-GEO-CDE = #T-NUM-CODE ( U )
                    {
                        ldaTwrl443c.getPnd_B_Tape_Pnd_B_State().setValue(pnd_T_Alpha_Code.getValue(u));                                                                   //Natural: ASSIGN #B-STATE := #T-ALPHA-CODE ( U )
                        if (condition(pnd_T_Combined.getValue(u).equals("C")))                                                                                            //Natural: IF #T-COMBINED ( U ) = 'C'
                        {
                            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Combined_State_Code().setValue(pnd_T_Irs_Code.getValue(u));                                                   //Natural: ASSIGN #B-COMBINED-STATE-CODE := #T-IRS-CODE ( U )
                                                                                                                                                                          //Natural: PERFORM UPDATE-K-RECORD
                            sub_Update_K_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Zip().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Zip());                                                                          //Natural: ASSIGN #B-ZIP := #F-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE THE FOLLOWING 3 IF STATEMENTS TO INCLUDE #F-IRA-RECHAR NE 0
        //*                                                        10/02/07    RM
        if (condition((((ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover().notEquals(getZero()) || ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar().notEquals(getZero()))       //Natural: IF ( #F-TRAD-IRA-ROLLOVER NE 0 OR #F-IRA-RECHAR NE 0 OR #F-ACCOUNT-FMV NE 0 ) AND ( #F-IRA-ACCT-TYPE = '01' OR = '03' )
            || ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv().notEquals(getZero())) && (ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Acct_Type().equals("01") || ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Acct_Type().equals("03")))))
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Ira_Ind().setValue("1");                                                                                                      //Natural: ASSIGN #B-IRA-IND := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Ira_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN #B-IRA-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*   SEP
        if (condition((ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover().notEquals(getZero()) || ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar().notEquals(getZero())          //Natural: IF ( #F-TRAD-IRA-ROLLOVER NE 0 OR #F-IRA-RECHAR NE 0 OR #F-ACCOUNT-FMV NE 0 ) AND #F-IRA-ACCT-TYPE = '05'
            || ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv().notEquals(getZero())) && ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Acct_Type().equals("05")))
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Sep_Ind().setValue("1");                                                                                                      //Natural: ASSIGN #B-SEP-IND := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Sep_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN #B-SEP-IND := ' '
            //*  BK
        }                                                                                                                                                                 //Natural: END-IF
        //*   ROTH IRA
        if (condition((ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover().notEquals(getZero()) || ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar().notEquals(getZero())          //Natural: IF ( #F-TRAD-IRA-ROLLOVER NE 0 OR #F-IRA-RECHAR NE 0 OR #F-ACCOUNT-FMV NE 0 ) AND #F-IRA-ACCT-TYPE = '02'
            || ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv().notEquals(getZero())) && ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Acct_Type().equals("02")))
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Roth_Ind().setValue("1");                                                                                                     //Natural: ASSIGN #B-ROTH-IND := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Roth_Ind().setValue(" ");                                                                                                     //Natural: ASSIGN #B-ROTH-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
        //*  THE FOLLOWING SET OF IF STATEMENT IS OBSOLETE           07/07/06  RM
        //*    IF #B-IRA-IND           =  ' '
        //*        AND #B-ROTH-IND     =  ' '
        //*        AND #B-SEP-IND      =  ' '                /* BK
        //*        AND #B-PAYMENT-AMOUNT-4  =  0.00     /*  #F-IRA-RECHAR
        //*      IF #F-IRA-ACCT-TYPE  =  '01'
        //*        #B-IRA-IND  :=  '1'
        //*      ELSE
        //*        IF #F-IRA-ACCT-TYPE  =  '02'
        //*          #B-ROTH-IND  :=  '1'
        //*        ELSE                                          /* BK
        //*          IF #F-IRA-ACCT-TYPE  =  '05'
        //*            #B-SEP-IND  :=  '1'
        //*          END-IF                                      /* BK
        //*        END-IF
        //*      END-IF
        //*      DISPLAY 'Tax ID'         #F-TIN (AL=9)
        //*    END-IF
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Rmd_Ind().reset();                                                                                                                //Natural: RESET #B-RMD-IND
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Md_Ind().notEquals(" ")))                                                                                             //Natural: IF #F-MD-IND NOT = ' '
        {
            ldaTwrl443c.getPnd_B_Tape_Pnd_B_Rmd_Ind().setValue(1);                                                                                                        //Natural: ASSIGN #B-RMD-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sequence_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQUENCE-NUMBER
        ldaTwrl443c.getPnd_B_Tape_Pnd_B_Sequence_Number().setValue(pnd_Sequence_Number);                                                                                  //Natural: ASSIGN #B-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
        getWorkFiles().write(5, false, ldaTwrl443c.getPnd_B_Tape());                                                                                                      //Natural: WRITE WORK FILE 05 #B-TAPE
        pnd_B_Ctr.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #B-CTR
    }
    private void sub_Update_C_Record() throws Exception                                                                                                                   //Natural: UPDATE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Number_Of_Payees().nadd(1);                                                                                                       //Natural: ADD 1 TO #C-NUMBER-OF-PAYEES
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_1().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_1());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-1 TO #C-TOTAL-1
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_2().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_2());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-2 TO #C-TOTAL-2
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_3().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_3());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-3 TO #C-TOTAL-3
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_4().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_4());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-4 TO #C-TOTAL-4
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_5().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_5());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-5 TO #C-TOTAL-5
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_A().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_A());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-A TO #C-TOTAL-A
        //*  BK
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_8().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_8());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-8 TO #C-TOTAL-8
        //*  DASDH
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_C().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-C TO #C-TOTAL-C
        //*  PALDE
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_D().nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_D());                                                               //Natural: ADD #B-PAYMENT-AMOUNT-D TO #C-TOTAL-D
    }
    private void sub_Update_K_Record() throws Exception                                                                                                                   //Natural: UPDATE-K-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Combined_Found.setValue(false);                                                                                                                               //Natural: ASSIGN #COMBINED-FOUND := FALSE
        FOR03:                                                                                                                                                            //Natural: FOR I = 1 TO K
        for (i.setValue(1); condition(i.lessOrEqual(k)); i.nadd(1))
        {
            if (condition(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Combined_State_Code().equals(pnd_K_Irs_Code.getValue(i))))                                                      //Natural: IF #B-COMBINED-STATE-CODE = #K-IRS-CODE ( I )
            {
                pnd_Combined_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #COMBINED-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_K_Payees.getValue(i).nadd(1);                                                                                                                             //Natural: ADD 1 TO #K-PAYEES ( I )
            pnd_K_Tot_1.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_1());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-1 TO #K-TOT-1 ( I )
            pnd_K_Tot_2.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_2());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-2 TO #K-TOT-2 ( I )
            pnd_K_Tot_3.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_3());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-3 TO #K-TOT-3 ( I )
            pnd_K_Tot_4.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_4());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-4 TO #K-TOT-4 ( I )
            pnd_K_Tot_5.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_5());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-5 TO #K-TOT-5 ( I )
            pnd_K_Tot_A.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_A());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-A TO #K-TOT-A ( I )
            //*  BK
            pnd_K_Tot_8.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_8());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-8 TO #K-TOT-8 ( I )
            //*  DASDH
            pnd_K_Tot_C.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-C TO #K-TOT-C ( I )
            //*  PALDE
            pnd_K_Tot_D.getValue(i).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_D());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-D TO #K-TOT-D ( I )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Combined_Found.equals(false)))                                                                                                                  //Natural: IF #COMBINED-FOUND = FALSE
        {
            k.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO K
            pnd_K_Irs_Code.getValue(k).setValue(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Combined_State_Code());                                                                   //Natural: ASSIGN #K-IRS-CODE ( K ) := #B-COMBINED-STATE-CODE
            pnd_K_Payees.getValue(k).nadd(1);                                                                                                                             //Natural: ADD 1 TO #K-PAYEES ( K )
            pnd_K_Tot_1.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_1());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-1 TO #K-TOT-1 ( K )
            pnd_K_Tot_2.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_2());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-2 TO #K-TOT-2 ( K )
            pnd_K_Tot_3.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_3());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-3 TO #K-TOT-3 ( K )
            pnd_K_Tot_4.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_4());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-4 TO #K-TOT-4 ( K )
            pnd_K_Tot_5.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_5());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-5 TO #K-TOT-5 ( K )
            pnd_K_Tot_A.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_A());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-A TO #K-TOT-A ( K )
            //*  BK
            pnd_K_Tot_8.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_8());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-8 TO #K-TOT-8 ( K )
            //*  DASDH
            pnd_K_Tot_C.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_C());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-C TO #K-TOT-C ( K )
            //*  PALDE
            pnd_K_Tot_D.getValue(k).nadd(ldaTwrl443c.getPnd_B_Tape_Pnd_B_Payment_Amount_D());                                                                             //Natural: ADD #B-PAYMENT-AMOUNT-D TO #K-TOT-D ( K )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_A_Record() throws Exception                                                                                                                   //Natural: CREATE-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //*  '133586143'
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  PALDE
        pnd_Sequence_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQUENCE-NUMBER
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payment_Year().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year());                                                                //Natural: ASSIGN #A-PAYMENT-YEAR := #F-TAX-YEAR
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Ein().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                                   //Natural: ASSIGN #A-PAYER-EIN := #TWRACOM2.#FED-ID
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                          //Natural: ASSIGN #A-PAYER-NAME-1 := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Name_2().setValue(" ");                                                                                                     //Natural: ASSIGN #A-PAYER-NAME-2 := ' '
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Shipping_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #A-PAYER-SHIPPING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                                    //Natural: ASSIGN #A-PAYER-CITY := #TWRACOM2.#CITY
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                                  //Natural: ASSIGN #A-PAYER-STATE := #TWRACOM2.#STATE
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                                    //Natural: ASSIGN #A-PAYER-ZIP := #TWRACOM2.#ZIP-9
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Payer_Phone_No().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                               //Natural: ASSIGN #A-PAYER-PHONE-NO := #TWRACOM2.#PHONE
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Amount_Indicators().setValue("123458ACD");                                                                                        //Natural: ASSIGN #A-AMOUNT-INDICATORS := '123458ACD'
        ldaTwrl443b.getPnd_A_Tape_Pnd_A_Sequence_Number().setValue(pnd_Sequence_Number);                                                                                  //Natural: ASSIGN #A-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
        getWorkFiles().write(5, false, ldaTwrl443b.getPnd_A_Tape());                                                                                                      //Natural: WRITE WORK FILE 05 #A-TAPE
        pnd_Number_Of_A_Recs.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NUMBER-OF-A-RECS
        pnd_A_Ctr.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #A-CTR
    }
    private void sub_Create_C_Record() throws Exception                                                                                                                   //Natural: CREATE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pnd_Sequence_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQUENCE-NUMBER
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Sequence_Number().setValue(pnd_Sequence_Number);                                                                                  //Natural: ASSIGN #C-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
        getWorkFiles().write(5, false, ldaTwrl443d.getPnd_C_Tape());                                                                                                      //Natural: WRITE WORK FILE 05 #C-TAPE
        pnd_C_Ctr.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #C-CTR
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Number_Of_Payees().reset();                                                                                                       //Natural: RESET #C-NUMBER-OF-PAYEES #C-TOTAL-1 #C-TOTAL-2 #C-TOTAL-3 #C-TOTAL-4 #C-TOTAL-5 #C-TOTAL-A
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_1().reset();
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_2().reset();
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_3().reset();
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_4().reset();
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_5().reset();
        ldaTwrl443d.getPnd_C_Tape_Pnd_C_Total_A().reset();
    }
    private void sub_Create_K_Records() throws Exception                                                                                                                  //Natural: CREATE-K-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR I = 1 TO K
        for (i.setValue(1); condition(i.lessOrEqual(k)); i.nadd(1))
        {
            //*  BK
            //*  BK
            pnd_Sequence_Number.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #SEQUENCE-NUMBER
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Number_Of_Payees().setValue(pnd_K_Payees.getValue(i));                                                                        //Natural: ASSIGN #K-NUMBER-OF-PAYEES := #K-PAYEES ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_1().setValue(pnd_K_Tot_1.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-1 := #K-TOT-1 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_2().setValue(pnd_K_Tot_2.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-2 := #K-TOT-2 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_3().setValue(pnd_K_Tot_3.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-3 := #K-TOT-3 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_4().setValue(pnd_K_Tot_4.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-4 := #K-TOT-4 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_5().setValue(pnd_K_Tot_5.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-5 := #K-TOT-5 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_A().setValue(pnd_K_Tot_A.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-A := #K-TOT-A ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_8().setValue(pnd_K_Tot_8.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-8 := #K-TOT-8 ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Total_C().setValue(pnd_K_Tot_C.getValue(i));                                                                                  //Natural: ASSIGN #K-TOTAL-C := #K-TOT-C ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_State_Code().setValue(pnd_K_Irs_Code.getValue(i));                                                                            //Natural: ASSIGN #K-STATE-CODE := #K-IRS-CODE ( I )
            ldaTwrl443e.getPnd_K_Tape_Pnd_K_Sequence_Number().setValue(pnd_Sequence_Number);                                                                              //Natural: ASSIGN #K-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
            getWorkFiles().write(5, false, ldaTwrl443e.getPnd_K_Tape());                                                                                                  //Natural: WRITE WORK FILE 05 #K-TAPE
            pnd_K_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K-CTR
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Create_T_Record() throws Exception                                                                                                                   //Natural: CREATE-T-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //*  '131624203'
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  12-03-2004
        //*  AAY
        pnd_Sequence_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQUENCE-NUMBER
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Payment_Year().setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year());                                                                //Natural: ASSIGN #T-PAYMENT-YEAR := #F-TAX-YEAR
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Transmitters_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                            //Natural: ASSIGN #T-TRANSMITTERS-TIN := #TWRACOM2.#FED-ID
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Transmitter_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                                  //Natural: ASSIGN #T-TRANSMITTER-CONTROL-CODE := #TWRACOM2.#COMP-TCC
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Transmitter_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                      //Natural: ASSIGN #T-TRANSMITTER-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Transmitter_Name_Continued().setValue(" ");                                                                                       //Natural: ASSIGN #T-TRANSMITTER-NAME-CONTINUED := ' '
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                          //Natural: ASSIGN #T-COMPANY-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_Name_Continued().setValue(" ");                                                                                           //Natural: ASSIGN #T-COMPANY-NAME-CONTINUED := ' '
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_Mailing_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                        //Natural: ASSIGN #T-COMPANY-MAILING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                                  //Natural: ASSIGN #T-COMPANY-CITY := #TWRACOM2.#CITY
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                                //Natural: ASSIGN #T-COMPANY-STATE := #TWRACOM2.#STATE
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Company_Zip_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                             //Natural: ASSIGN #T-COMPANY-ZIP-CODE := #TWRACOM2.#ZIP-9
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                                          //Natural: ASSIGN #T-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Contact_Phone_N_Extension().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                    //Natural: ASSIGN #T-CONTACT-PHONE-N-EXTENSION := #TWRACOM2.#PHONE
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Contact_E_Mail_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                                          //Natural: ASSIGN #T-CONTACT-E-MAIL-ADDRESS := #TWRACOM2.#CONTACT-EMAIL-ADDR
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Total_Number_Of_Payees().setValue(pnd_Total_B_Recs_Record);                                                                       //Natural: ASSIGN #T-TOTAL-NUMBER-OF-PAYEES := #TOTAL-B-RECS-RECORD
        ldaTwrl443a.getPnd_T_Tape_Pnd_T_Sequence_Number().setValue(pnd_Sequence_Number);                                                                                  //Natural: ASSIGN #T-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
        getWorkFiles().write(5, false, ldaTwrl443a.getPnd_T_Tape());                                                                                                      //Natural: WRITE WORK FILE 05 #T-TAPE
        pnd_T_Ctr.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #T-CTR
    }
    private void sub_Create_F_Record() throws Exception                                                                                                                   //Natural: CREATE-F-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pnd_Sequence_Number.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #SEQUENCE-NUMBER
        ldaTwrl443f.getPnd_F_Tape_Pnd_F_Number_Of_A_Records().setValue(pnd_Number_Of_A_Recs);                                                                             //Natural: ASSIGN #F-NUMBER-OF-A-RECORDS := #NUMBER-OF-A-RECS
        ldaTwrl443f.getPnd_F_Tape_Pnd_F_Total_B_Records().setValue(pnd_Total_B_Recs_Record);                                                                              //Natural: ASSIGN #F-TOTAL-B-RECORDS := #TOTAL-B-RECS-RECORD
        ldaTwrl443f.getPnd_F_Tape_Pnd_F_Sequence_Number().setValue(pnd_Sequence_Number);                                                                                  //Natural: ASSIGN #F-SEQUENCE-NUMBER := #SEQUENCE-NUMBER
        getWorkFiles().write(5, false, ldaTwrl443f.getPnd_F_Tape());                                                                                                      //Natural: WRITE WORK FILE 05 #F-TAPE
        pnd_F_Ctr.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F-CTR
    }
    private void sub_Read_Total_B_Records() throws Exception                                                                                                              //Natural: READ-TOTAL-B-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        getWorkFiles().read(2, pnd_Total_B_Recs_Record);                                                                                                                  //Natural: READ WORK FILE 02 ONCE #TOTAL-B-RECS-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            //* *
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form (5498) Extract File (Work File 02) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new                   //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) Extract File (Work File 02) Is Empty' 77T '***' / '***' 06T 'File Contains 1 Total Number Of All "B" Records ' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"File Contains 1 Total Number Of All 'B' Records ",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Nbr_Year_Pnd_Super_Nbr.setValue(2);                                                                                                                     //Natural: ASSIGN #SUPER-NBR := 2
        pnd_Super_Nbr_Year_Pnd_Super_Year.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year());                                                                             //Natural: ASSIGN #SUPER-YEAR := #F-TAX-YEAR
        vw_state.startDatabaseRead                                                                                                                                        //Natural: READ STATE WITH TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #SUPER-NBR-YEAR
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Super_Nbr_Year, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_state.readNextRow("READ01")))
        {
            if (condition(state_Tircntl_Tbl_Nbr.equals(2) && state_Tircntl_Tax_Year_A.equals(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year())))                                  //Natural: IF TIRCNTL-TBL-NBR = 2 AND TIRCNTL-TAX-YEAR-A = #F-TAX-YEAR
            {
                pnd_T_State_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #T-STATE-CNT
                pnd_T_Num_Code.getValue(pnd_T_State_Cnt).setValue(state_Pnd_State_Num_Code);                                                                              //Natural: ASSIGN #T-NUM-CODE ( #T-STATE-CNT ) := #STATE-NUM-CODE
                pnd_T_Alpha_Code.getValue(pnd_T_State_Cnt).setValue(state_Pnd_State_Alpha_Code);                                                                          //Natural: ASSIGN #T-ALPHA-CODE ( #T-STATE-CNT ) := #STATE-ALPHA-CODE
                pnd_T_Province.getValue(pnd_T_State_Cnt).setValue(state_Tircntl_State_Full_Name);                                                                         //Natural: ASSIGN #T-PROVINCE ( #T-STATE-CNT ) := TIRCNTL-STATE-FULL-NAME
                pnd_T_Combined.getValue(pnd_T_State_Cnt).setValue(state_Tircntl_Comb_Fed_Ind);                                                                            //Natural: ASSIGN #T-COMBINED ( #T-STATE-CNT ) := TIRCNTL-COMB-FED-IND
                pnd_T_Irs_Code.getValue(pnd_T_State_Cnt).setValue(state_Pnd_State_Irs_Code);                                                                              //Natural: ASSIGN #T-IRS-CODE ( #T-STATE-CNT ) := #STATE-IRS-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_T_State_Cnt.equals(getZero())))                                                                                                                 //Natural: IF #T-STATE-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"State Code Table '2' For Tax Year:",ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year(),"Is Missing",new                  //Natural: WRITE ( 00 ) '***' 06T 'State Code Table "2" For Tax Year:' #F-TAX-YEAR 'Is Missing' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Extract_Company_Data() throws Exception                                                                                                              //Natural: EXTRACT-COMPANY-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOM2.#ABEND-IND := FALSE
        pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(false);                                                                                                    //Natural: ASSIGN #TWRACOM2.#DISPLAY-IND := FALSE
        //* *WRACOM2.#COMP-CODE    :=  'S'                            /* 12-03-2004
        //*  EINCHG
        //*  EINCHG
        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year().val().greaterOrEqual(2018)))                                                                               //Natural: IF VAL ( #F-TAX-YEAR ) GE 2018
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("A");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'A'
            //*  EINCHG
            //*  12-03-2004
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(4);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 4
        //*  12-03-2004
        if (condition(DbsUtil.maskMatches(ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year(),"NNNN")))                                                                              //Natural: IF #F-TAX-YEAR = MASK ( NNNN )
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year()), ldaTwrl442a.getPnd_Form_Pnd_F_Tax_Year().val()); //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := VAL ( #F-TAX-YEAR )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(9999);                                                                                                    //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := 9999
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  03-06-07
        if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #TWRACOM2.#RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"IRS 5498 Original Abend In Company Info.'TWRNCOM2'",new TabSetting(77),"***",NEWLINE,"***",new                 //Natural: WRITE ( 00 ) '***' 06T 'IRS 5498 Original Abend In Company Info."TWRNCOM2"' 77T '***' / '***' 06T 'Return Message...:' #TWRACOM2.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(6),"Return Message...:",pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, NEWLINE,new TabSetting(1),"No. Of T Records...........................",pnd_T_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new            //Natural: WRITE ( 00 ) / 01T 'No. Of T Records...........................' #T-CTR / 01T 'No. Of A Records...........................' #A-CTR / 01T 'No. Of B Records...........................' #B-CTR / 01T 'No. Of C Records...........................' #C-CTR / 01T 'NO. OF K Records...........................' #K-CTR / 01T 'No. Of F Records...........................' #F-CTR / 01T 'No. Of No TIN Records......................' #N-CTR / 01T 'No. Of Foreign Records.....................' #X-CTR
            TabSetting(1),"No. Of A Records...........................",pnd_A_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of B Records...........................",pnd_B_Ctr, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of C Records...........................",pnd_C_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),"NO. OF K Records...........................",pnd_K_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of F Records...........................",pnd_F_Ctr, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of No TIN Records......................",pnd_N_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),"No. Of Foreign Records.....................",pnd_X_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  01T 'No. Of No Fair Market Value Records........'  #BAD-COUNT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"No. Of T Records...........................",pnd_T_Ctr, new ReportEditMask                  //Natural: WRITE ( 01 ) / 01T 'No. Of T Records...........................' #T-CTR / 01T 'No. Of A Records...........................' #A-CTR / 01T 'No. Of B Records...........................' #B-CTR / 01T 'No. Of C Records...........................' #C-CTR / 01T 'NO. OF K Records...........................' #K-CTR / 01T 'No. Of F Records...........................' #F-CTR / 01T 'No. Of No TIN Records......................' #N-CTR
            ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of A Records...........................",pnd_A_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),"No. Of B Records...........................",pnd_B_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of C Records...........................",pnd_C_Ctr, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"NO. OF K Records...........................",pnd_K_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),"No. Of F Records...........................",pnd_F_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"No. Of No TIN Records......................",pnd_N_Ctr, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  01T 'No. Of Foreign Records.....................'  #X-CTR
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRA Contributions IRS Control Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRA Contributions IRS Control Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
