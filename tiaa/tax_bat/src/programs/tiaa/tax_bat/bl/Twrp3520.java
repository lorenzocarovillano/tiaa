/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:29 PM
**        * FROM NATURAL PROGRAM : Twrp3520
************************************************************
**        * FILE NAME            : Twrp3520.java
**        * CLASS NAME           : Twrp3520
**        * INSTANCE NAME        : Twrp3520
************************************************************
************************************************************************
** PROGRAM : TWRP3520
** SYSTEM  : TAXWARS
** AUTHOR  : TED PROIMOS
** FUNCTION: DRIVER PROGRAM FOR STATE CORRECTION REPORTING
** HISTORY.....:
**    05/24/2004 - M. SUPONITSKY  - ADDED NEW PROGRAM STARTING 2003
**    10/22/2002 - MARINA NACHBER - ADDED NEW PROGRAM STARTING 2002
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3520 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_State;
    private DbsField pnd_Ws_Pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_State = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3520() throws Exception
    {
        super("Twrp3520");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3520|Main");
        while(true)
        {
            try
            {
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                short decideConditionsMet25 = 0;                                                                                                                          //Natural: DECIDE ON FIRST VALUE #TAX-YEAR;//Natural: VALUE 1999
                if (condition((pnd_Ws_Pnd_Tax_Year.equals(1999))))
                {
                    decideConditionsMet25++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3523' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3523"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2000
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2000))))
                {
                    decideConditionsMet25++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3525' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3525"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2001
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2001))))
                {
                    decideConditionsMet25++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3539' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3539"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2002
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2002))))
                {
                    decideConditionsMet25++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3543' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3543"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2003, 2004
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2003) || pnd_Ws_Pnd_Tax_Year.equals(2004))))
                {
                    decideConditionsMet25++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3546' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3546"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3552' #STATE #TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3552"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-DECIDE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
