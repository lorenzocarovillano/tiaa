/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:09 PM
**        * FROM NATURAL PROGRAM : Twrp3615
************************************************************
**        * FILE NAME            : Twrp3615.java
**        * CLASS NAME           : Twrp3615
**        * INSTANCE NAME        : Twrp3615
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP3615
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : REFINES EXTRACT FOR MAGNETIC MEDIA STATE REPORTING.
* CREATED  : 11 / 26 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS EXTRACT OF MAGNETIC MEDIA STATES FROM
*            THE TAX FORMS DATA BASE FILE.  SELECTS RECORDS TO BE
*            REPORTED, AND PRODUCES FIVE REPORTS.
*  HISTORY:
*  ------------------------------------
*  12/18/09 AY - REVISED DUE TO 2009 IRS CHANGES (TWRL3502).
*  12/12/07 RM - RECOMPILED FOR 2007 IRS LAYOUT CHANGES (TWRL3502).
*  01/22/05 MS - TOPS TIAA & TRUST PROCESSING
*  04/04/17  DY  RESTOW MODULE - PIN EXPANSION PROJECT
*  10/05/18 ARIVU - EIN CHANGE - EINCHG
*  10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3615 extends BLNatBase
{
    // Data Areas
    private PdaTwracom2 pdaTwracom2;
    private LdaTwrl9705 ldaTwrl9705;
    private LdaTwrl3502 ldaTwrl3502;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;
    private PdaTwradist pdaTwradist;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File1;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Work_File1_Pnd_Tirf_Sys_Err;
    private DbsField pnd_Work_File1_Pnd_Tirf_Empty_Form;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Distr;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Tax_Wthld;
    private DbsField pnd_Work_File1_Pnd_Tirf_Loc_Code;
    private DbsField pnd_Work_File1_Pnd_Tirf_Loc_Distr;
    private DbsField pnd_Work_File1_Pnd_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_Work_File1_Pnd_Tirf_Isn;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Rpt_Ind;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Code;
    private DbsField pnd_Work_File1_Pnd_Tirf_Tax_Year;
    private DbsField pnd_Work_File1_Pnd_Tirf_Name;
    private DbsField pnd_Work_File1_Pnd_Tirf_Tin;
    private DbsField pnd_Work_File1_Pnd_Tirf_Account;
    private DbsField pnd_Work_File1_Pnd_Tirf_Addr_Ln1;
    private DbsField pnd_Work_File1_Pnd_Tirf_Addr_Ln2;
    private DbsField pnd_Work_File1_Pnd_Tirf_Addr_Ln3;
    private DbsField pnd_Work_File1_Pnd_Tirf_Tin_Type;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Hardcopy_Ind;
    private DbsField pnd_Work_File1_Pnd_Tirf_Res_Reject_Ind;
    private DbsField pnd_Work_File1_Pnd_Tirf_State_Reporting;
    private DbsField pnd_Work_File2;

    private DbsGroup pnd_Work_File2__R_Field_2;
    private DbsField pnd_Work_File2_Pnd_Tirf_Box1;
    private DbsField pnd_Work_File2_Pnd_Tirf_Box2;
    private DbsField pnd_Work_File2_Pnd_Tirf_Box4;
    private DbsField pnd_Work_File2_Pnd_Tirf_Box5;
    private DbsField pnd_Work_File2_Pnd_Tirf_Box9;
    private DbsField pnd_Work_File2_Pnd_Tirf_Boxa;
    private DbsField pnd_Work_File2_Pnd_Tirf_Boxb;

    private DataAccessProgramView vw_tircntl;
    private DbsField tircntl_Tircntl_Tbl_Nbr;
    private DbsField tircntl_Tircntl_Tax_Year;
    private DbsField tircntl_Tircntl_Seq_Nbr;

    private DbsGroup tircntl_Tircntl_Reporting_Tbl;
    private DbsField tircntl_Tircntl_Rpt_Form_Name;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Today;
    private DbsField pnd_Ws_Pnd_Et;
    private DbsField pnd_Ws_Pnd_Rpt_Name;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Rpt_Name_1;
    private DbsField pnd_Ws_Pnd_Rpt_State_Code;
    private DbsField pnd_Ws_Pnd_Old_Company_Cde;
    private DbsField pnd_Ws_Pnd_Old_Accept_Reject;
    private DbsField pnd_Ws_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Tot_State_Distr;
    private DbsField pnd_Ws_Pnd_Tot_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Tot_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Tot_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Box1;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Box2;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Box4;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Box5;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Box9;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Boxa;
    private DbsField pnd_Ws_Pnd_Tot_Tirf_Boxb;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Box1;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Box2;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Box4;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Box5;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Box9;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa;
    private DbsField pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Box1;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Box2;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Box4;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Box5;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Box9;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa;
    private DbsField pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb;
    private DbsField pnd_Ws_Pnd_Rco_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rco_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rco_State_Distr;
    private DbsField pnd_Ws_Pnd_Rco_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rco_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rco_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rej_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rej_State_Distr;
    private DbsField pnd_Ws_Pnd_Rej_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rej_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rej_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Det_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Det_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Det_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Det_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Det_State_Distr;
    private DbsField pnd_Ws_Pnd_Det_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Det_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Det_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Fed_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rec_Res_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Res_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rec_State_Distr;
    private DbsField pnd_Ws_Pnd_Rec_State_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Tax_Wthld;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Distr;
    private DbsField pnd_Ws_Pnd_Rec_Irr_Amt;
    private DbsField pnd_Ws_Pnd_Rec_Gross_Amt_T;
    private DbsField pnd_Ws_Pnd_Rec_Taxable_Amt_T;
    private DbsField pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_T;
    private DbsField pnd_Ws_Pnd_Rec_Res_Gross_Amt_T;
    private DbsField pnd_Ws_Pnd_Rec_Res_Taxable_Amt_T;
    private DbsField pnd_Ws_Pnd_Rec_State_Distr_T;
    private DbsField pnd_Ws_Pnd_Rec_State_Tax_Wthld_T;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_T;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Distr_T;
    private DbsField pnd_Ws_Pnd_Rec_Irr_Amt_T;
    private DbsField pnd_Ws_Pnd_Rec_Counter_T;
    private DbsField pnd_Ws_Pnd_Rec_Gross_Amt_X;
    private DbsField pnd_Ws_Pnd_Rec_Taxable_Amt_X;
    private DbsField pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_X;
    private DbsField pnd_Ws_Pnd_Rec_Res_Gross_Amt_X;
    private DbsField pnd_Ws_Pnd_Rec_Res_Taxable_Amt_X;
    private DbsField pnd_Ws_Pnd_Rec_State_Distr_X;
    private DbsField pnd_Ws_Pnd_Rec_State_Tax_Wthld_X;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_X;
    private DbsField pnd_Ws_Pnd_Rec_Loc_Distr_X;
    private DbsField pnd_Ws_Pnd_Rec_Irr_Amt_X;
    private DbsField pnd_Ws_Pnd_Rec_Counter_X;
    private DbsField pnd_Ws_Pnd_Empty_Form_Count;
    private DbsField pnd_Ws_Pnd_Reject_Form_Count;
    private DbsField pnd_Ws_Pnd_Accept_Form_Count;
    private DbsField pnd_Ws_Pnd_Recon_Only;
    private DbsField pnd_Ws_Pnd_Zero;
    private DbsField pnd_Ws_Pnd_Company_Name;
    private DbsField pnd_Ws_Pnd_State_Indicators;
    private DbsField pnd_Ws_Pnd_Name;
    private DbsField pnd_Ws_Pnd_Address;
    private DbsField pnd_Ws_Pnd_Form_Desc;
    private DbsField pnd_Ws_Pnd_Form_Desc2;
    private DbsField pnd_Ws_Pnd_State_Numeric_Code;
    private DbsField pnd_Ws_Pnd_Header_Tax_Year;
    private DbsField pnd_Ws_Pnd_Combined_Fed_State;
    private DbsField pnd_Datn;
    private DbsField pnd_Datx;
    private DbsField pnd_Timx;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());
        pdaTwradist = new PdaTwradist(localVariables);

        // Local Variables
        pnd_Work_File1 = localVariables.newFieldInRecord("pnd_Work_File1", "#WORK-FILE1", FieldType.STRING, 246);

        pnd_Work_File1__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1);
        pnd_Work_File1_Pnd_Tirf_Company_Cde = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Tirf_Sys_Err = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Sys_Err", "#TIRF-SYS-ERR", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Tirf_Empty_Form = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Empty_Form", "#TIRF-EMPTY-FORM", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Tirf_State_Distr = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Distr", "#TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_File1_Pnd_Tirf_State_Tax_Wthld = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Tax_Wthld", "#TIRF-STATE-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Pnd_Tirf_Loc_Code = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Loc_Code", "#TIRF-LOC-CODE", FieldType.STRING, 
            5);
        pnd_Work_File1_Pnd_Tirf_Loc_Distr = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Loc_Distr", "#TIRF-LOC-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_File1_Pnd_Tirf_Loc_Tax_Wthld = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Loc_Tax_Wthld", "#TIRF-LOC-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Pnd_Tirf_Isn = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Isn", "#TIRF-ISN", FieldType.NUMERIC, 8);
        pnd_Work_File1_Pnd_Tirf_State_Rpt_Ind = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Rpt_Ind", "#TIRF-STATE-RPT-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Tirf_State_Code = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Code", "#TIRF-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Work_File1_Pnd_Tirf_Tax_Year = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Work_File1_Pnd_Tirf_Name = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Name", "#TIRF-NAME", FieldType.STRING, 40);
        pnd_Work_File1_Pnd_Tirf_Tin = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 9);
        pnd_Work_File1_Pnd_Tirf_Account = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Account", "#TIRF-ACCOUNT", FieldType.STRING, 
            10);
        pnd_Work_File1_Pnd_Tirf_Addr_Ln1 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Addr_Ln1", "#TIRF-ADDR-LN1", FieldType.STRING, 
            40);
        pnd_Work_File1_Pnd_Tirf_Addr_Ln2 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Addr_Ln2", "#TIRF-ADDR-LN2", FieldType.STRING, 
            40);
        pnd_Work_File1_Pnd_Tirf_Addr_Ln3 = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Addr_Ln3", "#TIRF-ADDR-LN3", FieldType.STRING, 
            40);
        pnd_Work_File1_Pnd_Tirf_Tin_Type = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Tin_Type", "#TIRF-TIN-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Pnd_Tirf_State_Hardcopy_Ind = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Hardcopy_Ind", "#TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Tirf_Res_Reject_Ind = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_Res_Reject_Ind", "#TIRF-RES-REJECT-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Pnd_Tirf_State_Reporting = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Pnd_Tirf_State_Reporting", "#TIRF-STATE-REPORTING", 
            FieldType.STRING, 1);
        pnd_Work_File2 = localVariables.newFieldInRecord("pnd_Work_File2", "#WORK-FILE2", FieldType.STRING, 84);

        pnd_Work_File2__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_File2__R_Field_2", "REDEFINE", pnd_Work_File2);
        pnd_Work_File2_Pnd_Tirf_Box1 = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Box1", "#TIRF-BOX1", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Box2 = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Box2", "#TIRF-BOX2", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Box4 = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Box4", "#TIRF-BOX4", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Box5 = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Box5", "#TIRF-BOX5", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Box9 = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Box9", "#TIRF-BOX9", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Boxa = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Boxa", "#TIRF-BOXA", FieldType.NUMERIC, 12, 
            2);
        pnd_Work_File2_Pnd_Tirf_Boxb = pnd_Work_File2__R_Field_2.newFieldInGroup("pnd_Work_File2_Pnd_Tirf_Boxb", "#TIRF-BOXB", FieldType.NUMERIC, 12, 
            2);

        vw_tircntl = new DataAccessProgramView(new NameInfo("vw_tircntl", "TIRCNTL"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        tircntl_Tircntl_Tbl_Nbr = vw_tircntl.getRecord().newFieldInGroup("tircntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        tircntl_Tircntl_Tax_Year = vw_tircntl.getRecord().newFieldInGroup("tircntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        tircntl_Tircntl_Seq_Nbr = vw_tircntl.getRecord().newFieldInGroup("tircntl_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        tircntl_Tircntl_Reporting_Tbl = vw_tircntl.getRecord().newGroupInGroup("TIRCNTL_TIRCNTL_REPORTING_TBL", "TIRCNTL-REPORTING-TBL");
        tircntl_Tircntl_Rpt_Form_Name = tircntl_Tircntl_Reporting_Tbl.newFieldInGroup("tircntl_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        registerRecord(vw_tircntl);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Today = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Ws_Pnd_Et = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et", "#ET", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Rpt_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rpt_Name", "#RPT-NAME", FieldType.STRING, 7);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Rpt_Name);
        pnd_Ws_Pnd_Rpt_Name_1 = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Rpt_Name_1", "#RPT-NAME-1", FieldType.STRING, 4);
        pnd_Ws_Pnd_Rpt_State_Code = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Rpt_State_Code", "#RPT-STATE-CODE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Old_Company_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Old_Accept_Reject = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Old_Accept_Reject", "#OLD-ACCEPT-REJECT", FieldType.STRING, 1);
        pnd_Ws_Pnd_Tot_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Fed_Tax_Wthld", "#TOT-FED-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Res_Gross_Amt", "#TOT-RES-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Res_Taxable_Amt", "#TOT-RES-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_State_Distr", "#TOT-STATE-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_State_Tax_Wthld", "#TOT-STATE-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Loc_Tax_Wthld", "#TOT-LOC-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Loc_Distr", "#TOT-LOC-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Irr_Amt", "#TOT-IRR-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Box1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Box1", "#TOT-TIRF-BOX1", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Box2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Box2", "#TOT-TIRF-BOX2", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Box4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Box4", "#TOT-TIRF-BOX4", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Box5 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Box5", "#TOT-TIRF-BOX5", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Box9 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Box9", "#TOT-TIRF-BOX9", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Boxa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Boxa", "#TOT-TIRF-BOXA", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Tirf_Boxb = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Tirf_Boxb", "#TOT-TIRF-BOXB", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Box1", "#TOT-RCO-TIRF-BOX1", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Box2", "#TOT-RCO-TIRF-BOX2", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Box4", "#TOT-RCO-TIRF-BOX4", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box5 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Box5", "#TOT-RCO-TIRF-BOX5", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box9 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Box9", "#TOT-RCO-TIRF-BOX9", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa", "#TOT-RCO-TIRF-BOXA", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb", "#TOT-RCO-TIRF-BOXB", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Box1", "#TOT-REJ-TIRF-BOX1", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Box2", "#TOT-REJ-TIRF-BOX2", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Box4", "#TOT-REJ-TIRF-BOX4", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box5 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Box5", "#TOT-REJ-TIRF-BOX5", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box9 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Box9", "#TOT-REJ-TIRF-BOX9", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa", "#TOT-REJ-TIRF-BOXA", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb", "#TOT-REJ-TIRF-BOXB", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Gross_Amt", "#RCO-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Taxable_Amt", "#RCO-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Fed_Tax_Wthld", "#RCO-FED-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Res_Gross_Amt", "#RCO-RES-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Res_Taxable_Amt", "#RCO-RES-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_State_Distr", "#RCO-STATE-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_State_Tax_Wthld", "#RCO-STATE-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Loc_Tax_Wthld", "#RCO-LOC-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Loc_Distr", "#RCO-LOC-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rco_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rco_Irr_Amt", "#RCO-IRR-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Amt", "#REJ-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Taxable_Amt", "#REJ-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Fed_Tax_Wthld", "#REJ-FED-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Res_Gross_Amt", "#REJ-RES-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Res_Taxable_Amt", "#REJ-RES-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_State_Distr", "#REJ-STATE-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_State_Tax_Wthld", "#REJ-STATE-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Loc_Tax_Wthld", "#REJ-LOC-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Loc_Distr", "#REJ-LOC-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rej_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rej_Irr_Amt", "#REJ-IRR-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Gross_Amt", "#DET-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Taxable_Amt", "#DET-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Fed_Tax_Wthld", "#DET-FED-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Res_Gross_Amt", "#DET-RES-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Res_Taxable_Amt", "#DET-RES-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_State_Distr", "#DET-STATE-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_State_Tax_Wthld", "#DET-STATE-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Loc_Tax_Wthld", "#DET-LOC-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Loc_Distr", "#DET-LOC-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Det_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Irr_Amt", "#DET-IRR-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Gross_Amt", "#REC-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Taxable_Amt", "#REC-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Fed_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Fed_Tax_Wthld", "#REC-FED-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Gross_Amt", "#REC-RES-GROSS-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Taxable_Amt", "#REC-RES-TAXABLE-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_State_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Distr", "#REC-STATE-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_State_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Tax_Wthld", "#REC-STATE-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Tax_Wthld", "#REC-LOC-TAX-WTHLD", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Loc_Distr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Distr", "#REC-LOC-DISTR", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Irr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Irr_Amt", "#REC-IRR-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Gross_Amt_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Gross_Amt_T", "#REC-GROSS-AMT-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Taxable_Amt_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Taxable_Amt_T", "#REC-TAXABLE-AMT-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_T", "#REC-FED-TAX-WTHLD-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Gross_Amt_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Gross_Amt_T", "#REC-RES-GROSS-AMT-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Taxable_Amt_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Taxable_Amt_T", "#REC-RES-TAXABLE-AMT-T", FieldType.NUMERIC, 13, 
            2);
        pnd_Ws_Pnd_Rec_State_Distr_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Distr_T", "#REC-STATE-DISTR-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_State_Tax_Wthld_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Tax_Wthld_T", "#REC-STATE-TAX-WTHLD-T", FieldType.NUMERIC, 13, 
            2);
        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_T", "#REC-LOC-TAX-WTHLD-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Loc_Distr_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Distr_T", "#REC-LOC-DISTR-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Irr_Amt_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Irr_Amt_T", "#REC-IRR-AMT-T", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Counter_T = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Counter_T", "#REC-COUNTER-T", FieldType.NUMERIC, 9);
        pnd_Ws_Pnd_Rec_Gross_Amt_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Gross_Amt_X", "#REC-GROSS-AMT-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Taxable_Amt_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Taxable_Amt_X", "#REC-TAXABLE-AMT-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_X", "#REC-FED-TAX-WTHLD-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Gross_Amt_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Gross_Amt_X", "#REC-RES-GROSS-AMT-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Res_Taxable_Amt_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Res_Taxable_Amt_X", "#REC-RES-TAXABLE-AMT-X", FieldType.NUMERIC, 13, 
            2);
        pnd_Ws_Pnd_Rec_State_Distr_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Distr_X", "#REC-STATE-DISTR-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_State_Tax_Wthld_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_State_Tax_Wthld_X", "#REC-STATE-TAX-WTHLD-X", FieldType.NUMERIC, 13, 
            2);
        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_X", "#REC-LOC-TAX-WTHLD-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Loc_Distr_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Loc_Distr_X", "#REC-LOC-DISTR-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Irr_Amt_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Irr_Amt_X", "#REC-IRR-AMT-X", FieldType.NUMERIC, 13, 2);
        pnd_Ws_Pnd_Rec_Counter_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Counter_X", "#REC-COUNTER-X", FieldType.NUMERIC, 9);
        pnd_Ws_Pnd_Empty_Form_Count = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Form_Count", "#EMPTY-FORM-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Reject_Form_Count = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Reject_Form_Count", "#REJECT-FORM-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Accept_Form_Count = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_Form_Count", "#ACCEPT-FORM-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Recon_Only = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Recon_Only", "#RECON-ONLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Zero = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Zero", "#ZERO", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Company_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Name", "#COMPANY-NAME", FieldType.STRING, 15);
        pnd_Ws_Pnd_State_Indicators = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Indicators", "#STATE-INDICATORS", FieldType.STRING, 20);
        pnd_Ws_Pnd_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_Ws_Pnd_Address = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Address", "#ADDRESS", FieldType.STRING, 126);
        pnd_Ws_Pnd_Form_Desc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Desc", "#FORM-DESC", FieldType.STRING, 30);
        pnd_Ws_Pnd_Form_Desc2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Desc2", "#FORM-DESC2", FieldType.STRING, 30);
        pnd_Ws_Pnd_State_Numeric_Code = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Numeric_Code", "#STATE-NUMERIC-CODE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Header_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Header_Tax_Year", "#HEADER-TAX-YEAR", FieldType.STRING, 4);
        pnd_Ws_Pnd_Combined_Fed_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Combined_Fed_State", "#COMBINED-FED-STATE", FieldType.STRING, 1);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 5);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 5);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_tircntl.reset();

        ldaTwrl9705.initializeValues();
        ldaTwrl3502.initializeValues();
        ldaTwrltb4r.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Rpt_Name.setInitialValue("9ST0");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3615() throws Exception
    {
        super("Twrp3615");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133;//Natural: FORMAT ( 05 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        pnd_Datx.setValue(Global.getDATX());                                                                                                                              //Natural: ASSIGN #DATX := *DATX
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: ASSIGN #TIMX := *TIMX
        pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRACOM2.#ABEND-IND := #TWRACOM2.#DISPLAY-IND := TRUE
        pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(true);
        pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
        pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
        pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #WORK-FILE1 #WORK-FILE2
        while (condition(getWorkFiles().read(1, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
            pnd_Work_File1, pnd_Work_File2)))
        {
            CheckAtStartofData1270();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(pnd_Work_File1_Pnd_Tirf_State_Hardcopy_Ind.equals(" ") && pnd_Work_File1_Pnd_Tirf_State_Reporting.notEquals("Y")))                              //Natural: IF #TIRF-STATE-HARDCOPY-IND = ' ' AND #TIRF-STATE-REPORTING NE 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            GET:                                                                                                                                                          //Natural: GET FORM-U #TIRF-ISN
            ldaTwrl9705.getVw_form_U().readByID(pnd_Work_File1_Pnd_Tirf_Isn.getLong(), "GET");
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO C*TIRF-1099-R-STATE-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_I.nadd(1))
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I).equals(pnd_Ws_Pnd_State_Numeric_Code)))                                             //Natural: IF TIRF-STATE-CODE ( #I ) = #STATE-NUMERIC-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_J.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #J := #I
                //*   REPORTABLE STATE PAYMENT
                if (condition(pnd_Work_File1_Pnd_Tirf_State_Reporting.equals("Y")))                                                                                       //Natural: IF #TIRF-STATE-REPORTING = 'Y'
                {
                    //*  ON HOLD (CORRECT)
                    if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y") || pnd_Work_File1_Pnd_Tirf_Res_Reject_Ind.equals("Y")))                                     //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-RES-REJECT-IND = 'Y'
                    {
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Ind().getValue(pnd_I).setValue("H");                                                                    //Natural: ASSIGN TIRF-STATE-AUTH-RPT-IND ( #I ) := 'H'
                        //*   ORIGINAL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Ind().getValue(pnd_I).setValue("O");                                                                    //Natural: ASSIGN TIRF-STATE-AUTH-RPT-IND ( #I ) := 'O'
                        //*   REPORTING DONE.
                    }                                                                                                                                                     //Natural: END-IF
                    //*   NOT A REPORTABLE PAPER STATE PAYMENT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*   RECONCILIATION
                    if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y") || pnd_Work_File1_Pnd_Tirf_Res_Reject_Ind.equals("Y")))                                     //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-RES-REJECT-IND = 'Y'
                    {
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Ind().getValue(pnd_I).setValue("J");                                                                    //Natural: ASSIGN TIRF-STATE-AUTH-RPT-IND ( #I ) := 'J'
                        //*   ON HOLD (CORRECT).
                        //*   RECONCILIATION
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Ind().getValue(pnd_I).setValue("X");                                                                    //Natural: ASSIGN TIRF-STATE-AUTH-RPT-IND ( #I ) := 'X'
                        //*   ONLY NO REPORTING.
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl9705.getForm_U_Tirf_State_Auth_Rpt_Date().getValue(pnd_I).setValue(pnd_Datx);                                                                      //Natural: ASSIGN TIRF-STATE-AUTH-RPT-DATE ( #I ) := #DATX
                ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_PROGRAM());                                                                                  //Natural: ASSIGN TIRF-LU-USER := *INIT-PROGRAM
                ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Timx);                                                                                                    //Natural: ASSIGN TIRF-LU-TS := #TIMX
                ldaTwrl9705.getVw_form_U().updateDBRow("GET");                                                                                                            //Natural: UPDATE ( GET. )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Pnd_Et.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ET
            if (condition(pnd_Ws_Pnd_Et.greater(200)))                                                                                                                    //Natural: IF #ET > 200
            {
                pnd_Ws_Pnd_Et.setValue(0);                                                                                                                                //Natural: ASSIGN #ET := 0
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Old_Company_Cde.notEquals(" ") && pnd_Work_File1_Pnd_Tirf_Company_Cde.notEquals(pnd_Ws_Pnd_Old_Company_Cde)))                        //Natural: IF #OLD-COMPANY-CDE NE ' ' AND #TIRF-COMPANY-CDE NE #OLD-COMPANY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
                sub_Company_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pnd_Ws_Pnd_Old_Company_Cde.notEquals(" ") && pnd_Work_File1_Pnd_Tirf_Company_Cde.notEquals(pnd_Ws_Pnd_Old_Company_Cde)) ||                     //Natural: IF ( #OLD-COMPANY-CDE NE ' ' AND #TIRF-COMPANY-CDE NE #OLD-COMPANY-CDE ) OR ( #OLD-ACCEPT-REJECT NE ' ' AND #OLD-ACCEPT-REJECT NE #TIRF-SYS-ERR )
                (pnd_Ws_Pnd_Old_Accept_Reject.notEquals(" ") && pnd_Ws_Pnd_Old_Accept_Reject.notEquals(pnd_Work_File1_Pnd_Tirf_Sys_Err))))
            {
                                                                                                                                                                          //Natural: PERFORM DETAIL-BREAK
                sub_Detail_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 02 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pnd_Ws_Pnd_Old_Company_Cde.notEquals(" ") && pnd_Work_File1_Pnd_Tirf_Company_Cde.notEquals(pnd_Ws_Pnd_Old_Company_Cde)) ||                     //Natural: IF ( #OLD-COMPANY-CDE NE ' ' AND #TIRF-COMPANY-CDE NE #OLD-COMPANY-CDE ) OR ( #OLD-ACCEPT-REJECT NE ' ' AND #OLD-ACCEPT-REJECT NE #TIRF-SYS-ERR )
                (pnd_Ws_Pnd_Old_Accept_Reject.notEquals(" ") && pnd_Ws_Pnd_Old_Accept_Reject.notEquals(pnd_Work_File1_Pnd_Tirf_Sys_Err))))
            {
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECO-BREAK
                sub_Detail_Reco_Break();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 04 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Old_Company_Cde.setValue(pnd_Work_File1_Pnd_Tirf_Company_Cde);                                                                                     //Natural: ASSIGN #OLD-COMPANY-CDE := #TIRF-COMPANY-CDE
            pnd_Ws_Pnd_Old_Accept_Reject.setValue(pnd_Work_File1_Pnd_Tirf_Sys_Err);                                                                                       //Natural: ASSIGN #OLD-ACCEPT-REJECT := #TIRF-SYS-ERR
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y") || pnd_Work_File1_Pnd_Tirf_State_Reporting.notEquals("Y")))                                         //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-STATE-REPORTING NE 'Y'
            {
                pnd_Ws_Pnd_Form_Desc.setValue("Reject Forms  ");                                                                                                          //Natural: ASSIGN #FORM-DESC := 'Reject Forms  '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Form_Desc.setValue("Accept Forms  ");                                                                                                          //Natural: ASSIGN #FORM-DESC := 'Accept Forms  '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y")))                                                                                                   //Natural: IF #TIRF-SYS-ERR = 'Y'
            {
                pnd_Ws_Pnd_Form_Desc2.setValue("Reject Forms  ");                                                                                                         //Natural: ASSIGN #FORM-DESC2 := 'Reject Forms  '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Form_Desc2.setValue("Accept Forms  ");                                                                                                         //Natural: ASSIGN #FORM-DESC2 := 'Accept Forms  '
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Empty_Form.equals("Y")))                                                                                                //Natural: IF #TIRF-EMPTY-FORM = 'Y'
            {
                pnd_Ws_Pnd_Empty_Form_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #EMPTY-FORM-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y")))                                                                                                   //Natural: IF #TIRF-SYS-ERR = 'Y'
            {
                pnd_Ws_Pnd_Reject_Form_Count.nadd(1);                                                                                                                     //Natural: ADD 1 TO #REJECT-FORM-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_Empty_Form.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_State_Reporting.equals(" "))) //Natural: IF #TIRF-SYS-ERR NE 'Y' AND #TIRF-EMPTY-FORM NE 'Y' AND #TIRF-STATE-REPORTING = ' '
            {
                pnd_Ws_Pnd_Recon_Only.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RECON-ONLY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_Empty_Form.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_State_Reporting.equals("Y"))) //Natural: IF #TIRF-SYS-ERR NE 'Y' AND #TIRF-EMPTY-FORM NE 'Y' AND #TIRF-STATE-REPORTING = 'Y'
            {
                pnd_Ws_Pnd_Accept_Form_Count.nadd(1);                                                                                                                     //Natural: ADD 1 TO #ACCEPT-FORM-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y")))                                                                                                   //Natural: IF #TIRF-SYS-ERR = 'Y'
            {
                pnd_Ws_Pnd_Rej_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                    //Natural: ADD TIRF-GROSS-AMT TO #REJ-GROSS-AMT
                pnd_Ws_Pnd_Rej_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                                //Natural: ADD TIRF-TAXABLE-AMT TO #REJ-TAXABLE-AMT
                pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD TIRF-FED-TAX-WTHLD TO #REJ-FED-TAX-WTHLD
                pnd_Ws_Pnd_Rej_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                            //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #REJ-RES-GROSS-AMT
                pnd_Ws_Pnd_Rej_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                        //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #REJ-RES-TAXABLE-AMT
                pnd_Ws_Pnd_Rej_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                                //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REJ-STATE-DISTR
                pnd_Ws_Pnd_Rej_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                        //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REJ-STATE-TAX-WTHLD
                pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                            //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REJ-LOC-TAX-WTHLD
                pnd_Ws_Pnd_Rej_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                                    //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REJ-LOC-DISTR
                //*  RCC
                pnd_Ws_Pnd_Rej_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                                    //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #REJ-IRR-AMT
                pnd_Ws_Pnd_Tot_Rej_Tirf_Box1.nadd(pnd_Work_File2_Pnd_Tirf_Box1);                                                                                          //Natural: ADD #TIRF-BOX1 TO #TOT-REJ-TIRF-BOX1
                pnd_Ws_Pnd_Tot_Rej_Tirf_Box2.nadd(pnd_Work_File2_Pnd_Tirf_Box2);                                                                                          //Natural: ADD #TIRF-BOX2 TO #TOT-REJ-TIRF-BOX2
                pnd_Ws_Pnd_Tot_Rej_Tirf_Box4.nadd(pnd_Work_File2_Pnd_Tirf_Box4);                                                                                          //Natural: ADD #TIRF-BOX4 TO #TOT-REJ-TIRF-BOX4
                pnd_Ws_Pnd_Tot_Rej_Tirf_Box5.nadd(pnd_Work_File2_Pnd_Tirf_Box5);                                                                                          //Natural: ADD #TIRF-BOX5 TO #TOT-REJ-TIRF-BOX5
                pnd_Ws_Pnd_Tot_Rej_Tirf_Box9.nadd(pnd_Work_File2_Pnd_Tirf_Box9);                                                                                          //Natural: ADD #TIRF-BOX9 TO #TOT-REJ-TIRF-BOX9
                pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa.nadd(pnd_Work_File2_Pnd_Tirf_Boxa);                                                                                          //Natural: ADD #TIRF-BOXA TO #TOT-REJ-TIRF-BOXA
                //*  RCC
                pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb.nadd(pnd_Work_File2_Pnd_Tirf_Boxb);                                                                                          //Natural: ADD #TIRF-BOXB TO #TOT-REJ-TIRF-BOXB
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_State_Reporting.equals(" ")))                                         //Natural: IF #TIRF-SYS-ERR NE 'Y' AND #TIRF-STATE-REPORTING = ' '
            {
                pnd_Ws_Pnd_Rco_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                    //Natural: ADD TIRF-GROSS-AMT TO #RCO-GROSS-AMT
                pnd_Ws_Pnd_Rco_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                                //Natural: ADD TIRF-TAXABLE-AMT TO #RCO-TAXABLE-AMT
                pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD TIRF-FED-TAX-WTHLD TO #RCO-FED-TAX-WTHLD
                pnd_Ws_Pnd_Rco_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                            //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #RCO-RES-GROSS-AMT
                pnd_Ws_Pnd_Rco_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                        //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #RCO-RES-TAXABLE-AMT
                pnd_Ws_Pnd_Rco_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                                //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #RCO-STATE-DISTR
                pnd_Ws_Pnd_Rco_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                        //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #RCO-STATE-TAX-WTHLD
                pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                            //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #RCO-LOC-TAX-WTHLD
                pnd_Ws_Pnd_Rco_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                                    //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #RCO-LOC-DISTR
                //*  RCC
                pnd_Ws_Pnd_Rco_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                                    //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #RCO-IRR-AMT
                pnd_Ws_Pnd_Tot_Rco_Tirf_Box1.nadd(pnd_Work_File2_Pnd_Tirf_Box1);                                                                                          //Natural: ADD #TIRF-BOX1 TO #TOT-RCO-TIRF-BOX1
                pnd_Ws_Pnd_Tot_Rco_Tirf_Box2.nadd(pnd_Work_File2_Pnd_Tirf_Box2);                                                                                          //Natural: ADD #TIRF-BOX2 TO #TOT-RCO-TIRF-BOX2
                pnd_Ws_Pnd_Tot_Rco_Tirf_Box4.nadd(pnd_Work_File2_Pnd_Tirf_Box4);                                                                                          //Natural: ADD #TIRF-BOX4 TO #TOT-RCO-TIRF-BOX4
                pnd_Ws_Pnd_Tot_Rco_Tirf_Box5.nadd(pnd_Work_File2_Pnd_Tirf_Box5);                                                                                          //Natural: ADD #TIRF-BOX5 TO #TOT-RCO-TIRF-BOX5
                pnd_Ws_Pnd_Tot_Rco_Tirf_Box9.nadd(pnd_Work_File2_Pnd_Tirf_Box9);                                                                                          //Natural: ADD #TIRF-BOX9 TO #TOT-RCO-TIRF-BOX9
                pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa.nadd(pnd_Work_File2_Pnd_Tirf_Boxa);                                                                                          //Natural: ADD #TIRF-BOXA TO #TOT-RCO-TIRF-BOXA
                //*  RCC
                pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb.nadd(pnd_Work_File2_Pnd_Tirf_Boxb);                                                                                          //Natural: ADD #TIRF-BOXB TO #TOT-RCO-TIRF-BOXB
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.notEquals("Y") && pnd_Work_File1_Pnd_Tirf_State_Reporting.equals("Y")))                                         //Natural: IF #TIRF-SYS-ERR NE 'Y' AND #TIRF-STATE-REPORTING = 'Y'
            {
                pnd_Ws_Pnd_Tot_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                    //Natural: ADD TIRF-GROSS-AMT TO #TOT-GROSS-AMT
                pnd_Ws_Pnd_Tot_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                                //Natural: ADD TIRF-TAXABLE-AMT TO #TOT-TAXABLE-AMT
                pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD TIRF-FED-TAX-WTHLD TO #TOT-FED-TAX-WTHLD
                pnd_Ws_Pnd_Tot_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                            //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #TOT-RES-GROSS-AMT
                pnd_Ws_Pnd_Tot_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                        //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #TOT-RES-TAXABLE-AMT
                pnd_Ws_Pnd_Tot_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                                //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #TOT-STATE-DISTR
                pnd_Ws_Pnd_Tot_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                        //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #TOT-STATE-TAX-WTHLD
                pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                            //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #TOT-LOC-TAX-WTHLD
                pnd_Ws_Pnd_Tot_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                                    //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #TOT-LOC-DISTR
                //*  RCC
                pnd_Ws_Pnd_Tot_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                                    //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #TOT-IRR-AMT
                pnd_Ws_Pnd_Tot_Tirf_Box1.nadd(pnd_Work_File2_Pnd_Tirf_Box1);                                                                                              //Natural: ADD #TIRF-BOX1 TO #TOT-TIRF-BOX1
                pnd_Ws_Pnd_Tot_Tirf_Box2.nadd(pnd_Work_File2_Pnd_Tirf_Box2);                                                                                              //Natural: ADD #TIRF-BOX2 TO #TOT-TIRF-BOX2
                pnd_Ws_Pnd_Tot_Tirf_Box4.nadd(pnd_Work_File2_Pnd_Tirf_Box4);                                                                                              //Natural: ADD #TIRF-BOX4 TO #TOT-TIRF-BOX4
                pnd_Ws_Pnd_Tot_Tirf_Box5.nadd(pnd_Work_File2_Pnd_Tirf_Box5);                                                                                              //Natural: ADD #TIRF-BOX5 TO #TOT-TIRF-BOX5
                pnd_Ws_Pnd_Tot_Tirf_Box9.nadd(pnd_Work_File2_Pnd_Tirf_Box9);                                                                                              //Natural: ADD #TIRF-BOX9 TO #TOT-TIRF-BOX9
                pnd_Ws_Pnd_Tot_Tirf_Boxa.nadd(pnd_Work_File2_Pnd_Tirf_Boxa);                                                                                              //Natural: ADD #TIRF-BOXA TO #TOT-TIRF-BOXA
                //*  RCC
                pnd_Ws_Pnd_Tot_Tirf_Boxb.nadd(pnd_Work_File2_Pnd_Tirf_Boxb);                                                                                              //Natural: ADD #TIRF-BOXB TO #TOT-TIRF-BOXB
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Det_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                        //Natural: ADD TIRF-GROSS-AMT TO #DET-GROSS-AMT
            pnd_Ws_Pnd_Det_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                                    //Natural: ADD TIRF-TAXABLE-AMT TO #DET-TAXABLE-AMT
            pnd_Ws_Pnd_Det_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                                //Natural: ADD TIRF-FED-TAX-WTHLD TO #DET-FED-TAX-WTHLD
            pnd_Ws_Pnd_Det_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                                //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #DET-RES-GROSS-AMT
            pnd_Ws_Pnd_Det_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                            //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #DET-RES-TAXABLE-AMT
            pnd_Ws_Pnd_Det_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                                    //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #DET-STATE-DISTR
            pnd_Ws_Pnd_Det_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                            //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #DET-STATE-TAX-WTHLD
            pnd_Ws_Pnd_Det_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                                //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #DET-LOC-TAX-WTHLD
            pnd_Ws_Pnd_Det_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                                        //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #DET-LOC-DISTR
            //*  RCC
            pnd_Ws_Pnd_Det_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                                        //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #DET-IRR-AMT
            if (condition(pnd_Work_File1_Pnd_Tirf_State_Tax_Wthld.notEquals(getZero()) || pnd_Work_File1_Pnd_Tirf_Loc_Tax_Wthld.notEquals(getZero())))                    //Natural: IF #TIRF-STATE-TAX-WTHLD NE 0 OR #TIRF-LOC-TAX-WTHLD NE 0
            {
                //*  EINCHG
                //* *    VALUE 'T'                                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #TIRF-COMPANY-CDE
                short decideConditionsMet1460 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_File1_Pnd_Tirf_Company_Cde.equals("T") || pnd_Work_File1_Pnd_Tirf_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1460++;
                    pnd_Ws_Pnd_Rec_Gross_Amt_T.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                              //Natural: ADD TIRF-GROSS-AMT TO #REC-GROSS-AMT-T
                    pnd_Ws_Pnd_Rec_Taxable_Amt_T.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                          //Natural: ADD TIRF-TAXABLE-AMT TO #REC-TAXABLE-AMT-T
                    pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_T.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                      //Natural: ADD TIRF-FED-TAX-WTHLD TO #REC-FED-TAX-WTHLD-T
                    pnd_Ws_Pnd_Rec_Res_Gross_Amt_T.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                      //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #REC-RES-GROSS-AMT-T
                    pnd_Ws_Pnd_Rec_Res_Taxable_Amt_T.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                  //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #REC-RES-TAXABLE-AMT-T
                    pnd_Ws_Pnd_Rec_State_Distr_T.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                          //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REC-STATE-DISTR-T
                    pnd_Ws_Pnd_Rec_State_Tax_Wthld_T.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                  //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REC-STATE-TAX-WTHLD-T
                    pnd_Ws_Pnd_Rec_Loc_Distr_T.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                              //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REC-LOC-DISTR-T
                    pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_T.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                      //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REC-LOC-TAX-WTHLD-T
                    //*  RCC
                    pnd_Ws_Pnd_Rec_Irr_Amt_T.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                              //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #REC-IRR-AMT-T
                    pnd_Ws_Pnd_Rec_Counter_T.nadd(1);                                                                                                                     //Natural: ADD 1 TO #REC-COUNTER-T
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_File1_Pnd_Tirf_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1460++;
                    pnd_Ws_Pnd_Rec_Gross_Amt_X.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                              //Natural: ADD TIRF-GROSS-AMT TO #REC-GROSS-AMT-X
                    pnd_Ws_Pnd_Rec_Taxable_Amt_X.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                          //Natural: ADD TIRF-TAXABLE-AMT TO #REC-TAXABLE-AMT-X
                    pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_X.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                      //Natural: ADD TIRF-FED-TAX-WTHLD TO #REC-FED-TAX-WTHLD-X
                    pnd_Ws_Pnd_Rec_Res_Gross_Amt_X.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                      //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #REC-RES-GROSS-AMT-X
                    pnd_Ws_Pnd_Rec_Res_Taxable_Amt_X.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                  //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #REC-RES-TAXABLE-AMT-X
                    pnd_Ws_Pnd_Rec_State_Distr_X.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                          //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REC-STATE-DISTR-X
                    pnd_Ws_Pnd_Rec_State_Tax_Wthld_X.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                  //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REC-STATE-TAX-WTHLD-X
                    pnd_Ws_Pnd_Rec_Loc_Distr_X.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                              //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REC-LOC-DISTR-X
                    pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_X.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                      //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REC-LOC-TAX-WTHLD-X
                    //*  RCC
                    pnd_Ws_Pnd_Rec_Irr_Amt_X.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                              //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #REC-IRR-AMT-X
                    pnd_Ws_Pnd_Rec_Counter_X.nadd(1);                                                                                                                     //Natural: ADD 1 TO #REC-COUNTER-X
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Empty_Form.equals("Y")))                                                                                                //Natural: IF #TIRF-EMPTY-FORM = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Name.setValue(pnd_Work_File1_Pnd_Tirf_Name);                                                                                                       //Natural: ASSIGN #NAME := #TIRF-NAME
            pdaTwradist.getPnd_Twradist_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwradist.getPnd_Twradist_Pnd_Tax_Year()), Global.getDATN().divide(10000)); //Natural: ASSIGN #TWRADIST.#TAX-YEAR := *DATN / 10000
            pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde());                                                            //Natural: ASSIGN #TWRADIST.#IN-DIST := TIRF-DISTRIBUTION-CDE
            DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradist.getPnd_Twradist_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST.#INPUT-PARMS ( AD = O ) #TWRADIST.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Address.setValue(DbsUtil.compress(pnd_Work_File1_Pnd_Tirf_Addr_Ln1, pnd_Work_File1_Pnd_Tirf_Addr_Ln2, pnd_Work_File1_Pnd_Tirf_Addr_Ln3));          //Natural: COMPRESS #TIRF-ADDR-LN1 #TIRF-ADDR-LN2 #TIRF-ADDR-LN3 INTO #WS.#ADDRESS
            getReports().print(2, "Name:",pnd_Ws_Pnd_Name);                                                                                                               //Natural: PRINT ( 02 ) 'Name:' #NAME
            getReports().print(2, "Addr:",pnd_Ws_Pnd_Address);                                                                                                            //Natural: PRINT ( 02 ) 'Addr:' #WS.#ADDRESS
            getReports().display(2, "//TIN",                                                                                                                              //Natural: DISPLAY ( 02 ) '//TIN' #TIRF-TIN '//C' #TIRF-COMPANY-CDE '//Contract' #TIRF-ACCOUNT ( AL = 8 ) '/Dist/Code' #TWRADIST.#OUT-DIST ( LC = � ) '/Not/Dtrmnd' TIRF-TAXABLE-NOT-DET ( LC = �� ) '/FEDERAL/GROSS' TIRF-GROSS-AMT ( EM = -ZZZZ,ZZZ.99 ) '/FEDERAL/TAXABLE' TIRF-TAXABLE-AMT ( EM = -ZZZZ,ZZZ.99 ) '/FEDERAL/TAX' TIRF-FED-TAX-WTHLD ( EM = -ZZZZ,ZZZ.99 ) 'State/Gross/Taxable' TIRF-RES-GROSS-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 ) / '/' TIRF-RES-TAXABLE-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 ) '/State/Distrib' TIRF-STATE-DISTR ( #J ) ( EM = -ZZZZ,ZZZ.99 ) 'State/Local/Tax' TIRF-STATE-TAX-WTHLD ( #J ) ( EM = -ZZZZ,ZZZ.99 ) / '/' TIRF-LOC-TAX-WTHLD ( #J ) ( EM = -ZZZZ,ZZZ.99 ) '//IRR Amt' TIRF-RES-IRR-AMT ( #J ) ( EM = -ZZZZ,ZZZ.99 )
            		pnd_Work_File1_Pnd_Tirf_Tin,"//C",
            		pnd_Work_File1_Pnd_Tirf_Company_Cde,"//Contract",
            		pnd_Work_File1_Pnd_Tirf_Account, new AlphanumericLength (8),"/Dist/Code",
            		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), new FieldAttributes("LC=�"),"/Not/Dtrmnd",
            		ldaTwrl9705.getForm_U_Tirf_Taxable_Not_Det(), new FieldAttributes("LC=��"),"/FEDERAL/GROSS",
            		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/FEDERAL/TAXABLE",
            		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/FEDERAL/TAX",
            		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Gross/Taxable",
            		ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
            		ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/State/Distrib",
            		ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Local/Tax",
            		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
            		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"),"//IRR Amt",
            		ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J), new ReportEditMask ("-ZZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    'STATE/GROSS'     TIRF-RES-GROSS-AMT   (#J) (EM=-ZZZZ,ZZZ.99)
            //*    'STATE/TAXABLE'   TIRF-RES-TAXABLE-AMT (#J) (EM=-ZZZZ,ZZZ.99)
            //*    'STATE/DISTRIB'   TIRF-STATE-DISTR     (#J) (EM=-ZZZZ,ZZZ.99)
            //*    'STATE/LOCAL'     TIRF-STATE-TAX-WTHLD (#J) (EM=-ZZZZ,ZZZ.99)
            //*                      TIRF-LOC-TAX-WTHLD   (#J) (EM=-ZZZZ.99)
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 02 ) 1
            if (condition(pnd_Work_File1_Pnd_Tirf_State_Tax_Wthld.notEquals(getZero()) || pnd_Work_File1_Pnd_Tirf_Loc_Tax_Wthld.notEquals(getZero())))                    //Natural: IF #TIRF-STATE-TAX-WTHLD NE 0 OR #TIRF-LOC-TAX-WTHLD NE 0
            {
                getReports().print(4, "Name:",pnd_Ws_Pnd_Name);                                                                                                           //Natural: PRINT ( 04 ) 'Name:' #NAME
                getReports().print(4, "Addr:",pnd_Ws_Pnd_Address);                                                                                                        //Natural: PRINT ( 04 ) 'Addr:' #WS.#ADDRESS
                getReports().display(4, "TIN",                                                                                                                            //Natural: DISPLAY ( 04 ) 'TIN' #TIRF-TIN 'COMPANY' #TIRF-COMPANY-CDE 'CONTRACT' #TIRF-ACCOUNT 'STATE/DIST' TIRF-STATE-DISTR ( #J ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'STATE/TAX WTHLD' TIRF-STATE-TAX-WTHLD ( #J ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 'LOCAL/DIST' TIRF-LOC-DISTR ( #J ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'LOCAL/TAX WTHLD' TIRF-LOC-TAX-WTHLD ( #J ) ( EM = -Z,ZZZ,ZZ9.99 )
                		pnd_Work_File1_Pnd_Tirf_Tin,"COMPANY",
                		pnd_Work_File1_Pnd_Tirf_Company_Cde,"CONTRACT",
                		pnd_Work_File1_Pnd_Tirf_Account,"STATE/DIST",
                		ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"STATE/TAX WTHLD",
                		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"LOCAL/DIST",
                		ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"LOCAL/TAX WTHLD",
                		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().skip(4, 1);                                                                                                                                  //Natural: SKIP ( 04 ) 1
                pnd_Ws_Pnd_Rec_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                                    //Natural: ADD TIRF-GROSS-AMT TO #REC-GROSS-AMT
                pnd_Ws_Pnd_Rec_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                                                //Natural: ADD TIRF-TAXABLE-AMT TO #REC-TAXABLE-AMT
                pnd_Ws_Pnd_Rec_Fed_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                                            //Natural: ADD TIRF-FED-TAX-WTHLD TO #REC-FED-TAX-WTHLD
                pnd_Ws_Pnd_Rec_Res_Gross_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_J));                                                            //Natural: ADD TIRF-RES-GROSS-AMT ( #J ) TO #REC-RES-GROSS-AMT
                pnd_Ws_Pnd_Rec_Res_Taxable_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_J));                                                        //Natural: ADD TIRF-RES-TAXABLE-AMT ( #J ) TO #REC-RES-TAXABLE-AMT
                pnd_Ws_Pnd_Rec_State_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_J));                                                                //Natural: ADD TIRF-STATE-DISTR ( #J ) TO #REC-STATE-DISTR
                pnd_Ws_Pnd_Rec_State_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_J));                                                        //Natural: ADD TIRF-STATE-TAX-WTHLD ( #J ) TO #REC-STATE-TAX-WTHLD
                pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_J));                                                            //Natural: ADD TIRF-LOC-TAX-WTHLD ( #J ) TO #REC-LOC-TAX-WTHLD
                pnd_Ws_Pnd_Rec_Loc_Distr.nadd(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_J));                                                                    //Natural: ADD TIRF-LOC-DISTR ( #J ) TO #REC-LOC-DISTR
                //*  RCC
                pnd_Ws_Pnd_Rec_Irr_Amt.nadd(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_J));                                                                    //Natural: ADD TIRF-RES-IRR-AMT ( #J ) TO #REC-IRR-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Pnd_Tirf_Sys_Err.equals("Y") || pnd_Work_File1_Pnd_Tirf_State_Reporting.notEquals("Y")))                                         //Natural: IF #TIRF-SYS-ERR = 'Y' OR #TIRF-STATE-REPORTING NE 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #WORK-FILE1 #WORK-FILE2
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
                pnd_Work_File1, pnd_Work_File2);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Empty_Form_Count.notEquals(getZero()) || pnd_Ws_Pnd_Reject_Form_Count.notEquals(getZero()) || pnd_Ws_Pnd_Accept_Form_Count.notEquals(getZero())  //Natural: IF #EMPTY-FORM-COUNT NE 0 OR #REJECT-FORM-COUNT NE 0 OR #ACCEPT-FORM-COUNT NE 0 OR #RECON-ONLY NE 0
            || pnd_Ws_Pnd_Recon_Only.notEquals(getZero())))
        {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
            sub_Company_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETAIL-BREAK
        sub_Detail_Break();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECO-BREAK
        sub_Detail_Reco_Break();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-REC
        sub_Update_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,"Withholding Information:",NEWLINE);                                                                           //Natural: WRITE ( 03 ) / 'Withholding Information:' /
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #K = 1 TO 2
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(2)); pnd_K.nadd(1))
        {
            //*  EINCHG
            //*  RCC
            //*  RCC
            short decideConditionsMet1563 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #K;//Natural: VALUE 1
            if (condition((pnd_K.equals(1))))
            {
                decideConditionsMet1563++;
                //*      #COMPANY-NAME         :=  'TIAA'
                pnd_Ws_Pnd_Company_Name.setValue("ADMN");                                                                                                                 //Natural: ASSIGN #COMPANY-NAME := 'ADMN'
                pnd_Ws_Pnd_Tot_Gross_Amt.setValue(pnd_Ws_Pnd_Rec_Gross_Amt_T);                                                                                            //Natural: ASSIGN #TOT-GROSS-AMT := #REC-GROSS-AMT-T
                pnd_Ws_Pnd_Tot_Taxable_Amt.setValue(pnd_Ws_Pnd_Rec_Taxable_Amt_T);                                                                                        //Natural: ASSIGN #TOT-TAXABLE-AMT := #REC-TAXABLE-AMT-T
                pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_T);                                                                                    //Natural: ASSIGN #TOT-FED-TAX-WTHLD := #REC-FED-TAX-WTHLD-T
                pnd_Ws_Pnd_Tot_Res_Gross_Amt.setValue(pnd_Ws_Pnd_Rec_Res_Gross_Amt_T);                                                                                    //Natural: ASSIGN #TOT-RES-GROSS-AMT := #REC-RES-GROSS-AMT-T
                pnd_Ws_Pnd_Tot_Res_Taxable_Amt.setValue(pnd_Ws_Pnd_Rec_Res_Taxable_Amt_T);                                                                                //Natural: ASSIGN #TOT-RES-TAXABLE-AMT := #REC-RES-TAXABLE-AMT-T
                pnd_Ws_Pnd_Tot_State_Distr.setValue(pnd_Ws_Pnd_Rec_State_Distr_T);                                                                                        //Natural: ASSIGN #TOT-STATE-DISTR := #REC-STATE-DISTR-T
                pnd_Ws_Pnd_Tot_State_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_State_Tax_Wthld_T);                                                                                //Natural: ASSIGN #TOT-STATE-TAX-WTHLD := #REC-STATE-TAX-WTHLD-T
                pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_T);                                                                                    //Natural: ASSIGN #TOT-LOC-TAX-WTHLD := #REC-LOC-TAX-WTHLD-T
                pnd_Ws_Pnd_Tot_Loc_Distr.setValue(pnd_Ws_Pnd_Rec_Loc_Distr_T);                                                                                            //Natural: ASSIGN #TOT-LOC-DISTR := #REC-LOC-DISTR-T
                pnd_Ws_Pnd_Tot_Irr_Amt.setValue(pnd_Ws_Pnd_Rec_Irr_Amt_T);                                                                                                //Natural: ASSIGN #TOT-IRR-AMT := #REC-IRR-AMT-T
                pnd_Ws_Pnd_Rec_Counter_T.setValue(pnd_Ws_Pnd_Rec_Counter_T);                                                                                              //Natural: ASSIGN #REC-COUNTER-T := #REC-COUNTER-T
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_K.equals(2))))
            {
                decideConditionsMet1563++;
                pnd_Ws_Pnd_Company_Name.setValue("TRST");                                                                                                                 //Natural: ASSIGN #COMPANY-NAME := 'TRST'
                pnd_Ws_Pnd_Tot_Gross_Amt.setValue(pnd_Ws_Pnd_Rec_Gross_Amt_X);                                                                                            //Natural: ASSIGN #TOT-GROSS-AMT := #REC-GROSS-AMT-X
                pnd_Ws_Pnd_Tot_Taxable_Amt.setValue(pnd_Ws_Pnd_Rec_Taxable_Amt_X);                                                                                        //Natural: ASSIGN #TOT-TAXABLE-AMT := #REC-TAXABLE-AMT-X
                pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_Fed_Tax_Wthld_X);                                                                                    //Natural: ASSIGN #TOT-FED-TAX-WTHLD := #REC-FED-TAX-WTHLD-X
                pnd_Ws_Pnd_Tot_Res_Gross_Amt.setValue(pnd_Ws_Pnd_Rec_Res_Gross_Amt_X);                                                                                    //Natural: ASSIGN #TOT-RES-GROSS-AMT := #REC-RES-GROSS-AMT-X
                pnd_Ws_Pnd_Tot_Res_Taxable_Amt.setValue(pnd_Ws_Pnd_Rec_Res_Taxable_Amt_X);                                                                                //Natural: ASSIGN #TOT-RES-TAXABLE-AMT := #REC-RES-TAXABLE-AMT-X
                pnd_Ws_Pnd_Tot_State_Distr.setValue(pnd_Ws_Pnd_Rec_State_Distr_X);                                                                                        //Natural: ASSIGN #TOT-STATE-DISTR := #REC-STATE-DISTR-X
                pnd_Ws_Pnd_Tot_State_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_State_Tax_Wthld_X);                                                                                //Natural: ASSIGN #TOT-STATE-TAX-WTHLD := #REC-STATE-TAX-WTHLD-X
                pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.setValue(pnd_Ws_Pnd_Rec_Loc_Tax_Wthld_X);                                                                                    //Natural: ASSIGN #TOT-LOC-TAX-WTHLD := #REC-LOC-TAX-WTHLD-X
                pnd_Ws_Pnd_Tot_Loc_Distr.setValue(pnd_Ws_Pnd_Rec_Loc_Distr_X);                                                                                            //Natural: ASSIGN #TOT-LOC-DISTR := #REC-LOC-DISTR-X
                pnd_Ws_Pnd_Tot_Irr_Amt.setValue(pnd_Ws_Pnd_Rec_Irr_Amt_X);                                                                                                //Natural: ASSIGN #TOT-IRR-AMT := #REC-IRR-AMT-X
                pnd_Ws_Pnd_Rec_Counter_T.setValue(pnd_Ws_Pnd_Rec_Counter_X);                                                                                              //Natural: ASSIGN #REC-COUNTER-T := #REC-COUNTER-X
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(3, "COMPANY",                                                                                                                            //Natural: DISPLAY ( 03 ) 'COMPANY' #COMPANY-NAME 'STATE/DIST' #TOT-STATE-DISTR ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'STATE/TAX WTHLD' #TOT-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) 'LOCAL/DIST' #TOT-LOC-DISTR ( EM = -ZZZ,ZZZ,ZZ9.99 ) 'LOCAL/TAX WTHLD' #TOT-LOC-TAX-WTHLD ( EM = -Z,ZZZ,ZZ9.99 ) 'COUNTER' #REC-COUNTER-T
            		pnd_Ws_Pnd_Company_Name,"STATE/DIST",
            		pnd_Ws_Pnd_Tot_State_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"STATE/TAX WTHLD",
            		pnd_Ws_Pnd_Tot_State_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"LOCAL/DIST",
            		pnd_Ws_Pnd_Tot_Loc_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"LOCAL/TAX WTHLD",
            		pnd_Ws_Pnd_Tot_Loc_Tax_Wthld, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"COUNTER",
            		pnd_Ws_Pnd_Rec_Counter_T);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 01 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(44),"end of report");                                                                                   //Natural: WRITE ( 01 ) 44T 'end of report'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 01 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 02 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(44),"end of report");                                                                                   //Natural: WRITE ( 02 ) 44T 'end of report'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 02 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 03 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(44),"END OF REPORT");                                                                                   //Natural: WRITE ( 03 ) 44T 'END OF REPORT'
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 03 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 04 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(44),"END OF REPORT");                                                                                   //Natural: WRITE ( 04 ) 44T 'END OF REPORT'
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 04 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 05 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(44),"END OF REPORT");                                                                                   //Natural: WRITE ( 05 ) 44T 'END OF REPORT'
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"=",new RepeatItem(131));                                                                            //Natural: WRITE ( 05 ) 01T '=' ( 131 )
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* ******************************
        //* ******************************
        //*  'Local Distribution.....' #TOT-LOC-DISTR
        //*  'Local Distribution.....' #RCO-LOC-DISTR
        //*  'Local Distribution.....' #REJ-LOC-DISTR
        //*  'Local Distribution.....' #REJ-LOC-DISTR
        //* *------------
        //*      'Local Distribution.....' #DET-LOC-DISTR
        //* *------------
        //* *******************************
        //* *******************************
        //* ***************************
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 03 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 04 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 05 )
    }
    private void sub_Company_Break() throws Exception                                                                                                                     //Natural: COMPANY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Ws_Pnd_Old_Company_Cde);                                                                                 //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #OLD-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Ws_Pnd_Header_Tax_Year);                                                 //Natural: MOVE EDITED #HEADER-TAX-YEAR TO #TWRACOM2.#TAX-YEAR ( EM = 9999 )
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Company_Name.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name());                                                                              //Natural: ASSIGN #COMPANY-NAME := #TWRACOM2.#COMP-SHORT-NAME
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"STATE REPORTABLE",NEWLINE,"Company................",pnd_Ws_Pnd_Company_Name,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Tot_Gross_Amt,  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'STATE REPORTABLE' / 'Company................' #COMPANY-NAME / 'Federal Gross..........' #TOT-GROSS-AMT / 'Federal Taxable........' #TOT-TAXABLE-AMT / 'Federal Withholding....' #TOT-FED-TAX-WTHLD / 'State Gross............' #TOT-RES-GROSS-AMT / 'State Taxable..........' #TOT-RES-TAXABLE-AMT / 'State Distribution.....' #TOT-STATE-DISTR / 'State Withholding......' #TOT-STATE-TAX-WTHLD / 'Local Withholding......' #TOT-LOC-TAX-WTHLD / 'IRR Amount.............' #TOT-IRR-AMT / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #ZERO / 'Rejected...............' #ZERO / 'Not Reportable.........' #ZERO / 'Reportable.............' #ACCEPT-FORM-COUNT
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Tot_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Tot_Fed_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Tot_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Tot_Res_Taxable_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Tot_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Tot_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Tot_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Tot_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Zero,
            NEWLINE,"Rejected...............",pnd_Ws_Pnd_Zero,NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Zero,NEWLINE,"Reportable.............",pnd_Ws_Pnd_Accept_Form_Count);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"NOT REPORTABLE",NEWLINE,"Company................",pnd_Ws_Pnd_Company_Name,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rco_Gross_Amt,  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 'NOT REPORTABLE' / 'Company................' #COMPANY-NAME / 'Federal Gross..........' #RCO-GROSS-AMT / 'Federal Taxable........' #RCO-TAXABLE-AMT / 'Federal Withholding....' #RCO-FED-TAX-WTHLD / 'State Gross............' #RCO-RES-GROSS-AMT / 'State Taxable..........' #RCO-RES-TAXABLE-AMT / 'State Distribution.....' #RCO-STATE-DISTR / 'State Withholding......' #RCO-STATE-TAX-WTHLD / 'Local Withholding......' #RCO-LOC-TAX-WTHLD / 'IRR Amount.............' #RCO-IRR-AMT / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #EMPTY-FORM-COUNT / 'Rejected...............' #ZERO / 'Not Reportable.........' #RECON-ONLY / 'Reportable.............' #ZERO
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rco_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rco_Fed_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rco_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rco_Res_Taxable_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rco_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rco_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rco_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rco_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Empty_Form_Count,
            NEWLINE,"Rejected...............",pnd_Ws_Pnd_Zero,NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Recon_Only,NEWLINE,"Reportable.............",
            pnd_Ws_Pnd_Zero);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"REJECTED  ",NEWLINE,"Company................",pnd_Ws_Pnd_Company_Name,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rej_Gross_Amt,  //Natural: WRITE ( 01 ) / 'REJECTED  ' / 'Company................' #COMPANY-NAME / 'Federal Gross..........' #REJ-GROSS-AMT / 'Federal Taxable........' #REJ-TAXABLE-AMT / 'Federal Withholding....' #REJ-FED-TAX-WTHLD / 'State Gross............' #REJ-RES-GROSS-AMT / 'State Taxable..........' #REJ-RES-TAXABLE-AMT / 'State Distribution.....' #REJ-STATE-DISTR / 'State Withholding......' #REJ-STATE-TAX-WTHLD / 'Local Withholding......' #REJ-LOC-TAX-WTHLD / 'IRR Amount.............' #REJ-IRR-AMT / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #EMPTY-FORM-COUNT / 'Rejected...............' #REJECT-FORM-COUNT / 'Not Reportable.........' #ZERO / 'Reportable.............' #ZERO
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rej_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rej_Fed_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rej_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rej_Res_Taxable_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rej_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rej_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rej_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rej_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Empty_Form_Count,
            NEWLINE,"Rejected...............",pnd_Ws_Pnd_Reject_Form_Count,NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Zero,NEWLINE,"Reportable.............",
            pnd_Ws_Pnd_Zero);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        pnd_Ws_Pnd_Rej_Gross_Amt.nadd(pnd_Ws_Pnd_Tot_Gross_Amt);                                                                                                          //Natural: ADD #TOT-GROSS-AMT TO #REJ-GROSS-AMT
        pnd_Ws_Pnd_Rej_Taxable_Amt.nadd(pnd_Ws_Pnd_Tot_Taxable_Amt);                                                                                                      //Natural: ADD #TOT-TAXABLE-AMT TO #REJ-TAXABLE-AMT
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.nadd(pnd_Ws_Pnd_Tot_Fed_Tax_Wthld);                                                                                                  //Natural: ADD #TOT-FED-TAX-WTHLD TO #REJ-FED-TAX-WTHLD
        pnd_Ws_Pnd_Rej_Res_Gross_Amt.nadd(pnd_Ws_Pnd_Tot_Res_Gross_Amt);                                                                                                  //Natural: ADD #TOT-RES-GROSS-AMT TO #REJ-RES-GROSS-AMT
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.nadd(pnd_Ws_Pnd_Tot_Res_Taxable_Amt);                                                                                              //Natural: ADD #TOT-RES-TAXABLE-AMT TO #REJ-RES-TAXABLE-AMT
        pnd_Ws_Pnd_Rej_State_Distr.nadd(pnd_Ws_Pnd_Tot_State_Distr);                                                                                                      //Natural: ADD #TOT-STATE-DISTR TO #REJ-STATE-DISTR
        pnd_Ws_Pnd_Rej_State_Tax_Wthld.nadd(pnd_Ws_Pnd_Tot_State_Tax_Wthld);                                                                                              //Natural: ADD #TOT-STATE-TAX-WTHLD TO #REJ-STATE-TAX-WTHLD
        pnd_Ws_Pnd_Rej_Loc_Distr.nadd(pnd_Ws_Pnd_Tot_Loc_Distr);                                                                                                          //Natural: ADD #TOT-LOC-DISTR TO #REJ-LOC-DISTR
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.nadd(pnd_Ws_Pnd_Tot_Loc_Tax_Wthld);                                                                                                  //Natural: ADD #TOT-LOC-TAX-WTHLD TO #REJ-LOC-TAX-WTHLD
        //*  RCC
        pnd_Ws_Pnd_Rej_Irr_Amt.nadd(pnd_Ws_Pnd_Tot_Irr_Amt);                                                                                                              //Natural: ADD #TOT-IRR-AMT TO #REJ-IRR-AMT
        pnd_Ws_Pnd_Rej_Gross_Amt.nadd(pnd_Ws_Pnd_Rco_Gross_Amt);                                                                                                          //Natural: ADD #RCO-GROSS-AMT TO #REJ-GROSS-AMT
        pnd_Ws_Pnd_Rej_Taxable_Amt.nadd(pnd_Ws_Pnd_Rco_Taxable_Amt);                                                                                                      //Natural: ADD #RCO-TAXABLE-AMT TO #REJ-TAXABLE-AMT
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.nadd(pnd_Ws_Pnd_Rco_Fed_Tax_Wthld);                                                                                                  //Natural: ADD #RCO-FED-TAX-WTHLD TO #REJ-FED-TAX-WTHLD
        pnd_Ws_Pnd_Rej_Res_Gross_Amt.nadd(pnd_Ws_Pnd_Rco_Res_Gross_Amt);                                                                                                  //Natural: ADD #RCO-RES-GROSS-AMT TO #REJ-RES-GROSS-AMT
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.nadd(pnd_Ws_Pnd_Rco_Res_Taxable_Amt);                                                                                              //Natural: ADD #RCO-RES-TAXABLE-AMT TO #REJ-RES-TAXABLE-AMT
        pnd_Ws_Pnd_Rej_State_Distr.nadd(pnd_Ws_Pnd_Rco_State_Distr);                                                                                                      //Natural: ADD #RCO-STATE-DISTR TO #REJ-STATE-DISTR
        pnd_Ws_Pnd_Rej_State_Tax_Wthld.nadd(pnd_Ws_Pnd_Rco_State_Tax_Wthld);                                                                                              //Natural: ADD #RCO-STATE-TAX-WTHLD TO #REJ-STATE-TAX-WTHLD
        pnd_Ws_Pnd_Rej_Loc_Distr.nadd(pnd_Ws_Pnd_Rco_Loc_Distr);                                                                                                          //Natural: ADD #RCO-LOC-DISTR TO #REJ-LOC-DISTR
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.nadd(pnd_Ws_Pnd_Rco_Loc_Tax_Wthld);                                                                                                  //Natural: ADD #RCO-LOC-TAX-WTHLD TO #REJ-LOC-TAX-WTHLD
        //*  RCC
        pnd_Ws_Pnd_Rej_Irr_Amt.nadd(pnd_Ws_Pnd_Rco_Irr_Amt);                                                                                                              //Natural: ADD #RCO-IRR-AMT TO #REJ-IRR-AMT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"COMPANY TOTAL",NEWLINE,"Company................",pnd_Ws_Pnd_Company_Name,NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Rej_Gross_Amt,  //Natural: WRITE ( 01 ) / 'COMPANY TOTAL' / 'Company................' #COMPANY-NAME / 'Federal Gross..........' #REJ-GROSS-AMT / 'Federal Taxable........' #REJ-TAXABLE-AMT / 'Federal Withholding....' #REJ-FED-TAX-WTHLD / 'State Gross............' #REJ-RES-GROSS-AMT / 'State Taxable..........' #REJ-RES-TAXABLE-AMT / 'State Distribution.....' #REJ-STATE-DISTR / 'State Withholding......' #REJ-STATE-TAX-WTHLD / 'IRR Amount.............' #REJ-IRR-AMT / 'Local Withholding......' #REJ-LOC-TAX-WTHLD / 'State Indicators.......' #STATE-INDICATORS / 'Empty..................' #EMPTY-FORM-COUNT / 'Rejected...............' #REJECT-FORM-COUNT / 'Not Reportable.........' #RECON-ONLY / 'Reportable.............' #ACCEPT-FORM-COUNT
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Rej_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Rej_Fed_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Rej_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Rej_Res_Taxable_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Rej_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Rej_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Rej_Irr_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Rej_Loc_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Indicators.......",pnd_Ws_Pnd_State_Indicators,NEWLINE,"Empty..................",pnd_Ws_Pnd_Empty_Form_Count,
            NEWLINE,"Rejected...............",pnd_Ws_Pnd_Reject_Form_Count,NEWLINE,"Not Reportable.........",pnd_Ws_Pnd_Recon_Only,NEWLINE,"Reportable.............",
            pnd_Ws_Pnd_Accept_Form_Count);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 01 ) 2
        if (condition(pnd_Work_File1_Pnd_Tirf_State_Code.notEquals("NY")))                                                                                                //Natural: IF #TIRF-STATE-CODE NE 'NY'
        {
            getReports().display(5, "COMPANY",                                                                                                                            //Natural: DISPLAY ( 05 ) 'COMPANY' #COMPANY-NAME 'BOX 1' #TOT-TIRF-BOX1 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'BOX 2' #TOT-TIRF-BOX2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'BOX 4' #TOT-TIRF-BOX4 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'BOX 5' #TOT-TIRF-BOX5 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'BOX 9' #TOT-TIRF-BOX9 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'BOX A' #TOT-TIRF-BOXA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'BOX B' #TOT-TIRF-BOXB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
            		pnd_Ws_Pnd_Company_Name,"BOX 1",
            		pnd_Ws_Pnd_Tot_Tirf_Box1, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 2",
            		pnd_Ws_Pnd_Tot_Tirf_Box2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 4",
            		pnd_Ws_Pnd_Tot_Tirf_Box4, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 5",
            		pnd_Ws_Pnd_Tot_Tirf_Box5, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 9",
            		pnd_Ws_Pnd_Tot_Tirf_Box9, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX A",
            		pnd_Ws_Pnd_Tot_Tirf_Boxa, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BOX B",
            		pnd_Ws_Pnd_Tot_Tirf_Boxb, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(5, ReportOption.NOTITLE,"NOT REPORTABLE ",pnd_Ws_Pnd_Tot_Rco_Tirf_Box1, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rco_Tirf_Box2,  //Natural: WRITE ( 05 ) 'NOT REPORTABLE ' #TOT-RCO-TIRF-BOX1 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-RCO-TIRF-BOX2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-RCO-TIRF-BOX4 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-RCO-TIRF-BOX5 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-RCO-TIRF-BOX9 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-RCO-TIRF-BOXA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / T*#TOT-TIRF-BOXA #TOT-RCO-TIRF-BOXB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rco_Tirf_Box4, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rco_Tirf_Box5, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rco_Tirf_Box9, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ReportTAsterisk(pnd_Ws_Pnd_Tot_Tirf_Boxa),pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(5, ReportOption.NOTITLE,"REJECTED       ",pnd_Ws_Pnd_Tot_Rej_Tirf_Box1, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rej_Tirf_Box2,  //Natural: WRITE ( 05 ) 'REJECTED       ' #TOT-REJ-TIRF-BOX1 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-REJ-TIRF-BOX2 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-REJ-TIRF-BOX4 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-REJ-TIRF-BOX5 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-REJ-TIRF-BOX9 ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) #TOT-REJ-TIRF-BOXA ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / T*#TOT-TIRF-BOXA #TOT-REJ-TIRF-BOXB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rej_Tirf_Box4, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rej_Tirf_Box5, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rej_Tirf_Box9, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ReportTAsterisk(pnd_Ws_Pnd_Tot_Tirf_Boxa),pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RCC
        //*  RCC
        //*  RCC
        pnd_Ws_Pnd_Tot_Gross_Amt.reset();                                                                                                                                 //Natural: RESET #TOT-GROSS-AMT #TOT-TAXABLE-AMT #TOT-FED-TAX-WTHLD #TOT-RES-GROSS-AMT #TOT-RES-TAXABLE-AMT #TOT-STATE-DISTR #TOT-STATE-TAX-WTHLD #TOT-LOC-DISTR #TOT-LOC-TAX-WTHLD #TOT-IRR-AMT #RCO-GROSS-AMT #RCO-TAXABLE-AMT #RCO-FED-TAX-WTHLD #RCO-RES-GROSS-AMT #RCO-RES-TAXABLE-AMT #RCO-STATE-DISTR #RCO-STATE-TAX-WTHLD #RCO-LOC-DISTR #RCO-LOC-TAX-WTHLD #RCO-IRR-AMT #REJ-GROSS-AMT #REJ-TAXABLE-AMT #REJ-FED-TAX-WTHLD #REJ-RES-GROSS-AMT #REJ-RES-TAXABLE-AMT #REJ-STATE-DISTR #REJ-STATE-TAX-WTHLD #REJ-LOC-DISTR #REJ-LOC-TAX-WTHLD #REJ-IRR-AMT #EMPTY-FORM-COUNT #REJECT-FORM-COUNT #ACCEPT-FORM-COUNT #RECON-ONLY #TOT-TIRF-BOX1 #TOT-TIRF-BOX1 #TOT-TIRF-BOX2 #TOT-TIRF-BOX4 #TOT-TIRF-BOX5 #TOT-TIRF-BOX9 #TOT-TIRF-BOXA #TOT-TIRF-BOXB #TOT-RCO-TIRF-BOX1 #TOT-RCO-TIRF-BOX2 #TOT-RCO-TIRF-BOX4 #TOT-RCO-TIRF-BOX5 #TOT-RCO-TIRF-BOX9 #TOT-RCO-TIRF-BOXA #TOT-RCO-TIRF-BOXB #TOT-REJ-TIRF-BOX1 #TOT-REJ-TIRF-BOX2 #TOT-REJ-TIRF-BOX4 #TOT-REJ-TIRF-BOX5 #TOT-REJ-TIRF-BOX9 #TOT-REJ-TIRF-BOXA #TOT-REJ-TIRF-BOXB
        pnd_Ws_Pnd_Tot_Taxable_Amt.reset();
        pnd_Ws_Pnd_Tot_Fed_Tax_Wthld.reset();
        pnd_Ws_Pnd_Tot_Res_Gross_Amt.reset();
        pnd_Ws_Pnd_Tot_Res_Taxable_Amt.reset();
        pnd_Ws_Pnd_Tot_State_Distr.reset();
        pnd_Ws_Pnd_Tot_State_Tax_Wthld.reset();
        pnd_Ws_Pnd_Tot_Loc_Distr.reset();
        pnd_Ws_Pnd_Tot_Loc_Tax_Wthld.reset();
        pnd_Ws_Pnd_Tot_Irr_Amt.reset();
        pnd_Ws_Pnd_Rco_Gross_Amt.reset();
        pnd_Ws_Pnd_Rco_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rco_Fed_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rco_Res_Gross_Amt.reset();
        pnd_Ws_Pnd_Rco_Res_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rco_State_Distr.reset();
        pnd_Ws_Pnd_Rco_State_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rco_Loc_Distr.reset();
        pnd_Ws_Pnd_Rco_Loc_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rco_Irr_Amt.reset();
        pnd_Ws_Pnd_Rej_Gross_Amt.reset();
        pnd_Ws_Pnd_Rej_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rej_Fed_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rej_Res_Gross_Amt.reset();
        pnd_Ws_Pnd_Rej_Res_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rej_State_Distr.reset();
        pnd_Ws_Pnd_Rej_State_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rej_Loc_Distr.reset();
        pnd_Ws_Pnd_Rej_Loc_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rej_Irr_Amt.reset();
        pnd_Ws_Pnd_Empty_Form_Count.reset();
        pnd_Ws_Pnd_Reject_Form_Count.reset();
        pnd_Ws_Pnd_Accept_Form_Count.reset();
        pnd_Ws_Pnd_Recon_Only.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box1.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box1.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box2.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box4.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box5.reset();
        pnd_Ws_Pnd_Tot_Tirf_Box9.reset();
        pnd_Ws_Pnd_Tot_Tirf_Boxa.reset();
        pnd_Ws_Pnd_Tot_Tirf_Boxb.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box1.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box2.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box4.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box5.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Box9.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Boxa.reset();
        pnd_Ws_Pnd_Tot_Rco_Tirf_Boxb.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box1.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box2.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box4.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box5.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Box9.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Boxa.reset();
        pnd_Ws_Pnd_Tot_Rej_Tirf_Boxb.reset();
    }
    private void sub_Detail_Break() throws Exception                                                                                                                      //Natural: DETAIL-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(131),NEWLINE,"TOTALS:",NEWLINE,"Federal Gross..........",pnd_Ws_Pnd_Det_Gross_Amt,                  //Natural: WRITE ( 02 ) '-' ( 131 ) / 'TOTALS:' / 'Federal Gross..........' #DET-GROSS-AMT / 'Federal Taxable........' #DET-TAXABLE-AMT / 'Federal Withholding....' #DET-FED-TAX-WTHLD / 'State Gross............' #DET-RES-GROSS-AMT / 'State Taxable..........' #DET-RES-TAXABLE-AMT / 'State Distribution.....' #DET-STATE-DISTR / 'State Withholding......' #DET-STATE-TAX-WTHLD / 'Local Withholding......' #DET-LOC-TAX-WTHLD / 'IRR Amount.............' #DET-IRR-AMT
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Taxable........",pnd_Ws_Pnd_Det_Taxable_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Federal Withholding....",pnd_Ws_Pnd_Det_Fed_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Gross............",pnd_Ws_Pnd_Det_Res_Gross_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Taxable..........",pnd_Ws_Pnd_Det_Res_Taxable_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Distribution.....",pnd_Ws_Pnd_Det_State_Distr, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"State Withholding......",pnd_Ws_Pnd_Det_State_Tax_Wthld, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Local Withholding......",pnd_Ws_Pnd_Det_Loc_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"IRR Amount.............",pnd_Ws_Pnd_Det_Irr_Amt, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Det_Gross_Amt.reset();                                                                                                                                 //Natural: RESET #DET-GROSS-AMT #DET-TAXABLE-AMT #DET-FED-TAX-WTHLD #DET-RES-GROSS-AMT #DET-RES-TAXABLE-AMT #DET-STATE-DISTR #DET-STATE-TAX-WTHLD #DET-LOC-DISTR #DET-LOC-TAX-WTHLD
        pnd_Ws_Pnd_Det_Taxable_Amt.reset();
        pnd_Ws_Pnd_Det_Fed_Tax_Wthld.reset();
        pnd_Ws_Pnd_Det_Res_Gross_Amt.reset();
        pnd_Ws_Pnd_Det_Res_Taxable_Amt.reset();
        pnd_Ws_Pnd_Det_State_Distr.reset();
        pnd_Ws_Pnd_Det_State_Tax_Wthld.reset();
        pnd_Ws_Pnd_Det_Loc_Distr.reset();
        pnd_Ws_Pnd_Det_Loc_Tax_Wthld.reset();
    }
    private void sub_Detail_Reco_Break() throws Exception                                                                                                                 //Natural: DETAIL-RECO-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(4, ReportOption.NOTITLE,"-",new RepeatItem(131),NEWLINE,"TOTALS:",new TabSetting(30),pnd_Ws_Pnd_Rec_State_Distr, new ReportEditMask            //Natural: WRITE ( 04 ) '-' ( 131 ) / 'TOTALS:' 30T #REC-STATE-DISTR ( EM = -ZZZ,ZZZ,ZZ9.99 ) #REC-STATE-TAX-WTHLD ( EM = -ZZ,ZZZ,ZZ9.99 ) #REC-LOC-DISTR ( EM = -ZZZ,ZZZ,ZZ9.99 ) #REC-LOC-TAX-WTHLD ( EM = -Z,ZZZ,ZZ9.99 )
            ("-ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_State_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_Loc_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),pnd_Ws_Pnd_Rec_Loc_Tax_Wthld, 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Rec_Gross_Amt.reset();                                                                                                                                 //Natural: RESET #REC-GROSS-AMT #REC-TAXABLE-AMT #REC-FED-TAX-WTHLD #REC-RES-GROSS-AMT #REC-RES-TAXABLE-AMT #REC-STATE-DISTR #REC-STATE-TAX-WTHLD #REC-LOC-DISTR #REC-LOC-TAX-WTHLD
        pnd_Ws_Pnd_Rec_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rec_Fed_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rec_Res_Gross_Amt.reset();
        pnd_Ws_Pnd_Rec_Res_Taxable_Amt.reset();
        pnd_Ws_Pnd_Rec_State_Distr.reset();
        pnd_Ws_Pnd_Rec_State_Tax_Wthld.reset();
        pnd_Ws_Pnd_Rec_Loc_Distr.reset();
        pnd_Ws_Pnd_Rec_Loc_Tax_Wthld.reset();
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), pnd_Work_File1_Pnd_Tirf_Tax_Year.val());     //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( #TIRF-TAX-YEAR )
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(true);                                                                                                           //Natural: ASSIGN TWRATBL2.#ABEND-IND := TWRATBL2.#DISPLAY-IND := TRUE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(true);
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_Work_File1_Pnd_Tirf_State_Code);                                                                             //Natural: ASSIGN TWRATBL2.#STATE-CDE := #TIRF-STATE-CODE
                                                                                                                                                                          //Natural: PERFORM STATE-INFO
        sub_State_Info();
        if (condition(Global.isEscape())) {return;}
        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pnd_Ws_Pnd_State_Numeric_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Old_Code());                                                                         //Natural: ASSIGN #STATE-NUMERIC-CODE := TWRATBL2.#STATE-CDE := TWRATBL2.TIRCNTL-STATE-OLD-CODE
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Old_Code());
        pnd_Ws_Pnd_State_Numeric_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Old_Code().getSubstring(2,2));                                                       //Natural: ASSIGN #STATE-NUMERIC-CODE := SUBSTR ( TWRATBL2.TIRCNTL-STATE-OLD-CODE,2,2 )
                                                                                                                                                                          //Natural: PERFORM STATE-INFO
        sub_State_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind().notEquals(" ")))                                                                                     //Natural: IF TIRCNTL-COMB-FED-IND NE ' '
        {
            pnd_Ws_Pnd_Combined_Fed_State.setValue("Y");                                                                                                                  //Natural: ASSIGN #COMBINED-FED-STATE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1781 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TIRCNTL-STATE-IND;//Natural: VALUE '1'
        if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("1"))))
        {
            decideConditionsMet1781++;
            pnd_Ws_Pnd_State_Indicators.setValue("Txbl, All");                                                                                                            //Natural: ASSIGN #STATE-INDICATORS := 'Txbl, All'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("2"))))
        {
            decideConditionsMet1781++;
            pnd_Ws_Pnd_State_Indicators.setValue("Txbl, Withhold > 0");                                                                                                   //Natural: ASSIGN #STATE-INDICATORS := 'Txbl, Withhold > 0'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("3"))))
        {
            decideConditionsMet1781++;
            pnd_Ws_Pnd_State_Indicators.setValue("Gross, All");                                                                                                           //Natural: ASSIGN #STATE-INDICATORS := 'Gross, All'
        }                                                                                                                                                                 //Natural: VALUE '4'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("4"))))
        {
            decideConditionsMet1781++;
            pnd_Ws_Pnd_State_Indicators.setValue("Gross, Withhold > 0");                                                                                                  //Natural: ASSIGN #STATE-INDICATORS := 'Gross, Withhold > 0'
        }                                                                                                                                                                 //Natural: VALUE '9'
        else if (condition((pdaTwratbl2.getTwratbl2_Tircntl_State_Ind().equals("9"))))
        {
            decideConditionsMet1781++;
            pnd_Ws_Pnd_State_Indicators.setValue("Non Reportable");                                                                                                       //Natural: ASSIGN #STATE-INDICATORS := 'Non Reportable'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_State_Info() throws Exception                                                                                                                        //Natural: STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control_Rec() throws Exception                                                                                                                //Natural: UPDATE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        //*  ESCAPE ROUTINE
        vw_tircntl.reset();                                                                                                                                               //Natural: RESET TIRCNTL
        tircntl_Tircntl_Tbl_Nbr.setValue(4);                                                                                                                              //Natural: ASSIGN TIRCNTL.TIRCNTL-TBL-NBR := 4
        tircntl_Tircntl_Tax_Year.compute(new ComputeParameters(false, tircntl_Tircntl_Tax_Year), pnd_Ws_Pnd_Header_Tax_Year.val());                                       //Natural: ASSIGN TIRCNTL.TIRCNTL-TAX-YEAR := VAL ( #HEADER-TAX-YEAR )
        tircntl_Tircntl_Seq_Nbr.setValue(1);                                                                                                                              //Natural: ASSIGN TIRCNTL.TIRCNTL-SEQ-NBR := 1
        pnd_Ws_Pnd_Rpt_State_Code.setValue(pnd_Ws_Pnd_State_Numeric_Code);                                                                                                //Natural: ASSIGN #RPT-STATE-CODE := #STATE-NUMERIC-CODE
        tircntl_Tircntl_Rpt_Form_Name.setValue(pnd_Ws_Pnd_Rpt_Name);                                                                                                      //Natural: ASSIGN TIRCNTL.TIRCNTL-RPT-FORM-NAME := #RPT-NAME
        vw_tircntl.insertDBRow();                                                                                                                                         //Natural: STORE TIRCNTL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year()), pnd_Ws_Pnd_Header_Tax_Year.val());   //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := VAL ( #HEADER-TAX-YEAR )
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().reset();                                                                                                               //Natural: RESET #TWRATBL4.#FORM-IND
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(pnd_Ws_Pnd_Rpt_Name);                                                                                         //Natural: ASSIGN #TWRATBL4.#REC-TYPE := #RPT-NAME
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                                        //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #TWRATBL4.#RET-CODE = FALSE
        {
            DbsUtil.terminate(112);  if (true) return;                                                                                                                    //Natural: TERMINATE 112
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(pnd_Datx);                                                                                     //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := #DATX
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                                        //Natural: CALLNAT 'TWRNTB4U' #TWRATBL4
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 01 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Original State Summary Reporting For:' #TIRF-STATE-CODE / 62T 'Tax Year:' #HEADER-TAX-YEAR /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(49),"Original State Summary Reporting For:",pnd_Work_File1_Pnd_Tirf_State_Code,NEWLINE,new 
                        TabSetting(62),"Tax Year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 02 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Original State Detail Reporting For:' #TIRF-STATE-CODE / 62T 'Tax Year:' #HEADER-TAX-YEAR / / #FORM-DESC /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"Original State Detail Reporting For:",pnd_Work_File1_Pnd_Tirf_State_Code,NEWLINE,new 
                        TabSetting(62),"Tax Year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE,NEWLINE,pnd_Ws_Pnd_Form_Desc,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 03 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Original State Reconciliation Summary For:' #TIRF-STATE-CODE / 62T 'Tax Year:' #HEADER-TAX-YEAR /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(46),"Original State Reconciliation Summary For:",pnd_Work_File1_Pnd_Tirf_State_Code,NEWLINE,new 
                        TabSetting(62),"Tax Year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 04 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Original State Reconciliation Detail For:' #TIRF-STATE-CODE / 62T 'Tax Year:' #HEADER-TAX-YEAR / / #FORM-DESC2 /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(47),"Original State Reconciliation Detail For:",pnd_Work_File1_Pnd_Tirf_State_Code,NEWLINE,new 
                        TabSetting(62),"Tax Year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE,NEWLINE,pnd_Ws_Pnd_Form_Desc2,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 05 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'State Reporting Amount Boxes For:' #TIRF-STATE-CODE / 62T 'Tax Year:' #HEADER-TAX-YEAR /
                        TabSetting(53),"Tax Withholding Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask 
                        ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"State Reporting Amount Boxes For:",pnd_Work_File1_Pnd_Tirf_State_Code,NEWLINE,new 
                        TabSetting(62),"Tax Year:",pnd_Ws_Pnd_Header_Tax_Year,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
        Global.format(5, "PS=60 LS=133");

        getReports().setDisplayColumns(2, "//TIN",
        		pnd_Work_File1_Pnd_Tirf_Tin,"//C",
        		pnd_Work_File1_Pnd_Tirf_Company_Cde,"//Contract",
        		pnd_Work_File1_Pnd_Tirf_Account, new AlphanumericLength (8),"/Dist/Code",
        		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), new FieldAttributes("LC=�"),"/Not/Dtrmnd",
        		ldaTwrl9705.getForm_U_Tirf_Taxable_Not_Det(), new FieldAttributes("LC=��"),"/FEDERAL/GROSS",
        		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/FEDERAL/TAXABLE",
        		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/FEDERAL/TAX",
        		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Gross/Taxable",
        		ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
        		ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"/State/Distrib",
        		ldaTwrl9705.getForm_U_Tirf_State_Distr(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"State/Local/Tax",
        		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE,"/",
        		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld(), new ReportEditMask ("-ZZZZ,ZZZ.99"),"//IRR Amt",
        		ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt(), new ReportEditMask ("-ZZZZ,ZZZ.99"));
        getReports().setDisplayColumns(4, "TIN",
        		pnd_Work_File1_Pnd_Tirf_Tin,"COMPANY",
        		pnd_Work_File1_Pnd_Tirf_Company_Cde,"CONTRACT",
        		pnd_Work_File1_Pnd_Tirf_Account,"STATE/DIST",
        		ldaTwrl9705.getForm_U_Tirf_State_Distr(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"STATE/TAX WTHLD",
        		ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld(), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"LOCAL/DIST",
        		ldaTwrl9705.getForm_U_Tirf_Loc_Distr(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"LOCAL/TAX WTHLD",
        		ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(3, "COMPANY",
        		pnd_Ws_Pnd_Company_Name,"STATE/DIST",
        		pnd_Ws_Pnd_Tot_State_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"STATE/TAX WTHLD",
        		pnd_Ws_Pnd_Tot_State_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),"LOCAL/DIST",
        		pnd_Ws_Pnd_Tot_Loc_Distr, new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),"LOCAL/TAX WTHLD",
        		pnd_Ws_Pnd_Tot_Loc_Tax_Wthld, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),"COUNTER",
        		pnd_Ws_Pnd_Rec_Counter_T);
        getReports().setDisplayColumns(5, "COMPANY",
        		pnd_Ws_Pnd_Company_Name,"BOX 1",
        		pnd_Ws_Pnd_Tot_Tirf_Box1, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 2",
        		pnd_Ws_Pnd_Tot_Tirf_Box2, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 4",
        		pnd_Ws_Pnd_Tot_Tirf_Box4, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 5",
        		pnd_Ws_Pnd_Tot_Tirf_Box5, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX 9",
        		pnd_Ws_Pnd_Tot_Tirf_Box9, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"BOX A",
        		pnd_Ws_Pnd_Tot_Tirf_Boxa, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"BOX B",
        		pnd_Ws_Pnd_Tot_Tirf_Boxb, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
    }
    private void CheckAtStartofData1270() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
            sub_Get_State_Info();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Header_Tax_Year.setValue(pnd_Work_File1_Pnd_Tirf_Tax_Year);                                                                                        //Natural: ASSIGN #HEADER-TAX-YEAR := #TIRF-TAX-YEAR
        }                                                                                                                                                                 //Natural: END-START
    }
}
