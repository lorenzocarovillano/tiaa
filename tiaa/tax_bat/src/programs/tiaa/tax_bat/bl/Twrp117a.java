/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:02 PM
**        * FROM NATURAL PROGRAM : Twrp117a
************************************************************
**        * FILE NAME            : Twrp117a.java
**        * CLASS NAME           : Twrp117a
**        * INSTANCE NAME        : Twrp117a
************************************************************
************************************************************************
************************  T W R P 1 1 7 A ******************************
************************************************************************
*************************  FORM SELECTION  *****************************
************************************************************************
**                                                                    **
** PROGRAM NAME:  TWRP117A                                            **
** SYSTEM      :  TAX WITHHOLDING & REPORTING SYSTEM                  **
** AUTHOR      :  A.WILNER                                            **
** PURPOSE     :  MAGNETIC REPORT TO PR   A (CONTROL REPORT)          **
**                                                                    **
** HISTORY.....:                                                      **
** 02/13/2013  RC  - BYPASS HEADER RECORD IDENTIFIED BY 'SU'
** 02/09/2010  A. YOUNG - REVISED TO ACCOMMODATE 2009 LAYOUT CHANGES:
**                        #TWRL117A-CONTROL-NUMBER   (A10) RE-DEFINED
**                          #TWRL117A-FILLER         (A02) SPACES ONLY
**                          #TWRL117A-CONTROL-NBR    (N08) 00008266
**                        #TWRL117A-S-CONTROL-NUMBER (A10) RE-DEFINED
**                          #TWRL117A-S-FILLER       (A02) SPACES ONLY
**                          #TWRL117A-S-CONTROL-NBR  (N08) 00008266
**                        #TWRL117A-TRANS-CODE       (A01) EXCLUDE 'X'
**                        #TWRL117A-PLAN-OF-ANN-TYPE (A01) INCLUDE 'N'
** 02/04/2009  A. YOUNG - REVISED TO ACCOMMODATE 2008 LAYOUT CHANGES:
**                        480.7C FIELD POSITIONS DELETED:  455 -  466
**                                                         467 -  478
**                        480.7C FIELD POSITIONS ADDED:    528 -  539
**                                                         540 -  551
**                        480.5  FIELD POSITIONS ADDED:   2495 - 2500
**                      - POPULATED & DISPLAYED #TWRL117A-ROLLOVER-CONTR
**                                              #TWRL117A-ROLLOVER-DISTR
** 01/28/2008  R. MA - NEW LDA TWRL117C WAS CREATED FOR 480.7C
**                     UPDATE THIS PGM TO ACCORMODATE THE NEW FORM.
**                    (NOTE: THIS PGM SHOWS A COMPANY TOTAL LINE, BUT
**                     NEVER HAD ANY COMPANY BREAK CODED, THUS THE
**                     COMPANY TOTALS ARE THE SAME AS REPORT TOTALS.)
** 02/20/2007  R. MA - UPDATE TO ACCORMODATE TWRL117A CHANGES.
** 02/07/2006  R. MA - FOR 2005, RECORD LENGTH WAS INCREASED FROM
**                     700 TO 2000. FIELDS WERE ADDED OR UPDATED.
**                     BOTH TWRL117A & TWRL117B HAD BEEN CHANGED.
**                     RE-COMPILE THIS PROGRAM FOR THE NEW SPECS.
** 02/08/2005  A. YOUNG -  RE-COMPILED DUE TO TWRL117A.               **
** 12/09/2002  BOTTERI - RECOMPILE W/NEW VERSION OF TWRL117A AND REPLACE
**             #TWRL117A-GROSS-PROCEEDS WITH #TWRL117A-PPD-NO-WTHLDNG
** 09/10/2002  BOTTERI - MAJOR CHANGES IN TWRP0117, ONLY COSMETIC
**             CHANGES HERE.
** 08/13/2002  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
**             LENGTH IN TWRLCOMP & TWRACOMP
** 02/12/2015  DUTTAD - WE WOULD BE BY-PASSING THE "PA" RECORD TYPE
**             ALONGWITH THE "SU" RECORD FOR GENERATING THE REPORT.
** 01/29/2018  DASDH1 - FOR CHANGE IN IRS LAYOUT FOR TAX YEAR
**             2017
** 01/22/2019  VIKRAM - 480.7C.1 LINE ADDED IN FILE WHICH NEEDS TO BE
**                      ESCAPED FROM REPORT GENERATION - TAG : VIKRAM
** 02/05/2019  SAIK   - RESTOW FOR CHANGE IN TWRL117C
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/01/2020 - VIKRAM - 480.7C   : POPULATE DISASTER RELATED
**                      DISTRIBUTIONS.                /* 480.7C DISASTER
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp117a extends BLNatBase
{
    // Data Areas
    private LdaTwrl117c ldaTwrl117c;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Tot_Read;
    private DbsField pnd_Work_Area_Pnd_Comp_Distr_Amt;
    private DbsField pnd_Work_Area_Pnd_Comp_Taxable_Amt;
    private DbsField pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax;
    private DbsField pnd_Work_Area_Pnd_Comp_Roll_Contr;
    private DbsField pnd_Work_Area_Pnd_Comp_Roll_Distr;
    private DbsField pnd_Work_Area_Pnd_Comp_A_Exempt;
    private DbsField pnd_Work_Area_Pnd_Comp_B_Taxable;
    private DbsField pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr;
    private DbsField pnd_Work_Area_Pnd_Comp_E_Total;
    private DbsField pnd_Work_Area_Pnd_Tot_Distr_Amt;
    private DbsField pnd_Work_Area_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Work_Area_Pnd_Tot_Contr_Aft_Tax;
    private DbsField pnd_Work_Area_Pnd_Tot_Roll_Contr;
    private DbsField pnd_Work_Area_Pnd_Tot_Roll_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_A_Exempt;
    private DbsField pnd_Work_Area_Pnd_Tot_B_Taxable;
    private DbsField pnd_Work_Area_Pnd_Tot_D_Aft_Tax_Cntr;
    private DbsField pnd_Work_Area_Pnd_Tot_E_Total;
    private DbsField pnd_Work_Area_Pnd_Ws_Read_Counter;
    private DbsField pnd_Work_Area_Pnd_Ws_Read_Counter1;
    private DbsField pnd_Work_Area_Pnd_Ws_Company;
    private DbsField pnd_Work_Area_Pnd_Ws_Filler;
    private DbsField pnd_Work_Area_Pnd_Ws_Trailer;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone;

    private DbsGroup pnd_Work_Area__R_Field_1;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F1;
    private DbsField pnd_Work_Area_Pnd_Ws_Area_Code;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_F3;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Num4;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Numbera;

    private DbsGroup pnd_Work_Area__R_Field_2;
    private DbsField pnd_Work_Area_Pnd_Ws_Phone_Number;
    private DbsField pnd_Temp_Cntl_Num;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl117c = new LdaTwrl117c();
        registerRecord(ldaTwrl117c);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Tot_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Read", "#TOT-READ", FieldType.PACKED_DECIMAL, 8);
        pnd_Work_Area_Pnd_Comp_Distr_Amt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_Distr_Amt", "#COMP-DISTR-AMT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_Taxable_Amt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_Taxable_Amt", "#COMP-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax", "#COMP-CONTR-AFT-TAX", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_Roll_Contr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_Roll_Contr", "#COMP-ROLL-CONTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_Roll_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_Roll_Distr", "#COMP-ROLL-DISTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_A_Exempt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_A_Exempt", "#COMP-A-EXEMPT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_B_Taxable = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_B_Taxable", "#COMP-B-TAXABLE", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr", "#COMP-D-AFT-TAX-CNTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Comp_E_Total = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Comp_E_Total", "#COMP-E-TOTAL", FieldType.PACKED_DECIMAL, 15, 
            2);
        pnd_Work_Area_Pnd_Tot_Distr_Amt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Distr_Amt", "#TOT-DISTR-AMT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_Taxable_Amt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_Contr_Aft_Tax = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Contr_Aft_Tax", "#TOT-CONTR-AFT-TAX", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_Roll_Contr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Roll_Contr", "#TOT-ROLL-CONTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_Roll_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Roll_Distr", "#TOT-ROLL-DISTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_A_Exempt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_A_Exempt", "#TOT-A-EXEMPT", FieldType.PACKED_DECIMAL, 15, 
            2);
        pnd_Work_Area_Pnd_Tot_B_Taxable = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_B_Taxable", "#TOT-B-TAXABLE", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_D_Aft_Tax_Cntr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_D_Aft_Tax_Cntr", "#TOT-D-AFT-TAX-CNTR", FieldType.PACKED_DECIMAL, 
            15, 2);
        pnd_Work_Area_Pnd_Tot_E_Total = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_E_Total", "#TOT-E-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Work_Area_Pnd_Ws_Read_Counter = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Read_Counter", "#WS-READ-COUNTER", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Work_Area_Pnd_Ws_Read_Counter1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Read_Counter1", "#WS-READ-COUNTER1", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Work_Area_Pnd_Ws_Company = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Company", "#WS-COMPANY", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Filler = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Filler", "#WS-FILLER", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Trailer", "#WS-TRAILER", FieldType.STRING, 25);
        pnd_Work_Area_Pnd_Ws_Phone = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone", "#WS-PHONE", FieldType.STRING, 14);

        pnd_Work_Area__R_Field_1 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_1", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone);
        pnd_Work_Area_Pnd_Ws_Phone_F1 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F1", "#WS-PHONE-F1", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Ws_Area_Code = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Area_Code", "#WS-AREA-CODE", FieldType.NUMERIC, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F2 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F2", "#WS-PHONE-F2", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num3 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num3", "#WS-PHONE-NUM3", FieldType.STRING, 
            3);
        pnd_Work_Area_Pnd_Ws_Phone_F3 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_F3", "#WS-PHONE-F3", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Ws_Phone_Num4 = pnd_Work_Area__R_Field_1.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Num4", "#WS-PHONE-NUM4", FieldType.STRING, 
            4);
        pnd_Work_Area_Pnd_Ws_Phone_Numbera = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Numbera", "#WS-PHONE-NUMBERA", FieldType.STRING, 
            7);

        pnd_Work_Area__R_Field_2 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_2", "REDEFINE", pnd_Work_Area_Pnd_Ws_Phone_Numbera);
        pnd_Work_Area_Pnd_Ws_Phone_Number = pnd_Work_Area__R_Field_2.newFieldInGroup("pnd_Work_Area_Pnd_Ws_Phone_Number", "#WS-PHONE-NUMBER", FieldType.NUMERIC, 
            7);
        pnd_Temp_Cntl_Num = localVariables.newFieldInRecord("pnd_Temp_Cntl_Num", "#TEMP-CNTL-NUM", FieldType.NUMERIC, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl117c.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp117a() throws Exception
    {
        super("Twrp117a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  MAIN PROGRAM
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #TWRL117C
        while (condition(getWorkFiles().read(1, ldaTwrl117c.getPnd_Twrl117c())))
        {
            //*  EB (ADDED)
            getReports().write(2, ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1a(),NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1b(),NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1c(), //Natural: WRITE ( 2 ) #TWRL117A-1A / #TWRL117A-1B / #TWRL117A-1C / #TWRL117A-1D / #TWRL117A-1E / #TWRL117A-1F / #TWRL117A-1G / '******* END OF RECORD  *******' /
                NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1d(),NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1e(),NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1f(),
                NEWLINE,ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_1g(),NEWLINE,"******* END OF RECORD  *******",NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    RECORD LENGTH INCREASED FROM 700 TO 2000, BUT NOT ALL DISPLAYED.  RM
            //*    RECORD LENGTH INCREASED FROM 2000 TO 2500, BUT NOT ALL DISPLAYED. RM
            //*  DASDH1
            //*  DASDH1
            //*  DASDH1
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep().reset();                                                                                                //Natural: RESET #TWRL117A-C-AMT-PREP #TWRL117A-FILL18 #TWRL117A-DTE-RECEIVE-PNSN
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Fill18().reset();
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn().reset();
            //* *  #TWRL117A-A-EXEMPT
            //* *  #TWRL117A-B-TAXABLE
            //* *  #TWRL117A-D-AFT-TAX-CNTR
            //* *  #TWRL117A-E-TOTAL
            //* *  #TWRL117A-TAX-WTH-ELG-DIST
            //*  02/20/2007
            //*  VIKRAM
            if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rec_Type().equals("2") || ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117t_Form_Type().equals("R")))           //Natural: IF #TWRL117A-REC-TYPE = '2' OR #TWRL117T-FORM-TYPE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  012915 DUTTAD    /* RCC
            if (condition(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Rec_Id().equals("SU") || ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117h_Rec_Id().equals("PA")))              //Natural: IF #TWRL117H-REC-ID = 'SU' OR = 'PA'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Area_Pnd_Ws_Read_Counter.nadd(1);                                                                                                                    //Natural: ADD 1 TO #WS-READ-COUNTER
            pnd_Temp_Cntl_Num.compute(new ComputeParameters(false, pnd_Temp_Cntl_Num), ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number().val());                  //Natural: ASSIGN #TEMP-CNTL-NUM := VAL ( #TWRL117A-CONTROL-NUMBER )
            ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number().setValueEdited(pnd_Temp_Cntl_Num,new ReportEditMask("Z999999999"));                                 //Natural: MOVE EDITED #TEMP-CNTL-NUM ( EM = Z999999999 ) TO #TWRL117A-CONTROL-NUMBER
            //*   01/28/08   RM
            //*  480.7C DISASTER
            //*   01/28/08   RM
            //*  480.7C DISASTER
            //*  480.7C DISASTER
            //*   01/28/08   RM
            //*  480.7C DISASTER
            //*  02/04/2009
            //*  02/04/2009
            getReports().display(1, "SSN",                                                                                                                                //Natural: DISPLAY ( 1 ) 'SSN' #TWRL117A-PAYEE-SSN 'NAME   ' #TWRL117A-PAYEE-NAME / 'ADDRESS' #TWRL117A-PAYEE-ADDRESS-1 / ' ' #TWRL117A-PAYEE-ADDRESS-2 'TOWN' #TWRL117A-PAYEE-TOWN / 'STATE' #TWRL117A-PAYEE-STATE / 'ZIP' #TWRL117A-PAYEE-ZIP-FULL 'Distr Amt' #TWRL117A-DISTR-AMT / 'CONTROL NUMBER' #TWRL117A-CONTROL-NUMBER / 'D-Distr Amt' #TWRL117A-E-TOTAL 'TAXABLE AMT' #TWRL117A-TAXABLE-AMT / 'A-Exempt Amt' #TWRL117A-A-EXEMPT / 'B-Taxable Amt' #TWRL117A-B-TAXABLE 'IVC AMT' #TWRL117A-CONTR-AFT-TAX / 'D-IVC Amt' #TWRL117A-D-AFT-TAX-CNTR 'ROLL CONTRIB' #TWRL117A-ROLLOVER-CONTR / 'ROLL DISTRIB' #TWRL117A-ROLLOVER-DISTR
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn(),"NAME   ",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Name(),NEWLINE,"ADDRESS",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1(),NEWLINE," ",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2(),"TOWN",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Town(),NEWLINE,"STATE",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_State(),NEWLINE,"ZIP",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full(),"Distr Amt",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt(),NEWLINE,"CONTROL NUMBER",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number(),NEWLINE,"D-Distr Amt",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total(),"TAXABLE AMT",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt(),NEWLINE,"A-Exempt Amt",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt(),NEWLINE,"B-Taxable Amt",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable(),"IVC AMT",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax(),NEWLINE,"D-IVC Amt",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr(),"ROLL CONTRIB",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr(),NEWLINE,"ROLL DISTRIB",
            		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    'GROSS'           #TWRL117A-GROSS-PROCEEDS
            //*    'PENSION'         #TWRL117A-PPD-NO-WTHLDNG
            //*    'INTEREST'        #TWRL117A-INTERST
            //*   01/28/08   RM
            pnd_Work_Area_Pnd_Comp_Distr_Amt.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt());                                                                  //Natural: ADD #TWRL117A-DISTR-AMT TO #COMP-DISTR-AMT
            //*   01/28/08   RM
            pnd_Work_Area_Pnd_Comp_Taxable_Amt.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt());                                                              //Natural: ADD #TWRL117A-TAXABLE-AMT TO #COMP-TAXABLE-AMT
            //*   01/28/08   RM
            pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax());                                                          //Natural: ADD #TWRL117A-CONTR-AFT-TAX TO #COMP-CONTR-AFT-TAX
            //*  02/04/2009
            pnd_Work_Area_Pnd_Comp_Roll_Contr.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr());                                                            //Natural: ADD #TWRL117A-ROLLOVER-CONTR TO #COMP-ROLL-CONTR
            //*  02/04/2009
            pnd_Work_Area_Pnd_Comp_Roll_Distr.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr());                                                            //Natural: ADD #TWRL117A-ROLLOVER-DISTR TO #COMP-ROLL-DISTR
            //*  480.7C DISASTER STARTS
            pnd_Work_Area_Pnd_Comp_A_Exempt.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt());                                                                    //Natural: ADD #TWRL117A-A-EXEMPT TO #COMP-A-EXEMPT
            pnd_Work_Area_Pnd_Comp_B_Taxable.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable());                                                                  //Natural: ADD #TWRL117A-B-TAXABLE TO #COMP-B-TAXABLE
            pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr());                                                        //Natural: ADD #TWRL117A-D-AFT-TAX-CNTR TO #COMP-D-AFT-TAX-CNTR
            //*  480.7C DISASTER ENDS
            pnd_Work_Area_Pnd_Comp_E_Total.nadd(ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total());                                                                      //Natural: ADD #TWRL117A-E-TOTAL TO #COMP-E-TOTAL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM COMPANY-TOT
        sub_Company_Tot();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-TOT
        sub_Report_Tot();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR," ");                                                                                               //Natural: WRITE ( 1 ) NOTITLE NOHDR ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(131));                                                                                              //Natural: WRITE ( 1 ) '=' ( 131 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(52),"End of Report");                                                                                   //Natural: WRITE ( 1 ) 52T 'End of Report'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"=",new RepeatItem(131));                                                                                              //Natural: WRITE ( 1 ) '=' ( 131 )
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-TOT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TOT
    }
    //*   01/28/08   RM
    //*   01/28/08   RM
    //*   01/28/08   RM
    //*  02/04/2009
    //*  02/04/2009
    //*  480.7C DISASTER STARTS
    //*  480.7C DISASTER ENDS
    private void sub_Company_Tot() throws Exception                                                                                                                       //Natural: COMPANY-TOT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Company Total - Number of Records: ",pnd_Work_Area_Pnd_Ws_Read_Counter,new ColumnSpacing(15),pnd_Work_Area_Pnd_Comp_Distr_Amt,  //Natural: WRITE ( 1 ) // 'Company Total - Number of Records: ' #WS-READ-COUNTER 15X #COMP-DISTR-AMT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-TAXABLE-AMT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-CONTR-AFT-TAX ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-ROLL-CONTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-ROLL-DISTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-A-EXEMPT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-B-TAXABLE ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-D-AFT-TAX-CNTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #COMP-E-TOTAL ( EM = ZZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_Taxable_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_Roll_Contr, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_Roll_Distr, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_A_Exempt, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_B_Taxable, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Comp_E_Total, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*   01/28/08   RM
        pnd_Work_Area_Pnd_Tot_Distr_Amt.nadd(pnd_Work_Area_Pnd_Comp_Distr_Amt);                                                                                           //Natural: ADD #COMP-DISTR-AMT TO #TOT-DISTR-AMT
        //*   01/28/08   RM
        pnd_Work_Area_Pnd_Tot_Taxable_Amt.nadd(pnd_Work_Area_Pnd_Comp_Taxable_Amt);                                                                                       //Natural: ADD #COMP-TAXABLE-AMT TO #TOT-TAXABLE-AMT
        //*   01/28/08   RM
        pnd_Work_Area_Pnd_Tot_Contr_Aft_Tax.nadd(pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax);                                                                                   //Natural: ADD #COMP-CONTR-AFT-TAX TO #TOT-CONTR-AFT-TAX
        //*  02/04/2009
        pnd_Work_Area_Pnd_Tot_Roll_Contr.nadd(pnd_Work_Area_Pnd_Comp_Roll_Contr);                                                                                         //Natural: ADD #COMP-ROLL-CONTR TO #TOT-ROLL-CONTR
        //*  02/04/2009
        pnd_Work_Area_Pnd_Tot_Roll_Distr.nadd(pnd_Work_Area_Pnd_Comp_Roll_Distr);                                                                                         //Natural: ADD #COMP-ROLL-DISTR TO #TOT-ROLL-DISTR
        pnd_Work_Area_Pnd_Tot_Read.nadd(pnd_Work_Area_Pnd_Ws_Read_Counter);                                                                                               //Natural: ADD #WS-READ-COUNTER TO #TOT-READ
        //*  480.7C DISASTER STARTS
        pnd_Work_Area_Pnd_Tot_A_Exempt.nadd(pnd_Work_Area_Pnd_Comp_A_Exempt);                                                                                             //Natural: ADD #COMP-A-EXEMPT TO #TOT-A-EXEMPT
        pnd_Work_Area_Pnd_Tot_B_Taxable.nadd(pnd_Work_Area_Pnd_Comp_B_Taxable);                                                                                           //Natural: ADD #COMP-B-TAXABLE TO #TOT-B-TAXABLE
        pnd_Work_Area_Pnd_Tot_D_Aft_Tax_Cntr.nadd(pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr);                                                                                 //Natural: ADD #COMP-D-AFT-TAX-CNTR TO #TOT-D-AFT-TAX-CNTR
        //*  480.7C DISASTER ENDS
        pnd_Work_Area_Pnd_Tot_E_Total.nadd(pnd_Work_Area_Pnd_Comp_E_Total);                                                                                               //Natural: ADD #COMP-E-TOTAL TO #TOT-E-TOTAL
        //*   01/28/08   RM
        //*   01/28/08   RM
        //*   01/28/08   RM
        //*  02/04/2009
        //*  02/04/2009
        //*  480.7C DISASTER STARTS
        //*  480.7C DISASTER ENDS
        pnd_Work_Area_Pnd_Comp_Distr_Amt.reset();                                                                                                                         //Natural: RESET #COMP-DISTR-AMT #COMP-TAXABLE-AMT #COMP-CONTR-AFT-TAX #COMP-ROLL-CONTR #COMP-ROLL-DISTR #WS-READ-COUNTER #COMP-A-EXEMPT #COMP-B-TAXABLE #COMP-D-AFT-TAX-CNTR #COMP-E-TOTAL
        pnd_Work_Area_Pnd_Comp_Taxable_Amt.reset();
        pnd_Work_Area_Pnd_Comp_Contr_Aft_Tax.reset();
        pnd_Work_Area_Pnd_Comp_Roll_Contr.reset();
        pnd_Work_Area_Pnd_Comp_Roll_Distr.reset();
        pnd_Work_Area_Pnd_Ws_Read_Counter.reset();
        pnd_Work_Area_Pnd_Comp_A_Exempt.reset();
        pnd_Work_Area_Pnd_Comp_B_Taxable.reset();
        pnd_Work_Area_Pnd_Comp_D_Aft_Tax_Cntr.reset();
        pnd_Work_Area_Pnd_Comp_E_Total.reset();
    }
    //*   01/28/08   RM
    //*   01/28/08   RM
    //*   01/28/08   RM
    //*  02/04/2009
    //*  02/04/2009
    //*  480.7C DISASTER STARTS
    //*  480.7C DISASTER ENDS
    private void sub_Report_Tot() throws Exception                                                                                                                        //Natural: REPORT-TOT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Report Total  - Number of Records: ",pnd_Work_Area_Pnd_Tot_Read,new ColumnSpacing(15),pnd_Work_Area_Pnd_Tot_Distr_Amt,  //Natural: WRITE ( 1 ) // 'Report Total  - Number of Records: ' #TOT-READ 15X #TOT-DISTR-AMT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-TAXABLE-AMT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-CONTR-AFT-TAX ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-ROLL-CONTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-ROLL-DISTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-A-EXEMPT ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-B-TAXABLE ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-D-AFT-TAX-CNTR ( EM = ZZZZ,ZZZ,ZZZ.99 ) #TOT-E-TOTAL ( EM = ZZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_Taxable_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_Contr_Aft_Tax, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_Roll_Contr, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_Roll_Distr, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_A_Exempt, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_B_Taxable, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_D_Aft_Tax_Cntr, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),pnd_Work_Area_Pnd_Tot_E_Total, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  01/28/08 RM
                    //*  01/28/08 RM
                    //*  01/28/08 RM
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"PGM:",Global.getPROGRAM(),new TabSetting(33),"TIAA-CREF Tax Withholding and Reporting System",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'PGM:' *PROGRAM 33T 'TIAA-CREF Tax Withholding and Reporting System' 110T 'Date:' *DATU / 'Page:' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) 43T 'PUERTO RICO Detail Report' 110T 'Time:' *TIMX / 43T '-------------------------'
                        TabSetting(110),"Date:",Global.getDATU(),NEWLINE,"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),new TabSetting(43),"PUERTO RICO Detail Report",new 
                        TabSetting(110),"Time:",Global.getTIMX(),NEWLINE,new TabSetting(43),"-------------------------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, "....+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8....+....9....+....100");                      //Natural: WRITE ( 2 ) '....+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8....+....9....+....100'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "SSN",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn(),"NAME   ",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Name(),NEWLINE,"ADDRESS",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1(),NEWLINE," ",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2(),"TOWN",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Town(),NEWLINE,"STATE",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_State(),NEWLINE,"ZIP",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full(),"Distr Amt",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt(),NEWLINE,"CONTROL NUMBER",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Control_Number(),NEWLINE,"D-Distr Amt",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_E_Total(),"TAXABLE AMT",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt(),NEWLINE,"A-Exempt Amt",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt(),NEWLINE,"B-Taxable Amt",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable(),"IVC AMT",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax(),NEWLINE,"D-IVC Amt",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr(),"ROLL CONTRIB",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr(),NEWLINE,"ROLL DISTRIB",
        		ldaTwrl117c.getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr());
    }
}
