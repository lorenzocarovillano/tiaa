/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:27 PM
**        * FROM NATURAL PROGRAM : Txwp5050
************************************************************
**        * FILE NAME            : Txwp5050.java
**        * CLASS NAME           : Txwp5050
**        * INSTANCE NAME        : Txwp5050
************************************************************
************************************************************************
* PROGRAM   : TXWP5050
* FUNCTION  : UPDATE LAST USER/DATES WHEN CPS FINISH PROCESS OF.....
* PROGRAMMER: ENRIQUE BOTTERI - 09/25/2001
* DESC      : THIS PROGRAM RUNS ON BATCH AS PART OF P1280TWM. IT TAKES
*             INPUT FROM SELECTED RECORDS IN PIA.ANN.IAADMIN.GTN.S461(0)
*             THAT HOLDS PROCESS-IND = 'F' (SELECTED BY SORT=COPY STEP).
*             UPDATE NEW AP ELECTIONS TO INDICATE THEY HAVE BEEN USED.
* HISTORY...: MARINA NACHBER 5/2002
*             ADDED WRITING DEFAULT ELECTIONS
*             MARINA NACHBER 6/2002
*             CHANGED 'get isn' TO 'read by isn' IN CONJUNCTION WITH THE
*             NEW 'DELETE ELECTIONS' PROCESS
*             (BOTH CHANGES ARE MOVED TO PRODUCTION AT THE SAME
*              TIME)
*
*             MARINA NACHBER 10/2002
*             MODIFIED TO NOT CREATE DEFAULT ELECTIONS IF PYMNT-SUSPEND-
*             CODE (FCPA800) IS POPULATED.
* 05/12/08 - ROSE MA - ROTH 401K/403B PROJECT
*                      RECOMPILED DUE TO UPDATED FCPA800 & FCPA802
* 12/13/11  R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPA800
* 01/31/12  R.SACHARNY RECOMPILE DUE SUNY/CUNY CHANGES
* 08/25/15  FENDAYA COR/NAS SUNSET. FE201608
* 04/19/17  J BREMER PIN EXP STOW FOR TXWA0270
* 04/22/19  ARIVU    ADDED MQ OPEN & CLOSE TO IMPROVE JOB PERFORMANCE
* 08/16/19  ARIVU    ADDED THE CHANGES FOR TAX UD
* 11/15/19  ARIVU    FIX FOR STOP TO CREATE STATE DEFAULT ELECTIONS
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp5050 extends BLNatBase
{
    // Data Areas
    private LdaTxwl5050 ldaTxwl5050;
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private PdaTxwa0270 pdaTxwa0270;
    private PdaTxwapart pdaTxwapart;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_J;
    private DbsField pnd_J_Et;
    private DbsField pnd_I;
    private DbsField pnd_I_Fed;
    private DbsField pnd_I_Sta;
    private DbsField pnd_I_Loc;
    private DbsField pnd_Errors;
    private DbsField pnd_Msg;
    private DbsField pnd_E_Isn;
    private DbsField pnd_E_Update_Flag;
    private DbsField pnd_Idx;

    private DbsGroup work_Fields;
    private DbsField work_Fields_Pnd_Election_Type;
    private DbsField work_Fields_Pnd_Written_Fed_Recs;
    private DbsField work_Fields_Pnd_Written_Sta_Recs;
    private DbsField work_Fields_Pnd_Records_Not_Found;
    private DbsField work_Fields_Pnd_Bypassed_Recs;
    private DbsField work_Fields_Pnd_Tot_Default_Written;
    private DbsField work_Fields_Pnd_D_Errors;
    private DbsField work_Fields_Pnd_Message;
    private DbsField work_Fields_Pnd_Pend_Message;

    private DbsGroup work_Fields__R_Field_1;
    private DbsField work_Fields_Pnd_Pend_Text;
    private DbsField work_Fields_Pnd_Pend_Code;
    private DbsField work_Fields_Pnd_Tin_Num;

    private DbsGroup work_Fields__R_Field_2;
    private DbsField work_Fields_Pnd_Contract_Num;
    private DbsField work_Fields_Pnd_Payee_Code;
    private DbsField work_Fields_Pnd_Save_Timx;
    private DbsField work_Fields_Pnd_Save_Datx;
    private DbsField work_Fields_Pnd_Date_D;
    private DbsField work_Fields_Pnd_Date_A;

    private DbsGroup work_Fields__R_Field_3;
    private DbsField work_Fields_Pnd_Date_N;
    private DbsField work_Fields_Pnd_Pay_Date_Time;

    private DbsGroup work_Fields__R_Field_4;
    private DbsField work_Fields_Pnd_Pay_Date;
    private DbsField work_Fields_Pnd_Pay_Time;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTxwl5050 = new LdaTxwl5050();
        registerRecord(ldaTxwl5050);
        registerRecord(ldaTxwl5050.getVw_txwl5050());
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        pdaTxwa0270 = new PdaTxwa0270(localVariables);
        pdaTxwapart = new PdaTxwapart(localVariables);

        // Local Variables
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 7);
        pnd_J_Et = localVariables.newFieldInRecord("pnd_J_Et", "#J-ET", FieldType.PACKED_DECIMAL, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_I_Fed = localVariables.newFieldInRecord("pnd_I_Fed", "#I-FED", FieldType.PACKED_DECIMAL, 7);
        pnd_I_Sta = localVariables.newFieldInRecord("pnd_I_Sta", "#I-STA", FieldType.PACKED_DECIMAL, 7);
        pnd_I_Loc = localVariables.newFieldInRecord("pnd_I_Loc", "#I-LOC", FieldType.PACKED_DECIMAL, 7);
        pnd_Errors = localVariables.newFieldInRecord("pnd_Errors", "#ERRORS", FieldType.PACKED_DECIMAL, 7);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 50);
        pnd_E_Isn = localVariables.newFieldInRecord("pnd_E_Isn", "#E-ISN", FieldType.PACKED_DECIMAL, 13);
        pnd_E_Update_Flag = localVariables.newFieldInRecord("pnd_E_Update_Flag", "#E-UPDATE-FLAG", FieldType.BOOLEAN, 1);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);

        work_Fields = localVariables.newGroupInRecord("work_Fields", "WORK-FIELDS");
        work_Fields_Pnd_Election_Type = work_Fields.newFieldInGroup("work_Fields_Pnd_Election_Type", "#ELECTION-TYPE", FieldType.STRING, 1);
        work_Fields_Pnd_Written_Fed_Recs = work_Fields.newFieldInGroup("work_Fields_Pnd_Written_Fed_Recs", "#WRITTEN-FED-RECS", FieldType.PACKED_DECIMAL, 
            7);
        work_Fields_Pnd_Written_Sta_Recs = work_Fields.newFieldInGroup("work_Fields_Pnd_Written_Sta_Recs", "#WRITTEN-STA-RECS", FieldType.PACKED_DECIMAL, 
            7);
        work_Fields_Pnd_Records_Not_Found = work_Fields.newFieldInGroup("work_Fields_Pnd_Records_Not_Found", "#RECORDS-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            7);
        work_Fields_Pnd_Bypassed_Recs = work_Fields.newFieldInGroup("work_Fields_Pnd_Bypassed_Recs", "#BYPASSED-RECS", FieldType.PACKED_DECIMAL, 7);
        work_Fields_Pnd_Tot_Default_Written = work_Fields.newFieldInGroup("work_Fields_Pnd_Tot_Default_Written", "#TOT-DEFAULT-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        work_Fields_Pnd_D_Errors = work_Fields.newFieldInGroup("work_Fields_Pnd_D_Errors", "#D-ERRORS", FieldType.PACKED_DECIMAL, 7);
        work_Fields_Pnd_Message = work_Fields.newFieldInGroup("work_Fields_Pnd_Message", "#MESSAGE", FieldType.STRING, 14);
        work_Fields_Pnd_Pend_Message = work_Fields.newFieldInGroup("work_Fields_Pnd_Pend_Message", "#PEND-MESSAGE", FieldType.STRING, 14);

        work_Fields__R_Field_1 = work_Fields.newGroupInGroup("work_Fields__R_Field_1", "REDEFINE", work_Fields_Pnd_Pend_Message);
        work_Fields_Pnd_Pend_Text = work_Fields__R_Field_1.newFieldInGroup("work_Fields_Pnd_Pend_Text", "#PEND-TEXT", FieldType.STRING, 13);
        work_Fields_Pnd_Pend_Code = work_Fields__R_Field_1.newFieldInGroup("work_Fields_Pnd_Pend_Code", "#PEND-CODE", FieldType.STRING, 1);
        work_Fields_Pnd_Tin_Num = work_Fields.newFieldInGroup("work_Fields_Pnd_Tin_Num", "#TIN-NUM", FieldType.STRING, 10);

        work_Fields__R_Field_2 = work_Fields.newGroupInGroup("work_Fields__R_Field_2", "REDEFINE", work_Fields_Pnd_Tin_Num);
        work_Fields_Pnd_Contract_Num = work_Fields__R_Field_2.newFieldInGroup("work_Fields_Pnd_Contract_Num", "#CONTRACT-NUM", FieldType.STRING, 8);
        work_Fields_Pnd_Payee_Code = work_Fields__R_Field_2.newFieldInGroup("work_Fields_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.STRING, 2);
        work_Fields_Pnd_Save_Timx = work_Fields.newFieldInGroup("work_Fields_Pnd_Save_Timx", "#SAVE-TIMX", FieldType.TIME);
        work_Fields_Pnd_Save_Datx = work_Fields.newFieldInGroup("work_Fields_Pnd_Save_Datx", "#SAVE-DATX", FieldType.DATE);
        work_Fields_Pnd_Date_D = work_Fields.newFieldInGroup("work_Fields_Pnd_Date_D", "#DATE-D", FieldType.DATE);
        work_Fields_Pnd_Date_A = work_Fields.newFieldInGroup("work_Fields_Pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        work_Fields__R_Field_3 = work_Fields.newGroupInGroup("work_Fields__R_Field_3", "REDEFINE", work_Fields_Pnd_Date_A);
        work_Fields_Pnd_Date_N = work_Fields__R_Field_3.newFieldInGroup("work_Fields_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        work_Fields_Pnd_Pay_Date_Time = work_Fields.newFieldInGroup("work_Fields_Pnd_Pay_Date_Time", "#PAY-DATE-TIME", FieldType.STRING, 15);

        work_Fields__R_Field_4 = work_Fields.newGroupInGroup("work_Fields__R_Field_4", "REDEFINE", work_Fields_Pnd_Pay_Date_Time);
        work_Fields_Pnd_Pay_Date = work_Fields__R_Field_4.newFieldInGroup("work_Fields_Pnd_Pay_Date", "#PAY-DATE", FieldType.STRING, 8);
        work_Fields_Pnd_Pay_Time = work_Fields__R_Field_4.newFieldInGroup("work_Fields_Pnd_Pay_Time", "#PAY-TIME", FieldType.STRING, 7);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTxwl5050.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp5050() throws Exception
    {
        super("Txwp5050");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ********************************************************************
        //*  M A I N   P R O G R A M
        //* ********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON
        work_Fields_Pnd_Pend_Text.setValue("Pend code: ");                                                                                                                //Natural: ASSIGN #PEND-TEXT := 'Pend code: '
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 0 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 53T 'TIAA CREF Tax System' ( I ) 118T 'Page  :' *PAGE-NUMBER ( 0 ) ( EM = Z,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Updates for USED new AP Elections' 118T 'Report:  RPT0' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ** MARINA 05/2002
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 53T 'TIAA CREF Tax System' ( I ) 118T 'Page  :' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'New Default AP Elections' 118T 'Report:  RPT1' //
        work_Fields_Pnd_Save_Timx.setValue(Global.getTIMX());                                                                                                             //Natural: ASSIGN #SAVE-TIMX := *TIMX
        work_Fields_Pnd_Save_Datx.setValue(Global.getDATX());                                                                                                             //Natural: ASSIGN #SAVE-DATX := *DATX
        //* ** END    05/2002
                                                                                                                                                                          //Natural: PERFORM PROCESS-FILE
        sub_Process_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_I.equals(getZero())))                                                                                                                           //Natural: IF #I EQ 0
        {
            getReports().write(0, NEWLINE,NEWLINE,NEWLINE,new TabSetting(15),"No records found to be processed.");                                                        //Natural: WRITE ( 0 ) /// 15T 'No records found to be processed.'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, NEWLINE,NEWLINE,"Total records read in Work File:",pnd_I, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"Processed records:",NEWLINE,"   Federal.......:",pnd_I_Fed,  //Natural: WRITE ( 0 ) // 'Total records read in Work File:' #I ( EM = ZZ,ZZZ,ZZ9 ) / 'Processed records:' / '   Federal.......:' #I-FED ( EM = Z,ZZZ,ZZ9 ) / '   State.........:' #I-STA ( EM = Z,ZZZ,ZZ9 ) / '   Local.........:' #I-LOC ( EM = Z,ZZZ,ZZ9 ) // 'Total number of records not found:' #RECORDS-NOT-FOUND ( EM = ZZ,ZZZ,ZZ9 ) // 'Total number of records updated:' #J ( EM = ZZ,ZZZ,ZZ9 ) // 'Total number of records w/error:' #ERRORS ( EM = ZZ,ZZZ,ZZ9 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"   State.........:",pnd_I_Sta, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"   Local.........:",pnd_I_Loc, 
                new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of records not found:",work_Fields_Pnd_Records_Not_Found, new ReportEditMask 
                ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of records updated:",pnd_J, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of records w/error:",pnd_Errors, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            //* ** MARINA 06/2002
            //* ** END    06/2002
        }                                                                                                                                                                 //Natural: END-IF
        //* ** MARINA 05/2002
        if (condition(work_Fields_Pnd_Tot_Default_Written.equals(getZero())))                                                                                             //Natural: IF #TOT-DEFAULT-WRITTEN EQ 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(15),"No Default elections created.");                                                            //Natural: WRITE ( 1 ) /// 15T 'No Default elections created.'
            if (Global.isEscape()) return;
            //*  10/2002
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,"Total records read in Work File:",pnd_I, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"Processed records to create Default elections:",NEWLINE,"   Federal.......:",work_Fields_Pnd_Written_Fed_Recs,  //Natural: WRITE ( 1 ) // 'Total records read in Work File:' #I ( EM = ZZ,ZZZ,ZZ9 ) / 'Processed records to create Default elections:' / '   Federal.......:' #WRITTEN-FED-RECS ( EM = Z,ZZZ,ZZ9 ) / '   State.........:' #WRITTEN-STA-RECS ( EM = Z,ZZZ,ZZ9 ) // 'Total number of Defaults created:' #TOT-DEFAULT-WRITTEN ( EM = ZZ,ZZZ,ZZ9 ) // 'Total number of records w/error:' #D-ERRORS ( EM = ZZ,ZZZ,ZZ9 ) // 'Total number of records bypassed:' #BYPASSED-RECS ( EM = ZZ,ZZZ,ZZ9 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"   State.........:",work_Fields_Pnd_Written_Sta_Recs, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of Defaults created:",work_Fields_Pnd_Tot_Default_Written, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of records w/error:",work_Fields_Pnd_D_Errors, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total number of records bypassed:",work_Fields_Pnd_Bypassed_Recs, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ** END    05/2002
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FILE
        //* ** TEST   05/2002
        //*  IF  #I > 300
        //*     ESCAPE ROUTINE
        //*  END-IF
        //* ** TEST END 05/2002
        //* ** MARINA 06/2002
        //*        #ISN := WF-PYMNT-TAX-GRP.GTN-FED-ELC-ISN(#IDX)
        //*        ADD 1 TO #I-FED
        //* ** END    06/2002
        //* ** MARINA 06/2002
        //*        #ISN := WF-PYMNT-TAX-GRP.GTN-STA-ELC-ISN(#IDX)
        //*        ADD 1 TO #I-STA
        //* ** END    06/2002
        //* ** MARINA 06/2002
        //*        #ISN := WF-PYMNT-TAX-GRP.GTN-LOC-ELC-ISN(#IDX)
        //*        ADD 1 TO #I-LOC
        //* ** END    06/2002
        //* ** MARINA 05/2002
        //*        RESET #TXWA0270.#TAX-INPUT-DATA
        //*        #TXWA0270.FED-PROCESS-IND := 'D'
        //*        #TXWA0270.ELC-TYPE        := 'F'
        //*        PERFORM WRITE-ELECTION
        //*        IF #TXWA0270.#RETURN-CODE
        //*        ELSE
        //*           ESCAPE BOTTOM
        //*        END-IF
        //*        RESET #TXWA0270.#TAX-INPUT-DATA
        //*        #TXWA0270.ST-PROCESS-IND  := 'D'
        //*        #TXWA0270.ELC-TYPE        := 'S'
        //*        PERFORM WRITE-ELECTION
        //*        IF #TXWA0270.#RETURN-CODE
        //*        ELSE
        //*            ESCAPE BOTTOM
        //*         END-IF
        //* **  END   05/2002
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RCRD
        //* ** END    06/2002
        //* *
        //*    '/Counter'     #J (EM=ZZ,Z99)
        //*    '/ISN'         #E-ISN
        //*   MARINA    06/2002
        //*   END
        //* ** MARINA 05/2002
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ELECTION
        //* *******************************
        //*  END    10/2002
        //*   #TXWA0270.RES-TYPE      := '1'
        //*   CALLING THE PARTICIPANT ROUTINE TO DETERMINE RES-TYPE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  END    10/2002
        //* ** END    05/2002
        //* *------------
    }
    private void sub_Process_File() throws Exception                                                                                                                      //Natural: PROCESS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 PYMNT-ADDR-INFO WF-PYMNT-TAX-REC WF-PYMNT-ADDR-GRP.PLAN-NUMBER WF-PYMNT-ADDR-GRP.SUB-PLAN WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan())))
        {
            //*     WF-PYMNT-ADDR-GRP.INV-INFO(*)
            pnd_E_Update_Flag.reset();                                                                                                                                    //Natural: RESET #E-UPDATE-FLAG
            //*  IN MOST CASES DATA WILL BE ON FIRST INSTANCE
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            FOR01:                                                                                                                                                        //Natural: FOR #IDX 1 TO 2
            for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(2)); pnd_Idx.nadd(1))
            {
                short decideConditionsMet1125 = 0;                                                                                                                        //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN WF-PYMNT-TAX-GRP.GTN-FED-PROCESS-IND ( #IDX ) = 'F'
                if (condition(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Process_Ind().getValue(pnd_Idx).equals("F")))
                {
                    decideConditionsMet1125++;
                    pnd_E_Isn.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Elc_Isn().getValue(pnd_Idx));                                                               //Natural: ASSIGN #E-ISN := WF-PYMNT-TAX-GRP.GTN-FED-ELC-ISN ( #IDX )
                    work_Fields_Pnd_Election_Type.setValue("F");                                                                                                          //Natural: ASSIGN #ELECTION-TYPE := 'F'
                    //*  ORIGINAL CODE
                                                                                                                                                                          //Natural: PERFORM UPDATE-RCRD
                    sub_Update_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN WF-PYMNT-TAX-GRP.GTN-STA-PROCESS-IND ( #IDX ) = 'F'
                if (condition(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Sta_Process_Ind().getValue(pnd_Idx).equals("F")))
                {
                    decideConditionsMet1125++;
                    pnd_E_Isn.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Sta_Elc_Isn().getValue(pnd_Idx));                                                               //Natural: ASSIGN #E-ISN := WF-PYMNT-TAX-GRP.GTN-STA-ELC-ISN ( #IDX )
                    work_Fields_Pnd_Election_Type.setValue("S");                                                                                                          //Natural: ASSIGN #ELECTION-TYPE := 'S'
                    //*  ORIGINAL CODE
                                                                                                                                                                          //Natural: PERFORM UPDATE-RCRD
                    sub_Update_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN WF-PYMNT-TAX-GRP.GTN-LOC-PROCESS-IND ( #IDX ) = 'F'
                if (condition(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Loc_Process_Ind().getValue(pnd_Idx).equals("F")))
                {
                    decideConditionsMet1125++;
                    pnd_E_Isn.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Loc_Elc_Isn().getValue(pnd_Idx));                                                               //Natural: ASSIGN #E-ISN := WF-PYMNT-TAX-GRP.GTN-LOC-ELC-ISN ( #IDX )
                    work_Fields_Pnd_Election_Type.setValue("L");                                                                                                          //Natural: ASSIGN #ELECTION-TYPE := 'L'
                    //*  ORIGINAL CODE
                                                                                                                                                                          //Natural: PERFORM UPDATE-RCRD
                    sub_Update_Rcrd();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN WF-PYMNT-TAX-GRP.GTN-FED-PROCESS-IND ( #IDX ) = 'D'
                if (condition(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Process_Ind().getValue(pnd_Idx).equals("D")))
                {
                    decideConditionsMet1125++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN WF-PYMNT-TAX-GRP.GTN-STA-PROCESS-IND ( #IDX ) = 'D'
                if (condition(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Sta_Process_Ind().getValue(pnd_Idx).equals("D")))
                {
                    decideConditionsMet1125++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet1125 > 0))
                {
                    pnd_E_Update_Flag.setValue(true);                                                                                                                     //Natural: ASSIGN #E-UPDATE-FLAG := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet1125 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ** MARINA 05/2002
            //*  IF NOT #E-UPDATE-FLAG
            //*    ESCAPE BOTTOM
            //*  END-IF
            //* ** END    05/2002
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_J_Et.greater(getZero())))                                                                                                                       //Natural: IF #J-ET GT 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_J_Et.reset();                                                                                                                                             //Natural: RESET #J-ET
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Rcrd() throws Exception                                                                                                                       //Natural: UPDATE-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaTxwl5050.getVw_txwl5050().reset();                                                                                                                             //Natural: RESET TXWL5050
        //*   MARINA    06/2002
        //*   CHANGES RELATED TO DELETE OPTION
        //*  GET TXWL5050 #E-ISN
        ldaTxwl5050.getVw_txwl5050().startDatabaseRead                                                                                                                    //Natural: READ TXWL5050 BY ISN = #E-ISN THRU #E-ISN
        (
        "RE",
        new Wc[] { new Wc("ISN", ">=", pnd_E_Isn, "And", WcType.BY) ,
        new Wc("ISN", "<=", pnd_E_Isn, WcType.BY) },
        new Oc[] { new Oc("ISN", "ASC") }
        );
        RE:
        while (condition(ldaTxwl5050.getVw_txwl5050().readNextRow("RE")))
        {
            //* ** END      06/2002
            //*  TXWL5050.LU-USER := *INIT-USER        /* CHANGES FOR TAX UD AUDIT
            //*   MARINA    06/2002
            //*  TXWL5050.USE-DTE := *DATX
            work_Fields_Pnd_Date_A.setValueEdited(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Tax_Calc_Timestamp().getValue(1),new ReportEditMask("YYYYMMDD"));                    //Natural: MOVE EDITED GTN-TAX-CALC-TIMESTAMP ( 1 ) ( EM = YYYYMMDD ) TO #DATE-A
            work_Fields_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),work_Fields_Pnd_Date_A);                                                                 //Natural: MOVE EDITED #DATE-A TO #DATE-D ( EM = YYYYMMDD )
            ldaTxwl5050.getTxwl5050_Use_Dte().setValue(work_Fields_Pnd_Date_D);                                                                                           //Natural: ASSIGN TXWL5050.USE-DTE := #DATE-D
            //*   END       06/2002
            //*  TXWL5050.LU-TS   := *TIMX        /* CHANGES FOR TAX UD AUDIT
            ldaTxwl5050.getVw_txwl5050().updateDBRow("RE");                                                                                                               //Natural: UPDATE
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            pnd_J_Et.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #J-ET
            //*   MARINA    06/2002
            if (condition(work_Fields_Pnd_Election_Type.equals("F")))                                                                                                     //Natural: IF #ELECTION-TYPE = 'F'
            {
                pnd_I_Fed.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I-FED
            }                                                                                                                                                             //Natural: END-IF
            if (condition(work_Fields_Pnd_Election_Type.equals("S")))                                                                                                     //Natural: IF #ELECTION-TYPE = 'S'
            {
                pnd_I_Sta.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I-STA
            }                                                                                                                                                             //Natural: END-IF
            if (condition(work_Fields_Pnd_Election_Type.equals("L")))                                                                                                     //Natural: IF #ELECTION-TYPE = 'L'
            {
                pnd_I_Loc.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I-LOC
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(0, new  ReportMatrixColumnSpacing(4),"/TIN",                                                                                             //Natural: DISPLAY ( 0 ) ( SF = 4 ) '/TIN' TXWL5050.TIN 'TIN/Type' TXWL5050.TIN-TYPE '/Contract' TXWL5050.CONTRACT-NBR 'Payee/Code' TXWL5050.PAYEE-CDE 'Active/Ind' TXWL5050.ACTIVE-IND 'Elct/Code' TXWL5050.ELC-CDE 'Elct/Type' TXWL5050.ELC-TYPE 'Elct/Rscdcy' TXWL5050.ELC-RES 'Used/Date' TXWL5050.USE-DTE 'Effective/TS' TXWL5050.EFFECTIVE-TS ( EM = YYYY/MM/DD�HH:II:SS.T )
            		ldaTxwl5050.getTxwl5050_Tin(),"TIN/Type",
            		ldaTxwl5050.getTxwl5050_Tin_Type(),"/Contract",
            		ldaTxwl5050.getTxwl5050_Contract_Nbr(),"Payee/Code",
            		ldaTxwl5050.getTxwl5050_Payee_Cde(),"Active/Ind",
            		ldaTxwl5050.getTxwl5050_Active_Ind(),"Elct/Code",
            		ldaTxwl5050.getTxwl5050_Elc_Cde(),"Elct/Type",
            		ldaTxwl5050.getTxwl5050_Elc_Type(),"Elct/Rscdcy",
            		ldaTxwl5050.getTxwl5050_Elc_Res(),"Used/Date",
            		ldaTxwl5050.getTxwl5050_Use_Dte(),"Effective/TS",
            		ldaTxwl5050.getTxwl5050_Effective_Ts(), new ReportEditMask ("YYYY/MM/DD HH:II:SS.T"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RE"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RE"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_J_Et.greater(100)))                                                                                                                         //Natural: IF #J-ET GT 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_J_Et.reset();                                                                                                                                         //Natural: RESET #J-ET
            }                                                                                                                                                             //Natural: END-IF
            //*   MARINA    06/2002
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        work_Fields_Pnd_Election_Type.reset();                                                                                                                            //Natural: RESET #ELECTION-TYPE
        if (condition(ldaTxwl5050.getVw_txwl5050().getAstCOUNTER().equals(getZero())))                                                                                    //Natural: IF *COUNTER ( RE. ) = 0
        {
            work_Fields_Pnd_Records_Not_Found.nadd(1);                                                                                                                    //Natural: ADD 1 TO #RECORDS-NOT-FOUND
            getReports().write(0, "                                ");                                                                                                    //Natural: WRITE ( 0 ) '                                '
            if (Global.isEscape()) return;
            getReports().write(0, "Election was not found to update USE-DATE for TIN ",pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr()," CONTRACT: ",                  //Natural: WRITE ( 0 ) 'Election was not found to update USE-DATE for TIN ' ANNT-SOC-SEC-NBR ' CONTRACT: ' CNTRCT-PPCN-NBR ' PAYEE: 'CNTRCT-PAYEE-CDE ' ISN:'#E-ISN
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr()," PAYEE: ",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde()," ISN:",pnd_E_Isn);
            if (Global.isEscape()) return;
            getReports().write(0, "                                ");                                                                                                    //Natural: WRITE ( 0 ) '                                '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   END       06/2002
    }
    private void sub_Write_Election() throws Exception                                                                                                                    //Natural: WRITE-ELECTION
    {
        if (BLNatReinput.isReinput()) return;

        pdaTxwa0270.getPnd_Txwa0270_Call_Sys().setValue("AP");                                                                                                            //Natural: ASSIGN #TXWA0270.CALL-SYS := 'AP'
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr().equals(getZero()) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr().equals(999999999)))      //Natural: IF ANNT-SOC-SEC-NBR = 0 OR = 999999999
        {
            work_Fields_Pnd_Contract_Num.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXXX"));                                //Natural: MOVE EDITED CNTRCT-PPCN-NBR ( EM = XXXXXXXX ) TO #CONTRACT-NUM
            work_Fields_Pnd_Payee_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde().getSubstring(1,2));                                                    //Natural: ASSIGN #PAYEE-CODE := SUBSTR ( CNTRCT-PAYEE-CDE,1,2 )
            pdaTxwa0270.getPnd_Txwa0270_Tin().setValue(work_Fields_Pnd_Tin_Num);                                                                                          //Natural: ASSIGN #TXWA0270.TIN := #TIN-NUM
            pdaTxwa0270.getPnd_Txwa0270_Tin_Type().setValue("5");                                                                                                         //Natural: ASSIGN #TXWA0270.TIN-TYPE := '5'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTxwa0270.getPnd_Txwa0270_Tin().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),new ReportEditMask("999999999"));                         //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 999999999 ) TO #TXWA0270.TIN
            pdaTxwa0270.getPnd_Txwa0270_Tin_Type().setValue("1");                                                                                                         //Natural: ASSIGN #TXWA0270.TIN-TYPE := '1'
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwa0270.getPnd_Txwa0270_Contract_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                           //Natural: ASSIGN #TXWA0270.CONTRACT-NBR := CNTRCT-PPCN-NBR
        pdaTxwa0270.getPnd_Txwa0270_Payee_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde().getSubstring(1,2));                                           //Natural: ASSIGN #TXWA0270.PAYEE-CDE := SUBSTR ( CNTRCT-PAYEE-CDE,1,2 )
        //*   POPULATE EFFECTIVE DATE (T) WITH PYMNT-CHECK-DTE (D) + 0000001
        work_Fields_Pnd_Pay_Date.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #PAY-DATE
        work_Fields_Pnd_Pay_Time.setValue("0000001");                                                                                                                     //Natural: ASSIGN #PAY-TIME := '0000001'
        pdaTxwa0270.getPnd_Txwa0270_Effective_Ts().setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),work_Fields_Pnd_Pay_Date_Time);                                   //Natural: MOVE EDITED #PAY-DATE-TIME TO #TXWA0270.EFFECTIVE-TS ( EM = YYYYMMDDHHIISST )
        //*   POPULATE USE DATE (D) WITH DATE PORTION OF GTN-TAX-CALC-TIMESTAMP (T)
        work_Fields_Pnd_Date_A.setValueEdited(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Tax_Calc_Timestamp().getValue(1),new ReportEditMask("YYYYMMDD"));                        //Natural: MOVE EDITED GTN-TAX-CALC-TIMESTAMP ( 1 ) ( EM = YYYYMMDD ) TO #DATE-A
        work_Fields_Pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),work_Fields_Pnd_Date_A);                                                                     //Natural: MOVE EDITED #DATE-A TO #DATE-D ( EM = YYYYMMDD )
        pdaTxwa0270.getPnd_Txwa0270_Use_Dte().setValue(work_Fields_Pnd_Date_D);                                                                                           //Natural: ASSIGN #TXWA0270.USE-DTE := #DATE-D
        pdaTxwa0270.getPnd_Txwa0270_Create_Ts().setValue(work_Fields_Pnd_Save_Timx);                                                                                      //Natural: ASSIGN #TXWA0270.CREATE-TS := #SAVE-TIMX
        pdaTxwa0270.getPnd_Txwa0270_Lu_Ts().setValue(work_Fields_Pnd_Save_Timx);                                                                                          //Natural: ASSIGN #TXWA0270.LU-TS := #SAVE-TIMX
        pdaTxwa0270.getPnd_Txwa0270_Save_Datx().setValue(work_Fields_Pnd_Save_Datx);                                                                                      //Natural: ASSIGN #TXWA0270.SAVE-DATX := #SAVE-DATX
        pdaTxwa0270.getPnd_Txwa0270_Comment().setValue("The election was created during payment generation");                                                             //Natural: ASSIGN #TXWA0270.COMMENT := 'The election was created during payment generation'
        pdaTxwa0270.getPnd_Txwa0270_Citizen_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Ctznshp_Cde());                                                        //Natural: ASSIGN #TXWA0270.CITIZEN-CDE := GTN-ANNT-CTZNSHP-CDE
        pdaTxwa0270.getPnd_Txwa0270_Res_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Rsdncy_Cde());                                                             //Natural: ASSIGN #TXWA0270.RES-CDE := GTN-ANNT-RSDNCY-CDE
        pdaTxwa0270.getPnd_Txwa0270_Loc_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Locality_Cde());                                                           //Natural: ASSIGN #TXWA0270.LOC-CDE := GTN-ANNT-LOCALITY-CDE
        pdaTxwa0270.getPnd_Txwa0270_Pin().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr());                                                                  //Natural: ASSIGN #TXWA0270.PIN := CNTRCT-UNQ-ID-NBR
        pdaTxwa0270.getPnd_Txwa0270_Elc_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Elec_Cde());                                                                    //Natural: ASSIGN #TXWA0270.ELC-CDE := GTN-ELEC-CDE
        //*  MARINA 10/2002
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals(" ")))                                                                                   //Natural: IF PYMNT-SUSPEND-CDE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_Fields_Pnd_Pend_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde());                                                                      //Natural: ASSIGN #PEND-CODE := PYMNT-SUSPEND-CDE
            pdaTxwa0270.getPnd_Txwa0270_Effective_Ts().reset();                                                                                                           //Natural: RESET #TXWA0270.EFFECTIVE-TS #TXWA0270.USE-DTE
            pdaTxwa0270.getPnd_Txwa0270_Use_Dte().reset();
            work_Fields_Pnd_Bypassed_Recs.nadd(1);                                                                                                                        //Natural: ADD 1 TO #BYPASSED-RECS
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwapart.getPnd_Txwapart_Pnd_Tin().setValue(pdaTxwa0270.getPnd_Txwa0270_Tin());                                                                                //Natural: ASSIGN #TXWAPART.#TIN := #TXWA0270.TIN
        pdaTxwapart.getPnd_Txwapart_Pnd_Func_Type().setValue("1");                                                                                                        //Natural: ASSIGN #TXWAPART.#FUNC-TYPE := '1'
        pdaTxwapart.getPnd_Txwapart_Pnd_Tin_Type().setValue(pdaTxwa0270.getPnd_Txwa0270_Tin_Type());                                                                      //Natural: ASSIGN #TXWAPART.#TIN-TYPE := #TXWA0270.TIN-TYPE
        pdaTxwapart.getPnd_Txwapart_Pnd_Naad_Lookup_Ind().setValue(true);                                                                                                 //Natural: ASSIGN #TXWAPART.#NAAD-LOOKUP-IND := TRUE
        pdaTxwapart.getPnd_Txwapart_Pnd_Pymnt_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                          //Natural: ASSIGN #TXWAPART.#PYMNT-DTE := PYMNT-CHECK-DTE
        pdaTxwapart.getPnd_Txwapart_Pnd_Citizen_Cde_I().setValue(pdaTxwa0270.getPnd_Txwa0270_Citizen_Cde());                                                              //Natural: ASSIGN #TXWAPART.#CITIZEN-CDE-I := #TXWA0270.CITIZEN-CDE
        pdaTxwapart.getPnd_Txwapart_Pnd_Res_Cde_I().setValue(pdaTxwa0270.getPnd_Txwa0270_Res_Cde());                                                                      //Natural: ASSIGN #TXWAPART.#RES-CDE-I := #TXWA0270.RES-CDE
        pdaTxwapart.getPnd_Txwapart_Pnd_Loc_Cde_I().setValue(pdaTxwa0270.getPnd_Txwa0270_Loc_Cde());                                                                      //Natural: ASSIGN #TXWAPART.#LOC-CDE-I := #TXWA0270.LOC-CDE
        pdaTxwapart.getPnd_Txwapart_Pnd_Ret_Code().setValue(true);                                                                                                        //Natural: ASSIGN #TXWAPART.#RET-CODE := TRUE
        //* **  TEST 8/2002
        //*   WRITE(0) *PROGRAM '#TXWAPART.#TIN             :' #TXWAPART.#TIN
        //*   WRITE(0) *PROGRAM '#TXWAPART.#TIN-TYPE        :' #TXWAPART.#TIN-TYPE
        //*   WRITE(0) *PROGRAM '#TXWAPART.#NAAD-LOOKUP-IND :'
        //*                      #TXWAPART.#NAAD-LOOKUP-IND
        //*   WRITE(0) *PROGRAM '#TXWAPART.#PYMNT-DTE       :'
        //*                      #TXWAPART.#PYMNT-DTE (EM=YYYYMMDD)
        //*   WRITE(0) *PROGRAM '#TXWAPART.#CITIZEN-CDE-I   :'
        //*                      #TXWAPART.#CITIZEN-CDE-I
        //*   WRITE(0) *PROGRAM '#TXWAPART.#RES-CDE-I       :' #TXWAPART.#RES-CDE-I
        //*   WRITE(0) *PROGRAM '#TXWAPART.#loc-CDE-I       :' #TXWAPART.#LOC-CDE-I
        //*     TEST END
        //* MQ OPEN
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  FE201508
        DbsUtil.callnat(Txwnparm.class , getCurrentProcessState(), pdaTxwapart.getPnd_Txwapart_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTxwapart.getPnd_Txwapart_Pnd_Output_Data(),  //Natural: CALLNAT 'TXWNPARM' USING #TXWAPART.#INPUT-PARMS ( AD = O ) #TXWAPART.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //* MQ CLOSE
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTxwapart.getPnd_Txwapart_Pnd_Ret_Code().getBoolean())))                                                                                       //Natural: IF NOT #TXWAPART.#RET-CODE
        {
            getReports().write(0, Global.getPROGRAM(),"Problem IN GET-PARTICIPANT-INFO Routine - TIN: ",pdaTxwapart.getPnd_Txwapart_Pnd_Tin());                           //Natural: WRITE ( 0 ) *PROGRAM 'Problem IN GET-PARTICIPANT-INFO Routine - TIN: ' #TXWAPART.#TIN
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwa0270.getPnd_Txwa0270_Res_Type().setValue(pdaTxwapart.getPnd_Txwapart_Pnd_Res_Type());                                                                      //Natural: ASSIGN #TXWA0270.RES-TYPE := #TXWAPART.#RES-TYPE
        pdaTxwa0270.getPnd_Txwa0270_Res_Cde().setValue(pdaTxwapart.getPnd_Txwapart_Pnd_Res_Cde_O());                                                                      //Natural: ASSIGN #TXWA0270.RES-CDE := #TXWAPART.#RES-CDE-O
        pdaTxwa0270.getPnd_Txwa0270_Loc_Cde().setValue(pdaTxwapart.getPnd_Txwapart_Pnd_Loc_Cde_O());                                                                      //Natural: ASSIGN #TXWA0270.LOC-CDE := #TXWAPART.#LOC-CDE-O
        DbsUtil.callnat(Txwn0270.class , getCurrentProcessState(), pdaTxwa0270.getPnd_Txwa0270());                                                                        //Natural: CALLNAT 'TXWN0270' USING #TXWA0270
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTxwa0270.getPnd_Txwa0270_Pnd_Return_Code().getBoolean())))                                                                                    //Natural: IF NOT #TXWA0270.#RETURN-CODE
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J_Et.nadd(1);                                                                                                                                                 //Natural: ADD 1 TO #J-ET
        work_Fields_Pnd_Tot_Default_Written.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOT-DEFAULT-WRITTEN
        if (condition(pdaTxwa0270.getPnd_Txwa0270_Fed_Process_Ind().equals("D")))                                                                                         //Natural: IF FED-PROCESS-IND = 'D'
        {
            work_Fields_Pnd_Written_Fed_Recs.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WRITTEN-FED-RECS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_Fields_Pnd_Written_Sta_Recs.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WRITTEN-STA-RECS
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_J_Et.greater(100)))                                                                                                                             //Natural: IF #J-ET GT 100
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_J_Et.reset();                                                                                                                                             //Natural: RESET #J-ET
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        work_Fields_Pnd_Message.reset();                                                                                                                                  //Natural: RESET #MESSAGE
        //*  MARINA 10/2002
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().notEquals(" ")))                                                                                //Natural: IF PYMNT-SUSPEND-CDE NE ' '
        {
            work_Fields_Pnd_Message.setValue(work_Fields_Pnd_Pend_Message);                                                                                               //Natural: ASSIGN #MESSAGE := #PEND-MESSAGE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_Fields_Pnd_Message.setValue(pdaTxwa0270.getPnd_Txwa0270_Pnd_Error_Desc().getSubstring(1,14));                                                            //Natural: ASSIGN #MESSAGE := SUBSTR ( #TXWA0270.#ERROR-DESC,1,14 )
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, new  ReportMatrixColumnSpacing(2),"/TIN",                                                                                                 //Natural: DISPLAY ( 1 ) ( SF = 2 ) '/TIN' #TXWA0270.TIN 'TIN/Type' #TXWA0270.TIN-TYPE '/Contract' #TXWA0270.CONTRACT-NBR 'Payee/Code' #TXWA0270.PAYEE-CDE 'Citiz/Code' #TXWA0270.CITIZEN-CDE 'Res /Code' #TXWA0270.RES-CDE 'Res /Type' #TXWA0270.RES-TYPE 'Loc /Code' #TXWA0270.LOC-CDE 'Elct/Code' #TXWA0270.ELC-CDE 'Elct/Type' #TXWA0270.ELC-TYPE 'Use/dte ' #TXWA0270.USE-DTE ( EM = YYYY/MM/DD ) 'Effective/TS' #TXWA0270.EFFECTIVE-TS ( EM = YYYY/MM/DD�HH:II:SS.T ) 'Message' #MESSAGE
        		pdaTxwa0270.getPnd_Txwa0270_Tin(),"TIN/Type",
        		pdaTxwa0270.getPnd_Txwa0270_Tin_Type(),"/Contract",
        		pdaTxwa0270.getPnd_Txwa0270_Contract_Nbr(),"Payee/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Payee_Cde(),"Citiz/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Citizen_Cde(),"Res /Code",
        		pdaTxwa0270.getPnd_Txwa0270_Res_Cde(),"Res /Type",
        		pdaTxwa0270.getPnd_Txwa0270_Res_Type(),"Loc /Code",
        		pdaTxwa0270.getPnd_Txwa0270_Loc_Cde(),"Elct/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Elc_Cde(),"Elct/Type",
        		pdaTxwa0270.getPnd_Txwa0270_Elc_Type(),"Use/dte ",
        		pdaTxwa0270.getPnd_Txwa0270_Use_Dte(), new ReportEditMask ("YYYY/MM/DD"),"Effective/TS",
        		pdaTxwa0270.getPnd_Txwa0270_Effective_Ts(), new ReportEditMask ("YYYY/MM/DD HH:II:SS.T"),"Message",
        		work_Fields_Pnd_Message);
        if (Global.isEscape()) return;
        //*    WRITE (1)  '             '#TXWA0270.#ERROR-DESC
        //*    WRITE (1) '                               '
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(53),"TIAA CREF Tax System", 
            new FieldAttributes ("AD=I"),new TabSetting(118),"Page  :",getReports().getPageNumberDbs(0), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Updates for USED new AP Elections",new TabSetting(118),"Report:  RPT0",NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(53),"TIAA CREF Tax System", 
            new FieldAttributes ("AD=I"),new TabSetting(118),"Page  :",getReports().getPageNumberDbs(1), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"New Default AP Elections",new TabSetting(118),"Report:  RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, new  ReportMatrixColumnSpacing(4),"/TIN",
        		ldaTxwl5050.getTxwl5050_Tin(),"TIN/Type",
        		ldaTxwl5050.getTxwl5050_Tin_Type(),"/Contract",
        		ldaTxwl5050.getTxwl5050_Contract_Nbr(),"Payee/Code",
        		ldaTxwl5050.getTxwl5050_Payee_Cde(),"Active/Ind",
        		ldaTxwl5050.getTxwl5050_Active_Ind(),"Elct/Code",
        		ldaTxwl5050.getTxwl5050_Elc_Cde(),"Elct/Type",
        		ldaTxwl5050.getTxwl5050_Elc_Type(),"Elct/Rscdcy",
        		ldaTxwl5050.getTxwl5050_Elc_Res(),"Used/Date",
        		ldaTxwl5050.getTxwl5050_Use_Dte(),"Effective/TS",
        		ldaTxwl5050.getTxwl5050_Effective_Ts(), new ReportEditMask ("YYYY/MM/DD HH:II:SS.T"));
        getReports().setDisplayColumns(1, new  ReportMatrixColumnSpacing(2),"/TIN",
        		pdaTxwa0270.getPnd_Txwa0270_Tin(),"TIN/Type",
        		pdaTxwa0270.getPnd_Txwa0270_Tin_Type(),"/Contract",
        		pdaTxwa0270.getPnd_Txwa0270_Contract_Nbr(),"Payee/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Payee_Cde(),"Citiz/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Citizen_Cde(),"Res /Code",
        		pdaTxwa0270.getPnd_Txwa0270_Res_Cde(),"Res /Type",
        		pdaTxwa0270.getPnd_Txwa0270_Res_Type(),"Loc /Code",
        		pdaTxwa0270.getPnd_Txwa0270_Loc_Cde(),"Elct/Code",
        		pdaTxwa0270.getPnd_Txwa0270_Elc_Cde(),"Elct/Type",
        		pdaTxwa0270.getPnd_Txwa0270_Elc_Type(),"Use/dte ",
        		pdaTxwa0270.getPnd_Txwa0270_Use_Dte(), new ReportEditMask ("YYYY/MM/DD"),"Effective/TS",
        		pdaTxwa0270.getPnd_Txwa0270_Effective_Ts(), new ReportEditMask ("YYYY/MM/DD HH:II:SS.T"),"Message",
        		work_Fields_Pnd_Message);
    }
}
