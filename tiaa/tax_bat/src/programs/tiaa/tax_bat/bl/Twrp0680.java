/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:51 PM
**        * FROM NATURAL PROGRAM : Twrp0680
************************************************************
**        * FILE NAME            : Twrp0680.java
**        * CLASS NAME           : Twrp0680
**        * INSTANCE NAME        : Twrp0680
************************************************************
********************************************************
** PROGRAM NAME:  TWRP0680
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ANATOLY
** PURPOSE     :  INITIAL LOAD/UPDATE OF THE
**             :  CITATION FILE(FILE# 99)
** DATE        :  02/26/99
** -----------------------------------------------------
** MODIFICATION LOG
** ----------------
** 01/27/99 - AVS - ADDED THE NEW REPORT(TOTAL CITED AMOUNTS).
** 02/02/99 - AVS - ADDED THE COMPANY TOTALS.
** 02/17/99 - AVS - ADDED THE COMPANY TOTALS FOR CANADA AND P.R.
** 02/18/99 - AVS - ADDED CANADA AND P.R. AMOUNTS FOR EACH RECORD.
** 02/26/99 - AVS - IMPROVED PAGE BREAK PROCESSING.
** 09/23/04 - MS  - TRUST CHANGES.
** 12/06/05 - BK. RESTOWED FOR NEW  TWRNCOMP PARMS
** 05/12/08 - RM  - ROTH 401K/403B PROJECT
**                  RECOMPILED DUE TO UPDATED TWRL0700.
** 10/18/11 - RC  - NON-ERISA TDA NEW COMPANY 'F'
** 02/24/12 - RC  - REPLACE TWRL0700 WITHY TWRL0702
**********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0680 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl9900 ldaTwrl9900;
    private LdaTwrl9807 ldaTwrl9807;
    private LdaTwrl0702 ldaTwrl0702;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Comp_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Invalid_Comp;
    private DbsField pnd_Ws_Pnd_Comp_Idx;

    private DbsGroup pnd_Cit_Table;
    private DbsField pnd_Cit_Table_Pnd_Comp_Nme;
    private DbsField pnd_Cit_Table_Pnd_Cit_Gross;
    private DbsField pnd_Cit_Table_Pnd_Cit_Ivc;
    private DbsField pnd_Cit_Table_Pnd_Cit_Int;
    private DbsField pnd_Cit_Table_Pnd_Cit_Can;
    private DbsField pnd_Cit_Table_Pnd_Cit_Nra;
    private DbsField pnd_Cit_Table_Pnd_Cit_Pr;
    private DbsField pnd_Cit_Table_Pnd_Cit_Fed;
    private DbsField pnd_Cit_Table_Pnd_Cit_State;
    private DbsField pnd_Cit_Table_Pnd_Cit_Local;
    private DbsField pnd_Cit_Table_Pnd_Cit_I;
    private DbsField pnd_Puri;
    private DbsField pnd_St_Amt;
    private DbsField pnd_Msg;
    private DbsField pnd_Msg1;
    private DbsField pnd_K;
    private DbsField pnd_Er_Su;

    private DbsGroup pnd_Er_Su__R_Field_1;
    private DbsField pnd_Er_Su_Pnd_Er_Tbl;
    private DbsField pnd_Er_Su_Pnd_Er_Yr;
    private DbsField pnd_Er_Su_Pnd_Er_No;
    private DbsField pnd_Ii;
    private DbsField pnd_Jj;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_J;
    private DbsField pnd_I;
    private DbsField pnd_Isn;
    private DbsField pnd_Del_Rec;
    private DbsField pnd_Cnt;
    private DbsField pnd_Err_No;
    private DbsField pnd_S;

    private DbsGroup pnd_S__R_Field_3;
    private DbsField pnd_S_Pnd_Tax_Id_Nbr;
    private DbsField pnd_S_Pnd_Contract_Nbr;
    private DbsField pnd_S_Pnd_Paymt_Dte;
    private DbsField pnd_Doit;
    private DbsField pnd_Cited_Dod;

    private DbsGroup pnd_Cited_Dod__R_Field_4;
    private DbsField pnd_Cited_Dod_Pnd_Cited_Dod_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl9900 = new LdaTwrl9900();
        registerRecord(ldaTwrl9900);
        registerRecord(ldaTwrl9900.getVw_citation_View());
        ldaTwrl9807 = new LdaTwrl9807();
        registerRecord(ldaTwrl9807);
        registerRecord(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View());
        ldaTwrl0702 = new LdaTwrl0702();
        registerRecord(ldaTwrl0702);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Comp_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Comp_Max", "COMP-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Invalid_Comp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Invalid_Comp", "#INVALID-COMP", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Cit_Table = localVariables.newGroupArrayInRecord("pnd_Cit_Table", "#CIT-TABLE", new DbsArrayController(1, 7));
        pnd_Cit_Table_Pnd_Comp_Nme = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Comp_Nme", "#COMP-NME", FieldType.STRING, 4);
        pnd_Cit_Table_Pnd_Cit_Gross = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Gross", "#CIT-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cit_Table_Pnd_Cit_Ivc = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Ivc", "#CIT-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Cit_Table_Pnd_Cit_Int = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Int", "#CIT-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_Can = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Can", "#CIT-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_Nra = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Nra", "#CIT-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_Pr = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Pr", "#CIT-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_Fed = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Fed", "#CIT-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Cit_Table_Pnd_Cit_State = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_State", "#CIT-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_Local = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_Local", "#CIT-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cit_Table_Pnd_Cit_I = pnd_Cit_Table.newFieldInGroup("pnd_Cit_Table_Pnd_Cit_I", "#CIT-I", FieldType.PACKED_DECIMAL, 8);
        pnd_Puri = localVariables.newFieldInRecord("pnd_Puri", "#PURI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_St_Amt = localVariables.newFieldInRecord("pnd_St_Amt", "#ST-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 33);
        pnd_Msg1 = localVariables.newFieldInRecord("pnd_Msg1", "#MSG1", FieldType.STRING, 33);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Er_Su = localVariables.newFieldInRecord("pnd_Er_Su", "#ER-SU", FieldType.NUMERIC, 8);

        pnd_Er_Su__R_Field_1 = localVariables.newGroupInRecord("pnd_Er_Su__R_Field_1", "REDEFINE", pnd_Er_Su);
        pnd_Er_Su_Pnd_Er_Tbl = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Tbl", "#ER-TBL", FieldType.NUMERIC, 1);
        pnd_Er_Su_Pnd_Er_Yr = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Yr", "#ER-YR", FieldType.NUMERIC, 4);
        pnd_Er_Su_Pnd_Er_No = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_No", "#ER-NO", FieldType.NUMERIC, 3);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.NUMERIC, 8);
        pnd_Jj = localVariables.newFieldInRecord("pnd_Jj", "#JJ", FieldType.NUMERIC, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 8);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Del_Rec = localVariables.newFieldInRecord("pnd_Del_Rec", "#DEL-REC", FieldType.NUMERIC, 3);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        pnd_Err_No = localVariables.newFieldArrayInRecord("pnd_Err_No", "#ERR-NO", FieldType.NUMERIC, 2, new DbsArrayController(1, 30));
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.STRING, 26);

        pnd_S__R_Field_3 = localVariables.newGroupInRecord("pnd_S__R_Field_3", "REDEFINE", pnd_S);
        pnd_S_Pnd_Tax_Id_Nbr = pnd_S__R_Field_3.newFieldInGroup("pnd_S_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Pnd_Contract_Nbr = pnd_S__R_Field_3.newFieldInGroup("pnd_S_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Pnd_Paymt_Dte = pnd_S__R_Field_3.newFieldInGroup("pnd_S_Pnd_Paymt_Dte", "#PAYMT-DTE", FieldType.NUMERIC, 8);
        pnd_Doit = localVariables.newFieldInRecord("pnd_Doit", "#DOIT", FieldType.STRING, 1);
        pnd_Cited_Dod = localVariables.newFieldInRecord("pnd_Cited_Dod", "#CITED-DOD", FieldType.STRING, 8);

        pnd_Cited_Dod__R_Field_4 = localVariables.newGroupInRecord("pnd_Cited_Dod__R_Field_4", "REDEFINE", pnd_Cited_Dod);
        pnd_Cited_Dod_Pnd_Cited_Dod_N = pnd_Cited_Dod__R_Field_4.newFieldInGroup("pnd_Cited_Dod_Pnd_Cited_Dod_N", "#CITED-DOD-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9900.initializeValues();
        ldaTwrl9807.initializeValues();
        ldaTwrl0702.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Comp_Max.setInitialValue(6);
        pnd_Cit_Table_Pnd_Comp_Nme.getValue(0 + 1).setInitialValue("????");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0680() throws Exception
    {
        super("Twrp0680");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *                                                                                                                                                             //Natural: FORMAT LS = 132 PS = 61;//Natural: FORMAT ( 01 ) LS = 132 PS = 61;//Natural: FORMAT ( 02 ) LS = 132 PS = 61
        //* * DELETE THE CITATION FILE RECS BEFORE LOADING WITH NEW RECORDS.
        //* *
        //* *READ CITATION-VIEW
        //* *  ADD 1 TO #CNT
        //* *  IF #CNT > 25
        //* *     RESET #CNT
        //* *     END TRANSACTION
        //* *  END-IF
        //* *  MOVE *ISN TO #ISN
        //* *  GET CITATION-VIEW #ISN
        //* *  DELETE
        //* *  END TRANSACTION
        //* *  ADD 1 TO #DEL-REC
        //* *END-READ
        //* *WRITE 'Cited records deleted: '#DEL-REC
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRACOMP.#ABEND-IND #TWRACOMP.#DISPLAY-IND
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().reset();
        //*  READ WORK FILE 01 TWRT-RECORD  #ERR-NO(1:30)
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0702.getTwrt_Record())))
        {
            CheckAtStartofData350();

            getReports().write(0, "=",ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Dob(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt(), //Natural: AT START OF DATA;//Natural: WRITE '=' TWRT-PAYMT-DTE '=' TWRT-DOB '=' TWRT-GROSS-AMT '=' TWRT-FED-WHHLD-AMT / '=' TWRT-STATE-WHHLD-AMT '=' TWRT-INTEREST-AMT '=' TWRT-IVC-AMT '=' TWRT-LOCAL-WHHLD-AMT / '=' TWRT-TOT-IVC-9B-AMT '=' TWRT-TAX-YEAR '=' TWRT-CAN-WHHLD-AMT '=' TWRT-NRA-WHHLD-AMT / '=' TWRT-TOTAL-DISTR-AMT '=' TWRT-DISTR-PCT '=' TWRT-ELECTION-CODE '=' TWRT-FED-NRA-TAX-RATE '=' TWRT-ROTH-FIRST-YEAR / '=' TWRT-ERROR-TABLE ( * )
                "=",ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt(),NEWLINE,"=",ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt(),
                "=",ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt(),NEWLINE,"=",ldaTwrl0702.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt(),
                "=",ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt(),
                NEWLINE,"=",ldaTwrl0702.getTwrt_Record_Twrt_Total_Distr_Amt(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Distr_Pct(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Election_Code(),
                "=",ldaTwrl0702.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate(),"=",ldaTwrl0702.getTwrt_Record_Twrt_Roth_First_Year(),NEWLINE,"=",ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue("*"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(!(ldaTwrl0702.getTwrt_Record_Twrt_Flag().equals("C"))))                                                                                         //Natural: ACCEPT IF TWRT-FLAG = 'C'
            {
                continue;
            }
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_S_Pnd_Tax_Id_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                                      //Natural: MOVE TWRT-TAX-ID TO #TAX-ID-NBR
            pnd_S_Pnd_Contract_Nbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                                                //Natural: MOVE TWRT-CNTRCT-NBR TO #CONTRACT-NBR
            pnd_S_Pnd_Paymt_Dte.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                                                    //Natural: MOVE TWRT-PAYMT-DTE TO #PAYMT-DTE
            pnd_Doit.reset();                                                                                                                                             //Natural: RESET #DOIT
            ldaTwrl9900.getVw_citation_View().startDatabaseFind                                                                                                           //Natural: FIND CITATION-VIEW WITH TWRCITED-TIN-CNTRCT-PMNT-DTE = #S
            (
            "FIND01",
            new Wc[] { new Wc("TWRCITED_TIN_CNTRCT_PMNT_DTE", "=", pnd_S, WcType.WITH) }
            );
            FIND01:
            while (condition(ldaTwrl9900.getVw_citation_View().readNextRow("FIND01", true)))
            {
                ldaTwrl9900.getVw_citation_View().setIfNotFoundControlFlag(false);
                if (condition(ldaTwrl9900.getVw_citation_View().getAstCOUNTER().equals(0)))                                                                               //Natural: IF NO RECORDS FOUND
                {
                    pnd_Doit.setValue("Y");                                                                                                                               //Natural: MOVE 'Y' TO #DOIT
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Doit.setValue("N");                                                                                                                                   //Natural: MOVE 'N' TO #DOIT
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Doit.equals("N")))                                                                                                                          //Natural: IF #DOIT = 'N'
            {
                getReports().write(0, "Attempt to load duplicate record:",pnd_S,NEWLINE,"=",pnd_S_Pnd_Tax_Id_Nbr,"=",pnd_S_Pnd_Contract_Nbr,"=",pnd_S_Pnd_Paymt_Dte);     //Natural: WRITE 'Attempt to load duplicate record:' #S / '=' #TAX-ID-NBR '=' #CONTRACT-NBR '=' #PAYMT-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_J.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #J
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   ADD RECORDS
            ldaTwrl9900.getCitation_View_Twrcited_Processed_Ind().setValue("N");                                                                                          //Natural: MOVE 'N' TO TWRCITED-PROCESSED-IND
            //* MOVE #ERR-NO (*) TO TWRCITED-ERR-MSG-NBRS(*)
            ldaTwrl9900.getCitation_View_Twrcited_Err_Msg_Nbrs().getValue("*").setValue(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(1,":",30));                //Natural: MOVE TWRT-ERROR-TABLE ( 1:30 ) TO TWRCITED-ERR-MSG-NBRS ( * )
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl9900.getCitation_View_Twrcited_Company_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde());                                                  //Natural: MOVE TWRT-COMPANY-CDE TO TWRCITED-COMPANY-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Orgn_Srce_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                     //Natural: MOVE TWRT-SOURCE TO TWRCITED-ORGN-SRCE-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Contract_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                  //Natural: MOVE TWRT-CNTRCT-NBR TO TWRCITED-CONTRACT-NBR
            ldaTwrl9900.getCitation_View_Twrcited_Payee_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde());                                                      //Natural: MOVE TWRT-PAYEE-CDE TO TWRCITED-PAYEE-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Type().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type());                                                  //Natural: MOVE TWRT-TAX-ID-TYPE TO TWRCITED-TAX-ID-TYPE
            ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                        //Natural: MOVE TWRT-TAX-ID TO TWRCITED-TAX-ID-NBR
            ldaTwrl9900.getCitation_View_Twrcited_Pin_Id_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                        //Natural: MOVE TWRT-PIN-ID TO TWRCITED-PIN-ID-NBR
            ldaTwrl9900.getCitation_View_Twrcited_Pymnt_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                      //Natural: MOVE TWRT-PAYMT-DTE TO TWRCITED-PYMNT-DTE
            ldaTwrl9900.getCitation_View_Twrcited_Payment_Type().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type());                                                  //Natural: MOVE TWRT-PAYMT-TYPE TO TWRCITED-PAYMENT-TYPE
            ldaTwrl9900.getCitation_View_Twrcited_Settle_Type().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type());                                                   //Natural: MOVE TWRT-SETTL-TYPE TO TWRCITED-SETTLE-TYPE
            ldaTwrl9900.getCitation_View_Twrcited_Currency_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Currency_Cde());                                                //Natural: MOVE TWRT-CURRENCY-CDE TO TWRCITED-CURRENCY-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                //Natural: MOVE TWRT-LOCALITY-CDE TO TWRCITED-LOCALITY-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Roll_Indicator().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind());                                              //Natural: MOVE TWRT-ROLLOVER-IND TO TWRCITED-ROLL-INDICATOR
            ldaTwrl9900.getCitation_View_Twrcited_Residency_Code().setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                              //Natural: MOVE TWRT-STATE-RSDNCY TO TWRCITED-RESIDENCY-CODE
            //*   MOVE TWRT-CITIZENSHIP          TO TWRCITED-TAX-CITIZENSHIP
            ldaTwrl9900.getCitation_View_Twrcited_Tax_Citizenship().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().getSubstring(2,1));                            //Natural: MOVE SUBSTR ( TWRT-CITIZENSHIP,2,1 ) TO TWRCITED-TAX-CITIZENSHIP
            ldaTwrl9900.getCitation_View_Twrcited_Date_Of_Birth().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dob());                                                        //Natural: MOVE TWRT-DOB TO TWRCITED-DATE-OF-BIRTH
            ldaTwrl9900.getCitation_View_Twrcited_Gross_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                                      //Natural: MOVE TWRT-GROSS-AMT TO TWRCITED-GROSS-AMT
            ldaTwrl9900.getCitation_View_Twrcited_Fed_Wthld_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                              //Natural: MOVE TWRT-FED-WHHLD-AMT TO TWRCITED-FED-WTHLD-AMT
            ldaTwrl9900.getCitation_View_Twrcited_State_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                              //Natural: MOVE TWRT-STATE-WHHLD-AMT TO TWRCITED-STATE-WTHLD
            ldaTwrl9900.getCitation_View_Twrcited_Int_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                                     //Natural: MOVE TWRT-INTEREST-AMT TO TWRCITED-INT-AMT
            ldaTwrl9900.getCitation_View_Twrcited_Ivc_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                                          //Natural: MOVE TWRT-IVC-AMT TO TWRCITED-IVC-AMT
            ldaTwrl9900.getCitation_View_Twrcited_Tax_Elct_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Elct_Cde());                                                //Natural: MOVE TWRT-TAX-ELCT-CDE TO TWRCITED-TAX-ELCT-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Ivc_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Cde());                                                          //Natural: MOVE TWRT-IVC-CDE TO TWRCITED-IVC-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Local_Wthld().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                                              //Natural: MOVE TWRT-LOCAL-WHHLD-AMT TO TWRCITED-LOCAL-WTHLD
            ldaTwrl9900.getCitation_View_Twrcited_Disability_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Disability_Cde());                                            //Natural: MOVE TWRT-DISABILITY-CDE TO TWRCITED-DISABILITY-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Tax_Year().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                        //Natural: MOVE TWRT-TAX-YEAR TO TWRCITED-TAX-YEAR
            //*  RCC
            pnd_Cited_Dod.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dod());                                                                                                //Natural: MOVE TWRT-DOD TO #CITED-DOD
            ldaTwrl9900.getCitation_View_Twrcited_Date_Of_Death().setValue(pnd_Cited_Dod_Pnd_Cited_Dod_N);                                                                //Natural: MOVE #CITED-DOD-N TO TWRCITED-DATE-OF-DEATH
            ldaTwrl9900.getCitation_View_Twrcited_Can_Wthld_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                              //Natural: MOVE TWRT-CAN-WHHLD-AMT TO TWRCITED-CAN-WTHLD-AMT
            ldaTwrl9900.getCitation_View_Twrcited_Nra_Wthld_Amt().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                              //Natural: MOVE TWRT-NRA-WHHLD-AMT TO TWRCITED-NRA-WTHLD-AMT
            ldaTwrl9900.getCitation_View_Twrcited_Form_1078_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078());                                        //Natural: MOVE TWRT-PYMNT-TAX-FORM-1078 TO TWRCITED-FORM-1078-IND
            ldaTwrl9900.getCitation_View_Twrcited_Form_1001_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                        //Natural: MOVE TWRT-PYMNT-TAX-FORM-1001 TO TWRCITED-FORM-1001-IND
            if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Acctg_Dte(),"99999999")))                                                             //Natural: IF TWRT-PYMNT-ACCTG-DTE = MASK ( 99999999 )
            {
                pnd_Date_A.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Acctg_Dte());                                                                                   //Natural: MOVE TWRT-PYMNT-ACCTG-DTE TO #DATE-A
                ldaTwrl9900.getCitation_View_Twrcited_Account_Dte().setValue(pnd_Date_A_Pnd_Date_N);                                                                      //Natural: MOVE #DATE-N TO TWRCITED-ACCOUNT-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Intrfce_Dte(),"99999999")))                                                           //Natural: IF TWRT-PYMNT-INTRFCE-DTE = MASK ( 99999999 )
            {
                pnd_Date_A.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Intrfce_Dte());                                                                                 //Natural: MOVE TWRT-PYMNT-INTRFCE-DTE TO #DATE-A
                ldaTwrl9900.getCitation_View_Twrcited_Pymnt_Intfce_Dte().setValue(pnd_Date_A_Pnd_Date_N);                                                                 //Natural: MOVE #DATE-N TO TWRCITED-PYMNT-INTFCE-DTE
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl9900.getCitation_View_Twrcited_Ivc_Rcvry_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Rcvry_Cde());                                              //Natural: MOVE TWRT-IVC-RCVRY-CDE TO TWRCITED-IVC-RCVRY-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Distribution_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Distrib_Cde());                                             //Natural: MOVE TWRT-DISTRIB-CDE TO TWRCITED-DISTRIBUTION-CDE
            ldaTwrl9900.getCitation_View_Twrcited_Updte_User().setValue(Global.getPROGRAM());                                                                             //Natural: MOVE *PROGRAM TO TWRCITED-UPDTE-USER
            ldaTwrl9900.getCitation_View_Twrcited_Updt_Dte().setValue(Global.getDATN());                                                                                  //Natural: MOVE *DATN TO TWRCITED-UPDT-DTE
            //* **
            ldaTwrl9900.getVw_citation_View().insertDBRow();                                                                                                              //Natural: STORE RECORD IN CITATION-VIEW
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //* * ----------------------------
            //* *
            //* ***********************************************
            pnd_Ii.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #II
            pnd_Er_Su_Pnd_Er_Tbl.setValue(7);                                                                                                                             //Natural: MOVE 7 TO #ER-TBL
            pnd_Er_Su_Pnd_Er_Yr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                                                     //Natural: MOVE TWRT-TAX-YEAR TO #ER-YR
            //* IF #ERR-NO(1) = 0
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(1).equals(getZero())))                                                                   //Natural: IF TWRT-ERROR-TABLE ( 1 ) = 0
            {
                pnd_Msg1.setValue("Unknown Error Number");                                                                                                                //Natural: MOVE 'Unknown Error Number' TO #MSG1
                pnd_Jj.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #JJ
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   MOVE #ERR-NO(1) TO #ER-NO
                pnd_Er_Su_Pnd_Er_No.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(1));                                                                  //Natural: MOVE TWRT-ERROR-TABLE ( 1 ) TO #ER-NO
                //*  GET ERROR MSG BY ERROR NUMBER
                                                                                                                                                                          //Natural: PERFORM GET-MSG
                sub_Get_Msg();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Msg1.setValue(pnd_Msg);                                                                                                                               //Natural: MOVE #MSG TO #MSG1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") ||                   //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
                ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ")))
            {
                pnd_Puri.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                     //Natural: ASSIGN #PURI := TWRT-STATE-WHHLD-AMT
                pnd_St_Amt.reset();                                                                                                                                       //Natural: RESET #ST-AMT
                pnd_Cit_Table_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                              //Natural: ADD TWRT-STATE-WHHLD-AMT TO #CIT-PR ( #COMP-IDX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Puri.reset();                                                                                                                                         //Natural: RESET #PURI
                pnd_St_Amt.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                   //Natural: ASSIGN #ST-AMT := TWRT-STATE-WHHLD-AMT
                pnd_Cit_Table_Pnd_Cit_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_State_Whhld_Amt());                           //Natural: ADD TWRT-STATE-WHHLD-AMT TO #CIT-STATE ( #COMP-IDX )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cit_Table_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt());                                     //Natural: ADD TWRT-GROSS-AMT TO #CIT-GROSS ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                   //Natural: ADD TWRT-FED-WHHLD-AMT TO #CIT-FED ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt());                                    //Natural: ADD TWRT-INTEREST-AMT TO #CIT-INT ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt());                                         //Natural: ADD TWRT-IVC-AMT TO #CIT-IVC ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt());                               //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #CIT-LOCAL ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                                   //Natural: ADD TWRT-CAN-WHHLD-AMT TO #CIT-CAN ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                   //Natural: ADD TWRT-NRA-WHHLD-AMT TO #CIT-NRA ( #COMP-IDX )
            pnd_Cit_Table_Pnd_Cit_I.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                   //Natural: ADD 1 TO #CIT-I ( #COMP-IDX )
            //* *
            if (condition(getReports().getAstLineCount(1).greater(59)))                                                                                                   //Natural: IF *LINE-COUNT ( 01 ) > 59
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),new               //Natural: WRITE ( 01 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X TWRT-PAYMT-DTE TWRT-TAX-ID 3X TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT #ST-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
                ColumnSpacing(1),ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte(),ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),new ColumnSpacing(3),ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),
                ldaTwrl0702.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Ivc_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_Fed_Whhld_Amt(),
                ldaTwrl0702.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_St_Amt,ldaTwrl0702.getTwrt_Record_Twrt_Local_Whhld_Amt(),ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),
                ldaTwrl0702.getTwrt_Record_Twrt_Citizenship());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Puri.greater(getZero()) || ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt().greater(getZero())))                                             //Natural: IF #PURI > 0 OR TWRT-CAN-WHHLD-AMT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(99),pnd_Puri,ldaTwrl0702.getTwrt_Record_Twrt_Can_Whhld_Amt());                               //Natural: WRITE ( 01 ) 99X #PURI TWRT-CAN-WHHLD-AMT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"Source Code:",ldaTwrl0702.getTwrt_Record_Twrt_Source(),"                        Error Description:",              //Natural: WRITE ( 01 ) 'Source Code:' TWRT-SOURCE '                        Error Description:' #MSG1
                pnd_Msg1);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #K 2 30
            for (pnd_K.setValue(2); condition(pnd_K.lessOrEqual(30)); pnd_K.nadd(1))
            {
                //*   IF #ERR-NO(#K) > 0
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(pnd_K).greater(getZero())))                                                          //Natural: IF TWRT-ERROR-TABLE ( #K ) > 0
                {
                    //*     MOVE #ERR-NO(#K) TO #ER-NO
                    pnd_Er_Su_Pnd_Er_No.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(pnd_K));                                                          //Natural: MOVE TWRT-ERROR-TABLE ( #K ) TO #ER-NO
                    //*  GET ERROR MSG BY ERROR NUMBER
                                                                                                                                                                          //Natural: PERFORM GET-MSG
                    sub_Get_Msg();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(59),pnd_Msg);                                                                            //Natural: WRITE ( 01 ) 59X #MSG
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_I.nsubtract(pnd_J);                                                                                                                                           //Natural: COMPUTE #I = #I - #J
        FOR02:                                                                                                                                                            //Natural: FOR #COMP-IDX = 0 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(0); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Comp_Idx.equals(getZero()) && ! (pnd_Ws_Pnd_Invalid_Comp.getBoolean())))                                                             //Natural: IF #COMP-IDX = 0 AND NOT #INVALID-COMP
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By",pnd_Cit_Table_Pnd_Comp_Nme.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),":",new  //Natural: WRITE ( 01 ) // '*' ( 131 ) / 'Totals By' #COMP-NME ( #COMP-IDX ) ':' 19X #CIT-GROSS ( #COMP-IDX ) #CIT-INT ( #COMP-IDX ) #CIT-IVC ( #COMP-IDX ) #CIT-FED ( #COMP-IDX ) #CIT-NRA ( #COMP-IDX ) #CIT-STATE ( #COMP-IDX ) #CIT-LOCAL ( #COMP-IDX )
                ColumnSpacing(19),pnd_Cit_Table_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Cit_Table_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                pnd_Cit_Table_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Cit_Table_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                pnd_Cit_Table_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Cit_Table_Pnd_Cit_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                pnd_Cit_Table_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Cit_Table_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Cit_Table_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: IF #CIT-PR ( #COMP-IDX ) > 0 OR #CIT-CAN ( #COMP-IDX ) > 0
                + 1).greater(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(99),pnd_Cit_Table_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Cit_Table_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 01 ) 99X #CIT-PR ( #COMP-IDX ) #CIT-CAN ( #COMP-IDX )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Company:",pnd_Cit_Table_Pnd_Comp_Nme.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),                  //Natural: WRITE ( 02 ) // 'Company:' #COMP-NME ( #COMP-IDX ) / 'Number of Cited    Records: ' #CIT-I ( #COMP-IDX ) / 'Gross            Amount   :' #CIT-GROSS ( #COMP-IDX ) / 'IVC              Amount   : ' #CIT-IVC ( #COMP-IDX ) / 'Interest         Amount   :  ' #CIT-INT ( #COMP-IDX ) / 'Federal      W/H Amount   : ' #CIT-FED ( #COMP-IDX ) / 'NRA          W/H Amount   :  ' #CIT-NRA ( #COMP-IDX ) / 'State        W/H Amount   :  ' #CIT-STATE ( #COMP-IDX ) / 'Local        W/H Amount   :  ' #CIT-LOCAL ( #COMP-IDX ) / 'Canadian     W/H Amount   :  ' #CIT-CAN ( #COMP-IDX ) / 'Puerto-Rican W/H Amount   :  ' #CIT-PR ( #COMP-IDX )
                NEWLINE,"Number of Cited    Records: ",pnd_Cit_Table_Pnd_Cit_I.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"Gross            Amount   :",
                pnd_Cit_Table_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"IVC              Amount   : ",pnd_Cit_Table_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                NEWLINE,"Interest         Amount   :  ",pnd_Cit_Table_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"Federal      W/H Amount   : ",
                pnd_Cit_Table_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"NRA          W/H Amount   :  ",pnd_Cit_Table_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                NEWLINE,"State        W/H Amount   :  ",pnd_Cit_Table_Pnd_Cit_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"Local        W/H Amount   :  ",
                pnd_Cit_Table_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),NEWLINE,"Canadian     W/H Amount   :  ",pnd_Cit_Table_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                NEWLINE,"Puerto-Rican W/H Amount   :  ",pnd_Cit_Table_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ******************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Cited records are to be loaded into Citation file  :",pnd_I);                                         //Natural: WRITE ( 01 ) // 'Cited records are to be loaded into Citation file  :' #I
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total cited records are to be loaded with Err # = 0:",pnd_Jj);                                                        //Natural: WRITE ( 01 ) 'Total cited records are to be loaded with Err # = 0:' #JJ
        if (Global.isEscape()) return;
        //* ********
        //* ****************************
        //* *****************************
        //* ****************************
        //*  ----------------------------
        //* * ----------------------------                                                                                                                                //Natural: AT TOP OF PAGE ( 01 )
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 02 )
    }
    private void sub_Get_Msg() throws Exception                                                                                                                           //Natural: GET-MSG
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) TIRCNTL-ERROR-MSG-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-SEQ = #ER-SU
        (
        "FIND02",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", "=", pnd_Er_Su, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().readNextRow("FIND02")))
        {
            ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().setIfNotFoundControlFlag(false);
            pnd_Msg.setValue(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr());                                                                       //Natural: MOVE TIRCNTL-ERROR-DESCR TO #MSG
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company() throws Exception                                                                                                                       //Natural: GET-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  RCC
        short decideConditionsMet527 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("C"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("S"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(3);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 3
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(4);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 4
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("X"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(5);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 5
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde().equals("F"))))
        {
            decideConditionsMet527++;
            pnd_Ws_Pnd_Comp_Idx.setValue(6);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 6
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Invalid_Comp.setValue(true);                                                                                                                       //Natural: ASSIGN #INVALID-COMP := TRUE
            pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                                  //Natural: RESET #COMP-IDX
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Load_Company() throws Exception                                                                                                                      //Natural: LOAD-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #COMP-IDX = 1 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(1); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            //*  RCC
            short decideConditionsMet549 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #COMP-IDX;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Comp_Idx.equals(1))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("C");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'C'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(2))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("L");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'L'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(3))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("S");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'S'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(4))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'T'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(5))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("X");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'X'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(6))))
            {
                decideConditionsMet549++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("F");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'F'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet549 > 0))
            {
                DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                    pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #TWRACOMP.#RET-CODE
                {
                    pnd_Cit_Table_Pnd_Comp_Nme.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name());                    //Natural: ASSIGN #COMP-NME ( #COMP-IDX ) := #TWRACOMP.#COMP-SHORT-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                          //Natural: RESET #COMP-IDX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0680-01",new                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0680-01' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 01 ) / 37X 'Participant Primary and Secondary Cites' 28X 'Date: '*DATU // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,new 
                        ColumnSpacing(37),"Participant Primary and Secondary Cites",new ColumnSpacing(28),"Date: ",Global.getDATU(),NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest",
                        "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ",
                        "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ",
                        "                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0680-02",new                    //Natural: WRITE ( 02 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0680-02' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 02 ) / 34X 'Participant Primary and Secondary Cites(Totals)' 23X 'Date: '*DATU
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(2),NEWLINE,new 
                        ColumnSpacing(34),"Participant Primary and Secondary Cites(Totals)",new ColumnSpacing(23),"Date: ",Global.getDATU());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=61");
        Global.format(1, "LS=132 PS=61");
        Global.format(2, "LS=132 PS=61");
    }
    private void CheckAtStartofData350() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-COMPANY
            sub_Load_Company();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
