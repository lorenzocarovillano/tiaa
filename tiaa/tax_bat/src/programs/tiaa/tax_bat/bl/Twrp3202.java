/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:47 PM
**        * FROM NATURAL PROGRAM : Twrp3202
************************************************************
**        * FILE NAME            : Twrp3202.java
**        * CLASS NAME           : Twrp3202
**        * INSTANCE NAME        : Twrp3202
************************************************************
************************************************************************
* PROGRAM   : TWRP3202 - ILLINOIS ST TAX WITHHOLDING QTRLY REPORTING
*
* FUNCTION  : QUARTERLY / YEAR-END EXTRACT OF ILLINOIS STATE TAX
*             WITHHOLDING RECORDS WITH PAYMENT DATE LESS THAN A GIVEN
*             PAYMENT DATE
*           : PRODUCES OUTPUT WORK FILE 1 AND THE FOLLOWING TWO REPORTS:
*             1. ERROR REPORT - RECS ARE REJECTED FROM FURTHER PROCESS
*                 (NO SSN OR INVALID SSN NO.)
*             2. DETAIL EXTRACT REPORT 2 & 3 - FOR AUDIT PURPOSE
*
* INPUT PARM: POSSIBLE VALUES FOR #PYMNT-DTE-LESS-THAN & #YEAR-END-ADJ
*               EXAMPLES:                   (YYYYMMDD)       (Y/N)
*               FOR 2020 1ST QTR             20200401          N
*                   2020 2ND QTR             20200701          N
*                   2020 3RD QTR             20201001          N
*                   2020 4TH QTR             20200101          N
*                   2020 YEAR END ADJ        20200101          Y
*
* DATE      : 06/25/2020 CREATED BY ARIVU
*
* HISTORY   :
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3202 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl3041 ldaTwrl3041;
    private PdaTwra105 pdaTwra105;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Detail_Rec;
    private DbsField pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Detail_Rec_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Detail_Rec__R_Field_1;
    private DbsField pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Detail_Rec_Form_Pymnt_Dte;
    private DbsField pnd_Detail_Rec_Form_Srce_Cde;
    private DbsField pnd_Detail_Rec_Form_Prdct_Cde;
    private DbsField pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Detail_Rec_Form_Pymnt_Gross_Amt;
    private DbsField pnd_Detail_Rec_Form_Pymnt_Loc_Wthhld_Amt;
    private DbsField pnd_Ssn_A10;

    private DbsGroup pnd_Ssn_A10__R_Field_2;
    private DbsField pnd_Ssn_A10_Pnd_Ssn_N9;
    private DbsField pnd_Ssn_A10_Pnd_Filler1;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_3;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_4;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;

    private DbsGroup pnd_Total_Amts;
    private DbsField pnd_Total_Amts_Pnd_Tot_Cnt_Accepted;
    private DbsField pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Total_Amts_Pnd_Tot_Cnt_Err;
    private DbsField pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt;
    private DbsField pnd_Total_Amts_Pnd_Tot_Gross;
    private DbsField pnd_Total_Amts_Pnd_Tot_Ivc;
    private DbsField pnd_Total_Amts_Pnd_Tot_Fed;
    private DbsField pnd_Total_Amts_Pnd_Tot_State;
    private DbsField pnd_I;
    private DbsField pnd_Sv_Co;
    private DbsField pnd_Prt_Co;
    private DbsField pnd_Yy;
    private DbsField pnd_Run_Type;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Cr_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Cr_Err_Amt;
    private DbsField pnd_Savers_Pnd_Cr_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Cr_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Cr_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Li_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Li_Err_Amt;
    private DbsField pnd_Savers_Pnd_Li_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Li_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Li_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Ti_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Ti_Err_Amt;
    private DbsField pnd_Savers_Pnd_Ti_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Ti_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Ti_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Tc_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Tc_Err_Amt;
    private DbsField pnd_Savers_Pnd_Tc_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Tc_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Tc_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Tr_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Tr_Err_Amt;
    private DbsField pnd_Savers_Pnd_Tr_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Tr_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Tr_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Fs_Rec_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Fs_Err_Amt;
    private DbsField pnd_Savers_Pnd_Fs_Er2_Amt;
    private DbsField pnd_Savers_Pnd_Fs_Rec_Cnt_Acc;
    private DbsField pnd_Savers_Pnd_Fs_Acc_Amt;
    private DbsField pnd_Savers_Pnd_Grd_Cnt_Err;
    private DbsField pnd_Savers_Pnd_Grd_Err_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Savers_Pnd_Grd_Cnt_Accepted;
    private DbsField pnd_Savers_Pnd_Grd_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Savers_Pnd_Cref;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl3041 = new LdaTwrl3041();
        registerRecord(ldaTwrl3041);
        localVariables = new DbsRecord();
        pdaTwra105 = new PdaTwra105(localVariables);

        // Local Variables

        pnd_Detail_Rec = localVariables.newGroupInRecord("pnd_Detail_Rec", "#DETAIL-REC");
        pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Detail_Rec_Form_Cntrct_Py_Nmbr = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", FieldType.STRING, 
            10);

        pnd_Detail_Rec__R_Field_1 = pnd_Detail_Rec.newGroupInGroup("pnd_Detail_Rec__R_Field_1", "REDEFINE", pnd_Detail_Rec_Form_Cntrct_Py_Nmbr);
        pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Detail_Rec__R_Field_1.newFieldInGroup("pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde = pnd_Detail_Rec__R_Field_1.newFieldInGroup("pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Detail_Rec_Form_Pymnt_Dte = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_Dte", "FORM-PYMNT-DTE", FieldType.STRING, 8);
        pnd_Detail_Rec_Form_Srce_Cde = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Detail_Rec_Form_Prdct_Cde = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Prdct_Cde", "FORM-PRDCT-CDE", FieldType.STRING, 1);
        pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt", "FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Detail_Rec_Form_Pymnt_Gross_Amt = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_Gross_Amt", "FORM-PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Detail_Rec_Form_Pymnt_Loc_Wthhld_Amt = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_Loc_Wthhld_Amt", "FORM-PYMNT-LOC-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ssn_A10 = localVariables.newFieldInRecord("pnd_Ssn_A10", "#SSN-A10", FieldType.STRING, 10);

        pnd_Ssn_A10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ssn_A10__R_Field_2", "REDEFINE", pnd_Ssn_A10);
        pnd_Ssn_A10_Pnd_Ssn_N9 = pnd_Ssn_A10__R_Field_2.newFieldInGroup("pnd_Ssn_A10_Pnd_Ssn_N9", "#SSN-N9", FieldType.NUMERIC, 9);
        pnd_Ssn_A10_Pnd_Filler1 = pnd_Ssn_A10__R_Field_2.newFieldInGroup("pnd_Ssn_A10_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_3 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_3", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_4 = pnd_Input_Parm__R_Field_3.newGroupInGroup("pnd_Input_Parm__R_Field_4", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);

        pnd_Total_Amts = localVariables.newGroupInRecord("pnd_Total_Amts", "#TOTAL-AMTS");
        pnd_Total_Amts_Pnd_Tot_Cnt_Accepted = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Cnt_Accepted", "#TOT-CNT-ACCEPTED", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt", "#TOT-FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Amts_Pnd_Tot_Cnt_Err = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Cnt_Err", "#TOT-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt", "#TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt", "#TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Amts_Pnd_Tot_Gross = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Gross", "#TOT-GROSS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Total_Amts_Pnd_Tot_Ivc = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Ivc", "#TOT-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Amts_Pnd_Tot_Fed = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_Fed", "#TOT-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Amts_Pnd_Tot_State = pnd_Total_Amts.newFieldInGroup("pnd_Total_Amts_Pnd_Tot_State", "#TOT-STATE", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Sv_Co = localVariables.newFieldInRecord("pnd_Sv_Co", "#SV-CO", FieldType.STRING, 1);
        pnd_Prt_Co = localVariables.newFieldInRecord("pnd_Prt_Co", "#PRT-CO", FieldType.STRING, 4);
        pnd_Yy = localVariables.newFieldInRecord("pnd_Yy", "#YY", FieldType.NUMERIC, 4);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Cr_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cr_Rec_Cnt_Err", "#CR-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Cr_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cr_Err_Amt", "#CR-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Cr_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cr_Er2_Amt", "#CR-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Cr_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cr_Rec_Cnt_Acc", "#CR-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Cr_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cr_Acc_Amt", "#CR-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Li_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Li_Rec_Cnt_Err", "#LI-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Li_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Li_Err_Amt", "#LI-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Li_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Li_Er2_Amt", "#LI-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Li_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Li_Rec_Cnt_Acc", "#LI-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Li_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Li_Acc_Amt", "#LI-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Ti_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Ti_Rec_Cnt_Err", "#TI-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Ti_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Ti_Err_Amt", "#TI-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Ti_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Ti_Er2_Amt", "#TI-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Ti_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Ti_Rec_Cnt_Acc", "#TI-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Ti_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Ti_Acc_Amt", "#TI-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tc_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tc_Rec_Cnt_Err", "#TC-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Tc_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tc_Err_Amt", "#TC-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tc_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tc_Er2_Amt", "#TC-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tc_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tc_Rec_Cnt_Acc", "#TC-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Tc_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tc_Acc_Amt", "#TC-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tr_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tr_Rec_Cnt_Err", "#TR-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Tr_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tr_Err_Amt", "#TR-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tr_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tr_Er2_Amt", "#TR-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Tr_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tr_Rec_Cnt_Acc", "#TR-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Tr_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Tr_Acc_Amt", "#TR-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Fs_Rec_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Fs_Rec_Cnt_Err", "#FS-REC-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Savers_Pnd_Fs_Err_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Fs_Err_Amt", "#FS-ERR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Fs_Er2_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Fs_Er2_Amt", "#FS-ER2-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Fs_Rec_Cnt_Acc = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Fs_Rec_Cnt_Acc", "#FS-REC-CNT-ACC", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Fs_Acc_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Fs_Acc_Amt", "#FS-ACC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Savers_Pnd_Grd_Cnt_Err = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Grd_Cnt_Err", "#GRD-CNT-ERR", FieldType.PACKED_DECIMAL, 9);
        pnd_Savers_Pnd_Grd_Err_Form_Pymnt_St_Wthhld_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Grd_Err_Form_Pymnt_St_Wthhld_Amt", "#GRD-ERR-FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Savers_Pnd_Grd_Cnt_Accepted = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Grd_Cnt_Accepted", "#GRD-CNT-ACCEPTED", FieldType.PACKED_DECIMAL, 
            11);
        pnd_Savers_Pnd_Grd_Form_Pymnt_St_Wthhld_Amt = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Grd_Form_Pymnt_St_Wthhld_Amt", "#GRD-FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Savers_Pnd_Cref = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Cref", "#CREF", FieldType.BOOLEAN, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3041.initializeValues();

        localVariables.reset();
        pnd_Savers_Pnd_Cref.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3202() throws Exception
    {
        super("Twrp3202");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3202|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                getReports().definePrinter(2, "PRT");                                                                                                                     //Natural: DEFINE PRINTER ( PRT = 01 ) OUTPUT 'CMPRT01'
                getReports().definePrinter(3, "PRT");                                                                                                                     //Natural: DEFINE PRINTER ( PRT = 02 ) OUTPUT 'CMPRT02'
                //*                                                                                                                                                       //Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  FE201505
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
                sub_Open_Mq();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 RECORD #EXT-CALIF
                while (condition(getWorkFiles().read(1, ldaTwrl3041.getPnd_Ext_Calif())))
                {
                    CheckAtStartofData247();

                    if (condition(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde().equals(pnd_Sv_Co)))                                                                 //Natural: AT START OF DATA;//Natural: IF TWRPYMNT-COMPANY-CDE = #SV-CO
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                        sub_Print_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Total_Amts.reset();                                                                                                                           //Natural: RESET #TOTAL-AMTS
                        getReports().newPage(new ReportSpecification(1));                                                                                                 //Natural: NEWPAGE ( 01 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 02 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Sv_Co.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde());                                                                          //Natural: ASSIGN #SV-CO := TWRPYMNT-COMPANY-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #I #C-TWRPYMNT-PAYMENTS 1 STEP -1
                    for (pnd_I.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_C_Twrpymnt_Payments()); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
                    {
                                                                                                                                                                          //Natural: PERFORM PRODUCE-#DETAIL-REC
                        sub_Produce_Pnd_Detail_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *-----------                                                                                                                                      //Natural: AT END OF DATA
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    //* *--------------
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                    sub_Print_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  TOPS RELEASE 3 CHANGES - ROLL-UP ALL COMPANIES UNDER TIAA OR TRUST
                    //*                                                    11/05/04      RM
                                                                                                                                                                          //Natural: PERFORM ROLL-UP-TOTALS
                    sub_Roll_Up_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().eject(1, true);                                                                                                                          //Natural: EJECT ( 01 )
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new           //Natural: WRITE ( 01 ) NOTITLE NOHDR *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 01' 40T 'MISSING OR INVALID SOCIAL SECURITY NUMBERS ERROR REPORT' / 57T #RUN-TYPE / 53T 'S U M M A R Y    R E P O R T' // / 13T 'NO. OF RECORDS' 8X 'TOTAL AMOUNT' / 16T 'REJECTED' 13X 'REJECTED' / / '*  TIAA     ' 3X #TI-REC-CNT-ERR 5X #TI-ERR-AMT / '*  TRUST    ' 3X #TR-REC-CNT-ERR 5X #TR-ERR-AMT // 'GRANDTOTAL  ' 1X #GRD-CNT-ERR 2X #GRD-ERR-FORM-PYMNT-ST-WTHHLD-AMT
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 01",new TabSetting(40),"MISSING OR INVALID SOCIAL SECURITY NUMBERS ERROR REPORT",NEWLINE,new 
                        TabSetting(57),pnd_Run_Type,NEWLINE,new TabSetting(53),"S U M M A R Y    R E P O R T",NEWLINE,NEWLINE,NEWLINE,new TabSetting(13),"NO. OF RECORDS",new 
                        ColumnSpacing(8),"TOTAL AMOUNT",NEWLINE,new TabSetting(16),"REJECTED",new ColumnSpacing(13),"REJECTED",NEWLINE,NEWLINE,"*  TIAA     ",new 
                        ColumnSpacing(3),pnd_Savers_Pnd_Ti_Rec_Cnt_Err, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(5),pnd_Savers_Pnd_Ti_Err_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"*  TRUST    ",new ColumnSpacing(3),pnd_Savers_Pnd_Tr_Rec_Cnt_Err, new ReportEditMask 
                        ("Z,ZZZ,ZZ9"),new ColumnSpacing(5),pnd_Savers_Pnd_Tr_Err_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"GRANDTOTAL  ",new 
                        ColumnSpacing(1),pnd_Savers_Pnd_Grd_Cnt_Err, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Savers_Pnd_Grd_Err_Form_Pymnt_St_Wthhld_Amt, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape())) return;
                    //*    /     '1. CREF     ' 3X #CR-REC-CNT-ERR  5X #CR-ERR-AMT
                    //*    /     '2. LIFE     ' 3X #LI-REC-CNT-ERR  5X #LI-ERR-AMT
                    //*    /     '3. TIAA     ' 3X #TI-REC-CNT-ERR  5X #TI-ERR-AMT
                    //*    /     '4. TCII     ' 3X #TC-REC-CNT-ERR  5X #TC-ERR-AMT
                    getReports().eject(2, true);                                                                                                                          //Natural: EJECT ( 02 )
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new           //Natural: WRITE ( 02 ) NOTITLE NOHDR *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 02' 56T 'DETAIL EXTRACT REPORT' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) / 57T #RUN-TYPE / 53T 'S U M M A R Y    R E P O R T' // / 14T 'NO. OF RECORDS' 9X 'TOTAL AMOUNT' / 18T 'ACCEPTED' 14X 'ACCEPTED' / / '*  TIAA     ' 4X #TI-REC-CNT-ACC 4X #TI-ACC-AMT / '*  TRUST    ' 4X #TR-REC-CNT-ACC 4X #TR-ACC-AMT // 'GRANDTOTAL  ' 1X #GRD-CNT-ACCEPTED 3X #GRD-FORM-PYMNT-ST-WTHHLD-AMT
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 02",new TabSetting(56),"DETAIL EXTRACT REPORT",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,new TabSetting(57),pnd_Run_Type,NEWLINE,new TabSetting(53),"S U M M A R Y    R E P O R T",NEWLINE,NEWLINE,NEWLINE,new 
                        TabSetting(14),"NO. OF RECORDS",new ColumnSpacing(9),"TOTAL AMOUNT",NEWLINE,new TabSetting(18),"ACCEPTED",new ColumnSpacing(14),"ACCEPTED",NEWLINE,NEWLINE,"*  TIAA     ",new 
                        ColumnSpacing(4),pnd_Savers_Pnd_Ti_Rec_Cnt_Acc, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),pnd_Savers_Pnd_Ti_Acc_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"*  TRUST    ",new ColumnSpacing(4),pnd_Savers_Pnd_Tr_Rec_Cnt_Acc, new ReportEditMask 
                        ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),pnd_Savers_Pnd_Tr_Acc_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"GRANDTOTAL  ",new 
                        ColumnSpacing(1),pnd_Savers_Pnd_Grd_Cnt_Accepted, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Savers_Pnd_Grd_Form_Pymnt_St_Wthhld_Amt, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape())) return;
                    //*    /     '1. CREF     ' 4X #CR-REC-CNT-ACC  4X #CR-ACC-AMT
                    //*    /     '2. LIFE     ' 4X #LI-REC-CNT-ACC  4X #LI-ACC-AMT
                    //*    /     '1. TIAA     ' 4X #TI-REC-CNT-ACC  4X #TI-ACC-AMT
                    //*    /     '2. TCII     ' 4X #TC-REC-CNT-ACC  4X #TC-ACC-AMT
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //* *------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //*  PIN-NBR EXPANSION START
                //*  'PIN'       #P-VARS.#PIN                         (EM=9999999)
                //*  PIN-NBR EXPANSION END
                //* *------------
                //* *------------
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 02 )
                //*  FE201505
                DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'MDMP0012'
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet350 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet350++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet350++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet350++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet350++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS REJECTED:",pnd_Total_Amts_Pnd_Tot_Cnt_Err, new ReportEditMask         //Natural: WRITE ( 01 ) /// 'TOTAL NUMBER OF RECORDS REJECTED:' #TOT-CNT-ERR / 'TOTAL ST AMOUNT REJECTED:' 39T #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT / 'TOTAL LOC AMOUNT REJECTED:' 39T #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL ST AMOUNT REJECTED:",new TabSetting(39),pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"TOTAL LOC AMOUNT REJECTED:",new TabSetting(39),pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"T O T A L S - - >",new TabSetting(79),pnd_Total_Amts_Pnd_Tot_Gross, new ReportEditMask                //Natural: WRITE ( 02 ) // 'T O T A L S - - >' 79T #TOT-GROSS 1X #TOT-IVC 1X #TOT-FED / 114T #TOT-STATE /// '** TOTAL NUMBER OF RECORDS ACCEPTED FOR FURTHER PROCESSING:' #TOT-CNT-ACCEPTED
            ("ZZ,ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Total_Amts_Pnd_Tot_Ivc, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Total_Amts_Pnd_Tot_Fed, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(114),pnd_Total_Amts_Pnd_Tot_State, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,NEWLINE,"** TOTAL NUMBER OF RECORDS ACCEPTED FOR FURTHER PROCESSING:",pnd_Total_Amts_Pnd_Tot_Cnt_Accepted, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (condition(pnd_Sv_Co.equals("C")))                                                                                                                             //Natural: IF #SV-CO = 'C'
        {
            pnd_Savers_Pnd_Cr_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #CR-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Cr_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #CR-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Cr_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #CR-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Cr_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #CR-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Cr_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #CR-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sv_Co.equals("L")))                                                                                                                             //Natural: IF #SV-CO = 'L'
        {
            pnd_Savers_Pnd_Li_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #LI-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Li_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #LI-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Li_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #LI-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Li_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #LI-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Li_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #LI-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sv_Co.equals("T")))                                                                                                                             //Natural: IF #SV-CO = 'T'
        {
            pnd_Savers_Pnd_Ti_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #TI-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Ti_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #TI-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Ti_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #TI-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Ti_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #TI-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Ti_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #TI-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sv_Co.equals("S")))                                                                                                                             //Natural: IF #SV-CO = 'S'
        {
            pnd_Savers_Pnd_Tc_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #TC-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Tc_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #TC-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Tc_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #TC-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Tc_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #TC-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Tc_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #TC-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sv_Co.equals("X")))                                                                                                                             //Natural: IF #SV-CO = 'X'
        {
            pnd_Savers_Pnd_Tr_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #TR-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Tr_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #TR-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Tr_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #TR-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Tr_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #TR-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Tr_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #TR-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sv_Co.equals("F")))                                                                                                                             //Natural: IF #SV-CO = 'F'
        {
            pnd_Savers_Pnd_Fs_Rec_Cnt_Err.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Err);                                                                                       //Natural: ASSIGN #FS-REC-CNT-ERR := #TOT-CNT-ERR
            pnd_Savers_Pnd_Fs_Err_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt);                                                                      //Natural: ASSIGN #FS-ERR-AMT := #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Savers_Pnd_Fs_Er2_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt);                                                                      //Natural: ASSIGN #FS-ER2-AMT := #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            pnd_Savers_Pnd_Fs_Rec_Cnt_Acc.setValue(pnd_Total_Amts_Pnd_Tot_Cnt_Accepted);                                                                                  //Natural: ASSIGN #FS-REC-CNT-ACC := #TOT-CNT-ACCEPTED
            pnd_Savers_Pnd_Fs_Acc_Amt.setValue(pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt);                                                                          //Natural: ASSIGN #FS-ACC-AMT := #TOT-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Roll_Up_Totals() throws Exception                                                                                                                    //Natural: ROLL-UP-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //*  MB
        pnd_Savers_Pnd_Ti_Rec_Cnt_Err.compute(new ComputeParameters(false, pnd_Savers_Pnd_Ti_Rec_Cnt_Err), pnd_Savers_Pnd_Ti_Rec_Cnt_Err.add(pnd_Savers_Pnd_Tc_Rec_Cnt_Err).add(pnd_Savers_Pnd_Li_Rec_Cnt_Err).add(pnd_Savers_Pnd_Cr_Rec_Cnt_Err).add(pnd_Savers_Pnd_Fs_Rec_Cnt_Err)); //Natural: COMPUTE #TI-REC-CNT-ERR = #TI-REC-CNT-ERR + #TC-REC-CNT-ERR + #LI-REC-CNT-ERR + #CR-REC-CNT-ERR + #FS-REC-CNT-ERR
        pnd_Savers_Pnd_Ti_Err_Amt.compute(new ComputeParameters(false, pnd_Savers_Pnd_Ti_Err_Amt), pnd_Savers_Pnd_Ti_Err_Amt.add(pnd_Savers_Pnd_Tc_Err_Amt).add(pnd_Savers_Pnd_Li_Err_Amt).add(pnd_Savers_Pnd_Cr_Err_Amt).add(pnd_Savers_Pnd_Fs_Err_Amt).add(pnd_Savers_Pnd_Ti_Er2_Amt).add(pnd_Savers_Pnd_Tc_Er2_Amt).add(pnd_Savers_Pnd_Li_Er2_Amt).add(pnd_Savers_Pnd_Cr_Er2_Amt).add(pnd_Savers_Pnd_Fs_Er2_Amt)); //Natural: COMPUTE #TI-ERR-AMT = #TI-ERR-AMT + #TC-ERR-AMT + #LI-ERR-AMT + #CR-ERR-AMT + #FS-ERR-AMT + #TI-ER2-AMT + #TC-ER2-AMT + #LI-ER2-AMT + #CR-ER2-AMT + #FS-ER2-AMT
        pnd_Savers_Pnd_Ti_Rec_Cnt_Acc.compute(new ComputeParameters(false, pnd_Savers_Pnd_Ti_Rec_Cnt_Acc), pnd_Savers_Pnd_Ti_Rec_Cnt_Acc.add(pnd_Savers_Pnd_Tc_Rec_Cnt_Acc).add(pnd_Savers_Pnd_Li_Rec_Cnt_Acc).add(pnd_Savers_Pnd_Cr_Rec_Cnt_Acc).add(pnd_Savers_Pnd_Fs_Rec_Cnt_Acc)); //Natural: COMPUTE #TI-REC-CNT-ACC = #TI-REC-CNT-ACC + #TC-REC-CNT-ACC + #LI-REC-CNT-ACC + #CR-REC-CNT-ACC + #FS-REC-CNT-ACC
        pnd_Savers_Pnd_Ti_Acc_Amt.compute(new ComputeParameters(false, pnd_Savers_Pnd_Ti_Acc_Amt), pnd_Savers_Pnd_Ti_Acc_Amt.add(pnd_Savers_Pnd_Tc_Acc_Amt).add(pnd_Savers_Pnd_Li_Acc_Amt).add(pnd_Savers_Pnd_Cr_Acc_Amt).add(pnd_Savers_Pnd_Fs_Acc_Amt)); //Natural: COMPUTE #TI-ACC-AMT = #TI-ACC-AMT + #TC-ACC-AMT + #LI-ACC-AMT + #CR-ACC-AMT + #FS-ACC-AMT
    }
    private void sub_Produce_Pnd_Detail_Rec() throws Exception                                                                                                            //Natural: PRODUCE-#DETAIL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        pnd_Detail_Rec.reset();                                                                                                                                           //Natural: RESET #DETAIL-REC
        pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Contract_Nbr());                                                                //Natural: ASSIGN #DETAIL-REC.#CNTRCT-PPCN-NBR := #EXT-CALIF.TWRPYMNT-CONTRACT-NBR
        pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Payee_Cde());                                                                  //Natural: ASSIGN #DETAIL-REC.#CNTRCT-PAYEE-CDE := #EXT-CALIF.TWRPYMNT-PAYEE-CDE
        pnd_Detail_Rec_Form_Pymnt_Dte.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                                    //Natural: ASSIGN FORM-PYMNT-DTE := #EXT-CALIF.#TWRPYMNT-PYMNT-DTE ( #I )
        pnd_Detail_Rec_Form_Srce_Cde.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I));                                                 //Natural: ASSIGN FORM-SRCE-CDE := #EXT-CALIF.#TWRPYMNT-ORGN-SRCE-CDE ( #I )
        pnd_Detail_Rec_Form_Prdct_Cde.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde());                                                                      //Natural: ASSIGN FORM-PRDCT-CDE := #EXT-CALIF.TWRPYMNT-COMPANY-CDE
        pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                                        //Natural: ASSIGN FORM-PYMNT-ST-WTHHLD-AMT := #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I )
        pnd_Detail_Rec_Form_Pymnt_Gross_Amt.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt().getValue(pnd_I));                                              //Natural: ASSIGN FORM-PYMNT-GROSS-AMT := #EXT-CALIF.#TWRPYMNT-GROSS-AMT ( #I )
        pnd_Detail_Rec_Form_Pymnt_Loc_Wthhld_Amt.setValue(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld().getValue(pnd_I));                                       //Natural: ASSIGN FORM-PYMNT-LOC-WTHHLD-AMT := #EXT-CALIF.#TWRPYMNT-LOCAL-WTHLD ( #I )
        if (condition(DbsUtil.maskMatches(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr(),"NNNNNNNNN")))                                                               //Natural: IF #EXT-CALIF.TWRPYMNT-TAX-ID-NBR = MASK ( NNNNNNNNN )
        {
            pnd_Ssn_A10.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr());                                                                                     //Natural: ASSIGN #SSN-A10 := #EXT-CALIF.TWRPYMNT-TAX-ID-NBR
            if (condition(pnd_Ssn_A10_Pnd_Filler1.equals(" ")))                                                                                                           //Natural: IF #FILLER1 = ' '
            {
                pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr.setValue(pnd_Ssn_A10_Pnd_Ssn_N9);                                                                                     //Natural: ASSIGN #DETAIL-REC.#ANNT-SOC-SEC-NBR := #SSN-N9
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*          /* (ALPHANUMERIC OR 0 TIN WILL BE EVALUATED)
        if (condition(pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr.equals(getZero())))                                                                                             //Natural: IF #DETAIL-REC.#ANNT-SOC-SEC-NBR = 0
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CORE-SSN
            sub_Obtain_Core_Ssn();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr.setValue(pdaTwra105.getPnd_P_Vars_Pnd_Ssn());                                                                             //Natural: ASSIGN #DETAIL-REC.#ANNT-SOC-SEC-NBR := #P-VARS.#SSN
        }                                                                                                                                                                 //Natural: END-IF
        //*  (INVALID TIN)
        if (condition(pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr.equals(getZero()) || ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type().equals("5")))                          //Natural: IF #DETAIL-REC.#ANNT-SOC-SEC-NBR = 0 OR #EXT-CALIF.TWRPYMNT-TAX-ID-TYPE = '5'
        {
                                                                                                                                                                          //Natural: PERFORM PRODUCE-ERROR-REPORT
            sub_Produce_Error_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Total_Amts_Pnd_Tot_Cnt_Err.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOT-CNT-ERR
            pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                            //Natural: ADD #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I ) TO #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Total_Amts_Pnd_Tot_Err_Form_Pymnt_Lc_Wthhld_Amt.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld().getValue(pnd_I));                            //Natural: ADD #EXT-CALIF.#TWRPYMNT-LOCAL-WTHLD ( #I ) TO #TOT-ERR-FORM-PYMNT-LC-WTHHLD-AMT
            //*  GRAND TOTALS
            pnd_Savers_Pnd_Grd_Cnt_Err.nadd(1);                                                                                                                           //Natural: ADD 1 TO #GRD-CNT-ERR
            pnd_Savers_Pnd_Grd_Err_Form_Pymnt_St_Wthhld_Amt.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                                //Natural: ADD #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I ) TO #GRD-ERR-FORM-PYMNT-ST-WTHHLD-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(2, false, pnd_Detail_Rec);                                                                                                               //Natural: WRITE WORK FILE 2 #DETAIL-REC
            //*  HUGE OUTPUT
                                                                                                                                                                          //Natural: PERFORM PRODUCE-DETAIL-REPORTS
            sub_Produce_Detail_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Total_Amts_Pnd_Tot_Cnt_Accepted.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOT-CNT-ACCEPTED
            pnd_Total_Amts_Pnd_Tot_Form_Pymnt_St_Wthhld_Amt.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                                //Natural: ADD #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I ) TO #TOT-FORM-PYMNT-ST-WTHHLD-AMT
            //*  GRAND TOTALS
            pnd_Savers_Pnd_Grd_Cnt_Accepted.nadd(1);                                                                                                                      //Natural: ADD 1 TO #GRD-CNT-ACCEPTED
            pnd_Savers_Pnd_Grd_Form_Pymnt_St_Wthhld_Amt.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                                    //Natural: ADD #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I ) TO #GRD-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Total_Amts_Pnd_Tot_Gross.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt().getValue(pnd_I));                                                     //Natural: ADD #EXT-CALIF.#TWRPYMNT-GROSS-AMT ( #I ) TO #TOT-GROSS
            pnd_Total_Amts_Pnd_Tot_Ivc.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt().getValue(pnd_I));                                                         //Natural: ADD #EXT-CALIF.#TWRPYMNT-IVC-AMT ( #I ) TO #TOT-IVC
            pnd_Total_Amts_Pnd_Tot_Fed.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I));                                                   //Natural: ADD #EXT-CALIF.#TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #TOT-FED
            pnd_Total_Amts_Pnd_Tot_State.nadd(ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I));                                                   //Natural: ADD #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( #I ) TO #TOT-STATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Obtain_Core_Ssn() throws Exception                                                                                                                   //Natural: OBTAIN-CORE-SSN
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pdaTwra105.getPnd_P_Vars().reset();                                                                                                                               //Natural: RESET #P-VARS
        pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr().setValue(pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr);                                                                      //Natural: ASSIGN #P-VARS.#CNTRCT-PPCN-NBR := #DETAIL-REC.#CNTRCT-PPCN-NBR
        pdaTwra105.getPnd_P_Vars_Pnd_Cntrct_Payee_Cde().setValue(pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde);                                                                    //Natural: ASSIGN #P-VARS.#CNTRCT-PAYEE-CDE := #DETAIL-REC.#CNTRCT-PAYEE-CDE
        pdaTwra105.getPnd_P_Vars_Pnd_Get_Ssn_Level_Info().setValue(true);                                                                                                 //Natural: ASSIGN #P-VARS.#GET-SSN-LEVEL-INFO := TRUE
        DbsUtil.callnat(Twrn105.class , getCurrentProcessState(), pdaTwra105.getPnd_P_Vars());                                                                            //Natural: CALLNAT 'TWRN105' #P-VARS
        if (condition(Global.isEscape())) return;
    }
    private void sub_Produce_Error_Report() throws Exception                                                                                                              //Natural: PRODUCE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        getReports().display(1, "SSN",                                                                                                                                    //Natural: DISPLAY ( 01 ) 'SSN' #DETAIL-REC.#ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'PIN' #P-VARS.#PIN ( EM = 999999999999 ) 'CNTRCT-PY' #DETAIL-REC.FORM-CNTRCT-PY-NMBR ( EM = XXXXXXXX-XX ) 'CHECK DTE' #DETAIL-REC.FORM-PYMNT-DTE 'STATE TAX' #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT ( EM = -Z,ZZZ,ZZ9.99 )
        		pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"PIN",
        		pdaTwra105.getPnd_P_Vars_Pnd_Pin(), new ReportEditMask ("999999999999"),"CNTRCT-PY",
        		pnd_Detail_Rec_Form_Cntrct_Py_Nmbr, new ReportEditMask ("XXXXXXXX-XX"),"CHECK DTE",
        		pnd_Detail_Rec_Form_Pymnt_Dte,"STATE TAX",
        		pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Produce_Detail_Reports() throws Exception                                                                                                            //Natural: PRODUCE-DETAIL-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().write(2, ReportOption.NOTITLE,ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Year(),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type(),new  //Natural: WRITE ( 02 ) TWRPYMNT-TAX-YEAR 1X TWRPYMNT-TAX-ID-TYPE 2X TWRPYMNT-TAX-ID-NBR 1X TWRPYMNT-TAX-CITIZENSHIP 1X TWRPYMNT-CONTRACT-NBR 1X TWRPYMNT-PAYEE-CDE 1X TWRPYMNT-DISTRIBUTION-CDE 1X TWRPYMNT-RESIDENCY-TYPE 3X TWRPYMNT-RESIDENCY-CODE 2X TWRPYMNT-IVC-INDICATOR 3X #TWRPYMNT-PYMNT-DTE ( #I ) ( EM = XXXX-XX-XX ) 1X #TWRPYMNT-PYMNT-INTFCE-DTE ( #I ) ( EM = XXXX-XX-XX ) 1X #TWRPYMNT-UPDTE-SRCE-CDE ( #I ) 1X #TWRPYMNT-ORGN-SRCE-CDE ( #I ) 1X #TWRPYMNT-PAYMENT-TYPE ( #I ) 1X #TWRPYMNT-SETTLE-TYPE ( #I ) 1X #TWRPYMNT-GROSS-AMT ( #I ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 3X #TWRPYMNT-IVC-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- ) 3X #TWRPYMNT-FED-WTHLD-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- ) / 116T #TWRPYMNT-STATE-WTHLD ( #I ) ( EM = Z,ZZZ,ZZ9.99- )
            ColumnSpacing(2),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr(),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Citizenship(),new 
            ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Contract_Nbr(),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Payee_Cde(),new 
            ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Distribution_Cde(),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Type(),new 
            ColumnSpacing(3),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Code(),new ColumnSpacing(2),ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Ivc_Indicator(),new 
            ColumnSpacing(3),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte().getValue(pnd_I), new ReportEditMask ("XXXX-XX-XX"),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_I), 
            new ReportEditMask ("XXXX-XX-XX"),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I),new 
            ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type().getValue(pnd_I),new ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type().getValue(pnd_I),new 
            ColumnSpacing(1),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt().getValue(pnd_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt().getValue(pnd_I), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(3),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_I), new ReportEditMask 
            ("Z,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(116),ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Co_Name() throws Exception                                                                                                                           //Natural: CO-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------
        //*  MB
        short decideConditionsMet515 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-CO;//Natural: VALUE 'C'
        if (condition((pnd_Sv_Co.equals("C"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("CREF");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Sv_Co.equals("L"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("LIFE");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Sv_Co.equals("S"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("TCII");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Sv_Co.equals("T"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("TIAA");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Sv_Co.equals("X"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("TRST");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Sv_Co.equals("F"))))
        {
            decideConditionsMet515++;
            pnd_Prt_Co.setValue("FSB");                                                                                                                                   //Natural: ASSIGN #PRT-CO := 'FSB'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Prt_Co.setValue("OTHR");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  FE201505 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
        //*  FE201505 END
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                                                                                                                                                                          //Natural: PERFORM CO-NAME
                    sub_Co_Name();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 01' 40T 'MISSING OR INVALID SOCIAL SECURITY NUMBERS ERROR REPORT' 110T 'PAGE:' *PAGE-NUMBER ( 01 ) / 57T #RUN-TYPE // 'COMPANY : ' #PRT-CO //
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 01",new TabSetting(40),"MISSING OR INVALID SOCIAL SECURITY NUMBERS ERROR REPORT",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,"COMPANY : ",pnd_Prt_Co,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                                                                                                                                                                          //Natural: PERFORM CO-NAME
                    sub_Co_Name();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new  //Natural: WRITE ( 02 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 02' 56T 'DETAIL EXTRACT REPORT' 110T 'PAGE:' *PAGE-NUMBER ( 02 ) / 57T #RUN-TYPE // 'COMPANY : ' #PRT-CO /// 'TAX  TX' 21T 'C' 32T 'PY' 4X 'RES RES IVC' 72T 'UP OR P S' / 'YEAR TP TAXID-NBR   Z CONTRCT# CD DC TYP CDE IND PAYMNT-DTE LOAD DATE' 72T 'SR SR T T' 4X 'GROSS AMOUNT' 6X 'IVC AMOUNT' 3X 'FEDERAL WTHLD' / 116T '-------------' / 116T ' STATE  WTHLD' //
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 02",new TabSetting(56),"DETAIL EXTRACT REPORT",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,"COMPANY : ",pnd_Prt_Co,NEWLINE,NEWLINE,NEWLINE,"TAX  TX",new 
                        TabSetting(21),"C",new TabSetting(32),"PY",new ColumnSpacing(4),"RES RES IVC",new TabSetting(72),"UP OR P S",NEWLINE,"YEAR TP TAXID-NBR   Z CONTRCT# CD DC TYP CDE IND PAYMNT-DTE LOAD DATE",new 
                        TabSetting(72),"SR SR T T",new ColumnSpacing(4),"GROSS AMOUNT",new ColumnSpacing(6),"IVC AMOUNT",new ColumnSpacing(3),"FEDERAL WTHLD",NEWLINE,new 
                        TabSetting(116),"-------------",NEWLINE,new TabSetting(116)," STATE  WTHLD",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "SSN",
        		pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"PIN",
        		pdaTwra105.getPnd_P_Vars_Pnd_Pin(), new ReportEditMask ("999999999999"),"CNTRCT-PY",
        		pnd_Detail_Rec_Form_Cntrct_Py_Nmbr, new ReportEditMask ("XXXXXXXX-XX"),"CHECK DTE",
        		pnd_Detail_Rec_Form_Pymnt_Dte,"STATE TAX",
        		pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
    }
    private void CheckAtStartofData247() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Sv_Co.setValue(ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde());                                                                                      //Natural: ASSIGN #SV-CO := TWRPYMNT-COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-START
    }
}
