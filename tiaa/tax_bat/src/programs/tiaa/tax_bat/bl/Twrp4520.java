/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:32 PM
**        * FROM NATURAL PROGRAM : Twrp4520
************************************************************
**        * FILE NAME            : Twrp4520.java
**        * CLASS NAME           : Twrp4520
**        * INSTANCE NAME        : Twrp4520
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4520
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : IRS FORMS EXTRACT CONTROL TOTALS REPORTS.
* CREATED  : 07 / 11 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS 5498 FORMS RECORDS, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
* 07/03/06  R. MA
*           ADD #F-SEP-AMT TO LDA TWRL452A.
*           ADD 'SEP' TO REPORT HEADERS. CREATE NEW SUBROUTINE TO PER-
*           FORM HEADER SET-UP, SO TO ELIMINATE REDENDUNT CODES.
*           ADD SEP-AMT TO ALL TOTAL FIELDS (BY REACTIVATE THE COMMENTED
*           OUT EDUCATION-AMT FIELDS). UPDATE ALL RELATED LOGIC.
*
* 23-11-20 : PALDE           - IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4520 extends BLNatBase
{
    // Data Areas
    private LdaTwrl452a ldaTwrl452a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Total_B_Recs_Record;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_1;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr;

    private DbsGroup pnd_Pin_Base_Key__R_Field_2;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind;
    private DbsField pnd_Twrc_S2_Forms;

    private DbsGroup pnd_Twrc_S2_Forms__R_Field_3;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Status;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Contract;
    private DbsField pnd_Twrc_S2_Forms_Pnd_S2_Payee;
    private DbsField pnd_Isn;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Dob_Ctr;
    private DbsField pnd_No_Of_Errors;
    private DbsField pnd_No_Of_Warnings;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Warning_Msg;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Form_Type;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Interface_Date;
    private DbsField pnd_Hdr_Simulation_Date;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_T_Postpn_Amt;
    private DbsField pnd_T_Repymnt_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_C_Postpn_Amt;
    private DbsField pnd_C_Repymnt_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_L_Postpn_Amt;
    private DbsField pnd_L_Repymnt_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;
    private DbsField pnd_S_Postpn_Amt;
    private DbsField pnd_S_Repymnt_Amt;
    private DbsField pnd_Rejected;
    private DbsField pnd_Dup_Found;
    private DbsField pnd_Naa_Found;
    private DbsField i1;
    private DbsField i2;
    private DbsField i3;
    private DbsField i4;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl452a = new LdaTwrl452a();
        registerRecord(ldaTwrl452a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Total_B_Recs_Record = localVariables.newFieldInRecord("pnd_Total_B_Recs_Record", "#TOTAL-B-RECS-RECORD", FieldType.NUMERIC, 8);
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 13);

        pnd_Pin_Base_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_1", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr = pnd_Pin_Base_Key__R_Field_1.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr", "#PH-UNQUE-ID-NMBR", 
            FieldType.STRING, 12);

        pnd_Pin_Base_Key__R_Field_2 = pnd_Pin_Base_Key__R_Field_1.newGroupInGroup("pnd_Pin_Base_Key__R_Field_2", "REDEFINE", pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N = pnd_Pin_Base_Key__R_Field_2.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N", "#PH-UNQUE-ID-NMBR-N", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind = pnd_Pin_Base_Key__R_Field_1.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind", "#PH-BSE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Twrc_S2_Forms = localVariables.newFieldInRecord("pnd_Twrc_S2_Forms", "#TWRC-S2-FORMS", FieldType.STRING, 25);

        pnd_Twrc_S2_Forms__R_Field_3 = localVariables.newGroupInRecord("pnd_Twrc_S2_Forms__R_Field_3", "REDEFINE", pnd_Twrc_S2_Forms);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Twrc_S2_Forms_Pnd_S2_Status = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Status", "#S2-STATUS", FieldType.STRING, 
            1);
        pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Tax_Id", "#S2-TAX-ID", FieldType.STRING, 
            10);
        pnd_Twrc_S2_Forms_Pnd_S2_Contract = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Contract", "#S2-CONTRACT", FieldType.STRING, 
            8);
        pnd_Twrc_S2_Forms_Pnd_S2_Payee = pnd_Twrc_S2_Forms__R_Field_3.newFieldInGroup("pnd_Twrc_S2_Forms_Pnd_S2_Payee", "#S2-PAYEE", FieldType.STRING, 
            2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Dob_Ctr = localVariables.newFieldInRecord("pnd_Dob_Ctr", "#DOB-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Of_Errors = localVariables.newFieldInRecord("pnd_No_Of_Errors", "#NO-OF-ERRORS", FieldType.PACKED_DECIMAL, 7);
        pnd_No_Of_Warnings = localVariables.newFieldInRecord("pnd_No_Of_Warnings", "#NO-OF-WARNINGS", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Msg = localVariables.newFieldArrayInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Warning_Msg = localVariables.newFieldArrayInRecord("pnd_Warning_Msg", "#WARNING-MSG", FieldType.STRING, 80, new DbsArrayController(1, 50));
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Form_Type = localVariables.newFieldInRecord("pnd_Prev_Form_Type", "#PREV-FORM-TYPE", FieldType.STRING, 2);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);
        pnd_Hdr_Simulation_Date = localVariables.newFieldInRecord("pnd_Hdr_Simulation_Date", "#HDR-SIMULATION-DATE", FieldType.DATE);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_T_Postpn_Amt", "#T-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_T_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_T_Repymnt_Amt", "#T-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_C_Postpn_Amt", "#C-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_C_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_C_Repymnt_Amt", "#C-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_L_Postpn_Amt", "#L-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_L_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_L_Repymnt_Amt", "#L-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_S_Postpn_Amt", "#S-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_S_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_S_Repymnt_Amt", "#S-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            8));
        pnd_Rejected = localVariables.newFieldInRecord("pnd_Rejected", "#REJECTED", FieldType.BOOLEAN, 1);
        pnd_Dup_Found = localVariables.newFieldInRecord("pnd_Dup_Found", "#DUP-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found = localVariables.newFieldInRecord("pnd_Naa_Found", "#NAA-FOUND", FieldType.BOOLEAN, 1);
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 4);
        i2 = localVariables.newFieldInRecord("i2", "I2", FieldType.PACKED_DECIMAL, 4);
        i3 = localVariables.newFieldInRecord("i3", "I3", FieldType.PACKED_DECIMAL, 4);
        i4 = localVariables.newFieldInRecord("i4", "I4", FieldType.PACKED_DECIMAL, 4);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl452a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4520() throws Exception
    {
        super("Twrp4520");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4520", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Header_Date.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN #HEADER-DATE := *DATX
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, ldaTwrl452a.getPnd_Form())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                pnd_Prev_Form_Type.setValue(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Type());                                                                                    //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Form_Type.equals(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Type())))                                                                           //Natural: IF #PREV-FORM-TYPE = #F-IRA-TYPE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  07/03/06 RM
                                                                                                                                                                          //Natural: PERFORM SETUP-HEADER-DISPLAY-TOTAL
                sub_Setup_Header_Display_Total();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Form_Type.setValue(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Type());                                                                                    //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
            }                                                                                                                                                             //Natural: END-IF
            j.setValue(1);                                                                                                                                                //Natural: ASSIGN J := 1
            if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Company_Cde().equals("T")))                                                                                       //Natural: IF #F-COMPANY-CDE = 'T'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Company_Cde().equals("C")))                                                                                   //Natural: IF #F-COMPANY-CDE = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                    sub_Update_C_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Company_Cde().equals("L")))                                                                               //Natural: IF #F-COMPANY-CDE = 'L'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                        sub_Update_L_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                        sub_Update_S_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //* *------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form (5498) IRS File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) IRS File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Read_Ctr.greater(getZero())))                                                                                                                   //Natural: IF #READ-CTR > 0
        {
            //*  07/03/06 RM
                                                                                                                                                                          //Natural: PERFORM SETUP-HEADER-DISPLAY-TOTAL
            sub_Setup_Header_Display_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------------------------------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    //*  07/03/06 RM
    private void sub_Setup_Header_Display_Total() throws Exception                                                                                                        //Natural: SETUP-HEADER-DISPLAY-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Prev_Form_Type.equals("10")))                                                                                                                   //Natural: IF #PREV-FORM-TYPE = '10'
        {
            pnd_Source_Total_Header.setValue("     Classic     ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '     Classic     '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prev_Form_Type.equals("20")))                                                                                                               //Natural: IF #PREV-FORM-TYPE = '20'
            {
                pnd_Source_Total_Header.setValue("      Roth       ");                                                                                                    //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      Roth       '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  07/03/06 RM
                if (condition(pnd_Prev_Form_Type.equals("30")))                                                                                                           //Natural: IF #PREV-FORM-TYPE = '30'
                {
                    pnd_Source_Total_Header.setValue("    Rollovers    ");                                                                                                //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '    Rollovers    '
                    //*  07/03/06 RM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Source_Total_Header.setValue("      SEP        ");                                                                                                //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      SEP        '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
        sub_Reset_Control_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_T_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( J )
        //*  DISPLAY #F-TRAD-IRA-CONTRIB J #T-CLASSIC-AMT (J)
        pnd_T_Classic_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #T-CLASSIC-AMT ( J )
        pnd_T_Roth_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #T-ROTH-AMT ( J )
        if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Year().less("2001")))                                                                                             //Natural: IF #F-TAX-YEAR < '2001'
        {
            if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_5498_Rechar_Ind().equals("Y")))                                                                                   //Natural: IF #F-5498-RECHAR-IND = 'Y'
            {
                pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                     //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-RECHAR-AMT ( J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                   //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                       //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( J )
            pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                //Natural: ADD #F-IRA-RECHAR TO #T-RECHAR-AMT ( J )
        }                                                                                                                                                                 //Natural: END-IF
        //*  07/03/06 RM
        pnd_T_Sep_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Sep_Amt());                                                                                          //Natural: ADD #F-SEP-AMT TO #T-SEP-AMT ( J )
        pnd_T_Fmv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #T-FMV-AMT ( J )
        pnd_T_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #T-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_T_Postpn_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #T-POSTPN-AMT ( J )
        //*  PALDE
        pnd_T_Repymnt_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #T-REPYMNT-AMT ( J )
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
        pnd_C_Classic_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #C-CLASSIC-AMT ( J )
        pnd_C_Roth_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #C-ROTH-AMT ( J )
        if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Year().less("2001")))                                                                                             //Natural: IF #F-TAX-YEAR < '2001'
        {
            if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_5498_Rechar_Ind().equals("Y")))                                                                                   //Natural: IF #F-5498-RECHAR-IND = 'Y'
            {
                pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                     //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #C-RECHAR-AMT ( J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                   //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #C-ROLLOVER-AMT ( J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                       //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #C-ROLLOVER-AMT ( J )
            pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                //Natural: ADD #F-IRA-RECHAR TO #C-RECHAR-AMT ( J )
        }                                                                                                                                                                 //Natural: END-IF
        //*  07/03/06 RM
        pnd_C_Sep_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Sep_Amt());                                                                                          //Natural: ADD #F-SEP-AMT TO #C-SEP-AMT ( J )
        pnd_C_Fmv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #C-FMV-AMT ( J )
        pnd_C_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #C-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_C_Postpn_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #C-POSTPN-AMT ( J )
        //*  PALDE
        pnd_C_Repymnt_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #C-REPYMNT-AMT ( J )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_L_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( J )
        pnd_L_Classic_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #L-CLASSIC-AMT ( J )
        pnd_L_Roth_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #L-ROTH-AMT ( J )
        if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Year().less("2001")))                                                                                             //Natural: IF #F-TAX-YEAR < '2001'
        {
            if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_5498_Rechar_Ind().equals("Y")))                                                                                   //Natural: IF #F-5498-RECHAR-IND = 'Y'
            {
                pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                     //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #L-RECHAR-AMT ( J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                   //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #L-ROLLOVER-AMT ( J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                       //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #L-ROLLOVER-AMT ( J )
            pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                //Natural: ADD #F-IRA-RECHAR TO #L-RECHAR-AMT ( J )
        }                                                                                                                                                                 //Natural: END-IF
        //*  07/03/06 RM
        pnd_L_Sep_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Sep_Amt());                                                                                          //Natural: ADD #F-SEP-AMT TO #L-SEP-AMT ( J )
        pnd_L_Fmv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #L-FMV-AMT ( J )
        pnd_L_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #L-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_L_Postpn_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #L-POSTPN-AMT ( J )
        //*  PALDE
        pnd_L_Repymnt_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #L-REPYMNT-AMT ( J )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_S_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #S-TRANS-COUNT ( J )
        pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #S-CLASSIC-AMT ( J )
        pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #S-ROTH-AMT ( J )
        if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_Tax_Year().less("2001")))                                                                                             //Natural: IF #F-TAX-YEAR < '2001'
        {
            if (condition(ldaTwrl452a.getPnd_Form_Pnd_F_5498_Rechar_Ind().equals("Y")))                                                                                   //Natural: IF #F-5498-RECHAR-IND = 'Y'
            {
                pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                     //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #S-RECHAR-AMT ( J )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                   //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #S-ROLLOVER-AMT ( J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                       //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #S-ROLLOVER-AMT ( J )
            pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                //Natural: ADD #F-IRA-RECHAR TO #S-RECHAR-AMT ( J )
        }                                                                                                                                                                 //Natural: END-IF
        //*  07/03/06 RM
        pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Sep_Amt());                                                                                          //Natural: ADD #F-SEP-AMT TO #S-SEP-AMT ( J )
        pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #S-FMV-AMT ( J )
        pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #S-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_S_Postpn_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #S-POSTPN-AMT ( J )
        //*  PALDE
        pnd_S_Repymnt_Amt.getValue(j).nadd(ldaTwrl452a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #S-REPYMNT-AMT ( J )
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        //*  54T #T-TRANS-COUNT (I2)
        //*  75T #T-TRANS-COUNT (I3)
        //*  96T #T-TRANS-COUNT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-CLASSIC-AMT   (I2)
        //*  68T #T-CLASSIC-AMT   (I3)
        //*  89T #T-CLASSIC-AMT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-ROTH-AMT      (I2)
        //*  68T #T-ROTH-AMT      (I3)
        //*  89T #T-ROTH-AMT      (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-ROLLOVER-AMT  (I2)
        //*  68T #T-ROLLOVER-AMT  (I3)
        //*  89T #T-ROLLOVER-AMT  (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-ROTH-CONV-AMT (I2)
        //*  68T #T-ROTH-CONV-AMT (I3)
        //*  89T #T-ROTH-CONV-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-RECHAR-AMT    (I2)
        //*  68T #T-RECHAR-AMT    (I3)
        //*  89T #T-RECHAR-AMT    (I4)  SKIP (04) 1
        //*  07/03/06 RM
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-EDUCATION-AMT (I2)
        //*  68T #T-EDUCATION-AMT (I3)
        //*  89T #T-EDUCATION-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_T_Fmv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #T-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #T-FMV-AMT       (I2)
        //*  68T #T-FMV-AMT       (I3)
        //*  89T #T-FMV-AMT       (I4)  SKIP (04) 3
        //*                                               /* PALDE STARTS
        getReports().write(1, ReportOption.NOTITLE,"LATE RO AMT.........",new TabSetting(26),pnd_T_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'LATE RO AMT.........' 26T #T-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_T_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #T-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        //*                                                  /* PALDE ENDS
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (CREF)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        //*  54T #C-TRANS-COUNT (I2)
        //*  75T #C-TRANS-COUNT (I3)
        //*  96T #C-TRANS-COUNT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-CLASSIC-AMT   (I2)
        //*  68T #C-CLASSIC-AMT   (I3)
        //*  89T #C-CLASSIC-AMT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-ROTH-AMT      (I2)
        //*  68T #C-ROTH-AMT      (I3)
        //*  89T #C-ROTH-AMT      (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-ROLLOVER-AMT  (I2)
        //*  68T #C-ROLLOVER-AMT  (I3)
        //*  89T #C-ROLLOVER-AMT  (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-ROTH-CONV-AMT (I2)
        //*  68T #C-ROTH-CONV-AMT (I3)
        //*  89T #C-ROTH-CONV-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-RECHAR-AMT    (I2)
        //*  68T #C-RECHAR-AMT    (I3)
        //*  89T #C-RECHAR-AMT    (I4)  SKIP (04) 1
        //*  07/03/06 RM
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-EDUCATION-AMT (I2)
        //*  68T #C-EDUCATION-AMT (I3)
        //*  89T #C-EDUCATION-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_C_Fmv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #C-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #C-FMV-AMT       (I2)
        //*  68T #C-FMV-AMT       (I3)
        //*  89T #C-FMV-AMT       (I4)  SKIP (04) 3
        //*                                               /* PALDE STARTS
        getReports().write(1, ReportOption.NOTITLE,"LATE RO AMT.........",new TabSetting(26),pnd_C_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'LATE RO AMT.........' 26T #C-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_C_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #C-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        //*                                                  /* PALDE ENDS
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (LIFE)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        //*  54T #L-TRANS-COUNT   (I2)
        //*  75T #L-TRANS-COUNT   (I3)
        //*  96T #L-TRANS-COUNT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-CLASSIC-AMT   (I2)
        //*  68T #L-CLASSIC-AMT   (I3)
        //*  89T #L-CLASSIC-AMT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-ROTH-AMT      (I2)
        //*  68T #L-ROTH-AMT      (I3)
        //*  89T #L-ROTH-AMT      (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-ROLLOVER-AMT  (I2)
        //*  68T #L-ROLLOVER-AMT  (I3)
        //*  89T #L-ROLLOVER-AMT  (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-ROTH-CONV-AMT (I2)
        //*  68T #L-ROTH-CONV-AMT (I3)
        //*  89T #L-ROTH-CONV-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-RECHAR-AMT    (I2)
        //*  68T #L-RECHAR-AMT    (I3)
        //*  89T #L-RECHAR-AMT    (I4)  SKIP (04) 1
        //*  07/03/06 RM
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-EDUCATION-AMT (I2)
        //*  68T #L-EDUCATION-AMT (I3)
        //*  89T #L-EDUCATION-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_L_Fmv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #L-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #L-FMV-AMT       (I2)
        //*  68T #L-FMV-AMT       (I3)
        //*  89T #L-FMV-AMT       (I4)  SKIP (04) 1
        //*                                               /* PALDE STARTS
        getReports().write(1, ReportOption.NOTITLE,"LATE RO AMT.........",new TabSetting(26),pnd_L_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'LATE RO AMT.........' 26T #L-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_L_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #L-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        //*                                                  /* PALDE ENDS
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TCII)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        //*  54T #S-TRANS-COUNT   (I2)
        //*  75T #S-TRANS-COUNT   (I3)
        //*  96T #S-TRANS-COUNT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));    //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-CLASSIC-AMT   (I2)
        //*  68T #S-CLASSIC-AMT   (I3)
        //*  89T #S-CLASSIC-AMT   (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));       //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-ROTH-AMT      (I2)
        //*  68T #S-ROTH-AMT      (I3)
        //*  89T #S-ROTH-AMT      (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));   //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-ROLLOVER-AMT  (I2)
        //*  68T #S-ROLLOVER-AMT  (I3)
        //*  89T #S-ROLLOVER-AMT  (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-ROTH-CONV-AMT (I2)
        //*  68T #S-ROTH-CONV-AMT (I3)
        //*  89T #S-ROTH-CONV-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));     //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-RECHAR-AMT    (I2)
        //*  68T #S-RECHAR-AMT    (I3)
        //*  89T #S-RECHAR-AMT    (I4)  SKIP (04) 1
        //*  07/03/06 RM
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-EDUCATION-AMT (I2)
        //*  68T #S-EDUCATION-AMT (I3)
        //*  89T #S-EDUCATION-AMT (I4)  SKIP (04) 1
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_S_Fmv_Amt.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));        //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #S-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  47T #S-FMV-AMT       (I2)
        //*  68T #S-FMV-AMT       (I3)
        //*  89T #S-FMV-AMT       (I4)  SKIP (04) 1
        //*                                               /* PALDE STARTS
        getReports().write(1, ReportOption.NOTITLE,"LATE RO AMT.........",new TabSetting(26),pnd_S_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'LATE RO AMT.........' 26T #S-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_S_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #S-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        //*                                                  /* PALDE ENDS
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_T_Trans_Count.getValue(5,":",8).nadd(pnd_T_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #T-TRANS-COUNT ( 1:4 ) TO #T-TRANS-COUNT ( 5:8 )
        pnd_T_Classic_Amt.getValue(5,":",8).nadd(pnd_T_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( 1:4 ) TO #T-CLASSIC-AMT ( 5:8 )
        pnd_T_Roth_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #T-ROTH-AMT ( 1:4 ) TO #T-ROTH-AMT ( 5:8 )
        pnd_T_Rollover_Amt.getValue(5,":",8).nadd(pnd_T_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( 1:4 ) TO #T-ROLLOVER-AMT ( 5:8 )
        pnd_T_Rechar_Amt.getValue(5,":",8).nadd(pnd_T_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #T-RECHAR-AMT ( 1:4 ) TO #T-RECHAR-AMT ( 5:8 )
        //*  07/03/06 RM
        pnd_T_Sep_Amt.getValue(5,":",8).nadd(pnd_T_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-SEP-AMT ( 1:4 ) TO #T-SEP-AMT ( 5:8 )
        pnd_T_Fmv_Amt.getValue(5,":",8).nadd(pnd_T_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #T-FMV-AMT ( 1:4 ) TO #T-FMV-AMT ( 5:8 )
        pnd_T_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_T_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #T-ROTH-CONV-AMT ( 1:4 ) TO #T-ROTH-CONV-AMT ( 5:8 )
        //*  PALDE
        pnd_T_Postpn_Amt.getValue(5,":",8).nadd(pnd_T_Postpn_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #T-POSTPN-AMT ( 1:4 ) TO #T-POSTPN-AMT ( 5:8 )
        //*  PALDE
        pnd_T_Repymnt_Amt.getValue(5,":",8).nadd(pnd_T_Repymnt_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #T-REPYMNT-AMT ( 1:4 ) TO #T-REPYMNT-AMT ( 5:8 )
        pnd_C_Trans_Count.getValue(5,":",8).nadd(pnd_C_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #C-TRANS-COUNT ( 1:4 ) TO #C-TRANS-COUNT ( 5:8 )
        pnd_C_Classic_Amt.getValue(5,":",8).nadd(pnd_C_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #C-CLASSIC-AMT ( 1:4 ) TO #C-CLASSIC-AMT ( 5:8 )
        pnd_C_Roth_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #C-ROTH-AMT ( 1:4 ) TO #C-ROTH-AMT ( 5:8 )
        pnd_C_Rollover_Amt.getValue(5,":",8).nadd(pnd_C_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #C-ROLLOVER-AMT ( 1:4 ) TO #C-ROLLOVER-AMT ( 5:8 )
        pnd_C_Rechar_Amt.getValue(5,":",8).nadd(pnd_C_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #C-RECHAR-AMT ( 1:4 ) TO #C-RECHAR-AMT ( 5:8 )
        //*  07/03/06 RM
        pnd_C_Sep_Amt.getValue(5,":",8).nadd(pnd_C_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-SEP-AMT ( 1:4 ) TO #C-SEP-AMT ( 5:8 )
        pnd_C_Fmv_Amt.getValue(5,":",8).nadd(pnd_C_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #C-FMV-AMT ( 1:4 ) TO #C-FMV-AMT ( 5:8 )
        pnd_C_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_C_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #C-ROTH-CONV-AMT ( 1:4 ) TO #C-ROTH-CONV-AMT ( 5:8 )
        //*  PALDE
        pnd_C_Postpn_Amt.getValue(5,":",8).nadd(pnd_C_Postpn_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #C-POSTPN-AMT ( 1:4 ) TO #C-POSTPN-AMT ( 5:8 )
        //*  PALDE
        pnd_C_Repymnt_Amt.getValue(5,":",8).nadd(pnd_C_Repymnt_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #C-REPYMNT-AMT ( 1:4 ) TO #C-REPYMNT-AMT ( 5:8 )
        pnd_L_Trans_Count.getValue(5,":",8).nadd(pnd_L_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #L-TRANS-COUNT ( 1:4 ) TO #L-TRANS-COUNT ( 5:8 )
        pnd_L_Classic_Amt.getValue(5,":",8).nadd(pnd_L_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #L-CLASSIC-AMT ( 1:4 ) TO #L-CLASSIC-AMT ( 5:8 )
        pnd_L_Roth_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #L-ROTH-AMT ( 1:4 ) TO #L-ROTH-AMT ( 5:8 )
        pnd_L_Rollover_Amt.getValue(5,":",8).nadd(pnd_L_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #L-ROLLOVER-AMT ( 1:4 ) TO #L-ROLLOVER-AMT ( 5:8 )
        pnd_L_Rechar_Amt.getValue(5,":",8).nadd(pnd_L_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #L-RECHAR-AMT ( 1:4 ) TO #L-RECHAR-AMT ( 5:8 )
        //*  07/03/06 RM
        pnd_L_Sep_Amt.getValue(5,":",8).nadd(pnd_L_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-SEP-AMT ( 1:4 ) TO #L-SEP-AMT ( 5:8 )
        pnd_L_Fmv_Amt.getValue(5,":",8).nadd(pnd_L_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #L-FMV-AMT ( 1:4 ) TO #L-FMV-AMT ( 5:8 )
        pnd_L_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_L_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #L-ROTH-CONV-AMT ( 1:4 ) TO #L-ROTH-CONV-AMT ( 5:8 )
        //*  PALDE
        pnd_L_Postpn_Amt.getValue(5,":",8).nadd(pnd_L_Postpn_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #L-POSTPN-AMT ( 1:4 ) TO #L-POSTPN-AMT ( 5:8 )
        //*  PALDE
        pnd_L_Repymnt_Amt.getValue(5,":",8).nadd(pnd_L_Repymnt_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #L-REPYMNT-AMT ( 1:4 ) TO #L-REPYMNT-AMT ( 5:8 )
        pnd_S_Trans_Count.getValue(5,":",8).nadd(pnd_S_Trans_Count.getValue(1,":",4));                                                                                    //Natural: ADD #S-TRANS-COUNT ( 1:4 ) TO #S-TRANS-COUNT ( 5:8 )
        pnd_S_Classic_Amt.getValue(5,":",8).nadd(pnd_S_Classic_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #S-CLASSIC-AMT ( 1:4 ) TO #S-CLASSIC-AMT ( 5:8 )
        pnd_S_Roth_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Amt.getValue(1,":",4));                                                                                          //Natural: ADD #S-ROTH-AMT ( 1:4 ) TO #S-ROTH-AMT ( 5:8 )
        pnd_S_Rollover_Amt.getValue(5,":",8).nadd(pnd_S_Rollover_Amt.getValue(1,":",4));                                                                                  //Natural: ADD #S-ROLLOVER-AMT ( 1:4 ) TO #S-ROLLOVER-AMT ( 5:8 )
        pnd_S_Rechar_Amt.getValue(5,":",8).nadd(pnd_S_Rechar_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #S-RECHAR-AMT ( 1:4 ) TO #S-RECHAR-AMT ( 5:8 )
        //*  07/03/06 RM
        pnd_S_Sep_Amt.getValue(5,":",8).nadd(pnd_S_Sep_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-SEP-AMT ( 1:4 ) TO #S-SEP-AMT ( 5:8 )
        pnd_S_Fmv_Amt.getValue(5,":",8).nadd(pnd_S_Fmv_Amt.getValue(1,":",4));                                                                                            //Natural: ADD #S-FMV-AMT ( 1:4 ) TO #S-FMV-AMT ( 5:8 )
        pnd_S_Roth_Conv_Amt.getValue(5,":",8).nadd(pnd_S_Roth_Conv_Amt.getValue(1,":",4));                                                                                //Natural: ADD #S-ROTH-CONV-AMT ( 1:4 ) TO #S-ROTH-CONV-AMT ( 5:8 )
        //*  PALDE
        pnd_S_Postpn_Amt.getValue(5,":",8).nadd(pnd_S_Postpn_Amt.getValue(1,":",4));                                                                                      //Natural: ADD #S-POSTPN-AMT ( 1:4 ) TO #S-POSTPN-AMT ( 5:8 )
        //*  PALDE
        pnd_S_Repymnt_Amt.getValue(5,":",8).nadd(pnd_S_Repymnt_Amt.getValue(1,":",4));                                                                                    //Natural: ADD #S-REPYMNT-AMT ( 1:4 ) TO #S-REPYMNT-AMT ( 5:8 )
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        //*   07/03/06         RM
        //*  PALDE
        //*  PALDE
        //*  PALDE
        pnd_T_Trans_Count.getValue(1,":",4).reset();                                                                                                                      //Natural: RESET #T-TRANS-COUNT ( 1:4 ) #C-TRANS-COUNT ( 1:4 ) #L-TRANS-COUNT ( 1:4 ) #T-CLASSIC-AMT ( 1:4 ) #C-CLASSIC-AMT ( 1:4 ) #L-CLASSIC-AMT ( 1:4 ) #T-ROTH-AMT ( 1:4 ) #C-ROTH-AMT ( 1:4 ) #L-ROTH-AMT ( 1:4 ) #T-ROLLOVER-AMT ( 1:4 ) #C-ROLLOVER-AMT ( 1:4 ) #L-ROLLOVER-AMT ( 1:4 ) #T-RECHAR-AMT ( 1:4 ) #C-RECHAR-AMT ( 1:4 ) #L-RECHAR-AMT ( 1:4 ) #T-SEP-AMT ( 1:4 ) #C-SEP-AMT ( 1:4 ) #L-SEP-AMT ( 1:4 ) #T-FMV-AMT ( 1:4 ) #C-FMV-AMT ( 1:4 ) #L-FMV-AMT ( 1:4 ) #T-ROTH-CONV-AMT ( 1:4 ) #C-ROTH-CONV-AMT ( 1:4 ) #L-ROTH-CONV-AMT ( 1:4 ) #S-TRANS-COUNT ( 1:4 ) #S-CLASSIC-AMT ( 1:4 ) #S-ROTH-AMT ( 1:4 ) #S-ROLLOVER-AMT ( 1:4 ) #S-RECHAR-AMT ( 1:4 ) #S-SEP-AMT ( 1:4 ) #S-FMV-AMT ( 1:4 ) #S-ROTH-CONV-AMT ( 1:4 ) #T-POSTPN-AMT ( 1:4 ) #T-REPYMNT-AMT ( 1:4 ) #S-POSTPN-AMT ( 1:4 ) #S-REPYMNT-AMT ( 1:4 ) #L-POSTPN-AMT ( 1:4 ) #L-REPYMNT-AMT ( 1:4 ) #C-POSTPN-AMT ( 1:4 ) #C-REPYMNT-AMT ( 1:4 )
        pnd_C_Trans_Count.getValue(1,":",4).reset();
        pnd_L_Trans_Count.getValue(1,":",4).reset();
        pnd_T_Classic_Amt.getValue(1,":",4).reset();
        pnd_C_Classic_Amt.getValue(1,":",4).reset();
        pnd_L_Classic_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Amt.getValue(1,":",4).reset();
        pnd_T_Rollover_Amt.getValue(1,":",4).reset();
        pnd_C_Rollover_Amt.getValue(1,":",4).reset();
        pnd_L_Rollover_Amt.getValue(1,":",4).reset();
        pnd_T_Rechar_Amt.getValue(1,":",4).reset();
        pnd_C_Rechar_Amt.getValue(1,":",4).reset();
        pnd_L_Rechar_Amt.getValue(1,":",4).reset();
        pnd_T_Sep_Amt.getValue(1,":",4).reset();
        pnd_C_Sep_Amt.getValue(1,":",4).reset();
        pnd_L_Sep_Amt.getValue(1,":",4).reset();
        pnd_T_Fmv_Amt.getValue(1,":",4).reset();
        pnd_C_Fmv_Amt.getValue(1,":",4).reset();
        pnd_L_Fmv_Amt.getValue(1,":",4).reset();
        pnd_T_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_C_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_L_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_S_Trans_Count.getValue(1,":",4).reset();
        pnd_S_Classic_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Amt.getValue(1,":",4).reset();
        pnd_S_Rollover_Amt.getValue(1,":",4).reset();
        pnd_S_Rechar_Amt.getValue(1,":",4).reset();
        pnd_S_Sep_Amt.getValue(1,":",4).reset();
        pnd_S_Fmv_Amt.getValue(1,":",4).reset();
        pnd_S_Roth_Conv_Amt.getValue(1,":",4).reset();
        pnd_T_Postpn_Amt.getValue(1,":",4).reset();
        pnd_T_Repymnt_Amt.getValue(1,":",4).reset();
        pnd_S_Postpn_Amt.getValue(1,":",4).reset();
        pnd_S_Repymnt_Amt.getValue(1,":",4).reset();
        pnd_L_Postpn_Amt.getValue(1,":",4).reset();
        pnd_L_Repymnt_Amt.getValue(1,":",4).reset();
        pnd_C_Postpn_Amt.getValue(1,":",4).reset();
        pnd_C_Repymnt_Amt.getValue(1,":",4).reset();
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(5);                                                                                                                                                   //Natural: ASSIGN I1 := 5
        i2.setValue(6);                                                                                                                                                   //Natural: ASSIGN I2 := 6
        i3.setValue(7);                                                                                                                                                   //Natural: ASSIGN I3 := 7
        i4.setValue(8);                                                                                                                                                   //Natural: ASSIGN I4 := 8
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, new TabSetting(1),"IRS Correction 5498 Records Read...........",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'IRS Correction 5498 Records Read...........' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"IRS Correction 5498 Records Read...........",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));       //Natural: WRITE ( 01 ) 01T 'IRS Correction 5498 Records Read...........' #READ-CTR
        if (Global.isEscape()) return;
        pnd_Total_B_Recs_Record.setValue(pnd_Read_Ctr);                                                                                                                   //Natural: ASSIGN #TOTAL-B-RECS-RECORD := #READ-CTR
        getWorkFiles().write(2, false, pnd_Total_B_Recs_Record);                                                                                                          //Natural: WRITE WORK FILE 02 #TOTAL-B-RECS-RECORD
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(48)," Tax Withholding & Reporting System ",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 48T ' Tax Withholding & Reporting System ' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),"IRS 5498 Corrections  Control Report",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T 'IRS 5498 Corrections  Control Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 01 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #HEADER-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"       Input        ");                                                                //Natural: WRITE ( 01 ) NOTITLE 22T '       Input        '
                    //*    43T '      Bypassed      '
                    //*    64T '      Rejected      '
                    //*    85T '      Accepted      '
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(22),"====================");                       //Natural: WRITE ( 01 ) NOTITLE 01T '                    ' 22T '===================='
                    //*    43T '===================='
                    //*    64T '===================='
                    //*    85T '===================='
                    //*   106T '===================='
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System: IR"));                                                                                          //Natural: COMPRESS 'Source System: IR' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
        i2.setValue(2);                                                                                                                                                   //Natural: ASSIGN I2 := 2
        i3.setValue(3);                                                                                                                                                   //Natural: ASSIGN I3 := 3
        i4.setValue(4);                                                                                                                                                   //Natural: ASSIGN I4 := 4
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
    }
}
