/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:55 PM
**        * FROM NATURAL PROGRAM : Twrp0908
************************************************************
**        * FILE NAME            : Twrp0908.java
**        * CLASS NAME           : Twrp0908
**        * INSTANCE NAME        : Twrp0908
************************************************************
********************************************************************
* PROGRAM : TWRP0908
* FUNCTION: CREATES A FLAT FILE (FLAT FILE2) WITH ATRA 1035 INTERNAL
*              PAYMENTS FOR OMNIPLUS AND OMNIPAY RECONCILIATION
* INPUT   : FLAT FILE1 CREATED BY TWRP0900 FOR CURRENT AND PRIOR YEAR
* OUTPUT  : FLAT FILE2 EXTRACT FOR RECONCILIATION
* AUTHOR  : J. OSTEEN 7/15/2008
* HISTORY :
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0908 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_parti;
    private DbsField parti_Twrparti_Tax_Id;
    private DbsField parti_Twrparti_Pin;
    private DbsField pnd_Atra_Recon;

    private DbsGroup pnd_Atra_Recon__R_Field_1;
    private DbsField pnd_Atra_Recon_Pnd_Plan;
    private DbsField pnd_Atra_Recon_Pnd_Ssn;

    private DbsGroup pnd_Atra_Recon__R_Field_2;
    private DbsField pnd_Atra_Recon_Pnd_Nssn;
    private DbsField pnd_Atra_Recon_Pnd_Sub_Plan;
    private DbsField pnd_Atra_Recon_Pnd_Trade_Date;

    private DbsGroup pnd_Atra_Recon__R_Field_3;
    private DbsField pnd_Atra_Recon_Pnd_Ccyy;
    private DbsField pnd_Atra_Recon_Pnd_Mmdd;
    private DbsField pnd_Atra_Recon_Pnd_Contract_Number;
    private DbsField pnd_Atra_Recon_Pnd_Pin;
    private DbsField pnd_Atra_Recon_Pnd_Gross_Amount;
    private DbsField pnd_Atra_Recon_Pnd_Filler;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Print;
    private DbsField pnd_Num_Bad_Ssn;
    private DbsField pnd_Num_Payment_Current;
    private DbsField pnd_Num_Payment_Prior;
    private DbsField pnd_I;
    private DbsField pnd_Process_Year;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Write_Current;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables
        localVariables = new DbsRecord();

        vw_parti = new DataAccessProgramView(new NameInfo("vw_parti", "PARTI"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        parti_Twrparti_Tax_Id = vw_parti.getRecord().newFieldInGroup("parti_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");
        parti_Twrparti_Pin = vw_parti.getRecord().newFieldInGroup("parti_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "TWRPARTI_PIN");
        registerRecord(vw_parti);

        pnd_Atra_Recon = localVariables.newFieldInRecord("pnd_Atra_Recon", "#ATRA-RECON", FieldType.STRING, 80);

        pnd_Atra_Recon__R_Field_1 = localVariables.newGroupInRecord("pnd_Atra_Recon__R_Field_1", "REDEFINE", pnd_Atra_Recon);
        pnd_Atra_Recon_Pnd_Plan = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Plan", "#PLAN", FieldType.STRING, 6);
        pnd_Atra_Recon_Pnd_Ssn = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Ssn", "#SSN", FieldType.STRING, 9);

        pnd_Atra_Recon__R_Field_2 = pnd_Atra_Recon__R_Field_1.newGroupInGroup("pnd_Atra_Recon__R_Field_2", "REDEFINE", pnd_Atra_Recon_Pnd_Ssn);
        pnd_Atra_Recon_Pnd_Nssn = pnd_Atra_Recon__R_Field_2.newFieldInGroup("pnd_Atra_Recon_Pnd_Nssn", "#NSSN", FieldType.NUMERIC, 9);
        pnd_Atra_Recon_Pnd_Sub_Plan = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Sub_Plan", "#SUB-PLAN", FieldType.STRING, 6);
        pnd_Atra_Recon_Pnd_Trade_Date = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Trade_Date", "#TRADE-DATE", FieldType.STRING, 8);

        pnd_Atra_Recon__R_Field_3 = pnd_Atra_Recon__R_Field_1.newGroupInGroup("pnd_Atra_Recon__R_Field_3", "REDEFINE", pnd_Atra_Recon_Pnd_Trade_Date);
        pnd_Atra_Recon_Pnd_Ccyy = pnd_Atra_Recon__R_Field_3.newFieldInGroup("pnd_Atra_Recon_Pnd_Ccyy", "#CCYY", FieldType.STRING, 4);
        pnd_Atra_Recon_Pnd_Mmdd = pnd_Atra_Recon__R_Field_3.newFieldInGroup("pnd_Atra_Recon_Pnd_Mmdd", "#MMDD", FieldType.STRING, 4);
        pnd_Atra_Recon_Pnd_Contract_Number = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 
            8);
        pnd_Atra_Recon_Pnd_Pin = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Pin", "#PIN", FieldType.STRING, 12);
        pnd_Atra_Recon_Pnd_Gross_Amount = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.NUMERIC, 
            11, 2);
        pnd_Atra_Recon_Pnd_Filler = pnd_Atra_Recon__R_Field_1.newFieldInGroup("pnd_Atra_Recon_Pnd_Filler", "#FILLER", FieldType.STRING, 20);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Print = localVariables.newFieldInRecord("pnd_Rec_Print", "#REC-PRINT", FieldType.PACKED_DECIMAL, 9);
        pnd_Num_Bad_Ssn = localVariables.newFieldInRecord("pnd_Num_Bad_Ssn", "#NUM-BAD-SSN", FieldType.PACKED_DECIMAL, 9);
        pnd_Num_Payment_Current = localVariables.newFieldInRecord("pnd_Num_Payment_Current", "#NUM-PAYMENT-CURRENT", FieldType.PACKED_DECIMAL, 9);
        pnd_Num_Payment_Prior = localVariables.newFieldInRecord("pnd_Num_Payment_Prior", "#NUM-PAYMENT-PRIOR", FieldType.PACKED_DECIMAL, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Process_Year = localVariables.newFieldInRecord("pnd_Process_Year", "#PROCESS-YEAR", FieldType.STRING, 4);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Write_Current = localVariables.newFieldInRecord("pnd_Write_Current", "#WRITE-CURRENT", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_parti.reset();

        ldaTwrl0900.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0908() throws Exception
    {
        super("Twrp0908");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        pnd_Process_Year.setValueEdited(Global.getDATX(),new ReportEditMask("YYYY"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYY ) TO #PROCESS-YEAR
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 RECORD #XTAXYR-F94
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            if (condition((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue("*").equals("OP") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue("*").equals("C"))  //Natural: IF TWRPYMNT-ORGN-SRCE-CDE ( * ) = 'OP' AND TWRPYMNT-PAYMENT-TYPE ( * ) = 'C' AND TWRPYMNT-SETTLE-TYPE ( * ) = 'X' AND ( #CHECK-REASON ( * ) = 'CY' OR = '  ' )
                && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue("*").equals("X")) && (ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Check_Reason().getValue("*").equals("CY") 
                || ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Check_Reason().getValue("*").equals("  ")))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                                     //Natural: ASSIGN #TAX-YEAR := TWRPYMNT-TAX-YEAR
            pnd_Atra_Recon_Pnd_Ssn.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                                         //Natural: ASSIGN #SSN := TWRPYMNT-TAX-ID-NBR
            pnd_Atra_Recon_Pnd_Pin.reset();                                                                                                                               //Natural: RESET #PIN
            vw_parti.startDatabaseRead                                                                                                                                    //Natural: READ PARTI BY TWRPARTI-CURR-REC-SD STARTING FROM TWRPYMNT-TAX-ID-NBR
            (
            "READ02",
            new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr(), WcType.BY) },
            new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") }
            );
            READ02:
            while (condition(vw_parti.readNextRow("READ02")))
            {
                if (condition(parti_Twrparti_Tax_Id.notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr())))                                                      //Natural: IF TWRPARTI-TAX-ID NE TWRPYMNT-TAX-ID-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(parti_Twrparti_Pin.notEquals(" ")))                                                                                                         //Natural: IF TWRPARTI-PIN NE ' '
                {
                    pnd_Atra_Recon_Pnd_Pin.setValue(parti_Twrparti_Pin);                                                                                                  //Natural: ASSIGN #PIN := TWRPARTI-PIN
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Atra_Recon_Pnd_Pin.equals(" ")))                                                                                                            //Natural: IF #PIN = ' '
            {
                getReports().write(0, "SSN NOT FOUND",pnd_Atra_Recon_Pnd_Ssn,"CONTRACT NUMBER",ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                    //Natural: WRITE 'SSN NOT FOUND' #SSN 'CONTRACT NUMBER' #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Num_Bad_Ssn.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NUM-BAD-SSN
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  RESET   LDAA1660
            //*  LDAA1660.#A-SSN      := #NSSN
            //*  CALLNAT  'LDAN1660' LDAA1660
            //*  IF LDAA1660.#A-RTN-CD              NE  ' '
            //*    WRITE 'SSN NOT FOUND' #SSN
            //*    ADD 1 TO #NUM-BAD-SSN
            //*    ESCAPE TOP
            //*  END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO #C-TWRPYMNT-PAYMENTS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_I.nadd(1))
            {
                if (condition((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("OP") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_I).equals("C"))  //Natural: IF TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'OP' AND TWRPYMNT-PAYMENT-TYPE ( #I ) = 'C' AND TWRPYMNT-SETTLE-TYPE ( #I ) = 'X' AND ( #CHECK-REASON ( #I ) = 'CY' OR = '  ' )
                    && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_I).equals("X")) && (ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Check_Reason().getValue(pnd_I).equals("CY") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Check_Reason().getValue(pnd_I).equals("  ")))))
                {
                    if (condition(!ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Filler().getValue(pnd_I).getSubstring(3,4).equals(" ")))                                             //Natural: IF SUBSTRING ( #XTAXYR-F94.#FILLER ( #I ) ,3,4 ) NE ' '
                    {
                        pnd_Atra_Recon_Pnd_Trade_Date.setValueEdited(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Trade_Date().getValue(pnd_I),new ReportEditMask("YYYYMMDD"));      //Natural: MOVE EDITED #XTAXYR-F94.#TRADE-DATE ( #I ) ( EM = YYYYMMDD ) TO #ATRA-RECON.#TRADE-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Atra_Recon_Pnd_Trade_Date.reset();                                                                                                            //Natural: RESET #ATRA-RECON.#TRADE-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet204 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CCYY = #PROCESS-YEAR
                    if (condition(pnd_Atra_Recon_Pnd_Ccyy.equals(pnd_Process_Year)))
                    {
                        decideConditionsMet204++;
                        pnd_Write_Current.setValue(true);                                                                                                                 //Natural: ASSIGN #WRITE-CURRENT := TRUE
                    }                                                                                                                                                     //Natural: WHEN #CCYY = ' ' AND #TAX-YEAR = #PROCESS-YEAR
                    else if (condition(pnd_Atra_Recon_Pnd_Ccyy.equals(" ") && pnd_Tax_Year.equals(pnd_Process_Year)))
                    {
                        decideConditionsMet204++;
                        pnd_Write_Current.setValue(true);                                                                                                                 //Natural: ASSIGN #WRITE-CURRENT := TRUE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Write_Current.setValue(false);                                                                                                                //Natural: ASSIGN #WRITE-CURRENT := FALSE
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*      IF #CCYY = #PROCESS-YEAR OR = ' '
                    //*      ADD 1 TO #NUM-PAYMENT-WRITTEN
                    pnd_Atra_Recon_Pnd_Plan.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_I));                                                      //Natural: ASSIGN #PLAN := #XTAXYR-F94.#OMNI-PLAN ( #I )
                    pnd_Atra_Recon_Pnd_Sub_Plan.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_I));                                               //Natural: ASSIGN #SUB-PLAN := #XTAXYR-F94.#OMNI-SUBPLAN ( #I )
                    pnd_Atra_Recon_Pnd_Contract_Number.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                   //Natural: ASSIGN #CONTRACT-NUMBER := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                    //*      #PIN := #A-PIN
                    pnd_Atra_Recon_Pnd_Gross_Amount.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_I));                                         //Natural: ASSIGN #GROSS-AMOUNT := #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I )
                    if (condition(pnd_Write_Current.getBoolean()))                                                                                                        //Natural: IF #WRITE-CURRENT
                    {
                        pnd_Num_Payment_Current.nadd(1);                                                                                                                  //Natural: ADD 1 TO #NUM-PAYMENT-CURRENT
                        getWorkFiles().write(2, false, pnd_Atra_Recon);                                                                                                   //Natural: WRITE WORK FILE 2 #ATRA-RECON
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Num_Payment_Prior.nadd(1);                                                                                                                    //Natural: ADD 1 TO #NUM-PAYMENT-PRIOR
                        getWorkFiles().write(3, false, pnd_Atra_Recon);                                                                                                   //Natural: WRITE WORK FILE 3 #ATRA-RECON
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(1, "OMNI/PLAN",                                                                                                                  //Natural: DISPLAY ( 1 ) 'OMNI/PLAN' #PLAN '/SSN' #SSN 'OMNI/SUB-PLAN' #SUB-PLAN 'TRADE/DATE' #ATRA-RECON.#TRADE-DATE 'CONTRACT/NUMBER' #CONTRACT-NUMBER '/PIN' #PIN 'GROSS/AMOUNT' #GROSS-AMOUNT
                    		pnd_Atra_Recon_Pnd_Plan,"/SSN",
                    		pnd_Atra_Recon_Pnd_Ssn,"OMNI/SUB-PLAN",
                    		pnd_Atra_Recon_Pnd_Sub_Plan,"TRADE/DATE",
                    		pnd_Atra_Recon_Pnd_Trade_Date,"CONTRACT/NUMBER",
                    		pnd_Atra_Recon_Pnd_Contract_Number,"/PIN",
                    		pnd_Atra_Recon_Pnd_Pin,"GROSS/AMOUNT",
                    		pnd_Atra_Recon_Pnd_Gross_Amount);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,"             Input Records Read:",pnd_Rec_Read,NEWLINE,"           Records With Bad SSN:",pnd_Num_Bad_Ssn,                 //Natural: WRITE // '             Input Records Read:' #REC-READ / '           Records With Bad SSN:' #NUM-BAD-SSN / 'Current Year Payments Extracted:' #NUM-PAYMENT-CURRENT / '  Prior Year Payments Extracted:' #NUM-PAYMENT-PRIOR
            NEWLINE,"Current Year Payments Extracted:",pnd_Num_Payment_Current,NEWLINE,"  Prior Year Payments Extracted:",pnd_Num_Payment_Prior);
        if (Global.isEscape()) return;
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 37T 'EXTRACTED 1035 INTERNAL RECORDS' / 'RUNTIME : ' *TIMX 44T 'TRADE YEAR ' *DATX ( EM = YYYY ) / 41T 'DETAIL AND CONTROL TOTALS'
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(37),"EXTRACTED 1035 INTERNAL RECORDS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(44),"TRADE YEAR ",Global.getDATX(), 
                        new ReportEditMask ("YYYY"),NEWLINE,new TabSetting(41),"DETAIL AND CONTROL TOTALS");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "OMNI/PLAN",
        		pnd_Atra_Recon_Pnd_Plan,"/SSN",
        		pnd_Atra_Recon_Pnd_Ssn,"OMNI/SUB-PLAN",
        		pnd_Atra_Recon_Pnd_Sub_Plan,"TRADE/DATE",
        		pnd_Atra_Recon_Pnd_Trade_Date,"CONTRACT/NUMBER",
        		pnd_Atra_Recon_Pnd_Contract_Number,"/PIN",
        		pnd_Atra_Recon_Pnd_Pin,"GROSS/AMOUNT",
        		pnd_Atra_Recon_Pnd_Gross_Amount);
    }
}
