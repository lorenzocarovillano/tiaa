/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:25 PM
**        * FROM NATURAL PROGRAM : Twrp3060
************************************************************
**        * FILE NAME            : Twrp3060.java
**        * CLASS NAME           : Twrp3060
**        * INSTANCE NAME        : Twrp3060
************************************************************
************************************************************************
* PROGRAM   : TWRP3060 - CALIFORNIA ST TAX WITHHOLDING QTRLY REPORTING
*           : RENAMED FROM NPDPCAT2
* FUNCTION  : CALCULATES #YTD-ST-TAX(#REPORTING-PERIOD) UPON DETAIL
*             RECORDS EXTRACTED FROM NPD FOR EACH UNIQUE VALUE OF
*             COMP/SSN/PPCN/PY
*           : PRODUCES OUTPUT WORK FILE 2 & A MISSING NAMES ERROR REPORT
*             RECORDS IN ERROR REPORT ARE REJECTED FROM FURTHER PROCESS.
*
* DATE      : 08/05/1997 CREATED BY LIN ZHENG
*
* HISTORY   :
* UPDATE    : ADDED MAJOR SORT KEY -  BY COMPANY
*             TO REMOVE PRODUCT FIELD ON THE REPORT.
*             TO PRINT QUARTER NO. & NAME OF COMPANY IN THE HEADER
*             & REALIGN 'TOTAL AMOUNT REJECTED'   (EDITH 2/98)
*
*           : CHANGE PARAMETER CARD (3/29/99)
*           : INTERFACE-DATE-TO IS EQUAL TO THE RUNDATE (EDITH 4/13/99)
*           : ADDED CONTROL IN CASE OF ABEND  6/11/99
* 03/02/04 RM - CHANGE REPORTS TO COUNT ALL RECORDS AS TCII RECORDS.
*               THE OUTPUT FIELD '#TIAA-CREF' HAS BEEN REASSIGNED WITH
*               A SORT CODE OF '1'
* 03/31/04 RM - CHANGE REPORTS TO COUNT ALL RECORDS AS TIAA RECORDS.
* 11/05/04: TOPS RELEASE 3 CHANGES    RM
*           ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY. REVERT BACK TO
*           USE COMPANY BREAK LOGIC. THEN COUNT ALL OLD COMPANY CODES
*           'T' 'C' 'L' 'S' AS TIAA RECORDS(SORT FLD '#TIAA-CREF' = '1')
*           AND THE NEW COMPANY CODE 'X' AS TRUST RECORDS(S FLD = '2').
* 10/19/11  MB  ADDED NEW COMPANY CODE 'F'               /* MB
* 30/06/15 PAULS - CHANGE THE SUPER DESCRIPTOR FROM
*                  TWRPARTI-TAXID-START-SD TO
*                  TWRPARTI-CURR-INVRSE-SD
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3060 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_f95_Parti_View;
    private DbsField f95_Parti_View_Twrparti_Tax_Id;
    private DbsField f95_Parti_View_Twrparti_Participant_Name;
    private DbsField f95_Parti_View_Twrparti_Curr_Invrse_Sd;

    private DbsGroup pnd_Qtrly_Rec;
    private DbsField pnd_Qtrly_Rec_Pnd_Tiaa_Cref;
    private DbsField pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Qtrly_Rec_Form_Srce_Cde;
    private DbsField pnd_Qtrly_Rec_Ss_Employee_Name;
    private DbsField pnd_Qtrly_Rec_Pnd_Qtr_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Ytd_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Adj_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Diff_St_Tax;

    private DbsGroup pnd_Detail_Rec;
    private DbsField pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Detail_Rec_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Detail_Rec__R_Field_1;
    private DbsField pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Detail_Rec_Form_Pymnt_Dte;
    private DbsField pnd_Detail_Rec_Form_Srce_Cde;
    private DbsField pnd_Detail_Rec_Form_Prdct_Cde;
    private DbsField pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt;

    private DbsGroup pnd_Qtrly_Rec_Cref;
    private DbsField pnd_Qtrly_Rec_Cref_Pnd_Ytd_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Life;
    private DbsField pnd_Qtrly_Rec_Life_Pnd_Ytd_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Tcii;
    private DbsField pnd_Qtrly_Rec_Tcii_Pnd_Ytd_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Tiaa;
    private DbsField pnd_Qtrly_Rec_Tiaa_Pnd_Ytd_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Trst;
    private DbsField pnd_Qtrly_Rec_Trst_Pnd_Ytd_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Fsb;
    private DbsField pnd_Qtrly_Rec_Fsb_Pnd_Ytd_St_Tax;
    private DbsField pnd_P_Key_Start;

    private DbsGroup pnd_P_Key_Start__R_Field_2;
    private DbsField pnd_P_Key_Start_Pnd_P_Key_Start_Num;
    private DbsField pnd_Prtcpnt_Name;
    private DbsField pnd_Tot_Cnt_Err;
    private DbsField pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt;
    private DbsField pnd_Tot_Cnt_Input;
    private DbsField pnd_Tot_Cnt_Output;
    private DbsField pnd_Reporting_Period;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_3;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_4;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Run_Type;
    private DbsField pnd_Prt_Co;
    private DbsField pnd_Sv_Co;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Form_Prdct_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_f95_Parti_View = new DataAccessProgramView(new NameInfo("vw_f95_Parti_View", "F95-PARTI-VIEW"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        f95_Parti_View_Twrparti_Tax_Id = vw_f95_Parti_View.getRecord().newFieldInGroup("f95_Parti_View_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        f95_Parti_View_Twrparti_Participant_Name = vw_f95_Parti_View.getRecord().newFieldInGroup("f95_Parti_View_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        f95_Parti_View_Twrparti_Curr_Invrse_Sd = vw_f95_Parti_View.getRecord().newFieldInGroup("f95_Parti_View_Twrparti_Curr_Invrse_Sd", "TWRPARTI-CURR-INVRSE-SD", 
            FieldType.STRING, 19, RepeatingFieldStrategy.None, "TWRPARTI_CURR_INVRSE_SD");
        f95_Parti_View_Twrparti_Curr_Invrse_Sd.setSuperDescriptor(true);
        registerRecord(vw_f95_Parti_View);

        pnd_Qtrly_Rec = localVariables.newGroupInRecord("pnd_Qtrly_Rec", "#QTRLY-REC");
        pnd_Qtrly_Rec_Pnd_Tiaa_Cref = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 1);
        pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Qtrly_Rec_Form_Srce_Cde = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Qtrly_Rec_Ss_Employee_Name = pnd_Qtrly_Rec.newFieldInGroup("pnd_Qtrly_Rec_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 27);
        pnd_Qtrly_Rec_Pnd_Qtr_St_Tax = pnd_Qtrly_Rec.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Adj_St_Tax = pnd_Qtrly_Rec.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Diff_St_Tax = pnd_Qtrly_Rec.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 5));

        pnd_Detail_Rec = localVariables.newGroupInRecord("pnd_Detail_Rec", "#DETAIL-REC");
        pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Detail_Rec_Form_Cntrct_Py_Nmbr = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", FieldType.STRING, 
            10);

        pnd_Detail_Rec__R_Field_1 = pnd_Detail_Rec.newGroupInGroup("pnd_Detail_Rec__R_Field_1", "REDEFINE", pnd_Detail_Rec_Form_Cntrct_Py_Nmbr);
        pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Detail_Rec__R_Field_1.newFieldInGroup("pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde = pnd_Detail_Rec__R_Field_1.newFieldInGroup("pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Detail_Rec_Form_Pymnt_Dte = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_Dte", "FORM-PYMNT-DTE", FieldType.NUMERIC, 8);
        pnd_Detail_Rec_Form_Srce_Cde = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Detail_Rec_Form_Prdct_Cde = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Prdct_Cde", "FORM-PRDCT-CDE", FieldType.STRING, 1);
        pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt = pnd_Detail_Rec.newFieldInGroup("pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt", "FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Qtrly_Rec_Cref = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Cref", "#QTRLY-REC-CREF");
        pnd_Qtrly_Rec_Cref_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Cref.newFieldInGroup("pnd_Qtrly_Rec_Cref_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Qtrly_Rec_Life = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Life", "#QTRLY-REC-LIFE");
        pnd_Qtrly_Rec_Life_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Life.newFieldInGroup("pnd_Qtrly_Rec_Life_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Qtrly_Rec_Tcii = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Tcii", "#QTRLY-REC-TCII");
        pnd_Qtrly_Rec_Tcii_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Tcii.newFieldInGroup("pnd_Qtrly_Rec_Tcii_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Qtrly_Rec_Tiaa = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Tiaa", "#QTRLY-REC-TIAA");
        pnd_Qtrly_Rec_Tiaa_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Tiaa.newFieldInGroup("pnd_Qtrly_Rec_Tiaa_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Qtrly_Rec_Trst = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Trst", "#QTRLY-REC-TRST");
        pnd_Qtrly_Rec_Trst_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Trst.newFieldInGroup("pnd_Qtrly_Rec_Trst_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Qtrly_Rec_Fsb = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Fsb", "#QTRLY-REC-FSB");
        pnd_Qtrly_Rec_Fsb_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Fsb.newFieldInGroup("pnd_Qtrly_Rec_Fsb_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_P_Key_Start = localVariables.newFieldInRecord("pnd_P_Key_Start", "#P-KEY-START", FieldType.STRING, 10);

        pnd_P_Key_Start__R_Field_2 = localVariables.newGroupInRecord("pnd_P_Key_Start__R_Field_2", "REDEFINE", pnd_P_Key_Start);
        pnd_P_Key_Start_Pnd_P_Key_Start_Num = pnd_P_Key_Start__R_Field_2.newFieldInGroup("pnd_P_Key_Start_Pnd_P_Key_Start_Num", "#P-KEY-START-NUM", FieldType.NUMERIC, 
            9);
        pnd_Prtcpnt_Name = localVariables.newFieldInRecord("pnd_Prtcpnt_Name", "#PRTCPNT-NAME", FieldType.STRING, 27);
        pnd_Tot_Cnt_Err = localVariables.newFieldInRecord("pnd_Tot_Cnt_Err", "#TOT-CNT-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt = localVariables.newFieldInRecord("pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt", "#TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Cnt_Input = localVariables.newFieldInRecord("pnd_Tot_Cnt_Input", "#TOT-CNT-INPUT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Cnt_Output = localVariables.newFieldInRecord("pnd_Tot_Cnt_Output", "#TOT-CNT-OUTPUT", FieldType.PACKED_DECIMAL, 9);
        pnd_Reporting_Period = localVariables.newFieldInRecord("pnd_Reporting_Period", "#REPORTING-PERIOD", FieldType.INTEGER, 1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_3 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_3", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_4 = pnd_Input_Parm__R_Field_3.newGroupInGroup("pnd_Input_Parm__R_Field_4", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        pnd_Prt_Co = localVariables.newFieldInRecord("pnd_Prt_Co", "#PRT-CO", FieldType.STRING, 4);
        pnd_Sv_Co = localVariables.newFieldInRecord("pnd_Sv_Co", "#SV-CO", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Form_Prdct_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Form_Prdct_Cde_OLD", "Form_Prdct_Cde_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_f95_Parti_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3060() throws Exception
    {
        super("Twrp3060");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3060|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                getReports().definePrinter(2, "PRT");                                                                                                                     //Natural: DEFINE PRINTER ( PRT = 1 ) OUTPUT 'CMPRT01'
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    pnd_Reporting_Period.setValue(5);                                                                                                                     //Natural: ASSIGN #REPORTING-PERIOD := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*                                                                                                                                                   //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM.#PYMNT-DATE-TO-MM
                    short decideConditionsMet145 = 0;                                                                                                                     //Natural: VALUE 03
                    if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
                    {
                        decideConditionsMet145++;
                        pnd_Reporting_Period.setValue(1);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 1
                    }                                                                                                                                                     //Natural: VALUE 06
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
                    {
                        decideConditionsMet145++;
                        pnd_Reporting_Period.setValue(2);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 2
                    }                                                                                                                                                     //Natural: VALUE 09
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
                    {
                        decideConditionsMet145++;
                        pnd_Reporting_Period.setValue(3);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 3
                    }                                                                                                                                                     //Natural: VALUE 12
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
                    {
                        decideConditionsMet145++;
                        pnd_Reporting_Period.setValue(4);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 4
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!",NEWLINE,"ERROR: #INPUT-PARM CONTAINS INVALID","COMBINATION OF INPUT VALUES", //Natural: WRITE '!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!' / 'ERROR: #INPUT-PARM CONTAINS INVALID' 'COMBINATION OF INPUT VALUES' / 'PLEASE CONTACT SYSTEM SUPPORT'
                            NEWLINE,"PLEASE CONTACT SYSTEM SUPPORT");
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 1 RECORD #DETAIL-REC
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pnd_Detail_Rec)))
                {
                    CheckAtStartofData168();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //* *---------                                                                                                                                        //Natural: AT START OF DATA
                    pnd_Tot_Cnt_Input.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOT-CNT-INPUT
                    //*  MB
                    //*                                                                                                                                                   //Natural: DECIDE ON FIRST VALUE OF #DETAIL-REC.FORM-PRDCT-CDE
                    short decideConditionsMet180 = 0;                                                                                                                     //Natural: VALUE 'C'
                    if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("C"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Cref_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                  //Natural: ASSIGN #QTRLY-REC-CREF.#YTD-ST-TAX := #QTRLY-REC-CREF.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    else if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("L"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Life_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                  //Natural: ASSIGN #QTRLY-REC-LIFE.#YTD-ST-TAX := #QTRLY-REC-LIFE.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: VALUE 'S'
                    else if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("S"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Tcii_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                  //Natural: ASSIGN #QTRLY-REC-TCII.#YTD-ST-TAX := #QTRLY-REC-TCII.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: VALUE 'T'
                    else if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("T"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Tiaa_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                  //Natural: ASSIGN #QTRLY-REC-TIAA.#YTD-ST-TAX := #QTRLY-REC-TIAA.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: VALUE 'X'
                    else if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("X"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Trst_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                  //Natural: ASSIGN #QTRLY-REC-TRST.#YTD-ST-TAX := #QTRLY-REC-TRST.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: VALUE 'F'
                    else if (condition((pnd_Detail_Rec_Form_Prdct_Cde.equals("F"))))
                    {
                        decideConditionsMet180++;
                        pnd_Qtrly_Rec_Fsb_Pnd_Ytd_St_Tax.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                                   //Natural: ASSIGN #QTRLY-REC-FSB.#YTD-ST-TAX := #QTRLY-REC-FSB.#YTD-ST-TAX + #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Qtrly_Rec.setValuesByName(pnd_Detail_Rec);                                                                                                        //Natural: MOVE BY NAME #DETAIL-REC TO #QTRLY-REC
                    //*                                                                                                                                                   //Natural: AT BREAK OF #DETAIL-REC.FORM-CNTRCT-PY-NMBR
                    //*                                                                                                                                                   //Natural: AT BREAK OF #DETAIL-REC.#ANNT-SOC-SEC-NBR
                    //*                                                                                                                                                   //Natural: AT BREAK OF #DETAIL-REC.FORM-PRDCT-CDE
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    if (condition(pnd_Prtcpnt_Name.equals(" ")))                                                                                                          //Natural: IF #PRTCPNT-NAME = ' '
                    {
                                                                                                                                                                          //Natural: PERFORM MISSING-NAME-DETAIL-ERROR-REPORT
                        sub_Missing_Name_Detail_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Tot_Cnt_Err.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOT-CNT-ERR
                        pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt.nadd(pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt);                                                               //Natural: ADD #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT TO #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    readWork01Form_Prdct_CdeOld.setValue(pnd_Detail_Rec_Form_Prdct_Cde);                                                                                  //Natural: END-WORK
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    getReports().eject(1, true);                                                                                                                          //Natural: EJECT ( 01 )
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"California Quarterly Tax Reporting",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'California Quarterly Tax Reporting' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 57T #RUN-TYPE / 54T 'S U M M A R Y   R E P O R T' /// 'TOTAL NUMBER OF RECORDS REJECTED : ' #TOT-CNT-ERR ( EM = Z,ZZZ,ZZ9 ) / 'TOTAL AMOUNT REJECTED            : ' 42T #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99- ) /// '(1) Total Number Of Input  Records : ' #TOT-CNT-INPUT ( EM = ZZZ,ZZZ,ZZ9 ) / '(2) Total Number Of Output Records : ' #TOT-CNT-OUTPUT ( EM = ZZZ,ZZZ,ZZ9 ) // 'Reconciliation Of Totals : ' / '(1) = Sum Of Total Number Of Records Accepted For Further' 'Processing Of TIAA (PROGRAM TWRP3050)' / '    = Summary Report Grandtotal (TWRP3050 REPORT 02)'
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,new TabSetting(57),pnd_Run_Type,NEWLINE,new 
                        TabSetting(54),"S U M M A R Y   R E P O R T",NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS REJECTED : ",pnd_Tot_Cnt_Err, new 
                        ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL AMOUNT REJECTED            : ",new TabSetting(42),pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,NEWLINE,"(1) Total Number Of Input  Records : ",pnd_Tot_Cnt_Input, new 
                        ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"(2) Total Number Of Output Records : ",pnd_Tot_Cnt_Output, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),
                        NEWLINE,NEWLINE,"Reconciliation Of Totals : ",NEWLINE,"(1) = Sum Of Total Number Of Records Accepted For Further","Processing Of TIAA (PROGRAM TWRP3050)",
                        NEWLINE,"    = Summary Report Grandtotal (TWRP3050 REPORT 02)");
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet328 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet328++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet328++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet328++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet328++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Ss_Employee_Name() throws Exception                                                                                                              //Natural: GET-SS-EMPLOYEE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_P_Key_Start.reset();                                                                                                                                          //Natural: RESET #P-KEY-START
        pnd_P_Key_Start_Pnd_P_Key_Start_Num.setValue(pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr);                                                                                //Natural: ASSIGN #P-KEY-START-NUM := #DETAIL-REC.#ANNT-SOC-SEC-NBR
                                                                                                                                                                          //Natural: PERFORM GET-PARTICIPANT-RECORD
        sub_Get_Participant_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Prtcpnt_Name.equals(" ")))                                                                                                                      //Natural: IF #PRTCPNT-NAME = ' '
        {
            setValueToSubstring(pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr,pnd_P_Key_Start,1,8);                                                                                  //Natural: MOVE #DETAIL-REC.#CNTRCT-PPCN-NBR TO SUBSTR ( #P-KEY-START,1,8 )
            setValueToSubstring(pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde,pnd_P_Key_Start,9,2);                                                                                 //Natural: MOVE #DETAIL-REC.#CNTRCT-PAYEE-CDE TO SUBSTR ( #P-KEY-START,9,2 )
                                                                                                                                                                          //Natural: PERFORM GET-PARTICIPANT-RECORD
            sub_Get_Participant_Record();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Participant_Record() throws Exception                                                                                                            //Natural: GET-PARTICIPANT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_Prtcpnt_Name.reset();                                                                                                                                         //Natural: RESET #PRTCPNT-NAME
        //* *READ (1)  F95-PARTI-VIEW  BY  TWRPARTI-TAXID-START-SD  =  #P-KEY-START
        //* PAULS
        //* PAULS
        vw_f95_Parti_View.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) F95-PARTI-VIEW BY TWRPARTI-CURR-INVRSE-SD = #P-KEY-START
        (
        "READ02",
        new Wc[] { new Wc("TWRPARTI_CURR_INVRSE_SD", ">=", pnd_P_Key_Start, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_INVRSE_SD", "ASC") },
        1
        );
        READ02:
        while (condition(vw_f95_Parti_View.readNextRow("READ02")))
        {
            if (condition(pnd_P_Key_Start.equals(f95_Parti_View_Twrparti_Tax_Id)))                                                                                        //Natural: IF #P-KEY-START = TWRPARTI-TAX-ID
            {
                pnd_Prtcpnt_Name.setValue(f95_Parti_View_Twrparti_Participant_Name);                                                                                      //Natural: ASSIGN #PRTCPNT-NAME := TWRPARTI-PARTICIPANT-NAME
                DbsUtil.examine(new ExamineSource(pnd_Prtcpnt_Name,true), new ExamineSearch("H'00'"), new ExamineDelete());                                               //Natural: EXAMINE FULL #PRTCPNT-NAME H'00' DELETE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Missing_Name_Detail_Error_Report() throws Exception                                                                                                  //Natural: MISSING-NAME-DETAIL-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------
        getReports().display(1, "SSN",                                                                                                                                    //Natural: DISPLAY ( 01 ) 'SSN' #DETAIL-REC.#ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'CONTRACT' #DETAIL-REC.#CNTRCT-PPCN-NBR 'PAYEE' #DETAIL-REC.#CNTRCT-PAYEE-CDE 'CHECK DTE' #DETAIL-REC.FORM-PYMNT-DTE 'SRCE CDE' #DETAIL-REC.FORM-SRCE-CDE 'STATE TAX' #DETAIL-REC.FORM-PYMNT-ST-WTHHLD-AMT ( EM = Z,ZZZ,ZZ9.99- )
        		pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
        		pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde,"CHECK DTE",
        		pnd_Detail_Rec_Form_Pymnt_Dte,"SRCE CDE",
        		pnd_Detail_Rec_Form_Srce_Cde,"STATE TAX",
        		pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Co_Name() throws Exception                                                                                                                           //Natural: CO-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------
        //*  MB
        short decideConditionsMet385 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-CO;//Natural: VALUE 'C'
        if (condition((pnd_Sv_Co.equals("C"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("CREF");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Sv_Co.equals("L"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("LIFE");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Sv_Co.equals("S"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("TCII");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Sv_Co.equals("T"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("TIAA");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Sv_Co.equals("X"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("TRST");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Sv_Co.equals("F"))))
        {
            decideConditionsMet385++;
            pnd_Prt_Co.setValue("FSB");                                                                                                                                   //Natural: ASSIGN #PRT-CO := 'FSB'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Prt_Co.setValue("OTHR");                                                                                                                                  //Natural: ASSIGN #PRT-CO := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Test_Print() throws Exception                                                                                                                        //Natural: TEST-PRINT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------
        getReports().display(0, "CNT",                                                                                                                                    //Natural: DISPLAY ( 00 ) 'CNT' #TOT-CNT-INPUT 'T/C' #TIAA-CREF 'SSS#' #QTRLY-REC.#ANNT-SOC-SEC-NBR 'CONTRACT#' #QTRLY-REC.#CNTRCT-PPCN-NBR 'PYE' #QTRLY-REC.#CNTRCT-PAYEE-CDE 'SRCDE' #QTRLY-REC.FORM-SRCE-CDE 'NAME' SS-EMPLOYEE-NAME 'QTR-ST-TAX' #QTR-ST-TAX ( 1:2 ) 'YTD-ST-TAX' #QTRLY-REC.#YTD-ST-TAX ( 1:2 ) 'ADJ-ST-TAX' #ADJ-ST-TAX ( 1:2 ) 'DIFF-ST-TAX' #DIFF-ST-TAX ( 1:2 )
        		pnd_Tot_Cnt_Input,"T/C",
        		pnd_Qtrly_Rec_Pnd_Tiaa_Cref,"SSS#",
        		pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr,"CONTRACT#",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr,"PYE",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde,"SRCDE",
        		pnd_Qtrly_Rec_Form_Srce_Cde,"NAME",
        		pnd_Qtrly_Rec_Ss_Employee_Name,"QTR-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax.getValue(1,":",2),"YTD-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(1,":",2),"ADJ-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax.getValue(1,":",2),"DIFF-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Diff_St_Tax.getValue(1,":",2));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-NAME
                    sub_Co_Name();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"CALIFORNIA QUARTERLY TAX REPORTING",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 50T 'CALIFORNIA QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 'REPORT 01' 48T 'MISSING PARTICIPANT NAMES ERROR REPORT' 110T 'PAGE:' *PAGE-NUMBER ( 1 ) / 57T #RUN-TYPE // 'COMPANY : ' #PRT-CO //
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,"REPORT 01",new TabSetting(48),"MISSING PARTICIPANT NAMES ERROR REPORT",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,new TabSetting(57),pnd_Run_Type,NEWLINE,NEWLINE,"COMPANY : ",pnd_Prt_Co,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Detail_Rec_Form_Cntrct_Py_NmbrIsBreak = pnd_Detail_Rec_Form_Cntrct_Py_Nmbr.isBreak(endOfData);
        boolean pnd_Detail_Rec_Pnd_Annt_Soc_Sec_NbrIsBreak = pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr.isBreak(endOfData);
        boolean pnd_Detail_Rec_Form_Prdct_CdeIsBreak = pnd_Detail_Rec_Form_Prdct_Cde.isBreak(endOfData);
        if (condition(pnd_Detail_Rec_Form_Cntrct_Py_NmbrIsBreak || pnd_Detail_Rec_Pnd_Annt_Soc_Sec_NbrIsBreak || pnd_Detail_Rec_Form_Prdct_CdeIsBreak))
        {
            if (condition(readWork01Form_Prdct_CdeOld.equals("T")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'T'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("1");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '1'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Tiaa_Pnd_Ytd_St_Tax);                                                  //Natural: MOVE #QTRLY-REC-TIAA.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(readWork01Form_Prdct_CdeOld.equals("C")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'C'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("1");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '1'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Cref_Pnd_Ytd_St_Tax);                                                  //Natural: MOVE #QTRLY-REC-CREF.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(readWork01Form_Prdct_CdeOld.equals("L")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'L'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("1");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '1'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Life_Pnd_Ytd_St_Tax);                                                  //Natural: MOVE #QTRLY-REC-LIFE.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(readWork01Form_Prdct_CdeOld.equals("S")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'S'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("1");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '1'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Tcii_Pnd_Ytd_St_Tax);                                                  //Natural: MOVE #QTRLY-REC-TCII.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(readWork01Form_Prdct_CdeOld.equals("X")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'X'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("2");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '2'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Trst_Pnd_Ytd_St_Tax);                                                  //Natural: MOVE #QTRLY-REC-TRST.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            //*  MB
            if (condition(readWork01Form_Prdct_CdeOld.equals("F")))                                                                                                       //Natural: IF OLD ( #DETAIL-REC.FORM-PRDCT-CDE ) = 'F'
            {
                pnd_Qtrly_Rec_Pnd_Tiaa_Cref.setValue("1");                                                                                                                //Natural: ASSIGN #QTRLY-REC.#TIAA-CREF := '1'
                pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Fsb_Pnd_Ytd_St_Tax);                                                   //Natural: MOVE #QTRLY-REC-FSB.#YTD-ST-TAX TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Qtrly_Rec_Ss_Employee_Name.setValue(pnd_Prtcpnt_Name);                                                                                                    //Natural: ASSIGN #QTRLY-REC.SS-EMPLOYEE-NAME := #PRTCPNT-NAME
            if (condition(pnd_Qtrly_Rec_Ss_Employee_Name.notEquals(" ")))                                                                                                 //Natural: IF #QTRLY-REC.SS-EMPLOYEE-NAME NE ' '
            {
                //* * AND #QTRLY-REC.#YTD-ST-TAX (#REPORTING-PERIOD)  NE  0
                getWorkFiles().write(2, false, pnd_Qtrly_Rec);                                                                                                            //Natural: WRITE WORK FILE 2 #QTRLY-REC
                pnd_Tot_Cnt_Output.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-CNT-OUTPUT
                //*       PERFORM TEST-PRINT     /* TEST ONLY
            }                                                                                                                                                             //Natural: END-IF
            //*  MB
            pnd_Qtrly_Rec.reset();                                                                                                                                        //Natural: RESET #QTRLY-REC #QTRLY-REC-TIAA #QTRLY-REC-CREF #QTRLY-REC-LIFE #QTRLY-REC-TCII #QTRLY-REC-TRST #QTRLY-REC-FSB
            pnd_Qtrly_Rec_Tiaa.reset();
            pnd_Qtrly_Rec_Cref.reset();
            pnd_Qtrly_Rec_Life.reset();
            pnd_Qtrly_Rec_Tcii.reset();
            pnd_Qtrly_Rec_Trst.reset();
            pnd_Qtrly_Rec_Fsb.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Detail_Rec_Pnd_Annt_Soc_Sec_NbrIsBreak || pnd_Detail_Rec_Form_Prdct_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-SS-EMPLOYEE-NAME
            sub_Get_Ss_Employee_Name();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*   BREAK OF COMPANY
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Detail_Rec_Form_Prdct_CdeIsBreak))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS REJECTED : ",pnd_Tot_Cnt_Err, new ReportEditMask                  //Natural: WRITE ( 01 ) /// 'TOTAL NUMBER OF RECORDS REJECTED : ' #TOT-CNT-ERR ( EM = Z,ZZZ,ZZ9 ) / 'TOTAL AMOUNT REJECTED            : ' 42T #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99- )
                ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL AMOUNT REJECTED            : ",new TabSetting(42),pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask 
                ("ZZ,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape())) return;
            pnd_Sv_Co.setValue(pnd_Detail_Rec_Form_Prdct_Cde);                                                                                                            //Natural: ASSIGN #SV-CO := #DETAIL-REC.FORM-PRDCT-CDE
            pnd_Tot_Cnt_Err.reset();                                                                                                                                      //Natural: RESET #TOT-CNT-ERR #TOT-ERR-FORM-PYMNT-ST-WTHHLD-AMT
            pnd_Tot_Err_Form_Pymnt_St_Wthhld_Amt.reset();
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "SSN",
        		pnd_Detail_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Detail_Rec_Pnd_Cntrct_Ppcn_Nbr,"PAYEE",
        		pnd_Detail_Rec_Pnd_Cntrct_Payee_Cde,"CHECK DTE",
        		pnd_Detail_Rec_Form_Pymnt_Dte,"SRCE CDE",
        		pnd_Detail_Rec_Form_Srce_Cde,"STATE TAX",
        		pnd_Detail_Rec_Form_Pymnt_St_Wthhld_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(0, "CNT",
        		pnd_Tot_Cnt_Input,"T/C",
        		pnd_Qtrly_Rec_Pnd_Tiaa_Cref,"SSS#",
        		pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr,"CONTRACT#",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr,"PYE",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde,"SRCDE",
        		pnd_Qtrly_Rec_Form_Srce_Cde,"NAME",
        		pnd_Qtrly_Rec_Ss_Employee_Name,"QTR-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax,"YTD-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax,"ADJ-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax,"DIFF-ST-TAX",
        		pnd_Qtrly_Rec_Pnd_Diff_St_Tax);
    }
    private void CheckAtStartofData168() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //* *----------------
                                                                                                                                                                          //Natural: PERFORM GET-SS-EMPLOYEE-NAME
            sub_Get_Ss_Employee_Name();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Sv_Co.setValue(pnd_Detail_Rec_Form_Prdct_Cde);                                                                                                            //Natural: ASSIGN #SV-CO := #DETAIL-REC.FORM-PRDCT-CDE
        }                                                                                                                                                                 //Natural: END-START
    }
}
