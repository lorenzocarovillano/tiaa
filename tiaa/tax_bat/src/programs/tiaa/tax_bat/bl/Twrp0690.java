/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:53 PM
**        * FROM NATURAL PROGRAM : Twrp0690
************************************************************
**        * FILE NAME            : Twrp0690.java
**        * CLASS NAME           : Twrp0690
**        * INSTANCE NAME        : Twrp0690
************************************************************
*********************************************************
** PROGRAM NAME:  TWRP0690
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ANATOLY
** PURPOSE     :  CREATES A REPORT OF MISSING DOB,
**             :  DEFAULTED PAYEES,
**             :  MISSING TAX ID FOR NON-CITIZENS
**             :  AND MISSING PIN's.
** INPUT       :  1.TRANSACTION FILE EXTRACT FROM FEEDERS
** (FILES)  -  :    (DETAIL) - FILE# 1
**-------------------------------------------------------
** OUTPUT      :
** (REPORTS) - :  MISSING FIELDS REPORTS (5)
** DATE        :  03/03/99
************************************************
** MODIFICATION LOG
************************************************
** DATE      PURPOSE
** 01/06/99  FIXED TAX ID PROCESSING - AVS.
** 01/11/99  ADDED NEW REPORT (MISSING PIN's) - AVS.
** 01/14/99  EXTRACT LOCIC WAS CHANGED FOR NRA MISSING TAX ID.
** 01/21/99  REMOVED ASSUMED RESIDENCY REPORT.
** 03/03/99  FIXED THE LOGIC FOR MISSING TIN's.
** 09/10/04  TOPS CHANGES.
**           TOPS PROCESSING
**           COMPANY DECODING USING TWRNCOMP.
**           REMOVE OLD CODE FOR IA.
** 08/16/05  ADDED CHECK FOR TIN STERTED FROM '99' (NAVISYS)
** 12/06/05 - BK. RESTOWED FOR NEW  TWRNCOMP PARMS
** 05/07/08  ROSE MA - ROTH 401K/403B PROJECT
**           RECOMPILED DUE TO UPDATED TWRL0700.
** 01/6/02   RESTOW FOR SUNY/CUNY
** 04/10/17  J BREMER PIN EXP   SCAN 08/2017
************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0690 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0700 ldaTwrl0700;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Dte2;

    private DbsGroup pnd_Dte2__R_Field_1;
    private DbsField pnd_Dte2_Pnd_Mm2;
    private DbsField pnd_Dte2_Pnd_F1;
    private DbsField pnd_Dte2_Pnd_Dd2;
    private DbsField pnd_Dte2_Pnd_F2;
    private DbsField pnd_Dte2_Pnd_Yyyy2;
    private DbsField pnd_Dte1;

    private DbsGroup pnd_Dte1__R_Field_2;
    private DbsField pnd_Dte1_Pnd_Yyyy1;
    private DbsField pnd_Dte1_Pnd_Mm1;
    private DbsField pnd_Dte1_Pnd_Dd1;
    private DbsField pnd_J1;
    private DbsField pnd_J2;
    private DbsField pnd_J4;
    private DbsField pnd_J5;
    private DbsField pnd_I;
    private DbsField pnd_Aa;

    private DbsGroup pnd_Aa__R_Field_3;
    private DbsField pnd_Aa_Pnd_Aaa;
    private DbsField pnd_Aa_Pnd_Wc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Dte2 = localVariables.newFieldInRecord("pnd_Dte2", "#DTE2", FieldType.STRING, 10);

        pnd_Dte2__R_Field_1 = localVariables.newGroupInRecord("pnd_Dte2__R_Field_1", "REDEFINE", pnd_Dte2);
        pnd_Dte2_Pnd_Mm2 = pnd_Dte2__R_Field_1.newFieldInGroup("pnd_Dte2_Pnd_Mm2", "#MM2", FieldType.NUMERIC, 2);
        pnd_Dte2_Pnd_F1 = pnd_Dte2__R_Field_1.newFieldInGroup("pnd_Dte2_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Dte2_Pnd_Dd2 = pnd_Dte2__R_Field_1.newFieldInGroup("pnd_Dte2_Pnd_Dd2", "#DD2", FieldType.NUMERIC, 2);
        pnd_Dte2_Pnd_F2 = pnd_Dte2__R_Field_1.newFieldInGroup("pnd_Dte2_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Dte2_Pnd_Yyyy2 = pnd_Dte2__R_Field_1.newFieldInGroup("pnd_Dte2_Pnd_Yyyy2", "#YYYY2", FieldType.NUMERIC, 4);
        pnd_Dte1 = localVariables.newFieldInRecord("pnd_Dte1", "#DTE1", FieldType.NUMERIC, 8);

        pnd_Dte1__R_Field_2 = localVariables.newGroupInRecord("pnd_Dte1__R_Field_2", "REDEFINE", pnd_Dte1);
        pnd_Dte1_Pnd_Yyyy1 = pnd_Dte1__R_Field_2.newFieldInGroup("pnd_Dte1_Pnd_Yyyy1", "#YYYY1", FieldType.NUMERIC, 4);
        pnd_Dte1_Pnd_Mm1 = pnd_Dte1__R_Field_2.newFieldInGroup("pnd_Dte1_Pnd_Mm1", "#MM1", FieldType.NUMERIC, 2);
        pnd_Dte1_Pnd_Dd1 = pnd_Dte1__R_Field_2.newFieldInGroup("pnd_Dte1_Pnd_Dd1", "#DD1", FieldType.NUMERIC, 2);
        pnd_J1 = localVariables.newFieldInRecord("pnd_J1", "#J1", FieldType.NUMERIC, 8);
        pnd_J2 = localVariables.newFieldInRecord("pnd_J2", "#J2", FieldType.NUMERIC, 8);
        pnd_J4 = localVariables.newFieldInRecord("pnd_J4", "#J4", FieldType.NUMERIC, 8);
        pnd_J5 = localVariables.newFieldInRecord("pnd_J5", "#J5", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 8);
        pnd_Aa = localVariables.newFieldInRecord("pnd_Aa", "#AA", FieldType.STRING, 8);

        pnd_Aa__R_Field_3 = localVariables.newGroupInRecord("pnd_Aa__R_Field_3", "REDEFINE", pnd_Aa);
        pnd_Aa_Pnd_Aaa = pnd_Aa__R_Field_3.newFieldInGroup("pnd_Aa_Pnd_Aaa", "#AAA", FieldType.STRING, 3);
        pnd_Aa_Pnd_Wc = pnd_Aa__R_Field_3.newFieldInGroup("pnd_Aa_Pnd_Wc", "#WC", FieldType.STRING, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0700.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0690() throws Exception
    {
        super("Twrp0690");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        //* *--------
        //*  MISSING DOB       REPORT
        //*  DEFAULTED PAYEES  REPORT                                                                                                                                     //Natural: FORMAT ( 01 ) LS = 132 PS = 61
        //*  MISSING TAX ID    REPORT                                                                                                                                     //Natural: FORMAT ( 02 ) LS = 132 PS = 61
        //*  MISSING PIN's     report                                                                                                                                     //Natural: FORMAT ( 04 ) LS = 132 PS = 61
        //*                                                                                                                                                               //Natural: FORMAT ( 05 ) LS = 132 PS = 61
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        pnd_Dte2_Pnd_F1.setValue("/");                                                                                                                                    //Natural: MOVE '/' TO #F1 #F2
        pnd_Dte2_Pnd_F2.setValue("/");
        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(pnd_Ws_Const_Low_Values);                                                                                    //Natural: ASSIGN #TWRACOMP.#COMP-CODE := LOW-VALUES
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRACOMP.#ABEND-IND #TWRACOMP.#DISPLAY-IND
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().reset();
        //* *
        //* *   READ TRANSACTION FILE
        //* *
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().notEquals(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde())))                                          //Natural: IF #TWRACOMP.#COMP-CODE NE TWRT-COMPANY-CDE
            {
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());                                                      //Natural: ASSIGN #TWRACOMP.#COMP-CODE := TWRT-COMPANY-CDE
                DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                    pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dte1.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte());                                                                                               //Natural: MOVE TWRT-PAYMT-DTE TO #DTE1
            pnd_Dte2_Pnd_Yyyy2.setValue(pnd_Dte1_Pnd_Yyyy1);                                                                                                              //Natural: MOVE #YYYY1 TO #YYYY2
            pnd_Dte2_Pnd_Mm2.setValue(pnd_Dte1_Pnd_Mm1);                                                                                                                  //Natural: MOVE #MM1 TO #MM2
            pnd_Dte2_Pnd_Dd2.setValue(pnd_Dte1_Pnd_Dd1);                                                                                                                  //Natural: MOVE #DD1 TO #DD2
            //* *
            //* * NRA MISSING TAX ID
            //* *
            //*  BK
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().equals(" ") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().equals("000000000") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().greaterOrEqual("99"))) //Natural: IF TWRT-TAX-ID = ' ' OR = '000000000' OR TWRT-TAX-ID >= '99'
            {
                //*  1099-R/I - TOPS
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1) || ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(11) || ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")  //Natural: IF TWRT-CITIZENSHIP = 1 OR = 11 OR TWRT-PYMNT-TAX-FORM-1078 = 'Y' OR TWRT-TAX-TYPE = 'I' OR = 'R'
                    || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("I") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("R")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J4.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #J4
                                                                                                                                                                          //Natural: PERFORM #PRINT
                    sub_Pnd_Print();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Dob_A().equals("00000000")))                                                                                    //Natural: IF TWRT-DOB-A = '00000000'
            {
                //*  11-01-99 FRANK
                //*  11-01-99 FRANK
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().equals("2")))                                                                                 //Natural: IF TWRT-TAX-ID-TYPE = '2'
                {
                    ignore();
                    //*  11-01-99 FRANK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_J1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #J1
                    //* *
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new       //Natural: WRITE ( 01 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #DTE2 TWRT-TAX-ID ( EM = XXX-XX-XXXX ) TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
                        ColumnSpacing(1),pnd_Dte2,ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* **
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde().equals(" ") || ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde().equals("00")))                           //Natural: IF TWRT-PAYEE-CDE = ' ' OR = '00'
            {
                pnd_J2.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #J2
                //* *
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new           //Natural: WRITE ( 02 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #DTE2 TWRT-TAX-ID ( EM = XXX-XX-XXXX ) TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
                    ColumnSpacing(1),pnd_Dte2,ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),
                    ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),
                    ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),
                    ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *
            }                                                                                                                                                             //Natural: END-IF
            //* *
            //*  INSERTED 11-10-99 BY FRANK-SEEPESAUD
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("GS") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("IS")))                                //Natural: IF TWRT-SOURCE = 'GS' OR = 'IS'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF TWRT-PIN-ID = '0000000' OR = ' '
                //*  PIN EXP 08/2017
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id().equals("000000000000") || ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id().equals(" ")))                   //Natural: IF TWRT-PIN-ID = '000000000000' OR = ' '
                {
                    pnd_J5.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #J5
                    //* *
                    getReports().write(5, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new       //Natural: WRITE ( 05 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #DTE2 TWRT-TAX-ID ( EM = XXX-XX-XXXX ) TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
                        ColumnSpacing(1),pnd_Dte2,ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),
                        ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_J1.equals(getZero())))                                                                                                                          //Natural: IF #J1 = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(11),"*** There is no missing DOB on the transaction file ***");          //Natural: WRITE ( 01 ) /// 11X '*** There is no missing DOB on the transaction file ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pnd_J2.equals(getZero())))                                                                                                                          //Natural: IF #J2 = 0
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(11),"*** There is no defaulted payees on the transaction file ***");     //Natural: WRITE ( 02 ) /// 11X '*** There is no defaulted payees on the transaction file ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_J4.equals(getZero())))                                                                                                                          //Natural: IF #J4 = 0
        {
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(11),"*** There is no NRA missing Tax IDGs on the transaction file ***"); //Natural: WRITE ( 04 ) /// 11X '*** There is no NRA missing Tax ID''s on the transaction file ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pnd_J5.equals(getZero())))                                                                                                                          //Natural: IF #J5 = 0
        {
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(11),"*** There is no missing PINGs on the transaction file ***");        //Natural: WRITE ( 05 ) /// 11X '*** There is no missing PIN''s on the transaction file ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *------------
        //* *
        //* * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 01 )
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 02 )
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 03 )
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 04 )
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 05 )
    private void sub_Pnd_Print() throws Exception                                                                                                                         //Natural: #PRINT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new                   //Natural: WRITE ( 04 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #DTE2 TWRT-TAX-ID ( EM = XXX-XX-XXXX ) TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
            ColumnSpacing(1),pnd_Dte2,ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),
            ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),
            ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),
            ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0690-01",new                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0690-01' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 01 ) / 43X '  Missing Dates of Birth      ' 31X 'Date: '*DATU / 54X #TWRACOMP.#COMP-SHORT-NAME // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,new 
                        ColumnSpacing(43),"  Missing Dates of Birth      ",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest",
                        "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ",
                        "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ",
                        "                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0690-02",new                    //Natural: WRITE ( 02 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0690-02' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 02 ) / 43X '  Defaulted Payee Codes       ' 31X 'Date: '*DATU / 54X #TWRACOMP.#COMP-SHORT-NAME // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(2),NEWLINE,new 
                        ColumnSpacing(43),"  Defaulted Payee Codes       ",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest",
                        "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ",
                        "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ",
                        "                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0690-03",new                    //Natural: WRITE ( 03 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0690-03' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 03 ) / 48X '  Assumed Residency      ' 31X 'Date: '*DATU / 54X #TWRACOMP.#COMP-SHORT-NAME // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(3),NEWLINE,new 
                        ColumnSpacing(48),"  Assumed Residency      ",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),
                        NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","     IVC        Fed Tax        NRA       State       Local  Res Ctz",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ","   Amount     Amount       Amount      Amount      Amount Cde Cde",
                        NEWLINE,"                                                                ","                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0690-04",new                    //Natural: WRITE ( 04 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0690-04' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 04 ) / 45X 'NRA missing Tax ID''s  ' 31X 'Date: '*DATU / 54X #TWRACOMP.#COMP-SHORT-NAME // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(4),NEWLINE,new 
                        ColumnSpacing(45),"NRA missing Tax IDGs  ",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),
                        NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","     IVC        Fed Tax        NRA       State       Local  Res Ctz",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ","   Amount     Amount       Amount      Amount      Amount Cde Cde",
                        NEWLINE,"                                                                ","                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0690-05",new                    //Natural: WRITE ( 05 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0690-05' 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 05 ) / 40X '         Missing PIN''s           ' 31X 'Date: '*DATU / 54X #TWRACOMP.#COMP-SHORT-NAME // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(5),NEWLINE,new 
                        ColumnSpacing(40),"         Missing PINGs           ",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest",
                        "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ",
                        "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ",
                        "                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=61");
        Global.format(2, "LS=132 PS=61");
        Global.format(4, "LS=132 PS=61");
        Global.format(5, "LS=132 PS=61");
    }
}
