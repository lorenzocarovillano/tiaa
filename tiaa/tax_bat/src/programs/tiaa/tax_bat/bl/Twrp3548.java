/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:37:09 PM
**        * FROM NATURAL PROGRAM : Twrp3548
************************************************************
**        * FILE NAME            : Twrp3548.java
**        * CLASS NAME           : Twrp3548
**        * INSTANCE NAME        : Twrp3548
************************************************************
************************************************************************
** PROGRAM : TWRP3548
** SYSTEM  : TAXWARS
** DATE    : 12/16/2002
** AUTHOR  : MARINA NACHBER: MODIFIED BY D. APRIL
** FUNCTION: WRITES RECORD SEQUENCE NUMBER TO IRS CORRECTION RECORDS
** HISTORY.....:
*  10/23/06  RM  UPDATE LDAS TWRL3326 & TWRL3328 FOR 2006 IRS SPEC.
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3548 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3327 ldaTwrl3327;
    private LdaTwrl3326 ldaTwrl3326;
    private LdaTwrl3328 ldaTwrl3328;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Irs_Record;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_1;

    private DbsGroup pnd_Irs_Record__R_Field_1;
    private DbsField pnd_Irs_Record_Pnd_Irs_Record_Type;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_2;
    private DbsField pnd_Irs_Record_Pnd_Rec_Seq_No;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_3;
    private DbsField pnd_Rec_Count;
    private DbsField pnd_F_Rec_Count;
    private DbsField pnd_T_Rec_Count;
    private DbsField pnd_T_Record_Output;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3327 = new LdaTwrl3327();
        registerRecord(ldaTwrl3327);
        ldaTwrl3326 = new LdaTwrl3326();
        registerRecord(ldaTwrl3326);
        ldaTwrl3328 = new LdaTwrl3328();
        registerRecord(ldaTwrl3328);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Irs_Record = localVariables.newGroupInRecord("pnd_Irs_Record", "#IRS-RECORD");
        pnd_Irs_Record_Pnd_Irs_Filler_1 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_1", "#IRS-FILLER-1", FieldType.STRING, 250);

        pnd_Irs_Record__R_Field_1 = pnd_Irs_Record.newGroupInGroup("pnd_Irs_Record__R_Field_1", "REDEFINE", pnd_Irs_Record_Pnd_Irs_Filler_1);
        pnd_Irs_Record_Pnd_Irs_Record_Type = pnd_Irs_Record__R_Field_1.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Record_Type", "#IRS-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Irs_Record_Pnd_Irs_Filler_2 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_2", "#IRS-FILLER-2", FieldType.STRING, 249);
        pnd_Irs_Record_Pnd_Rec_Seq_No = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Rec_Seq_No", "#REC-SEQ-NO", FieldType.NUMERIC, 8);
        pnd_Irs_Record_Pnd_Irs_Filler_3 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_3", "#IRS-FILLER-3", FieldType.STRING, 243);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 8);
        pnd_F_Rec_Count = localVariables.newFieldInRecord("pnd_F_Rec_Count", "#F-REC-COUNT", FieldType.NUMERIC, 8);
        pnd_T_Rec_Count = localVariables.newFieldInRecord("pnd_T_Rec_Count", "#T-REC-COUNT", FieldType.NUMERIC, 8);
        pnd_T_Record_Output = localVariables.newFieldInRecord("pnd_T_Record_Output", "#T-RECORD-OUTPUT", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3324.initializeValues();
        ldaTwrl3327.initializeValues();
        ldaTwrl3326.initializeValues();
        ldaTwrl3328.initializeValues();

        localVariables.reset();
        pnd_T_Record_Output.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3548() throws Exception
    {
        super("Twrp3548");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #IRS-RECORD
        while (condition(getWorkFiles().read(1, pnd_Irs_Record)))
        {
            short decideConditionsMet245 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #IRS-RECORD-TYPE;//Natural: VALUE 'F'
            if (condition((pnd_Irs_Record_Pnd_Irs_Record_Type.equals("F"))))
            {
                decideConditionsMet245++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-F-RECORDS
                sub_Process_F_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Irs_Record_Pnd_Irs_Record_Type.equals("T"))))
            {
                decideConditionsMet245++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-T-RECORDS
                sub_Process_T_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                if (condition(pnd_T_Rec_Count.greater(getZero()) && ! (pnd_T_Record_Output.getBoolean())))                                                                //Natural: IF #T-REC-COUNT > 0 AND NOT #T-RECORD-OUTPUT
                {
                    pnd_Rec_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-COUNT
                    ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Record_Sequence_No().setValue(pnd_Rec_Count);                                                                     //Natural: ASSIGN IRS-T-OUT-TAPE.IRS-T-RECORD-SEQUENCE-NO := #REC-COUNT
                    //*  OUTPUT T W/COMBINED TOTALS
                    getWorkFiles().write(2, false, ldaTwrl3326.getIrs_T_Out_Tape());                                                                                      //Natural: WRITE WORK FILE 2 IRS-T-OUT-TAPE
                    pnd_T_Record_Output.setValue(true);                                                                                                                   //Natural: MOVE TRUE TO #T-RECORD-OUTPUT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Rec_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-COUNT
            //*  WRITE (0) '#rec-count ' #REC-COUNT
            pnd_Irs_Record_Pnd_Rec_Seq_No.setValue(pnd_Rec_Count);                                                                                                        //Natural: ASSIGN #REC-SEQ-NO := #REC-COUNT
            getWorkFiles().write(2, false, pnd_Irs_Record);                                                                                                               //Natural: WRITE WORK FILE 2 #IRS-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Rec_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #REC-COUNT
        ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Record_Sequence_No().setValue(pnd_Rec_Count);                                                                                 //Natural: ASSIGN IRS-F-OUT-TAPE.IRS-F-RECORD-SEQUENCE-NO := #REC-COUNT
        //*  OUTPUT F W/COMBINED TOTALS
        getWorkFiles().write(2, false, ldaTwrl3324.getIrs_F_Out_Tape());                                                                                                  //Natural: WRITE WORK FILE 2 IRS-F-OUT-TAPE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-F-RECORDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-T-RECORDS
    }
    private void sub_Process_F_Records() throws Exception                                                                                                                 //Natural: PROCESS-F-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Move_1().setValue(pnd_Irs_Record_Pnd_Irs_Filler_1);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-1 TO IRS-F-INPUT-TAPE.IRS-F-MOVE-1
        ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Move_2().setValue(pnd_Irs_Record_Pnd_Irs_Filler_2);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-2 TO IRS-F-INPUT-TAPE.IRS-F-MOVE-2
        ldaTwrl3327.getIrs_F_Input_Tape_Rec_Seq_No().setValue(pnd_Irs_Record_Pnd_Rec_Seq_No);                                                                             //Natural: MOVE #IRS-RECORD.#REC-SEQ-NO TO IRS-F-INPUT-TAPE.REC-SEQ-NO
        ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Move_3().setValue(pnd_Irs_Record_Pnd_Irs_Filler_3);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-3 TO IRS-F-INPUT-TAPE.IRS-F-MOVE-3
        if (condition(pnd_F_Rec_Count.equals(getZero())))                                                                                                                 //Natural: IF #F-REC-COUNT = 0
        {
            ldaTwrl3324.getIrs_F_Out_Tape().setValuesByName(ldaTwrl3327.getIrs_F_Input_Tape());                                                                           //Natural: MOVE BY NAME IRS-F-INPUT-TAPE TO IRS-F-OUT-TAPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records().nadd(ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Number_Of_A_Records());                                  //Natural: ADD IRS-F-INPUT-TAPE.IRS-F-NUMBER-OF-A-RECORDS TO IRS-F-OUT-TAPE.IRS-F-NUMBER-OF-A-RECORDS
            ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_B_Records().nadd(ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Number_Of_B_Records());                                  //Natural: ADD IRS-F-INPUT-TAPE.IRS-F-NUMBER-OF-B-RECORDS TO IRS-F-OUT-TAPE.IRS-F-NUMBER-OF-B-RECORDS
            ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Blank_Cr_Lf().setValue(ldaTwrl3327.getIrs_F_Input_Tape_Irs_F_Blank_Cr_Lf());                                              //Natural: MOVE IRS-F-INPUT-TAPE.IRS-F-BLANK-CR-LF TO IRS-F-OUT-TAPE.IRS-F-BLANK-CR-LF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_F_Rec_Count.nadd(1);                                                                                                                                          //Natural: COMPUTE #F-REC-COUNT = #F-REC-COUNT + 1
    }
    private void sub_Process_T_Records() throws Exception                                                                                                                 //Natural: PROCESS-T-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl3328.getIrs_T_Input_Tape_Irs_T_Move_A().setValue(pnd_Irs_Record_Pnd_Irs_Filler_1);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-1 TO IRS-T-INPUT-TAPE.IRS-T-MOVE-A
        ldaTwrl3328.getIrs_T_Input_Tape_Irs_T_Move_B().setValue(pnd_Irs_Record_Pnd_Irs_Filler_2);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-2 TO IRS-T-INPUT-TAPE.IRS-T-MOVE-B
        ldaTwrl3328.getIrs_T_Input_Tape_Irs_T_Record_Sequence_No().setValue(pnd_Irs_Record_Pnd_Rec_Seq_No);                                                               //Natural: MOVE #IRS-RECORD.#REC-SEQ-NO TO IRS-T-INPUT-TAPE.IRS-T-RECORD-SEQUENCE-NO
        ldaTwrl3328.getIrs_T_Input_Tape_Irs_T_Move_C().setValue(pnd_Irs_Record_Pnd_Irs_Filler_3);                                                                         //Natural: MOVE #IRS-RECORD.#IRS-FILLER-3 TO IRS-T-INPUT-TAPE.IRS-T-MOVE-C
        if (condition(pnd_T_Rec_Count.equals(getZero())))                                                                                                                 //Natural: IF #T-REC-COUNT = 0
        {
            ldaTwrl3326.getIrs_T_Out_Tape().setValuesByName(ldaTwrl3328.getIrs_T_Input_Tape());                                                                           //Natural: MOVE BY NAME IRS-T-INPUT-TAPE TO IRS-T-OUT-TAPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees().nadd(ldaTwrl3328.getIrs_T_Input_Tape_Irs_T_Total_Number_Of_Payees());                            //Natural: ADD IRS-T-INPUT-TAPE.IRS-T-TOTAL-NUMBER-OF-PAYEES TO IRS-T-OUT-TAPE.IRS-T-TOTAL-NUMBER-OF-PAYEES
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T_Rec_Count.nadd(1);                                                                                                                                          //Natural: COMPUTE #T-REC-COUNT = #T-REC-COUNT + 1
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
    }
}
