/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:06 PM
**        * FROM NATURAL PROGRAM : Twrp3610
************************************************************
**        * FILE NAME            : Twrp3610.java
**        * CLASS NAME           : Twrp3610
**        * INSTANCE NAME        : Twrp3610
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP3610
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : CREATES FORMS EXTRACT FOR MAGNETIC MEDIA STATE REPORTING.
* CREATED  : 11 / 25 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM CREATES EXTRACT OF MAGNETIC MEDIA STATES FROM
*            THE TAX FORMS DATA BASE FILE.
**
** HISTORY:
** ------------------------------------
** 03/03/11 AY - EDIT FOR EMBEDDED BLANKS IN NAME CONTROL (ALL STATES).
** 12/18/09 AY - REVISED DUE TO 2009 IRS CHANGES (TWRL3502).
** 12/12/07 RM - UPDATED FOR 2007 IRS LAYOUT CHANGES (TWRL3502).
** 01/22/05 MS - TOPS TIAA & TRUST PROCESSING
** 09/24/03 RM - 2003 1099 & 5498 SDR
**               DISTRIBUTION CODE CONVERTED FROM 1 BYTE TO 2 BYTES,
**               USING TWRNDIST.
** 04/07/17 DY  RESTOW MODULE - PIN EXPANSION PROJECT
** 11/28/17 RM - RESTOW MODULE - CHANGES TO TWRL3502
** 10/05/18 SA - RESTOW MODULE - CHANGES TO TWRL3502 - EINCHG
** 01/04/19 SV - DATE OF PAYMENT LOGIC ADDED FOR "DC" PAYMENTS- VIKRAM
** 10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3610 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9705 ldaTwrl9705;
    private LdaTwrl3502 ldaTwrl3502;
    private LdaTwrl3503 ldaTwrl3503;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrltb4r ldaTwrltb4r;
    private PdaTwradist pdaTwradist;
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_P_S_Cnt;
    private DbsField pnd_S_Twrpymnt_Form_Sd;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_1;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_2;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Account_Num;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Tin_4;
    private DbsField pnd_Ws_Pnd_Cntrct;
    private DbsField pnd_Ws_Pnd_Payee;
    private DbsField pnd_Ws_Pnd_Acct_Seq_Num;
    private DbsField pnd_Ws_Pnd_Res_Loc_Cde;
    private DbsField pnd_Ws_Pnd_Year_A4;
    private DbsField pnd_Ws_Pnd_Name_Control_Edit;
    private DbsField pnd_Ws_Pnd_Blank_Count;
    private DbsField pnd_Ws_Pnd_Warning;

    private DbsGroup pnd_Out_Area;
    private DbsField pnd_Out_Area_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Out_Area_Pnd_Tirf_Sys_Err;
    private DbsField pnd_Out_Area_Pnd_Tirf_Empty_Form;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Distr;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Tax_Wthld;
    private DbsField pnd_Out_Area_Pnd_Tirf_Loc_Code;
    private DbsField pnd_Out_Area_Pnd_Tirf_Loc_Distr;
    private DbsField pnd_Out_Area_Pnd_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_Out_Area_Pnd_Tirf_Isn;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Rpt_Ind;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Code;
    private DbsField pnd_Out_Area_Pnd_Tirf_Tax_Year;
    private DbsField pnd_Out_Area_Pnd_Tirf_Name;
    private DbsField pnd_Out_Area_Pnd_Tirf_Tin;
    private DbsField pnd_Out_Area_Pnd_Tirf_Account;
    private DbsField pnd_Out_Area_Pnd_Tirf_Addr_Ln1;
    private DbsField pnd_Out_Area_Pnd_Tirf_Addr_Ln2;
    private DbsField pnd_Out_Area_Pnd_Tirf_Addr_Ln3;
    private DbsField pnd_Out_Area_Pnd_Tirf_Tin_Type;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Hardcopy_Ind;
    private DbsField pnd_Out_Area_Pnd_Tirf_Res_Reject_Ind;
    private DbsField pnd_Out_Area_Pnd_Tirf_State_Reporting;
    private DbsField pnd_Out_Area_Pnd_Tirf_Box1;
    private DbsField pnd_Out_Area_Pnd_Tirf_Box2;
    private DbsField pnd_Out_Area_Pnd_Tirf_Box4;
    private DbsField pnd_Out_Area_Pnd_Tirf_Box5;
    private DbsField pnd_Out_Area_Pnd_Tirf_Box9;
    private DbsField pnd_Out_Area_Pnd_Tirf_Boxa;
    private DbsField pnd_Out_Area_Pnd_Tirf_Boxb;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_4;
    private DbsField pnd_Input_Parm_Pnd_In_Form;
    private DbsField pnd_Input_Parm_Pnd_In_Form_Fill;
    private DbsField pnd_Input_Parm_Pnd_In_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok;
    private DbsField pnd_Input_Parm_Pnd_In_Input_State;
    private DbsField pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Year__R_Field_5;
    private DbsField pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A;
    private DbsField pnd_Super_Start;

    private DbsGroup pnd_Super_Start__R_Field_6;
    private DbsField pnd_Super_Start_Pnd_Sup_Tax_Year;
    private DbsField pnd_Super_Start_Pnd_Sup_Form_Type;
    private DbsField pnd_Super_Start_Pnd_Sup_State_Code;
    private DbsField pnd_I;
    private DbsField pnd_Fil1;
    private DbsField pnd_Fil2;
    private DbsField pnd_Fil3;
    private DbsField pnd_Fil4;
    private DbsField pnd_Zip_5_4;

    private DbsGroup pnd_Zip_5_4__R_Field_7;
    private DbsField pnd_Zip_5_4_Pnd_Zip_5;
    private DbsField pnd_Zip_5_4_Pnd_Zip_4;
    private DbsField pnd_Zip_Ext_4;

    private DbsGroup pnd_Zip_Ext_4__R_Field_8;
    private DbsField pnd_Zip_Ext_4_Pnd_Zip_Dash;
    private DbsField pnd_Zip_Ext_4_Pnd_Zip_Ext;
    private DbsField pnd_Mid_Name;

    private DbsGroup pnd_Mid_Name__R_Field_9;
    private DbsField pnd_Mid_Name_Pnd_Mid_Name1;
    private DbsField pnd_Rpt_Name;

    private DbsGroup pnd_Rpt_Name__R_Field_10;
    private DbsField pnd_Rpt_Name_Pnd_Rpt_Name_1;
    private DbsField pnd_Rpt_Name_Pnd_Rpt_State_Code;
    private DbsField pnd_Out_Count;
    private DbsField pnd_Dec_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);
        ldaTwrl3503 = new LdaTwrl3503();
        registerRecord(ldaTwrl3503);
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrltb4r = new LdaTwrltb4r();
        registerRecord(ldaTwrltb4r);
        registerRecord(ldaTwrltb4r.getVw_tircntl_Rpt());
        pdaTwradist = new PdaTwradist(localVariables);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // Local Variables
        pnd_P_S_Cnt = localVariables.newFieldInRecord("pnd_P_S_Cnt", "#P-S-CNT", FieldType.NUMERIC, 3);
        pnd_S_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd", "#S-TWRPYMNT-FORM-SD", FieldType.STRING, 37);

        pnd_S_Twrpymnt_Form_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd__R_Field_1", "REDEFINE", pnd_S_Twrpymnt_Form_Sd);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr", 
            "#S-TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr", 
            "#S-TWRPYMNT-CONTRACT-PAYEE-NBR", FieldType.STRING, 10);

        pnd_S_Twrpymnt_Form_Sd__R_Field_2 = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newGroupInGroup("pnd_S_Twrpymnt_Form_Sd__R_Field_2", "REDEFINE", pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Account_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Account_Num", "#ACCOUNT-NUM", FieldType.STRING, 20);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Account_Num);
        pnd_Ws_Pnd_Tin_4 = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Tin_4", "#TIN-4", FieldType.STRING, 4);
        pnd_Ws_Pnd_Cntrct = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 8);
        pnd_Ws_Pnd_Payee = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Payee", "#PAYEE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Acct_Seq_Num = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Acct_Seq_Num", "#ACCT-SEQ-NUM", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Res_Loc_Cde = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Res_Loc_Cde", "#RES-LOC-CDE", FieldType.STRING, 3);
        pnd_Ws_Pnd_Year_A4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Year_A4", "#YEAR-A4", FieldType.STRING, 4);
        pnd_Ws_Pnd_Name_Control_Edit = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Name_Control_Edit", "#NAME-CONTROL-EDIT", FieldType.STRING, 30);
        pnd_Ws_Pnd_Blank_Count = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Blank_Count", "#BLANK-COUNT", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Warning = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Warning", "#WARNING", FieldType.STRING, 30);

        pnd_Out_Area = localVariables.newGroupInRecord("pnd_Out_Area", "#OUT-AREA");
        pnd_Out_Area_Pnd_Tirf_Company_Cde = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Out_Area_Pnd_Tirf_Sys_Err = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Sys_Err", "#TIRF-SYS-ERR", FieldType.STRING, 1);
        pnd_Out_Area_Pnd_Tirf_Empty_Form = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Empty_Form", "#TIRF-EMPTY-FORM", FieldType.STRING, 1);
        pnd_Out_Area_Pnd_Tirf_State_Distr = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Distr", "#TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_Out_Area_Pnd_Tirf_State_Tax_Wthld = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Tax_Wthld", "#TIRF-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Out_Area_Pnd_Tirf_Loc_Code = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Loc_Code", "#TIRF-LOC-CODE", FieldType.STRING, 5);
        pnd_Out_Area_Pnd_Tirf_Loc_Distr = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Loc_Distr", "#TIRF-LOC-DISTR", FieldType.NUMERIC, 11, 2);
        pnd_Out_Area_Pnd_Tirf_Loc_Tax_Wthld = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Loc_Tax_Wthld", "#TIRF-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Out_Area_Pnd_Tirf_Isn = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Isn", "#TIRF-ISN", FieldType.NUMERIC, 8);
        pnd_Out_Area_Pnd_Tirf_State_Rpt_Ind = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Rpt_Ind", "#TIRF-STATE-RPT-IND", FieldType.STRING, 
            1);
        pnd_Out_Area_Pnd_Tirf_State_Code = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Code", "#TIRF-STATE-CODE", FieldType.STRING, 2);
        pnd_Out_Area_Pnd_Tirf_Tax_Year = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        pnd_Out_Area_Pnd_Tirf_Name = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Name", "#TIRF-NAME", FieldType.STRING, 40);
        pnd_Out_Area_Pnd_Tirf_Tin = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 9);
        pnd_Out_Area_Pnd_Tirf_Account = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Account", "#TIRF-ACCOUNT", FieldType.STRING, 10);
        pnd_Out_Area_Pnd_Tirf_Addr_Ln1 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Addr_Ln1", "#TIRF-ADDR-LN1", FieldType.STRING, 40);
        pnd_Out_Area_Pnd_Tirf_Addr_Ln2 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Addr_Ln2", "#TIRF-ADDR-LN2", FieldType.STRING, 40);
        pnd_Out_Area_Pnd_Tirf_Addr_Ln3 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Addr_Ln3", "#TIRF-ADDR-LN3", FieldType.STRING, 40);
        pnd_Out_Area_Pnd_Tirf_Tin_Type = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Tin_Type", "#TIRF-TIN-TYPE", FieldType.STRING, 1);
        pnd_Out_Area_Pnd_Tirf_State_Hardcopy_Ind = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Hardcopy_Ind", "#TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1);
        pnd_Out_Area_Pnd_Tirf_Res_Reject_Ind = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Res_Reject_Ind", "#TIRF-RES-REJECT-IND", FieldType.STRING, 
            1);
        pnd_Out_Area_Pnd_Tirf_State_Reporting = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_State_Reporting", "#TIRF-STATE-REPORTING", FieldType.STRING, 
            1);
        pnd_Out_Area_Pnd_Tirf_Box1 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Box1", "#TIRF-BOX1", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Box2 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Box2", "#TIRF-BOX2", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Box4 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Box4", "#TIRF-BOX4", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Box5 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Box5", "#TIRF-BOX5", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Box9 = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Box9", "#TIRF-BOX9", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Boxa = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Boxa", "#TIRF-BOXA", FieldType.NUMERIC, 12, 2);
        pnd_Out_Area_Pnd_Tirf_Boxb = pnd_Out_Area.newFieldInGroup("pnd_Out_Area_Pnd_Tirf_Boxb", "#TIRF-BOXB", FieldType.NUMERIC, 12, 2);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 30);

        pnd_Input_Parm__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_4", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_In_Form = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form", "#IN-FORM", FieldType.STRING, 4);
        pnd_Input_Parm_Pnd_In_Form_Fill = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_In_Form_Fill", "#IN-FORM-FILL", FieldType.STRING, 
            2);
        pnd_Input_Parm_Pnd_In_Tax_Year = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_In_Tax_Year", "#IN-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok", "#IN-NOT-FIRST-TIME-RUN-OK", 
            FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_In_Input_State = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_In_Input_State", "#IN-INPUT-STATE", FieldType.STRING, 
            2);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws_Tax_Year__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Tax_Year__R_Field_5", "REDEFINE", pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A = pnd_Ws_Tax_Year__R_Field_5.newFieldInGroup("pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A", "#WS-TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Super_Start = localVariables.newFieldInRecord("pnd_Super_Start", "#SUPER-START", FieldType.STRING, 8);

        pnd_Super_Start__R_Field_6 = localVariables.newGroupInRecord("pnd_Super_Start__R_Field_6", "REDEFINE", pnd_Super_Start);
        pnd_Super_Start_Pnd_Sup_Tax_Year = pnd_Super_Start__R_Field_6.newFieldInGroup("pnd_Super_Start_Pnd_Sup_Tax_Year", "#SUP-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Super_Start_Pnd_Sup_Form_Type = pnd_Super_Start__R_Field_6.newFieldInGroup("pnd_Super_Start_Pnd_Sup_Form_Type", "#SUP-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Super_Start_Pnd_Sup_State_Code = pnd_Super_Start__R_Field_6.newFieldInGroup("pnd_Super_Start_Pnd_Sup_State_Code", "#SUP-STATE-CODE", FieldType.STRING, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Fil1 = localVariables.newFieldInRecord("pnd_Fil1", "#FIL1", FieldType.STRING, 122);
        pnd_Fil2 = localVariables.newFieldInRecord("pnd_Fil2", "#FIL2", FieldType.STRING, 250);
        pnd_Fil3 = localVariables.newFieldInRecord("pnd_Fil3", "#FIL3", FieldType.STRING, 250);
        pnd_Fil4 = localVariables.newFieldInRecord("pnd_Fil4", "#FIL4", FieldType.STRING, 225);
        pnd_Zip_5_4 = localVariables.newFieldInRecord("pnd_Zip_5_4", "#ZIP-5-4", FieldType.STRING, 9);

        pnd_Zip_5_4__R_Field_7 = localVariables.newGroupInRecord("pnd_Zip_5_4__R_Field_7", "REDEFINE", pnd_Zip_5_4);
        pnd_Zip_5_4_Pnd_Zip_5 = pnd_Zip_5_4__R_Field_7.newFieldInGroup("pnd_Zip_5_4_Pnd_Zip_5", "#ZIP-5", FieldType.STRING, 5);
        pnd_Zip_5_4_Pnd_Zip_4 = pnd_Zip_5_4__R_Field_7.newFieldInGroup("pnd_Zip_5_4_Pnd_Zip_4", "#ZIP-4", FieldType.STRING, 4);
        pnd_Zip_Ext_4 = localVariables.newFieldInRecord("pnd_Zip_Ext_4", "#ZIP-EXT-4", FieldType.STRING, 5);

        pnd_Zip_Ext_4__R_Field_8 = localVariables.newGroupInRecord("pnd_Zip_Ext_4__R_Field_8", "REDEFINE", pnd_Zip_Ext_4);
        pnd_Zip_Ext_4_Pnd_Zip_Dash = pnd_Zip_Ext_4__R_Field_8.newFieldInGroup("pnd_Zip_Ext_4_Pnd_Zip_Dash", "#ZIP-DASH", FieldType.STRING, 1);
        pnd_Zip_Ext_4_Pnd_Zip_Ext = pnd_Zip_Ext_4__R_Field_8.newFieldInGroup("pnd_Zip_Ext_4_Pnd_Zip_Ext", "#ZIP-EXT", FieldType.STRING, 4);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 30);

        pnd_Mid_Name__R_Field_9 = localVariables.newGroupInRecord("pnd_Mid_Name__R_Field_9", "REDEFINE", pnd_Mid_Name);
        pnd_Mid_Name_Pnd_Mid_Name1 = pnd_Mid_Name__R_Field_9.newFieldInGroup("pnd_Mid_Name_Pnd_Mid_Name1", "#MID-NAME1", FieldType.STRING, 1);
        pnd_Rpt_Name = localVariables.newFieldInRecord("pnd_Rpt_Name", "#RPT-NAME", FieldType.STRING, 7);

        pnd_Rpt_Name__R_Field_10 = localVariables.newGroupInRecord("pnd_Rpt_Name__R_Field_10", "REDEFINE", pnd_Rpt_Name);
        pnd_Rpt_Name_Pnd_Rpt_Name_1 = pnd_Rpt_Name__R_Field_10.newFieldInGroup("pnd_Rpt_Name_Pnd_Rpt_Name_1", "#RPT-NAME-1", FieldType.STRING, 4);
        pnd_Rpt_Name_Pnd_Rpt_State_Code = pnd_Rpt_Name__R_Field_10.newFieldInGroup("pnd_Rpt_Name_Pnd_Rpt_State_Code", "#RPT-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Out_Count = localVariables.newFieldInRecord("pnd_Out_Count", "#OUT-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Dec_Tax_Year = localVariables.newFieldInRecord("pnd_Dec_Tax_Year", "#DEC-TAX-YEAR", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9705.initializeValues();
        ldaTwrl3502.initializeValues();
        ldaTwrl3503.initializeValues();
        ldaTwrltb4r.initializeValues();
        ldaTwrl9415.initializeValues();

        localVariables.reset();
        pnd_Fil1.setInitialValue(" ");
        pnd_Fil2.setInitialValue(" ");
        pnd_Fil3.setInitialValue(" ");
        pnd_Fil4.setInitialValue(" ");
        pnd_Rpt_Name.setInitialValue("9ST0");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3610() throws Exception
    {
        super("Twrp3610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3610|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 60 LS = 132
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                getReports().print(0, NEWLINE,"=",pnd_Input_Parm_Pnd_In_Form,NEWLINE,"=",pnd_Input_Parm_Pnd_In_Tax_Year,NEWLINE,"=",pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok, //Natural: PRINT ( 00 ) / '=' #IN-FORM / '=' #IN-TAX-YEAR / '=' #IN-NOT-FIRST-TIME-RUN-OK / '=' #IN-INPUT-STATE
                    NEWLINE,"=",pnd_Input_Parm_Pnd_In_Input_State);
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                if (condition(pnd_Input_Parm_Pnd_In_Tax_Year.equals(getZero())))                                                                                          //Natural: IF #IN-TAX-YEAR = 0
                {
                    pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #WS-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Tax_Year.setValue(pnd_Input_Parm_Pnd_In_Tax_Year);                                                                                             //Natural: ASSIGN #WS-TAX-YEAR := #IN-TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-PREV-RUN
                sub_Check_Prev_Run();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
                pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                         //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS-TAX-YEAR
                pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                  //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
                pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
                pnd_Super_Start.setValue("H'00'");                                                                                                                        //Natural: ASSIGN #SUPER-START := H'00'
                pnd_Super_Start_Pnd_Sup_Tax_Year.setValue(pnd_Ws_Tax_Year);                                                                                               //Natural: ASSIGN #SUPER-START.#SUP-TAX-YEAR := #WS-TAX-YEAR
                pnd_Super_Start_Pnd_Sup_Form_Type.setValue(1);                                                                                                            //Natural: ASSIGN #SUPER-START.#SUP-FORM-TYPE := 01
                pnd_Super_Start_Pnd_Sup_State_Code.setValue(pnd_Input_Parm_Pnd_In_Input_State);                                                                           //Natural: ASSIGN #SUPER-START.#SUP-STATE-CODE := #IN-INPUT-STATE
                ldaTwrl9705.getVw_form_U().startDatabaseFind                                                                                                              //Natural: FIND FORM-U WITH TIRF-SUPERDE-8 = #SUPER-START
                (
                "RD_1",
                new Wc[] { new Wc("TIRF_SUPERDE_8", "=", pnd_Super_Start, WcType.WITH) }
                );
                RD_1:
                while (condition(ldaTwrl9705.getVw_form_U().readNextRow("RD_1")))
                {
                    ldaTwrl9705.getVw_form_U().setIfNotFoundControlFlag(false);
                    if (condition(!(ldaTwrl9705.getForm_U_Tirf_Active_Ind().equals("A"))))                                                                                //Natural: ACCEPT IF TIRF-ACTIVE-IND = 'A'
                    {
                        continue;
                    }
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO C*TIRF-1099-R-STATE-GRP
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I).notEquals(pnd_Super_Start_Pnd_Sup_State_Code)))                             //Natural: IF TIRF-STATE-CODE ( #I ) NE #SUPER-START.#SUP-STATE-CODE
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        //*  STATE TAX
                        //*  IRR-AMT - RCC
                        pnd_Out_Area.reset();                                                                                                                             //Natural: RESET #OUT-AREA
                        pnd_Out_Area_Pnd_Tirf_Box1.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                                                  //Natural: ASSIGN #TIRF-BOX1 := TIRF-RES-GROSS-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box2.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_I));                                                //Natural: ASSIGN #TIRF-BOX2 := TIRF-RES-TAXABLE-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box4.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_I));                                              //Natural: ASSIGN #TIRF-BOX4 := TIRF-RES-FED-TAX-WTHLD ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box5.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Ivc_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #TIRF-BOX5 := TIRF-RES-IVC-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box9.setValue(ldaTwrl9705.getForm_U_Tirf_Tot_Contractual_Ivc());                                                            //Natural: ASSIGN #TIRF-BOX9 := TIRF-TOT-CONTRACTUAL-IVC
                        pnd_Out_Area_Pnd_Tirf_Boxb.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #TIRF-BOXB := TIRF-RES-IRR-AMT ( #I )
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Ira_Distr().equals("Y")))                                                                                //Natural: IF TIRF-IRA-DISTR = 'Y'
                        {
                            pnd_Out_Area_Pnd_Tirf_Boxa.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                                              //Natural: ASSIGN #TIRF-BOXA := TIRF-RES-GROSS-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean()))                                                                              //Natural: IF TIRF-EMPTY-FORM
                        {
                            pnd_Out_Area_Pnd_Tirf_Empty_Form.setValue("Y");                                                                                               //Natural: ASSIGN #TIRF-EMPTY-FORM := 'Y'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Out_Area_Pnd_Tirf_Empty_Form.setValue("N");                                                                                               //Natural: ASSIGN #TIRF-EMPTY-FORM := 'N'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                           //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
                        {
                            pnd_Out_Area_Pnd_Tirf_Sys_Err.setValue("Y");                                                                                                  //Natural: ASSIGN #TIRF-SYS-ERR := 'Y'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Out_Area_Pnd_Tirf_Sys_Err.setValue("N");                                                                                                  //Natural: ASSIGN #TIRF-SYS-ERR := 'N'
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Out_Area_Pnd_Tirf_State_Distr.setValue(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_I));                                             //Natural: ASSIGN #TIRF-STATE-DISTR := TIRF-STATE-DISTR ( #I )
                        pnd_Out_Area_Pnd_Tirf_State_Tax_Wthld.setValue(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_I));                                     //Natural: ASSIGN #TIRF-STATE-TAX-WTHLD := TIRF-STATE-TAX-WTHLD ( #I )
                        pnd_Out_Area_Pnd_Tirf_Loc_Code.setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Code().getValue(pnd_I));                                                   //Natural: ASSIGN #TIRF-LOC-CODE := TIRF-LOC-CODE ( #I )
                        pnd_Out_Area_Pnd_Tirf_Loc_Distr.setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_I));                                                 //Natural: ASSIGN #TIRF-LOC-DISTR := TIRF-LOC-DISTR ( #I )
                        pnd_Out_Area_Pnd_Tirf_Loc_Tax_Wthld.setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_I));                                         //Natural: ASSIGN #TIRF-LOC-TAX-WTHLD := TIRF-LOC-TAX-WTHLD ( #I )
                        pnd_Out_Area_Pnd_Tirf_Tin_Type.setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type());                                                                //Natural: ASSIGN #TIRF-TIN-TYPE := TIRF-TAX-ID-TYPE
                        short decideConditionsMet1456 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
                        if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("1") || ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("3"))))
                        {
                            decideConditionsMet1456++;
                            pnd_Out_Area_Pnd_Tirf_Tin_Type.setValue("2");                                                                                                 //Natural: ASSIGN #TIRF-TIN-TYPE := '2'
                        }                                                                                                                                                 //Natural: VALUE '2'
                        else if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("2"))))
                        {
                            decideConditionsMet1456++;
                            pnd_Out_Area_Pnd_Tirf_Tin_Type.setValue("1");                                                                                                 //Natural: ASSIGN #TIRF-TIN-TYPE := '1'
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            pnd_Out_Area_Pnd_Tirf_Tin_Type.setValue(" ");                                                                                                 //Natural: ASSIGN #TIRF-TIN-TYPE := ' '
                        }                                                                                                                                                 //Natural: END-DECIDE
                        pnd_Out_Area_Pnd_Tirf_Isn.setValue(ldaTwrl9705.getVw_form_U().getAstISN("RD_1"));                                                                 //Natural: ASSIGN #TIRF-ISN := *ISN
                        pnd_Out_Area_Pnd_Tirf_Company_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                             //Natural: ASSIGN #TIRF-COMPANY-CDE := TIRF-COMPANY-CDE
                        pnd_Out_Area_Pnd_Tirf_State_Rpt_Ind.setValue(ldaTwrl9705.getForm_U_Tirf_State_Rpt_Ind().getValue(pnd_I));                                         //Natural: ASSIGN #TIRF-STATE-RPT-IND := TIRF-STATE-RPT-IND ( #I )
                        pnd_Out_Area_Pnd_Tirf_State_Hardcopy_Ind.setValue(ldaTwrl9705.getForm_U_Tirf_State_Hardcopy_Ind().getValue(pnd_I));                               //Natural: ASSIGN #TIRF-STATE-HARDCOPY-IND := TIRF-STATE-HARDCOPY-IND ( #I )
                        pnd_Out_Area_Pnd_Tirf_Res_Reject_Ind.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Reject_Ind().getValue(pnd_I));                                       //Natural: ASSIGN #TIRF-RES-REJECT-IND := TIRF-RES-REJECT-IND ( #I )
                        pnd_Out_Area_Pnd_Tirf_State_Reporting.setValue(ldaTwrl9705.getForm_U_Tirf_State_Reporting().getValue(pnd_I));                                     //Natural: ASSIGN #TIRF-STATE-REPORTING := TIRF-STATE-REPORTING ( #I )
                        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I))); //Natural: COMPRESS '0' TIRF-STATE-CODE ( #I ) INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
                        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                            pdaTwratbl2.getTwratbl2_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        pnd_Out_Area_Pnd_Tirf_State_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                    //Natural: ASSIGN #TIRF-STATE-CODE := TIRCNTL-STATE-ALPHA-CODE
                        pnd_Out_Area_Pnd_Tirf_Tax_Year.setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                                   //Natural: ASSIGN #TIRF-TAX-YEAR := TIRF-TAX-YEAR
                        pnd_Out_Area_Pnd_Tirf_Name.setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(),     //Natural: COMPRESS TIRF-PART-LAST-NME TIRF-PART-FIRST-NME TIRF-PART-MDDLE-NME INTO #TIRF-NAME
                            ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme()));
                        pnd_Out_Area_Pnd_Tirf_Tin.setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                             //Natural: ASSIGN #TIRF-TIN := TIRF-TIN
                        pnd_Out_Area_Pnd_Tirf_Account.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),                 //Natural: COMPRESS TIRF-CONTRACT-NBR TIRF-PAYEE-CDE INTO #TIRF-ACCOUNT LEAVING NO SPACE
                            ldaTwrl9705.getForm_U_Tirf_Payee_Cde()));
                        pnd_Out_Area_Pnd_Tirf_Addr_Ln1.setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln1());                                                                   //Natural: ASSIGN #TIRF-ADDR-LN1 := TIRF-ADDR-LN1
                        pnd_Out_Area_Pnd_Tirf_Addr_Ln2.setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln2());                                                                   //Natural: ASSIGN #TIRF-ADDR-LN2 := TIRF-ADDR-LN2
                        pnd_Out_Area_Pnd_Tirf_Addr_Ln3.setValue(ldaTwrl9705.getForm_U_Tirf_Addr_Ln3());                                                                   //Natural: ASSIGN #TIRF-ADDR-LN3 := TIRF-ADDR-LN3
                        //*  - - - - - - - - - - - - - - - - -
                        ldaTwrl3502.getPnd_Out_Irs_Record().reset();                                                                                                      //Natural: RESET #OUT-IRS-RECORD
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Rec_Id().setValue("B");                                                                                //Natural: ASSIGN #OUTB-REC-ID := 'B'
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Year().compute(new ComputeParameters(false, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Year()),  //Natural: ASSIGN #OUTB-PAY-YEAR := VAL ( TIRF-TAX-YEAR )
                            ldaTwrl9705.getForm_U_Tirf_Tax_Year().val());
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Corr_Ind().setValue(" ");                                                                              //Natural: ASSIGN #OUTB-CORR-IND := ' '
                        //*    #OUTB-NAME-CONTROL  :=  TIRF-PART-LAST-NME             /* 03/03/11
                        //*  03/03/11
                                                                                                                                                                          //Natural: PERFORM NAME-CONTROL-EDIT
                        sub_Name_Control_Edit();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin_Type().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type());                                         //Natural: ASSIGN #OUTB-TIN-TYPE := TIRF-TAX-ID-TYPE
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                      //Natural: ASSIGN #OUTB-TIN := TIRF-TIN
                        short decideConditionsMet1500 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
                        if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("1") || ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("3"))))
                        {
                            decideConditionsMet1500++;
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin_Type().setValue("2");                                                                          //Natural: ASSIGN #OUTB-TIN-TYPE := '2'
                        }                                                                                                                                                 //Natural: VALUE '2'
                        else if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("2"))))
                        {
                            decideConditionsMet1500++;
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin_Type().setValue("1");                                                                          //Natural: ASSIGN #OUTB-TIN-TYPE := '1'
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin_Type().setValue(" ");                                                                          //Natural: ASSIGN #OUTB-TIN-TYPE := ' '
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin().setValue(" ");                                                                               //Natural: ASSIGN #OUTB-TIN := ' '
                        }                                                                                                                                                 //Natural: END-DECIDE
                        //*  CONTRACT/PAYEE
                        if (condition(!ldaTwrl9705.getForm_U_Tirf_Tin().getSubstring(10,1).equals(" ")))                                                                  //Natural: IF SUBSTRING ( FORM-U.TIRF-TIN,10,1 ) NE ' '
                        {
                            pnd_Ws_Pnd_Tin_4.setValue(ldaTwrl9705.getForm_U_Tirf_Tin().getSubstring(7,4));                                                                //Natural: MOVE SUBSTR ( FORM-U.TIRF-TIN,7,4 ) TO #WS.#TIN-4
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Pnd_Tin_4.setValue(ldaTwrl9705.getForm_U_Tirf_Tin().getSubstring(6,4));                                                                //Natural: MOVE SUBSTR ( FORM-U.TIRF-TIN,6,4 ) TO #WS.#TIN-4
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Ws_Pnd_Cntrct.setValue(ldaTwrl9705.getForm_U_Tirf_Contract_Nbr());                                                                            //Natural: ASSIGN #WS.#CNTRCT := FORM-U.TIRF-CONTRACT-NBR
                        pnd_Ws_Pnd_Payee.setValue(ldaTwrl9705.getForm_U_Tirf_Payee_Cde());                                                                                //Natural: ASSIGN #WS.#PAYEE := FORM-U.TIRF-PAYEE-CDE
                        pnd_Ws_Pnd_Acct_Seq_Num.setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Form_Seq());                                                                      //Natural: ASSIGN #WS.#ACCT-SEQ-NUM := FORM-U.TIRF-IRS-FORM-SEQ
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Loc_Code().getValue(pnd_I).equals(" ")))                                                                 //Natural: IF FORM-U.TIRF-LOC-CODE ( #I ) = ' '
                        {
                            pnd_Ws_Pnd_Res_Loc_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I));                                                     //Natural: ASSIGN #WS.#RES-LOC-CDE := FORM-U.TIRF-STATE-CODE ( #I )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Pnd_Res_Loc_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Code().getValue(pnd_I));                                                       //Natural: ASSIGN #WS.#RES-LOC-CDE := FORM-U.TIRF-LOC-CODE ( #I )
                            //*  PA STATE TAX
                            //*  PA STATE TAX
                            //*  IRR-AMT - RCC
                        }                                                                                                                                                 //Natural: END-IF
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Acct().setValue(pnd_Ws_Pnd_Account_Num);                                                               //Natural: ASSIGN #OUTB-ACCT := #WS.#ACCOUNT-NUM
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Office_Cde().setValue(" ");                                                                            //Natural: ASSIGN #OUTB-OFFICE-CDE := ' '
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                       //Natural: ASSIGN #OUTB-PAY-AMT1 := TIRF-RES-GROSS-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box1.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                                                  //Natural: ASSIGN #TIRF-BOX1 := TIRF-RES-GROSS-AMT ( #I )
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_I));                     //Natural: ASSIGN #OUTB-PAY-AMT2 := TIRF-RES-TAXABLE-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box2.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_I));                                                //Natural: ASSIGN #TIRF-BOX2 := TIRF-RES-TAXABLE-AMT ( #I )
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMT3 := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_I));                   //Natural: ASSIGN #OUTB-PAY-AMT4 := TIRF-RES-FED-TAX-WTHLD ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box4.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_I));                                              //Natural: ASSIGN #TIRF-BOX4 := TIRF-RES-FED-TAX-WTHLD ( #I )
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Ivc_Amt().getValue(pnd_I));                         //Natural: ASSIGN #OUTB-PAY-AMT5 := TIRF-RES-IVC-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Box5.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Ivc_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #TIRF-BOX5 := TIRF-RES-IVC-AMT ( #I )
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMT6 := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMT7 := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMT8 := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9().setValue(ldaTwrl9705.getForm_U_Tirf_Tot_Contractual_Ivc());                                 //Natural: ASSIGN #OUTB-PAY-AMT9 := TIRF-TOT-CONTRACTUAL-IVC
                        pnd_Out_Area_Pnd_Tirf_Box9.setValue(ldaTwrl9705.getForm_U_Tirf_Tot_Contractual_Ivc());                                                            //Natural: ASSIGN #TIRF-BOX9 := TIRF-TOT-CONTRACTUAL-IVC
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_I));                         //Natural: ASSIGN #OUTB-PAY-AMTB := TIRF-RES-IRR-AMT ( #I )
                        pnd_Out_Area_Pnd_Tirf_Boxb.setValue(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_I));                                                    //Natural: ASSIGN #TIRF-BOXB := TIRF-RES-IRR-AMT ( #I )
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amta().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTA := 0
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Ira_Distr().equals("Y")))                                                                                //Natural: IF TIRF-IRA-DISTR = 'Y'
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amta().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                   //Natural: ASSIGN #OUTB-PAY-AMTA := TIRF-RES-GROSS-AMT ( #I )
                            //*  12/18/09
                            //*  12/18/09
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Out_Area_Pnd_Tirf_Boxa.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amta());                                                       //Natural: ASSIGN #TIRF-BOXA := #OUTB-PAY-AMTA
                        //*    #OUTB-PAY-AMTB  :=  0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTC := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTD := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amte().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTE := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTF := 0
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg().setValue(0);                                                                                //Natural: ASSIGN #OUTB-PAY-AMTG := 0
                        //*    #OUTB-RESERVED  :=  ' '
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind().setValue(" ");                                                                           //Natural: ASSIGN #OUTB-FOREIGN-IND := ' '
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Foreign_Addr().equals("Y")))                                                                             //Natural: IF TIRF-FOREIGN-ADDR = 'Y'
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind().setValue("1");                                                                       //Natural: ASSIGN #OUTB-FOREIGN-IND := '1'
                        }                                                                                                                                                 //Natural: END-IF
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name().setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme(),               //Natural: COMPRESS TIRF-PART-LAST-NME TIRF-PART-FIRST-NME TIRF-PART-MDDLE-NME INTO #OUTB-FIRST-PAYEE-NAME
                            ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme()));
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name().setValue(" ");                                                                        //Natural: ASSIGN #OUTB-SEC-PAYEE-NAME := ' '
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Payee_Address().setValue(ldaTwrl9705.getForm_U_Tirf_Street_Addr());                                    //Natural: ASSIGN #OUTB-PAYEE-ADDRESS := TIRF-STREET-ADDR
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Payee_City().setValue(ldaTwrl9705.getForm_U_Tirf_City());                                              //Natural: ASSIGN #OUTB-PAYEE-CITY := TIRF-CITY
                        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9705.getForm_U_Tirf_Geo_Cde()));     //Natural: COMPRESS '0' TIRF-GEO-CDE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
                        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                            pdaTwratbl2.getTwratbl2_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Payee_State().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                            //Natural: ASSIGN #OUTB-PAYEE-STATE := TIRCNTL-STATE-ALPHA-CODE
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Payee_Zip().setValue(ldaTwrl9705.getForm_U_Tirf_Zip());                                                //Natural: ASSIGN #OUTB-PAYEE-ZIP := TIRF-ZIP
                        //* ** USE NEW ROUTINE TO CONVERT DISTRIBUTION CODE          9/24/03  RM
                        pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                              //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
                        pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
                        pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                         //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
                        pdaTwradist.getPnd_Twradist_Pnd_Tax_Year_A().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                     //Natural: ASSIGN #TWRADIST.#TAX-YEAR-A := TIRF-TAX-YEAR
                        pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde());                                                //Natural: ASSIGN #TWRADIST.#IN-DIST := TIRF-DISTRIBUTION-CDE
                        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist());                                                        //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST
                        if (condition(Global.isEscape())) return;
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Dist_Code().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                      //Natural: ASSIGN #OUTB-DIST-CODE := #TWRADIST.#OUT-DIST
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Not_Det().getValue(pnd_I).equals("Y")))                                                      //Natural: IF TIRF-RES-TAXABLE-NOT-DET ( #I ) = 'Y'
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det().setValue("1");                                                                   //Natural: ASSIGN #OUTB-TAXABLE-NOT-DET := '1'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Ira_Distr().equals("Y")))                                                                                //Natural: IF TIRF-IRA-DISTR = 'Y'
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind().setValue("1");                                                                       //Natural: ASSIGN #OUTB-IRA-SEP-IND := '1'
                        }                                                                                                                                                 //Natural: END-IF
                        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_I));                    //Natural: ASSIGN #OUTB-STATE-TAX := TIRF-STATE-TAX-WTHLD ( #I )
                        //*  ROTH YR CHG FR 8 TO 4   12/12/07  RM
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Roth_Year().notEquals(getZero())))                                                                       //Natural: IF TIRF-ROTH-YEAR NE 0
                        {
                            //*      MOVE '0000'
                            //*        TO SUBSTR(#OUTB-1ST-YEAR-ROTH-CONTRIB,1,4)
                            //*      MOVE EDITED TIRF-ROTH-YEAR(EM=9999) TO #YEAR-A4
                            //*      MOVE #YEAR-A4 TO SUBSTR(#OUTB-1ST-YEAR-ROTH-CONTRIB,5,4)
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib().setValue(ldaTwrl9705.getForm_U_Tirf_Roth_Year());                          //Natural: MOVE TIRF-ROTH-YEAR TO #OUTB-1ST-YEAR-ROTH-CONTRIB
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib().reset();                                                                   //Natural: RESET #OUTB-1ST-YEAR-ROTH-CONTRIB
                        }                                                                                                                                                 //Natural: END-IF
                        //*  VIKRAM STARTS
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde().equals("C")))                                                                         //Natural: IF FORM-U.TIRF-DISTRIBUTION-CDE EQ 'C'
                        {
                            pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                               //Natural: RESET #S-TWRPYMNT-FORM-SD
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.setValue(pnd_Input_Parm_Pnd_In_Tax_Year);                                                      //Natural: MOVE #IN-TAX-YEAR TO #S-TWRPYMNT-TAX-YEAR
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                  //Natural: MOVE FORM-U.TIRF-TIN TO #S-TWRPYMNT-TAX-ID-NBR
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.setValue(ldaTwrl9705.getForm_U_Tirf_Contract_Nbr());                                       //Natural: MOVE FORM-U.TIRF-CONTRACT-NBR TO #S-TWRPYMNT-CONTRACT-NBR
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_Payee_Cde());                                             //Natural: MOVE FORM-U.TIRF-PAYEE-CDE TO #S-TWRPYMNT-PAYEE-CDE
                            ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                     //Natural: READ PAY BY TWRPYMNT-FORM-SD STARTING FROM #S-TWRPYMNT-FORM-SD
                            (
                            "READ01",
                            new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
                            new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
                            );
                            READ01:
                            while (condition(ldaTwrl9415.getVw_pay().readNextRow("READ01")))
                            {
                                if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Year().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year) || ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr)  //Natural: IF PAY.TWRPYMNT-TAX-YEAR NE #S-TWRPYMNT-TAX-YEAR OR PAY.TWRPYMNT-TAX-ID-NBR NE #S-TWRPYMNT-TAX-ID-NBR OR PAY.TWRPYMNT-CONTRACT-NBR NE #S-TWRPYMNT-CONTRACT-NBR OR PAY.TWRPYMNT-PAYEE-CDE NE #S-TWRPYMNT-PAYEE-CDE
                                    || ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr) || ldaTwrl9415.getPay_Twrpymnt_Payee_Cde().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde)))
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde().equals("C")))                                                                //Natural: IF PAY.TWRPYMNT-DISTRIBUTION-CDE EQ 'C'
                                {
                                    FOR02:                                                                                                                                //Natural: FOR #P-S-CNT = 1 TO 24
                                    for (pnd_P_S_Cnt.setValue(1); condition(pnd_P_S_Cnt.lessOrEqual(24)); pnd_P_S_Cnt.nadd(1))
                                    {
                                        if (condition((((ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_P_S_Cnt).equals("D") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_P_S_Cnt).equals("C"))  //Natural: IF PAY.TWRPYMNT-PAYMENT-TYPE ( #P-S-CNT ) EQ 'D' AND PAY.TWRPYMNT-SETTLE-TYPE ( #P-S-CNT ) EQ 'C' AND PAY.TWRPYMNT-PYMNT-STATUS ( #P-S-CNT ) EQ 'C' OR EQ ' ' AND PAY.TWRPYMNT-PYMNT-DTE ( #P-S-CNT ) NE ' '
                                            && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_P_S_Cnt).equals("C") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_P_S_Cnt).equals(" "))) 
                                            && ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_P_S_Cnt).notEquals(" "))))
                                        {
                                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment().setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_P_S_Cnt)); //Natural: ASSIGN #OUTB-DATE-OF-PAYMENT := PAY.TWRPYMNT-PYMNT-DTE ( #P-S-CNT )
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-FOR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-READ
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  VIKRAM ENDS
                        }                                                                                                                                                 //Natural: END-IF
                        //*   NEW YORK
                        if (condition(pnd_Input_Parm_Pnd_In_Input_State.equals("35")))                                                                                    //Natural: IF #IN-INPUT-STATE = '35'
                        {
                            ldaTwrl3503.getPnd_Out_Ny_Record().reset();                                                                                                   //Natural: RESET #OUT-NY-RECORD
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id().setValue("1W");                                                                             //Natural: ASSIGN #OUTW-NY-ID := '1W'
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                //Natural: ASSIGN #OUTW-NY-TIN := TIRF-TIN
                            pnd_Mid_Name.setValue(ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme());                                                                           //Natural: ASSIGN #MID-NAME := TIRF-PART-MDDLE-NME
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Name().setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme(),                     //Natural: COMPRESS TIRF-PART-LAST-NME TIRF-PART-FIRST-NME #MID-NAME1 INTO #OUTW-NY-NAME
                                ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(), pnd_Mid_Name_Pnd_Mid_Name1));
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1().setValue(" ");                                                                          //Natural: ASSIGN #OUTW-NY-BLANK1 := ' '
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind().setValue("O");                                                                       //Natural: ASSIGN #OUTW-NY-WAGES-IND := 'O'
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2().setValue(" ");                                                                          //Natural: ASSIGN #OUTW-NY-BLANK2 := ' '
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross().setValue(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_I));                      //Natural: ASSIGN #OUTW-NY-GROSS := TIRF-STATE-DISTR ( #I )
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross().setValue(0);                                                                             //Natural: ASSIGN #OUTW-NY-GROSS := 0
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3().setValue(" ");                                                                          //Natural: ASSIGN #OUTW-NY-BLANK3 := ' '
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_I));                //Natural: ASSIGN #OUTW-NY-TAXABLE-AMT := TIRF-STATE-DISTR ( #I )
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4().setValue(" ");                                                                          //Natural: ASSIGN #OUTW-NY-BLANK4 := ' '
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld().compute(new ComputeParameters(false, ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld()),  //Natural: ASSIGN #OUTW-NY-TAX-WTHLD := TIRF-STATE-TAX-WTHLD ( #I ) + TIRF-LOC-TAX-WTHLD ( #I )
                                ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_I).add(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_I)));
                            pnd_Out_Area_Pnd_Tirf_State_Tax_Wthld.setValue(ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld());                                     //Natural: ASSIGN #TIRF-STATE-TAX-WTHLD := #OUTW-NY-TAX-WTHLD
                            ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5().setValue(" ");                                                                          //Natural: ASSIGN #OUTW-NY-BLANK5 := ' '
                            getWorkFiles().write(1, false, ldaTwrl3503.getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1(), pnd_Fil1, pnd_Fil2, pnd_Fil3, pnd_Out_Area);               //Natural: WRITE WORK FILE 01 #OUT-NY-REC1 #FIL1 #FIL2 #FIL3 #OUT-AREA
                            pnd_Out_Count.nadd(1);                                                                                                                        //Natural: ADD 1 TO #OUT-COUNT
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        getWorkFiles().write(1, false, ldaTwrl3502.getPnd_Out_Irs_Record(), pnd_Out_Area);                                                                //Natural: WRITE WORK FILE 01 #OUT-IRS-RECORD #OUT-AREA
                        pnd_Out_Count.nadd(1);                                                                                                                            //Natural: ADD 1 TO #OUT-COUNT
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PREV-RUN
                //* *-------------------------------
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-CONTROL-EDIT
                //*  #WARNING             := 'Embedded blanks in Last Name  '
                //* *---------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_Prev_Run() throws Exception                                                                                                                    //Natural: CHECK-PREV-RUN
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Input_Parm_Pnd_In_Tax_Year);                                                                              //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #IN-TAX-YEAR
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(0);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 0
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRATBL4.#ABEND-IND := FALSE
        pnd_Rpt_Name_Pnd_Rpt_State_Code.setValue(pnd_Input_Parm_Pnd_In_Input_State);                                                                                      //Natural: ASSIGN #RPT-STATE-CODE := #IN-INPUT-STATE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Rec_Type().setValue(pnd_Rpt_Name);                                                                                                //Natural: ASSIGN #TWRATBL4.#REC-TYPE := #RPT-NAME
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4());                                                                        //Natural: CALLNAT 'TWRNTB4R' #TWRATBL4
        if (condition(Global.isEscape())) return;
        getReports().write(0, ReportOption.NOTITLE,"=",pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code(),"=",pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok);                       //Natural: WRITE '=' #TWRATBL4.#RET-CODE '=' #IN-NOT-FIRST-TIME-RUN-OK
        if (Global.isEscape()) return;
        if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().equals(true)))                                                                                           //Natural: IF #TWRATBL4.#RET-CODE = TRUE
        {
            if (condition(pnd_Input_Parm_Pnd_In_Not_First_Time_Run_Ok.equals("N")))                                                                                       //Natural: IF #IN-NOT-FIRST-TIME-RUN-OK = 'N'
            {
                DbsUtil.terminate(111);  if (true) return;                                                                                                                //Natural: TERMINATE 111
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                GET1:                                                                                                                                                     //Natural: GET TIRCNTL-RPT #TWRATBL4.#ISN
                ldaTwrltb4r.getVw_tircntl_Rpt().readByID(pdaTwratbl4.getPnd_Twratbl4_Pnd_Isn().getLong(), "GET1");
                ldaTwrltb4r.getVw_tircntl_Rpt().deleteDBRow("GET1");                                                                                                      //Natural: DELETE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  03/03/11
    private void sub_Name_Control_Edit() throws Exception                                                                                                                 //Natural: NAME-CONTROL-EDIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Ws_Pnd_Blank_Count.reset();                                                                                                                                   //Natural: RESET #WS.#BLANK-COUNT #WS.#WARNING
        pnd_Ws_Pnd_Warning.reset();
        pnd_Ws_Pnd_Name_Control_Edit.setValue(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme());                                                                                //Natural: ASSIGN #WS.#NAME-CONTROL-EDIT := #OUTB-NAME-CONTROL := TIRF-PART-LAST-NME
        ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Name_Control().setValue(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme());
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Name_Control_Edit,1,4), new ExamineSearch(" "), new ExamineGivingNumber(pnd_Ws_Pnd_Blank_Count));                    //Natural: EXAMINE SUBSTR ( #WS.#NAME-CONTROL-EDIT,1,4 ) FOR ' ' GIVING NUMBER #WS.#BLANK-COUNT
        if (condition(pnd_Ws_Pnd_Blank_Count.greater(getZero())))                                                                                                         //Natural: IF #WS.#BLANK-COUNT > 0
        {
            DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Name_Control_Edit,true), new ExamineSearch(" "), new ExamineDelete());                                           //Natural: EXAMINE FULL #WS.#NAME-CONTROL-EDIT FOR ' ' DELETE
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Name_Control().setValue(pnd_Ws_Pnd_Name_Control_Edit.getSubstring(1,4));                                           //Natural: ASSIGN #OUTB-NAME-CONTROL := SUBSTR ( #WS.#NAME-CONTROL-EDIT,1,4 )
            //*  DISPLAY (00)
            //*    '/TIN'                TIRF-TIN
            //*    '/PIN'                TIRF-PIN
            //*    '/Contract'           TIRF-CONTRACT-NBR
            //*    '/Payee'              TIRF-PAYEE-CDE
            //*    'Name/Cntl'           #OUTB-NAME-CONTROL
            //*    '/*** Last Name ***'  TIRF-PART-LAST-NME
            //*    '/*** Warning ***'    #WARNING
        }                                                                                                                                                                 //Natural: END-IF
        //*  NAME-CONTROL-EDIT                        /* 03/03/11
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"Number Of State Reporting Records Found.......",pnd_Out_Count);                  //Natural: WRITE ( 00 ) NOTITLE NOHDR 01T 'Number Of State Reporting Records Found.......' #OUT-COUNT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
