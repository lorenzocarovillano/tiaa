/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:18 PM
**        * FROM NATURAL PROGRAM : Twrp1737
************************************************************
**        * FILE NAME            : Twrp1737.java
**        * CLASS NAME           : Twrp1737
**        * INSTANCE NAME        : Twrp1737
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP0737
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  M. BERLIN
** PURPOSE     :  CREATE A REPORT OF OLDER THAN PREVIOUS YEAR MCCAMISH
**             :  TRANSMISSION DATA TO BE MANUALLY ADDED TO TAXWARS.
** ---------------------------------------------------------------------
** INPUT       :  TRANSACTION FILE EXTRACT FROM MCCAMISH
**
** OUTPUT      : TRANSACTION REPORT
** DATE        :  09/26/2011
************************************************
** MODIFICATION LOG
** 03/13/2012  M BERLIN - ADDED ERROR MESSAGE TO INDICATE RECORDS WITH
**                        INVALID OR MISSING TAX YEAR. /* MB 03/13/2012
************************************************
** DATE      PURPOSE
** 04/21/2017  J BREMER  PIN EXPANSION STOW FOR TWRL0700
************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1737 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0700 ldaTwrl0700;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Name_Address_Line;
    private DbsField pnd_Twrt_Record_Counter;
    private DbsField pnd_Error_Message;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Name_Address_Line = localVariables.newFieldInRecord("pnd_Name_Address_Line", "#NAME-ADDRESS-LINE", FieldType.STRING, 130);
        pnd_Twrt_Record_Counter = localVariables.newFieldInRecord("pnd_Twrt_Record_Counter", "#TWRT-RECORD-COUNTER", FieldType.NUMERIC, 7);
        pnd_Error_Message = localVariables.newFieldInRecord("pnd_Error_Message", "#ERROR-MESSAGE", FieldType.STRING, 50);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0700.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1737() throws Exception
    {
        super("Twrp1737");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*  TRANSACTION REPORT
        //* *   READ TRANSACTION FILE                                                                                                                                     //Natural: FORMAT ( 01 ) LS = 132 PS = 61
        WK01:                                                                                                                                                             //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            pnd_Twrt_Record_Counter.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TWRT-RECORD-COUNTER
            //*  MB 03/13/2012
            pnd_Error_Message.reset();                                                                                                                                    //Natural: RESET #ERROR-MESSAGE
            ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(1),  //Natural: COMPRESS TWRT-NA-LINE ( 1 ) ',' INTO TWRT-NA-LINE ( 1 ) LEAVING NO
                ","));
            ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(2).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(2),  //Natural: COMPRESS TWRT-NA-LINE ( 2 ) ',' INTO TWRT-NA-LINE ( 2 ) LEAVING NO
                ","));
            pnd_Name_Address_Line.setValue(DbsUtil.compress(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(1), ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(2), //Natural: COMPRESS TWRT-NA-LINE ( 1 ) TWRT-NA-LINE ( 2 ) TWRT-NA-LINE ( 3 ) TWRT-NA-LINE ( 4 ) TWRT-NA-LINE ( 5 ) TWRT-NA-LINE ( 6 ) TWRT-NA-LINE ( 7 ) TWRT-NA-LINE ( 8 ) INTO #NAME-ADDRESS-LINE
                ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(3), ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(4), ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(5), 
                ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(6), ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(7), ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(8)));
            //*  MB 03/13/2012 STARTS
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Filler().equals("INV.YEAR")))                                                                                   //Natural: IF TWRT-FILLER EQ 'INV.YEAR'
            {
                pnd_Error_Message.setValue(DbsUtil.compress("There is invalid TAX YEAR on the record:  ", "'", ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A(),              //Natural: COMPRESS 'There is invalid TAX YEAR on the record:  ' '"' TWRT-TAX-YEAR-A '"' INTO #ERROR-MESSAGE
                    "'"));
                //*  MB 03/13/2012 ENDS
            }                                                                                                                                                             //Natural: END-IF
            //* *
            //*  MB 03/13/2012
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Name_Address_Line,NEWLINE,pnd_Error_Message,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new  //Natural: WRITE ( 01 ) / #NAME-ADDRESS-LINE / #ERROR-MESSAGE / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X TWRT-PAYMT-DTE TWRT-TAX-ID ( EM = XXX-XX-XXXX ) TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT TWRT-IVC-AMT TWRT-FED-WHHLD-AMT TWRT-NRA-WHHLD-AMT TWRT-STATE-WHHLD-AMT TWRT-LOCAL-WHHLD-AMT TWRT-STATE-RSDNCY TWRT-CITIZENSHIP
                ColumnSpacing(1),ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),
                ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),
                ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),
                ldaTwrl0700.getTwrt_Record_Twrt_Citizenship());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("WK01"))) break;
                else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 01 ) 1
            //* *
            //*  (WK01.)
        }                                                                                                                                                                 //Natural: END-WORK
        WK01_Exit:
        if (Global.isEscape()) return;
        //* * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        getReports().skip(1, 2);                                                                                                                                          //Natural: AT TOP OF PAGE ( 01 );//Natural: SKIP ( 01 ) 2
        //* *
        if (condition(pnd_Twrt_Record_Counter.equals(getZero())))                                                                                                         //Natural: IF #TWRT-RECORD-COUNTER EQ 0
        {
            getReports().write(1, ReportOption.NOTITLE,"There are no records with years before the previous year","on transmission file today.");                         //Natural: WRITE ( 01 ) 'There are no records with years before the previous year' 'on transmission file today.'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,"Total Number of records in file: ",pnd_Twrt_Record_Counter);                                                          //Natural: WRITE ( 01 ) 'Total Number of records in file: ' #TWRT-RECORD-COUNTER
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(55),"***** End of Report *****");                                                                    //Natural: WRITE ( 01 ) 55X '***** End of Report *****'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: ",Global.getPROGRAM(),new           //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: ' *PROGRAM 18X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 01 ) / 35X 'McCamish before the previous year data' 28X 'Date: '*DATU // 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                ' '                                    /Puerto-Rico  /Canada        '
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,new 
                        ColumnSpacing(35),"McCamish before the previous year data",new ColumnSpacing(28),"Date: ",Global.getDATU(),NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest",
                        "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount  ",
                        "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ",
                        "                                    /Puerto-Rico  /Canada        ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=61");
    }
}
