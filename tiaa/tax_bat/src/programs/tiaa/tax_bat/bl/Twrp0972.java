/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:14 PM
**        * FROM NATURAL PROGRAM : Twrp0972
************************************************************
**        * FILE NAME            : Twrp0972.java
**        * CLASS NAME           : Twrp0972
**        * INSTANCE NAME        : Twrp0972
************************************************************
***********************************************************************
* PROGRAM  : TWRP0972
* SYSTEM   : TAXWARS
* TITLE    : GET PLAN INFO FROM CPS FILES
* HISTORY  :
* 08/10/14 - CTS    - INCORPORATE THE LOGIC OF IAACNTR TO GET PLAN INFO
*                     FROM IAA-CNTRCT FILE FOR ORIGIN CODE = AP OR NZ
*                     REF: PRB66126     TAG: CTS
* 11/20/17  SAI.K     RESTOW FOR PIN EXPANSION CHANGES
*  05/08/2018 GHOSHJ : PAYMENT STRATEGY (CHGXXXXX) TAG : GHOSHJ  0508
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0972 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Plan_Nmbr;

    private DbsGroup pnd_File1_Rec;
    private DbsField pnd_File1_Rec_Pnd_Tax_Company_Cde;
    private DbsField pnd_File1_Rec_Pnd_Filler1;
    private DbsField pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr;
    private DbsField pnd_File1_Rec_Pnd_Filler2;
    private DbsField pnd_File1_Rec_Pnd_Inp_Soc_Sec_Nbr;
    private DbsField pnd_File1_Rec_Pnd_Filler3;
    private DbsField pnd_File1_Rec_Pnd_Inp_Orgn_Cde;
    private DbsField pnd_File1_Rec_Pnd_Filler4;
    private DbsField pnd_File1_Rec_Pnd_Inp_Check_Dte;
    private DbsField pnd_File1_Rec_Pnd_Filler5;
    private DbsField pnd_File1_Rec_Pnd_Inp_Gross_Amt;
    private DbsField pnd_File1_Rec_Pnd_Filler6;
    private DbsField pnd_File1_Rec_Pnd_Inp_Fed_Tax;
    private DbsField pnd_File1_Rec_Pnd_Filler7;
    private DbsField pnd_File1_Rec_Pnd_Inp_Omni_Plan;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Plan_Type;
    private DbsField pnd_Plan_Num;
    private DbsField pnd_Inp_Check_Dte_N;

    private DbsGroup pnd_Inp_Check_Dte_N__R_Field_1;
    private DbsField pnd_Inp_Check_Dte_N_Pnd_Inp_Check_Dte_A;
    private DbsField pnd_Chk_Date;
    private DbsField pnd_Temp_Plan_Num;
    private DbsField pnd_I;
    private DbsField pnd_Select;
    private DbsField pnd_Cnt;

    private DbsGroup pnd_Out_Planrec;
    private DbsField pnd_Out_Planrec_Pnd_Out_Plan_Num;
    private DbsField pnd_Out_Planrec_Pnd_Filler11;
    private DbsField pnd_Out_Planrec_Pnd_Out_Company_Cde;
    private DbsField pnd_Out_Planrec_Pnd_Filler12;
    private DbsField pnd_Out_Planrec_Pnd_Out_Cntrct_Nbr;
    private DbsField pnd_Out_Planrec_Pnd_Filler13;
    private DbsField pnd_Out_Planrec_Pnd_Out_Soc_Sec_Nbr;
    private DbsField pnd_Out_Planrec_Pnd_Filler14;
    private DbsField pnd_Out_Planrec_Pnd_Out_Orgn_Cde;
    private DbsField pnd_Out_Planrec_Pnd_Filler15;
    private DbsField pnd_Out_Planrec_Pnd_Out_Check_Dte;
    private DbsField pnd_Out_Planrec_Pnd_Filler16;
    private DbsField pnd_Out_Planrec_Pnd_Out_Gross_Amt;
    private DbsField pnd_Out_Planrec_Pnd_Filler17;
    private DbsField pnd_Out_Planrec_Pnd_Out_Fed_Tax;
    private DbsField pnd_Out_Planrec_Pnd_Filler18;
    private DbsField pnd_Para_Rec;

    private DbsGroup pnd_Para_Rec__R_Field_2;
    private DbsField pnd_Para_Rec_Pnd_Cntrct_Nbr;
    private DbsField pnd_Para_Rec_Pnd_Plan_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        registerRecord(vw_iaa_Cntrct);

        pnd_File1_Rec = localVariables.newGroupInRecord("pnd_File1_Rec", "#FILE1-REC");
        pnd_File1_Rec_Pnd_Tax_Company_Cde = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Tax_Company_Cde", "#TAX-COMPANY-CDE", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Filler1 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr", "#INP-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_File1_Rec_Pnd_Filler2 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Soc_Sec_Nbr = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Soc_Sec_Nbr", "#INP-SOC-SEC-NBR", FieldType.STRING, 9);
        pnd_File1_Rec_Pnd_Filler3 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Orgn_Cde = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Orgn_Cde", "#INP-ORGN-CDE", FieldType.STRING, 2);
        pnd_File1_Rec_Pnd_Filler4 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler4", "#FILLER4", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Check_Dte = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Check_Dte", "#INP-CHECK-DTE", FieldType.STRING, 8);
        pnd_File1_Rec_Pnd_Filler5 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler5", "#FILLER5", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Gross_Amt = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Gross_Amt", "#INP-GROSS-AMT", FieldType.STRING, 13);
        pnd_File1_Rec_Pnd_Filler6 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler6", "#FILLER6", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Fed_Tax = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Fed_Tax", "#INP-FED-TAX", FieldType.STRING, 11);
        pnd_File1_Rec_Pnd_Filler7 = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Filler7", "#FILLER7", FieldType.STRING, 1);
        pnd_File1_Rec_Pnd_Inp_Omni_Plan = pnd_File1_Rec.newFieldInGroup("pnd_File1_Rec_Pnd_Inp_Omni_Plan", "#INP-OMNI-PLAN", FieldType.STRING, 6);
        pnd_Cntrct_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Plan_Type = localVariables.newFieldArrayInRecord("pnd_Plan_Type", "#PLAN-TYPE", FieldType.STRING, 10, new DbsArrayController(1, 15));
        pnd_Plan_Num = localVariables.newFieldInRecord("pnd_Plan_Num", "#PLAN-NUM", FieldType.STRING, 6);
        pnd_Inp_Check_Dte_N = localVariables.newFieldInRecord("pnd_Inp_Check_Dte_N", "#INP-CHECK-DTE-N", FieldType.NUMERIC, 8);

        pnd_Inp_Check_Dte_N__R_Field_1 = localVariables.newGroupInRecord("pnd_Inp_Check_Dte_N__R_Field_1", "REDEFINE", pnd_Inp_Check_Dte_N);
        pnd_Inp_Check_Dte_N_Pnd_Inp_Check_Dte_A = pnd_Inp_Check_Dte_N__R_Field_1.newFieldInGroup("pnd_Inp_Check_Dte_N_Pnd_Inp_Check_Dte_A", "#INP-CHECK-DTE-A", 
            FieldType.STRING, 8);
        pnd_Chk_Date = localVariables.newFieldInRecord("pnd_Chk_Date", "#CHK-DATE", FieldType.STRING, 8);
        pnd_Temp_Plan_Num = localVariables.newFieldInRecord("pnd_Temp_Plan_Num", "#TEMP-PLAN-NUM", FieldType.STRING, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Select = localVariables.newFieldInRecord("pnd_Select", "#SELECT", FieldType.BOOLEAN, 1);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 5);

        pnd_Out_Planrec = localVariables.newGroupInRecord("pnd_Out_Planrec", "#OUT-PLANREC");
        pnd_Out_Planrec_Pnd_Out_Plan_Num = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Plan_Num", "#OUT-PLAN-NUM", FieldType.STRING, 6);
        pnd_Out_Planrec_Pnd_Filler11 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler11", "#FILLER11", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Company_Cde = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Company_Cde", "#OUT-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Out_Planrec_Pnd_Filler12 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler12", "#FILLER12", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Cntrct_Nbr = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Cntrct_Nbr", "#OUT-CNTRCT-NBR", FieldType.STRING, 
            8);
        pnd_Out_Planrec_Pnd_Filler13 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler13", "#FILLER13", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Soc_Sec_Nbr = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Soc_Sec_Nbr", "#OUT-SOC-SEC-NBR", FieldType.STRING, 
            9);
        pnd_Out_Planrec_Pnd_Filler14 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler14", "#FILLER14", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Orgn_Cde = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Orgn_Cde", "#OUT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Out_Planrec_Pnd_Filler15 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler15", "#FILLER15", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Check_Dte = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Check_Dte", "#OUT-CHECK-DTE", FieldType.STRING, 8);
        pnd_Out_Planrec_Pnd_Filler16 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler16", "#FILLER16", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Gross_Amt = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Gross_Amt", "#OUT-GROSS-AMT", FieldType.STRING, 13);
        pnd_Out_Planrec_Pnd_Filler17 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler17", "#FILLER17", FieldType.STRING, 1);
        pnd_Out_Planrec_Pnd_Out_Fed_Tax = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Out_Fed_Tax", "#OUT-FED-TAX", FieldType.STRING, 11);
        pnd_Out_Planrec_Pnd_Filler18 = pnd_Out_Planrec.newFieldInGroup("pnd_Out_Planrec_Pnd_Filler18", "#FILLER18", FieldType.STRING, 1);
        pnd_Para_Rec = localVariables.newFieldInRecord("pnd_Para_Rec", "#PARA-REC", FieldType.STRING, 16);

        pnd_Para_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Para_Rec__R_Field_2", "REDEFINE", pnd_Para_Rec);
        pnd_Para_Rec_Pnd_Cntrct_Nbr = pnd_Para_Rec__R_Field_2.newFieldInGroup("pnd_Para_Rec_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Para_Rec_Pnd_Plan_Nbr = pnd_Para_Rec__R_Field_2.newFieldInGroup("pnd_Para_Rec_Pnd_Plan_Nbr", "#PLAN-NBR", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Cntrct.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0972() throws Exception
    {
        super("Twrp0972");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FILE1-REC
        while (condition(getWorkFiles().read(1, pnd_File1_Rec)))
        {
            pnd_Select.reset();                                                                                                                                           //Natural: RESET #SELECT
            //*  GHOSHJ
            if (condition(pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("OP") || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("  ") || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("VT")       //Natural: IF #INP-ORGN-CDE = 'OP' OR = '  ' OR = 'VT' OR = 'NL' OR = 'PL' OR = 'OL' OR = 'ML'
                || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("NL") || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("PL") || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("OL") 
                || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("ML")))
            {
                //*      #RCRD-TYP := '6'                            /* GHOSHJ  0508 >>
                //*      #ORGN-CDE := #INP-ORGN-CDE
                //*      #PPCN-NBR := #INP-CNTRCT-NBR
                //*      #PRCSS-SEQ-NBR := 0
                //*      MOVE #INP-CHECK-DTE  TO #CHECK-DTE
                //*      READ FCP-CONS-PLAN BY RCRD-ORGN-PPCN-PRCSS-CHKDT
                //*                         STARTING FROM #PLAN-KEY
                //*      IF CNTRCT-RCRD-TYP NE #RCRD-TYP OR CNTRCT-ORGN-CDE NE 'OP'  OR
                //*         CNTRCT-PPCN-NBR NE #PPCN-NBR
                //*          ESCAPE BOTTOM
                //*      END-IF
                //*      MOVE EDITED CNTL-CHECK-DTE(EM=YYYYMMDD)  TO #CHK-DATE
                //*      IF #CHK-DATE  = #CHECK-DTE
                //*         #SELECT := TRUE                          /* GHOSHJ  0508 <<
                pnd_Out_Planrec.reset();                                                                                                                                  //Natural: RESET #OUT-PLANREC
                pnd_Out_Planrec_Pnd_Filler11.setValue("|");                                                                                                               //Natural: MOVE '|' TO #FILLER11 #FILLER12 #FILLER13 #FILLER14 #FILLER15 #FILLER16 #FILLER17 #FILLER18
                pnd_Out_Planrec_Pnd_Filler12.setValue("|");
                pnd_Out_Planrec_Pnd_Filler13.setValue("|");
                pnd_Out_Planrec_Pnd_Filler14.setValue("|");
                pnd_Out_Planrec_Pnd_Filler15.setValue("|");
                pnd_Out_Planrec_Pnd_Filler16.setValue("|");
                pnd_Out_Planrec_Pnd_Filler17.setValue("|");
                pnd_Out_Planrec_Pnd_Filler18.setValue("|");
                //*        #CNT := #CNT + 1
                //*         MOVE PLAN-NUM TO #TEMP-PLAN-NUM           /* GHOSHJ  0508 >>
                //*      END-IF
                //*  END-READ                                         /* GHOSHJ  0508 <<
                pnd_Out_Planrec_Pnd_Out_Company_Cde.setValue(pnd_File1_Rec_Pnd_Tax_Company_Cde);                                                                          //Natural: MOVE #TAX-COMPANY-CDE TO #OUT-COMPANY-CDE
                //*      MOVE  #TEMP-PLAN-NUM      TO #OUT-PLAN-NUM   /* GHOSHJ  0508 >>
                pnd_Out_Planrec_Pnd_Out_Plan_Num.setValue(pnd_File1_Rec_Pnd_Inp_Omni_Plan);                                                                               //Natural: MOVE #INP-OMNI-PLAN TO #OUT-PLAN-NUM
                //*  GHOSHJ  0508 <<
                pnd_Out_Planrec_Pnd_Out_Soc_Sec_Nbr.setValue(pnd_File1_Rec_Pnd_Inp_Soc_Sec_Nbr);                                                                          //Natural: MOVE #INP-SOC-SEC-NBR TO #OUT-SOC-SEC-NBR
                pnd_Out_Planrec_Pnd_Out_Cntrct_Nbr.setValue(pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr);                                                                            //Natural: MOVE #INP-CNTRCT-NBR TO #OUT-CNTRCT-NBR
                pnd_Out_Planrec_Pnd_Out_Check_Dte.setValue(pnd_File1_Rec_Pnd_Inp_Check_Dte);                                                                              //Natural: MOVE #INP-CHECK-DTE TO #OUT-CHECK-DTE
                //*     #OUT-GROSS-AMT            := VAL(#INP-GROSS-AMT)
                //*     #OUT-FED-TAX              := VAL(#INP-FED-TAX)
                pnd_Out_Planrec_Pnd_Out_Gross_Amt.setValue(pnd_File1_Rec_Pnd_Inp_Gross_Amt);                                                                              //Natural: MOVE #INP-GROSS-AMT TO #OUT-GROSS-AMT
                pnd_Out_Planrec_Pnd_Out_Fed_Tax.setValue(pnd_File1_Rec_Pnd_Inp_Fed_Tax);                                                                                  //Natural: MOVE #INP-FED-TAX TO #OUT-FED-TAX
                pnd_Out_Planrec_Pnd_Out_Orgn_Cde.setValue(pnd_File1_Rec_Pnd_Inp_Orgn_Cde);                                                                                //Natural: MOVE #INP-ORGN-CDE TO #OUT-ORGN-CDE
                getWorkFiles().write(2, false, pnd_Out_Planrec);                                                                                                          //Natural: WRITE WORK FILE 2 #OUT-PLANREC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("AP") || pnd_File1_Rec_Pnd_Inp_Orgn_Cde.equals("NZ")))                                                //Natural: IF #INP-ORGN-CDE = 'AP' OR = 'NZ'
                {
                    //*  GHOSHJ >>
                    pnd_Out_Planrec_Pnd_Filler11.setValue("|");                                                                                                           //Natural: MOVE '|' TO #FILLER11 #FILLER12 #FILLER13 #FILLER14 #FILLER15 #FILLER16 #FILLER17 #FILLER18
                    pnd_Out_Planrec_Pnd_Filler12.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler13.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler14.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler15.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler16.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler17.setValue("|");
                    pnd_Out_Planrec_Pnd_Filler18.setValue("|");
                    //*  GHOSHJ <<
                    pnd_Para_Rec_Pnd_Cntrct_Nbr.setValue(pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr);                                                                               //Natural: MOVE #INP-CNTRCT-NBR TO #CNTRCT-NBR
                    //*      CALLNAT 'IAACNTR' #PARA-REC                            /* CTS
                    //*  CTS
                                                                                                                                                                          //Natural: PERFORM LOOKUP-IAACNTR
                    sub_Lookup_Iaacntr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().display(0, "PLAN NUM FOR AP",                                                                                                            //Natural: DISPLAY 'PLAN NUM FOR AP' #PARA-REC
                    		pnd_Para_Rec);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Temp_Plan_Num.setValue(pnd_Para_Rec_Pnd_Plan_Nbr);                                                                                                //Natural: MOVE #PLAN-NBR TO #TEMP-PLAN-NUM
                    pnd_Out_Planrec_Pnd_Out_Soc_Sec_Nbr.setValue(pnd_File1_Rec_Pnd_Inp_Soc_Sec_Nbr);                                                                      //Natural: MOVE #INP-SOC-SEC-NBR TO #OUT-SOC-SEC-NBR
                    if (condition(pnd_Temp_Plan_Num.notEquals(" ")))                                                                                                      //Natural: IF #TEMP-PLAN-NUM NOT = ' '
                    {
                        pnd_Out_Planrec_Pnd_Out_Plan_Num.setValue(pnd_Temp_Plan_Num);                                                                                     //Natural: MOVE #TEMP-PLAN-NUM TO #OUT-PLAN-NUM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Out_Planrec_Pnd_Out_Plan_Num.setValue("NOPLAN");                                                                                              //Natural: MOVE 'NOPLAN' TO #OUT-PLAN-NUM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Out_Planrec_Pnd_Out_Cntrct_Nbr.setValue(pnd_File1_Rec_Pnd_Inp_Cntrct_Nbr);                                                                        //Natural: MOVE #INP-CNTRCT-NBR TO #OUT-CNTRCT-NBR
                    pnd_Out_Planrec_Pnd_Out_Company_Cde.setValue(pnd_File1_Rec_Pnd_Tax_Company_Cde);                                                                      //Natural: MOVE #TAX-COMPANY-CDE TO #OUT-COMPANY-CDE
                    pnd_Out_Planrec_Pnd_Out_Check_Dte.setValue(pnd_File1_Rec_Pnd_Inp_Check_Dte);                                                                          //Natural: MOVE #INP-CHECK-DTE TO #OUT-CHECK-DTE
                    //*      #OUT-GROSS-AMT            := VAL(#INP-GROSS-AMT)
                    //*      #OUT-FED-TAX              := VAL(#INP-FED-TAX)
                    pnd_Out_Planrec_Pnd_Out_Gross_Amt.setValue(pnd_File1_Rec_Pnd_Inp_Gross_Amt);                                                                          //Natural: MOVE #INP-GROSS-AMT TO #OUT-GROSS-AMT
                    pnd_Out_Planrec_Pnd_Out_Fed_Tax.setValue(pnd_File1_Rec_Pnd_Inp_Fed_Tax);                                                                              //Natural: MOVE #INP-FED-TAX TO #OUT-FED-TAX
                    pnd_Out_Planrec_Pnd_Out_Orgn_Cde.setValue(pnd_File1_Rec_Pnd_Inp_Orgn_Cde);                                                                            //Natural: MOVE #INP-ORGN-CDE TO #OUT-ORGN-CDE
                    getWorkFiles().write(2, false, pnd_Out_Planrec);                                                                                                      //Natural: WRITE WORK FILE 2 #OUT-PLANREC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*   IF NOT #SELECT
            //*      WRITE 'MATCHING REC NOT FOUND ' #INP-CNTRCT-NBR #INP-ORGN-CDE
            //*   END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  WRITE 'TOTAL PROCESSED ' #CNT
        //* ******************************************************** CTS START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-IAACNTR
        //* ******************************************************************
        //* ********************************************************* CTS END
    }
    private void sub_Lookup_Iaacntr() throws Exception                                                                                                                    //Natural: LOOKUP-IAACNTR
    {
        if (BLNatReinput.isReinput()) return;

        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #PARA-REC.#CNTRCT-NBR
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Para_Rec_Pnd_Cntrct_Nbr, WcType.WITH) }
        );
        F1:
        while (condition(vw_iaa_Cntrct.readNextRow("F1", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "CONTRACT NOT FOUND",pnd_Para_Rec_Pnd_Cntrct_Nbr);                                                                                  //Natural: WRITE 'CONTRACT NOT FOUND' #PARA-REC.#CNTRCT-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Para_Rec_Pnd_Plan_Nbr.setValue(iaa_Cntrct_Plan_Nmbr);                                                                                                     //Natural: ASSIGN #PARA-REC.#PLAN-NBR := IAA-CNTRCT.PLAN-NMBR
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "PLAN NUM FOR AP",
        		pnd_Para_Rec);
    }
}
