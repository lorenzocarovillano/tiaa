/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:45:27 PM
**        * FROM NATURAL PROGRAM : Twrp9559
************************************************************
**        * FILE NAME            : Twrp9559.java
**        * CLASS NAME           : Twrp9559
**        * INSTANCE NAME        : Twrp9559
************************************************************
************************************************************************
* PROGRAM  : TWRP9559
* SYSTEM   : TAXWARS
* AUTHOR   : SNEHA SINHA
* TITLE    : BUILD MQFTE COMMANDS FOR MAINFRAME TO CCP DATA TRANSFER
* FUNCTION : THIS MODULE BUILDS MQFTE COMMANDS CONTROL CARDS FOR CCP.
*          : USED FOR CREATING MULTIPLE COPY COMMANDS FOR SENDING
*          : EXTRACT & TRIGGER FILES TO CCP SERVER.
* CREATED  : JULY 2018
*          :
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
*
*
* **********************************************************************
*
*
* OPTIONS TQMARK=OFF
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp9559 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_curr_Environment;
    private DbsField curr_Environment_Env_Id;
    private DbsField curr_Environment_Env_Descp;

    private DbsGroup pnd_Input_Parameters;
    private DbsField pnd_Input_Parameters_Pnd_Input_Rec;
    private DbsField pnd_Input_Rec1;

    private DbsGroup pnd_Input_Rec1__R_Field_1;
    private DbsField pnd_Input_Rec1_Pnd_Jobname;
    private DbsField pnd_Input_Rec1_Pnd_Curr_Env;
    private DbsField pnd_Input_Rec1_Pnd_Src_Hlq;
    private DbsField pnd_Input_Rec1_Pnd_Form_Type;
    private DbsField pnd_Input_Rec1_Pnd_Run_Type;
    private DbsField pnd_Input_Rec1_Pnd_Sys_Id;
    private DbsField pnd_Input_Rec1_Pnd_Sub_Sys;
    private DbsField pnd_Input_Rec2;

    private DbsGroup pnd_Input_Rec2__R_Field_2;
    private DbsField pnd_Input_Rec2_Pnd_Dst_Agent_Ccp;
    private DbsField pnd_Input_Rec2_Pnd_Dst_Agent_Om;
    private DbsField pnd_File_Id;
    private DbsField pnd_Src_Data_Fl;
    private DbsField pnd_Src_Trg_Fl;
    private DbsField pnd_Dst_Ccpdata_Fl;
    private DbsField pnd_Dst_Ccptrg_Fl;
    private DbsField pnd_Dst_Omdata_Fl;
    private DbsField pnd_Dst_Omtrg_Fl;
    private DbsField pnd_Dst_Ccp_Path;
    private DbsField pnd_Dst_Om_Path;
    private DbsField pnd_Dst_Filename;
    private DbsField pnd_Src_Agentname;
    private DbsField pnd_Input_Dsn_To;
    private DbsField pnd_Dest_Path;
    private DbsField pnd_Sys_Subsys;
    private DbsField pnd_Dts;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_3;
    private DbsField pnd_Date_Pnd_Cc;
    private DbsField pnd_Date_Pnd_Yymmdd;
    private DbsField pnd_Time;
    private DbsField pnd_Date_5;
    private DbsField pnd_Xml_Quote;
    private DbsField pnd_Txt_Quote;
    private DbsField pnd_Trg_Quote;

    private DbsGroup pnd_Profile_Data_In;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Id;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_App;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys;
    private DbsField pnd_Workfile;
    private DbsField pnd_In_Profile;
    private DbsField pnd_Found_Pkg;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_P;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Wk_Line;
    private DbsField pnd_Mqfte_Line;

    private DbsGroup pnd_Mqfte_Line__R_Field_4;
    private DbsField pnd_Mqfte_Line_Pnd_Mqfte_Parm80;
    private DbsField pnd_Mqfte_Line_Pnd_Mqfte_Filler;
    private DbsField pnd_Mqfte_Command;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_curr_Environment = new DataAccessProgramView(new NameInfo("vw_curr_Environment", "CURR-ENVIRONMENT"), "ENV_CURR", "ENV_CURR");
        curr_Environment_Env_Id = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Id", "ENV-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENV_ID");
        curr_Environment_Env_Descp = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Descp", "ENV-DESCP", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "ENV_DESCP");
        registerRecord(vw_curr_Environment);

        pnd_Input_Parameters = localVariables.newGroupInRecord("pnd_Input_Parameters", "#INPUT-PARAMETERS");
        pnd_Input_Parameters_Pnd_Input_Rec = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 
            80);
        pnd_Input_Rec1 = localVariables.newFieldInRecord("pnd_Input_Rec1", "#INPUT-REC1", FieldType.STRING, 80);

        pnd_Input_Rec1__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Rec1__R_Field_1", "REDEFINE", pnd_Input_Rec1);
        pnd_Input_Rec1_Pnd_Jobname = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Jobname", "#JOBNAME", FieldType.STRING, 8);
        pnd_Input_Rec1_Pnd_Curr_Env = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Curr_Env", "#CURR-ENV", FieldType.STRING, 2);
        pnd_Input_Rec1_Pnd_Src_Hlq = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Src_Hlq", "#SRC-HLQ", FieldType.STRING, 22);
        pnd_Input_Rec1_Pnd_Form_Type = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Form_Type", "#FORM-TYPE", FieldType.STRING, 8);
        pnd_Input_Rec1_Pnd_Run_Type = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 3);
        pnd_Input_Rec1_Pnd_Sys_Id = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Sys_Id", "#SYS-ID", FieldType.STRING, 3);
        pnd_Input_Rec1_Pnd_Sub_Sys = pnd_Input_Rec1__R_Field_1.newFieldInGroup("pnd_Input_Rec1_Pnd_Sub_Sys", "#SUB-SYS", FieldType.STRING, 3);
        pnd_Input_Rec2 = localVariables.newFieldInRecord("pnd_Input_Rec2", "#INPUT-REC2", FieldType.STRING, 80);

        pnd_Input_Rec2__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Rec2__R_Field_2", "REDEFINE", pnd_Input_Rec2);
        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp = pnd_Input_Rec2__R_Field_2.newFieldInGroup("pnd_Input_Rec2_Pnd_Dst_Agent_Ccp", "#DST-AGENT-CCP", FieldType.STRING, 
            30);
        pnd_Input_Rec2_Pnd_Dst_Agent_Om = pnd_Input_Rec2__R_Field_2.newFieldInGroup("pnd_Input_Rec2_Pnd_Dst_Agent_Om", "#DST-AGENT-OM", FieldType.STRING, 
            30);
        pnd_File_Id = localVariables.newFieldInRecord("pnd_File_Id", "#FILE-ID", FieldType.STRING, 8);
        pnd_Src_Data_Fl = localVariables.newFieldInRecord("pnd_Src_Data_Fl", "#SRC-DATA-FL", FieldType.STRING, 48);
        pnd_Src_Trg_Fl = localVariables.newFieldInRecord("pnd_Src_Trg_Fl", "#SRC-TRG-FL", FieldType.STRING, 48);
        pnd_Dst_Ccpdata_Fl = localVariables.newFieldInRecord("pnd_Dst_Ccpdata_Fl", "#DST-CCPDATA-FL", FieldType.STRING, 80);
        pnd_Dst_Ccptrg_Fl = localVariables.newFieldInRecord("pnd_Dst_Ccptrg_Fl", "#DST-CCPTRG-FL", FieldType.STRING, 80);
        pnd_Dst_Omdata_Fl = localVariables.newFieldInRecord("pnd_Dst_Omdata_Fl", "#DST-OMDATA-FL", FieldType.STRING, 80);
        pnd_Dst_Omtrg_Fl = localVariables.newFieldInRecord("pnd_Dst_Omtrg_Fl", "#DST-OMTRG-FL", FieldType.STRING, 80);
        pnd_Dst_Ccp_Path = localVariables.newFieldInRecord("pnd_Dst_Ccp_Path", "#DST-CCP-PATH", FieldType.STRING, 80);
        pnd_Dst_Om_Path = localVariables.newFieldInRecord("pnd_Dst_Om_Path", "#DST-OM-PATH", FieldType.STRING, 80);
        pnd_Dst_Filename = localVariables.newFieldInRecord("pnd_Dst_Filename", "#DST-FILENAME", FieldType.STRING, 80);
        pnd_Src_Agentname = localVariables.newFieldInRecord("pnd_Src_Agentname", "#SRC-AGENTNAME", FieldType.STRING, 44);
        pnd_Input_Dsn_To = localVariables.newFieldInRecord("pnd_Input_Dsn_To", "#INPUT-DSN-TO", FieldType.STRING, 80);
        pnd_Dest_Path = localVariables.newFieldInRecord("pnd_Dest_Path", "#DEST-PATH", FieldType.STRING, 60);
        pnd_Sys_Subsys = localVariables.newFieldInRecord("pnd_Sys_Subsys", "#SYS-SUBSYS", FieldType.STRING, 6);
        pnd_Dts = localVariables.newFieldInRecord("pnd_Dts", "#DTS", FieldType.STRING, 12);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Date__R_Field_3", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Cc = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Date_Pnd_Yymmdd = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Yymmdd", "#YYMMDD", FieldType.STRING, 6);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Date_5 = localVariables.newFieldInRecord("pnd_Date_5", "#DATE-5", FieldType.STRING, 5);
        pnd_Xml_Quote = localVariables.newFieldInRecord("pnd_Xml_Quote", "#XML-QUOTE", FieldType.STRING, 5);
        pnd_Txt_Quote = localVariables.newFieldInRecord("pnd_Txt_Quote", "#TXT-QUOTE", FieldType.STRING, 5);
        pnd_Trg_Quote = localVariables.newFieldInRecord("pnd_Trg_Quote", "#TRG-QUOTE", FieldType.STRING, 5);

        pnd_Profile_Data_In = localVariables.newGroupInRecord("pnd_Profile_Data_In", "#PROFILE-DATA-IN");
        pnd_Profile_Data_In_Pnd_Profile_Id = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Id", "#PROFILE-ID", FieldType.STRING, 
            2);
        pnd_Profile_Data_In_Pnd_Profile_App = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_App", "#PROFILE-APP", FieldType.STRING, 
            8);
        pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys", "#PROFILE-SYS-SUBSYS", 
            FieldType.STRING, 6);
        pnd_Workfile = localVariables.newFieldInRecord("pnd_Workfile", "#WORKFILE", FieldType.PACKED_DECIMAL, 2);
        pnd_In_Profile = localVariables.newFieldInRecord("pnd_In_Profile", "#IN-PROFILE", FieldType.STRING, 2);
        pnd_Found_Pkg = localVariables.newFieldInRecord("pnd_Found_Pkg", "#FOUND-PKG", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Wk_Line = localVariables.newFieldInRecord("pnd_Wk_Line", "#WK-LINE", FieldType.STRING, 80);
        pnd_Mqfte_Line = localVariables.newFieldInRecord("pnd_Mqfte_Line", "#MQFTE-LINE", FieldType.STRING, 200);

        pnd_Mqfte_Line__R_Field_4 = localVariables.newGroupInRecord("pnd_Mqfte_Line__R_Field_4", "REDEFINE", pnd_Mqfte_Line);
        pnd_Mqfte_Line_Pnd_Mqfte_Parm80 = pnd_Mqfte_Line__R_Field_4.newFieldInGroup("pnd_Mqfte_Line_Pnd_Mqfte_Parm80", "#MQFTE-PARM80", FieldType.STRING, 
            80);
        pnd_Mqfte_Line_Pnd_Mqfte_Filler = pnd_Mqfte_Line__R_Field_4.newFieldInGroup("pnd_Mqfte_Line_Pnd_Mqfte_Filler", "#MQFTE-FILLER", FieldType.STRING, 
            120);
        pnd_Mqfte_Command = localVariables.newFieldArrayInRecord("pnd_Mqfte_Command", "#MQFTE-COMMAND", FieldType.STRING, 120, new DbsArrayController(1, 
            9));

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_curr_Environment.reset();

        localVariables.reset();
        pnd_Xml_Quote.setInitialValue(".xml'");
        pnd_Txt_Quote.setInitialValue(".txt'");
        pnd_Trg_Quote.setInitialValue(".trg'");
        pnd_Mqfte_Command.getValue(1).setInitialValue("TIAA_WMQFTE_USERXFER_FUNCTION=FILECOPY");
        pnd_Mqfte_Command.getValue(2).setInitialValue("TIAA_WMQFTE_USERXFER_SRC_AGENTNAME=@SRC-AGENTNAME");
        pnd_Mqfte_Command.getValue(3).setInitialValue("TIAA_WMQFTE_USERXFER_SRC_FILENAME=@SRC-FILENAME");
        pnd_Mqfte_Command.getValue(4).setInitialValue("TIAA_WMQFTE_USERXFER_DST_AGENTNAME=@DST-AGENTNAME");
        pnd_Mqfte_Command.getValue(5).setInitialValue("TIAA_WMQFTE_USERXFER_DST_FILENAME=@DST-FILENAME");
        pnd_Mqfte_Command.getValue(6).setInitialValue("TIAA_WMQFTE_USERXFER_JOBNAME=@JOBNAME");
        pnd_Mqfte_Command.getValue(7).setInitialValue("TIAA_WMQFTE_USERXFER_OVERWRITE=False");
        pnd_Mqfte_Command.getValue(8).setInitialValue("TIAA_WMQFTE_USERXFER_CONVERSION=TEXT");
        pnd_Mqfte_Command.getValue(9).setInitialValue("TIAA_WMQFTE_USERXFER_PRIORITY=3");
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue(Global.getPROGRAM());
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("POST");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp9559() throws Exception
    {
        super("Twrp9559");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp9559|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT LS = 80 PS = 60;//Natural: FORMAT ( 0 ) PS = 60 LS = 132
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parameters);                                                                                 //Natural: INPUT #INPUT-PARAMETERS
                pnd_Input_Rec1.setValue(pnd_Input_Parameters_Pnd_Input_Rec);                                                                                              //Natural: ASSIGN #INPUT-REC1 := #INPUT-REC
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Input_Parameters);                                                                                 //Natural: INPUT #INPUT-PARAMETERS
                pnd_Input_Rec2.setValue(pnd_Input_Parameters_Pnd_Input_Rec);                                                                                              //Natural: ASSIGN #INPUT-REC2 := #INPUT-REC
                getReports().write(0, "=",pnd_Input_Rec1_Pnd_Jobname,"=",pnd_Input_Rec1_Pnd_Curr_Env,"=",pnd_Input_Rec1_Pnd_Src_Hlq,"=",pnd_Input_Rec1_Pnd_Form_Type,     //Natural: WRITE '=' #JOBNAME '=' #CURR-ENV '=' #SRC-HLQ '=' #FORM-TYPE '=' #RUN-TYPE '=' #SYS-ID '=' #SUB-SYS '=' #DST-AGENT-CCP '=' #DST-AGENT-OM
                    "=",pnd_Input_Rec1_Pnd_Run_Type,"=",pnd_Input_Rec1_Pnd_Sys_Id,"=",pnd_Input_Rec1_Pnd_Sub_Sys,"=",pnd_Input_Rec2_Pnd_Dst_Agent_Ccp,"=",
                    pnd_Input_Rec2_Pnd_Dst_Agent_Om);
                if (Global.isEscape()) return;
                if (condition(pnd_Input_Rec1_Pnd_Jobname.equals(" ")))                                                                                                    //Natural: IF #JOBNAME = ' '
                {
                    pnd_Input_Rec1_Pnd_Jobname.setValue(Global.getINIT_USER());                                                                                           //Natural: ASSIGN #JOBNAME := *INIT-USER
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Rec1_Pnd_Form_Type.equals(" ")))                                                                                                  //Natural: IF #FORM-TYPE = ' '
                {
                    short decideConditionsMet139 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1650A'
                    if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1650A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F1099R");                                                                                                  //Natural: ASSIGN #FORM-TYPE := 'F1099R'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1660A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1660A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F1099RS");                                                                                                 //Natural: ASSIGN #FORM-TYPE := 'F1099RS'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1654A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1654A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F1099I");                                                                                                  //Natural: ASSIGN #FORM-TYPE := 'F1099I'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1696A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1696A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F5498");                                                                                                   //Natural: ASSIGN #FORM-TYPE := 'F5498'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1670A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1670A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("FNR4");                                                                                                    //Natural: ASSIGN #FORM-TYPE := 'FNR4'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1680A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1680A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F4807C");                                                                                                  //Natural: ASSIGN #FORM-TYPE := 'F4807C'
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1685A'
                    else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1685A")))
                    {
                        decideConditionsMet139++;
                        pnd_Input_Rec1_Pnd_Form_Type.setValue("F1042S");                                                                                                  //Natural: ASSIGN #FORM-TYPE := 'F1042S'
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Rec1_Pnd_Sys_Id.equals(" ")))                                                                                                     //Natural: IF #SYS-ID = ' '
                {
                    pnd_Input_Rec1_Pnd_Sys_Id.setValue("041");                                                                                                            //Natural: ASSIGN #SYS-ID := '041'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Rec1_Pnd_Run_Type.equals("DRY")))                                                                                                 //Natural: IF #RUN-TYPE = 'DRY'
                {
                    if (condition(pnd_Input_Rec1_Pnd_Sub_Sys.equals(" ")))                                                                                                //Natural: IF #SUB-SYS = ' '
                    {
                        //*  1099R
                        //*  1099RS
                        //*  1099I
                        //*  5498
                        //*  NR4
                        //*  480.7C
                        //*  1042S
                        short decideConditionsMet170 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1650A'
                        if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1650A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("207");                                                                                                   //Natural: ASSIGN #SUB-SYS := '207'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1660A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1660A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("213");                                                                                                   //Natural: ASSIGN #SUB-SYS := '213'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1654A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1654A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("208");                                                                                                   //Natural: ASSIGN #SUB-SYS := '208'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1696A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1696A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("209");                                                                                                   //Natural: ASSIGN #SUB-SYS := '209'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1670A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1670A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("212");                                                                                                   //Natural: ASSIGN #SUB-SYS := '212'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1680A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1680A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("211");                                                                                                   //Natural: ASSIGN #SUB-SYS := '211'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1685A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1685A")))
                        {
                            decideConditionsMet170++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("210");                                                                                                   //Natural: ASSIGN #SUB-SYS := '210'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Rec1_Pnd_Sub_Sys.equals(" ")))                                                                                                //Natural: IF #SUB-SYS = ' '
                    {
                        //*  1099R
                        //*  1099RS
                        //*  1099I
                        //*  5498
                        //*  NR4
                        //*  480.7C
                        //*  1042S
                        short decideConditionsMet198 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1650A'
                        if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1650A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("107");                                                                                                   //Natural: ASSIGN #SUB-SYS := '107'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1660A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1660A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("113");                                                                                                   //Natural: ASSIGN #SUB-SYS := '113'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1654A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1654A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("108");                                                                                                   //Natural: ASSIGN #SUB-SYS := '108'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1696A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1696A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("109");                                                                                                   //Natural: ASSIGN #SUB-SYS := '109'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1670A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1670A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("112");                                                                                                   //Natural: ASSIGN #SUB-SYS := '112'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1680A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1680A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("111");                                                                                                   //Natural: ASSIGN #SUB-SYS := '111'
                        }                                                                                                                                                 //Natural: WHEN SUBSTR ( #JOBNAME,4,5 ) = '1685A'
                        else if (condition(pnd_Input_Rec1_Pnd_Jobname.getSubstring(4,5).equals("1685A")))
                        {
                            decideConditionsMet198++;
                            pnd_Input_Rec1_Pnd_Sub_Sys.setValue("110");                                                                                                   //Natural: ASSIGN #SUB-SYS := '110'
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mqfte_Line.reset();                                                                                                                                   //Natural: RESET #MQFTE-LINE
                if (condition(pnd_Input_Rec1_Pnd_Curr_Env.equals(" ")))                                                                                                   //Natural: IF #CURR-ENV = ' '
                {
                    vw_curr_Environment.startDatabaseRead                                                                                                                 //Natural: READ ( 1 ) CURR-ENVIRONMENT WITH ENV-ID = ' '
                    (
                    "READ01",
                    new Wc[] { new Wc("ENV_ID", ">=", " ", WcType.BY) },
                    new Oc[] { new Oc("ENV_ID", "ASC") },
                    1
                    );
                    READ01:
                    while (condition(vw_curr_Environment.readNextRow("READ01")))
                    {
                        pnd_Input_Rec1_Pnd_Curr_Env.setValue(curr_Environment_Env_Id);                                                                                    //Natural: ASSIGN #CURR-ENV := ENV-ID
                    }                                                                                                                                                     //Natural: END-READ
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "#CURR-ENV",pnd_Input_Rec1_Pnd_Curr_Env);                                                                                           //Natural: WRITE '#CURR-ENV' #CURR-ENV
                if (Global.isEscape()) return;
                if (condition(pnd_Input_Rec1_Pnd_Src_Hlq.equals(" ")))                                                                                                    //Natural: IF #SRC-HLQ = ' '
                {
                    short decideConditionsMet228 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2'
                    if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("P") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P2"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PNPD.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'PNPD.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'PF'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("PF"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TPFX.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TPFX.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'U'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("U"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TEMP.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TEMP.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'I1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I1"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TINT.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TINT.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'I2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I2"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TINT.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TINT.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'S1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S1"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS.P01.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TSYS.P01.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'S2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S2"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS2.P01.TAX.", pnd_File_Id));                              //Natural: COMPRESS 'TSYS2.P01.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'S4'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S4"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS4.P01.TAX.", pnd_File_Id));                              //Natural: COMPRESS 'TSYS4.P01.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'A'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("A"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TACT.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'TACT.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: VALUE 'DR'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("DR"))))
                    {
                        decideConditionsMet228++;
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PNPD.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'PNPD.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Input_Rec1_Pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PNPD.COR.TAX.", pnd_File_Id));                               //Natural: COMPRESS 'PNPD.COR.TAX.' #FILE-ID INTO #SRC-HLQ LEAVING NO
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Src_Agentname.equals(" ")))                                                                                                             //Natural: IF #SRC-AGENTNAME = ' '
                {
                    short decideConditionsMet255 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2','DR'
                    if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("P") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P2") 
                        || pnd_Input_Rec1_Pnd_Curr_Env.equals("DR"))))
                    {
                        decideConditionsMet255++;
                        pnd_Src_Agentname.setValue("AGMVSPROD1_SND");                                                                                                     //Natural: ASSIGN #SRC-AGENTNAME := 'AGMVSPROD1_SND'
                    }                                                                                                                                                     //Natural: VALUE 'PF','U','I1','I2','S1','S2','S4','A'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("PF") || pnd_Input_Rec1_Pnd_Curr_Env.equals("U") || pnd_Input_Rec1_Pnd_Curr_Env.equals("I1") 
                        || pnd_Input_Rec1_Pnd_Curr_Env.equals("I2") || pnd_Input_Rec1_Pnd_Curr_Env.equals("S1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("S2") 
                        || pnd_Input_Rec1_Pnd_Curr_Env.equals("S4") || pnd_Input_Rec1_Pnd_Curr_Env.equals("A"))))
                    {
                        decideConditionsMet255++;
                        pnd_Src_Agentname.setValue("AGMVSDNDV_SND");                                                                                                      //Natural: ASSIGN #SRC-AGENTNAME := 'AGMVSDNDV_SND'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Src_Agentname.setValue("AGMVSPROD1_SND");                                                                                                     //Natural: ASSIGN #SRC-AGENTNAME := 'AGMVSPROD1_SND'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.equals(" ")))                                                                                              //Natural: IF #DST-AGENT-CCP = ' '
                {
                    short decideConditionsMet266 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2'
                    if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("P") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P2"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGPDCHAPDA3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGPDCHAPDA3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'PF'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("PF"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGPFDENPFA3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGPFDENPFA3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'U','S1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("U") || pnd_Input_Rec1_Pnd_Curr_Env.equals("S1"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGD2DEND2B3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGD2DEND2B3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'I1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I1"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGI1DENI1B3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGI1DENI1B3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'I2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I2"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGI1DENI1B3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGI1DENI1B3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'S2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S2"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGS2DENS2B3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGS2DENS2B3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'S4'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S4"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGS4DENS4B3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGS4DENS4B3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'A'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("A"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGA1DENATA3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGA1DENATA3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'DR'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("DR"))))
                    {
                        decideConditionsMet266++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGDRDENDRA3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGDRDENDRA3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Input_Rec2_Pnd_Dst_Agent_Ccp.setValue("AGPDCHAPDA3WLCPE01_RCV");                                                                              //Natural: ASSIGN #DST-AGENT-CCP := 'AGPDCHAPDA3WLCPE01_RCV'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Rec2_Pnd_Dst_Agent_Om.equals(" ")))                                                                                               //Natural: IF #DST-AGENT-OM = ' '
                {
                    short decideConditionsMet291 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2'
                    if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("P") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P2"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGPDCHAPDDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGPDCHAPDDSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'PF'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("PF"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGPFCHAPFDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGPFCHAPFDSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'U'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("U"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGS1CHAST1DSHN01_RCV");                                                                                 //Natural: ASSIGN #DST-AGENT-OM := 'AGS1CHAST1DSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'I1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I1"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGI1CHASTDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGI1CHASTDSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'I2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I2"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGI1CHASTDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGI1CHASTDSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'S1'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S1"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGS1CHAST1DSHN01_RCV");                                                                                 //Natural: ASSIGN #DST-AGENT-OM := 'AGS1CHAST1DSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'S2'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S2"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGS2CHAST2DSHN01_RCV");                                                                                 //Natural: ASSIGN #DST-AGENT-OM := 'AGS2CHAST2DSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'S4'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S4"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGS4CHAST4DSHN01_RCV");                                                                                 //Natural: ASSIGN #DST-AGENT-OM := 'AGS4CHAST4DSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'A'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("A"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGA1CHAATDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGA1CHAATDSHN01_RCV'
                    }                                                                                                                                                     //Natural: VALUE 'DR'
                    else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("DR"))))
                    {
                        decideConditionsMet291++;
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGDRDENDRDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGDRDENDRDSHN01_RCV'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Input_Rec2_Pnd_Dst_Agent_Om.setValue("AGPDCHAPDDSHN01_RCV");                                                                                  //Natural: ASSIGN #DST-AGENT-OM := 'AGPDCHAPDDSHN01_RCV'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //* *
                pnd_Src_Data_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Rec1_Pnd_Src_Hlq, pnd_Input_Rec1_Pnd_Jobname, ".EXTR.",                //Natural: COMPRESS #SRC-HLQ #JOBNAME '.EXTR.' #FORM-TYPE INTO #SRC-DATA-FL LEAVING NO
                    pnd_Input_Rec1_Pnd_Form_Type));
                pnd_Src_Trg_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Rec1_Pnd_Src_Hlq, pnd_Input_Rec1_Pnd_Jobname, ".TRG.",                  //Natural: COMPRESS #SRC-HLQ #JOBNAME '.TRG.' #FORM-TYPE INTO #SRC-TRG-FL LEAVING NO
                    pnd_Input_Rec1_Pnd_Form_Type));
                //*   SETUP THE DESTINATION FILE NAME
                pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE
                pnd_Date_5.setValue(pnd_Date.getSubstring(4,5));                                                                                                          //Natural: ASSIGN #DATE-5 := SUBSTR ( #DATE,4,5 )
                pnd_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                  //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIME
                pnd_Dts.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Date_5, pnd_Time));                                                                  //Natural: COMPRESS #DATE-5 #TIME INTO #DTS LEAVING NO
                if (condition(pnd_Input_Rec1_Pnd_Curr_Env.equals("S1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("U")))                                                       //Natural: IF #CURR-ENV = 'S1' OR = 'U'
                {
                    pnd_Dst_Ccp_Path.setValue("/ecs-app/ccp-dev-2/event-preprocessor/drop/");                                                                             //Natural: ASSIGN #DST-CCP-PATH := '/ecs-app/ccp-dev-2/event-preprocessor/drop/'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Dst_Ccp_Path.setValue("/ecs-app/ccp/event-preprocessor/drop/");                                                                                   //Natural: ASSIGN #DST-CCP-PATH := '/ecs-app/ccp/event-preprocessor/drop/'
                }                                                                                                                                                         //Natural: END-IF
                //*  BUILD OM PATH
                //*  IF #DST-AGENT-OM = ' '
                short decideConditionsMet336 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2'
                if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("P") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P1") || pnd_Input_Rec1_Pnd_Curr_Env.equals("P2"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/pd1/resrc_disk1/DS_RIT_QRS/source/");                                                                                      //Natural: ASSIGN #DST-OM-PATH := '/pd1/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'PF'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("PF"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/pf/resrc_disk1/DS_RIT_QRS/source/");                                                                                       //Natural: ASSIGN #DST-OM-PATH := '/pf/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'U','S1'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("U") || pnd_Input_Rec1_Pnd_Curr_Env.equals("S1"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/dv/resrc_disk1/DS_RIT_QRS/source/");                                                                                       //Natural: ASSIGN #DST-OM-PATH := '/dv/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'I1'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I1"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/it/resrc_disk1/DS_RIT_QRS/source/");                                                                                       //Natural: ASSIGN #DST-OM-PATH := '/it/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'I2'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("I2"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/it/resrc_disk1/DS_RIT_QRS/source/");                                                                                       //Natural: ASSIGN #DST-OM-PATH := '/it/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'S2'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S2"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/st2/resrc_disk1/DS_RIT_QRS/source/");                                                                                      //Natural: ASSIGN #DST-OM-PATH := '/st2/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'S4'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("S4"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/st4/resrc_disk1/DS_RIT_QRS/source/");                                                                                      //Natural: ASSIGN #DST-OM-PATH := '/st4/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'A'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("A"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/at/resrc_disk1/DS_RIT_QRS/source/");                                                                                       //Natural: ASSIGN #DST-OM-PATH := '/at/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: VALUE 'DR'
                else if (condition((pnd_Input_Rec1_Pnd_Curr_Env.equals("DR"))))
                {
                    decideConditionsMet336++;
                    pnd_Dst_Om_Path.setValue("/dr1/resrc_disk1/DS_RIT_QRS/source/");                                                                                      //Natural: ASSIGN #DST-OM-PATH := '/dr1/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Dst_Om_Path.setValue("/pd1/resrc_disk1/DS_RIT_QRS/source/");                                                                                      //Natural: ASSIGN #DST-OM-PATH := '/pd1/resrc_disk1/DS_RIT_QRS/source/'
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  END-IF
                pnd_Dst_Ccpdata_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dst_Ccp_Path, pnd_Input_Rec1_Pnd_Sys_Id, pnd_Input_Rec1_Pnd_Sub_Sys,      //Natural: COMPRESS #DST-CCP-PATH #SYS-ID #SUB-SYS '-' #YYMMDD '-' #DTS '0000001.txt' INTO #DST-CCPDATA-FL LEAVING NO
                    "-", pnd_Date_Pnd_Yymmdd, "-", pnd_Dts, "0000001.txt"));
                pnd_Dst_Ccptrg_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dst_Ccp_Path, pnd_Input_Rec1_Pnd_Sys_Id, pnd_Input_Rec1_Pnd_Sub_Sys,       //Natural: COMPRESS #DST-CCP-PATH #SYS-ID #SUB-SYS '-' #YYMMDD '-' #DTS '0000001.trg' INTO #DST-CCPTRG-FL LEAVING NO
                    "-", pnd_Date_Pnd_Yymmdd, "-", pnd_Dts, "0000001.trg"));
                pnd_Dst_Omdata_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dst_Om_Path, pnd_Input_Rec1_Pnd_Sys_Id, pnd_Input_Rec1_Pnd_Sub_Sys,        //Natural: COMPRESS #DST-OM-PATH #SYS-ID #SUB-SYS '-' #YYMMDD '-' #DTS '0000001.txt' INTO #DST-OMDATA-FL LEAVING NO
                    "-", pnd_Date_Pnd_Yymmdd, "-", pnd_Dts, "0000001.txt"));
                pnd_Dst_Omtrg_Fl.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dst_Om_Path, pnd_Input_Rec1_Pnd_Sys_Id, pnd_Input_Rec1_Pnd_Sub_Sys,         //Natural: COMPRESS #DST-OM-PATH #SYS-ID #SUB-SYS '-' #YYMMDD '-' #DTS '0000001.trg' INTO #DST-OMTRG-FL LEAVING NO
                    "-", pnd_Date_Pnd_Yymmdd, "-", pnd_Dts, "0000001.trg"));
                //*  BUILD MQFTE COMMAND FILES
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO 9
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
                {
                    short decideConditionsMet373 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I = 1
                    if (condition(pnd_I.equals(1)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Command.getValue(pnd_I), "'"));                            //Natural: COMPRESS "'" #MQFTE-COMMAND ( #I ) "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 2
                    else if (condition(pnd_I.equals(2)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-AGENTNAME"), new ExamineReplace(pnd_Src_Agentname));                   //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-AGENTNAME' AND REPLACE WITH #SRC-AGENTNAME
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 3
                    else if (condition(pnd_I.equals(3)))
                    {
                        decideConditionsMet373++;
                        //*  WRITE EXTR FILE COMMANDS
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-FILENAME"), new ExamineReplace(pnd_Src_Data_Fl));                      //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-FILENAME' AND REPLACE WITH #SRC-DATA-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        //*  WRITE TRG FILE COMMANDS
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-FILENAME"), new ExamineReplace(pnd_Src_Trg_Fl));                       //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-FILENAME' AND REPLACE WITH #SRC-TRG-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 4
                    else if (condition(pnd_I.equals(4)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-AGENTNAME"), new ExamineReplace(pnd_Input_Rec2_Pnd_Dst_Agent_Ccp));    //Natural: EXAMINE #MQFTE-LINE FOR '@DST-AGENTNAME' AND REPLACE WITH #DST-AGENT-CCP
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        //* *
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-AGENTNAME"), new ExamineReplace(pnd_Input_Rec2_Pnd_Dst_Agent_Om));     //Natural: EXAMINE #MQFTE-LINE FOR '@DST-AGENTNAME' AND REPLACE WITH #DST-AGENT-OM
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 5
                    else if (condition(pnd_I.equals(5)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineReplace(pnd_Dst_Ccpdata_Fl));                   //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' AND REPLACE WITH #DST-CCPDATA-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineReplace(pnd_Dst_Ccptrg_Fl));                    //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' AND REPLACE WITH #DST-CCPTRG-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineReplace(pnd_Dst_Omdata_Fl));                    //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' AND REPLACE WITH #DST-OMDATA-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineReplace(pnd_Dst_Omtrg_Fl));                     //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' AND REPLACE WITH #DST-OMTRG-FL
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 6
                    else if (condition(pnd_I.equals(6)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(pnd_Mqfte_Command.getValue(pnd_I));                                                                                       //Natural: MOVE #MQFTE-COMMAND ( #I ) TO #MQFTE-LINE
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@JOBNAME"), new ExamineReplace(pnd_Input_Rec1_Pnd_Jobname));                //Natural: EXAMINE #MQFTE-LINE FOR '@JOBNAME' REPLACE WITH #JOBNAME
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Line, "'"));                                               //Natural: COMPRESS "'" #MQFTE-LINE "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 7
                    else if (condition(pnd_I.equals(7)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Command.getValue(pnd_I), "'"));                            //Natural: COMPRESS "'" #MQFTE-COMMAND ( #I ) "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 8
                    else if (condition(pnd_I.equals(8)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Command.getValue(pnd_I), "'"));                            //Natural: COMPRESS "'" #MQFTE-COMMAND ( #I ) "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN #I = 9
                    else if (condition(pnd_I.equals(9)))
                    {
                        decideConditionsMet373++;
                        pnd_Mqfte_Line.reset();                                                                                                                           //Natural: RESET #MQFTE-LINE
                        pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "'", pnd_Mqfte_Command.getValue(pnd_I), "'"));                            //Natural: COMPRESS "'" #MQFTE-COMMAND ( #I ) "'" INTO #MQFTE-LINE LEAVING NO
                        getWorkFiles().write(1, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 01 #MQFTE-LINE
                        getWorkFiles().write(2, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 02 #MQFTE-LINE
                        getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 03 #MQFTE-LINE
                        getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                   //Natural: WRITE WORK FILE 04 #MQFTE-LINE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60");
        Global.format(0, "PS=60 LS=132");
    }
}
