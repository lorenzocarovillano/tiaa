/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:44 PM
**        * FROM NATURAL PROGRAM : Twrp4560
************************************************************
**        * FILE NAME            : Twrp4560.java
**        * CLASS NAME           : Twrp4560
**        * INSTANCE NAME        : Twrp4560
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4560
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : "IRS" FORMS EXTRACT UPDATE THE FORM DATA BASE FILE.
* CREATED  : 07 / 11 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS IRS 5498 CORRECTIONS, REJECTS, AND LINKED
*            TINS FORM RECORDS, AND UPDATES THEM AS PROCESSED.
*            PROGRAM ALSO UPDATES THE MASS MAILING DATE ON THE
*            CONTROL DATA BASE FILE.
* 07/05/06  RM  RESTOW FOR TWRL452A UPDATES.
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4560 extends BLNatBase
{
    // Data Areas
    private LdaTwrl452a ldaTwrl452a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Lu_User;
    private DbsField form_Tirf_Irs_Rpt_Ind;
    private DbsField form_Tirf_Lu_Ts;
    private DbsField form_Tirf_Irs_Rpt_Date;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_Tbl_Nbr;
    private DbsField cntl_Tircntl_Tax_Year;
    private DbsField cntl_Tircntl_Rpt_Form_Name;

    private DataAccessProgramView vw_cntlu;
    private DbsField cntlu_Count_Casttircntl_Rpt_Tbl_Pe;

    private DbsGroup cntlu_Tircntl_Rpt_Tbl_Pe;
    private DbsField cntlu_Tircntl_Rpt_Dte;
    private DbsField pnd_Super_Cntl;

    private DbsGroup pnd_Super_Cntl__R_Field_1;
    private DbsField pnd_Super_Cntl_Pnd_S_Tbl;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year;

    private DbsGroup pnd_Super_Cntl__R_Field_2;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year_N;
    private DbsField pnd_Super_Cntl_Pnd_S_Form;
    private DbsField pnd_Input_Tax_Year;
    private DbsField pnd_Corr_Ctr;
    private DbsField pnd_Hold_Ctr;
    private DbsField pnd_Ltin_Ctr;
    private DbsField pnd_Et_Counter;
    private DbsField pnd_Datn;
    private DbsField pnd_Datx;
    private DbsField pnd_Timx;
    private DbsField i;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl452a = new LdaTwrl452a();
        registerRecord(ldaTwrl452a);

        // Local Variables
        localVariables = new DbsRecord();

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Tirf_Lu_User = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Tirf_Irs_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_Tirf_Lu_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Tirf_Irs_Rpt_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        registerRecord(vw_form);

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        cntl_Tircntl_Tbl_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_Tircntl_Tax_Year = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_Tircntl_Rpt_Form_Name = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIRCNTL_RPT_FORM_NAME");
        registerRecord(vw_cntl);

        vw_cntlu = new DataAccessProgramView(new NameInfo("vw_cntlu", "CNTLU"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        cntlu_Count_Casttircntl_Rpt_Tbl_Pe = vw_cntlu.getRecord().newFieldInGroup("cntlu_Count_Casttircntl_Rpt_Tbl_Pe", "C*TIRCNTL-RPT-TBL-PE", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");

        cntlu_Tircntl_Rpt_Tbl_Pe = vw_cntlu.getRecord().newGroupInGroup("cntlu_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntlu_Tircntl_Rpt_Dte = cntlu_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("cntlu_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntlu_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        registerRecord(vw_cntlu);

        pnd_Super_Cntl = localVariables.newFieldInRecord("pnd_Super_Cntl", "#SUPER-CNTL", FieldType.STRING, 12);

        pnd_Super_Cntl__R_Field_1 = localVariables.newGroupInRecord("pnd_Super_Cntl__R_Field_1", "REDEFINE", pnd_Super_Cntl);
        pnd_Super_Cntl_Pnd_S_Tbl = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tbl", "#S-TBL", FieldType.NUMERIC, 1);
        pnd_Super_Cntl_Pnd_S_Tax_Year = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.STRING, 4);

        pnd_Super_Cntl__R_Field_2 = pnd_Super_Cntl__R_Field_1.newGroupInGroup("pnd_Super_Cntl__R_Field_2", "REDEFINE", pnd_Super_Cntl_Pnd_S_Tax_Year);
        pnd_Super_Cntl_Pnd_S_Tax_Year_N = pnd_Super_Cntl__R_Field_2.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year_N", "#S-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_Cntl_Pnd_S_Form = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Form", "#S-FORM", FieldType.STRING, 7);
        pnd_Input_Tax_Year = localVariables.newFieldInRecord("pnd_Input_Tax_Year", "#INPUT-TAX-YEAR", FieldType.STRING, 4);
        pnd_Corr_Ctr = localVariables.newFieldInRecord("pnd_Corr_Ctr", "#CORR-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Hold_Ctr = localVariables.newFieldInRecord("pnd_Hold_Ctr", "#HOLD-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ltin_Ctr = localVariables.newFieldInRecord("pnd_Ltin_Ctr", "#LTIN-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Counter = localVariables.newFieldInRecord("pnd_Et_Counter", "#ET-COUNTER", FieldType.PACKED_DECIMAL, 4);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_cntl.reset();
        vw_cntlu.reset();

        ldaTwrl452a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4560() throws Exception
    {
        super("Twrp4560");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp4560|Main");
        OnErrorManager.pushEvent("TWRP4560", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Tax_Year);                                                                                   //Natural: INPUT #INPUT-TAX-YEAR
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Datn.setValue(Global.getDATN());                                                                                                                      //Natural: ASSIGN #DATN := *DATN
                pnd_Datx.setValue(Global.getDATX());                                                                                                                      //Natural: ASSIGN #DATX := *DATX
                pnd_Timx.setValue(Global.getTIMX());                                                                                                                      //Natural: ASSIGN #TIMX := *TIMX
                RW1:                                                                                                                                                      //Natural: READ WORK FILE 01 RECORD #FORM
                while (condition(getWorkFiles().read(1, ldaTwrl452a.getPnd_Form())))
                {
                    pnd_Corr_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CORR-CTR
                    G1:                                                                                                                                                   //Natural: GET FORM #F-ISN
                    vw_form.readByID(ldaTwrl452a.getPnd_Form_Pnd_F_Isn().getLong(), "G1");
                    form_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN TIRF-LU-USER := *INIT-USER
                    form_Tirf_Irs_Rpt_Ind.setValue("C");                                                                                                                  //Natural: ASSIGN TIRF-IRS-RPT-IND := 'C'
                    form_Tirf_Lu_Ts.setValue(pnd_Timx);                                                                                                                   //Natural: ASSIGN TIRF-LU-TS := #TIMX
                    form_Tirf_Irs_Rpt_Date.setValue(pnd_Datx);                                                                                                            //Natural: ASSIGN TIRF-IRS-RPT-DATE := #DATX
                    vw_form.updateDBRow("G1");                                                                                                                            //Natural: UPDATE ( G1. )
                    //*  ADD  1  TO  #ET-COUNTER
                    //*  IF #ET-COUNTER  >  50
                    //*     #ET-COUNTER :=  0
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    //*  END-IF
                }                                                                                                                                                         //Natural: END-WORK
                RW1_Exit:
                if (Global.isEscape()) return;
                //* *------
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Corr_Ctr.equals(getZero())))                                                                                                            //Natural: IF #CORR-CTR = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(6),"Form (5498) Extract File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new           //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) Extract File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                RW2:                                                                                                                                                      //Natural: READ WORK FILE 02 RECORD #FORM
                while (condition(getWorkFiles().read(2, ldaTwrl452a.getPnd_Form())))
                {
                    pnd_Hold_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #HOLD-CTR
                    G2:                                                                                                                                                   //Natural: GET FORM #F-ISN
                    vw_form.readByID(ldaTwrl452a.getPnd_Form_Pnd_F_Isn().getLong(), "G2");
                    form_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN TIRF-LU-USER := *INIT-USER
                    form_Tirf_Irs_Rpt_Ind.setValue("H");                                                                                                                  //Natural: ASSIGN TIRF-IRS-RPT-IND := 'H'
                    form_Tirf_Lu_Ts.setValue(pnd_Timx);                                                                                                                   //Natural: ASSIGN TIRF-LU-TS := #TIMX
                    form_Tirf_Irs_Rpt_Date.setValue(pnd_Datx);                                                                                                            //Natural: ASSIGN TIRF-IRS-RPT-DATE := #DATX
                    vw_form.updateDBRow("G2");                                                                                                                            //Natural: UPDATE ( G2. )
                    //*  ADD  1  TO  #ET-COUNTER
                    //*  IF #ET-COUNTER  >  50
                    //*     #ET-COUNTER :=  0
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    //*  END-IF
                }                                                                                                                                                         //Natural: END-WORK
                RW2_Exit:
                if (Global.isEscape()) return;
                //* *------
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                RW3:                                                                                                                                                      //Natural: READ WORK FILE 03 RECORD #FORM
                while (condition(getWorkFiles().read(3, ldaTwrl452a.getPnd_Form())))
                {
                    pnd_Ltin_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #LTIN-CTR
                    G3:                                                                                                                                                   //Natural: GET FORM #F-ISN
                    vw_form.readByID(ldaTwrl452a.getPnd_Form_Pnd_F_Isn().getLong(), "G3");
                    form_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                    //Natural: ASSIGN TIRF-LU-USER := *INIT-USER
                    form_Tirf_Irs_Rpt_Ind.setValue("L");                                                                                                                  //Natural: ASSIGN TIRF-IRS-RPT-IND := 'L'
                    form_Tirf_Lu_Ts.setValue(pnd_Timx);                                                                                                                   //Natural: ASSIGN TIRF-LU-TS := #TIMX
                    form_Tirf_Irs_Rpt_Date.setValue(pnd_Datx);                                                                                                            //Natural: ASSIGN TIRF-IRS-RPT-DATE := #DATX
                    vw_form.updateDBRow("G3");                                                                                                                            //Natural: UPDATE ( G3. )
                    //*  ADD  1  TO  #ET-COUNTER
                    //*  IF #ET-COUNTER  >  50
                    //*     #ET-COUNTER :=  0
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    //*  END-IF
                }                                                                                                                                                         //Natural: END-WORK
                RW3_Exit:
                if (Global.isEscape()) return;
                //* *------
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
                sub_Update_Control_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *--------------------------------------
                //* *------------
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 02 )
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 03 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //* *-------                                                                                                                                              //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Cntl_Pnd_S_Tbl.setValue(4);                                                                                                                             //Natural: ASSIGN #S-TBL := 4
        pnd_Super_Cntl_Pnd_S_Tax_Year.setValue(pnd_Input_Tax_Year);                                                                                                       //Natural: ASSIGN #S-TAX-YEAR := #INPUT-TAX-YEAR
        pnd_Super_Cntl_Pnd_S_Form.setValue("5498");                                                                                                                       //Natural: ASSIGN #S-FORM := '5498'
        vw_cntl.startDatabaseRead                                                                                                                                         //Natural: READ ( 1 ) CNTL WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER-CNTL
        (
        "RL1",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super_Cntl, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") },
        1
        );
        RL1:
        while (condition(vw_cntl.readNextRow("RL1")))
        {
            if (condition(cntl_Tircntl_Tbl_Nbr.equals(pnd_Super_Cntl_Pnd_S_Tbl) && cntl_Tircntl_Tax_Year.equals(pnd_Super_Cntl_Pnd_S_Tax_Year_N) && cntl_Tircntl_Rpt_Form_Name.equals(pnd_Super_Cntl_Pnd_S_Form))) //Natural: IF TIRCNTL-TBL-NBR = #S-TBL AND TIRCNTL-TAX-YEAR = #S-TAX-YEAR-N AND TIRCNTL-RPT-FORM-NAME = #S-FORM
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            G9:                                                                                                                                                           //Natural: GET CNTLU *ISN ( RL1. )
            vw_cntlu.readByID(vw_cntl.getAstISN("RL1"), "G9");
            i.compute(new ComputeParameters(false, i), cntlu_Count_Casttircntl_Rpt_Tbl_Pe.add(1));                                                                        //Natural: ASSIGN I := C*TIRCNTL-RPT-TBL-PE + 1
            if (condition(i.greater(12)))                                                                                                                                 //Natural: IF I > 12
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cntlu_Tircntl_Rpt_Dte.getValue(i).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Datn);                                                                //Natural: MOVE EDITED #DATN TO CNTLU.TIRCNTL-RPT-DTE ( I ) ( EM = YYYYMMDD )
                vw_cntlu.updateDBRow("G9");                                                                                                                               //Natural: UPDATE ( G9. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(vw_cntl.getAstCOUNTER().equals(getZero())))                                                                                                         //Natural: IF *COUNTER ( RL1. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Report Form (5498) Control File Record Is Missing",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'Report Form (5498) Control File Record Is Missing' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, new TabSetting(1),"IRS 5498 Correction Forms Updated..........",pnd_Corr_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                  //Natural: WRITE ( 00 ) 01T 'IRS 5498 Correction Forms Updated..........' #CORR-CTR / 01T 'IRS 5498 On Hold Forms Updated.............' #HOLD-CTR / 01T 'IRS 5498 Link Tin Forms Updated............' #LTIN-CTR
            TabSetting(1),"IRS 5498 On Hold Forms Updated.............",pnd_Hold_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(1),"IRS 5498 Link Tin Forms Updated............",pnd_Ltin_Ctr, 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"IRS 5498 Correction Forms Updated..........",pnd_Corr_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));       //Natural: WRITE ( 01 ) 01T 'IRS 5498 Correction Forms Updated..........' #CORR-CTR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"IRS 5498 On Hold Forms Updated.............",pnd_Hold_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));       //Natural: WRITE ( 02 ) 01T 'IRS 5498 On Hold Forms Updated.............' #HOLD-CTR
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"IRS 5498 Link Tin Forms Updated............",pnd_Ltin_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));       //Natural: WRITE ( 03 ) 01T 'IRS 5498 Link Tin Forms Updated............' #LTIN-CTR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(41),"        Tax Withholding & Reporting System        ",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 41T '        Tax Withholding & Reporting System        ' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"IRS 5498 (CORR) Data Base, And Control File Update",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 41T 'IRS 5498 (CORR) Data Base, And Control File Update' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    //*  WRITE (01) NOTITLE
                    //*    25T #HEADER-DATE (EM=MM/DD/YYYY)
                    //*    58T #SOURCE-TOTAL-HEADER
                    //*    97T #HEADER-DATE (EM=MM/DD/YYYY)
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    //*  WRITE (01) NOTITLE
                    //*    22T '       Input        '
                    //*    43T '      Bypassed      '
                    //*    64T '      Rejected      '
                    //*    85T '      Accepted      '
                    //*  WRITE (01) NOTITLE
                    //*    01T '                    '
                    //*    22T '===================='
                    //*    43T '===================='
                    //*    64T '===================='
                    //*    85T '===================='
                    //*   106T '===================='
                    //*  SKIP (01) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(41),"        Tax Withholding & Reporting System        ",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 41T '        Tax Withholding & Reporting System        ' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"IRS 5498 (HOLD) Data Base, And Control File Update",new  //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 41T 'IRS 5498 (HOLD) Data Base, And Control File Update' 120T 'REPORT: RPT2'
                        TabSetting(120),"REPORT: RPT2");
                    //*  WRITE (02) NOTITLE
                    //*    25T #HEADER-DATE (EM=MM/DD/YYYY)
                    //*    58T #SOURCE-TOTAL-HEADER
                    //*    97T #HEADER-DATE (EM=MM/DD/YYYY)
                    getReports().skip(2, 2);                                                                                                                              //Natural: SKIP ( 02 ) 2 LINES
                    //*  WRITE (02) NOTITLE
                    //*    22T '       Input        '
                    //*    43T '      Bypassed      '
                    //*    64T '      Rejected      '
                    //*    85T '      Accepted      '
                    //*  WRITE (02) NOTITLE
                    //*    01T '                    '
                    //*    22T '===================='
                    //*    43T '===================='
                    //*    64T '===================='
                    //*    85T '===================='
                    //*   106T '===================='
                    //*  SKIP (02) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(3, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(41),"        Tax Withholding & Reporting System        ",new  //Natural: WRITE ( 03 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 41T '        Tax Withholding & Reporting System        ' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(3, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"IRS 5498 (LTIN) Data Base, And Control File Update",new  //Natural: WRITE ( 03 ) NOTITLE *INIT-USER '-' *PROGRAM 41T 'IRS 5498 (LTIN) Data Base, And Control File Update' 120T 'REPORT: RPT3'
                        TabSetting(120),"REPORT: RPT3");
                    //*  WRITE (03) NOTITLE
                    //*    25T #HEADER-DATE (EM=MM/DD/YYYY)
                    //*    58T #SOURCE-TOTAL-HEADER
                    //*    97T #HEADER-DATE (EM=MM/DD/YYYY)
                    getReports().skip(3, 2);                                                                                                                              //Natural: SKIP ( 03 ) 2 LINES
                    //*  WRITE (03) NOTITLE
                    //*    22T '       Input        '
                    //*    43T '      Bypassed      '
                    //*    64T '      Rejected      '
                    //*    85T '      Accepted      '
                    //*  WRITE (03) NOTITLE
                    //*    01T '                    '
                    //*    22T '===================='
                    //*    43T '===================='
                    //*    64T '===================='
                    //*    85T '===================='
                    //*   106T '===================='
                    //*  SKIP (03) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
    }
}
