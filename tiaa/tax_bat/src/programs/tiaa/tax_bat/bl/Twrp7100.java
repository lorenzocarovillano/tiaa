/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:18 PM
**        * FROM NATURAL PROGRAM : Twrp7100
************************************************************
**        * FILE NAME            : Twrp7100.java
**        * CLASS NAME           : Twrp7100
**        * INSTANCE NAME        : Twrp7100
************************************************************
************************************************************************
**                                                                    **
** PROGRAM:  TWRP7100 - TAXWARRS EXTRACT FOR MDM
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  ROXAN CARREON
** DATE   :  FEB 18, 2016
** PURPOSE:  EXTRACT FILE WILL BE SENT TO MDM AND WILL BE RETURNED TO
**           TAXWARS TO UPDATE THE PARTICIPANT FILE
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp7100 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Count;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Save_Tin;
    private DbsField pnd_Save_Tin_Typ;
    private DbsField pnd_F1;
    private DbsField pnd_F2;
    private DbsField pnd_F3;
    private DbsField pnd_F4;
    private DbsField pnd_F5;
    private DbsField pnd_F6;
    private DbsField pnd_F7;
    private DbsField pnd_F8;
    private DbsField pnd_F9;
    private DbsField pnd_F10;
    private DbsField pnd_F11;
    private DbsField pnd_F12;
    private DbsField pnd_F13;
    private DbsField pnd_F14;
    private DbsField pnd_F15;
    private DbsField pnd_F16;
    private DbsField pnd_F17;
    private DbsField pnd_F18;
    private DbsField pnd_F19;
    private DbsField pnd_F20;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Count = localVariables.newFieldInRecord("pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 11);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Save_Tin = localVariables.newFieldInRecord("pnd_Save_Tin", "#SAVE-TIN", FieldType.STRING, 9);
        pnd_Save_Tin_Typ = localVariables.newFieldInRecord("pnd_Save_Tin_Typ", "#SAVE-TIN-TYP", FieldType.STRING, 1);
        pnd_F1 = localVariables.newFieldInRecord("pnd_F1", "#F1", FieldType.STRING, 12);
        pnd_F2 = localVariables.newFieldInRecord("pnd_F2", "#F2", FieldType.STRING, 30);
        pnd_F3 = localVariables.newFieldInRecord("pnd_F3", "#F3", FieldType.STRING, 30);
        pnd_F4 = localVariables.newFieldInRecord("pnd_F4", "#F4", FieldType.STRING, 30);
        pnd_F5 = localVariables.newFieldInRecord("pnd_F5", "#F5", FieldType.STRING, 8);
        pnd_F6 = localVariables.newFieldInRecord("pnd_F6", "#F6", FieldType.STRING, 8);
        pnd_F7 = localVariables.newFieldInRecord("pnd_F7", "#F7", FieldType.STRING, 35);
        pnd_F8 = localVariables.newFieldInRecord("pnd_F8", "#F8", FieldType.STRING, 35);
        pnd_F9 = localVariables.newFieldInRecord("pnd_F9", "#F9", FieldType.STRING, 35);
        pnd_F10 = localVariables.newFieldInRecord("pnd_F10", "#F10", FieldType.STRING, 35);
        pnd_F11 = localVariables.newFieldInRecord("pnd_F11", "#F11", FieldType.STRING, 35);
        pnd_F12 = localVariables.newFieldInRecord("pnd_F12", "#F12", FieldType.STRING, 35);
        pnd_F13 = localVariables.newFieldInRecord("pnd_F13", "#F13", FieldType.STRING, 35);
        pnd_F14 = localVariables.newFieldInRecord("pnd_F14", "#F14", FieldType.STRING, 32);
        pnd_F15 = localVariables.newFieldInRecord("pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_F16 = localVariables.newFieldInRecord("pnd_F16", "#F16", FieldType.STRING, 2);
        pnd_F17 = localVariables.newFieldInRecord("pnd_F17", "#F17", FieldType.STRING, 1);
        pnd_F18 = localVariables.newFieldInRecord("pnd_F18", "#F18", FieldType.STRING, 8);
        pnd_F19 = localVariables.newFieldInRecord("pnd_F19", "#F19", FieldType.STRING, 70);
        pnd_F20 = localVariables.newFieldInRecord("pnd_F20", "#F20", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9415.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp7100() throws Exception
    {
        super("Twrp7100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp7100|Main");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Tax_Year);                                                                                         //Natural: INPUT #TAX-YEAR
                if (condition(pnd_Tax_Year.equals(getZero())))                                                                                                            //Natural: IF #TAX-YEAR = 0
                {
                    pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                         //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "PROCESSING TAX YEAR : ",pnd_Tax_Year);                                                                                             //Natural: WRITE 'PROCESSING TAX YEAR : ' #TAX-YEAR
                if (Global.isEscape()) return;
                ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                 //Natural: READ PAY BY TWRPYMNT-FORM-SD-2 STARTING FROM #TAX-YEAR
                (
                "READ01",
                new Wc[] { new Wc("TWRPYMNT_FORM_SD_2", ">=", pnd_Tax_Year, WcType.BY) },
                new Oc[] { new Oc("TWRPYMNT_FORM_SD_2", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9415.getVw_pay().readNextRow("READ01")))
                {
                    //*    ACCEPT TWRPYMNT-TAX-CITIZENSHIP EQ 'U' OR = 'F'
                    //*    ACCEPT TWRPYMNT-RESIDENCY-TYPE  =  '3'
                    //*    ACCEPT PAY.TWRPYMNT-CAN-WTHLD-AMT(1)    GT 0.00
                    if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Year().notEquals(pnd_Tax_Year)))                                                                        //Natural: IF TWRPYMNT-TAX-YEAR NE #TAX-YEAR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Save_Tin.equals(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr()) && pnd_Save_Tin_Typ.equals(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Type())))   //Natural: IF #SAVE-TIN = TWRPYMNT-TAX-ID-NBR AND #SAVE-TIN-TYP = TWRPYMNT-TAX-ID-TYPE
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #READ-COUNT
                    pnd_Save_Tin.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr());                                                                                      //Natural: ASSIGN #SAVE-TIN := TWRPYMNT-TAX-ID-NBR
                    pnd_Save_Tin_Typ.setValue(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Type());                                                                                 //Natural: ASSIGN #SAVE-TIN-TYP := TWRPYMNT-TAX-ID-TYPE
                    getWorkFiles().write(1, false, ldaTwrl9415.getPay_Twrpymnt_Tax_Year(), "~", ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Type(), "~", ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),  //Natural: WRITE WORK FILE 1 TWRPYMNT-TAX-YEAR '~' TWRPYMNT-TAX-ID-TYPE '~' TWRPYMNT-TAX-ID-NBR '~' TWRPYMNT-CONTRACT-NBR '~' TWRPYMNT-PAYEE-CDE '~' #F1 '~' #F2 '~' #F3 '~' #F4 '~' #F5 '~' #F6 '~' #F7 '~' #F8 '~' #F9 '~' #F10 '~' #F11 '~' #F12 '~' #F13 '~' #F14 '~' #F15 '~' #F16 '~' #F17 '~' #F18 '~' #F19 '~' #F20 '~'
                        "~", ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(), "~", ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(), "~", pnd_F1, "~", pnd_F2, "~", pnd_F3, 
                        "~", pnd_F4, "~", pnd_F5, "~", pnd_F6, "~", pnd_F7, "~", pnd_F8, "~", pnd_F9, "~", pnd_F10, "~", pnd_F11, "~", pnd_F12, "~", pnd_F13, 
                        "~", pnd_F14, "~", pnd_F15, "~", pnd_F16, "~", pnd_F17, "~", pnd_F18, "~", pnd_F19, "~", pnd_F20, "~");
                    //*    DISPLAY
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OR PARTICIPANTS ",pnd_Read_Count);                                                                                          //Natural: WRITE 'NUMBER OR PARTICIPANTS ' #READ-COUNT
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
