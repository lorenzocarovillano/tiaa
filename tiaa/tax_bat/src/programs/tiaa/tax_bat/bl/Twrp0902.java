/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:43 PM
**        * FROM NATURAL PROGRAM : Twrp0902
************************************************************
**        * FILE NAME            : Twrp0902.java
**        * CLASS NAME           : Twrp0902
**        * INSTANCE NAME        : Twrp0902
************************************************************
********************************************************************
* PROGRAM : TWRP0902
* FUNCTION: TO COUNT THE TOTAL PHYSICAL RECORDS FROM FLAT-FILE6
*           BASED ON CONTRACT NBR AND RESIDENCY CODE
*           AND UPDATE FLAT FILE1 FOR THE TOTAL COUNT.
* INPUTS  : SORTED FLAT-FILE6
* SORT KEY: COMPANY CODE, CONTRACT NBR. & RESIDENCY CODE
* OUTPUT  : CONTROL TOTAL
* AUTHOR  : EDITH
* UPDATE  : STOWED DUE TO UPDATED TWRL0900  6/3/99
*         : TO READ FLAT-FILE6 W/C CONTAINS COMPANY, CONTRACT NBR
*                AND RESIDENCY CODE ONLY  6/9/99
* 10/8/99 : INCLUSION OF NEW COMPANY , TIAA-LIFE
*  (EDITH)
* 04/02/02: ADD NEW COMPANY - TCII
* 09/16/04: TOPS RELEASE 3 CHANGES
*           ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*           CODE 'OP', 'NL' AND 'PL'.
* 10/18/11: SUPPORTING NEW COMPANY CODE "F"; CHANGE CAN BE
*           REFERENCED AS DY1                                  (D.YHUN)
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0902 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Flat_File6;
    private DbsField pnd_Flat_File6_Pnd_Ff6_Company_Code;
    private DbsField pnd_Flat_File6_Pnd_Ff6_Contract_Nbr;
    private DbsField pnd_Flat_File6_Pnd_Ff6_Payee;
    private DbsField pnd_Flat_File6_Pnd_Ff6_Residency_Code;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Tcii_Cnt;
    private DbsField pnd_Fsbc_Cnt;
    private DbsField pnd_Trst_Cnt;
    private DbsField pnd_Othr_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Flat_File6 = localVariables.newGroupInRecord("pnd_Flat_File6", "#FLAT-FILE6");
        pnd_Flat_File6_Pnd_Ff6_Company_Code = pnd_Flat_File6.newFieldInGroup("pnd_Flat_File6_Pnd_Ff6_Company_Code", "#FF6-COMPANY-CODE", FieldType.STRING, 
            1);
        pnd_Flat_File6_Pnd_Ff6_Contract_Nbr = pnd_Flat_File6.newFieldInGroup("pnd_Flat_File6_Pnd_Ff6_Contract_Nbr", "#FF6-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Flat_File6_Pnd_Ff6_Payee = pnd_Flat_File6.newFieldInGroup("pnd_Flat_File6_Pnd_Ff6_Payee", "#FF6-PAYEE", FieldType.STRING, 2);
        pnd_Flat_File6_Pnd_Ff6_Residency_Code = pnd_Flat_File6.newFieldInGroup("pnd_Flat_File6_Pnd_Ff6_Residency_Code", "#FF6-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Life_Cnt = localVariables.newFieldInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tiaa_Cnt = localVariables.newFieldInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tcii_Cnt = localVariables.newFieldInRecord("pnd_Tcii_Cnt", "#TCII-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Fsbc_Cnt = localVariables.newFieldInRecord("pnd_Fsbc_Cnt", "#FSBC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Trst_Cnt = localVariables.newFieldInRecord("pnd_Trst_Cnt", "#TRST-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Othr_Cnt = localVariables.newFieldInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0902() throws Exception
    {
        super("Twrp0902");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                                      //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE6
        while (condition(getWorkFiles().read(2, pnd_Flat_File6)))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*                            ( WRITE PHYSICAL RECORD CNT TO FLAT-FILE1)
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tot_Rec_Cnt().setValue(pnd_Rec_Read);                                                                                       //Natural: ASSIGN #FF1-TOT-REC-CNT := #REC-READ
        getWorkFiles().write(3, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                                  //Natural: WRITE WORK FILE 3 #FLAT-FILE1
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new           //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'REPORTABLE DISTRIBUTIONS' / 'RUNTIME : ' *TIMX 44T 'TAX YEAR ' #TAX-YEAR / 34T 'CONTROL TOTALS - PHYSICAL RECORDS CNT'
            TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new TabSetting(39),"REPORTABLE DISTRIBUTIONS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
            TabSetting(44),"TAX YEAR ",pnd_Tax_Year,NEWLINE,new TabSetting(34),"CONTROL TOTALS - PHYSICAL RECORDS CNT");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL RECORDS READ           : ",pnd_Rec_Read, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  1. CREF RECORDS            : ",pnd_Cref_Cnt,  //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL RECORDS READ           : ' #REC-READ / '  1. CREF RECORDS            : ' #CREF-CNT / '  2. LIFE RECORDS            : ' #LIFE-CNT / '  3. TIAA RECORDS            : ' #TIAA-CNT / '  4. TCII RECORDS            : ' #TCII-CNT / '  5. FSB  RECORDS            : ' #FSBC-CNT / '  6. TRUST RECORDS           : ' #TRST-CNT / '  7. OTHER CO. RECORDS       : ' #OTHR-CNT ///
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  2. LIFE RECORDS            : ",pnd_Life_Cnt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  3. TIAA RECORDS            : ",pnd_Tiaa_Cnt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  4. TCII RECORDS            : ",pnd_Tcii_Cnt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  5. FSB  RECORDS            : ",pnd_Fsbc_Cnt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  6. TRUST RECORDS           : ",pnd_Trst_Cnt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,"  7. OTHER CO. RECORDS       : ",pnd_Othr_Cnt, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*   DY1 FIX BEGINS >>
        //*  '  5. TRUST RECORDS           : ' #TRST-CNT    /
        //*  '  6. OTHER CO. RECORDS       : ' #OTHR-CNT   ///
        //*   DY1 FIX ENDS   <<
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-CNT
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        //*   DY1
        short decideConditionsMet86 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #FF6-COMPANY-CODE;//Natural: VALUE 'C'
        if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("C"))))
        {
            decideConditionsMet86++;
            pnd_Cref_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CREF-CNT
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("L"))))
        {
            decideConditionsMet86++;
            pnd_Life_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #LIFE-CNT
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("T"))))
        {
            decideConditionsMet86++;
            pnd_Tiaa_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TIAA-CNT
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("S"))))
        {
            decideConditionsMet86++;
            pnd_Tcii_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TCII-CNT
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("F"))))
        {
            decideConditionsMet86++;
            pnd_Fsbc_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #FSBC-CNT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Flat_File6_Pnd_Ff6_Company_Code.equals("X"))))
        {
            decideConditionsMet86++;
            pnd_Trst_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TRST-CNT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #OTHR-CNT
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
