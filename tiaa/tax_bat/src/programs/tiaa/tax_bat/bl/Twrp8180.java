/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:59 PM
**        * FROM NATURAL PROGRAM : Twrp8180
************************************************************
**        * FILE NAME            : Twrp8180.java
**        * CLASS NAME           : Twrp8180
**        * INSTANCE NAME        : Twrp8180
************************************************************
************************************************************************
* PROGRAM     :  TWRP8180
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  MARINA NACHBER
* PURPOSE     :  CREATES A NEW YEAR IRC TABLE
*                DATA BASE FILE (003/112) TABLE NUMBER 9
* DATE        :  02/10/2003
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8180 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ircr;
    private DbsField ircr_Rt_A_I_Ind;
    private DbsField ircr_Rt_Table_Id;
    private DbsField ircr_Rt_Short_Key;

    private DbsGroup ircr__R_Field_1;
    private DbsField ircr_Pnd_Rt_Short_Key_Irc_Old;

    private DbsGroup ircr__R_Field_2;
    private DbsField ircr_Pnd_Rt_Short_Key_Tax_Year;
    private DbsField ircr_Pnd_Rt_Short_Key_Irc;
    private DbsField ircr_Rt_Long_Key;
    private DbsField ircr_Rt_Desc5;

    private DbsGroup ircr__R_Field_3;
    private DbsField ircr_Pnd_Rt_Desc5_Desc;
    private DbsField ircr_Pnd_Rt_Desc5_Arr;

    private DataAccessProgramView vw_ircu;
    private DbsField ircu_Rt_A_I_Ind;
    private DbsField ircu_Rt_Table_Id;
    private DbsField ircu_Rt_Short_Key;

    private DbsGroup ircu__R_Field_4;
    private DbsField ircu_Pnd_Rt_Short_Key_Irc_Old;

    private DbsGroup ircu__R_Field_5;
    private DbsField ircu_Pnd_Rt_Short_Key_Tax_Year;
    private DbsField ircu_Pnd_Rt_Short_Key_Irc;
    private DbsField ircu_Rt_Long_Key;
    private DbsField ircu_Rt_Desc5;

    private DbsGroup ircu__R_Field_6;
    private DbsField ircu_Pnd_Rt_Desc5_Desc;
    private DbsField ircu_Pnd_Rt_Desc5_Arr;

    private DataAccessProgramView vw_ircs;
    private DbsField ircs_Rt_A_I_Ind;
    private DbsField ircs_Rt_Table_Id;
    private DbsField ircs_Rt_Short_Key;

    private DbsGroup ircs__R_Field_7;
    private DbsField ircs_Pnd_Rt_Short_Key_Irc_Old;

    private DbsGroup ircs__R_Field_8;
    private DbsField ircs_Pnd_Rt_Short_Key_Tax_Year;
    private DbsField ircs_Pnd_Rt_Short_Key_Irc;
    private DbsField ircs_Rt_Long_Key;
    private DbsField ircs_Rt_Desc5;

    private DbsGroup ircs__R_Field_9;
    private DbsField ircs_Pnd_Rt_Desc5_Desc;
    private DbsField ircs_Pnd_Rt_Desc5_Arr;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_10;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_11;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super_5a;

    private DbsGroup pnd_Super_5a__R_Field_12;
    private DbsField pnd_Super_5a_Pnd_Sa_Tbl_Id;
    private DbsField pnd_Super_5a_Pnd_Sa_A_I_Ind;
    private DbsField pnd_Super_5a_Pnd_Sa_Tax_Year;
    private DbsField pnd_Super_5a_Pnd_Sa_Irc_Cde;
    private DbsField pnd_Super_5b;

    private DbsGroup pnd_Super_5b__R_Field_13;
    private DbsField pnd_Super_5b_Pnd_Sb_Tbl_Id;
    private DbsField pnd_Super_5b_Pnd_Sb_A_I_Ind;
    private DbsField pnd_Super_5b_Pnd_Sb_Tax_Year;
    private DbsField pnd_Super_5b_Pnd_Sb_Irc_Cde;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_ircr = new DataAccessProgramView(new NameInfo("vw_ircr", "IRCR"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ircr_Rt_A_I_Ind = vw_ircr.getRecord().newFieldInGroup("ircr_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ircr_Rt_Table_Id = vw_ircr.getRecord().newFieldInGroup("ircr_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ircr_Rt_Short_Key = vw_ircr.getRecord().newFieldInGroup("ircr_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");

        ircr__R_Field_1 = vw_ircr.getRecord().newGroupInGroup("ircr__R_Field_1", "REDEFINE", ircr_Rt_Short_Key);
        ircr_Pnd_Rt_Short_Key_Irc_Old = ircr__R_Field_1.newFieldInGroup("ircr_Pnd_Rt_Short_Key_Irc_Old", "#RT-SHORT-KEY-IRC-OLD", FieldType.STRING, 2);

        ircr__R_Field_2 = vw_ircr.getRecord().newGroupInGroup("ircr__R_Field_2", "REDEFINE", ircr_Rt_Short_Key);
        ircr_Pnd_Rt_Short_Key_Tax_Year = ircr__R_Field_2.newFieldInGroup("ircr_Pnd_Rt_Short_Key_Tax_Year", "#RT-SHORT-KEY-TAX-YEAR", FieldType.NUMERIC, 
            4);
        ircr_Pnd_Rt_Short_Key_Irc = ircr__R_Field_2.newFieldInGroup("ircr_Pnd_Rt_Short_Key_Irc", "#RT-SHORT-KEY-IRC", FieldType.STRING, 2);
        ircr_Rt_Long_Key = vw_ircr.getRecord().newFieldInGroup("ircr_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ircr_Rt_Desc5 = vw_ircr.getRecord().newFieldInGroup("ircr_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");

        ircr__R_Field_3 = vw_ircr.getRecord().newGroupInGroup("ircr__R_Field_3", "REDEFINE", ircr_Rt_Desc5);
        ircr_Pnd_Rt_Desc5_Desc = ircr__R_Field_3.newFieldInGroup("ircr_Pnd_Rt_Desc5_Desc", "#RT-DESC5-DESC", FieldType.STRING, 25);
        ircr_Pnd_Rt_Desc5_Arr = ircr__R_Field_3.newFieldInGroup("ircr_Pnd_Rt_Desc5_Arr", "#RT-DESC5-ARR", FieldType.STRING, 40);
        registerRecord(vw_ircr);

        vw_ircu = new DataAccessProgramView(new NameInfo("vw_ircu", "IRCU"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ircu_Rt_A_I_Ind = vw_ircu.getRecord().newFieldInGroup("ircu_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ircu_Rt_Table_Id = vw_ircu.getRecord().newFieldInGroup("ircu_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ircu_Rt_Short_Key = vw_ircu.getRecord().newFieldInGroup("ircu_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");

        ircu__R_Field_4 = vw_ircu.getRecord().newGroupInGroup("ircu__R_Field_4", "REDEFINE", ircu_Rt_Short_Key);
        ircu_Pnd_Rt_Short_Key_Irc_Old = ircu__R_Field_4.newFieldInGroup("ircu_Pnd_Rt_Short_Key_Irc_Old", "#RT-SHORT-KEY-IRC-OLD", FieldType.STRING, 2);

        ircu__R_Field_5 = vw_ircu.getRecord().newGroupInGroup("ircu__R_Field_5", "REDEFINE", ircu_Rt_Short_Key);
        ircu_Pnd_Rt_Short_Key_Tax_Year = ircu__R_Field_5.newFieldInGroup("ircu_Pnd_Rt_Short_Key_Tax_Year", "#RT-SHORT-KEY-TAX-YEAR", FieldType.NUMERIC, 
            4);
        ircu_Pnd_Rt_Short_Key_Irc = ircu__R_Field_5.newFieldInGroup("ircu_Pnd_Rt_Short_Key_Irc", "#RT-SHORT-KEY-IRC", FieldType.STRING, 2);
        ircu_Rt_Long_Key = vw_ircu.getRecord().newFieldInGroup("ircu_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ircu_Rt_Desc5 = vw_ircu.getRecord().newFieldInGroup("ircu_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");

        ircu__R_Field_6 = vw_ircu.getRecord().newGroupInGroup("ircu__R_Field_6", "REDEFINE", ircu_Rt_Desc5);
        ircu_Pnd_Rt_Desc5_Desc = ircu__R_Field_6.newFieldInGroup("ircu_Pnd_Rt_Desc5_Desc", "#RT-DESC5-DESC", FieldType.STRING, 25);
        ircu_Pnd_Rt_Desc5_Arr = ircu__R_Field_6.newFieldInGroup("ircu_Pnd_Rt_Desc5_Arr", "#RT-DESC5-ARR", FieldType.STRING, 40);
        registerRecord(vw_ircu);

        vw_ircs = new DataAccessProgramView(new NameInfo("vw_ircs", "IRCS"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ircs_Rt_A_I_Ind = vw_ircs.getRecord().newFieldInGroup("ircs_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ircs_Rt_Table_Id = vw_ircs.getRecord().newFieldInGroup("ircs_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ircs_Rt_Short_Key = vw_ircs.getRecord().newFieldInGroup("ircs_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");

        ircs__R_Field_7 = vw_ircs.getRecord().newGroupInGroup("ircs__R_Field_7", "REDEFINE", ircs_Rt_Short_Key);
        ircs_Pnd_Rt_Short_Key_Irc_Old = ircs__R_Field_7.newFieldInGroup("ircs_Pnd_Rt_Short_Key_Irc_Old", "#RT-SHORT-KEY-IRC-OLD", FieldType.STRING, 2);

        ircs__R_Field_8 = vw_ircs.getRecord().newGroupInGroup("ircs__R_Field_8", "REDEFINE", ircs_Rt_Short_Key);
        ircs_Pnd_Rt_Short_Key_Tax_Year = ircs__R_Field_8.newFieldInGroup("ircs_Pnd_Rt_Short_Key_Tax_Year", "#RT-SHORT-KEY-TAX-YEAR", FieldType.NUMERIC, 
            4);
        ircs_Pnd_Rt_Short_Key_Irc = ircs__R_Field_8.newFieldInGroup("ircs_Pnd_Rt_Short_Key_Irc", "#RT-SHORT-KEY-IRC", FieldType.STRING, 2);
        ircs_Rt_Long_Key = vw_ircs.getRecord().newFieldInGroup("ircs_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ircs_Rt_Desc5 = vw_ircs.getRecord().newFieldInGroup("ircs_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");

        ircs__R_Field_9 = vw_ircs.getRecord().newGroupInGroup("ircs__R_Field_9", "REDEFINE", ircs_Rt_Desc5);
        ircs_Pnd_Rt_Desc5_Desc = ircs__R_Field_9.newFieldInGroup("ircs_Pnd_Rt_Desc5_Desc", "#RT-DESC5-DESC", FieldType.STRING, 25);
        ircs_Pnd_Rt_Desc5_Arr = ircs__R_Field_9.newFieldInGroup("ircs_Pnd_Rt_Desc5_Arr", "#RT-DESC5-ARR", FieldType.STRING, 40);
        registerRecord(vw_ircs);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_10 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_10", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_10.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_11 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_11", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_11.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_5a = localVariables.newFieldInRecord("pnd_Super_5a", "#SUPER-5A", FieldType.STRING, 226);

        pnd_Super_5a__R_Field_12 = localVariables.newGroupInRecord("pnd_Super_5a__R_Field_12", "REDEFINE", pnd_Super_5a);
        pnd_Super_5a_Pnd_Sa_Tbl_Id = pnd_Super_5a__R_Field_12.newFieldInGroup("pnd_Super_5a_Pnd_Sa_Tbl_Id", "#SA-TBL-ID", FieldType.STRING, 5);
        pnd_Super_5a_Pnd_Sa_A_I_Ind = pnd_Super_5a__R_Field_12.newFieldInGroup("pnd_Super_5a_Pnd_Sa_A_I_Ind", "#SA-A-I-IND", FieldType.STRING, 1);
        pnd_Super_5a_Pnd_Sa_Tax_Year = pnd_Super_5a__R_Field_12.newFieldInGroup("pnd_Super_5a_Pnd_Sa_Tax_Year", "#SA-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super_5a_Pnd_Sa_Irc_Cde = pnd_Super_5a__R_Field_12.newFieldInGroup("pnd_Super_5a_Pnd_Sa_Irc_Cde", "#SA-IRC-CDE", FieldType.STRING, 2);
        pnd_Super_5b = localVariables.newFieldInRecord("pnd_Super_5b", "#SUPER-5B", FieldType.STRING, 226);

        pnd_Super_5b__R_Field_13 = localVariables.newGroupInRecord("pnd_Super_5b__R_Field_13", "REDEFINE", pnd_Super_5b);
        pnd_Super_5b_Pnd_Sb_Tbl_Id = pnd_Super_5b__R_Field_13.newFieldInGroup("pnd_Super_5b_Pnd_Sb_Tbl_Id", "#SB-TBL-ID", FieldType.STRING, 5);
        pnd_Super_5b_Pnd_Sb_A_I_Ind = pnd_Super_5b__R_Field_13.newFieldInGroup("pnd_Super_5b_Pnd_Sb_A_I_Ind", "#SB-A-I-IND", FieldType.STRING, 1);
        pnd_Super_5b_Pnd_Sb_Tax_Year = pnd_Super_5b__R_Field_13.newFieldInGroup("pnd_Super_5b_Pnd_Sb_Tax_Year", "#SB-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super_5b_Pnd_Sb_Irc_Cde = pnd_Super_5b__R_Field_13.newFieldInGroup("pnd_Super_5b_Pnd_Sb_Irc_Cde", "#SB-IRC-CDE", FieldType.STRING, 2);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ircr.reset();
        vw_ircu.reset();
        vw_ircs.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8180() throws Exception
    {
        super("Twrp8180");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8180|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super_5a.reset();                                                                                                                                     //Natural: RESET #SUPER-5A
                pnd_Super_5a_Pnd_Sa_Tbl_Id.setValue("TX009");                                                                                                             //Natural: ASSIGN #SA-TBL-ID := 'TX009'
                pnd_Super_5a_Pnd_Sa_A_I_Ind.setValue("A");                                                                                                                //Natural: ASSIGN #SA-A-I-IND := 'A'
                pnd_Super_5a_Pnd_Sa_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                               //Natural: ASSIGN #SA-TAX-YEAR := #OLD-TAX-YEAR-N
                vw_ircr.startDatabaseRead                                                                                                                                 //Natural: READ IRCR WITH RT-SUPER5 = #SUPER-5A
                (
                "R1",
                new Wc[] { new Wc("RT_SUPER5", ">=", pnd_Super_5a, WcType.BY) },
                new Oc[] { new Oc("RT_SUPER5", "ASC") }
                );
                R1:
                while (condition(vw_ircr.readNextRow("R1")))
                {
                    if (condition(ircr_Rt_A_I_Ind.equals(pnd_Super_5a_Pnd_Sa_A_I_Ind) && ircr_Rt_Table_Id.equals(pnd_Super_5a_Pnd_Sa_Tbl_Id) && ircr_Pnd_Rt_Short_Key_Tax_Year.equals(pnd_Super_5a_Pnd_Sa_Tax_Year))) //Natural: IF IRCR.RT-A-I-IND = #SA-A-I-IND AND IRCR.RT-TABLE-ID = #SA-TBL-ID AND IRCR.#RT-SHORT-KEY-TAX-YEAR = #SA-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super_5b_Pnd_Sb_Tbl_Id.setValue("TX009");                                                                                                         //Natural: ASSIGN #SB-TBL-ID := 'TX009'
                    pnd_Super_5b_Pnd_Sb_A_I_Ind.setValue("A");                                                                                                            //Natural: ASSIGN #SB-A-I-IND := 'A'
                    pnd_Super_5b_Pnd_Sb_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                           //Natural: ASSIGN #SB-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super_5b_Pnd_Sb_Irc_Cde.setValue(ircr_Pnd_Rt_Short_Key_Irc);                                                                                      //Natural: ASSIGN #SB-IRC-CDE := IRCR.#RT-SHORT-KEY-IRC
                    vw_ircu.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IRCU WITH RT-SUPER5 = #SUPER-5B
                    (
                    "R2",
                    new Wc[] { new Wc("RT_SUPER5", ">=", pnd_Super_5b, WcType.BY) },
                    new Oc[] { new Oc("RT_SUPER5", "ASC") },
                    1
                    );
                    R2:
                    while (condition(vw_ircu.readNextRow("R2")))
                    {
                        if (condition(ircu_Rt_A_I_Ind.equals(pnd_Super_5b_Pnd_Sb_A_I_Ind) && ircu_Rt_Table_Id.equals(pnd_Super_5b_Pnd_Sb_Tbl_Id) && ircu_Pnd_Rt_Short_Key_Tax_Year.equals(pnd_Super_5b_Pnd_Sb_Tax_Year)  //Natural: IF IRCU.RT-A-I-IND = #SB-A-I-IND AND IRCU.RT-TABLE-ID = #SB-TBL-ID AND IRCU.#RT-SHORT-KEY-TAX-YEAR = #SB-TAX-YEAR AND IRCU.#RT-SHORT-KEY-IRC = #SB-IRC-CDE
                            && ircu_Pnd_Rt_Short_Key_Irc.equals(pnd_Super_5b_Pnd_Sb_Irc_Cde)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(ircu_Rt_Desc5.equals(ircr_Rt_Desc5)))                                                                                               //Natural: IF IRCU.RT-DESC5 = IRCR.RT-DESC5
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET IRCU *ISN ( R2. )
                            vw_ircu.readByID(vw_ircu.getAstISN("R2"), "G1");
                            ircu_Rt_Desc5.setValue(ircr_Rt_Desc5);                                                                                                        //Natural: ASSIGN IRCU.RT-DESC5 := IRCR.RT-DESC5
                            vw_ircu.updateDBRow("G1");                                                                                                                    //Natural: UPDATE ( G1. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Updt_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (R2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        ircs_Rt_Table_Id.setValue(ircr_Rt_Table_Id);                                                                                                      //Natural: ASSIGN IRCS.RT-TABLE-ID := IRCR.RT-TABLE-ID
                        ircs_Rt_A_I_Ind.setValue("A");                                                                                                                    //Natural: ASSIGN IRCS.RT-A-I-IND := 'A'
                        ircs_Pnd_Rt_Short_Key_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                     //Natural: ASSIGN IRCS.#RT-SHORT-KEY-TAX-YEAR := #NEW-TAX-YEAR-N
                        ircs_Pnd_Rt_Short_Key_Irc.setValue(ircr_Pnd_Rt_Short_Key_Irc);                                                                                    //Natural: ASSIGN IRCS.#RT-SHORT-KEY-IRC := IRCR.#RT-SHORT-KEY-IRC
                        ircs_Rt_Desc5.setValue(ircr_Rt_Desc5);                                                                                                            //Natural: ASSIGN IRCS.RT-DESC5 := IRCR.RT-DESC5
                        vw_ircs.insertDBRow();                                                                                                                            //Natural: STORE IRCS
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (R1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super_5a.reset();                                                                                                                                     //Natural: RESET #SUPER-5A
                pnd_Super_5a_Pnd_Sa_Tbl_Id.setValue("TX009");                                                                                                             //Natural: ASSIGN #SA-TBL-ID := 'TX009'
                vw_ircr.startDatabaseRead                                                                                                                                 //Natural: READ IRCR WITH RT-SUPER5 = #SUPER-5A
                (
                "R3",
                new Wc[] { new Wc("RT_SUPER5", ">=", pnd_Super_5a, WcType.BY) },
                new Oc[] { new Oc("RT_SUPER5", "ASC") }
                );
                R3:
                while (condition(vw_ircr.readNextRow("R3")))
                {
                    if (condition(ircr_Rt_Table_Id.equals("TX009")))                                                                                                      //Natural: IF IRCR.RT-TABLE-ID = 'TX009'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),ircr_Rt_Table_Id,new TabSetting(8),ircr_Rt_A_I_Ind,new                //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T IRCR.RT-TABLE-ID 08T IRCR.RT-A-I-IND 11T IRCR.#RT-SHORT-KEY-TAX-YEAR 18T IRCR.#RT-SHORT-KEY-IRC 22T IRCR.#RT-DESC5-DESC 48T IRCR.#RT-DESC5-ARR *ISN ( R3. )
                        TabSetting(11),ircr_Pnd_Rt_Short_Key_Tax_Year,new TabSetting(18),ircr_Pnd_Rt_Short_Key_Irc,new TabSetting(22),ircr_Pnd_Rt_Desc5_Desc,new 
                        TabSetting(48),ircr_Pnd_Rt_Desc5_Arr,vw_ircr.getAstISN("R3"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (R3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(50),"IRC Rollover Matrix Table Report",new    //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 50T 'IRC Rollover Matrix Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Tbl",new TabSetting(7),"Act",new TabSetting(11),"Tax ",new TabSetting(17),"IRC",new     //Natural: WRITE ( 01 ) NOTITLE 01T 'Tbl' 07T 'Act' 11T 'Tax ' 17T 'IRC' 22T 'IRC' 48T 'Distributing'
                        TabSetting(22),"IRC",new TabSetting(48),"Distributing");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Num",new TabSetting(7),"Ind",new TabSetting(11),"Year",new TabSetting(17),"Code",new    //Natural: WRITE ( 01 ) NOTITLE 01T 'Num' 07T 'Ind' 11T 'Year' 17T 'Code' 22T 'Description ' 48T 'IRC Codes '
                        TabSetting(22),"Description ",new TabSetting(48),"IRC Codes ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(7),"===",new TabSetting(11),"====",new TabSetting(17),"====",new    //Natural: WRITE ( 01 ) NOTITLE 01T '===' 07T '===' 11T '====' 17T '====' 22T '===========' 48T '============'
                        TabSetting(22),"===========",new TabSetting(48),"============");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
