/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:49 PM
**        * FROM NATURAL PROGRAM : Twrp1071
************************************************************
**        * FILE NAME            : Twrp1071.java
**        * CLASS NAME           : Twrp1071
**        * INSTANCE NAME        : Twrp1071
************************************************************
************************************************************************
* PROGRAM  : TWRP1071 CLONE OF TWRP4720
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : E-DELIVERY FORMS EXTRACT CONTROL TOTALS REPORTS.
* CREATED  : 04/11/2013
*   BY     : ROXAN CARREON
* FUNCTION : PROGRAM READS 5498 FORMS RECORDS, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
* DASDH    10/19/2017 - 5498 BOX CHANGES
* ARIVU    10/05/2018 - ARIVU - EIN CHANGES , TAG:EINCHG
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1071 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Form;
    private DbsField pnd_Form_Pnd_F_Ira_Type;
    private DbsField pnd_Form_Pnd_F_Tax_Year;
    private DbsField pnd_Form_Pnd_F_Form_Type;
    private DbsField pnd_Form_Pnd_F_Company_Cde;
    private DbsField pnd_Form_Pnd_F_Tin;
    private DbsField pnd_Form_Pnd_F_Contract_Nbr;
    private DbsField pnd_Form_Pnd_F_Payee_Cde;
    private DbsField pnd_Form_Pnd_F_Srce_Cde;
    private DbsField pnd_Form_Pnd_F_Ira_Acct_Type;
    private DbsField pnd_Form_Pnd_F_Contract_Xref;
    private DbsField pnd_Form_Pnd_F_Trad_Ira_Contrib;
    private DbsField pnd_Form_Pnd_F_Roth_Ira_Contrib;
    private DbsField pnd_Form_Pnd_F_Trad_Ira_Rollover;
    private DbsField pnd_Form_Pnd_F_Roth_Ira_Conversion;
    private DbsField pnd_Form_Pnd_F_Ira_Rechar;
    private DbsField pnd_Form_Pnd_F_Account_Fmv;
    private DbsField pnd_Form_Pnd_F_Sep_Amt;
    private DbsField pnd_Form_Pnd_F_Postpn_Amt;
    private DbsField pnd_P2;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Comp_Err;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Form_Type;
    private DbsField pnd_Header_Date;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Roth_Cnv_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Postpn_Amt;
    private DbsField pnd_G_Trans_Count;
    private DbsField pnd_G_Classic_Amt;
    private DbsField pnd_G_Rollover_Amt;
    private DbsField pnd_G_Roth_Cnv_Amt;
    private DbsField pnd_G_Rechar_Amt;
    private DbsField pnd_G_Fmv_Amt;
    private DbsField pnd_G_Sep_Amt;
    private DbsField pnd_G_Roth_Amt;
    private DbsField pnd_G_Postpn_Amt;
    private DbsField pnd_Header_Col_2;
    private DbsField pnd_Header_Col_3;
    private DbsField pnd_Header_Underline;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Form = localVariables.newGroupInRecord("pnd_Form", "#FORM");
        pnd_Form_Pnd_F_Ira_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Type", "#F-IRA-TYPE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Tax_Year = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tax_Year", "#F-TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Form_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Form_Type", "#F-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Form_Pnd_F_Company_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Company_Cde", "#F-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Tin = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tin", "#F-TIN", FieldType.STRING, 10);
        pnd_Form_Pnd_F_Contract_Nbr = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Contract_Nbr", "#F-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Payee_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Payee_Cde", "#F-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Srce_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Srce_Cde", "#F-SRCE-CDE", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Ira_Acct_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Acct_Type", "#F-IRA-ACCT-TYPE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Contract_Xref = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Contract_Xref", "#F-CONTRACT-XREF", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Trad_Ira_Contrib = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Trad_Ira_Contrib", "#F-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Form_Pnd_F_Roth_Ira_Contrib = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Roth_Ira_Contrib", "#F-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Form_Pnd_F_Trad_Ira_Rollover = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Trad_Ira_Rollover", "#F-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Form_Pnd_F_Roth_Ira_Conversion = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Roth_Ira_Conversion", "#F-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Form_Pnd_F_Ira_Rechar = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Rechar", "#F-IRA-RECHAR", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Form_Pnd_F_Account_Fmv = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Account_Fmv", "#F-ACCOUNT-FMV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Form_Pnd_F_Sep_Amt = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Sep_Amt", "#F-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Form_Pnd_F_Postpn_Amt = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Postpn_Amt", "#F-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_P2 = localVariables.newFieldInRecord("pnd_P2", "#P2", FieldType.BOOLEAN, 1);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Comp_Err = localVariables.newFieldInRecord("pnd_Comp_Err", "#COMP-ERR", FieldType.PACKED_DECIMAL, 7);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Form_Type = localVariables.newFieldInRecord("pnd_Prev_Form_Type", "#PREV-FORM-TYPE", FieldType.STRING, 2);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            3));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Roth_Cnv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Cnv_Amt", "#T-ROTH-CNV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_T_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_T_Postpn_Amt", "#T-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_G_Trans_Count = localVariables.newFieldArrayInRecord("pnd_G_Trans_Count", "#G-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            3));
        pnd_G_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_G_Classic_Amt", "#G-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_G_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_G_Rollover_Amt", "#G-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_G_Roth_Cnv_Amt = localVariables.newFieldArrayInRecord("pnd_G_Roth_Cnv_Amt", "#G-ROTH-CNV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_G_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_G_Rechar_Amt", "#G-RECHAR-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_G_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_G_Fmv_Amt", "#G-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            3));
        pnd_G_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_G_Sep_Amt", "#G-SEP-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_G_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_G_Roth_Amt", "#G-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_G_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_G_Postpn_Amt", "#G-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            3));
        pnd_Header_Col_2 = localVariables.newFieldInRecord("pnd_Header_Col_2", "#HEADER-COL-2", FieldType.STRING, 19);
        pnd_Header_Col_3 = localVariables.newFieldInRecord("pnd_Header_Col_3", "#HEADER-COL-3", FieldType.STRING, 19);
        pnd_Header_Underline = localVariables.newFieldInRecord("pnd_Header_Underline", "#HEADER-UNDERLINE", FieldType.STRING, 19);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_P2.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1071() throws Exception
    {
        super("Twrp1071");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP1071", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* **
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - -
        pnd_Header_Date.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN #HEADER-DATE := *DATX
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #FORM
        while (condition(getWorkFiles().read(1, pnd_Form)))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            if (condition(! (pnd_P2.getBoolean()) && pnd_Form_Pnd_F_Srce_Cde.equals("IR")))                                                                               //Natural: IF NOT #P2 AND #F-SRCE-CDE = 'IR'
            {
                pnd_P2.setValue(true);                                                                                                                                    //Natural: ASSIGN #P2 := TRUE
                pnd_Header_Col_2.setValue("     Loaded P2     ");                                                                                                         //Natural: ASSIGN #HEADER-COL-2 := '     Loaded P2     '
                pnd_Header_Col_3.setValue(" On Form w/P2 Load ");                                                                                                         //Natural: ASSIGN #HEADER-COL-3 := ' On Form w/P2 Load '
                pnd_Header_Underline.setValue("===================");                                                                                                     //Natural: ASSIGN #HEADER-UNDERLINE := '==================='
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                        //Natural: IF #READ-CTR = 1
            {
                pnd_Prev_Form_Type.setValue(pnd_Form_Pnd_F_Ira_Type);                                                                                                     //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Form_Type.equals(pnd_Form_Pnd_F_Ira_Type)))                                                                                            //Natural: IF #PREV-FORM-TYPE = #F-IRA-TYPE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
                sub_Display_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM UPDATE-FINAL-TOTALS
                sub_Update_Final_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
                sub_Reset_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Form_Type.setValue(pnd_Form_Pnd_F_Ira_Type);                                                                                                     //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
            }                                                                                                                                                             //Natural: END-IF
            //*  EINCHG
            if (condition(pnd_Form_Pnd_F_Company_Cde.equals("T") || pnd_Form_Pnd_F_Company_Cde.equals("A")))                                                              //Natural: IF #F-COMPANY-CDE = 'T' OR #F-COMPANY-CDE = 'A'
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                sub_Update_T_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Comp_Err.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #COMP-ERR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* **
        if (condition(pnd_Read_Ctr.equals(getZero()) || pnd_Comp_Err.greater(getZero())))                                                                                 //Natural: IF #READ-CTR = 0 OR #COMP-ERR GT 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, NEWLINE,"***",new ColumnSpacing(2),"FORM (5498) Extract File (Work File 01) is Empty  OR ",NEWLINE,"***",new ColumnSpacing(2),"This File Contains Company Code Other Than 'T'",pnd_Comp_Err,  //Natural: WRITE ( 00 ) / '***' 2X 'FORM (5498) Extract File (Work File 01) is Empty  OR ' / '***' 2X 'This File Contains Company Code Other Than "T"' #COMP-ERR / '***' 2X 'PROGRAM...:' *PROGRAM
                new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"***",new ColumnSpacing(2),"PROGRAM...:",Global.getPROGRAM());
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-FINAL-TOTALS
        sub_Update_Final_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pnd_P2.getBoolean())))                                                                                                                           //Natural: IF NOT #P2
        {
                                                                                                                                                                          //Natural: PERFORM P1-END-OF-PROGRAM-PROCESSING
            sub_P1_End_Of_Program_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM P2-END-OF-PROGRAM-PROCESSING
            sub_P2_End_Of_Program_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************************
        //* *****************************************
        //* ********************************
        //* ********************************
        //* **************************************
        //* **************************************
        //* ***************************************
        //* ***********************************************
        //* ***********************************************
        //* ***********************************************
        //* ***********************************************
        //* ******************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //* ***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //* **************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* **                                                                                                                                                            //Natural: ON ERROR
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        pnd_T_Trans_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( 1 )
        pnd_T_Classic_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Trad_Ira_Contrib);                                                                                              //Natural: ADD #F-TRAD-IRA-CONTRIB TO #T-CLASSIC-AMT ( 1 )
        pnd_T_Roth_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Roth_Ira_Contrib);                                                                                                 //Natural: ADD #F-ROTH-IRA-CONTRIB TO #T-ROTH-AMT ( 1 )
        pnd_T_Rollover_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Trad_Ira_Rollover);                                                                                            //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( 1 )
        pnd_T_Rechar_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Ira_Rechar);                                                                                                     //Natural: ADD #F-IRA-RECHAR TO #T-RECHAR-AMT ( 1 )
        pnd_T_Fmv_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Account_Fmv);                                                                                                       //Natural: ADD #F-ACCOUNT-FMV TO #T-FMV-AMT ( 1 )
        pnd_T_Sep_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Sep_Amt);                                                                                                           //Natural: ADD #F-SEP-AMT TO #T-SEP-AMT ( 1 )
        pnd_T_Roth_Cnv_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Roth_Ira_Conversion);                                                                                          //Natural: ADD #F-ROTH-IRA-CONVERSION TO #T-ROTH-CNV-AMT ( 1 )
        //*  DASDH
        pnd_T_Postpn_Amt.getValue(1).nadd(pnd_Form_Pnd_F_Postpn_Amt);                                                                                                     //Natural: ADD #F-POSTPN-AMT TO #T-POSTPN-AMT ( 1 )
        if (condition(pnd_Form_Pnd_F_Srce_Cde.equals("IR")))                                                                                                              //Natural: IF #F-SRCE-CDE = 'IR'
        {
            pnd_I.setValue(2);                                                                                                                                            //Natural: ASSIGN #I := 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_I.setValue(3);                                                                                                                                            //Natural: ASSIGN #I := 3
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T_Trans_Count.getValue(pnd_I).nadd(1);                                                                                                                        //Natural: ADD 1 TO #T-TRANS-COUNT ( #I )
        pnd_T_Classic_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Trad_Ira_Contrib);                                                                                          //Natural: ADD #F-TRAD-IRA-CONTRIB TO #T-CLASSIC-AMT ( #I )
        pnd_T_Roth_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Roth_Ira_Contrib);                                                                                             //Natural: ADD #F-ROTH-IRA-CONTRIB TO #T-ROTH-AMT ( #I )
        pnd_T_Rollover_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Trad_Ira_Rollover);                                                                                        //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( #I )
        pnd_T_Rechar_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Ira_Rechar);                                                                                                 //Natural: ADD #F-IRA-RECHAR TO #T-RECHAR-AMT ( #I )
        pnd_T_Fmv_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Account_Fmv);                                                                                                   //Natural: ADD #F-ACCOUNT-FMV TO #T-FMV-AMT ( #I )
        pnd_T_Sep_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Sep_Amt);                                                                                                       //Natural: ADD #F-SEP-AMT TO #T-SEP-AMT ( #I )
        pnd_T_Roth_Cnv_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Roth_Ira_Conversion);                                                                                      //Natural: ADD #F-ROTH-IRA-CONVERSION TO #T-ROTH-CNV-AMT ( #I )
        //*  DASDH
        pnd_T_Postpn_Amt.getValue(pnd_I).nadd(pnd_Form_Pnd_F_Postpn_Amt);                                                                                                 //Natural: ADD #F-POSTPN-AMT TO #T-POSTPN-AMT ( #I )
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        short decideConditionsMet213 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PREV-FORM-TYPE;//Natural: VALUE '10'
        if (condition((pnd_Prev_Form_Type.equals("10"))))
        {
            decideConditionsMet213++;
            pnd_Source_Total_Header.setValue("     CLASSIC     ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '     CLASSIC     '
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pnd_Prev_Form_Type.equals("20"))))
        {
            decideConditionsMet213++;
            pnd_Source_Total_Header.setValue("      ROTH       ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      ROTH       '
        }                                                                                                                                                                 //Natural: VALUE '30'
        else if (condition((pnd_Prev_Form_Type.equals("30"))))
        {
            decideConditionsMet213++;
            pnd_Source_Total_Header.setValue("    ROLLOVER     ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '    ROLLOVER     '
        }                                                                                                                                                                 //Natural: VALUE '50'
        else if (condition((pnd_Prev_Form_Type.equals("50"))))
        {
            decideConditionsMet213++;
            pnd_Source_Total_Header.setValue("    SEP IRA      ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '    SEP IRA      '
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_P2.getBoolean())))                                                                                                                           //Natural: IF NOT #P2
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-PERIOD-1
            sub_Write_Period_1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-PERIOD-2
            sub_Write_Period_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_Period_1() throws Exception                                                                                                                    //Natural: WRITE-PERIOD-1
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE,NEWLINE,"Transaction Count...",new TabSetting(33),pnd_T_Trans_Count.getValue(1),  //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' / / 'Transaction Count...' 33T #T-TRANS-COUNT ( 1 ) / 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( 1 ) / 'Roth Contrib........' 26T #T-ROTH-AMT ( 1 ) / 'Rollover............' 26T #T-ROLLOVER-AMT ( 1 ) / 'Roth Conversion.....' 26T #T-ROTH-CNV-AMT ( 1 ) / 'Recharacter.........' 26T #T-RECHAR-AMT ( 1 ) / 'SEP Contrib.........' 26T #T-SEP-AMT ( 1 ) / 'Fair Market.........' 25T #T-FMV-AMT ( 1 ) / 'Late R/O Amt........' 25T #T-POSTPN-AMT ( 1 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Contrib........",new 
            TabSetting(26),pnd_T_Roth_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Cnv_Amt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Contrib.........",new 
            TabSetting(26),pnd_T_Sep_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt........",new TabSetting(25),pnd_T_Postpn_Amt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Period_2() throws Exception                                                                                                                    //Natural: WRITE-PERIOD-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE,NEWLINE,"Transaction Count...",new TabSetting(33),pnd_T_Trans_Count.getValue(1),  //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' / / 'Transaction Count...' 33T #T-TRANS-COUNT ( 1 ) 53T #T-TRANS-COUNT ( 2 ) 73T #T-TRANS-COUNT ( 3 ) / 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( 1 ) 46T #T-CLASSIC-AMT ( 2 ) 66T #T-CLASSIC-AMT ( 3 ) / 'Roth Contrib........' 26T #T-ROTH-AMT ( 1 ) 46T #T-ROTH-AMT ( 2 ) 66T #T-ROTH-AMT ( 3 ) / 'Rollover............' 26T #T-ROLLOVER-AMT ( 1 ) 46T #T-ROLLOVER-AMT ( 2 ) 66T #T-ROLLOVER-AMT ( 3 ) / 'Roth Conversion.....' 26T #T-ROTH-CNV-AMT ( 1 ) 46T #T-ROTH-CNV-AMT ( 2 ) 66T #T-ROTH-CNV-AMT ( 3 ) / 'Recharacter.........' 26T #T-RECHAR-AMT ( 1 ) 46T #T-RECHAR-AMT ( 2 ) 66T #T-RECHAR-AMT ( 3 ) / 'SEP Contrib.........' 26T #T-SEP-AMT ( 1 ) 46T #T-SEP-AMT ( 2 ) 66T #T-SEP-AMT ( 3 ) / 'Fair Market.........' 25T #T-FMV-AMT ( 1 ) 45T #T-FMV-AMT ( 2 ) 65T #T-FMV-AMT ( 3 ) / 'Late R/O Amt........' 25T #T-POSTPN-AMT ( 1 ) 45T #T-POSTPN-AMT ( 2 ) 65T #T-POSTPN-AMT ( 3 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(53),pnd_T_Trans_Count.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(73),pnd_T_Trans_Count.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Classic_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Classic_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Roth_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Roth_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Rollover_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Rollover_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Cnv_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Roth_Cnv_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Roth_Cnv_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Rechar_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Rechar_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Contrib.........",new TabSetting(26),pnd_T_Sep_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_T_Sep_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_T_Sep_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Fair Market.........",new TabSetting(25),pnd_T_Fmv_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(45),pnd_T_Fmv_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(65),pnd_T_Fmv_Amt.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt........",new TabSetting(25),pnd_T_Postpn_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(45),pnd_T_Postpn_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(65),pnd_T_Postpn_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  DASDH
    }
    private void sub_Update_Final_Totals() throws Exception                                                                                                               //Natural: UPDATE-FINAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #J 1 3
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
        {
            pnd_G_Trans_Count.getValue(pnd_J).nadd(pnd_T_Trans_Count.getValue(pnd_J));                                                                                    //Natural: ADD #T-TRANS-COUNT ( #J ) TO #G-TRANS-COUNT ( #J )
            pnd_G_Classic_Amt.getValue(pnd_J).nadd(pnd_T_Classic_Amt.getValue(pnd_J));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( #J ) TO #G-CLASSIC-AMT ( #J )
            pnd_G_Roth_Amt.getValue(pnd_J).nadd(pnd_T_Roth_Amt.getValue(pnd_J));                                                                                          //Natural: ADD #T-ROTH-AMT ( #J ) TO #G-ROTH-AMT ( #J )
            pnd_G_Rollover_Amt.getValue(pnd_J).nadd(pnd_T_Rollover_Amt.getValue(pnd_J));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( #J ) TO #G-ROLLOVER-AMT ( #J )
            pnd_G_Rechar_Amt.getValue(pnd_J).nadd(pnd_T_Rechar_Amt.getValue(pnd_J));                                                                                      //Natural: ADD #T-RECHAR-AMT ( #J ) TO #G-RECHAR-AMT ( #J )
            pnd_G_Sep_Amt.getValue(pnd_J).nadd(pnd_T_Sep_Amt.getValue(pnd_J));                                                                                            //Natural: ADD #T-SEP-AMT ( #J ) TO #G-SEP-AMT ( #J )
            pnd_G_Fmv_Amt.getValue(pnd_J).nadd(pnd_T_Fmv_Amt.getValue(pnd_J));                                                                                            //Natural: ADD #T-FMV-AMT ( #J ) TO #G-FMV-AMT ( #J )
            pnd_G_Roth_Cnv_Amt.getValue(pnd_J).nadd(pnd_T_Roth_Cnv_Amt.getValue(pnd_J));                                                                                  //Natural: ADD #T-ROTH-CNV-AMT ( #J ) TO #G-ROTH-CNV-AMT ( #J )
            //*  DASDH
            pnd_G_Postpn_Amt.getValue(pnd_J).nadd(pnd_T_Postpn_Amt.getValue(pnd_J));                                                                                      //Natural: ADD #T-POSTPN-AMT ( #J ) TO #G-POSTPN-AMT ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  DASDH
        pnd_T_Trans_Count.getValue("*").reset();                                                                                                                          //Natural: RESET #T-TRANS-COUNT ( * ) #T-CLASSIC-AMT ( * ) #T-ROTH-AMT ( * ) #T-ROLLOVER-AMT ( * ) #T-RECHAR-AMT ( * ) #T-SEP-AMT ( * ) #T-FMV-AMT ( * ) #T-ROTH-CNV-AMT ( * ) #T-POSTPN-AMT ( * )
        pnd_T_Classic_Amt.getValue("*").reset();
        pnd_T_Roth_Amt.getValue("*").reset();
        pnd_T_Rollover_Amt.getValue("*").reset();
        pnd_T_Rechar_Amt.getValue("*").reset();
        pnd_T_Sep_Amt.getValue("*").reset();
        pnd_T_Fmv_Amt.getValue("*").reset();
        pnd_T_Roth_Cnv_Amt.getValue("*").reset();
        pnd_T_Postpn_Amt.getValue("*").reset();
    }
    private void sub_P1_End_Of_Program_Processing() throws Exception                                                                                                      //Natural: P1-END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE,NEWLINE,"Transaction Count...",new TabSetting(33),pnd_G_Trans_Count.getValue(1),  //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' / / 'Transaction Count...' 33T #G-TRANS-COUNT ( 1 ) / 'Classic Contrib.....' 26T #G-CLASSIC-AMT ( 1 ) / 'Roth Contrib........' 26T #G-ROTH-AMT ( 1 ) / 'Rollover............' 26T #G-ROLLOVER-AMT ( 1 ) / 'Roth Conversion.....' 26T #G-ROTH-CNV-AMT ( 1 ) / 'Recharacter.........' 26T #G-RECHAR-AMT ( 1 ) / 'SEP Contrib.........' 26T #G-SEP-AMT ( 1 ) / 'Fair Market.........' 25T #G-FMV-AMT ( 1 ) / 'Late R/O Amt........' 25T #G-POSTPN-AMT ( 1 ) // 'Records Read...............................' #READ-CTR
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Classic Contrib.....",new TabSetting(26),pnd_G_Classic_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Contrib........",new 
            TabSetting(26),pnd_G_Roth_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Rollover............",new TabSetting(26),pnd_G_Rollover_Amt.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Conversion.....",new TabSetting(26),pnd_G_Roth_Cnv_Amt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Recharacter.........",new TabSetting(26),pnd_G_Rechar_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Contrib.........",new 
            TabSetting(26),pnd_G_Sep_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Fair Market.........",new TabSetting(25),pnd_G_Fmv_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt........",new TabSetting(25),pnd_G_Postpn_Amt.getValue(1), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'Records Read...............................' #READ-CTR
        if (Global.isEscape()) return;
    }
    private void sub_P2_End_Of_Program_Processing() throws Exception                                                                                                      //Natural: P2-END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE,NEWLINE,"Transaction Count...",new TabSetting(33),pnd_G_Trans_Count.getValue(1),  //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' / / 'Transaction Count...' 33T #G-TRANS-COUNT ( 1 ) 53T #G-TRANS-COUNT ( 2 ) 73T #G-TRANS-COUNT ( 3 ) / 'Classic Contrib.....' 26T #G-CLASSIC-AMT ( 1 ) 46T #G-CLASSIC-AMT ( 2 ) 66T #G-CLASSIC-AMT ( 3 ) / 'Roth Contrib........' 26T #G-ROTH-AMT ( 1 ) 46T #G-ROTH-AMT ( 2 ) 66T #G-ROTH-AMT ( 3 ) / 'Rollover............' 26T #G-ROLLOVER-AMT ( 1 ) 46T #G-ROLLOVER-AMT ( 2 ) 66T #G-ROLLOVER-AMT ( 3 ) / 'Roth Conversion.....' 26T #G-ROTH-CNV-AMT ( 1 ) 46T #G-ROTH-CNV-AMT ( 2 ) 66T #G-ROTH-CNV-AMT ( 3 ) / 'Recharacter.........' 26T #G-RECHAR-AMT ( 1 ) 46T #G-RECHAR-AMT ( 2 ) 66T #G-RECHAR-AMT ( 3 ) / 'SEP Contrib.........' 26T #G-SEP-AMT ( 1 ) 46T #G-SEP-AMT ( 2 ) 66T #G-SEP-AMT ( 3 ) / 'Fair Market.........' 25T #G-FMV-AMT ( 1 ) 45T #G-FMV-AMT ( 2 ) 65T #G-FMV-AMT ( 3 ) / 'Late R/O Amt........' 25T #G-POSTPN-AMT ( 1 ) 45T #G-POSTPN-AMT ( 2 ) 65T #G-POSTPN-AMT ( 3 ) // 'Records Read...............................' #READ-CTR
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(53),pnd_G_Trans_Count.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(73),pnd_G_Trans_Count.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Classic Contrib.....",new TabSetting(26),pnd_G_Classic_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Classic_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Classic_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Contrib........",new TabSetting(26),pnd_G_Roth_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Roth_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Roth_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Rollover............",new TabSetting(26),pnd_G_Rollover_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Rollover_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Rollover_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Roth Conversion.....",new TabSetting(26),pnd_G_Roth_Cnv_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Roth_Cnv_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Roth_Cnv_Amt.getValue(3), new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Recharacter.........",new TabSetting(26),pnd_G_Rechar_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Rechar_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Rechar_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"SEP Contrib.........",new TabSetting(26),pnd_G_Sep_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(46),pnd_G_Sep_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(66),pnd_G_Sep_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Fair Market.........",new TabSetting(25),pnd_G_Fmv_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(45),pnd_G_Fmv_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(65),pnd_G_Fmv_Amt.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Late R/O Amt........",new TabSetting(25),pnd_G_Postpn_Amt.getValue(1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(45),pnd_G_Postpn_Amt.getValue(2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(65),pnd_G_Postpn_Amt.getValue(3), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(1),"Records Read...............................",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'Records Read...............................' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"NOTE:  For reconciliation purposes the data displayed in columns",NEWLINE,"       should be interpreted as follows:                        ", //Natural: WRITE ( 01 ) 'NOTE:  For reconciliation purposes the data displayed in columns' / '       should be interpreted as follows:                        ' // '1. INPUT               - Contract totals extracted for Moore' / '2. LOADED P2           - Contract totals from Period 2 load' / '3. ON FORM W/P2 LOAD   - Contract totals for contracts unchanged in ' / '                         Period 2 but where other 5498 contracts' / '                         for participant were loaded in Period 2'
            NEWLINE,NEWLINE,"1. INPUT               - Contract totals extracted for Moore",NEWLINE,"2. LOADED P2           - Contract totals from Period 2 load",
            NEWLINE,"3. ON FORM W/P2 LOAD   - Contract totals for contracts unchanged in ",NEWLINE,"                         Period 2 but where other 5498 contracts",
            NEWLINE,"                         for participant were loaded in Period 2");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* ******************
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 44T 'IRA Contributions E-delivery Control Report' 120T 'REPORT: RPT1' / 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #HEADER-DATE ( EM = MM/DD/YYYY ) // 22T '       Input       ' 42T #HEADER-COL-2 62T #HEADER-COL-3 / 23T '===================' 43T #HEADER-UNDERLINE 63T #HEADER-UNDERLINE /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(44),"IRA Contributions E-delivery Control Report",new TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(25),pnd_Header_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new TabSetting(97),pnd_Header_Date, new ReportEditMask 
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(22),"       Input       ",new TabSetting(42),pnd_Header_Col_2,new TabSetting(62),pnd_Header_Col_3,NEWLINE,new 
                        TabSetting(23),"===================",new TabSetting(43),pnd_Header_Underline,new TabSetting(63),pnd_Header_Underline,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, NEWLINE,"***",new ColumnSpacing(2),"PROGRAM...:",Global.getPROGRAM(),"Abended, Contact TaxWaRS group");                                     //Natural: WRITE ( 00 ) / '***' 2X 'PROGRAM...:' *PROGRAM 'Abended, Contact TaxWaRS group'
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
