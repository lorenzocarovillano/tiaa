/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:28 PM
**        * FROM NATURAL PROGRAM : Twrp3513
************************************************************
**        * FILE NAME            : Twrp3513.java
**        * CLASS NAME           : Twrp3513
**        * INSTANCE NAME        : Twrp3513
************************************************************
************************************************************************
** PROGRAM   : TWRP3513
** SYSTEM    : TAXWARS
** AUTHOR    : DIANE YHUN
** FUNCTION  : DISPLAY ACTIVE SUNY-CUNY SUBPLANS.
**           : THIS IS A MONTHLY JOB.
** HISTORY   : 07-16-2012 SUNY-CUNY PHASE 2
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3513 extends BLNatBase
{
    // Data Areas
    private LdaTwrlsprd ldaTwrlsprd;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Subplan_Start;
    private DbsField pnd_Ws_Pnd_Subplan_End;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Subplan_Count;
    private DbsField pnd_Ws_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlsprd = new LdaTwrlsprd();
        registerRecord(ldaTwrlsprd);
        registerRecord(ldaTwrlsprd.getVw_twrlspln());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Subplan_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Subplan_Start", "#SUBPLAN-START", FieldType.STRING, 7);
        pnd_Ws_Pnd_Subplan_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Subplan_End", "#SUBPLAN-END", FieldType.STRING, 7);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 3);
        pnd_Subplan_Count = localVariables.newFieldInRecord("pnd_Subplan_Count", "#SUBPLAN-COUNT", FieldType.NUMERIC, 9);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlsprd.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Pnd_Subplan_Start.setInitialValue("ATX012");
        pnd_Ws_Pnd_Subplan_End.setInitialValue("ATX012");
        pnd_Page_Number.setInitialValue(0);
        pnd_Subplan_Count.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3513() throws Exception
    {
        super("Twrp3513");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP3513", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Ws_Date.setValue(Global.getDATU());                                                                                                                           //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: MOVE *DATU TO #WS-DATE
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Subplan_Start,7,1);                                                                                        //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#SUBPLAN-START,7,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Subplan_End,7,1);                                                                                         //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#SUBPLAN-END,7,1 )
        ldaTwrlsprd.getVw_twrlspln().startDatabaseRead                                                                                                                    //Natural: READ TWRLSPLN BY RT-SUPER1 = #WS.#SUBPLAN-START THRU #WS.#SUBPLAN-END
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Ws_Pnd_Subplan_Start, "And", WcType.BY) ,
        new Wc("RT_SUPER1", "<=", pnd_Ws_Pnd_Subplan_End, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        READ01:
        while (condition(ldaTwrlsprd.getVw_twrlspln().readNextRow("READ01")))
        {
            if (condition(ldaTwrlsprd.getTwrlspln_Rt_Sp_Comm_Ind().notEquals(true)))                                                                                      //Natural: IF RT-SP-COMM-IND NOT = TRUE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-READ
        if (condition(ldaTwrlsprd.getVw_twrlspln().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-SUBPLAN
            sub_Write_Total_Subplan();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BODY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-SUBPLAN
        //* ***********************************************************************
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name: PTW1211M",new ColumnSpacing(25),"Tax Reporting And Withholding","System",new             //Natural: WRITE ( 1 ) NOTITLE NOHDR 'Job Name: PTW1211M' 25X 'Tax Reporting And Withholding' 'System' 30X 'Page:' #PAGE-NUMBER
            ColumnSpacing(30),"Page:",pnd_Page_Number);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Program:",Global.getPROGRAM(),new ColumnSpacing(17),"Subplans Selected for Participant","Letter Reporting",new        //Natural: WRITE ( 1 ) 'Program:' *PROGRAM 17X 'Subplans Selected for Participant' 'Letter Reporting' 25X 'Date:' #WS-DATE
            ColumnSpacing(25),"Date:",pnd_Ws_Date);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"        ");                                                                                                           //Natural: WRITE ( 1 ) '        '
        if (Global.isEscape()) return;
    }
    private void sub_Write_Body() throws Exception                                                                                                                        //Natural: WRITE-BODY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(1, new ColumnSpacing(1),"SubPlan/Nbr.",                                                                                                      //Natural: DISPLAY ( 1 ) 1X 'SubPlan/Nbr.' RT-SUBPLAN 'SubPlan Name/Letter SubPlan Name' RT-SP-NAME 'Comm/Ind' RT-SP-COMM-IND ( EM = N/Y ) 'Exc/Ind' RT-SP-EXCL-IND ( EM = N/Y ) '/Source' RT-SP-SRCE
        		ldaTwrlsprd.getTwrlspln_Rt_Subplan(),"SubPlan Name/Letter SubPlan Name",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Name(),"Comm/Ind",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Comm_Ind().getBoolean(), new ReportEditMask ("N/Y"),"Exc/Ind",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Excl_Ind().getBoolean(), new ReportEditMask ("N/Y"),"/Source",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Srce());
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(9),ldaTwrlsprd.getTwrlspln_Rt_Sp_Comm_Name());                                                       //Natural: WRITE ( 1 ) 9X RT-SP-COMM-NAME
        if (Global.isEscape()) return;
        pnd_Subplan_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SUBPLAN-COUNT
    }
    private void sub_Write_Total_Subplan() throws Exception                                                                                                               //Natural: WRITE-TOTAL-SUBPLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(8),"Total Number of SubPlans =",pnd_Subplan_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));              //Natural: WRITE ( 1 ) 8X 'Total Number of SubPlans =' #SUBPLAN-COUNT ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                    sub_Write_Headings();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "ON ERROR FOR SUBPLAN ",ldaTwrlsprd.getTwrlspln_Rt_Subplan());                                                                              //Natural: WRITE 'ON ERROR FOR SUBPLAN ' RT-SUBPLAN
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        getReports().write(0, "ELIN: ",Global.getERROR_LINE());                                                                                                           //Natural: WRITE '=' *ERROR-LINE
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, new ColumnSpacing(1),"SubPlan/Nbr.",
        		ldaTwrlsprd.getTwrlspln_Rt_Subplan(),"SubPlan Name/Letter SubPlan Name",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Name(),"Comm/Ind",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Comm_Ind().getBoolean(), new ReportEditMask ("N/Y"),"Exc/Ind",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Excl_Ind().getBoolean(), new ReportEditMask ("N/Y"),"/Source",
        		ldaTwrlsprd.getTwrlspln_Rt_Sp_Srce());
    }
}
