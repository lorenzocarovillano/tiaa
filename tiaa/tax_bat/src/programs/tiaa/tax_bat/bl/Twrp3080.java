/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:28 PM
**        * FROM NATURAL PROGRAM : Twrp3080
************************************************************
**        * FILE NAME            : Twrp3080.java
**        * CLASS NAME           : Twrp3080
**        * INSTANCE NAME        : Twrp3080
************************************************************
************************************************************************
* PROGRAM   : TWRP3080 CALIFORNIA ST TAX WITHHOLDING QTRLY REPORTING
*           : RENAMED FROM NPDPCAT4
* FUNCTION  : CALCALATES REPORTING QTR'S TAX & PRIOR QTRS' AJUSTMENTS
*             IF APPLICABLE.
*             FOR 1ST QTR,  CALC #QTR-ST-TAX(#REPORTING-PERIOD)
*             FOR 2-4 QTRS, CALC #QTR-ST-TAX(#REPORTING-PERIOD)
*                                #ADJ-ST-TAX(1:#REPORTING-PERIOD -1)
*             FOR YEAR-END ADJ PERIOD,
*                           CALC #ADJ-ST-TAX(1:#REPORTING-PERIOD -1)
*
* DATE      : 08/06/1997 CREATED BY LIN ZHENG
* UPDATE    : PUT HEADER TO A CONTROL TOTAL BEING PRINTED (EDITH)
*           : CHANGED PARAMETER CARD (EDITH - 3/29/99)
*           : INTERFACE-DATE-TO IS EQUAL TO RUNDATE (EDITH 4/13/99)
*           : ADDED CONDITION CODE IN TERMINATE STATEMENT 6/11/99
* 03/03/04 RM - CHANGE REPORTS TO COUNT ALL RECORDS AS TCII RECORDS.
*               THE INPUT FIELD '#TIAA-CREF' HAD BEEN REASSIGNED WITH
*               A SORT CODE OF '1' BY TWRP3060. NO CODE CHANGE IS NEEDED
*               IN THIS PROGRAM.
* 03/31/04 RM - CHANGE REPORTS TO COUNT ALL RECORDS AS TIAA RECORDS.
*               NO CODE CHANGE IS NEEDED IN THIS PROGRAM.
* 11/08/04: TOPS RELEASE 3 CHANGES       RM
*           A NEW COMPANY CODE 'X' FOR TRUST COMPANY HAD BEEN ADDED. THE
*           SORT CODE NOW CONTAINS EITHER A '1' OR A '2'. BUT NO CODE
*           CHANGE IS NEEDED IN THIS PROGRAM.
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3080 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Qtrly_Rec_Input;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Long_Key;

    private DbsGroup pnd_Qtrly_Rec_Input__R_Field_1;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Tiaa_Cref;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Qtrly_Rec_Input_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Qtrly_Rec_Input__R_Field_2;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Qtrly_Rec_Input_Form_Srce_Cde;
    private DbsField pnd_Qtrly_Rec_Input_Ss_Employee_Name;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Qtr_St_Tax;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Adj_St_Tax;
    private DbsField pnd_Qtrly_Rec_Input_Pnd_Diff_St_Tax;

    private DbsGroup pnd_Qtrly_Rec_Output;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Long_Key;

    private DbsGroup pnd_Qtrly_Rec_Output__R_Field_3;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Tiaa_Cref;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Qtrly_Rec_Output_Form_Cntrct_Py_Nmbr;

    private DbsGroup pnd_Qtrly_Rec_Output__R_Field_4;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Qtrly_Rec_Output_Form_Srce_Cde;
    private DbsField pnd_Qtrly_Rec_Output_Ss_Employee_Name;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax;
    private DbsField pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Adj_Idx_Max;
    private DbsField pnd_Ytd_Adj_St_Tax;
    private DbsField pnd_Cnt_Rec_Input;
    private DbsField pnd_Cnt_Rec_Output;
    private DbsField pnd_Reporting_Period;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_5;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_6;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Run_Type;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Long_KeyOld;
    private DbsField readWork01Form_Srce_CdeOld;
    private DbsField readWork01Ss_Employee_NameOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Qtrly_Rec_Input = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Input", "#QTRLY-REC-INPUT");
        pnd_Qtrly_Rec_Input_Pnd_Long_Key = pnd_Qtrly_Rec_Input.newFieldInGroup("pnd_Qtrly_Rec_Input_Pnd_Long_Key", "#LONG-KEY", FieldType.STRING, 20);

        pnd_Qtrly_Rec_Input__R_Field_1 = pnd_Qtrly_Rec_Input.newGroupInGroup("pnd_Qtrly_Rec_Input__R_Field_1", "REDEFINE", pnd_Qtrly_Rec_Input_Pnd_Long_Key);
        pnd_Qtrly_Rec_Input_Pnd_Tiaa_Cref = pnd_Qtrly_Rec_Input__R_Field_1.newFieldInGroup("pnd_Qtrly_Rec_Input_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 
            1);
        pnd_Qtrly_Rec_Input_Pnd_Annt_Soc_Sec_Nbr = pnd_Qtrly_Rec_Input__R_Field_1.newFieldInGroup("pnd_Qtrly_Rec_Input_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Qtrly_Rec_Input_Form_Cntrct_Py_Nmbr = pnd_Qtrly_Rec_Input__R_Field_1.newFieldInGroup("pnd_Qtrly_Rec_Input_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", 
            FieldType.STRING, 10);

        pnd_Qtrly_Rec_Input__R_Field_2 = pnd_Qtrly_Rec_Input__R_Field_1.newGroupInGroup("pnd_Qtrly_Rec_Input__R_Field_2", "REDEFINE", pnd_Qtrly_Rec_Input_Form_Cntrct_Py_Nmbr);
        pnd_Qtrly_Rec_Input_Pnd_Cntrct_Ppcn_Nbr = pnd_Qtrly_Rec_Input__R_Field_2.newFieldInGroup("pnd_Qtrly_Rec_Input_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Qtrly_Rec_Input_Pnd_Cntrct_Payee_Cde = pnd_Qtrly_Rec_Input__R_Field_2.newFieldInGroup("pnd_Qtrly_Rec_Input_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Qtrly_Rec_Input_Form_Srce_Cde = pnd_Qtrly_Rec_Input.newFieldInGroup("pnd_Qtrly_Rec_Input_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 
            2);
        pnd_Qtrly_Rec_Input_Ss_Employee_Name = pnd_Qtrly_Rec_Input.newFieldInGroup("pnd_Qtrly_Rec_Input_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 
            27);
        pnd_Qtrly_Rec_Input_Pnd_Qtr_St_Tax = pnd_Qtrly_Rec_Input.newFieldArrayInGroup("pnd_Qtrly_Rec_Input_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Input.newFieldArrayInGroup("pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Input_Pnd_Adj_St_Tax = pnd_Qtrly_Rec_Input.newFieldArrayInGroup("pnd_Qtrly_Rec_Input_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Input_Pnd_Diff_St_Tax = pnd_Qtrly_Rec_Input.newFieldArrayInGroup("pnd_Qtrly_Rec_Input_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));

        pnd_Qtrly_Rec_Output = localVariables.newGroupInRecord("pnd_Qtrly_Rec_Output", "#QTRLY-REC-OUTPUT");
        pnd_Qtrly_Rec_Output_Pnd_Long_Key = pnd_Qtrly_Rec_Output.newFieldInGroup("pnd_Qtrly_Rec_Output_Pnd_Long_Key", "#LONG-KEY", FieldType.STRING, 20);

        pnd_Qtrly_Rec_Output__R_Field_3 = pnd_Qtrly_Rec_Output.newGroupInGroup("pnd_Qtrly_Rec_Output__R_Field_3", "REDEFINE", pnd_Qtrly_Rec_Output_Pnd_Long_Key);
        pnd_Qtrly_Rec_Output_Pnd_Tiaa_Cref = pnd_Qtrly_Rec_Output__R_Field_3.newFieldInGroup("pnd_Qtrly_Rec_Output_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 
            1);
        pnd_Qtrly_Rec_Output_Pnd_Annt_Soc_Sec_Nbr = pnd_Qtrly_Rec_Output__R_Field_3.newFieldInGroup("pnd_Qtrly_Rec_Output_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Qtrly_Rec_Output_Form_Cntrct_Py_Nmbr = pnd_Qtrly_Rec_Output__R_Field_3.newFieldInGroup("pnd_Qtrly_Rec_Output_Form_Cntrct_Py_Nmbr", "FORM-CNTRCT-PY-NMBR", 
            FieldType.STRING, 10);

        pnd_Qtrly_Rec_Output__R_Field_4 = pnd_Qtrly_Rec_Output__R_Field_3.newGroupInGroup("pnd_Qtrly_Rec_Output__R_Field_4", "REDEFINE", pnd_Qtrly_Rec_Output_Form_Cntrct_Py_Nmbr);
        pnd_Qtrly_Rec_Output_Pnd_Cntrct_Ppcn_Nbr = pnd_Qtrly_Rec_Output__R_Field_4.newFieldInGroup("pnd_Qtrly_Rec_Output_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Qtrly_Rec_Output_Pnd_Cntrct_Payee_Cde = pnd_Qtrly_Rec_Output__R_Field_4.newFieldInGroup("pnd_Qtrly_Rec_Output_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Qtrly_Rec_Output_Form_Srce_Cde = pnd_Qtrly_Rec_Output.newFieldInGroup("pnd_Qtrly_Rec_Output_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 
            2);
        pnd_Qtrly_Rec_Output_Ss_Employee_Name = pnd_Qtrly_Rec_Output.newFieldInGroup("pnd_Qtrly_Rec_Output_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 
            27);
        pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax = pnd_Qtrly_Rec_Output.newFieldArrayInGroup("pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Output.newFieldArrayInGroup("pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax = pnd_Qtrly_Rec_Output.newFieldArrayInGroup("pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax = pnd_Qtrly_Rec_Output.newFieldArrayInGroup("pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 1);
        pnd_Adj_Idx_Max = localVariables.newFieldInRecord("pnd_Adj_Idx_Max", "#ADJ-IDX-MAX", FieldType.INTEGER, 1);
        pnd_Ytd_Adj_St_Tax = localVariables.newFieldInRecord("pnd_Ytd_Adj_St_Tax", "#YTD-ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cnt_Rec_Input = localVariables.newFieldInRecord("pnd_Cnt_Rec_Input", "#CNT-REC-INPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Rec_Output = localVariables.newFieldInRecord("pnd_Cnt_Rec_Output", "#CNT-REC-OUTPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Reporting_Period = localVariables.newFieldInRecord("pnd_Reporting_Period", "#REPORTING-PERIOD", FieldType.INTEGER, 1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_5", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_6 = pnd_Input_Parm__R_Field_5.newGroupInGroup("pnd_Input_Parm__R_Field_6", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Long_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Long_Key_OLD", "Pnd_Long_Key_OLD", FieldType.STRING, 20);
        readWork01Form_Srce_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Form_Srce_Cde_OLD", "Form_Srce_Cde_OLD", FieldType.STRING, 2);
        readWork01Ss_Employee_NameOld = internalLoopRecord.newFieldInRecord("ReadWork01_Ss_Employee_Name_OLD", "Ss_Employee_Name_OLD", FieldType.STRING, 
            27);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3080() throws Exception
    {
        super("Twrp3080");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3080|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: MOVE 'INFP9000' TO *ERROR-TA
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    pnd_Reporting_Period.setValue(5);                                                                                                                     //Natural: ASSIGN #REPORTING-PERIOD := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    short decideConditionsMet101 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM.#PYMNT-DATE-TO-MM;//Natural: VALUE 03
                    if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
                    {
                        decideConditionsMet101++;
                        pnd_Reporting_Period.setValue(1);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 1
                    }                                                                                                                                                     //Natural: VALUE 06
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
                    {
                        decideConditionsMet101++;
                        pnd_Reporting_Period.setValue(2);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 2
                    }                                                                                                                                                     //Natural: VALUE 09
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
                    {
                        decideConditionsMet101++;
                        pnd_Reporting_Period.setValue(3);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 3
                    }                                                                                                                                                     //Natural: VALUE 12
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
                    {
                        decideConditionsMet101++;
                        pnd_Reporting_Period.setValue(4);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 4
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!",NEWLINE,"ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES", //Natural: WRITE '!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!' /'ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES' /'PLEASE CONTACT SYSTEM SUPPORT'
                            NEWLINE,"PLEASE CONTACT SYSTEM SUPPORT");
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Adj_Idx_Max.compute(new ComputeParameters(false, pnd_Adj_Idx_Max), pnd_Reporting_Period.subtract(1));                                                 //Natural: ASSIGN #ADJ-IDX-MAX := #REPORTING-PERIOD -1
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK 1 #QTRLY-REC-INPUT
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pnd_Qtrly_Rec_Input)))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Cnt_Rec_Input.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-REC-INPUT
                    if (condition(pnd_Reporting_Period.equals(1)))                                                                                                        //Natural: IF #REPORTING-PERIOD = 1
                    {
                        pnd_Qtrly_Rec_Output.setValuesByName(pnd_Qtrly_Rec_Input);                                                                                        //Natural: MOVE BY NAME #QTRLY-REC-INPUT TO #QTRLY-REC-OUTPUT
                        pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period));   //Natural: MOVE #QTRLY-REC-INPUT.#YTD-ST-TAX ( #REPORTING-PERIOD ) TO #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #REPORTING-PERIOD )
                        getWorkFiles().write(2, false, pnd_Qtrly_Rec_Output);                                                                                             //Natural: WRITE WORK FILE 2 #QTRLY-REC-OUTPUT
                        pnd_Cnt_Rec_Output.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CNT-REC-OUTPUT
                        //*    PERFORM PRODUCE-REPORT   /* FOR TEST ONLY --- EDS
                        pnd_Qtrly_Rec_Output.reset();                                                                                                                     //Natural: RESET #QTRLY-REC-OUTPUT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO #ADJ-IDX-MAX
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Adj_Idx_Max)); pnd_I.nadd(1))
                    {
                        pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_I).nadd(pnd_Qtrly_Rec_Input_Pnd_Qtr_St_Tax.getValue(pnd_I));                                     //Natural: ASSIGN #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) + #QTRLY-REC-INPUT.#QTR-ST-TAX ( #I )
                        pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(pnd_I).nadd(pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax.getValue(pnd_I));                                     //Natural: ASSIGN #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #I ) + #QTRLY-REC-INPUT.#YTD-ST-TAX ( #I )
                        pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I).nadd(pnd_Qtrly_Rec_Input_Pnd_Adj_St_Tax.getValue(pnd_I));                                     //Natural: ASSIGN #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I ) + #QTRLY-REC-INPUT.#ADJ-ST-TAX ( #I )
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).nadd(pnd_Qtrly_Rec_Input_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period));           //Natural: ASSIGN #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #REPORTING-PERIOD ) := #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #REPORTING-PERIOD ) + #QTRLY-REC-INPUT.#YTD-ST-TAX ( #REPORTING-PERIOD )
                    //*                                                                                                                                                   //Natural: AT BREAK OF #QTRLY-REC-INPUT.#LONG-KEY
                    readWork01Pnd_Long_KeyOld.setValue(pnd_Qtrly_Rec_Input_Pnd_Long_Key);                                                                                 //Natural: END-WORK
                    readWork01Form_Srce_CdeOld.setValue(pnd_Qtrly_Rec_Input_Form_Srce_Cde);
                    readWork01Ss_Employee_NameOld.setValue(pnd_Qtrly_Rec_Input_Ss_Employee_Name);
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(30),"CALIFORNIA QUARTERLY TAX REPORTING",NEWLINE,Global.getDATX(),  //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 30T 'CALIFORNIA QUARTERLY TAX REPORTING' / *DATX ( EM = MM/DD/YYYY ) *TIMX 38T #RUN-TYPE / 35T 'C O N T R O L   T O T A L' /// 'TOTAL NUMBER OF INPUT RECORDS: ' #CNT-REC-INPUT / 'TOTAL NUMBER OF OUTPUT RECORDS:' #CNT-REC-OUTPUT
                    new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),new TabSetting(38),pnd_Run_Type,NEWLINE,new TabSetting(35),"C O N T R O L   T O T A L",
                    NEWLINE,NEWLINE,NEWLINE,"TOTAL NUMBER OF INPUT RECORDS: ",pnd_Cnt_Rec_Input,NEWLINE,"TOTAL NUMBER OF OUTPUT RECORDS:",pnd_Cnt_Rec_Output);
                if (Global.isEscape()) return;
                //* *-----------------------------------------------------------------
                //* *-----------------------------------------------------------------
                //* *******************************
                //* *******************************
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet205 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet205++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet205++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet205++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet205++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Reporting_Qtr_Tax_And_Prior_Qtrs_Adjustments() throws Exception                                                                                 //Natural: CALC-REPORTING-QTR-TAX-AND-PRIOR-QTRS-ADJUSTMENTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_Reporting_Period).compute(new ComputeParameters(false, pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_Reporting_Period)),  //Natural: ASSIGN #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #REPORTING-PERIOD ) := #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #REPORTING-PERIOD ) - #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #ADJ-IDX-MAX )
            pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).subtract(pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(pnd_Adj_Idx_Max)));
        //*  NO NEED TO CALC #QTR-ST-TAX FOR Y-E ADJ
        if (condition(pnd_Reporting_Period.notEquals(5)))                                                                                                                 //Natural: IF #REPORTING-PERIOD NE 5
        {
            if (condition(pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_Reporting_Period).greaterOrEqual(getZero())))                                                 //Natural: IF #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #REPORTING-PERIOD ) >= 0
            {
                pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_Reporting_Period));         //Natural: ASSIGN #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #REPORTING-PERIOD ) := #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #REPORTING-PERIOD )
                //*  THERE WON'T BE ANY ADJUSTMENT TO MAKE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_Reporting_Period).setValue(0);                                                                           //Natural: ASSIGN #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #REPORTING-PERIOD ) := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I = #ADJ-IDX-MAX TO 1 STEP -1
        for (pnd_I.setValue(pnd_Adj_Idx_Max); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
        {
            pnd_J.compute(new ComputeParameters(false, pnd_J), pnd_I.add(1));                                                                                             //Natural: ASSIGN #J := #I + 1
            pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_I)),              //Natural: ASSIGN #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #J ) + #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I )
                pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_J).add(pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_I)));
            if (condition(pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_I).greaterOrEqual(getZero())))                                                                //Natural: IF #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #I ) >= 0
            {
                pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I)),            //Natural: ASSIGN #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I ) := ( #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) - #QTRLY-REC-OUTPUT.#DIFF-ST-TAX ( #I ) ) * -1
                    (pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_I).subtract(pnd_Qtrly_Rec_Output_Pnd_Diff_St_Tax.getValue(pnd_I))).multiply(-1));
                //*  THERE WON'T BE ANY FURTHER ADJUSTMENTS TO MAKE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I)),            //Natural: ASSIGN #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) * -1
                    pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_I).multiply(-1));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Produce_Report() throws Exception                                                                                                                    //Natural: PRODUCE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "SSN",                                                                                                                                    //Natural: DISPLAY ( 1 ) 'SSN' #QTRLY-REC-OUTPUT.#ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'CONTRACT' #QTRLY-REC-OUTPUT.#CNTRCT-PPCN-NBR ( AL = 8 ) / 'PAYEE' #QTRLY-REC-OUTPUT.#CNTRCT-PAYEE-CDE 'CURRENT TAX' #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) / 'YTD-EOY-ADJ ' #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( 5 ) ( EM = ZZZ,ZZ9.99- )
        		pnd_Qtrly_Rec_Output_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Qtrly_Rec_Output_Pnd_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),NEWLINE,"PAYEE",
        		pnd_Qtrly_Rec_Output_Pnd_Cntrct_Payee_Cde,"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"YTD-EOY-ADJ ",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(5), new ReportEditMask ("ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),new TabSetting(50),"CALIFORNIA QUARTERLY TAX REPORTING",new       //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER 50T 'CALIFORNIA QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / *PROGRAM 38T 'DATA TO BE USED FOR REPORTING QUARTERLY TAX AND ADJUSTMENTS' 110T 'PAGE:' *PAGE-NUMBER ( 1 ) // 27T 'FIRST QUARTER' 51T 'SECOND QUARTER' 77T 'THIRD QUARTER' 97T 'FOURTH QUARTER / EOY ADJ' /
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,Global.getPROGRAM(),new TabSetting(38),"DATA TO BE USED FOR REPORTING QUARTERLY TAX AND ADJUSTMENTS",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(27),"FIRST QUARTER",new TabSetting(51),"SECOND QUARTER",new 
                        TabSetting(77),"THIRD QUARTER",new TabSetting(97),"FOURTH QUARTER / EOY ADJ",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Qtrly_Rec_Input_Pnd_Long_KeyIsBreak = pnd_Qtrly_Rec_Input_Pnd_Long_Key.isBreak(endOfData);
        if (condition(pnd_Qtrly_Rec_Input_Pnd_Long_KeyIsBreak))
        {
            if (condition(pnd_Reporting_Period.notEquals(1)))                                                                                                             //Natural: IF #REPORTING-PERIOD NE 1
            {
                pnd_Qtrly_Rec_Output_Pnd_Long_Key.setValue(readWork01Pnd_Long_KeyOld);                                                                                    //Natural: ASSIGN #QTRLY-REC-OUTPUT.#LONG-KEY := OLD ( #QTRLY-REC-INPUT.#LONG-KEY )
                pnd_Qtrly_Rec_Output_Form_Srce_Cde.setValue(readWork01Form_Srce_CdeOld);                                                                                  //Natural: ASSIGN #QTRLY-REC-OUTPUT.FORM-SRCE-CDE := OLD ( #QTRLY-REC-INPUT.FORM-SRCE-CDE )
                pnd_Qtrly_Rec_Output_Ss_Employee_Name.setValue(readWork01Ss_Employee_NameOld);                                                                            //Natural: ASSIGN #QTRLY-REC-OUTPUT.SS-EMPLOYEE-NAME := OLD ( #QTRLY-REC-INPUT.SS-EMPLOYEE-NAME )
                //* * IF THERE EXISTS PREVIOUS ADJUSTMENTS, REFLECTS THEM BACK TO
                //* * ITS #QTR-ST-TAX & #YTD-ST-TAX. THEN RESET ALL ADJ-ST-TAX(*)
                //* * TO GET READY FOR NEW ADJ-ST-TAX(1:#ADJ-IDX-MAX) CALCULATION
                pnd_Ytd_Adj_St_Tax.reset();                                                                                                                               //Natural: RESET #YTD-ADJ-ST-TAX
                FOR02:                                                                                                                                                    //Natural: FOR #I = 1 TO #ADJ-IDX-MAX
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Adj_Idx_Max)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I).notEquals(getZero())))                                                              //Natural: IF #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I ) NE 0
                    {
                        pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax.getValue(pnd_I).nadd(pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I));                                    //Natural: ASSIGN #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#QTR-ST-TAX ( #I ) + #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I )
                        pnd_Ytd_Adj_St_Tax.nadd(pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I));                                                                     //Natural: ASSIGN #YTD-ADJ-ST-TAX := #YTD-ADJ-ST-TAX + #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I )
                        pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax.getValue(pnd_I).nadd(pnd_Ytd_Adj_St_Tax);                                                                     //Natural: ASSIGN #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #I ) := #QTRLY-REC-OUTPUT.#YTD-ST-TAX ( #I ) + #YTD-ADJ-ST-TAX
                        pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax.getValue(pnd_I).reset();                                                                                      //Natural: RESET #QTRLY-REC-OUTPUT.#ADJ-ST-TAX ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CALC-REPORTING-QTR-TAX-AND-PRIOR-QTRS-ADJUSTMENTS
                sub_Calc_Reporting_Qtr_Tax_And_Prior_Qtrs_Adjustments();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getWorkFiles().write(2, false, pnd_Qtrly_Rec_Output);                                                                                                     //Natural: WRITE WORK FILE 2 #QTRLY-REC-OUTPUT
                pnd_Cnt_Rec_Output.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-REC-OUTPUT
                //*      PERFORM PRODUCE-REPORT    /* FOR TEST ONLY --- EDS
                pnd_Qtrly_Rec_Output.reset();                                                                                                                             //Natural: RESET #QTRLY-REC-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "SSN",
        		pnd_Qtrly_Rec_Output_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Qtrly_Rec_Output_Pnd_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),NEWLINE,"PAYEE",
        		pnd_Qtrly_Rec_Output_Pnd_Cntrct_Payee_Cde,"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Output_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Output_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"YTD-EOY-ADJ ",
        		pnd_Qtrly_Rec_Output_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"));
    }
}
