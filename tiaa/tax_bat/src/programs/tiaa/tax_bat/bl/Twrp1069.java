/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:31 PM
**        * FROM NATURAL PROGRAM : Twrp1069
************************************************************
**        * FILE NAME            : Twrp1069.java
**        * CLASS NAME           : Twrp1069
**        * INSTANCE NAME        : Twrp1069
************************************************************
************************************************************************
** PROGRAM : TWRP1069
** THIS PROGRAM IS A COPY OF TWRP5950 TO CALL TWRP1070 TO EXECUTE
** E-DELIVERY REPORT FOR TAX YEARS 2011 AND LATER.
** CREATED BY M. BERLIN.
** SYSTEM  : TAXWARS
** AUTHOR  : FELIX ORTIZ
** FUNCTION: DRIVER FOR FORMS DATABASE CONTROL REPORTS
**           (REPLACES OLD REPORT WITH SAME MODULE NAME;
**            FETCHES TWRP5954 FOR YEARS < 2001 AND TWRP5955
**            FOR YEARS >= 2001)
** HISTORY.....:
** 01/10/08  RM - TAKE OUT THE TEMPORARY FIX FOR CURRENT YR AND CHANGE
**                THE INPUT PARM TO BE '-1'.
**                CHANGE TERMINATE CODE FROM 16 TO 96.
** 01/07/2008  : A. YOUNG    - REVISED TAX-YEAR CALCULATION.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1069 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Rec;

    private DbsGroup pnd_Input_Rec__R_Field_1;
    private DbsField pnd_Input_Rec_Pnd_Tax_Year_A;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Sys_Date_N;
    private DbsField pnd_Debug;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_Rec = localVariables.newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 80);

        pnd_Input_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_1", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Tax_Year_A = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Sys_Date_N = localVariables.newFieldInRecord("pnd_Sys_Date_N", "#SYS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Debug.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1069() throws Exception
    {
        super("Twrp1069");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pnd_Sys_Date_N.setValue(Global.getDATN());                                                                                                                        //Natural: ASSIGN #SYS-DATE-N := *DATN
        //*   GET OPTIONAL TAX YEAR PARAMETER
        getWorkFiles().read(1, pnd_Input_Rec);                                                                                                                            //Natural: READ WORK FILE 1 ONCE #INPUT-REC
        //*  01/07/2008
        pnd_Tax_Year.reset();                                                                                                                                             //Natural: RESET #TAX-YEAR
        //*   CALCULATE TAX YEAR
        //*  01/07/2008
        if (condition(DbsUtil.maskMatches(pnd_Input_Rec_Pnd_Tax_Year_A,"NNNN")))                                                                                          //Natural: IF #INPUT-REC.#TAX-YEAR-A EQ MASK ( NNNN )
        {
            pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Input_Rec_Pnd_Tax_Year_A.val());                                                         //Natural: ASSIGN #TAX-YEAR := VAL ( #TAX-YEAR-A )
            //*  01/07/2008
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  NEXT YEAR
            //*  CURRENT YR
            //*  CURRENT YR - 1
            //*  CURRENT YR - 2
            //*  CURRENT YR - 3
            short decideConditionsMet64 = 0;                                                                                                                              //Natural: DECIDE ON FIRST #INPUT-REC.#TAX-YEAR-A;//Natural: VALUE '+1'
            if (condition((pnd_Input_Rec_Pnd_Tax_Year_A.equals("+1"))))
            {
                decideConditionsMet64++;
                pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Sys_Date_N.divide(10000).add(1));                                                    //Natural: ASSIGN #TAX-YEAR := #SYS-DATE-N / 10000 + 1
            }                                                                                                                                                             //Natural: VALUE ' ', '0'
            else if (condition((pnd_Input_Rec_Pnd_Tax_Year_A.equals(" ") || pnd_Input_Rec_Pnd_Tax_Year_A.equals("0"))))
            {
                decideConditionsMet64++;
                pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Sys_Date_N.divide(10000));                                                           //Natural: ASSIGN #TAX-YEAR := #SYS-DATE-N / 10000
            }                                                                                                                                                             //Natural: VALUE '-1'
            else if (condition((pnd_Input_Rec_Pnd_Tax_Year_A.equals("-1"))))
            {
                decideConditionsMet64++;
                pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Sys_Date_N.divide(10000).subtract(1));                                               //Natural: ASSIGN #TAX-YEAR := #SYS-DATE-N / 10000 - 1
            }                                                                                                                                                             //Natural: VALUE '-2'
            else if (condition((pnd_Input_Rec_Pnd_Tax_Year_A.equals("-2"))))
            {
                decideConditionsMet64++;
                pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Sys_Date_N.divide(10000).subtract(2));                                               //Natural: ASSIGN #TAX-YEAR := #SYS-DATE-N / 10000 - 2
            }                                                                                                                                                             //Natural: VALUE '-3'
            else if (condition((pnd_Input_Rec_Pnd_Tax_Year_A.equals("-3"))))
            {
                decideConditionsMet64++;
                pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), pnd_Sys_Date_N.divide(10000).subtract(3));                                               //Natural: ASSIGN #TAX-YEAR := #SYS-DATE-N / 10000 - 3
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                getReports().write(1, "Invalid tax year parameter found.","=",pnd_Input_Rec_Pnd_Tax_Year_A,"Program Terminated.");                                        //Natural: WRITE ( 1 ) 'Invalid tax year parameter found.' '=' #INPUT-REC.#TAX-YEAR-A 'Program Terminated.'
                if (Global.isEscape()) return;
                //*  01/10/08  RM
                DbsUtil.terminate(96);  if (true) return;                                                                                                                 //Natural: TERMINATE 96
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  01/07/2008
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,NEWLINE,Global.getPROGRAM(),"=",pnd_Input_Rec_Pnd_Tax_Year_A,"=",pnd_Tax_Year, new ReportEditMask ("9999"));                        //Natural: WRITE // *PROGRAM '=' #INPUT-REC.#TAX-YEAR-A '=' #TAX-YEAR
        if (Global.isEscape()) return;
        short decideConditionsMet86 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #TAX-YEAR;//Natural: VALUE 2008 : 2010
        if (condition(((pnd_Tax_Year.greaterOrEqual(2008) && pnd_Tax_Year.lessOrEqual(2010)))))
        {
            decideConditionsMet86++;
            //*    FETCH 'TWRP5958' #TAX-YEAR
            //*  NEW 480.7C LOGIC
            getReports().write(0, NEWLINE,NEWLINE,"Please run P1060TWD for Tax-Year ",pnd_Tax_Year, new ReportEditMask ("9999"));                                         //Natural: WRITE // 'Please run P1060TWD for Tax-Year ' #TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2007
        else if (condition((pnd_Tax_Year.equals(2007))))
        {
            decideConditionsMet86++;
            //*    FETCH 'TWRP5957' #TAX-YEAR
            //*  NEW W/5498 LOGIC
            getReports().write(0, NEWLINE,NEWLINE,"Please run P1060TWD for Tax-Year ",pnd_Tax_Year, new ReportEditMask ("9999"));                                         //Natural: WRITE // 'Please run P1060TWD for Tax-Year ' #TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2001 : 2006
        else if (condition(((pnd_Tax_Year.greaterOrEqual(2001) && pnd_Tax_Year.lessOrEqual(2006)))))
        {
            decideConditionsMet86++;
            //*    FETCH 'TWRP5956' #TAX-YEAR                   /* & NEW REPORT (9)
            getReports().write(0, NEWLINE,NEWLINE,"Please run P1060TWD for Tax-Year ",pnd_Tax_Year, new ReportEditMask ("9999"));                                         //Natural: WRITE // 'Please run P1060TWD for Tax-Year ' #TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2000
        else if (condition((pnd_Tax_Year.equals(2000))))
        {
            decideConditionsMet86++;
            //*    FETCH 'TWRP5955' #TAX-YEAR                   /* W/NEW REPORT
            getReports().write(0, NEWLINE,NEWLINE,"Please run P1060TWD for Tax-Year ",pnd_Tax_Year, new ReportEditMask ("9999"));                                         //Natural: WRITE // 'Please run P1060TWD for Tax-Year ' #TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 1999
        else if (condition((pnd_Tax_Year.equals(1999))))
        {
            decideConditionsMet86++;
            //*     FETCH 'TWRP5954' #TAX-YEAR
            getReports().write(0, NEWLINE,NEWLINE,"Please run P1060TWD for Tax-Year ",pnd_Tax_Year, new ReportEditMask ("9999"));                                         //Natural: WRITE // 'Please run P1060TWD for Tax-Year ' #TAX-YEAR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*  >= 2011 - IRR AMT
            Global.getSTACK().pushData(StackOption.TOP, pnd_Tax_Year);                                                                                                    //Natural: FETCH 'TWRP1070' #TAX-YEAR
            Global.setFetchProgram(DbsUtil.getBlType("TWRP1070"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
