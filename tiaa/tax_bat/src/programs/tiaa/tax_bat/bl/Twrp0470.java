/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:23 PM
**        * FROM NATURAL PROGRAM : Twrp0470
************************************************************
**        * FILE NAME            : Twrp0470.java
**        * CLASS NAME           : Twrp0470
**        * INSTANCE NAME        : Twrp0470
************************************************************
************************************************************************
** PROGRAM : TWRP0470
** SYSTEM  : TAXWARS
** AUTHOR  : RITA SALGADO
** FUNCTION: LOAD PLAN TABLE WITH DATA FROM EDW.
** HISTORY : 01/2012  SUNY/CUNY
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0470 extends BLNatBase
{
    // Data Areas
    private LdaTwrlplrd ldaTwrlplrd;
    private LdaTwrlplup ldaTwrlplup;
    private PdaTwratbl2 pdaTwratbl2;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup edw_Record;
    private DbsField edw_Record_Edw_Plan;
    private DbsField edw_Record_Edw_Plan_Name;
    private DbsField edw_Record_Edw_Employee_Type;
    private DbsField edw_Record_Edw_Comp_Addr_State;

    private DbsGroup edw_Record__R_Field_1;
    private DbsField edw_Record__Filler1;
    private DbsField edw_Record_Edw_State;
    private DbsField pnd_Timx;
    private DbsField pnd_Isn;
    private DbsField pnd_Update;
    private DbsField pnd_Add;
    private DbsField pnd_Inverse_Lu_Ts;
    private DbsField pnd_Action;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Loaded;
    private DbsField pnd_Total_Not_Loaded;
    private DbsField pnd_Total_No_Info;
    private DbsField pnd_Total_Dup_Feed;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_2;
    private DbsField pnd_Rt_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Plan;
    private DbsField pnd_Rt_Data;

    private DbsGroup pnd_Rt_Data__R_Field_3;
    private DbsField pnd_Rt_Data_Pnd_Rt_Pl_Name;
    private DbsField pnd_Rt_Data_Pnd_Rt_Pl_Type;
    private DbsField pnd_Rt_Data_Pnd_Rt_Pl_Res;
    private DbsField pnd_Prior_Plan;
    private DbsField pnd_Edw_Data;

    private DbsGroup pnd_Edw_Data__R_Field_4;
    private DbsField pnd_Edw_Data_Pnd_Edw_Plan_Name;
    private DbsField pnd_Edw_Data_Pnd_Edw_Employee_Type;
    private DbsField pnd_Edw_Data_Pnd_Edw_Comp_Addr_State;
    private DbsField pnd_Rt_Pl_Comm_Ind;
    private DbsField pnd_Rt_Pl_Excl_Ind;
    private DbsField pnd_Rt_Pl_Hybrid_Ind;
    private DbsField pnd_Rt_Pl_Comm_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlplrd = new LdaTwrlplrd();
        registerRecord(ldaTwrlplrd);
        registerRecord(ldaTwrlplrd.getVw_twrlpln());
        ldaTwrlplup = new LdaTwrlplup();
        registerRecord(ldaTwrlplup);
        registerRecord(ldaTwrlplup.getVw_twrlpln_U());
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);

        // Local Variables

        edw_Record = localVariables.newGroupInRecord("edw_Record", "EDW-RECORD");
        edw_Record_Edw_Plan = edw_Record.newFieldInGroup("edw_Record_Edw_Plan", "EDW-PLAN", FieldType.STRING, 6);
        edw_Record_Edw_Plan_Name = edw_Record.newFieldInGroup("edw_Record_Edw_Plan_Name", "EDW-PLAN-NAME", FieldType.STRING, 64);
        edw_Record_Edw_Employee_Type = edw_Record.newFieldInGroup("edw_Record_Edw_Employee_Type", "EDW-EMPLOYEE-TYPE", FieldType.STRING, 2);
        edw_Record_Edw_Comp_Addr_State = edw_Record.newFieldInGroup("edw_Record_Edw_Comp_Addr_State", "EDW-COMP-ADDR-STATE", FieldType.STRING, 3);

        edw_Record__R_Field_1 = edw_Record.newGroupInGroup("edw_Record__R_Field_1", "REDEFINE", edw_Record_Edw_Comp_Addr_State);
        edw_Record__Filler1 = edw_Record__R_Field_1.newFieldInGroup("edw_Record__Filler1", "_FILLER1", FieldType.STRING, 1);
        edw_Record_Edw_State = edw_Record__R_Field_1.newFieldInGroup("edw_Record_Edw_State", "EDW-STATE", FieldType.STRING, 2);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Inverse_Lu_Ts = localVariables.newFieldInRecord("pnd_Inverse_Lu_Ts", "#INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 6);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Loaded = localVariables.newFieldInRecord("pnd_Total_Loaded", "#TOTAL-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Not_Loaded = localVariables.newFieldInRecord("pnd_Total_Not_Loaded", "#TOTAL-NOT-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_No_Info = localVariables.newFieldInRecord("pnd_Total_No_Info", "#TOTAL-NO-INFO", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Dup_Feed = localVariables.newFieldInRecord("pnd_Total_Dup_Feed", "#TOTAL-DUP-FEED", FieldType.PACKED_DECIMAL, 7);
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_2 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_2", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind = pnd_Rt_Super1__R_Field_2.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt_Table_Id = pnd_Rt_Super1__R_Field_2.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Pnd_Rt_Plan = pnd_Rt_Super1__R_Field_2.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Plan", "#RT-PLAN", FieldType.STRING, 6);
        pnd_Rt_Data = localVariables.newFieldInRecord("pnd_Rt_Data", "#RT-DATA", FieldType.STRING, 69);

        pnd_Rt_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Rt_Data__R_Field_3", "REDEFINE", pnd_Rt_Data);
        pnd_Rt_Data_Pnd_Rt_Pl_Name = pnd_Rt_Data__R_Field_3.newFieldInGroup("pnd_Rt_Data_Pnd_Rt_Pl_Name", "#RT-PL-NAME", FieldType.STRING, 64);
        pnd_Rt_Data_Pnd_Rt_Pl_Type = pnd_Rt_Data__R_Field_3.newFieldInGroup("pnd_Rt_Data_Pnd_Rt_Pl_Type", "#RT-PL-TYPE", FieldType.STRING, 2);
        pnd_Rt_Data_Pnd_Rt_Pl_Res = pnd_Rt_Data__R_Field_3.newFieldInGroup("pnd_Rt_Data_Pnd_Rt_Pl_Res", "#RT-PL-RES", FieldType.STRING, 3);
        pnd_Prior_Plan = localVariables.newFieldInRecord("pnd_Prior_Plan", "#PRIOR-PLAN", FieldType.STRING, 6);
        pnd_Edw_Data = localVariables.newFieldInRecord("pnd_Edw_Data", "#EDW-DATA", FieldType.STRING, 69);

        pnd_Edw_Data__R_Field_4 = localVariables.newGroupInRecord("pnd_Edw_Data__R_Field_4", "REDEFINE", pnd_Edw_Data);
        pnd_Edw_Data_Pnd_Edw_Plan_Name = pnd_Edw_Data__R_Field_4.newFieldInGroup("pnd_Edw_Data_Pnd_Edw_Plan_Name", "#EDW-PLAN-NAME", FieldType.STRING, 
            64);
        pnd_Edw_Data_Pnd_Edw_Employee_Type = pnd_Edw_Data__R_Field_4.newFieldInGroup("pnd_Edw_Data_Pnd_Edw_Employee_Type", "#EDW-EMPLOYEE-TYPE", FieldType.STRING, 
            2);
        pnd_Edw_Data_Pnd_Edw_Comp_Addr_State = pnd_Edw_Data__R_Field_4.newFieldInGroup("pnd_Edw_Data_Pnd_Edw_Comp_Addr_State", "#EDW-COMP-ADDR-STATE", 
            FieldType.STRING, 3);
        pnd_Rt_Pl_Comm_Ind = localVariables.newFieldInRecord("pnd_Rt_Pl_Comm_Ind", "#RT-PL-COMM-IND", FieldType.BOOLEAN, 1);
        pnd_Rt_Pl_Excl_Ind = localVariables.newFieldInRecord("pnd_Rt_Pl_Excl_Ind", "#RT-PL-EXCL-IND", FieldType.BOOLEAN, 1);
        pnd_Rt_Pl_Hybrid_Ind = localVariables.newFieldInRecord("pnd_Rt_Pl_Hybrid_Ind", "#RT-PL-HYBRID-IND", FieldType.BOOLEAN, 1);
        pnd_Rt_Pl_Comm_Name = localVariables.newFieldInRecord("pnd_Rt_Pl_Comm_Name", "#RT-PL-COMM-NAME", FieldType.STRING, 64);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlplrd.initializeValues();
        ldaTwrlplup.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0470() throws Exception
    {
        super("Twrp0470");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        //*  ONE TIME STAMP FOR THE ENTIRE LOAD
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *INIT-USER '-' *PROGRAM 45T 'TAX WITHHOLDING AND REPORTING SYSTEM' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / 'RUNDATE :' *DATU 50T 'PLAN TABLE CONTROL REPORT' / 'RUNTIME :' *TIMX ( EM = HH:IIAP ) // 'TAX YEAR:' //
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: ASSIGN #TIMX := *TIMX
        R_EDW:                                                                                                                                                            //Natural: READ WORK FILE 1 EDW-RECORD
        while (condition(getWorkFiles().read(1, edw_Record)))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(edw_Record_Edw_Plan.equals(" ") && edw_Record_Edw_Plan_Name.equals(" ")))                                                                       //Natural: IF EDW-PLAN = ' ' AND EDW-PLAN-NAME = ' '
            {
                pnd_Total_No_Info.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-NO-INFO
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(edw_Record_Edw_Plan.equals(pnd_Prior_Plan)))                                                                                                    //Natural: IF EDW-PLAN = #PRIOR-PLAN
            {
                pnd_Total_Dup_Feed.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-DUP-FEED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prior_Plan.setValue(edw_Record_Edw_Plan);                                                                                                             //Natural: ASSIGN #PRIOR-PLAN := EDW-PLAN
                //*  STATE LOOK UP BY ALPHA
            }                                                                                                                                                             //Natural: END-IF
            //*  TRANSLATE ALPHA STATE TO NUMERIC
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(edw_Record_Edw_State);                                                                                       //Natural: ASSIGN TWRATBL2.#STATE-CDE := EDW-STATE
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), Global.getDATN().divide(10000));         //Natural: ASSIGN TWRATBL2.#TAX-YEAR := *DATN / 10000
            pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
            pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                    //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), pdaTwratbl2.getTwratbl2_Output_Data());                     //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS TWRATBL2.OUTPUT-DATA
            if (condition(Global.isEscape())) return;
            //*  #EDW-DATA
            pnd_Edw_Data_Pnd_Edw_Plan_Name.setValue(edw_Record_Edw_Plan_Name);                                                                                            //Natural: ASSIGN #EDW-PLAN-NAME := EDW-PLAN-NAME
            pnd_Edw_Data_Pnd_Edw_Employee_Type.setValue(edw_Record_Edw_Employee_Type);                                                                                    //Natural: ASSIGN #EDW-EMPLOYEE-TYPE := EDW-EMPLOYEE-TYPE
            pnd_Edw_Data_Pnd_Edw_Comp_Addr_State.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Old_Code());                                                              //Natural: ASSIGN #EDW-COMP-ADDR-STATE := TIRCNTL-STATE-OLD-CODE
            pnd_Rt_Super1.setValue(" ");                                                                                                                                  //Natural: ASSIGN #RT-SUPER1 := ' '
            pnd_Rt_Super1_Pnd_Rt_A_I_Ind.setValue("A");                                                                                                                   //Natural: ASSIGN #RT-A-I-IND := 'A'
            pnd_Rt_Super1_Pnd_Rt_Table_Id.setValue("TX011");                                                                                                              //Natural: ASSIGN #RT-TABLE-ID := 'TX011'
            pnd_Rt_Super1_Pnd_Rt_Plan.setValue(edw_Record_Edw_Plan);                                                                                                      //Natural: ASSIGN #RT-PLAN := EDW-PLAN
            ldaTwrlplrd.getVw_twrlpln().startDatabaseRead                                                                                                                 //Natural: READ ( 1 ) TWRLPLN BY RT-SUPER1 STARTING FROM #RT-SUPER1
            (
            "R_PLAN",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            R_PLAN:
            while (condition(ldaTwrlplrd.getVw_twrlpln().readNextRow("R_PLAN")))
            {
                pnd_Isn.setValue(ldaTwrlplrd.getVw_twrlpln().getAstISN("R_PLAN"));                                                                                        //Natural: ASSIGN #ISN := *ISN
                //*    #RT-DATA
                pnd_Rt_Data_Pnd_Rt_Pl_Name.setValue(ldaTwrlplrd.getTwrlpln_Rt_Pl_Name());                                                                                 //Natural: ASSIGN #RT-PL-NAME := TWRLPLN.RT-PL-NAME
                pnd_Rt_Data_Pnd_Rt_Pl_Type.setValue(ldaTwrlplrd.getTwrlpln_Rt_Pl_Type());                                                                                 //Natural: ASSIGN #RT-PL-TYPE := TWRLPLN.RT-PL-TYPE
                pnd_Rt_Data_Pnd_Rt_Pl_Res.setValue(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res());                                                                                   //Natural: ASSIGN #RT-PL-RES := TWRLPLN.RT-PL-RES
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R_EDW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Add.setValue(false);                                                                                                                                      //Natural: ASSIGN #ADD := FALSE
            pnd_Update.setValue(false);                                                                                                                                   //Natural: ASSIGN #UPDATE := FALSE
            short decideConditionsMet243 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRLPLN.RT-TABLE-ID NE 'TX011' OR TWRLPLN.RT-A-I-IND NE 'A' OR TWRLPLN.RT-PLAN NE EDW-PLAN
            if (condition(ldaTwrlplrd.getTwrlpln_Rt_Table_Id().notEquals("TX011") || ldaTwrlplrd.getTwrlpln_Rt_A_I_Ind().notEquals("A") || ldaTwrlplrd.getTwrlpln_Rt_Plan().notEquals(edw_Record_Edw_Plan)))
            {
                decideConditionsMet243++;
                pnd_Add.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADD := TRUE
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-PLAN
                sub_Add_New_Plan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN TWRLPLN.RT-TABLE-ID = 'TX011' AND TWRLPLN.RT-A-I-IND = 'A' AND TWRLPLN.RT-PLAN = EDW-PLAN AND #RT-DATA NE #EDW-DATA
            else if (condition(ldaTwrlplrd.getTwrlpln_Rt_Table_Id().equals("TX011") && ldaTwrlplrd.getTwrlpln_Rt_A_I_Ind().equals("A") && ldaTwrlplrd.getTwrlpln_Rt_Plan().equals(edw_Record_Edw_Plan) 
                && pnd_Rt_Data.notEquals(pnd_Edw_Data)))
            {
                decideConditionsMet243++;
                pnd_Update.setValue(true);                                                                                                                                //Natural: ASSIGN #UPDATE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-PLAN
                sub_Update_Plan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet243 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-PLAN
                sub_Print_Plan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Total_Not_Loaded.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-NOT-LOADED
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        R_EDW_Exit:
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PLAN
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-NEW-PLAN
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PLAN
        //* ********************************************************************
        getReports().write(1, NEWLINE,"Total Records from file  :",pnd_Total_Read,NEWLINE,"Total Records with Blanks:",pnd_Total_No_Info,NEWLINE,"Total Duplicate EDW Recds:", //Natural: WRITE ( 01 ) / 'Total Records from file  :' #TOTAL-READ / 'Total Records with Blanks:' #TOTAL-NO-INFO / 'Total Duplicate EDW Recds:' #TOTAL-DUP-FEED / 'Total Records not Loaded :' #TOTAL-NOT-LOADED / 'Total Records Loaded     :' #TOTAL-LOADED
            pnd_Total_Dup_Feed,NEWLINE,"Total Records not Loaded :",pnd_Total_Not_Loaded,NEWLINE,"Total Records Loaded     :",pnd_Total_Loaded);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Plan() throws Exception                                                                                                                       //Natural: UPDATE-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        UPDATE_PLAN:                                                                                                                                                      //Natural: GET TWRLPLN-U #ISN
        ldaTwrlplup.getVw_twrlpln_U().readByID(pnd_Isn.getLong(), "UPDATE_PLAN");
        ldaTwrlplup.getTwrlpln_U_Rt_A_I_Ind().setValue("I");                                                                                                              //Natural: ASSIGN TWRLPLN-U.RT-A-I-IND := 'I'
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Lu_Ts().setValue(pnd_Timx);                                                                                                        //Natural: ASSIGN TWRLPLN-U.RT-PL-LU-TS := #TIMX
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlplup.getTwrlpln_U_Rt_Pl_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Lu_Ts())); //Natural: ASSIGN TWRLPLN-U.RT-PL-INVERSE-LU-TS := 1000000000000 - TWRLPLN-U.RT-PL-LU-TS
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                      //Natural: ASSIGN TWRLPLN-U.RT-PL-LU-USER-ID := *INIT-USER
        pnd_Rt_Pl_Comm_Ind.setValue(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Ind().getBoolean());                                                                              //Natural: ASSIGN #RT-PL-COMM-IND := TWRLPLN-U.RT-PL-COMM-IND
        pnd_Rt_Pl_Excl_Ind.setValue(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Excl_Ind().getBoolean());                                                                              //Natural: ASSIGN #RT-PL-EXCL-IND := TWRLPLN-U.RT-PL-EXCL-IND
        pnd_Rt_Pl_Hybrid_Ind.setValue(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Hybrid_Ind().getBoolean());                                                                          //Natural: ASSIGN #RT-PL-HYBRID-IND := TWRLPLN-U.RT-PL-HYBRID-IND
        pnd_Rt_Pl_Comm_Name.setValue(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Name());                                                                                         //Natural: ASSIGN #RT-PL-COMM-NAME := TWRLPLN-U.RT-PL-COMM-NAME
        ldaTwrlplup.getVw_twrlpln_U().updateDBRow("UPDATE_PLAN");                                                                                                         //Natural: UPDATE ( UPDATE-PLAN. )
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-PLAN
        sub_Add_New_Plan();
        if (condition(Global.isEscape())) {return;}
        //*  UPDATE-PLAN
    }
    private void sub_Add_New_Plan() throws Exception                                                                                                                      //Natural: ADD-NEW-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrlplup.getTwrlpln_U_Rt_A_I_Ind().setValue("A");                                                                                                              //Natural: ASSIGN TWRLPLN-U.RT-A-I-IND := 'A'
        ldaTwrlplup.getTwrlpln_U_Rt_Table_Id().setValue("TX011");                                                                                                         //Natural: ASSIGN TWRLPLN-U.RT-TABLE-ID := 'TX011'
        ldaTwrlplup.getTwrlpln_U_Rt_Plan().setValue(edw_Record_Edw_Plan);                                                                                                 //Natural: ASSIGN TWRLPLN-U.RT-PLAN := EDW-PLAN
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Cr_User_Id().setValue(Global.getINIT_USER());                                                                                      //Natural: ASSIGN TWRLPLN-U.RT-PL-CR-USER-ID := *INIT-USER
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Cr_Ts().setValue(pnd_Timx);                                                                                                        //Natural: ASSIGN TWRLPLN-U.RT-PL-CR-TS := #TIMX
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                      //Natural: ASSIGN TWRLPLN-U.RT-PL-LU-USER-ID := *INIT-USER
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Lu_Ts().setValue(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Cr_Ts());                                                                          //Natural: ASSIGN TWRLPLN-U.RT-PL-LU-TS := TWRLPLN-U.RT-PL-CR-TS
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlplup.getTwrlpln_U_Rt_Pl_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlplup.getTwrlpln_U_Rt_Pl_Cr_Ts())); //Natural: ASSIGN TWRLPLN-U.RT-PL-INVERSE-LU-TS := 1000000000000 - TWRLPLN-U.RT-PL-CR-TS
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Srce().setValue("B");                                                                                                              //Natural: ASSIGN TWRLPLN-U.RT-PL-SRCE := 'B'
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Type().setValue(edw_Record_Edw_Employee_Type);                                                                                     //Natural: ASSIGN TWRLPLN-U.RT-PL-TYPE := EDW-EMPLOYEE-TYPE
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Res().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Old_Code());                                                                  //Natural: ASSIGN TWRLPLN-U.RT-PL-RES := TIRCNTL-STATE-OLD-CODE
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Ind().reset();                                                                                                            //Natural: RESET TWRLPLN-U.RT-PL-COMM-IND TWRLPLN-U.RT-PL-EXCL-IND TWRLPLN-U.RT-PL-HYBRID-IND TWRLPLN-U.RT-PL-COMM-NAME
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Excl_Ind().reset();
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Hybrid_Ind().reset();
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Name().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Ind().setValue(pnd_Rt_Pl_Comm_Ind.getBoolean());                                                                          //Natural: ASSIGN TWRLPLN-U.RT-PL-COMM-IND := #RT-PL-COMM-IND
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Excl_Ind().setValue(pnd_Rt_Pl_Excl_Ind.getBoolean());                                                                          //Natural: ASSIGN TWRLPLN-U.RT-PL-EXCL-IND := #RT-PL-EXCL-IND
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Hybrid_Ind().setValue(pnd_Rt_Pl_Hybrid_Ind.getBoolean());                                                                      //Natural: ASSIGN TWRLPLN-U.RT-PL-HYBRID-IND := #RT-PL-HYBRID-IND
            ldaTwrlplup.getTwrlpln_U_Rt_Pl_Comm_Name().setValue(pnd_Rt_Pl_Comm_Name);                                                                                     //Natural: ASSIGN TWRLPLN-U.RT-PL-COMM-NAME := #RT-PL-COMM-NAME
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrlplup.getTwrlpln_U_Rt_Pl_Name().setValue(edw_Record_Edw_Plan_Name);                                                                                         //Natural: ASSIGN TWRLPLN-U.RT-PL-NAME := EDW-PLAN-NAME
        ldaTwrlplup.getTwrlpln_U_Rt_Long_Key().setValue(edw_Record_Edw_Plan_Name);                                                                                        //Natural: ASSIGN TWRLPLN-U.RT-LONG-KEY := EDW-PLAN-NAME
        ldaTwrlplup.getVw_twrlpln_U().insertDBRow();                                                                                                                      //Natural: STORE TWRLPLN-U
        //*  ADD-NEW-PLAN
    }
    private void sub_Print_Plan() throws Exception                                                                                                                        //Natural: PRINT-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Update.getBoolean()))                                                                                                                           //Natural: IF #UPDATE
        {
            pnd_Action.setValue("Update");                                                                                                                                //Natural: ASSIGN #ACTION := 'Update'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            pnd_Action.setValue("Add");                                                                                                                                   //Natural: ASSIGN #ACTION := 'Add'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "Plan",                                                                                                                                   //Natural: DISPLAY ( 01 ) 'Plan' EDW-PLAN 'Plan Name' EDW-PLAN-NAME 'Emp Type' EDW-EMPLOYEE-TYPE 'Co. State' EDW-COMP-ADDR-STATE 'Action' #ACTION
        		edw_Record_Edw_Plan,"Plan Name",
        		edw_Record_Edw_Plan_Name,"Emp Type",
        		edw_Record_Edw_Employee_Type,"Co. State",
        		edw_Record_Edw_Comp_Addr_State,"Action",
        		pnd_Action);
        if (Global.isEscape()) return;
        pnd_Total_Loaded.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-LOADED
        //*  PRINT-PLAN
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"TAX WITHHOLDING AND REPORTING SYSTEM",new 
            TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,"RUNDATE :",Global.getDATU(),new TabSetting(50),"PLAN TABLE CONTROL REPORT",NEWLINE,"RUNTIME :",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),NEWLINE,NEWLINE,"TAX YEAR:",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Plan",
        		edw_Record_Edw_Plan,"Plan Name",
        		edw_Record_Edw_Plan_Name,"Emp Type",
        		edw_Record_Edw_Employee_Type,"Co. State",
        		edw_Record_Edw_Comp_Addr_State,"Action",
        		pnd_Action);
    }
}
