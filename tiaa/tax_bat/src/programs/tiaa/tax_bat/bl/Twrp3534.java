/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:36:00 PM
**        * FROM NATURAL PROGRAM : Twrp3534
************************************************************
**        * FILE NAME            : Twrp3534.java
**        * CLASS NAME           : Twrp3534
**        * INSTANCE NAME        : Twrp3534
************************************************************
************************************************************************
** PROGRAM : TWRP3534
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: 1099 IRS CORRECTION REPORTING - RECONCILIATION BETWEEN
**           'B', 'C', AND 'K' RECORDS.
** HISTORY.:
** 01/30/08: AY - REVISED TO ACCOMMODATE FORM 480.7C, WHICH REPLACES
**                FORMS 480.6A AND 480.6B AS OF TAX YEAR 2007.
**                REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
** 11/17/09: AY - RE-COMPILED TO ACCOMMODATE 2009 IRS CHANGES TO
**                'B', 'C', AND 'K' RECS.
** 10/01/10: AY - RE-COMPILED TO ACCOMMODATE 2010 IRS CHANGES TO 'A'
**                AND 'B' RECORDS.
** 04/04/19: VIKRAM - RESTOW COMPONENT TWRL3507
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3534 extends BLNatBase
{
    // Data Areas
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl3508 ldaTwrl3508;
    private LdaTwrl3321 ldaTwrl3321;
    private LdaTwrl3507 ldaTwrl3507;
    private LdaTwrl3323 ldaTwrl3323;
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3325 ldaTwrl3325;
    private PdaTwrafrmn pdaTwrafrmn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_State_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_A_Recs;
    private DbsField pnd_Ws_Pnd_A_Recs_Bal;
    private DbsField pnd_Ws_Pnd_C_Not_Processed;
    private DbsField pnd_Ws_Pnd_State_Idx;
    private DbsField pnd_Ws_Pnd_Form_Idx;
    private DbsField pnd_Ws_Pnd_Disp_Idx;
    private DbsField pnd_Ws_Pnd_Disp_Msg;
    private DbsField pnd_Ws_Pnd_Disp_Msg_1;
    private DbsField pnd_Ws_Pnd_Disp_Header;
    private DbsField pnd_Ws_Pnd_1099_Int_Header;

    private DbsGroup pnd_Ws_Pnd_B_Table;
    private DbsField pnd_Ws_Pnd_B_Amt;
    private DbsField pnd_Ws_Pnd_B_Cmb;
    private DbsField pnd_Ws_Pnd_Disp_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Bal;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_Tircntl_State_Full_Name;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_FormOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl3508 = new LdaTwrl3508();
        registerRecord(ldaTwrl3508);
        ldaTwrl3321 = new LdaTwrl3321();
        registerRecord(ldaTwrl3321);
        ldaTwrl3507 = new LdaTwrl3507();
        registerRecord(ldaTwrl3507);
        ldaTwrl3323 = new LdaTwrl3323();
        registerRecord(ldaTwrl3323);
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3325 = new LdaTwrl3325();
        registerRecord(ldaTwrl3325);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_State_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_State_Max", "#STATE-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_A_Recs = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_A_Recs", "#A-RECS", FieldType.NUMERIC, 8, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_A_Recs_Bal = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_A_Recs_Bal", "#A-RECS-BAL", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_C_Not_Processed = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_C_Not_Processed", "#C-NOT-PROCESSED", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_State_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Idx", "#STATE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Form_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Idx", "#FORM-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Disp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Idx", "#DISP-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Disp_Msg = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Msg", "#DISP-MSG", FieldType.STRING, 53);
        pnd_Ws_Pnd_Disp_Msg_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Msg_1", "#DISP-MSG-1", FieldType.STRING, 26);
        pnd_Ws_Pnd_Disp_Header = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Disp_Header", "#DISP-HEADER", FieldType.STRING, 22, new DbsArrayController(1, 
            16));
        pnd_Ws_Pnd_1099_Int_Header = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_1099_Int_Header", "#1099-INT-HEADER", FieldType.STRING, 22, new DbsArrayController(1, 
            16));

        pnd_Ws_Pnd_B_Table = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_B_Table", "#B-TABLE", new DbsArrayController(1, 101));
        pnd_Ws_Pnd_B_Amt = pnd_Ws_Pnd_B_Table.newFieldArrayInGroup("pnd_Ws_Pnd_B_Amt", "#B-AMT", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 
            16));
        pnd_Ws_Pnd_B_Cmb = pnd_Ws_Pnd_B_Table.newFieldInGroup("pnd_Ws_Pnd_B_Cmb", "#B-CMB", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Disp_Amt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Disp_Amt", "#DISP-AMT", FieldType.PACKED_DECIMAL, 18, 2, new DbsArrayController(1, 
            2, 1, 16));
        pnd_Ws_Pnd_Disp_Bal = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Disp_Bal", "#DISP-BAL", FieldType.BOOLEAN, 1, new DbsArrayController(1, 16));

        pnd_State_Table = localVariables.newGroupArrayInRecord("pnd_State_Table", "#STATE-TABLE", new DbsArrayController(1, 101));
        pnd_State_Table_Tircntl_State_Alpha_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_Tircntl_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", 
            FieldType.STRING, 19);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_FormOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Form_OLD", "Pnd_Form_OLD", FieldType.NUMERIC, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl3508.initializeValues();
        ldaTwrl3321.initializeValues();
        ldaTwrl3507.initializeValues();
        ldaTwrl3323.initializeValues();
        ldaTwrl3324.initializeValues();
        ldaTwrl3325.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_State_Max.setInitialValue(99);
        pnd_Ws_Pnd_C_Not_Processed.setInitialValue(true);
        pnd_Ws_Pnd_Disp_Header.getValue(1).setInitialValue("(1) Gross Amount");
        pnd_Ws_Pnd_Disp_Header.getValue(2).setInitialValue("(2) Taxable Amount");
        pnd_Ws_Pnd_Disp_Header.getValue(3).setInitialValue("(3)");
        pnd_Ws_Pnd_Disp_Header.getValue(4).setInitialValue("(4) Federal Tax");
        pnd_Ws_Pnd_Disp_Header.getValue(5).setInitialValue("(5) IVC");
        pnd_Ws_Pnd_Disp_Header.getValue(6).setInitialValue("(6)");
        pnd_Ws_Pnd_Disp_Header.getValue(7).setInitialValue("(7)");
        pnd_Ws_Pnd_Disp_Header.getValue(8).setInitialValue("(8)");
        pnd_Ws_Pnd_Disp_Header.getValue(9).setInitialValue("(9) Tot Contr. IVC");
        pnd_Ws_Pnd_Disp_Header.getValue(10).setInitialValue("(A) IRA Ditribution");
        pnd_Ws_Pnd_Disp_Header.getValue(11).setInitialValue("(B) IRR Amount");
        pnd_Ws_Pnd_Disp_Header.getValue(12).setInitialValue("(C)");
        pnd_Ws_Pnd_Disp_Header.getValue(13).setInitialValue("(D)");
        pnd_Ws_Pnd_Disp_Header.getValue(14).setInitialValue("(E)");
        pnd_Ws_Pnd_Disp_Header.getValue(15).setInitialValue("    State Tax");
        pnd_Ws_Pnd_Disp_Header.getValue(16).setInitialValue("    Local Tax");
        pnd_Ws_Pnd_1099_Int_Header.getValue(1).setInitialValue("(1) Interest Amount");
        pnd_Ws_Pnd_1099_Int_Header.getValue(2).setInitialValue("(2)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(3).setInitialValue("(3)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(4).setInitialValue("(4) Backup Withholding");
        pnd_Ws_Pnd_1099_Int_Header.getValue(5).setInitialValue("(5)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(6).setInitialValue("(6)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(7).setInitialValue("(7)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(8).setInitialValue("(8)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(9).setInitialValue("(9)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(10).setInitialValue("(A)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(11).setInitialValue("(B)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(12).setInitialValue("(C)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(13).setInitialValue("(D)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(14).setInitialValue("(E)");
        pnd_Ws_Pnd_1099_Int_Header.getValue(15).setInitialValue("    State Tax");
        pnd_Ws_Pnd_1099_Int_Header.getValue(16).setInitialValue("    Local Tax");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp3534() throws Exception
    {
        super("Twrp3534");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
            //*  01/30/08 - AY
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'IRS Correction Reconciliation' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 60T #TWRACOM2.#COMP-SHORT-NAME '-' #TWRAFRMN.#FORM-NAME ( #FORM-IDX ) / 55T 'Correction Indicator:' #TWRL3508.#CORR-IND /// 4T #DISP-MSG / 4T #DISP-MSG-1 //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'IRS Correction Reconciliation' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) //
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 IRS-B-REC-ARRAY ( * ) #TWRL3508
        while (condition(getWorkFiles().read(1, ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue("*"), ldaTwrl3508.getPnd_Twrl3508())))
        {
            short decideConditionsMet517 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF IRS-B-RECORD-TYPE;//Natural: VALUE 'A'
            if (condition((ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("A"))))
            {
                decideConditionsMet517++;
                pnd_Ws_Pnd_A_Recs.getValue(1).nadd(1);                                                                                                                    //Natural: ADD 1 TO #WS.#A-RECS ( 1 )
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("F"))))
            {
                decideConditionsMet517++;
                ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Move_1().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(1));                                            //Natural: ASSIGN IRS-F-MOVE-1 := IRS-B-REC-ARRAY ( 1 )
                pnd_Ws_Pnd_A_Recs.getValue(2).setValue(ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records());                                                        //Natural: ASSIGN #WS.#A-RECS ( 2 ) := IRS-F-NUMBER-OF-A-RECORDS
                pnd_Ws_Pnd_Tax_Year.setValueEdited(new ReportEditMask("9999"),ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year());                                                //Natural: MOVE EDITED #TWRL3508.#TAX-YEAR TO #WS.#TAX-YEAR ( EM = 9999 )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(!(ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("A") || ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("B") || ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("C")  //Natural: ACCEPT IF IRS-B-RECORD-TYPE = 'A' OR = 'B' OR = 'C' OR = 'K'
                || ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("K"))))
            {
                continue;
            }
            getSort().writeSortInData(ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind(),  //Natural: END-ALL
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Form(), ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State(), 
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num(), ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(1), ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(2), 
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(3));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind(),        //Natural: SORT BY #TWRL3508.#TAX-YEAR #TWRL3508.#COMPANY-CODE #TWRL3508.#CORR-IND #TWRL3508.#FORM IRS-B-RECORD-TYPE #TWRL3508.#IRS-K-STATE #TWRL3508.#IRS-B-STATE #TWRL3508.#SEQ-NUM USING IRS-B-REC-ARRAY ( * )
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Form(), ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State(), 
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num());
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind(), 
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Form(), ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State(), ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State(), 
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num(), ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(1), ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(2), 
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(3))))
        {
            CheckAtStartofData531();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            short decideConditionsMet563 = 0;                                                                                                                             //Natural: AT START OF DATA;//Natural: AT BREAK OF #TWRL3508.#COMPANY-CODE;//Natural: AT BREAK OF #TWRL3508.#FORM;//Natural: AT BREAK OF IRS-B-RECORD-TYPE;//Natural: DECIDE ON FIRST VALUE OF IRS-B-RECORD-TYPE;//Natural: VALUE 'B'
            if (condition((ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("B"))))
            {
                decideConditionsMet563++;
                //*     FEDERAL TOTALS
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,1).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #WS.#B-AMT ( -1,01 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,2).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #WS.#B-AMT ( -1,02 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,3).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_3());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-3 TO #WS.#B-AMT ( -1,03 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,4).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #WS.#B-AMT ( -1,04 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,5).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #WS.#B-AMT ( -1,05 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,6).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_6());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-6 TO #WS.#B-AMT ( -1,06 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,7).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_7());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-7 TO #WS.#B-AMT ( -1,07 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,8).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_8());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-8 TO #WS.#B-AMT ( -1,08 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,9).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());                                                                 //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #WS.#B-AMT ( -1,09 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,10).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());                                                                //Natural: ADD IRS-B-PAYMENT-AMT-A TO #WS.#B-AMT ( -1,10 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,11).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_B());                                                                //Natural: ADD IRS-B-PAYMENT-AMT-B TO #WS.#B-AMT ( -1,11 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,12).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_C());                                                                //Natural: ADD IRS-B-PAYMENT-AMT-C TO #WS.#B-AMT ( -1,12 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,13).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_D());                                                                //Natural: ADD IRS-B-PAYMENT-AMT-D TO #WS.#B-AMT ( -1,13 )
                pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,14).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_E());                                                                //Natural: ADD IRS-B-PAYMENT-AMT-E TO #WS.#B-AMT ( -1,14 )
                if (condition(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().notEquals(0)))                                                                                //Natural: IF #TWRL3508.#IRS-B-STATE NE 00
                {
                    pnd_Ws_Pnd_B_Cmb.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2).setValue(true);                                                 //Natural: ASSIGN #WS.#B-CMB ( #IRS-B-STATE ) := TRUE
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,1).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());         //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #WS.#B-AMT ( #IRS-B-STATE,01 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,2).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());         //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #WS.#B-AMT ( #IRS-B-STATE,02 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,3).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_3());         //Natural: ADD IRS-B-PAYMENT-AMT-3 TO #WS.#B-AMT ( #IRS-B-STATE,03 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,4).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());         //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #WS.#B-AMT ( #IRS-B-STATE,04 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,5).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());         //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #WS.#B-AMT ( #IRS-B-STATE,05 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,6).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_6());         //Natural: ADD IRS-B-PAYMENT-AMT-6 TO #WS.#B-AMT ( #IRS-B-STATE,06 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,7).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_7());         //Natural: ADD IRS-B-PAYMENT-AMT-7 TO #WS.#B-AMT ( #IRS-B-STATE,07 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,8).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_8());         //Natural: ADD IRS-B-PAYMENT-AMT-8 TO #WS.#B-AMT ( #IRS-B-STATE,08 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,9).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());         //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #WS.#B-AMT ( #IRS-B-STATE,09 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,10).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());        //Natural: ADD IRS-B-PAYMENT-AMT-A TO #WS.#B-AMT ( #IRS-B-STATE,10 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,11).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_B());        //Natural: ADD IRS-B-PAYMENT-AMT-B TO #WS.#B-AMT ( #IRS-B-STATE,11 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,12).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_C());        //Natural: ADD IRS-B-PAYMENT-AMT-C TO #WS.#B-AMT ( #IRS-B-STATE,12 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,13).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_D());        //Natural: ADD IRS-B-PAYMENT-AMT-D TO #WS.#B-AMT ( #IRS-B-STATE,13 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,14).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_E());        //Natural: ADD IRS-B-PAYMENT-AMT-E TO #WS.#B-AMT ( #IRS-B-STATE,14 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,15).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num());        //Natural: ADD IRS-B-STATE-TAX-NUM TO #WS.#B-AMT ( #IRS-B-STATE,15 )
                    pnd_Ws_Pnd_B_Amt.getValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().getInt() + 2,16).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num());        //Natural: ADD IRS-B-LOCAL-TAX-NUM TO #WS.#B-AMT ( #IRS-B-STATE,16 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("C"))))
            {
                decideConditionsMet563++;
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_1().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(1));                                            //Natural: ASSIGN IRS-C-MOVE-1 := IRS-B-REC-ARRAY ( 1 )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_2().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(2));                                            //Natural: ASSIGN IRS-C-MOVE-2 := IRS-B-REC-ARRAY ( 2 )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Move_3().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(3));                                            //Natural: ASSIGN IRS-C-MOVE-3 := IRS-B-REC-ARRAY ( 3 )
                pnd_Ws_Pnd_Disp_Amt.getValue(2,1).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,1 ) := IRS-C-CONTROL-TOTAL-1
                pnd_Ws_Pnd_Disp_Amt.getValue(2,2).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,2 ) := IRS-C-CONTROL-TOTAL-2
                pnd_Ws_Pnd_Disp_Amt.getValue(2,3).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_3());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,3 ) := IRS-C-CONTROL-TOTAL-3
                pnd_Ws_Pnd_Disp_Amt.getValue(2,4).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,4 ) := IRS-C-CONTROL-TOTAL-4
                pnd_Ws_Pnd_Disp_Amt.getValue(2,5).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,5 ) := IRS-C-CONTROL-TOTAL-5
                pnd_Ws_Pnd_Disp_Amt.getValue(2,6).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_6());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,6 ) := IRS-C-CONTROL-TOTAL-6
                pnd_Ws_Pnd_Disp_Amt.getValue(2,7).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_7());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,7 ) := IRS-C-CONTROL-TOTAL-7
                pnd_Ws_Pnd_Disp_Amt.getValue(2,8).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_8());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,8 ) := IRS-C-CONTROL-TOTAL-8
                pnd_Ws_Pnd_Disp_Amt.getValue(2,9).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,9 ) := IRS-C-CONTROL-TOTAL-9
                pnd_Ws_Pnd_Disp_Amt.getValue(2,10).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_A());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,10 ) := IRS-C-CONTROL-TOTAL-A
                pnd_Ws_Pnd_Disp_Amt.getValue(2,11).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,11 ) := IRS-C-CONTROL-TOTAL-B
                pnd_Ws_Pnd_Disp_Amt.getValue(2,12).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_C());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,12 ) := IRS-C-CONTROL-TOTAL-C
                pnd_Ws_Pnd_Disp_Amt.getValue(2,13).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_D());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,13 ) := IRS-C-CONTROL-TOTAL-D
                pnd_Ws_Pnd_Disp_Amt.getValue(2,14).setValue(ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_E());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,14 ) := IRS-C-CONTROL-TOTAL-E
                                                                                                                                                                          //Natural: PERFORM PROCESS-C
                sub_Process_C();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("K"))))
            {
                decideConditionsMet563++;
                ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_1().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(1));                                            //Natural: ASSIGN IRS-K-MOVE-1 := IRS-B-REC-ARRAY ( 1 )
                ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_2().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(2));                                            //Natural: ASSIGN IRS-K-MOVE-2 := IRS-B-REC-ARRAY ( 2 )
                ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Move_3().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Rec_Array().getValue(3));                                            //Natural: ASSIGN IRS-K-MOVE-3 := IRS-B-REC-ARRAY ( 3 )
                pnd_Ws_Pnd_State_Idx.setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State());                                                                             //Natural: ASSIGN #WS.#STATE-IDX := #IRS-K-STATE
                pnd_Ws_Pnd_Disp_Amt.getValue(2,1).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,1 ) := IRS-K-CONTROL-TOTAL-1
                pnd_Ws_Pnd_Disp_Amt.getValue(2,2).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,2 ) := IRS-K-CONTROL-TOTAL-2
                pnd_Ws_Pnd_Disp_Amt.getValue(2,3).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,3 ) := IRS-K-CONTROL-TOTAL-3
                pnd_Ws_Pnd_Disp_Amt.getValue(2,4).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,4 ) := IRS-K-CONTROL-TOTAL-4
                pnd_Ws_Pnd_Disp_Amt.getValue(2,5).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,5 ) := IRS-K-CONTROL-TOTAL-5
                pnd_Ws_Pnd_Disp_Amt.getValue(2,6).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,6 ) := IRS-K-CONTROL-TOTAL-6
                pnd_Ws_Pnd_Disp_Amt.getValue(2,7).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,7 ) := IRS-K-CONTROL-TOTAL-7
                pnd_Ws_Pnd_Disp_Amt.getValue(2,8).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_8());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,8 ) := IRS-K-CONTROL-TOTAL-8
                pnd_Ws_Pnd_Disp_Amt.getValue(2,9).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9());                                                        //Natural: ASSIGN #WS.#DISP-AMT ( 2,9 ) := IRS-K-CONTROL-TOTAL-9
                pnd_Ws_Pnd_Disp_Amt.getValue(2,10).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,10 ) := IRS-K-CONTROL-TOTAL-A
                pnd_Ws_Pnd_Disp_Amt.getValue(2,11).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,11 ) := IRS-K-CONTROL-TOTAL-B
                pnd_Ws_Pnd_Disp_Amt.getValue(2,12).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_C());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,12 ) := IRS-K-CONTROL-TOTAL-C
                pnd_Ws_Pnd_Disp_Amt.getValue(2,13).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_D());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,13 ) := IRS-K-CONTROL-TOTAL-D
                pnd_Ws_Pnd_Disp_Amt.getValue(2,14).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_E());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,14 ) := IRS-K-CONTROL-TOTAL-E
                pnd_Ws_Pnd_Disp_Amt.getValue(2,15).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,15 ) := IRS-K-ST-TAX-WITHHELD
                pnd_Ws_Pnd_Disp_Amt.getValue(2,16).setValue(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld());                                                       //Natural: ASSIGN #WS.#DISP-AMT ( 2,16 ) := IRS-K-LC-TAX-WITHHELD
                                                                                                                                                                          //Natural: PERFORM PROCESS-K
                sub_Process_K();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  DISPLAY
            //*    #TWRL3508.#TAX-YEAR
            //*    #TWRL3508.#COMPANY-CODE
            //*    #TWRL3508.#CORR-IND
            //*    #TWRL3508.#FORM
            //*    IRS-B-RECORD-TYPE
            //*    #TWRL3508.#IRS-K-STATE
            //*    #TWRL3508.#IRS-B-STATE
            //*    #TWRL3508.#SEQ-NUM
            sort01Pnd_FormOld.setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Form());                                                                                           //Natural: AT END OF DATA;//Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            if (condition(pnd_Ws_Pnd_C_Not_Processed.getBoolean()))                                                                                                       //Natural: IF #WS.#C-NOT-PROCESSED
            {
                pnd_Ws_Pnd_Disp_Msg_1.setValue("*** Missing 'C' record ***");                                                                                             //Natural: ASSIGN #WS.#DISP-MSG-1 := '*** Missing "C" record ***'
                pnd_Ws_Pnd_Disp_Amt.getValue(2,"*").reset();                                                                                                              //Natural: RESET #WS.#DISP-AMT ( 2,* )
                                                                                                                                                                          //Natural: PERFORM PROCESS-C
                sub_Process_C();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Disp_Msg_1.setValue("*** Missing 'K' record ***");                                                                                                 //Natural: ASSIGN #WS.#DISP-MSG-1 := '*** Missing "K" record ***'
            FOR02:                                                                                                                                                        //Natural: FOR #WS.#STATE-IDX = 0 TO #STATE-MAX
            for (pnd_Ws_Pnd_State_Idx.setValue(0); condition(pnd_Ws_Pnd_State_Idx.lessOrEqual(pnd_Ws_Const_Pnd_State_Max)); pnd_Ws_Pnd_State_Idx.nadd(1))
            {
                if (condition(pnd_Ws_Pnd_B_Cmb.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2).getBoolean()))                                                                 //Natural: IF #WS.#B-CMB ( #STATE-IDX )
                {
                    pnd_Ws_Pnd_Disp_Amt.getValue(2,"*").reset();                                                                                                          //Natural: RESET #WS.#DISP-AMT ( 2,* )
                                                                                                                                                                          //Natural: PERFORM PROCESS-K
                    sub_Process_K();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-A-RECS
            sub_Print_A_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //* **********************
        //*  S U B R O U T I N E S
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PAGE
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-K
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-C
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-BREAK
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-BREAK
        //* ******************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-A-RECS
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-AND-FORM-NAME-TABLES
        //* ************************************************
    }
    private void sub_Print_Page() throws Exception                                                                                                                        //Natural: PRINT-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO #WS.#DISP-IDX
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Pnd_Disp_Idx)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Disp_Bal.getValue(pnd_Ws_Pnd_I).reset();                                                                                                           //Natural: RESET #WS.#DISP-BAL ( #I )
            if (condition(pnd_Ws_Pnd_Disp_Msg_1.equals(" ") && pnd_Ws_Pnd_Disp_Amt.getValue(1,pnd_Ws_Pnd_I).equals(pnd_Ws_Pnd_Disp_Amt.getValue(2,pnd_Ws_Pnd_I))))        //Natural: IF #WS.#DISP-MSG-1 = ' ' AND #WS.#DISP-AMT ( 1,#I ) = #WS.#DISP-AMT ( 2,#I )
            {
                pnd_Ws_Pnd_Disp_Bal.getValue(pnd_Ws_Pnd_I).setValue(true);                                                                                                //Natural: ASSIGN #WS.#DISP-BAL ( #I ) := TRUE
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "/",                                                                                                                                  //Natural: DISPLAY ( 1 ) '/' #WS.#DISP-HEADER ( #I ) 'Detail Amount' #WS.#DISP-AMT ( 1,#I ) 'Trailer Amount' #WS.#DISP-AMT ( 2,#I ) 'Balanced' #WS.#DISP-BAL ( #I )
            		pnd_Ws_Pnd_Disp_Header.getValue(pnd_Ws_Pnd_I),"Detail Amount",
            		pnd_Ws_Pnd_Disp_Amt.getValue(1,pnd_Ws_Pnd_I),"Trailer Amount",
            		pnd_Ws_Pnd_Disp_Amt.getValue(2,pnd_Ws_Pnd_I),"Balanced",
            		pnd_Ws_Pnd_Disp_Bal.getValue(pnd_Ws_Pnd_I).getBoolean());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_K() throws Exception                                                                                                                         //Natural: PROCESS-K
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Ws_Pnd_B_Cmb.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2).reset();                                                                                             //Natural: RESET #WS.#B-CMB ( #STATE-IDX )
        pnd_Ws_Pnd_Disp_Msg.setValue(DbsUtil.compress("'B' records vs. 'K' record", "-", pnd_State_Table_Tircntl_State_Alpha_Code.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2),  //Natural: COMPRESS '"B" records vs. "K" record' '-' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ( #STATE-IDX ) '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME ( #STATE-IDX ) INTO #WS.#DISP-MSG
            "-", pnd_State_Table_Tircntl_State_Full_Name.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2)));
        pnd_Ws_Pnd_Disp_Idx.setValue(16);                                                                                                                                 //Natural: ASSIGN #WS.#DISP-IDX := 16
        pnd_Ws_Pnd_Disp_Amt.getValue(1,"*").setValue(pnd_Ws_Pnd_B_Amt.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2,"*"));                                                   //Natural: ASSIGN #WS.#DISP-AMT ( 1,* ) := #WS.#B-AMT ( #STATE-IDX,* )
                                                                                                                                                                          //Natural: PERFORM PRINT-PAGE
        sub_Print_Page();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Process_C() throws Exception                                                                                                                         //Natural: PROCESS-C
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        pnd_Ws_Pnd_C_Not_Processed.reset();                                                                                                                               //Natural: RESET #WS.#C-NOT-PROCESSED
        pnd_Ws_Pnd_Disp_Msg.setValue("'B' records vs. 'C' record");                                                                                                       //Natural: ASSIGN #WS.#DISP-MSG := '"B" records vs. "C" record'
        pnd_Ws_Pnd_Disp_Idx.setValue(14);                                                                                                                                 //Natural: ASSIGN #WS.#DISP-IDX := 14
        pnd_Ws_Pnd_Disp_Amt.getValue(1,"*").setValue(pnd_Ws_Pnd_B_Amt.getValue(-1 + 2,"*"));                                                                              //Natural: ASSIGN #WS.#DISP-AMT ( 1,* ) := #WS.#B-AMT ( -1,* )
                                                                                                                                                                          //Natural: PERFORM PRINT-PAGE
        sub_Print_Page();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Form_Break() throws Exception                                                                                                                        //Natural: FORM-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(pnd_Ws_Pnd_Form_Idx.equals(1)))                                                                                                                     //Natural: IF #WS.#FORM-IDX = 1
        {
            pnd_Ws_Pnd_Disp_Header.getValue("*").resetInitial();                                                                                                          //Natural: RESET INITIAL #WS.#DISP-HEADER ( * )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Disp_Header.getValue("*").setValue(pnd_Ws_Pnd_1099_Int_Header.getValue("*"));                                                                      //Natural: MOVE #1099-INT-HEADER ( * ) TO #WS.#DISP-HEADER ( * )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Break() throws Exception                                                                                                                     //Natural: COMPANY-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code());                                                             //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #TWRL3508.#COMPANY-CODE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Form());                                                                     //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := #TWRL3508.#FORM
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Print_A_Recs() throws Exception                                                                                                                      //Natural: PRINT-A-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().definePrinter(3, "'CMPRT01'");                                                                                                                       //Natural: DEFINE PRINTER ( 02 ) OUTPUT 'CMPRT01'
        if (condition(pnd_Ws_Pnd_A_Recs.getValue(1).equals(pnd_Ws_Pnd_A_Recs.getValue(2))))                                                                               //Natural: IF #WS.#A-RECS ( 1 ) = #WS.#A-RECS ( 2 )
        {
            pnd_Ws_Pnd_A_Recs_Bal.setValue(true);                                                                                                                         //Natural: ASSIGN #A-RECS-BAL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(2, "A records/on file",                                                                                                                      //Natural: DISPLAY ( 2 ) 'A records/on file' #WS.#A-RECS ( 1 ) 'A records/on "F" Record' #WS.#A-RECS ( 2 ) '/Balanced' #A-RECS-BAL
        		pnd_Ws_Pnd_A_Recs.getValue(1),"A records/on 'F' Record",
        		pnd_Ws_Pnd_A_Recs.getValue(2),"/Balanced",
        		pnd_Ws_Pnd_A_Recs_Bal.getBoolean());
        if (Global.isEscape()) return;
    }
    //*  01/30/08 - AY
    //*  01/30/08 - AY
    private void sub_Load_State_And_Form_Name_Tables() throws Exception                                                                                                   //Natural: LOAD-STATE-AND-FORM-NAME-TABLES
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                             //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().reset();                                                                                                                  //Natural: RESET TWRATBL2.#ABEND-IND TWRATBL2.#DISPLAY-IND
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().reset();
        FOR04:                                                                                                                                                            //Natural: FOR #I = 0 TO #STATE-MAX
        for (pnd_Ws_Pnd_I.setValue(0); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Const_Pnd_State_Max)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                               //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                pnd_State_Table.getValue(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr().getInt() + 2).setValuesByName(pdaTwratbl2.getTwratbl2());                              //Natural: MOVE BY NAME TWRATBL2 TO #STATE-TABLE ( TIRCNTL-SEQ-NBR )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  01/30/08 - AY
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl3508_getPnd_Twrl3508_Pnd_Company_CodeIsBreak = ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().isBreak(endOfData);
        boolean ldaTwrl3508_getPnd_Twrl3508_Pnd_FormIsBreak = ldaTwrl3508.getPnd_Twrl3508_Pnd_Form().isBreak(endOfData);
        boolean ldaTwrl3507_getIrs_B_Rec_Irs_B_Record_TypeIsBreak = ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().isBreak(endOfData);
        if (condition(ldaTwrl3508_getPnd_Twrl3508_Pnd_Company_CodeIsBreak || ldaTwrl3508_getPnd_Twrl3508_Pnd_FormIsBreak || ldaTwrl3507_getIrs_B_Rec_Irs_B_Record_TypeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
            sub_Company_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl3508_getPnd_Twrl3508_Pnd_FormIsBreak || ldaTwrl3507_getIrs_B_Rec_Irs_B_Record_TypeIsBreak))
        {
            pnd_Ws_Pnd_Form_Idx.setValue(sort01Pnd_FormOld);                                                                                                              //Natural: ASSIGN #WS.#FORM-IDX := OLD ( #TWRL3508.#FORM )
                                                                                                                                                                          //Natural: PERFORM FORM-BREAK
            sub_Form_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl3507_getIrs_B_Rec_Irs_B_Record_TypeIsBreak))
        {
            if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_Record_Type().equals("A")))                                                                                      //Natural: IF IRS-B-RECORD-TYPE = 'A'
            {
                if (condition(pnd_Ws_Pnd_C_Not_Processed.getBoolean()))                                                                                                   //Natural: IF #WS.#C-NOT-PROCESSED
                {
                    pnd_Ws_Pnd_Disp_Msg_1.setValue("*** Missing 'C' record ***");                                                                                         //Natural: ASSIGN #WS.#DISP-MSG-1 := '*** Missing "C" record ***'
                    pnd_Ws_Pnd_Disp_Amt.getValue(2,"*").reset();                                                                                                          //Natural: RESET #WS.#DISP-AMT ( 2,* )
                                                                                                                                                                          //Natural: PERFORM PROCESS-C
                    sub_Process_C();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_C_Not_Processed.setValue(true);                                                                                                                //Natural: ASSIGN #WS.#C-NOT-PROCESSED := TRUE
                pnd_Ws_Pnd_Disp_Msg_1.setValue("*** Missing 'K' record ***");                                                                                             //Natural: ASSIGN #WS.#DISP-MSG-1 := '*** Missing "K" record ***'
                FOR01:                                                                                                                                                    //Natural: FOR #WS.#STATE-IDX = 0 TO #STATE-MAX
                for (pnd_Ws_Pnd_State_Idx.setValue(0); condition(pnd_Ws_Pnd_State_Idx.lessOrEqual(pnd_Ws_Const_Pnd_State_Max)); pnd_Ws_Pnd_State_Idx.nadd(1))
                {
                    if (condition(pnd_Ws_Pnd_B_Cmb.getValue(pnd_Ws_Pnd_State_Idx.getInt() + 2).getBoolean()))                                                             //Natural: IF #WS.#B-CMB ( #STATE-IDX )
                    {
                        pnd_Ws_Pnd_Disp_Amt.getValue(2,"*").reset();                                                                                                      //Natural: RESET #WS.#DISP-AMT ( 2,* )
                                                                                                                                                                          //Natural: PERFORM PROCESS-K
                        sub_Process_K();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape())) return;
                pnd_Ws_Pnd_B_Table.getValue("*").reset();                                                                                                                 //Natural: RESET #WS.#B-TABLE ( * ) #WS.#DISP-MSG-1
                pnd_Ws_Pnd_Disp_Msg_1.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"IRS Correction Reconciliation",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(60),pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),"-",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,new 
            TabSetting(55),"Correction Indicator:",ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind(),NEWLINE,NEWLINE,NEWLINE,new TabSetting(4),pnd_Ws_Pnd_Disp_Msg,NEWLINE,new 
            TabSetting(4),pnd_Ws_Pnd_Disp_Msg_1,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"IRS Correction Reconciliation",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "/",
        		pnd_Ws_Pnd_Disp_Header,"Detail Amount",
        		pnd_Ws_Pnd_Disp_Amt,"Trailer Amount",
        		pnd_Ws_Pnd_Disp_Amt,"Balanced",
        		pnd_Ws_Pnd_Disp_Bal.getBoolean());
        getReports().setDisplayColumns(2, "A records/on file",
        		pnd_Ws_Pnd_A_Recs,"A records/on 'F' Record",
        		pnd_Ws_Pnd_A_Recs,"/Balanced",
        		pnd_Ws_Pnd_A_Recs_Bal.getBoolean());
    }
    private void CheckAtStartofData531() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            //*  01/30/08 - AY
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-AND-FORM-NAME-TABLES
            sub_Load_State_And_Form_Name_Tables();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COMPANY-BREAK
            sub_Company_Break();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Form_Idx.setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Form());                                                                                         //Natural: ASSIGN #WS.#FORM-IDX := #TWRL3508.#FORM
                                                                                                                                                                          //Natural: PERFORM FORM-BREAK
            sub_Form_Break();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
