/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:08 PM
**        * FROM NATURAL PROGRAM : Twrp3310
************************************************************
**        * FILE NAME            : Twrp3310.java
**        * CLASS NAME           : Twrp3310
**        * INSTANCE NAME        : Twrp3310
************************************************************
********************************************************************
*                                                                  *
*      PROJECT  - TWARS
*                 IRS ORIGINAL MAGNETIC MEDIA REPORTING            *
*                                                                  *
*      TWRP3310 - THIS IS THE MAIN PROGRAM TO PRODUCE IRS TAPE     *
*                 MAILING REPORT.                                  *
*                                                                  *
* HISTORY: 07/2002  J.ROTHOLZ - ADD LOGIC FOR TCII
*          08/13/02 J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN
*                   #COMP-NAME LENGTH IN TWRLCOMP & TWRACOMP
*          11/24/04 D. APRIL - ADDED NEW INPUT PARM: #F-OUT-TAPE   *
*          11/24/04 D. APRIL - MODIFIED CODE TO OUTPUT TOTALS FOR  *
*                              TIAA AND TRUST COMPANIES.           *
*          04/19/2005 - MS -   REMOVED UNUSED PROVINCE TABLE       *
*          11-28-05   - AY -   CHARLOTTE, NC COMPANY ADDRESS.      *
*          10/12/06   - RM -   UPDATE LDAS TWRL3322 & TWRL3326 FOR *
*                              2006 IRS CHANGES.                   *
*                              UPDATE THIS PGM TO POPULATE NEW AND *
*                              CHANGED FIELDS.                     *
*          12/05/06   - JR -   FIX ACCUM. FOR NEGATIVE AMOUNTS     *
*          03-06-2007 - AY -   REVISED TO CALL NAME CONTROL SUB-   *
*          05-01-2007          ROUTINE 'TWRN5052' TO POPULATE IRS  *
*                              FIELD IRS-B-NAME-CONTROL.           *
*          11-13-2008 - AY -   REVISED TO ADD ENVIRONMENT ROUTINE  *
*                              TO AUTOMATE TEST IND POPULATION.    *
*          11-17-2009 - AY -   RE-COMPILED TO ACCOMMODATE 2009 IRS *
*                              CHANGES TO 'B', 'C', AND 'K' RECS.  *
*          10-01-2010 - AY -   RE-COMPILED TO ACCOMMODATE 2010 IRS *
*                              CHANGES TO 'A' AND 'B' RECORDS.     *
*                              IRS-A-TYPE-OF-RETURN = '9 ' 1099-R  *
*                                                   = '6 ' 1099-INT*
*          10-20-2011 - DY -   ADD NEW COMPANY CODE "F" FOR        *
*                              TIAA-CREF TRUST COMPANY,FSB;        *
*                              REF CHG AS DY1          (D. YHUN)   *
*          10-30-2011 - RC -   COMPLIANCE REPORTING                *
*          12-10-2012 - JB -   INCREASE REPORT DISPLAY FLDS        *
*                              BECAUSE OF TRUNCATION   JB01        *
*          11-27-2013 - RC -   CHANGES IN TWRL3300                 *
*          02-20-2015 - DUTTAD MODIFIED THE PROGRAM TO CORRECT THE *
*                              DATA TRUNCATION ISSUE IN TRUST      *
*                              REPORTS.                            *
*                              CHANGES ARE TAGGED WITH 'duttad'.   *
*          12-11-2017 - SNJY - RESTOW FOR CHANGES IN TWRL3322      *
*          10-05-2018 - ARIVU - EIN CHANGE - TAG: EINCHG           *
*          10-10-2018  VIKRAM - PAYMENT DATE ADDED FOR DISTRIBUTION *
*                               CODE AS "DC" ONLY IN IRS B RECORDS *
*                       TAGGING : VIKRAM                           *
*          11-03-2018  VIKRAM - STATE ID ADDED FOR STATE MD & NM   *
*                       TAGGING : VIKRAM                           *
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3310 extends BLNatBase
{
    // Data Areas
    private PdaTwra0001 pdaTwra0001;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwra5052 pdaTwra5052;
    private LdaTwrl3300 ldaTwrl3300;
    private LdaTwrl3321 ldaTwrl3321;
    private LdaTwrl3322 ldaTwrl3322;
    private LdaTwrl3323 ldaTwrl3323;
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3325 ldaTwrl3325;
    private LdaTwrl3326 ldaTwrl3326;
    private PdaTwradist pdaTwradist;
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Passed_Namaddr;
    private DbsField pnd_Passed_Namaddr_Pnd_Passed_Line;

    private DbsGroup pnd_Irs_Data;
    private DbsField pnd_Irs_Data_Pnd_Irs_Name_1;
    private DbsField pnd_Irs_Data_Pnd_Irs_Name_2;
    private DbsField pnd_Irs_Data_Pnd_Irs_Address;
    private DbsField pnd_Irs_Data_Pnd_Irs_City;
    private DbsField pnd_Irs_Data_Pnd_Irs_State;
    private DbsField pnd_Irs_Data_Pnd_Irs_Zip;
    private DbsField contr_Tot_1;
    private DbsField contr_Tot_2;
    private DbsField contr_Tot_3;
    private DbsField contr_Tot_4;
    private DbsField contr_Tot_5;
    private DbsField contr_Tot_6;
    private DbsField contr_Tot_7;
    private DbsField contr_Tot_8;
    private DbsField contr_Tot_9;
    private DbsField contr_Tot_10;
    private DbsField contr_Tot_11;
    private DbsField contr_Tot_12;
    private DbsField contr_Tot_13;
    private DbsField contr_Tot_B;
    private DbsField pnd_Max_States;
    private DbsField state_Name;
    private DbsField state_Alpha;
    private DbsField state_Index;

    private DbsGroup state_Totals;
    private DbsField state_Totals_State_Code;
    private DbsField state_Totals_St_B_Rec_Ctr;
    private DbsField state_Totals_State_Tot_1;
    private DbsField state_Totals_State_Tot_2;
    private DbsField state_Totals_State_Tot_3;
    private DbsField state_Totals_State_Tot_4;
    private DbsField state_Totals_State_Tot_5;
    private DbsField state_Totals_State_Tot_6;
    private DbsField state_Totals_State_Tot_7;
    private DbsField state_Totals_State_Tot_8;
    private DbsField state_Totals_State_Tot_9;
    private DbsField state_Totals_State_Tot_10;
    private DbsField state_Totals_State_Tot_11;
    private DbsField state_Totals_State_Tot_12;
    private DbsField state_Totals_State_Tot_13;
    private DbsField state_Totals_State_Tot_B;

    private DbsGroup local_Totals;
    private DbsField local_Totals_Local_Code;
    private DbsField local_Totals_Lc_B_Rec_Ctr;
    private DbsField local_Totals_Local_Tot_1;
    private DbsField pnd_K;
    private DbsField pnd_Ws_Save_Prdct_Name;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Ws_Contract_8;
    private DbsField cntr_Cref_Ira;
    private DbsField cntr_Tiaa_Ira;
    private DbsField cntr_Tcii_Ira;
    private DbsField cntr_Fsbc_Ira;
    private DbsField cntr_Trst_Ira;
    private DbsField a_Record_Ctr;
    private DbsField b_Record_Ctr;
    private DbsField b_Recs_Written;
    private DbsField c_Record_Ctr;
    private DbsField f_Record_Ctr;
    private DbsField k_Record_Ctr;
    private DbsField t_Record_Ctr;
    private DbsField pnd_Ws_Rejects;
    private DbsField pnd_Ws_Rejects_File;
    private DbsField pnd_First_Rec_Sw;
    private DbsField pnd_Save_Prod_Ind;
    private DbsField pnd_Save_Type_Code;
    private DbsField pnd_Ws_Split_Field;

    private DbsGroup pnd_Ws_Split_Field__R_Field_1;
    private DbsField pnd_Ws_Split_Field_Pnd_Ws_Split_1;
    private DbsField pnd_Ws_Split_Field_Pnd_Ws_Split_2;
    private DbsField pnd_Work_Amt;
    private DbsField pnd_T_C_Title;
    private DbsField pnd_Cref_Int_Ctr;
    private DbsField pnd_Cref_Int_Gross;
    private DbsField pnd_Cref_Int_Income;
    private DbsField pnd_Cref_Int_Ivc;
    private DbsField pnd_Cref_Int_Tax;
    private DbsField pnd_Cref_Int_St_Tax;
    private DbsField pnd_Cref_Int_Reported;
    private DbsField pnd_Cref_Int_Miss_Tin;
    private DbsField pnd_Cref_Int_Miss_Nad;
    private DbsField pnd_Cref_Int_Miss_Hld;
    private DbsField pnd_Cref_Int_Tot_Hold;
    private DbsField pnd_Cref_R___Ctr;
    private DbsField pnd_Cref_R___Gross;
    private DbsField pnd_Cref_R___Income;
    private DbsField pnd_Cref_R___Ivc;
    private DbsField pnd_Cref_R___Tax;
    private DbsField pnd_Cref_R___St_Tax;
    private DbsField pnd_Cref_R___Lc_Tax;
    private DbsField pnd_Cref_R___9b_Amt;
    private DbsField pnd_Cref_R___Reported;
    private DbsField pnd_Cref_R___Miss_Tin;
    private DbsField pnd_Cref_R___Miss_Nad;
    private DbsField pnd_Cref_R___Miss_Hld;
    private DbsField pnd_Cref_R___Tot_Hold;
    private DbsField pnd_Cref_Tot_Ctr;
    private DbsField pnd_Cref_Tot_Gross;
    private DbsField pnd_Cref_Tot_Income;
    private DbsField pnd_Cref_Tot_Ivc;
    private DbsField pnd_Cref_Tot_Tax;
    private DbsField pnd_Cref_Tot_St_Tax;
    private DbsField pnd_Cref_Tot_Lc_Tax;
    private DbsField pnd_Cref_Tot_9b_Amt;
    private DbsField pnd_Cref_Tot_Reported;
    private DbsField pnd_Cref_Tot_Miss_Tin;
    private DbsField pnd_Cref_Tot_Miss_Nad;
    private DbsField pnd_Cref_Tot_Miss_Hld;
    private DbsField pnd_Cref_Tot_Tot_Hold;
    private DbsField pnd_Life_Int_Ctr;
    private DbsField pnd_Life_Int_Gross;
    private DbsField pnd_Life_Int_Income;
    private DbsField pnd_Life_Int_Ivc;
    private DbsField pnd_Life_Int_Tax;
    private DbsField pnd_Life_Int_St_Tax;
    private DbsField pnd_Life_Int_Reported;
    private DbsField pnd_Life_Int_Miss_Tin;
    private DbsField pnd_Life_Int_Miss_Nad;
    private DbsField pnd_Life_Int_Miss_Hld;
    private DbsField pnd_Life_Int_Tot_Hold;
    private DbsField pnd_Life_R___Ctr;
    private DbsField pnd_Life_R___Gross;
    private DbsField pnd_Life_R___Income;
    private DbsField pnd_Life_R___Ivc;
    private DbsField pnd_Life_R___Tax;
    private DbsField pnd_Life_R___St_Tax;
    private DbsField pnd_Life_R___Lc_Tax;
    private DbsField pnd_Life_R___9b_Amt;
    private DbsField pnd_Life_R___Reported;
    private DbsField pnd_Life_R___Miss_Tin;
    private DbsField pnd_Life_R___Miss_Nad;
    private DbsField pnd_Life_R___Miss_Hld;
    private DbsField pnd_Life_R___Tot_Hold;
    private DbsField pnd_Life_Tot_Ctr;
    private DbsField pnd_Life_Tot_Gross;
    private DbsField pnd_Life_Tot_Income;
    private DbsField pnd_Life_Tot_Ivc;
    private DbsField pnd_Life_Tot_Tax;
    private DbsField pnd_Life_Tot_St_Tax;
    private DbsField pnd_Life_Tot_Lc_Tax;
    private DbsField pnd_Life_Tot_9b_Tax;
    private DbsField pnd_Life_Tot_Reported;
    private DbsField pnd_Life_Tot_Miss_Tin;
    private DbsField pnd_Life_Tot_Miss_Nad;
    private DbsField pnd_Life_Tot_Miss_Hld;
    private DbsField pnd_Life_Tot_Tot_Hold;
    private DbsField pnd_Tiaa_Int_Ctr;
    private DbsField pnd_Tiaa_Int_Gross;
    private DbsField pnd_Tiaa_Int_Income;
    private DbsField pnd_Tiaa_Int_Ivc;
    private DbsField pnd_Tiaa_Int_Tax;
    private DbsField pnd_Tiaa_Int_St_Tax;
    private DbsField pnd_Tiaa_Int_Reported;
    private DbsField pnd_Tiaa_Int_Miss_Tin;
    private DbsField pnd_Tiaa_Int_Miss_Nad;
    private DbsField pnd_Tiaa_Int_Miss_Hld;
    private DbsField pnd_Tiaa_Int_Tot_Hold;
    private DbsField pnd_Tiaa_R___Ctr;
    private DbsField pnd_Tiaa_R___Gross;
    private DbsField pnd_Tiaa_R___Income;
    private DbsField pnd_Tiaa_R___Ivc;
    private DbsField pnd_Tiaa_R___Tax;
    private DbsField pnd_Tiaa_R___St_Tax;
    private DbsField pnd_Tiaa_R___Lc_Tax;
    private DbsField pnd_Tiaa_R___9b_Amt;
    private DbsField pnd_Tiaa_R___Reported;
    private DbsField pnd_Tiaa_R___Miss_Tin;
    private DbsField pnd_Tiaa_R___Miss_Nad;
    private DbsField pnd_Tiaa_R___Miss_Hld;
    private DbsField pnd_Tiaa_R___Tot_Hold;
    private DbsField pnd_Tiaa_Tot_Ctr;
    private DbsField pnd_Tiaa_Tot_Gross;
    private DbsField pnd_Tiaa_Tot_Income;
    private DbsField pnd_Tiaa_Tot_Ivc;
    private DbsField pnd_Tiaa_Tot_Tax;
    private DbsField pnd_Tiaa_Tot_St_Tax;
    private DbsField pnd_Tiaa_Tot_Reported;
    private DbsField pnd_Tiaa_Tot_Miss_Tin;
    private DbsField pnd_Tiaa_Tot_Miss_Nad;
    private DbsField pnd_Tiaa_Tot_Miss_Hld;
    private DbsField pnd_Tiaa_Tot_Tot_Hold;
    private DbsField pnd_Tcii_Int_Ctr;
    private DbsField pnd_Tcii_Int_Gross;
    private DbsField pnd_Tcii_Int_Income;
    private DbsField pnd_Tcii_Int_Ivc;
    private DbsField pnd_Tcii_Int_Tax;
    private DbsField pnd_Tcii_Int_St_Tax;
    private DbsField pnd_Tcii_Int_Reported;
    private DbsField pnd_Tcii_Int_Miss_Tin;
    private DbsField pnd_Tcii_Int_Miss_Nad;
    private DbsField pnd_Tcii_Int_Miss_Hld;
    private DbsField pnd_Tcii_Int_Tot_Hold;
    private DbsField pnd_Tcii_R___Ctr;
    private DbsField pnd_Tcii_R___Gross;
    private DbsField pnd_Tcii_R___Income;
    private DbsField pnd_Tcii_R___Ivc;
    private DbsField pnd_Tcii_R___Tax;
    private DbsField pnd_Tcii_R___St_Tax;
    private DbsField pnd_Tcii_R___Lc_Tax;
    private DbsField pnd_Tcii_R___9b_Amt;
    private DbsField pnd_Tcii_R___Reported;
    private DbsField pnd_Tcii_R___Miss_Tin;
    private DbsField pnd_Tcii_R___Miss_Nad;
    private DbsField pnd_Tcii_R___Miss_Hld;
    private DbsField pnd_Tcii_R___Tot_Hold;
    private DbsField pnd_Tcii_Tot_Ctr;
    private DbsField pnd_Tcii_Tot_Gross;
    private DbsField pnd_Tcii_Tot_Income;
    private DbsField pnd_Tcii_Tot_Ivc;
    private DbsField pnd_Tcii_Tot_Tax;
    private DbsField pnd_Tcii_Tot_St_Tax;
    private DbsField pnd_Tcii_Tot_Lc_Tax;
    private DbsField pnd_Tcii_Tot_9b_Amt;
    private DbsField pnd_Tcii_Tot_Reported;
    private DbsField pnd_Tcii_Tot_Miss_Tin;
    private DbsField pnd_Tcii_Tot_Miss_Nad;
    private DbsField pnd_Tcii_Tot_Miss_Hld;
    private DbsField pnd_Fsbc_Int_Ctr;
    private DbsField pnd_Fsbc_Int_Gross;
    private DbsField pnd_Fsbc_Int_Income;
    private DbsField pnd_Fsbc_Int_Ivc;
    private DbsField pnd_Fsbc_Int_Tax;
    private DbsField pnd_Fsbc_Int_St_Tax;
    private DbsField pnd_Fsbc_Int_Reported;
    private DbsField pnd_Fsbc_Int_Miss_Tin;
    private DbsField pnd_Fsbc_Int_Miss_Nad;
    private DbsField pnd_Fsbc_Int_Miss_Hld;
    private DbsField pnd_Fsbc_Int_Tot_Hold;
    private DbsField pnd_Fsbc_R___Ctr;
    private DbsField pnd_Fsbc_R___Gross;
    private DbsField pnd_Fsbc_R___Income;
    private DbsField pnd_Fsbc_R___Ivc;
    private DbsField pnd_Fsbc_R___Tax;
    private DbsField pnd_Fsbc_R___St_Tax;
    private DbsField pnd_Fsbc_R___Lc_Tax;
    private DbsField pnd_Fsbc_R___9b_Amt;
    private DbsField pnd_Fsbc_R___Reported;
    private DbsField pnd_Fsbc_R___Miss_Tin;
    private DbsField pnd_Fsbc_R___Miss_Nad;
    private DbsField pnd_Fsbc_R___Miss_Hld;
    private DbsField pnd_Fsbc_R___Tot_Hold;
    private DbsField pnd_Fsbc_Tot_Ctr;
    private DbsField pnd_Fsbc_Tot_Gross;
    private DbsField pnd_Fsbc_Tot_Income;
    private DbsField pnd_Fsbc_Tot_Ivc;
    private DbsField pnd_Fsbc_Tot_Tax;
    private DbsField pnd_Fsbc_Tot_St_Tax;
    private DbsField pnd_Fsbc_Tot_Lc_Tax;
    private DbsField pnd_Fsbc_Tot_9b_Amt;
    private DbsField pnd_Fsbc_Tot_Reported;
    private DbsField pnd_Fsbc_Tot_Miss_Tin;
    private DbsField pnd_Fsbc_Tot_Miss_Nad;
    private DbsField pnd_Fsbc_Tot_Miss_Hld;
    private DbsField pnd_Fsbc_Tot_Tot_Hold;
    private DbsField pnd_Trst_Int_Ctr;
    private DbsField pnd_Trst_Int_Gross;
    private DbsField pnd_Trst_Int_Income;
    private DbsField pnd_Trst_Int_Ivc;
    private DbsField pnd_Trst_Int_Tax;
    private DbsField pnd_Trst_Int_St_Tax;
    private DbsField pnd_Trst_Int_Reported;
    private DbsField pnd_Trst_Int_Miss_Tin;
    private DbsField pnd_Trst_Int_Miss_Nad;
    private DbsField pnd_Trst_Int_Miss_Hld;
    private DbsField pnd_Trst_Int_Tot_Hold;
    private DbsField pnd_Trst_R___Ctr;
    private DbsField pnd_Trst_R___Gross;
    private DbsField pnd_Trst_R___Income;
    private DbsField pnd_Trst_R___Ivc;
    private DbsField pnd_Trst_R___Tax;
    private DbsField pnd_Trst_R___St_Tax;
    private DbsField pnd_Trst_R___Lc_Tax;
    private DbsField pnd_Trst_R___9b_Amt;
    private DbsField pnd_Trst_R___Reported;
    private DbsField pnd_Trst_R___Miss_Tin;
    private DbsField pnd_Trst_R___Miss_Nad;
    private DbsField pnd_Trst_R___Miss_Hld;
    private DbsField pnd_Trst_R___Tot_Hold;
    private DbsField pnd_Trst_Tot_Ctr;
    private DbsField pnd_Trst_Tot_Gross;
    private DbsField pnd_Trst_Tot_Income;
    private DbsField pnd_Trst_Tot_Ivc;
    private DbsField pnd_Trst_Tot_Tax;
    private DbsField pnd_Trst_Tot_St_Tax;
    private DbsField pnd_Trst_Tot_Reported;
    private DbsField pnd_Trst_Tot_Miss_Tin;
    private DbsField pnd_Trst_Tot_Miss_Nad;
    private DbsField pnd_Trst_Tot_Miss_Hld;
    private DbsField pnd_Trst_Tot_Tot_Hold;
    private DbsField pnd_Grand_Tot_Ctr;
    private DbsField pnd_Grand_Tot_Gross;
    private DbsField pnd_Grand_Tot_Income;
    private DbsField pnd_Grand_Tot_Ivc;
    private DbsField pnd_Grand_Tot_Tax;
    private DbsField pnd_Grand_Tot_St_Tax;
    private DbsField pnd_Grand_Tot_Reported;
    private DbsField pnd_Grand_Tot_Miss_Tin;
    private DbsField pnd_Grand_Tot_Miss_Nad;
    private DbsField pnd_Grand_Tot_Miss_Hld;
    private DbsField pnd_Rollover_Fix;
    private DbsField pnd_Tiaa__Fix_Ctr;
    private DbsField pnd_Tiaa__Fix_Gross;
    private DbsField pnd_Tiaa__Fix_Income;
    private DbsField pnd_Tiaa__Fix_Ivc;
    private DbsField pnd_Cref__Fix_Ctr;
    private DbsField pnd_Cref__Fix_Gross;
    private DbsField pnd_Cref__Fix_Income;
    private DbsField pnd_Cref__Fix_Ivc;
    private DbsField pnd_Life_Fix_Ctr;
    private DbsField pnd_Life_Fix_Gross;
    private DbsField pnd_Life_Fix_Income;
    private DbsField pnd_Life_Fix_Ivc;
    private DbsField pnd_Tcii__Fix_Ctr;
    private DbsField pnd_Tcii__Fix_Gross;
    private DbsField pnd_Tcii__Fix_Income;
    private DbsField pnd_Tcii__Fix_Ivc;
    private DbsField pnd_Trst__Fix_Ctr;
    private DbsField pnd_Trst__Fix_Gross;
    private DbsField pnd_Trst__Fix_Income;
    private DbsField pnd_Trst__Fix_Ivc;
    private DbsField pnd_Roll_Fix_Amt_2;
    private DbsField pnd_Roll_Fix_Amt_5;
    private DbsField pnd_Ws_Tax_Year;
    private DbsField pnd_Ws_Recs_Read;
    private DbsField pnd_Ws_Recs_Written;
    private DbsField pnd_Ws_Subtotal_Rec_Ctr;
    private DbsField pnd_Ws_Subtotal_State_Tot_1;
    private DbsField pnd_Ws_Subtotal_State_Tot_10;
    private DbsField pnd_Ws_Subtotal_Local_Tot_1;
    private DbsField pnd_Ws_Subtotal_State_Tot_11;
    private DbsField pnd_Ws_Subtotal_State_Tot_B;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Form_A;
    private DbsField pnd_Input_Parm_Pnd_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok;
    private DbsField pnd_Input_Parm_Pnd_Test_Run;
    private DbsField pnd_Input_Parm_Pnd_Company;
    private DbsField pnd_Ws_State_Product;
    private DbsField pnd_I;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_Ws_State_Seq_Nbr;
    private DbsField pnd_State_Table_Pnd_Ws_State_Alpha;
    private DbsField pnd_State_Table_Pnd_Ws_State_Full_Name;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref;
    private DbsField pnd_State_Table_Pnd_Ws_State_Tax_Id_Life;
    private DbsField pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind;
    private DbsField pnd_Temp_State_Id_20_Md;
    private DbsField pnd_Temp_State_Id_20_Nm;
    private DbsField pnd_P_S_Cnt;
    private DbsField pnd_S_Twrpymnt_Form_Sd;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_2;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_3;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwra0001 = new PdaTwra0001(localVariables);
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwra5052 = new PdaTwra5052(localVariables);
        ldaTwrl3300 = new LdaTwrl3300();
        registerRecord(ldaTwrl3300);
        ldaTwrl3321 = new LdaTwrl3321();
        registerRecord(ldaTwrl3321);
        ldaTwrl3322 = new LdaTwrl3322();
        registerRecord(ldaTwrl3322);
        ldaTwrl3323 = new LdaTwrl3323();
        registerRecord(ldaTwrl3323);
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3325 = new LdaTwrl3325();
        registerRecord(ldaTwrl3325);
        ldaTwrl3326 = new LdaTwrl3326();
        registerRecord(ldaTwrl3326);
        pdaTwradist = new PdaTwradist(localVariables);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // Local Variables

        pnd_Passed_Namaddr = localVariables.newGroupInRecord("pnd_Passed_Namaddr", "#PASSED-NAMADDR");
        pnd_Passed_Namaddr_Pnd_Passed_Line = pnd_Passed_Namaddr.newFieldArrayInGroup("pnd_Passed_Namaddr_Pnd_Passed_Line", "#PASSED-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 7));

        pnd_Irs_Data = localVariables.newGroupInRecord("pnd_Irs_Data", "#IRS-DATA");
        pnd_Irs_Data_Pnd_Irs_Name_1 = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_Name_1", "#IRS-NAME-1", FieldType.STRING, 40);
        pnd_Irs_Data_Pnd_Irs_Name_2 = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_Name_2", "#IRS-NAME-2", FieldType.STRING, 40);
        pnd_Irs_Data_Pnd_Irs_Address = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_Address", "#IRS-ADDRESS", FieldType.STRING, 40);
        pnd_Irs_Data_Pnd_Irs_City = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_City", "#IRS-CITY", FieldType.STRING, 29);
        pnd_Irs_Data_Pnd_Irs_State = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_State", "#IRS-STATE", FieldType.STRING, 2);
        pnd_Irs_Data_Pnd_Irs_Zip = pnd_Irs_Data.newFieldInGroup("pnd_Irs_Data_Pnd_Irs_Zip", "#IRS-ZIP", FieldType.STRING, 9);
        contr_Tot_1 = localVariables.newFieldInRecord("contr_Tot_1", "CONTR-TOT-1", FieldType.NUMERIC, 15, 2);
        contr_Tot_2 = localVariables.newFieldInRecord("contr_Tot_2", "CONTR-TOT-2", FieldType.NUMERIC, 15, 2);
        contr_Tot_3 = localVariables.newFieldInRecord("contr_Tot_3", "CONTR-TOT-3", FieldType.NUMERIC, 15, 2);
        contr_Tot_4 = localVariables.newFieldInRecord("contr_Tot_4", "CONTR-TOT-4", FieldType.NUMERIC, 15, 2);
        contr_Tot_5 = localVariables.newFieldInRecord("contr_Tot_5", "CONTR-TOT-5", FieldType.NUMERIC, 15, 2);
        contr_Tot_6 = localVariables.newFieldInRecord("contr_Tot_6", "CONTR-TOT-6", FieldType.NUMERIC, 15, 2);
        contr_Tot_7 = localVariables.newFieldInRecord("contr_Tot_7", "CONTR-TOT-7", FieldType.NUMERIC, 15, 2);
        contr_Tot_8 = localVariables.newFieldInRecord("contr_Tot_8", "CONTR-TOT-8", FieldType.NUMERIC, 15, 2);
        contr_Tot_9 = localVariables.newFieldInRecord("contr_Tot_9", "CONTR-TOT-9", FieldType.NUMERIC, 15, 2);
        contr_Tot_10 = localVariables.newFieldInRecord("contr_Tot_10", "CONTR-TOT-10", FieldType.NUMERIC, 15, 2);
        contr_Tot_11 = localVariables.newFieldInRecord("contr_Tot_11", "CONTR-TOT-11", FieldType.NUMERIC, 15, 2);
        contr_Tot_12 = localVariables.newFieldInRecord("contr_Tot_12", "CONTR-TOT-12", FieldType.NUMERIC, 15, 2);
        contr_Tot_13 = localVariables.newFieldInRecord("contr_Tot_13", "CONTR-TOT-13", FieldType.NUMERIC, 15, 2);
        contr_Tot_B = localVariables.newFieldInRecord("contr_Tot_B", "CONTR-TOT-B", FieldType.NUMERIC, 15, 2);
        pnd_Max_States = localVariables.newFieldInRecord("pnd_Max_States", "#MAX-STATES", FieldType.NUMERIC, 2);
        state_Name = localVariables.newFieldArrayInRecord("state_Name", "STATE-NAME", FieldType.STRING, 15, new DbsArrayController(1, 50));
        state_Alpha = localVariables.newFieldArrayInRecord("state_Alpha", "STATE-ALPHA", FieldType.STRING, 3, new DbsArrayController(1, 50));
        state_Index = localVariables.newFieldArrayInRecord("state_Index", "STATE-INDEX", FieldType.NUMERIC, 2, new DbsArrayController(1, 50));

        state_Totals = localVariables.newGroupArrayInRecord("state_Totals", "STATE-TOTALS", new DbsArrayController(1, 50));
        state_Totals_State_Code = state_Totals.newFieldInGroup("state_Totals_State_Code", "STATE-CODE", FieldType.NUMERIC, 2);
        state_Totals_St_B_Rec_Ctr = state_Totals.newFieldInGroup("state_Totals_St_B_Rec_Ctr", "ST-B-REC-CTR", FieldType.NUMERIC, 6);
        state_Totals_State_Tot_1 = state_Totals.newFieldInGroup("state_Totals_State_Tot_1", "STATE-TOT-1", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_2 = state_Totals.newFieldInGroup("state_Totals_State_Tot_2", "STATE-TOT-2", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_3 = state_Totals.newFieldInGroup("state_Totals_State_Tot_3", "STATE-TOT-3", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_4 = state_Totals.newFieldInGroup("state_Totals_State_Tot_4", "STATE-TOT-4", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_5 = state_Totals.newFieldInGroup("state_Totals_State_Tot_5", "STATE-TOT-5", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_6 = state_Totals.newFieldInGroup("state_Totals_State_Tot_6", "STATE-TOT-6", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_7 = state_Totals.newFieldInGroup("state_Totals_State_Tot_7", "STATE-TOT-7", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_8 = state_Totals.newFieldInGroup("state_Totals_State_Tot_8", "STATE-TOT-8", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_9 = state_Totals.newFieldInGroup("state_Totals_State_Tot_9", "STATE-TOT-9", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_10 = state_Totals.newFieldInGroup("state_Totals_State_Tot_10", "STATE-TOT-10", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_11 = state_Totals.newFieldInGroup("state_Totals_State_Tot_11", "STATE-TOT-11", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_12 = state_Totals.newFieldInGroup("state_Totals_State_Tot_12", "STATE-TOT-12", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_13 = state_Totals.newFieldInGroup("state_Totals_State_Tot_13", "STATE-TOT-13", FieldType.NUMERIC, 15, 2);
        state_Totals_State_Tot_B = state_Totals.newFieldInGroup("state_Totals_State_Tot_B", "STATE-TOT-B", FieldType.NUMERIC, 15, 2);

        local_Totals = localVariables.newGroupArrayInRecord("local_Totals", "LOCAL-TOTALS", new DbsArrayController(1, 50));
        local_Totals_Local_Code = local_Totals.newFieldInGroup("local_Totals_Local_Code", "LOCAL-CODE", FieldType.STRING, 2);
        local_Totals_Lc_B_Rec_Ctr = local_Totals.newFieldInGroup("local_Totals_Lc_B_Rec_Ctr", "LC-B-REC-CTR", FieldType.NUMERIC, 6);
        local_Totals_Local_Tot_1 = local_Totals.newFieldInGroup("local_Totals_Local_Tot_1", "LOCAL-TOT-1", FieldType.NUMERIC, 15, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 6);
        pnd_Ws_Save_Prdct_Name = localVariables.newFieldInRecord("pnd_Ws_Save_Prdct_Name", "#WS-SAVE-PRDCT-NAME", FieldType.STRING, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 6);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 6);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 6);
        pnd_Ws_Contract_8 = localVariables.newFieldInRecord("pnd_Ws_Contract_8", "#WS-CONTRACT-8", FieldType.STRING, 8);
        cntr_Cref_Ira = localVariables.newFieldInRecord("cntr_Cref_Ira", "CNTR-CREF-IRA", FieldType.NUMERIC, 15, 2);
        cntr_Tiaa_Ira = localVariables.newFieldInRecord("cntr_Tiaa_Ira", "CNTR-TIAA-IRA", FieldType.NUMERIC, 15, 2);
        cntr_Tcii_Ira = localVariables.newFieldInRecord("cntr_Tcii_Ira", "CNTR-TCII-IRA", FieldType.NUMERIC, 15, 2);
        cntr_Fsbc_Ira = localVariables.newFieldInRecord("cntr_Fsbc_Ira", "CNTR-FSBC-IRA", FieldType.NUMERIC, 15, 2);
        cntr_Trst_Ira = localVariables.newFieldInRecord("cntr_Trst_Ira", "CNTR-TRST-IRA", FieldType.NUMERIC, 15, 2);
        a_Record_Ctr = localVariables.newFieldInRecord("a_Record_Ctr", "A-RECORD-CTR", FieldType.NUMERIC, 6);
        b_Record_Ctr = localVariables.newFieldInRecord("b_Record_Ctr", "B-RECORD-CTR", FieldType.NUMERIC, 8);
        b_Recs_Written = localVariables.newFieldInRecord("b_Recs_Written", "B-RECS-WRITTEN", FieldType.NUMERIC, 8);
        c_Record_Ctr = localVariables.newFieldInRecord("c_Record_Ctr", "C-RECORD-CTR", FieldType.NUMERIC, 6);
        f_Record_Ctr = localVariables.newFieldInRecord("f_Record_Ctr", "F-RECORD-CTR", FieldType.NUMERIC, 6);
        k_Record_Ctr = localVariables.newFieldInRecord("k_Record_Ctr", "K-RECORD-CTR", FieldType.NUMERIC, 6);
        t_Record_Ctr = localVariables.newFieldInRecord("t_Record_Ctr", "T-RECORD-CTR", FieldType.NUMERIC, 6);
        pnd_Ws_Rejects = localVariables.newFieldInRecord("pnd_Ws_Rejects", "#WS-REJECTS", FieldType.STRING, 9);
        pnd_Ws_Rejects_File = localVariables.newFieldInRecord("pnd_Ws_Rejects_File", "#WS-REJECTS-FILE", FieldType.STRING, 1);
        pnd_First_Rec_Sw = localVariables.newFieldInRecord("pnd_First_Rec_Sw", "#FIRST-REC-SW", FieldType.STRING, 1);
        pnd_Save_Prod_Ind = localVariables.newFieldInRecord("pnd_Save_Prod_Ind", "#SAVE-PROD-IND", FieldType.STRING, 1);
        pnd_Save_Type_Code = localVariables.newFieldInRecord("pnd_Save_Type_Code", "#SAVE-TYPE-CODE", FieldType.STRING, 1);
        pnd_Ws_Split_Field = localVariables.newFieldInRecord("pnd_Ws_Split_Field", "#WS-SPLIT-FIELD", FieldType.STRING, 2);

        pnd_Ws_Split_Field__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Split_Field__R_Field_1", "REDEFINE", pnd_Ws_Split_Field);
        pnd_Ws_Split_Field_Pnd_Ws_Split_1 = pnd_Ws_Split_Field__R_Field_1.newFieldInGroup("pnd_Ws_Split_Field_Pnd_Ws_Split_1", "#WS-SPLIT-1", FieldType.STRING, 
            1);
        pnd_Ws_Split_Field_Pnd_Ws_Split_2 = pnd_Ws_Split_Field__R_Field_1.newFieldInGroup("pnd_Ws_Split_Field_Pnd_Ws_Split_2", "#WS-SPLIT-2", FieldType.STRING, 
            1);
        pnd_Work_Amt = localVariables.newFieldInRecord("pnd_Work_Amt", "#WORK-AMT", FieldType.NUMERIC, 11, 2);
        pnd_T_C_Title = localVariables.newFieldInRecord("pnd_T_C_Title", "#T-C-TITLE", FieldType.STRING, 10);
        pnd_Cref_Int_Ctr = localVariables.newFieldInRecord("pnd_Cref_Int_Ctr", "#CREF-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Cref_Int_Gross = localVariables.newFieldInRecord("pnd_Cref_Int_Gross", "#CREF-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Int_Income = localVariables.newFieldInRecord("pnd_Cref_Int_Income", "#CREF-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Int_Ivc = localVariables.newFieldInRecord("pnd_Cref_Int_Ivc", "#CREF-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Int_Tax = localVariables.newFieldInRecord("pnd_Cref_Int_Tax", "#CREF-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Int_St_Tax = localVariables.newFieldInRecord("pnd_Cref_Int_St_Tax", "#CREF-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Int_Reported = localVariables.newFieldInRecord("pnd_Cref_Int_Reported", "#CREF-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Cref_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Cref_Int_Miss_Tin", "#CREF-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Cref_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Cref_Int_Miss_Nad", "#CREF-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Cref_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Cref_Int_Miss_Hld", "#CREF-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Cref_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Cref_Int_Tot_Hold", "#CREF-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Cref_R___Ctr = localVariables.newFieldInRecord("pnd_Cref_R___Ctr", "#CREF-R---CTR", FieldType.NUMERIC, 7);
        pnd_Cref_R___Gross = localVariables.newFieldInRecord("pnd_Cref_R___Gross", "#CREF-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___Income = localVariables.newFieldInRecord("pnd_Cref_R___Income", "#CREF-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___Ivc = localVariables.newFieldInRecord("pnd_Cref_R___Ivc", "#CREF-R---IVC", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___Tax = localVariables.newFieldInRecord("pnd_Cref_R___Tax", "#CREF-R---TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___St_Tax = localVariables.newFieldInRecord("pnd_Cref_R___St_Tax", "#CREF-R---ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Cref_R___Lc_Tax", "#CREF-R---LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___9b_Amt = localVariables.newFieldInRecord("pnd_Cref_R___9b_Amt", "#CREF-R---9B-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Cref_R___Reported = localVariables.newFieldInRecord("pnd_Cref_R___Reported", "#CREF-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Cref_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Cref_R___Miss_Tin", "#CREF-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Cref_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Cref_R___Miss_Nad", "#CREF-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Cref_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Cref_R___Miss_Hld", "#CREF-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Cref_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Cref_R___Tot_Hold", "#CREF-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Cref_Tot_Ctr = localVariables.newFieldInRecord("pnd_Cref_Tot_Ctr", "#CREF-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Cref_Tot_Gross = localVariables.newFieldInRecord("pnd_Cref_Tot_Gross", "#CREF-TOT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_Income = localVariables.newFieldInRecord("pnd_Cref_Tot_Income", "#CREF-TOT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_Ivc = localVariables.newFieldInRecord("pnd_Cref_Tot_Ivc", "#CREF-TOT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_Tax = localVariables.newFieldInRecord("pnd_Cref_Tot_Tax", "#CREF-TOT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Cref_Tot_St_Tax", "#CREF-TOT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_Lc_Tax = localVariables.newFieldInRecord("pnd_Cref_Tot_Lc_Tax", "#CREF-TOT-LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_9b_Amt = localVariables.newFieldInRecord("pnd_Cref_Tot_9b_Amt", "#CREF-TOT-9B-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Cref_Tot_Reported = localVariables.newFieldInRecord("pnd_Cref_Tot_Reported", "#CREF-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Cref_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Cref_Tot_Miss_Tin", "#CREF-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Cref_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Cref_Tot_Miss_Nad", "#CREF-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Cref_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Cref_Tot_Miss_Hld", "#CREF-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Cref_Tot_Tot_Hold = localVariables.newFieldInRecord("pnd_Cref_Tot_Tot_Hold", "#CREF-TOT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Life_Int_Ctr = localVariables.newFieldInRecord("pnd_Life_Int_Ctr", "#LIFE-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Life_Int_Gross = localVariables.newFieldInRecord("pnd_Life_Int_Gross", "#LIFE-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Life_Int_Income = localVariables.newFieldInRecord("pnd_Life_Int_Income", "#LIFE-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Life_Int_Ivc = localVariables.newFieldInRecord("pnd_Life_Int_Ivc", "#LIFE-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Life_Int_Tax = localVariables.newFieldInRecord("pnd_Life_Int_Tax", "#LIFE-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Int_St_Tax = localVariables.newFieldInRecord("pnd_Life_Int_St_Tax", "#LIFE-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Int_Reported = localVariables.newFieldInRecord("pnd_Life_Int_Reported", "#LIFE-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Life_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Life_Int_Miss_Tin", "#LIFE-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Life_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Life_Int_Miss_Nad", "#LIFE-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Life_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Life_Int_Miss_Hld", "#LIFE-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Life_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Life_Int_Tot_Hold", "#LIFE-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Life_R___Ctr = localVariables.newFieldInRecord("pnd_Life_R___Ctr", "#LIFE-R---CTR", FieldType.NUMERIC, 7);
        pnd_Life_R___Gross = localVariables.newFieldInRecord("pnd_Life_R___Gross", "#LIFE-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___Income = localVariables.newFieldInRecord("pnd_Life_R___Income", "#LIFE-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___Ivc = localVariables.newFieldInRecord("pnd_Life_R___Ivc", "#LIFE-R---IVC", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___Tax = localVariables.newFieldInRecord("pnd_Life_R___Tax", "#LIFE-R---TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___St_Tax = localVariables.newFieldInRecord("pnd_Life_R___St_Tax", "#LIFE-R---ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Life_R___Lc_Tax", "#LIFE-R---LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___9b_Amt = localVariables.newFieldInRecord("pnd_Life_R___9b_Amt", "#LIFE-R---9B-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Life_R___Reported = localVariables.newFieldInRecord("pnd_Life_R___Reported", "#LIFE-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Life_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Life_R___Miss_Tin", "#LIFE-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Life_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Life_R___Miss_Nad", "#LIFE-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Life_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Life_R___Miss_Hld", "#LIFE-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Life_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Life_R___Tot_Hold", "#LIFE-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Life_Tot_Ctr = localVariables.newFieldInRecord("pnd_Life_Tot_Ctr", "#LIFE-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Life_Tot_Gross = localVariables.newFieldInRecord("pnd_Life_Tot_Gross", "#LIFE-TOT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_Income = localVariables.newFieldInRecord("pnd_Life_Tot_Income", "#LIFE-TOT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_Ivc = localVariables.newFieldInRecord("pnd_Life_Tot_Ivc", "#LIFE-TOT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_Tax = localVariables.newFieldInRecord("pnd_Life_Tot_Tax", "#LIFE-TOT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Life_Tot_St_Tax", "#LIFE-TOT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_Lc_Tax = localVariables.newFieldInRecord("pnd_Life_Tot_Lc_Tax", "#LIFE-TOT-LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_9b_Tax = localVariables.newFieldInRecord("pnd_Life_Tot_9b_Tax", "#LIFE-TOT-9B-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Life_Tot_Reported = localVariables.newFieldInRecord("pnd_Life_Tot_Reported", "#LIFE-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Life_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Life_Tot_Miss_Tin", "#LIFE-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Life_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Life_Tot_Miss_Nad", "#LIFE-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Life_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Life_Tot_Miss_Hld", "#LIFE-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Life_Tot_Tot_Hold = localVariables.newFieldInRecord("pnd_Life_Tot_Tot_Hold", "#LIFE-TOT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Int_Ctr = localVariables.newFieldInRecord("pnd_Tiaa_Int_Ctr", "#TIAA-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Tiaa_Int_Gross = localVariables.newFieldInRecord("pnd_Tiaa_Int_Gross", "#TIAA-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Int_Income = localVariables.newFieldInRecord("pnd_Tiaa_Int_Income", "#TIAA-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Int_Ivc = localVariables.newFieldInRecord("pnd_Tiaa_Int_Ivc", "#TIAA-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Int_Tax = localVariables.newFieldInRecord("pnd_Tiaa_Int_Tax", "#TIAA-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Int_St_Tax = localVariables.newFieldInRecord("pnd_Tiaa_Int_St_Tax", "#TIAA-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Int_Reported = localVariables.newFieldInRecord("pnd_Tiaa_Int_Reported", "#TIAA-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Tiaa_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Tiaa_Int_Miss_Tin", "#TIAA-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tiaa_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Tiaa_Int_Miss_Nad", "#TIAA-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Tiaa_Int_Miss_Hld", "#TIAA-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Tiaa_Int_Tot_Hold", "#TIAA-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_R___Ctr = localVariables.newFieldInRecord("pnd_Tiaa_R___Ctr", "#TIAA-R---CTR", FieldType.NUMERIC, 7);
        pnd_Tiaa_R___Gross = localVariables.newFieldInRecord("pnd_Tiaa_R___Gross", "#TIAA-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___Income = localVariables.newFieldInRecord("pnd_Tiaa_R___Income", "#TIAA-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___Ivc = localVariables.newFieldInRecord("pnd_Tiaa_R___Ivc", "#TIAA-R---IVC", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___Tax = localVariables.newFieldInRecord("pnd_Tiaa_R___Tax", "#TIAA-R---TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___St_Tax = localVariables.newFieldInRecord("pnd_Tiaa_R___St_Tax", "#TIAA-R---ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Tiaa_R___Lc_Tax", "#TIAA-R---LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___9b_Amt = localVariables.newFieldInRecord("pnd_Tiaa_R___9b_Amt", "#TIAA-R---9B-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_R___Reported = localVariables.newFieldInRecord("pnd_Tiaa_R___Reported", "#TIAA-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Tiaa_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Tiaa_R___Miss_Tin", "#TIAA-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tiaa_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Tiaa_R___Miss_Nad", "#TIAA-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tiaa_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Tiaa_R___Miss_Hld", "#TIAA-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Tiaa_R___Tot_Hold", "#TIAA-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Tot_Ctr = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Ctr", "#TIAA-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Tiaa_Tot_Gross = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Gross", "#TIAA-TOT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Tot_Income = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Income", "#TIAA-TOT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Tot_Ivc = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Ivc", "#TIAA-TOT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Tot_Tax = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Tax", "#TIAA-TOT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Tiaa_Tot_St_Tax", "#TIAA-TOT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa_Tot_Reported = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Reported", "#TIAA-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Tiaa_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Miss_Tin", "#TIAA-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tiaa_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Miss_Nad", "#TIAA-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Miss_Hld", "#TIAA-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Tiaa_Tot_Tot_Hold = localVariables.newFieldInRecord("pnd_Tiaa_Tot_Tot_Hold", "#TIAA-TOT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tcii_Int_Ctr = localVariables.newFieldInRecord("pnd_Tcii_Int_Ctr", "#TCII-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Tcii_Int_Gross = localVariables.newFieldInRecord("pnd_Tcii_Int_Gross", "#TCII-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_Int_Income = localVariables.newFieldInRecord("pnd_Tcii_Int_Income", "#TCII-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_Int_Ivc = localVariables.newFieldInRecord("pnd_Tcii_Int_Ivc", "#TCII-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_Int_Tax = localVariables.newFieldInRecord("pnd_Tcii_Int_Tax", "#TCII-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_Int_St_Tax = localVariables.newFieldInRecord("pnd_Tcii_Int_St_Tax", "#TCII-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_Int_Reported = localVariables.newFieldInRecord("pnd_Tcii_Int_Reported", "#TCII-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Tcii_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Tcii_Int_Miss_Tin", "#TCII-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tcii_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Tcii_Int_Miss_Nad", "#TCII-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tcii_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Tcii_Int_Miss_Hld", "#TCII-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Tcii_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Tcii_Int_Tot_Hold", "#TCII-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tcii_R___Ctr = localVariables.newFieldInRecord("pnd_Tcii_R___Ctr", "#TCII-R---CTR", FieldType.NUMERIC, 7);
        pnd_Tcii_R___Gross = localVariables.newFieldInRecord("pnd_Tcii_R___Gross", "#TCII-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_R___Income = localVariables.newFieldInRecord("pnd_Tcii_R___Income", "#TCII-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tcii_R___Ivc = localVariables.newFieldInRecord("pnd_Tcii_R___Ivc", "#TCII-R---IVC", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_R___Tax = localVariables.newFieldInRecord("pnd_Tcii_R___Tax", "#TCII-R---TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_R___St_Tax = localVariables.newFieldInRecord("pnd_Tcii_R___St_Tax", "#TCII-R---ST-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Tcii_R___Lc_Tax", "#TCII-R---LC-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_R___9b_Amt = localVariables.newFieldInRecord("pnd_Tcii_R___9b_Amt", "#TCII-R---9B-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_R___Reported = localVariables.newFieldInRecord("pnd_Tcii_R___Reported", "#TCII-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Tcii_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Tcii_R___Miss_Tin", "#TCII-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tcii_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Tcii_R___Miss_Nad", "#TCII-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tcii_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Tcii_R___Miss_Hld", "#TCII-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Tcii_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Tcii_R___Tot_Hold", "#TCII-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Tcii_Tot_Ctr = localVariables.newFieldInRecord("pnd_Tcii_Tot_Ctr", "#TCII-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Tcii_Tot_Gross = localVariables.newFieldInRecord("pnd_Tcii_Tot_Gross", "#TCII-TOT-GROSS", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_Income = localVariables.newFieldInRecord("pnd_Tcii_Tot_Income", "#TCII-TOT-INCOME", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_Ivc = localVariables.newFieldInRecord("pnd_Tcii_Tot_Ivc", "#TCII-TOT-IVC", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_Tax = localVariables.newFieldInRecord("pnd_Tcii_Tot_Tax", "#TCII-TOT-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Tcii_Tot_St_Tax", "#TCII-TOT-ST-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_Lc_Tax = localVariables.newFieldInRecord("pnd_Tcii_Tot_Lc_Tax", "#TCII-TOT-LC-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_9b_Amt = localVariables.newFieldInRecord("pnd_Tcii_Tot_9b_Amt", "#TCII-TOT-9B-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Tcii_Tot_Reported = localVariables.newFieldInRecord("pnd_Tcii_Tot_Reported", "#TCII-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Tcii_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Tcii_Tot_Miss_Tin", "#TCII-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Tcii_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Tcii_Tot_Miss_Nad", "#TCII-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Tcii_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Tcii_Tot_Miss_Hld", "#TCII-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Int_Ctr = localVariables.newFieldInRecord("pnd_Fsbc_Int_Ctr", "#FSBC-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Fsbc_Int_Gross = localVariables.newFieldInRecord("pnd_Fsbc_Int_Gross", "#FSBC-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_Int_Income = localVariables.newFieldInRecord("pnd_Fsbc_Int_Income", "#FSBC-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_Int_Ivc = localVariables.newFieldInRecord("pnd_Fsbc_Int_Ivc", "#FSBC-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_Int_Tax = localVariables.newFieldInRecord("pnd_Fsbc_Int_Tax", "#FSBC-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_Int_St_Tax = localVariables.newFieldInRecord("pnd_Fsbc_Int_St_Tax", "#FSBC-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_Int_Reported = localVariables.newFieldInRecord("pnd_Fsbc_Int_Reported", "#FSBC-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Fsbc_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Fsbc_Int_Miss_Tin", "#FSBC-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Fsbc_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Fsbc_Int_Miss_Nad", "#FSBC-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Fsbc_Int_Miss_Hld", "#FSBC-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Fsbc_Int_Tot_Hold", "#FSBC-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_R___Ctr = localVariables.newFieldInRecord("pnd_Fsbc_R___Ctr", "#FSBC-R---CTR", FieldType.NUMERIC, 7);
        pnd_Fsbc_R___Gross = localVariables.newFieldInRecord("pnd_Fsbc_R___Gross", "#FSBC-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_R___Income = localVariables.newFieldInRecord("pnd_Fsbc_R___Income", "#FSBC-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Fsbc_R___Ivc = localVariables.newFieldInRecord("pnd_Fsbc_R___Ivc", "#FSBC-R---IVC", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_R___Tax = localVariables.newFieldInRecord("pnd_Fsbc_R___Tax", "#FSBC-R---TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_R___St_Tax = localVariables.newFieldInRecord("pnd_Fsbc_R___St_Tax", "#FSBC-R---ST-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Fsbc_R___Lc_Tax", "#FSBC-R---LC-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_R___9b_Amt = localVariables.newFieldInRecord("pnd_Fsbc_R___9b_Amt", "#FSBC-R---9B-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_R___Reported = localVariables.newFieldInRecord("pnd_Fsbc_R___Reported", "#FSBC-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Fsbc_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Fsbc_R___Miss_Tin", "#FSBC-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Fsbc_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Fsbc_R___Miss_Nad", "#FSBC-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Fsbc_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Fsbc_R___Miss_Hld", "#FSBC-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Fsbc_R___Tot_Hold", "#FSBC-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Tot_Ctr = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Ctr", "#FSBC-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Fsbc_Tot_Gross = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Gross", "#FSBC-TOT-GROSS", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_Income = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Income", "#FSBC-TOT-INCOME", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_Ivc = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Ivc", "#FSBC-TOT-IVC", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_Tax = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Tax", "#FSBC-TOT-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Fsbc_Tot_St_Tax", "#FSBC-TOT-ST-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_Lc_Tax = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Lc_Tax", "#FSBC-TOT-LC-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_9b_Amt = localVariables.newFieldInRecord("pnd_Fsbc_Tot_9b_Amt", "#FSBC-TOT-9B-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Fsbc_Tot_Reported = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Reported", "#FSBC-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Fsbc_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Miss_Tin", "#FSBC-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Fsbc_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Miss_Nad", "#FSBC-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Miss_Hld", "#FSBC-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Fsbc_Tot_Tot_Hold = localVariables.newFieldInRecord("pnd_Fsbc_Tot_Tot_Hold", "#FSBC-TOT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Trst_Int_Ctr = localVariables.newFieldInRecord("pnd_Trst_Int_Ctr", "#TRST-INT-CTR", FieldType.NUMERIC, 7);
        pnd_Trst_Int_Gross = localVariables.newFieldInRecord("pnd_Trst_Int_Gross", "#TRST-INT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Int_Income = localVariables.newFieldInRecord("pnd_Trst_Int_Income", "#TRST-INT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Int_Ivc = localVariables.newFieldInRecord("pnd_Trst_Int_Ivc", "#TRST-INT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Int_Tax = localVariables.newFieldInRecord("pnd_Trst_Int_Tax", "#TRST-INT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Int_St_Tax = localVariables.newFieldInRecord("pnd_Trst_Int_St_Tax", "#TRST-INT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Int_Reported = localVariables.newFieldInRecord("pnd_Trst_Int_Reported", "#TRST-INT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Trst_Int_Miss_Tin = localVariables.newFieldInRecord("pnd_Trst_Int_Miss_Tin", "#TRST-INT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Trst_Int_Miss_Nad = localVariables.newFieldInRecord("pnd_Trst_Int_Miss_Nad", "#TRST-INT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Trst_Int_Miss_Hld = localVariables.newFieldInRecord("pnd_Trst_Int_Miss_Hld", "#TRST-INT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Trst_Int_Tot_Hold = localVariables.newFieldInRecord("pnd_Trst_Int_Tot_Hold", "#TRST-INT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Trst_R___Ctr = localVariables.newFieldInRecord("pnd_Trst_R___Ctr", "#TRST-R---CTR", FieldType.NUMERIC, 7);
        pnd_Trst_R___Gross = localVariables.newFieldInRecord("pnd_Trst_R___Gross", "#TRST-R---GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___Income = localVariables.newFieldInRecord("pnd_Trst_R___Income", "#TRST-R---INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___Ivc = localVariables.newFieldInRecord("pnd_Trst_R___Ivc", "#TRST-R---IVC", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___Tax = localVariables.newFieldInRecord("pnd_Trst_R___Tax", "#TRST-R---TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___St_Tax = localVariables.newFieldInRecord("pnd_Trst_R___St_Tax", "#TRST-R---ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___Lc_Tax = localVariables.newFieldInRecord("pnd_Trst_R___Lc_Tax", "#TRST-R---LC-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___9b_Amt = localVariables.newFieldInRecord("pnd_Trst_R___9b_Amt", "#TRST-R---9B-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Trst_R___Reported = localVariables.newFieldInRecord("pnd_Trst_R___Reported", "#TRST-R---REPORTED", FieldType.NUMERIC, 7);
        pnd_Trst_R___Miss_Tin = localVariables.newFieldInRecord("pnd_Trst_R___Miss_Tin", "#TRST-R---MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Trst_R___Miss_Nad = localVariables.newFieldInRecord("pnd_Trst_R___Miss_Nad", "#TRST-R---MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Trst_R___Miss_Hld = localVariables.newFieldInRecord("pnd_Trst_R___Miss_Hld", "#TRST-R---MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Trst_R___Tot_Hold = localVariables.newFieldInRecord("pnd_Trst_R___Tot_Hold", "#TRST-R---TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Trst_Tot_Ctr = localVariables.newFieldInRecord("pnd_Trst_Tot_Ctr", "#TRST-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Trst_Tot_Gross = localVariables.newFieldInRecord("pnd_Trst_Tot_Gross", "#TRST-TOT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Tot_Income = localVariables.newFieldInRecord("pnd_Trst_Tot_Income", "#TRST-TOT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Tot_Ivc = localVariables.newFieldInRecord("pnd_Trst_Tot_Ivc", "#TRST-TOT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Tot_Tax = localVariables.newFieldInRecord("pnd_Trst_Tot_Tax", "#TRST-TOT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Trst_Tot_St_Tax", "#TRST-TOT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Trst_Tot_Reported = localVariables.newFieldInRecord("pnd_Trst_Tot_Reported", "#TRST-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Trst_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Trst_Tot_Miss_Tin", "#TRST-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Trst_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Trst_Tot_Miss_Nad", "#TRST-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Trst_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Trst_Tot_Miss_Hld", "#TRST-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Trst_Tot_Tot_Hold = localVariables.newFieldInRecord("pnd_Trst_Tot_Tot_Hold", "#TRST-TOT-TOT-HOLD", FieldType.NUMERIC, 6);
        pnd_Grand_Tot_Ctr = localVariables.newFieldInRecord("pnd_Grand_Tot_Ctr", "#GRAND-TOT-CTR", FieldType.NUMERIC, 7);
        pnd_Grand_Tot_Gross = localVariables.newFieldInRecord("pnd_Grand_Tot_Gross", "#GRAND-TOT-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Income = localVariables.newFieldInRecord("pnd_Grand_Tot_Income", "#GRAND-TOT-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Ivc = localVariables.newFieldInRecord("pnd_Grand_Tot_Ivc", "#GRAND-TOT-IVC", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Tax = localVariables.newFieldInRecord("pnd_Grand_Tot_Tax", "#GRAND-TOT-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_St_Tax = localVariables.newFieldInRecord("pnd_Grand_Tot_St_Tax", "#GRAND-TOT-ST-TAX", FieldType.NUMERIC, 13, 2);
        pnd_Grand_Tot_Reported = localVariables.newFieldInRecord("pnd_Grand_Tot_Reported", "#GRAND-TOT-REPORTED", FieldType.NUMERIC, 7);
        pnd_Grand_Tot_Miss_Tin = localVariables.newFieldInRecord("pnd_Grand_Tot_Miss_Tin", "#GRAND-TOT-MISS-TIN", FieldType.NUMERIC, 6);
        pnd_Grand_Tot_Miss_Nad = localVariables.newFieldInRecord("pnd_Grand_Tot_Miss_Nad", "#GRAND-TOT-MISS-NAD", FieldType.NUMERIC, 6);
        pnd_Grand_Tot_Miss_Hld = localVariables.newFieldInRecord("pnd_Grand_Tot_Miss_Hld", "#GRAND-TOT-MISS-HLD", FieldType.NUMERIC, 6);
        pnd_Rollover_Fix = localVariables.newFieldInRecord("pnd_Rollover_Fix", "#ROLLOVER-FIX", FieldType.STRING, 1);
        pnd_Tiaa__Fix_Ctr = localVariables.newFieldInRecord("pnd_Tiaa__Fix_Ctr", "#TIAA--FIX-CTR", FieldType.NUMERIC, 7);
        pnd_Tiaa__Fix_Gross = localVariables.newFieldInRecord("pnd_Tiaa__Fix_Gross", "#TIAA--FIX-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa__Fix_Income = localVariables.newFieldInRecord("pnd_Tiaa__Fix_Income", "#TIAA--FIX-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tiaa__Fix_Ivc = localVariables.newFieldInRecord("pnd_Tiaa__Fix_Ivc", "#TIAA--FIX-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Cref__Fix_Ctr = localVariables.newFieldInRecord("pnd_Cref__Fix_Ctr", "#CREF--FIX-CTR", FieldType.NUMERIC, 7);
        pnd_Cref__Fix_Gross = localVariables.newFieldInRecord("pnd_Cref__Fix_Gross", "#CREF--FIX-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Cref__Fix_Income = localVariables.newFieldInRecord("pnd_Cref__Fix_Income", "#CREF--FIX-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Cref__Fix_Ivc = localVariables.newFieldInRecord("pnd_Cref__Fix_Ivc", "#CREF--FIX-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Life_Fix_Ctr = localVariables.newFieldInRecord("pnd_Life_Fix_Ctr", "#LIFE-FIX-CTR", FieldType.NUMERIC, 7);
        pnd_Life_Fix_Gross = localVariables.newFieldInRecord("pnd_Life_Fix_Gross", "#LIFE-FIX-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Life_Fix_Income = localVariables.newFieldInRecord("pnd_Life_Fix_Income", "#LIFE-FIX-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Life_Fix_Ivc = localVariables.newFieldInRecord("pnd_Life_Fix_Ivc", "#LIFE-FIX-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Tcii__Fix_Ctr = localVariables.newFieldInRecord("pnd_Tcii__Fix_Ctr", "#TCII--FIX-CTR", FieldType.NUMERIC, 7);
        pnd_Tcii__Fix_Gross = localVariables.newFieldInRecord("pnd_Tcii__Fix_Gross", "#TCII--FIX-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Tcii__Fix_Income = localVariables.newFieldInRecord("pnd_Tcii__Fix_Income", "#TCII--FIX-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Tcii__Fix_Ivc = localVariables.newFieldInRecord("pnd_Tcii__Fix_Ivc", "#TCII--FIX-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Trst__Fix_Ctr = localVariables.newFieldInRecord("pnd_Trst__Fix_Ctr", "#TRST--FIX-CTR", FieldType.NUMERIC, 7);
        pnd_Trst__Fix_Gross = localVariables.newFieldInRecord("pnd_Trst__Fix_Gross", "#TRST--FIX-GROSS", FieldType.NUMERIC, 13, 2);
        pnd_Trst__Fix_Income = localVariables.newFieldInRecord("pnd_Trst__Fix_Income", "#TRST--FIX-INCOME", FieldType.NUMERIC, 13, 2);
        pnd_Trst__Fix_Ivc = localVariables.newFieldInRecord("pnd_Trst__Fix_Ivc", "#TRST--FIX-IVC", FieldType.NUMERIC, 11, 2);
        pnd_Roll_Fix_Amt_2 = localVariables.newFieldInRecord("pnd_Roll_Fix_Amt_2", "#ROLL-FIX-AMT-2", FieldType.NUMERIC, 10, 2);
        pnd_Roll_Fix_Amt_5 = localVariables.newFieldInRecord("pnd_Roll_Fix_Amt_5", "#ROLL-FIX-AMT-5", FieldType.NUMERIC, 10, 2);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Recs_Read = localVariables.newFieldInRecord("pnd_Ws_Recs_Read", "#WS-RECS-READ", FieldType.NUMERIC, 11);
        pnd_Ws_Recs_Written = localVariables.newFieldInRecord("pnd_Ws_Recs_Written", "#WS-RECS-WRITTEN", FieldType.NUMERIC, 11);
        pnd_Ws_Subtotal_Rec_Ctr = localVariables.newFieldInRecord("pnd_Ws_Subtotal_Rec_Ctr", "#WS-SUBTOTAL-REC-CTR", FieldType.NUMERIC, 9);
        pnd_Ws_Subtotal_State_Tot_1 = localVariables.newFieldInRecord("pnd_Ws_Subtotal_State_Tot_1", "#WS-SUBTOTAL-STATE-TOT-1", FieldType.NUMERIC, 15, 
            2);
        pnd_Ws_Subtotal_State_Tot_10 = localVariables.newFieldInRecord("pnd_Ws_Subtotal_State_Tot_10", "#WS-SUBTOTAL-STATE-TOT-10", FieldType.NUMERIC, 
            15, 2);
        pnd_Ws_Subtotal_Local_Tot_1 = localVariables.newFieldInRecord("pnd_Ws_Subtotal_Local_Tot_1", "#WS-SUBTOTAL-LOCAL-TOT-1", FieldType.NUMERIC, 15, 
            2);
        pnd_Ws_Subtotal_State_Tot_11 = localVariables.newFieldInRecord("pnd_Ws_Subtotal_State_Tot_11", "#WS-SUBTOTAL-STATE-TOT-11", FieldType.NUMERIC, 
            15, 2);
        pnd_Ws_Subtotal_State_Tot_B = localVariables.newFieldInRecord("pnd_Ws_Subtotal_State_Tot_B", "#WS-SUBTOTAL-STATE-TOT-B", FieldType.NUMERIC, 15, 
            2);

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Form_A = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Form_A", "#FORM-A", FieldType.STRING, 6);
        pnd_Input_Parm_Pnd_Tax_Year = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok", "#NOT-FIRST-TIME-RUN-OK", 
            FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Test_Run = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Test_Run", "#TEST-RUN", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Company = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Company", "#COMPANY", FieldType.STRING, 4);
        pnd_Ws_State_Product = localVariables.newFieldInRecord("pnd_Ws_State_Product", "#WS-STATE-PRODUCT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);

        pnd_State_Table = localVariables.newGroupArrayInRecord("pnd_State_Table", "#STATE-TABLE", new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_Ws_State_Seq_Nbr = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Seq_Nbr", "#WS-STATE-SEQ-NBR", FieldType.NUMERIC, 
            3);
        pnd_State_Table_Pnd_Ws_State_Alpha = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Alpha", "#WS-STATE-ALPHA", FieldType.STRING, 
            2);
        pnd_State_Table_Pnd_Ws_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Full_Name", "#WS-STATE-FULL-NAME", FieldType.STRING, 
            19);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa", "#WS-STATE-TAX-ID-TIAA", 
            FieldType.STRING, 15);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref", "#WS-STATE-TAX-ID-CREF", 
            FieldType.STRING, 14);
        pnd_State_Table_Pnd_Ws_State_Tax_Id_Life = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Tax_Id_Life", "#WS-STATE-TAX-ID-LIFE", 
            FieldType.STRING, 14);
        pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind", "#WS-STATE-COMB-FED-IND", 
            FieldType.STRING, 1);
        pnd_Temp_State_Id_20_Md = localVariables.newFieldInRecord("pnd_Temp_State_Id_20_Md", "#TEMP-STATE-ID-20-MD", FieldType.STRING, 20);
        pnd_Temp_State_Id_20_Nm = localVariables.newFieldInRecord("pnd_Temp_State_Id_20_Nm", "#TEMP-STATE-ID-20-NM", FieldType.STRING, 20);
        pnd_P_S_Cnt = localVariables.newFieldInRecord("pnd_P_S_Cnt", "#P-S-CNT", FieldType.NUMERIC, 3);
        pnd_S_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd", "#S-TWRPYMNT-FORM-SD", FieldType.STRING, 37);

        pnd_S_Twrpymnt_Form_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd__R_Field_2", "REDEFINE", pnd_S_Twrpymnt_Form_Sd);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr", 
            "#S-TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr", 
            "#S-TWRPYMNT-CONTRACT-PAYEE-NBR", FieldType.STRING, 10);

        pnd_S_Twrpymnt_Form_Sd__R_Field_3 = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newGroupInGroup("pnd_S_Twrpymnt_Form_Sd__R_Field_3", "REDEFINE", pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_3.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_3.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3300.initializeValues();
        ldaTwrl3321.initializeValues();
        ldaTwrl3322.initializeValues();
        ldaTwrl3323.initializeValues();
        ldaTwrl3324.initializeValues();
        ldaTwrl3325.initializeValues();
        ldaTwrl3326.initializeValues();
        ldaTwrl9415.initializeValues();

        localVariables.reset();
        contr_Tot_1.setInitialValue(0);
        contr_Tot_2.setInitialValue(0);
        contr_Tot_3.setInitialValue(0);
        contr_Tot_4.setInitialValue(0);
        contr_Tot_5.setInitialValue(0);
        contr_Tot_6.setInitialValue(0);
        contr_Tot_7.setInitialValue(0);
        contr_Tot_8.setInitialValue(0);
        contr_Tot_9.setInitialValue(0);
        contr_Tot_10.setInitialValue(0);
        contr_Tot_11.setInitialValue(0);
        contr_Tot_12.setInitialValue(0);
        contr_Tot_13.setInitialValue(0);
        contr_Tot_B.setInitialValue(0);
        state_Totals_State_Code.getValue(1).setInitialValue(0);
        state_Totals_St_B_Rec_Ctr.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_1.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_2.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_3.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_4.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_5.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_6.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_7.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_8.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_9.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_10.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_11.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_12.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_13.getValue(1).setInitialValue(0);
        state_Totals_State_Tot_B.getValue(1).setInitialValue(0);
        local_Totals_Local_Code.getValue(1).setInitialValue(" ");
        local_Totals_Lc_B_Rec_Ctr.getValue(1).setInitialValue(0);
        local_Totals_Local_Tot_1.getValue(1).setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_X.setInitialValue(0);
        pnd_Y.setInitialValue(0);
        pnd_Z.setInitialValue(0);
        pnd_Ws_Contract_8.setInitialValue(" ");
        cntr_Cref_Ira.setInitialValue(0);
        cntr_Tiaa_Ira.setInitialValue(0);
        cntr_Tcii_Ira.setInitialValue(0);
        cntr_Fsbc_Ira.setInitialValue(0);
        cntr_Trst_Ira.setInitialValue(0);
        a_Record_Ctr.setInitialValue(0);
        b_Record_Ctr.setInitialValue(0);
        b_Recs_Written.setInitialValue(0);
        c_Record_Ctr.setInitialValue(0);
        f_Record_Ctr.setInitialValue(0);
        k_Record_Ctr.setInitialValue(0);
        t_Record_Ctr.setInitialValue(0);
        pnd_Ws_Rejects.setInitialValue(" ");
        pnd_Ws_Rejects_File.setInitialValue("N");
        pnd_First_Rec_Sw.setInitialValue("F");
        pnd_Save_Prod_Ind.setInitialValue(" ");
        pnd_Save_Type_Code.setInitialValue(" ");
        pnd_T_C_Title.setInitialValue("**      **");
        pnd_Cref_Int_Ctr.setInitialValue(0);
        pnd_Cref_Int_Gross.setInitialValue(0);
        pnd_Cref_Int_Income.setInitialValue(0);
        pnd_Cref_Int_Ivc.setInitialValue(0);
        pnd_Cref_Int_Tax.setInitialValue(0);
        pnd_Cref_Int_St_Tax.setInitialValue(0);
        pnd_Cref_Int_Reported.setInitialValue(0);
        pnd_Cref_Int_Miss_Tin.setInitialValue(0);
        pnd_Cref_Int_Miss_Nad.setInitialValue(0);
        pnd_Cref_Int_Miss_Hld.setInitialValue(0);
        pnd_Cref_Int_Tot_Hold.setInitialValue(0);
        pnd_Cref_R___Ctr.setInitialValue(0);
        pnd_Cref_R___Gross.setInitialValue(0);
        pnd_Cref_R___Income.setInitialValue(0);
        pnd_Cref_R___Ivc.setInitialValue(0);
        pnd_Cref_R___Tax.setInitialValue(0);
        pnd_Cref_R___St_Tax.setInitialValue(0);
        pnd_Cref_R___Lc_Tax.setInitialValue(0);
        pnd_Cref_R___9b_Amt.setInitialValue(0);
        pnd_Cref_R___Reported.setInitialValue(0);
        pnd_Cref_R___Miss_Tin.setInitialValue(0);
        pnd_Cref_R___Miss_Nad.setInitialValue(0);
        pnd_Cref_R___Miss_Hld.setInitialValue(0);
        pnd_Cref_R___Tot_Hold.setInitialValue(0);
        pnd_Cref_Tot_Ctr.setInitialValue(0);
        pnd_Cref_Tot_Gross.setInitialValue(0);
        pnd_Cref_Tot_Income.setInitialValue(0);
        pnd_Cref_Tot_Ivc.setInitialValue(0);
        pnd_Cref_Tot_Tax.setInitialValue(0);
        pnd_Cref_Tot_St_Tax.setInitialValue(0);
        pnd_Cref_Tot_Lc_Tax.setInitialValue(0);
        pnd_Cref_Tot_9b_Amt.setInitialValue(0);
        pnd_Cref_Tot_Reported.setInitialValue(0);
        pnd_Cref_Tot_Miss_Tin.setInitialValue(0);
        pnd_Cref_Tot_Miss_Nad.setInitialValue(0);
        pnd_Cref_Tot_Miss_Hld.setInitialValue(0);
        pnd_Cref_Tot_Tot_Hold.setInitialValue(0);
        pnd_Life_Int_Ctr.setInitialValue(0);
        pnd_Life_Int_Gross.setInitialValue(0);
        pnd_Life_Int_Income.setInitialValue(0);
        pnd_Life_Int_Ivc.setInitialValue(0);
        pnd_Life_Int_Tax.setInitialValue(0);
        pnd_Life_Int_St_Tax.setInitialValue(0);
        pnd_Life_Int_Reported.setInitialValue(0);
        pnd_Life_Int_Miss_Tin.setInitialValue(0);
        pnd_Life_Int_Miss_Nad.setInitialValue(0);
        pnd_Life_Int_Miss_Hld.setInitialValue(0);
        pnd_Life_Int_Tot_Hold.setInitialValue(0);
        pnd_Life_R___Ctr.setInitialValue(0);
        pnd_Life_R___Gross.setInitialValue(0);
        pnd_Life_R___Income.setInitialValue(0);
        pnd_Life_R___Ivc.setInitialValue(0);
        pnd_Life_R___Tax.setInitialValue(0);
        pnd_Life_R___St_Tax.setInitialValue(0);
        pnd_Life_R___Lc_Tax.setInitialValue(0);
        pnd_Life_R___9b_Amt.setInitialValue(0);
        pnd_Life_R___Reported.setInitialValue(0);
        pnd_Life_R___Miss_Tin.setInitialValue(0);
        pnd_Life_R___Miss_Nad.setInitialValue(0);
        pnd_Life_R___Miss_Hld.setInitialValue(0);
        pnd_Life_R___Tot_Hold.setInitialValue(0);
        pnd_Life_Tot_Ctr.setInitialValue(0);
        pnd_Life_Tot_Gross.setInitialValue(0);
        pnd_Life_Tot_Income.setInitialValue(0);
        pnd_Life_Tot_Ivc.setInitialValue(0);
        pnd_Life_Tot_Tax.setInitialValue(0);
        pnd_Life_Tot_St_Tax.setInitialValue(0);
        pnd_Life_Tot_Lc_Tax.setInitialValue(0);
        pnd_Life_Tot_9b_Tax.setInitialValue(0);
        pnd_Life_Tot_Reported.setInitialValue(0);
        pnd_Life_Tot_Miss_Tin.setInitialValue(0);
        pnd_Life_Tot_Miss_Nad.setInitialValue(0);
        pnd_Life_Tot_Miss_Hld.setInitialValue(0);
        pnd_Life_Tot_Tot_Hold.setInitialValue(0);
        pnd_Tiaa_Int_Ctr.setInitialValue(0);
        pnd_Tiaa_Int_Gross.setInitialValue(0);
        pnd_Tiaa_Int_Income.setInitialValue(0);
        pnd_Tiaa_Int_Ivc.setInitialValue(0);
        pnd_Tiaa_Int_Tax.setInitialValue(0);
        pnd_Tiaa_Int_St_Tax.setInitialValue(0);
        pnd_Tiaa_Int_Reported.setInitialValue(0);
        pnd_Tiaa_Int_Miss_Tin.setInitialValue(0);
        pnd_Tiaa_Int_Miss_Nad.setInitialValue(0);
        pnd_Tiaa_Int_Miss_Hld.setInitialValue(0);
        pnd_Tiaa_Int_Tot_Hold.setInitialValue(0);
        pnd_Tiaa_R___Ctr.setInitialValue(0);
        pnd_Tiaa_R___Gross.setInitialValue(0);
        pnd_Tiaa_R___Income.setInitialValue(0);
        pnd_Tiaa_R___Ivc.setInitialValue(0);
        pnd_Tiaa_R___Tax.setInitialValue(0);
        pnd_Tiaa_R___St_Tax.setInitialValue(0);
        pnd_Tiaa_R___Lc_Tax.setInitialValue(0);
        pnd_Tiaa_R___9b_Amt.setInitialValue(0);
        pnd_Tiaa_R___Reported.setInitialValue(0);
        pnd_Tiaa_R___Miss_Tin.setInitialValue(0);
        pnd_Tiaa_R___Miss_Nad.setInitialValue(0);
        pnd_Tiaa_R___Miss_Hld.setInitialValue(0);
        pnd_Tiaa_R___Tot_Hold.setInitialValue(0);
        pnd_Tiaa_Tot_Ctr.setInitialValue(0);
        pnd_Tiaa_Tot_Gross.setInitialValue(0);
        pnd_Tiaa_Tot_Income.setInitialValue(0);
        pnd_Tiaa_Tot_Ivc.setInitialValue(0);
        pnd_Tiaa_Tot_Tax.setInitialValue(0);
        pnd_Tiaa_Tot_St_Tax.setInitialValue(0);
        pnd_Tiaa_Tot_Reported.setInitialValue(0);
        pnd_Tiaa_Tot_Miss_Tin.setInitialValue(0);
        pnd_Tiaa_Tot_Miss_Nad.setInitialValue(0);
        pnd_Tiaa_Tot_Miss_Hld.setInitialValue(0);
        pnd_Tiaa_Tot_Tot_Hold.setInitialValue(0);
        pnd_Tcii_Int_Ctr.setInitialValue(0);
        pnd_Tcii_Int_Gross.setInitialValue(0);
        pnd_Tcii_Int_Income.setInitialValue(0);
        pnd_Tcii_Int_Ivc.setInitialValue(0);
        pnd_Tcii_Int_Tax.setInitialValue(0);
        pnd_Tcii_Int_St_Tax.setInitialValue(0);
        pnd_Tcii_Int_Reported.setInitialValue(0);
        pnd_Tcii_Int_Miss_Tin.setInitialValue(0);
        pnd_Tcii_Int_Miss_Nad.setInitialValue(0);
        pnd_Tcii_Int_Miss_Hld.setInitialValue(0);
        pnd_Tcii_Int_Tot_Hold.setInitialValue(0);
        pnd_Tcii_R___Ctr.setInitialValue(0);
        pnd_Tcii_R___Gross.setInitialValue(0);
        pnd_Tcii_R___Income.setInitialValue(0);
        pnd_Tcii_R___Ivc.setInitialValue(0);
        pnd_Tcii_R___Tax.setInitialValue(0);
        pnd_Tcii_R___St_Tax.setInitialValue(0);
        pnd_Tcii_R___Lc_Tax.setInitialValue(0);
        pnd_Tcii_R___9b_Amt.setInitialValue(0);
        pnd_Tcii_R___Reported.setInitialValue(0);
        pnd_Tcii_R___Miss_Tin.setInitialValue(0);
        pnd_Tcii_R___Miss_Nad.setInitialValue(0);
        pnd_Tcii_R___Miss_Hld.setInitialValue(0);
        pnd_Tcii_R___Tot_Hold.setInitialValue(0);
        pnd_Tcii_Tot_Ctr.setInitialValue(0);
        pnd_Tcii_Tot_Gross.setInitialValue(0);
        pnd_Tcii_Tot_Income.setInitialValue(0);
        pnd_Tcii_Tot_Ivc.setInitialValue(0);
        pnd_Tcii_Tot_Tax.setInitialValue(0);
        pnd_Tcii_Tot_St_Tax.setInitialValue(0);
        pnd_Tcii_Tot_Lc_Tax.setInitialValue(0);
        pnd_Tcii_Tot_9b_Amt.setInitialValue(0);
        pnd_Tcii_Tot_Reported.setInitialValue(0);
        pnd_Tcii_Tot_Miss_Tin.setInitialValue(0);
        pnd_Tcii_Tot_Miss_Nad.setInitialValue(0);
        pnd_Tcii_Tot_Miss_Hld.setInitialValue(0);
        pnd_Fsbc_Int_Ctr.setInitialValue(0);
        pnd_Fsbc_Int_Gross.setInitialValue(0);
        pnd_Fsbc_Int_Income.setInitialValue(0);
        pnd_Fsbc_Int_Ivc.setInitialValue(0);
        pnd_Fsbc_Int_Tax.setInitialValue(0);
        pnd_Fsbc_Int_St_Tax.setInitialValue(0);
        pnd_Fsbc_Int_Reported.setInitialValue(0);
        pnd_Fsbc_Int_Miss_Tin.setInitialValue(0);
        pnd_Fsbc_Int_Miss_Nad.setInitialValue(0);
        pnd_Fsbc_Int_Miss_Hld.setInitialValue(0);
        pnd_Fsbc_Int_Tot_Hold.setInitialValue(0);
        pnd_Fsbc_R___Ctr.setInitialValue(0);
        pnd_Fsbc_R___Gross.setInitialValue(0);
        pnd_Fsbc_R___Income.setInitialValue(0);
        pnd_Fsbc_R___Ivc.setInitialValue(0);
        pnd_Fsbc_R___Tax.setInitialValue(0);
        pnd_Fsbc_R___St_Tax.setInitialValue(0);
        pnd_Fsbc_R___Lc_Tax.setInitialValue(0);
        pnd_Fsbc_R___9b_Amt.setInitialValue(0);
        pnd_Fsbc_R___Reported.setInitialValue(0);
        pnd_Fsbc_R___Miss_Tin.setInitialValue(0);
        pnd_Fsbc_R___Miss_Nad.setInitialValue(0);
        pnd_Fsbc_R___Miss_Hld.setInitialValue(0);
        pnd_Fsbc_R___Tot_Hold.setInitialValue(0);
        pnd_Fsbc_Tot_Ctr.setInitialValue(0);
        pnd_Fsbc_Tot_Gross.setInitialValue(0);
        pnd_Fsbc_Tot_Income.setInitialValue(0);
        pnd_Fsbc_Tot_Ivc.setInitialValue(0);
        pnd_Fsbc_Tot_Tax.setInitialValue(0);
        pnd_Fsbc_Tot_St_Tax.setInitialValue(0);
        pnd_Fsbc_Tot_Lc_Tax.setInitialValue(0);
        pnd_Fsbc_Tot_9b_Amt.setInitialValue(0);
        pnd_Fsbc_Tot_Reported.setInitialValue(0);
        pnd_Fsbc_Tot_Miss_Tin.setInitialValue(0);
        pnd_Fsbc_Tot_Miss_Nad.setInitialValue(0);
        pnd_Fsbc_Tot_Miss_Hld.setInitialValue(0);
        pnd_Fsbc_Tot_Tot_Hold.setInitialValue(0);
        pnd_Trst_Int_Ctr.setInitialValue(0);
        pnd_Trst_Int_Gross.setInitialValue(0);
        pnd_Trst_Int_Income.setInitialValue(0);
        pnd_Trst_Int_Ivc.setInitialValue(0);
        pnd_Trst_Int_Tax.setInitialValue(0);
        pnd_Trst_Int_St_Tax.setInitialValue(0);
        pnd_Trst_Int_Reported.setInitialValue(0);
        pnd_Trst_Int_Miss_Tin.setInitialValue(0);
        pnd_Trst_Int_Miss_Nad.setInitialValue(0);
        pnd_Trst_Int_Miss_Hld.setInitialValue(0);
        pnd_Trst_Int_Tot_Hold.setInitialValue(0);
        pnd_Trst_R___Ctr.setInitialValue(0);
        pnd_Trst_R___Gross.setInitialValue(0);
        pnd_Trst_R___Income.setInitialValue(0);
        pnd_Trst_R___Ivc.setInitialValue(0);
        pnd_Trst_R___Tax.setInitialValue(0);
        pnd_Trst_R___St_Tax.setInitialValue(0);
        pnd_Trst_R___Lc_Tax.setInitialValue(0);
        pnd_Trst_R___9b_Amt.setInitialValue(0);
        pnd_Trst_R___Reported.setInitialValue(0);
        pnd_Trst_R___Miss_Tin.setInitialValue(0);
        pnd_Trst_R___Miss_Nad.setInitialValue(0);
        pnd_Trst_R___Miss_Hld.setInitialValue(0);
        pnd_Trst_R___Tot_Hold.setInitialValue(0);
        pnd_Trst_Tot_Ctr.setInitialValue(0);
        pnd_Trst_Tot_Gross.setInitialValue(0);
        pnd_Trst_Tot_Income.setInitialValue(0);
        pnd_Trst_Tot_Ivc.setInitialValue(0);
        pnd_Trst_Tot_Tax.setInitialValue(0);
        pnd_Trst_Tot_St_Tax.setInitialValue(0);
        pnd_Trst_Tot_Reported.setInitialValue(0);
        pnd_Trst_Tot_Miss_Tin.setInitialValue(0);
        pnd_Trst_Tot_Miss_Nad.setInitialValue(0);
        pnd_Trst_Tot_Miss_Hld.setInitialValue(0);
        pnd_Trst_Tot_Tot_Hold.setInitialValue(0);
        pnd_Grand_Tot_Ctr.setInitialValue(0);
        pnd_Grand_Tot_Gross.setInitialValue(0);
        pnd_Grand_Tot_Income.setInitialValue(0);
        pnd_Grand_Tot_Ivc.setInitialValue(0);
        pnd_Grand_Tot_Tax.setInitialValue(0);
        pnd_Grand_Tot_St_Tax.setInitialValue(0);
        pnd_Grand_Tot_Reported.setInitialValue(0);
        pnd_Grand_Tot_Miss_Tin.setInitialValue(0);
        pnd_Grand_Tot_Miss_Nad.setInitialValue(0);
        pnd_Grand_Tot_Miss_Hld.setInitialValue(0);
        pnd_Ws_Recs_Written.setInitialValue(0);
        pnd_Ws_Subtotal_Rec_Ctr.setInitialValue(0);
        pnd_Ws_Subtotal_State_Tot_1.setInitialValue(0);
        pnd_Ws_Subtotal_State_Tot_10.setInitialValue(0);
        pnd_Ws_Subtotal_Local_Tot_1.setInitialValue(0);
        pnd_Ws_Subtotal_State_Tot_11.setInitialValue(0);
        pnd_Ws_Subtotal_State_Tot_B.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3310() throws Exception
    {
        super("Twrp3310");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3310|Main");
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) LS = 133 PS = 60;//Natural: FORMAT ( 01 ) LS = 133 PS = 60;//Natural: FORMAT ( 02 ) LS = 133 PS = 60
                //*  #GEO-CODE-HALF (1)  :=
                //*    'ALAKAZARCA06COCTDEDCFLGA13HIIDILINIAKSKYLAMEMDMAMIMNMSMOMTNE'
                //*  #GEO-CODE-HALF (2)  :=
                //*    'NVNHNJNMNYNCNDOHOKORPAPRRI44SCSDTNTXUTVTVAVI53WAWVWIWY585960'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  11-13-2008
                                                                                                                                                                          //Natural: PERFORM ENVIRONMENT-IDENTIFICATION
                sub_Environment_Identification();
                if (condition(Global.isEscape())) {return;}
                //*  1991 IF COMBINED FILING
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Combined_Filer().setValue("1");                                                                                       //Natural: MOVE '1' TO IRS-A-COMBINED-FILER
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                getReports().write(0, "INPUT PARMS ARE","=",pnd_Input_Parm_Pnd_Form_A,"=",pnd_Input_Parm_Pnd_Tax_Year,"=",pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok,       //Natural: WRITE 'INPUT PARMS ARE' '=' #FORM-A '=' #INPUT-PARM.#TAX-YEAR '=' #NOT-FIRST-TIME-RUN-OK '=' #TEST-RUN '=' #COMPANY
                    "=",pnd_Input_Parm_Pnd_Test_Run,"=",pnd_Input_Parm_Pnd_Company);
                if (Global.isEscape()) return;
                if (condition(pnd_Input_Parm_Pnd_Tax_Year.equals(getZero())))                                                                                             //Natural: IF #INPUT-PARM.#TAX-YEAR = 0
                {
                    pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #WS-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Tax_Year.setValue(pnd_Input_Parm_Pnd_Tax_Year);                                                                                                //Natural: ASSIGN #WS-TAX-YEAR := #INPUT-PARM.#TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  11-13-2008
                if (condition(pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return().equals("00") && pdaTwra0001.getTwra0001_Pnd_Prod_Test().equals("TEST")))                      //Natural: IF #TWRN0001-RETURN = '00' AND TWRA0001.#PROD-TEST = 'TEST'
                {
                    if (condition(pnd_Input_Parm_Pnd_Test_Run.notEquals("Y")))                                                                                            //Natural: IF #INPUT-PARM.#TEST-RUN NE 'Y'
                    {
                        pnd_Input_Parm_Pnd_Test_Run.setValue("Y");                                                                                                        //Natural: ASSIGN #TEST-RUN := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  11-13-2008
                }                                                                                                                                                         //Natural: END-IF
                //*  12-09-00 FRANK
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
                sub_Load_State_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  12-09-00 FRANK
                pnd_Y.reset();                                                                                                                                            //Natural: RESET #Y
                FOR01:                                                                                                                                                    //Natural: FOR #Z = 00 TO 99
                for (pnd_Z.setValue(0); condition(pnd_Z.lessOrEqual(99)); pnd_Z.nadd(1))
                {
                    if (condition(pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_Z.getInt() + 1).notEquals(" ")))                                                 //Natural: IF #WS-STATE-COMB-FED-IND ( #Z ) NE ' '
                    {
                        pnd_Y.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #Y
                        state_Totals_State_Code.getValue(pnd_Y).setValue(pnd_State_Table_Pnd_Ws_State_Seq_Nbr.getValue(pnd_Z.getInt() + 1));                              //Natural: ASSIGN STATE-CODE ( #Y ) := #WS-STATE-SEQ-NBR ( #Z )
                        state_Name.getValue(pnd_Y).setValue(pnd_State_Table_Pnd_Ws_State_Full_Name.getValue(pnd_Z.getInt() + 1));                                         //Natural: ASSIGN STATE-NAME ( #Y ) := #WS-STATE-FULL-NAME ( #Z )
                        state_Alpha.getValue(pnd_Y).setValue(pnd_State_Table_Pnd_Ws_State_Alpha.getValue(pnd_Z.getInt() + 1));                                            //Natural: ASSIGN STATE-ALPHA ( #Y ) := #WS-STATE-ALPHA ( #Z )
                        //*  VIKRAM STARTS
                        if (condition(state_Alpha.getValue(pnd_Y).equals("NM")))                                                                                          //Natural: IF STATE-ALPHA ( #Y ) = 'NM'
                        {
                            pnd_Temp_State_Id_20_Nm.setValue(pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_Z.getInt() + 1));                                      //Natural: ASSIGN #TEMP-STATE-ID-20-NM := #WS-STATE-TAX-ID-TIAA ( #Z )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(state_Alpha.getValue(pnd_Y).equals("MD")))                                                                                          //Natural: IF STATE-ALPHA ( #Y ) = 'MD'
                        {
                            pnd_Temp_State_Id_20_Md.setValue(pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_Z.getInt() + 1));                                      //Natural: ASSIGN #TEMP-STATE-ID-20-MD := #WS-STATE-TAX-ID-TIAA ( #Z )
                            //*  VIKRAM ENDS
                        }                                                                                                                                                 //Natural: END-IF
                        state_Index.getValue(pnd_Y).setValue(pnd_Y);                                                                                                      //Natural: ASSIGN STATE-INDEX ( #Y ) := #Y
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Max_States.setValue(pnd_Y);                                                                                                                           //Natural: ASSIGN #MAX-STATES := #Y
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 RECORD EXTRACT-NPD-FILE-0
                while (condition(getWorkFiles().read(1, ldaTwrl3300.getExtract_Npd_File_0())))
                {
                    //*  12-09-00 FRANK
                    pnd_Ws_Recs_Read.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-RECS-READ
                    //*  12-06-99 FRANK
                    //*  12-11-00 FRANK
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_Reject_Ind().equals("Y")))                                                                //Natural: IF EXTRACT-IRS-REJECT-IND = 'Y'
                    {
                        pnd_Ws_Rejects_File.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-REJECTS-FILE := 'Y'
                        pnd_Ws_Rejects.setValue("- REJECTS");                                                                                                             //Natural: ASSIGN #WS-REJECTS := '- REJECTS'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  INSERTED 08-13-98 BY FRANK
                    //*   DY1 FIX - THE NEXT 3 LINES
                    //*  IF (EXTRACT-PRDCT-CDE = 'T' OR= 'C' OR= 'L' OR = 'S' OR = 'X')
                    //*  EINCHG
                    if (condition((((((((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("T") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("C"))  //Natural: IF ( EXTRACT-PRDCT-CDE = 'T' OR = 'C' OR = 'L' OR = 'S' OR = 'F' OR = 'X' OR = 'A' ) AND ( EXTRACT-TYPE-CDE = 'I' OR = 'R' )
                        || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("L")) || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("S")) 
                        || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("F")) || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("X")) 
                        || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("A")) && (ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I") 
                        || ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R")))))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*    WRITE 'PRODUCT CODE NOT = T,C,L,S,X AND TYPE NOT = I,R'   /* DY1
                        //*    WRITE 'PRODUCT CODE NOT = T,C,L,S,F,X AND TYPE NOT = I,R' /* DY1
                        //*  EINCHG
                        getReports().write(0, "PRODUCT CODE NOT = T,C,L,S,F,X,A AND TYPE NOT = I,R","CONTRACT PAYEE=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr()); //Natural: WRITE 'PRODUCT CODE NOT = T,C,L,S,F,X,A AND TYPE NOT = I,R' 'CONTRACT PAYEE=' EXTRACT-CNTRCT-PY-NMBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  12-11-00 FRANK
                    if (condition(pnd_Ws_Recs_Read.equals(1)))                                                                                                            //Natural: IF #WS-RECS-READ = 1
                    {
                        //*  DY1
                        //*  DY1
                        //*  EINCHG
                        //*  EINCHG
                        short decideConditionsMet1425 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF EXTRACT-PRDCT-CDE;//Natural: VALUE 'C'
                        if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("C"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("CREF");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'CREF'
                        }                                                                                                                                                 //Natural: VALUE 'L'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("L"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("LIFE");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'LIFE'
                        }                                                                                                                                                 //Natural: VALUE 'T'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("T"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("TIAA");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'TIAA'
                        }                                                                                                                                                 //Natural: VALUE 'S'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("S"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("TCII");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'TCII'
                        }                                                                                                                                                 //Natural: VALUE 'F'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("F"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("FSBC");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'FSBC'
                        }                                                                                                                                                 //Natural: VALUE 'X'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("X"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("TRST");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'TRST'
                        }                                                                                                                                                 //Natural: VALUE 'A'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("A"))))
                        {
                            decideConditionsMet1425++;
                            pnd_Ws_Save_Prdct_Name.setValue("ADMN");                                                                                                      //Natural: ASSIGN #WS-SAVE-PRDCT-NAME := 'ADMN'
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_First_Rec_Sw.notEquals("F") && (ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind().notEquals(pnd_Save_Prod_Ind)                   //Natural: IF #FIRST-REC-SW NE 'F' AND ( EXTRACT-PROD-IND NE #SAVE-PROD-IND OR EXTRACT-TYPE-CDE NE #SAVE-TYPE-CODE )
                        || ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().notEquals(pnd_Save_Type_Code))))
                    {
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees().setValue(b_Record_Ctr);                                                                    //Natural: MOVE B-RECORD-CTR TO IRS-C-NUMBER-OF-PAYEES
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1().setValue(contr_Tot_1);                                                                      //Natural: MOVE CONTR-TOT-1 TO IRS-C-CONTROL-TOTAL-1
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2().setValue(contr_Tot_2);                                                                      //Natural: MOVE CONTR-TOT-2 TO IRS-C-CONTROL-TOTAL-2
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_3().setValue(contr_Tot_3);                                                                      //Natural: MOVE CONTR-TOT-3 TO IRS-C-CONTROL-TOTAL-3
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4().setValue(contr_Tot_4);                                                                      //Natural: MOVE CONTR-TOT-4 TO IRS-C-CONTROL-TOTAL-4
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5().setValue(contr_Tot_5);                                                                      //Natural: MOVE CONTR-TOT-5 TO IRS-C-CONTROL-TOTAL-5
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_6().setValue(contr_Tot_6);                                                                      //Natural: MOVE CONTR-TOT-6 TO IRS-C-CONTROL-TOTAL-6
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_7().setValue(contr_Tot_7);                                                                      //Natural: MOVE CONTR-TOT-7 TO IRS-C-CONTROL-TOTAL-7
                        //* 02-20-99 FC
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_A().setValue(contr_Tot_8);                                                                      //Natural: MOVE CONTR-TOT-8 TO IRS-C-CONTROL-TOTAL-A
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9().setValue(contr_Tot_9);                                                                      //Natural: MOVE CONTR-TOT-9 TO IRS-C-CONTROL-TOTAL-9
                        //*  RCC
                        ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B().setValue(contr_Tot_B);                                                                      //Natural: MOVE CONTR-TOT-B TO IRS-C-CONTROL-TOTAL-B
                        getWorkFiles().write(2, false, ldaTwrl3323.getIrs_C_Out_Tape());                                                                                  //Natural: WRITE WORK FILE 02 IRS-C-OUT-TAPE
                        //*  12-11-00 FRANK
                        c_Record_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO C-RECORD-CTR
                        //* *  IF #SAVE-TYPE-CODE NE 'I'                         /* 03-19-03 FRANK
                        //*  04-17-03 FRANK
                        //*  1991
                        if (condition(pnd_Save_Type_Code.equals("I") || pnd_Save_Type_Code.equals("R")))                                                                  //Natural: IF #SAVE-TYPE-CODE = 'I' OR = 'R'
                        {
                            FOR02:                                                                                                                                        //Natural: FOR #K = 1 TO #MAX-STATES
                            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Max_States)); pnd_K.nadd(1))
                            {
                                if (condition(state_Totals_St_B_Rec_Ctr.getValue(pnd_K).greater(getZero())))                                                              //Natural: IF ST-B-REC-CTR ( #K ) > 0
                                {
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().setValue(state_Totals_St_B_Rec_Ctr.getValue(pnd_K));                           //Natural: MOVE ST-B-REC-CTR ( #K ) TO IRS-K-NUMBER-OF-PAYEES
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().setValue(state_Totals_State_Tot_1.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-1 ( #K ) TO IRS-K-CONTROL-TOTAL-1
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().setValue(state_Totals_State_Tot_2.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-2 ( #K ) TO IRS-K-CONTROL-TOTAL-2
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3().setValue(state_Totals_State_Tot_3.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-3 ( #K ) TO IRS-K-CONTROL-TOTAL-3
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().setValue(state_Totals_State_Tot_4.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-4 ( #K ) TO IRS-K-CONTROL-TOTAL-4
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().setValue(state_Totals_State_Tot_5.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-5 ( #K ) TO IRS-K-CONTROL-TOTAL-5
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6().setValue(state_Totals_State_Tot_6.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-6 ( #K ) TO IRS-K-CONTROL-TOTAL-6
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7().setValue(state_Totals_State_Tot_7.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-7 ( #K ) TO IRS-K-CONTROL-TOTAL-7
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A().setValue(state_Totals_State_Tot_8.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-8 ( #K ) TO IRS-K-CONTROL-TOTAL-A
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().setValue(state_Totals_State_Tot_9.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-9 ( #K ) TO IRS-K-CONTROL-TOTAL-9
                                    //* 03-03-99 FC
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().setValue(state_Totals_State_Tot_10.getValue(pnd_K));                            //Natural: MOVE STATE-TOT-10 ( #K ) TO IRS-K-ST-TAX-WITHHELD
                                    //*  RCC
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B().setValue(state_Totals_State_Tot_B.getValue(pnd_K));                             //Natural: MOVE STATE-TOT-B ( #K ) TO IRS-K-CONTROL-TOTAL-B
                                    //* *  COMMENTED-OUT 02-25-04 BY FRANK
                                    //* *        MOVE STATE-TOT-11 (#K) TO IRS-K-CONTROL-TOTAL-B /*03-01-99 FC
                                    //*  12-29-00 FRANK
                                    ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().setValueEdited(state_Totals_State_Code.getValue(pnd_K),new ReportEditMask("99"));    //Natural: MOVE EDITED STATE-CODE ( #K ) ( EM = 99 ) TO IRS-K-STATE-CODE
                                    //*  02-11-00 FRANK
                                    if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().equals(getZero())))                                               //Natural: IF IRS-K-ST-TAX-WITHHELD = 0
                                    {
                                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld_A().reset();                                                                  //Natural: RESET IRS-K-ST-TAX-WITHHELD-A
                                    }                                                                                                                                     //Natural: END-IF
                                    getWorkFiles().write(2, false, ldaTwrl3325.getIrs_K_Out_Tape());                                                                      //Natural: WRITE WORK FILE 02 IRS-K-OUT-TAPE
                                    //*  12-11-00 FRANK
                                    k_Record_Ctr.nadd(1);                                                                                                                 //Natural: ADD 1 TO K-RECORD-CTR
                                                                                                                                                                          //Natural: PERFORM STATE-SUB-TOTALS
                                    sub_State_Sub_Totals();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(Map.getDoInput())) {return;}
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  1991
                        //*  02-17-04 FRANK
                        //*  02-17-04 FRANK
                        //*  RCC
                        //*  02-03-00 FRANK
                        //*  RCC
                        b_Record_Ctr.reset();                                                                                                                             //Natural: RESET B-RECORD-CTR ST-B-REC-CTR ( * ) CONTR-TOT-1 STATE-TOT-1 ( * ) CONTR-TOT-2 STATE-TOT-2 ( * ) CONTR-TOT-3 STATE-TOT-3 ( * ) CONTR-TOT-4 STATE-TOT-4 ( * ) CONTR-TOT-5 STATE-TOT-5 ( * ) CONTR-TOT-6 STATE-TOT-6 ( * ) CONTR-TOT-7 STATE-TOT-7 ( * ) CONTR-TOT-8 STATE-TOT-8 ( * ) CONTR-TOT-9 STATE-TOT-9 ( * ) CONTR-TOT-10 STATE-TOT-10 ( * ) CONTR-TOT-11 STATE-TOT-11 ( * ) CONTR-TOT-12 STATE-TOT-12 ( * ) CONTR-TOT-13 STATE-TOT-13 ( * ) CONTR-TOT-B STATE-TOT-B ( * ) LOCAL-TOT-1 ( * ) #WS-SUBTOTAL-REC-CTR #WS-SUBTOTAL-STATE-TOT-1 #WS-SUBTOTAL-STATE-TOT-10 #WS-SUBTOTAL-LOCAL-TOT-1 #WS-SUBTOTAL-STATE-TOT-11 #WS-SUBTOTAL-STATE-TOT-B
                        state_Totals_St_B_Rec_Ctr.getValue("*").reset();
                        contr_Tot_1.reset();
                        state_Totals_State_Tot_1.getValue("*").reset();
                        contr_Tot_2.reset();
                        state_Totals_State_Tot_2.getValue("*").reset();
                        contr_Tot_3.reset();
                        state_Totals_State_Tot_3.getValue("*").reset();
                        contr_Tot_4.reset();
                        state_Totals_State_Tot_4.getValue("*").reset();
                        contr_Tot_5.reset();
                        state_Totals_State_Tot_5.getValue("*").reset();
                        contr_Tot_6.reset();
                        state_Totals_State_Tot_6.getValue("*").reset();
                        contr_Tot_7.reset();
                        state_Totals_State_Tot_7.getValue("*").reset();
                        contr_Tot_8.reset();
                        state_Totals_State_Tot_8.getValue("*").reset();
                        contr_Tot_9.reset();
                        state_Totals_State_Tot_9.getValue("*").reset();
                        contr_Tot_10.reset();
                        state_Totals_State_Tot_10.getValue("*").reset();
                        contr_Tot_11.reset();
                        state_Totals_State_Tot_11.getValue("*").reset();
                        contr_Tot_12.reset();
                        state_Totals_State_Tot_12.getValue("*").reset();
                        contr_Tot_13.reset();
                        state_Totals_State_Tot_13.getValue("*").reset();
                        contr_Tot_B.reset();
                        state_Totals_State_Tot_B.getValue("*").reset();
                        local_Totals_Local_Tot_1.getValue("*").reset();
                        pnd_Ws_Subtotal_Rec_Ctr.reset();
                        pnd_Ws_Subtotal_State_Tot_1.reset();
                        pnd_Ws_Subtotal_State_Tot_10.reset();
                        pnd_Ws_Subtotal_Local_Tot_1.reset();
                        pnd_Ws_Subtotal_State_Tot_11.reset();
                        pnd_Ws_Subtotal_State_Tot_B.reset();
                        //*      #ROLLOVER-FIX
                    }                                                                                                                                                     //Natural: END-IF
                    //*  11-26-02 FRANK
                    if (condition(pnd_First_Rec_Sw.equals("F") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind().notEquals(pnd_Save_Prod_Ind) ||                    //Natural: IF #FIRST-REC-SW = 'F' OR EXTRACT-PROD-IND NE #SAVE-PROD-IND OR EXTRACT-TYPE-CDE NE #SAVE-TYPE-CODE
                        ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().notEquals(pnd_Save_Type_Code)))
                    {
                        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind());                                       //Natural: ASSIGN #TWRACOM2.#COMP-CODE := EXTRACT-PROD-IND
                        //*  11-26-02 FRANK
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'R'
                        {
                            pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                      //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                              //Natural: IF EXTRACT-TYPE-CDE = 'I'
                            {
                                pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(2);                                                                                  //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 2
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                             //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #WS-TAX-YEAR
                        //*  11-26-02 FRANK
                        //*  03-19-03 FRANK
                        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
                            pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                               //Natural: ASSIGN IRS-A-PAYER-EIN := #FED-ID
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_Control().setValue(" ");                                                                           //Natural: ASSIGN IRS-A-PAYER-NAME-CONTROL := ' '
                        pnd_Ws_State_Product.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind());                                                              //Natural: ASSIGN #WS-STATE-PRODUCT := EXTRACT-PROD-IND
                        //*    IRS-A-PAYER-NAME-1  :=  #TWRACOM2.#COMP-NAME
                        //*    IRS-A-PAYER-NAME-2  :=  ' '
                        //*    IF #TWRACOM2.#COMP-CODE = 'X'
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                      //Natural: ASSIGN IRS-A-PAYER-NAME-1 := #TWRACOM2.#COMP-PAYER-A
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_B());                                      //Natural: ASSIGN IRS-A-PAYER-NAME-2 := #TWRACOM2.#COMP-PAYER-B
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Shipping_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                     //Natural: ASSIGN IRS-A-PAYER-SHIPPING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                //Natural: ASSIGN IRS-A-PAYER-CITY := #TWRACOM2.#CITY
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                              //Natural: ASSIGN IRS-A-PAYER-STATE := #TWRACOM2.#STATE
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                //Natural: ASSIGN IRS-A-PAYER-ZIP := #TWRACOM2.#ZIP-9
                        //*    END-IF
                        //*  -----------------START OF 'T' RECORD FORMATTING-------------------
                        //*  EINCHG
                        //*  EINCHG
                        if (condition(pnd_Ws_Tax_Year.greaterOrEqual(2018)))                                                                                              //Natural: IF #WS-TAX-YEAR GE 2018
                        {
                            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("A");                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'A'
                            //*  EINCHG
                            //*  03-19-03 FRANK
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
                            //*  EINCHG
                        }                                                                                                                                                 //Natural: END-IF
                        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                             //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := #WS-TAX-YEAR
                        //*  03-19-03 FRANK
                        //*  11-26-02 FRANK
                        //*  11-26-02 FRANK
                        //*  11-26-02 FRANK
                        //*  11-26-02 FRANK
                        //*  11-28-05
                        //*  11-28-05
                        //*  11-28-05
                        //*  11-28-05
                        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
                            pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                  //Natural: ASSIGN IRS-T-TRANSMITTER-NAME := #COMP-TRANS-A
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name_Continued().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                        //Natural: ASSIGN IRS-T-TRANSMITTER-NAME-CONTINUED := #COMP-TRANS-B
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                      //Natural: ASSIGN IRS-T-COMPANY-NAME := #COMP-TRANS-A
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name_Continued().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                            //Natural: ASSIGN IRS-T-COMPANY-NAME-CONTINUED := #COMP-TRANS-B
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Mailing_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                    //Natural: ASSIGN IRS-T-COMPANY-MAILING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                              //Natural: ASSIGN IRS-T-COMPANY-CITY := #TWRACOM2.#CITY
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                            //Natural: ASSIGN IRS-T-COMPANY-STATE := #TWRACOM2.#STATE
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Zip_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                         //Natural: ASSIGN IRS-T-COMPANY-ZIP-CODE := #TWRACOM2.#ZIP-9
                        //*  01-15-03 FRANK
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                      //Natural: ASSIGN IRS-T-CONTACT-NAME := #CONTACT-NAME
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                //Natural: ASSIGN IRS-T-CONTACT-PHONE-N-EXTENSION := #PHONE
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Email_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                       //Natural: ASSIGN IRS-T-CONTACT-EMAIL-ADDRESS := #CONTACT-EMAIL-ADDR
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitters_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                        //Natural: ASSIGN IRS-T-TRANSMITTERS-TIN := #FED-ID
                        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                              //Natural: ASSIGN IRS-T-TRANSMITTER-CONTROL-CODE := #COMP-TCC
                        //*  ------------------END OF 'T' RECORD FORMATTING--------------------
                        //*  -----------------START OF 'A' RECORD FORMATTING-------------------
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'R'
                        {
                            //*  10-01-2010
                            ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("9 ");                                                                          //Natural: MOVE '9 ' TO IRS-A-TYPE-OF-RETURN
                            //*  RCC ADDED B
                            ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12459AB");                                                                  //Natural: MOVE '12459AB' TO IRS-A-AMOUNT-INDICATORS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  10-01-2010
                            ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("6 ");                                                                          //Natural: MOVE '6 ' TO IRS-A-TYPE-OF-RETURN
                            ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12345");                                                                    //Natural: MOVE '12345' TO IRS-A-AMOUNT-INDICATORS
                            //*  01-15-03 FRANK
                            //*  1993
                        }                                                                                                                                                 //Natural: END-IF
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Phone_No().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                           //Natural: ASSIGN IRS-A-PAYER-PHONE-NO := #PHONE
                        ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payment_Year().setValue(pnd_Ws_Tax_Year);                                                                     //Natural: ASSIGN IRS-A-PAYMENT-YEAR := #WS-TAX-YEAR
                        getWorkFiles().write(2, false, ldaTwrl3321.getIrs_A_Out_Tape());                                                                                  //Natural: WRITE WORK FILE 02 IRS-A-OUT-TAPE
                        a_Record_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO A-RECORD-CTR
                        //*  1995
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ------------------END OF 'A' RECORD FORMATTING--------------------
                    //*  -----------------START OF 'B' RECORD FORMATTING-------------------
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Year().setValue(pnd_Ws_Tax_Year);                                                                         //Natural: ASSIGN IRS-B-PAYMENT-YEAR := #WS-TAX-YEAR
                    pnd_First_Rec_Sw.setValue("N");                                                                                                                       //Natural: MOVE 'N' TO #FIRST-REC-SW
                    pnd_Save_Prod_Ind.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind());                                                                     //Natural: MOVE EXTRACT-PROD-IND TO #SAVE-PROD-IND
                    //*  12-06-99 FRANK
                    pnd_Save_Type_Code.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde());                                                                    //Natural: MOVE EXTRACT-TYPE-CDE TO #SAVE-TYPE-CODE
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Type_Of_Tin().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Ind());                                       //Natural: ASSIGN IRS-B-TYPE-OF-TIN := EXTRACT-ID-IND
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr());                                 //Natural: MOVE EXTRACT-ID-NMBR TO IRS-B-TAXPAYER-ID-NUMB
                    //*  11-13-00 FRANK
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_1099i_Or_1099r().reset();                                                                                         //Natural: RESET IRS-B-1099I-OR-1099R
                    //*  1991
                    //*  02-15-00 FRANK
                    //*  RCC
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind().reset();                                                                                      //Natural: RESET IRS-B-TXBLE-NOT-DET-IND
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                           //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-1 := EXTRACT-FED-GROSS-AMT
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Res_Irr_Amt());                             //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-B := EXTRACT-RES-IRR-AMT
                    //*    MOVE  EXTRACT-CNTRCT-PY-NMBR  TO  IRS-B-CONTRACT-PAYEE
                    //*  UNIQUE IRS ID NUMBER RESIDES IN THE CONTRACT-PAYEE FIELD
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Contract_Payee().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_B_Contract_Payee());                      //Natural: ASSIGN IRS-B-CONTRACT-PAYEE := EXTRACT-IRS-B-CONTRACT-PAYEE
                    //*  11-17-99 FRANK
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R") && ldaTwrl3300.getExtract_Npd_File_0_Extract_Ira_Sep_Ind().notEquals(" "))) //Natural: IF EXTRACT-TYPE-CDE = 'R' AND EXTRACT-IRA-SEP-IND NE ' '
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Ira_Sep_Indicator().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Ira_Sep_Ind());                        //Natural: ASSIGN IRS-B-IRA-SEP-INDICATOR := EXTRACT-IRA-SEP-IND
                        //* * UNCOMMENTED & COMMENTED NEXT 2 LINES
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                       //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-A := EXTRACT-FED-GROSS-AMT
                        //*    IRS-B-PAYMENT-AMOUNT-A  :=  EXTRACT-ST-WTHHLD-AMT /* 03/08/02 JRR
                        getReports().write(0, "IRA REC==>  PRODUCT:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde(),"CNTRCT-PY:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr(),"FED GROSS:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt(),  //Natural: WRITE 'IRA REC==>  PRODUCT:' EXTRACT-PRDCT-CDE 'CNTRCT-PY:' EXTRACT-CNTRCT-PY-NMBR 'FED GROSS:' EXTRACT-FED-GROSS-AMT
                            new ReportEditMask ("ZZ,ZZZ,ZZ9.99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        short decideConditionsMet1653 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF EXTRACT-PRDCT-CDE;//Natural: VALUE 'C'
                        if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("C"))))
                        {
                            decideConditionsMet1653++;
                            cntr_Cref_Ira.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                                                                //Natural: ADD EXTRACT-FED-GROSS-AMT TO CNTR-CREF-IRA
                            //*      VALUE 'T'
                        }                                                                                                                                                 //Natural: VALUE 'T' , 'A'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("T") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("A"))))
                        {
                            decideConditionsMet1653++;
                            //*  DY1
                            cntr_Tiaa_Ira.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                                                                //Natural: ADD EXTRACT-FED-GROSS-AMT TO CNTR-TIAA-IRA
                        }                                                                                                                                                 //Natural: VALUE 'F'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("F"))))
                        {
                            decideConditionsMet1653++;
                            //*  DY1
                            cntr_Fsbc_Ira.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                                                                //Natural: ADD EXTRACT-FED-GROSS-AMT TO CNTR-FSBC-IRA
                        }                                                                                                                                                 //Natural: VALUE 'X'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("X"))))
                        {
                            decideConditionsMet1653++;
                            cntr_Trst_Ira.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                                                                //Natural: ADD EXTRACT-FED-GROSS-AMT TO CNTR-TRST-IRA
                        }                                                                                                                                                 //Natural: VALUE 'S'
                        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("S"))))
                        {
                            decideConditionsMet1653++;
                            cntr_Tcii_Ira.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt());                                                                //Natural: ADD EXTRACT-FED-GROSS-AMT TO CNTR-TCII-IRA
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  1992
                        //* 03-03-99 FC
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Ira_Sep_Indicator().reset();                                                                                  //Natural: RESET IRS-B-IRA-SEP-INDICATOR IRS-B-PAYMENT-AMOUNT-A
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A().reset();
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Special_Data_Entries().reset();                                                                                   //Natural: RESET IRS-B-SPECIAL-DATA-ENTRIES
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt().less(new DbsDecimal("0.00"))))                                            //Natural: IF EXTRACT-TOT-IVC-CNTRT-AMT < 0.00
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9().setValue(0);                                                                               //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-9 := 0.00
                        getReports().write(0, "TOTAL NEGATIVE IVC 9B ==>","TAX ID:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr(),"PPCN-PAYEE:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr(),"TOTAL IVC AMT:",ldaTwrl3300.getExtract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt(),  //Natural: WRITE 'TOTAL NEGATIVE IVC 9B ==>' 'TAX ID:' EXTRACT-ID-NMBR 'PPCN-PAYEE:' EXTRACT-CNTRCT-PY-NMBR 'TOTAL IVC AMT:' EXTRACT-TOT-IVC-CNTRT-AMT
                            new ReportEditMask ("Z,ZZZ,ZZ9,99"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt());                   //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-9 := EXTRACT-TOT-IVC-CNTRT-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Wthhld_Amt());                          //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-4 := EXTRACT-FED-WTHHLD-AMT
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Wthhld_Amt());                         //Natural: ASSIGN IRS-B-ME-ST-TAX-WITHHELD := EXTRACT-ST-WTHHLD-AMT
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Wthhld_Amt());                        //Natural: ASSIGN IRS-B-ME-LC-TAX-WITHHELD := EXTRACT-LCL-WTHHLD-AMT
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                      //Natural: IF EXTRACT-TYPE-CDE = 'I'
                    {
                        //*  11-16-99 FRANK
                        //*  11-16-99 FRANK
                        //*  11-23-99 FRANK
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2().reset();                                                                                   //Natural: RESET IRS-B-PAYMENT-AMOUNT-2 IRS-B-PAYMENT-AMOUNT-5 IRS-B-ME-ST-TAX-WITHHELD-A
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5().reset();
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A().reset();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Ivc_Ind().equals("U")))                                                                   //Natural: IF EXTRACT-IVC-IND = 'U'
                        {
                            //*  1991
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind().setValue("1");                                                                        //Natural: MOVE '1' TO IRS-B-TXBLE-NOT-DET-IND
                            //* * 10-03-03 COMMENTED-OUT/INSERTED BY FRANK
                            //* *    RESET IRS-B-PAYMENT-AMOUNT-2
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Taxable_Amt());                     //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-2 := EXTRACT-TAXABLE-AMT
                            //*  11-16-99 FRANK
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5().reset();                                                                               //Natural: RESET IRS-B-PAYMENT-AMOUNT-5
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  1991
                            //*  12-08-00 FRANK
                            //*  11-16-99 FRANK
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind().reset();                                                                              //Natural: RESET IRS-B-TXBLE-NOT-DET-IND
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Ivc_Amt());                      //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-5 := EXTRACT-ST-IVC-AMT
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Taxable_Amt());                     //Natural: ASSIGN IRS-B-PAYMENT-AMOUNT-2 := EXTRACT-TAXABLE-AMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF IRS-B-PAYMENT-AMOUNT-1    <  0  /* CHECK FOR NEGATIVE VALUES 1993
                    //*      OR IRS-B-PAYMENT-AMOUNT-2    <  0
                    //*      OR IRS-B-PAYMENT-AMOUNT-4    <  0
                    //*      OR IRS-B-PAYMENT-AMOUNT-5    <  0
                    //*    WRITE (0) '*** NEGATIVE VALUES:' IRS-B-TAXPAYER-ID-NUMB
                    //*      IRS-B-CONTRACT-PAYEE
                    //*      IRS-B-DOCUMENT-CODE
                    //*      IRS-B-PAYMENT-AMOUNT-1
                    //*      IRS-B-PAYMENT-AMOUNT-2
                    //*      IRS-B-PAYMENT-AMOUNT-4
                    //*      IRS-B-PAYMENT-AMOUNT-5
                    //*    ESCAPE TOP
                    //*  END-IF
                    //*  11-13-00 FRANK
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R")))                                                                      //Natural: IF EXTRACT-TYPE-CDE = 'R'
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Document_Code().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde());                        //Natural: ASSIGN IRS-B-DOCUMENT-CODE := EXTRACT-CNTRL-CTGRY-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    //* ** USE NEW ROUTINE TO CONVERT DISTRIBUTION CODE      10/10/2018 VIKRAM
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Date_Of_Payment().reset();                                                                                        //Natural: RESET IRS-B-DATE-OF-PAYMENT
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde().equals("DC")))                                                              //Natural: IF EXTRACT-CNTRL-CTGRY-CDE EQ 'DC'
                    {
                        pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                              //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
                        pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
                        pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("2");                                                                                         //Natural: ASSIGN #TWRADIST.#FUNCTION := '2'
                        pdaTwradist.getPnd_Twradist_Pnd_Tax_Year_A().setValue(pnd_Ws_Tax_Year);                                                                           //Natural: ASSIGN #TWRADIST.#TAX-YEAR-A := #WS-TAX-YEAR
                        pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde());                                  //Natural: ASSIGN #TWRADIST.#IN-DIST := EXTRACT-CNTRL-CTGRY-CDE
                        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist());                                                        //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST
                        if (condition(Global.isEscape())) return;
                        ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                 //Natural: ASSIGN EXTRACT-CNTRL-CTGRY-CDE := #TWRADIST.#OUT-DIST
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde().equals("C")))                                                           //Natural: IF EXTRACT-CNTRL-CTGRY-CDE EQ 'C'
                        {
                            pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                               //Natural: RESET #S-TWRPYMNT-FORM-SD
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.setValue(pnd_Ws_Tax_Year);                                                                     //Natural: MOVE #WS-TAX-YEAR TO #S-TWRPYMNT-TAX-YEAR
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr());                               //Natural: MOVE EXTRACT-ID-NMBR TO #S-TWRPYMNT-TAX-ID-NBR
                            pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Payee_Nbr.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                //Natural: MOVE EXTRACT-CNTRCT-PY-NMBR TO #S-TWRPYMNT-CONTRACT-PAYEE-NBR
                            ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                     //Natural: READ PAY BY TWRPYMNT-FORM-SD FROM #S-TWRPYMNT-FORM-SD
                            (
                            "READ02",
                            new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
                            new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
                            );
                            READ02:
                            while (condition(ldaTwrl9415.getVw_pay().readNextRow("READ02")))
                            {
                                if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Year().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year) || ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr)  //Natural: IF PAY.TWRPYMNT-TAX-YEAR NE #S-TWRPYMNT-TAX-YEAR OR PAY.TWRPYMNT-TAX-ID-NBR NE #S-TWRPYMNT-TAX-ID-NBR OR PAY.TWRPYMNT-CONTRACT-NBR NE #S-TWRPYMNT-CONTRACT-NBR OR PAY.TWRPYMNT-PAYEE-CDE NE #S-TWRPYMNT-PAYEE-CDE
                                    || ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr) || ldaTwrl9415.getPay_Twrpymnt_Payee_Cde().notEquals(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde)))
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde().equals("C")))                                                                //Natural: IF PAY.TWRPYMNT-DISTRIBUTION-CDE EQ 'C'
                                {
                                    FOR03:                                                                                                                                //Natural: FOR #P-S-CNT = 1 TO 24
                                    for (pnd_P_S_Cnt.setValue(1); condition(pnd_P_S_Cnt.lessOrEqual(24)); pnd_P_S_Cnt.nadd(1))
                                    {
                                        if (condition(((ldaTwrl9415.getPay_Twrpymnt_Payment_Type().getValue(pnd_P_S_Cnt).equals("D") && ldaTwrl9415.getPay_Twrpymnt_Settle_Type().getValue(pnd_P_S_Cnt).equals("C"))  //Natural: IF PAY.TWRPYMNT-PAYMENT-TYPE ( #P-S-CNT ) EQ 'D' AND PAY.TWRPYMNT-SETTLE-TYPE ( #P-S-CNT ) EQ 'C' AND ( PAY.TWRPYMNT-PYMNT-STATUS ( #P-S-CNT ) EQ 'C' OR EQ ' ' )
                                            && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_P_S_Cnt).equals("C") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(pnd_P_S_Cnt).equals(" ")))))
                                        {
                                            if (condition(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_P_S_Cnt).notEquals(" ")))                                  //Natural: IF PAY.TWRPYMNT-PYMNT-DTE ( #P-S-CNT ) NE ' '
                                            {
                                                ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Date_Of_Payment().setValue(ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(pnd_P_S_Cnt)); //Natural: ASSIGN IRS-B-DATE-OF-PAYMENT := PAY.TWRPYMNT-PYMNT-DTE ( #P-S-CNT )
                                            }                                                                                                                             //Natural: END-IF
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-FOR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-READ
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  VIKRAM ENDS
                    }                                                                                                                                                     //Natural: END-IF
                    //* *
                    //*  02-17-00 FRANK
                    contr_Tot_1.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO CONTR-TOT-1
                    contr_Tot_2.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO CONTR-TOT-2
                    contr_Tot_4.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO CONTR-TOT-4
                    contr_Tot_5.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO CONTR-TOT-5
                    //*  1991
                    contr_Tot_9.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO CONTR-TOT-9
                    //*  02-20-99 FC
                    contr_Tot_8.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-A TO CONTR-TOT-8
                    //*  1995
                    contr_Tot_10.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                                          //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO CONTR-TOT-10
                    //*  1995
                    contr_Tot_11.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                                          //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO CONTR-TOT-11
                    //*  RCC
                    contr_Tot_B.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B());                                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-B TO CONTR-TOT-B
                    //*  12-09-00 FRANK
                    pnd_K.reset();                                                                                                                                        //Natural: RESET #K IRS-B-FEDERAL-STATE-CODE
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Federal_State_Code().reset();
                    FOR04:                                                                                                                                                //Natural: FOR #Z = 01 TO #MAX-STATES
                    for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(pnd_Max_States)); pnd_Z.nadd(1))
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().equals(state_Alpha.getValue(pnd_Z))))                                         //Natural: IF EXTRACT-STATE-CDE = STATE-ALPHA ( #Z )
                        {
                            pnd_K.setValue(state_Index.getValue(pnd_Z));                                                                                                  //Natural: ASSIGN #K := STATE-INDEX ( #Z )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  1991
                    if (condition(pnd_K.notEquals(getZero()) && ldaTwrl3300.getExtract_Npd_File_0_Extract_Combine_Ind().equals("Y")))                                     //Natural: IF #K NE 0 AND EXTRACT-COMBINE-IND = 'Y'
                    {
                        //*  12-29-00 FRANK
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Federal_State_Code().setValueEdited(state_Totals_State_Code.getValue(pnd_K),new ReportEditMask("99"));        //Natural: MOVE EDITED STATE-CODE ( #K ) ( EM = 99 ) TO IRS-B-FEDERAL-STATE-CODE
                        //* *  #WS-COMBINED-FEDERAL-STATE := 'Y'
                        //*  02-17-00 FRANK
                        state_Totals_State_Tot_1.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO STATE-TOT-1 ( #K )
                        state_Totals_State_Tot_2.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO STATE-TOT-2 ( #K )
                        state_Totals_State_Tot_4.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO STATE-TOT-4 ( #K )
                        state_Totals_State_Tot_5.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO STATE-TOT-5 ( #K )
                        //*  1991
                        state_Totals_State_Tot_9.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO STATE-TOT-9 ( #K )
                        //* 03-03-99 FC
                        state_Totals_State_Tot_8.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_A());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-A TO STATE-TOT-8 ( #K )
                        //*   RCC
                        state_Totals_State_Tot_B.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_B());                                            //Natural: ADD IRS-B-PAYMENT-AMOUNT-B TO STATE-TOT-B ( #K )
                        //*  1995
                        state_Totals_State_Tot_10.getValue(pnd_K).nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                         //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO STATE-TOT-10 ( #K )
                        //*  02-03-00 FRANK
                        local_Totals_Local_Tot_1.getValue(pnd_K).nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Gross_Amt());                                         //Natural: ADD EXTRACT-LCL-GROSS-AMT TO LOCAL-TOT-1 ( #K )
                        //* *  COMMENTED-OUT 02-25-04 BY FRANK
                        //* *  ADD IRS-B-ME-LC-TAX-WITHHELD TO STATE-TOT-11 (#K) /* 02-01-00 FRANK
                        pnd_Ws_Subtotal_State_Tot_1.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Gross_Amt());                                                       //Natural: ADD EXTRACT-ST-GROSS-AMT TO #WS-SUBTOTAL-STATE-TOT-1
                        pnd_Ws_Subtotal_State_Tot_10.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                      //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #WS-SUBTOTAL-STATE-TOT-10
                        pnd_Ws_Subtotal_Local_Tot_1.nadd(ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Gross_Amt());                                                      //Natural: ADD EXTRACT-LCL-GROSS-AMT TO #WS-SUBTOTAL-LOCAL-TOT-1
                        //* *  COMMENTED-OUT 02-25-04 BY FRANK
                        //* *  ADD IRS-B-ME-LC-TAX-WITHHELD TO #WS-SUBTOTAL-STATE-TOT-11
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_3().reset();                                                                                       //Natural: RESET IRS-B-PAYMENT-AMOUNT-3
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_6().reset();                                                                                       //Natural: RESET IRS-B-PAYMENT-AMOUNT-6
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_7().reset();                                                                                       //Natural: RESET IRS-B-PAYMENT-AMOUNT-7
                    //*  1991
                    //*  03-06-2007
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_8().reset();                                                                                       //Natural: RESET IRS-B-PAYMENT-AMOUNT-8
                    //*  03-06-2007 : ALTHOUGH IT DOESN't matter, #TWRA5052.#LNAME will
                    //*               TRUNCATE THE LAST 5 BYTES OF EXTRACT-NAME.
                    pdaTwra5052.getPnd_Twra5052_Pnd_Lname().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name());                                                   //Natural: ASSIGN #TWRA5052.#LNAME := EXTRACT-NAME
                    //*  03-06-2007
                    DbsUtil.callnat(Twrn5052.class , getCurrentProcessState(), pdaTwra5052.getPnd_Twra5052());                                                            //Natural: CALLNAT 'TWRN5052' #TWRA5052
                    if (condition(Global.isEscape())) return;
                    //*  05-01-2007
                    if (condition(pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Code().equals(false)))                                                                              //Natural: IF #TWRA5052.#RET-CODE = FALSE
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(0, "***",new TabSetting(6),"Abend In Name Control Routine 'TWRN5052'",new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 00 ) '***' 06T 'Abend In Name Control Routine "TWRN5052"' 77T '***' / '***' 06T 'Return Message...:' #TWRA5052.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                            TabSetting(6),"Return Message...:",pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                            TabSetting(77),"***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(93);  if (true) return;                                                                                                         //Natural: TERMINATE 93
                        //*  05-01-2007
                        //*  03-06-2007
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Name_Control().setValue(pdaTwra5052.getPnd_Twra5052_Pnd_Lname_Control());                                         //Natural: ASSIGN IRS-B-NAME-CONTROL := #TWRA5052.#LNAME-CONTROL
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Name_Line_1().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name());                                         //Natural: ASSIGN IRS-B-NAME-LINE-1 := EXTRACT-NAME
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Name_Line_2().reset();                                                                                            //Natural: RESET IRS-B-NAME-LINE-2
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Address_Line().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1());                                //Natural: ASSIGN IRS-B-ADDRESS-LINE := EXTRACT-ADDRSS-TXT-1
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_City().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_2());                                        //Natural: ASSIGN IRS-B-CITY := EXTRACT-ADDRSS-TXT-2
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_State().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Geo_Cd());                                      //Natural: ASSIGN IRS-B-STATE := EXTRACT-ADDR-7-GEO-CD
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Zip(),  //Natural: COMPRESS EXTRACT-ADDR-7-ZIP EXTRACT-ADDR-7-PLUS-4 INTO IRS-B-ZIP LEAVING NO SPACE
                        ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Plus_4()));
                    //*  11-18-99 FRANK
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Foreign_Addr().equals("Y")))                                                                  //Natural: IF EXTRACT-FOREIGN-ADDR = 'Y'
                    {
                        //*  11-13-00 FRANK
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_City_State_Zip().setValue(DbsUtil.compress(ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_2(),          //Natural: COMPRESS EXTRACT-ADDRSS-TXT-2 EXTRACT-PROVINCE-CODE EXTRACT-ADDR-7-ZIP EXTRACT-ADDR-7-PLUS-4 EXTRACT-COUNTRY-NAME INTO IRS-B-CITY-STATE-ZIP
                            ldaTwrl3300.getExtract_Npd_File_0_Extract_Province_Code(), ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Zip(), ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Plus_4(), 
                            ldaTwrl3300.getExtract_Npd_File_0_Extract_Country_Name()));
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_State().equals("AB") || ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_State().equals("AC")))                  //Natural: IF IRS-B-STATE = 'AB' OR = 'AC'
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_State().reset();                                                                                              //Natural: RESET IRS-B-STATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  11-13-00 FRANK
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Foreign_Addr().equals("Y")))                                                                  //Natural: IF EXTRACT-FOREIGN-ADDR = 'Y'
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Foreign_Indicator().setValue("1");                                                                            //Natural: MOVE '1' TO IRS-B-FOREIGN-INDICATOR
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Foreign_Indicator().setValue(" ");                                                                            //Natural: MOVE ' ' TO IRS-B-FOREIGN-INDICATOR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  1991 MAINE SPECIAL PROCESSING
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().equals("ME")))                                                                    //Natural: IF EXTRACT-STATE-CDE = 'ME'
                    {
                        //*  DETERMINE GROSS/TAXABLE
                        if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind().equals("1")))                                                               //Natural: IF IRS-B-TXBLE-NOT-DET-IND = '1'
                        {
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income().setValue(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                  //Natural: MOVE IRS-B-PAYMENT-AMOUNT-1 TO IRS-B-ME-ST-TAXABLE-INCOME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income().setValue(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                  //Natural: MOVE IRS-B-PAYMENT-AMOUNT-2 TO IRS-B-ME-ST-TAXABLE-INCOME
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SNJY
                        //*  SNJY
                        if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A().equals("000000000")))                                                  //Natural: IF IRS-B-ME-ST-TAXABLE-INCOME-A = '000000000'
                        {
                            ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A().setValue("         ");                                                           //Natural: ASSIGN IRS-B-ME-ST-TAXABLE-INCOME-A := '         '
                            //*  SNJY
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VIKRAM STARTS
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().equals("MD")))                                                                    //Natural: IF EXTRACT-STATE-CDE EQ 'MD'
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20_Md,true), new ExamineSearch("-"), new ExamineDelete());                                    //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20-MD '-' DELETE
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No().setValue(pnd_Temp_State_Id_20_Md);                                                  //Natural: ASSIGN IRS-B-MD-CNTRAL-REGISTRATN-NO := #TEMP-STATE-ID-20-MD
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().equals("NM")))                                                                    //Natural: IF EXTRACT-STATE-CDE EQ 'NM'
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20_Nm,true), new ExamineSearch("-"), new ExamineDelete());                                    //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20-NM '-' DELETE
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No().setValue(pnd_Temp_State_Id_20_Nm);                                                  //Natural: ASSIGN IRS-B-NM-CNTRAL-REGISTRATN-NO := #TEMP-STATE-ID-20-NM
                        //*  VIKRAM ENDS
                    }                                                                                                                                                     //Natural: END-IF
                    //*  02-15-00/* 11-23-99 FRANK
                    if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld().equals(getZero())))                                                            //Natural: IF IRS-B-ME-ST-TAX-WITHHELD = 0
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A().reset();                                                                               //Natural: RESET IRS-B-ME-ST-TAX-WITHHELD-A
                    }                                                                                                                                                     //Natural: END-IF
                    //*  03-03-00/* 11-23-99 FRANK
                    if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld().equals(getZero())))                                                            //Natural: IF IRS-B-ME-LC-TAX-WITHHELD = 0
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A().reset();                                                                               //Natural: RESET IRS-B-ME-LC-TAX-WITHHELD-A
                    }                                                                                                                                                     //Natural: END-IF
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Contract_Payee().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_B_Contract_Payee());                      //Natural: ASSIGN IRS-B-CONTRACT-PAYEE := EXTRACT-IRS-B-CONTRACT-PAYEE
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Roth_Contrib_Dte().setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Roth_Year());                               //Natural: ASSIGN IRS-B-ROTH-CONTRIB-DTE := EXTRACT-ROTH-YEAR
                    //*  SNJY
                    //*  SNJY
                    if (condition(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy().equals("0000")))                                                                //Natural: IF IRS-B-ROTH-CONTRIB-YYYY = '0000'
                    {
                        ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy().setValue("    ");                                                                         //Natural: ASSIGN IRS-B-ROTH-CONTRIB-YYYY := '    '
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(2, false, ldaTwrl3322.getIrs_B_Out_Tape());                                                                                      //Natural: WRITE WORK FILE 2 IRS-B-OUT-TAPE
                    //*  VIKRAM
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No().reset();                                                                                //Natural: RESET IRS-B-MD-CNTRAL-REGISTRATN-NO
                    //*  VIKRAM
                    ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No().reset();                                                                                //Natural: RESET IRS-B-NM-CNTRAL-REGISTRATN-NO
                    b_Record_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO B-RECORD-CTR
                    b_Recs_Written.nadd(1);                                                                                                                               //Natural: ADD 1 TO B-RECS-WRITTEN
                    //* 02-22-99 FC
                    ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees().nadd(1);                                                                                 //Natural: ADD 1 TO IRS-T-TOTAL-NUMBER-OF-PAYEES
                    //*  ------------------END OF 'B' RECORD FORMATTING--------------------
                    //*  1991
                    if (condition(pnd_K.notEquals(getZero()) && ldaTwrl3300.getExtract_Npd_File_0_Extract_Combine_Ind().equals("Y")))                                     //Natural: IF #K NE 0 AND EXTRACT-COMBINE-IND = 'Y'
                    {
                        state_Totals_St_B_Rec_Ctr.getValue(pnd_K).nadd(1);                                                                                                //Natural: ADD 1 TO ST-B-REC-CTR ( #K )
                        //*  02-03-00 FRANK
                        pnd_Ws_Subtotal_Rec_Ctr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #WS-SUBTOTAL-REC-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("C")))                                                                     //Natural: IF EXTRACT-PRDCT-CDE = 'C'
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Cref_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-INT-CTR
                            pnd_Cref_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #CREF-INT-REPORTED
                            pnd_Cref_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #CREF-INT-GROSS
                            pnd_Cref_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #CREF-INT-INCOME
                            pnd_Cref_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #CREF-INT-TAX
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Cref_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #CREF-INT-MISS-HLD
                                pnd_Cref_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #CREF-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Cref_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #CREF-INT-MISS-NAD
                                    getReports().write(0, "ERR1: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR1: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Cref_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #CREF-INT-TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Cref_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-R---CTR
                            pnd_Cref_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #CREF-R---REPORTED
                            pnd_Cref_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #CREF-R---GROSS
                            pnd_Cref_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #CREF-R---INCOME
                            pnd_Cref_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #CREF-R---TAX
                            pnd_Cref_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #CREF-R---IVC
                            //*  1991
                            pnd_Cref_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #CREF-R---ST-TAX
                            //*  1991
                            pnd_Cref_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #CREF-R---LC-TAX
                            //*  1991
                            pnd_Cref_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #CREF-R---9B-AMT
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Cref_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #CREF-R---MISS-HLD
                                pnd_Cref_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #CREF-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Cref_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #CREF-R---MISS-NAD
                                    getReports().write(0, "ERR2: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR2: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Cref_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #CREF-R---TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("L")))                                                                     //Natural: IF EXTRACT-PRDCT-CDE = 'L'
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Life_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #LIFE-INT-CTR
                            pnd_Life_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #LIFE-INT-REPORTED
                            pnd_Life_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #LIFE-INT-GROSS
                            pnd_Life_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #LIFE-INT-INCOME
                            pnd_Life_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #LIFE-INT-TAX
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Life_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #LIFE-INT-MISS-HLD
                                pnd_Life_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #LIFE-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Life_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #LIFE-INT-MISS-NAD
                                    pnd_Life_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #LIFE-INT-TOT-HOLD
                                    getReports().write(0, "ERR3: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR3: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Life_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #LIFE-R---CTR
                            pnd_Life_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #LIFE-R---REPORTED
                            pnd_Life_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #LIFE-R---GROSS
                            pnd_Life_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #LIFE-R---INCOME
                            pnd_Life_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #LIFE-R---TAX
                            pnd_Life_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #LIFE-R---IVC
                            //*  1991
                            pnd_Life_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #LIFE-R---ST-TAX
                            //*  1991
                            pnd_Life_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #LIFE-R---LC-TAX
                            //*  1991
                            pnd_Life_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #LIFE-R---9B-AMT
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Life_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #LIFE-R---MISS-HLD
                                pnd_Life_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #LIFE-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Life_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #LIFE-R---MISS-NAD
                                    pnd_Life_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #LIFE-R---TOT-HOLD
                                    getReports().write(0, "ERR4: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR4: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //* *IF   EXTRACT-PRDCT-CDE = 'T'
                    //*  EINCHG
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("T") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("A"))) //Natural: IF ( EXTRACT-PRDCT-CDE = 'T' OR = 'A' )
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Tiaa_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INT-CTR
                            pnd_Tiaa_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TIAA-INT-REPORTED
                            pnd_Tiaa_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TIAA-INT-GROSS
                            pnd_Tiaa_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TIAA-INT-INCOME
                            pnd_Tiaa_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TIAA-INT-TAX
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Tiaa_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-INT-MISS-HLD
                                pnd_Tiaa_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Tiaa_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TIAA-INT-MISS-NAD
                                    getReports().write(0, "ERR5: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR5: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Tiaa_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TIAA-INT-TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tiaa_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-R---CTR
                            pnd_Tiaa_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TIAA-R---REPORTED
                            pnd_Tiaa_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TIAA-R---GROSS
                            pnd_Tiaa_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #TIAA-R---INCOME
                            pnd_Tiaa_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TIAA-R---TAX
                            pnd_Tiaa_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #TIAA-R---IVC
                            //*  1991
                            pnd_Tiaa_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #TIAA-R---ST-TAX
                            //*  1991
                            pnd_Tiaa_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #TIAA-R---LC-TAX
                            pnd_Tiaa_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #TIAA-R---9B-AMT
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Tiaa_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-R---MISS-HLD
                                pnd_Tiaa_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TIAA-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Tiaa_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TIAA-R---MISS-NAD
                                    pnd_Tiaa_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TIAA-R---TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("S")))                                                                     //Natural: IF EXTRACT-PRDCT-CDE = 'S'
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Tcii_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TCII-INT-CTR
                            pnd_Tcii_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TCII-INT-REPORTED
                            pnd_Tcii_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TCII-INT-GROSS
                            pnd_Tcii_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TCII-INT-INCOME
                            pnd_Tcii_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TCII-INT-TAX
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Tcii_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TCII-INT-MISS-HLD
                                pnd_Tcii_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TCII-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Tcii_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TCII-INT-MISS-NAD
                                    getReports().write(0, "ERR1: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR1: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Tcii_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TCII-INT-TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Tcii_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TCII-R---CTR
                            pnd_Tcii_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TCII-R---REPORTED
                            pnd_Tcii_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TCII-R---GROSS
                            pnd_Tcii_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #TCII-R---INCOME
                            pnd_Tcii_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TCII-R---TAX
                            pnd_Tcii_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #TCII-R---IVC
                            //*  1991
                            pnd_Tcii_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #TCII-R---ST-TAX
                            //*  1991
                            pnd_Tcii_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #TCII-R---LC-TAX
                            //*  1991
                            pnd_Tcii_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #TCII-R---9B-AMT
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Tcii_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TCII-R---MISS-HLD
                                pnd_Tcii_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TCII-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Tcii_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TCII-R---MISS-NAD
                                    getReports().write(0, "ERR2: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR2: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Tcii_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TCII-R---TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DY1 FIX BEGINS >>
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("F")))                                                                     //Natural: IF EXTRACT-PRDCT-CDE = 'F'
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Fsbc_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #FSBC-INT-CTR
                            pnd_Fsbc_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #FSBC-INT-REPORTED
                            pnd_Fsbc_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #FSBC-INT-GROSS
                            pnd_Fsbc_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #FSBC-INT-INCOME
                            pnd_Fsbc_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #FSBC-INT-TAX
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Fsbc_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #FSBC-INT-MISS-HLD
                                pnd_Fsbc_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #FSBC-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Fsbc_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #FSBC-INT-MISS-NAD
                                    getReports().write(0, "ERR1: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR1: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Fsbc_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #FSBC-INT-TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Fsbc_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #FSBC-R---CTR
                            pnd_Fsbc_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #FSBC-R---REPORTED
                            pnd_Fsbc_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #FSBC-R---GROSS
                            pnd_Fsbc_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #FSBC-R---INCOME
                            pnd_Fsbc_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #FSBC-R---TAX
                            pnd_Fsbc_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #FSBC-R---IVC
                            pnd_Fsbc_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #FSBC-R---ST-TAX
                            pnd_Fsbc_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #FSBC-R---LC-TAX
                            pnd_Fsbc_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #FSBC-R---9B-AMT
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Fsbc_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #FSBC-R---MISS-HLD
                                pnd_Fsbc_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #FSBC-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Fsbc_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #FSBC-R---MISS-NAD
                                    getReports().write(0, "ERR2: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR2: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Fsbc_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #FSBC-R---TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DY1 FIX ENDS   <<
                    if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("X")))                                                                     //Natural: IF EXTRACT-PRDCT-CDE = 'X'
                    {
                        if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I")))                                                                  //Natural: IF EXTRACT-TYPE-CDE = 'I'
                        {
                            pnd_Trst_Int_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TRST-INT-CTR
                            pnd_Trst_Int_Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TRST-INT-REPORTED
                            pnd_Trst_Int_Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TRST-INT-GROSS
                            pnd_Trst_Int_Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TRST-INT-INCOME
                            pnd_Trst_Int_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TRST-INT-TAX
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Trst_Int_Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TRST-INT-MISS-HLD
                                pnd_Trst_Int_Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TRST-INT-TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Trst_Int_Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRST-INT-MISS-NAD
                                    getReports().write(0, "ERR5: MISSING NAD=",ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                               //Natural: WRITE 'ERR5: MISSING NAD=' EXTRACT-CNTRCT-PY-NMBR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Trst_Int_Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRST-INT-TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Trst_R___Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TRST-R---CTR
                            pnd_Trst_R___Reported.nadd(1);                                                                                                                //Natural: ADD 1 TO #TRST-R---REPORTED
                            pnd_Trst_R___Gross.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_1());                                                              //Natural: ADD IRS-B-PAYMENT-AMOUNT-1 TO #TRST-R---GROSS
                            pnd_Trst_R___Income.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_2());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-2 TO #TRST-R---INCOME
                            pnd_Trst_R___Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_4());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-4 TO #TRST-R---TAX
                            pnd_Trst_R___Ivc.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_5());                                                                //Natural: ADD IRS-B-PAYMENT-AMOUNT-5 TO #TRST-R---IVC
                            //*  1991
                            pnd_Trst_R___St_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-ST-TAX-WITHHELD TO #TRST-R---ST-TAX
                            //*  1991
                            pnd_Trst_R___Lc_Tax.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld());                                                           //Natural: ADD IRS-B-ME-LC-TAX-WITHHELD TO #TRST-R---LC-TAX
                            pnd_Trst_R___9b_Amt.nadd(ldaTwrl3322.getIrs_B_Out_Tape_Irs_B_Payment_Amount_9());                                                             //Natural: ADD IRS-B-PAYMENT-AMOUNT-9 TO #TRST-R---9B-AMT
                            //*  1991
                            if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().notEquals(" ")))                                                           //Natural: IF EXTRACT-HOLD-CDE NE ' '
                            {
                                pnd_Trst_R___Miss_Hld.nadd(1);                                                                                                            //Natural: ADD 1 TO #TRST-R---MISS-HLD
                                pnd_Trst_R___Tot_Hold.nadd(1);                                                                                                            //Natural: ADD 1 TO #TRST-R---TOT-HOLD
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().equals(" ") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().equals(" "))) //Natural: IF EXTRACT-NAME = ' ' OR EXTRACT-ADDRSS-TXT-1 = ' '
                                {
                                    pnd_Trst_R___Miss_Nad.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRST-R---MISS-NAD
                                    pnd_Trst_R___Tot_Hold.nadd(1);                                                                                                        //Natural: ADD 1 TO #TRST-R---TOT-HOLD
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                if (condition(pnd_Ws_Recs_Read.equals(getZero())))                                                                                                        //Natural: IF #WS-RECS-READ = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, "***",new TabSetting(6),"Form (1099)",pnd_Input_Parm_Pnd_Company,"FILE IS EMPTY",new TabSetting(77),"***",NEWLINE,"***",new     //Natural: WRITE ( 01 ) '***' 06T 'Form (1099)' #COMPANY 'FILE IS EMPTY' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                    //* *  '***' 06T 'Form (1099) Extract File (Work File 01) Is Empty'
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-IF
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees().setValue(b_Record_Ctr);                                                                            //Natural: MOVE B-RECORD-CTR TO IRS-C-NUMBER-OF-PAYEES
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1().setValue(contr_Tot_1);                                                                              //Natural: MOVE CONTR-TOT-1 TO IRS-C-CONTROL-TOTAL-1
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2().setValue(contr_Tot_2);                                                                              //Natural: MOVE CONTR-TOT-2 TO IRS-C-CONTROL-TOTAL-2
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_3().setValue(contr_Tot_3);                                                                              //Natural: MOVE CONTR-TOT-3 TO IRS-C-CONTROL-TOTAL-3
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4().setValue(contr_Tot_4);                                                                              //Natural: MOVE CONTR-TOT-4 TO IRS-C-CONTROL-TOTAL-4
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5().setValue(contr_Tot_5);                                                                              //Natural: MOVE CONTR-TOT-5 TO IRS-C-CONTROL-TOTAL-5
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_6().setValue(contr_Tot_6);                                                                              //Natural: MOVE CONTR-TOT-6 TO IRS-C-CONTROL-TOTAL-6
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_7().setValue(contr_Tot_7);                                                                              //Natural: MOVE CONTR-TOT-7 TO IRS-C-CONTROL-TOTAL-7
                //* 02-20-99 FC
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_A().setValue(contr_Tot_8);                                                                              //Natural: MOVE CONTR-TOT-8 TO IRS-C-CONTROL-TOTAL-A
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9().setValue(contr_Tot_9);                                                                              //Natural: MOVE CONTR-TOT-9 TO IRS-C-CONTROL-TOTAL-9
                //*  RCC
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_B().setValue(contr_Tot_B);                                                                              //Natural: MOVE CONTR-TOT-B TO IRS-C-CONTROL-TOTAL-B
                getWorkFiles().write(2, false, ldaTwrl3323.getIrs_C_Out_Tape());                                                                                          //Natural: WRITE WORK FILE 2 IRS-C-OUT-TAPE
                //*  12-11-00 FRANK
                c_Record_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO C-RECORD-CTR
                //* *IF #SAVE-TYPE-CODE NE 'I'                         /* 03-19-03 FRANK
                //*  04-17-03 FRANK
                //*  1991
                if (condition(pnd_Save_Type_Code.equals("I") || pnd_Save_Type_Code.equals("R")))                                                                          //Natural: IF #SAVE-TYPE-CODE = 'I' OR = 'R'
                {
                    FOR05:                                                                                                                                                //Natural: FOR #K = 1 TO #MAX-STATES
                    for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Max_States)); pnd_K.nadd(1))
                    {
                        if (condition(state_Totals_St_B_Rec_Ctr.getValue(pnd_K).greater(getZero())))                                                                      //Natural: IF ST-B-REC-CTR ( #K ) > 0
                        {
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().setValue(state_Totals_St_B_Rec_Ctr.getValue(pnd_K));                                   //Natural: MOVE ST-B-REC-CTR ( #K ) TO IRS-K-NUMBER-OF-PAYEES
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().setValue(state_Totals_State_Tot_1.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-1 ( #K ) TO IRS-K-CONTROL-TOTAL-1
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().setValue(state_Totals_State_Tot_2.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-2 ( #K ) TO IRS-K-CONTROL-TOTAL-2
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3().setValue(state_Totals_State_Tot_3.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-3 ( #K ) TO IRS-K-CONTROL-TOTAL-3
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().setValue(state_Totals_State_Tot_4.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-4 ( #K ) TO IRS-K-CONTROL-TOTAL-4
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().setValue(state_Totals_State_Tot_5.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-5 ( #K ) TO IRS-K-CONTROL-TOTAL-5
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6().setValue(state_Totals_State_Tot_6.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-6 ( #K ) TO IRS-K-CONTROL-TOTAL-6
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7().setValue(state_Totals_State_Tot_7.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-7 ( #K ) TO IRS-K-CONTROL-TOTAL-7
                            //* 03-03-99 FC
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A().setValue(state_Totals_State_Tot_8.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-8 ( #K ) TO IRS-K-CONTROL-TOTAL-A
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().setValue(state_Totals_State_Tot_9.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-9 ( #K ) TO IRS-K-CONTROL-TOTAL-9
                            //* 03-01-99 FC
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().setValue(state_Totals_State_Tot_10.getValue(pnd_K));                                    //Natural: MOVE STATE-TOT-10 ( #K ) TO IRS-K-ST-TAX-WITHHELD
                            //*  RCC
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B().setValue(state_Totals_State_Tot_B.getValue(pnd_K));                                     //Natural: MOVE STATE-TOT-B ( #K ) TO IRS-K-CONTROL-TOTAL-B
                            //* *  COMMENTED-OUT 02-25-04 BY FRANK
                            //* *    MOVE STATE-TOT-11 (#K) TO IRS-K-CONTROL-TOTAL-B /*03-01-99 FC
                            //*  12-29-00 FRANK
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().setValueEdited(state_Totals_State_Code.getValue(pnd_K),new ReportEditMask("99"));            //Natural: MOVE EDITED STATE-CODE ( #K ) ( EM = 99 ) TO IRS-K-STATE-CODE
                            //*  02-11-00 FRANK
                            if (condition(ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().equals(getZero())))                                                       //Natural: IF IRS-K-ST-TAX-WITHHELD = 0
                            {
                                ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld_A().reset();                                                                          //Natural: RESET IRS-K-ST-TAX-WITHHELD-A
                            }                                                                                                                                             //Natural: END-IF
                            getWorkFiles().write(2, false, ldaTwrl3325.getIrs_K_Out_Tape());                                                                              //Natural: WRITE WORK FILE 2 IRS-K-OUT-TAPE
                            //*  12-11-00 FRANK
                            k_Record_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO K-RECORD-CTR
                                                                                                                                                                          //Natural: PERFORM STATE-SUB-TOTALS
                            sub_State_Sub_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  -----------------START OF 'F' RECORD--------------------------
                ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records().setValue(a_Record_Ctr);                                                                         //Natural: MOVE A-RECORD-CTR TO IRS-F-NUMBER-OF-A-RECORDS
                getWorkFiles().write(2, false, ldaTwrl3324.getIrs_F_Out_Tape());                                                                                          //Natural: WRITE WORK FILE 2 IRS-F-OUT-TAPE
                //*  12-11-00 FRANK
                //*  02-17-99 FRANK
                f_Record_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO F-RECORD-CTR
                //*  --------------------END OF 'F' RECORD------------------------
                //*  -----------------START OF 'T' RECORD-OUTPUT-------------------
                ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Payment_Year().setValue(pnd_Ws_Tax_Year);                                                                             //Natural: ASSIGN IRS-T-PAYMENT-YEAR := #WS-TAX-YEAR
                //*  01-04-01 FRANK
                //*  12-04-00 FRANK
                if (condition(pnd_Input_Parm_Pnd_Test_Run.equals("Y")))                                                                                                   //Natural: IF #TEST-RUN = 'Y'
                {
                    ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Test_File_Indicator().setValue("T");                                                                              //Natural: ASSIGN IRS-T-TEST-FILE-INDICATOR := 'T'
                    //*  04-18-01 FRANK
                    //*  01-02-02 J.ROTHOLZ
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind().setValue("  ");                                                                              //Natural: ASSIGN IRS-T-MAGNETIC-TAPE-FILE-IND := '  '
                ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Vendor_Ind().setValue("I");                                                                                           //Natural: ASSIGN IRS-T-VENDOR-IND := 'I'
                //*  02-17-99 FRANK
                getWorkFiles().write(3, false, ldaTwrl3326.getIrs_T_Out_Tape());                                                                                          //Natural: WRITE WORK FILE 3 IRS-T-OUT-TAPE
                //*  02-17-99 FRANK
                t_Record_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO T-RECORD-CTR
                //*  --------------------END OF 'T' RECORD-OUTPUT-----------------
                //* * WRITE (1)    62T '** CREF  **'  /
                //*  108T '--- WARNINGS ---'       /
                //*  057T 'EMPLOYEE'
                //*  116T 'MISSING'                /
                //*  012T 'STATE'
                //*  041T 'TAXABLE'
                //*  055T 'CONTRIBUTION'
                //*  071T 'FEDERAL'
                //*  088T 'STATE'
                //*  108T 'MISSING'
                //*  117T 'NAME/'
                //*  126T 'IRS'                    /
                //*  002T 'FORM'
                //*  011T 'FORM CNT'
                //*  022T 'GROSS INCOME'
                //*  042T 'INCOME'
                //*  059T '(IVC)'
                //*  070T 'TAX WITHHELD'
                //*  085T 'TAX WITHHELD'
                //*  099T 'REPORTED'
                //*  108T 'TAX ID'
                //*  116T 'ADDRESS'
                //*  125T 'HOLDS'                  /
                //*  001T '--------'                       /* FORM
                //*  011T '--------'                       /* STATE FORM CNT
                //*  021T '---------------'                /* GROSS INCOME
                //*  038T '---------------'                /* TAXABLE INCOME
                //*  055T '-------------'                  /* EMPLOYEE CONTRIBUTION (IVC)
                //*  070T '-------------'                  /* FEDERAL TAX WITHHELD
                //*  085T '------------'                   /* STATE TAX WITHHELD
                //*  098T '---------'                      /* REPORTED
                //*  108T '-------'                        /* MISSING TAX ID
                //*  116T '-------'                        /* MISSING NAME/ADDRESS
                //*  124T '-------'  /                     /* IRS HOLDS
                //*  '1099-INT'
                //*  1X #CREF-INT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-INT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-INT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-INT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-INT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-INT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-INT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-INT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-INT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-INT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF) //
                //*  '1099-R  '
                //*  1X #CREF-R---CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-R---GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-R---INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-R---IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-R---TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-R---ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-R---REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-R---MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-R---MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-R---MISS-HLD (EM=ZZZ,ZZ9 SG=OFF) //
                //*  011T '--------'
                //*  021T '---------------'
                //*  038T '---------------'
                //*  055T '-------------'
                //*  070T '-------------'
                //*  085T '------------'
                //*  098T '---------'
                //*  108T '-------'
                //*  116T '-------'
                //*  124T '-------'  /
                //*  *KIP  (1) 1
                //*  *DD #CREF-INT-CTR      #CREF-R---CTR
                //*  GIVING #CREF-TOT-CTR
                //*  *DD #CREF-INT-GROSS    #CREF-R---GROSS    #CREF--FIX-GROSS
                //*  GIVING #CREF-TOT-GROSS
                //*  *DD #CREF-INT-INCOME   #CREF-R---INCOME   #CREF--FIX-INCOME
                //*  GIVING #CREF-TOT-INCOME
                //*  *DD #CREF-INT-IVC      #CREF-R---IVC      #CREF--FIX-IVC
                //*  GIVING #CREF-TOT-IVC
                //*  *DD #CREF-INT-TAX      #CREF-R---TAX      GIVING #CREF-TOT-TAX
                //*  *DD #CREF-INT-ST-TAX   #CREF-R---ST-TAX   GIVING #CREF-TOT-ST-TAX
                //*  *DD #CREF-INT-REPORTED #CREF-R---REPORTED GIVING #CREF-TOT-REPORTED
                //*  *DD #CREF-INT-MISS-TIN #CREF-R---MISS-TIN GIVING #CREF-TOT-MISS-TIN
                //*  *DD #CREF-INT-MISS-NAD #CREF-R---MISS-NAD GIVING #CREF-TOT-MISS-NAD
                //*  *DD #CREF-INT-MISS-HLD #CREF-R---MISS-HLD GIVING #CREF-TOT-MISS-HLD
                //*  *RITE (1)        'TOTALS  '
                //*  1X #CREF-TOT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-TOT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-TOT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-TOT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-TOT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-TOT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #CREF-TOT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-TOT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-TOT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #CREF-TOT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)
                //*  // 40X '**CREF TOTAL 9B AMT ' #CREF-R---9B-AMT (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //*  2X '**CREF NYC TAX AMT ' #CREF-R---LC-TAX (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //*  // 62T '** LIFE **' /
                //*  108T '--- WARNINGS ---'       /
                //*  057T 'EMPLOYEE'
                //*  116T 'MISSING'                /
                //*  012T 'STATE'
                //*  041T 'TAXABLE'
                //*  055T 'CONTRIBUTION'
                //*  071T 'FEDERAL'
                //*  088T 'STATE'
                //*  108T 'MISSING'
                //*  117T 'NAME/'
                //*  126T 'IRS'                    /
                //*  002T 'FORM'
                //*  011T 'FORM CNT'
                //*  022T 'GROSS INCOME'
                //*  042T 'INCOME'
                //*  059T '(IVC)'
                //*  070T 'TAX WITHHELD'
                //*  085T 'TAX WITHHELD'
                //*  099T 'REPORTED'
                //*  108T 'TAX ID'
                //*  116T 'ADDRESS'
                //*  125T 'HOLDS'                  /
                //*  001T '--------'                       /* FORM
                //*  011T '--------'                       /* STATE FORM CNT
                //*  021T '---------------'                /* GROSS INCOME
                //*  038T '---------------'                /* TAXABLE INCOME
                //*  055T '-------------'                  /* EMPLOYEE CONTRIBUTION (IVC)
                //*  070T '-------------'                  /* FEDERAL TAX WITHHELD
                //*  085T '------------'                   /* STATE TAX WITHHELD
                //*  098T '---------'                      /* REPORTED
                //*  108T '-------'                        /* MISSING TAX ID
                //*  116T '-------'                        /* MISSING NAME/ADDRESS
                //*  124T '-------'  /                     /* IRS HOLDS
                //*  '1099-INT'
                //*  1X #LIFE-INT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-INT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-INT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-INT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-INT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-INT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-INT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-INT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-INT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-INT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)
                //*  //    '1099-R  '
                //*  1X #LIFE-R---CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-R---GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-R---INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-R---IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-R---TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-R---ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-R---REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-R---MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-R---MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-R---MISS-HLD (EM=ZZZ,ZZ9 SG=OFF) //
                //*  011T '--------'
                //*  021T '---------------'
                //*  038T '---------------'
                //*  055T '-------------'
                //*  070T '-------------'
                //*  085T '------------'
                //*  098T '---------'
                //*  108T '-------'
                //*  116T '-------'
                //*  124T '-------'  /
                //*  *KIP  (1) 1
                //*  *DD #LIFE-INT-CTR      #LIFE-R---CTR
                //*  GIVING #LIFE-TOT-CTR
                //*  *DD #LIFE-INT-GROSS    #LIFE-R---GROSS    #LIFE-FIX-GROSS
                //*  GIVING #LIFE-TOT-GROSS
                //*  *DD #LIFE-INT-INCOME   #LIFE-R---INCOME   #LIFE-FIX-INCOME
                //*  GIVING #LIFE-TOT-INCOME
                //*  *DD #LIFE-INT-IVC      #LIFE-R---IVC      #LIFE-FIX-IVC
                //*  GIVING #LIFE-TOT-IVC
                //*  *DD #LIFE-INT-TAX      #LIFE-R---TAX      GIVING #LIFE-TOT-TAX
                //*  *DD #LIFE-INT-ST-TAX   #LIFE-R---ST-TAX   GIVING #LIFE-TOT-ST-TAX
                //*  *DD #LIFE-INT-REPORTED #LIFE-R---REPORTED GIVING #LIFE-TOT-REPORTED
                //*  *DD #LIFE-INT-MISS-TIN #LIFE-R---MISS-TIN GIVING #LIFE-TOT-MISS-TIN
                //*  *DD #LIFE-INT-MISS-NAD #LIFE-R---MISS-NAD GIVING #LIFE-TOT-MISS-NAD
                //*  *DD #LIFE-INT-MISS-HLD #LIFE-R---MISS-HLD GIVING #LIFE-TOT-MISS-HLD
                //*  *RITE (1)        'TOTALS  '
                //*  1X #LIFE-TOT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-TOT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-TOT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-TOT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-TOT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-TOT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #LIFE-TOT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-TOT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-TOT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #LIFE-TOT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)
                //*  // 40X '**LIFE TOTAL 9B AMT ' #LIFE-R---9B-AMT (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //*  2X '**LIFE NYC TAX AMT ' #LIFE-R---LC-TAX (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //* *IF #COMPANY  EQ 'TIAA'                             /* 09-26-02 FRANK
                //*  EINCHG
                if (condition(pnd_Input_Parm_Pnd_Company.equals("TIAA") || pnd_Input_Parm_Pnd_Company.equals("ADMN")))                                                    //Natural: IF #COMPANY EQ 'TIAA' OR EQ 'ADMN'
                {
                    //*   JB01
                    //*  DUTTAD
                    //*  FORM
                    //*  STATE FORM CNT
                    //*  GROSS INCOME
                    //*  TAXABLE INCOME
                    //*  EMPLOYEE CONTRIBUTION (IVC)
                    //*  FEDERAL TAX WITHHELD
                    //*  STATE TAX WITHHELD
                    //*  REPORTED
                    //*  MISSING TAX ID
                    //*  MISSING NAME/ADDRESS
                    //*  IRS HOLDS
                    getReports().write(1, new TabSetting(62),"** TIAA  **",NEWLINE,new TabSetting(111),"--- WARNINGS ---",NEWLINE,new TabSetting(57),"EMPLOYEE",new       //Natural: WRITE ( 1 ) 62T '** TIAA  **' / 111T '--- WARNINGS ---' / 057T 'EMPLOYEE' 116T 'MISSING' / 012T 'STATE' 042T 'TAXABLE' 056T 'CONTRIBUTION' 074T 'FEDERAL' 092T 'STATE' 111T 'MISSING' 119T 'NAME/' 130T 'IRS' / 002T 'FORM' 011T 'FORM CNT' 022T 'GROSS INCOME' 043T 'INCOME' 060T '(IVC)' 073T 'TAX WITHHELD' 088T 'TAX WITHHELD' 102T 'REPORTED' 111T 'TAX ID' 118T 'ADDRESS' 128T 'HOLDS' / 001T '--------' 011T '--------' 021T '---------------' 039T '---------------' 056T '-------------' 072T '-------------' 088T '------------' 101T '---------' 111T '------' 118T '-------' 126T '-------' / '1099-INT' 1X #TIAA-INT-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-INT-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-INT-INCOME ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-INT-IVC ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-INT-TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-INT-ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-INT-REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-INT-MISS-TIN ( EM = ZZ,ZZ9 SG = OFF ) 1X #TIAA-INT-MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TIAA-INT-MISS-HLD ( EM = ZZZ,ZZ9 SG = OFF ) // '1099-R  ' 1X #TIAA-R---CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-R---GROSS ( EM = ZZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-R---INCOME ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-R---IVC ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-R---TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-R---ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-R---REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-R---MISS-TIN ( EM = ZZ,ZZ9 SG = OFF ) 1X #TIAA-R---MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TIAA-R---MISS-HLD ( EM = ZZZ,ZZ9 SG = OFF ) // 011T '--------' 021T '---------------' 039T '---------------' 056T '-------------' 072T '-------------' 088T '------------' 101T '---------' 111T '------' 118T '-------' 126T '-------' /
                        TabSetting(116),"MISSING",NEWLINE,new TabSetting(12),"STATE",new TabSetting(42),"TAXABLE",new TabSetting(56),"CONTRIBUTION",new 
                        TabSetting(74),"FEDERAL",new TabSetting(92),"STATE",new TabSetting(111),"MISSING",new TabSetting(119),"NAME/",new TabSetting(130),"IRS",NEWLINE,new 
                        TabSetting(2),"FORM",new TabSetting(11),"FORM CNT",new TabSetting(22),"GROSS INCOME",new TabSetting(43),"INCOME",new TabSetting(60),"(IVC)",new 
                        TabSetting(73),"TAX WITHHELD",new TabSetting(88),"TAX WITHHELD",new TabSetting(102),"REPORTED",new TabSetting(111),"TAX ID",new 
                        TabSetting(118),"ADDRESS",new TabSetting(128),"HOLDS",NEWLINE,new TabSetting(1),"--------",new TabSetting(11),"--------",new TabSetting(21),"---------------",new 
                        TabSetting(39),"---------------",new TabSetting(56),"-------------",new TabSetting(72),"-------------",new TabSetting(88),"------------",new 
                        TabSetting(101),"---------",new TabSetting(111),"------",new TabSetting(118),"-------",new TabSetting(126),"-------",NEWLINE,"1099-INT",new 
                        ColumnSpacing(1),pnd_Tiaa_Int_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Gross, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Income, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Ivc, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Tiaa_Int_Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_St_Tax, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Reported, new ReportEditMask ("Z,ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Miss_Tin, new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Tiaa_Int_Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Int_Miss_Hld, 
                        new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,"1099-R  ",new ColumnSpacing(1),pnd_Tiaa_R___Ctr, new ReportEditMask 
                        ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Gross, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"), new SignPosition 
                        (false),new ColumnSpacing(1),pnd_Tiaa_R___Income, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Ivc, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___St_Tax, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Tiaa_R___Reported, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Miss_Tin, 
                        new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_R___Miss_Hld, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,new 
                        TabSetting(11),"--------",new TabSetting(21),"---------------",new TabSetting(39),"---------------",new TabSetting(56),"-------------",new 
                        TabSetting(72),"-------------",new TabSetting(88),"------------",new TabSetting(101),"---------",new TabSetting(111),"------",new 
                        TabSetting(118),"-------",new TabSetting(126),"-------",NEWLINE);
                    if (Global.isEscape()) return;
                    //*  // 62T '** TIAA  **' /
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    pnd_Tiaa_Tot_Ctr.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Ctr), pnd_Tiaa_Int_Ctr.add(pnd_Tiaa_R___Ctr));                                     //Natural: ADD #TIAA-INT-CTR #TIAA-R---CTR GIVING #TIAA-TOT-CTR
                    pnd_Tiaa_Tot_Gross.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Gross), pnd_Tiaa_Int_Gross.add(pnd_Tiaa_R___Gross).add(pnd_Tiaa__Fix_Gross));    //Natural: ADD #TIAA-INT-GROSS #TIAA-R---GROSS #TIAA--FIX-GROSS GIVING #TIAA-TOT-GROSS
                    pnd_Tiaa_Tot_Income.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Income), pnd_Tiaa_Int_Income.add(pnd_Tiaa_R___Income).add(pnd_Tiaa__Fix_Income)); //Natural: ADD #TIAA-INT-INCOME #TIAA-R---INCOME #TIAA--FIX-INCOME GIVING #TIAA-TOT-INCOME
                    pnd_Tiaa_Tot_Ivc.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Ivc), pnd_Tiaa_Int_Ivc.add(pnd_Tiaa_R___Ivc).add(pnd_Tiaa__Fix_Ivc));              //Natural: ADD #TIAA-INT-IVC #TIAA-R---IVC #TIAA--FIX-IVC GIVING #TIAA-TOT-IVC
                    pnd_Tiaa_Tot_Tax.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Tax), pnd_Tiaa_Int_Tax.add(pnd_Tiaa_R___Tax));                                     //Natural: ADD #TIAA-INT-TAX #TIAA-R---TAX GIVING #TIAA-TOT-TAX
                    pnd_Tiaa_Tot_St_Tax.compute(new ComputeParameters(false, pnd_Tiaa_Tot_St_Tax), pnd_Tiaa_Int_St_Tax.add(pnd_Tiaa_R___St_Tax));                         //Natural: ADD #TIAA-INT-ST-TAX #TIAA-R---ST-TAX GIVING #TIAA-TOT-ST-TAX
                    pnd_Tiaa_Tot_Reported.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Reported), pnd_Tiaa_Int_Reported.add(pnd_Tiaa_R___Reported));                 //Natural: ADD #TIAA-INT-REPORTED #TIAA-R---REPORTED GIVING #TIAA-TOT-REPORTED
                    pnd_Tiaa_Tot_Miss_Tin.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Miss_Tin), pnd_Tiaa_Int_Miss_Tin.add(pnd_Tiaa_R___Miss_Tin));                 //Natural: ADD #TIAA-INT-MISS-TIN #TIAA-R---MISS-TIN GIVING #TIAA-TOT-MISS-TIN
                    pnd_Tiaa_Tot_Miss_Nad.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Miss_Nad), pnd_Tiaa_Int_Miss_Nad.add(pnd_Tiaa_R___Miss_Nad));                 //Natural: ADD #TIAA-INT-MISS-NAD #TIAA-R---MISS-NAD GIVING #TIAA-TOT-MISS-NAD
                    pnd_Tiaa_Tot_Miss_Hld.compute(new ComputeParameters(false, pnd_Tiaa_Tot_Miss_Hld), pnd_Tiaa_Int_Miss_Hld.add(pnd_Tiaa_R___Miss_Hld));                 //Natural: ADD #TIAA-INT-MISS-HLD #TIAA-R---MISS-HLD GIVING #TIAA-TOT-MISS-HLD
                    getReports().write(1, "TOTALS  ",new ColumnSpacing(1),pnd_Tiaa_Tot_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new                //Natural: WRITE ( 1 ) 'TOTALS  ' 1X #TIAA-TOT-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-TOT-GROSS ( EM = ZZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-TOT-INCOME ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-TOT-IVC ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-TOT-TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-TOT-ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TIAA-TOT-REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TIAA-TOT-MISS-TIN ( EM = ZZ,ZZ9 SG = OFF ) 1X #TIAA-TOT-MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TIAA-TOT-MISS-HLD ( EM = ZZZ,ZZ9 SG = OFF ) // 40X '**TIAA TOTAL 9B AMT ' #TIAA-R---9B-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 2X '**TIAA NYC TAX AMT ' #TIAA-R---LC-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                        ColumnSpacing(1),pnd_Tiaa_Tot_Gross, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Income, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Ivc, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Tiaa_Tot_St_Tax, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Reported, 
                        new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Miss_Tin, new ReportEditMask ("ZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Tiaa_Tot_Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Tiaa_Tot_Miss_Hld, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,new ColumnSpacing(40),"**TIAA TOTAL 9B AMT ",pnd_Tiaa_R___9b_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"**TIAA NYC TAX AMT ",pnd_Tiaa_R___Lc_Tax, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  WRITE (1)    62T '** TCII  **'  /
                //*  108T '--- WARNINGS ---'       /
                //*  057T 'EMPLOYEE'
                //*  116T 'MISSING'                /
                //*  012T 'STATE'
                //*  041T 'TAXABLE'
                //*  055T 'CONTRIBUTION'
                //*  071T 'FEDERAL'
                //*  088T 'STATE'
                //*  108T 'MISSING'
                //*  117T 'NAME/'
                //*  126T 'IRS'                    /
                //*  002T 'FORM'
                //*  011T 'FORM CNT'
                //*  022T 'GROSS INCOME'
                //*  042T 'INCOME'
                //*  059T '(IVC)'
                //*  070T 'TAX WITHHELD'
                //*  085T 'TAX WITHHELD'
                //*  099T 'REPORTED'
                //*  108T 'TAX ID'
                //*  116T 'ADDRESS'
                //*  125T 'HOLDS'                  /
                //*  001T '--------'                       /* FORM
                //*  011T '--------'                       /* STATE FORM CNT
                //*  021T '---------------'                /* GROSS INCOME
                //*  038T '---------------'                /* TAXABLE INCOME
                //*  055T '-------------'                  /* EMPLOYEE CONTRIBUTION (IVC)
                //*  070T '-------------'                  /* FEDERAL TAX WITHHELD
                //*  085T '------------'                   /* STATE TAX WITHHELD
                //*  098T '---------'                      /* REPORTED
                //*  108T '-------'                        /* MISSING TAX ID
                //*  116T '-------'                        /* MISSING NAME/ADDRESS
                //*  124T '-------'  /                     /* IRS HOLDS
                //*  '1099-INT'
                //*  1X #TCII-INT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-INT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-INT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-INT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-INT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-INT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-INT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-INT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-INT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-INT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)
                //*  //    '1099-R  '
                //*  1X #TCII-R---CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-R---GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-R---INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-R---IVC      (EM=ZZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-R---TAX      (EM=ZZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-R---ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-R---REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-R---MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-R---MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-R---MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)  //
                //*  011T '--------'
                //*  021T '---------------'
                //*  038T '---------------'
                //*  055T '-------------'
                //*  070T '-------------'
                //*  085T '------------'
                //*  098T '---------'
                //*  108T '-------'
                //*  116T '-------'
                //*  124T '-------'  /
                //*  SKIP  (1) 1
                //*  ADD #TCII-INT-CTR      #TCII-R---CTR
                //*   GIVING #TCII-TOT-CTR
                //*  ADD #TCII-INT-GROSS    #TCII-R---GROSS    #TCII--FIX-GROSS
                //*   GIVING #TCII-TOT-GROSS
                //*  ADD #TCII-INT-INCOME   #TCII-R---INCOME   #TCII--FIX-INCOME
                //*   GIVING #TCII-TOT-INCOME
                //*  ADD #TCII-INT-IVC      #TCII-R---IVC      #TCII--FIX-IVC
                //*   GIVING #TCII-TOT-IVC
                //*  ADD #TCII-INT-TAX      #TCII-R---TAX      GIVING #TCII-TOT-TAX
                //*  ADD #TCII-INT-ST-TAX   #TCII-R---ST-TAX   GIVING #TCII-TOT-ST-TAX
                //*  ADD #TCII-INT-REPORTED #TCII-R---REPORTED GIVING #TCII-TOT-REPORTED
                //*  ADD #TCII-INT-MISS-TIN #TCII-R---MISS-TIN GIVING #TCII-TOT-MISS-TIN
                //*  ADD #TCII-INT-MISS-NAD #TCII-R---MISS-NAD GIVING #TCII-TOT-MISS-NAD
                //*  ADD #TCII-INT-MISS-HLD #TCII-R---MISS-HLD GIVING #TCII-TOT-MISS-HLD
                //*  WRITE (1)        'TOTALS  '
                //*   1X #TCII-TOT-CTR      (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*   1X #TCII-TOT-GROSS    (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*   1X #TCII-TOT-INCOME   (EM=Z,ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*   1X #TCII-TOT-IVC      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*   1X #TCII-TOT-TAX      (EM=ZZZ,ZZZ,ZZ9.99 SG=OFF)
                //*   1X #TCII-TOT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                //*  1X #TCII-TOT-REPORTED (EM=Z,ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-TOT-MISS-TIN (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-TOT-MISS-NAD (EM=ZZZ,ZZ9 SG=OFF)
                //*  1X #TCII-TOT-MISS-HLD (EM=ZZZ,ZZ9 SG=OFF)
                //*  // 40X '**TCII TOTAL 9B AMT ' #TCII-R---9B-AMT (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //*  2X '**TIAA NYC TAX AMT ' #TCII-R---LC-TAX (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
                //*  09-26-02 FRANK
                if (condition(pnd_Input_Parm_Pnd_Company.equals("TRST")))                                                                                                 //Natural: IF #COMPANY EQ 'TRST'
                {
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  FORM
                    //*  STATE FORM CNT
                    //*  DUTTAD   /* GROSS INCOME
                    //*  DUTTAD   /* TAXABLE INCOME
                    //*  DUTTAD   /* EMPLOYEE CONTRIBUTION (IVC)
                    //*  DUTTAD   /* FEDERAL TAX WITHHELD
                    //*  DUTTAD   /* STATE TAX WITHHELD
                    //*  DUTTAD   /* REPORTED
                    //*  DUTTAD   /* MISSING TAX ID
                    //*  DUTTAD   /* MISSING NAME/ADDRESS
                    //*  DUTTAD   /* IRS HOLDS
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    getReports().write(1, new TabSetting(62),"** TRST  **",NEWLINE,new TabSetting(111),"--- WARNINGS ---",NEWLINE,new TabSetting(57),"EMPLOYEE",new       //Natural: WRITE ( 1 ) 62T '** TRST  **' / 111T '--- WARNINGS ---' / 057T 'EMPLOYEE' 119T 'MISSING' / 012T 'STATE' 041T 'TAXABLE' 055T 'CONTRIBUTION' 073T 'FEDERAL' 090T 'STATE' 111T 'MISSING' 120T 'NAME/' 128T 'IRS' / 002T 'FORM' 011T 'FORM CNT' 022T 'GROSS INCOME' 042T 'INCOME' 059T '(IVC)' 071T 'TAX WITHHELD' 087T 'TAX WITHHELD' 101T 'REPORTED' 111T 'TAX ID' 119T 'ADDRESS' 127T 'HOLDS' / 001T '--------' 011T '--------' 020T '----------------' 037T '----------------' 054T '---------------' 070T '---------------' 086T '--------------' 101T '---------' 111T '-------' 119T '-------' 127T '------' / '1099-INT' 1X #TRST-INT-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-INT-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-INT-INCOME ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-INT-IVC ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-INT-TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-INT-ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-INT-REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-INT-MISS-TIN ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-INT-MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-INT-MISS-HLD ( EM = ZZ,ZZ9 SG = OFF ) // '1099-R  ' 1X #TRST-R---CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-R---GROSS ( EM = ZZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-R---INCOME ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-R---IVC ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-R---TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-R---ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-R---REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-R---MISS-TIN ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-R---MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-R---MISS-HLD ( EM = ZZ,ZZ9 SG = OFF ) // 011T '--------' 020T '----------------' 037T '----------------' 054T '---------------' 070T '---------------' 086T '--------------' 101T '---------' 111T '-------' 119T '-------' 127T '------' /
                        TabSetting(119),"MISSING",NEWLINE,new TabSetting(12),"STATE",new TabSetting(41),"TAXABLE",new TabSetting(55),"CONTRIBUTION",new 
                        TabSetting(73),"FEDERAL",new TabSetting(90),"STATE",new TabSetting(111),"MISSING",new TabSetting(120),"NAME/",new TabSetting(128),"IRS",NEWLINE,new 
                        TabSetting(2),"FORM",new TabSetting(11),"FORM CNT",new TabSetting(22),"GROSS INCOME",new TabSetting(42),"INCOME",new TabSetting(59),"(IVC)",new 
                        TabSetting(71),"TAX WITHHELD",new TabSetting(87),"TAX WITHHELD",new TabSetting(101),"REPORTED",new TabSetting(111),"TAX ID",new 
                        TabSetting(119),"ADDRESS",new TabSetting(127),"HOLDS",NEWLINE,new TabSetting(1),"--------",new TabSetting(11),"--------",new TabSetting(20),"----------------",new 
                        TabSetting(37),"----------------",new TabSetting(54),"---------------",new TabSetting(70),"---------------",new TabSetting(86),"--------------",new 
                        TabSetting(101),"---------",new TabSetting(111),"-------",new TabSetting(119),"-------",new TabSetting(127),"------",NEWLINE,"1099-INT",new 
                        ColumnSpacing(1),pnd_Trst_Int_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Gross, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Income, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Ivc, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Trst_Int_Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_St_Tax, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Reported, new ReportEditMask ("Z,ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Miss_Tin, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Trst_Int_Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Int_Miss_Hld, 
                        new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,"1099-R  ",new ColumnSpacing(1),pnd_Trst_R___Ctr, new ReportEditMask 
                        ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Gross, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"), new SignPosition 
                        (false),new ColumnSpacing(1),pnd_Trst_R___Income, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Ivc, 
                        new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___St_Tax, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Trst_R___Reported, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Miss_Tin, 
                        new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_R___Miss_Hld, new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,new 
                        TabSetting(11),"--------",new TabSetting(20),"----------------",new TabSetting(37),"----------------",new TabSetting(54),"---------------",new 
                        TabSetting(70),"---------------",new TabSetting(86),"--------------",new TabSetting(101),"---------",new TabSetting(111),"-------",new 
                        TabSetting(119),"-------",new TabSetting(127),"------",NEWLINE);
                    if (Global.isEscape()) return;
                    //*  071T 'FEDERAL'
                    //*  088T 'STATE'
                    //*  1X #TRST-R---ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                    pnd_Trst_Tot_Ctr.compute(new ComputeParameters(false, pnd_Trst_Tot_Ctr), pnd_Trst_Int_Ctr.add(pnd_Trst_R___Ctr));                                     //Natural: ADD #TRST-INT-CTR #TRST-R---CTR GIVING #TRST-TOT-CTR
                    pnd_Trst_Tot_Gross.compute(new ComputeParameters(false, pnd_Trst_Tot_Gross), pnd_Trst_Int_Gross.add(pnd_Trst_R___Gross).add(pnd_Trst__Fix_Gross));    //Natural: ADD #TRST-INT-GROSS #TRST-R---GROSS #TRST--FIX-GROSS GIVING #TRST-TOT-GROSS
                    pnd_Trst_Tot_Income.compute(new ComputeParameters(false, pnd_Trst_Tot_Income), pnd_Trst_Int_Income.add(pnd_Trst_R___Income).add(pnd_Trst__Fix_Income)); //Natural: ADD #TRST-INT-INCOME #TRST-R---INCOME #TRST--FIX-INCOME GIVING #TRST-TOT-INCOME
                    pnd_Trst_Tot_Ivc.compute(new ComputeParameters(false, pnd_Trst_Tot_Ivc), pnd_Trst_Int_Ivc.add(pnd_Trst_R___Ivc).add(pnd_Trst__Fix_Ivc));              //Natural: ADD #TRST-INT-IVC #TRST-R---IVC #TRST--FIX-IVC GIVING #TRST-TOT-IVC
                    pnd_Trst_Tot_Tax.compute(new ComputeParameters(false, pnd_Trst_Tot_Tax), pnd_Trst_Int_Tax.add(pnd_Trst_R___Tax));                                     //Natural: ADD #TRST-INT-TAX #TRST-R---TAX GIVING #TRST-TOT-TAX
                    pnd_Trst_Tot_St_Tax.compute(new ComputeParameters(false, pnd_Trst_Tot_St_Tax), pnd_Trst_Int_St_Tax.add(pnd_Trst_R___St_Tax));                         //Natural: ADD #TRST-INT-ST-TAX #TRST-R---ST-TAX GIVING #TRST-TOT-ST-TAX
                    pnd_Trst_Tot_Reported.compute(new ComputeParameters(false, pnd_Trst_Tot_Reported), pnd_Trst_Int_Reported.add(pnd_Trst_R___Reported));                 //Natural: ADD #TRST-INT-REPORTED #TRST-R---REPORTED GIVING #TRST-TOT-REPORTED
                    pnd_Trst_Tot_Miss_Tin.compute(new ComputeParameters(false, pnd_Trst_Tot_Miss_Tin), pnd_Trst_Int_Miss_Tin.add(pnd_Trst_R___Miss_Tin));                 //Natural: ADD #TRST-INT-MISS-TIN #TRST-R---MISS-TIN GIVING #TRST-TOT-MISS-TIN
                    pnd_Trst_Tot_Miss_Nad.compute(new ComputeParameters(false, pnd_Trst_Tot_Miss_Nad), pnd_Trst_Int_Miss_Nad.add(pnd_Trst_R___Miss_Nad));                 //Natural: ADD #TRST-INT-MISS-NAD #TRST-R---MISS-NAD GIVING #TRST-TOT-MISS-NAD
                    pnd_Trst_Tot_Miss_Hld.compute(new ComputeParameters(false, pnd_Trst_Tot_Miss_Hld), pnd_Trst_Int_Miss_Hld.add(pnd_Trst_R___Miss_Hld));                 //Natural: ADD #TRST-INT-MISS-HLD #TRST-R---MISS-HLD GIVING #TRST-TOT-MISS-HLD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    getReports().write(1, "TOTALS  ",new ColumnSpacing(1),pnd_Trst_Tot_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new                //Natural: WRITE ( 1 ) 'TOTALS  ' 1X #TRST-TOT-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-TOT-GROSS ( EM = ZZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-TOT-INCOME ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-TOT-IVC ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-TOT-TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-TOT-ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #TRST-TOT-REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #TRST-TOT-MISS-TIN ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-TOT-MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #TRST-TOT-MISS-HLD ( EM = ZZ,ZZ9 SG = OFF ) // 40X '**TRST TOTAL 9B AMT ' #TRST-R---9B-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 2X '**TRST NYC TAX AMT ' #TRST-R---LC-TAX ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
                        ColumnSpacing(1),pnd_Trst_Tot_Gross, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Income, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Ivc, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Trst_Tot_St_Tax, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Reported, 
                        new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Miss_Tin, new ReportEditMask ("ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Trst_Tot_Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Trst_Tot_Miss_Hld, new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),NEWLINE,NEWLINE,new ColumnSpacing(40),"**TRST TOTAL 9B AMT ",pnd_Trst_R___9b_Amt, 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),"**TRST NYC TAX AMT ",pnd_Trst_R___Lc_Tax, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
                    if (Global.isEscape()) return;
                    //*  1X #TRST-TOT-ST-TAX   (EM=ZZ,ZZZ,ZZ9.99 SG=OFF)
                    pnd_Grand_Tot_Ctr.compute(new ComputeParameters(false, pnd_Grand_Tot_Ctr), pnd_Cref_Tot_Ctr.add(pnd_Tiaa_Tot_Ctr).add(pnd_Life_Tot_Ctr).add(pnd_Tcii_Tot_Ctr).add(pnd_Trst_Tot_Ctr)); //Natural: ADD #CREF-TOT-CTR #TIAA-TOT-CTR #LIFE-TOT-CTR #TCII-TOT-CTR #TRST-TOT-CTR GIVING #GRAND-TOT-CTR
                    pnd_Grand_Tot_Gross.compute(new ComputeParameters(false, pnd_Grand_Tot_Gross), pnd_Cref_Tot_Gross.add(pnd_Tiaa_Tot_Gross).add(pnd_Life_Tot_Gross).add(pnd_Tcii_Tot_Gross).add(pnd_Trst_Tot_Gross)); //Natural: ADD #CREF-TOT-GROSS #TIAA-TOT-GROSS #LIFE-TOT-GROSS #TCII-TOT-GROSS #TRST-TOT-GROSS GIVING #GRAND-TOT-GROSS
                    pnd_Grand_Tot_Income.compute(new ComputeParameters(false, pnd_Grand_Tot_Income), pnd_Cref_Tot_Income.add(pnd_Tiaa_Tot_Income).add(pnd_Life_Tot_Income).add(pnd_Tcii_Tot_Income).add(pnd_Trst_Tot_Income)); //Natural: ADD #CREF-TOT-INCOME #TIAA-TOT-INCOME #LIFE-TOT-INCOME #TCII-TOT-INCOME #TRST-TOT-INCOME GIVING #GRAND-TOT-INCOME
                    pnd_Grand_Tot_Ivc.compute(new ComputeParameters(false, pnd_Grand_Tot_Ivc), pnd_Cref_Tot_Ivc.add(pnd_Tiaa_Tot_Ivc).add(pnd_Life_Tot_Ivc).add(pnd_Tcii_Tot_Ivc).add(pnd_Trst_Tot_Ivc)); //Natural: ADD #CREF-TOT-IVC #TIAA-TOT-IVC #LIFE-TOT-IVC #TCII-TOT-IVC #TRST-TOT-IVC GIVING #GRAND-TOT-IVC
                    pnd_Grand_Tot_Tax.compute(new ComputeParameters(false, pnd_Grand_Tot_Tax), pnd_Cref_Tot_Tax.add(pnd_Tiaa_Tot_Tax).add(pnd_Life_Tot_Tax).add(pnd_Tcii_Tot_Tax).add(pnd_Trst_Tot_Tax)); //Natural: ADD #CREF-TOT-TAX #TIAA-TOT-TAX #LIFE-TOT-TAX #TCII-TOT-TAX #TRST-TOT-TAX GIVING #GRAND-TOT-TAX
                    pnd_Grand_Tot_St_Tax.compute(new ComputeParameters(false, pnd_Grand_Tot_St_Tax), pnd_Cref_Tot_St_Tax.add(pnd_Tiaa_Tot_St_Tax).add(pnd_Life_Tot_St_Tax).add(pnd_Tcii_Tot_St_Tax).add(pnd_Trst_Tot_St_Tax)); //Natural: ADD #CREF-TOT-ST-TAX #TIAA-TOT-ST-TAX #LIFE-TOT-ST-TAX #TCII-TOT-ST-TAX #TRST-TOT-ST-TAX GIVING #GRAND-TOT-ST-TAX
                    pnd_Grand_Tot_Reported.compute(new ComputeParameters(false, pnd_Grand_Tot_Reported), pnd_Cref_Tot_Reported.add(pnd_Tiaa_Tot_Reported).add(pnd_Life_Tot_Reported).add(pnd_Tcii_Tot_Reported).add(pnd_Trst_Tot_Reported)); //Natural: ADD #CREF-TOT-REPORTED #TIAA-TOT-REPORTED #LIFE-TOT-REPORTED #TCII-TOT-REPORTED #TRST-TOT-REPORTED GIVING #GRAND-TOT-REPORTED
                    pnd_Grand_Tot_Miss_Tin.compute(new ComputeParameters(false, pnd_Grand_Tot_Miss_Tin), pnd_Cref_Tot_Miss_Tin.add(pnd_Tiaa_Tot_Miss_Tin).add(pnd_Life_Tot_Miss_Tin).add(pnd_Tcii_Tot_Miss_Tin).add(pnd_Trst_Tot_Miss_Tin)); //Natural: ADD #CREF-TOT-MISS-TIN #TIAA-TOT-MISS-TIN #LIFE-TOT-MISS-TIN #TCII-TOT-MISS-TIN #TRST-TOT-MISS-TIN GIVING #GRAND-TOT-MISS-TIN
                    pnd_Grand_Tot_Miss_Nad.compute(new ComputeParameters(false, pnd_Grand_Tot_Miss_Nad), pnd_Cref_Tot_Miss_Nad.add(pnd_Tiaa_Tot_Miss_Nad).add(pnd_Life_Tot_Miss_Nad).add(pnd_Tcii_Tot_Miss_Nad).add(pnd_Trst_Tot_Miss_Nad)); //Natural: ADD #CREF-TOT-MISS-NAD #TIAA-TOT-MISS-NAD #LIFE-TOT-MISS-NAD #TCII-TOT-MISS-NAD #TRST-TOT-MISS-NAD GIVING #GRAND-TOT-MISS-NAD
                    pnd_Grand_Tot_Miss_Hld.compute(new ComputeParameters(false, pnd_Grand_Tot_Miss_Hld), pnd_Cref_Tot_Miss_Hld.add(pnd_Tiaa_Tot_Miss_Hld).add(pnd_Life_Tot_Miss_Hld).add(pnd_Tcii_Tot_Miss_Hld).add(pnd_Trst_Tot_Miss_Hld)); //Natural: ADD #CREF-TOT-MISS-HLD #TIAA-TOT-MISS-HLD #LIFE-TOT-MISS-HLD #TCII-TOT-MISS-HLD #TRST-TOT-MISS-HLD GIVING #GRAND-TOT-MISS-HLD
                    getReports().skip(1, 3);                                                                                                                              //Natural: SKIP ( 1 ) 3
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    //*  DUTTAD
                    getReports().write(1, "GRAND",NEWLINE,"TOTALS  ",new ColumnSpacing(1),pnd_Grand_Tot_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition           //Natural: WRITE ( 1 ) 'GRAND' / 'TOTALS  ' 1X #GRAND-TOT-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #GRAND-TOT-GROSS ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #GRAND-TOT-INCOME ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #GRAND-TOT-IVC ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #GRAND-TOT-TAX ( EM = ZZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #GRAND-TOT-ST-TAX ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 1X #GRAND-TOT-REPORTED ( EM = Z,ZZZ,ZZ9 SG = OFF ) 1X #GRAND-TOT-MISS-TIN ( EM = ZZZ,ZZ9 SG = OFF ) 1X #GRAND-TOT-MISS-NAD ( EM = ZZZ,ZZ9 SG = OFF ) 1X #GRAND-TOT-MISS-HLD ( EM = ZZ,ZZ9 SG = OFF ) / 011T '========' 020T '================' 037T '================' 054T '===============' 070T '===============' 086T '==============' 101T '=========' 111T '=======' 119T '=======' 127T '======' /
                        (false),new ColumnSpacing(1),pnd_Grand_Tot_Gross, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Income, 
                        new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Ivc, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Tax, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Grand_Tot_St_Tax, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Reported, 
                        new ReportEditMask ("Z,ZZZ,ZZ9"), new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Miss_Tin, new ReportEditMask ("ZZZ,ZZ9"), 
                        new SignPosition (false),new ColumnSpacing(1),pnd_Grand_Tot_Miss_Nad, new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new 
                        ColumnSpacing(1),pnd_Grand_Tot_Miss_Hld, new ReportEditMask ("ZZ,ZZ9"), new SignPosition (false),NEWLINE,new TabSetting(11),"========",new 
                        TabSetting(20),"================",new TabSetting(37),"================",new TabSetting(54),"===============",new TabSetting(70),"===============",new 
                        TabSetting(86),"==============",new TabSetting(101),"=========",new TabSetting(111),"=======",new TabSetting(119),"=======",new 
                        TabSetting(127),"======",NEWLINE);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-SUB-TOTALS
                //* *------------
                getReports().write(0, "IRA TIAA TOTALS =",cntr_Tiaa_Ira,"IRA TRST TOTALS =",cntr_Trst_Ira);                                                               //Natural: WRITE 'IRA TIAA TOTALS =' CNTR-TIAA-IRA 'IRA TRST TOTALS =' CNTR-TRST-IRA
                if (Global.isEscape()) return;
                //*  'IRA CREF TOTALS =' CNTR-CREF-IRA
                //*  'IRA LIFE TOTALS =' CNTR-LIFE-IRA
                //*  'IRA TCII TOTALS =' CNTR-TCII-IRA
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE 'TWRP3310-01' 41X 'Tax Withholding & Payment System' 41X 'PAGE  1' /50X 'NON-PERIODIC CONTROL REPORT' #WS-TAX-YEAR ( EM = 9999 ) 41X *DATU /55X 'IRS ORIGINAL REPORTING' #WS-REJECTS 46X *TIME ( EM = XXXXXXXX ) ///
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 2 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //*  12-11-00 FRANK
                pnd_Ws_Recs_Written.compute(new ComputeParameters(false, pnd_Ws_Recs_Written), pnd_Ws_Recs_Written.add(a_Record_Ctr).add(b_Recs_Written).add(c_Record_Ctr).add(f_Record_Ctr).add(k_Record_Ctr).add(t_Record_Ctr)); //Natural: ADD A-RECORD-CTR B-RECS-WRITTEN C-RECORD-CTR F-RECORD-CTR K-RECORD-CTR T-RECORD-CTR TO #WS-RECS-WRITTEN
                //*  12-11-00 FRANK
                if (condition(pnd_Ws_Rejects_File.equals("Y")))                                                                                                           //Natural: IF #WS-REJECTS-FILE = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, "THE # OF RECS FOR",pnd_Ws_Save_Prdct_Name,"TO BE ELECTRONICALLY TRANSMITTED ARE:",pnd_Ws_Recs_Written);                        //Natural: WRITE ( 01 ) 'THE # OF RECS FOR' #WS-SAVE-PRDCT-NAME 'TO BE ELECTRONICALLY TRANSMITTED ARE:' #WS-RECS-WRITTEN
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  ENVIRONMENT ROUTINE        /* 11-13-2008
                //* *----------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ENVIRONMENT-IDENTIFICATION
                //* *----------------------------------------**
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  1991 - NEW REPORT
    private void sub_State_Sub_Totals() throws Exception                                                                                                                  //Natural: STATE-SUB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  12-09-00 FRANK
        if (condition(pnd_K.equals(1)))                                                                                                                                   //Natural: IF #K = 1
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EINCHG
        //*  EINCHG
        //*  DY1
        //*  DY1
        short decideConditionsMet2814 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-STATE-PRODUCT;//Natural: VALUE 'C'
        if (condition((pnd_Ws_State_Product.equals("C"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** CREF **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** CREF **'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Ws_State_Product.equals("L"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** LIFE **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** LIFE **'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Ws_State_Product.equals("T"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** TIAA **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** TIAA **'
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((pnd_Ws_State_Product.equals("A"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** ADMN **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** ADMN **'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Ws_State_Product.equals("S"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** TCII **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** TCII **'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Ws_State_Product.equals("F"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** FSBC **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** FSBC **'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Ws_State_Product.equals("X"))))
        {
            decideConditionsMet2814++;
            pnd_T_C_Title.setValue("** TRST **");                                                                                                                         //Natural: ASSIGN #T-C-TITLE := '** TRST **'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().setValue(state_Totals_St_B_Rec_Ctr.getValue(pnd_K));                                                       //Natural: MOVE ST-B-REC-CTR ( #K ) TO IRS-K-NUMBER-OF-PAYEES
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().setValue(state_Totals_State_Tot_1.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-1 ( #K ) TO IRS-K-CONTROL-TOTAL-1
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().setValue(state_Totals_State_Tot_2.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-2 ( #K ) TO IRS-K-CONTROL-TOTAL-2
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_3().setValue(state_Totals_State_Tot_3.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-3 ( #K ) TO IRS-K-CONTROL-TOTAL-3
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().setValue(state_Totals_State_Tot_4.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-4 ( #K ) TO IRS-K-CONTROL-TOTAL-4
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().setValue(state_Totals_State_Tot_5.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-5 ( #K ) TO IRS-K-CONTROL-TOTAL-5
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_6().setValue(state_Totals_State_Tot_6.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-6 ( #K ) TO IRS-K-CONTROL-TOTAL-6
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_7().setValue(state_Totals_State_Tot_7.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-7 ( #K ) TO IRS-K-CONTROL-TOTAL-7
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().setValue(state_Totals_State_Tot_9.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-9 ( #K ) TO IRS-K-CONTROL-TOTAL-9
        //* 03-01-99 FC
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().setValue(state_Totals_State_Tot_10.getValue(pnd_K));                                                        //Natural: MOVE STATE-TOT-10 ( #K ) TO IRS-K-ST-TAX-WITHHELD
        //*   RCC
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_B().setValue(state_Totals_State_Tot_B.getValue(pnd_K));                                                         //Natural: MOVE STATE-TOT-B ( #K ) TO IRS-K-CONTROL-TOTAL-B
        //* *  COMMENTED-OUT 02-25-04 BY FRANK
        //* *MOVE STATE-TOT-11 (#K) TO IRS-K-CONTROL-TOTAL-B   /*03-01-99 FC
        //*  12-29-00 FRANK
        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().setValueEdited(state_Totals_State_Code.getValue(pnd_K),new ReportEditMask("99"));                                //Natural: MOVE EDITED STATE-CODE ( #K ) ( EM = 99 ) TO IRS-K-STATE-CODE
        //*  GROSS INCOME
        //*  STATE TAX WITHHELD
        //*  LOCAL GROSS INCOME
        //*  LOCAL TAX WITHHELD
        getReports().write(2, NEWLINE,new TabSetting(1),state_Totals_State_Code.getValue(pnd_K),new TabSetting(6),state_Name.getValue(pnd_K),new TabSetting(24),state_Totals_St_B_Rec_Ctr.getValue(pnd_K),  //Natural: WRITE ( 2 ) / 01T STATE-CODE ( #K ) 06T STATE-NAME ( #K ) 24T ST-B-REC-CTR ( #K ) ( EM = Z,ZZZ,ZZ9 SG = OFF ) 33T STATE-TOT-1 ( #K ) ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 50T STATE-TOT-10 ( #K ) ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 65T LOCAL-TOT-1 ( #K ) ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 81T STATE-TOT-11 ( #K ) ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF )
            new ReportEditMask ("ZZZ,ZZ9"), new SignPosition (false),new TabSetting(33),state_Totals_State_Tot_1.getValue(pnd_K), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), 
            new SignPosition (false),new TabSetting(50),state_Totals_State_Tot_10.getValue(pnd_K), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition 
            (false),new TabSetting(65),local_Totals_Local_Tot_1.getValue(pnd_K), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(81),state_Totals_State_Tot_11.getValue(pnd_K), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false));
        if (Global.isEscape()) return;
        //*  02-03-00 FRANK
        if (condition(pnd_K.equals(pnd_Max_States)))                                                                                                                      //Natural: IF #K = #MAX-STATES
        {
            //*  02-03-00 FRANK
            getReports().write(2, NEWLINE,new TabSetting(1),"TOTALS",new TabSetting(22),pnd_Ws_Subtotal_Rec_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"), new                    //Natural: WRITE ( 2 ) / 1T 'TOTALS' 22T #WS-SUBTOTAL-REC-CTR ( EM = Z,ZZZ,ZZ9 SG = OFF ) 33T #WS-SUBTOTAL-STATE-TOT-1 ( EM = Z,ZZZ,ZZZ,ZZ9.99 SG = OFF ) 50T #WS-SUBTOTAL-STATE-TOT-10 ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 65T #WS-SUBTOTAL-LOCAL-TOT-1 ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF ) 81T #WS-SUBTOTAL-STATE-TOT-11 ( EM = ZZZ,ZZZ,ZZ9.99 SG = OFF )
                SignPosition (false),new TabSetting(33),pnd_Ws_Subtotal_State_Tot_1, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new 
                TabSetting(50),pnd_Ws_Subtotal_State_Tot_10, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(65),pnd_Ws_Subtotal_Local_Tot_1, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), new SignPosition (false),new TabSetting(81),pnd_Ws_Subtotal_State_Tot_11, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"), 
                new SignPosition (false));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  12-09-00 FRANK
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                                 //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        FOR06:                                                                                                                                                            //Natural: FOR #I = 0 TO 99
        for (pnd_I.setValue(0); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_I,new ReportEditMask("999"));                                                                      //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                             //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS-TAX-YEAR
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().equals(true) || ! (pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().getBoolean())))                           //Natural: IF #RETURN-CDE = TRUE OR NOT TWRATBL2.#DISPLAY-IND
            {
                pnd_State_Table_Pnd_Ws_State_Seq_Nbr.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr());                                    //Natural: ASSIGN #WS-STATE-SEQ-NBR ( #I ) := TIRCNTL-SEQ-NBR
                pnd_State_Table_Pnd_Ws_State_Alpha.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                             //Natural: ASSIGN #WS-STATE-ALPHA ( #I ) := TIRCNTL-STATE-ALPHA-CODE
                pnd_State_Table_Pnd_Ws_State_Full_Name.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Full_Name());                          //Natural: ASSIGN #WS-STATE-FULL-NAME ( #I ) := TIRCNTL-STATE-FULL-NAME
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                      //Natural: ASSIGN #WS-STATE-TAX-ID-TIAA ( #I ) := TIRCNTL-STATE-TAX-ID-TIAA
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Cref.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Cref());                      //Natural: ASSIGN #WS-STATE-TAX-ID-CREF ( #I ) := TIRCNTL-STATE-TAX-ID-CREF
                pnd_State_Table_Pnd_Ws_State_Tax_Id_Life.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Life());                      //Natural: ASSIGN #WS-STATE-TAX-ID-LIFE ( #I ) := TIRCNTL-STATE-TAX-ID-LIFE
                pnd_State_Table_Pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                          //Natural: ASSIGN #WS-STATE-COMB-FED-IND ( #I ) := TWRATBL2.TIRCNTL-COMB-FED-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Environment_Identification() throws Exception                                                                                                        //Natural: ENVIRONMENT-IDENTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------**
        //*  DETERMINE IF 'PROD' OR 'TEST' ENVIRONMENT.
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"- Calling Sub-Program 'TWRN0001'...");                                                                                 //Natural: WRITE ( 00 ) *PROGRAM '- Calling Sub-Program "TWRN0001"...'
        if (Global.isEscape()) return;
        pdaTwra0001.getTwra0001().reset();                                                                                                                                //Natural: RESET TWRA0001
        DbsUtil.callnat(Twrn0001.class , getCurrentProcessState(), pdaTwra0001.getTwra0001());                                                                            //Natural: CALLNAT 'TWRN0001' TWRA0001
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Prod_Test());                                                                                               //Natural: WRITE ( 00 ) '=' TWRA0001.#PROD-TEST
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return());                                                                                         //Natural: WRITE ( 00 ) '=' TWRA0001.#TWRN0001-RETURN
        if (Global.isEscape()) return;
        //* *----------------------------------------**
        //*  ENVIRONMENT-IDENTIFICATION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *----------------
                    getReports().write(2, "TWRP3310-02",new TabSetting(50),"Tax Withholding & Payment System",new TabSetting(123),"PAGE",getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) 'TWRP3310-02' 50T 'Tax Withholding & Payment System' 123T 'PAGE' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / 58T 'FEDERAL COMBINED' 123T *DATU / 47T 'NON-PERIODIC STATE CONTROL REPORT' #WS-TAX-YEAR ( EM = 9999 ) 123T *TIME ( EM = XXXXXXXX ) / / 55T 'IRS ORIGINAL REPORTING' #WS-REJECTS
                        new ReportEditMask ("ZZ9"),NEWLINE,new TabSetting(58),"FEDERAL COMBINED",new TabSetting(123),Global.getDATU(),NEWLINE,new TabSetting(47),"NON-PERIODIC STATE CONTROL REPORT",pnd_Ws_Tax_Year, 
                        new ReportEditMask ("9999"),new TabSetting(123),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(55),
                        "IRS ORIGINAL REPORTING",pnd_Ws_Rejects);
                    getReports().write(2, new ColumnSpacing(61),pnd_T_C_Title,NEWLINE,new ColumnSpacing(69),"        ",NEWLINE,new TabSetting(1),"IRS",new                //Natural: WRITE ( 2 ) 61X #T-C-TITLE /69X '        ' / 1T 'IRS' 6T 'STATE' 25T 'STATE' 38T 'STATE' 55T 'STATE' 70T 'LOCAL' 87T 'LOCAL' / 1T 'CDE' 6T 'NAME' 25T 'FORMS' 35T 'DISTRIBUTION' 52T 'TAX WITHHELD' 67T 'DISTRIBUTION' 83T 'TAX WITHHELD' / 1T '---' 5T '---------------' 24T '--------' 34T '---------------' 50T '---------------' 66T '--------------' 82T '--------------' /
                        TabSetting(6),"STATE",new TabSetting(25),"STATE",new TabSetting(38),"STATE",new TabSetting(55),"STATE",new TabSetting(70),"LOCAL",new 
                        TabSetting(87),"LOCAL",NEWLINE,new TabSetting(1),"CDE",new TabSetting(6),"NAME",new TabSetting(25),"FORMS",new TabSetting(35),"DISTRIBUTION",new 
                        TabSetting(52),"TAX WITHHELD",new TabSetting(67),"DISTRIBUTION",new TabSetting(83),"TAX WITHHELD",NEWLINE,new TabSetting(1),"---",new 
                        TabSetting(5),"---------------",new TabSetting(24),"--------",new TabSetting(34),"---------------",new TabSetting(50),"---------------",new 
                        TabSetting(66),"--------------",new TabSetting(82),"--------------",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");

        getReports().write(1, ReportOption.TITLE,"TWRP3310-01",new ColumnSpacing(41),"Tax Withholding & Payment System",new ColumnSpacing(41),"PAGE  1",NEWLINE,new 
            ColumnSpacing(50),"NON-PERIODIC CONTROL REPORT",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),new ColumnSpacing(41),Global.getDATU(),NEWLINE,new 
            ColumnSpacing(55),"IRS ORIGINAL REPORTING",pnd_Ws_Rejects,new ColumnSpacing(46),Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,
            NEWLINE);
    }
}
