/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:47 PM
**        * FROM NATURAL PROGRAM : Twrp5500
************************************************************
**        * FILE NAME            : Twrp5500.java
**        * CLASS NAME           : Twrp5500
**        * INSTANCE NAME        : Twrp5500
************************************************************
************************************************************************
** PROGRAM : TWRP5500
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: THIS PROGRAM GENERATES PARM FOR PAYMNET/FORM RECONCILIATION
**           REPORTS.
** HISTORY.....:
** MM/DD/YYYY - MICHAEL SUPONITSKY - XXXXXXXXXXXXXXXXXXXXX
**
** 12/06/1999 - EDITH - UPDATED TO CREATE PARAMETER FOR DAILY RUN
**                      AND  TO ACCEPT BLANK TAX YEAR AS INPUT
** 12/27/1999 - EDITH - READ CONTROL RECORD FOR THE INTERFACE DATE
**                      TO BE USED BY TWRP0901
** 06/20/2007 : A. YOUNG - REVISED FOR TAXWARS DATA WAREHOUSE.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5500 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year_A;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Intf_To_Date;
    private DbsField pnd_Intf_Date;
    private DbsField pnd_Sytd_From_Date;
    private DbsField pnd_Datn;
    private DbsField pnd_T_Date;
    private DbsField pnd_Monthly_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Tax_Year_A);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Intf_To_Date = localVariables.newFieldInRecord("pnd_Intf_To_Date", "#INTF-TO-DATE", FieldType.STRING, 8);
        pnd_Intf_Date = localVariables.newFieldInRecord("pnd_Intf_Date", "#INTF-DATE", FieldType.STRING, 8);
        pnd_Sytd_From_Date = localVariables.newFieldInRecord("pnd_Sytd_From_Date", "#SYTD-FROM-DATE", FieldType.STRING, 8);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.NUMERIC, 4);
        pnd_T_Date = localVariables.newFieldInRecord("pnd_T_Date", "#T-DATE", FieldType.DATE);
        pnd_Monthly_Ind = localVariables.newFieldInRecord("pnd_Monthly_Ind", "#MONTHLY-IND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5500() throws Exception
    {
        super("Twrp5500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 68T 'Report: RPT1' / //
        getWorkFiles().read(4, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 4 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Intf_To_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                                 //Natural: ASSIGN #INTF-TO-DATE := #TWRP0600-TO-CCYYMMDD
        pnd_Intf_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                                                             //Natural: ASSIGN #INTF-DATE := #TWRP0600-INTERFACE-CCYYMMDD
        pnd_Sytd_From_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                             //Natural: ASSIGN #SYTD-FROM-DATE := #TWRP0600-FROM-CCYYMMDD
        pnd_Datn.setValueEdited(new ReportEditMask("9999"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                      //Natural: MOVE EDITED #TWRP0600-TAX-YEAR-CCYY TO #DATN ( EM = 9999 )
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        //*  12/27/99
        ldaTwrl0600.getPnd_Twrp0600_Control_Record().reset();                                                                                                             //Natural: RESET #TWRP0600-CONTROL-RECORD
        //*                                   ( YTD & DAILY PARAMETER )
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Intf_Date);                                                             //Natural: ASSIGN #TWRP0600-CONTROL-RECORD.#TWRP0600-INTERFACE-CCYYMMDD := #INTF-DATE
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                           //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #TWRP0600-CONTROL-RECORD.#TWRP0600-TAX-YEAR-CCYY
        //*                                   ( YTD PARAMETER )
        getWorkFiles().write(2, false, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: WRITE WORK FILE 2 #TWRP0600-CONTROL-RECORD
        //*                                   ( DAILY PARAMETER )
        //*  06/20/2007
        if (condition(pnd_Monthly_Ind.getBoolean()))                                                                                                                      //Natural: IF #MONTHLY-IND
        {
            pnd_T_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Intf_Date);                                                                                      //Natural: MOVE EDITED #INTF-DATE TO #T-DATE ( EM = YYYYMMDD )
            pnd_Intf_To_Date.setValueEdited(pnd_T_Date,new ReportEditMask("YYYYMMDD"));                                                                                   //Natural: MOVE EDITED #T-DATE ( EM = YYYYMMDD ) TO #INTF-TO-DATE
            pnd_Sytd_From_Date.setValueEdited(pnd_T_Date,new ReportEditMask("YYYYMM01"));                                                                                 //Natural: MOVE EDITED #T-DATE ( EM = YYYYMM01 ) TO #SYTD-FROM-DATE
            //*  06/20/2007
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Intf_To_Date);                                                                 //Natural: ASSIGN #TWRP0600-CONTROL-RECORD.#TWRP0600-TO-CCYYMMDD := #INTF-TO-DATE
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Sytd_From_Date);                                                             //Natural: ASSIGN #TWRP0600-CONTROL-RECORD.#TWRP0600-FROM-CCYYMMDD := #SYTD-FROM-DATE
        //*  06/20/2007
        getWorkFiles().write(3, false, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: WRITE WORK FILE 3 #TWRP0600-CONTROL-RECORD
        getReports().write(1, new TabSetting(7),"Payment vs. Form Reconciliation Parameters:",NEWLINE,new TabSetting(7),"Tax Year ...........:",pnd_Ws_Pnd_Tax_Year,      //Natural: WRITE ( 1 ) 7T 'Payment vs. Form Reconciliation Parameters:' / 07T 'Tax Year ...........:' #WS.#TAX-YEAR / 07T 'Interface Date .....:' #INTF-DATE / 07T 'Interface From Date :' #SYTD-FROM-DATE / 07T 'Interface To Date ..:' #INTF-TO-DATE
            new ReportEditMask ("9999"),NEWLINE,new TabSetting(7),"Interface Date .....:",pnd_Intf_Date,NEWLINE,new TabSetting(7),"Interface From Date :",pnd_Sytd_From_Date,NEWLINE,new 
            TabSetting(7),"Interface To Date ..:",pnd_Intf_To_Date);
        if (Global.isEscape()) return;
        //* **********************
        //*  S U B R O U T I N E S
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getWorkFiles().read(1, pnd_Ws_Pnd_Tax_Year_A);                                                                                                                    //Natural: READ WORK FILE 1 ONCE #WS.#TAX-YEAR-A
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***   INPUT PARAMETER IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***   INPUT PARAMETER IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  06/20/2007
        //*  NEXT YEAR
        //*  CURRENT YR
        //*  CURRENT YR - 1
        //*  CURRENT YR - 2
        //*  CURRENT YR - 3
        short decideConditionsMet111 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #WS.#TAX-YEAR-A;//Natural: VALUE '+1'
        if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("+1"))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), pnd_Datn.add(1));                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #DATN + 1
        }                                                                                                                                                                 //Natural: VALUE ' ', '0'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals(" ") || pnd_Ws_Pnd_Tax_Year_A.equals("0"))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Tax_Year.setValue(pnd_Datn);                                                                                                                       //Natural: ASSIGN #WS.#TAX-YEAR := #DATN
        }                                                                                                                                                                 //Natural: VALUE '-1'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-1"))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), pnd_Datn.subtract(1));                                                         //Natural: ASSIGN #WS.#TAX-YEAR := #DATN - 1
        }                                                                                                                                                                 //Natural: VALUE '-2'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-2"))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), pnd_Datn.subtract(2));                                                         //Natural: ASSIGN #WS.#TAX-YEAR := #DATN - 2
            pnd_Monthly_Ind.setValue(true);                                                                                                                               //Natural: ASSIGN #MONTHLY-IND := TRUE
        }                                                                                                                                                                 //Natural: VALUE '-3'
        else if (condition((pnd_Ws_Pnd_Tax_Year_A.equals("-3"))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), pnd_Datn.subtract(3));                                                         //Natural: ASSIGN #WS.#TAX-YEAR := #DATN - 3
            pnd_Monthly_Ind.setValue(true);                                                                                                                               //Natural: ASSIGN #MONTHLY-IND := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  06/20/2007
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
