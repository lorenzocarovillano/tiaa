/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:31 PM
**        * FROM NATURAL PROGRAM : Twrp8120
************************************************************
**        * FILE NAME            : Twrp8120.java
**        * CLASS NAME           : Twrp8120
**        * INSTANCE NAME        : Twrp8120
************************************************************
************************************************************************
* PROGRAM     :  TWRP8120
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATES A NEW TABLE OF ERROR CODES, AND MESSAGES
*                DATA BASE FILE (003/098) TABLE NUMBER 7
* DATE        :  09/14/2000
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8120 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rerr;
    private DbsField rerr_Tircntl_Tbl_Nbr;
    private DbsField rerr_Tircntl_Tax_Year;
    private DbsField rerr_Tircntl_Seq_Nbr;

    private DbsGroup rerr_Tircntl_Error_Msg_Tbl;
    private DbsField rerr_Tircntl_Error_Type;
    private DbsField rerr_Tircntl_Error_Descr;

    private DataAccessProgramView vw_uerr;
    private DbsField uerr_Tircntl_Tbl_Nbr;
    private DbsField uerr_Tircntl_Tax_Year;
    private DbsField uerr_Tircntl_Seq_Nbr;

    private DbsGroup uerr_Tircntl_Error_Msg_Tbl;
    private DbsField uerr_Tircntl_Error_Type;
    private DbsField uerr_Tircntl_Error_Descr;

    private DataAccessProgramView vw_serr;
    private DbsField serr_Tircntl_Tbl_Nbr;
    private DbsField serr_Tircntl_Tax_Year;
    private DbsField serr_Tircntl_Seq_Nbr;

    private DbsGroup serr_Tircntl_Error_Msg_Tbl;
    private DbsField serr_Tircntl_Error_Type;
    private DbsField serr_Tircntl_Error_Descr;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Seq_Nbr;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Seq_Nbr;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rerr = new DataAccessProgramView(new NameInfo("vw_rerr", "RERR"), "TIRCNTL_ERROR_MSG_TBL_VIEW", "TIR_CONTROL");
        rerr_Tircntl_Tbl_Nbr = vw_rerr.getRecord().newFieldInGroup("rerr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rerr_Tircntl_Tax_Year = vw_rerr.getRecord().newFieldInGroup("rerr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rerr_Tircntl_Seq_Nbr = vw_rerr.getRecord().newFieldInGroup("rerr_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rerr_Tircntl_Error_Msg_Tbl = vw_rerr.getRecord().newGroupInGroup("RERR_TIRCNTL_ERROR_MSG_TBL", "TIRCNTL-ERROR-MSG-TBL");
        rerr_Tircntl_Error_Type = rerr_Tircntl_Error_Msg_Tbl.newFieldInGroup("rerr_Tircntl_Error_Type", "TIRCNTL-ERROR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_ERROR_TYPE");
        rerr_Tircntl_Error_Descr = rerr_Tircntl_Error_Msg_Tbl.newFieldInGroup("rerr_Tircntl_Error_Descr", "TIRCNTL-ERROR-DESCR", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "TIRCNTL_ERROR_DESCR");
        registerRecord(vw_rerr);

        vw_uerr = new DataAccessProgramView(new NameInfo("vw_uerr", "UERR"), "TIRCNTL_ERROR_MSG_TBL_VIEW", "TIR_CONTROL");
        uerr_Tircntl_Tbl_Nbr = vw_uerr.getRecord().newFieldInGroup("uerr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        uerr_Tircntl_Tax_Year = vw_uerr.getRecord().newFieldInGroup("uerr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        uerr_Tircntl_Seq_Nbr = vw_uerr.getRecord().newFieldInGroup("uerr_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        uerr_Tircntl_Error_Msg_Tbl = vw_uerr.getRecord().newGroupInGroup("UERR_TIRCNTL_ERROR_MSG_TBL", "TIRCNTL-ERROR-MSG-TBL");
        uerr_Tircntl_Error_Type = uerr_Tircntl_Error_Msg_Tbl.newFieldInGroup("uerr_Tircntl_Error_Type", "TIRCNTL-ERROR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_ERROR_TYPE");
        uerr_Tircntl_Error_Descr = uerr_Tircntl_Error_Msg_Tbl.newFieldInGroup("uerr_Tircntl_Error_Descr", "TIRCNTL-ERROR-DESCR", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "TIRCNTL_ERROR_DESCR");
        registerRecord(vw_uerr);

        vw_serr = new DataAccessProgramView(new NameInfo("vw_serr", "SERR"), "TIRCNTL_ERROR_MSG_TBL_VIEW", "TIR_CONTROL");
        serr_Tircntl_Tbl_Nbr = vw_serr.getRecord().newFieldInGroup("serr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        serr_Tircntl_Tax_Year = vw_serr.getRecord().newFieldInGroup("serr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        serr_Tircntl_Seq_Nbr = vw_serr.getRecord().newFieldInGroup("serr_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        serr_Tircntl_Error_Msg_Tbl = vw_serr.getRecord().newGroupInGroup("SERR_TIRCNTL_ERROR_MSG_TBL", "TIRCNTL-ERROR-MSG-TBL");
        serr_Tircntl_Error_Type = serr_Tircntl_Error_Msg_Tbl.newFieldInGroup("serr_Tircntl_Error_Type", "TIRCNTL-ERROR-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_ERROR_TYPE");
        serr_Tircntl_Error_Descr = serr_Tircntl_Error_Msg_Tbl.newFieldInGroup("serr_Tircntl_Error_Descr", "TIRCNTL-ERROR-DESCR", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "TIRCNTL_ERROR_DESCR");
        registerRecord(vw_serr);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.NUMERIC, 8);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Seq_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Seq_Nbr", "#S1-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.NUMERIC, 8);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Seq_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Seq_Nbr", "#S2-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rerr.reset();
        vw_uerr.reset();
        vw_serr.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8120() throws Exception
    {
        super("Twrp8120");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8120|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(7);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 7
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Seq_Nbr.setValue(0);                                                                                                                    //Natural: ASSIGN #S1-SEQ-NBR := 0
                vw_rerr.startDatabaseRead                                                                                                                                 //Natural: READ RERR WITH TIRCNTL-NBR-YEAR-SEQ = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") }
                );
                READ01:
                while (condition(vw_rerr.readNextRow("READ01")))
                {
                    if (condition(rerr_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rerr_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                    //Natural: IF RERR.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RERR.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(7);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 7
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Seq_Nbr.setValue(rerr_Tircntl_Seq_Nbr);                                                                                             //Natural: ASSIGN #S2-SEQ-NBR := RERR.TIRCNTL-SEQ-NBR
                    vw_uerr.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) UERR WITH TIRCNTL-NBR-YEAR-SEQ = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") },
                    1
                    );
                    RL2:
                    while (condition(vw_uerr.readNextRow("RL2")))
                    {
                        if (condition(uerr_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && uerr_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year)                  //Natural: IF UERR.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UERR.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UERR.TIRCNTL-SEQ-NBR = #S2-SEQ-NBR
                            && uerr_Tircntl_Seq_Nbr.equals(pnd_Super2_Pnd_S2_Seq_Nbr)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(uerr_Tircntl_Error_Type.equals(rerr_Tircntl_Error_Type) && uerr_Tircntl_Error_Descr.equals(rerr_Tircntl_Error_Descr)))              //Natural: IF UERR.TIRCNTL-ERROR-TYPE = RERR.TIRCNTL-ERROR-TYPE AND UERR.TIRCNTL-ERROR-DESCR = RERR.TIRCNTL-ERROR-DESCR
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET UERR *ISN ( RL2. )
                            vw_uerr.readByID(vw_uerr.getAstISN("RL2"), "G1");
                            uerr_Tircntl_Error_Type.setValue(rerr_Tircntl_Error_Type);                                                                                    //Natural: ASSIGN UERR.TIRCNTL-ERROR-TYPE := RERR.TIRCNTL-ERROR-TYPE
                            uerr_Tircntl_Error_Descr.setValue(rerr_Tircntl_Error_Descr);                                                                                  //Natural: ASSIGN UERR.TIRCNTL-ERROR-DESCR := RERR.TIRCNTL-ERROR-DESCR
                            vw_uerr.updateDBRow("G1");                                                                                                                    //Natural: UPDATE ( G1. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Updt_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        serr_Tircntl_Tbl_Nbr.setValue(rerr_Tircntl_Tbl_Nbr);                                                                                              //Natural: ASSIGN SERR.TIRCNTL-TBL-NBR := RERR.TIRCNTL-TBL-NBR
                        serr_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                              //Natural: ASSIGN SERR.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        serr_Tircntl_Seq_Nbr.setValue(rerr_Tircntl_Seq_Nbr);                                                                                              //Natural: ASSIGN SERR.TIRCNTL-SEQ-NBR := RERR.TIRCNTL-SEQ-NBR
                        serr_Tircntl_Error_Type.setValue(rerr_Tircntl_Error_Type);                                                                                        //Natural: ASSIGN SERR.TIRCNTL-ERROR-TYPE := RERR.TIRCNTL-ERROR-TYPE
                        serr_Tircntl_Error_Descr.setValue(rerr_Tircntl_Error_Descr);                                                                                      //Natural: ASSIGN SERR.TIRCNTL-ERROR-DESCR := RERR.TIRCNTL-ERROR-DESCR
                        vw_serr.insertDBRow();                                                                                                                            //Natural: STORE SERR
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(7);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 7
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Seq_Nbr.setValue(0);                                                                                                                    //Natural: ASSIGN #S1-SEQ-NBR := 0
                vw_rerr.startDatabaseRead                                                                                                                                 //Natural: READ RERR WITH TIRCNTL-NBR-YEAR-SEQ = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") }
                );
                RL3:
                while (condition(vw_rerr.readNextRow("RL3")))
                {
                    if (condition(rerr_Tircntl_Tbl_Nbr.equals(7)))                                                                                                        //Natural: IF RERR.TIRCNTL-TBL-NBR = 7
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rerr_Tircntl_Tbl_Nbr,new TabSetting(5),rerr_Tircntl_Tax_Year,new      //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 19T TIRCNTL-ERROR-TYPE 24T TIRCNTL-ERROR-DESCR
                        TabSetting(11),rerr_Tircntl_Seq_Nbr,new TabSetting(19),rerr_Tircntl_Error_Type,new TabSetting(24),rerr_Tircntl_Error_Descr);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        *ISN (RL3.)
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Error Messages Table Report",new         //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Error Messages Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Error",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Error' 24T '                                        '
                        TabSetting(24),"                                        ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Type ",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Type ' 24T 'Error Description                       '
                        TabSetting(24),"Error Description                       ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '========================================'
                        TabSetting(24),"========================================");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
