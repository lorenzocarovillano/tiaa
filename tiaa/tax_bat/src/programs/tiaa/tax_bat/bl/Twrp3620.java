/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:23 PM
**        * FROM NATURAL PROGRAM : Twrp3620
************************************************************
**        * FILE NAME            : Twrp3620.java
**        * CLASS NAME           : Twrp3620
**        * INSTANCE NAME        : Twrp3620
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP3620
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCES MAG. MEDIA FILES FOR MAG16954133STATE REPORTING.
* CREATED  : 12 / 02 / 2000.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM PRODUCES STATE MAGNETIC MEDIA FILES FOR
*            MAGNETIC MEDIA STATE REPORTING.
*
*  HISTORY:
*
*  05/10/11 RO - REVISED TO INSURE THAT STATE CODE IS
*                DERIVED FROM IRS STATE CODE FOR INDIANA AND THAT
*                COUNTY TAX WITHHELD CONTAINS ZEROS.
*  03/03/11 AY - REVISED TO ACCOMMODATE 15-CHARACTER COMPANY TAX ID
*                (430516559589F01) FOR VERMONT, PER USER REQUEST.
*              - EDIT FOR EMBEDDED BLANKS IN NAME CONTROL (ALL STATES).
*  10/01/10 AY - REVISED DUE TO 2010 IRS CHANGES (TWRL3502 & TWRL3512).
*              - #OUT1A-RET-TYPE := '9 ' (1099-R).
*  12/18/09 AY - REVISED DUE TO 2009 IRS CHANGES (TWRL3502 & TWRL3512).
*  12/31/07 MS - UPDATED FOR 07 KANSAS STATE CHANGES.
*  12/12/07 RM - UPDATED FOR 07 IRS LAYOUT CHANGES (TWRL3502 & TWRL3512)
*  06/03/07  J.ROTHOLZ - NEW SOUTH CAROLINA REQUIREMENTS IMPLEMENTED
*  05/02/07  J.ROTHOLZ - NEW UTAH REQUIREMENTS IMPLEMENTED
*  01/30/06  J.ROTHOLZ - NEW NEBRASKA REQUIREMENTS IMPLEMENTED
*  01/22/05 MS - TOPS TIAA & TRUST PROCESSING
*  08/13/02  J.ROTHOLZ - RECOMPILED DUE TO INCREASE IN #COMP-NAME
*            LENGTH IN TWRLCOMP & TWRACOMP
*  09/13/12  J.BREMER  - STATE CHANGES FOR 2011/2012    JB01
*  12/26/13  R.CARREON - FOR 1220 STATES ONLY
*                       1 REPLACE NUM CDE BY ALPHA STATE CODE - K RECORD
*                         EXCEPT PA, AL, KY, NY AND RI   RC01
*                       2 CHANGES IN B-RECORD
* 1/26/2016 PAULS - FOR STATES UT,NY,OK,OR THE LAYOUT HAS BEEN MODIFIED
*                    1. CHANGE IN K REC - OK,OR,UT
*                    2. CHANGE IN B REC - OR
*                    TAGG - PAULS
* 11/28/17 RUPOLEENA - TWRL3502 MODIFIED 2017 LAYOUT CHANGE FOR STATE
*                      NE,SC & WI  TAG - KS17
* 10/05/18 ARIVU     - EIN CHANGE - TAG: EINCHG
* 01/04/19 S VIKRAM  - STATES ORIGNIAL REPORTING CHANGES - TAG: VIKRAM
* 01/02/20 S VIKRAM  - STATES ORIGNIAL REPORTING CHANGES - TAG: VIKRAM
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3620 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl3502 ldaTwrl3502;
    private LdaTwrl3512 ldaTwrl3512;
    private LdaTwrl3513 ldaTwrl3513;
    private PdaTwracom2 pdaTwracom2;
    private LdaTwrl9415 ldaTwrl9415;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;
    private DbsField pnd_Out6;
    private DbsField pnd_S_Twrpymnt_Form_Sd;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_1;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year;

    private DbsGroup pnd_S_Twrpymnt_Form_Sd__R_Field_2;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year_A;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde;
    private DbsField pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Company_Cde;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Work_File_Pnd_Tirf_Sys_Err;
    private DbsField pnd_Work_File_Pnd_Tirf_Empty_Form;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Distr;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Tax_Wthld;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Code;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Distr;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_Work_File_Pnd_Tirf_Isn;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Pnd_Tirf_Isn_A;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Rpt_Ind;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Code;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year;

    private DbsGroup pnd_Work_File__R_Field_4;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year_Cc;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year_Yy;
    private DbsField pnd_Work_File_Pnd_Tirf_Name;
    private DbsField pnd_Work_File_Pnd_Tirf_Tin;
    private DbsField pnd_Work_File_Pnd_Tirf_Account;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln1;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln2;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln3;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Tot_Code3i;
    private DbsField pnd_Work_Area_Pnd_Tot_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1e;
    private DbsField pnd_Work_Area_Pnd_Tot_Brec;
    private DbsField pnd_Work_Area_Pnd_Tot_Crec;
    private DbsField pnd_Work_Area_Pnd_Tot_Krec;
    private DbsField pnd_Work_Area_Pnd_Grand_Code3i;
    private DbsField pnd_Work_Area_Pnd_Grand_Brec;
    private DbsField pnd_Work_Area_Pnd_Grand_Code2i;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt1;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt2;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt3;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt4;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt5;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt6;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt7;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt8;
    private DbsField pnd_Work_Area_Pnd_Tot_Amt9;
    private DbsField pnd_Work_Area_Pnd_Tot_Amta;
    private DbsField pnd_Work_Area_Pnd_Tot_Amtb;
    private DbsField pnd_Work_Area_Pnd_Tot_Amtc;
    private DbsField pnd_Work_Area_Pnd_Tot_Amtd;
    private DbsField pnd_Work_Area_Pnd_Tot_Amte;
    private DbsField pnd_Work_Area_Pnd_Tot_Amtf;
    private DbsField pnd_Work_Area_Pnd_Tot_Amtg;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Today;
    private DbsField pnd_Work_Area_Pnd_Trailer;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Old_Company_Cde;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Fil1;
    private DbsField pnd_Work_Area_Pnd_Fil2;
    private DbsField pnd_Work_Area_Pnd_Fil3;
    private DbsField pnd_Work_Area_Pnd_Fil4;
    private DbsField pnd_Work_Area_Pnd_Daten;

    private DbsGroup pnd_Work_Area__R_Field_5;
    private DbsField pnd_Work_Area_Pnd_Daten_Cc;
    private DbsField pnd_Work_Area_Pnd_Daten_Yy;
    private DbsField pnd_Work_Area_Pnd_Daten_Mm;
    private DbsField pnd_Work_Area_Pnd_Daten_Dd;
    private DbsField pnd_Rec_Seq_Number;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Temp_State_Id;

    private DbsGroup pnd_Temp_State_Id__R_Field_6;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_7;
    private DbsField pnd_Temp_State_Id_20;
    private DbsField pnd_K;
    private DbsField pnd_J;
    private DbsField pnd_9_K;
    private DbsField pnd_8_K;
    private DbsField pnd_Cntr_Ne_Stat_Withld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);
        ldaTwrl3512 = new LdaTwrl3512();
        registerRecord(ldaTwrl3512);
        ldaTwrl3513 = new LdaTwrl3513();
        registerRecord(ldaTwrl3513);
        pdaTwracom2 = new PdaTwracom2(localVariables);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Out6 = localVariables.newFieldInRecord("pnd_Out6", "#OUT6", FieldType.STRING, 30);
        pnd_S_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Form_Sd", "#S-TWRPYMNT-FORM-SD", FieldType.STRING, 25);

        pnd_S_Twrpymnt_Form_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Form_Sd__R_Field_1", "REDEFINE", pnd_S_Twrpymnt_Form_Sd);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_S_Twrpymnt_Form_Sd__R_Field_2 = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newGroupInGroup("pnd_S_Twrpymnt_Form_Sd__R_Field_2", "REDEFINE", pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year_A = pnd_S_Twrpymnt_Form_Sd__R_Field_2.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year_A", 
            "#S-TWRPYMNT-TAX-YEAR-A", FieldType.STRING, 4);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr", 
            "#S-TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Company_Cde = pnd_S_Twrpymnt_Form_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Company_Cde", 
            "#S-TWRPYMNT-COMPANY-CDE", FieldType.STRING, 1);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Tirf_Company_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Tirf_Sys_Err = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Sys_Err", "#TIRF-SYS-ERR", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Tirf_Empty_Form = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Empty_Form", "#TIRF-EMPTY-FORM", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Tirf_State_Distr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Distr", "#TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_File_Pnd_Tirf_State_Tax_Wthld = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Tax_Wthld", "#TIRF-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_Pnd_Tirf_Loc_Code = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Code", "#TIRF-LOC-CODE", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Tirf_Loc_Distr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Distr", "#TIRF-LOC-DISTR", FieldType.NUMERIC, 11, 
            2);
        pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld", "#TIRF-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_Pnd_Tirf_Isn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Isn", "#TIRF-ISN", FieldType.NUMERIC, 8);

        pnd_Work_File__R_Field_3 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Pnd_Tirf_Isn);
        pnd_Work_File_Pnd_Tirf_Isn_A = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Isn_A", "#TIRF-ISN-A", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Tirf_State_Rpt_Ind = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Rpt_Ind", "#TIRF-STATE-RPT-IND", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Tirf_State_Code = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Code", "#TIRF-STATE-CODE", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Tirf_Tax_Year = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);

        pnd_Work_File__R_Field_4 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_4", "REDEFINE", pnd_Work_File_Pnd_Tirf_Tax_Year);
        pnd_Work_File_Pnd_Tirf_Tax_Year_Cc = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year_Cc", "#TIRF-TAX-YEAR-CC", FieldType.STRING, 
            2);
        pnd_Work_File_Pnd_Tirf_Tax_Year_Yy = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year_Yy", "#TIRF-TAX-YEAR-YY", FieldType.STRING, 
            2);
        pnd_Work_File_Pnd_Tirf_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Name", "#TIRF-NAME", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Tin = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 9);
        pnd_Work_File_Pnd_Tirf_Account = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Account", "#TIRF-ACCOUNT", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Tirf_Addr_Ln1 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln1", "#TIRF-ADDR-LN1", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Addr_Ln2 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln2", "#TIRF-ADDR-LN2", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Addr_Ln3 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln3", "#TIRF-ADDR-LN3", FieldType.STRING, 40);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Tot_Code3i = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Code3i", "#TOT-CODE3I", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_1w", "#TOT-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1w", "#GRAND-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1e = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1e", "#GRAND-1E", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_Brec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Brec", "#TOT-BREC", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_Crec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Crec", "#TOT-CREC", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_Krec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Krec", "#TOT-KREC", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_Code3i = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_Code3i", "#GRAND-CODE3I", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_Brec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_Brec", "#GRAND-BREC", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_Code2i = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_Code2i", "#GRAND-CODE2I", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_Amt1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt1", "#TOT-AMT1", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt2 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt2", "#TOT-AMT2", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt3 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt3", "#TOT-AMT3", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt4 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt4", "#TOT-AMT4", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt5 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt5", "#TOT-AMT5", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt6 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt6", "#TOT-AMT6", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt7 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt7", "#TOT-AMT7", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt8 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt8", "#TOT-AMT8", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amt9 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amt9", "#TOT-AMT9", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amta = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amta", "#TOT-AMTA", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amtb = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amtb", "#TOT-AMTB", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amtc = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amtc", "#TOT-AMTC", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amtd = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amtd", "#TOT-AMTD", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amte = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amte", "#TOT-AMTE", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amtf = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amtf", "#TOT-AMTF", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Amtg = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Amtg", "#TOT-AMTG", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_State_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Distr", "#TOT-STATE-DISTR", FieldType.NUMERIC, 
            18, 2);
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Tax_Wthld", "#TOT-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Today = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Work_Area_Pnd_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trailer", "#TRAILER", FieldType.STRING, 20);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 5);
        pnd_Work_Area_Pnd_Old_Company_Cde = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Tot_Loc_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Distr", "#TOT-LOC-DISTR", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld", "#TOT-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Fil1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil1", "#FIL1", FieldType.STRING, 122);
        pnd_Work_Area_Pnd_Fil2 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil2", "#FIL2", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil3 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil3", "#FIL3", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil4 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil4", "#FIL4", FieldType.STRING, 225);
        pnd_Work_Area_Pnd_Daten = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Work_Area__R_Field_5 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_5", "REDEFINE", pnd_Work_Area_Pnd_Daten);
        pnd_Work_Area_Pnd_Daten_Cc = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Cc", "#DATEN-CC", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Yy = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Yy", "#DATEN-YY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Mm = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Mm", "#DATEN-MM", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Dd = pnd_Work_Area__R_Field_5.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Dd", "#DATEN-DD", FieldType.STRING, 2);
        pnd_Rec_Seq_Number = localVariables.newFieldInRecord("pnd_Rec_Seq_Number", "#REC-SEQ-NUMBER", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Temp_State_Id = localVariables.newFieldInRecord("pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 14);

        pnd_Temp_State_Id__R_Field_6 = localVariables.newGroupInRecord("pnd_Temp_State_Id__R_Field_6", "REDEFINE", pnd_Temp_State_Id);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill = pnd_Temp_State_Id__R_Field_6.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill", "#TEMP-STATE-ID-FILL", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_7 = pnd_Temp_State_Id__R_Field_6.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_7", "#TEMP-STATE-ID-7", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_20 = localVariables.newFieldInRecord("pnd_Temp_State_Id_20", "#TEMP-STATE-ID-20", FieldType.STRING, 20);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_9_K = localVariables.newFieldInRecord("pnd_9_K", "#9-K", FieldType.NUMERIC, 2);
        pnd_8_K = localVariables.newFieldInRecord("pnd_8_K", "#8-K", FieldType.NUMERIC, 2);
        pnd_Cntr_Ne_Stat_Withld = localVariables.newFieldInRecord("pnd_Cntr_Ne_Stat_Withld", "#CNTR-NE-STAT-WITHLD", FieldType.NUMERIC, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3502.initializeValues();
        ldaTwrl3512.initializeValues();
        ldaTwrl3513.initializeValues();
        ldaTwrl9415.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3620() throws Exception
    {
        super("Twrp3620");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Work_Area_Pnd_Today.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #TODAY := *DATX
        pnd_Work_Area_Pnd_Daten.setValue(Global.getDATN());                                                                                                               //Natural: ASSIGN #DATEN := *DATN
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.notEquals("NY")))                                                                                                 //Natural: IF #TIRF-STATE-CODE NE 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM COMPUTE-B-TOTAL-RECORDS
            sub_Compute_B_Total_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #WORK-FILE
        while (condition(getWorkFiles().read(1, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
            pnd_Work_File)))
        {
            CheckAtStartofData1448();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(pnd_Prev_C_Rec_Company.notEquals(pnd_Work_File_Pnd_Tirf_Company_Cde)))                                                                          //Natural: IF #PREV-C-REC-COMPANY NE #TIRF-COMPANY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
                sub_Create_C_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_C_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                      //Natural: ASSIGN #PREV-C-REC-COMPANY := #TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_A_Rec_Company.notEquals(pnd_Work_File_Pnd_Tirf_Company_Cde)))                                                                          //Natural: IF #PREV-A-REC-COMPANY NE #TIRF-COMPANY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
                sub_Create_A_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_A_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                      //Natural: ASSIGN #PREV-A-REC-COMPANY := #TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Seq_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-SEQ-NUMBER
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                            //Natural: ASSIGN #OUTB-SEQ-NUM := #REC-SEQ-NUMBER
            pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "3", pnd_Work_File_Pnd_Tirf_Isn_A));                                   //Natural: COMPRESS #OLD-COMPANY-CDE '3' #TIRF-ISN-A INTO #TRAILER
            //* ***************************************************
            //*  ADDED CODE FOR SC - 2017 - LAYOUT CHANGES
            //* ***************************************************
            //* *  IF #TIRF-STATE-CODE = 'SC'      /* KS17 ,  VIKRAM 1099-R COMMENTED
            //* * #OUTB-STATE-TAX            := #TIRF-STATE-TAX-WTHLD /* KS17
            //* *  #OUTB-CMB-STATE-CODE       := '45'  /* KS17
            //* *  #OUTB-LOCAL-TAX := #TIRF-LOC-TAX-WTHLD   /* KS17
            //* *  IF #OUTB-LOCAL-TAX  GE 0                            /* KS17
            //* *    MOVE ' '    TO #OUTB-LOCAL-TAX-A                 /* KS17
            //* *  END-IF                                             /* KS17
            //* *  IF VAL(#TIRF-TAX-YEAR) GE 2018                     /* EINCHG
            //* *    #OUTB-SC-STATE-IND:= '104314582'                 /* EINCHG
            //* *  ELSE
            //* *    #OUTB-SC-STATE-IND         := '253179064'           /* KS17
            //* *  END-IF                                                /* KS17
            //* *  END-IF                                                /* KS17
            //*  NEW NEBRASKA
            //*  REQUIREMENT AS OF
            //*  JAN. 2006
            //*  KS17
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NE")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'NE'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code().setValue("31");                                                                               //Natural: ASSIGN #OUTB-CMB-STATE-CODE := '31'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt().setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1());                                //Natural: ASSIGN #OUTB-PAY-AMT1-ALT := #OUTB-PAY-AMT1
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld().setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax());                         //Natural: ASSIGN #OUTB-STATE-TAX-WITHHELD := #OUTB-STATE-TAX
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2().setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2());                                    //Natural: ASSIGN #OUTB-NE-FILL2 := #OUTB-PAY-AMT2
                //*  KS17
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id().setValue(" ");                                                                               //Natural: MOVE ' ' TO #OUTB-NE-STATE-TAX-ID
                //*  KS17
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries().setValue(" ");                                                                          //Natural: MOVE ' ' TO #OUTB-SPECIAL-DATA-ENTRIES
                //*  KS17
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax_A().setValue(" ");                                                                                   //Natural: MOVE ' ' TO #OUTB-STATE-TAX-A
                //*  VIKRAM SPECIAL DATA ENTRIES STARTS
                pnd_Cntr_Ne_Stat_Withld.reset();                                                                                                                          //Natural: RESET #CNTR-NE-STAT-WITHLD
                pnd_S_Twrpymnt_Form_Sd.reset();                                                                                                                           //Natural: RESET #S-TWRPYMNT-FORM-SD
                pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year_A.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Year());                                         //Natural: ASSIGN #S-TWRPYMNT-TAX-YEAR-A := #OUTB-PAY-YEAR
                pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Tin());                                              //Natural: ASSIGN #S-TWRPYMNT-TAX-ID-NBR := #OUTB-TIN
                pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.setValue(pnd_Work_File_Pnd_Tirf_Account.getSubstring(1,8));                                            //Natural: ASSIGN #S-TWRPYMNT-CONTRACT-NBR := SUBSTR ( #TIRF-ACCOUNT,1,8 )
                pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.setValue(pnd_Work_File_Pnd_Tirf_Account.getSubstring(9,2));                                               //Natural: ASSIGN #S-TWRPYMNT-PAYEE-CDE := SUBSTR ( #TIRF-ACCOUNT,9,2 )
                ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                 //Natural: READ PAY WITH TWRPYMNT-FORM-SD = #S-TWRPYMNT-FORM-SD
                (
                "SV1",
                new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_S_Twrpymnt_Form_Sd, WcType.BY) },
                new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
                );
                SV1:
                while (condition(ldaTwrl9415.getVw_pay().readNextRow("SV1")))
                {
                    if (condition(pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Year.notEquals(ldaTwrl9415.getPay_Twrpymnt_Tax_Year()) || pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Tax_Id_Nbr.notEquals(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr())  //Natural: IF #S-TWRPYMNT-TAX-YEAR NE PAY.TWRPYMNT-TAX-YEAR OR #S-TWRPYMNT-TAX-ID-NBR NE PAY.TWRPYMNT-TAX-ID-NBR OR #S-TWRPYMNT-CONTRACT-NBR NE PAY.TWRPYMNT-CONTRACT-NBR OR #S-TWRPYMNT-PAYEE-CDE NE PAY.TWRPYMNT-PAYEE-CDE
                        || pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Contract_Nbr.notEquals(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr()) || pnd_S_Twrpymnt_Form_Sd_Pnd_S_Twrpymnt_Payee_Cde.notEquals(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde())))
                    {
                        if (true) break SV1;                                                                                                                              //Natural: ESCAPE BOTTOM ( SV1. )
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO C*TWRPYMNT-PAYMENTS
                    for (pnd_Work_Area_Pnd_I.setValue(1); condition(pnd_Work_Area_Pnd_I.lessOrEqual(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments())); 
                        pnd_Work_Area_Pnd_I.nadd(1))
                    {
                        if (condition(ldaTwrl9415.getPay_Twrpymnt_State_Wthld().getValue(pnd_Work_Area_Pnd_I).greater(getZero())))                                        //Natural: IF TWRPYMNT-STATE-WTHLD ( #I ) GT 0
                        {
                            pnd_Cntr_Ne_Stat_Withld.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTR-NE-STAT-WITHLD
                            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1().setValue(pnd_Cntr_Ne_Stat_Withld);                                                      //Natural: ASSIGN #OUTB-NE-FILL1 := #CNTR-NE-STAT-WITHLD
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("SV1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("SV1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  VIKRAM NEBRASKA STATE ENDS
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*    DECIDE ON FIRST VALUE #OLD-COMPANY-CDE        /* KS17
            //*      VALUE  'T'                                  /* KS17
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TIAA /* KS17
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE /* KS17
            //*        #OUTB-NE-STATE-TAX-ID  := #TEMP-STATE-ID-20     /* KS17
            //*      VALUE  'X'                                        /* KS17
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TRUST /* KS17
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE  /* KS17
            //*        #OUTB-NE-STATE-TAX-ID  := #TEMP-STATE-ID-20      /* KS17
            //*      NONE                                               /* KS17
            //*        IGNORE                                           /* KS17
            //*    END-DECIDE                                           /* KS17
            //*  END-IF                                          /* J.ROTHOLZ
            //*  CONNECTICUT JB01
            //*  1099-R VIKRAM
            //*  1099-R VIKRAM
            //*  1099- R VIKRAM
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'CT'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                  //Natural: ASSIGN #OUTB-STATE-TAX := #TIRF-STATE-TAX-WTHLD
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A().setValue(" ");                                                                                   //Natural: ASSIGN #OUTB-LOCAL-TAX-A := ' '
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Seq_Num().setValue(0);                                                                                         //Natural: ASSIGN #OUTB-SEQ-NUM := 0
                //*  EINCHG
                //* *    VALUE  'T'                                                                                                                                       //Natural: DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
                short decideConditionsMet1548 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1548++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                   //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-CT-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1548++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                  //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TRUST
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-CT-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Temp_State_Id_20.reset();                                                                                                                             //Natural: RESET #TEMP-STATE-ID-20
                //*  J.ROTHOLZ
            }                                                                                                                                                             //Natural: END-IF
            //*  KENTUCKY    JB01
            //*  VIKRAM 1099-R
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("KY")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'KY'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code().setValue("21");                                                                                //Natural: ASSIGN #OUTB-KY-STATE-CODE := '21'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                  //Natural: ASSIGN #OUTB-STATE-TAX := #TIRF-STATE-TAX-WTHLD
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id().setValue("973602");                                                                          //Natural: ASSIGN #OUTB-KY-STATE-TAX-ID := '973602'
            }                                                                                                                                                             //Natural: END-IF
            //*    IF #OUTB-STATE-TAX NE MASK('NNNNNNNNNNNN')
            //*      #OUTB-STATE-TAX := 0
            //*    END-IF
            //*    IF #OUTB-LOCAL-TAX NE MASK('NNNNNNNNNNNN')
            //*      #OUTB-LOCAL-TAX := 0
            //*    END-IF
            //*    DECIDE ON FIRST VALUE #OLD-COMPANY-CDE  /* VIKRAM 1099-R COMMENTED
            //*     VALUE  'T'                      /* EINCHG                    STARTS
            //*      VALUE  'T' , 'A'                /* EINCHG
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TIAA
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //*        #OUTB-KY-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //*      VALUE  'X'
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TRUST
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //*        #OUTB-KY-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //*      NONE
            //*        IGNORE
            //*    END-DECIDE
            //*    RESET #TEMP-STATE-ID-20 /* VIKRAM 1099-R COMMENTED ENDS
            //*  END-IF                                          /* J.ROTHOLZ
            //*  IF #TIRF-STATE-CODE = 'NC'                  /* NORTH CAROLINA  JB01
            //*    #OUTB-CMB-STATE-CODE := '37'
            //*    DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
            //*      VALUE  'T'                              /* EINCHG
            //*      VALUE  'T' , 'A'                        /* EINCHG
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TIAA
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //*        #OUTB-NC-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //*      VALUE  'X'
            //*        #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TRUST
            //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //*        #OUTB-NC-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //*      NONE
            //*        IGNORE
            //*    END-DECIDE
            //*    RESET #TEMP-STATE-ID-20
            //*  END-IF                                          /* J.ROTHOLZ
            //*  WISCONSIN       JB01
            //*  VIKRAM 1099-R
            //*  KS17
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("WI")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'WI'
            {
                //* *  #OUTB-CMB-STATE-CODE := '55'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde().setValue("55");                                                                                 //Natural: ASSIGN #OUTB-WI-STATE-CDE := '55'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                  //Natural: ASSIGN #OUTB-STATE-TAX := #TIRF-STATE-TAX-WTHLD
                //*  EINCHG
                //*      VALUE  'T'                                                                                                                                       //Natural: DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
                short decideConditionsMet1620 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1620++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                   //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-WI-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1620++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                  //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TRUST
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-WI-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Temp_State_Id_20.reset();                                                                                                                             //Natural: RESET #TEMP-STATE-ID-20
                //*  J.ROTHOLZ
            }                                                                                                                                                             //Natural: END-IF
            //* *IF #TIRF-STATE-CODE = 'MA' /* MASSACHUSETTS JB01, VIKRAM COMMENTED
            //* *  DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
            //* *    VALUE  'T'                                  /* EINCHG
            //* *    VALUE  'T', 'A'                             /* EINCHG
            //* *      #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TIAA
            //* *      EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //* *      #OUTB-MA-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //* *    VALUE  'X'
            //* *      #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TRUST
            //* *      EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
            //* *      #OUTB-MA-STATE-TAX-ID  := #TEMP-STATE-ID-20
            //* *    NONE
            //* *      IGNORE
            //* *  END-DECIDE
            //* *  RESET #TEMP-STATE-ID-20
            //* *END-IF                                          /* J.ROTHOLZ
            //*  ARKANSAS      JB01
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("AR")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'AR'
            {
                //* *  #OUTB-AR-STATE-IND   := 'AR'
                //* *  #OUTB-CMB-STATE-CODE := '05'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                  //Natural: ASSIGN #OUTB-STATE-TAX := #TIRF-STATE-TAX-WTHLD
                //*  EINCHG
                //*      VALUE  'T'                                                                                                                                       //Natural: DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
                short decideConditionsMet1660 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1660++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                   //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                    //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-AR-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1660++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                  //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TRUST
                    //*        EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-AR-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Temp_State_Id_20.reset();                                                                                                                             //Natural: RESET #TEMP-STATE-ID-20
                //*  J. BREMER
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #TIRF-STATE-CODE = 'IN'    /* INDIANA     JB01  VIKRAM COMMENTED
            //*   #OUTB-LOCAL-TAX  := 0
            //*  #OUTB-IN-COUNTY:= '00'
            //*  END-IF
            //*  IF #TIRF-STATE-CODE = 'VA'    /* VIRGINIA    JB01 VIKRAM COMMENTED
            //*   #OUTB-CMB-STATE-CODE := '51'
            //*  END-IF
            //*   IF #TIRF-STATE-CODE = 'CO'       /* COLORADO    JB01 VIKRAM COMMENTED
            //*     #OUTB-CMB-STATE-CODE := '07'
            //*   END-IF
            //*  VIKRAM 1099
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("UT")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'UT'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind().setValue("UT");                                                                                 //Natural: ASSIGN #OUTB-UT-STATE-IND := 'UT'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib().setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1());                            //Natural: ASSIGN #OUTB-UT-STATE-DISTRIB := #OUTB-PAY-AMT1
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                //Natural: ASSIGN #OUTB-UT-STATE-TAX-ID := TIRCNTL-STATE-ALPHA-CODE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A().setValue("000000000000");                                                                        //Natural: ASSIGN #OUTB-LOCAL-TAX-A := '000000000000'
                //*  EINCHG
                //* *    VALUE  'T'                                                                                                                                       //Natural: DECIDE ON FIRST VALUE #OLD-COMPANY-CDE
                short decideConditionsMet1698 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1698++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                   //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-UT-STATE-TAX-ID := #TEMP-STATE-ID-20
                    //*        #OUTB-UT-STATE-TAX-ID  := TIRCNTL-STATE-TAX-ID-TIAA
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1698++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                  //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TRUST
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-UT-STATE-TAX-ID := #TEMP-STATE-ID-20
                    //*        #OUTB-UT-STATE-TAX-ID  := TIRCNTL-STATE-TAX-ID-TRUST
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Temp_State_Id_20.reset();                                                                                                                             //Natural: RESET #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: END-IF
            //*  VERMONT          /* 03/03/11
            //*  VIKRAM 1099-R
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("VT")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'VT'
            {
                //*    DECIDE ON FIRST #OLD-COMPANY-CDE
                //*      VALUE  'T'
                //*      VALUE  'T' , 'A'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-VT-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
                //*      VALUE  'X'
                //*        #OUTB-VT-STATE-TAX-ID  := TIRCNTL-STATE-TAX-ID-TRUST
                //*      NONE
                //*        COMPRESS TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID'
                //*          INTO #OUTB-VT-STATE-TAX-ID
                //*    END-DECIDE
                //*  03/03/11
            }                                                                                                                                                             //Natural: END-IF
            //*  OREGON  JB01     /* 09/12/12
            //*  VIKRAM 1099-R
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("OR")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'OR'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id().setValue(17310704);                                                                          //Natural: ASSIGN #OUTB-OR-STATE-TAX-ID := 17310704
                //* *  #OUTB-OR-STATE-WTHHLD-SSN := '000000000'
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind().setValue(pnd_Work_File_Pnd_Tirf_State_Code);                                                    //Natural: ASSIGN #OUTB-OR-STATE-IND := #TIRF-STATE-CODE
                //* *  DECIDE ON FIRST #OLD-COMPANY-CDE          /* VIKRAM 1099-R COMMENTED
                //* *    VALUE  'T'                                           /* EINCHG
                //* *    VALUE  'T' , 'A'                                     /* EINCHG
                //* *      #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TIAA
                //* *      EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                //* *      EXAMINE #TEMP-STATE-ID-20 FOR ' ' GIVING LENGTH IN #K  /*<<PAULS
                //* *      #9-K := 9 - #K
                //* *      #8-K := 8 - #K
                //* *      MOVE SUBSTR(#TEMP-STATE-ID-20,1,#K) TO
                //* *        SUBSTR(#OUTB-OR-STATE-TAX-ID,#9-K,#K)
                //* *      FOR #J = 1 TO #8-K
                //* *        MOVE '0' TO SUBSTR(#OUTB-OR-STATE-TAX-ID,#J,1)
                //* *      END-FOR                                                /*>>PAULS
                //* *      WRITE #OUTB-OR-STATE-TAX-ID
                //* *    VALUE  'X'
                //* *      #TEMP-STATE-ID-20  := TIRCNTL-STATE-TAX-ID-TRUST
                //* *      EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                //* *      EXAMINE #TEMP-STATE-ID-20 FOR ' ' GIVING LENGTH IN #K /*<<PAULS
                //* *      #9-K := 9 - #K
                //* *      #8-K := 8 - #K
                //* *      MOVE SUBSTR(#TEMP-STATE-ID-20,1,#K) TO
                //* *        SUBSTR(#OUTB-OR-STATE-TAX-ID,#9-K,#K)
                //* *      FOR #J = 1 TO #8-K
                //* *        MOVE '0' TO SUBSTR(#OUTB-OR-STATE-TAX-ID,#J,1)
                //* *      END-FOR                                               /*>>PAULS
                //* *      WRITE #OUTB-OR-STATE-TAX-ID
                //* *    NONE
                //* *      COMPRESS TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID'
                //* *        INTO #OUTB-OR-STATE-TAX-ID
                //* *  END-DECIDE
                //* *
                //*  03/03/11
            }                                                                                                                                                             //Natural: END-IF
            //*  MARYLAND  JB01   /* 11/12/12
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("MD")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'MD'
            {
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup().setValue("000000000000");                                                                    //Natural: ASSIGN #OUTB-MD-STATE-PICKUP := '000000000000'
                //*  EINCHG
                //* *    VALUE  'T'                                                                                                                                       //Natural: DECIDE ON FIRST #OLD-COMPANY-CDE
                short decideConditionsMet1781 = 0;                                                                                                                        //Natural: VALUE 'T' , 'A'
                if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
                {
                    decideConditionsMet1781++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                   //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-MD-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
                {
                    decideConditionsMet1781++;
                    pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                  //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TRUST
                    DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                           //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                    ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                          //Natural: ASSIGN #OUTB-MD-STATE-TAX-ID := #TEMP-STATE-ID-20
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                    //*        COMPRESS TIRCNTL-STATE-ALPHA-CODE ' - NO TAX ID'
                    //*          INTO #OUTB-MB-STATE-TAX-ID
                }                                                                                                                                                         //Natural: END-DECIDE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax());                                      //Natural: ASSIGN #OUTB-STATE-TAX := #OUTB-STATE-TAX + #OUTB-LOCAL-TAX
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code().setValue("24");                                                                               //Natural: ASSIGN #OUTB-CMB-STATE-CODE := '24'
                //*  03/03/11
            }                                                                                                                                                             //Natural: END-IF
            //* * IF #TIRF-STATE-CODE = 'IN'           /* INDIANA           /* 05/06/11
            //* *   #OUTB-CMB-STATE-CODE := TWRATBL2.TIRCNTL-SEQ-NBR
            //* * END-IF                                                    /* 05/06/11
            //*                                                          /* RC01
            //*   VIKRAM 1099-R STARTS
            short decideConditionsMet1811 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #TIRF-STATE-CODE;//Natural: VALUE 'OK'
            if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("OK"))))
            {
                decideConditionsMet1811++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                   //Natural: ASSIGN #OUTB-OK-STATE-IND := TIRCNTL-STATE-ALPHA-CODE
                //*    VALUE 'MI'
                //*      #OUTB-MI-MN-STATE-IND := TIRCNTL-STATE-ALPHA-CODE
                //*      MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR (EM=99)  TO
                //*        #OUTB-CMB-STATE-CODE
                //*      IF #OLD-COMPANY-CDE  = 'T'
                //*        #OUTB-MI-MN-COMP-ACCT-NO := '131624203'
                //*      END-IF
                //*      IF #OLD-COMPANY-CDE  = 'X'
                //*        #OUTB-MI-MN-COMP-ACCT-NO := '516559589'
                //*      END-IF
                //*      IF #OLD-COMPANY-CDE  = 'A'
                //*        #OUTB-MI-MN-COMP-ACCT-NO := '822826183'
                //*      END-IF
                //*    VALUE 'MN'
                //*      #OUTB-MI-MN-STATE-IND := TIRCNTL-STATE-ALPHA-CODE
                //*      MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR (EM=99)  TO
                //*        #OUTB-CMB-STATE-CODE
                //*      IF #OLD-COMPANY-CDE  = 'T'
                //*        #OUTB-MI-MN-COMP-ACCT-NO := '3332566'
                //*      END-IF
                //*      IF #OLD-COMPANY-CDE  = 'X'
                //*        #OUTB-MI-MN-COMP-ACCT-NO := '7471462'
                //*      END-IF
                //*      IF #OLD-COMPANY-CDE  = 'A'
                //*      #OUTB-MI-MN-COMP-ACCT-NO := '5302471'
                //*      END-IF
                //* *  VALUE 'NC'
                //* *    IF #OLD-COMPANY-CDE  = 'T'
                //* *      #OUTB-NC-COMP-ACCT-NO := '101036544'
                //* *    END-IF
                //* *    IF #OLD-COMPANY-CDE  = 'X'
                //* *      #OUTB-NC-COMP-ACCT-NO := '200003369'
                //* *    END-IF
                //* *    IF #OLD-COMPANY-CDE  = 'A'
                //* *      #OUTB-NC-COMP-ACCT-NO := '200006307'
                //* *    END-IF
                //* *  VALUE 'OH' , 'ND' , 'MT'
                //* *   #OUTB-OH-ND-MT-STATE-IND := TIRCNTL-STATE-ALPHA-CODE
                //* *   MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR (EM=99)  TO
                //* *      #OUTB-CMB-STATE-CODE
                //* ** VALUE 'ME' , 'VT' ,  'CT'
                //* *    MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR (EM=99)  TO
                //* *      #OUTB-CMB-STATE-CODE
            }                                                                                                                                                             //Natural: VALUE 'NC'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("NC"))))
            {
                decideConditionsMet1811++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-NC-COMP-ACCT-NO := TIRCNTL-STATE-TAX-ID-TIAA
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind().setValue(pnd_Work_File_Pnd_Tirf_State_Code);                                                    //Natural: ASSIGN #OUTB-NC-STATE-IND := #TIRF-STATE-CODE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code().setValueEdited(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr(),new ReportEditMask("99"));           //Natural: MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR ( EM = 99 ) TO #OUTB-CMB-STATE-CODE
            }                                                                                                                                                             //Natural: VALUE 'AZ' , 'AR' , 'CO' , 'CT' , 'DE' , 'GA' , 'IN' , 'IA' , 'KS' ,'LA' , 'ME' , 'MI' , 'MS' , 'ND' , 'SC' , 'MT' , 'MN' ,'NE' , 'OH' , 'MA' , 'VA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("AZ") || pnd_Work_File_Pnd_Tirf_State_Code.equals("AR") || pnd_Work_File_Pnd_Tirf_State_Code.equals("CO") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("CT") || pnd_Work_File_Pnd_Tirf_State_Code.equals("DE") || pnd_Work_File_Pnd_Tirf_State_Code.equals("GA") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("IN") || pnd_Work_File_Pnd_Tirf_State_Code.equals("IA") || pnd_Work_File_Pnd_Tirf_State_Code.equals("KS") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("LA") || pnd_Work_File_Pnd_Tirf_State_Code.equals("ME") || pnd_Work_File_Pnd_Tirf_State_Code.equals("MI") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("MS") || pnd_Work_File_Pnd_Tirf_State_Code.equals("ND") || pnd_Work_File_Pnd_Tirf_State_Code.equals("SC") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("MT") || pnd_Work_File_Pnd_Tirf_State_Code.equals("MN") || pnd_Work_File_Pnd_Tirf_State_Code.equals("NE") 
                || pnd_Work_File_Pnd_Tirf_State_Code.equals("OH") || pnd_Work_File_Pnd_Tirf_State_Code.equals("MA") || pnd_Work_File_Pnd_Tirf_State_Code.equals("VA"))))
            {
                decideConditionsMet1811++;
                //*  VIKRAM 1099-R ENDS
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code().setValueEdited(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr(),new ReportEditMask("99"));           //Natural: MOVE EDITED TWRATBL2.TIRCNTL-SEQ-NBR ( EM = 99 ) TO #OUTB-CMB-STATE-CODE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  VIKRAM 1099-R STARTS
            //*  SPECIAL DATA
            //*  LOCAL AMT
            short decideConditionsMet1870 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #TIRF-STATE-CODE;//Natural: VALUE 'AZ'
            if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("AZ"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-AZ-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'IA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("IA"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                         //Natural: ASSIGN #OUTB-IA-WITHHELD-PERMIT-NO := TIRCNTL-STATE-TAX-ID-TIAA
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt().setValue(pnd_Work_File_Pnd_Tirf_State_Distr);                                              //Natural: ASSIGN #OUTB-IA-OTHER-IOWA-AMT := #TIRF-STATE-DISTR
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax().setValue(pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld);                                                    //Natural: ASSIGN #OUTB-LOCAL-TAX := #TIRF-LOC-TAX-WTHLD
            }                                                                                                                                                             //Natural: VALUE 'CO'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("CO"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-CO-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'MA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("MA"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-MA-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'NC'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("NC"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-NC-COMP-ACCT-NO := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'MS'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("MS"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-MS-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'OH'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("OH"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-OH-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'WV'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("WV"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-WV-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'MT'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("MT"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-MT-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'MN'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("MN"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                               //Natural: ASSIGN #OUTB-MN-STATE-TAX-ID := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'SC'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("SC"))))
            {
                decideConditionsMet1870++;
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                  //Natural: ASSIGN #OUTB-SC-STATE-IND := TIRCNTL-STATE-TAX-ID-TIAA
            }                                                                                                                                                             //Natural: VALUE 'VA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("VA"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                //*  STATE TAX AMT
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-VA-STATE-TAX-ID := #TEMP-STATE-ID-20
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_State_Tax().setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                  //Natural: ASSIGN #OUTB-STATE-TAX := #TIRF-STATE-TAX-WTHLD
            }                                                                                                                                                             //Natural: VALUE 'ND'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("ND"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch(" "), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 ' ' DELETE
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-ND-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'MI'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("MI"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-MI-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'ME'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("ME"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-ME-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'KS'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("KS"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-KS-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'LA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("LA"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-LA-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'GA'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("GA"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-GA-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'DE'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("DE"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-DE-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: VALUE 'DC'
            else if (condition((pnd_Work_File_Pnd_Tirf_State_Code.equals("DC"))))
            {
                decideConditionsMet1870++;
                pnd_Temp_State_Id_20.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: ASSIGN #TEMP-STATE-ID-20 := TIRCNTL-STATE-TAX-ID-TIAA
                DbsUtil.examine(new ExamineSource(pnd_Temp_State_Id_20,true), new ExamineSearch("-"), new ExamineDelete());                                               //Natural: EXAMINE FULL VALUE #TEMP-STATE-ID-20 '-' DELETE
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id().setValue(pnd_Temp_State_Id_20);                                                              //Natural: ASSIGN #OUTB-DC-STATE-TAX-ID := #TEMP-STATE-ID-20
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  VIKRAM 1099-R ENDS
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(2, false, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #TRAILER
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
                ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
                pnd_Work_Area_Pnd_Trailer);
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'NY'
            {
                pnd_Work_Area_Pnd_Grand_1w.nadd(1);                                                                                                                       //Natural: ADD 1 TO #GRAND-1W
                pnd_Work_Area_Pnd_Tot_1w.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-1W
                pnd_Work_Area_Pnd_Tot_State_Distr.nadd(pnd_Work_File_Pnd_Tirf_State_Distr);                                                                               //Natural: ADD #TIRF-STATE-DISTR TO #TOT-STATE-DISTR
                pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.nadd(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                                       //Natural: ADD #TIRF-STATE-TAX-WTHLD TO #TOT-STATE-TAX-WTHLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Area_Pnd_Tot_Brec.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOT-BREC
                pnd_Work_Area_Pnd_Tot_Amt1.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1());                                                                   //Natural: ADD #OUTB-PAY-AMT1 TO #TOT-AMT1
                pnd_Work_Area_Pnd_Tot_Amt2.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2());                                                                   //Natural: ADD #OUTB-PAY-AMT2 TO #TOT-AMT2
                pnd_Work_Area_Pnd_Tot_Amt3.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3());                                                                   //Natural: ADD #OUTB-PAY-AMT3 TO #TOT-AMT3
                pnd_Work_Area_Pnd_Tot_Amt4.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4());                                                                   //Natural: ADD #OUTB-PAY-AMT4 TO #TOT-AMT4
                pnd_Work_Area_Pnd_Tot_Amt5.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5());                                                                   //Natural: ADD #OUTB-PAY-AMT5 TO #TOT-AMT5
                pnd_Work_Area_Pnd_Tot_Amt6.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6());                                                                   //Natural: ADD #OUTB-PAY-AMT6 TO #TOT-AMT6
                pnd_Work_Area_Pnd_Tot_Amt7.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7());                                                                   //Natural: ADD #OUTB-PAY-AMT7 TO #TOT-AMT7
                pnd_Work_Area_Pnd_Tot_Amt8.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8());                                                                   //Natural: ADD #OUTB-PAY-AMT8 TO #TOT-AMT8
                pnd_Work_Area_Pnd_Tot_Amt9.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9());                                                                   //Natural: ADD #OUTB-PAY-AMT9 TO #TOT-AMT9
                pnd_Work_Area_Pnd_Tot_Amta.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amta());                                                                   //Natural: ADD #OUTB-PAY-AMTA TO #TOT-AMTA
                pnd_Work_Area_Pnd_Tot_Amtb.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb());                                                                   //Natural: ADD #OUTB-PAY-AMTB TO #TOT-AMTB
                pnd_Work_Area_Pnd_Tot_Amtc.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc());                                                                   //Natural: ADD #OUTB-PAY-AMTC TO #TOT-AMTC
                pnd_Work_Area_Pnd_Tot_Amtd.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd());                                                                   //Natural: ADD #OUTB-PAY-AMTD TO #TOT-AMTD
                pnd_Work_Area_Pnd_Tot_Amte.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amte());                                                                   //Natural: ADD #OUTB-PAY-AMTE TO #TOT-AMTE
                //*  12/18/09
                pnd_Work_Area_Pnd_Tot_Amtf.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf());                                                                   //Natural: ADD #OUTB-PAY-AMTF TO #TOT-AMTF
                //*  12/18/09
                pnd_Work_Area_Pnd_Tot_Amtg.nadd(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg());                                                                   //Natural: ADD #OUTB-PAY-AMTG TO #TOT-AMTG
                pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.nadd(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                                       //Natural: ADD #TIRF-STATE-TAX-WTHLD TO #TOT-STATE-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Seq_Number.notEquals(getZero())))                                                                                                           //Natural: IF #REC-SEQ-NUMBER NE 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
            sub_Create_C_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TRAILER-REC
            sub_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-A-RECORD
        //* ********************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OTHER-COMPANY-A-RECORD
        //*  #OUT1A-ORIG-FILE        :=  '1'
        //*  #OUT1A-REPLACE          :=  ' '
        //*  #OUT1A-CORRECT          :=  ' '
        //*    VALUE 'T'
        //*  #OUT1A-STATE-ID := 215594634
        //*  #OUT1A-STATE-ID := 2113008692
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-A-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-C-RECORD
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OTHER-COMPANY-C-RECORD
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-C-RECORD
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-REQUIRES-K-RECORD
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-RECORD
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OTHER-HEADER-REC
        //* *#OUT1T-MAG-FILE-IND      :=  '  '
        //*  #OUT1T-ELEC-FILE-NAME  :=  ' '
        //* *#OUT1T-TRANSMITTER-MEDIA :=  ' '
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-HEADER-REC
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRAILER-REC
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OTHER-TRAILER-REC
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-TRAILER-REC
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* ********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
        //* *********************************
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-B-TOTAL-RECORDS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Create_A_Record() throws Exception                                                                                                                   //Natural: CREATE-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Old_Company_Cde.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                   //Natural: ASSIGN #OLD-COMPANY-CDE := #TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-A-RECORD
            sub_Ny_A_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OTHER-COMPANY-A-RECORD
            sub_Other_Company_A_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Other_Company_A_Record() throws Exception                                                                                                            //Natural: OTHER-COMPANY-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record().reset();                                                                                                                     //Natural: RESET #OUT1-IRS-RECORD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id().setValue("A");                                                                                              //Natural: ASSIGN #OUT1A-REC-ID := 'A'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year().compute(new ComputeParameters(false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year()),            //Natural: ASSIGN #OUT1A-PAY-YEAR := VAL ( #TIRF-TAX-YEAR )
            pnd_Work_File_Pnd_Tirf_Tax_Year.val());
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                            //Natural: ASSIGN #OUT1A-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name());                                          //Natural: ASSIGN #OUT1A-CONTROL-CODE := #TWRACOM2.#COMP-SHORT-NAME
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind().setValue(" ");                                                                                          //Natural: ASSIGN #OUT1A-FILING-IND := ' '
        if (condition(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind().equals("C")))                                                                                        //Natural: IF TWRATBL2.TIRCNTL-COMB-FED-IND = 'C'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Combine().setValue("1");                                                                                         //Natural: ASSIGN #OUT1A-COMBINE := '1'
            //*  1099-R    10/01/10
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type().setValue("9 ");                                                                                           //Natural: ASSIGN #OUT1A-RET-TYPE := '9 '
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code().setValue("12459AB");                                                                                   //Natural: ASSIGN #OUT1A-AMOUNT-CODE := '12459AB'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind().setValue(" ");                                                                                         //Natural: ASSIGN #OUT1A-FOREIGN-IND := ' '
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                               //Natural: ASSIGN #OUT1A-TRANS-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_B());                                              //Natural: ASSIGN #OUT1A-TRANS-NAME1 := #TWRACOM2.#COMP-PAYER-B
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind().setValue("0");                                                                                           //Natural: ASSIGN #OUT1A-TRANS-IND := '0'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Company_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                   //Natural: ASSIGN #OUT1A-COMPANY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                     //Natural: ASSIGN #OUT1A-COMPANY-CITY := #CITY
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                   //Natural: ASSIGN #OUT1A-COMPANY-STATE := #STATE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                     //Natural: ASSIGN #OUT1A-COMPANY-ZIP := #ZIP-9
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Phone().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                           //Natural: ASSIGN #OUT1A-PHONE := #TWRACOM2.#PHONE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                              //Natural: ASSIGN #OUT1A-SEQ-NUM := #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Crlf().setValue(" ");                                                                                                //Natural: ASSIGN #OUT1A-CRLF := ' '
        //*  1099-R VIKRAM STARTS
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'CT'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Control_Code().setValue(" ");                                                                                    //Natural: ASSIGN #OUT1A-CONTROL-CODE := ' '
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num().setValue(0);                                                                                           //Natural: ASSIGN #OUT1A-SEQ-NUM := 0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("IA")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'IA'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_Control_Code().setValue(" ");                                                                                    //Natural: ASSIGN #OUT1A-CONTROL-CODE := ' '
            //*  1099-R VIKRAM ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------------------------------------------
        //*  NEBRASKA STATE ID LOGIC ADDED 01/31/06  J.ROTHOLZ
        //*  --------------------------------------------------
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NE")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NE'
        {
            //*  EINCHG
            short decideConditionsMet2111 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #OLD-COMPANY-CDE;//Natural: VALUE 'T' , 'A'
            if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("T") || pnd_Work_Area_Pnd_Old_Company_Cde.equals("A"))))
            {
                decideConditionsMet2111++;
                pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa(), MoveOption.RightJustified);                                               //Natural: MOVE RIGHT JUSTIFIED TWRATBL2.TIRCNTL-STATE-TAX-ID-TIAA TO #TEMP-STATE-ID
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("C"))))
            {
                decideConditionsMet2111++;
                pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Cref(), MoveOption.RightJustified);                                               //Natural: MOVE RIGHT JUSTIFIED TWRATBL2.TIRCNTL-STATE-TAX-ID-CREF TO #TEMP-STATE-ID
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("L"))))
            {
                decideConditionsMet2111++;
                pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Life(), MoveOption.RightJustified);                                               //Natural: MOVE RIGHT JUSTIFIED TWRATBL2.TIRCNTL-STATE-TAX-ID-LIFE TO #TEMP-STATE-ID
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("S"))))
            {
                decideConditionsMet2111++;
                pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tcii(), MoveOption.RightJustified);                                               //Natural: MOVE RIGHT JUSTIFIED TWRATBL2.TIRCNTL-STATE-TAX-ID-TCII TO #TEMP-STATE-ID
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((pnd_Work_Area_Pnd_Old_Company_Cde.equals("X"))))
            {
                decideConditionsMet2111++;
                pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust(), MoveOption.RightJustified);                                              //Natural: MOVE RIGHT JUSTIFIED TWRATBL2.TIRCNTL-STATE-TAX-ID-TRUST TO #TEMP-STATE-ID
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet2111 > 0))
            {
                ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_State_Id().setValue(pnd_Temp_State_Id_Pnd_Temp_State_Id_7, MoveOption.RightJustified);                       //Natural: MOVE RIGHT JUSTIFIED #TEMP-STATE-ID-7 TO #OUT1A-STATE-ID
                DbsUtil.examine(new ExamineSource(ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_State_Id()), new ExamineSearch(" "), new ExamineReplace("0"));             //Natural: EXAMINE #OUT1A-STATE-ID FOR ' ' REPLACE WITH '0'
                DbsUtil.examine(new ExamineSource(ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_State_Id()), new ExamineSearch(" "), new ExamineReplace("0"));             //Natural: EXAMINE #OUT1A-STATE-ID FOR ' ' REPLACE WITH '0'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  VIKRAM
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1a_State_Id().setValue("013008692");                                                                                //Natural: ASSIGN #OUT1A-STATE-ID := '013008692'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "2"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '2' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT1-IRS-REC1 #OUT1-IRS-REC2 #OUT1-IRS-REC3 #OUT1-IRS-REC4 #OUT1-IRS-REC5 #OUT1-IRS-REC6 #OUT1-IRS-REC7 #OUT1-IRS-REC8 #TRAILER
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5(), 
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8(), 
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Ny_A_Record() throws Exception                                                                                                                       //Natural: NY-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Work_File_Pnd_Tirf_Tax_Year_Yy)); //Natural: COMPRESS '12' #TIRF-TAX-YEAR-YY INTO #OUT1E-NY-REPORT-QUARTER LEAVING NO SPACE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id().setValue("1E");                                                                                               //Natural: ASSIGN #OUT1E-NY-ID := '1E'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                          //Natural: ASSIGN #OUT1E-NY-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                             //Natural: ASSIGN #OUT1E-NY-TRANS-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1E-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1E-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1E-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1E-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1E-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type().setValue("O");                                                                                          //Natural: ASSIGN #OUT1E-NY-RET-TYPE := 'O'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind().setValue(" ");                                                                                           //Natural: ASSIGN #OUT1E-NY-EMP-IND := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "2"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '2' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Create_C_Record() throws Exception                                                                                                                   //Natural: CREATE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-C-RECORD
            sub_Ny_C_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OTHER-COMPANY-C-RECORD
            sub_Other_Company_C_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Other_Company_C_Record() throws Exception                                                                                                            //Natural: OTHER-COMPANY-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        pnd_Work_Area_Pnd_Tot_Crec.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-CREC
        //*  12/18/09
        //*  12/18/09
        ldaTwrl3512.getPnd_Out1_Irs_Record().reset();                                                                                                                     //Natural: RESET #OUT1-IRS-RECORD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id().setValue("C");                                                                                              //Natural: ASSIGN #OUT1C-REC-ID := 'C'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer().setValue(pnd_Work_Area_Pnd_Tot_Brec);                                                                  //Natural: ASSIGN #OUT1C-NO-OF-PAYER := #TOT-BREC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1().setValue(pnd_Work_Area_Pnd_Tot_Amt1);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT1 := #TOT-AMT1
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2().setValue(pnd_Work_Area_Pnd_Tot_Amt2);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT2 := #TOT-AMT2
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3().setValue(pnd_Work_Area_Pnd_Tot_Amt3);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT3 := #TOT-AMT3
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4().setValue(pnd_Work_Area_Pnd_Tot_Amt4);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT4 := #TOT-AMT4
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5().setValue(pnd_Work_Area_Pnd_Tot_Amt5);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT5 := #TOT-AMT5
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6().setValue(pnd_Work_Area_Pnd_Tot_Amt6);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT6 := #TOT-AMT6
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7().setValue(pnd_Work_Area_Pnd_Tot_Amt7);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT7 := #TOT-AMT7
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8().setValue(pnd_Work_Area_Pnd_Tot_Amt8);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT8 := #TOT-AMT8
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9().setValue(pnd_Work_Area_Pnd_Tot_Amt9);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMT9 := #TOT-AMT9
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta().setValue(pnd_Work_Area_Pnd_Tot_Amta);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTA := #TOT-AMTA
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb().setValue(pnd_Work_Area_Pnd_Tot_Amtb);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTB := #TOT-AMTB
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc().setValue(pnd_Work_Area_Pnd_Tot_Amtc);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTC := #TOT-AMTC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd().setValue(pnd_Work_Area_Pnd_Tot_Amtd);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTD := #TOT-AMTD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte().setValue(pnd_Work_Area_Pnd_Tot_Amte);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTE := #TOT-AMTE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf().setValue(pnd_Work_Area_Pnd_Tot_Amtf);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTF := #TOT-AMTF
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg().setValue(pnd_Work_Area_Pnd_Tot_Amtg);                                                                     //Natural: ASSIGN #OUT1C-PAY-AMTG := #TOT-AMTG
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                              //Natural: ASSIGN #OUT1C-SEQ-NUM := #REC-SEQ-NUMBER
        //*  1099-R VIKRAM
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'CT'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num().setValue(0);                                                                                           //Natural: ASSIGN #OUT1C-SEQ-NUM := 0
            //*  1099-R VIKRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "4"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '4' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT1-IRS-REC1 #OUT1-IRS-REC2 #OUT1-IRS-REC3 #OUT1-IRS-REC4 #OUT1-IRS-REC5 #OUT1-IRS-REC6 #OUT1-IRS-REC7 #OUT1-IRS-REC8 #TRAILER
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5(), 
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8(), 
            pnd_Work_Area_Pnd_Trailer);
        //* *IF #TIRF-STATE-CODE  =   'SC'
                                                                                                                                                                          //Natural: PERFORM STATE-REQUIRES-K-RECORD
        sub_State_Requires_K_Record();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF
        pnd_Work_Area_Pnd_Tot_Brec.reset();                                                                                                                               //Natural: RESET #TOT-BREC #TOT-AMT1 #TOT-AMT2 #TOT-AMT3 #TOT-AMT4 #TOT-AMT5 #TOT-AMT6 #TOT-AMT7 #TOT-AMT8 #TOT-AMT9 #TOT-AMTA #TOT-AMTB #TOT-AMTC #TOT-STATE-TAX-WTHLD #TOT-KREC
        pnd_Work_Area_Pnd_Tot_Amt1.reset();
        pnd_Work_Area_Pnd_Tot_Amt2.reset();
        pnd_Work_Area_Pnd_Tot_Amt3.reset();
        pnd_Work_Area_Pnd_Tot_Amt4.reset();
        pnd_Work_Area_Pnd_Tot_Amt5.reset();
        pnd_Work_Area_Pnd_Tot_Amt6.reset();
        pnd_Work_Area_Pnd_Tot_Amt7.reset();
        pnd_Work_Area_Pnd_Tot_Amt8.reset();
        pnd_Work_Area_Pnd_Tot_Amt9.reset();
        pnd_Work_Area_Pnd_Tot_Amta.reset();
        pnd_Work_Area_Pnd_Tot_Amtb.reset();
        pnd_Work_Area_Pnd_Tot_Amtc.reset();
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.reset();
        pnd_Work_Area_Pnd_Tot_Krec.reset();
    }
    private void sub_Ny_C_Record() throws Exception                                                                                                                       //Natural: NY-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        pnd_Work_Area_Pnd_Grand_1e.nadd(1);                                                                                                                               //Natural: ADD 1 TO #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id().setValue("1T");                                                                                               //Natural: ASSIGN #OUT1T-NY-ID := '1T'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Tot_1w);                                                                     //Natural: ASSIGN #OUT1T-NY-TOTAL-1W := #TOT-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1T-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross().setValue(pnd_Work_Area_Pnd_Tot_State_Distr);                                                         //Natural: ASSIGN #OUT1T-NY-TOTAL-GROSS := #TOT-STATE-DISTR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross().setValue(0);                                                                                         //Natural: ASSIGN #OUT1T-NY-TOTAL-GROSS := 0
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt().setValue(pnd_Work_Area_Pnd_Tot_State_Distr);                                                   //Natural: ASSIGN #OUT1T-NY-TOTAL-TAXABLE-AMT := #TOT-STATE-DISTR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld().setValue(pnd_Work_Area_Pnd_Tot_State_Tax_Wthld);                                                       //Natural: ASSIGN #OUT1T-NY-TAX-WTHLD := #TOT-STATE-TAX-WTHLD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK3 := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "4"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '4' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
        pnd_Work_Area_Pnd_Tot_1w.reset();                                                                                                                                 //Natural: RESET #TOT-1W #TOT-STATE-DISTR #TOT-STATE-TAX-WTHLD
        pnd_Work_Area_Pnd_Tot_State_Distr.reset();
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.reset();
    }
    private void sub_State_Requires_K_Record() throws Exception                                                                                                           //Natural: STATE-REQUIRES-K-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        pnd_Work_Area_Pnd_Tot_Krec.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOT-KREC
        ldaTwrl3512.getPnd_Out1_Irs_Record().reset();                                                                                                                     //Natural: RESET #OUT1-IRS-RECORD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id().setValue("K");                                                                                              //Natural: ASSIGN #OUT1K-REC-ID := 'K'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees().setValue(pnd_Work_Area_Pnd_Tot_Brec);                                                             //Natural: ASSIGN #OUT1K-NUMBER-OF-PAYEES := #TOT-BREC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1().setValue(pnd_Work_Area_Pnd_Tot_Amt1);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-1 := #TOT-AMT1
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2().setValue(pnd_Work_Area_Pnd_Tot_Amt2);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-2 := #TOT-AMT2
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3().setValue(pnd_Work_Area_Pnd_Tot_Amt3);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-3 := #TOT-AMT3
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4().setValue(pnd_Work_Area_Pnd_Tot_Amt4);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-4 := #TOT-AMT4
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5().setValue(pnd_Work_Area_Pnd_Tot_Amt5);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-5 := #TOT-AMT5
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6().setValue(pnd_Work_Area_Pnd_Tot_Amt6);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-6 := #TOT-AMT6
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7().setValue(pnd_Work_Area_Pnd_Tot_Amt7);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-7 := #TOT-AMT7
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8().setValue(pnd_Work_Area_Pnd_Tot_Amt8);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-8 := #TOT-AMT8
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9().setValue(pnd_Work_Area_Pnd_Tot_Amt9);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-9 := #TOT-AMT9
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A().setValue(pnd_Work_Area_Pnd_Tot_Amta);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-A := #TOT-AMTA
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B().setValue(pnd_Work_Area_Pnd_Tot_Amtb);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-B := #TOT-AMTB
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C().setValue(pnd_Work_Area_Pnd_Tot_Amtc);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-C := #TOT-AMTC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D().setValue(pnd_Work_Area_Pnd_Tot_Amtd);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-D := #TOT-AMTD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E().setValue(pnd_Work_Area_Pnd_Tot_Amte);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-E := #TOT-AMTE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F().setValue(pnd_Work_Area_Pnd_Tot_Amtf);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-F := #TOT-AMTF
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G().setValue(pnd_Work_Area_Pnd_Tot_Amtg);                                                              //Natural: ASSIGN #OUT1K-CONTROL-TOTAL-G := #TOT-AMTG
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No().setValue(pnd_Rec_Seq_Number);                                                                   //Natural: ASSIGN #OUT1K-RECORD-SEQUENCE-NO := #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld().setValue(pnd_Work_Area_Pnd_Tot_State_Tax_Wthld);                                                   //Natural: ASSIGN #OUT1K-ST-TAX-WITHHELD := #TOT-STATE-TAX-WTHLD
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "5"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '5' INTO #TRAILER
        //*  IOWA VIKRAM 1099-R
        //*  1099-R VIKRAM
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("IA")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'IA'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr().setValue("0000000000");                                                                             //Natural: ASSIGN #OUT1K-IA-CNF-NBR := '0000000000'
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld().setValue(0);                                                                                   //Natural: ASSIGN #OUT1K-LC-TAX-WITHHELD := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  INDIANA           /* 05/06/11
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("IN")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'IN'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A().setValue("0", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT JUSTIFIED '0' TO #OUT1K-LC-TAX-WITHHELD-A
            DbsUtil.examine(new ExamineSource(ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A()), new ExamineSearch(" "), new ExamineReplace("0"));        //Natural: EXAMINE #OUT1K-LC-TAX-WITHHELD-A FOR ' ' REPLACE WITH '0'
            //*  05/06/11
        }                                                                                                                                                                 //Natural: END-IF
        //* * IF #TIRF-STATE-CODE = 'AL'  OR  = 'KY' OR = 'NY' OR     /* RC01
        //*  VIKRAM 1099-R
        //*  1099-R VIKRAM
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("AL") || pnd_Work_File_Pnd_Tirf_State_Code.equals("NY") || pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")  //Natural: IF #TIRF-STATE-CODE = 'AL' OR = 'NY' OR = 'CT' OR = 'IA' OR = 'PA' OR = 'RI'
            || pnd_Work_File_Pnd_Tirf_State_Code.equals("IA") || pnd_Work_File_Pnd_Tirf_State_Code.equals("PA") || pnd_Work_File_Pnd_Tirf_State_Code.equals("RI")))
        {
            //*  JB01
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_State_Code().setValue(pdaTwratbl2.getTwratbl2_Tircntl_Seq_Nbr());                                                //Natural: MOVE TWRATBL2.TIRCNTL-SEQ-NBR TO #OUT1K-STATE-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_State_Code().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                       //Natural: ASSIGN #OUT1K-STATE-CODE := TIRCNTL-STATE-ALPHA-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #TIRF-STATE-CODE = 'OK' OR = 'OR' OR = 'UT'          /*<<PAULS
        //*  VIKRAM 1099-R
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("OK") || pnd_Work_File_Pnd_Tirf_State_Code.equals("OR") || pnd_Work_File_Pnd_Tirf_State_Code.equals("UT")  //Natural: IF #TIRF-STATE-CODE = 'OK' OR = 'OR' OR = 'UT' OR = 'WV' OR = 'KY' OR = 'VT' OR = 'DC'
            || pnd_Work_File_Pnd_Tirf_State_Code.equals("WV") || pnd_Work_File_Pnd_Tirf_State_Code.equals("KY") || pnd_Work_File_Pnd_Tirf_State_Code.equals("VT") 
            || pnd_Work_File_Pnd_Tirf_State_Code.equals("DC")))
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1k_State_Code().reset();                                                                                            //Natural: RESET #OUT1K-STATE-CODE
            //* >>PAULS
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(2, false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT1-IRS-REC1 #OUT1-IRS-REC2 #OUT1-IRS-REC3 #OUT1-IRS-REC4 #OUT1-IRS-REC5 #OUT1-IRS-REC6 #OUT1-IRS-REC7 #OUT1-IRS-REC8 #TRAILER
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5(), 
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8(), 
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Header_Record() throws Exception                                                                                                                     //Natural: HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  EINCHG
        //*  EINCHG
        if (condition(pnd_Work_File_Pnd_Tirf_Tax_Year.val().greaterOrEqual(2018)))                                                                                        //Natural: IF VAL ( #TIRF-TAX-YEAR ) GE 2018
        {
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue("A");                                                                                                              //Natural: ASSIGN #OLD-COMPANY-CDE := 'A'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue("T");                                                                                                              //Natural: ASSIGN #OLD-COMPANY-CDE := 'T'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-HEADER-REC
            sub_Ny_Header_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OTHER-HEADER-REC
            sub_Other_Header_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Other_Header_Rec() throws Exception                                                                                                                  //Natural: OTHER-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record().reset();                                                                                                                     //Natural: RESET #OUT1-IRS-RECORD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id().setValue("T");                                                                                              //Natural: ASSIGN #OUT1T-REC-ID := 'T'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year().compute(new ComputeParameters(false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year()),            //Natural: ASSIGN #OUT1T-PAY-YEAR := VAL ( #TIRF-TAX-YEAR )
            pnd_Work_File_Pnd_Tirf_Tax_Year.val());
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind().setValue(" ");                                                                                      //Natural: ASSIGN #OUT1T-PRIOR-YEAR-IND := ' '
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                            //Natural: ASSIGN #OUT1T-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                                 //Natural: ASSIGN #OUT1T-CONTROL-CODE := #TWRACOM2.#COMP-TCC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                               //Natural: ASSIGN #OUT1T-TRANS-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                                              //Natural: ASSIGN #OUT1T-TRANS-NAME1 := #TWRACOM2.#COMP-TRANS-B
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                             //Natural: ASSIGN #OUT1T-COMPANY-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_B());                                            //Natural: ASSIGN #OUT1T-COMPANY-NAME1 := #TWRACOM2.#COMP-TRANS-B
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                   //Natural: ASSIGN #OUT1T-COMPANY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                     //Natural: ASSIGN #OUT1T-COMPANY-CITY := #CITY
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                   //Natural: ASSIGN #OUT1T-COMPANY-STATE := #STATE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                     //Natural: ASSIGN #OUT1T-COMPANY-ZIP := #ZIP-9
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer().setValue(pnd_Work_Area_Pnd_Grand_Brec);                                                                  //Natural: ASSIGN #OUT1T-TOT-PAYER := #GRAND-BREC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                             //Natural: ASSIGN #OUT1T-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                   //Natural: ASSIGN #OUT1T-CONTACT-PHONE := #TWRACOM2.#PHONE
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                                      //Natural: ASSIGN #OUT1T-CONTACT-EMAIL := #TWRACOM2.#CONTACT-EMAIL-ADDR
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                              //Natural: ASSIGN #OUT1T-SEQ-NUM := #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind().setValue("I");                                                                                          //Natural: ASSIGN #OUT1T-VENDOR-IND := 'I'
        //*  1099-R VIKRAM
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'CT'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num().setValue(0);                                                                                           //Natural: ASSIGN #OUT1T-SEQ-NUM := 0
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind().setValue(" ");                                                                                      //Natural: ASSIGN #OUT1T-VENDOR-IND := ' '
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1t_Control_Code().setValue(" ");                                                                                    //Natural: ASSIGN #OUT1T-CONTROL-CODE := ' '
            //*  1099-R VIKRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Trailer.setValue("A 1");                                                                                                                        //Natural: ASSIGN #TRAILER := 'A 1'
        getWorkFiles().write(2, false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT1-IRS-REC1 #OUT1-IRS-REC2 #OUT1-IRS-REC3 #OUT1-IRS-REC4 #OUT1-IRS-REC5 #OUT1-IRS-REC6 #OUT1-IRS-REC7 #OUT1-IRS-REC8 #TRAILER
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5(), 
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8(), 
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Ny_Header_Rec() throws Exception                                                                                                                     //Natural: NY-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        //*   #FED-ID
        //*   #COMP-NAME
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().setValue("1A");                                                                                               //Natural: ASSIGN #OUT1A-NY-ID := '1A'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm().setValue(pnd_Work_Area_Pnd_Daten_Mm);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-MM := #DATEN-MM
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd().setValue(pnd_Work_Area_Pnd_Daten_Dd);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-DD := #DATEN-DD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy().setValue(pnd_Work_Area_Pnd_Daten_Yy);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-YY := #DATEN-YY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                       //Natural: ASSIGN #OUT1A-NY-ID-NUM := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                             //Natural: ASSIGN #OUT1A-NY-TRANS-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1A-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1A-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1A-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1A-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1A-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("A", "1"));                                                                                                   //Natural: COMPRESS 'A' '1' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Trailer_Rec() throws Exception                                                                                                                       //Natural: TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-TRAILER-REC
            sub_Ny_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OTHER-TRAILER-REC
            sub_Other_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Other_Trailer_Rec() throws Exception                                                                                                                 //Natural: OTHER-TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3512.getPnd_Out1_Irs_Record().reset();                                                                                                                     //Natural: RESET #OUT1-IRS-RECORD
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id().setValue("F");                                                                                              //Natural: ASSIGN #OUT1F-REC-ID := 'F'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A().setValue(pnd_Work_Area_Pnd_Tot_Crec);                                                                     //Natural: ASSIGN #OUT1F-NUM-OF-A := #TOT-CREC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Zero().setValue("000000000000000000000");                                                                            //Natural: ASSIGN #OUT1F-ZERO := '000000000000000000000'
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees().setValue(pnd_Work_Area_Pnd_Grand_Brec);                                                                 //Natural: ASSIGN #OUT1F-TOT-PAYEES := #GRAND-BREC
        ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                              //Natural: ASSIGN #OUT1F-SEQ-NUM := #REC-SEQ-NUMBER
        //*  1099-R VIKRAM
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("CT")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'CT'
        {
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees().setValue(0);                                                                                        //Natural: ASSIGN #OUT1F-TOT-PAYEES := 0
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num().setValue(0);                                                                                           //Natural: ASSIGN #OUT1F-SEQ-NUM := 0
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1f_Zero().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1F-ZERO := ' '
            //*  1099-R VIKRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("Z", "9"));                                                                                                   //Natural: COMPRESS 'Z' '9' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2(),                    //Natural: WRITE WORK FILE 02 #OUT1-IRS-REC1 #OUT1-IRS-REC2 #OUT1-IRS-REC3 #OUT1-IRS-REC4 #OUT1-IRS-REC5 #OUT1-IRS-REC6 #OUT1-IRS-REC7 #OUT1-IRS-REC8 #TRAILER
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5(), 
            ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7(), ldaTwrl3512.getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8(), 
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Ny_Trailer_Rec() throws Exception                                                                                                                    //Natural: NY-TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id().setValue("1F");                                                                                               //Natural: ASSIGN #OUT1F-NY-ID := '1F'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e().setValue(pnd_Work_Area_Pnd_Grand_1e);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1E := #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Grand_1w);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1W := #GRAND-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1F-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("Z", "9"));                                                                                                   //Natural: COMPRESS 'Z' '9' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), pnd_Work_File_Pnd_Tirf_Tax_Year.val());      //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( #TIRF-TAX-YEAR )
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        if (condition(DbsUtil.maskMatches(pnd_Work_File_Pnd_Tirf_State_Code,"NN")))                                                                                       //Natural: IF #TIRF-STATE-CODE = MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_Work_File_Pnd_Tirf_State_Code));                    //Natural: COMPRESS '0' #TIRF-STATE-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_Work_File_Pnd_Tirf_State_Code);                                                                          //Natural: ASSIGN TWRATBL2.#STATE-CDE := #TIRF-STATE-CODE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Work_Area_Pnd_Old_Company_Cde);                                                                          //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #OLD-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Work_File_Pnd_Tirf_Tax_Year);                                            //Natural: MOVE EDITED #TIRF-TAX-YEAR TO #TWRACOM2.#TAX-YEAR ( EM = 9999 )
        pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOM2.#ABEND-IND := #TWRACOM2.#DISPLAY-IND := FALSE
        pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(false);
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"State Original Return Code Company Inf.'TWRNCOM2'",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'State Original Return Code Company Inf."TWRNCOM2"' 77T '***' / '***' 06T 'Return Message...:' #TWRACOM2.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(6),"Return Message...:",pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Compute_B_Total_Records() throws Exception                                                                                                           //Natural: COMPUTE-B-TOTAL-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 01 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #WORK-FILE
        while (condition(getWorkFiles().read(1, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
            pnd_Work_File)))
        {
            pnd_Work_Area_Pnd_Grand_Brec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #GRAND-BREC
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getWorkFiles().close(1);                                                                                                                                          //Natural: CLOSE WORK FILE 01
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
    private void CheckAtStartofData1448() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
            sub_Get_State_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM HEADER-RECORD
            sub_Header_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                               //Natural: ASSIGN #OLD-COMPANY-CDE := #PREV-A-REC-COMPANY := #PREV-C-REC-COMPANY := #TIRF-COMPANY-CDE
            pnd_Prev_A_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);
            pnd_Prev_C_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
            sub_Create_A_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
