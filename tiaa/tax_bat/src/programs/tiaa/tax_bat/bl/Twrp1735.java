/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:17 PM
**        * FROM NATURAL PROGRAM : Twrp1735
************************************************************
**        * FILE NAME            : Twrp1735.java
**        * CLASS NAME           : Twrp1735
**        * INSTANCE NAME        : Twrp1735
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP1735
** SYSTEM      :  TAX REPORTING SYSTEM - TAXWARS
** AUTHOR      :  M. BERLIN
** PURPOSE     :  SPLIT MCCAMISH TRANSACTION FILE INTO CURRENT TAX YEAR,
**             :  PREVIOUS TAX YEAR, AND BEFORE PREVIOUS TAX YEAR.
** INPUT       :  1. CONVERTED EXTRACT FROM MCCAMISH FROM JOB PTW1730D
**             :  AND JOB PTW1732D
**----------------------------------------------------------------------
** OUTPUT      :  2. THREE FILES BASED ON THE TAX YEAR
**----------------------------------------------------------------------
**             :     DETAIL RECORD LAYOUT IS LOCAL TWRL0700
**             :     CONTROL RECORD LAYOUT IS LOCAL TWRL0710
**             :     CONTROL YEAR CARD RECORD LAYOUT IS LOCAL TWRL0600
**----------------------------------------------------------------------
** DATE        :  09/13/2011
************************************************************************
** MODIFICATION LOG
** 03/13/2012  :  M BERLIN  - PROD FIX FOR INVALID OR MISSING TAX YEAR
**                            FIELD.  SEE COMMENTS /* MB 03/13/2012
** 10/18/2011  :  R CARREON - STOW FOR NON-ERISA TDA
** 01/18/2012  :  R CARREON - STOW FOR SUNY/CUNY
** 04/21/2017  :  J BREMER  - PIN EXPANSION STOW FOR TWRL0700
** 06/07/2018  :  S VIKRAM  - T COMPANY CODE LOGIC ADDED IN PROGRAM
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1735 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl0710 ldaTwrl0710;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Tax_Year_Previous;
    private DbsField pnd_L_Gross_A_Current;
    private DbsField pnd_L_Ivc_A_Current;
    private DbsField pnd_L_Int_A_Current;
    private DbsField pnd_L_Fed_A_Current;
    private DbsField pnd_L_Nra_A_Current;
    private DbsField pnd_L_State_A_Current;
    private DbsField pnd_L_Local_A_Current;
    private DbsField pnd_L_Canadian_A_Current;
    private DbsField pnd_L_Gross_A_Previous;
    private DbsField pnd_L_Ivc_A_Previous;
    private DbsField pnd_L_Int_A_Previous;
    private DbsField pnd_L_Fed_A_Previous;
    private DbsField pnd_L_Nra_A_Previous;
    private DbsField pnd_L_State_A_Previous;
    private DbsField pnd_L_Local_A_Previous;
    private DbsField pnd_L_Canadian_A_Previous;
    private DbsField pnd_L_Gross_A_Rest;
    private DbsField pnd_L_Ivc_A_Rest;
    private DbsField pnd_L_Int_A_Rest;
    private DbsField pnd_L_Fed_A_Rest;
    private DbsField pnd_L_Nra_A_Rest;
    private DbsField pnd_L_State_A_Rest;
    private DbsField pnd_L_Local_A_Rest;
    private DbsField pnd_L_Canadian_A_Rest;
    private DbsField pnd_T_Gross_A_Current;
    private DbsField pnd_T_Ivc_A_Current;
    private DbsField pnd_T_Int_A_Current;
    private DbsField pnd_T_Fed_A_Current;
    private DbsField pnd_T_Nra_A_Current;
    private DbsField pnd_T_State_A_Current;
    private DbsField pnd_T_Local_A_Current;
    private DbsField pnd_T_Canadian_A_Current;
    private DbsField pnd_T_Gross_A_Previous;
    private DbsField pnd_T_Ivc_A_Previous;
    private DbsField pnd_T_Int_A_Previous;
    private DbsField pnd_T_Fed_A_Previous;
    private DbsField pnd_T_Nra_A_Previous;
    private DbsField pnd_T_State_A_Previous;
    private DbsField pnd_T_Local_A_Previous;
    private DbsField pnd_T_Canadian_A_Previous;
    private DbsField pnd_T_Gross_A_Rest;
    private DbsField pnd_T_Ivc_A_Rest;
    private DbsField pnd_T_Int_A_Rest;
    private DbsField pnd_T_Fed_A_Rest;
    private DbsField pnd_T_Nra_A_Rest;
    private DbsField pnd_T_State_A_Rest;
    private DbsField pnd_T_Local_A_Rest;
    private DbsField pnd_T_Canadian_A_Rest;
    private DbsField pnd_Counter_Current;
    private DbsField pnd_Counter_Current_L;
    private DbsField pnd_Counter_Current_T;
    private DbsField pnd_Counter_Previous;
    private DbsField pnd_Counter_Previous_L;
    private DbsField pnd_Counter_Previous_T;
    private DbsField pnd_Counter_Rest;
    private DbsField pnd_Counter_Rest_L;
    private DbsField pnd_Counter_Rest_T;
    private DbsField pnd_Last_Bus_Date;
    private DbsField pnd_Last_Bus_Day;
    private DbsField pnd_Last_Bus_Compare;
    private DbsField pnd_P_Year;

    private DbsGroup pnd_P_Year__R_Field_1;
    private DbsField pnd_P_Year_Pnd_P_Year_N;
    private DbsField pnd_Curr_Year;

    private DbsGroup pnd_Curr_Year__R_Field_2;
    private DbsField pnd_Curr_Year_Pnd_Curr_Year_A;
    private DbsField pnd_Prev_Year;

    private DbsGroup pnd_Prev_Year__R_Field_3;
    private DbsField pnd_Prev_Year_Pnd_Prev_Year_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl0710 = new LdaTwrl0710();
        registerRecord(ldaTwrl0710);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Tax_Year_Previous = localVariables.newFieldInRecord("pnd_Tax_Year_Previous", "#TAX-YEAR-PREVIOUS", FieldType.NUMERIC, 4);
        pnd_L_Gross_A_Current = localVariables.newFieldInRecord("pnd_L_Gross_A_Current", "#L-GROSS-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Ivc_A_Current = localVariables.newFieldInRecord("pnd_L_Ivc_A_Current", "#L-IVC-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Int_A_Current = localVariables.newFieldInRecord("pnd_L_Int_A_Current", "#L-INT-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Fed_A_Current = localVariables.newFieldInRecord("pnd_L_Fed_A_Current", "#L-FED-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Nra_A_Current = localVariables.newFieldInRecord("pnd_L_Nra_A_Current", "#L-NRA-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_State_A_Current = localVariables.newFieldInRecord("pnd_L_State_A_Current", "#L-STATE-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Local_A_Current = localVariables.newFieldInRecord("pnd_L_Local_A_Current", "#L-LOCAL-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Canadian_A_Current = localVariables.newFieldInRecord("pnd_L_Canadian_A_Current", "#L-CANADIAN-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Gross_A_Previous = localVariables.newFieldInRecord("pnd_L_Gross_A_Previous", "#L-GROSS-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Ivc_A_Previous = localVariables.newFieldInRecord("pnd_L_Ivc_A_Previous", "#L-IVC-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Int_A_Previous = localVariables.newFieldInRecord("pnd_L_Int_A_Previous", "#L-INT-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Fed_A_Previous = localVariables.newFieldInRecord("pnd_L_Fed_A_Previous", "#L-FED-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Nra_A_Previous = localVariables.newFieldInRecord("pnd_L_Nra_A_Previous", "#L-NRA-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_State_A_Previous = localVariables.newFieldInRecord("pnd_L_State_A_Previous", "#L-STATE-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Local_A_Previous = localVariables.newFieldInRecord("pnd_L_Local_A_Previous", "#L-LOCAL-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Canadian_A_Previous = localVariables.newFieldInRecord("pnd_L_Canadian_A_Previous", "#L-CANADIAN-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_L_Gross_A_Rest = localVariables.newFieldInRecord("pnd_L_Gross_A_Rest", "#L-GROSS-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Ivc_A_Rest = localVariables.newFieldInRecord("pnd_L_Ivc_A_Rest", "#L-IVC-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Int_A_Rest = localVariables.newFieldInRecord("pnd_L_Int_A_Rest", "#L-INT-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Fed_A_Rest = localVariables.newFieldInRecord("pnd_L_Fed_A_Rest", "#L-FED-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Nra_A_Rest = localVariables.newFieldInRecord("pnd_L_Nra_A_Rest", "#L-NRA-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_State_A_Rest = localVariables.newFieldInRecord("pnd_L_State_A_Rest", "#L-STATE-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Local_A_Rest = localVariables.newFieldInRecord("pnd_L_Local_A_Rest", "#L-LOCAL-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_L_Canadian_A_Rest = localVariables.newFieldInRecord("pnd_L_Canadian_A_Rest", "#L-CANADIAN-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Gross_A_Current = localVariables.newFieldInRecord("pnd_T_Gross_A_Current", "#T-GROSS-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Ivc_A_Current = localVariables.newFieldInRecord("pnd_T_Ivc_A_Current", "#T-IVC-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Int_A_Current = localVariables.newFieldInRecord("pnd_T_Int_A_Current", "#T-INT-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Fed_A_Current = localVariables.newFieldInRecord("pnd_T_Fed_A_Current", "#T-FED-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Nra_A_Current = localVariables.newFieldInRecord("pnd_T_Nra_A_Current", "#T-NRA-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_State_A_Current = localVariables.newFieldInRecord("pnd_T_State_A_Current", "#T-STATE-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Local_A_Current = localVariables.newFieldInRecord("pnd_T_Local_A_Current", "#T-LOCAL-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Canadian_A_Current = localVariables.newFieldInRecord("pnd_T_Canadian_A_Current", "#T-CANADIAN-A-CURRENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Gross_A_Previous = localVariables.newFieldInRecord("pnd_T_Gross_A_Previous", "#T-GROSS-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Ivc_A_Previous = localVariables.newFieldInRecord("pnd_T_Ivc_A_Previous", "#T-IVC-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Int_A_Previous = localVariables.newFieldInRecord("pnd_T_Int_A_Previous", "#T-INT-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Fed_A_Previous = localVariables.newFieldInRecord("pnd_T_Fed_A_Previous", "#T-FED-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Nra_A_Previous = localVariables.newFieldInRecord("pnd_T_Nra_A_Previous", "#T-NRA-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_State_A_Previous = localVariables.newFieldInRecord("pnd_T_State_A_Previous", "#T-STATE-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Local_A_Previous = localVariables.newFieldInRecord("pnd_T_Local_A_Previous", "#T-LOCAL-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Canadian_A_Previous = localVariables.newFieldInRecord("pnd_T_Canadian_A_Previous", "#T-CANADIAN-A-PREVIOUS", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_T_Gross_A_Rest = localVariables.newFieldInRecord("pnd_T_Gross_A_Rest", "#T-GROSS-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Ivc_A_Rest = localVariables.newFieldInRecord("pnd_T_Ivc_A_Rest", "#T-IVC-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Int_A_Rest = localVariables.newFieldInRecord("pnd_T_Int_A_Rest", "#T-INT-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Fed_A_Rest = localVariables.newFieldInRecord("pnd_T_Fed_A_Rest", "#T-FED-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Nra_A_Rest = localVariables.newFieldInRecord("pnd_T_Nra_A_Rest", "#T-NRA-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_State_A_Rest = localVariables.newFieldInRecord("pnd_T_State_A_Rest", "#T-STATE-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Local_A_Rest = localVariables.newFieldInRecord("pnd_T_Local_A_Rest", "#T-LOCAL-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_T_Canadian_A_Rest = localVariables.newFieldInRecord("pnd_T_Canadian_A_Rest", "#T-CANADIAN-A-REST", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counter_Current = localVariables.newFieldInRecord("pnd_Counter_Current", "#COUNTER-CURRENT", FieldType.NUMERIC, 7);
        pnd_Counter_Current_L = localVariables.newFieldInRecord("pnd_Counter_Current_L", "#COUNTER-CURRENT-L", FieldType.NUMERIC, 7);
        pnd_Counter_Current_T = localVariables.newFieldInRecord("pnd_Counter_Current_T", "#COUNTER-CURRENT-T", FieldType.NUMERIC, 7);
        pnd_Counter_Previous = localVariables.newFieldInRecord("pnd_Counter_Previous", "#COUNTER-PREVIOUS", FieldType.NUMERIC, 7);
        pnd_Counter_Previous_L = localVariables.newFieldInRecord("pnd_Counter_Previous_L", "#COUNTER-PREVIOUS-L", FieldType.NUMERIC, 7);
        pnd_Counter_Previous_T = localVariables.newFieldInRecord("pnd_Counter_Previous_T", "#COUNTER-PREVIOUS-T", FieldType.NUMERIC, 7);
        pnd_Counter_Rest = localVariables.newFieldInRecord("pnd_Counter_Rest", "#COUNTER-REST", FieldType.NUMERIC, 7);
        pnd_Counter_Rest_L = localVariables.newFieldInRecord("pnd_Counter_Rest_L", "#COUNTER-REST-L", FieldType.NUMERIC, 7);
        pnd_Counter_Rest_T = localVariables.newFieldInRecord("pnd_Counter_Rest_T", "#COUNTER-REST-T", FieldType.NUMERIC, 7);
        pnd_Last_Bus_Date = localVariables.newFieldInRecord("pnd_Last_Bus_Date", "#LAST-BUS-DATE", FieldType.DATE);
        pnd_Last_Bus_Day = localVariables.newFieldInRecord("pnd_Last_Bus_Day", "#LAST-BUS-DAY", FieldType.STRING, 1);
        pnd_Last_Bus_Compare = localVariables.newFieldInRecord("pnd_Last_Bus_Compare", "#LAST-BUS-COMPARE", FieldType.STRING, 4);
        pnd_P_Year = localVariables.newFieldInRecord("pnd_P_Year", "#P-YEAR", FieldType.STRING, 4);

        pnd_P_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_P_Year__R_Field_1", "REDEFINE", pnd_P_Year);
        pnd_P_Year_Pnd_P_Year_N = pnd_P_Year__R_Field_1.newFieldInGroup("pnd_P_Year_Pnd_P_Year_N", "#P-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Curr_Year = localVariables.newFieldInRecord("pnd_Curr_Year", "#CURR-YEAR", FieldType.NUMERIC, 4);

        pnd_Curr_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_Curr_Year__R_Field_2", "REDEFINE", pnd_Curr_Year);
        pnd_Curr_Year_Pnd_Curr_Year_A = pnd_Curr_Year__R_Field_2.newFieldInGroup("pnd_Curr_Year_Pnd_Curr_Year_A", "#CURR-YEAR-A", FieldType.STRING, 4);
        pnd_Prev_Year = localVariables.newFieldInRecord("pnd_Prev_Year", "#PREV-YEAR", FieldType.NUMERIC, 4);

        pnd_Prev_Year__R_Field_3 = localVariables.newGroupInRecord("pnd_Prev_Year__R_Field_3", "REDEFINE", pnd_Prev_Year);
        pnd_Prev_Year_Pnd_Prev_Year_A = pnd_Prev_Year__R_Field_3.newFieldInGroup("pnd_Prev_Year_Pnd_Prev_Year_A", "#PREV-YEAR-A", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0700.initializeValues();
        ldaTwrl0710.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1735() throws Exception
    {
        super("Twrp1735");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ******************************************************
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 1 ONCE RECORD #TWRP0600-CONTROL-RECORD
        //* ******************************************************
        pnd_P_Year.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                                     //Natural: ASSIGN #P-YEAR := #TWRP0600-TAX-YEAR-CCYY
        pnd_Curr_Year.setValue(pnd_P_Year_Pnd_P_Year_N);                                                                                                                  //Natural: ASSIGN #CURR-YEAR := #P-YEAR-N
        pnd_P_Year_Pnd_P_Year_N.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #P-YEAR-N
        pnd_Last_Bus_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                    //Natural: MOVE EDITED #TWRP0600-INTERFACE-CCYYMMDD TO #LAST-BUS-DATE ( EM = YYYYMMDD )
        pnd_Last_Bus_Day.setValueEdited(pnd_Last_Bus_Date,new ReportEditMask("O"));                                                                                       //Natural: MOVE EDITED #LAST-BUS-DATE ( EM = O ) TO #LAST-BUS-DAY
        //*  FRI
        if (condition((((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals("1229") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals("1230"))  //Natural: IF SUBSTR ( #TWRP0600-INTERFACE-CCYYMMDD,5,4 ) = '1229' OR = '1230' AND #LAST-BUS-DAY = '6' OR SUBSTR ( #TWRP0600-INTERFACE-CCYYMMDD,5,4 ) = '1231'
            && pnd_Last_Bus_Day.equals("6")) || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals("1231"))))
        {
            pnd_Curr_Year.setValue(pnd_P_Year_Pnd_P_Year_N);                                                                                                              //Natural: ASSIGN #CURR-YEAR := #P-YEAR-N
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Year.compute(new ComputeParameters(false, pnd_Prev_Year), pnd_Curr_Year.subtract(1));                                                                    //Natural: ASSIGN #PREV-YEAR := #CURR-YEAR - 1
        //* ******************************************************
        WK01:                                                                                                                                                             //Natural: READ WORK FILE 2 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(2, ldaTwrl0700.getTwrt_Record())))
        {
            short decideConditionsMet410 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRT-COMPANY-CDE EQ 'Z'
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("Z")))
            {
                decideConditionsMet410++;
                //*  MB 03/13/2012
                                                                                                                                                                          //Natural: PERFORM S040-PROCESS-CONTROL
                sub_S040_Process_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN TWRT-TAX-YEAR-A = #CURR-YEAR-A
            else if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A().equals(pnd_Curr_Year_Pnd_Curr_Year_A)))
            {
                decideConditionsMet410++;
                pnd_Counter_Current.nadd(1);                                                                                                                              //Natural: ADD 1 TO #COUNTER-CURRENT
                //*  MB 03/13/2012
                                                                                                                                                                          //Natural: PERFORM S010-PROCESS-CURRENT
                sub_S010_Process_Current();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN TWRT-TAX-YEAR-A = #PREV-YEAR-A AND #PREV-YEAR GT 2010
            else if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A().equals(pnd_Prev_Year_Pnd_Prev_Year_A) && pnd_Prev_Year.greater(2010)))
            {
                decideConditionsMet410++;
                //* **  BECAUSE 2010 RECORDS SHOULD BE PROCESSED MANUALLY IN 2011, AND
                //* **  THE START OF THE PROJECT IS EXPECTED ON NOVEMBER 18TH 2011,
                //* **  ONLY IN THE YEAR 2011 THE PREVIOUS YEAR IS 2010 AND IT SHOULD BE IN
                //* **  THE MANUAL RECORDS FILE 5 (REST).  "AND #TAX-YEAR-PREVIOUS GT 2010"
                //* **  CODE CAN BE REMOVED ANY TIME AFTER YEAR 2012 STARTS.
                pnd_Counter_Previous.nadd(1);                                                                                                                             //Natural: ADD 1 TO #COUNTER-PREVIOUS
                                                                                                                                                                          //Natural: PERFORM S020-PROCESS-PREVIOUS
                sub_S020_Process_Previous();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Counter_Rest.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #COUNTER-REST
                                                                                                                                                                          //Natural: PERFORM S030-PROCESS-REST
                sub_S030_Process_Rest();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("WK01"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("WK01"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  (WK01.)
        }                                                                                                                                                                 //Natural: END-WORK
        WK01_Exit:
        if (Global.isEscape()) return;
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S010-PROCESS-CURRENT
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S010-PROCESS-CURRENT-L
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S010-PROCESS-CURRENT-T
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S020-PROCESS-PREVIOUS
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S020-PROCESS-PREVIOUS-L
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S020-PROCESS-PREVIOUS-T
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S030-PROCESS-REST
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S030-PROCESS-REST-L
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S030-PROCESS-REST-T
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S040-PROCESS-CONTROL
        //* **********************************************************
    }
    private void sub_S010_Process_Current() throws Exception                                                                                                              //Natural: S010-PROCESS-CURRENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        short decideConditionsMet457 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'L'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet457++;
                                                                                                                                                                          //Natural: PERFORM S010-PROCESS-CURRENT-L
            sub_S010_Process_Current_L();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet457++;
                                                                                                                                                                          //Natural: PERFORM S010-PROCESS-CURRENT-T
            sub_S010_Process_Current_T();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, "Non Acceptable Company code in Input file");                                                                                           //Natural: WRITE 'Non Acceptable Company code in Input file'
            if (Global.isEscape()) return;
            DbsUtil.terminate(69);  if (true) return;                                                                                                                     //Natural: TERMINATE 69
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  S010-PROCESS-CURRENT
    }
    private void sub_S010_Process_Current_L() throws Exception                                                                                                            //Natural: S010-PROCESS-CURRENT-L
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Current_L.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-CURRENT-L
        pnd_L_Gross_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                          //Natural: ADD TWRT-GROSS-AMT TO #L-GROSS-A-CURRENT
        pnd_L_Ivc_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                              //Natural: ADD TWRT-IVC-AMT TO #L-IVC-A-CURRENT
        pnd_L_Int_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                         //Natural: ADD TWRT-INTEREST-AMT TO #L-INT-A-CURRENT
        pnd_L_Fed_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                        //Natural: ADD TWRT-FED-WHHLD-AMT TO #L-FED-A-CURRENT
        pnd_L_Nra_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                        //Natural: ADD TWRT-NRA-WHHLD-AMT TO #L-NRA-A-CURRENT
        pnd_L_State_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                    //Natural: ADD TWRT-STATE-WHHLD-AMT TO #L-STATE-A-CURRENT
        pnd_L_Local_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                    //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #L-LOCAL-A-CURRENT
        pnd_L_Canadian_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                   //Natural: ADD TWRT-CAN-WHHLD-AMT TO #L-CANADIAN-A-CURRENT
        //* **
        getWorkFiles().write(3, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 3 TWRT-RECORD
        //*  S010-PROCESS-CURRENT-L
    }
    private void sub_S010_Process_Current_T() throws Exception                                                                                                            //Natural: S010-PROCESS-CURRENT-T
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Current_T.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-CURRENT-T
        pnd_T_Gross_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                          //Natural: ADD TWRT-GROSS-AMT TO #T-GROSS-A-CURRENT
        pnd_T_Ivc_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                              //Natural: ADD TWRT-IVC-AMT TO #T-IVC-A-CURRENT
        pnd_T_Int_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                         //Natural: ADD TWRT-INTEREST-AMT TO #T-INT-A-CURRENT
        pnd_T_Fed_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                        //Natural: ADD TWRT-FED-WHHLD-AMT TO #T-FED-A-CURRENT
        pnd_T_Nra_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                        //Natural: ADD TWRT-NRA-WHHLD-AMT TO #T-NRA-A-CURRENT
        pnd_T_State_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                    //Natural: ADD TWRT-STATE-WHHLD-AMT TO #T-STATE-A-CURRENT
        pnd_T_Local_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                    //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #T-LOCAL-A-CURRENT
        pnd_T_Canadian_A_Current.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                   //Natural: ADD TWRT-CAN-WHHLD-AMT TO #T-CANADIAN-A-CURRENT
        //* **
        getWorkFiles().write(3, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 3 TWRT-RECORD
        //*  S010-PROCESS-CURRENT-T
    }
    private void sub_S020_Process_Previous() throws Exception                                                                                                             //Natural: S020-PROCESS-PREVIOUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        short decideConditionsMet500 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'T'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet500++;
                                                                                                                                                                          //Natural: PERFORM S020-PROCESS-PREVIOUS-T
            sub_S020_Process_Previous_T();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet500++;
                                                                                                                                                                          //Natural: PERFORM S020-PROCESS-PREVIOUS-L
            sub_S020_Process_Previous_L();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, "Non Acceptable Company code in Input file");                                                                                           //Natural: WRITE 'Non Acceptable Company code in Input file'
            if (Global.isEscape()) return;
            DbsUtil.terminate(69);  if (true) return;                                                                                                                     //Natural: TERMINATE 69
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  S020-PROCESS-PREVIOUS
    }
    private void sub_S020_Process_Previous_L() throws Exception                                                                                                           //Natural: S020-PROCESS-PREVIOUS-L
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Previous_L.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #COUNTER-PREVIOUS-L
        pnd_L_Gross_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                         //Natural: ADD TWRT-GROSS-AMT TO #L-GROSS-A-PREVIOUS
        pnd_L_Ivc_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                             //Natural: ADD TWRT-IVC-AMT TO #L-IVC-A-PREVIOUS
        pnd_L_Int_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                        //Natural: ADD TWRT-INTEREST-AMT TO #L-INT-A-PREVIOUS
        pnd_L_Fed_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                       //Natural: ADD TWRT-FED-WHHLD-AMT TO #L-FED-A-PREVIOUS
        pnd_L_Nra_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                       //Natural: ADD TWRT-NRA-WHHLD-AMT TO #L-NRA-A-PREVIOUS
        pnd_L_State_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                   //Natural: ADD TWRT-STATE-WHHLD-AMT TO #L-STATE-A-PREVIOUS
        pnd_L_Local_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                   //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #L-LOCAL-A-PREVIOUS
        pnd_L_Canadian_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                  //Natural: ADD TWRT-CAN-WHHLD-AMT TO #L-CANADIAN-A-PREVIOUS
        //* **
        getWorkFiles().write(4, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 4 TWRT-RECORD
        //*   S020-PROCESS-PREVIOUS-L
    }
    private void sub_S020_Process_Previous_T() throws Exception                                                                                                           //Natural: S020-PROCESS-PREVIOUS-T
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Previous_T.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #COUNTER-PREVIOUS-T
        pnd_T_Gross_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                         //Natural: ADD TWRT-GROSS-AMT TO #T-GROSS-A-PREVIOUS
        pnd_T_Ivc_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                             //Natural: ADD TWRT-IVC-AMT TO #T-IVC-A-PREVIOUS
        pnd_T_Int_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                        //Natural: ADD TWRT-INTEREST-AMT TO #T-INT-A-PREVIOUS
        pnd_T_Fed_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                       //Natural: ADD TWRT-FED-WHHLD-AMT TO #T-FED-A-PREVIOUS
        pnd_T_Nra_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                       //Natural: ADD TWRT-NRA-WHHLD-AMT TO #T-NRA-A-PREVIOUS
        pnd_T_State_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                   //Natural: ADD TWRT-STATE-WHHLD-AMT TO #T-STATE-A-PREVIOUS
        pnd_T_Local_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                   //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #T-LOCAL-A-PREVIOUS
        pnd_T_Canadian_A_Previous.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                  //Natural: ADD TWRT-CAN-WHHLD-AMT TO #T-CANADIAN-A-PREVIOUS
        //* **
        getWorkFiles().write(4, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 4 TWRT-RECORD
        //*   S020-PROCESS-PREVIOUS-T
    }
    private void sub_S030_Process_Rest() throws Exception                                                                                                                 //Natural: S030-PROCESS-REST
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        short decideConditionsMet543 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'T'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet543++;
                                                                                                                                                                          //Natural: PERFORM S030-PROCESS-REST-T
            sub_S030_Process_Rest_T();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet543++;
                                                                                                                                                                          //Natural: PERFORM S030-PROCESS-REST-L
            sub_S030_Process_Rest_L();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, "Non Acceptable Company code in Input file");                                                                                           //Natural: WRITE 'Non Acceptable Company code in Input file'
            if (Global.isEscape()) return;
            DbsUtil.terminate(69);  if (true) return;                                                                                                                     //Natural: TERMINATE 69
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  S030-PROCESS-REST
    }
    private void sub_S030_Process_Rest_L() throws Exception                                                                                                               //Natural: S030-PROCESS-REST-L
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Rest_L.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #COUNTER-REST-L
        pnd_L_Gross_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                             //Natural: ADD TWRT-GROSS-AMT TO #L-GROSS-A-REST
        pnd_L_Ivc_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                                 //Natural: ADD TWRT-IVC-AMT TO #L-IVC-A-REST
        pnd_L_Int_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                            //Natural: ADD TWRT-INTEREST-AMT TO #L-INT-A-REST
        pnd_L_Fed_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                           //Natural: ADD TWRT-FED-WHHLD-AMT TO #L-FED-A-REST
        pnd_L_Nra_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                           //Natural: ADD TWRT-NRA-WHHLD-AMT TO #L-NRA-A-REST
        pnd_L_State_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                       //Natural: ADD TWRT-STATE-WHHLD-AMT TO #L-STATE-A-REST
        pnd_L_Local_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                       //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #L-LOCAL-A-REST
        pnd_L_Canadian_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #L-CANADIAN-A-REST
        //* **
        //*  MB 03/13/2012
        //*  MB 03/13/2012
        if (condition(! (DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"NNNN"))))                                                                        //Natural: IF TWRT-TAX-YEAR NE MASK ( NNNN )
        {
            ldaTwrl0700.getTwrt_Record_Twrt_Filler().setValue("INV.YEAR");                                                                                                //Natural: ASSIGN TWRT-FILLER := 'INV.YEAR'
            //*  MB 03/13/2012
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        getWorkFiles().write(5, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 5 TWRT-RECORD
        //*  S030-PROCESS-REST-L
    }
    private void sub_S030_Process_Rest_T() throws Exception                                                                                                               //Natural: S030-PROCESS-REST-T
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Counter_Rest_T.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #COUNTER-REST-T
        pnd_T_Gross_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                             //Natural: ADD TWRT-GROSS-AMT TO #T-GROSS-A-REST
        pnd_T_Ivc_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                                 //Natural: ADD TWRT-IVC-AMT TO #T-IVC-A-REST
        pnd_T_Int_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                                                            //Natural: ADD TWRT-INTEREST-AMT TO #T-INT-A-REST
        pnd_T_Fed_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                           //Natural: ADD TWRT-FED-WHHLD-AMT TO #T-FED-A-REST
        pnd_T_Nra_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                                                                           //Natural: ADD TWRT-NRA-WHHLD-AMT TO #T-NRA-A-REST
        pnd_T_State_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                       //Natural: ADD TWRT-STATE-WHHLD-AMT TO #T-STATE-A-REST
        pnd_T_Local_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                                                                       //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #T-LOCAL-A-REST
        pnd_T_Canadian_A_Rest.nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #T-CANADIAN-A-REST
        //* **
        //*  MB 03/13/2012
        //*  MB 03/13/2012
        if (condition(! (DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"NNNN"))))                                                                        //Natural: IF TWRT-TAX-YEAR NE MASK ( NNNN )
        {
            ldaTwrl0700.getTwrt_Record_Twrt_Filler().setValue("INV.YEAR");                                                                                                //Natural: ASSIGN TWRT-FILLER := 'INV.YEAR'
            //*  MB 03/13/2012
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        getWorkFiles().write(5, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 5 TWRT-RECORD
        //*  S030-PROCESS-REST-T
    }
    private void sub_S040_Process_Control() throws Exception                                                                                                              //Natural: S040-PROCESS-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_Inp_Sum_1().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Rec_1());                                                                 //Natural: ASSIGN #INP-SUM-1 = TWRT-REC-1
        if (condition(pnd_L_Gross_A_Current.add(pnd_L_Gross_A_Previous).add(pnd_L_Gross_A_Rest).add(pnd_T_Gross_A_Current).add(pnd_T_Gross_A_Previous).add(pnd_T_Gross_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A()))  //Natural: IF #L-GROSS-A-CURRENT + #L-GROSS-A-PREVIOUS + #L-GROSS-A-REST + #T-GROSS-A-CURRENT + #T-GROSS-A-PREVIOUS + #T-GROSS-A-REST EQ #L-GROSS-A + #T-GROSS-A AND #L-IVC-A-CURRENT + #L-IVC-A-PREVIOUS + #L-IVC-A-REST + #T-IVC-A-CURRENT + #T-IVC-A-PREVIOUS + #T-IVC-A-REST EQ #L-IVC-A + #T-IVC-A AND #L-INT-A-CURRENT + #L-INT-A-PREVIOUS + #L-INT-A-REST + #T-INT-A-CURRENT + #T-INT-A-PREVIOUS + #T-INT-A-REST EQ #L-INT-A + #T-INT-A AND #L-FED-A-CURRENT + #L-FED-A-PREVIOUS + #L-FED-A-REST + #T-FED-A-CURRENT + #T-FED-A-PREVIOUS + #T-FED-A-REST EQ #L-FED-A + #T-FED-A AND #L-NRA-A-CURRENT + #L-NRA-A-PREVIOUS + #L-NRA-A-REST + #T-NRA-A-CURRENT + #T-NRA-A-PREVIOUS + #T-NRA-A-REST EQ #L-NRA-A + #T-NRA-A AND #L-STATE-A-CURRENT + #L-STATE-A-PREVIOUS + #L-STATE-A-REST + #T-STATE-A-CURRENT + #T-STATE-A-PREVIOUS + #T-STATE-A-REST EQ #L-STATE-A + #T-STATE-A AND #L-LOCAL-A-CURRENT + #L-LOCAL-A-PREVIOUS + #L-LOCAL-A-REST + #T-LOCAL-A-CURRENT + #T-LOCAL-A-PREVIOUS + #T-LOCAL-A-REST EQ #L-LOCAL-A + #T-LOCAL-A AND #L-CANADIAN-A-CURRENT + #L-CANADIAN-A-PREVIOUS + #L-CANADIAN-A-REST + #T-CANADIAN-A-CURRENT + #T-CANADIAN-A-PREVIOUS + #T-CANADIAN-A-REST EQ #L-CANADIAN-A + #T-CANADIAN-A
            && pnd_L_Ivc_A_Current.add(pnd_L_Ivc_A_Previous).add(pnd_L_Ivc_A_Rest).add(pnd_T_Ivc_A_Current).add(pnd_T_Ivc_A_Previous).add(pnd_T_Ivc_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A())) 
            && pnd_L_Int_A_Current.add(pnd_L_Int_A_Previous).add(pnd_L_Int_A_Rest).add(pnd_T_Int_A_Current).add(pnd_T_Int_A_Previous).add(pnd_T_Int_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A())) 
            && pnd_L_Fed_A_Current.add(pnd_L_Fed_A_Previous).add(pnd_L_Fed_A_Rest).add(pnd_T_Fed_A_Current).add(pnd_T_Fed_A_Previous).add(pnd_T_Fed_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A())) 
            && pnd_L_Nra_A_Current.add(pnd_L_Nra_A_Previous).add(pnd_L_Nra_A_Rest).add(pnd_T_Nra_A_Current).add(pnd_T_Nra_A_Previous).add(pnd_T_Nra_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A())) 
            && pnd_L_State_A_Current.add(pnd_L_State_A_Previous).add(pnd_L_State_A_Rest).add(pnd_T_State_A_Current).add(pnd_T_State_A_Previous).add(pnd_T_State_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A())) 
            && pnd_L_Local_A_Current.add(pnd_L_Local_A_Previous).add(pnd_L_Local_A_Rest).add(pnd_T_Local_A_Current).add(pnd_T_Local_A_Previous).add(pnd_T_Local_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A())) 
            && pnd_L_Canadian_A_Current.add(pnd_L_Canadian_A_Previous).add(pnd_L_Canadian_A_Rest).add(pnd_T_Canadian_A_Current).add(pnd_T_Canadian_A_Previous).add(pnd_T_Canadian_A_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A()))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "MISBALANCE DETECTED");                                                                                                                 //Natural: WRITE 'MISBALANCE DETECTED'
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-GROSS-A-CURRENT: ",pnd_L_Gross_A_Current);                                                                                          //Natural: WRITE '#L-GROSS-A-CURRENT: ' #L-GROSS-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-GROSS-A-PREVIOUS:",pnd_L_Gross_A_Previous);                                                                                         //Natural: WRITE '#L-GROSS-A-PREVIOUS:' #L-GROSS-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-GROSS-A-RESTS:   ",pnd_L_Gross_A_Rest);                                                                                             //Natural: WRITE '#L-GROSS-A-RESTS:   ' #L-GROSS-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-GROSS-A:         ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A());                                                                 //Natural: WRITE '#L-GROSS-A:         ' #L-GROSS-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-GROSS-A-CURRENT: ",pnd_T_Gross_A_Current);                                                                                          //Natural: WRITE '#T-GROSS-A-CURRENT: ' #T-GROSS-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-GROSS-A-PREVIOUS:",pnd_T_Gross_A_Previous);                                                                                         //Natural: WRITE '#T-GROSS-A-PREVIOUS:' #T-GROSS-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-GROSS-A-RESTS:   ",pnd_T_Gross_A_Rest);                                                                                             //Natural: WRITE '#T-GROSS-A-RESTS:   ' #T-GROSS-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-GROSS-A:         ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A());                                                                 //Natural: WRITE '#T-GROSS-A:         ' #T-GROSS-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-IVC-A-CURRENT:     ",pnd_L_Ivc_A_Current);                                                                                          //Natural: WRITE '#L-IVC-A-CURRENT:     ' #L-IVC-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-IVC-A-PREVIOUS:    ",pnd_L_Ivc_A_Previous);                                                                                         //Natural: WRITE '#L-IVC-A-PREVIOUS:    ' #L-IVC-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-IVC-A-REST:        ",pnd_L_Ivc_A_Rest);                                                                                             //Natural: WRITE '#L-IVC-A-REST:        ' #L-IVC-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-IVC-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A());                                                                 //Natural: WRITE '#L-IVC-A:             ' #L-IVC-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-IVC-A-CURRENT:     ",pnd_T_Ivc_A_Current);                                                                                          //Natural: WRITE '#T-IVC-A-CURRENT:     ' #T-IVC-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-IVC-A-PREVIOUS:    ",pnd_T_Ivc_A_Previous);                                                                                         //Natural: WRITE '#T-IVC-A-PREVIOUS:    ' #T-IVC-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-IVC-A-REST:        ",pnd_T_Ivc_A_Rest);                                                                                             //Natural: WRITE '#T-IVC-A-REST:        ' #T-IVC-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-IVC-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A());                                                                 //Natural: WRITE '#T-IVC-A:             ' #T-IVC-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-INT-A-CURRENT:     ",pnd_L_Int_A_Current);                                                                                          //Natural: WRITE '#L-INT-A-CURRENT:     ' #L-INT-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-INT-A-PREVIOUS:    ",pnd_L_Int_A_Previous);                                                                                         //Natural: WRITE '#L-INT-A-PREVIOUS:    ' #L-INT-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-INT-A-REST:        ",pnd_L_Int_A_Rest);                                                                                             //Natural: WRITE '#L-INT-A-REST:        ' #L-INT-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-INT-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A());                                                                 //Natural: WRITE '#L-INT-A:             ' #L-INT-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-INT-A-CURRENT:     ",pnd_T_Int_A_Current);                                                                                          //Natural: WRITE '#T-INT-A-CURRENT:     ' #T-INT-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-INT-A-PREVIOUS:    ",pnd_T_Int_A_Previous);                                                                                         //Natural: WRITE '#T-INT-A-PREVIOUS:    ' #T-INT-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-INT-A-REST:        ",pnd_T_Int_A_Rest);                                                                                             //Natural: WRITE '#T-INT-A-REST:        ' #T-INT-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-INT-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A());                                                                 //Natural: WRITE '#T-INT-A:             ' #T-INT-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-FED-A-CURRENT:     ",pnd_L_Fed_A_Current);                                                                                          //Natural: WRITE '#L-FED-A-CURRENT:     ' #L-FED-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-FED-A-PREVIOUS:    ",pnd_L_Fed_A_Previous);                                                                                         //Natural: WRITE '#L-FED-A-PREVIOUS:    ' #L-FED-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-FED-A-REST:        ",pnd_L_Fed_A_Rest);                                                                                             //Natural: WRITE '#L-FED-A-REST:        ' #L-FED-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-FED-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A());                                                                 //Natural: WRITE '#L-FED-A:             ' #L-FED-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-FED-A-CURRENT:     ",pnd_T_Fed_A_Current);                                                                                          //Natural: WRITE '#T-FED-A-CURRENT:     ' #T-FED-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-FED-A-PREVIOUS:    ",pnd_T_Fed_A_Previous);                                                                                         //Natural: WRITE '#T-FED-A-PREVIOUS:    ' #T-FED-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-FED-A-REST:        ",pnd_T_Fed_A_Rest);                                                                                             //Natural: WRITE '#T-FED-A-REST:        ' #T-FED-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-FED-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A());                                                                 //Natural: WRITE '#T-FED-A:             ' #T-FED-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-NRA-A-CURRENT:     ",pnd_L_Nra_A_Current);                                                                                          //Natural: WRITE '#L-NRA-A-CURRENT:     ' #L-NRA-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-NRA-A-PREVIOUS:    ",pnd_L_Nra_A_Previous);                                                                                         //Natural: WRITE '#L-NRA-A-PREVIOUS:    ' #L-NRA-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-NRA-A-REST:        ",pnd_L_Nra_A_Rest);                                                                                             //Natural: WRITE '#L-NRA-A-REST:        ' #L-NRA-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-NRA-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A());                                                                 //Natural: WRITE '#L-NRA-A:             ' #L-NRA-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-NRA-A-CURRENT:     ",pnd_T_Nra_A_Current);                                                                                          //Natural: WRITE '#T-NRA-A-CURRENT:     ' #T-NRA-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-NRA-A-PREVIOUS:    ",pnd_T_Nra_A_Previous);                                                                                         //Natural: WRITE '#T-NRA-A-PREVIOUS:    ' #T-NRA-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-NRA-A-REST:        ",pnd_T_Nra_A_Rest);                                                                                             //Natural: WRITE '#T-NRA-A-REST:        ' #T-NRA-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-NRA-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A());                                                                 //Natural: WRITE '#T-NRA-A:             ' #T-NRA-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-STATE-A-CURRENT:   ",pnd_L_State_A_Current);                                                                                        //Natural: WRITE '#L-STATE-A-CURRENT:   ' #L-STATE-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-STATE-A-PREVIOUS:  ",pnd_L_State_A_Previous);                                                                                       //Natural: WRITE '#L-STATE-A-PREVIOUS:  ' #L-STATE-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-STATE-A-REST:      ",pnd_L_State_A_Rest);                                                                                           //Natural: WRITE '#L-STATE-A-REST:      ' #L-STATE-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-STATE-A:           ",pnd_L_State_A_Current);                                                                                        //Natural: WRITE '#L-STATE-A:           ' #L-STATE-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-STATE-A-CURRENT:   ",pnd_T_State_A_Current);                                                                                        //Natural: WRITE '#T-STATE-A-CURRENT:   ' #T-STATE-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-STATE-A-PREVIOUS:  ",pnd_T_State_A_Previous);                                                                                       //Natural: WRITE '#T-STATE-A-PREVIOUS:  ' #T-STATE-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-STATE-A-REST:      ",pnd_T_State_A_Rest);                                                                                           //Natural: WRITE '#T-STATE-A-REST:      ' #T-STATE-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-STATE-A:           ",pnd_T_State_A_Current);                                                                                        //Natural: WRITE '#T-STATE-A:           ' #T-STATE-A-CURRENT
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-LOCAL-A-CURRENT:     ",pnd_L_Local_A_Current);                                                                                      //Natural: WRITE '#L-LOCAL-A-CURRENT:     ' #L-LOCAL-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-LOCAL-A-PREVIOUS:    ",pnd_L_Local_A_Previous);                                                                                     //Natural: WRITE '#L-LOCAL-A-PREVIOUS:    ' #L-LOCAL-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-LOCAL-A-REST:        ",pnd_L_Local_A_Rest);                                                                                         //Natural: WRITE '#L-LOCAL-A-REST:        ' #L-LOCAL-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-LOCAL-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A());                                                             //Natural: WRITE '#L-LOCAL-A:             ' #L-LOCAL-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-LOCAL-A-CURRENT:     ",pnd_T_Local_A_Current);                                                                                      //Natural: WRITE '#T-LOCAL-A-CURRENT:     ' #T-LOCAL-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-LOCAL-A-PREVIOUS:    ",pnd_T_Local_A_Previous);                                                                                     //Natural: WRITE '#T-LOCAL-A-PREVIOUS:    ' #T-LOCAL-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-LOCAL-A-REST:        ",pnd_T_Local_A_Rest);                                                                                         //Natural: WRITE '#T-LOCAL-A-REST:        ' #T-LOCAL-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-LOCAL-A:             ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A());                                                             //Natural: WRITE '#T-LOCAL-A:             ' #T-LOCAL-A
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, "#L-CANADIAN-A-CURRENT:  ",pnd_L_Canadian_A_Current);                                                                                   //Natural: WRITE '#L-CANADIAN-A-CURRENT:  ' #L-CANADIAN-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#L-CANADIAN-A-PREVIOUS: ",pnd_L_Canadian_A_Previous);                                                                                  //Natural: WRITE '#L-CANADIAN-A-PREVIOUS: ' #L-CANADIAN-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#L-CANADIAN-A-REST:     ",pnd_L_Canadian_A_Rest);                                                                                      //Natural: WRITE '#L-CANADIAN-A-REST:     ' #L-CANADIAN-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#L-CANADIAN-A:          ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A());                                                          //Natural: WRITE '#L-CANADIAN-A:          ' #L-CANADIAN-A
            if (Global.isEscape()) return;
            getReports().write(0, "#T-CANADIAN-A-CURRENT:  ",pnd_T_Canadian_A_Current);                                                                                   //Natural: WRITE '#T-CANADIAN-A-CURRENT:  ' #T-CANADIAN-A-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#T-CANADIAN-A-PREVIOUS: ",pnd_T_Canadian_A_Previous);                                                                                  //Natural: WRITE '#T-CANADIAN-A-PREVIOUS: ' #T-CANADIAN-A-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#T-CANADIAN-A-REST:     ",pnd_T_Canadian_A_Rest);                                                                                      //Natural: WRITE '#T-CANADIAN-A-REST:     ' #T-CANADIAN-A-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#T-CANADIAN-A:          ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A());                                                          //Natural: WRITE '#T-CANADIAN-A:          ' #T-CANADIAN-A
            if (Global.isEscape()) return;
            DbsUtil.terminate(64);  if (true) return;                                                                                                                     //Natural: TERMINATE 64
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Counter_Current.add(pnd_Counter_Previous).add(pnd_Counter_Rest).equals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().add(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt())))) //Natural: IF #COUNTER-CURRENT + #COUNTER-PREVIOUS + #COUNTER-REST EQ #L-REC-CNT + #T-REC-CNT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "MISMATCH ON THE NUMBER OF RECORDS");                                                                                                   //Natural: WRITE 'MISMATCH ON THE NUMBER OF RECORDS'
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-CURRENT  :    ",pnd_Counter_Current);                                                                                         //Natural: WRITE '#COUNTER-CURRENT  :    ' #COUNTER-CURRENT
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-CURRENT-L:    ",pnd_Counter_Current_L);                                                                                       //Natural: WRITE '#COUNTER-CURRENT-L:    ' #COUNTER-CURRENT-L
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-CURRENT-T:    ",pnd_Counter_Current_T);                                                                                       //Natural: WRITE '#COUNTER-CURRENT-T:    ' #COUNTER-CURRENT-T
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-PREVIOUS  :   ",pnd_Counter_Previous);                                                                                        //Natural: WRITE '#COUNTER-PREVIOUS  :   ' #COUNTER-PREVIOUS
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-PREVIOUS-L:   ",pnd_Counter_Previous_L);                                                                                      //Natural: WRITE '#COUNTER-PREVIOUS-L:   ' #COUNTER-PREVIOUS-L
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-PREVIOUS-T:   ",pnd_Counter_Previous_T);                                                                                      //Natural: WRITE '#COUNTER-PREVIOUS-T:   ' #COUNTER-PREVIOUS-T
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-REST  :       ",pnd_Counter_Rest);                                                                                            //Natural: WRITE '#COUNTER-REST  :       ' #COUNTER-REST
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-REST-L:       ",pnd_Counter_Rest_L);                                                                                          //Natural: WRITE '#COUNTER-REST-L:       ' #COUNTER-REST-L
            if (Global.isEscape()) return;
            getReports().write(0, "#COUNTER-REST-T:       ",pnd_Counter_Rest_T);                                                                                          //Natural: WRITE '#COUNTER-REST-T:       ' #COUNTER-REST-T
            if (Global.isEscape()) return;
            getReports().write(0, "TOTAL COUNTER-L:       ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt());                                                              //Natural: WRITE 'TOTAL COUNTER-L:       ' #L-REC-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "TOTAL COUNTER-T:       ",ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt());                                                              //Natural: WRITE 'TOTAL COUNTER-T:       ' #T-REC-CNT
            if (Global.isEscape()) return;
            DbsUtil.terminate(74);  if (true) return;                                                                                                                     //Natural: TERMINATE 74
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------
        //* ** CREATE CONTROL RECORDS FOR CURRENT AND PREVIOUS YEARS
        //* ** RECORDS WITH YEARS BEFORE PREVIOUS DO NOT NEED CONTROL RECORD
        //* *----------------------------------------------------------------
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().setValue(pnd_Counter_Current_L);                                                                                   //Natural: MOVE #COUNTER-CURRENT-L TO #L-REC-CNT
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().setValue(pnd_L_Gross_A_Current);                                                                                   //Natural: MOVE #L-GROSS-A-CURRENT TO #L-GROSS-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().setValue(pnd_L_Ivc_A_Current);                                                                                       //Natural: MOVE #L-IVC-A-CURRENT TO #L-IVC-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().setValue(pnd_L_Int_A_Current);                                                                                       //Natural: MOVE #L-INT-A-CURRENT TO #L-INT-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().setValue(pnd_L_Fed_A_Current);                                                                                       //Natural: MOVE #L-FED-A-CURRENT TO #L-FED-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().setValue(pnd_L_Nra_A_Current);                                                                                       //Natural: MOVE #L-NRA-A-CURRENT TO #L-NRA-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().setValue(pnd_L_State_A_Current);                                                                                   //Natural: MOVE #L-STATE-A-CURRENT TO #L-STATE-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().setValue(pnd_L_Local_A_Current);                                                                                   //Natural: MOVE #L-LOCAL-A-CURRENT TO #L-LOCAL-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().setValue(pnd_L_Canadian_A_Current);                                                                             //Natural: MOVE #L-CANADIAN-A-CURRENT TO #L-CANADIAN-A
        //* **
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().setValue(pnd_Counter_Current_T);                                                                                   //Natural: MOVE #COUNTER-CURRENT-T TO #T-REC-CNT
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().setValue(pnd_T_Gross_A_Current);                                                                                   //Natural: MOVE #T-GROSS-A-CURRENT TO #T-GROSS-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().setValue(pnd_T_Ivc_A_Current);                                                                                       //Natural: MOVE #T-IVC-A-CURRENT TO #T-IVC-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().setValue(pnd_T_Int_A_Current);                                                                                       //Natural: MOVE #T-INT-A-CURRENT TO #T-INT-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().setValue(pnd_T_Fed_A_Current);                                                                                       //Natural: MOVE #T-FED-A-CURRENT TO #T-FED-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().setValue(pnd_T_Nra_A_Current);                                                                                       //Natural: MOVE #T-NRA-A-CURRENT TO #T-NRA-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().setValue(pnd_T_State_A_Current);                                                                                   //Natural: MOVE #T-STATE-A-CURRENT TO #T-STATE-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().setValue(pnd_T_Local_A_Current);                                                                                   //Natural: MOVE #T-LOCAL-A-CURRENT TO #T-LOCAL-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().setValue(pnd_T_Canadian_A_Current);                                                                             //Natural: MOVE #T-CANADIAN-A-CURRENT TO #T-CANADIAN-A
        //* **
        getWorkFiles().write(3, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                                 //Natural: WRITE WORK FILE 3 #INP-SUMMARY
        //* **
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().setValue(pnd_Counter_Previous_L);                                                                                  //Natural: MOVE #COUNTER-PREVIOUS-L TO #L-REC-CNT
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().setValue(pnd_L_Gross_A_Previous);                                                                                  //Natural: MOVE #L-GROSS-A-PREVIOUS TO #L-GROSS-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().setValue(pnd_L_Ivc_A_Previous);                                                                                      //Natural: MOVE #L-IVC-A-PREVIOUS TO #L-IVC-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().setValue(pnd_L_Int_A_Previous);                                                                                      //Natural: MOVE #L-INT-A-PREVIOUS TO #L-INT-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().setValue(pnd_L_Fed_A_Previous);                                                                                      //Natural: MOVE #L-FED-A-PREVIOUS TO #L-FED-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().setValue(pnd_L_Nra_A_Previous);                                                                                      //Natural: MOVE #L-NRA-A-PREVIOUS TO #L-NRA-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().setValue(pnd_L_State_A_Previous);                                                                                  //Natural: MOVE #L-STATE-A-PREVIOUS TO #L-STATE-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().setValue(pnd_L_Local_A_Previous);                                                                                  //Natural: MOVE #L-LOCAL-A-PREVIOUS TO #L-LOCAL-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().setValue(pnd_L_Canadian_A_Previous);                                                                            //Natural: MOVE #L-CANADIAN-A-PREVIOUS TO #L-CANADIAN-A
        //* **
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().setValue(pnd_Counter_Previous_T);                                                                                  //Natural: MOVE #COUNTER-PREVIOUS-T TO #T-REC-CNT
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().setValue(pnd_T_Gross_A_Previous);                                                                                  //Natural: MOVE #T-GROSS-A-PREVIOUS TO #T-GROSS-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().setValue(pnd_T_Ivc_A_Previous);                                                                                      //Natural: MOVE #T-IVC-A-PREVIOUS TO #T-IVC-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().setValue(pnd_T_Int_A_Previous);                                                                                      //Natural: MOVE #T-INT-A-PREVIOUS TO #T-INT-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().setValue(pnd_T_Fed_A_Previous);                                                                                      //Natural: MOVE #T-FED-A-PREVIOUS TO #T-FED-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().setValue(pnd_T_Nra_A_Previous);                                                                                      //Natural: MOVE #T-NRA-A-PREVIOUS TO #T-NRA-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().setValue(pnd_T_State_A_Previous);                                                                                  //Natural: MOVE #T-STATE-A-PREVIOUS TO #T-STATE-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().setValue(pnd_T_Local_A_Previous);                                                                                  //Natural: MOVE #T-LOCAL-A-PREVIOUS TO #T-LOCAL-A
        ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().setValue(pnd_T_Canadian_A_Previous);                                                                            //Natural: MOVE #T-CANADIAN-A-PREVIOUS TO #T-CANADIAN-A
        getWorkFiles().write(4, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                                 //Natural: WRITE WORK FILE 4 #INP-SUMMARY
        //* **
        //*  S040-PROCESS-CONTROL
    }

    //
}
