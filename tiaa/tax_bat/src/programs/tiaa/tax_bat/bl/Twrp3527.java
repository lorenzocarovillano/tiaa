/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:42 PM
**        * FROM NATURAL PROGRAM : Twrp3527
************************************************************
**        * FILE NAME            : Twrp3527.java
**        * CLASS NAME           : Twrp3527
**        * INSTANCE NAME        : Twrp3527
************************************************************
************************************************************************
** PROGRAM : TWRP3527
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: NEW YORK STATE CORRECTION REPORTING / RECONCILIATION
**
**
** HISTORY.....:
** MM/DD/YY - XX - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3527 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_State;
    private DbsField pnd_Ws_Pnd_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_State = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3527() throws Exception
    {
        super("Twrp3527");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3527|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_State,pnd_Ws_Pnd_Tax_Year);                                                                 //Natural: INPUT #WS.#STATE #WS.#TAX-YEAR
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet38 = 0;                                                                                                                          //Natural: DECIDE ON FIRST VALUE #WS.#TAX-YEAR;//Natural: VALUE 1999 : 2001
                if (condition(((pnd_Ws_Pnd_Tax_Year.greaterOrEqual(1999) && pnd_Ws_Pnd_Tax_Year.lessOrEqual(2001)))))
                {
                    decideConditionsMet38++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3544' #WS.#STATE #WS.#TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3544"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2002
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2002))))
                {
                    decideConditionsMet38++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3545' #WS.#STATE #WS.#TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3545"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: VALUE 2003
                else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2003))))
                {
                    decideConditionsMet38++;
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3547' #WS.#STATE #WS.#TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3547"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    Global.getSTACK().pushData(StackOption.TOP, pnd_Ws_Pnd_State, pnd_Ws_Pnd_Tax_Year);                                                                   //Natural: FETCH 'TWRP3551' #WS.#STATE #WS.#TAX-YEAR
                    Global.setFetchProgram(DbsUtil.getBlType("TWRP3551"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-DECIDE
                //* ************************
                //*  S U B R O U T I N E S
                //* ************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
    }
}
