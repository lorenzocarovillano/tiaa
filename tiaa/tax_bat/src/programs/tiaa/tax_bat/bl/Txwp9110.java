/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:50 PM
**        * FROM NATURAL PROGRAM : Txwp9110
************************************************************
**        * FILE NAME            : Txwp9110.java
**        * CLASS NAME           : Txwp9110
**        * INSTANCE NAME        : Txwp9110
************************************************************
************************************************************************
****     TAX ALERTS - COMBINE ALL RECORDS WITH SAME PIN AND CONTRACT NO.
************************************************************************
* PROGRAM   : TXWP9110
* SYSTEM    : TAX ELECTION
* TITLE     : TAX ALERTS NOTIFICATION
* GENERATED :
* FUNCTION  : PROGRAM TO COMBINE ALL RATE CHANGES FOR THE DAY BY PIN
*           : AND CONTRACT NUMBER.
*           :
* HISTORY   :
* 11/22/2014: F.ENDAYA    NEW
* 10/20/2014: O. SOTTO    CHANGES TO OUTPUT FILE MARKED BY 102014.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp9110 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_In_Workfile;

    private DbsGroup pnd_In_Workfile__R_Field_1;
    private DbsField pnd_In_Workfile_Pnd_Pin_Cont;

    private DbsGroup pnd_In_Workfile__R_Field_2;
    private DbsField pnd_In_Workfile_Pnd_Pin;
    private DbsField pnd_In_Workfile_Pnd_Contract_Nbr_4;
    private DbsField pnd_In_Workfile_Pnd_Old_Withholding_Rate;
    private DbsField pnd_In_Workfile_Pnd_New_Withholding_Rate;
    private DbsField pnd_In_Workfile_Pnd_Elc_Type;
    private DbsField pnd_In_Workfile_Pnd_Filler;
    private DbsField pnd_Out_Workfile;

    private DbsGroup pnd_Out_Workfile__R_Field_3;
    private DbsField pnd_Out_Workfile_Pnd_O_Pin;
    private DbsField pnd_Out_Workfile_Pnd_O_Contract_Nbr_4;
    private DbsField pnd_Out_Workfile_Pnd_O_Fed_St_Lo_Rate;

    private DbsGroup pnd_Out_Workfile__R_Field_4;

    private DbsGroup pnd_Out_Workfile_Pnd_O_Rate_Tab;
    private DbsField pnd_Out_Workfile_Pnd_O_Elc_Type;
    private DbsField pnd_Out_Workfile_Pnd_O_New_Withholding_Rate;
    private DbsField pnd_Out_Workfile_Pnd_O_Old_Withholding_Rate;

    private DbsGroup pnd_Out_Workfile__R_Field_5;
    private DbsField pnd_Out_Workfile_Pnd_Out_Record;
    private DbsField pnd_Out_Workfile_Pnd_Out_Fill;
    private DbsField pnd_Sv_Area;
    private DbsField pnd_First;
    private DbsField pnd_Write;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Wrt_Cnt;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_In_Workfile = localVariables.newFieldInRecord("pnd_In_Workfile", "#IN-WORKFILE", FieldType.STRING, 80);

        pnd_In_Workfile__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Workfile__R_Field_1", "REDEFINE", pnd_In_Workfile);
        pnd_In_Workfile_Pnd_Pin_Cont = pnd_In_Workfile__R_Field_1.newFieldInGroup("pnd_In_Workfile_Pnd_Pin_Cont", "#PIN-CONT", FieldType.STRING, 16);

        pnd_In_Workfile__R_Field_2 = pnd_In_Workfile__R_Field_1.newGroupInGroup("pnd_In_Workfile__R_Field_2", "REDEFINE", pnd_In_Workfile_Pnd_Pin_Cont);
        pnd_In_Workfile_Pnd_Pin = pnd_In_Workfile__R_Field_2.newFieldInGroup("pnd_In_Workfile_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_In_Workfile_Pnd_Contract_Nbr_4 = pnd_In_Workfile__R_Field_2.newFieldInGroup("pnd_In_Workfile_Pnd_Contract_Nbr_4", "#CONTRACT-NBR-4", FieldType.STRING, 
            4);
        pnd_In_Workfile_Pnd_Old_Withholding_Rate = pnd_In_Workfile__R_Field_1.newFieldInGroup("pnd_In_Workfile_Pnd_Old_Withholding_Rate", "#OLD-WITHHOLDING-RATE", 
            FieldType.STRING, 12);
        pnd_In_Workfile_Pnd_New_Withholding_Rate = pnd_In_Workfile__R_Field_1.newFieldInGroup("pnd_In_Workfile_Pnd_New_Withholding_Rate", "#NEW-WITHHOLDING-RATE", 
            FieldType.STRING, 12);
        pnd_In_Workfile_Pnd_Elc_Type = pnd_In_Workfile__R_Field_1.newFieldInGroup("pnd_In_Workfile_Pnd_Elc_Type", "#ELC-TYPE", FieldType.STRING, 1);
        pnd_In_Workfile_Pnd_Filler = pnd_In_Workfile__R_Field_1.newFieldInGroup("pnd_In_Workfile_Pnd_Filler", "#FILLER", FieldType.STRING, 39);
        pnd_Out_Workfile = localVariables.newFieldInRecord("pnd_Out_Workfile", "#OUT-WORKFILE", FieldType.STRING, 109);

        pnd_Out_Workfile__R_Field_3 = localVariables.newGroupInRecord("pnd_Out_Workfile__R_Field_3", "REDEFINE", pnd_Out_Workfile);
        pnd_Out_Workfile_Pnd_O_Pin = pnd_Out_Workfile__R_Field_3.newFieldInGroup("pnd_Out_Workfile_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 12);
        pnd_Out_Workfile_Pnd_O_Contract_Nbr_4 = pnd_Out_Workfile__R_Field_3.newFieldInGroup("pnd_Out_Workfile_Pnd_O_Contract_Nbr_4", "#O-CONTRACT-NBR-4", 
            FieldType.STRING, 4);
        pnd_Out_Workfile_Pnd_O_Fed_St_Lo_Rate = pnd_Out_Workfile__R_Field_3.newFieldInGroup("pnd_Out_Workfile_Pnd_O_Fed_St_Lo_Rate", "#O-FED-ST-LO-RATE", 
            FieldType.STRING, 93);

        pnd_Out_Workfile__R_Field_4 = pnd_Out_Workfile__R_Field_3.newGroupInGroup("pnd_Out_Workfile__R_Field_4", "REDEFINE", pnd_Out_Workfile_Pnd_O_Fed_St_Lo_Rate);

        pnd_Out_Workfile_Pnd_O_Rate_Tab = pnd_Out_Workfile__R_Field_4.newGroupArrayInGroup("pnd_Out_Workfile_Pnd_O_Rate_Tab", "#O-RATE-TAB", new DbsArrayController(1, 
            3));
        pnd_Out_Workfile_Pnd_O_Elc_Type = pnd_Out_Workfile_Pnd_O_Rate_Tab.newFieldInGroup("pnd_Out_Workfile_Pnd_O_Elc_Type", "#O-ELC-TYPE", FieldType.STRING, 
            7);
        pnd_Out_Workfile_Pnd_O_New_Withholding_Rate = pnd_Out_Workfile_Pnd_O_Rate_Tab.newFieldInGroup("pnd_Out_Workfile_Pnd_O_New_Withholding_Rate", "#O-NEW-WITHHOLDING-RATE", 
            FieldType.STRING, 12);
        pnd_Out_Workfile_Pnd_O_Old_Withholding_Rate = pnd_Out_Workfile_Pnd_O_Rate_Tab.newFieldInGroup("pnd_Out_Workfile_Pnd_O_Old_Withholding_Rate", "#O-OLD-WITHHOLDING-RATE", 
            FieldType.STRING, 12);

        pnd_Out_Workfile__R_Field_5 = localVariables.newGroupInRecord("pnd_Out_Workfile__R_Field_5", "REDEFINE", pnd_Out_Workfile);
        pnd_Out_Workfile_Pnd_Out_Record = pnd_Out_Workfile__R_Field_5.newFieldInGroup("pnd_Out_Workfile_Pnd_Out_Record", "#OUT-RECORD", FieldType.STRING, 
            108);
        pnd_Out_Workfile_Pnd_Out_Fill = pnd_Out_Workfile__R_Field_5.newFieldInGroup("pnd_Out_Workfile_Pnd_Out_Fill", "#OUT-FILL", FieldType.STRING, 1);
        pnd_Sv_Area = localVariables.newFieldInRecord("pnd_Sv_Area", "#SV-AREA", FieldType.STRING, 11);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Write = localVariables.newFieldInRecord("pnd_Write", "#WRITE", FieldType.BOOLEAN, 1);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Wrt_Cnt = localVariables.newFieldInRecord("pnd_Wrt_Cnt", "#WRT-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Write.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp9110() throws Exception
    {
        super("Txwp9110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 01 #IN-WORKFILE
        while (condition(getWorkFiles().read(1, pnd_In_Workfile)))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            //*  AT END OF DATA
            //*    WRITE WORK FILE 2  #OUT-WORKFILE
            //*    ESCAPE BOTTOM
            //*  END-ENDDATA
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
                sub_Init_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_First.setValue(false);                                                                                                                                //Natural: ASSIGN #FIRST := FALSE
                pnd_Sv_Area.setValue(pnd_In_Workfile_Pnd_Pin_Cont);                                                                                                       //Natural: ASSIGN #SV-AREA := #PIN-CONT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sv_Area.equals(pnd_In_Workfile_Pnd_Pin_Cont)))                                                                                              //Natural: IF #SV-AREA = #PIN-CONT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  102014
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-VALID-TO-WRITE
                sub_Check_If_Valid_To_Write();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  102014
                //*  102014
                if (condition(pnd_Write.getBoolean()))                                                                                                                    //Natural: IF #WRITE
                {
                    pnd_Out_Workfile_Pnd_Out_Fill.setValue(",");                                                                                                          //Natural: ASSIGN #OUT-FILL := ','
                    getWorkFiles().write(2, false, pnd_Out_Workfile);                                                                                                     //Natural: WRITE WORK FILE 2 #OUT-WORKFILE
                    pnd_Wrt_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WRT-CNT
                    //*  102014
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
                sub_Init_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet93 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE #ELC-TYPE;//Natural: VALUE 'F'
            if (condition((pnd_In_Workfile_Pnd_Elc_Type.equals("F"))))
            {
                decideConditionsMet93++;
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I := 1
                pnd_Out_Workfile_Pnd_O_Elc_Type.getValue(pnd_I).setValue("Federal");                                                                                      //Natural: ASSIGN #O-ELC-TYPE ( #I ) := 'Federal'
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_In_Workfile_Pnd_Elc_Type.equals("S"))))
            {
                decideConditionsMet93++;
                pnd_I.setValue(2);                                                                                                                                        //Natural: ASSIGN #I := 2
                pnd_Out_Workfile_Pnd_O_Elc_Type.getValue(pnd_I).setValue("State  ");                                                                                      //Natural: ASSIGN #O-ELC-TYPE ( #I ) := 'State  '
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_In_Workfile_Pnd_Elc_Type.equals("T"))))
            {
                decideConditionsMet93++;
                pnd_I.setValue(3);                                                                                                                                        //Natural: ASSIGN #I := 3
                pnd_Out_Workfile_Pnd_O_Elc_Type.getValue(pnd_I).setValue("Local  ");                                                                                      //Natural: ASSIGN #O-ELC-TYPE ( #I ) := 'Local  '
                //*      #WRITE := TRUE
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet93 > 0))
            {
                pnd_Out_Workfile_Pnd_O_Old_Withholding_Rate.getValue(pnd_I).setValue(pnd_In_Workfile_Pnd_Old_Withholding_Rate);                                           //Natural: MOVE #OLD-WITHHOLDING-RATE TO #O-OLD-WITHHOLDING-RATE ( #I )
                pnd_Out_Workfile_Pnd_O_New_Withholding_Rate.getValue(pnd_I).setValue(pnd_In_Workfile_Pnd_New_Withholding_Rate);                                           //Natural: MOVE #NEW-WITHHOLDING-RATE TO #O-NEW-WITHHOLDING-RATE ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Sv_Area.setValue(pnd_In_Workfile_Pnd_Pin_Cont);                                                                                                           //Natural: ASSIGN #SV-AREA := #PIN-CONT
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Out_Workfile.notEquals(" ")))                                                                                                                   //Natural: IF #OUT-WORKFILE NE ' '
        {
            //*  102014
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-VALID-TO-WRITE
            sub_Check_If_Valid_To_Write();
            if (condition(Global.isEscape())) {return;}
            //*  102014
            //*  102014
            if (condition(pnd_Write.getBoolean()))                                                                                                                        //Natural: IF #WRITE
            {
                pnd_Out_Workfile_Pnd_Out_Fill.setValue(",");                                                                                                              //Natural: ASSIGN #OUT-FILL := ','
                getWorkFiles().write(2, false, pnd_Out_Workfile);                                                                                                         //Natural: WRITE WORK FILE 2 #OUT-WORKFILE
                pnd_Wrt_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WRT-CNT
                //*  102014
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, new TabSetting(5),"TOTAL RECORDS READ  ",new TabSetting(45),pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZZ,Z99"));                           //Natural: WRITE 5T 'TOTAL RECORDS READ  ' 45T #READ-CNT ( EM = ZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(5),"TOTAL RECORDS WRITTEN",new TabSetting(45),pnd_Wrt_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZZ,Z99"));                           //Natural: WRITE 5T 'TOTAL RECORDS WRITTEN' 45T #WRT-CNT ( EM = ZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        //*  102014 START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-VALID-TO-WRITE
        //* ***********************************************************************
        //*  102014 END
        //*  *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //*  ********************************
    }
    private void sub_Check_If_Valid_To_Write() throws Exception                                                                                                           //Natural: CHECK-IF-VALID-TO-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Write.setValue(true);                                                                                                                                         //Natural: ASSIGN #WRITE := TRUE
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            //*  DON't write
            if (condition(pnd_Out_Workfile_Pnd_O_New_Withholding_Rate.getValue(pnd_I).equals("Special") || pnd_Out_Workfile_Pnd_O_Old_Withholding_Rate.getValue(pnd_I).equals("Special"))) //Natural: IF #O-NEW-WITHHOLDING-RATE ( #I ) = 'Special' OR #O-OLD-WITHHOLDING-RATE ( #I ) = 'Special'
            {
                pnd_Write.setValue(false);                                                                                                                                //Natural: ASSIGN #WRITE := FALSE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Write.setValue(false);                                                                                                                                        //Natural: ASSIGN #WRITE := FALSE
        pnd_Out_Workfile.reset();                                                                                                                                         //Natural: RESET #OUT-WORKFILE
        //*  MOVE #PIN-CONT TO SUBSTR(#OUT-WORKFILE,1,11)
        //*  PIN-EXPANSION
        setValueToSubstring(pnd_In_Workfile_Pnd_Pin_Cont,pnd_Out_Workfile,1,16);                                                                                          //Natural: MOVE #PIN-CONT TO SUBSTR ( #OUT-WORKFILE,1,16 )
        //*  MOVE RIGHT '0.0%' TO #O-OLD-WITHHOLDING-RATE (1:3)
        //*  MOVE RIGHT '0.0%' TO #O-NEW-WITHHOLDING-RATE (1:3)
        //*  INIT-RTN
    }

    //
}
