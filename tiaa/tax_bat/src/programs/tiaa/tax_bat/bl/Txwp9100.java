/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:47 PM
**        * FROM NATURAL PROGRAM : Txwp9100
************************************************************
**        * FILE NAME            : Txwp9100.java
**        * CLASS NAME           : Txwp9100
**        * INSTANCE NAME        : Txwp9100
************************************************************
************************************************************************
****     TWR ELECTION - ADD INVERTED LU-TS FIELD INVRSE-UP-TS
************************************************************************
* PROGRAM   : TXWP9100
* SYSTEM    : TAX ELECTION
* TITLE     : TWR ELECTION INVERTED LU-TS
* GENERATED :
* FUNCTION  : PROGRAM TO POPULATE INVRSE-UP-TS FOR RECORDS ALREADY
*           : CREATED IN TWR-ELECTION.
*           :
* HISTORY   :
* 11/22/2014: F.ENDAYA    NEW
* 10/30/2014: O. SOTTO    CHANGES TO SELECTION MARKED BY 102014.
* 11/06/2014: F.ENDAYA    INCLUDE 'No Default option', ELC-TYPE='N'.
* TAG       : 110614
* 06/18/2015: FENDAYA COR AND NAS SUNSET. FE201506
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp9100 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl0600 ldaTwrl0600;
    private PdaMdma100 pdaMdma100;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_twr_Election;
    private DbsField twr_Election_Tin;
    private DbsField twr_Election_Tin_Type;
    private DbsField twr_Election_Contract_Nbr;
    private DbsField twr_Election_Payee_Cde;
    private DbsField twr_Election_Active_Ind;
    private DbsField twr_Election_Elc_Cde;
    private DbsField twr_Election_Elc_Option;
    private DbsField twr_Election_Elc_Type;
    private DbsField twr_Election_Create_User;
    private DbsField twr_Election_Lu_User;
    private DbsField twr_Election_Flat_Amt;
    private DbsField twr_Election_Fix_Pct;
    private DbsField twr_Election_Lu_Ts;
    private DbsField twr_Election_Invrse_Up_Ts;
    private DbsField twr_Election_Invrse_Effective_Ts;

    private DataAccessProgramView vw_elect_View2;
    private DbsField elect_View2_Tin;
    private DbsField elect_View2_Tin_Type;
    private DbsField elect_View2_Contract_Nbr;
    private DbsField elect_View2_Payee_Cde;
    private DbsField elect_View2_Active_Ind;
    private DbsField elect_View2_Elc_Cde;
    private DbsField elect_View2_Elc_Type;
    private DbsField elect_View2_Elc_Option;
    private DbsField elect_View2_Flat_Amt;
    private DbsField elect_View2_Fix_Pct;
    private DbsField elect_View2_Lu_Ts;
    private DbsField elect_View2_Invrse_Up_Ts;
    private DbsField elect_View2_Invrse_Effective_Ts;
    private DbsField pnd_Pnd_Lu_Ts_Date;

    private DbsGroup work_Area;
    private DbsField work_Area_Pnd_Work_Yyyymmdd_A;

    private DbsGroup work_Area__R_Field_1;
    private DbsField work_Area_Pnd_Work_Yyyymmdd;
    private DbsField work_Area_Pnd_Work_Inv;
    private DbsField work_Area_Pnd_Work_Inv_Low;

    private DbsGroup work_Area__R_Field_2;
    private DbsField work_Area_Pnd_Work_Inv_Low_A;
    private DbsField work_Area_Pnd_Work_Inv_High;

    private DbsGroup work_Area__R_Field_3;
    private DbsField work_Area_Pnd_Work_Inv_High_A;
    private DbsField work_Area_Pnd_Date_X;

    private DbsGroup work_Area__R_Field_4;
    private DbsField work_Area_Pnd_Date_N;
    private DbsField work_Area_Pnd_Inv_Lu_N;
    private DbsField work_Area_Pnd_Wk_Option;
    private DbsField work_Area_Pnd_Wk_Pct;

    private DbsGroup work_Area__R_Field_5;
    private DbsField work_Area_Pnd_Wk_Pct_A;
    private DbsField work_Area_Pnd_Wk_Rate;
    private DbsField work_Area_Pnd_Wk_Inp_Amt;

    private DbsGroup work_Area__R_Field_6;
    private DbsField work_Area_Pnd_Wk_Inp_Amt_A;
    private DbsField work_Area_Pnd_Wk_Amt;
    private DbsField pnd_Super2_Key;
    private DbsField pnd_Match;
    private DbsField pnd_First_Time;
    private DbsField pnd_Old_Rate0;
    private DbsField pnd_Old_Amt0;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Wrt_Cnt;
    private DbsField pnd_Sv_Lu_Ts;
    private DbsField pnd_In_Lu_Ts;
    private DbsField pnd_New_Is_Spcl;
    private DbsField pnd_Old_Is_Spcl;
    private DbsField pnd_New_Is_Def;
    private DbsField pnd_Old_Is_Def;
    private DbsField pnd_New_Is_Nodef;
    private DbsField pnd_Old_Is_Nodef;
    private DbsField pnd_Output_Workfile;

    private DbsGroup pnd_Output_Workfile__R_Field_7;
    private DbsField pnd_Output_Workfile_Pnd_Pin;
    private DbsField pnd_Output_Workfile_Pnd_Contract_Nbr_4;
    private DbsField pnd_Output_Workfile_Pnd_Old_Withholding_Rate;
    private DbsField pnd_Output_Workfile_Pnd_New_Withholding_Rate;
    private DbsField pnd_Output_Workfile_Pnd_Elc_Type;
    private DbsField pnd_Output_Workfile_Pnd_Filler;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        localVariables = new DbsRecord();
        pdaMdma100 = new PdaMdma100(localVariables);
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_twr_Election = new DataAccessProgramView(new NameInfo("vw_twr_Election", "TWR-ELECTION"), "TWR_ELECTION", "TWR_ELECTION");
        twr_Election_Tin = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        twr_Election_Tin.setDdmHeader("TAX/ID/NUMBER");
        twr_Election_Tin_Type = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        twr_Election_Tin_Type.setDdmHeader("TIN/TYPE");
        twr_Election_Contract_Nbr = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        twr_Election_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        twr_Election_Payee_Cde = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PAYEE_CDE");
        twr_Election_Payee_Cde.setDdmHeader("PAYEE/CODE");
        twr_Election_Active_Ind = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        twr_Election_Active_Ind.setDdmHeader("ACT/IND");
        twr_Election_Elc_Cde = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ELC_CDE");
        twr_Election_Elc_Cde.setDdmHeader("ELC/CDE");
        twr_Election_Elc_Option = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Elc_Option", "ELC-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_OPTION");
        twr_Election_Elc_Option.setDdmHeader("ELC/OPT");
        twr_Election_Elc_Type = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_TYPE");
        twr_Election_Elc_Type.setDdmHeader("ELC/TYP");
        twr_Election_Create_User = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Create_User", "CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CREATE_USER");
        twr_Election_Create_User.setDdmHeader("CREATE/USER/ID");
        twr_Election_Lu_User = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LU_USER");
        twr_Election_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        twr_Election_Flat_Amt = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Flat_Amt", "FLAT-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "FLAT_AMT");
        twr_Election_Flat_Amt.setDdmHeader("FLAT/AMOUNT");
        twr_Election_Fix_Pct = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Fix_Pct", "FIX-PCT", FieldType.PACKED_DECIMAL, 5, 2, RepeatingFieldStrategy.None, 
            "FIX_PCT");
        twr_Election_Fix_Pct.setDdmHeader("FIXED/PER-/CENT");
        twr_Election_Lu_Ts = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        twr_Election_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        twr_Election_Invrse_Up_Ts = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Invrse_Up_Ts", "INVRSE-UP-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_UP_TS");
        twr_Election_Invrse_Effective_Ts = vw_twr_Election.getRecord().newFieldInGroup("twr_Election_Invrse_Effective_Ts", "INVRSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_EFFECTIVE_TS");
        twr_Election_Invrse_Effective_Ts.setDdmHeader("INVERSE/EFFECTIVE/TIMESTAMP");
        registerRecord(vw_twr_Election);

        vw_elect_View2 = new DataAccessProgramView(new NameInfo("vw_elect_View2", "ELECT-VIEW2"), "TWR_ELECTION", "TWR_ELECTION");
        elect_View2_Tin = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        elect_View2_Tin.setDdmHeader("TAX/ID/NUMBER");
        elect_View2_Tin_Type = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        elect_View2_Tin_Type.setDdmHeader("TIN/TYPE");
        elect_View2_Contract_Nbr = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        elect_View2_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        elect_View2_Payee_Cde = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PAYEE_CDE");
        elect_View2_Payee_Cde.setDdmHeader("PAYEE/CODE");
        elect_View2_Active_Ind = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        elect_View2_Active_Ind.setDdmHeader("ACT/IND");
        elect_View2_Elc_Cde = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ELC_CDE");
        elect_View2_Elc_Cde.setDdmHeader("ELC/CDE");
        elect_View2_Elc_Type = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_TYPE");
        elect_View2_Elc_Type.setDdmHeader("ELC/TYP");
        elect_View2_Elc_Option = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Elc_Option", "ELC-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_OPTION");
        elect_View2_Elc_Option.setDdmHeader("ELC/OPT");
        elect_View2_Flat_Amt = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Flat_Amt", "FLAT-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "FLAT_AMT");
        elect_View2_Flat_Amt.setDdmHeader("FLAT/AMOUNT");
        elect_View2_Fix_Pct = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Fix_Pct", "FIX-PCT", FieldType.PACKED_DECIMAL, 5, 2, RepeatingFieldStrategy.None, 
            "FIX_PCT");
        elect_View2_Fix_Pct.setDdmHeader("FIXED/PER-/CENT");
        elect_View2_Lu_Ts = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        elect_View2_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        elect_View2_Invrse_Up_Ts = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Invrse_Up_Ts", "INVRSE-UP-TS", FieldType.PACKED_DECIMAL, 13, 
            RepeatingFieldStrategy.None, "INVRSE_UP_TS");
        elect_View2_Invrse_Effective_Ts = vw_elect_View2.getRecord().newFieldInGroup("elect_View2_Invrse_Effective_Ts", "INVRSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_EFFECTIVE_TS");
        elect_View2_Invrse_Effective_Ts.setDdmHeader("INVERSE/EFFECTIVE/TIMESTAMP");
        registerRecord(vw_elect_View2);

        pnd_Pnd_Lu_Ts_Date = localVariables.newFieldInRecord("pnd_Pnd_Lu_Ts_Date", "##LU-TS-DATE", FieldType.STRING, 8);

        work_Area = localVariables.newGroupInRecord("work_Area", "WORK-AREA");
        work_Area_Pnd_Work_Yyyymmdd_A = work_Area.newFieldInGroup("work_Area_Pnd_Work_Yyyymmdd_A", "#WORK-YYYYMMDD-A", FieldType.STRING, 8);

        work_Area__R_Field_1 = work_Area.newGroupInGroup("work_Area__R_Field_1", "REDEFINE", work_Area_Pnd_Work_Yyyymmdd_A);
        work_Area_Pnd_Work_Yyyymmdd = work_Area__R_Field_1.newFieldInGroup("work_Area_Pnd_Work_Yyyymmdd", "#WORK-YYYYMMDD", FieldType.NUMERIC, 8);
        work_Area_Pnd_Work_Inv = work_Area.newFieldInGroup("work_Area_Pnd_Work_Inv", "#WORK-INV", FieldType.NUMERIC, 12);
        work_Area_Pnd_Work_Inv_Low = work_Area.newFieldInGroup("work_Area_Pnd_Work_Inv_Low", "#WORK-INV-LOW", FieldType.NUMERIC, 12);

        work_Area__R_Field_2 = work_Area.newGroupInGroup("work_Area__R_Field_2", "REDEFINE", work_Area_Pnd_Work_Inv_Low);
        work_Area_Pnd_Work_Inv_Low_A = work_Area__R_Field_2.newFieldInGroup("work_Area_Pnd_Work_Inv_Low_A", "#WORK-INV-LOW-A", FieldType.STRING, 12);
        work_Area_Pnd_Work_Inv_High = work_Area.newFieldInGroup("work_Area_Pnd_Work_Inv_High", "#WORK-INV-HIGH", FieldType.NUMERIC, 12);

        work_Area__R_Field_3 = work_Area.newGroupInGroup("work_Area__R_Field_3", "REDEFINE", work_Area_Pnd_Work_Inv_High);
        work_Area_Pnd_Work_Inv_High_A = work_Area__R_Field_3.newFieldInGroup("work_Area_Pnd_Work_Inv_High_A", "#WORK-INV-HIGH-A", FieldType.STRING, 12);
        work_Area_Pnd_Date_X = work_Area.newFieldInGroup("work_Area_Pnd_Date_X", "#DATE-X", FieldType.STRING, 8);

        work_Area__R_Field_4 = work_Area.newGroupInGroup("work_Area__R_Field_4", "REDEFINE", work_Area_Pnd_Date_X);
        work_Area_Pnd_Date_N = work_Area__R_Field_4.newFieldInGroup("work_Area_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        work_Area_Pnd_Inv_Lu_N = work_Area.newFieldInGroup("work_Area_Pnd_Inv_Lu_N", "#INV-LU-N", FieldType.NUMERIC, 8);
        work_Area_Pnd_Wk_Option = work_Area.newFieldInGroup("work_Area_Pnd_Wk_Option", "#WK-OPTION", FieldType.STRING, 1);
        work_Area_Pnd_Wk_Pct = work_Area.newFieldInGroup("work_Area_Pnd_Wk_Pct", "#WK-PCT", FieldType.NUMERIC, 5, 2);

        work_Area__R_Field_5 = work_Area.newGroupInGroup("work_Area__R_Field_5", "REDEFINE", work_Area_Pnd_Wk_Pct);
        work_Area_Pnd_Wk_Pct_A = work_Area__R_Field_5.newFieldInGroup("work_Area_Pnd_Wk_Pct_A", "#WK-PCT-A", FieldType.STRING, 5);
        work_Area_Pnd_Wk_Rate = work_Area.newFieldInGroup("work_Area_Pnd_Wk_Rate", "#WK-RATE", FieldType.STRING, 12);
        work_Area_Pnd_Wk_Inp_Amt = work_Area.newFieldInGroup("work_Area_Pnd_Wk_Inp_Amt", "#WK-INP-AMT", FieldType.NUMERIC, 9, 2);

        work_Area__R_Field_6 = work_Area.newGroupInGroup("work_Area__R_Field_6", "REDEFINE", work_Area_Pnd_Wk_Inp_Amt);
        work_Area_Pnd_Wk_Inp_Amt_A = work_Area__R_Field_6.newFieldInGroup("work_Area_Pnd_Wk_Inp_Amt_A", "#WK-INP-AMT-A", FieldType.STRING, 9);
        work_Area_Pnd_Wk_Amt = work_Area.newFieldInGroup("work_Area_Pnd_Wk_Amt", "#WK-AMT", FieldType.STRING, 12);
        pnd_Super2_Key = localVariables.newFieldInRecord("pnd_Super2_Key", "#SUPER2-KEY", FieldType.STRING, 37);
        pnd_Match = localVariables.newFieldInRecord("pnd_Match", "#MATCH", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Old_Rate0 = localVariables.newFieldInRecord("pnd_Old_Rate0", "#OLD-RATE0", FieldType.BOOLEAN, 1);
        pnd_Old_Amt0 = localVariables.newFieldInRecord("pnd_Old_Amt0", "#OLD-AMT0", FieldType.BOOLEAN, 1);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Wrt_Cnt = localVariables.newFieldInRecord("pnd_Wrt_Cnt", "#WRT-CNT", FieldType.PACKED_DECIMAL, 11);
        pnd_Sv_Lu_Ts = localVariables.newFieldInRecord("pnd_Sv_Lu_Ts", "#SV-LU-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_In_Lu_Ts = localVariables.newFieldInRecord("pnd_In_Lu_Ts", "#IN-LU-TS", FieldType.STRING, 13);
        pnd_New_Is_Spcl = localVariables.newFieldInRecord("pnd_New_Is_Spcl", "#NEW-IS-SPCL", FieldType.BOOLEAN, 1);
        pnd_Old_Is_Spcl = localVariables.newFieldInRecord("pnd_Old_Is_Spcl", "#OLD-IS-SPCL", FieldType.BOOLEAN, 1);
        pnd_New_Is_Def = localVariables.newFieldInRecord("pnd_New_Is_Def", "#NEW-IS-DEF", FieldType.BOOLEAN, 1);
        pnd_Old_Is_Def = localVariables.newFieldInRecord("pnd_Old_Is_Def", "#OLD-IS-DEF", FieldType.BOOLEAN, 1);
        pnd_New_Is_Nodef = localVariables.newFieldInRecord("pnd_New_Is_Nodef", "#NEW-IS-NODEF", FieldType.BOOLEAN, 1);
        pnd_Old_Is_Nodef = localVariables.newFieldInRecord("pnd_Old_Is_Nodef", "#OLD-IS-NODEF", FieldType.BOOLEAN, 1);
        pnd_Output_Workfile = localVariables.newFieldInRecord("pnd_Output_Workfile", "#OUTPUT-WORKFILE", FieldType.STRING, 80);

        pnd_Output_Workfile__R_Field_7 = localVariables.newGroupInRecord("pnd_Output_Workfile__R_Field_7", "REDEFINE", pnd_Output_Workfile);
        pnd_Output_Workfile_Pnd_Pin = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Output_Workfile_Pnd_Contract_Nbr_4 = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_Contract_Nbr_4", "#CONTRACT-NBR-4", 
            FieldType.STRING, 4);
        pnd_Output_Workfile_Pnd_Old_Withholding_Rate = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_Old_Withholding_Rate", 
            "#OLD-WITHHOLDING-RATE", FieldType.STRING, 12);
        pnd_Output_Workfile_Pnd_New_Withholding_Rate = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_New_Withholding_Rate", 
            "#NEW-WITHHOLDING-RATE", FieldType.STRING, 12);
        pnd_Output_Workfile_Pnd_Elc_Type = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_Elc_Type", "#ELC-TYPE", FieldType.STRING, 
            1);
        pnd_Output_Workfile_Pnd_Filler = pnd_Output_Workfile__R_Field_7.newFieldInGroup("pnd_Output_Workfile_Pnd_Filler", "#FILLER", FieldType.STRING, 
            39);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_twr_Election.reset();
        vw_elect_View2.reset();

        ldaTwrl0600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp9100() throws Exception
    {
        super("Txwp9100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 01 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(1, "***",new TabSetting(25),"CONTROL RECORD IS EMPTY",new TabSetting(77),"***");                                                           //Natural: WRITE ( 1 ) '***' 25T 'CONTROL RECORD IS EMPTY' 77T '***'
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        work_Area_Pnd_Work_Yyyymmdd_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                  //Natural: MOVE #TWRP0600-FROM-CCYYMMDD TO #WORK-YYYYMMDD-A
        setValueToSubstring(work_Area_Pnd_Work_Yyyymmdd_A,work_Area_Pnd_Work_Inv_Low_A,1,8);                                                                              //Natural: MOVE #WORK-YYYYMMDD-A TO SUBSTR ( #WORK-INV-LOW-A,1,8 )
        setValueToSubstring("9999",work_Area_Pnd_Work_Inv_Low_A,9,4);                                                                                                     //Natural: MOVE '9999' TO SUBSTR ( #WORK-INV-LOW-A,9,4 )
        work_Area_Pnd_Work_Inv.compute(new ComputeParameters(false, work_Area_Pnd_Work_Inv), new DbsDecimal("1000000000000").subtract(work_Area_Pnd_Work_Inv_Low));       //Natural: COMPUTE #WORK-INV = 1000000000000 - #WORK-INV-LOW
        work_Area_Pnd_Work_Inv_Low.setValue(work_Area_Pnd_Work_Inv);                                                                                                      //Natural: ASSIGN #WORK-INV-LOW := #WORK-INV-HIGH := #WORK-INV
        work_Area_Pnd_Work_Inv_High.setValue(work_Area_Pnd_Work_Inv);
        setValueToSubstring("0000",work_Area_Pnd_Work_Inv_Low_A,9,4);                                                                                                     //Natural: MOVE '0000' TO SUBSTR ( #WORK-INV-LOW-A,9,4 )
        setValueToSubstring("9999",work_Area_Pnd_Work_Inv_High_A,9,4);                                                                                                    //Natural: MOVE '9999' TO SUBSTR ( #WORK-INV-HIGH-A,9,4 )
        vw_twr_Election.startDatabaseRead                                                                                                                                 //Natural: READ TWR-ELECTION WITH TWR-ELECTION.INVRSE-UP-TS EQ #WORK-INV-LOW
        (
        "R1",
        new Wc[] { new Wc("INVRSE_UP_TS", ">=", work_Area_Pnd_Work_Inv_Low, WcType.BY) },
        new Oc[] { new Oc("INVRSE_UP_TS", "ASC") }
        );
        R1:
        while (condition(vw_twr_Election.readNextRow("R1")))
        {
            if (condition(twr_Election_Invrse_Up_Ts.greater(work_Area_Pnd_Work_Inv_High)))                                                                                //Natural: IF TWR-ELECTION.INVRSE-UP-TS GT #WORK-INV-HIGH
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            if (condition(twr_Election_Create_User.equals("PAN2005D") || twr_Election_Create_User.equals("P1280TWM") || twr_Election_Lu_User.equals("PAN2005D")           //Natural: IF ( CREATE-USER = 'PAN2005D' OR = 'P1280TWM' ) OR ( LU-USER = 'PAN2005D' OR = 'P1280TWM' )
                || twr_Election_Lu_User.equals("P1280TWM")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  102014 START
            //*  110614
            //*  110614
            pnd_New_Is_Def.reset();                                                                                                                                       //Natural: RESET #NEW-IS-DEF #OLD-IS-DEF #NEW-IS-SPCL #OLD-IS-SPCL #NEW-IS-NODEF #OLD-IS-NODEF
            pnd_Old_Is_Def.reset();
            pnd_New_Is_Spcl.reset();
            pnd_Old_Is_Spcl.reset();
            pnd_New_Is_Nodef.reset();
            pnd_Old_Is_Nodef.reset();
            if (condition(twr_Election_Flat_Amt.equals(getZero()) && twr_Election_Fix_Pct.equals(getZero())))                                                             //Natural: IF TWR-ELECTION.FLAT-AMT = 0 AND TWR-ELECTION.FIX-PCT = 0
            {
                short decideConditionsMet442 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWR-ELECTION.ELC-OPTION EQ 'D'
                if (condition(twr_Election_Elc_Option.equals("D")))
                {
                    decideConditionsMet442++;
                    pnd_New_Is_Def.setValue(true);                                                                                                                        //Natural: ASSIGN #NEW-IS-DEF := TRUE
                }                                                                                                                                                         //Natural: WHEN TWR-ELECTION.ELC-OPTION EQ 'Y'
                else if (condition(twr_Election_Elc_Option.equals("Y")))
                {
                    decideConditionsMet442++;
                    pnd_New_Is_Spcl.setValue(true);                                                                                                                       //Natural: ASSIGN #NEW-IS-SPCL := TRUE
                }                                                                                                                                                         //Natural: WHEN TWR-ELECTION.ELC-OPTION EQ 'N'
                else if (condition(twr_Election_Elc_Option.equals("N")))
                {
                    decideConditionsMet442++;
                    pnd_New_Is_Nodef.setValue(true);                                                                                                                      //Natural: ASSIGN #NEW-IS-NODEF := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  102014 END
            //*  MOVE EDITED R1.LU-TS (EM=YYYYMMDDHHII)     TO #IN-LU-TS
            //*  102014
            pnd_Super2_Key.reset();                                                                                                                                       //Natural: RESET #SUPER2-KEY #WK-RATE #WK-OPTION #WK-PCT #WK-INP-AMT #SV-LU-TS
            work_Area_Pnd_Wk_Rate.reset();
            work_Area_Pnd_Wk_Option.reset();
            work_Area_Pnd_Wk_Pct.reset();
            work_Area_Pnd_Wk_Inp_Amt.reset();
            pnd_Sv_Lu_Ts.reset();
            pnd_Match.setValue(false);                                                                                                                                    //Natural: ASSIGN #MATCH := FALSE
            pnd_First_Time.setValue(true);                                                                                                                                //Natural: ASSIGN #FIRST-TIME := TRUE
            //*         TIN(1-10)
            //*         TIN-TYPE(1-1)
            //*         CONTRACT-NBR(1-10)
            //*         PAYEE-CDE(1-2)
            setValueToSubstring(twr_Election_Tin,pnd_Super2_Key,1,10);                                                                                                    //Natural: MOVE TWR-ELECTION.TIN TO SUBSTR ( #SUPER2-KEY,01,10 )
            setValueToSubstring(twr_Election_Tin_Type,pnd_Super2_Key,11,1);                                                                                               //Natural: MOVE TWR-ELECTION.TIN-TYPE TO SUBSTR ( #SUPER2-KEY,11,01 )
            setValueToSubstring(twr_Election_Contract_Nbr,pnd_Super2_Key,12,10);                                                                                          //Natural: MOVE TWR-ELECTION.CONTRACT-NBR TO SUBSTR ( #SUPER2-KEY,12,10 )
            setValueToSubstring(twr_Election_Payee_Cde,pnd_Super2_Key,23,2);                                                                                              //Natural: MOVE TWR-ELECTION.PAYEE-CDE TO SUBSTR ( #SUPER2-KEY,23,02 )
            vw_elect_View2.startDatabaseRead                                                                                                                              //Natural: READ ELECT-VIEW2 WITH SUPER-2 EQ #SUPER2-KEY
            (
            "R2",
            new Wc[] { new Wc("SUPER_2", ">=", pnd_Super2_Key.getBinary(), WcType.BY) },
            new Oc[] { new Oc("SUPER_2", "ASC") }
            );
            R2:
            while (condition(vw_elect_View2.readNextRow("R2")))
            {
                if (condition(twr_Election_Tin.notEquals(elect_View2_Tin) || twr_Election_Tin_Type.notEquals(elect_View2_Tin_Type) || twr_Election_Contract_Nbr.notEquals(elect_View2_Contract_Nbr)  //Natural: IF TWR-ELECTION.TIN NE ELECT-VIEW2.TIN OR TWR-ELECTION.TIN-TYPE NE ELECT-VIEW2.TIN-TYPE OR TWR-ELECTION.CONTRACT-NBR NE ELECT-VIEW2.CONTRACT-NBR OR TWR-ELECTION.PAYEE-CDE NE ELECT-VIEW2.PAYEE-CDE
                    || twr_Election_Payee_Cde.notEquals(elect_View2_Payee_Cde)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(twr_Election_Elc_Type.equals(elect_View2_Elc_Type)))                                                                                        //Natural: IF TWR-ELECTION.ELC-TYPE = ELECT-VIEW2.ELC-TYPE
                {
                    if (condition(twr_Election_Lu_Ts.greater(elect_View2_Lu_Ts)))                                                                                         //Natural: IF TWR-ELECTION.LU-TS GT ELECT-VIEW2.LU-TS
                    {
                        //*  110614
                        if (condition((twr_Election_Flat_Amt.equals(elect_View2_Flat_Amt) && twr_Election_Fix_Pct.equals(elect_View2_Fix_Pct) && twr_Election_Elc_Option.equals(elect_View2_Elc_Option)))) //Natural: IF ( TWR-ELECTION.FLAT-AMT = ELECT-VIEW2.FLAT-AMT AND TWR-ELECTION.FIX-PCT = ELECT-VIEW2.FIX-PCT AND TWR-ELECTION.ELC-OPTION = ELECT-VIEW2.ELC-OPTION )
                        {
                            //*            AND NOT (#NEW-IS-SPCL OR #NEW-IS-DEF) /* 102014
                            //*  NO CHANGES IN ELECTION
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        //*  102014
                        if (condition(pnd_First_Time.getBoolean()))                                                                                                       //Natural: IF #FIRST-TIME
                        {
                            pnd_First_Time.setValue(false);                                                                                                               //Natural: ASSIGN #FIRST-TIME := FALSE
                            pnd_Match.setValue(true);                                                                                                                     //Natural: ASSIGN #MATCH := TRUE
                            pnd_Sv_Lu_Ts.setValue(elect_View2_Lu_Ts);                                                                                                     //Natural: ASSIGN #SV-LU-TS := ELECT-VIEW2.LU-TS
                            work_Area_Pnd_Wk_Option.setValue(elect_View2_Elc_Option);                                                                                     //Natural: ASSIGN #WK-OPTION := ELECT-VIEW2.ELC-OPTION
                            work_Area_Pnd_Wk_Pct.setValue(elect_View2_Fix_Pct);                                                                                           //Natural: ASSIGN #WK-PCT := ELECT-VIEW2.FIX-PCT
                            work_Area_Pnd_Wk_Inp_Amt.setValue(elect_View2_Flat_Amt);                                                                                      //Natural: ASSIGN #WK-INP-AMT := ELECT-VIEW2.FLAT-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  102014
                            if (condition(elect_View2_Lu_Ts.greater(pnd_Sv_Lu_Ts)))                                                                                       //Natural: IF ELECT-VIEW2.LU-TS GT #SV-LU-TS
                            {
                                pnd_Sv_Lu_Ts.setValue(elect_View2_Lu_Ts);                                                                                                 //Natural: ASSIGN #SV-LU-TS := ELECT-VIEW2.LU-TS
                                work_Area_Pnd_Wk_Option.setValue(elect_View2_Elc_Option);                                                                                 //Natural: ASSIGN #WK-OPTION := ELECT-VIEW2.ELC-OPTION
                                work_Area_Pnd_Wk_Pct.setValue(elect_View2_Fix_Pct);                                                                                       //Natural: ASSIGN #WK-PCT := ELECT-VIEW2.FIX-PCT
                                work_Area_Pnd_Wk_Inp_Amt.setValue(elect_View2_Flat_Amt);                                                                                  //Natural: ASSIGN #WK-INP-AMT := ELECT-VIEW2.FLAT-AMT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Match.getBoolean()))                                                                                                                        //Natural: IF #MATCH
            {
                pnd_Output_Workfile.reset();                                                                                                                              //Natural: RESET #OUTPUT-WORKFILE
                //*  102014 START
                if (condition((work_Area_Pnd_Wk_Inp_Amt.equals(getZero()) && work_Area_Pnd_Wk_Pct.equals(getZero()))))                                                    //Natural: IF ( #WK-INP-AMT = 0 AND #WK-PCT = 0 )
                {
                    //*  110614
                    //*  110614
                    short decideConditionsMet509 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WK-OPTION = 'D'
                    if (condition(work_Area_Pnd_Wk_Option.equals("D")))
                    {
                        decideConditionsMet509++;
                        pnd_Old_Is_Def.setValue(true);                                                                                                                    //Natural: ASSIGN #OLD-IS-DEF := TRUE
                    }                                                                                                                                                     //Natural: WHEN #WK-OPTION EQ 'Y'
                    else if (condition(work_Area_Pnd_Wk_Option.equals("Y")))
                    {
                        decideConditionsMet509++;
                        pnd_Old_Is_Spcl.setValue(true);                                                                                                                   //Natural: ASSIGN #OLD-IS-SPCL := TRUE
                    }                                                                                                                                                     //Natural: WHEN #WK-OPTION EQ 'N'
                    else if (condition(work_Area_Pnd_Wk_Option.equals("N")))
                    {
                        decideConditionsMet509++;
                        pnd_Old_Is_Nodef.setValue(true);                                                                                                                  //Natural: ASSIGN #OLD-IS-NODEF := TRUE
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  102014 END
                pnd_Output_Workfile_Pnd_Contract_Nbr_4.setValue(twr_Election_Contract_Nbr.getSubstring(5,4));                                                             //Natural: MOVE SUBSTR ( TWR-ELECTION.CONTRACT-NBR,5,4 ) TO #CONTRACT-NBR-4
                pnd_Output_Workfile_Pnd_Elc_Type.setValue(twr_Election_Elc_Type);                                                                                         //Natural: ASSIGN #ELC-TYPE := TWR-ELECTION.ELC-TYPE
                //*  PIN LOOKUP HERE
                //*    #TWRAPART.#FUNCTION  :=  'C'  /* FE201506 START COMMENT OUT
                //*    #TWRAPART.#TIN       :=  R1.TIN
                //*    IF R1.TIN = ' '
                //*      #TWRAPART.#TIN-TYPE  :=  R1.TIN-TYPE
                //*    ELSE
                //*      #TWRAPART.#TIN-TYPE  :=  '5'
                //*    END-IF
                //*    CALLNAT  'TWRNPART'  #TWRAPART /* FE201506 END COMMENT OUT
                //*  FE201506 START
                pdaMdma101.getPnd_Mdma101().reset();                                                                                                                      //Natural: RESET #MDMA101
                pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(twr_Election_Tin);                                                                                      //Natural: ASSIGN #MDMA101.#I-SSN-A9 := TWR-ELECTION.TIN
                DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                  //Natural: CALLNAT 'MDMN101A' USING #MDMA101
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*  FE201506 END
                //*  PIN-EXPANSION
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                              //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
                {
                    pnd_Output_Workfile_Pnd_Pin.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                      //Natural: ASSIGN #PIN := #MDMA101.#O-PIN-N12
                }                                                                                                                                                         //Natural: END-IF
                pnd_Old_Rate0.setValue(false);                                                                                                                            //Natural: ASSIGN #OLD-RATE0 := FALSE
                pnd_Old_Amt0.setValue(false);                                                                                                                             //Natural: ASSIGN #OLD-AMT0 := FALSE
                work_Area_Pnd_Wk_Rate.reset();                                                                                                                            //Natural: RESET #WK-RATE #WK-AMT
                work_Area_Pnd_Wk_Amt.reset();
                //*    IF #WK-PCT NE R1.FIX-PCT
                //*  OLD RATE
                if (condition(work_Area_Pnd_Wk_Pct.notEquals(getZero())))                                                                                                 //Natural: IF #WK-PCT NE 0
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-RATE
                    sub_Format_Rate();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue(work_Area_Pnd_Wk_Rate);                                                                         //Natural: ASSIGN #OLD-WITHHOLDING-RATE := #WK-RATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Old_Rate0.setValue(true);                                                                                                                         //Natural: ASSIGN #OLD-RATE0 := TRUE
                    if (condition(work_Area_Pnd_Wk_Inp_Amt.notEquals(getZero())))                                                                                         //Natural: IF #WK-INP-AMT NE 0
                    {
                        work_Area_Pnd_Wk_Amt.reset();                                                                                                                     //Natural: RESET #WK-AMT
                                                                                                                                                                          //Natural: PERFORM FORMAT-AMOUNT
                        sub_Format_Amount();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue(work_Area_Pnd_Wk_Amt);                                                                      //Natural: ASSIGN #OLD-WITHHOLDING-RATE := #WK-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Old_Amt0.setValue(true);                                                                                                                      //Natural: ASSIGN #OLD-AMT0 := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   NEW RATES
                work_Area_Pnd_Wk_Rate.reset();                                                                                                                            //Natural: RESET #WK-RATE #WK-AMT
                work_Area_Pnd_Wk_Amt.reset();
                work_Area_Pnd_Wk_Pct.setValue(twr_Election_Fix_Pct);                                                                                                      //Natural: MOVE TWR-ELECTION.FIX-PCT TO #WK-PCT
                work_Area_Pnd_Wk_Inp_Amt.setValue(twr_Election_Flat_Amt);                                                                                                 //Natural: MOVE TWR-ELECTION.FLAT-AMT TO #WK-INP-AMT
                if (condition(work_Area_Pnd_Wk_Pct.notEquals(getZero())))                                                                                                 //Natural: IF #WK-PCT NE 0
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-RATE
                    sub_Format_Rate();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Output_Workfile_Pnd_New_Withholding_Rate.setValue(work_Area_Pnd_Wk_Rate);                                                                         //Natural: ASSIGN #NEW-WITHHOLDING-RATE := #WK-RATE
                    if (condition(pnd_Old_Rate0.getBoolean() && pnd_Output_Workfile_Pnd_Old_Withholding_Rate.equals(" ")))                                                //Natural: IF #OLD-RATE0 AND #OLD-WITHHOLDING-RATE = ' '
                    {
                        pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue("0%");                                                                                      //Natural: MOVE '0%' TO #OLD-WITHHOLDING-RATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(work_Area_Pnd_Wk_Inp_Amt.notEquals(getZero())))                                                                                         //Natural: IF #WK-INP-AMT NE 0
                    {
                        work_Area_Pnd_Wk_Amt.reset();                                                                                                                     //Natural: RESET #WK-AMT
                                                                                                                                                                          //Natural: PERFORM FORMAT-AMOUNT
                        sub_Format_Amount();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Output_Workfile_Pnd_New_Withholding_Rate.setValue(work_Area_Pnd_Wk_Amt);                                                                      //Natural: ASSIGN #NEW-WITHHOLDING-RATE := #WK-AMT
                        if (condition(pnd_Old_Rate0.getBoolean() && pnd_Output_Workfile_Pnd_Old_Withholding_Rate.equals(" ")))                                            //Natural: IF #OLD-RATE0 AND #OLD-WITHHOLDING-RATE = ' '
                        {
                            pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue("$0.00");                                                                               //Natural: MOVE '$0.00' TO #OLD-WITHHOLDING-RATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    ELSE /* AMOUNT WAS CHANGED
                //*      PERFORM FORMAT-AMOUNT
                //*      #OLD-WITHHOLDING-RATE := #WK-AMT
                //*   NEW RATE AMOUNT
                //*      RESET #WK-AMT
                //*      MOVE R1.FLAT-AMT TO #WK-INP-AMT
                //*      PERFORM FORMAT-AMOUNT
                //*      #NEW-WITHHOLDING-RATE := #WK-AMT
                //*    END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  DO NOT WRITE NEW INFORMATION
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  102014 START
            if (condition(pnd_New_Is_Def.getBoolean()))                                                                                                                   //Natural: IF #NEW-IS-DEF
            {
                pnd_Output_Workfile_Pnd_New_Withholding_Rate.setValue("Default");                                                                                         //Natural: ASSIGN #NEW-WITHHOLDING-RATE := 'Default'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_New_Is_Spcl.getBoolean()))                                                                                                                  //Natural: IF #NEW-IS-SPCL
            {
                pnd_Output_Workfile_Pnd_New_Withholding_Rate.setValue("Special");                                                                                         //Natural: ASSIGN #NEW-WITHHOLDING-RATE := 'Special'
            }                                                                                                                                                             //Natural: END-IF
            //*  110614
            //*  110614
            if (condition(pnd_New_Is_Nodef.getBoolean()))                                                                                                                 //Natural: IF #NEW-IS-NODEF
            {
                pnd_Output_Workfile_Pnd_New_Withholding_Rate.setValue("$0.00");                                                                                           //Natural: ASSIGN #NEW-WITHHOLDING-RATE := '$0.00'
                //*  110614
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_Is_Def.getBoolean()))                                                                                                                   //Natural: IF #OLD-IS-DEF
            {
                pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue("Default");                                                                                         //Natural: ASSIGN #OLD-WITHHOLDING-RATE := 'Default'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_Is_Spcl.getBoolean()))                                                                                                                  //Natural: IF #OLD-IS-SPCL
            {
                pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue("Special");                                                                                         //Natural: ASSIGN #OLD-WITHHOLDING-RATE := 'Special'
            }                                                                                                                                                             //Natural: END-IF
            //*  110614
            //*  110614
            if (condition(pnd_Old_Is_Nodef.getBoolean()))                                                                                                                 //Natural: IF #OLD-IS-NODEF
            {
                pnd_Output_Workfile_Pnd_Old_Withholding_Rate.setValue("$0.00");                                                                                           //Natural: ASSIGN #OLD-WITHHOLDING-RATE := '$0.00'
                //*  110614
            }                                                                                                                                                             //Natural: END-IF
            //*  102014 END
            getWorkFiles().write(2, false, pnd_Output_Workfile);                                                                                                          //Natural: WRITE WORK FILE 2 #OUTPUT-WORKFILE
            pnd_Wrt_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WRT-CNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(5),"TOTAL RECORDS READ  ",new TabSetting(45),pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZZ,Z99"));                           //Natural: WRITE 5T 'TOTAL RECORDS READ  ' 45T #READ-CNT ( EM = ZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(5),"TOTAL RECORDS WRITTEN",new TabSetting(45),pnd_Wrt_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZZ,Z99"));                           //Natural: WRITE 5T 'TOTAL RECORDS WRITTEN' 45T #WRT-CNT ( EM = ZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        //*  *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-RATE
        //*  ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-AMOUNT
        //*  ********************************
        //* ***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  *************************************
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Format_Rate() throws Exception                                                                                                                       //Natural: FORMAT-RATE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(work_Area_Pnd_Wk_Pct.notEquals(getZero())))                                                                                                         //Natural: IF #WK-PCT NE 0
        {
            setValueToSubstring(work_Area_Pnd_Wk_Pct_A.getSubstring(1,3),work_Area_Pnd_Wk_Rate,1,3);                                                                      //Natural: MOVE SUBSTR ( #WK-PCT-A,1,3 ) TO SUBSTR ( #WK-RATE,1,3 )
            if (condition(work_Area_Pnd_Wk_Pct_A.getSubstring(4,2).equals("00")))                                                                                         //Natural: IF SUBSTR ( #WK-PCT-A,4,2 ) = '00'
            {
                setValueToSubstring("%",work_Area_Pnd_Wk_Rate,4,1);                                                                                                       //Natural: MOVE '%' TO SUBSTR ( #WK-RATE,4,1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring(work_Area_Pnd_Wk_Pct_A.getSubstring(4,2),work_Area_Pnd_Wk_Rate,5,2);                                                                  //Natural: MOVE SUBSTR ( #WK-PCT-A,4,2 ) TO SUBSTR ( #WK-RATE,5,2 )
                setValueToSubstring(".",work_Area_Pnd_Wk_Rate,4,1);                                                                                                       //Natural: MOVE '.' TO SUBSTR ( #WK-RATE,4,1 )
                setValueToSubstring("%",work_Area_Pnd_Wk_Rate,7,1);                                                                                                       //Natural: MOVE '%' TO SUBSTR ( #WK-RATE,7,1 )
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE LEFT #WK-PCT-A TO #WK-PCT-A
            //*  ***** 'delete leading zeros
            if (condition(work_Area_Pnd_Wk_Rate.getSubstring(1,1).equals("0")))                                                                                           //Natural: IF SUBSTR ( #WK-RATE,1,1 ) = '0'
            {
                setValueToSubstring(work_Area_Pnd_Wk_Rate.getSubstring(2,11),work_Area_Pnd_Wk_Rate,1,11);                                                                 //Natural: MOVE SUBSTR ( #WK-RATE,2,11 ) TO SUBSTR ( #WK-RATE,1,11 )
                if (condition(work_Area_Pnd_Wk_Rate.getSubstring(1,1).equals("0")))                                                                                       //Natural: IF SUBSTR ( #WK-RATE,1,1 ) = '0'
                {
                    setValueToSubstring(work_Area_Pnd_Wk_Rate.getSubstring(2,11),work_Area_Pnd_Wk_Rate,1,11);                                                             //Natural: MOVE SUBSTR ( #WK-RATE,2,11 ) TO SUBSTR ( #WK-RATE,1,11 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_Area_Pnd_Wk_Rate.setValue("0%");                                                                                                                         //Natural: MOVE '0%' TO #WK-RATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT-RATE
    }
    private void sub_Format_Amount() throws Exception                                                                                                                     //Natural: FORMAT-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(work_Area_Pnd_Wk_Inp_Amt.notEquals(getZero())))                                                                                                     //Natural: IF #WK-INP-AMT NE 0
        {
            work_Area_Pnd_Wk_Amt.setValueEdited(work_Area_Pnd_Wk_Inp_Amt,new ReportEditMask("ZZZZZZ9.99"));                                                               //Natural: MOVE EDITED #WK-INP-AMT ( EM = ZZZZZZ9.99 ) TO #WK-AMT
            work_Area_Pnd_Wk_Amt.setValue(work_Area_Pnd_Wk_Amt, MoveOption.LeftJustified);                                                                                //Natural: MOVE LEFT #WK-AMT TO #WK-AMT
            setValueToSubstring(work_Area_Pnd_Wk_Amt.getSubstring(1,11),work_Area_Pnd_Wk_Amt,2,11);                                                                       //Natural: MOVE SUBSTR ( #WK-AMT,1,11 ) TO SUBSTR ( #WK-AMT,2,11 )
            setValueToSubstring("$",work_Area_Pnd_Wk_Amt,1,1);                                                                                                            //Natural: MOVE '$' TO SUBSTR ( #WK-AMT,1,1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            work_Area_Pnd_Wk_Amt.setValue("$0.00");                                                                                                                       //Natural: MOVE '$0.00' TO #WK-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT-AMOUNT
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //
}
