/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:47 PM
**        * FROM NATURAL PROGRAM : Twrp8160
************************************************************
**        * FILE NAME            : Twrp8160.java
**        * CLASS NAME           : Twrp8160
**        * INSTANCE NAME        : Twrp8160
************************************************************
************************************************************************
* PROGRAM     :  TWRP8160
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE A TABLE OF NEW TAX YEAR REPORTING FORMS USING
*                AN OLD TAX YEAR REPORTING FORMS TABLE.
*                TABLE NUMBER 4
* DATE        :  09/15/2000
*
* HISTORY.....:
*    01/02/2013 MS SUNY CHANGES
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8160 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rr;
    private DbsField rr_Tircntl_Tbl_Nbr;
    private DbsField rr_Tircntl_Tax_Year;
    private DbsField rr_Tircntl_Seq_Nbr;

    private DbsGroup rr_Tircntl_Reporting_Tbl;
    private DbsField rr_Tircntl_Rpt_Form_Name;
    private DbsField rr_Tircntl_Future_Use_Date;
    private DbsField rr_Tircntl_Rpt_2nd_Mass_Mail_Dte;
    private DbsField rr_Tircntl_Rpt_Mass_Mail_Dte;
    private DbsField rr_Tircntl_Eff_Dte;
    private DbsField rr_Tircntl_Pr_Cntl_Start;
    private DbsField rr_Tircntl_Pr_Cntl_End;
    private DbsField rr_Tircntl_Pr_Cntl_Curr;

    private DbsGroup rr_Tircntl_Rpt_Tbl_Pe;
    private DbsField rr_Tircntl_Rpt_Dte;
    private DbsField rr_Tircntl_Rpt_Cref_Gross_Amt;
    private DbsField rr_Tircntl_Rpt_Cref_Int_Amt;
    private DbsField rr_Tircntl_Rpt_Cref_Taxable_Amt;
    private DbsField rr_Tircntl_Rpt_Cref_Ivc_Amt;
    private DbsField rr_Tircntl_Rpt_Cref_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Cref_Loc_Amt;
    private DbsField rr_Tircntl_Rpt_Cref_Loc_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Life_Gross_Amt;
    private DbsField rr_Tircntl_Rpt_Life_Int_Amt;
    private DbsField rr_Tircntl_Rpt_Life_Taxable_Amt;
    private DbsField rr_Tircntl_Rpt_Life_Ivc_Amt;
    private DbsField rr_Tircntl_Rpt_Life_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Life_Loc_Amt;
    private DbsField rr_Tircntl_Rpt_Life_Loc_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Tiaa_Gross_Amt;
    private DbsField rr_Tircntl_Rpt_Tiaa_Int_Amt;
    private DbsField rr_Tircntl_Rpt_Tiaa_Taxable_Amt;
    private DbsField rr_Tircntl_Rpt_Tiaa_Ivc_Amt;
    private DbsField rr_Tircntl_Rpt_Tiaa_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Tiaa_Loc_Amt;
    private DbsField rr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld;
    private DbsField rr_Tircntl_Rpt_Amount_1;
    private DbsField rr_Tircntl_Rpt_Amount_2;

    private DataAccessProgramView vw_ur;
    private DbsField ur_Tircntl_Tbl_Nbr;
    private DbsField ur_Tircntl_Tax_Year;
    private DbsField ur_Tircntl_Seq_Nbr;

    private DbsGroup ur_Tircntl_Reporting_Tbl;
    private DbsField ur_Tircntl_Rpt_Form_Name;
    private DbsField ur_Tircntl_Future_Use_Date;
    private DbsField ur_Tircntl_Rpt_2nd_Mass_Mail_Dte;
    private DbsField ur_Tircntl_Rpt_Mass_Mail_Dte;
    private DbsField ur_Tircntl_Eff_Dte;
    private DbsField ur_Tircntl_Pr_Cntl_Start;
    private DbsField ur_Tircntl_Pr_Cntl_End;
    private DbsField ur_Tircntl_Pr_Cntl_Curr;

    private DbsGroup ur_Tircntl_Rpt_Tbl_Pe;
    private DbsField ur_Tircntl_Rpt_Dte;
    private DbsField ur_Tircntl_Rpt_Cref_Gross_Amt;
    private DbsField ur_Tircntl_Rpt_Cref_Int_Amt;
    private DbsField ur_Tircntl_Rpt_Cref_Taxable_Amt;
    private DbsField ur_Tircntl_Rpt_Cref_Ivc_Amt;
    private DbsField ur_Tircntl_Rpt_Cref_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Cref_Loc_Amt;
    private DbsField ur_Tircntl_Rpt_Cref_Loc_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Life_Gross_Amt;
    private DbsField ur_Tircntl_Rpt_Life_Int_Amt;
    private DbsField ur_Tircntl_Rpt_Life_Taxable_Amt;
    private DbsField ur_Tircntl_Rpt_Life_Ivc_Amt;
    private DbsField ur_Tircntl_Rpt_Life_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Life_Loc_Amt;
    private DbsField ur_Tircntl_Rpt_Life_Loc_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Tiaa_Gross_Amt;
    private DbsField ur_Tircntl_Rpt_Tiaa_Int_Amt;
    private DbsField ur_Tircntl_Rpt_Tiaa_Taxable_Amt;
    private DbsField ur_Tircntl_Rpt_Tiaa_Ivc_Amt;
    private DbsField ur_Tircntl_Rpt_Tiaa_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Tiaa_Loc_Amt;
    private DbsField ur_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld;
    private DbsField ur_Tircntl_Rpt_Amount_1;
    private DbsField ur_Tircntl_Rpt_Amount_2;

    private DataAccessProgramView vw_sr;
    private DbsField sr_Tircntl_Tbl_Nbr;
    private DbsField sr_Tircntl_Tax_Year;
    private DbsField sr_Tircntl_Seq_Nbr;

    private DbsGroup sr_Tircntl_Reporting_Tbl;
    private DbsField sr_Tircntl_Rpt_Form_Name;
    private DbsField sr_Tircntl_Future_Use_Date;
    private DbsField sr_Tircntl_Rpt_2nd_Mass_Mail_Dte;
    private DbsField sr_Tircntl_Rpt_Mass_Mail_Dte;
    private DbsField sr_Tircntl_Eff_Dte;
    private DbsField sr_Tircntl_Pr_Cntl_Start;
    private DbsField sr_Tircntl_Pr_Cntl_End;
    private DbsField sr_Tircntl_Pr_Cntl_Curr;

    private DbsGroup sr_Tircntl_Rpt_Tbl_Pe;
    private DbsField sr_Tircntl_Rpt_Dte;
    private DbsField sr_Tircntl_Rpt_Cref_Gross_Amt;
    private DbsField sr_Tircntl_Rpt_Cref_Int_Amt;
    private DbsField sr_Tircntl_Rpt_Cref_Taxable_Amt;
    private DbsField sr_Tircntl_Rpt_Cref_Ivc_Amt;
    private DbsField sr_Tircntl_Rpt_Cref_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Cref_Loc_Amt;
    private DbsField sr_Tircntl_Rpt_Cref_Loc_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Life_Gross_Amt;
    private DbsField sr_Tircntl_Rpt_Life_Int_Amt;
    private DbsField sr_Tircntl_Rpt_Life_Taxable_Amt;
    private DbsField sr_Tircntl_Rpt_Life_Ivc_Amt;
    private DbsField sr_Tircntl_Rpt_Life_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Life_Loc_Amt;
    private DbsField sr_Tircntl_Rpt_Life_Loc_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Tiaa_Gross_Amt;
    private DbsField sr_Tircntl_Rpt_Tiaa_Int_Amt;
    private DbsField sr_Tircntl_Rpt_Tiaa_Taxable_Amt;
    private DbsField sr_Tircntl_Rpt_Tiaa_Ivc_Amt;
    private DbsField sr_Tircntl_Rpt_Tiaa_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Tiaa_Loc_Amt;
    private DbsField sr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld;
    private DbsField sr_Tircntl_Rpt_Amount_1;
    private DbsField sr_Tircntl_Rpt_Amount_2;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Form_Name;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Form_Name;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rr = new DataAccessProgramView(new NameInfo("vw_rr", "RR"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        rr_Tircntl_Tbl_Nbr = vw_rr.getRecord().newFieldInGroup("rr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rr_Tircntl_Tax_Year = vw_rr.getRecord().newFieldInGroup("rr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rr_Tircntl_Seq_Nbr = vw_rr.getRecord().newFieldInGroup("rr_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rr_Tircntl_Reporting_Tbl = vw_rr.getRecord().newGroupInGroup("RR_TIRCNTL_REPORTING_TBL", "TIRCNTL-REPORTING-TBL");
        rr_Tircntl_Rpt_Form_Name = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        rr_Tircntl_Future_Use_Date = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Future_Use_Date", "TIRCNTL-FUTURE-USE-DATE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FUTURE_USE_DATE");
        rr_Tircntl_Rpt_2nd_Mass_Mail_Dte = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Rpt_2nd_Mass_Mail_Dte", "TIRCNTL-RPT-2ND-MASS-MAIL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_2ND_MASS_MAIL_DTE");
        rr_Tircntl_Rpt_Mass_Mail_Dte = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Rpt_Mass_Mail_Dte", "TIRCNTL-RPT-MASS-MAIL-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_MASS_MAIL_DTE");
        rr_Tircntl_Eff_Dte = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Eff_Dte", "TIRCNTL-EFF-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "TIRCNTL_EFF_DTE");
        rr_Tircntl_Pr_Cntl_Start = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Pr_Cntl_Start", "TIRCNTL-PR-CNTL-START", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_START");
        rr_Tircntl_Pr_Cntl_Start.setDdmHeader("PUERTORICO/CONTROL #/START");
        rr_Tircntl_Pr_Cntl_End = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Pr_Cntl_End", "TIRCNTL-PR-CNTL-END", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_END");
        rr_Tircntl_Pr_Cntl_End.setDdmHeader("PUERTO RICO/CONTROL #/END");
        rr_Tircntl_Pr_Cntl_Curr = rr_Tircntl_Reporting_Tbl.newFieldInGroup("rr_Tircntl_Pr_Cntl_Curr", "TIRCNTL-PR-CNTL-CURR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_CURR");
        rr_Tircntl_Pr_Cntl_Curr.setDdmHeader("PUERTO RICO/CONTROL #/CURRENT");

        rr_Tircntl_Rpt_Tbl_Pe = vw_rr.getRecord().newGroupInGroup("rr_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Dte = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        rr_Tircntl_Rpt_Cref_Gross_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Gross_Amt", "TIRCNTL-RPT-CREF-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Int_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Int_Amt", "TIRCNTL-RPT-CREF-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Taxable_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Taxable_Amt", "TIRCNTL-RPT-CREF-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Ivc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Ivc_Amt", "TIRCNTL-RPT-CREF-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Tax_Wthld", "TIRCNTL-RPT-CREF-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Loc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Loc_Amt", "TIRCNTL-RPT-CREF-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Cref_Loc_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Cref_Loc_Tax_Wthld", "TIRCNTL-RPT-CREF-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Gross_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Gross_Amt", "TIRCNTL-RPT-LIFE-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Int_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Int_Amt", "TIRCNTL-RPT-LIFE-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Taxable_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Taxable_Amt", "TIRCNTL-RPT-LIFE-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Ivc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Ivc_Amt", "TIRCNTL-RPT-LIFE-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Tax_Wthld", "TIRCNTL-RPT-LIFE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Loc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Loc_Amt", "TIRCNTL-RPT-LIFE-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Life_Loc_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Life_Loc_Tax_Wthld", "TIRCNTL-RPT-LIFE-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Gross_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Gross_Amt", "TIRCNTL-RPT-TIAA-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Int_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Int_Amt", "TIRCNTL-RPT-TIAA-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Taxable_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Taxable_Amt", "TIRCNTL-RPT-TIAA-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Ivc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Ivc_Amt", "TIRCNTL-RPT-TIAA-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Tax_Wthld", "TIRCNTL-RPT-TIAA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Loc_Amt = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Loc_Amt", "TIRCNTL-RPT-TIAA-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld", "TIRCNTL-RPT-TIAA-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Amount_1 = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Amount_1", "TIRCNTL-RPT-AMOUNT-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_1", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        rr_Tircntl_Rpt_Amount_2 = rr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("rr_Tircntl_Rpt_Amount_2", "TIRCNTL-RPT-AMOUNT-2", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_2", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        registerRecord(vw_rr);

        vw_ur = new DataAccessProgramView(new NameInfo("vw_ur", "UR"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        ur_Tircntl_Tbl_Nbr = vw_ur.getRecord().newFieldInGroup("ur_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        ur_Tircntl_Tax_Year = vw_ur.getRecord().newFieldInGroup("ur_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        ur_Tircntl_Seq_Nbr = vw_ur.getRecord().newFieldInGroup("ur_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        ur_Tircntl_Reporting_Tbl = vw_ur.getRecord().newGroupInGroup("UR_TIRCNTL_REPORTING_TBL", "TIRCNTL-REPORTING-TBL");
        ur_Tircntl_Rpt_Form_Name = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        ur_Tircntl_Future_Use_Date = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Future_Use_Date", "TIRCNTL-FUTURE-USE-DATE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FUTURE_USE_DATE");
        ur_Tircntl_Rpt_2nd_Mass_Mail_Dte = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Rpt_2nd_Mass_Mail_Dte", "TIRCNTL-RPT-2ND-MASS-MAIL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_2ND_MASS_MAIL_DTE");
        ur_Tircntl_Rpt_Mass_Mail_Dte = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Rpt_Mass_Mail_Dte", "TIRCNTL-RPT-MASS-MAIL-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_MASS_MAIL_DTE");
        ur_Tircntl_Eff_Dte = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Eff_Dte", "TIRCNTL-EFF-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "TIRCNTL_EFF_DTE");
        ur_Tircntl_Pr_Cntl_Start = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Pr_Cntl_Start", "TIRCNTL-PR-CNTL-START", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_START");
        ur_Tircntl_Pr_Cntl_Start.setDdmHeader("PUERTORICO/CONTROL #/START");
        ur_Tircntl_Pr_Cntl_End = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Pr_Cntl_End", "TIRCNTL-PR-CNTL-END", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_END");
        ur_Tircntl_Pr_Cntl_End.setDdmHeader("PUERTO RICO/CONTROL #/END");
        ur_Tircntl_Pr_Cntl_Curr = ur_Tircntl_Reporting_Tbl.newFieldInGroup("ur_Tircntl_Pr_Cntl_Curr", "TIRCNTL-PR-CNTL-CURR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_CURR");
        ur_Tircntl_Pr_Cntl_Curr.setDdmHeader("PUERTO RICO/CONTROL #/CURRENT");

        ur_Tircntl_Rpt_Tbl_Pe = vw_ur.getRecord().newGroupInGroup("ur_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Dte = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        ur_Tircntl_Rpt_Cref_Gross_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Gross_Amt", "TIRCNTL-RPT-CREF-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Int_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Int_Amt", "TIRCNTL-RPT-CREF-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Taxable_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Taxable_Amt", "TIRCNTL-RPT-CREF-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Ivc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Ivc_Amt", "TIRCNTL-RPT-CREF-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Tax_Wthld", "TIRCNTL-RPT-CREF-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Loc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Loc_Amt", "TIRCNTL-RPT-CREF-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Cref_Loc_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Cref_Loc_Tax_Wthld", "TIRCNTL-RPT-CREF-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Gross_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Gross_Amt", "TIRCNTL-RPT-LIFE-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Int_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Int_Amt", "TIRCNTL-RPT-LIFE-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Taxable_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Taxable_Amt", "TIRCNTL-RPT-LIFE-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Ivc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Ivc_Amt", "TIRCNTL-RPT-LIFE-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Tax_Wthld", "TIRCNTL-RPT-LIFE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Loc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Loc_Amt", "TIRCNTL-RPT-LIFE-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Life_Loc_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Life_Loc_Tax_Wthld", "TIRCNTL-RPT-LIFE-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Gross_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Gross_Amt", "TIRCNTL-RPT-TIAA-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Int_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Int_Amt", "TIRCNTL-RPT-TIAA-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Taxable_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Taxable_Amt", "TIRCNTL-RPT-TIAA-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Ivc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Ivc_Amt", "TIRCNTL-RPT-TIAA-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Tax_Wthld", "TIRCNTL-RPT-TIAA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Loc_Amt = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Loc_Amt", "TIRCNTL-RPT-TIAA-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld", "TIRCNTL-RPT-TIAA-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Amount_1 = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Amount_1", "TIRCNTL-RPT-AMOUNT-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_1", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        ur_Tircntl_Rpt_Amount_2 = ur_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("ur_Tircntl_Rpt_Amount_2", "TIRCNTL-RPT-AMOUNT-2", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_2", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        registerRecord(vw_ur);

        vw_sr = new DataAccessProgramView(new NameInfo("vw_sr", "SR"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        sr_Tircntl_Tbl_Nbr = vw_sr.getRecord().newFieldInGroup("sr_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        sr_Tircntl_Tax_Year = vw_sr.getRecord().newFieldInGroup("sr_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        sr_Tircntl_Seq_Nbr = vw_sr.getRecord().newFieldInGroup("sr_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        sr_Tircntl_Reporting_Tbl = vw_sr.getRecord().newGroupInGroup("SR_TIRCNTL_REPORTING_TBL", "TIRCNTL-REPORTING-TBL");
        sr_Tircntl_Rpt_Form_Name = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        sr_Tircntl_Future_Use_Date = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Future_Use_Date", "TIRCNTL-FUTURE-USE-DATE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FUTURE_USE_DATE");
        sr_Tircntl_Rpt_2nd_Mass_Mail_Dte = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Rpt_2nd_Mass_Mail_Dte", "TIRCNTL-RPT-2ND-MASS-MAIL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_2ND_MASS_MAIL_DTE");
        sr_Tircntl_Rpt_Mass_Mail_Dte = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Rpt_Mass_Mail_Dte", "TIRCNTL-RPT-MASS-MAIL-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_MASS_MAIL_DTE");
        sr_Tircntl_Eff_Dte = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Eff_Dte", "TIRCNTL-EFF-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "TIRCNTL_EFF_DTE");
        sr_Tircntl_Pr_Cntl_Start = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Pr_Cntl_Start", "TIRCNTL-PR-CNTL-START", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_START");
        sr_Tircntl_Pr_Cntl_Start.setDdmHeader("PUERTORICO/CONTROL #/START");
        sr_Tircntl_Pr_Cntl_End = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Pr_Cntl_End", "TIRCNTL-PR-CNTL-END", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_END");
        sr_Tircntl_Pr_Cntl_End.setDdmHeader("PUERTO RICO/CONTROL #/END");
        sr_Tircntl_Pr_Cntl_Curr = sr_Tircntl_Reporting_Tbl.newFieldInGroup("sr_Tircntl_Pr_Cntl_Curr", "TIRCNTL-PR-CNTL-CURR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRCNTL_PR_CNTL_CURR");
        sr_Tircntl_Pr_Cntl_Curr.setDdmHeader("PUERTO RICO/CONTROL #/CURRENT");

        sr_Tircntl_Rpt_Tbl_Pe = vw_sr.getRecord().newGroupInGroup("sr_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Dte = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        sr_Tircntl_Rpt_Cref_Gross_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Gross_Amt", "TIRCNTL-RPT-CREF-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Int_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Int_Amt", "TIRCNTL-RPT-CREF-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Taxable_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Taxable_Amt", "TIRCNTL-RPT-CREF-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Ivc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Ivc_Amt", "TIRCNTL-RPT-CREF-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Tax_Wthld", "TIRCNTL-RPT-CREF-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Loc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Loc_Amt", "TIRCNTL-RPT-CREF-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Cref_Loc_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Cref_Loc_Tax_Wthld", "TIRCNTL-RPT-CREF-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_CREF_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Gross_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Gross_Amt", "TIRCNTL-RPT-LIFE-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Int_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Int_Amt", "TIRCNTL-RPT-LIFE-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Taxable_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Taxable_Amt", "TIRCNTL-RPT-LIFE-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Ivc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Ivc_Amt", "TIRCNTL-RPT-LIFE-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Tax_Wthld", "TIRCNTL-RPT-LIFE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Loc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Loc_Amt", "TIRCNTL-RPT-LIFE-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Life_Loc_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Life_Loc_Tax_Wthld", "TIRCNTL-RPT-LIFE-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_LIFE_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Gross_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Gross_Amt", "TIRCNTL-RPT-TIAA-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_GROSS_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Int_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Int_Amt", "TIRCNTL-RPT-TIAA-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_INT_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Taxable_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Taxable_Amt", "TIRCNTL-RPT-TIAA-TAXABLE-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAXABLE_AMT", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Ivc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Ivc_Amt", "TIRCNTL-RPT-TIAA-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_IVC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Tax_Wthld", "TIRCNTL-RPT-TIAA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_TAX_WTHLD", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Loc_Amt = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Loc_Amt", "TIRCNTL-RPT-TIAA-LOC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_AMT", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Tiaa_Loc_Tax_Wthld", "TIRCNTL-RPT-TIAA-LOC-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_TIAA_LOC_TAX_WTHLD", 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Amount_1 = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Amount_1", "TIRCNTL-RPT-AMOUNT-1", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_1", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        sr_Tircntl_Rpt_Amount_2 = sr_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("sr_Tircntl_Rpt_Amount_2", "TIRCNTL-RPT-AMOUNT-2", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_AMOUNT_2", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        registerRecord(vw_sr);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.STRING, 12);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Form_Name = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Form_Name", "#S1-FORM-NAME", FieldType.STRING, 7);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 12);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Form_Name = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Form_Name", "#S2-FORM-NAME", FieldType.STRING, 7);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rr.reset();
        vw_ur.reset();
        vw_sr.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8160() throws Exception
    {
        super("Twrp8160");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8160|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(4);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 4
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Form_Name.setValue(" ");                                                                                                                //Natural: ASSIGN #S1-FORM-NAME := ' '
                vw_rr.startDatabaseRead                                                                                                                                   //Natural: READ RR WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") }
                );
                READ01:
                while (condition(vw_rr.readNextRow("READ01")))
                {
                    if (condition(rr_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rr_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                        //Natural: IF RR.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RR.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  01-14-04 FRANK,MIKE
                    if (condition(!(rr_Tircntl_Rpt_Form_Name.equals("NR4") || rr_Tircntl_Rpt_Form_Name.equals("1042-S") || rr_Tircntl_Rpt_Form_Name.equals("1099-R")      //Natural: ACCEPT IF RR.TIRCNTL-RPT-FORM-NAME = 'NR4' OR = '1042-S' OR = '1099-R' OR = '1099INT' OR = '480.6a' OR = '480.6b' OR = '5498' OR = 'W-2' OR = 'SUNY'
                        || rr_Tircntl_Rpt_Form_Name.equals("1099INT") || rr_Tircntl_Rpt_Form_Name.equals("480.6a") || rr_Tircntl_Rpt_Form_Name.equals("480.6b") 
                        || rr_Tircntl_Rpt_Form_Name.equals("5498") || rr_Tircntl_Rpt_Form_Name.equals("W-2") || rr_Tircntl_Rpt_Form_Name.equals("SUNY"))))
                    {
                        continue;
                    }
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(4);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 4
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Form_Name.setValue(rr_Tircntl_Rpt_Form_Name);                                                                                       //Natural: ASSIGN #S2-FORM-NAME := RR.TIRCNTL-RPT-FORM-NAME
                    vw_ur.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) UR WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") },
                    1
                    );
                    RL2:
                    while (condition(vw_ur.readNextRow("RL2")))
                    {
                        if (condition(ur_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && ur_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year) &&                   //Natural: IF UR.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UR.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UR.TIRCNTL-RPT-FORM-NAME = #S2-FORM-NAME
                            ur_Tircntl_Rpt_Form_Name.equals(pnd_Super2_Pnd_S2_Form_Name)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        sr_Tircntl_Tbl_Nbr.setValue(rr_Tircntl_Tbl_Nbr);                                                                                                  //Natural: ASSIGN SR.TIRCNTL-TBL-NBR := RR.TIRCNTL-TBL-NBR
                        sr_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                                //Natural: ASSIGN SR.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        sr_Tircntl_Seq_Nbr.setValue(rr_Tircntl_Seq_Nbr);                                                                                                  //Natural: ASSIGN SR.TIRCNTL-SEQ-NBR := RR.TIRCNTL-SEQ-NBR
                        sr_Tircntl_Rpt_Form_Name.setValue(rr_Tircntl_Rpt_Form_Name);                                                                                      //Natural: ASSIGN SR.TIRCNTL-RPT-FORM-NAME := RR.TIRCNTL-RPT-FORM-NAME
                        sr_Tircntl_Future_Use_Date.setValue(99999999);                                                                                                    //Natural: ASSIGN SR.TIRCNTL-FUTURE-USE-DATE := 99999999
                        sr_Tircntl_Rpt_2nd_Mass_Mail_Dte.setValue(99999999);                                                                                              //Natural: ASSIGN SR.TIRCNTL-RPT-2ND-MASS-MAIL-DTE := 99999999
                        sr_Tircntl_Rpt_Mass_Mail_Dte.setValue(99999999);                                                                                                  //Natural: ASSIGN SR.TIRCNTL-RPT-MASS-MAIL-DTE := 99999999
                        sr_Tircntl_Eff_Dte.setValue(99999999);                                                                                                            //Natural: ASSIGN SR.TIRCNTL-EFF-DTE := 99999999
                        sr_Tircntl_Pr_Cntl_Start.setValue(" ");                                                                                                           //Natural: ASSIGN SR.TIRCNTL-PR-CNTL-START := ' '
                        sr_Tircntl_Pr_Cntl_End.setValue(" ");                                                                                                             //Natural: ASSIGN SR.TIRCNTL-PR-CNTL-END := ' '
                        sr_Tircntl_Pr_Cntl_Curr.setValue(" ");                                                                                                            //Natural: ASSIGN SR.TIRCNTL-PR-CNTL-CURR := ' '
                        vw_sr.insertDBRow();                                                                                                                              //Natural: STORE SR
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(4);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 4
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Form_Name.setValue(" ");                                                                                                                //Natural: ASSIGN #S1-FORM-NAME := ' '
                vw_rr.startDatabaseRead                                                                                                                                   //Natural: READ RR WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") }
                );
                RL3:
                while (condition(vw_rr.readNextRow("RL3")))
                {
                    if (condition(rr_Tircntl_Tbl_Nbr.equals(4)))                                                                                                          //Natural: IF RR.TIRCNTL-TBL-NBR = 4
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rr_Tircntl_Tbl_Nbr,new TabSetting(5),rr_Tircntl_Tax_Year,new          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T RR.TIRCNTL-TBL-NBR 05T RR.TIRCNTL-TAX-YEAR 11T RR.TIRCNTL-SEQ-NBR 17T RR.TIRCNTL-RPT-FORM-NAME 25T RR.TIRCNTL-RPT-MASS-MAIL-DTE 35T RR.TIRCNTL-RPT-2ND-MASS-MAIL-DTE 45T RR.TIRCNTL-FUTURE-USE-DATE 56T RR.TIRCNTL-EFF-DTE 67T RR.TIRCNTL-RPT-DTE ( 1 ) ( EM = YYYYMMDD )
                        TabSetting(11),rr_Tircntl_Seq_Nbr,new TabSetting(17),rr_Tircntl_Rpt_Form_Name,new TabSetting(25),rr_Tircntl_Rpt_Mass_Mail_Dte,new 
                        TabSetting(35),rr_Tircntl_Rpt_2nd_Mass_Mail_Dte,new TabSetting(45),rr_Tircntl_Future_Use_Date,new TabSetting(56),rr_Tircntl_Eff_Dte,new 
                        TabSetting(67),rr_Tircntl_Rpt_Dte.getValue(1), new ReportEditMask ("YYYYMMDD"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(55),"Reporting Table Report",new              //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 55T 'Reporting Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17)," Form  ",new //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T ' Form  ' 26T '  Mass  ' 36T '2nd Mass' 46T ' Future ' 56T 'Effective' 67T 'Original'
                        TabSetting(26),"  Mass  ",new TabSetting(36),"2nd Mass",new TabSetting(46)," Future ",new TabSetting(56),"Effective",new TabSetting(67),
                        "Original");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17)," Name  ",new //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T ' Name  ' 26T 'Mailing ' 36T 'Mailing ' 46T '  Date  ' 56T '  Date   ' 67T '  Date  '
                        TabSetting(26),"Mailing ",new TabSetting(36),"Mailing ",new TabSetting(46),"  Date  ",new TabSetting(56),"  Date   ",new TabSetting(67),
                        "  Date  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=======",new //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=======' 26T '========' 36T '========' 46T '========' 56T '=========' 67T '========'
                        TabSetting(26),"========",new TabSetting(36),"========",new TabSetting(46),"========",new TabSetting(56),"=========",new TabSetting(67),
                        "========");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
