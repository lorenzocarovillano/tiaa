/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:15 PM
**        * FROM NATURAL PROGRAM : Twrp4420
************************************************************
**        * FILE NAME            : Twrp4420.java
**        * CLASS NAME           : Twrp4420
**        * INSTANCE NAME        : Twrp4420
************************************************************
************************************************************************
*
* PROGRAM  : TWRP4420
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : IRS FORMS EXTRACT CONTROL TOTALS REPORTS.
* CREATED  : 09 / 23 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS 5498 FORMS RECORDS, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
* HISTORY  :
* 11/25/05 : BK  - TWRL442A SEP-AMT ADDED
*            ADDED PARM FOR REPORT TO SUBST TWRP4430 AND TWRP4435
*            ALL UNUSED CODE DELETED
* 12/22/04 : AAY - REVISED TO INCREASE ALL VARIABLES DEFINED AS P10.2
*          :       TO P15.2, AND (P7/8) TO (P12/8).  EDIT MASKS WERE
*          :       ALSO INCREASED ACCORDINGLY.
*          :
* 12/03/17 :  DASDH 5498 BOX CHANGES
* 10/05/18 : ARIVU - EIN CHANGE - TAG EINCHG
* 18-11-20 : PALDE           - IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4420 extends BLNatBase
{
    // Data Areas
    private LdaTwrl442a ldaTwrl442a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Total_B_Recs_Record;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Source_Total_Header;
    private DbsField pnd_Prev_Form_Type;
    private DbsField pnd_Header_Date;
    private DbsField pnd_Report_Number;
    private DbsField pnd_Header_Line;
    private DbsField pnd_Trailer_Line;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_T_Postpn_Amt;
    private DbsField pnd_T_Repymnt_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;
    private DbsField pnd_S_Postpn_Amt;
    private DbsField pnd_S_Repymnt_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_C_Postpn_Amt;
    private DbsField pnd_C_Repymnt_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_L_Postpn_Amt;
    private DbsField pnd_L_Repymnt_Amt;
    private DbsField i1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl442a = new LdaTwrl442a();
        registerRecord(ldaTwrl442a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Total_B_Recs_Record = localVariables.newFieldInRecord("pnd_Total_B_Recs_Record", "#TOTAL-B-RECS-RECORD", FieldType.NUMERIC, 8);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Source_Total_Header = localVariables.newFieldInRecord("pnd_Source_Total_Header", "#SOURCE-TOTAL-HEADER", FieldType.STRING, 17);
        pnd_Prev_Form_Type = localVariables.newFieldInRecord("pnd_Prev_Form_Type", "#PREV-FORM-TYPE", FieldType.STRING, 2);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.DATE);
        pnd_Report_Number = localVariables.newFieldInRecord("pnd_Report_Number", "#REPORT-NUMBER", FieldType.STRING, 1);
        pnd_Header_Line = localVariables.newFieldInRecord("pnd_Header_Line", "#HEADER-LINE", FieldType.STRING, 50);
        pnd_Trailer_Line = localVariables.newFieldInRecord("pnd_Trailer_Line", "#TRAILER-LINE", FieldType.STRING, 50);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 
            2));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_T_Postpn_Amt", "#T-POSTPN-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_T_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_T_Repymnt_Amt", "#T-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 
            2));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_S_Postpn_Amt", "#S-POSTPN-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_S_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_S_Repymnt_Amt", "#S-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 
            2));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_C_Postpn_Amt", "#C-POSTPN-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_C_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_C_Repymnt_Amt", "#C-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 12, new DbsArrayController(1, 
            2));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_L_Postpn_Amt", "#L-POSTPN-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_L_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_L_Repymnt_Amt", "#L-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        i1 = localVariables.newFieldInRecord("i1", "I1", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl442a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4420() throws Exception
    {
        super("Twrp4420");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp4420|Main");
        OnErrorManager.pushEvent("TWRP4420", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Report_Number);                                                                                    //Natural: INPUT #REPORT-NUMBER
                if (condition(pnd_Report_Number.equals("1")))                                                                                                             //Natural: IF #REPORT-NUMBER = '1' THEN
                {
                    pnd_Header_Line.setValue("IRA CONTRIBUTIONS IRS CONTROL REPORT");                                                                                     //Natural: ASSIGN #HEADER-LINE := 'IRA CONTRIBUTIONS IRS CONTROL REPORT'
                    pnd_Trailer_Line.setValue("IRS IRA RECORDS READ.......................");                                                                             //Natural: ASSIGN #TRAILER-LINE := 'IRS IRA RECORDS READ.......................'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Report_Number.equals("2")))                                                                                                         //Natural: IF #REPORT-NUMBER = '2' THEN
                    {
                        pnd_Header_Line.setValue("EMPTY IRA CONTRIBUTIONS IRS CONTROL REPORT");                                                                           //Natural: ASSIGN #HEADER-LINE := 'EMPTY IRA CONTRIBUTIONS IRS CONTROL REPORT'
                        pnd_Trailer_Line.setValue("EMPTY IRS IRA RECORDS READ..............");                                                                            //Natural: ASSIGN #TRAILER-LINE := 'EMPTY IRS IRA RECORDS READ..............'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Report_Number.equals("3")))                                                                                                     //Natural: IF #REPORT-NUMBER = '3' THEN
                        {
                            pnd_Header_Line.setValue("REJECTED IRA CONTRIBUTIONS IRS CONTROL REPORT");                                                                    //Natural: ASSIGN #HEADER-LINE := 'REJECTED IRA CONTRIBUTIONS IRS CONTROL REPORT'
                            pnd_Trailer_Line.setValue("REJECTED IRS IRA RECORDS READ..............");                                                                     //Natural: ASSIGN #TRAILER-LINE := 'REJECTED IRS IRA RECORDS READ..............'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                            sub_Error_Display_Start();
                            if (condition(Global.isEscape())) {return;}
                            if (condition(Map.getDoInput())) {return;}
                            getReports().write(0, "***",new TabSetting(6),"FORM (5498) IRS REPORT NUMBER WRONG",pnd_Report_Number,new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'FORM (5498) IRS REPORT NUMBER WRONG' #REPORT-NUMBER 77T '***' / '***' 06T 'MUST BE 1 OR 2 OR 3' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                                TabSetting(6),"MUST BE 1 OR 2 OR 3",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new 
                                TabSetting(77),"***");
                            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                            sub_Error_Display_End();
                            if (condition(Global.isEscape())) {return;}
                            if (condition(Map.getDoInput())) {return;}
                            DbsUtil.terminate(90);  if (true) return;                                                                                                     //Natural: TERMINATE 90
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Header_Date.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #HEADER-DATE := *DATX
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 RECORD #FORM
                while (condition(getWorkFiles().read(1, ldaTwrl442a.getPnd_Form())))
                {
                    pnd_Read_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CTR
                    if (condition(pnd_Read_Ctr.equals(1)))                                                                                                                //Natural: IF #READ-CTR = 1
                    {
                        pnd_Prev_Form_Type.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Type());                                                                            //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Prev_Form_Type.equals(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Type())))                                                                   //Natural: IF #PREV-FORM-TYPE = #F-IRA-TYPE
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM #TOTAL-HEADER
                        sub_Pnd_Total_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        i1.setValue(1);                                                                                                                                   //Natural: ASSIGN I1 := 1
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
                        sub_Display_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
                        sub_Update_General_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-TOTALS
                        sub_Reset_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Prev_Form_Type.setValue(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Type());                                                                            //Natural: ASSIGN #PREV-FORM-TYPE := #F-IRA-TYPE
                    }                                                                                                                                                     //Natural: END-IF
                    //* *IF #F-COMPANY-CDE  =  'T'                                 /* EINCHG
                    //*  EINCHG
                    if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde().equals("T") || ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde().equals("A")))                    //Natural: IF #F-COMPANY-CDE = 'T' OR = 'A'
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
                        sub_Update_T_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde().equals("S")))                                                                           //Natural: IF #F-COMPANY-CDE = 'S'
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
                            sub_Update_S_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaTwrl442a.getPnd_Form_Pnd_F_Company_Cde().equals("L")))                                                                       //Natural: IF #F-COMPANY-CDE = 'L'
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
                                sub_Update_L_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
                                sub_Update_C_Control_Totals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //* *------
                if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                            //Natural: IF #READ-CTR = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(6),"Form (5498) IRS File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 00 ) '***' 06T 'Form (5498) IRS File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                    if (condition(pnd_Report_Number.equals("1")))                                                                                                         //Natural: IF #REPORT-NUMBER = '1' THEN
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #TOTAL-HEADER
                sub_Pnd_Total_Header();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                i1.setValue(1);                                                                                                                                           //Natural: ASSIGN I1 := 1
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
                sub_Display_Control_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
                sub_Update_General_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //* *------------
                //* *------------------------------------------
                //* *----------------------
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //* *-------                                                                                                                                              //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_T_Trans_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( 1 )
        pnd_T_Classic_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #T-CLASSIC-AMT ( 1 )
        pnd_T_Roth_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #T-ROTH-AMT ( 1 )
        pnd_T_Rollover_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                           //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( 1 )
        pnd_T_Rechar_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                    //Natural: ADD #F-IRA-RECHAR TO #T-RECHAR-AMT ( 1 )
        pnd_T_Fmv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #T-FMV-AMT ( 1 )
        pnd_T_Roth_Conv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #T-ROTH-CONV-AMT ( 1 )
        pnd_T_Sep_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Sep_Contrib());                                                                                      //Natural: ADD #F-SEP-CONTRIB TO #T-SEP-AMT ( 1 )
        //*  DASDH
        pnd_T_Postpn_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #T-POSTPN-AMT ( 1 )
        //*  PALDE
        pnd_T_Repymnt_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #T-REPYMNT-AMT ( 1 )
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_C_Trans_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( 1 )
        pnd_C_Classic_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #C-CLASSIC-AMT ( 1 )
        pnd_C_Roth_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #C-ROTH-AMT ( 1 )
        pnd_C_Rollover_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                           //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #C-ROLLOVER-AMT ( 1 )
        pnd_C_Rechar_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                    //Natural: ADD #F-IRA-RECHAR TO #C-RECHAR-AMT ( 1 )
        pnd_C_Fmv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #C-FMV-AMT ( 1 )
        pnd_C_Roth_Conv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #C-ROTH-CONV-AMT ( 1 )
        pnd_C_Sep_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Sep_Contrib());                                                                                      //Natural: ADD #F-SEP-CONTRIB TO #C-SEP-AMT ( 1 )
        //*  DASDH
        pnd_C_Postpn_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #C-POSTPN-AMT ( 1 )
        //*  PALDE
        pnd_C_Repymnt_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #C-REPYMNT-AMT ( 1 )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_L_Trans_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( 1 )
        pnd_L_Classic_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #L-CLASSIC-AMT ( 1 )
        pnd_L_Roth_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #L-ROTH-AMT ( 1 )
        pnd_L_Rollover_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                           //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #L-ROLLOVER-AMT ( 1 )
        pnd_L_Rechar_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                    //Natural: ADD #F-IRA-RECHAR TO #L-RECHAR-AMT ( 1 )
        pnd_L_Fmv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #L-FMV-AMT ( 1 )
        pnd_L_Roth_Conv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #L-ROTH-CONV-AMT ( 1 )
        pnd_L_Sep_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Sep_Contrib());                                                                                      //Natural: ADD #F-SEP-CONTRIB TO #L-SEP-AMT ( 1 )
        //*  DASDH
        pnd_L_Postpn_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #L-POSTPN-AMT ( 1 )
        //*  PALDE
        pnd_L_Repymnt_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #L-REPYMNT-AMT ( 1 )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_S_Trans_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #S-TRANS-COUNT ( 1 )
        pnd_S_Classic_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Contrib());                                                                             //Natural: ADD #F-TRAD-IRA-CONTRIB TO #S-CLASSIC-AMT ( 1 )
        pnd_S_Roth_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Contrib());                                                                                //Natural: ADD #F-ROTH-IRA-CONTRIB TO #S-ROTH-AMT ( 1 )
        pnd_S_Rollover_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Trad_Ira_Rollover());                                                                           //Natural: ADD #F-TRAD-IRA-ROLLOVER TO #S-ROLLOVER-AMT ( 1 )
        pnd_S_Rechar_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Ira_Rechar());                                                                                    //Natural: ADD #F-IRA-RECHAR TO #S-RECHAR-AMT ( 1 )
        pnd_S_Fmv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Account_Fmv());                                                                                      //Natural: ADD #F-ACCOUNT-FMV TO #S-FMV-AMT ( 1 )
        pnd_S_Roth_Conv_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Roth_Ira_Conversion());                                                                        //Natural: ADD #F-ROTH-IRA-CONVERSION TO #S-ROTH-CONV-AMT ( 1 )
        pnd_S_Sep_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Sep_Contrib());                                                                                      //Natural: ADD #F-SEP-CONTRIB TO #S-SEP-AMT ( 1 )
        //*  DASDH
        pnd_S_Postpn_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Postpn_Amt());                                                                               //Natural: ADD #F-TIRF-POSTPN-AMT TO #S-POSTPN-AMT ( 1 )
        //*  PALDE
        pnd_S_Repymnt_Amt.getValue(1).nadd(ldaTwrl442a.getPnd_Form_Pnd_F_Tirf_Repayments_Amt());                                                                          //Natural: ADD #F-TIRF-REPAYMENTS-AMT TO #S-REPYMNT-AMT ( 1 )
    }
    private void sub_Display_Control_Totals() throws Exception                                                                                                            //Natural: DISPLAY-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TIAA)       ",NEWLINE);                                                                     //Natural: WRITE ( 01 ) NOTITLE 01T '       (TIAA)       ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_T_Trans_Count.getValue(i1), new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #T-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_T_Classic_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #T-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_T_Roth_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #T-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_T_Rollover_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #T-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_T_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #T-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_T_Rechar_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #T-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_T_Sep_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #T-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_T_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #T-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,"Late RO Amt.........",new TabSetting(26),pnd_T_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Late RO Amt.........' 26T #T-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  PALDE
        //*  PALDE
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_T_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #T-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (TCII)      ",NEWLINE);                                                                      //Natural: WRITE ( 01 ) NOTITLE 01T '       (TCII)      ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_S_Trans_Count.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));     //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #S-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_S_Classic_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #S-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_S_Roth_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #S-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_S_Rollover_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #S-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_S_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #S-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_S_Rechar_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #S-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_S_Sep_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #S-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_S_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #S-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,"Late RO Amt.........",new TabSetting(26),pnd_S_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Late RO Amt.........' 26T #S-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  PALDE
        //*  PALDE
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT AMT.......",new TabSetting(26),pnd_S_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT AMT.......' 26T #S-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (LIFE)      ",NEWLINE);                                                                      //Natural: WRITE ( 01 ) NOTITLE 01T '       (LIFE)      ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_L_Trans_Count.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));     //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #L-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_L_Classic_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #L-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_L_Roth_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #L-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_L_Rollover_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #L-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_L_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #L-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_L_Rechar_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #L-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_L_Sep_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #L-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_L_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #L-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,"Late RO Amt.........",new TabSetting(26),pnd_L_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Late RO Amt.........' 26T #L-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  PALDE
        //*  PALDE
        getReports().write(1, ReportOption.NOTITLE,"REPAYMENT-AMT.......",new TabSetting(26),pnd_L_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMENT-AMT.......' 26T #L-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"       (CREF)      ",NEWLINE);                                                                      //Natural: WRITE ( 01 ) NOTITLE 01T '       (CREF)      ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Transaction Count...",new TabSetting(30),pnd_C_Trans_Count.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));     //Natural: WRITE ( 01 ) NOTITLE 'Transaction Count...' 30T #C-TRANS-COUNT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Classic Contrib.....",new TabSetting(26),pnd_C_Classic_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Classic Contrib.....' 26T #C-CLASSIC-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Contrib........",new TabSetting(26),pnd_C_Roth_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Contrib........' 26T #C-ROTH-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Rollover............",new TabSetting(26),pnd_C_Rollover_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Rollover............' 26T #C-ROLLOVER-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Roth Conversion.....",new TabSetting(26),pnd_C_Roth_Conv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Roth Conversion.....' 26T #C-ROTH-CONV-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Recharacter.........",new TabSetting(26),pnd_C_Rechar_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Recharacter.........' 26T #C-RECHAR-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"SEP.................",new TabSetting(26),pnd_C_Sep_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'SEP.................' 26T #C-SEP-AMT ( I1 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Fair Market.........",new TabSetting(26),pnd_C_Fmv_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));  //Natural: WRITE ( 01 ) NOTITLE 'Fair Market.........' 26T #C-FMV-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  DASDH
        //*  DASDH
        getReports().write(1, ReportOption.NOTITLE,"Late RO Amt.........",new TabSetting(26),pnd_C_Postpn_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'Late RO Amt.........' 26T #C-POSTPN-AMT ( I1 )
        if (Global.isEscape()) return;
        //*  PALDE
        //*  PALDE
        getReports().write(1, ReportOption.NOTITLE,"REPAYMNET AMT.......",new TabSetting(26),pnd_C_Repymnt_Amt.getValue(i1), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: WRITE ( 01 ) NOTITLE 'REPAYMNET AMT.......' 26T #C-REPYMNT-AMT ( I1 )
        if (Global.isEscape()) return;
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_T_Trans_Count.getValue(2).nadd(pnd_T_Trans_Count.getValue(1));                                                                                                //Natural: ADD #T-TRANS-COUNT ( 1 ) TO #T-TRANS-COUNT ( 2 )
        pnd_T_Classic_Amt.getValue(2).nadd(pnd_T_Classic_Amt.getValue(1));                                                                                                //Natural: ADD #T-CLASSIC-AMT ( 1 ) TO #T-CLASSIC-AMT ( 2 )
        pnd_T_Roth_Amt.getValue(2).nadd(pnd_T_Roth_Amt.getValue(1));                                                                                                      //Natural: ADD #T-ROTH-AMT ( 1 ) TO #T-ROTH-AMT ( 2 )
        pnd_T_Rollover_Amt.getValue(2).nadd(pnd_T_Rollover_Amt.getValue(1));                                                                                              //Natural: ADD #T-ROLLOVER-AMT ( 1 ) TO #T-ROLLOVER-AMT ( 2 )
        pnd_T_Rechar_Amt.getValue(2).nadd(pnd_T_Rechar_Amt.getValue(1));                                                                                                  //Natural: ADD #T-RECHAR-AMT ( 1 ) TO #T-RECHAR-AMT ( 2 )
        pnd_T_Sep_Amt.getValue(2).nadd(pnd_T_Sep_Amt.getValue(1));                                                                                                        //Natural: ADD #T-SEP-AMT ( 1 ) TO #T-SEP-AMT ( 2 )
        pnd_T_Fmv_Amt.getValue(2).nadd(pnd_T_Fmv_Amt.getValue(1));                                                                                                        //Natural: ADD #T-FMV-AMT ( 1 ) TO #T-FMV-AMT ( 2 )
        pnd_T_Roth_Conv_Amt.getValue(2).nadd(pnd_T_Roth_Conv_Amt.getValue(1));                                                                                            //Natural: ADD #T-ROTH-CONV-AMT ( 1 ) TO #T-ROTH-CONV-AMT ( 2 )
        //*  DASDH
        pnd_T_Postpn_Amt.getValue(2).nadd(pnd_T_Postpn_Amt.getValue(1));                                                                                                  //Natural: ADD #T-POSTPN-AMT ( 1 ) TO #T-POSTPN-AMT ( 2 )
        //*  PALDE
        pnd_T_Repymnt_Amt.getValue(2).nadd(pnd_T_Repymnt_Amt.getValue(1));                                                                                                //Natural: ADD #T-REPYMNT-AMT ( 1 ) TO #T-REPYMNT-AMT ( 2 )
        pnd_S_Trans_Count.getValue(2).nadd(pnd_S_Trans_Count.getValue(1));                                                                                                //Natural: ADD #S-TRANS-COUNT ( 1 ) TO #S-TRANS-COUNT ( 2 )
        pnd_S_Classic_Amt.getValue(2).nadd(pnd_S_Classic_Amt.getValue(1));                                                                                                //Natural: ADD #S-CLASSIC-AMT ( 1 ) TO #S-CLASSIC-AMT ( 2 )
        pnd_S_Roth_Amt.getValue(2).nadd(pnd_S_Roth_Amt.getValue(1));                                                                                                      //Natural: ADD #S-ROTH-AMT ( 1 ) TO #S-ROTH-AMT ( 2 )
        pnd_S_Rollover_Amt.getValue(2).nadd(pnd_S_Rollover_Amt.getValue(1));                                                                                              //Natural: ADD #S-ROLLOVER-AMT ( 1 ) TO #S-ROLLOVER-AMT ( 2 )
        pnd_S_Rechar_Amt.getValue(2).nadd(pnd_S_Rechar_Amt.getValue(1));                                                                                                  //Natural: ADD #S-RECHAR-AMT ( 1 ) TO #S-RECHAR-AMT ( 2 )
        pnd_S_Sep_Amt.getValue(2).nadd(pnd_S_Sep_Amt.getValue(1));                                                                                                        //Natural: ADD #S-SEP-AMT ( 1 ) TO #S-SEP-AMT ( 2 )
        pnd_S_Fmv_Amt.getValue(2).nadd(pnd_S_Fmv_Amt.getValue(1));                                                                                                        //Natural: ADD #S-FMV-AMT ( 1 ) TO #S-FMV-AMT ( 2 )
        pnd_S_Roth_Conv_Amt.getValue(2).nadd(pnd_S_Roth_Conv_Amt.getValue(1));                                                                                            //Natural: ADD #S-ROTH-CONV-AMT ( 1 ) TO #S-ROTH-CONV-AMT ( 2 )
        //*  DASDH
        pnd_S_Postpn_Amt.getValue(2).nadd(pnd_S_Postpn_Amt.getValue(1));                                                                                                  //Natural: ADD #S-POSTPN-AMT ( 1 ) TO #S-POSTPN-AMT ( 2 )
        //*  PALDE
        pnd_S_Repymnt_Amt.getValue(2).nadd(pnd_S_Repymnt_Amt.getValue(1));                                                                                                //Natural: ADD #S-REPYMNT-AMT ( 1 ) TO #S-REPYMNT-AMT ( 2 )
        pnd_L_Trans_Count.getValue(2).nadd(pnd_L_Trans_Count.getValue(1));                                                                                                //Natural: ADD #L-TRANS-COUNT ( 1 ) TO #L-TRANS-COUNT ( 2 )
        pnd_L_Classic_Amt.getValue(2).nadd(pnd_L_Classic_Amt.getValue(1));                                                                                                //Natural: ADD #L-CLASSIC-AMT ( 1 ) TO #L-CLASSIC-AMT ( 2 )
        pnd_L_Roth_Amt.getValue(2).nadd(pnd_L_Roth_Amt.getValue(1));                                                                                                      //Natural: ADD #L-ROTH-AMT ( 1 ) TO #L-ROTH-AMT ( 2 )
        pnd_L_Rollover_Amt.getValue(2).nadd(pnd_L_Rollover_Amt.getValue(1));                                                                                              //Natural: ADD #L-ROLLOVER-AMT ( 1 ) TO #L-ROLLOVER-AMT ( 2 )
        pnd_L_Rechar_Amt.getValue(2).nadd(pnd_L_Rechar_Amt.getValue(1));                                                                                                  //Natural: ADD #L-RECHAR-AMT ( 1 ) TO #L-RECHAR-AMT ( 2 )
        pnd_L_Sep_Amt.getValue(2).nadd(pnd_L_Sep_Amt.getValue(1));                                                                                                        //Natural: ADD #L-SEP-AMT ( 1 ) TO #L-SEP-AMT ( 2 )
        pnd_L_Fmv_Amt.getValue(2).nadd(pnd_L_Fmv_Amt.getValue(1));                                                                                                        //Natural: ADD #L-FMV-AMT ( 1 ) TO #L-FMV-AMT ( 2 )
        pnd_L_Roth_Conv_Amt.getValue(2).nadd(pnd_L_Roth_Conv_Amt.getValue(1));                                                                                            //Natural: ADD #L-ROTH-CONV-AMT ( 1 ) TO #L-ROTH-CONV-AMT ( 2 )
        //*  DASDH
        pnd_L_Postpn_Amt.getValue(2).nadd(pnd_L_Postpn_Amt.getValue(1));                                                                                                  //Natural: ADD #L-POSTPN-AMT ( 1 ) TO #L-POSTPN-AMT ( 2 )
        //*  PALDE
        pnd_L_Repymnt_Amt.getValue(2).nadd(pnd_L_Repymnt_Amt.getValue(1));                                                                                                //Natural: ADD #L-REPYMNT-AMT ( 1 ) TO #L-REPYMNT-AMT ( 2 )
        pnd_C_Trans_Count.getValue(2).nadd(pnd_C_Trans_Count.getValue(1));                                                                                                //Natural: ADD #C-TRANS-COUNT ( 1 ) TO #C-TRANS-COUNT ( 2 )
        pnd_C_Classic_Amt.getValue(2).nadd(pnd_C_Classic_Amt.getValue(1));                                                                                                //Natural: ADD #C-CLASSIC-AMT ( 1 ) TO #C-CLASSIC-AMT ( 2 )
        pnd_C_Roth_Amt.getValue(2).nadd(pnd_C_Roth_Amt.getValue(1));                                                                                                      //Natural: ADD #C-ROTH-AMT ( 1 ) TO #C-ROTH-AMT ( 2 )
        pnd_C_Rollover_Amt.getValue(2).nadd(pnd_C_Rollover_Amt.getValue(1));                                                                                              //Natural: ADD #C-ROLLOVER-AMT ( 1 ) TO #C-ROLLOVER-AMT ( 2 )
        pnd_C_Rechar_Amt.getValue(2).nadd(pnd_C_Rechar_Amt.getValue(1));                                                                                                  //Natural: ADD #C-RECHAR-AMT ( 1 ) TO #C-RECHAR-AMT ( 2 )
        pnd_C_Sep_Amt.getValue(2).nadd(pnd_C_Sep_Amt.getValue(1));                                                                                                        //Natural: ADD #C-SEP-AMT ( 1 ) TO #C-SEP-AMT ( 2 )
        pnd_C_Fmv_Amt.getValue(2).nadd(pnd_C_Fmv_Amt.getValue(1));                                                                                                        //Natural: ADD #C-FMV-AMT ( 1 ) TO #C-FMV-AMT ( 2 )
        pnd_C_Roth_Conv_Amt.getValue(2).nadd(pnd_C_Roth_Conv_Amt.getValue(1));                                                                                            //Natural: ADD #C-ROTH-CONV-AMT ( 1 ) TO #C-ROTH-CONV-AMT ( 2 )
        pnd_C_Postpn_Amt.getValue(2).nadd(pnd_C_Postpn_Amt.getValue(1));                                                                                                  //Natural: ADD #C-POSTPN-AMT ( 1 ) TO #C-POSTPN-AMT ( 2 )
    }
    private void sub_Reset_Control_Totals() throws Exception                                                                                                              //Natural: RESET-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        //*  DASDH
        //*  PALDE
        //*  DASDH
        //*  PALDE
        pnd_T_Trans_Count.getValue(1).reset();                                                                                                                            //Natural: RESET #T-TRANS-COUNT ( 1 ) #C-TRANS-COUNT ( 1 ) #T-CLASSIC-AMT ( 1 ) #C-CLASSIC-AMT ( 1 ) #T-ROTH-AMT ( 1 ) #C-ROTH-AMT ( 1 ) #T-ROLLOVER-AMT ( 1 ) #C-ROLLOVER-AMT ( 1 ) #T-RECHAR-AMT ( 1 ) #C-RECHAR-AMT ( 1 ) #T-SEP-AMT ( 1 ) #C-SEP-AMT ( 1 ) #T-FMV-AMT ( 1 ) #C-FMV-AMT ( 1 ) #T-ROTH-CONV-AMT ( 1 ) #C-ROTH-CONV-AMT ( 1 ) #T-POSTPN-AMT ( 1 ) #C-POSTPN-AMT ( 1 ) #T-REPYMNT-AMT ( 1 ) #C-REPYMNT-AMT ( 1 ) #L-TRANS-COUNT ( 1 ) #S-TRANS-COUNT ( 1 ) #L-CLASSIC-AMT ( 1 ) #S-CLASSIC-AMT ( 1 ) #L-ROTH-AMT ( 1 ) #S-ROTH-AMT ( 1 ) #L-ROLLOVER-AMT ( 1 ) #S-ROLLOVER-AMT ( 1 ) #L-RECHAR-AMT ( 1 ) #S-RECHAR-AMT ( 1 ) #L-SEP-AMT ( 1 ) #S-SEP-AMT ( 1 ) #L-FMV-AMT ( 1 ) #S-FMV-AMT ( 1 ) #L-ROTH-CONV-AMT ( 1 ) #S-ROTH-CONV-AMT ( 1 ) #L-POSTPN-AMT ( 1 ) #S-POSTPN-AMT ( 1 ) #L-REPYMNT-AMT ( 1 ) #S-REPYMNT-AMT ( 1 )
        pnd_C_Trans_Count.getValue(1).reset();
        pnd_T_Classic_Amt.getValue(1).reset();
        pnd_C_Classic_Amt.getValue(1).reset();
        pnd_T_Roth_Amt.getValue(1).reset();
        pnd_C_Roth_Amt.getValue(1).reset();
        pnd_T_Rollover_Amt.getValue(1).reset();
        pnd_C_Rollover_Amt.getValue(1).reset();
        pnd_T_Rechar_Amt.getValue(1).reset();
        pnd_C_Rechar_Amt.getValue(1).reset();
        pnd_T_Sep_Amt.getValue(1).reset();
        pnd_C_Sep_Amt.getValue(1).reset();
        pnd_T_Fmv_Amt.getValue(1).reset();
        pnd_C_Fmv_Amt.getValue(1).reset();
        pnd_T_Roth_Conv_Amt.getValue(1).reset();
        pnd_C_Roth_Conv_Amt.getValue(1).reset();
        pnd_T_Postpn_Amt.getValue(1).reset();
        pnd_C_Postpn_Amt.getValue(1).reset();
        pnd_T_Repymnt_Amt.getValue(1).reset();
        pnd_C_Repymnt_Amt.getValue(1).reset();
        pnd_L_Trans_Count.getValue(1).reset();
        pnd_S_Trans_Count.getValue(1).reset();
        pnd_L_Classic_Amt.getValue(1).reset();
        pnd_S_Classic_Amt.getValue(1).reset();
        pnd_L_Roth_Amt.getValue(1).reset();
        pnd_S_Roth_Amt.getValue(1).reset();
        pnd_L_Rollover_Amt.getValue(1).reset();
        pnd_S_Rollover_Amt.getValue(1).reset();
        pnd_L_Rechar_Amt.getValue(1).reset();
        pnd_S_Rechar_Amt.getValue(1).reset();
        pnd_L_Sep_Amt.getValue(1).reset();
        pnd_S_Sep_Amt.getValue(1).reset();
        pnd_L_Fmv_Amt.getValue(1).reset();
        pnd_S_Fmv_Amt.getValue(1).reset();
        pnd_L_Roth_Conv_Amt.getValue(1).reset();
        pnd_S_Roth_Conv_Amt.getValue(1).reset();
        pnd_L_Postpn_Amt.getValue(1).reset();
        pnd_S_Postpn_Amt.getValue(1).reset();
        pnd_L_Repymnt_Amt.getValue(1).reset();
        pnd_S_Repymnt_Amt.getValue(1).reset();
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source_Total_Header.setValue("  Report Totals  ");                                                                                                            //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '  Report Totals  '
        i1.setValue(2);                                                                                                                                                   //Natural: ASSIGN I1 := 2
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(0, new TabSetting(1),pnd_Trailer_Line,pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                         //Natural: WRITE ( 00 ) 01T #TRAILER-LINE #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Trailer_Line,pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                                    //Natural: WRITE ( 01 ) 01T #TRAILER-LINE #READ-CTR
        if (Global.isEscape()) return;
        if (condition(pnd_Report_Number.equals("1")))                                                                                                                     //Natural: IF #REPORT-NUMBER = '1' THEN
        {
            pnd_Total_B_Recs_Record.setValue(pnd_Read_Ctr);                                                                                                               //Natural: ASSIGN #TOTAL-B-RECS-RECORD := #READ-CTR
            getWorkFiles().write(2, false, pnd_Total_B_Recs_Record);                                                                                                      //Natural: WRITE WORK FILE 02 #TOTAL-B-RECS-RECORD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pnd_Total_Header() throws Exception                                                                                                                  //Natural: #TOTAL-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Prev_Form_Type.equals("10")))                                                                                                                   //Natural: IF #PREV-FORM-TYPE = '10'
        {
            pnd_Source_Total_Header.setValue("     Classic     ");                                                                                                        //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '     Classic     '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prev_Form_Type.equals("20")))                                                                                                               //Natural: IF #PREV-FORM-TYPE = '20'
            {
                pnd_Source_Total_Header.setValue("      Roth       ");                                                                                                    //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      Roth       '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  BK
                if (condition(pnd_Prev_Form_Type.equals("30")))                                                                                                           //Natural: IF #PREV-FORM-TYPE = '30'
                {
                    pnd_Source_Total_Header.setValue("    ROLLOVERS    ");                                                                                                //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '    ROLLOVERS    '
                    //*  BK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Source_Total_Header.setValue("      SEP        ");                                                                                                //Natural: ASSIGN #SOURCE-TOTAL-HEADER := '      SEP        '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(48),pnd_Header_Line,new TabSetting(120),      //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 48T #HEADER-LINE 120T 'REPORT: RPT1'
                        "REPORT: RPT1");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(58),pnd_Source_Total_Header,new  //Natural: WRITE ( 01 ) NOTITLE 25T #HEADER-DATE ( EM = MM/DD/YYYY ) 58T #SOURCE-TOTAL-HEADER 97T #HEADER-DATE ( EM = MM/DD/YYYY )
                        TabSetting(97),pnd_Header_Date, new ReportEditMask ("MM/DD/YYYY"));
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(22),"       Input        ");                                                                //Natural: WRITE ( 01 ) NOTITLE 22T '       Input        '
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                    ",new TabSetting(22),"====================");                       //Natural: WRITE ( 01 ) NOTITLE 01T '                    ' 22T '===================='
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
        pnd_Source_Total_Header.setValue(DbsUtil.compress("Source System: IR"));                                                                                          //Natural: COMPRESS 'Source System: IR' INTO #SOURCE-TOTAL-HEADER
        i1.setValue(1);                                                                                                                                                   //Natural: ASSIGN I1 := 1
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-TOTALS
        sub_Display_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
