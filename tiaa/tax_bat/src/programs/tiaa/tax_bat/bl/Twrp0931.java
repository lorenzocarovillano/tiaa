/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:03 PM
**        * FROM NATURAL PROGRAM : Twrp0931
************************************************************
**        * FILE NAME            : Twrp0931.java
**        * CLASS NAME           : Twrp0931
**        * INSTANCE NAME        : Twrp0931
************************************************************
************************************************************************
** EXTRACT PARTICIPANT SSSN FOR LEAN FINANCIAL
** HISTORY
**    12/30/2014 FE - FATCA CHANGES. RESTOW TO PICKUP FIELDS IN TWRA5006
**    07/10/2015 FE - COR AND NAS SUNSET PROCOBOL VERSION. FE201507
**
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0931 extends BLNatBase
{
    // Data Areas
    private LdaTwrlpsg1 ldaTwrlpsg1;
    private LdaTwrlpsg3 ldaTwrlpsg3;
    private LdaTwrl9515 ldaTwrl9515;
    private LdaTwrl9520 ldaTwrl9520;
    private PdaTwratin pdaTwratin;
    private PdaTwra5020 pdaTwra5020;
    private PdaTwra5006 pdaTwra5006;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Tax_Year_Start;
    private DbsField pnd_Tax_Year_End;
    private DbsField pnd_Wf_Record;

    private DbsGroup pnd_Wf_Record__R_Field_1;
    private DbsField pnd_Wf_Record_Pnd_Wf_Fill1;
    private DbsField pnd_Wf_Record_Pnd_Wf_Ssn;
    private DbsField pnd_Wf_Record_Pnd_Wf_Fill2;
    private DbsField pnd_Wf_Record_Pnd_Wf_Name;
    private DbsField pnd_Wf_Limit;
    private DbsField pnd_Part_Key;

    private DbsGroup pnd_Part_Key__R_Field_2;
    private DbsField pnd_Part_Key_Pnd_Part_Key_Tin;

    private DbsGroup pnd_Part_Key__R_Field_3;
    private DbsField pnd_Part_Key_Pnd_Part_Key_Ssn;
    private DbsField pnd_Part_Key_Pnd_Part_Key_Status;
    private DbsField pnd_Part_Key_Pnd_Part_Key_Inv_Dt;
    private DbsField pnd_Year_Tin;

    private DbsGroup pnd_Year_Tin__R_Field_4;
    private DbsField pnd_Year_Tin_Pnd_Year;
    private DbsField pnd_Year_Tin_Pnd_Taxid;

    private DbsGroup pnd_Local;
    private DbsField pnd_Local_Pnd_Form_Cnt;
    private DbsField pnd_Datn;
    private DbsField pnd_Scrn_1st_Tax_Year;
    private DbsField pnd_Scrn_Last_Tax_Year;
    private DbsField pnd_Scrn_1st_Form_Year;
    private DbsField pnd_Scrn_Last_Form_Year;

    private DbsGroup pnd_Counts;
    private DbsField pnd_Counts_Pnd_Cnt_Form;
    private DbsField pnd_Counts_Pnd_Cnt_Part;
    private DbsField pnd_Counts_Pnd_Cnt_Part2;
    private DbsField pnd_Counts_Pnd_Wf_Cnt;
    private DbsField pnd_Counts_Pnd_Rd_Cnt;
    private DbsField pnd_Counts_Pnd_Part_Act_Nf_Cnt;
    private DbsField pnd_Counts_Pnd_Part_Act_Cnt;
    private DbsField pnd_Counts_Pnd_Wf_Rec_Processed_Cnt;
    private DbsField pnd_Counts_Pnd_Part_Not_Ein_Cnt;
    private DbsField pnd_Counts_Pnd_Part_Inact_Cnt;
    private DbsField pnd_Counts_Pnd_No_Name_Addr_Cnt;
    private DbsField pnd_Counts_Pnd_Already_Have_Cnt;
    private DbsField pnd_Counts_Pnd_Et_Cnt;
    private DbsField pnd_New_Tax_Id_Type;
    private DbsField pnd_Name_Addr_Upd;
    private DbsField pnd_Prev_Tin;
    private DbsField pnd_Name;
    private DbsField pnd_Addr1;
    private DbsField pnd_Addr2;
    private DbsField pnd_Addr3;
    private DbsField pnd_Addr4;
    private DbsField pnd_Addr5;
    private DbsField pnd_Addr6;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_5;
    private DbsField pnd_Dob_Pnd_Dob_A;
    private DbsField pnd_Tin_Type_5;
    private DbsField pnd_Write_Sw;
    private DbsField pnd_Cnt_001co;
    private DbsField pnd_Cnt_001rs;
    private DbsField pnd_Cnt_Bad;
    private DbsField pnd_Cnt_003;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlpsg1 = new LdaTwrlpsg1();
        registerRecord(ldaTwrlpsg1);
        ldaTwrlpsg3 = new LdaTwrlpsg3();
        registerRecord(ldaTwrlpsg3);
        ldaTwrl9515 = new LdaTwrl9515();
        registerRecord(ldaTwrl9515);
        registerRecord(ldaTwrl9515.getVw_part());
        ldaTwrl9520 = new LdaTwrl9520();
        registerRecord(ldaTwrl9520);
        registerRecord(ldaTwrl9520.getVw_partu());
        localVariables = new DbsRecord();
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwra5020 = new PdaTwra5020(localVariables);
        pdaTwra5006 = new PdaTwra5006(localVariables);

        // Local Variables
        pnd_Tax_Year_Start = localVariables.newFieldInRecord("pnd_Tax_Year_Start", "#TAX-YEAR-START", FieldType.NUMERIC, 4);
        pnd_Tax_Year_End = localVariables.newFieldInRecord("pnd_Tax_Year_End", "#TAX-YEAR-END", FieldType.NUMERIC, 4);
        pnd_Wf_Record = localVariables.newFieldInRecord("pnd_Wf_Record", "#WF-RECORD", FieldType.STRING, 282);

        pnd_Wf_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Wf_Record__R_Field_1", "REDEFINE", pnd_Wf_Record);
        pnd_Wf_Record_Pnd_Wf_Fill1 = pnd_Wf_Record__R_Field_1.newFieldInGroup("pnd_Wf_Record_Pnd_Wf_Fill1", "#WF-FILL1", FieldType.STRING, 2);
        pnd_Wf_Record_Pnd_Wf_Ssn = pnd_Wf_Record__R_Field_1.newFieldInGroup("pnd_Wf_Record_Pnd_Wf_Ssn", "#WF-SSN", FieldType.STRING, 10);
        pnd_Wf_Record_Pnd_Wf_Fill2 = pnd_Wf_Record__R_Field_1.newFieldInGroup("pnd_Wf_Record_Pnd_Wf_Fill2", "#WF-FILL2", FieldType.STRING, 9);
        pnd_Wf_Record_Pnd_Wf_Name = pnd_Wf_Record__R_Field_1.newFieldInGroup("pnd_Wf_Record_Pnd_Wf_Name", "#WF-NAME", FieldType.STRING, 35);
        pnd_Wf_Limit = localVariables.newFieldInRecord("pnd_Wf_Limit", "#WF-LIMIT", FieldType.NUMERIC, 7);
        pnd_Part_Key = localVariables.newFieldInRecord("pnd_Part_Key", "#PART-KEY", FieldType.STRING, 19);

        pnd_Part_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Part_Key__R_Field_2", "REDEFINE", pnd_Part_Key);
        pnd_Part_Key_Pnd_Part_Key_Tin = pnd_Part_Key__R_Field_2.newFieldInGroup("pnd_Part_Key_Pnd_Part_Key_Tin", "#PART-KEY-TIN", FieldType.STRING, 10);

        pnd_Part_Key__R_Field_3 = pnd_Part_Key__R_Field_2.newGroupInGroup("pnd_Part_Key__R_Field_3", "REDEFINE", pnd_Part_Key_Pnd_Part_Key_Tin);
        pnd_Part_Key_Pnd_Part_Key_Ssn = pnd_Part_Key__R_Field_3.newFieldInGroup("pnd_Part_Key_Pnd_Part_Key_Ssn", "#PART-KEY-SSN", FieldType.STRING, 10);
        pnd_Part_Key_Pnd_Part_Key_Status = pnd_Part_Key__R_Field_2.newFieldInGroup("pnd_Part_Key_Pnd_Part_Key_Status", "#PART-KEY-STATUS", FieldType.STRING, 
            1);
        pnd_Part_Key_Pnd_Part_Key_Inv_Dt = pnd_Part_Key__R_Field_2.newFieldInGroup("pnd_Part_Key_Pnd_Part_Key_Inv_Dt", "#PART-KEY-INV-DT", FieldType.STRING, 
            8);
        pnd_Year_Tin = localVariables.newFieldInRecord("pnd_Year_Tin", "#YEAR-TIN", FieldType.STRING, 14);

        pnd_Year_Tin__R_Field_4 = localVariables.newGroupInRecord("pnd_Year_Tin__R_Field_4", "REDEFINE", pnd_Year_Tin);
        pnd_Year_Tin_Pnd_Year = pnd_Year_Tin__R_Field_4.newFieldInGroup("pnd_Year_Tin_Pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Year_Tin_Pnd_Taxid = pnd_Year_Tin__R_Field_4.newFieldInGroup("pnd_Year_Tin_Pnd_Taxid", "#TAXID", FieldType.STRING, 10);

        pnd_Local = localVariables.newGroupInRecord("pnd_Local", "#LOCAL");
        pnd_Local_Pnd_Form_Cnt = pnd_Local.newFieldInGroup("pnd_Local_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);
        pnd_Scrn_1st_Tax_Year = localVariables.newFieldInRecord("pnd_Scrn_1st_Tax_Year", "#SCRN-1ST-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Scrn_Last_Tax_Year = localVariables.newFieldInRecord("pnd_Scrn_Last_Tax_Year", "#SCRN-LAST-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Scrn_1st_Form_Year = localVariables.newFieldInRecord("pnd_Scrn_1st_Form_Year", "#SCRN-1ST-FORM-YEAR", FieldType.NUMERIC, 4);
        pnd_Scrn_Last_Form_Year = localVariables.newFieldInRecord("pnd_Scrn_Last_Form_Year", "#SCRN-LAST-FORM-YEAR", FieldType.NUMERIC, 4);

        pnd_Counts = localVariables.newGroupInRecord("pnd_Counts", "#COUNTS");
        pnd_Counts_Pnd_Cnt_Form = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Cnt_Form", "#CNT-FORM", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Cnt_Part = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Cnt_Part", "#CNT-PART", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Cnt_Part2 = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Cnt_Part2", "#CNT-PART2", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Wf_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Wf_Cnt", "#WF-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Rd_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Rd_Cnt", "#RD-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Part_Act_Nf_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Part_Act_Nf_Cnt", "#PART-ACT-NF-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Part_Act_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Part_Act_Cnt", "#PART-ACT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_Wf_Rec_Processed_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Wf_Rec_Processed_Cnt", "#WF-REC-PROCESSED-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counts_Pnd_Part_Not_Ein_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Part_Not_Ein_Cnt", "#PART-NOT-EIN-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counts_Pnd_Part_Inact_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Part_Inact_Cnt", "#PART-INACT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Counts_Pnd_No_Name_Addr_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_No_Name_Addr_Cnt", "#NO-NAME-ADDR-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counts_Pnd_Already_Have_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Already_Have_Cnt", "#ALREADY-HAVE-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counts_Pnd_Et_Cnt = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Et_Cnt", "#ET-CNT", FieldType.INTEGER, 2);
        pnd_New_Tax_Id_Type = localVariables.newFieldInRecord("pnd_New_Tax_Id_Type", "#NEW-TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_Name_Addr_Upd = localVariables.newFieldInRecord("pnd_Name_Addr_Upd", "#NAME-ADDR-UPD", FieldType.BOOLEAN, 1);
        pnd_Prev_Tin = localVariables.newFieldInRecord("pnd_Prev_Tin", "#PREV-TIN", FieldType.STRING, 10);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 40);
        pnd_Addr1 = localVariables.newFieldInRecord("pnd_Addr1", "#ADDR1", FieldType.STRING, 40);
        pnd_Addr2 = localVariables.newFieldInRecord("pnd_Addr2", "#ADDR2", FieldType.STRING, 40);
        pnd_Addr3 = localVariables.newFieldInRecord("pnd_Addr3", "#ADDR3", FieldType.STRING, 40);
        pnd_Addr4 = localVariables.newFieldInRecord("pnd_Addr4", "#ADDR4", FieldType.STRING, 40);
        pnd_Addr5 = localVariables.newFieldInRecord("pnd_Addr5", "#ADDR5", FieldType.STRING, 40);
        pnd_Addr6 = localVariables.newFieldInRecord("pnd_Addr6", "#ADDR6", FieldType.STRING, 40);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.NUMERIC, 8);

        pnd_Dob__R_Field_5 = localVariables.newGroupInRecord("pnd_Dob__R_Field_5", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_A = pnd_Dob__R_Field_5.newFieldInGroup("pnd_Dob_Pnd_Dob_A", "#DOB-A", FieldType.STRING, 8);
        pnd_Tin_Type_5 = localVariables.newFieldInRecord("pnd_Tin_Type_5", "#TIN-TYPE-5", FieldType.BOOLEAN, 1);
        pnd_Write_Sw = localVariables.newFieldInRecord("pnd_Write_Sw", "#WRITE-SW", FieldType.BOOLEAN, 1);
        pnd_Cnt_001co = localVariables.newFieldInRecord("pnd_Cnt_001co", "#CNT-001CO", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_001rs = localVariables.newFieldInRecord("pnd_Cnt_001rs", "#CNT-001RS", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_Bad = localVariables.newFieldInRecord("pnd_Cnt_Bad", "#CNT-BAD", FieldType.PACKED_DECIMAL, 9);
        pnd_Cnt_003 = localVariables.newFieldInRecord("pnd_Cnt_003", "#CNT-003", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlpsg1.initializeValues();
        ldaTwrlpsg3.initializeValues();
        ldaTwrl9515.initializeValues();
        ldaTwrl9520.initializeValues();

        localVariables.reset();
        pnd_Tax_Year_Start.setInitialValue(2010);
        pnd_Tax_Year_End.setInitialValue(2011);
        pnd_Wf_Limit.setInitialValue(9999999);
        pnd_New_Tax_Id_Type.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0931() throws Exception
    {
        super("Twrp0931", true);
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  -----------------------------------
        setKeys(ControlKeys.PF5, "%<40", (String)null);                                                                                                                   //Natural: FORMAT ( 0 ) LS = 132 PS = 58;//Natural: FORMAT ( 1 ) LS = 132 PS = 58;//Natural: FORMAT ( 2 ) LS = 132 PS = 58;//Natural: SET KEY PF5 = '%<40' PF6 = '%>40'
        setKeys(ControlKeys.PF6, "%>40", (String)null);
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*   MAIN PROCESSING
        pnd_Part_Key_Pnd_Part_Key_Status.setValue(" ");                                                                                                                   //Natural: ASSIGN #PART-KEY-STATUS := ' '
        pnd_Part_Key_Pnd_Part_Key_Inv_Dt.setValue("H'0000000000000000'");                                                                                                 //Natural: MOVE H'0000000000000000' TO #PART-KEY-INV-DT
        ldaTwrl9515.getVw_part().startDatabaseRead                                                                                                                        //Natural: READ PART BY TWRPARTI-CURR-INVRSE-SD FROM #PART-KEY
        (
        "RD1",
        new Wc[] { new Wc("TWRPARTI_CURR_INVRSE_SD", ">=", pnd_Part_Key, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_INVRSE_SD", "ASC") }
        );
        RD1:
        while (condition(ldaTwrl9515.getVw_part().readNextRow("RD1")))
        {
            pnd_Name.reset();                                                                                                                                             //Natural: RESET #NAME #DOB #ADDR1 #ADDR2 #ADDR3 #ADDR4 #ADDR5 #ADDR6
            pnd_Dob.reset();
            pnd_Addr1.reset();
            pnd_Addr2.reset();
            pnd_Addr3.reset();
            pnd_Addr4.reset();
            pnd_Addr5.reset();
            pnd_Addr6.reset();
            if (condition(pnd_Prev_Tin.notEquals(ldaTwrl9515.getPart_Twrparti_Tax_Id())))                                                                                 //Natural: IF #PREV-TIN NE TWRPARTI-TAX-ID
            {
                if (condition(ldaTwrl9515.getPart_Twrparti_Part_Eff_End_Dte().notEquals("99999999")))                                                                     //Natural: IF TWRPARTI-PART-EFF-END-DTE NE '99999999'
                {
                    getReports().write(0, "NEW TIN NOT 99999999",ldaTwrl9515.getPart_Twrparti_Tax_Id(),ldaTwrl9515.getPart_Twrparti_Status());                            //Natural: WRITE 'NEW TIN NOT 99999999' TWRPARTI-TAX-ID TWRPARTI-STATUS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  FE201507
                if (condition(ldaTwrl9515.getPart_Twrparti_Status().equals(" ")))                                                                                         //Natural: IF TWRPARTI-STATUS = ' '
                {
                    pnd_Tin_Type_5.setValue(false);                                                                                                                       //Natural: ASSIGN #TIN-TYPE-5 := FALSE
                    //*        PERFORM GET-ADDRESS
                    //*        IF #RT-RCRD-STATUS EQ 'Y'
                    //*          COMPRESS #PSGM001.#RT-FIRST-NAME
                    //*            #PSGM001.#RT-SECOND-NAME
                    //*            #PSGM001.#RT-LAST-NAME INTO #NAME
                    //*          #DOB-A := #PSGM001.#RT-BIRTH-DATE
                    //*          #ADDR1 := #PSGM001.#RT-ADDR-1
                    //*          #ADDR2 := #PSGM001.#RT-ADDR-2
                    //*          #ADDR3 := #PSGM001.#RT-ADDR-3
                    //*          #ADDR4 := #PSGM001.#RT-ADDR-4
                    //*          #ADDR5 := #PSGM001.#RT-ADDR-5
                    //*          RESET #ADDR6
                    //*        ELSE
                    //*          IF #RT-RECORD-STATUS  = 'Y' AND #TIN-TYPE-5
                    //*            COMPRESS #PSGM003.#RT-FIRST-NAME
                    //*              #PSGM003.#RT-GIVEN-NAME-TWO
                    //*              #PSGM003.#RT-LAST-NAME INTO #NAME
                    //*            #DOB-A := TWRPARTI-PARTICIPANT-DOB /* NO DOB IN PSGM003
                    //*            #ADDR1 := #PSGM003.#RT-CO-ADDR-LINE-ONE
                    //*            #ADDR2 := #PSGM003.#RT-CO-ADDR-LINE-TWO
                    //*            #ADDR3 := #PSGM003.#RT-CO-ADDR-LINE-THREE
                    //*            #ADDR4 := #PSGM003.#RT-CO-ADDR-LINE-FOUR
                    //*            #ADDR5 := #PSGM003.#RT-CO-ADDR-LINE-FIVE
                    //*            RESET #ADDR6
                    //*         END-IF
                    //*        END-IF
                    //*      IF    #DOB-A = '00000000' OR = ' ' OR = '0       '
                    //*        #DOB-A :=  TWRPARTI-PARTICIPANT-DOB
                    //*      END-IF
                    //*      IF    #ADDR1 = ' '
                    //*        #ADDR1 :=      TWRPARTI-ADDR-LN1
                    //*        #ADDR2 :=      TWRPARTI-ADDR-LN2
                    //*        #ADDR3 :=      TWRPARTI-ADDR-LN3
                    //*        #ADDR4 :=      TWRPARTI-ADDR-LN4
                    //*        #ADDR5 :=      TWRPARTI-ADDR-LN5
                    //*        #ADDR6 :=      TWRPARTI-ADDR-LN6
                    //*      END-IF
                    //*      IF #NAME = ' '
                    //*        #NAME := TWRPARTI-PARTICIPANT-NAME
                    //*      END-IF
                    //*  10 >>  1  - 11
                    //*  40 >> 12  - 52
                    //*   8 >> 53  - 61
                    //*  40 >> 62  - 102
                    //*  40 >> 103 - 143
                    //*  40 >> 144 - 184
                    //*  40 >> 185 - 225
                    //*  40 >> 226 - 266
                    //*  40 >> 267 - 307
                    getWorkFiles().write(1, false, ldaTwrl9515.getPart_Twrparti_Tax_Id(), "|", pnd_Name, "|", pnd_Dob, "|", pnd_Addr1, "|", pnd_Addr2,                    //Natural: WRITE WORK FILE 1 TWRPARTI-TAX-ID '|' #NAME '|' #DOB '|' #ADDR1 '|' #ADDR2 '|' #ADDR3 '|' #ADDR4 '|' #ADDR5 '|' #ADDR6 '|'
                        "|", pnd_Addr3, "|", pnd_Addr4, "|", pnd_Addr5, "|", pnd_Addr6, "|");
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "99999999 END DATE NOT ACTIVE",ldaTwrl9515.getPart_Twrparti_Tax_Id(),ldaTwrl9515.getPart_Twrparti_Status());                    //Natural: WRITE '99999999 END DATE NOT ACTIVE' TWRPARTI-TAX-ID TWRPARTI-STATUS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Tin.setValue(ldaTwrl9515.getPart_Twrparti_Tax_Id());                                                                                                 //Natural: ASSIGN #PREV-TIN := TWRPARTI-TAX-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TIN Records Read             :",pnd_Counts_Pnd_Rd_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TIN Records Written          :",pnd_Counts_Pnd_Wf_Cnt,  //Natural: WRITE / 'TIN Records Read             :' #RD-CNT ( EM = Z,ZZZ,ZZ9 ) / 'TIN Records Written          :' #WF-CNT ( EM = Z,ZZZ,ZZ9 ) / 'Active Participant Records   :' #PART-ACT-CNT ( EM = Z,ZZZ,ZZ9 ) / 'Active Participant Not Found :' #PART-ACT-NF-CNT ( EM = Z,ZZZ,ZZ9 ) / 'Inactive Participant Found   :' #PART-INACT-CNT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Active Participant Records   :",pnd_Counts_Pnd_Part_Act_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Active Participant Not Found :",pnd_Counts_Pnd_Part_Act_Nf_Cnt, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"Inactive Participant Found   :",pnd_Counts_Pnd_Part_Inact_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  /  'Successful PSGM001 CO CALL   :' #CNT-001CO          (EM=Z,ZZZ,ZZ9)
        //*  /  'SUCCESSFUL PSGM001 RS CALL   :' #CNT-001RS          (EM=Z,ZZZ,ZZ9)
        //*  /  'SUCCESSFUL PSGM003 CALL      :' #CNT-003            (EM=Z,ZZZ,ZZ9)
        //*  /  'No Records Found             :' #CNT-BAD            (EM=Z,ZZZ,ZZ9)
        //*  /  'TINs Updated w/Name&Address  :' #CNT-PART2          (EM=Z,ZZZ,ZZ9)
        //*  /  'TINs Not Updated w/Name&Addr :' #NO-NAME-ADDR-CNT   (EM=Z,ZZZ,ZZ9)
        //*  / 'TINs Already Have Name&Addr  :' #ALREADY-HAVE-CNT   (EM=Z,ZZZ,ZZ9)
        //*  / 'Non-SSN Participant on File  :' #PART-NOT-EIN-CNT   (EM=Z,ZZZ,ZZ9)
        //*  / 'Curr Part. Recs Updated      :' #CNT-PART2          (EM=Z,ZZZ,ZZ9)
        //*  / 'Tot Part. Recs Updated       :' #CNT-PART            (EM=Z,ZZZ,ZZ9)
        //*   /
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-CONTROL-FIELDS
        //* ********************************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDRESS
        //* ****************************
        //* ************************
    }
    private void sub_Initialize_Control_Fields() throws Exception                                                                                                         //Natural: INITIALIZE-CONTROL-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET PART *ISN ( RD1. )
        ldaTwrl9515.getVw_part().readByID(ldaTwrl9515.getVw_part().getAstISN("RD1"), "G1");
        pnd_Scrn_Last_Tax_Year.setValue(pnd_Tax_Year_End);                                                                                                                //Natural: ASSIGN #SCRN-LAST-TAX-YEAR := #TAX-YEAR-END
        pnd_Scrn_1st_Tax_Year.setValue(pnd_Tax_Year_Start);                                                                                                               //Natural: ASSIGN #SCRN-1ST-TAX-YEAR := #TAX-YEAR-START
        pnd_Scrn_Last_Form_Year.setValue(pnd_Tax_Year_End);                                                                                                               //Natural: ASSIGN #SCRN-LAST-FORM-YEAR := #TAX-YEAR-END
        pnd_Scrn_1st_Form_Year.setValue(pnd_Tax_Year_Start);                                                                                                              //Natural: ASSIGN #SCRN-1ST-FORM-YEAR := #TAX-YEAR-START
    }
    private void sub_Get_Address() throws Exception                                                                                                                       //Natural: GET-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Year_Tin_Pnd_Taxid.setValue(ldaTwrl9515.getPart_Twrparti_Tax_Id());                                                                                           //Natural: ASSIGN #TAXID := TWRPARTI-TAX-ID
        //*  #TWRAPART.#FUNCTION      := 'C'  /* FE201507 START COMMENT OUT
        //*  #TWRAPART.#TIN           := TWRPARTI-TAX-ID
        //*  #TWRAPART.#TIN-TYPE      := '1'  /* SPECIFY SSN (EVEN THOUGH EIN)
        //*  #TWRAPART.#CONTRACT-NBR  := ' '
        //*  #TWRAPART.#PAYEE-CDE     := ' '
        //*  #TWRAPART.#TNAD-ADDR     := FALSE
        //*  #TWRAPART.#ABEND-IND     := FALSE
        //*  #TWRAPART.#DISPLAY-IND   := FALSE
        //*  #TWRAPART.#DEBUG-IND     := FALSE /* FE201507 END COMMENT OUT
        //*  CALLNAT 'TWRNPART' USING #TWRAPART
        if (condition(DbsUtil.maskMatches(ldaTwrl9515.getPart_Twrparti_Tax_Id().getSubstring(1,9),"999999999") && ldaTwrl9515.getPart_Twrparti_Tax_Id().getSubstring(10,  //Natural: IF SUBSTR ( TWRPARTI-TAX-ID,1,9 ) = MASK ( 999999999 ) AND SUBSTR ( TWRPARTI-TAX-ID,10,1 ) = ' '
            1).equals(" ")))
        {
            //*  FE201505 START
            ldaTwrlpsg1.getPnd_Psgm001().reset();                                                                                                                         //Natural: RESET #PSGM001
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Function().setValue("PR");                                                                                                  //Natural: ASSIGN #PSGM001.#IN-FUNCTION := 'PR'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Addr_Usg_Typ().setValue("CO");                                                                                              //Natural: ASSIGN #PSGM001.#IN-ADDR-USG-TYP := 'CO'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Type().setValue("SSN");                                                                                                     //Natural: ASSIGN #PSGM001.#IN-TYPE := 'SSN'
            ldaTwrlpsg1.getPnd_Psgm001_Pnd_In_Pin_Ssn().setValue(ldaTwrl9515.getPart_Twrparti_Tax_Id());                                                                  //Natural: MOVE TWRPARTI-TAX-ID TO #PSGM001.#IN-PIN-SSN
            //*  CALL 'PSGM001' #PSGM001
            //*  IF #RT-RCRD-STATUS EQ 'Y'
            //*      AND  (#PSGM001.#RT-LAST-NAME NE ' '
            //*      OR  #PSGM001.#RT-ADDR-1 NE ' ')
            //*    IGNORE
            //*  ELSE
            //*    RESET #PSGM001             /* FE201505 START
            //*    #PSGM001.#IN-FUNCTION      := 'PR'
            //*    #PSGM001.#IN-ADDR-USG-TYP  := 'RS'
            //*    #PSGM001.#IN-TYPE          := 'SSN'
            //*    MOVE TWRPARTI-TAX-ID TO #PSGM001.#IN-PIN-SSN
            //*    CALL 'PSGM001' #PSGM001
            //*    IF #RT-RCRD-STATUS EQ 'Y'
            //*        AND  (#PSGM001.#RT-LAST-NAME NE ' '
            //*        OR  #PSGM001.#RT-ADDR-1 NE ' ')
            //*      IGNORE
            //*    ELSE
            //*      WRITE /
            //*        'Return Code:'    #RT-RCRD-STATUS         /* (EM=BAD/GOOD)
            //*        'Return Msg :'    ' not on MDM PSGM001'
            //*        'TIN        :'    TWRPARTI-TAX-ID
            //*    END-IF
            //*  END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tin_Type_5.setValue(true);                                                                                                                                //Natural: ASSIGN #TIN-TYPE-5 := TRUE
            ldaTwrlpsg3.getPnd_Psgm003().reset();                                                                                                                         //Natural: RESET #PSGM003
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Function().setValue("PR");                                                                                                  //Natural: ASSIGN #PSGM003.#IN-FUNCTION := 'PR'
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Contract_Num_Typ().setValue("T");                                                                                           //Natural: ASSIGN #PSGM003.#IN-CONTRACT-NUM-TYP := 'T'
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Type().setValue(" ");                                                                                                       //Natural: ASSIGN #PSGM003.#IN-TYPE := ' '
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Ssnpin().setValue(" ");                                                                                                     //Natural: ASSIGN #PSGM003.#IN-SSNPIN := ' '
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Contract().setValue(ldaTwrl9515.getPart_Twrparti_Tax_Id().getSubstring(1,8));                                               //Natural: MOVE SUBSTR ( TWRPARTI-TAX-ID,1,8 ) TO #PSGM003.#IN-CONTRACT
            ldaTwrlpsg3.getPnd_Psgm003_Pnd_In_Payee_Cd().setValue(ldaTwrl9515.getPart_Twrparti_Tax_Id().getSubstring(9,2));                                               //Natural: MOVE SUBSTR ( TWRPARTI-TAX-ID,9,2 ) TO #PSGM003.#IN-PAYEE-CD
            //*  CALL    'PSGM003' USING #PSGM003
            //*  IF #RT-RECORD-STATUS   = 'Y'
            //*      AND  (#PSGM001.#RT-LAST-NAME NE ' '
            //*      OR  #PSGM001.#RT-ADDR-1 NE ' ')
            //*    IGNORE
            //*  ELSE
            //*    WRITE /
            //*      'RETURN CODE:'    #RT-RECORD-STATUS       /* (EM=BAD/GOOD)
            //*      'Return Msg :'    ' not on MDM PSGM003'
            //*      'TIN        :'    TWRPARTI-TAX-ID
            //*    STOP
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=58");
        Global.format(1, "LS=132 PS=58");
        Global.format(2, "LS=132 PS=58");
    }
}
