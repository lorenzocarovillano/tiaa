/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:20 PM
**        * FROM NATURAL PROGRAM : Twrp4910
************************************************************
**        * FILE NAME            : Twrp4910.java
**        * CLASS NAME           : Twrp4910
**        * INSTANCE NAME        : Twrp4910
************************************************************
************************************************************************
* PROGRAM    : TWRP4910
* SYSTEM     : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE      : CREATES BATCH EXTRACT OF ALL IRS "5498" CORRECTION FORMS
*              FROM THE FORM DATA BASE FILE.
* CREATED    : 03 / 07 / 2002.
*   BY       : RIAD LOUTFI.
* FUNCTION   : PROGRAM READS THE FORM DATA BASE FILE TO PRODUCE AN
*              EXTRACT OF ALL 5498 CORRECTION FORM RECORDS FOR
*              IRS CORRECTION REPORTING TAX YEAR 2001 & LATER YEARS.
* HISTORY
* 08/01/13  R. CARREON   ROX
*           LINK TINS
* 08/15/07  R. MA
*           REVISE LOGIC TO CORRECT ERROR OF WRITING REJECTED RECORDS
*           OUT ONTO THE IRS FILE (WKF1).
* 11/29/06  J.ROTHOLZ
*           REMOVE LOGIC TO DETERMINE IRA TYPE BY CONTRACT RANGE AND
*           INSTEAD TAKE IRA TYPE FROM DATA STORED ON FORM FILE
* 06/30/06  R. MA
*           ADD TIRF-SEP-AMT TO LDA TWRL451A AND TWRL451B.
*           ADD SEP-AMT TO ALL TOTAL FIELDS (BY REACTIVATE THE COMMENTED
*           OUT EDUCATION-AMT FIELDS). UPDATED ALL RELATED LOGIC AND
*           REALIAGNED REPORT PRINT COLUMNS.
* 07-13-2005 : A. YOUNG      - TRUNCATION ERROR FOR #T-FMV-AMT,
*            :                 #G-FMV-AMT, AND #R-FMV-AMT.  INCREASED
*            :                 FORMAT OF ALL FMV VARIABLES BY 2 BYTES.
* 02/09/17  STOW FOR TWRL4001 NEW ONEIRA CONTRACT RANGES - JB
* 10/05/18 - ARIVU - EIN CHANGES - TAG: EINCHG
* 23-11-20 : PALDE           - IRS REPORTING 2020  CHANGES
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4910 extends BLNatBase
{
    // Data Areas
    private LdaTwrl451a ldaTwrl451a;
    private LdaTwrl451b ldaTwrl451b;
    private LdaTwrl4001 ldaTwrl4001;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_Tbl_Nbr;
    private DbsField cntl_Tircntl_Tax_Year;
    private DbsField cntl_Tircntl_Rpt_Form_Name;
    private DbsField cntl_Count_Casttircntl_Rpt_Tbl_Pe;

    private DbsGroup cntl_Tircntl_Rpt_Tbl_Pe;
    private DbsField cntl_Tircntl_Rpt_Dte;

    private DataAccessProgramView vw_hform;
    private DbsField hform_Tirf_Superde_4;

    private DbsGroup hform__R_Field_1;
    private DbsField hform_Pnd_Tirf_Tax_Year;
    private DbsField hform_Pnd_Tirf_Tin;
    private DbsField hform_Pnd_Tirf_Form_Type;
    private DbsField hform_Pnd_Tirf_Contract_Nbr;
    private DbsField hform_Pnd_Tirf_Payee_Cde;
    private DbsField hform_Pnd_Tirf_Key;
    private DbsField hform_Pnd_Tirf_Invrse_Create_Ts;
    private DbsField hform_Pnd_Tirf_Irs_Rpt_Ind;
    private DbsField pnd_Tirf_Superde_3_1;

    private DbsGroup pnd_Tirf_Superde_3_1__R_Field_2;
    private DbsField pnd_Tirf_Superde_3_1_Pnd_S3_Tax_Year;
    private DbsField pnd_Tirf_Superde_3_1_Pnd_S3_Active_Ind;
    private DbsField pnd_Tirf_Superde_3_1_Pnd_S3_Form_Type;
    private DbsField pnd_Tirf_Superde_4_1;

    private DbsGroup pnd_Tirf_Superde_4_1__R_Field_3;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Tin;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Key;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts;
    private DbsField pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind;
    private DbsField pnd_Super_Cntl;

    private DbsGroup pnd_Super_Cntl__R_Field_4;
    private DbsField pnd_Super_Cntl_Pnd_S_Tbl;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year;

    private DbsGroup pnd_Super_Cntl__R_Field_5;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year_N;
    private DbsField pnd_Super_Cntl_Pnd_S_Form;

    private DbsGroup pnd_Input_Parms;
    private DbsField pnd_Input_Parms_Pnd_Form;
    private DbsField pnd_Input_Parms_Pnd_Tax_Year_N;
    private DbsField pnd_Corrected_Return_Ind;
    private DbsField pnd_Cntrct_Found;
    private DbsField pnd_Previous_Form_Found;
    private DbsField pnd_Two_Transaction_Corr;
    private DbsField pnd_One_Transaction_Corr;
    private DbsField pnd_No_Change;
    private DbsField pnd_Company_Cde;
    private DbsField pnd_Company_Name;
    private DbsField pnd_5498_Type;

    private DbsGroup pnd_5498_Type__R_Field_6;
    private DbsField pnd_5498_Type_Pnd_5498_Type_N;
    private DbsField pnd_Label;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Empty_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Datx;
    private DbsField pnd_Timx;
    private DbsField pnd_T_Trans_Count;
    private DbsField pnd_T_Classic_Amt;
    private DbsField pnd_T_Roth_Amt;
    private DbsField pnd_T_Rollover_Amt;
    private DbsField pnd_T_Rechar_Amt;
    private DbsField pnd_T_Sep_Amt;
    private DbsField pnd_T_Fmv_Amt;
    private DbsField pnd_T_Roth_Conv_Amt;
    private DbsField pnd_T_Postpn_Amt;
    private DbsField pnd_T_Repymnt_Amt;
    private DbsField pnd_C_Trans_Count;
    private DbsField pnd_C_Classic_Amt;
    private DbsField pnd_C_Roth_Amt;
    private DbsField pnd_C_Rollover_Amt;
    private DbsField pnd_C_Rechar_Amt;
    private DbsField pnd_C_Sep_Amt;
    private DbsField pnd_C_Fmv_Amt;
    private DbsField pnd_C_Roth_Conv_Amt;
    private DbsField pnd_C_Postpn_Amt;
    private DbsField pnd_C_Repymnt_Amt;
    private DbsField pnd_L_Trans_Count;
    private DbsField pnd_L_Classic_Amt;
    private DbsField pnd_L_Roth_Amt;
    private DbsField pnd_L_Rollover_Amt;
    private DbsField pnd_L_Rechar_Amt;
    private DbsField pnd_L_Sep_Amt;
    private DbsField pnd_L_Fmv_Amt;
    private DbsField pnd_L_Roth_Conv_Amt;
    private DbsField pnd_L_Postpn_Amt;
    private DbsField pnd_L_Repymnt_Amt;
    private DbsField pnd_S_Trans_Count;
    private DbsField pnd_S_Classic_Amt;
    private DbsField pnd_S_Roth_Amt;
    private DbsField pnd_S_Rollover_Amt;
    private DbsField pnd_S_Rechar_Amt;
    private DbsField pnd_S_Sep_Amt;
    private DbsField pnd_S_Fmv_Amt;
    private DbsField pnd_S_Roth_Conv_Amt;
    private DbsField pnd_S_Postpn_Amt;
    private DbsField pnd_S_Repymnt_Amt;
    private DbsField pnd_G_Trans_Count;
    private DbsField pnd_G_Classic_Amt;
    private DbsField pnd_G_Roth_Amt;
    private DbsField pnd_G_Rollover_Amt;
    private DbsField pnd_G_Rechar_Amt;
    private DbsField pnd_G_Sep_Amt;
    private DbsField pnd_G_Fmv_Amt;
    private DbsField pnd_G_Roth_Conv_Amt;
    private DbsField pnd_G_Postpn_Amt;
    private DbsField pnd_G_Repymnt_Amt;
    private DbsField pnd_R_Trans_Count;
    private DbsField pnd_R_Classic_Amt;
    private DbsField pnd_R_Roth_Amt;
    private DbsField pnd_R_Rollover_Amt;
    private DbsField pnd_R_Rechar_Amt;
    private DbsField pnd_R_Sep_Amt;
    private DbsField pnd_R_Fmv_Amt;
    private DbsField pnd_R_Roth_Conv_Amt;
    private DbsField pnd_R_Postpn_Amt;
    private DbsField pnd_R_Repymnt_Amt;
    private DbsField i;
    private DbsField j;
    private DbsField k;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl451a = new LdaTwrl451a();
        registerRecord(ldaTwrl451a);
        registerRecord(ldaTwrl451a.getVw_form());
        ldaTwrl451b = new LdaTwrl451b();
        registerRecord(ldaTwrl451b);
        registerRecord(ldaTwrl451b.getVw_form2());
        ldaTwrl4001 = new LdaTwrl4001();
        registerRecord(ldaTwrl4001);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL", DdmPeriodicGroups.getInstance().getGroups("TIRCNTL_REPORTING_TBL_VIEW"));
        cntl_Tircntl_Tbl_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_Tircntl_Tax_Year = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_Tircntl_Rpt_Form_Name = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TIRCNTL_RPT_FORM_NAME");
        cntl_Count_Casttircntl_Rpt_Tbl_Pe = vw_cntl.getRecord().newFieldInGroup("cntl_Count_Casttircntl_Rpt_Tbl_Pe", "C*TIRCNTL-RPT-TBL-PE", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");

        cntl_Tircntl_Rpt_Tbl_Pe = vw_cntl.getRecord().newGroupInGroup("cntl_Tircntl_Rpt_Tbl_Pe", "TIRCNTL-RPT-TBL-PE", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntl_Tircntl_Rpt_Dte = cntl_Tircntl_Rpt_Tbl_Pe.newFieldArrayInGroup("cntl_Tircntl_Rpt_Dte", "TIRCNTL-RPT-DTE", FieldType.DATE, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRCNTL_RPT_DTE", "TIR_CONTROL_TIRCNTL_RPT_TBL_PE");
        cntl_Tircntl_Rpt_Dte.setDdmHeader("REPOR-/TING/DATE");
        registerRecord(vw_cntl);

        vw_hform = new DataAccessProgramView(new NameInfo("vw_hform", "HFORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        hform_Tirf_Superde_4 = vw_hform.getRecord().newFieldInGroup("hform_Tirf_Superde_4", "TIRF-SUPERDE-4", FieldType.BINARY, 39, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_4");
        hform_Tirf_Superde_4.setSuperDescriptor(true);

        hform__R_Field_1 = vw_hform.getRecord().newGroupInGroup("hform__R_Field_1", "REDEFINE", hform_Tirf_Superde_4);
        hform_Pnd_Tirf_Tax_Year = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        hform_Pnd_Tirf_Tin = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        hform_Pnd_Tirf_Form_Type = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        hform_Pnd_Tirf_Contract_Nbr = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 8);
        hform_Pnd_Tirf_Payee_Cde = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 2);
        hform_Pnd_Tirf_Key = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Key", "#TIRF-KEY", FieldType.STRING, 5);
        hform_Pnd_Tirf_Invrse_Create_Ts = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Invrse_Create_Ts", "#TIRF-INVRSE-CREATE-TS", FieldType.TIME);
        hform_Pnd_Tirf_Irs_Rpt_Ind = hform__R_Field_1.newFieldInGroup("hform_Pnd_Tirf_Irs_Rpt_Ind", "#TIRF-IRS-RPT-IND", FieldType.STRING, 1);
        registerRecord(vw_hform);

        pnd_Tirf_Superde_3_1 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3_1", "#TIRF-SUPERDE-3-1", FieldType.STRING, 7);

        pnd_Tirf_Superde_3_1__R_Field_2 = localVariables.newGroupInRecord("pnd_Tirf_Superde_3_1__R_Field_2", "REDEFINE", pnd_Tirf_Superde_3_1);
        pnd_Tirf_Superde_3_1_Pnd_S3_Tax_Year = pnd_Tirf_Superde_3_1__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_1_Pnd_S3_Tax_Year", "#S3-TAX-YEAR", 
            FieldType.STRING, 4);
        pnd_Tirf_Superde_3_1_Pnd_S3_Active_Ind = pnd_Tirf_Superde_3_1__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_1_Pnd_S3_Active_Ind", "#S3-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Tirf_Superde_3_1_Pnd_S3_Form_Type = pnd_Tirf_Superde_3_1__R_Field_2.newFieldInGroup("pnd_Tirf_Superde_3_1_Pnd_S3_Form_Type", "#S3-FORM-TYPE", 
            FieldType.NUMERIC, 2);
        pnd_Tirf_Superde_4_1 = localVariables.newFieldInRecord("pnd_Tirf_Superde_4_1", "#TIRF-SUPERDE-4-1", FieldType.STRING, 39);

        pnd_Tirf_Superde_4_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Tirf_Superde_4_1__R_Field_3", "REDEFINE", pnd_Tirf_Superde_4_1);
        pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year", "#S4-TAX-YEAR", 
            FieldType.STRING, 4);
        pnd_Tirf_Superde_4_1_Pnd_S4_Tin = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Tin", "#S4-TIN", FieldType.STRING, 
            10);
        pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type", "#S4-FORM-TYPE", 
            FieldType.NUMERIC, 2);
        pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr", "#S4-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde", "#S4-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Tirf_Superde_4_1_Pnd_S4_Key = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Key", "#S4-KEY", FieldType.STRING, 
            5);
        pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts", 
            "#S4-INVRSE-CREATE-TS", FieldType.TIME);
        pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind = pnd_Tirf_Superde_4_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind", "#S4-IRS-RPT-IND", 
            FieldType.STRING, 1);
        pnd_Super_Cntl = localVariables.newFieldInRecord("pnd_Super_Cntl", "#SUPER-CNTL", FieldType.STRING, 12);

        pnd_Super_Cntl__R_Field_4 = localVariables.newGroupInRecord("pnd_Super_Cntl__R_Field_4", "REDEFINE", pnd_Super_Cntl);
        pnd_Super_Cntl_Pnd_S_Tbl = pnd_Super_Cntl__R_Field_4.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tbl", "#S-TBL", FieldType.NUMERIC, 1);
        pnd_Super_Cntl_Pnd_S_Tax_Year = pnd_Super_Cntl__R_Field_4.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.STRING, 4);

        pnd_Super_Cntl__R_Field_5 = pnd_Super_Cntl__R_Field_4.newGroupInGroup("pnd_Super_Cntl__R_Field_5", "REDEFINE", pnd_Super_Cntl_Pnd_S_Tax_Year);
        pnd_Super_Cntl_Pnd_S_Tax_Year_N = pnd_Super_Cntl__R_Field_5.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year_N", "#S-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_Cntl_Pnd_S_Form = pnd_Super_Cntl__R_Field_4.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Form", "#S-FORM", FieldType.STRING, 7);

        pnd_Input_Parms = localVariables.newGroupInRecord("pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Input_Parms_Pnd_Form = pnd_Input_Parms.newFieldInGroup("pnd_Input_Parms_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Input_Parms_Pnd_Tax_Year_N = pnd_Input_Parms.newFieldInGroup("pnd_Input_Parms_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Corrected_Return_Ind = localVariables.newFieldInRecord("pnd_Corrected_Return_Ind", "#CORRECTED-RETURN-IND", FieldType.STRING, 1);
        pnd_Cntrct_Found = localVariables.newFieldInRecord("pnd_Cntrct_Found", "#CNTRCT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Previous_Form_Found = localVariables.newFieldInRecord("pnd_Previous_Form_Found", "#PREVIOUS-FORM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Two_Transaction_Corr = localVariables.newFieldInRecord("pnd_Two_Transaction_Corr", "#TWO-TRANSACTION-CORR", FieldType.BOOLEAN, 1);
        pnd_One_Transaction_Corr = localVariables.newFieldInRecord("pnd_One_Transaction_Corr", "#ONE-TRANSACTION-CORR", FieldType.BOOLEAN, 1);
        pnd_No_Change = localVariables.newFieldInRecord("pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Company_Cde = localVariables.newFieldInRecord("pnd_Company_Cde", "#COMPANY-CDE", FieldType.STRING, 1);
        pnd_Company_Name = localVariables.newFieldInRecord("pnd_Company_Name", "#COMPANY-NAME", FieldType.STRING, 50);
        pnd_5498_Type = localVariables.newFieldInRecord("pnd_5498_Type", "#5498-TYPE", FieldType.STRING, 2);

        pnd_5498_Type__R_Field_6 = localVariables.newGroupInRecord("pnd_5498_Type__R_Field_6", "REDEFINE", pnd_5498_Type);
        pnd_5498_Type_Pnd_5498_Type_N = pnd_5498_Type__R_Field_6.newFieldInGroup("pnd_5498_Type_Pnd_5498_Type_N", "#5498-TYPE-N", FieldType.NUMERIC, 2);
        pnd_Label = localVariables.newFieldInRecord("pnd_Label", "#LABEL", FieldType.STRING, 15);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Ctr = localVariables.newFieldInRecord("pnd_Empty_Ctr", "#EMPTY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_T_Trans_Count = localVariables.newFieldArrayInRecord("pnd_T_Trans_Count", "#T-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_T_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_T_Classic_Amt", "#T-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Amt", "#T-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rollover_Amt", "#T-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_T_Rechar_Amt", "#T-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_T_Sep_Amt", "#T-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Fmv_Amt", "#T-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_T_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_T_Roth_Conv_Amt", "#T-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_T_Postpn_Amt", "#T-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_T_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_T_Repymnt_Amt", "#T-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Trans_Count = localVariables.newFieldArrayInRecord("pnd_C_Trans_Count", "#C-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_C_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_C_Classic_Amt", "#C-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Amt", "#C-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rollover_Amt", "#C-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_C_Rechar_Amt", "#C-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_C_Sep_Amt", "#C-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Fmv_Amt", "#C-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_C_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_C_Roth_Conv_Amt", "#C-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_C_Postpn_Amt", "#C-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_C_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_C_Repymnt_Amt", "#C-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Trans_Count = localVariables.newFieldArrayInRecord("pnd_L_Trans_Count", "#L-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_L_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_L_Classic_Amt", "#L-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Amt", "#L-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rollover_Amt", "#L-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_L_Rechar_Amt", "#L-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_L_Sep_Amt", "#L-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Fmv_Amt", "#L-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_L_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_L_Roth_Conv_Amt", "#L-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_L_Postpn_Amt", "#L-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_L_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_L_Repymnt_Amt", "#L-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Trans_Count = localVariables.newFieldArrayInRecord("pnd_S_Trans_Count", "#S-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_S_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_S_Classic_Amt", "#S-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Amt", "#S-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rollover_Amt", "#S-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_S_Rechar_Amt", "#S-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_S_Sep_Amt", "#S-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Fmv_Amt", "#S-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_S_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_S_Roth_Conv_Amt", "#S-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_S_Postpn_Amt", "#S-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_S_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_S_Repymnt_Amt", "#S-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Trans_Count = localVariables.newFieldArrayInRecord("pnd_G_Trans_Count", "#G-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_G_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_G_Classic_Amt", "#G-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_G_Roth_Amt", "#G-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_G_Rollover_Amt", "#G-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_G_Rechar_Amt", "#G-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_G_Sep_Amt", "#G-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_G_Fmv_Amt", "#G-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_G_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_G_Roth_Conv_Amt", "#G-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_G_Postpn_Amt", "#G-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_G_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_G_Repymnt_Amt", "#G-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Trans_Count = localVariables.newFieldArrayInRecord("pnd_R_Trans_Count", "#R-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            9));
        pnd_R_Classic_Amt = localVariables.newFieldArrayInRecord("pnd_R_Classic_Amt", "#R-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Roth_Amt = localVariables.newFieldArrayInRecord("pnd_R_Roth_Amt", "#R-ROTH-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Rollover_Amt = localVariables.newFieldArrayInRecord("pnd_R_Rollover_Amt", "#R-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Rechar_Amt = localVariables.newFieldArrayInRecord("pnd_R_Rechar_Amt", "#R-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Sep_Amt = localVariables.newFieldArrayInRecord("pnd_R_Sep_Amt", "#R-SEP-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Fmv_Amt = localVariables.newFieldArrayInRecord("pnd_R_Fmv_Amt", "#R-FMV-AMT", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            9));
        pnd_R_Roth_Conv_Amt = localVariables.newFieldArrayInRecord("pnd_R_Roth_Conv_Amt", "#R-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Postpn_Amt = localVariables.newFieldArrayInRecord("pnd_R_Postpn_Amt", "#R-POSTPN-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        pnd_R_Repymnt_Amt = localVariables.newFieldArrayInRecord("pnd_R_Repymnt_Amt", "#R-REPYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 
            9));
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 5);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 5);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl.reset();
        vw_hform.reset();

        ldaTwrl451a.initializeValues();
        ldaTwrl451b.initializeValues();
        ldaTwrl4001.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4910() throws Exception
    {
        super("Twrp4910");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP4910", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *--------
        //*  PALDE                                                                                                                                                        //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: FORMAT ( 02 ) PS = 60 LS = 170;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-ALREADY-PROCESSED
        sub_Check_If_Already_Processed();
        if (condition(Global.isEscape())) {return;}
        pnd_Tirf_Superde_3_1_Pnd_S3_Tax_Year.setValue(pnd_Input_Parms_Pnd_Tax_Year_N);                                                                                    //Natural: ASSIGN #S3-TAX-YEAR := #TAX-YEAR-N
        pnd_Tirf_Superde_3_1_Pnd_S3_Active_Ind.setValue("A");                                                                                                             //Natural: ASSIGN #S3-ACTIVE-IND := 'A'
        pnd_Tirf_Superde_3_1_Pnd_S3_Form_Type.setValue(4);                                                                                                                //Natural: ASSIGN #S3-FORM-TYPE := 04
        //*  --------------------------------------------
        ldaTwrl451a.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM WITH TIRF-SUPERDE-3 = #TIRF-SUPERDE-3-1
        (
        "RD1",
        new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Tirf_Superde_3_1, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
        );
        RD1:
        while (condition(ldaTwrl451a.getVw_form().readNextRow("RD1")))
        {
            if (condition(ldaTwrl451a.getForm_Tirf_Tax_Year().equals(pnd_Tirf_Superde_3_1_Pnd_S3_Tax_Year) && ldaTwrl451a.getForm_Tirf_Active_Ind().equals("A")           //Natural: IF TIRF-TAX-YEAR = #S3-TAX-YEAR AND TIRF-ACTIVE-IND = 'A' AND TIRF-FORM-TYPE = 04
                && ldaTwrl451a.getForm_Tirf_Form_Type().equals(4)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*  PERFORM  LOOKUP-IRA-TYPE
            if (condition(ldaTwrl451a.getForm_Tirf_Ira_Acct_Type().greater(" ")))                                                                                         //Natural: IF TIRF-IRA-ACCT-TYPE > ' '
            {
                pnd_5498_Type_Pnd_5498_Type_N.compute(new ComputeParameters(false, pnd_5498_Type_Pnd_5498_Type_N), ldaTwrl451a.getForm_Tirf_Ira_Acct_Type().val().multiply(10)); //Natural: ASSIGN #5498-TYPE-N := VAL ( TIRF-IRA-ACCT-TYPE ) * 10
            }                                                                                                                                                             //Natural: END-IF
            j.setValue(1);                                                                                                                                                //Natural: ASSIGN J := 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
            sub_Update_Control_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl451a.getForm_Tirf_Irs_Rpt_Ind().equals("O") || ldaTwrl451a.getForm_Tirf_Irs_Rpt_Ind().equals("C")))                                      //Natural: IF TIRF-IRS-RPT-IND = 'O' OR = 'C'
            {
                j.setValue(2);                                                                                                                                            //Natural: ASSIGN J := 2
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                sub_Update_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl451a.getForm_Tirf_Empty_Form().equals(true) && ldaTwrl451a.getForm_Tirf_Link_Tin().notEquals(" ")))                                      //Natural: IF TIRF-EMPTY-FORM = TRUE AND TIRF-LINK-TIN NE ' '
            {
                j.setValue(5);                                                                                                                                            //Natural: ASSIGN J := 5
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                sub_Update_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Corrected_Return_Ind.setValue(" ");                                                                                                                   //Natural: ASSIGN #CORRECTED-RETURN-IND := ' '
                getWorkFiles().write(4, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));             //Natural: WRITE WORK FILE 04 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl451a.getForm_Tirf_Empty_Form().equals(true) && ldaTwrl451a.getForm_Tirf_Link_Tin().equals(" ")))                                         //Natural: IF TIRF-EMPTY-FORM = TRUE AND TIRF-LINK-TIN = ' '
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-PREVIOUS-FORM
                sub_Check_For_Previous_Form();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Previous_Form_Found.equals(true)))                                                                                                      //Natural: IF #PREVIOUS-FORM-FOUND = TRUE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    j.setValue(4);                                                                                                                                        //Natural: ASSIGN J := 4
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Corrected_Return_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN #CORRECTED-RETURN-IND := ' '
                    getWorkFiles().write(2, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));         //Natural: WRITE WORK FILE 02 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl451a.getForm_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                         //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
            {
                if (condition(ldaTwrl451a.getForm_Tirf_Irs_Rpt_Ind().equals(" ")))                                                                                        //Natural: IF TIRF-IRS-RPT-IND = ' '
                {
                    j.setValue(7);                                                                                                                                        //Natural: ASSIGN J := 7
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Corrected_Return_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN #CORRECTED-RETURN-IND := ' '
                    getWorkFiles().write(3, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));         //Natural: WRITE WORK FILE 03 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
                    //*   08/15/07        RM
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    j.setValue(6);                                                                                                                                        //Natural: ASSIGN J := 6
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl451a.getForm_Tirf_Irs_Rpt_Ind().equals(" ")))                                                                                            //Natural: IF TIRF-IRS-RPT-IND = ' '
            {
                if (condition(ldaTwrl451a.getForm_Tirf_Link_Tin().equals(" ")))                                                                                           //Natural: IF TIRF-LINK-TIN = ' '
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-PREVIOUS-FORM
                    sub_Check_For_Previous_Form();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   REPORTING A NEW 5498
                    if (condition(pnd_Previous_Form_Found.equals(false)))                                                                                                 //Natural: IF #PREVIOUS-FORM-FOUND = FALSE
                    {
                        pnd_Corrected_Return_Ind.setValue(" ");                                                                                                           //Natural: ASSIGN #CORRECTED-RETURN-IND := ' '
                        getWorkFiles().write(1, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));     //Natural: WRITE WORK FILE 01 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
                        j.setValue(8);                                                                                                                                    //Natural: ASSIGN J := 8
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                        sub_Update_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM TWO-TRANSACTION-LINK-TIN-CHANGE
                    sub_Two_Transaction_Link_Tin_Change();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    j.setValue(8);                                                                                                                                        //Natural: ASSIGN J := 8
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    j.setValue(9);                                                                                                                                        //Natural: ASSIGN J := 9
                    pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-COMPARE-PREVIOUS-FORM
                sub_Read_Compare_Previous_Form();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   NAME CHANGE
                if (condition(pnd_Two_Transaction_Corr.equals(true)))                                                                                                     //Natural: IF #TWO-TRANSACTION-CORR = TRUE
                {
                    j.setValue(8);                                                                                                                                        //Natural: ASSIGN J := 8
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    j.setValue(9);                                                                                                                                        //Natural: ASSIGN J := 9
                    pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   NOTHING CHANGED
                if (condition(pnd_No_Change.equals(true)))                                                                                                                //Natural: IF #NO-CHANGE = TRUE
                {
                    j.setValue(3);                                                                                                                                        //Natural: ASSIGN J := 3
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_One_Transaction_Corr.equals(true)))                                                                                                     //Natural: IF #ONE-TRANSACTION-CORR = TRUE
                {
                    j.setValue(8);                                                                                                                                        //Natural: ASSIGN J := 8
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TOTALS
                    sub_Update_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Corrected_Return_Ind.setValue("G");                                                                                                               //Natural: ASSIGN #CORRECTED-RETURN-IND := 'G'
                    getWorkFiles().write(1, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));         //Natural: WRITE WORK FILE 01 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  --------------------------------------------
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"No (5498) Forms Found For IRS Reporting",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'No (5498) Forms Found For IRS Reporting' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *----------------------------------------
        //* *------------
        //* *-------------------------------------------
        //* *------------
        //* *------------------------------------------------
        //* *------------
        //* *--------------------------------------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *-------------------------------------------
        //* *------------
        //* *------------------------------------
        //* *------------
        //* *-------------------------------------
        //* *------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_Check_For_Previous_Form() throws Exception                                                                                                           //Natural: CHECK-FOR-PREVIOUS-FORM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Previous_Form_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #PREVIOUS-FORM-FOUND := FALSE
        pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year.setValue(ldaTwrl451a.getForm_Tirf_Tax_Year());                                                                               //Natural: ASSIGN #S4-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Tirf_Superde_4_1_Pnd_S4_Tin.setValue(ldaTwrl451a.getForm_Tirf_Tin());                                                                                         //Natural: ASSIGN #S4-TIN := FORM.TIRF-TIN
        pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type.setValue(ldaTwrl451a.getForm_Tirf_Form_Type());                                                                             //Natural: ASSIGN #S4-FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr.setValue(ldaTwrl451a.getForm_Tirf_Contract_Nbr());                                                                       //Natural: ASSIGN #S4-CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde.setValue(ldaTwrl451a.getForm_Tirf_Payee_Cde());                                                                             //Natural: ASSIGN #S4-PAYEE-CDE := FORM.TIRF-PAYEE-CDE
        pnd_Tirf_Superde_4_1_Pnd_S4_Key.setValue(ldaTwrl451a.getForm_Tirf_Key());                                                                                         //Natural: ASSIGN #S4-KEY := FORM.TIRF-KEY
        pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts.reset();                                                                                                             //Natural: RESET #S4-INVRSE-CREATE-TS #S4-IRS-RPT-IND
        pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind.reset();
        vw_hform.createHistogram                                                                                                                                          //Natural: HISTOGRAM ( 1 ) HFORM TIRF-SUPERDE-4 STARTING FROM #TIRF-SUPERDE-4-1
        (
        "HIST01",
        "TIRF_SUPERDE_4",
        new Wc[] { new Wc("TIRF_SUPERDE_4", ">=", pnd_Tirf_Superde_4_1.getBinary(), WcType.WITH) },
        1
        );
        HIST01:
        while (condition(vw_hform.readNextRow("HIST01")))
        {
            if (condition(((((((hform_Pnd_Tirf_Tax_Year.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year) && hform_Pnd_Tirf_Tin.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tin))       //Natural: IF #TIRF-TAX-YEAR = #S4-TAX-YEAR AND #TIRF-TIN = #S4-TIN AND #TIRF-FORM-TYPE = #S4-FORM-TYPE AND #TIRF-CONTRACT-NBR = #S4-CONTRACT-NBR AND #TIRF-PAYEE-CDE = #S4-PAYEE-CDE AND #TIRF-KEY = #S4-KEY AND ( #TIRF-IRS-RPT-IND = 'O' OR = 'C' )
                && hform_Pnd_Tirf_Form_Type.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type)) && hform_Pnd_Tirf_Contract_Nbr.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr)) 
                && hform_Pnd_Tirf_Payee_Cde.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde)) && hform_Pnd_Tirf_Key.equals(pnd_Tirf_Superde_4_1_Pnd_S4_Key)) 
                && (hform_Pnd_Tirf_Irs_Rpt_Ind.equals("O") || hform_Pnd_Tirf_Irs_Rpt_Ind.equals("C")))))
            {
                pnd_Previous_Form_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #PREVIOUS-FORM-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
    }
    private void sub_Read_Compare_Previous_Form() throws Exception                                                                                                        //Natural: READ-COMPARE-PREVIOUS-FORM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Previous_Form_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #PREVIOUS-FORM-FOUND := FALSE
        pnd_Two_Transaction_Corr.setValue(false);                                                                                                                         //Natural: ASSIGN #TWO-TRANSACTION-CORR := FALSE
        pnd_One_Transaction_Corr.setValue(false);                                                                                                                         //Natural: ASSIGN #ONE-TRANSACTION-CORR := FALSE
        pnd_No_Change.setValue(false);                                                                                                                                    //Natural: ASSIGN #NO-CHANGE := FALSE
        pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year.setValue(ldaTwrl451a.getForm_Tirf_Tax_Year());                                                                               //Natural: ASSIGN #S4-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Tirf_Superde_4_1_Pnd_S4_Tin.setValue(ldaTwrl451a.getForm_Tirf_Tin());                                                                                         //Natural: ASSIGN #S4-TIN := FORM.TIRF-TIN
        pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type.setValue(ldaTwrl451a.getForm_Tirf_Form_Type());                                                                             //Natural: ASSIGN #S4-FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr.setValue(ldaTwrl451a.getForm_Tirf_Contract_Nbr());                                                                       //Natural: ASSIGN #S4-CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde.setValue(ldaTwrl451a.getForm_Tirf_Payee_Cde());                                                                             //Natural: ASSIGN #S4-PAYEE-CDE := FORM.TIRF-PAYEE-CDE
        pnd_Tirf_Superde_4_1_Pnd_S4_Key.setValue(ldaTwrl451a.getForm_Tirf_Key());                                                                                         //Natural: ASSIGN #S4-KEY := FORM.TIRF-KEY
        pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts.reset();                                                                                                             //Natural: RESET #S4-INVRSE-CREATE-TS #S4-IRS-RPT-IND
        pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind.reset();
        ldaTwrl451b.getVw_form2().startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) FORM2 WITH TIRF-SUPERDE-4 = #TIRF-SUPERDE-4-1
        (
        "READ01",
        new Wc[] { new Wc("TIRF_SUPERDE_4", ">=", pnd_Tirf_Superde_4_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") },
        1
        );
        READ01:
        while (condition(ldaTwrl451b.getVw_form2().readNextRow("READ01")))
        {
            if (condition(ldaTwrl451b.getForm2_Tirf_Tax_Year().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year) && ldaTwrl451b.getForm2_Tirf_Tin().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tin)  //Natural: IF FORM2.TIRF-TAX-YEAR = #S4-TAX-YEAR AND FORM2.TIRF-TIN = #S4-TIN AND FORM2.TIRF-FORM-TYPE = #S4-FORM-TYPE AND FORM2.TIRF-CONTRACT-NBR = #S4-CONTRACT-NBR AND FORM2.TIRF-PAYEE-CDE = #S4-PAYEE-CDE AND FORM2.TIRF-KEY = #S4-KEY
                && ldaTwrl451b.getForm2_Tirf_Form_Type().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type) && ldaTwrl451b.getForm2_Tirf_Contract_Nbr().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr) 
                && ldaTwrl451b.getForm2_Tirf_Payee_Cde().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde) && ldaTwrl451b.getForm2_Tirf_Key().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Key)))
            {
                pnd_Previous_Form_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #PREVIOUS-FORM-FOUND := TRUE
                if (condition(ldaTwrl451a.getForm_Tirf_Part_Last_Nme().equals(ldaTwrl451b.getForm2_Tirf_Part_Last_Nme()) || ldaTwrl451a.getForm_Tirf_Part_First_Nme().equals(ldaTwrl451b.getForm2_Tirf_Part_First_Nme())  //Natural: IF FORM.TIRF-PART-LAST-NME = FORM2.TIRF-PART-LAST-NME OR FORM.TIRF-PART-FIRST-NME = FORM2.TIRF-PART-FIRST-NME OR FORM.TIRF-PART-MDDLE-NME = FORM2.TIRF-PART-MDDLE-NME
                    || ldaTwrl451a.getForm_Tirf_Part_Mddle_Nme().equals(ldaTwrl451b.getForm2_Tirf_Part_Mddle_Nme())))
                {
                    //*                                                         /*   ROX
                    //*   ROX
                    if (condition(ldaTwrl451a.getForm_Tirf_Tax_Id_Type().notEquals(ldaTwrl451b.getForm2_Tirf_Tax_Id_Type())))                                             //Natural: IF FORM.TIRF-TAX-ID-TYPE NE FORM2.TIRF-TAX-ID-TYPE
                    {
                        pnd_Two_Transaction_Corr.setValue(true);                                                                                                          //Natural: ASSIGN #TWO-TRANSACTION-CORR := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TWO-PARTS-TRANSACTION
                        sub_Write_Two_Parts_Transaction();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //*       IGNORE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Two_Transaction_Corr.setValue(true);                                                                                                              //Natural: ASSIGN #TWO-TRANSACTION-CORR := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TWO-PARTS-TRANSACTION
                    sub_Write_Two_Parts_Transaction();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***",new TabSetting(6),"Cannot Find Previous Part Transaction",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Cannot Find Previous Part Transaction' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 98
            }                                                                                                                                                             //Natural: END-IF
            //*  06/30/06 RM
            if (condition(ldaTwrl451a.getForm_Tirf_Company_Cde().equals(ldaTwrl451b.getForm2_Tirf_Company_Cde()) && ldaTwrl451a.getForm_Tirf_Tax_Id_Type().equals(ldaTwrl451b.getForm2_Tirf_Tax_Id_Type())  //Natural: IF FORM.TIRF-COMPANY-CDE = FORM2.TIRF-COMPANY-CDE AND FORM.TIRF-TAX-ID-TYPE = FORM2.TIRF-TAX-ID-TYPE AND FORM.TIRF-5498-RECHAR-IND = FORM2.TIRF-5498-RECHAR-IND AND FORM.TIRF-IRA-ACCT-TYPE = FORM2.TIRF-IRA-ACCT-TYPE AND FORM.TIRF-FOREIGN-ADDR = FORM2.TIRF-FOREIGN-ADDR AND FORM.TIRF-STREET-ADDR = FORM2.TIRF-STREET-ADDR AND FORM.TIRF-CITY = FORM2.TIRF-CITY AND FORM.TIRF-GEO-CDE = FORM2.TIRF-GEO-CDE AND FORM.TIRF-APO-GEO-CDE = FORM2.TIRF-APO-GEO-CDE AND FORM.TIRF-COUNTRY-NAME = FORM2.TIRF-COUNTRY-NAME AND FORM.TIRF-PROVINCE-CODE = FORM2.TIRF-PROVINCE-CODE AND FORM.TIRF-ZIP = FORM2.TIRF-ZIP AND FORM.TIRF-TRAD-IRA-CONTRIB = FORM2.TIRF-TRAD-IRA-CONTRIB AND FORM.TIRF-ROTH-IRA-CONTRIB = FORM2.TIRF-ROTH-IRA-CONTRIB AND FORM.TIRF-SEP-AMT = FORM2.TIRF-SEP-AMT AND FORM.TIRF-TRAD-IRA-ROLLOVER = FORM2.TIRF-TRAD-IRA-ROLLOVER AND FORM.TIRF-ROTH-IRA-CONVERSION = FORM2.TIRF-ROTH-IRA-CONVERSION AND FORM.TIRF-IRA-RECHAR-AMT = FORM2.TIRF-IRA-RECHAR-AMT AND FORM.TIRF-ACCOUNT-FMV = FORM2.TIRF-ACCOUNT-FMV AND FORM.TIRF-MD-IND = FORM2.TIRF-MD-IND AND FORM.TIRF-POSTPN-AMT = FORM2.TIRF-POSTPN-AMT AND FORM.TIRF-POSTPN-YR = FORM2.TIRF-POSTPN-YR AND FORM.TIRF-POSTPN-CODE = FORM2.TIRF-POSTPN-CODE AND FORM.TIRF-REPAYMENTS-AMT = FORM2.TIRF-REPAYMENTS-AMT AND FORM.TIRF-REPAYMENTS-CODE = FORM2.TIRF-REPAYMENTS-CODE
                && ldaTwrl451a.getForm_Tirf_5498_Rechar_Ind().equals(ldaTwrl451b.getForm2_Tirf_5498_Rechar_Ind()) && ldaTwrl451a.getForm_Tirf_Ira_Acct_Type().equals(ldaTwrl451b.getForm2_Tirf_Ira_Acct_Type()) 
                && ldaTwrl451a.getForm_Tirf_Foreign_Addr().equals(ldaTwrl451b.getForm2_Tirf_Foreign_Addr()) && ldaTwrl451a.getForm_Tirf_Street_Addr().equals(ldaTwrl451b.getForm2_Tirf_Street_Addr()) 
                && ldaTwrl451a.getForm_Tirf_City().equals(ldaTwrl451b.getForm2_Tirf_City()) && ldaTwrl451a.getForm_Tirf_Geo_Cde().equals(ldaTwrl451b.getForm2_Tirf_Geo_Cde()) 
                && ldaTwrl451a.getForm_Tirf_Apo_Geo_Cde().equals(ldaTwrl451b.getForm2_Tirf_Apo_Geo_Cde()) && ldaTwrl451a.getForm_Tirf_Country_Name().equals(ldaTwrl451b.getForm2_Tirf_Country_Name()) 
                && ldaTwrl451a.getForm_Tirf_Province_Code().equals(ldaTwrl451b.getForm2_Tirf_Province_Code()) && ldaTwrl451a.getForm_Tirf_Zip().equals(ldaTwrl451b.getForm2_Tirf_Zip()) 
                && ldaTwrl451a.getForm_Tirf_Trad_Ira_Contrib().equals(ldaTwrl451b.getForm2_Tirf_Trad_Ira_Contrib()) && ldaTwrl451a.getForm_Tirf_Roth_Ira_Contrib().equals(ldaTwrl451b.getForm2_Tirf_Roth_Ira_Contrib()) 
                && ldaTwrl451a.getForm_Tirf_Sep_Amt().equals(ldaTwrl451b.getForm2_Tirf_Sep_Amt()) && ldaTwrl451a.getForm_Tirf_Trad_Ira_Rollover().equals(ldaTwrl451b.getForm2_Tirf_Trad_Ira_Rollover()) 
                && ldaTwrl451a.getForm_Tirf_Roth_Ira_Conversion().equals(ldaTwrl451b.getForm2_Tirf_Roth_Ira_Conversion()) && ldaTwrl451a.getForm_Tirf_Ira_Rechar_Amt().equals(ldaTwrl451b.getForm2_Tirf_Ira_Rechar_Amt()) 
                && ldaTwrl451a.getForm_Tirf_Account_Fmv().equals(ldaTwrl451b.getForm2_Tirf_Account_Fmv()) && ldaTwrl451a.getForm_Tirf_Md_Ind().equals(ldaTwrl451b.getForm2_Tirf_Md_Ind()) 
                && ldaTwrl451a.getForm_Tirf_Postpn_Amt().equals(ldaTwrl451b.getForm2_Tirf_Postpn_Amt()) && ldaTwrl451a.getForm_Tirf_Postpn_Yr().equals(ldaTwrl451b.getForm2_Tirf_Postpn_Yr()) 
                && ldaTwrl451a.getForm_Tirf_Postpn_Code().equals(ldaTwrl451b.getForm2_Tirf_Postpn_Code()) && ldaTwrl451a.getForm_Tirf_Repayments_Amt().equals(ldaTwrl451b.getForm2_Tirf_Repayments_Amt()) 
                && ldaTwrl451a.getForm_Tirf_Repayments_Code().equals(ldaTwrl451b.getForm2_Tirf_Repayments_Code())))
            {
                pnd_No_Change.setValue(true);                                                                                                                             //Natural: ASSIGN #NO-CHANGE := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_One_Transaction_Corr.setValue(true);                                                                                                                  //Natural: ASSIGN #ONE-TRANSACTION-CORR := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Two_Transaction_Link_Tin_Change() throws Exception                                                                                                   //Natural: TWO-TRANSACTION-LINK-TIN-CHANGE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Previous_Form_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #PREVIOUS-FORM-FOUND := FALSE
        pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year.setValue(ldaTwrl451a.getForm_Tirf_Tax_Year());                                                                               //Natural: ASSIGN #S4-TAX-YEAR := FORM.TIRF-TAX-YEAR
        pnd_Tirf_Superde_4_1_Pnd_S4_Tin.setValue(ldaTwrl451a.getForm_Tirf_Link_Tin());                                                                                    //Natural: ASSIGN #S4-TIN := FORM.TIRF-LINK-TIN
        pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type.setValue(ldaTwrl451a.getForm_Tirf_Form_Type());                                                                             //Natural: ASSIGN #S4-FORM-TYPE := FORM.TIRF-FORM-TYPE
        pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr.setValue(ldaTwrl451a.getForm_Tirf_Contract_Nbr());                                                                       //Natural: ASSIGN #S4-CONTRACT-NBR := FORM.TIRF-CONTRACT-NBR
        pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde.setValue(ldaTwrl451a.getForm_Tirf_Payee_Cde());                                                                             //Natural: ASSIGN #S4-PAYEE-CDE := FORM.TIRF-PAYEE-CDE
        pnd_Tirf_Superde_4_1_Pnd_S4_Key.setValue(ldaTwrl451a.getForm_Tirf_Key());                                                                                         //Natural: ASSIGN #S4-KEY := FORM.TIRF-KEY
        pnd_Tirf_Superde_4_1_Pnd_S4_Invrse_Create_Ts.reset();                                                                                                             //Natural: RESET #S4-INVRSE-CREATE-TS #S4-IRS-RPT-IND
        pnd_Tirf_Superde_4_1_Pnd_S4_Irs_Rpt_Ind.reset();
        ldaTwrl451b.getVw_form2().startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) FORM2 WITH TIRF-SUPERDE-4 = #TIRF-SUPERDE-4-1
        (
        "READ02",
        new Wc[] { new Wc("TIRF_SUPERDE_4", ">=", pnd_Tirf_Superde_4_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") },
        1
        );
        READ02:
        while (condition(ldaTwrl451b.getVw_form2().readNextRow("READ02")))
        {
            if (condition(ldaTwrl451b.getForm2_Tirf_Tax_Year().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tax_Year) && ldaTwrl451b.getForm2_Tirf_Tin().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Tin)  //Natural: IF FORM2.TIRF-TAX-YEAR = #S4-TAX-YEAR AND FORM2.TIRF-TIN = #S4-TIN AND FORM2.TIRF-FORM-TYPE = #S4-FORM-TYPE AND FORM2.TIRF-CONTRACT-NBR = #S4-CONTRACT-NBR AND FORM2.TIRF-PAYEE-CDE = #S4-PAYEE-CDE AND FORM2.TIRF-KEY = #S4-KEY
                && ldaTwrl451b.getForm2_Tirf_Form_Type().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Form_Type) && ldaTwrl451b.getForm2_Tirf_Contract_Nbr().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Contract_Nbr) 
                && ldaTwrl451b.getForm2_Tirf_Payee_Cde().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Payee_Cde) && ldaTwrl451b.getForm2_Tirf_Key().equals(pnd_Tirf_Superde_4_1_Pnd_S4_Key)))
            {
                pnd_Previous_Form_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #PREVIOUS-FORM-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-TWO-PARTS-TRANSACTION
                sub_Write_Two_Parts_Transaction();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***",new TabSetting(6),"Link Tin 2 Parts Transaction","Cannot Find Previous Part",new TabSetting(77),"***",NEWLINE,"***",new       //Natural: WRITE ( 00 ) '***' 06T 'Link Tin 2 Parts Transaction' 'Cannot Find Previous Part' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                    TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Two_Parts_Transaction() throws Exception                                                                                                       //Natural: WRITE-TWO-PARTS-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Corrected_Return_Ind.setValue("C");                                                                                                                           //Natural: ASSIGN #CORRECTED-RETURN-IND := 'C'
        //*  06/30/06   RM
        //*  PALDE
        //*  PALDE
        getWorkFiles().write(1, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451a.getVw_form(), ldaTwrl451a.getVw_form().getAstISN("RD1"));                     //Natural: WRITE WORK FILE 01 #5498-TYPE #CORRECTED-RETURN-IND FORM *ISN ( RD1. )
        ldaTwrl451b.getForm2_Tirf_Trad_Ira_Contrib().setValue(0);                                                                                                         //Natural: ASSIGN FORM2.TIRF-TRAD-IRA-CONTRIB := 0.0
        ldaTwrl451b.getForm2_Tirf_Roth_Ira_Contrib().setValue(0);                                                                                                         //Natural: ASSIGN FORM2.TIRF-ROTH-IRA-CONTRIB := 0.0
        ldaTwrl451b.getForm2_Tirf_Sep_Amt().setValue(0);                                                                                                                  //Natural: ASSIGN FORM2.TIRF-SEP-AMT := 0.0
        ldaTwrl451b.getForm2_Tirf_Trad_Ira_Rollover().setValue(0);                                                                                                        //Natural: ASSIGN FORM2.TIRF-TRAD-IRA-ROLLOVER := 0.0
        ldaTwrl451b.getForm2_Tirf_Roth_Ira_Conversion().setValue(0);                                                                                                      //Natural: ASSIGN FORM2.TIRF-ROTH-IRA-CONVERSION := 0.0
        ldaTwrl451b.getForm2_Tirf_Ira_Rechar_Amt().setValue(0);                                                                                                           //Natural: ASSIGN FORM2.TIRF-IRA-RECHAR-AMT := 0.0
        ldaTwrl451b.getForm2_Tirf_Account_Fmv().setValue(0);                                                                                                              //Natural: ASSIGN FORM2.TIRF-ACCOUNT-FMV := 0.0
        ldaTwrl451b.getForm2_Tirf_Postpn_Amt().setValue(0);                                                                                                               //Natural: ASSIGN FORM2.TIRF-POSTPN-AMT := 0.0
        ldaTwrl451b.getForm2_Tirf_Repayments_Code().setValue(0);                                                                                                          //Natural: ASSIGN FORM2.TIRF-REPAYMENTS-CODE := 0.0
        pnd_Corrected_Return_Ind.setValue("G");                                                                                                                           //Natural: ASSIGN #CORRECTED-RETURN-IND := 'G'
        getWorkFiles().write(1, false, pnd_5498_Type, pnd_Corrected_Return_Ind, ldaTwrl451b.getVw_form2(), ldaTwrl451a.getVw_form().getAstISN("RD1"));                    //Natural: WRITE WORK FILE 01 #5498-TYPE #CORRECTED-RETURN-IND FORM2 *ISN ( RD1. )
    }
    private void sub_Lookup_Ira_Type() throws Exception                                                                                                                   //Natural: LOOKUP-IRA-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //* *
        //* *  FIND IRA-TYPE BY CONTRACT RANGE
        //* *
        pnd_Cntrct_Found.reset();                                                                                                                                         //Natural: RESET #CNTRCT-FOUND #COMPANY-CDE #5498-TYPE
        pnd_Company_Cde.reset();
        pnd_5498_Type.reset();
        FOR01:                                                                                                                                                            //Natural: FOR I = 1 TO #IRA-CNTRCT-TBL-MAX
        for (i.setValue(1); condition(i.lessOrEqual(ldaTwrl4001.getPnd_Ira_Cntrct_Tbl_Max())); i.nadd(1))
        {
            if (condition(ldaTwrl451a.getForm_Tirf_Contract_Nbr().greaterOrEqual(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Cntrct_Begin_Range().getValue(i))            //Natural: IF FORM.TIRF-CONTRACT-NBR >= #IRA-CNTRCT-BEGIN-RANGE ( I ) AND FORM.TIRF-CONTRACT-NBR <= #IRA-CNTRCT-END-RANGE ( I )
                && ldaTwrl451a.getForm_Tirf_Contract_Nbr().lessOrEqual(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Cntrct_End_Range().getValue(i))))
            {
                pnd_Cntrct_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #CNTRCT-FOUND := TRUE
                pnd_Company_Cde.setValue(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Company_Cde().getValue(i));                                                          //Natural: ASSIGN #COMPANY-CDE := #IRA-COMPANY-CDE ( I )
                pnd_5498_Type.setValue(ldaTwrl4001.getPnd_Ira_Contract_Tbl_Pnd_Ira_Type().getValue(i));                                                                   //Natural: ASSIGN #5498-TYPE := #IRA-CONTRACT-TBL.#IRA-TYPE ( I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_C_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-C-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_C_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-TRANS-COUNT ( J )
        pnd_C_Classic_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Contrib());                                                                                  //Natural: ADD FORM.TIRF-TRAD-IRA-CONTRIB TO #C-CLASSIC-AMT ( J )
        pnd_C_Roth_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Contrib());                                                                                     //Natural: ADD FORM.TIRF-ROTH-IRA-CONTRIB TO #C-ROTH-AMT ( J )
        pnd_C_Rollover_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Rollover());                                                                                //Natural: ADD FORM.TIRF-TRAD-IRA-ROLLOVER TO #C-ROLLOVER-AMT ( J )
        pnd_C_Rechar_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Ira_Rechar_Amt());                                                                                     //Natural: ADD FORM.TIRF-IRA-RECHAR-AMT TO #C-RECHAR-AMT ( J )
        //*  06/30/06 RM
        pnd_C_Sep_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Sep_Amt());                                                                                               //Natural: ADD FORM.TIRF-SEP-AMT TO #C-SEP-AMT ( J )
        pnd_C_Fmv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Account_Fmv());                                                                                           //Natural: ADD FORM.TIRF-ACCOUNT-FMV TO #C-FMV-AMT ( J )
        pnd_C_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Conversion());                                                                             //Natural: ADD FORM.TIRF-ROTH-IRA-CONVERSION TO #C-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_C_Postpn_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Postpn_Amt());                                                                                         //Natural: ADD FORM.TIRF-POSTPN-AMT TO #C-POSTPN-AMT ( J )
        //*  PALDE
        pnd_C_Repymnt_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Repayments_Amt());                                                                                    //Natural: ADD FORM.TIRF-REPAYMENTS-AMT TO #C-REPYMNT-AMT ( J )
    }
    private void sub_Update_L_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-L-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_L_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #L-TRANS-COUNT ( J )
        pnd_L_Classic_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Contrib());                                                                                  //Natural: ADD FORM.TIRF-TRAD-IRA-CONTRIB TO #L-CLASSIC-AMT ( J )
        pnd_L_Roth_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Contrib());                                                                                     //Natural: ADD FORM.TIRF-ROTH-IRA-CONTRIB TO #L-ROTH-AMT ( J )
        pnd_L_Rollover_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Rollover());                                                                                //Natural: ADD FORM.TIRF-TRAD-IRA-ROLLOVER TO #L-ROLLOVER-AMT ( J )
        pnd_L_Rechar_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Ira_Rechar_Amt());                                                                                     //Natural: ADD FORM.TIRF-IRA-RECHAR-AMT TO #L-RECHAR-AMT ( J )
        //*  06/30/06 RM
        pnd_L_Sep_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Sep_Amt());                                                                                               //Natural: ADD FORM.TIRF-SEP-AMT TO #L-SEP-AMT ( J )
        pnd_L_Fmv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Account_Fmv());                                                                                           //Natural: ADD FORM.TIRF-ACCOUNT-FMV TO #L-FMV-AMT ( J )
        pnd_L_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Conversion());                                                                             //Natural: ADD FORM.TIRF-ROTH-IRA-CONVERSION TO #L-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_L_Postpn_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Postpn_Amt());                                                                                         //Natural: ADD FORM.TIRF-POSTPN-AMT TO #L-POSTPN-AMT ( J )
        //*  PALDE
        pnd_L_Repymnt_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Repayments_Amt());                                                                                    //Natural: ADD FORM.TIRF-REPAYMENTS-AMT TO #L-REPYMNT-AMT ( J )
    }
    private void sub_Update_T_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-T-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_T_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-TRANS-COUNT ( J )
        pnd_T_Classic_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Contrib());                                                                                  //Natural: ADD FORM.TIRF-TRAD-IRA-CONTRIB TO #T-CLASSIC-AMT ( J )
        pnd_T_Roth_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Contrib());                                                                                     //Natural: ADD FORM.TIRF-ROTH-IRA-CONTRIB TO #T-ROTH-AMT ( J )
        pnd_T_Rollover_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Rollover());                                                                                //Natural: ADD FORM.TIRF-TRAD-IRA-ROLLOVER TO #T-ROLLOVER-AMT ( J )
        pnd_T_Rechar_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Ira_Rechar_Amt());                                                                                     //Natural: ADD FORM.TIRF-IRA-RECHAR-AMT TO #T-RECHAR-AMT ( J )
        //*  06/30/06 RM
        pnd_T_Sep_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Sep_Amt());                                                                                               //Natural: ADD FORM.TIRF-SEP-AMT TO #T-SEP-AMT ( J )
        pnd_T_Fmv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Account_Fmv());                                                                                           //Natural: ADD FORM.TIRF-ACCOUNT-FMV TO #T-FMV-AMT ( J )
        pnd_T_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Conversion());                                                                             //Natural: ADD FORM.TIRF-ROTH-IRA-CONVERSION TO #T-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_T_Postpn_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Postpn_Amt());                                                                                         //Natural: ADD FORM.TIRF-POSTPN-AMT TO #T-POSTPN-AMT ( J )
        //*  PALDE
        pnd_T_Repymnt_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Repayments_Amt());                                                                                    //Natural: ADD FORM.TIRF-REPAYMENTS-AMT TO #T-REPYMNT-AMT ( J )
    }
    private void sub_Update_S_Control_Totals() throws Exception                                                                                                           //Natural: UPDATE-S-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pnd_S_Trans_Count.getValue(j).nadd(1);                                                                                                                            //Natural: ADD 1 TO #S-TRANS-COUNT ( J )
        pnd_S_Classic_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Contrib());                                                                                  //Natural: ADD FORM.TIRF-TRAD-IRA-CONTRIB TO #S-CLASSIC-AMT ( J )
        pnd_S_Roth_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Contrib());                                                                                     //Natural: ADD FORM.TIRF-ROTH-IRA-CONTRIB TO #S-ROTH-AMT ( J )
        pnd_S_Rollover_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Trad_Ira_Rollover());                                                                                //Natural: ADD FORM.TIRF-TRAD-IRA-ROLLOVER TO #S-ROLLOVER-AMT ( J )
        pnd_S_Rechar_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Ira_Rechar_Amt());                                                                                     //Natural: ADD FORM.TIRF-IRA-RECHAR-AMT TO #S-RECHAR-AMT ( J )
        //*  06/30/06 RM
        pnd_S_Sep_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Sep_Amt());                                                                                               //Natural: ADD FORM.TIRF-SEP-AMT TO #S-SEP-AMT ( J )
        pnd_S_Fmv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Account_Fmv());                                                                                           //Natural: ADD FORM.TIRF-ACCOUNT-FMV TO #S-FMV-AMT ( J )
        pnd_S_Roth_Conv_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Roth_Ira_Conversion());                                                                             //Natural: ADD FORM.TIRF-ROTH-IRA-CONVERSION TO #S-ROTH-CONV-AMT ( J )
        //*  PALDE
        pnd_S_Postpn_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Postpn_Amt());                                                                                         //Natural: ADD FORM.TIRF-POSTPN-AMT TO #S-POSTPN-AMT ( J )
        //*  PALDE
        pnd_S_Repymnt_Amt.getValue(j).nadd(ldaTwrl451a.getForm_Tirf_Repayments_Amt());                                                                                    //Natural: ADD FORM.TIRF-REPAYMENTS-AMT TO #S-REPYMNT-AMT ( J )
    }
    private void sub_Update_Control_Totals() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        short decideConditionsMet929 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF FORM.TIRF-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl451a.getForm_Tirf_Company_Cde().equals("C"))))
        {
            decideConditionsMet929++;
                                                                                                                                                                          //Natural: PERFORM UPDATE-C-CONTROL-TOTALS
            sub_Update_C_Control_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl451a.getForm_Tirf_Company_Cde().equals("L"))))
        {
            decideConditionsMet929++;
            //*  EINCHG
                                                                                                                                                                          //Natural: PERFORM UPDATE-L-CONTROL-TOTALS
            sub_Update_L_Control_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'T', 'A'
        else if (condition((ldaTwrl451a.getForm_Tirf_Company_Cde().equals("T") || ldaTwrl451a.getForm_Tirf_Company_Cde().equals("A"))))
        {
            decideConditionsMet929++;
                                                                                                                                                                          //Natural: PERFORM UPDATE-T-CONTROL-TOTALS
            sub_Update_T_Control_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl451a.getForm_Tirf_Company_Cde().equals("S"))))
        {
            decideConditionsMet929++;
                                                                                                                                                                          //Natural: PERFORM UPDATE-S-CONTROL-TOTALS
            sub_Update_S_Control_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Update_General_Totals() throws Exception                                                                                                             //Natural: UPDATE-GENERAL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_G_Trans_Count.getValue(1,":",9).nadd(pnd_C_Trans_Count.getValue(1,":",9));                                                                                    //Natural: ADD #C-TRANS-COUNT ( 1:9 ) TO #G-TRANS-COUNT ( 1:9 )
        pnd_G_Classic_Amt.getValue(1,":",9).nadd(pnd_C_Classic_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #C-CLASSIC-AMT ( 1:9 ) TO #G-CLASSIC-AMT ( 1:9 )
        pnd_G_Roth_Amt.getValue(1,":",9).nadd(pnd_C_Roth_Amt.getValue(1,":",9));                                                                                          //Natural: ADD #C-ROTH-AMT ( 1:9 ) TO #G-ROTH-AMT ( 1:9 )
        pnd_G_Rollover_Amt.getValue(1,":",9).nadd(pnd_C_Rollover_Amt.getValue(1,":",9));                                                                                  //Natural: ADD #C-ROLLOVER-AMT ( 1:9 ) TO #G-ROLLOVER-AMT ( 1:9 )
        pnd_G_Rechar_Amt.getValue(1,":",9).nadd(pnd_C_Rechar_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #C-RECHAR-AMT ( 1:9 ) TO #G-RECHAR-AMT ( 1:9 )
        //*  06/30/06 RM
        pnd_G_Sep_Amt.getValue(1,":",9).nadd(pnd_C_Sep_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #C-SEP-AMT ( 1:9 ) TO #G-SEP-AMT ( 1:9 )
        pnd_G_Fmv_Amt.getValue(1,":",9).nadd(pnd_C_Fmv_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #C-FMV-AMT ( 1:9 ) TO #G-FMV-AMT ( 1:9 )
        pnd_G_Roth_Conv_Amt.getValue(1,":",9).nadd(pnd_C_Roth_Conv_Amt.getValue(1,":",9));                                                                                //Natural: ADD #C-ROTH-CONV-AMT ( 1:9 ) TO #G-ROTH-CONV-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Postpn_Amt.getValue(1,":",9).nadd(pnd_C_Postpn_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #C-POSTPN-AMT ( 1:9 ) TO #G-POSTPN-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Repymnt_Amt.getValue(1,":",9).nadd(pnd_C_Repymnt_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #C-REPYMNT-AMT ( 1:9 ) TO #G-REPYMNT-AMT ( 1:9 )
        pnd_G_Trans_Count.getValue(1,":",9).nadd(pnd_L_Trans_Count.getValue(1,":",9));                                                                                    //Natural: ADD #L-TRANS-COUNT ( 1:9 ) TO #G-TRANS-COUNT ( 1:9 )
        pnd_G_Classic_Amt.getValue(1,":",9).nadd(pnd_L_Classic_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #L-CLASSIC-AMT ( 1:9 ) TO #G-CLASSIC-AMT ( 1:9 )
        pnd_G_Roth_Amt.getValue(1,":",9).nadd(pnd_L_Roth_Amt.getValue(1,":",9));                                                                                          //Natural: ADD #L-ROTH-AMT ( 1:9 ) TO #G-ROTH-AMT ( 1:9 )
        pnd_G_Rollover_Amt.getValue(1,":",9).nadd(pnd_L_Rollover_Amt.getValue(1,":",9));                                                                                  //Natural: ADD #L-ROLLOVER-AMT ( 1:9 ) TO #G-ROLLOVER-AMT ( 1:9 )
        pnd_G_Rechar_Amt.getValue(1,":",9).nadd(pnd_L_Rechar_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #L-RECHAR-AMT ( 1:9 ) TO #G-RECHAR-AMT ( 1:9 )
        //*  06/30/06 RM
        pnd_G_Sep_Amt.getValue(1,":",9).nadd(pnd_L_Sep_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #L-SEP-AMT ( 1:9 ) TO #G-SEP-AMT ( 1:9 )
        pnd_G_Fmv_Amt.getValue(1,":",9).nadd(pnd_L_Fmv_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #L-FMV-AMT ( 1:9 ) TO #G-FMV-AMT ( 1:9 )
        pnd_G_Roth_Conv_Amt.getValue(1,":",9).nadd(pnd_L_Roth_Conv_Amt.getValue(1,":",9));                                                                                //Natural: ADD #L-ROTH-CONV-AMT ( 1:9 ) TO #G-ROTH-CONV-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Postpn_Amt.getValue(1,":",9).nadd(pnd_L_Postpn_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #L-POSTPN-AMT ( 1:9 ) TO #G-POSTPN-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Repymnt_Amt.getValue(1,":",9).nadd(pnd_L_Repymnt_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #L-REPYMNT-AMT ( 1:9 ) TO #G-REPYMNT-AMT ( 1:9 )
        pnd_G_Trans_Count.getValue(1,":",9).nadd(pnd_T_Trans_Count.getValue(1,":",9));                                                                                    //Natural: ADD #T-TRANS-COUNT ( 1:9 ) TO #G-TRANS-COUNT ( 1:9 )
        pnd_G_Classic_Amt.getValue(1,":",9).nadd(pnd_T_Classic_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #T-CLASSIC-AMT ( 1:9 ) TO #G-CLASSIC-AMT ( 1:9 )
        pnd_G_Roth_Amt.getValue(1,":",9).nadd(pnd_T_Roth_Amt.getValue(1,":",9));                                                                                          //Natural: ADD #T-ROTH-AMT ( 1:9 ) TO #G-ROTH-AMT ( 1:9 )
        pnd_G_Rollover_Amt.getValue(1,":",9).nadd(pnd_T_Rollover_Amt.getValue(1,":",9));                                                                                  //Natural: ADD #T-ROLLOVER-AMT ( 1:9 ) TO #G-ROLLOVER-AMT ( 1:9 )
        pnd_G_Rechar_Amt.getValue(1,":",9).nadd(pnd_T_Rechar_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #T-RECHAR-AMT ( 1:9 ) TO #G-RECHAR-AMT ( 1:9 )
        //*  06/30/06 RM
        pnd_G_Sep_Amt.getValue(1,":",9).nadd(pnd_T_Sep_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #T-SEP-AMT ( 1:9 ) TO #G-SEP-AMT ( 1:9 )
        pnd_G_Fmv_Amt.getValue(1,":",9).nadd(pnd_T_Fmv_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #T-FMV-AMT ( 1:9 ) TO #G-FMV-AMT ( 1:9 )
        pnd_G_Roth_Conv_Amt.getValue(1,":",9).nadd(pnd_T_Roth_Conv_Amt.getValue(1,":",9));                                                                                //Natural: ADD #T-ROTH-CONV-AMT ( 1:9 ) TO #G-ROTH-CONV-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Postpn_Amt.getValue(1,":",9).nadd(pnd_T_Postpn_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #T-POSTPN-AMT ( 1:9 ) TO #G-POSTPN-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Repymnt_Amt.getValue(1,":",9).nadd(pnd_T_Repymnt_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #T-REPYMNT-AMT ( 1:9 ) TO #G-REPYMNT-AMT ( 1:9 )
        pnd_G_Trans_Count.getValue(1,":",9).nadd(pnd_S_Trans_Count.getValue(1,":",9));                                                                                    //Natural: ADD #S-TRANS-COUNT ( 1:9 ) TO #G-TRANS-COUNT ( 1:9 )
        pnd_G_Classic_Amt.getValue(1,":",9).nadd(pnd_S_Classic_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #S-CLASSIC-AMT ( 1:9 ) TO #G-CLASSIC-AMT ( 1:9 )
        pnd_G_Roth_Amt.getValue(1,":",9).nadd(pnd_S_Roth_Amt.getValue(1,":",9));                                                                                          //Natural: ADD #S-ROTH-AMT ( 1:9 ) TO #G-ROTH-AMT ( 1:9 )
        pnd_G_Rollover_Amt.getValue(1,":",9).nadd(pnd_S_Rollover_Amt.getValue(1,":",9));                                                                                  //Natural: ADD #S-ROLLOVER-AMT ( 1:9 ) TO #G-ROLLOVER-AMT ( 1:9 )
        pnd_G_Rechar_Amt.getValue(1,":",9).nadd(pnd_S_Rechar_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #S-RECHAR-AMT ( 1:9 ) TO #G-RECHAR-AMT ( 1:9 )
        //*  06/30/06 RM
        pnd_G_Sep_Amt.getValue(1,":",9).nadd(pnd_S_Sep_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #S-SEP-AMT ( 1:9 ) TO #G-SEP-AMT ( 1:9 )
        pnd_G_Fmv_Amt.getValue(1,":",9).nadd(pnd_S_Fmv_Amt.getValue(1,":",9));                                                                                            //Natural: ADD #S-FMV-AMT ( 1:9 ) TO #G-FMV-AMT ( 1:9 )
        pnd_G_Roth_Conv_Amt.getValue(1,":",9).nadd(pnd_S_Roth_Conv_Amt.getValue(1,":",9));                                                                                //Natural: ADD #S-ROTH-CONV-AMT ( 1:9 ) TO #G-ROTH-CONV-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Postpn_Amt.getValue(1,":",9).nadd(pnd_S_Postpn_Amt.getValue(1,":",9));                                                                                      //Natural: ADD #S-POSTPN-AMT ( 1:9 ) TO #G-POSTPN-AMT ( 1:9 )
        //*  PALDE
        pnd_G_Repymnt_Amt.getValue(1,":",9).nadd(pnd_S_Repymnt_Amt.getValue(1,":",9));                                                                                    //Natural: ADD #S-REPYMNT-AMT ( 1:9 ) TO #G-REPYMNT-AMT ( 1:9 )
    }
    private void sub_Check_If_Already_Processed() throws Exception                                                                                                        //Natural: CHECK-IF-ALREADY-PROCESSED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Cntl_Pnd_S_Tbl.setValue(4);                                                                                                                             //Natural: ASSIGN #S-TBL := 4
        pnd_Super_Cntl_Pnd_S_Tax_Year.setValue(pnd_Input_Parms_Pnd_Tax_Year_N);                                                                                           //Natural: ASSIGN #S-TAX-YEAR := #TAX-YEAR-N
        pnd_Super_Cntl_Pnd_S_Form.setValue("5498");                                                                                                                       //Natural: ASSIGN #S-FORM := '5498'
        vw_cntl.startDatabaseRead                                                                                                                                         //Natural: READ ( 1 ) CNTL WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER-CNTL
        (
        "RL1",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super_Cntl, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") },
        1
        );
        RL1:
        while (condition(vw_cntl.readNextRow("RL1")))
        {
            if (condition(cntl_Tircntl_Tbl_Nbr.equals(pnd_Super_Cntl_Pnd_S_Tbl) && cntl_Tircntl_Tax_Year.equals(pnd_Super_Cntl_Pnd_S_Tax_Year_N) && cntl_Tircntl_Rpt_Form_Name.equals(pnd_Super_Cntl_Pnd_S_Form))) //Natural: IF TIRCNTL-TBL-NBR = #S-TBL AND TIRCNTL-TAX-YEAR = #S-TAX-YEAR-N AND TIRCNTL-RPT-FORM-NAME = #S-FORM
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            i.setValue(cntl_Count_Casttircntl_Rpt_Tbl_Pe);                                                                                                                //Natural: ASSIGN I := C*TIRCNTL-RPT-TBL-PE
            if (condition(i.equals(getZero())))                                                                                                                           //Natural: IF I = 0
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"IRS Original Reporting for 5498",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 01 ) '***' 25T 'IRS Original Reporting for 5498' 77T '***' / '***' 25T 'had not been run' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                    TabSetting(25),"had not been run",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),
                    "***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(91);  if (true) return;                                                                                                                 //Natural: TERMINATE 91
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().print(0, "=",cntl_Tircntl_Tax_Year,"=",cntl_Tircntl_Rpt_Dte.getValue(i),"=",pnd_Datx);                                                       //Natural: PRINT '=' TIRCNTL-TAX-YEAR '=' TIRCNTL-RPT-DTE ( I ) '=' #DATX
                //*     IF (TIRCNTL-RPT-DTE (I) + 25)  >  #DATX
                //*    AND  I  >  1
                //*        PERFORM  ERROR-DISPLAY-START
                //*        WRITE (01)
                //*              '***' 25T 'Last cycle of IRS Correction Reporting had'
                //*          77T '***'
                //*        /     '***' 25T 'been run for IRS Forms 5498'
                //*          77T '***'
                //*        /     '***' 25T 'within previous 25 days on' TIRCNTL-RPT-DTE (I)
                //*          77T '***'
                //*        /     '***' 06T 'PROGRAM...:' *PROGRAM
                //*          77T '***'
                //*        PERFORM  ERROR-DISPLAY-END
                //*        TERMINATE 92
                //*     END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(vw_cntl.getAstCOUNTER().equals(getZero())))                                                                                                         //Natural: IF *COUNTER ( RL1. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Report Form (5498) Control File Record Is Missing",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'Report Form (5498) Control File Record Is Missing' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP4910|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parms);                                                                                      //Natural: INPUT #INPUT-PARMS
                if (condition(pnd_Input_Parms_Pnd_Form.equals("5498")))                                                                                                   //Natural: IF #FORM = '5498'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"Unknown input parmameter:",pnd_Input_Parms_Pnd_Form,new TabSetting(77),"***",NEWLINE,"***",new        //Natural: WRITE ( 00 ) '***' 25T 'Unknown input parmameter:' #FORM 77T '***' / '***' 25T 'Valid Form Parameter is..."5498"' 77T '***'
                        TabSetting(25),"Valid Form Parameter is...'5498'",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Parms_Pnd_Tax_Year_N.equals(getZero())))                                                                                          //Natural: IF #TAX-YEAR-N = 0
                {
                    pnd_Input_Parms_Pnd_Tax_Year_N.compute(new ComputeParameters(false, pnd_Input_Parms_Pnd_Tax_Year_N), Global.getDATN().divide(10000).subtract(1));     //Natural: ASSIGN #TAX-YEAR-N := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Datx.setValue(Global.getDATX());                                                                                                                      //Natural: ASSIGN #DATX := *DATX
                pnd_Timx.setValue(Global.getTIMX());                                                                                                                      //Natural: ASSIGN #TIMX := *TIMX
                getReports().write(0, NEWLINE,new TabSetting(7),"IRS Correction Reporting parameters:",NEWLINE,new TabSetting(23),"Tax Year.............",pnd_Input_Parms_Pnd_Tax_Year_N,NEWLINE,new  //Natural: WRITE ( 00 ) / 07T 'IRS Correction Reporting parameters:' / 23T 'Tax Year.............' #TAX-YEAR-N / 23T 'Form.................' 'IRS 5498 Correction Reporting' / 13T 'System Date used for this run:' #DATX ( EM = MM/DD/YYYY ) / 13T 'System Time used for this run:' #TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    TabSetting(23),"Form.................","IRS 5498 Correction Reporting",NEWLINE,new TabSetting(13),"System Date used for this run:",pnd_Datx, 
                    new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(13),"System Time used for this run:",pnd_Timx, new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"IRS Correction Reporting parameters:",NEWLINE,new TabSetting(23),"Tax Year.............",pnd_Input_Parms_Pnd_Tax_Year_N,NEWLINE,new  //Natural: WRITE ( 01 ) / 07T 'IRS Correction Reporting parameters:' / 23T 'Tax Year.............' #TAX-YEAR-N / 23T 'Form.................' 'IRS 5498 Correction Reporting' / 13T 'System Date used for this run:' #DATX ( EM = MM/DD/YYYY ) / 13T 'System Time used for this run:' #TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    TabSetting(23),"Form.................","IRS 5498 Correction Reporting",NEWLINE,new TabSetting(13),"System Date used for this run:",pnd_Datx, 
                    new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(13),"System Time used for this run:",pnd_Timx, new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR K = 1 TO 9
        for (k.setValue(1); condition(k.lessOrEqual(9)); k.nadd(1))
        {
            short decideConditionsMet1081 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF K;//Natural: VALUES 1
            if (condition((k.equals(1))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("Form 5498 Total");                                                                                                                    //Natural: ASSIGN #LABEL := 'Form 5498 Total'
            }                                                                                                                                                             //Natural: VALUES 2
            else if (condition((k.equals(2))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("Prev. Reported ");                                                                                                                    //Natural: ASSIGN #LABEL := 'Prev. Reported '
            }                                                                                                                                                             //Natural: VALUES 3
            else if (condition((k.equals(3))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("No Change Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'No Change Forms'
            }                                                                                                                                                             //Natural: VALUES 4
            else if (condition((k.equals(4))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("Orig Zero Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'Orig Zero Forms'
            }                                                                                                                                                             //Natural: VALUES 5
            else if (condition((k.equals(5))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("LTin Zero Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'LTin Zero Forms'
            }                                                                                                                                                             //Natural: VALUES 6
            else if (condition((k.equals(6))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("Prev Rjct Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'Prev Rjct Forms'
            }                                                                                                                                                             //Natural: VALUES 7
            else if (condition((k.equals(7))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("New  Rjct Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'New  Rjct Forms'
            }                                                                                                                                                             //Natural: VALUES 8
            else if (condition((k.equals(8))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("Accepted  Forms");                                                                                                                    //Natural: ASSIGN #LABEL := 'Accepted  Forms'
            }                                                                                                                                                             //Natural: VALUES 9
            else if (condition((k.equals(9))))
            {
                decideConditionsMet1081++;
                pnd_Label.setValue("1st Of 2 Parts ");                                                                                                                    //Natural: ASSIGN #LABEL := '1st Of 2 Parts '
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  06/30/06  RM
            //*  07-13-2005
            //*  PALDE
            //*  PALDE
            getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),pnd_Label,new TabSetting(17),pnd_R_Trans_Count.getValue(k),                   //Natural: WRITE ( 02 ) NOTITLE NOHDR 01T #LABEL 17T #R-TRANS-COUNT ( K ) 27T #R-CLASSIC-AMT ( K ) 41T #R-ROLLOVER-AMT ( K ) 55T #R-ROTH-CONV-AMT ( K ) 69T #R-RECHAR-AMT ( K ) 83T #R-SEP-AMT ( K ) 97T #R-FMV-AMT ( K ) 113T #R-ROTH-AMT ( K ) 127T #R-POSTPN-AMT ( K ) 150T #R-REPYMNT-AMT ( K )
                new ReportEditMask ("ZZZZ,ZZ9"),new TabSetting(27),pnd_R_Classic_Amt.getValue(k), new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(41),pnd_R_Rollover_Amt.getValue(k), 
                new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(55),pnd_R_Roth_Conv_Amt.getValue(k), new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(69),pnd_R_Rechar_Amt.getValue(k), 
                new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(83),pnd_R_Sep_Amt.getValue(k), new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(97),pnd_R_Fmv_Amt.getValue(k), 
                new ReportEditMask ("ZZZZZZZZZZZ9.99"),new TabSetting(113),pnd_R_Roth_Amt.getValue(k), new ReportEditMask ("ZZZZZZZZZ9.99"),new TabSetting(127),pnd_R_Postpn_Amt.getValue(k), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(150),pnd_R_Repymnt_Amt.getValue(k), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(k.equals(7)))                                                                                                                                   //Natural: IF K = 7
            {
                getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"=",new RepeatItem(131));                                                 //Natural: WRITE ( 02 ) NOTITLE NOHDR 01T '=' ( 131 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        //*  06/30/06  RM
        //*  PALDE
        //*  PALDE
                                                                                                                                                                          //Natural: PERFORM UPDATE-GENERAL-TOTALS
        sub_Update_General_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_R_Trans_Count.getValue("*").setValue(pnd_C_Trans_Count.getValue("*"));                                                                                        //Natural: ASSIGN #R-TRANS-COUNT ( * ) := #C-TRANS-COUNT ( * )
        pnd_R_Classic_Amt.getValue("*").setValue(pnd_C_Classic_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-CLASSIC-AMT ( * ) := #C-CLASSIC-AMT ( * )
        pnd_R_Roth_Amt.getValue("*").setValue(pnd_C_Roth_Amt.getValue("*"));                                                                                              //Natural: ASSIGN #R-ROTH-AMT ( * ) := #C-ROTH-AMT ( * )
        pnd_R_Rollover_Amt.getValue("*").setValue(pnd_C_Rollover_Amt.getValue("*"));                                                                                      //Natural: ASSIGN #R-ROLLOVER-AMT ( * ) := #C-ROLLOVER-AMT ( * )
        pnd_R_Rechar_Amt.getValue("*").setValue(pnd_C_Rechar_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-RECHAR-AMT ( * ) := #C-RECHAR-AMT ( * )
        pnd_R_Sep_Amt.getValue("*").setValue(pnd_C_Sep_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-SEP-AMT ( * ) := #C-SEP-AMT ( * )
        pnd_R_Fmv_Amt.getValue("*").setValue(pnd_C_Fmv_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-FMV-AMT ( * ) := #C-FMV-AMT ( * )
        pnd_R_Roth_Conv_Amt.getValue("*").setValue(pnd_C_Roth_Conv_Amt.getValue("*"));                                                                                    //Natural: ASSIGN #R-ROTH-CONV-AMT ( * ) := #C-ROTH-CONV-AMT ( * )
        pnd_R_Postpn_Amt.getValue("*").setValue(pnd_C_Postpn_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-POSTPN-AMT ( * ) := #C-POSTPN-AMT ( * )
        pnd_R_Repymnt_Amt.getValue("*").setValue(pnd_C_Repymnt_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-REPYMNT-AMT ( * ) := #C-REPYMNT-AMT ( * )
        pnd_Company_Name.setValue("CREF - College Retirement Equities Fund");                                                                                             //Natural: ASSIGN #COMPANY-NAME := 'CREF - College Retirement Equities Fund'
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  06/30/06  RM
        //*  PALDE
        //*  PALDE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        pnd_R_Trans_Count.getValue("*").setValue(pnd_T_Trans_Count.getValue("*"));                                                                                        //Natural: ASSIGN #R-TRANS-COUNT ( * ) := #T-TRANS-COUNT ( * )
        pnd_R_Classic_Amt.getValue("*").setValue(pnd_T_Classic_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-CLASSIC-AMT ( * ) := #T-CLASSIC-AMT ( * )
        pnd_R_Roth_Amt.getValue("*").setValue(pnd_T_Roth_Amt.getValue("*"));                                                                                              //Natural: ASSIGN #R-ROTH-AMT ( * ) := #T-ROTH-AMT ( * )
        pnd_R_Rollover_Amt.getValue("*").setValue(pnd_T_Rollover_Amt.getValue("*"));                                                                                      //Natural: ASSIGN #R-ROLLOVER-AMT ( * ) := #T-ROLLOVER-AMT ( * )
        pnd_R_Rechar_Amt.getValue("*").setValue(pnd_T_Rechar_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-RECHAR-AMT ( * ) := #T-RECHAR-AMT ( * )
        pnd_R_Sep_Amt.getValue("*").setValue(pnd_T_Sep_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-SEP-AMT ( * ) := #T-SEP-AMT ( * )
        pnd_R_Fmv_Amt.getValue("*").setValue(pnd_T_Fmv_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-FMV-AMT ( * ) := #T-FMV-AMT ( * )
        pnd_R_Roth_Conv_Amt.getValue("*").setValue(pnd_T_Roth_Conv_Amt.getValue("*"));                                                                                    //Natural: ASSIGN #R-ROTH-CONV-AMT ( * ) := #T-ROTH-CONV-AMT ( * )
        pnd_R_Postpn_Amt.getValue("*").setValue(pnd_T_Postpn_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-POSTPN-AMT ( * ) := #T-POSTPN-AMT ( * )
        pnd_R_Repymnt_Amt.getValue("*").setValue(pnd_T_Repymnt_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-REPYMNT-AMT ( * ) := #T-REPYMNT-AMT ( * )
        pnd_Company_Name.setValue("TIAA - Teachers Insurance and Annuity Association");                                                                                   //Natural: ASSIGN #COMPANY-NAME := 'TIAA - Teachers Insurance and Annuity Association'
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  06/30/06  RM
        //*  PALDE
        //*  PALDE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        pnd_R_Trans_Count.getValue("*").setValue(pnd_L_Trans_Count.getValue("*"));                                                                                        //Natural: ASSIGN #R-TRANS-COUNT ( * ) := #L-TRANS-COUNT ( * )
        pnd_R_Classic_Amt.getValue("*").setValue(pnd_L_Classic_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-CLASSIC-AMT ( * ) := #L-CLASSIC-AMT ( * )
        pnd_R_Roth_Amt.getValue("*").setValue(pnd_L_Roth_Amt.getValue("*"));                                                                                              //Natural: ASSIGN #R-ROTH-AMT ( * ) := #L-ROTH-AMT ( * )
        pnd_R_Rollover_Amt.getValue("*").setValue(pnd_L_Rollover_Amt.getValue("*"));                                                                                      //Natural: ASSIGN #R-ROLLOVER-AMT ( * ) := #L-ROLLOVER-AMT ( * )
        pnd_R_Rechar_Amt.getValue("*").setValue(pnd_L_Rechar_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-RECHAR-AMT ( * ) := #L-RECHAR-AMT ( * )
        pnd_R_Sep_Amt.getValue("*").setValue(pnd_L_Sep_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-SEP-AMT ( * ) := #L-SEP-AMT ( * )
        pnd_R_Fmv_Amt.getValue("*").setValue(pnd_L_Fmv_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-FMV-AMT ( * ) := #L-FMV-AMT ( * )
        pnd_R_Roth_Conv_Amt.getValue("*").setValue(pnd_L_Roth_Conv_Amt.getValue("*"));                                                                                    //Natural: ASSIGN #R-ROTH-CONV-AMT ( * ) := #L-ROTH-CONV-AMT ( * )
        pnd_R_Postpn_Amt.getValue("*").setValue(pnd_L_Postpn_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-POSTPN-AMT ( * ) := #L-POSTPN-AMT ( * )
        pnd_R_Repymnt_Amt.getValue("*").setValue(pnd_L_Repymnt_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-REPYMNT-AMT ( * ) := #L-REPYMNT-AMT ( * )
        pnd_Company_Name.setValue("LIFE - Life Insurance Company");                                                                                                       //Natural: ASSIGN #COMPANY-NAME := 'LIFE - Life Insurance Company'
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  06/30/06  RM
        //*  PALDE
        //*  PALDE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        pnd_R_Trans_Count.getValue("*").setValue(pnd_S_Trans_Count.getValue("*"));                                                                                        //Natural: ASSIGN #R-TRANS-COUNT ( * ) := #S-TRANS-COUNT ( * )
        pnd_R_Classic_Amt.getValue("*").setValue(pnd_S_Classic_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-CLASSIC-AMT ( * ) := #S-CLASSIC-AMT ( * )
        pnd_R_Roth_Amt.getValue("*").setValue(pnd_S_Roth_Amt.getValue("*"));                                                                                              //Natural: ASSIGN #R-ROTH-AMT ( * ) := #S-ROTH-AMT ( * )
        pnd_R_Rollover_Amt.getValue("*").setValue(pnd_S_Rollover_Amt.getValue("*"));                                                                                      //Natural: ASSIGN #R-ROLLOVER-AMT ( * ) := #S-ROLLOVER-AMT ( * )
        pnd_R_Rechar_Amt.getValue("*").setValue(pnd_S_Rechar_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-RECHAR-AMT ( * ) := #S-RECHAR-AMT ( * )
        pnd_R_Sep_Amt.getValue("*").setValue(pnd_S_Sep_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-SEP-AMT ( * ) := #S-SEP-AMT ( * )
        pnd_R_Fmv_Amt.getValue("*").setValue(pnd_S_Fmv_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-FMV-AMT ( * ) := #S-FMV-AMT ( * )
        pnd_R_Roth_Conv_Amt.getValue("*").setValue(pnd_S_Roth_Conv_Amt.getValue("*"));                                                                                    //Natural: ASSIGN #R-ROTH-CONV-AMT ( * ) := #S-ROTH-CONV-AMT ( * )
        pnd_R_Postpn_Amt.getValue("*").setValue(pnd_S_Postpn_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-POSTPN-AMT ( * ) := #S-POSTPN-AMT ( * )
        pnd_R_Repymnt_Amt.getValue("*").setValue(pnd_S_Repymnt_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-REPYMNT-AMT ( * ) := #S-REPYMNT-AMT ( * )
        pnd_Company_Name.setValue("TCII - TIAA-CREF Indiv. and Institut. Serv. Inc.");                                                                                    //Natural: ASSIGN #COMPANY-NAME := 'TCII - TIAA-CREF Indiv. and Institut. Serv. Inc.'
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  06/30/06  RM
        //*  PALDE
        //*  PALDE
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        pnd_R_Trans_Count.getValue("*").setValue(pnd_G_Trans_Count.getValue("*"));                                                                                        //Natural: ASSIGN #R-TRANS-COUNT ( * ) := #G-TRANS-COUNT ( * )
        pnd_R_Classic_Amt.getValue("*").setValue(pnd_G_Classic_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-CLASSIC-AMT ( * ) := #G-CLASSIC-AMT ( * )
        pnd_R_Roth_Amt.getValue("*").setValue(pnd_G_Roth_Amt.getValue("*"));                                                                                              //Natural: ASSIGN #R-ROTH-AMT ( * ) := #G-ROTH-AMT ( * )
        pnd_R_Rollover_Amt.getValue("*").setValue(pnd_G_Rollover_Amt.getValue("*"));                                                                                      //Natural: ASSIGN #R-ROLLOVER-AMT ( * ) := #G-ROLLOVER-AMT ( * )
        pnd_R_Rechar_Amt.getValue("*").setValue(pnd_G_Rechar_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-RECHAR-AMT ( * ) := #G-RECHAR-AMT ( * )
        pnd_R_Sep_Amt.getValue("*").setValue(pnd_G_Sep_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-SEP-AMT ( * ) := #G-SEP-AMT ( * )
        pnd_R_Fmv_Amt.getValue("*").setValue(pnd_G_Fmv_Amt.getValue("*"));                                                                                                //Natural: ASSIGN #R-FMV-AMT ( * ) := #G-FMV-AMT ( * )
        pnd_R_Roth_Conv_Amt.getValue("*").setValue(pnd_G_Roth_Conv_Amt.getValue("*"));                                                                                    //Natural: ASSIGN #R-ROTH-CONV-AMT ( * ) := #G-ROTH-CONV-AMT ( * )
        pnd_R_Postpn_Amt.getValue("*").setValue(pnd_G_Postpn_Amt.getValue("*"));                                                                                          //Natural: ASSIGN #R-POSTPN-AMT ( * ) := #G-POSTPN-AMT ( * )
        pnd_R_Repymnt_Amt.getValue("*").setValue(pnd_G_Repymnt_Amt.getValue("*"));                                                                                        //Natural: ASSIGN #R-REPYMNT-AMT ( * ) := #G-REPYMNT-AMT ( * )
        pnd_Company_Name.setValue("General Total");                                                                                                                       //Natural: ASSIGN #COMPANY-NAME := 'General Total'
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"IRA Contributions Form (5498) IRS Extract",new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 45T 'IRA Contributions Form (5498) IRS Extract' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    //*  WRITE (01) NOTITLE
                    //*    25T #HEADER-DATE    (EM=MM/DD/YYYY)
                    //*    58T #SOURCE-TOTAL-HEADER
                    //*    97T #INTERFACE-DATE (EM=MM/DD/YYYY)
                    //*  SKIP (01) 2 LINES
                    //*  WRITE (01) NOTITLE
                    //*    22T '       Input        '
                    //*    43T '      Bypassed      '
                    //*    64T '      Rejected      '
                    //*    85T '      Accepted      '
                    //*  WRITE (01) NOTITLE
                    //*    01T '                    '
                    //*    22T '===================='
                    //*    43T '===================='
                    //*    64T '===================='
                    //*    85T '===================='
                    //*   106T '===================='
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(45),"   Tax Withholding & Reporting System    ",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 45T '   Tax Withholding & Reporting System    ' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"IRS 5498 Correction Reporting Summary Rpt",new  //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 45T 'IRS 5498 Correction Reporting Summary Rpt' 120T 'REPORT: RPT2'
                        TabSetting(120),"REPORT: RPT2");
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),"             Tax Year:",pnd_Super_Cntl_Pnd_S_Tax_Year);                                //Natural: WRITE ( 02 ) NOTITLE 45T '             Tax Year:' #S-TAX-YEAR
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(45),"                Form 5498                ");                                           //Natural: WRITE ( 02 ) NOTITLE 45T '                Form 5498                '
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Company: ",pnd_Company_Name);                                                           //Natural: WRITE ( 02 ) NOTITLE 01T 'Company: ' #COMPANY-NAME
                    getReports().skip(2, 4);                                                                                                                              //Natural: SKIP ( 02 ) 4 LINES
                    //*   06/30/06 RM
                    //*  07-13-2005
                    //*  07-13-2005
                    //*  PALDE
                    //*  PALDE
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(17),"Form Cnt ",new TabSetting(27),"   CLASSIC   ",new TabSetting(41),"  ROLLOVER   ",new   //Natural: WRITE ( 02 ) NOTITLE 17T 'Form Cnt ' 27T '   CLASSIC   ' 41T '  ROLLOVER   ' 55T '  ROTH CONV  ' 69T '  RECHARAC   ' 83T '     SEP     ' 97T '    F.M.V.     ' 113T '    ROTH     ' 127T '    POSTPN   ' 141T '    REPYMNT  '
                        TabSetting(55),"  ROTH CONV  ",new TabSetting(69),"  RECHARAC   ",new TabSetting(83),"     SEP     ",new TabSetting(97),"    F.M.V.     ",new 
                        TabSetting(113),"    ROTH     ",new TabSetting(127),"    POSTPN   ",new TabSetting(141),"    REPYMNT  ");
                    //*   06/30/06 RM
                    //*  07-13-2005
                    //*  07-13-2005
                    //*  PALDE
                    //*  PALDE
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(17),"=========",new TabSetting(27),"=============",new TabSetting(41),"=============",new   //Natural: WRITE ( 02 ) NOTITLE 17T '=========' 27T '=============' 41T '=============' 55T '=============' 69T '=============' 83T '=============' 97T '===============' 113T '=============' 127T '=============' 141T '============='
                        TabSetting(55),"=============",new TabSetting(69),"=============",new TabSetting(83),"=============",new TabSetting(97),"===============",new 
                        TabSetting(113),"=============",new TabSetting(127),"=============",new TabSetting(141),"=============");
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=170");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
    }
}
