/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:41 PM
**        * FROM NATURAL PROGRAM : Twrp3185
************************************************************
**        * FILE NAME            : Twrp3185.java
**        * CLASS NAME           : Twrp3185
**        * INSTANCE NAME        : Twrp3185
************************************************************
************************************************************************
* PROGRAM  : TWRP3185 (COPY VERSION OF TWRP0985)
* FUNCTION : PRINTS STATE TRANSACTIONS BY SOURCE CODES
*            PROCESS US STATE ONLY (RESIDENCY-CODE = 1 TO 57)
* INPUT    : FLAT-FILE1 & 3
* OUTPUT   : STATE TRANSACTIONS BY SOURCE CODES REPORT
*            EXCEPTION REPORT
* HISTORY  :
* -------------------------------------------------------------
* 07/13/99 : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*              IVC-IND = K BUT NOT ROLL-OVER  (EDITH)
*            INCLUDED NEW COMPANY CODE 'L' (TIAA-LIFE)
* 12/10/99 : NEW DEFINITION OF ROLLOVER BASED ON DISTRIBUTION CODE
*            INCLUDED THE FF. SOURCE CODES :
*                'IP' FOR IPRO
*                'TMAP' (APAL) - CREATED DUE TO DELETED 'APTM'
*                'TMIL' (IAIL) - CREATED DUE TO DELETED 'ILTM'
*            INCLUDED PROCESSING OF APO,UNKNOWN US STATES, US CTZ/
*                 RESIDENT ALIEN LIVING ABROAD
*            INCLUDED PRINTING OF 3 REPORTS - TAXABLE/GROSS/NON-RPTBLE
*                                                STATE REPORTS.
* 12/14/99 : DISTRIBUTION CODES '6' & 'Y' NOT ROLLOVER ANYMORE
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 07/22/03 RM INVESTMENT SOLUTIONS - AUTOMATE SSSS PROJECT
*             ADD NEW SOURCE CODE 'SI' FOR INVESTMENT SOLUTIONS
*             UPDATE ALL RELATED LOGIC TO INCLUDE 'SI'.
* 11/08/04 : MS - TOPS RELEASE 3 - TRUST
* 12/01/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'OP', 'NL', 'PL'.
* 06/06/06  RM - NAVISYS CHANGES
*           ADD NEW SOURCE CODE 'NV'.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'NV'.
* 10/31/06  RM - VUL CHANGES
*           ADD NEW SOURCE CODE 'VL'.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'VL'.
* 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*              'Roll Roth 403 to Roth IRA - Death'
* 09/28/11  M BERLIN
*           ADDED NEW MCCAMISH SOURCE CODE 'AM'
*           MODELLED FROM 'VL'.                           /* 09/28/11
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3185 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3001 ldaTwrl3001;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_State_Wthld;
    private DbsField pnd_Counters_Pnd_Local_Wthld;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Reject;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Source;
    private DbsField pnd_New_Source;
    private DbsField pnd_Company;
    private DbsField pnd_Occ;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_First;
    private DbsField pnd_First_Rec;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;
    private DbsField pnd_Var_File1_Pnd_Intf_Date_Fr;
    private DbsField pnd_Var_File1_Pnd_Intf_Date_To;
    private DbsField pnd_Var_File1_Pnd_Pymt_Date_Fr;
    private DbsField pnd_Var_File1_Pnd_Pymt_Date_To;
    private DbsField pnd_Tot_Prt;
    private DbsField pnd_Sv_Search_Rc;
    private DbsField pnd_State_Desc;
    private DbsField pnd_Prt_Tot_Ol;
    private DbsField pnd_Prt_Source;
    private DbsField pnd_Sub_Src;
    private DbsField pnd_Eof;
    private DbsField pnd_Error_Source;
    private DbsField pnd_Total_For_Ap_Ia;
    private DbsField pnd_Found;
    private DbsField pnd_Ctr;
    private DbsField pnd_Sv_Srce12;
    private DbsField pnd_Source12;
    private DbsField pnd_Source34;
    private DbsField pnd_Tax_Amt;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Company;
    private DbsField pnd_Savers_Pnd_Sv_Srce_Code;
    private DbsField pnd_Savers_Pnd_Sv_Res_Code;
    private DbsField pnd_Savers_Pnd_Sv_Tax_Ctz;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Pnd_Residency_Code;
    private DbsField pnd_State_Table_Pnd_State_Indicator;
    private DbsField pnd_State_Table_Pnd_State_Description;
    private DbsField pnd_Arr;
    private DbsField pnd_Rc_Arr;
    private DbsField pnd_Found_Desc;
    private DbsField pnd_Rpt;
    private DbsField pnd_State_Ind;
    private DbsField pnd_L;
    private DbsField pnd_First_Prt;
    private DbsField pnd_Print_Sc;
    private DbsField pnd_Print_State;
    private DbsField pnd_Print_Tc;
    private DbsField pnd_Reset_Rc_Arr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3001 = new LdaTwrl3001();
        registerRecord(ldaTwrl3001);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 4, 1, 65));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_State_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_State_Wthld", "#STATE-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Local_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Local_Wthld", "#LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Rec_Reject = localVariables.newFieldInRecord("pnd_Rec_Reject", "#REC-REJECT", FieldType.NUMERIC, 7);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_New_Source = localVariables.newFieldInRecord("pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Company = localVariables.newFieldInRecord("pnd_Company", "#COMPANY", FieldType.STRING, 1);
        pnd_Occ = localVariables.newFieldInRecord("pnd_Occ", "#OCC", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Intf_Date_Fr = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Intf_Date_Fr", "#INTF-DATE-FR", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Intf_Date_To = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Intf_Date_To", "#INTF-DATE-TO", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Pymt_Date_Fr = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Pymt_Date_Fr", "#PYMT-DATE-FR", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Pymt_Date_To = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Pymt_Date_To", "#PYMT-DATE-TO", FieldType.STRING, 8);
        pnd_Tot_Prt = localVariables.newFieldInRecord("pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Sv_Search_Rc = localVariables.newFieldInRecord("pnd_Sv_Search_Rc", "#SV-SEARCH-RC", FieldType.STRING, 2);
        pnd_State_Desc = localVariables.newFieldArrayInRecord("pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 17, new DbsArrayController(1, 61));
        pnd_Prt_Tot_Ol = localVariables.newFieldInRecord("pnd_Prt_Tot_Ol", "#PRT-TOT-OL", FieldType.NUMERIC, 1);
        pnd_Prt_Source = localVariables.newFieldInRecord("pnd_Prt_Source", "#PRT-SOURCE", FieldType.STRING, 4);
        pnd_Sub_Src = localVariables.newFieldInRecord("pnd_Sub_Src", "#SUB-SRC", FieldType.STRING, 2);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Error_Source = localVariables.newFieldInRecord("pnd_Error_Source", "#ERROR-SOURCE", FieldType.BOOLEAN, 1);
        pnd_Total_For_Ap_Ia = localVariables.newFieldInRecord("pnd_Total_For_Ap_Ia", "#TOTAL-FOR-AP-IA", FieldType.BOOLEAN, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 2);
        pnd_Sv_Srce12 = localVariables.newFieldInRecord("pnd_Sv_Srce12", "#SV-SRCE12", FieldType.STRING, 2);
        pnd_Source12 = localVariables.newFieldInRecord("pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Source34 = localVariables.newFieldInRecord("pnd_Source34", "#SOURCE34", FieldType.STRING, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Company = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Srce_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 4);
        pnd_Savers_Pnd_Sv_Res_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Res_Code", "#SV-RES-CODE", FieldType.STRING, 2);
        pnd_Savers_Pnd_Sv_Tax_Ctz = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Tax_Ctz", "#SV-TAX-CTZ", FieldType.STRING, 1);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Pnd_Residency_Code = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 
            2, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Indicator = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Indicator", "#STATE-INDICATOR", FieldType.STRING, 
            1, new DbsArrayController(1, 100));
        pnd_State_Table_Pnd_State_Description = pnd_State_Table.newFieldArrayInGroup("pnd_State_Table_Pnd_State_Description", "#STATE-DESCRIPTION", FieldType.STRING, 
            19, new DbsArrayController(1, 100));
        pnd_Arr = localVariables.newFieldInRecord("pnd_Arr", "#ARR", FieldType.NUMERIC, 2);
        pnd_Rc_Arr = localVariables.newFieldArrayInRecord("pnd_Rc_Arr", "#RC-ARR", FieldType.STRING, 2, new DbsArrayController(1, 59));
        pnd_Found_Desc = localVariables.newFieldInRecord("pnd_Found_Desc", "#FOUND-DESC", FieldType.BOOLEAN, 1);
        pnd_Rpt = localVariables.newFieldInRecord("pnd_Rpt", "#RPT", FieldType.NUMERIC, 1);
        pnd_State_Ind = localVariables.newFieldInRecord("pnd_State_Ind", "#STATE-IND", FieldType.STRING, 1);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 1);
        pnd_First_Prt = localVariables.newFieldArrayInRecord("pnd_First_Prt", "#FIRST-PRT", FieldType.BOOLEAN, 1, new DbsArrayController(1, 4));
        pnd_Print_Sc = localVariables.newFieldArrayInRecord("pnd_Print_Sc", "#PRINT-SC", FieldType.STRING, 4, new DbsArrayController(1, 4));
        pnd_Print_State = localVariables.newFieldInRecord("pnd_Print_State", "#PRINT-STATE", FieldType.STRING, 17);
        pnd_Print_Tc = localVariables.newFieldInRecord("pnd_Print_Tc", "#PRINT-TC", FieldType.STRING, 21);
        pnd_Reset_Rc_Arr = localVariables.newFieldInRecord("pnd_Reset_Rc_Arr", "#RESET-RC-ARR", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3001.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_First_Rec.setInitialValue(true);
        pnd_Tot_Prt.setInitialValue(false);
        pnd_Prt_Tot_Ol.setInitialValue(0);
        pnd_Eof.setInitialValue(false);
        pnd_Error_Source.setInitialValue(false);
        pnd_Total_For_Ap_Ia.setInitialValue(false);
        pnd_Found.setInitialValue(false);
        pnd_Found_Desc.setInitialValue(false);
        pnd_First_Prt.getValue(1).setInitialValue(true);
        pnd_Reset_Rc_Arr.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3185() throws Exception
    {
        super("Twrp3185");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        setupReports();
        //*  STATE TRANS
        //*  STATE CODE NOT ON TABLE
        //*  ERRONEOUS SOURCE CODES
        //*  TAXABLE REPORTABLE STATE TRANS
        //*  GROSS REPORTABLE STATE TRANS
        //*  NON-REPORTABLE STATE TRANS
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132;//Natural: FORMAT ( 3 ) PS = 60 LS = 132;//Natural: FORMAT ( 4 ) PS = 60 LS = 132;//Natural: FORMAT ( 5 ) PS = 60 LS = 132;//Natural: FORMAT ( 6 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INFO-FF1
        while (condition(getWorkFiles().read(1, ldaTwrl3001.getPnd_Info_Ff1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr());                                                                            //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YR
            pnd_Var_File1_Pnd_Intf_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr());                                                                  //Natural: ASSIGN #INTF-DATE-FR := #FF1-INTF-DATE-FR
            pnd_Var_File1_Pnd_Intf_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_To());                                                                  //Natural: ASSIGN #INTF-DATE-TO := #FF1-INTF-DATE-TO
            pnd_Var_File1_Pnd_Pymt_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr());                                                                  //Natural: ASSIGN #PYMT-DATE-FR := #FF1-PYMT-DATE-FR
            pnd_Var_File1_Pnd_Pymt_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To());                                                                  //Natural: ASSIGN #PYMT-DATE-TO := #FF1-PYMT-DATE-TO
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                 /* CREATE A STATE TABLE BASED ON FILE98-TABLE2
        DbsUtil.callnat(Twrn0902.class , getCurrentProcessState(), pnd_Var_File1_Pnd_Tax_Year, pnd_State_Table_Pnd_Residency_Code.getValue("*"), pnd_State_Table_Pnd_State_Indicator.getValue("*"),  //Natural: CALLNAT 'TWRN0902' #TAX-YEAR #RESIDENCY-CODE ( * ) #STATE-INDICATOR ( * ) #STATE-DESCRIPTION ( * )
            pnd_State_Table_Pnd_State_Description.getValue("*"));
        if (condition(Global.isEscape())) return;
        //*  - - - - - - - - - - - - - - - - - - -
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 02 RECORD #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            if (condition(pnd_First_Rec.getBoolean()))                                                                                                                    //Natural: IF #FIRST-REC
            {
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                              //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals(pnd_Savers_Pnd_Sv_Tax_Ctz)))                                                 //Natural: IF #FF3-TAX-CITIZENSHIP = #SV-TAX-CTZ
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                               //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                        sub_Source_Brk();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                        sub_Reset_Ctrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                    sub_Company_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- PROCESS-RECORD
            //*                  -------------
            //*                         (-- DEFINE ARR-ELEM TO ACCOUNT RECORD --)
            pnd_Arr.reset();                                                                                                                                              //Natural: RESET #ARR
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Type().equals("1")))                                                                            //Natural: IF #FF3-RESIDENCY-TYPE = '1'
            {
                short decideConditionsMet226 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #FF3-RESIDENCY-CODE;//Natural: VALUE '00' : '57'
                if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("00' : '57"))))
                {
                    decideConditionsMet226++;
                    //*  NUMERIC
                    //*  US-STATES
                    if (condition(DbsUtil.maskMatches(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code(),"99")))                                                      //Natural: IF #FF3-RESIDENCY-CODE = MASK ( 99 )
                    {
                        pnd_Arr.compute(new ComputeParameters(false, pnd_Arr), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().val().add(1));                      //Natural: ASSIGN #ARR := VAL ( #FF3-RESIDENCY-CODE ) + 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Arr.setValue(60);                                                                                                                             //Natural: ASSIGN #ARR := 60
                        //*  UNKNOWN U.S. STATES
                        //*  RT1 NOT U.S.
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE '97'
                else if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("97"))))
                {
                    decideConditionsMet226++;
                    pnd_Arr.setValue(59);                                                                                                                                 //Natural: ASSIGN #ARR := 59
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Arr.setValue(60);                                                                                                                                 //Natural: ASSIGN #ARR := 60
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  US CTZN ABROAD & OTHERS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Arr.setValue(61);                                                                                                                                     //Natural: ASSIGN #ARR := 61
                //*  STATE SUMMARY REPORT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rpt.setValue(1);                                                                                                                                          //Natural: ASSIGN #RPT := 1
                                                                                                                                                                          //Natural: PERFORM COMPUTE-TAX-AMT
            sub_Compute_Tax_Amt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-RECORD
            sub_Account_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*           ( -- SAVE U.S. STATES BEING PROCESSED TO #RC-ARR --)
            if (condition(pnd_Arr.greaterOrEqual(1) && pnd_Arr.lessOrEqual(59)))                                                                                          //Natural: IF #ARR = 1 THRU 59
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Savers_Pnd_Sv_Res_Code)))                                                 //Natural: IF #FF3-RESIDENCY-CODE = #SV-RES-CODE
                {
                    //*                                        12/14/99
                    //*  TRUE WHEN BREAK OF SRCE-CD/TAX-CZ/CO
                    if (condition(pnd_Reset_Rc_Arr.getBoolean()))                                                                                                         //Natural: IF #RESET-RC-ARR
                    {
                        pnd_Reset_Rc_Arr.setValue(false);                                                                                                                 //Natural: ASSIGN #RESET-RC-ARR := FALSE
                        pnd_Rc_Arr.getValue(pnd_Arr).setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                    //Natural: ASSIGN #RC-ARR ( #ARR ) := #FF3-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM SEARCH-STATE-IND
                        sub_Search_State_Ind();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rc_Arr.getValue(pnd_Arr).setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                        //Natural: ASSIGN #RC-ARR ( #ARR ) := #FF3-RESIDENCY-CODE
                    pnd_Savers_Pnd_Sv_Res_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                          //Natural: ASSIGN #SV-RES-CODE := #FF3-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM SEARCH-STATE-IND
                    sub_Search_State_Ind();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-RECORD-OTHER-REPORT
                sub_Account_Record_Other_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-TAX-AMT
        //*                                           BG
        //*      OR= '6')      H4
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCOUNT-RECORD
        //*          ( -- ACCOUNT RECORD TO STATE SUMMARY 'totals' BUCKET --)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEARCH-STATE-IND
        //*                  ---------------
        //*    (-- CHECK FOR STATE INDICATOR OF A RES.CDE. FROM STATE TABLE --)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCOUNT-RECORD-OTHER-REPORT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-PER-SC
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-1
        //*                  --------------
        //*    'SYSTEM/SRCE'        #NEW-SOURCE  (IS=ON)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-2
        //*                  ------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-3
        //*                  ------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-4
        //*                  ------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SOURCE-TO-PRT
        //*                  -------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SOURCE-BRK
        //*                  ----------
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Eof.setValue(true);                                                                                                                                       //Natural: ASSIGN #EOF := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_First_Prt.getValue(2).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 2 )
        {
            ignore();
            pnd_Savers_Pnd_Sv_Tax_Ctz.reset();                                                                                                                            //Natural: RESET #SV-TAX-CTZ #SV-COMPANY
            pnd_Savers_Pnd_Sv_Company.reset();
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 4 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_First_Prt.getValue(3).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 3 )
        {
            ignore();
            pnd_Savers_Pnd_Sv_Tax_Ctz.reset();                                                                                                                            //Natural: RESET #SV-TAX-CTZ #SV-COMPANY
            pnd_Savers_Pnd_Sv_Company.reset();
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 5 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_First_Prt.getValue(4).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 4 )
        {
            ignore();
            pnd_Savers_Pnd_Sv_Tax_Ctz.reset();                                                                                                                            //Natural: RESET #SV-TAX-CTZ #SV-COMPANY
            pnd_Savers_Pnd_Sv_Company.reset();
            getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 6 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"------ CONTROL TOTAL",NEWLINE,NEWLINE,NEWLINE,"RUNDATE    : ",Global.getDATX(),  //Natural: WRITE ( 1 ) *INIT-USER '-' *PROGRAM '------ CONTROL TOTAL' // / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TAX-YEAR / 'INTERFACE DATE :' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / ' PAYMENT  DATE :' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// '1) NO. OF RECORDS READ      : ' #REC-READ / '2) NO. OF RECORDS PROCESSED : ' #REC-PROCESS / '3) NO. OF RECORDS REJECTED  : ' #REC-REJECT
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",pnd_Var_File1_Pnd_Tax_Year,NEWLINE,"INTERFACE DATE :",
            pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE," PAYMENT  DATE :",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",pnd_Var_File1_Pnd_Pymt_Date_To,
            NEWLINE,NEWLINE,NEWLINE,"1) NO. OF RECORDS READ      : ",pnd_Rec_Read,NEWLINE,"2) NO. OF RECORDS PROCESSED : ",pnd_Rec_Process,NEWLINE,"3) NO. OF RECORDS REJECTED  : ",
            pnd_Rec_Reject);
        if (Global.isEscape()) return;
        //*                  ------------
        //* *------------
        //*                  ----------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
    }
    private void sub_Compute_Tax_Amt() throws Exception                                                                                                                   //Natural: COMPUTE-TAX-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pnd_Tax_Amt.reset();                                                                                                                                              //Natural: RESET #TAX-AMT
        //*  RECHAR  5/30/2002
        //*  ROLLOVER
        //*  RS0711
        short decideConditionsMet402 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
        if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
        {
            decideConditionsMet402++;
            pnd_Tax_Amt.setValue(0);                                                                                                                                      //Natural: ASSIGN #TAX-AMT := 0
        }                                                                                                                                                                 //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
        else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
        {
            decideConditionsMet402++;
            pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Tax_Amt.setValue(0);                                                                                                                                      //Natural: ASSIGN #TAX-AMT := 0
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Account_Record() throws Exception                                                                                                                    //Natural: ACCOUNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------------
        //*          ( -- ACCOUNT RECORD TO STATE SUMMARY BUCKET -- )
        pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Rpt,pnd_Arr).nadd(1);                                                                                                     //Natural: ADD 1 TO #TRANS-CNT ( #RPT,#ARR )
        pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Rpt,pnd_Arr).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                     //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #RPT,#ARR )
        pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Rpt,pnd_Arr).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                         //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #RPT,#ARR )
        pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Rpt,pnd_Arr).nadd(pnd_Tax_Amt);                                                                                         //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #RPT,#ARR )
        pnd_Counters_Pnd_Int_Amt.getValue(pnd_Rpt,pnd_Arr).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                         //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #RPT,#ARR )
        pnd_Counters_Pnd_State_Wthld.getValue(pnd_Rpt,pnd_Arr).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                 //Natural: ADD #FF3-STATE-WTHLD TO #STATE-WTHLD ( #RPT,#ARR )
        //*   62-(SUBTTL)   63-(TOTAL)   64-(GRNDTTL)
        pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Rpt,pnd_Arr).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                                 //Natural: ADD #FF3-LOCAL-WTHLD TO #LOCAL-WTHLD ( #RPT,#ARR )
        FOR01:                                                                                                                                                            //Natural: FOR #I 62 64
        for (pnd_I.setValue(62); condition(pnd_I.lessOrEqual(64)); pnd_I.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Rpt,pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #TRANS-CNT ( #RPT,#I )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Rpt,pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                   //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #RPT,#I )
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Rpt,pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                       //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #RPT,#I )
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Rpt,pnd_I).nadd(pnd_Tax_Amt);                                                                                       //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #RPT,#I )
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_Rpt,pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                       //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #RPT,#I )
            pnd_Counters_Pnd_State_Wthld.getValue(pnd_Rpt,pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                               //Natural: ADD #FF3-STATE-WTHLD TO #STATE-WTHLD ( #RPT,#I )
            pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Rpt,pnd_I).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                               //Natural: ADD #FF3-LOCAL-WTHLD TO #LOCAL-WTHLD ( #RPT,#I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Search_State_Ind() throws Exception                                                                                                                  //Natural: SEARCH-STATE-IND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Found.setValue(false);                                                                                                                                        //Natural: ASSIGN #FOUND := FALSE
        pnd_State_Ind.reset();                                                                                                                                            //Natural: RESET #STATE-IND
        PND_PND_L2660:                                                                                                                                                    //Natural: FOR #J 1 100
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(100)); pnd_J.nadd(1))
        {
            if (condition(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_J).equals("  ")))                                                                               //Natural: IF #RESIDENCY-CODE ( #J ) = '  '
            {
                if (true) break PND_PND_L2660;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L2660. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Rc_Arr.getValue(pnd_Arr).equals(pnd_State_Table_Pnd_Residency_Code.getValue(pnd_J))))                                                   //Natural: IF #RC-ARR ( #ARR ) = #RESIDENCY-CODE ( #J )
                {
                    pnd_State_Ind.setValue(pnd_State_Table_Pnd_State_Indicator.getValue(pnd_J));                                                                          //Natural: ASSIGN #STATE-IND := #STATE-INDICATOR ( #J )
                    pnd_State_Desc.getValue(pnd_Arr).setValue(pnd_State_Table_Pnd_State_Description.getValue(pnd_J));                                                     //Natural: ASSIGN #STATE-DESC ( #ARR ) := #STATE-DESCRIPTION ( #J )
                    pnd_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #FOUND := TRUE
                    if (true) break PND_PND_L2660;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L2660. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*           ( PRINT RESIDENCY CODE NOT FOUND FROM STATE TABLE)
        if (condition(pnd_Found.equals(false)))                                                                                                                           //Natural: IF #FOUND = FALSE
        {
            pnd_State_Desc.getValue(pnd_Arr).setValue("NO DESCRIPTION");                                                                                                  //Natural: ASSIGN #STATE-DESC ( #ARR ) := 'NO DESCRIPTION'
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals(pnd_Sv_Search_Rc)))                                                               //Natural: IF #FF3-RESIDENCY-CODE = #SV-SEARCH-RC
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(4),ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code(),new ColumnSpacing(8),                //Natural: WRITE ( 2 ) 4X #FF3-COMPANY-CODE 8X #FF3-RESIDENCY-CODE
                    ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());
                if (Global.isEscape()) return;
                pnd_Sv_Search_Rc.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code());                                                                        //Natural: ASSIGN #SV-SEARCH-RC := #FF3-RESIDENCY-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Account_Record_Other_Report() throws Exception                                                                                                       //Natural: ACCOUNT-RECORD-OTHER-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------------------
        //*                 (--  CHECK FOR THE TYPE OF REPORT THE RECORD
        //*                     WILL BE REPORTED BASED ON STATE INDICATOR --)
        //*  --TAXABLE REPORT--
        //*  -- GROSS REPORT --
        //*  -- NON-REPORTABLE --
        short decideConditionsMet467 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STATE-IND = '1' OR = '5' OR ( ( #STATE-IND = '2' OR = '6' ) AND ( #FF3-STATE-WTHLD NE 0 OR #FF3-LOCAL-WTHLD NE 0 ) )
        if (condition(((pnd_State_Ind.equals("1") || pnd_State_Ind.equals("5")) || ((pnd_State_Ind.equals("2") || pnd_State_Ind.equals("6")) && (ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().notEquals(getZero()) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().notEquals(getZero()))))))
        {
            decideConditionsMet467++;
            pnd_Rpt.setValue(2);                                                                                                                                          //Natural: ASSIGN #RPT := 2
        }                                                                                                                                                                 //Natural: WHEN #STATE-IND = '7' OR ( #STATE-IND = '3' AND #FF3-RESIDENCY-CODE NE '41' ) OR ( ( #STATE-IND = '4' OR = '8' OR = '9' ) AND ( #FF3-STATE-WTHLD NE 0 OR #FF3-LOCAL-WTHLD NE 0 ) ) OR ( #STATE-IND = '3' AND #FF3-RESIDENCY-CODE = '41' AND ( #FF3-STATE-WTHLD NE 0 OR #FF3-LOCAL-WTHLD NE 0 OR #FF3-DISTRIBUTION-CDE = '1' ) )
        else if (condition((((pnd_State_Ind.equals("7") || (pnd_State_Ind.equals("3") && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().notEquals("41"))) 
            || (((pnd_State_Ind.equals("4") || pnd_State_Ind.equals("8")) || pnd_State_Ind.equals("9")) && (ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().notEquals(getZero()) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().notEquals(getZero())))) || ((pnd_State_Ind.equals("3") && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("41")) 
            && ((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().notEquals(getZero()) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().notEquals(getZero())) 
            || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("1"))))))
        {
            decideConditionsMet467++;
            pnd_Rpt.setValue(3);                                                                                                                                          //Natural: ASSIGN #RPT := 3
        }                                                                                                                                                                 //Natural: WHEN ( ( #STATE-IND = '2' OR = '4' OR = '6' OR = '8' OR = '9' ) AND #FF3-STATE-WTHLD = 0 AND #FF3-LOCAL-WTHLD = 0 ) OR ( #STATE-IND = '3' AND #FF3-RESIDENCY-CODE = '41' AND #FF3-STATE-WTHLD = 0 AND #FF3-LOCAL-WTHLD = 0 AND #FF3-DISTRIBUTION-CDE NE '1' )
        else if (condition((((((((pnd_State_Ind.equals("2") || pnd_State_Ind.equals("4")) || pnd_State_Ind.equals("6")) || pnd_State_Ind.equals("8")) 
            || pnd_State_Ind.equals("9")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().equals(getZero())) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().equals(getZero())) 
            || ((((pnd_State_Ind.equals("3") && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("41")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().equals(getZero())) 
            && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld().equals(getZero())) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().notEquals("1")))))
        {
            decideConditionsMet467++;
            pnd_Rpt.setValue(4);                                                                                                                                          //Natural: ASSIGN #RPT := 4
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet467 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-RECORD
            sub_Account_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Detail_Per_Sc() throws Exception                                                                                                               //Natural: PRINT-DETAIL-PER-SC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------------------
        //*  TO DEFINE SOURCE TO BE PRINTED
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
        sub_Source_To_Prt();
        if (condition(Global.isEscape())) {return;}
        //*  PRINTS 4 REPORTS
        pnd_Print_Sc.getValue("*").reset();                                                                                                                               //Natural: RESET #PRINT-SC ( * )
        pnd_Print_Sc.getValue("*").setValue(pnd_New_Source);                                                                                                              //Natural: ASSIGN #PRINT-SC ( * ) := #NEW-SOURCE
        FOR02:                                                                                                                                                            //Natural: FOR #L 1 4
        for (pnd_L.setValue(1); condition(pnd_L.lessOrEqual(4)); pnd_L.nadd(1))
        {
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 61
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(61)); pnd_I.nadd(1))
            {
                //*  FOR US-STATES
                if (condition(pnd_I.greaterOrEqual(1) && pnd_I.lessOrEqual(59)))                                                                                          //Natural: IF #I = 1 THRU 59
                {
                    if (condition(pnd_Rc_Arr.getValue(pnd_I).equals(" ")))                                                                                                //Natural: IF #RC-ARR ( #I ) = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        short decideConditionsMet495 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #L;//Natural: VALUE 1
                        if (condition((pnd_L.equals(1))))
                        {
                            decideConditionsMet495++;
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-1
                            sub_Print_Detail_1();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: VALUE 2
                        else if (condition((pnd_L.equals(2))))
                        {
                            decideConditionsMet495++;
                            if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(2,pnd_I).equals(getZero())))                                                                //Natural: IF #TRANS-CNT ( 2,#I ) = 0
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-2
                                sub_Print_Detail_2();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 3
                        else if (condition((pnd_L.equals(3))))
                        {
                            decideConditionsMet495++;
                            if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(3,pnd_I).equals(getZero())))                                                                //Natural: IF #TRANS-CNT ( 3,#I ) = 0
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-3
                                sub_Print_Detail_3();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 4
                        else if (condition((pnd_L.equals(4))))
                        {
                            decideConditionsMet495++;
                            if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(4,pnd_I).equals(getZero())))                                                                //Natural: IF #TRANS-CNT ( 4,#I ) = 0
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-4
                                sub_Print_Detail_4();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                    //* (FOR RT1-NOT -US & RT-NOT-1)
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_L.equals(1)))                                                                                                                       //Natural: IF #L = 1
                    {
                        short decideConditionsMet523 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 60
                        if (condition((pnd_I.equals(60))))
                        {
                            decideConditionsMet523++;
                            pnd_State_Desc.getValue(pnd_I).setValue("RT1-NOT-U.S-STATE");                                                                                 //Natural: ASSIGN #STATE-DESC ( #I ) := 'RT1-NOT-U.S-STATE'
                        }                                                                                                                                                 //Natural: VALUE 61
                        else if (condition((pnd_I.equals(61))))
                        {
                            decideConditionsMet523++;
                            pnd_State_Desc.getValue(pnd_I).setValue("RES.TYPE-NOT-1");                                                                                    //Natural: ASSIGN #STATE-DESC ( #I ) := 'RES.TYPE-NOT-1'
                        }                                                                                                                                                 //Natural: ANY VALUE
                        if (condition(decideConditionsMet523 > 0))
                        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-1
                            sub_Print_Detail_1();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Detail_1() throws Exception                                                                                                                    //Natural: PRINT-DETAIL-1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "SYSTEM/SRCE",                                                                                                                            //Natural: DISPLAY ( 1 ) 'SYSTEM/SRCE' #PRINT-SC ( 1 ) 'STATE/CODE' #STATE-DESC ( #I ) 'TRANS/COUNT' #TRANS-CNT ( 1,#I ) ( EM = ZZZZ,ZZ9 ) 'GROSS/AMOUNT' #GROSS-AMT ( 1,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'TAX FREE/IVC' #IVC-AMT ( 1,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT' #TAXABLE-AMT ( 1,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT' #INT-AMT ( 1,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'STATE/WITHHOLDING' #STATE-WTHLD ( 1,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'LOCAL/WITHHOLDING' #LOCAL-WTHLD ( 1,#I ) ( EM = ZZZZZZ,ZZ9.99- )
        		pnd_Print_Sc.getValue(1),"STATE/CODE",
        		pnd_State_Desc.getValue(pnd_I),"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt.getValue(1,pnd_I), new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld.getValue(1,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        //*  TO CONTROL PRINTING OF SRCE-CDE
        //*  IS=ON  NOT EFFECTIVE IN THIS CASE
        if (condition(pnd_First_Prt.getValue(1).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 1 )
        {
            pnd_First_Prt.getValue(1).setValue(false);                                                                                                                    //Natural: ASSIGN #FIRST-PRT ( 1 ) := FALSE
            pnd_Print_Sc.getValue(1).reset();                                                                                                                             //Natural: RESET #PRINT-SC ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_2() throws Exception                                                                                                                    //Natural: PRINT-DETAIL-2
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(4, "SYSTEM/SRCE",                                                                                                                            //Natural: DISPLAY ( 4 ) 'SYSTEM/SRCE' #PRINT-SC ( 2 ) 'STATE/CODE' #STATE-DESC ( #I ) 'TRANS/COUNT' #TRANS-CNT ( 2,#I ) ( EM = ZZZZ,ZZ9 ) 'GROSS/AMOUNT' #GROSS-AMT ( 2,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'TAX FREE/IVC' #IVC-AMT ( 2,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT' #TAXABLE-AMT ( 2,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT' #INT-AMT ( 2,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'STATE/WITHHOLDING' #STATE-WTHLD ( 2,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'LOCAL/WITHHOLDING' #LOCAL-WTHLD ( 2,#I ) ( EM = ZZZZZZ,ZZ9.99- )
        		pnd_Print_Sc.getValue(2),"STATE/CODE",
        		pnd_State_Desc.getValue(pnd_I),"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt.getValue(2,pnd_I), new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld.getValue(2,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        if (condition(pnd_First_Prt.getValue(2).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 2 )
        {
            pnd_First_Prt.getValue(2).setValue(false);                                                                                                                    //Natural: ASSIGN #FIRST-PRT ( 2 ) := FALSE
            pnd_Print_Sc.getValue(2).reset();                                                                                                                             //Natural: RESET #PRINT-SC ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_3() throws Exception                                                                                                                    //Natural: PRINT-DETAIL-3
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(5, "SYSTEM/SRCE",                                                                                                                            //Natural: DISPLAY ( 5 ) 'SYSTEM/SRCE' #PRINT-SC ( 3 ) 'STATE/CODE' #STATE-DESC ( #I ) 'TRANS/COUNT' #TRANS-CNT ( 3,#I ) ( EM = ZZZZ,ZZ9 ) 'GROSS/AMOUNT' #GROSS-AMT ( 3,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'TAX FREE/IVC' #IVC-AMT ( 3,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT' #TAXABLE-AMT ( 3,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT' #INT-AMT ( 3,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'STATE/WITHHOLDING' #STATE-WTHLD ( 3,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'LOCAL/WITHHOLDING' #LOCAL-WTHLD ( 3,#I ) ( EM = ZZZZZZ,ZZ9.99- )
        		pnd_Print_Sc.getValue(3),"STATE/CODE",
        		pnd_State_Desc.getValue(pnd_I),"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt.getValue(3,pnd_I), new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld.getValue(3,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        if (condition(pnd_First_Prt.getValue(3).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 3 )
        {
            pnd_First_Prt.getValue(3).setValue(false);                                                                                                                    //Natural: ASSIGN #FIRST-PRT ( 3 ) := FALSE
            pnd_Print_Sc.getValue(3).reset();                                                                                                                             //Natural: RESET #PRINT-SC ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_4() throws Exception                                                                                                                    //Natural: PRINT-DETAIL-4
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(6, "SYSTEM/SRCE",                                                                                                                            //Natural: DISPLAY ( 6 ) 'SYSTEM/SRCE' #PRINT-SC ( 4 ) 'STATE/CODE' #STATE-DESC ( #I ) 'TRANS/COUNT' #TRANS-CNT ( 4,#I ) ( EM = ZZZZ,ZZ9 ) 'GROSS/AMOUNT' #GROSS-AMT ( 4,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'TAX FREE/IVC' #IVC-AMT ( 4,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT' #TAXABLE-AMT ( 4,#I ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT' #INT-AMT ( 4,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'STATE/WITHHOLDING' #STATE-WTHLD ( 4,#I ) ( EM = ZZZZZZ,ZZ9.99- ) 'LOCAL/WITHHOLDING' #LOCAL-WTHLD ( 4,#I ) ( EM = ZZZZZZ,ZZ9.99- )
        		pnd_Print_Sc.getValue(4),"STATE/CODE",
        		pnd_State_Desc.getValue(pnd_I),"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt.getValue(4,pnd_I), new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld.getValue(4,pnd_I), new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        if (condition(pnd_First_Prt.getValue(4).getBoolean()))                                                                                                            //Natural: IF #FIRST-PRT ( 4 )
        {
            pnd_First_Prt.getValue(4).setValue(false);                                                                                                                    //Natural: ASSIGN #FIRST-PRT ( 4 ) := FALSE
            pnd_Print_Sc.getValue(4).reset();                                                                                                                             //Natural: RESET #PRINT-SC ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source12.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                                             //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Source34.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                                             //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        short decideConditionsMet572 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Source34.equals("  "))))
        {
            decideConditionsMet572++;
            //*  07/22/03   RM
            //*  12/01/04   RM
            //*  06/06/06   RM
            //*  10/31/06   RM
            //*  09/28/11
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("CP") || pnd_Source12.equals("DC") || pnd_Source12.equals("DS") || pnd_Source12.equals("ED")   //Natural: IF #SOURCE12 = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IS' OR = 'MS' OR = 'NZ' OR = 'RE' OR = 'SS' OR = 'IP' OR = 'SI' OR = 'OP' OR = 'NV' OR = 'VL' OR = 'AM'
                || pnd_Source12.equals("EW") || pnd_Source12.equals("GS") || pnd_Source12.equals("IA") || pnd_Source12.equals("IS") || pnd_Source12.equals("MS") 
                || pnd_Source12.equals("NZ") || pnd_Source12.equals("RE") || pnd_Source12.equals("SS") || pnd_Source12.equals("IP") || pnd_Source12.equals("SI") 
                || pnd_Source12.equals("OP") || pnd_Source12.equals("NV") || pnd_Source12.equals("VL") || pnd_Source12.equals("AM")))
            {
                pnd_Source.setValue(pnd_Source12);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE12
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source12);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'OL'
        else if (condition((pnd_Source34.equals("OL"))))
        {
            decideConditionsMet572++;
            //*  07/22/03   RM
            //*  12/01/04   RM
            //*  06/06/06   RM
            //*  10/31/06   RM
            //*  09/28/11
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("CP") || pnd_Source12.equals("DC") || pnd_Source12.equals("DS") || pnd_Source12.equals("ED")   //Natural: IF #SOURCE12 = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IS' OR = 'MS' OR = 'NZ' OR = 'RE' OR = 'SS' OR = 'ZZ' OR = 'IP' OR = 'SI' OR = 'OP' OR = 'YY' OR = 'NV' OR = 'VL' OR = 'AM'
                || pnd_Source12.equals("EW") || pnd_Source12.equals("GS") || pnd_Source12.equals("IA") || pnd_Source12.equals("IS") || pnd_Source12.equals("MS") 
                || pnd_Source12.equals("NZ") || pnd_Source12.equals("RE") || pnd_Source12.equals("SS") || pnd_Source12.equals("ZZ") || pnd_Source12.equals("IP") 
                || pnd_Source12.equals("SI") || pnd_Source12.equals("OP") || pnd_Source12.equals("YY") || pnd_Source12.equals("NV") || pnd_Source12.equals("VL") 
                || pnd_Source12.equals("AM")))
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  3-3-99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Source34.equals("TM"))))
        {
            decideConditionsMet572++;
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("IA")))                                                                                        //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'ML'
        else if (condition((pnd_Source34.equals("ML"))))
        {
            decideConditionsMet572++;
            if (condition(pnd_Source12.equals("ZZ")))                                                                                                                     //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
                //*   12/01/04        RM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'NL'
        else if (condition((pnd_Source34.equals("NL"))))
        {
            decideConditionsMet572++;
            if (condition(pnd_Source12.equals("YY")))                                                                                                                     //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Source34.equals("AL"))))
        {
            decideConditionsMet572++;
            if (condition(pnd_Source12.equals("AP")))                                                                                                                     //Natural: IF #SOURCE12 = 'AP'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Source34.equals("IL"))))
        {
            decideConditionsMet572++;
            if (condition(pnd_Source12.equals("IA")))                                                                                                                     //Natural: IF #SOURCE12 = 'IA'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet645 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Source.equals("DS"))))
        {
            decideConditionsMet645++;
            pnd_New_Source.setValue("DSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Source.equals("GS"))))
        {
            decideConditionsMet645++;
            pnd_New_Source.setValue("GSRA");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Source.equals("MS"))))
        {
            decideConditionsMet645++;
            pnd_New_Source.setValue("MSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Source.equals("SS"))))
        {
            decideConditionsMet645++;
            pnd_New_Source.setValue("SSSS");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_New_Source.setValue(pnd_Source);                                                                                                                          //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Prt_Source.setValue(pnd_New_Source);                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := #NEW-SOURCE
        short decideConditionsMet659 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'OL'
        if (condition((pnd_Source.equals("OL"))))
        {
            decideConditionsMet659++;
            //*  FOR PRINTING OF TOTAL(ML)
            if (condition(pnd_Source12.equals("ZZ")))                                                                                                                     //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Prt_Source.setValue("MLOL");                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := 'MLOL'
                //*  TO CHECK ORGN/UPDATE SRCE CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                     //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'PL'
        else if (condition((pnd_Source.equals("PL"))))
        {
            decideConditionsMet659++;
            //*  FOR PRINTING OF TOTAL(NL)
            if (condition(pnd_Source12.equals("YY")))                                                                                                                     //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Prt_Source.setValue("NLPL");                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := 'NLPL'
                //*  TO CHECK ORGN/UPDATE SRCE CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                     //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Source.equals("TM"))))
        {
            decideConditionsMet659++;
            pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                         //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Source.equals("AL"))))
        {
            decideConditionsMet659++;
            pnd_Prt_Source.setValue("TMAL");                                                                                                                              //Natural: ASSIGN #PRT-SOURCE := 'TMAL'
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Source.equals("IL"))))
        {
            decideConditionsMet659++;
            pnd_Prt_Source.setValue("TMIL");                                                                                                                              //Natural: ASSIGN #PRT-SOURCE := 'TMIL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_First_Prt.getValue("*").setValue(true);                                                                                                                       //Natural: ASSIGN #FIRST-PRT ( * ) := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-PER-SC
        sub_Print_Detail_Per_Sc();
        if (condition(Global.isEscape())) {return;}
        //*  SUBTOTAL
        pnd_Print_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL   ( ", pnd_Prt_Source, ")"));                                                  //Natural: COMPRESS 'SUBTOTAL   ( ' #PRT-SOURCE ')' TO #PRINT-STATE LEAVING NO SPACE
        pnd_Occ.setValue(62);                                                                                                                                             //Natural: ASSIGN #OCC := 62
        //*  PRINT SUBTOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
        sub_Print_Rtn();
        if (condition(Global.isEscape())) {return;}
        //*  12/01/04  RM
        if (condition(pnd_Source.equals("OL") || pnd_Source.equals("PL")))                                                                                                //Natural: IF #SOURCE = 'OL' OR = 'PL'
        {
            pnd_Prt_Tot_Ol.setValue(1);                                                                                                                                   //Natural: ASSIGN #PRT-TOT-OL := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Prt.setValue(false);                                                                                                                                  //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*                      /* TO PRINT TOTAL AFTER SUBTOTAL OF 'OL' OR 'PL'
        if (condition(pnd_Prt_Tot_Ol.equals(1)))                                                                                                                          //Natural: IF #PRT-TOT-OL = 1
        {
            //*                                        (3-3-99)
            //*  EX. 'APOL'  & 'APTM'
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                   //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                                   //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                pnd_Print_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Sub_Src, ")"));                                             //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRINT-STATE LEAVING NO SPACE
                pnd_Occ.setValue(63);                                                                                                                                     //Natural: ASSIGN #OCC := 63
                //*  PRINT TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
                sub_Print_Rtn();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape())) {return;}
                pnd_Tot_Prt.setValue(true);                                                                                                                               //Natural: ASSIGN #TOT-PRT := TRUE
                pnd_Prt_Tot_Ol.setValue(0);                                                                                                                               //Natural: ASSIGN #PRT-TOT-OL := 0
                //*                                           3-3-99
                //*  TOTAL FOR AP/IA IS PRINTED
                //*  HERE , IF THERE ARE 'OL'
                if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("IA")))                                                                                    //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
                {
                    pnd_Total_For_Ap_Ia.setValue(true);                                                                                                                   //Natural: ASSIGN #TOTAL-FOR-AP-IA := TRUE
                    //*   & 'TM'  PROCESSED
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*      3-3-99 : TO PRINT TOTAL FOR 'AP' OR 'AI' IF THERE IS NO
        //*              'APOL' OR 'IAOL read & there'S 'APTM' OR 'IATM'
        if (condition(pnd_Total_For_Ap_Ia.equals(true)))                                                                                                                  //Natural: IF #TOTAL-FOR-AP-IA = TRUE
        {
            pnd_Total_For_Ap_Ia.setValue(false);                                                                                                                          //Natural: ASSIGN #TOTAL-FOR-AP-IA := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Source.equals("TM")))                                                                                                                       //Natural: IF #SOURCE = 'TM'
            {
                pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                                   //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                pnd_Print_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Sub_Src, ")"));                                             //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRINT-STATE LEAVING NO SPACE
                pnd_Occ.setValue(63);                                                                                                                                     //Natural: ASSIGN #OCC := 63
                //*  PRINT TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
                sub_Print_Rtn();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape())) {return;}
                pnd_Tot_Prt.setValue(true);                                                                                                                               //Natural: ASSIGN #TOT-PRT := TRUE
                pnd_Prt_Tot_Ol.setValue(0);                                                                                                                               //Natural: ASSIGN #PRT-TOT-OL := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  --
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                       //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
            //*                      (SAVE CNT/AMT OF OLD SOURCE-CODE FOR PRINTING)
            //*  TOTAL IS PRINTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                      //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Occ.setValue(65);                                                                                                                                     //Natural: ASSIGN #OCC := 65
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
                //*  TO RESET TOTAL CTR FOR NEW SRCE-CDE
                pnd_Counters.getValue("*",65).reset();                                                                                                                    //Natural: RESET #COUNTERS ( *,65 )
                pnd_Occ.setValue(63);                                                                                                                                     //Natural: ASSIGN #OCC := 63
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        //*  TO PRINT TOTAL OF LAST BATCH SOURCE-CDE
        if (condition(pnd_Eof.getBoolean()))                                                                                                                              //Natural: IF #EOF
        {
            //*  TOTAL IS PRINTED
            if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                      //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Occ.setValue(65);                                                                                                                                     //Natural: ASSIGN #OCC := 65
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  PRINT GRANDTOTAL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Occ.setValue(64);                                                                                                                                             //Natural: ASSIGN #OCC := 64
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
        pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                                      //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
        pnd_Savers_Pnd_Sv_Res_Code.reset();                                                                                                                               //Natural: RESET #SV-RES-CODE
    }
    private void sub_Move_Value_5() throws Exception                                                                                                                      //Natural: MOVE-VALUE-5
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue("*",65).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue("*",63));                                                                //Natural: ASSIGN #TRANS-CNT ( *,65 ) := #TRANS-CNT ( *,63 )
        pnd_Counters_Pnd_Gross_Amt.getValue("*",65).setValue(pnd_Counters_Pnd_Gross_Amt.getValue("*",63));                                                                //Natural: ASSIGN #GROSS-AMT ( *,65 ) := #GROSS-AMT ( *,63 )
        pnd_Counters_Pnd_Ivc_Amt.getValue("*",65).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue("*",63));                                                                    //Natural: ASSIGN #IVC-AMT ( *,65 ) := #IVC-AMT ( *,63 )
        pnd_Counters_Pnd_Taxable_Amt.getValue("*",65).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue("*",63));                                                            //Natural: ASSIGN #TAXABLE-AMT ( *,65 ) := #TAXABLE-AMT ( *,63 )
        pnd_Counters_Pnd_Int_Amt.getValue("*",65).setValue(pnd_Counters_Pnd_Int_Amt.getValue("*",63));                                                                    //Natural: ASSIGN #INT-AMT ( *,65 ) := #INT-AMT ( *,63 )
        pnd_Counters_Pnd_State_Wthld.getValue("*",65).setValue(pnd_Counters_Pnd_State_Wthld.getValue("*",63));                                                            //Natural: ASSIGN #STATE-WTHLD ( *,65 ) := #STATE-WTHLD ( *,63 )
        pnd_Counters_Pnd_Local_Wthld.getValue("*",65).setValue(pnd_Counters_Pnd_Local_Wthld.getValue("*",63));                                                            //Natural: ASSIGN #LOCAL-WTHLD ( *,65 ) := #LOCAL-WTHLD ( *,63 )
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------
        //*    12/01/04    RM
        short decideConditionsMet798 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("TRST");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet798++;
            pnd_Print_Com.setValue("FSB");                                                                                                                                //Natural: ASSIGN #PRINT-COM := 'FSB'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Print_Com.reset();                                                                                                                                        //Natural: RESET #PRINT-COM
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet816 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-TAX-CTZ;//Natural: VALUE 'F'
        if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F"))))
        {
            decideConditionsMet816++;
            pnd_Print_Tc.setValue("F - FOREIGN");                                                                                                                         //Natural: ASSIGN #PRINT-TC := 'F - FOREIGN'
        }                                                                                                                                                                 //Natural: VALUE 'U'
        else if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U"))))
        {
            decideConditionsMet816++;
            pnd_Print_Tc.setValue("U - US CTZN/RES.ALIEN");                                                                                                               //Natural: ASSIGN #PRINT-TC := 'U - US CTZN/RES.ALIEN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Print_Tc.reset();                                                                                                                                         //Natural: RESET #PRINT-TC
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Occ)); pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue("*",pnd_J).reset();                                                                                                       //Natural: RESET #TRANS-CNT ( *,#J ) #GROSS-AMT ( *,#J ) #IVC-AMT ( *,#J ) #TAXABLE-AMT ( *,#J ) #INT-AMT ( *,#J ) #STATE-WTHLD ( *,#J ) #LOCAL-WTHLD ( *,#J )
            pnd_Counters_Pnd_Gross_Amt.getValue("*",pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue("*",pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue("*",pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue("*",pnd_J).reset();
            pnd_Counters_Pnd_State_Wthld.getValue("*",pnd_J).reset();
            pnd_Counters_Pnd_Local_Wthld.getValue("*",pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  12/14/99
        pnd_Rc_Arr.getValue("*").reset();                                                                                                                                 //Natural: RESET #RC-ARR ( * )
        pnd_Reset_Rc_Arr.setValue(true);                                                                                                                                  //Natural: ASSIGN #RESET-RC-ARR := TRUE
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        short decideConditionsMet838 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 64
        if (condition((pnd_Occ.equals(64))))
        {
            decideConditionsMet838++;
            pnd_Print_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTTL ( ", pnd_Print_Com, "-", pnd_Savers_Pnd_Sv_Tax_Ctz, ")"));                 //Natural: COMPRESS 'GRANDTTL ( ' #PRINT-COM '-' #SV-TAX-CTZ ')' TO #PRINT-STATE LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: VALUE 65
        else if (condition((pnd_Occ.equals(65))))
        {
            decideConditionsMet838++;
            short decideConditionsMet850 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PRT-SOURCE;//Natural: VALUE 'TMAL'
            if (condition((pnd_Prt_Source.equals("TMAL"))))
            {
                decideConditionsMet850++;
                pnd_Sub_Src.setValue("AP");                                                                                                                               //Natural: ASSIGN #SUB-SRC := 'AP'
            }                                                                                                                                                             //Natural: VALUE 'TMIL'
            else if (condition((pnd_Prt_Source.equals("TMIL"))))
            {
                decideConditionsMet850++;
                pnd_Sub_Src.setValue("IA");                                                                                                                               //Natural: ASSIGN #SUB-SRC := 'IA'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                                   //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Print_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Sub_Src, ")"));                                                 //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRINT-STATE LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Error_Source_Prt() throws Exception                                                                                                                  //Natural: ERROR-SOURCE-PRT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Error_Source.getBoolean()))                                                                                                                     //Natural: IF #ERROR-SOURCE
        {
            getReports().display(3, "COMPANY",                                                                                                                            //Natural: DISPLAY ( 3 ) 'COMPANY' #SV-COMPANY 'SOURCE/CODE' #SV-SRCE-CODE 'TAX/CTZ' #SV-TAX-CTZ
            		pnd_Savers_Pnd_Sv_Company,"SOURCE/CODE",
            		pnd_Savers_Pnd_Sv_Srce_Code,"TAX/CTZ",
            		pnd_Savers_Pnd_Sv_Tax_Ctz);
            if (Global.isEscape()) return;
            pnd_Error_Source.setValue(false);                                                                                                                             //Natural: ASSIGN #ERROR-SOURCE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  RTN TO PRINT TOTALS
    private void sub_Print_Rtn() throws Exception                                                                                                                         //Natural: PRINT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(1,pnd_Occ).equals(getZero())))                                                                                  //Natural: IF #TRANS-CNT ( 1,#OCC ) = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SUBTOTAL
            if (condition(pnd_Occ.equals(62)))                                                                                                                            //Natural: IF #OCC = 62
            {
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,new ReportTAsterisk(pnd_State_Desc),pnd_Print_State,new ReportTAsterisk(pnd_Counters_Pnd_Trans_Cnt.getValue(1,1)),pnd_Counters_Pnd_Trans_Cnt.getValue(1,pnd_Occ),  //Natural: WRITE ( 1 ) T*#STATE-DESC #PRINT-STATE T*#TRANS-CNT ( 1,1 ) #TRANS-CNT ( 1,#OCC ) ( EM = ZZZZ,ZZ9 ) T*#GROSS-AMT ( 1,1 ) #GROSS-AMT ( 1,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#IVC-AMT ( 1,1 ) #IVC-AMT ( 1,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#TAXABLE-AMT ( 1,1 ) #TAXABLE-AMT ( 1,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#INT-AMT ( 1,1 ) #INT-AMT ( 1,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#STATE-WTHLD ( 1,1 ) #STATE-WTHLD ( 1,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#LOCAL-WTHLD ( 1,1 ) #LOCAL-WTHLD ( 1,#OCC ) ( EM = ZZZZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZZ,ZZ9"),new ReportTAsterisk(pnd_Counters_Pnd_Gross_Amt.getValue(1,1)),pnd_Counters_Pnd_Gross_Amt.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Ivc_Amt.getValue(1,1)),pnd_Counters_Pnd_Ivc_Amt.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Taxable_Amt.getValue(1,1)),pnd_Counters_Pnd_Taxable_Amt.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Int_Amt.getValue(1,1)),pnd_Counters_Pnd_Int_Amt.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_State_Wthld.getValue(1,1)),pnd_Counters_Pnd_State_Wthld.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Local_Wthld.getValue(1,1)),pnd_Counters_Pnd_Local_Wthld.getValue(1,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(2,pnd_Occ).equals(getZero())))                                                                                  //Natural: IF #TRANS-CNT ( 2,#OCC ) = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SUBTOTAL
            if (condition(pnd_Occ.equals(62)))                                                                                                                            //Natural: IF #OCC = 62
            {
                getReports().skip(4, 1);                                                                                                                                  //Natural: SKIP ( 4 ) 1
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(4, ReportOption.NOTITLE,new ReportTAsterisk(pnd_State_Desc),pnd_Print_State,new ReportTAsterisk(pnd_Counters_Pnd_Trans_Cnt.getValue(1,1)),pnd_Counters_Pnd_Trans_Cnt.getValue(2,pnd_Occ),  //Natural: WRITE ( 4 ) T*#STATE-DESC #PRINT-STATE T*#TRANS-CNT ( 1,1 ) #TRANS-CNT ( 2,#OCC ) ( EM = ZZZZ,ZZ9 ) T*#GROSS-AMT ( 1,1 ) #GROSS-AMT ( 2,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#IVC-AMT ( 1,1 ) #IVC-AMT ( 2,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#TAXABLE-AMT ( 1,1 ) #TAXABLE-AMT ( 2,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#INT-AMT ( 1,1 ) #INT-AMT ( 2,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#STATE-WTHLD ( 1,1 ) #STATE-WTHLD ( 2,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#LOCAL-WTHLD ( 1,1 ) #LOCAL-WTHLD ( 2,#OCC ) ( EM = ZZZZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZZ,ZZ9"),new ReportTAsterisk(pnd_Counters_Pnd_Gross_Amt.getValue(1,1)),pnd_Counters_Pnd_Gross_Amt.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Ivc_Amt.getValue(1,1)),pnd_Counters_Pnd_Ivc_Amt.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Taxable_Amt.getValue(1,1)),pnd_Counters_Pnd_Taxable_Amt.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Int_Amt.getValue(1,1)),pnd_Counters_Pnd_Int_Amt.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_State_Wthld.getValue(1,1)),pnd_Counters_Pnd_State_Wthld.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Local_Wthld.getValue(1,1)),pnd_Counters_Pnd_Local_Wthld.getValue(2,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            getReports().skip(4, 1);                                                                                                                                      //Natural: SKIP ( 4 ) 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(3,pnd_Occ).equals(getZero())))                                                                                  //Natural: IF #TRANS-CNT ( 3,#OCC ) = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SUBTOTAL
            if (condition(pnd_Occ.equals(62)))                                                                                                                            //Natural: IF #OCC = 62
            {
                getReports().skip(5, 1);                                                                                                                                  //Natural: SKIP ( 5 ) 1
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(5, ReportOption.NOTITLE,new ReportTAsterisk(pnd_State_Desc),pnd_Print_State,new ReportTAsterisk(pnd_Counters_Pnd_Trans_Cnt.getValue(1,1)),pnd_Counters_Pnd_Trans_Cnt.getValue(3,pnd_Occ),  //Natural: WRITE ( 5 ) T*#STATE-DESC #PRINT-STATE T*#TRANS-CNT ( 1,1 ) #TRANS-CNT ( 3,#OCC ) ( EM = ZZZZ,ZZ9 ) T*#GROSS-AMT ( 1,1 ) #GROSS-AMT ( 3,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#IVC-AMT ( 1,1 ) #IVC-AMT ( 3,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#TAXABLE-AMT ( 1,1 ) #TAXABLE-AMT ( 3,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#INT-AMT ( 1,1 ) #INT-AMT ( 3,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#STATE-WTHLD ( 1,1 ) #STATE-WTHLD ( 3,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#LOCAL-WTHLD ( 1,1 ) #LOCAL-WTHLD ( 3,#OCC ) ( EM = ZZZZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZZ,ZZ9"),new ReportTAsterisk(pnd_Counters_Pnd_Gross_Amt.getValue(1,1)),pnd_Counters_Pnd_Gross_Amt.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Ivc_Amt.getValue(1,1)),pnd_Counters_Pnd_Ivc_Amt.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Taxable_Amt.getValue(1,1)),pnd_Counters_Pnd_Taxable_Amt.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Int_Amt.getValue(1,1)),pnd_Counters_Pnd_Int_Amt.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_State_Wthld.getValue(1,1)),pnd_Counters_Pnd_State_Wthld.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Local_Wthld.getValue(1,1)),pnd_Counters_Pnd_Local_Wthld.getValue(3,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Counters_Pnd_Trans_Cnt.getValue(4,pnd_Occ).equals(getZero())))                                                                                  //Natural: IF #TRANS-CNT ( 4,#OCC ) = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SUBTOTAL
            if (condition(pnd_Occ.equals(62)))                                                                                                                            //Natural: IF #OCC = 62
            {
                getReports().skip(6, 1);                                                                                                                                  //Natural: SKIP ( 6 ) 1
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(6, ReportOption.NOTITLE,new ReportTAsterisk(pnd_State_Desc),pnd_Print_State,new ReportTAsterisk(pnd_Counters_Pnd_Trans_Cnt.getValue(1,1)),pnd_Counters_Pnd_Trans_Cnt.getValue(4,pnd_Occ),  //Natural: WRITE ( 6 ) T*#STATE-DESC #PRINT-STATE T*#TRANS-CNT ( 1,1 ) #TRANS-CNT ( 4,#OCC ) ( EM = ZZZZ,ZZ9 ) T*#GROSS-AMT ( 1,1 ) #GROSS-AMT ( 4,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#IVC-AMT ( 1,1 ) #IVC-AMT ( 4,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#TAXABLE-AMT ( 1,1 ) #TAXABLE-AMT ( 4,#OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) T*#INT-AMT ( 1,1 ) #INT-AMT ( 4,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#STATE-WTHLD ( 1,1 ) #STATE-WTHLD ( 4,#OCC ) ( EM = ZZZZZZ,ZZ9.99- ) T*#LOCAL-WTHLD ( 1,1 ) #LOCAL-WTHLD ( 4,#OCC ) ( EM = ZZZZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZZ,ZZ9"),new ReportTAsterisk(pnd_Counters_Pnd_Gross_Amt.getValue(1,1)),pnd_Counters_Pnd_Gross_Amt.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Ivc_Amt.getValue(1,1)),pnd_Counters_Pnd_Ivc_Amt.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Taxable_Amt.getValue(1,1)),pnd_Counters_Pnd_Taxable_Amt.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Int_Amt.getValue(1,1)),pnd_Counters_Pnd_Int_Amt.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_State_Wthld.getValue(1,1)),pnd_Counters_Pnd_State_Wthld.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Local_Wthld.getValue(1,1)),pnd_Counters_Pnd_Local_Wthld.getValue(4,pnd_Occ), 
                new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 45T 'STATE TRANSACTIONS BY SOURCE CODE' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(45),"STATE TRANSACTIONS BY SOURCE CODE",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Print_Tc,
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 45T 'STATE TRANSACTIONS BY SOURCE CODE' / 'RUNTIME : ' *TIMX 41T 'EXCEPTION REPORT - STATE CODES NOT IN TABLE' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 54T 'TAX YEAR ' #TAX-YEAR // 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO // 1X 'COMPANY' 3X 'STATE' / 3X 'CODE' 5X 'CODE'
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(45),"STATE TRANSACTIONS BY SOURCE CODE",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(41),"EXCEPTION REPORT - STATE CODES NOT IN TABLE",new 
                        TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,new TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,NEWLINE,NEWLINE,new 
                        TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,new ColumnSpacing(1),"COMPANY",new 
                        ColumnSpacing(3),"STATE",NEWLINE,new ColumnSpacing(3),"CODE",new ColumnSpacing(5),"CODE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 3 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 3 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 45T 'STATE TRANSACTIONS BY SOURCE CODE' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 41T 'EXCEPTION REPORT - ERRONEOUS SOURCE CODES' 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(3),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(45),"STATE TRANSACTIONS BY SOURCE CODE",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(41),"EXCEPTION REPORT - ERRONEOUS SOURCE CODES",new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 4 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 4 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 44T 'TAXABLE REPORTABLE STATE TRANSACTIONS' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(4),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(44),"TAXABLE REPORTABLE STATE TRANSACTIONS",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Print_Tc,
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 5 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 5 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 44T 'GROSS REPORTABLE STATE TRANSACTIONS' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(5),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(44),"GROSS REPORTABLE STATE TRANSACTIONS",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Print_Tc,
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 6 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 6 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 46T 'NON-REPORTABLE STATE TRANSACTIONS' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(6),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(46),"NON-REPORTABLE STATE TRANSACTIONS",new TabSetting(95),"INTERFACE DATE",pnd_Var_File1_Pnd_Intf_Date_Fr,"THRU",pnd_Var_File1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Var_File1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Var_File1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Print_Tc,
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");
        Global.format(4, "PS=60 LS=132");
        Global.format(5, "PS=60 LS=132");
        Global.format(6, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "SYSTEM/SRCE",
        		pnd_Print_Sc,"STATE/CODE",
        		pnd_State_Desc,"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(4, "SYSTEM/SRCE",
        		pnd_Print_Sc,"STATE/CODE",
        		pnd_State_Desc,"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(5, "SYSTEM/SRCE",
        		pnd_Print_Sc,"STATE/CODE",
        		pnd_State_Desc,"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(6, "SYSTEM/SRCE",
        		pnd_Print_Sc,"STATE/CODE",
        		pnd_State_Desc,"TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"TAXABLE/AMOUNT",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"STATE/WITHHOLDING",
        		pnd_Counters_Pnd_State_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),"LOCAL/WITHHOLDING",
        		pnd_Counters_Pnd_Local_Wthld, new ReportEditMask ("ZZZZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(3, "COMPANY",
        		pnd_Savers_Pnd_Sv_Company,"SOURCE/CODE",
        		pnd_Savers_Pnd_Sv_Srce_Code,"TAX/CTZ",
        		pnd_Savers_Pnd_Sv_Tax_Ctz);
    }
}
