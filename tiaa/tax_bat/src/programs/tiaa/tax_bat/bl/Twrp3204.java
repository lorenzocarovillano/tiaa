/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:51 PM
**        * FROM NATURAL PROGRAM : Twrp3204
************************************************************
**        * FILE NAME            : Twrp3204.java
**        * CLASS NAME           : Twrp3204
**        * INSTANCE NAME        : Twrp3204
************************************************************
***********************************************************************
* PROGRAM     : TWRP3204 - ILLINOIS ST TAX WITHHOLDING QTRLY REPORTING
*
* FUNCTION    : MERGE TWO INPUT WORK FILES TO PRODUCE AN OUTPUT WORK
*               FILE WHICH WILL LATER ON BE USED TO CALCULATE
*               #QTR-ST-TAX(#REPORTING-PERIOD) AND #ADJ-ST-TAX(*)
*
* INPUTS FILES: WF1 -CONTAINS FINAL RESULT OF #QTRLY-REC FROM PRIOR QTR
*               WF2 -CONTAINS REPORTING QTR'S #QTRLY-REC FROM PRIOR PGM
*
* OUTPUT FILE : FOR 1ST QUARTER REPORTING,
*                 WF3 -SHOULD CONTAINS ALL WF2 RECORDS;
*               FOR REPORTING QUARTERS OTHER THAN 1ST QUARTER,
*                 WF3 -CONTAINS #QTRLY-REC RESULTING FROM:
*                  1. MERGING WF1 & WF2 RECS ON IDENTICAL LONG-KEY VALUE
*                  2. WF1 RECORDS WITH MATCHING WF2 KEY VALUE
*                  3. ALL WF2 RECORDS OTHER THAN WF2 RECS IN 1.
*
* DATE        : 06/25/2020 CREATED BY ARIVU
*
* HISTORY     :
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3204 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Qtrly_Rec;

    private DbsGroup pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts;
    private DbsField pnd_Qtrly_Rec_Pnd_Tiaa_Cref;
    private DbsField pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Qtrly_Rec_Form_Srce_Cde;
    private DbsField pnd_Qtrly_Rec_Ss_Employee_Name;

    private DbsGroup pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts;
    private DbsField pnd_Qtrly_Rec_Pnd_Qtr_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Ytd_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Adj_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Diff_St_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Qtr_Gross_Amt;
    private DbsField pnd_Qtrly_Rec_Pnd_Ytd_Gross_Amt;
    private DbsField pnd_Qtrly_Rec_Pnd_Adj_Gross_Amt;
    private DbsField pnd_Qtrly_Rec_Pnd_Diff_Gross_Amt;
    private DbsField pnd_Qtrly_Rec_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Adj_Lc_Tax;
    private DbsField pnd_Qtrly_Rec_Pnd_Diff_Lc_Tax;

    private DbsGroup pnd_Wf1_Rec;
    private DbsField pnd_Wf1_Rec_Pnd_Wf1_Long_Key;

    private DbsGroup pnd_Wf1_Rec__R_Field_1;
    private DbsField pnd_Wf1_Rec_Pnd_Wf1_Key;

    private DbsGroup pnd_Wf1_Rec__R_Field_2;
    private DbsField pnd_Wf1_Rec_Pnd_Tiaa_Cref;
    private DbsField pnd_Wf1_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Wf1_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Wf1_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Wf1_Rec_Form_Srce_Cde;
    private DbsField pnd_Wf1_Rec_Ss_Employee_Name;
    private DbsField pnd_Wf1_Rec_Pnd_Qtr_St_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Ytd_St_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Adj_St_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Diff_St_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Qtr_Gross_Amt;
    private DbsField pnd_Wf1_Rec_Pnd_Ytd_Gross_Amt;
    private DbsField pnd_Wf1_Rec_Pnd_Adj_Gross_Amt;
    private DbsField pnd_Wf1_Rec_Pnd_Diff_Gross_Amt;
    private DbsField pnd_Wf1_Rec_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Adj_Lc_Tax;
    private DbsField pnd_Wf1_Rec_Pnd_Diff_Lc_Tax;

    private DbsGroup pnd_Wf2_Rec;
    private DbsField pnd_Wf2_Rec_Pnd_Wf2_Long_Key;

    private DbsGroup pnd_Wf2_Rec__R_Field_3;
    private DbsField pnd_Wf2_Rec_Pnd_Wf2_Key;

    private DbsGroup pnd_Wf2_Rec__R_Field_4;
    private DbsField pnd_Wf2_Rec_Pnd_Tiaa_Cref;
    private DbsField pnd_Wf2_Rec_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Wf2_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Wf2_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Wf2_Rec_Form_Srce_Cde;
    private DbsField pnd_Wf2_Rec_Ss_Employee_Name;
    private DbsField pnd_Wf2_Rec_Pnd_Qtr_St_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Ytd_St_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Adj_St_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Diff_St_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Qtr_Gross_Amt;
    private DbsField pnd_Wf2_Rec_Pnd_Ytd_Gross_Amt;
    private DbsField pnd_Wf2_Rec_Pnd_Adj_Gross_Amt;
    private DbsField pnd_Wf2_Rec_Pnd_Diff_Gross_Amt;
    private DbsField pnd_Wf2_Rec_Pnd_Qtr_Lc_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Ytd_Lc_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Adj_Lc_Tax;
    private DbsField pnd_Wf2_Rec_Pnd_Diff_Lc_Tax;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Cnt_Output_Rec_From_Wf1_Wf2;
    private DbsField pnd_Cnt_Output_Rec_From_Wf1;
    private DbsField pnd_Cnt_Output_Rec_From_Wf2;
    private DbsField pnd_Cnt_Output_Rec_Tot;
    private DbsField pnd_Cnt_Wf1_Input;
    private DbsField pnd_Cnt_Wf2_Input;
    private DbsField pnd_I;
    private DbsField pnd_Reporting_Period;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_5;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_6;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Yy;
    private DbsField pnd_Run_Type;
    private DbsField pnd_First_Run;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Qtrly_Rec = localVariables.newGroupInRecord("pnd_Qtrly_Rec", "#QTRLY-REC");

        pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts = pnd_Qtrly_Rec.newGroupInGroup("pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts", "#QTRLY-REC-NON-AMTS");
        pnd_Qtrly_Rec_Pnd_Tiaa_Cref = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 
            1);
        pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Qtrly_Rec_Form_Srce_Cde = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 
            2);
        pnd_Qtrly_Rec_Ss_Employee_Name = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.newFieldInGroup("pnd_Qtrly_Rec_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 
            27);

        pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts = pnd_Qtrly_Rec.newGroupInGroup("pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts", "#QTRLY-REC-AMTS");
        pnd_Qtrly_Rec_Pnd_Qtr_St_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Ytd_St_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Adj_St_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Diff_St_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Qtr_Gross_Amt = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Qtr_Gross_Amt", "#QTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Ytd_Gross_Amt = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Ytd_Gross_Amt", "#YTD-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Adj_Gross_Amt = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Adj_Gross_Amt", "#ADJ-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Diff_Gross_Amt = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Diff_Gross_Amt", "#DIFF-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Qtr_Lc_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Ytd_Lc_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));
        pnd_Qtrly_Rec_Pnd_Adj_Lc_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 4));
        pnd_Qtrly_Rec_Pnd_Diff_Lc_Tax = pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Amts.newFieldArrayInGroup("pnd_Qtrly_Rec_Pnd_Diff_Lc_Tax", "#DIFF-LC-TAX", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 5));

        pnd_Wf1_Rec = localVariables.newGroupInRecord("pnd_Wf1_Rec", "#WF1-REC");
        pnd_Wf1_Rec_Pnd_Wf1_Long_Key = pnd_Wf1_Rec.newFieldInGroup("pnd_Wf1_Rec_Pnd_Wf1_Long_Key", "#WF1-LONG-KEY", FieldType.STRING, 20);

        pnd_Wf1_Rec__R_Field_1 = pnd_Wf1_Rec.newGroupInGroup("pnd_Wf1_Rec__R_Field_1", "REDEFINE", pnd_Wf1_Rec_Pnd_Wf1_Long_Key);
        pnd_Wf1_Rec_Pnd_Wf1_Key = pnd_Wf1_Rec__R_Field_1.newFieldInGroup("pnd_Wf1_Rec_Pnd_Wf1_Key", "#WF1-KEY", FieldType.STRING, 10);

        pnd_Wf1_Rec__R_Field_2 = pnd_Wf1_Rec__R_Field_1.newGroupInGroup("pnd_Wf1_Rec__R_Field_2", "REDEFINE", pnd_Wf1_Rec_Pnd_Wf1_Key);
        pnd_Wf1_Rec_Pnd_Tiaa_Cref = pnd_Wf1_Rec__R_Field_2.newFieldInGroup("pnd_Wf1_Rec_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 1);
        pnd_Wf1_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Wf1_Rec__R_Field_2.newFieldInGroup("pnd_Wf1_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Wf1_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Wf1_Rec__R_Field_1.newFieldInGroup("pnd_Wf1_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Wf1_Rec_Pnd_Cntrct_Payee_Cde = pnd_Wf1_Rec__R_Field_1.newFieldInGroup("pnd_Wf1_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Wf1_Rec_Form_Srce_Cde = pnd_Wf1_Rec.newFieldInGroup("pnd_Wf1_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Wf1_Rec_Ss_Employee_Name = pnd_Wf1_Rec.newFieldInGroup("pnd_Wf1_Rec_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 27);
        pnd_Wf1_Rec_Pnd_Qtr_St_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Ytd_St_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf1_Rec_Pnd_Adj_St_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Diff_St_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf1_Rec_Pnd_Qtr_Gross_Amt = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Qtr_Gross_Amt", "#QTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Ytd_Gross_Amt = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Ytd_Gross_Amt", "#YTD-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 5));
        pnd_Wf1_Rec_Pnd_Adj_Gross_Amt = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Adj_Gross_Amt", "#ADJ-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Diff_Gross_Amt = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Diff_Gross_Amt", "#DIFF-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Wf1_Rec_Pnd_Qtr_Lc_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Ytd_Lc_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf1_Rec_Pnd_Adj_Lc_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf1_Rec_Pnd_Diff_Lc_Tax = pnd_Wf1_Rec.newFieldArrayInGroup("pnd_Wf1_Rec_Pnd_Diff_Lc_Tax", "#DIFF-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));

        pnd_Wf2_Rec = localVariables.newGroupInRecord("pnd_Wf2_Rec", "#WF2-REC");
        pnd_Wf2_Rec_Pnd_Wf2_Long_Key = pnd_Wf2_Rec.newFieldInGroup("pnd_Wf2_Rec_Pnd_Wf2_Long_Key", "#WF2-LONG-KEY", FieldType.STRING, 20);

        pnd_Wf2_Rec__R_Field_3 = pnd_Wf2_Rec.newGroupInGroup("pnd_Wf2_Rec__R_Field_3", "REDEFINE", pnd_Wf2_Rec_Pnd_Wf2_Long_Key);
        pnd_Wf2_Rec_Pnd_Wf2_Key = pnd_Wf2_Rec__R_Field_3.newFieldInGroup("pnd_Wf2_Rec_Pnd_Wf2_Key", "#WF2-KEY", FieldType.STRING, 10);

        pnd_Wf2_Rec__R_Field_4 = pnd_Wf2_Rec__R_Field_3.newGroupInGroup("pnd_Wf2_Rec__R_Field_4", "REDEFINE", pnd_Wf2_Rec_Pnd_Wf2_Key);
        pnd_Wf2_Rec_Pnd_Tiaa_Cref = pnd_Wf2_Rec__R_Field_4.newFieldInGroup("pnd_Wf2_Rec_Pnd_Tiaa_Cref", "#TIAA-CREF", FieldType.STRING, 1);
        pnd_Wf2_Rec_Pnd_Annt_Soc_Sec_Nbr = pnd_Wf2_Rec__R_Field_4.newFieldInGroup("pnd_Wf2_Rec_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Wf2_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Wf2_Rec__R_Field_3.newFieldInGroup("pnd_Wf2_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Wf2_Rec_Pnd_Cntrct_Payee_Cde = pnd_Wf2_Rec__R_Field_3.newFieldInGroup("pnd_Wf2_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Wf2_Rec_Form_Srce_Cde = pnd_Wf2_Rec.newFieldInGroup("pnd_Wf2_Rec_Form_Srce_Cde", "FORM-SRCE-CDE", FieldType.STRING, 2);
        pnd_Wf2_Rec_Ss_Employee_Name = pnd_Wf2_Rec.newFieldInGroup("pnd_Wf2_Rec_Ss_Employee_Name", "SS-EMPLOYEE-NAME", FieldType.STRING, 27);
        pnd_Wf2_Rec_Pnd_Qtr_St_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Qtr_St_Tax", "#QTR-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Ytd_St_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Ytd_St_Tax", "#YTD-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf2_Rec_Pnd_Adj_St_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Adj_St_Tax", "#ADJ-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Diff_St_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Diff_St_Tax", "#DIFF-ST-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf2_Rec_Pnd_Qtr_Gross_Amt = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Qtr_Gross_Amt", "#QTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Ytd_Gross_Amt = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Ytd_Gross_Amt", "#YTD-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 5));
        pnd_Wf2_Rec_Pnd_Adj_Gross_Amt = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Adj_Gross_Amt", "#ADJ-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Diff_Gross_Amt = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Diff_Gross_Amt", "#DIFF-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Wf2_Rec_Pnd_Qtr_Lc_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Qtr_Lc_Tax", "#QTR-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Ytd_Lc_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Ytd_Lc_Tax", "#YTD-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));
        pnd_Wf2_Rec_Pnd_Adj_Lc_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Adj_Lc_Tax", "#ADJ-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 4));
        pnd_Wf2_Rec_Pnd_Diff_Lc_Tax = pnd_Wf2_Rec.newFieldArrayInGroup("pnd_Wf2_Rec_Pnd_Diff_Lc_Tax", "#DIFF-LC-TAX", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1, 5));

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Cnt_Output_Rec_From_Wf1_Wf2 = localVariables.newFieldInRecord("pnd_Cnt_Output_Rec_From_Wf1_Wf2", "#CNT-OUTPUT-REC-FROM-WF1-WF2", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Cnt_Output_Rec_From_Wf1 = localVariables.newFieldInRecord("pnd_Cnt_Output_Rec_From_Wf1", "#CNT-OUTPUT-REC-FROM-WF1", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Cnt_Output_Rec_From_Wf2 = localVariables.newFieldInRecord("pnd_Cnt_Output_Rec_From_Wf2", "#CNT-OUTPUT-REC-FROM-WF2", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Cnt_Output_Rec_Tot = localVariables.newFieldInRecord("pnd_Cnt_Output_Rec_Tot", "#CNT-OUTPUT-REC-TOT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Wf1_Input = localVariables.newFieldInRecord("pnd_Cnt_Wf1_Input", "#CNT-WF1-INPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Wf2_Input = localVariables.newFieldInRecord("pnd_Cnt_Wf2_Input", "#CNT-WF2-INPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 1);
        pnd_Reporting_Period = localVariables.newFieldInRecord("pnd_Reporting_Period", "#REPORTING-PERIOD", FieldType.INTEGER, 1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_5", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_6 = pnd_Input_Parm__R_Field_5.newGroupInGroup("pnd_Input_Parm__R_Field_6", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Yy = localVariables.newFieldInRecord("pnd_Yy", "#YY", FieldType.NUMERIC, 4);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        pnd_First_Run = localVariables.newFieldInRecord("pnd_First_Run", "#FIRST-RUN", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_First_Run.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3204() throws Exception
    {
        super("Twrp3204");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3204|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    pnd_Reporting_Period.setValue(5);                                                                                                                     //Natural: ASSIGN #REPORTING-PERIOD := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    short decideConditionsMet152 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM.#PYMNT-DATE-TO-MM;//Natural: VALUE 03
                    if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
                    {
                        decideConditionsMet152++;
                        pnd_Reporting_Period.setValue(1);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 1
                    }                                                                                                                                                     //Natural: VALUE 06
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
                    {
                        decideConditionsMet152++;
                        pnd_Reporting_Period.setValue(2);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 2
                    }                                                                                                                                                     //Natural: VALUE 09
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
                    {
                        decideConditionsMet152++;
                        pnd_Reporting_Period.setValue(3);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 3
                    }                                                                                                                                                     //Natural: VALUE 12
                    else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
                    {
                        decideConditionsMet152++;
                        pnd_Reporting_Period.setValue(4);                                                                                                                 //Natural: ASSIGN #REPORTING-PERIOD := 4
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!",NEWLINE,"ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES", //Natural: WRITE '!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!' /'ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES' /'PLEASE CONTACT SYSTEM SUPPORT'
                            NEWLINE,"PLEASE CONTACT SYSTEM SUPPORT");
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(90);  if (true) return;                                                                                                         //Natural: TERMINATE 90
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                RD_WF1:                                                                                                                                                   //Natural: READ WORK FILE 1 RECORD #WF1-REC
                while (condition(getWorkFiles().read(1, pnd_Wf1_Rec)))
                {
                    CheckAtStartofData169();

                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    pnd_Cnt_Wf1_Input.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CNT-WF1-INPUT
                    REP_LP:                                                                                                                                               //Natural: REPEAT
                    while (condition(whileTrue))
                    {
                        short decideConditionsMet179 = 0;                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WF1-LONG-KEY = #WF2-LONG-KEY
                        if (condition(pnd_Wf1_Rec_Pnd_Wf1_Long_Key.equals(pnd_Wf2_Rec_Pnd_Wf2_Long_Key)))
                        {
                            decideConditionsMet179++;
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT-REC-FROM-WF1-WF2-DATA
                            sub_Gen_Output_Rec_From_Wf1_Wf2_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("REP_LP"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("REP_LP"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Cnt_Output_Rec_From_Wf1_Wf2.nadd(1);                                                                                                      //Natural: ADD 1 TO #CNT-OUTPUT-REC-FROM-WF1-WF2
                            //*        PERFORM PRODUCE-REPORT
                                                                                                                                                                          //Natural: PERFORM READ-WF2-DATA
                            sub_Read_Wf2_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("REP_LP"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("REP_LP"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (true) break REP_LP;                                                                                                                       //Natural: ESCAPE BOTTOM ( REP-LP. )
                        }                                                                                                                                                 //Natural: WHEN #WF1-LONG-KEY > #WF2-LONG-KEY
                        else if (condition(pnd_Wf1_Rec_Pnd_Wf1_Long_Key.greater(pnd_Wf2_Rec_Pnd_Wf2_Long_Key)))
                        {
                            decideConditionsMet179++;
                            pnd_Cnt_Output_Rec_From_Wf2.nadd(1);                                                                                                          //Natural: ADD 1 TO #CNT-OUTPUT-REC-FROM-WF2
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT-REC-FROM-WF2-DATA
                            sub_Gen_Output_Rec_From_Wf2_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("REP_LP"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("REP_LP"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*        PERFORM PRODUCE-REPORT
                            //*        PERFORM PRODUCE-REPORT
                                                                                                                                                                          //Natural: PERFORM READ-WF2-DATA
                            sub_Read_Wf2_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("REP_LP"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("REP_LP"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: WHEN #WF1-LONG-KEY < #WF2-LONG-KEY
                        else if (condition(pnd_Wf1_Rec_Pnd_Wf1_Long_Key.less(pnd_Wf2_Rec_Pnd_Wf2_Long_Key)))
                        {
                            decideConditionsMet179++;
                            pnd_Cnt_Output_Rec_From_Wf1.nadd(1);                                                                                                          //Natural: ADD 1 TO #CNT-OUTPUT-REC-FROM-WF1
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT-REC-FROM-WF1-DATA
                            sub_Gen_Output_Rec_From_Wf1_Data();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("REP_LP"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("REP_LP"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*        PERFORM PRODUCE-REPORT
                            //*        PERFORM PRODUCE-REPORT
                            if (true) break REP_LP;                                                                                                                       //Natural: ESCAPE BOTTOM ( REP-LP. )
                        }                                                                                                                                                 //Natural: WHEN NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-REPEAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_WF1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_WF1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-WORK
                RD_WF1_Exit:
                if (Global.isEscape()) return;
                if (condition(pnd_Cnt_Wf1_Input.equals(getZero())))                                                                                                       //Natural: IF #CNT-WF1-INPUT = 0
                {
                    pnd_First_Run.setValue(true);                                                                                                                         //Natural: ASSIGN #FIRST-RUN := TRUE
                }                                                                                                                                                         //Natural: END-IF
                REPEAT01:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pnd_Wf2_Rec_Pnd_Wf2_Long_Key.equals(pnd_Ws_Const_High_Values))) {break;}                                                                //Natural: UNTIL #WF2-LONG-KEY = HIGH-VALUES
                    pnd_Cnt_Output_Rec_From_Wf2.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-OUTPUT-REC-FROM-WF2
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT-REC-FROM-WF2-DATA
                    sub_Gen_Output_Rec_From_Wf2_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  PERFORM PRODUCE-REPORT
                                                                                                                                                                          //Natural: PERFORM READ-WF2-DATA
                    sub_Read_Wf2_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  EDITH 2/22/99
                }                                                                                                                                                         //Natural: END-REPEAT
                if (Global.isEscape()) return;
                pnd_Cnt_Output_Rec_Tot.compute(new ComputeParameters(false, pnd_Cnt_Output_Rec_Tot), pnd_Cnt_Output_Rec_From_Wf1_Wf2.add(pnd_Cnt_Output_Rec_From_Wf1).add(pnd_Cnt_Output_Rec_From_Wf2)); //Natural: ASSIGN #CNT-OUTPUT-REC-TOT := #CNT-OUTPUT-REC-FROM-WF1-WF2 + #CNT-OUTPUT-REC-FROM-WF1 + #CNT-OUTPUT-REC-FROM-WF2
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new               //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / 42T 'RECORDS RESULTING FROM MERGING TWO INPUT DATA SETS' / 57T #RUN-TYPE / 53T 'C O N T R O L    T O T A L S' // / '(1) TOTAL # OF RECORDS READ FROM WF1 INPUT FILE:' 70T #CNT-WF1-INPUT / '(2) TOTAL # OF RECORDS READ FROM WF2 INPUT FILE:' 70T #CNT-WF2-INPUT / '(3) TOTAL # OF OUTPUT RECORDS QUALIFIED FOR FURTHER PROCESSING:' 70T #CNT-OUTPUT-REC-TOT // '(4) TOTAL # OF OUTPUT RECORDS FROM MERGING WF1 & WF2 RECORDS:' 65T #CNT-OUTPUT-REC-FROM-WF1-WF2 / '(5) TOTAL # OF OUTPUT RECORDS FROM WF1 RECORDS ONLY:' 65T #CNT-OUTPUT-REC-FROM-WF1 / '(6) TOTAL # OF OUTPUT RECORDS FROM WF2 RECORDS ONLY:' 65T #CNT-OUTPUT-REC-FROM-WF2 /// '**FOR AUDITING PURPOSE, THE FOLLOWING EQUATIONS SHOULD BE SATISFIED**' // 10T '(4) + (5) + (6) = (3)' / 10T '(4) + (5)       = (1)' / 10T '(4) + (6)       = (2)'
                    TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,new TabSetting(42),"RECORDS RESULTING FROM MERGING TWO INPUT DATA SETS",NEWLINE,new 
                    TabSetting(57),pnd_Run_Type,NEWLINE,new TabSetting(53),"C O N T R O L    T O T A L S",NEWLINE,NEWLINE,NEWLINE,"(1) TOTAL # OF RECORDS READ FROM WF1 INPUT FILE:",new 
                    TabSetting(70),pnd_Cnt_Wf1_Input,NEWLINE,"(2) TOTAL # OF RECORDS READ FROM WF2 INPUT FILE:",new TabSetting(70),pnd_Cnt_Wf2_Input,NEWLINE,"(3) TOTAL # OF OUTPUT RECORDS QUALIFIED FOR FURTHER PROCESSING:",new 
                    TabSetting(70),pnd_Cnt_Output_Rec_Tot,NEWLINE,NEWLINE,"(4) TOTAL # OF OUTPUT RECORDS FROM MERGING WF1 & WF2 RECORDS:",new TabSetting(65),pnd_Cnt_Output_Rec_From_Wf1_Wf2,NEWLINE,"(5) TOTAL # OF OUTPUT RECORDS FROM WF1 RECORDS ONLY:",new 
                    TabSetting(65),pnd_Cnt_Output_Rec_From_Wf1,NEWLINE,"(6) TOTAL # OF OUTPUT RECORDS FROM WF2 RECORDS ONLY:",new TabSetting(65),pnd_Cnt_Output_Rec_From_Wf2,NEWLINE,NEWLINE,NEWLINE,"**FOR AUDITING PURPOSE, THE FOLLOWING EQUATIONS SHOULD BE SATISFIED**",NEWLINE,NEWLINE,new 
                    TabSetting(10),"(4) + (5) + (6) = (3)",NEWLINE,new TabSetting(10),"(4) + (5)       = (1)",NEWLINE,new TabSetting(10),"(4) + (6)       = (2)");
                if (Global.isEscape()) return;
                //* ******************************
                //* *********************************************
                //* *********************************************
                //* *************************************************
                //* *******************************
                //* *******************************
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet253 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet253++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet253++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet253++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet253++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Wf2_Data() throws Exception                                                                                                                     //Natural: READ-WF2-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        getWorkFiles().read(2, pnd_Wf2_Rec);                                                                                                                              //Natural: READ WORK FILE 2 ONCE #WF2-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Wf2_Rec_Pnd_Wf2_Long_Key.setValue(pnd_Ws_Const_High_Values);                                                                                              //Natural: ASSIGN #WF2-LONG-KEY := HIGH-VALUES
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Cnt_Wf2_Input.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #CNT-WF2-INPUT
    }
    private void sub_Gen_Output_Rec_From_Wf1_Data() throws Exception                                                                                                      //Natural: GEN-OUTPUT-REC-FROM-WF1-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pnd_Qtrly_Rec.reset();                                                                                                                                            //Natural: RESET #QTRLY-REC
        pnd_Qtrly_Rec.setValuesByName(pnd_Wf1_Rec);                                                                                                                       //Natural: MOVE BY NAME #WF1-REC TO #QTRLY-REC
        getWorkFiles().write(3, false, pnd_Qtrly_Rec);                                                                                                                    //Natural: WRITE WORK FILE 3 #QTRLY-REC
    }
    private void sub_Gen_Output_Rec_From_Wf2_Data() throws Exception                                                                                                      //Natural: GEN-OUTPUT-REC-FROM-WF2-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pnd_Qtrly_Rec.reset();                                                                                                                                            //Natural: RESET #QTRLY-REC
        if (condition(pnd_First_Run.getBoolean() && pnd_Wf2_Rec_Pnd_Cntrct_Ppcn_Nbr.equals(" ")))                                                                         //Natural: IF #FIRST-RUN AND #WF2-REC.#CNTRCT-PPCN-NBR EQ ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Qtrly_Rec_Pnd_Qtrly_Rec_Non_Amts.setValuesByName(pnd_Wf2_Rec);                                                                                            //Natural: MOVE BY NAME #WF2-REC TO #QTRLY-REC.#QTRLY-REC-NON-AMTS
            pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period));                              //Natural: MOVE #WF2-REC.#YTD-ST-TAX ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
            pnd_Qtrly_Rec_Pnd_Ytd_Lc_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_Lc_Tax.getValue(pnd_Reporting_Period));                              //Natural: MOVE #WF2-REC.#YTD-LC-TAX ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-LC-TAX ( #REPORTING-PERIOD )
            pnd_Qtrly_Rec_Pnd_Ytd_Gross_Amt.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_Gross_Amt.getValue(pnd_Reporting_Period));                        //Natural: MOVE #WF2-REC.#YTD-GROSS-AMT ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-GROSS-AMT ( #REPORTING-PERIOD )
            getWorkFiles().write(3, false, pnd_Qtrly_Rec);                                                                                                                //Natural: WRITE WORK FILE 3 #QTRLY-REC
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Output_Rec_From_Wf1_Wf2_Data() throws Exception                                                                                                  //Natural: GEN-OUTPUT-REC-FROM-WF1-WF2-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        pnd_Qtrly_Rec.reset();                                                                                                                                            //Natural: RESET #QTRLY-REC
        pnd_Qtrly_Rec.setValuesByName(pnd_Wf1_Rec);                                                                                                                       //Natural: MOVE BY NAME #WF1-REC TO #QTRLY-REC
        pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_St_Tax.getValue(pnd_Reporting_Period));                                  //Natural: MOVE #WF2-REC.#YTD-ST-TAX ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-ST-TAX ( #REPORTING-PERIOD )
        pnd_Qtrly_Rec_Pnd_Ytd_Lc_Tax.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_Lc_Tax.getValue(pnd_Reporting_Period));                                  //Natural: MOVE #WF2-REC.#YTD-LC-TAX ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-LC-TAX ( #REPORTING-PERIOD )
        pnd_Qtrly_Rec_Pnd_Ytd_Gross_Amt.getValue(pnd_Reporting_Period).setValue(pnd_Wf2_Rec_Pnd_Ytd_Gross_Amt.getValue(pnd_Reporting_Period));                            //Natural: MOVE #WF2-REC.#YTD-GROSS-AMT ( #REPORTING-PERIOD ) TO #QTRLY-REC.#YTD-GROSS-AMT ( #REPORTING-PERIOD )
        getWorkFiles().write(3, false, pnd_Qtrly_Rec);                                                                                                                    //Natural: WRITE WORK FILE 3 #QTRLY-REC
    }
    private void sub_Produce_Report() throws Exception                                                                                                                    //Natural: PRODUCE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "SSN",                                                                                                                                    //Natural: DISPLAY ( 1 ) 'SSN' #QTRLY-REC.#ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'CONTRACT' #QTRLY-REC.#CNTRCT-PPCN-NBR ( AL = 8 ) / 'PAYEE' #QTRLY-REC.#CNTRCT-PAYEE-CDE 'CURRENT TAX' #QTRLY-REC.#QTR-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC.#ADJ-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC.#YTD-ST-TAX ( 1 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC.#QTR-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC.#ADJ-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC.#YTD-ST-TAX ( 2 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC.#QTR-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC.#ADJ-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC.#YTD-ST-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) 'CURRENT TAX' #QTRLY-REC.#QTR-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) / 'ADJUSTMENT ' #QTRLY-REC.#ADJ-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) 'YEAR-TO-DATE' #QTRLY-REC.#YTD-ST-TAX ( 4 ) ( EM = ZZZ,ZZ9.99- ) / 'YTD-EOY-ADJ ' #QTRLY-REC.#YTD-ST-TAX ( 5 ) ( EM = ZZZ,ZZ9.99- )
        		pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),NEWLINE,"PAYEE",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde,"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(1), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(2), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(4), new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"YTD-EOY-ADJ ",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax.getValue(5), new ReportEditMask ("ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),new TabSetting(50),"ILLINOIS QUARTERLY TAX REPORTING",new         //Natural: WRITE ( 01 ) NOTITLE NOHDR *INIT-USER 50T 'ILLINOIS QUARTERLY TAX REPORTING' 110T *DATX ( EM = MM/DD/YYYY ) *TIMX / *PROGRAM 42T 'RECORDS RESULTING FROM MERGING TWO INPUT DATA SETS' 110T 'PAGE:' *PAGE-NUMBER ( 01 ) // 27T 'FIRST QUARTER' 51T 'SECOND QUARTER' 77T 'THIRD QUARTER' 97T 'FOURTH QUARTER / EOY ADJ' /
                        TabSetting(110),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(),NEWLINE,Global.getPROGRAM(),new TabSetting(42),"RECORDS RESULTING FROM MERGING TWO INPUT DATA SETS",new 
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(27),"FIRST QUARTER",new TabSetting(51),"SECOND QUARTER",new 
                        TabSetting(77),"THIRD QUARTER",new TabSetting(97),"FOURTH QUARTER / EOY ADJ",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "SSN",
        		pnd_Qtrly_Rec_Pnd_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),"CONTRACT",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),NEWLINE,"PAYEE",
        		pnd_Qtrly_Rec_Pnd_Cntrct_Payee_Cde,"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"CURRENT TAX",
        		pnd_Qtrly_Rec_Pnd_Qtr_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"ADJUSTMENT ",
        		pnd_Qtrly_Rec_Pnd_Adj_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),"YEAR-TO-DATE",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),NEWLINE,"YTD-EOY-ADJ ",
        		pnd_Qtrly_Rec_Pnd_Ytd_St_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"));
    }
    private void CheckAtStartofData169() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM READ-WF2-DATA
            sub_Read_Wf2_Data();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(pnd_Reporting_Period.equals(1)))                                                                                                                //Natural: IF #REPORTING-PERIOD = 1
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "RD_WF1");                                                                                //Natural: ESCAPE BOTTOM ( RD-WF1. )
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
