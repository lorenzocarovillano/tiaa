/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:26 PM
**        * FROM NATURAL PROGRAM : Twrp0472
************************************************************
**        * FILE NAME            : Twrp0472.java
**        * CLASS NAME           : Twrp0472
**        * INSTANCE NAME        : Twrp0472
************************************************************
************************************************************************
** PROGRAM : TWRP0472
** SYSTEM  : TAXWARS
** AUTHOR  : DIANE YHUN
** FUNCTION: LOAD CLIENT ID FILE TO THE TAXWARS CLIENT ID TABLE.
** HISTORY : 07/2012  SUNY-CUNY PHASE 2
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0472 extends BLNatBase
{
    // Data Areas
    private LdaTwrlclrd ldaTwrlclrd;
    private LdaTwrlclup ldaTwrlclup;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup edw_Client_Id_Record;
    private DbsField edw_Client_Id_Record_Edw_Client_Id;
    private DbsField edw_Client_Id_Record_Edw_Client_Name;
    private DbsField edw_Client_Id_Record_Filler;
    private DbsField pnd_Timx;
    private DbsField pnd_Isn;
    private DbsField pnd_Update;
    private DbsField pnd_Add;
    private DbsField pnd_Inverse_Lu_Ts;
    private DbsField pnd_Action;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Loaded;
    private DbsField pnd_Total_Not_Loaded;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_1;
    private DbsField pnd_Rt_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Client_Id;
    private DbsField pnd_Rt_Data;

    private DbsGroup pnd_Rt_Data__R_Field_2;
    private DbsField pnd_Rt_Data_Pnd_Rt_Ci_Name;
    private DbsField pnd_Edw_Data;

    private DbsGroup pnd_Edw_Data__R_Field_3;
    private DbsField pnd_Edw_Data_Pnd_Edw_Ci_Name;
    private DbsField pnd_Rt_Ci_Comm_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlclrd = new LdaTwrlclrd();
        registerRecord(ldaTwrlclrd);
        registerRecord(ldaTwrlclrd.getVw_twrlclnt());
        ldaTwrlclup = new LdaTwrlclup();
        registerRecord(ldaTwrlclup);
        registerRecord(ldaTwrlclup.getVw_twrlclnt_U());

        // Local Variables
        localVariables = new DbsRecord();

        edw_Client_Id_Record = localVariables.newGroupInRecord("edw_Client_Id_Record", "EDW-CLIENT-ID-RECORD");
        edw_Client_Id_Record_Edw_Client_Id = edw_Client_Id_Record.newFieldInGroup("edw_Client_Id_Record_Edw_Client_Id", "EDW-CLIENT-ID", FieldType.STRING, 
            6);
        edw_Client_Id_Record_Edw_Client_Name = edw_Client_Id_Record.newFieldInGroup("edw_Client_Id_Record_Edw_Client_Name", "EDW-CLIENT-NAME", FieldType.STRING, 
            64);
        edw_Client_Id_Record_Filler = edw_Client_Id_Record.newFieldInGroup("edw_Client_Id_Record_Filler", "FILLER", FieldType.STRING, 130);
        pnd_Timx = localVariables.newFieldInRecord("pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Update = localVariables.newFieldInRecord("pnd_Update", "#UPDATE", FieldType.BOOLEAN, 1);
        pnd_Add = localVariables.newFieldInRecord("pnd_Add", "#ADD", FieldType.BOOLEAN, 1);
        pnd_Inverse_Lu_Ts = localVariables.newFieldInRecord("pnd_Inverse_Lu_Ts", "#INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 13);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 6);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Loaded = localVariables.newFieldInRecord("pnd_Total_Loaded", "#TOTAL-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Not_Loaded = localVariables.newFieldInRecord("pnd_Total_Not_Loaded", "#TOTAL-NOT-LOADED", FieldType.PACKED_DECIMAL, 7);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 4);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_1", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt_Table_Id = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Pnd_Rt_Client_Id = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Client_Id", "#RT-CLIENT-ID", FieldType.STRING, 
            6);
        pnd_Rt_Data = localVariables.newFieldInRecord("pnd_Rt_Data", "#RT-DATA", FieldType.STRING, 64);

        pnd_Rt_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Rt_Data__R_Field_2", "REDEFINE", pnd_Rt_Data);
        pnd_Rt_Data_Pnd_Rt_Ci_Name = pnd_Rt_Data__R_Field_2.newFieldInGroup("pnd_Rt_Data_Pnd_Rt_Ci_Name", "#RT-CI-NAME", FieldType.STRING, 64);
        pnd_Edw_Data = localVariables.newFieldInRecord("pnd_Edw_Data", "#EDW-DATA", FieldType.STRING, 64);

        pnd_Edw_Data__R_Field_3 = localVariables.newGroupInRecord("pnd_Edw_Data__R_Field_3", "REDEFINE", pnd_Edw_Data);
        pnd_Edw_Data_Pnd_Edw_Ci_Name = pnd_Edw_Data__R_Field_3.newFieldInGroup("pnd_Edw_Data_Pnd_Edw_Ci_Name", "#EDW-CI-NAME", FieldType.STRING, 64);
        pnd_Rt_Ci_Comm_Ind = localVariables.newFieldInRecord("pnd_Rt_Ci_Comm_Ind", "#RT-CI-COMM-IND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlclrd.initializeValues();
        ldaTwrlclup.initializeValues();

        localVariables.reset();
        pnd_Page_Number.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0472() throws Exception
    {
        super("Twrp0472");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ONE TIME STAMP FOR THE ENTIRE LOAD
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132
        pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000));                                                                 //Natural: ASSIGN #TAX-YEAR := *DATN / 10000
        pnd_Timx.setValue(Global.getTIMX());                                                                                                                              //Natural: ASSIGN #TIMX := *TIMX
        R_EDW:                                                                                                                                                            //Natural: READ WORK FILE 1 EDW-CLIENT-ID-RECORD
        while (condition(getWorkFiles().read(1, edw_Client_Id_Record)))
        {
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            if (condition(edw_Client_Id_Record_Edw_Client_Id.equals(" ") && edw_Client_Id_Record_Edw_Client_Name.equals(" ")))                                            //Natural: IF EDW-CLIENT-ID = ' ' AND EDW-CLIENT-NAME = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  #EDW-DATA
            pnd_Edw_Data_Pnd_Edw_Ci_Name.setValue(edw_Client_Id_Record_Edw_Client_Name);                                                                                  //Natural: ASSIGN #EDW-CI-NAME := EDW-CLIENT-NAME
            pnd_Rt_Super1.setValue(" ");                                                                                                                                  //Natural: ASSIGN #RT-SUPER1 := ' '
            pnd_Rt_Super1_Pnd_Rt_A_I_Ind.setValue("A");                                                                                                                   //Natural: ASSIGN #RT-A-I-IND := 'A'
            pnd_Rt_Super1_Pnd_Rt_Table_Id.setValue("TX013");                                                                                                              //Natural: ASSIGN #RT-TABLE-ID := 'TX013'
            if (condition(edw_Client_Id_Record_Edw_Client_Id.notEquals(" ")))                                                                                             //Natural: IF EDW-CLIENT-ID NE ' '
            {
                pnd_Rt_Super1_Pnd_Rt_Client_Id.setValue(edw_Client_Id_Record_Edw_Client_Id);                                                                              //Natural: ASSIGN #RT-CLIENT-ID := EDW-CLIENT-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rt_Super1_Pnd_Rt_Client_Id.setValue("Name not supplied by EDW");                                                                                      //Natural: ASSIGN #RT-CLIENT-ID := 'Name not supplied by EDW'
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrlclrd.getVw_twrlclnt().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) TWRLCLNT BY RT-SUPER1 STARTING FROM #RT-SUPER1
            (
            "R_CLIENT",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            R_CLIENT:
            while (condition(ldaTwrlclrd.getVw_twrlclnt().readNextRow("R_CLIENT")))
            {
                pnd_Isn.setValue(ldaTwrlclrd.getVw_twrlclnt().getAstISN("R_CLIENT"));                                                                                     //Natural: ASSIGN #ISN := *ISN
                //*    #RT-DATA
                pnd_Rt_Data_Pnd_Rt_Ci_Name.setValue(ldaTwrlclrd.getTwrlclnt_Rt_Ci_Name());                                                                                //Natural: ASSIGN #RT-CI-NAME := TWRLCLNT.RT-CI-NAME
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R_EDW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            pnd_Add.setValue(false);                                                                                                                                      //Natural: ASSIGN #ADD := FALSE
            pnd_Update.setValue(false);                                                                                                                                   //Natural: ASSIGN #UPDATE := FALSE
            short decideConditionsMet181 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRLCLNT.RT-TABLE-ID NE 'TX013' OR TWRLCLNT.RT-A-I-IND NE 'A' OR TWRLCLNT.RT-CLIENT-ID NE EDW-CLIENT-ID
            if (condition(ldaTwrlclrd.getTwrlclnt_Rt_Table_Id().notEquals("TX013") || ldaTwrlclrd.getTwrlclnt_Rt_A_I_Ind().notEquals("A") || ldaTwrlclrd.getTwrlclnt_Rt_Client_Id().notEquals(edw_Client_Id_Record_Edw_Client_Id)))
            {
                decideConditionsMet181++;
                pnd_Add.setValue(true);                                                                                                                                   //Natural: ASSIGN #ADD := TRUE
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-CLIENT-ID
                sub_Add_New_Client_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN TWRLCLNT.RT-TABLE-ID = 'TX013' AND TWRLCLNT.RT-A-I-IND = 'A' AND TWRLCLNT.RT-CLIENT-ID = EDW-CLIENT-ID AND #RT-DATA NE #EDW-DATA
            else if (condition(ldaTwrlclrd.getTwrlclnt_Rt_Table_Id().equals("TX013") && ldaTwrlclrd.getTwrlclnt_Rt_A_I_Ind().equals("A") && ldaTwrlclrd.getTwrlclnt_Rt_Client_Id().equals(edw_Client_Id_Record_Edw_Client_Id) 
                && pnd_Rt_Data.notEquals(pnd_Edw_Data)))
            {
                decideConditionsMet181++;
                pnd_Update.setValue(true);                                                                                                                                //Natural: ASSIGN #UPDATE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CLIENT-ID
                sub_Update_Client_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet181 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-CLIENT-ID
                sub_Print_Client_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_EDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_EDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Total_Not_Loaded.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-NOT-LOADED
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        R_EDW_Exit:
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CLIENT-ID
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-NEW-CLIENT-ID
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CLIENT-ID
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //*  WRITE (1) ' VTW1121D - ' *PROGRAM 14X    DY1
        //* *********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total Records from file  :",pnd_Total_Read,NEWLINE,"Total Records not Loaded :",pnd_Total_Not_Loaded,         //Natural: WRITE ( 1 ) / 'Total Records from file  :' #TOTAL-READ / 'Total Records not Loaded :' #TOTAL-NOT-LOADED / 'Total Records Loaded     :' #TOTAL-LOADED
            NEWLINE,"Total Records Loaded     :",pnd_Total_Loaded);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Client_Id() throws Exception                                                                                                                  //Natural: UPDATE-CLIENT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        UPDATE_CLIENT_ID:                                                                                                                                                 //Natural: GET TWRLCLNT-U #ISN
        ldaTwrlclup.getVw_twrlclnt_U().readByID(pnd_Isn.getLong(), "UPDATE_CLIENT_ID");
        ldaTwrlclup.getTwrlclnt_U_Rt_A_I_Ind().setValue("I");                                                                                                             //Natural: ASSIGN TWRLCLNT-U.RT-A-I-IND := 'I'
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Lu_Ts().setValue(pnd_Timx);                                                                                                       //Natural: ASSIGN TWRLCLNT-U.RT-CI-LU-TS := #TIMX
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Lu_Ts())); //Natural: ASSIGN TWRLCLNT-U.RT-CI-INVERSE-LU-TS := 1000000000000 - TWRLCLNT-U.RT-CI-LU-TS
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLCLNT-U.RT-CI-LU-USER-ID := *INIT-USER
        pnd_Rt_Ci_Comm_Ind.setValue(ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Comm_Ind().getBoolean());                                                                             //Natural: ASSIGN #RT-CI-COMM-IND := TWRLCLNT-U.RT-CI-COMM-IND
        ldaTwrlclup.getVw_twrlclnt_U().updateDBRow("UPDATE_CLIENT_ID");                                                                                                   //Natural: UPDATE ( UPDATE-CLIENT-ID. )
                                                                                                                                                                          //Natural: PERFORM ADD-NEW-CLIENT-ID
        sub_Add_New_Client_Id();
        if (condition(Global.isEscape())) {return;}
        //*  UPDATE-CLIENT-ID
    }
    private void sub_Add_New_Client_Id() throws Exception                                                                                                                 //Natural: ADD-NEW-CLIENT-ID
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrlclup.getTwrlclnt_U_Rt_A_I_Ind().setValue("A");                                                                                                             //Natural: ASSIGN TWRLCLNT-U.RT-A-I-IND := 'A'
        ldaTwrlclup.getTwrlclnt_U_Rt_Table_Id().setValue("TX013");                                                                                                        //Natural: ASSIGN TWRLCLNT-U.RT-TABLE-ID := 'TX013'
        ldaTwrlclup.getTwrlclnt_U_Rt_Client_Id().setValue(edw_Client_Id_Record_Edw_Client_Id);                                                                            //Natural: ASSIGN TWRLCLNT-U.RT-CLIENT-ID := EDW-CLIENT-ID
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Cr_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLCLNT-U.RT-CI-CR-USER-ID := *INIT-USER
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Cr_Ts().setValue(pnd_Timx);                                                                                                       //Natural: ASSIGN TWRLCLNT-U.RT-CI-CR-TS := #TIMX
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Lu_User_Id().setValue(Global.getINIT_USER());                                                                                     //Natural: ASSIGN TWRLCLNT-U.RT-CI-LU-USER-ID := *INIT-USER
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Lu_Ts().setValue(ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Cr_Ts());                                                                        //Natural: ASSIGN TWRLCLNT-U.RT-CI-LU-TS := TWRLCLNT-U.RT-CI-CR-TS
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Inverse_Lu_Ts().compute(new ComputeParameters(false, ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Inverse_Lu_Ts()), new DbsDecimal("1000000000000").subtract(ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Cr_Ts())); //Natural: ASSIGN TWRLCLNT-U.RT-CI-INVERSE-LU-TS := 1000000000000 - TWRLCLNT-U.RT-CI-CR-TS
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Srce().setValue("B");                                                                                                             //Natural: ASSIGN TWRLCLNT-U.RT-CI-SRCE := 'B'
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Comm_Ind().reset();                                                                                                           //Natural: RESET TWRLCLNT-U.RT-CI-COMM-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Comm_Ind().setValue(pnd_Rt_Ci_Comm_Ind.getBoolean());                                                                         //Natural: ASSIGN TWRLCLNT-U.RT-CI-COMM-IND := #RT-CI-COMM-IND
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrlclup.getTwrlclnt_U_Rt_Ci_Name().setValue(edw_Client_Id_Record_Edw_Client_Name);                                                                            //Natural: ASSIGN TWRLCLNT-U.RT-CI-NAME := EDW-CLIENT-NAME
        ldaTwrlclup.getTwrlclnt_U_Rt_Long_Key().setValue(edw_Client_Id_Record_Edw_Client_Name);                                                                           //Natural: ASSIGN TWRLCLNT-U.RT-LONG-KEY := EDW-CLIENT-NAME
        ldaTwrlclup.getVw_twrlclnt_U().insertDBRow();                                                                                                                     //Natural: STORE TWRLCLNT-U
        //*  ADD-NEW-CLIENT-ID
    }
    private void sub_Print_Client_Id() throws Exception                                                                                                                   //Natural: PRINT-CLIENT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Update.getBoolean()))                                                                                                                           //Natural: IF #UPDATE
        {
            pnd_Action.setValue("Update");                                                                                                                                //Natural: ASSIGN #ACTION := 'Update'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Add.getBoolean()))                                                                                                                              //Natural: IF #ADD
        {
            pnd_Action.setValue("Add");                                                                                                                                   //Natural: ASSIGN #ACTION := 'Add'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "CLIENT ID",                                                                                                                              //Natural: DISPLAY ( 1 ) 'CLIENT ID' EDW-CLIENT-ID 'CLIENT NAME' EDW-CLIENT-NAME 'ACTION' #ACTION
        		edw_Client_Id_Record_Edw_Client_Id,"CLIENT NAME",
        		edw_Client_Id_Record_Edw_Client_Name,"ACTION",
        		pnd_Action);
        if (Global.isEscape()) return;
        pnd_Total_Loaded.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-LOADED
        //*  PRINT-CLIENT-ID
    }
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR," Page",pnd_Page_Number, new ReportEditMask ("Z,ZZ9"));                                             //Natural: WRITE ( 1 ) NOTITLE NOHDR ' Page' #PAGE-NUMBER ( EM = ZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER()," - ",Global.getPROGRAM(),new ColumnSpacing(14),"Tax Reporting And Withholding System");         //Natural: WRITE ( 1 ) *INIT-USER ' - ' *PROGRAM 14X 'Tax Reporting And Withholding System'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," RUNDATE: ",Global.getDAT4U(),new ColumnSpacing(17),"CLIENT ID TABLE CONTROL REPORT");                                //Natural: WRITE ( 1 ) ' RUNDATE: ' *DAT4U 17X 'CLIENT ID TABLE CONTROL REPORT'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," RUNTIME: ",Global.getTIME(),NEWLINE);                                                                                //Natural: WRITE ( 1 ) ' RUNTIME: ' *TIME /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," TAX YEAR:",pnd_Tax_Year,NEWLINE);                                                                                    //Natural: WRITE ( 1 ) ' TAX YEAR:' #TAX-YEAR /
        if (Global.isEscape()) return;
        //*  WRITE-HEADINGS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                    sub_Write_Headings();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "CLIENT ID",
        		edw_Client_Id_Record_Edw_Client_Id,"CLIENT NAME",
        		edw_Client_Id_Record_Edw_Client_Name,"ACTION",
        		pnd_Action);
    }
}
