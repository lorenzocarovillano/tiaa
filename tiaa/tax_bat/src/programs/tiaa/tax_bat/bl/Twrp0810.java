/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:24 PM
**        * FROM NATURAL PROGRAM : Twrp0810
************************************************************
**        * FILE NAME            : Twrp0810.java
**        * CLASS NAME           : Twrp0810
**        * INSTANCE NAME        : Twrp0810
************************************************************
***********************************************************************
*
* PROGRAM  : TWRP0810
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : EDIT / UPDATE THE PARTICIPANT FILE.
* CREATED  : 10 / 09 / 1998.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM EDITS & UPDATES THE PARTICIPANT DATA BASE FILE.
* HISTORY  :
* 04/07/15  FENDAYA   - COR/NAS SUNSET. FE201506
* 03/12/13  R CARREON - ADDED SRCE CODE 'EW' GENERIC WARRANTS  /* RC01
* 01/12/12  R CARREON - REPLACED TWRL810A WITH TWRL0702. SUNY/CUNY
* 09/28/11  M BERLIN - ADDED MCCAMISH SOURCE CODE "AM" MODELLED
*           FROM "NV"                                  /* 09/28/11
* 05/08/08  ROSE MA - ROTH 401K/403B PROJECT
*           NEW CONTRACT TYPE 'ROTHP' WAS CREATED. FIVE ADDITIONAL NEW
*           FIELDS WERE PASSED FROM CPS TO TAXWARS.
*           UPDATED PROGRAMS IN JOB P1010TWD TO INCORPORATE THESE FIELDS
*           AND WRITE A NEW REPORT TO LIST ALL NEW FIELDS.
*           BASED ON THESE FIELDS, TWO EXISTING TAXWARS FIELDS WERE
*           DETERMINED AND POPULATED.
*           THIS PGM IS RESTOWED TO ACCORMODATE THE UPDATED LDA TWRL810A
* 11/21/2005 BK - SYNCHRONIZED LOCAL TWRL810E TO PROD
* 04/19/2005 MS - NEW PROVINCE CODE 88 - NU
* 09/22/2004 MS - TRUST COMPANY - OP
* 07/10/2003 FELIX ORTIZ
*            ADD ORIGIN CODE SI.
* 04/14/2017 J BREMER  PIN EXPANSION SCAN 08/2017
* 04/11/18   'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0810 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl0702 ldaTwrl0702;
    private LdaTwrl810b ldaTwrl810b;
    private LdaTwrl810c ldaTwrl810c;
    private LdaTwrl810d ldaTwrl810d;
    private LdaTwrl810e ldaTwrl810e;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_oldpar;
    private DbsField oldpar_Twrparti_Citation_Ind;
    private DbsField oldpar_Twrparti_Part_Eff_Start_Dte;
    private DbsField oldpar_Twrparti_Part_Eff_End_Dte;
    private DbsField oldpar_Twrparti_Part_Invrse_End_Dte;

    private DataAccessProgramView vw_country;
    private DbsField country_Tircntl_Nbr_Year_Alpha_Cde;

    private DbsGroup country__R_Field_1;
    private DbsField country_Country_Tbl_Nbr;
    private DbsField country_Country_Tax_Year;
    private DbsField country_Country_Alpha_Code;
    private DbsField pnd_S_Twrparti_Curr_Rec;

    private DbsGroup pnd_S_Twrparti_Curr_Rec__R_Field_2;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status;
    private DbsField pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte;
    private DbsField pnd_Prev_Source;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Reject_Ctr;
    private DbsField pnd_Bypass_Ctr;
    private DbsField pnd_Duplicate_Ctr;
    private DbsField pnd_Retroactive_Ctr;
    private DbsField pnd_F1001_Y_To_N_Ctr;
    private DbsField pnd_F1001_Y_To_N_Ctr2;
    private DbsField pnd_Forms_Cited_Ctr;
    private DbsField pnd_Report_Forms_Ctr;
    private DbsField pnd_New_Store_Ctr;
    private DbsField pnd_Old_Store_Ctr;
    private DbsField pnd_Update_Ctr;
    private DbsField pnd_Oc_Residency_Ctr;
    private DbsField pnd_Conv_Resdency_Ctr;
    private DbsField pnd_Feed_Address_Ctr;
    private DbsField pnd_Country_Found_Ctr;
    private DbsField pnd_Country_Not_Found;
    private DbsField pnd_Default_New_Ctr;
    private DbsField pnd_Default_File_Ctr;
    private DbsField pnd_Reversal_Ctr;
    private DbsField pnd_Twrparti_Residency_Type;
    private DbsField pnd_Twrparti_Frm1078_Eff_Dte;
    private DbsField pnd_Twrparti_Frm1001_Eff_Dte;
    private DbsField pnd_Twrparti_Residency_Code;
    private DbsField pnd_Twrparti_Country_Desc;
    private DbsField pnd_Upd_Part_Eff_End_Dte;

    private DbsGroup pnd_Upd_Part_Eff_End_Dte__R_Field_3;
    private DbsField pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N;
    private DbsField pnd_Addrss_Geographic_Cde;
    private DbsField pnd_Citation_Message_Code;
    private DbsField pnd_Pin_Base_Key;

    private DbsGroup pnd_Pin_Base_Key__R_Field_4;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr;

    private DbsGroup pnd_Pin_Base_Key__R_Field_5;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N;
    private DbsField pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind;
    private DbsField pnd_Country_3_Yr_Alpha_Code;

    private DbsGroup pnd_Country_3_Yr_Alpha_Code__R_Field_6;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year;
    private DbsField pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code;
    private DbsField pnd_Exp_1001_Ccyymmdd_A;

    private DbsGroup pnd_Exp_1001_Ccyymmdd_A__R_Field_7;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N;

    private DbsGroup pnd_Exp_1001_Ccyymmdd_A__R_Field_8;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy;
    private DbsField pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd;
    private DbsField pnd_Country_Init;

    private DbsGroup pnd_Country_Init__R_Field_9;
    private DbsField pnd_Country_Init_Pnd_Country;
    private DbsField pnd_Participant_Found;
    private DbsField pnd_Store_Participant;
    private DbsField pnd_Start_Date_Found;
    private DbsField pnd_Retroactive_Found;
    private DbsField pnd_Country_Found;
    private DbsField pnd_Naa_Found;
    private DbsField pnd_Residency_Found;
    private DbsField pnd_Multi_Country;
    private DbsField pnd_Locality_Found;
    private DbsField pnd_Start_Date_Cite;
    private DbsField pnd_Timn_N;

    private DbsGroup pnd_Timn_N__R_Field_10;
    private DbsField pnd_Timn_N_Pnd_Timn_A;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Et_Const;
    private DbsField pnd_Date_D;
    private DbsField pnd_Part_Isn;
    private DbsField w;
    private DbsField x;
    private DbsField y;
    private DbsField z;
    private DbsField pnd_Rc;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl0702 = new LdaTwrl0702();
        registerRecord(ldaTwrl0702);
        ldaTwrl810b = new LdaTwrl810b();
        registerRecord(ldaTwrl810b);
        registerRecord(ldaTwrl810b.getVw_par());
        ldaTwrl810c = new LdaTwrl810c();
        registerRecord(ldaTwrl810c);
        registerRecord(ldaTwrl810c.getVw_newpar());
        ldaTwrl810d = new LdaTwrl810d();
        registerRecord(ldaTwrl810d);
        registerRecord(ldaTwrl810d.getVw_updpar());
        ldaTwrl810e = new LdaTwrl810e();
        registerRecord(ldaTwrl810e);
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_oldpar = new DataAccessProgramView(new NameInfo("vw_oldpar", "OLDPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        oldpar_Twrparti_Citation_Ind = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        oldpar_Twrparti_Part_Eff_Start_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Eff_Start_Dte", "TWRPARTI-PART-EFF-START-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_START_DTE");
        oldpar_Twrparti_Part_Eff_End_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        oldpar_Twrparti_Part_Invrse_End_Dte = vw_oldpar.getRecord().newFieldInGroup("oldpar_Twrparti_Part_Invrse_End_Dte", "TWRPARTI-PART-INVRSE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_INVRSE_END_DTE");
        registerRecord(vw_oldpar);

        vw_country = new DataAccessProgramView(new NameInfo("vw_country", "COUNTRY"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        country_Tircntl_Nbr_Year_Alpha_Cde = vw_country.getRecord().newFieldInGroup("country_Tircntl_Nbr_Year_Alpha_Cde", "TIRCNTL-NBR-YEAR-ALPHA-CDE", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "TIRCNTL_NBR_YEAR_ALPHA_CDE");
        country_Tircntl_Nbr_Year_Alpha_Cde.setSuperDescriptor(true);

        country__R_Field_1 = vw_country.getRecord().newGroupInGroup("country__R_Field_1", "REDEFINE", country_Tircntl_Nbr_Year_Alpha_Cde);
        country_Country_Tbl_Nbr = country__R_Field_1.newFieldInGroup("country_Country_Tbl_Nbr", "COUNTRY-TBL-NBR", FieldType.STRING, 1);
        country_Country_Tax_Year = country__R_Field_1.newFieldInGroup("country_Country_Tax_Year", "COUNTRY-TAX-YEAR", FieldType.STRING, 4);
        country_Country_Alpha_Code = country__R_Field_1.newFieldInGroup("country_Country_Alpha_Code", "COUNTRY-ALPHA-CODE", FieldType.STRING, 2);
        registerRecord(vw_country);

        pnd_S_Twrparti_Curr_Rec = localVariables.newFieldInRecord("pnd_S_Twrparti_Curr_Rec", "#S-TWRPARTI-CURR-REC", FieldType.STRING, 19);

        pnd_S_Twrparti_Curr_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_S_Twrparti_Curr_Rec__R_Field_2", "REDEFINE", pnd_S_Twrparti_Curr_Rec);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id", 
            "#S-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status", 
            "#S-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte = pnd_S_Twrparti_Curr_Rec__R_Field_2.newFieldInGroup("pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte", 
            "#S-TWRPARTI-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Prev_Source = localVariables.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 2);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reject_Ctr = localVariables.newFieldInRecord("pnd_Reject_Ctr", "#REJECT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypass_Ctr = localVariables.newFieldInRecord("pnd_Bypass_Ctr", "#BYPASS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Duplicate_Ctr = localVariables.newFieldInRecord("pnd_Duplicate_Ctr", "#DUPLICATE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Retroactive_Ctr = localVariables.newFieldInRecord("pnd_Retroactive_Ctr", "#RETROACTIVE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_F1001_Y_To_N_Ctr = localVariables.newFieldInRecord("pnd_F1001_Y_To_N_Ctr", "#F1001-Y-TO-N-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_F1001_Y_To_N_Ctr2 = localVariables.newFieldInRecord("pnd_F1001_Y_To_N_Ctr2", "#F1001-Y-TO-N-CTR2", FieldType.PACKED_DECIMAL, 7);
        pnd_Forms_Cited_Ctr = localVariables.newFieldInRecord("pnd_Forms_Cited_Ctr", "#FORMS-CITED-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Forms_Ctr = localVariables.newFieldInRecord("pnd_Report_Forms_Ctr", "#REPORT-FORMS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Store_Ctr = localVariables.newFieldInRecord("pnd_New_Store_Ctr", "#NEW-STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Store_Ctr = localVariables.newFieldInRecord("pnd_Old_Store_Ctr", "#OLD-STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Update_Ctr = localVariables.newFieldInRecord("pnd_Update_Ctr", "#UPDATE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Oc_Residency_Ctr = localVariables.newFieldInRecord("pnd_Oc_Residency_Ctr", "#OC-RESIDENCY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Conv_Resdency_Ctr = localVariables.newFieldInRecord("pnd_Conv_Resdency_Ctr", "#CONV-RESDENCY-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Feed_Address_Ctr = localVariables.newFieldInRecord("pnd_Feed_Address_Ctr", "#FEED-ADDRESS-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Country_Found_Ctr = localVariables.newFieldInRecord("pnd_Country_Found_Ctr", "#COUNTRY-FOUND-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Country_Not_Found = localVariables.newFieldInRecord("pnd_Country_Not_Found", "#COUNTRY-NOT-FOUND", FieldType.PACKED_DECIMAL, 7);
        pnd_Default_New_Ctr = localVariables.newFieldInRecord("pnd_Default_New_Ctr", "#DEFAULT-NEW-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Default_File_Ctr = localVariables.newFieldInRecord("pnd_Default_File_Ctr", "#DEFAULT-FILE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Reversal_Ctr = localVariables.newFieldInRecord("pnd_Reversal_Ctr", "#REVERSAL-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Twrparti_Residency_Type = localVariables.newFieldInRecord("pnd_Twrparti_Residency_Type", "#TWRPARTI-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Twrparti_Frm1078_Eff_Dte = localVariables.newFieldInRecord("pnd_Twrparti_Frm1078_Eff_Dte", "#TWRPARTI-FRM1078-EFF-DTE", FieldType.STRING, 
            8);
        pnd_Twrparti_Frm1001_Eff_Dte = localVariables.newFieldInRecord("pnd_Twrparti_Frm1001_Eff_Dte", "#TWRPARTI-FRM1001-EFF-DTE", FieldType.STRING, 
            8);
        pnd_Twrparti_Residency_Code = localVariables.newFieldInRecord("pnd_Twrparti_Residency_Code", "#TWRPARTI-RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_Twrparti_Country_Desc = localVariables.newFieldInRecord("pnd_Twrparti_Country_Desc", "#TWRPARTI-COUNTRY-DESC", FieldType.STRING, 35);
        pnd_Upd_Part_Eff_End_Dte = localVariables.newFieldInRecord("pnd_Upd_Part_Eff_End_Dte", "#UPD-PART-EFF-END-DTE", FieldType.STRING, 8);

        pnd_Upd_Part_Eff_End_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Upd_Part_Eff_End_Dte__R_Field_3", "REDEFINE", pnd_Upd_Part_Eff_End_Dte);
        pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N = pnd_Upd_Part_Eff_End_Dte__R_Field_3.newFieldInGroup("pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N", 
            "#UPD-PART-EFF-END-DTE-N", FieldType.NUMERIC, 8);
        pnd_Addrss_Geographic_Cde = localVariables.newFieldInRecord("pnd_Addrss_Geographic_Cde", "#ADDRSS-GEOGRAPHIC-CDE", FieldType.STRING, 2);
        pnd_Citation_Message_Code = localVariables.newFieldInRecord("pnd_Citation_Message_Code", "#CITATION-MESSAGE-CODE", FieldType.NUMERIC, 2);
        pnd_Pin_Base_Key = localVariables.newFieldInRecord("pnd_Pin_Base_Key", "#PIN-BASE-KEY", FieldType.STRING, 13);

        pnd_Pin_Base_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Pin_Base_Key__R_Field_4", "REDEFINE", pnd_Pin_Base_Key);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr = pnd_Pin_Base_Key__R_Field_4.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr", "#PH-UNQUE-ID-NMBR", 
            FieldType.STRING, 12);

        pnd_Pin_Base_Key__R_Field_5 = pnd_Pin_Base_Key__R_Field_4.newGroupInGroup("pnd_Pin_Base_Key__R_Field_5", "REDEFINE", pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr);
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N = pnd_Pin_Base_Key__R_Field_5.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr_N", "#PH-UNQUE-ID-NMBR-N", 
            FieldType.NUMERIC, 12);
        pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind = pnd_Pin_Base_Key__R_Field_4.newFieldInGroup("pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind", "#PH-BSE-ADDRSS-IND", 
            FieldType.STRING, 1);
        pnd_Country_3_Yr_Alpha_Code = localVariables.newFieldInRecord("pnd_Country_3_Yr_Alpha_Code", "#COUNTRY-3-YR-ALPHA-CODE", FieldType.STRING, 7);

        pnd_Country_3_Yr_Alpha_Code__R_Field_6 = localVariables.newGroupInRecord("pnd_Country_3_Yr_Alpha_Code__R_Field_6", "REDEFINE", pnd_Country_3_Yr_Alpha_Code);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr = pnd_Country_3_Yr_Alpha_Code__R_Field_6.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr", 
            "#COUNTRY-TBL-NBR", FieldType.STRING, 1);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year = pnd_Country_3_Yr_Alpha_Code__R_Field_6.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year", 
            "#COUNTRY-TAX-YEAR", FieldType.STRING, 4);
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code = pnd_Country_3_Yr_Alpha_Code__R_Field_6.newFieldInGroup("pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code", 
            "#COUNTRY-ALPHA-CODE", FieldType.STRING, 2);
        pnd_Exp_1001_Ccyymmdd_A = localVariables.newFieldInRecord("pnd_Exp_1001_Ccyymmdd_A", "#EXP-1001-CCYYMMDD-A", FieldType.STRING, 8);

        pnd_Exp_1001_Ccyymmdd_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Exp_1001_Ccyymmdd_A__R_Field_7", "REDEFINE", pnd_Exp_1001_Ccyymmdd_A);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N = pnd_Exp_1001_Ccyymmdd_A__R_Field_7.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyymmdd_N", 
            "#EXP-1001-CCYYMMDD-N", FieldType.NUMERIC, 8);

        pnd_Exp_1001_Ccyymmdd_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Exp_1001_Ccyymmdd_A__R_Field_8", "REDEFINE", pnd_Exp_1001_Ccyymmdd_A);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy = pnd_Exp_1001_Ccyymmdd_A__R_Field_8.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy", "#EXP-1001-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd = pnd_Exp_1001_Ccyymmdd_A__R_Field_8.newFieldInGroup("pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd", "#EXP-1001-MMDD", 
            FieldType.STRING, 4);
        pnd_Country_Init = localVariables.newFieldInRecord("pnd_Country_Init", "#COUNTRY-INIT", FieldType.STRING, 24);

        pnd_Country_Init__R_Field_9 = localVariables.newGroupInRecord("pnd_Country_Init__R_Field_9", "REDEFINE", pnd_Country_Init);
        pnd_Country_Init_Pnd_Country = pnd_Country_Init__R_Field_9.newFieldArrayInGroup("pnd_Country_Init_Pnd_Country", "#COUNTRY", FieldType.STRING, 
            2, new DbsArrayController(1, 12));
        pnd_Participant_Found = localVariables.newFieldInRecord("pnd_Participant_Found", "#PARTICIPANT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Store_Participant = localVariables.newFieldInRecord("pnd_Store_Participant", "#STORE-PARTICIPANT", FieldType.BOOLEAN, 1);
        pnd_Start_Date_Found = localVariables.newFieldInRecord("pnd_Start_Date_Found", "#START-DATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Retroactive_Found = localVariables.newFieldInRecord("pnd_Retroactive_Found", "#RETROACTIVE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Country_Found = localVariables.newFieldInRecord("pnd_Country_Found", "#COUNTRY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Naa_Found = localVariables.newFieldInRecord("pnd_Naa_Found", "#NAA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Residency_Found = localVariables.newFieldInRecord("pnd_Residency_Found", "#RESIDENCY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Multi_Country = localVariables.newFieldInRecord("pnd_Multi_Country", "#MULTI-COUNTRY", FieldType.BOOLEAN, 1);
        pnd_Locality_Found = localVariables.newFieldInRecord("pnd_Locality_Found", "#LOCALITY-FOUND", FieldType.BOOLEAN, 1);
        pnd_Start_Date_Cite = localVariables.newFieldInRecord("pnd_Start_Date_Cite", "#START-DATE-CITE", FieldType.BOOLEAN, 1);
        pnd_Timn_N = localVariables.newFieldInRecord("pnd_Timn_N", "#TIMN-N", FieldType.NUMERIC, 7);

        pnd_Timn_N__R_Field_10 = localVariables.newGroupInRecord("pnd_Timn_N__R_Field_10", "REDEFINE", pnd_Timn_N);
        pnd_Timn_N_Pnd_Timn_A = pnd_Timn_N__R_Field_10.newFieldInGroup("pnd_Timn_N_Pnd_Timn_A", "#TIMN-A", FieldType.STRING, 7);
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Et_Const = localVariables.newFieldInRecord("pnd_Et_Const", "#ET-CONST", FieldType.PACKED_DECIMAL, 3);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Part_Isn = localVariables.newFieldInRecord("pnd_Part_Isn", "#PART-ISN", FieldType.PACKED_DECIMAL, 12);
        w = localVariables.newFieldInRecord("w", "W", FieldType.PACKED_DECIMAL, 4);
        x = localVariables.newFieldInRecord("x", "X", FieldType.PACKED_DECIMAL, 4);
        y = localVariables.newFieldInRecord("y", "Y", FieldType.PACKED_DECIMAL, 4);
        z = localVariables.newFieldInRecord("z", "Z", FieldType.PACKED_DECIMAL, 4);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_oldpar.reset();
        vw_country.reset();

        ldaTwrl0702.initializeValues();
        ldaTwrl810b.initializeValues();
        ldaTwrl810c.initializeValues();
        ldaTwrl810d.initializeValues();
        ldaTwrl810e.initializeValues();

        localVariables.reset();
        pnd_Country_Init.setInitialValue("BFJAMYNTPORSSPTCUKVCVIYO");
        pnd_Et_Const.setInitialValue(200);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0810() throws Exception
    {
        super("Twrp0810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP0810", onError);
        setupReports();
        //* **
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 13 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 14 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 15 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 16 ) PS = 60 LS = 133 ZP = ON
        //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0702.getTwrt_Record())))
        {
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*  PERFORM  REPORT-TAX-TRANSACTION-READ
            pnd_Part_Isn.setValue(0);                                                                                                                                     //Natural: ASSIGN #PART-ISN := 0
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PARTICIPANT
            sub_Lookup_Participant();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  REVERSAL FOUND
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                      //Natural: IF TWRT-OP-REV-IND = 'X'
            {
                pnd_Reversal_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REVERSAL-CTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Participant_Found.equals(false)))                                                                                                       //Natural: IF #PARTICIPANT-FOUND = FALSE
                {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-PARTICIPANT
                    sub_Store_New_Participant();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Store_Participant.equals(true)))                                                                                                    //Natural: IF #STORE-PARTICIPANT = TRUE
                    {
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-PARTICIPANT-FROM-OLD
                        sub_Store_New_Participant_From_Old();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0702.getTwrt_Record_Twrt_Isn().setValue(pnd_Part_Isn);                                                                                                 //Natural: ASSIGN TWRT-ISN := #PART-ISN
            //*   #PART-ISN
            getWorkFiles().write(2, false, ldaTwrl0702.getTwrt_Record());                                                                                                 //Natural: WRITE WORK FILE 02 TWRT-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
            //*  PERFORM  ERROR-DISPLAY-START
            getReports().write(0, "***",new TabSetting(6),"Tax Transaction File (Work File 01) Is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Tax Transaction File (Work File 01) Is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
            //*  PERFORM  ERROR-DISPLAY-END
            //*  TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PARTICIPANT
        //* *-----------------------------------
        //*  ND PAR.TWRPARTI-FRM1078-EFF-DTE   =  '00000000'
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-NEW-PARTICIPANT-FROM-OLD
        //* *-----------------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-NEW-PARTICIPANT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-EXISTING-PARTICIPANT
        //* *--------------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CITE-PARTICIPANT-RECORD
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-RESIDENCY-TYPE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-RESIDENCY-CODE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-NAME-AND-ADDRESS
        //* *----------------------------------------
        //*  #MDMA100.#I-PIN-A7 := TWRT-PIN-ID
        //*         #MDMA100.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-COUNTRY-CODE
        //* *------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CITATION-TABLE
        //* *--------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-MULTI-COUNTRY-CODES
        //* *-------------------------------------------
        //*             PRINT '=' #TWRPARTI-COUNTRY-DESC  '=' #LOCAL-NAME (X)
        //*                   '=' TWRT-PIN-ID
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-LAST-ADDRESS-LINE
        //* *-----------------------------------------
        //*      #TWRPARTI-COUNTRY-DESC  :=  #MDMA100.#O-BASE-ADDRESS-LINE-4
        //*      #TWRPARTI-COUNTRY-DESC  :=  #MDMA100.#O-BASE-ADDRESS-LINE-3
        //*      #TWRPARTI-COUNTRY-DESC  :=  #MDMA100.#O-BASE-ADDRESS-LINE-2
        //*      #TWRPARTI-COUNTRY-DESC  :=  #MDMA100.#O-BASE-ADDRESS-LINE-1
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TAX-TRANSACTION-READ
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TAX-TRANSACTION-BYPASSED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-NEW-PARTICIPANTS-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-RETROACTIVE-DATE-BYPASS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DUPLICATE-DATE-BYPASS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-FORM1001-Y-TO-N-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-FORMS-CITED-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-FORMS-ADDED
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-OC-RESIDENCY-CODE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-CONVERTED-RESIDENCY-CODE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-ADDRESS-FROM-FEEDER
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-FORM1001-Y-TO-N-BYPASS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-LOCALITY-FOUND
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DEFAULT-LOCALITY-NEW
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-DEFAULT-LOCALITY-FILE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-LOCALITY-NOT-FOUND
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 52T 'Tax Transactions Records Read' 120T 'REPORT: RPT1' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Tax Transactions Records Bypassed' 120T 'REPORT: RPT2' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'Participants Added For The First Time' 120T 'REPORT: RPT3' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Retroactive Participants Bypassed' 120T 'REPORT: RPT4' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'Participants Duplicate Date Bypassed' 120T 'REPORT: RPT5' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participants Added Form 1001 From "Y" to "N"' 120T 'REPORT: RPT6' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'Participants Added Forms Cited' 120T 'REPORT: RPT7' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Participants Added Forms Report' 120T 'REPORT: RPT8' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'Participants "OC" Residency Code' 120T 'REPORT: RPT9' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participants Converted Residency Code' 120T 'REPORT: RPT10' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'Participants Address From Feeder' 120T 'REPORT: RPT11' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 12 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 12 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participants Cited Form 1001 From "Y" to "N"' 120T 'REPORT: RPT12' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 13 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 13 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'Participant Country Locality Code Found' 120T 'REPORT: RPT13' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 14 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 14 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Participant Country Locality Default New' 120T 'REPORT: RPT14' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'Participant File Country Locality Default' 120T 'REPORT: RPT15' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 16 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 16 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Participant Country Locality Code NOT Found' 120T 'REPORT: RPT16'
        //* *-------------------
        //*  *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *-------                                                                                                                                                      //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Lookup_Participant() throws Exception                                                                                                                //Natural: LOOKUP-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Participant_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #PARTICIPANT-FOUND := FALSE
        pnd_Store_Participant.setValue(false);                                                                                                                            //Natural: ASSIGN #STORE-PARTICIPANT := FALSE
        pnd_Start_Date_Found.setValue(false);                                                                                                                             //Natural: ASSIGN #START-DATE-FOUND := FALSE
        pnd_Retroactive_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #RETROACTIVE-FOUND := FALSE
        pnd_Country_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #COUNTRY-FOUND := FALSE
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Tax_Id.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                 //Natural: ASSIGN #S-TWRPARTI-TAX-ID := TWRT-TAX-ID
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Status.setValue(" ");                                                                                                      //Natural: ASSIGN #S-TWRPARTI-STATUS := ' '
        pnd_S_Twrparti_Curr_Rec_Pnd_S_Twrparti_Eff_End_Dte.setValue("99999999");                                                                                          //Natural: ASSIGN #S-TWRPARTI-EFF-END-DTE := '99999999'
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
        sub_Lookup_Residency_Type();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("96") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("99")))           //Natural: IF TWRT-STATE-RSDNCY = '96' THRU '99'
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id().notEquals(" ")))                                                                                       //Natural: IF TWRT-PIN-ID NE ' '
            {
                pnd_Residency_Found.setValue(false);                                                                                                                      //Natural: ASSIGN #RESIDENCY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-CODE
                sub_Lookup_Residency_Code();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Residency_Found.equals(true)))                                                                                                          //Natural: IF #RESIDENCY-FOUND = TRUE
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().setValue(pnd_Twrparti_Residency_Code);                                                                 //Natural: ASSIGN TWRT-STATE-RSDNCY := #TWRPARTI-RESIDENCY-CODE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
                    sub_Lookup_Residency_Type();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                                                 //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := TWRT-STATE-RSDNCY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  DEFINED N2 IN TWRL0702
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().equals("01") || ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))           //Natural: IF TWRT-CITIZENSHIP = '01' OR TWRT-PYMNT-TAX-FORM-1078 = 'Y'
                {
                    pnd_Twrparti_Residency_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                                                 //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := TWRT-STATE-RSDNCY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue("OC");                                                                                                           //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := 'OC'
                    ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().setValue("OC");                                                                                        //Natural: ASSIGN TWRT-STATE-RSDNCY := 'OC'
                                                                                                                                                                          //Natural: PERFORM REPORT-OC-RESIDENCY-CODE
                    sub_Report_Oc_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrparti_Residency_Code.setValue(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy());                                                                         //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := TWRT-STATE-RSDNCY
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl810b.getVw_par().startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) PAR WITH TWRPARTI-CURR-REC-SD = #S-TWRPARTI-CURR-REC
        (
        "RL1",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_S_Twrparti_Curr_Rec, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        RL1:
        while (condition(ldaTwrl810b.getVw_par().readNextRow("RL1")))
        {
            if (condition(ldaTwrl810b.getPar_Twrparti_Tax_Id().equals(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id()) && ldaTwrl810b.getPar_Twrparti_Status().equals(" ")       //Natural: IF PAR.TWRPARTI-TAX-ID = TWRT-TAX-ID AND PAR.TWRPARTI-STATUS = ' ' AND PAR.TWRPARTI-PART-EFF-END-DTE = '99999999'
                && ldaTwrl810b.getPar_Twrparti_Part_Eff_End_Dte().equals("99999999")))
            {
                pnd_Participant_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #PARTICIPANT-FOUND := TRUE
                //*   REVERSAL FOUND
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                  //Natural: IF TWRT-OP-REV-IND = 'X'
                {
                    pnd_Part_Isn.setValue(ldaTwrl810b.getVw_par().getAstISN("RL1"));                                                                                      //Natural: ASSIGN #PART-ISN := *ISN ( RL1. )
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   ADD PARTICIPANT FOR THE FIRST TIME
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Tax_Id_Type().equals("2")))                                                                                         //Natural: IF PAR.TWRPARTI-TAX-ID-TYPE = '2'
            {
                pnd_Bypass_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BYPASS-CTR
                //*    PRINT (00)
                //*      'T/C'       TWRT-COMPANY-CDE
                //*      'SR/CE'     TWRT-SOURCE
                //*      'CONTRACT'  TWRT-CNTRCT-NBR
                //*      'PY/EE'     TWRT-PAYEE-CDE
                //*      'TAX ID'    TWRT-TAX-ID
                //*      'T/Y'       TWRT-TAX-ID-TYPE
                //*      'PIN ID'    TWRT-PIN-ID
                //*      'NAME'      TWRT-NA-LINE (1)
                //*      'PAYMENT'   TWRT-PAYMT-DTE (EM=XXXX/XX/XX)
                //*      'P/Y'       TWRT-PAYMT-TYPE
                //*      'S/E'       TWRT-SETTL-TYPE
                //*      'RS/DN'     TWRT-STATE-RSDNCY
                //*      'LOC'       TWRT-LOCALITY-CDE
                //*      'CI/TI'     TWRT-CITIZENSHIP
                //*      'TAX/YEAR'  TWRT-TAX-YEAR
                //*      'RO/LL'     TWRT-ROLLOVER-IND
                //*      'CI/TA'     TWRT-CITATION-FLAG
                //*      'D-O-B'     TWRT-DOB-A  (EM=XXXX/XX/XX)
                //*      '10/78'     TWRT-PYMNT-TAX-FORM-1078
                //*      '10/01'     TWRT-PYMNT-TAX-FORM-1001
                                                                                                                                                                          //Natural: PERFORM REPORT-TAX-TRANSACTION-BYPASSED
                sub_Report_Tax_Transaction_Bypassed();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"NN") || pnd_Twrparti_Residency_Code.equals("OC")))                                             //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( NN ) OR #TWRPARTI-RESIDENCY-CODE = 'OC'
            {
                if (condition(pnd_Twrparti_Residency_Code.equals("35") && ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                                   //Natural: IF #TWRPARTI-RESIDENCY-CODE = '35' AND TWRT-LOCALITY-CDE = '3E'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                         //Natural: ASSIGN TWRT-LOCALITY-CDE := PAR.TWRPARTI-LOCALITY-CDE := ' '
                    ldaTwrl810b.getPar_Twrparti_Locality_Cde().setValue(" ");
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl810b.getPar_Twrparti_Locality_Cde().notEquals(" ") && ldaTwrl810b.getPar_Twrparti_Locality_Cde().notEquals("3E")                     //Natural: IF PAR.TWRPARTI-LOCALITY-CDE NE ' ' AND PAR.TWRPARTI-LOCALITY-CDE NE '3E' AND PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE
                    && ldaTwrl810b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code)))
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(ldaTwrl810b.getPar_Twrparti_Locality_Cde());                                                  //Natural: ASSIGN TWRT-LOCALITY-CDE := PAR.TWRPARTI-LOCALITY-CDE
                                                                                                                                                                          //Natural: PERFORM REPORT-DEFAULT-LOCALITY-FILE
                    sub_Report_Default_Locality_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                         //Natural: ASSIGN TWRT-LOCALITY-CDE := PAR.TWRPARTI-LOCALITY-CDE := ' '
                    ldaTwrl810b.getPar_Twrparti_Locality_Cde().setValue(" ");
                    if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"AA")))                                                                                 //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( AA )
                    {
                        pnd_Multi_Country.setValue(false);                                                                                                                //Natural: ASSIGN #MULTI-COUNTRY := FALSE
                        pnd_Locality_Found.setValue(false);                                                                                                               //Natural: ASSIGN #LOCALITY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM SELECT-MULTI-COUNTRY-CODES
                        sub_Select_Multi_Country_Codes();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Multi_Country.equals(true)))                                                                                                    //Natural: IF #MULTI-COUNTRY = TRUE
                        {
                            if (condition(pnd_Locality_Found.equals(true)))                                                                                               //Natural: IF #LOCALITY-FOUND = TRUE
                            {
                                //*                 PRINT 'OLD'
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-FOUND
                                sub_Report_Locality_Found();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RL1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-NOT-FOUND
                                sub_Report_Locality_Not_Found();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RL1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code) && ldaTwrl810b.getPar_Twrparti_Citizen_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship())  //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE AND PAR.TWRPARTI-CITIZEN-CDE = TWRT-CITIZENSHIP AND PAR.TWRPARTI-FRM1001-IND = TWRT-PYMNT-TAX-FORM-1001 AND PAR.TWRPARTI-FRM1078-IND = TWRT-PYMNT-TAX-FORM-1078 AND ( PAR.TWRPARTI-LOCALITY-CDE = TWRT-LOCALITY-CDE OR TWRT-LOCALITY-CDE = ' ' ) AND ( PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 ) OR TWRT-NA-LINE ( 2 ) = ' ' )
                && ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001()) && ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078()) 
                && (ldaTwrl810b.getPar_Twrparti_Locality_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde()) || ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals(" ")) 
                && (ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2)) || ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" "))))
            {
                if (condition(ldaTwrl810b.getPar_Twrparti_Pin().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id()) && ldaTwrl810b.getPar_Twrparti_Participant_Dob().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A())  //Natural: IF PAR.TWRPARTI-PIN = TWRT-PIN-ID AND PAR.TWRPARTI-PARTICIPANT-DOB = TWRT-DOB-A AND PAR.TWRPARTI-PARTICIPANT-DOD = TWRT-DOD AND PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 ) AND PAR.TWRPARTI-ADDR-LN2 = TWRT-NA-LINE ( 3 ) AND PAR.TWRPARTI-ADDR-LN3 = TWRT-NA-LINE ( 4 ) AND PAR.TWRPARTI-ADDR-LN4 = TWRT-NA-LINE ( 5 ) AND PAR.TWRPARTI-ADDR-LN5 = TWRT-NA-LINE ( 6 ) AND PAR.TWRPARTI-ADDR-LN6 = TWRT-NA-LINE ( 7 ) AND ( PAR.TWRPARTI-PARTICIPANT-NAME = TWRT-NA-LINE ( 1 ) OR PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 ) OR TWRT-NA-LINE ( 1 ) = ' ' )
                    && ldaTwrl810b.getPar_Twrparti_Participant_Dod().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dod()) && ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2)) 
                    && ldaTwrl810b.getPar_Twrparti_Addr_Ln2().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3)) && ldaTwrl810b.getPar_Twrparti_Addr_Ln3().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4)) 
                    && ldaTwrl810b.getPar_Twrparti_Addr_Ln4().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5)) && ldaTwrl810b.getPar_Twrparti_Addr_Ln5().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6)) 
                    && ldaTwrl810b.getPar_Twrparti_Addr_Ln6().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7)) && (ldaTwrl810b.getPar_Twrparti_Participant_Name().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1)) 
                    || ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2)) || ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1).equals(" "))))
                {
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-TAX-TRANSACTION-BYPASSED
                    sub_Report_Tax_Transaction_Bypassed();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2)) || ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" "))  //Natural: IF ( PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 ) OR TWRT-NA-LINE ( 2 ) = ' ' ) AND PAR.TWRPARTI-PARTICIPANT-DOB = TWRT-DOB-A AND PAR.TWRPARTI-PARTICIPANT-DOD = TWRT-DOD AND ( PAR.TWRPARTI-PARTICIPANT-NAME = TWRT-NA-LINE ( 1 ) OR TWRT-NA-LINE ( 1 ) = ' ' )
                        && ldaTwrl810b.getPar_Twrparti_Participant_Dob().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A()) && ldaTwrl810b.getPar_Twrparti_Participant_Dod().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dod()) 
                        && (ldaTwrl810b.getPar_Twrparti_Participant_Name().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1)) || ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1).equals(" "))))
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-EXISTING-PARTICIPANT
                        sub_Update_Existing_Participant();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RESIDENCE, CITIZENSHIP, OR TAX FORMS CHANGED...
            //* IF PAR.TWRPARTI-PART-EFF-START-DTE  >  TWRT-PAYMT-DTE       /* RCC
            if (condition(ldaTwrl810b.getPar_Twrparti_Part_Eff_Start_Dte().greater(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha())))                                   //Natural: IF PAR.TWRPARTI-PART-EFF-START-DTE > TWRT-PAYMT-DTE-ALPHA
            {
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("IP")))                                                                                     //Natural: IF TWRT-SOURCE = 'IP'
                {
                    pnd_Retroactive_Found.setValue(true);                                                                                                                 //Natural: ASSIGN #RETROACTIVE-FOUND := TRUE
                    pnd_Bypass_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #BYPASS-CTR
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Retroactive_Found.setValue(true);                                                                                                                 //Natural: ASSIGN #RETROACTIVE-FOUND := TRUE
                    pnd_Retroactive_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RETROACTIVE-CTR
                                                                                                                                                                          //Natural: PERFORM CITE-PARTICIPANT-RECORD
                    sub_Cite_Participant_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                 //Natural: ASSIGN TWRT-FLAG := 'C'
                    pnd_Citation_Message_Code.setValue(51);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 51
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                    sub_Update_Citation_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM REPORT-RETROACTIVE-DATE-BYPASS
                    sub_Report_Retroactive_Date_Bypass();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* IF PAR.TWRPARTI-PART-EFF-START-DTE  =  TWRT-PAYMT-DTE          /* RCC
            if (condition(ldaTwrl810b.getPar_Twrparti_Part_Eff_Start_Dte().equals(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha())))                                    //Natural: IF PAR.TWRPARTI-PART-EFF-START-DTE = TWRT-PAYMT-DTE-ALPHA
            {
                pnd_Start_Date_Cite.setValue(false);                                                                                                                      //Natural: ASSIGN #START-DATE-CITE := FALSE
                if (condition(ldaTwrl810b.getPar_Twrparti_Residency_Code().notEquals(pnd_Twrparti_Residency_Code)))                                                       //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE NE #TWRPARTI-RESIDENCY-CODE
                {
                    pnd_Citation_Message_Code.setValue(52);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 52
                    pnd_Start_Date_Cite.setValue(true);                                                                                                                   //Natural: ASSIGN #START-DATE-CITE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                    sub_Update_Citation_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Citizen_Cde().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship())))                                        //Natural: IF PAR.TWRPARTI-CITIZEN-CDE NE TWRT-CITIZENSHIP
                {
                    pnd_Citation_Message_Code.setValue(62);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 62
                    pnd_Start_Date_Cite.setValue(true);                                                                                                                   //Natural: ASSIGN #START-DATE-CITE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                    sub_Update_Citation_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("N") && ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().equals("00000000")                   //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'N' AND PAR.TWRPARTI-FRM1001-EFF-DTE = '00000000' AND PAR.TWRPARTI-FRM1001-IND NE TWRT-PYMNT-TAX-FORM-1001
                    && ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001())))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001())))                            //Natural: IF PAR.TWRPARTI-FRM1001-IND NE TWRT-PYMNT-TAX-FORM-1001
                    {
                        pnd_Citation_Message_Code.setValue(63);                                                                                                           //Natural: ASSIGN #CITATION-MESSAGE-CODE := 63
                        pnd_Start_Date_Cite.setValue(true);                                                                                                               //Natural: ASSIGN #START-DATE-CITE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                        sub_Update_Citation_Table();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("N") && ldaTwrl810b.getPar_Twrparti_Frm1078_Eff_Dte().equals("00000000")                   //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'N' AND PAR.TWRPARTI-FRM1078-EFF-DTE = '00000000' AND PAR.TWRPARTI-FRM1078-IND NE TWRT-PYMNT-TAX-FORM-1078
                    && ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078())))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078())))                            //Natural: IF PAR.TWRPARTI-FRM1078-IND NE TWRT-PYMNT-TAX-FORM-1078
                    {
                        pnd_Citation_Message_Code.setValue(64);                                                                                                           //Natural: ASSIGN #CITATION-MESSAGE-CODE := 64
                        pnd_Start_Date_Cite.setValue(true);                                                                                                               //Natural: ASSIGN #START-DATE-CITE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                        sub_Update_Citation_Table();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Locality_Cde().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde())))                                      //Natural: IF PAR.TWRPARTI-LOCALITY-CDE NE TWRT-LOCALITY-CDE
                {
                    pnd_Citation_Message_Code.setValue(46);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 46
                    pnd_Start_Date_Cite.setValue(true);                                                                                                                   //Natural: ASSIGN #START-DATE-CITE := TRUE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                    sub_Update_Citation_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Start_Date_Cite.equals(true)))                                                                                                          //Natural: IF #START-DATE-CITE = TRUE
                {
                    pnd_Duplicate_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUPLICATE-CTR
                    pnd_Start_Date_Found.setValue(true);                                                                                                                  //Natural: ASSIGN #START-DATE-FOUND := TRUE
                    ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                 //Natural: ASSIGN TWRT-FLAG := 'C'
                                                                                                                                                                          //Natural: PERFORM CITE-PARTICIPANT-RECORD
                    sub_Cite_Participant_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM REPORT-DUPLICATE-DATE-BYPASS
                    sub_Report_Duplicate_Date_Bypass();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*   TO CITE THE OTHER SAME
                    getWorkFiles().write(3, false, ldaTwrl0702.getTwrt_Record());                                                                                         //Natural: WRITE WORK FILE 03 TWRT-RECORD
                    //*                                          /*  PAYMENT DATE RECORD
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("N")))                    //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1001 = 'N'
            {
                pnd_Exp_1001_Ccyymmdd_A.setValue(ldaTwrl810b.getPar_Twrparti_Part_Eff_Start_Dte());                                                                       //Natural: ASSIGN #EXP-1001-CCYYMMDD-A := PAR.TWRPARTI-PART-EFF-START-DTE
                pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Ccyy.nadd(2);                                                                                                        //Natural: ASSIGN #EXP-1001-CCYY := #EXP-1001-CCYY + 2
                pnd_Exp_1001_Ccyymmdd_A_Pnd_Exp_1001_Mmdd.setValue("1231");                                                                                               //Natural: ASSIGN #EXP-1001-MMDD := '1231'
                //*   IF TWRT-PAYMT-DTE  >=  #EXP-1001-CCYYMMDD-A            /* RCC
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha().greaterOrEqual(pnd_Exp_1001_Ccyymmdd_A)))                                                 //Natural: IF TWRT-PAYMT-DTE-ALPHA >= #EXP-1001-CCYYMMDD-A
                {
                    pnd_F1001_Y_To_N_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #F1001-Y-TO-N-CTR
                    //*        TWRT-CITATION-FLAG         :=  'C'
                    //*        PAR.TWRPARTI-CITATION-IND  :=  'C'
                    //*        #CITATION-MESSAGE-CODE     :=  53
                    //*        PERFORM  UPDATE-CITATION-TABLE
                                                                                                                                                                          //Natural: PERFORM REPORT-FORM1001-Y-TO-N-ADDED
                    sub_Report_Form1001_Y_To_N_Added();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl810b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code) && ldaTwrl810b.getPar_Twrparti_Citizen_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship())  //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE AND PAR.TWRPARTI-CITIZEN-CDE = TWRT-CITIZENSHIP AND PAR.TWRPARTI-FRM1078-IND = TWRT-PYMNT-TAX-FORM-1078 AND PAR.TWRPARTI-LOCALITY-CDE = TWRT-LOCALITY-CDE AND ( PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 ) OR TWRT-NA-LINE ( 2 ) = ' ' )
                        && ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078()) && ldaTwrl810b.getPar_Twrparti_Locality_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde()) 
                        && (ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2)) || ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" "))))
                    {
                        pnd_F1001_Y_To_N_Ctr2.nadd(1);                                                                                                                    //Natural: ADD 1 TO #F1001-Y-TO-N-CTR2
                        ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                             //Natural: ASSIGN TWRT-FLAG := 'C'
                        pnd_Citation_Message_Code.setValue(53);                                                                                                           //Natural: ASSIGN #CITATION-MESSAGE-CODE := 53
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                        sub_Update_Citation_Table();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM REPORT-FORM1001-Y-TO-N-BYPASS
                        sub_Report_Form1001_Y_To_N_Bypass();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM CITE-PARTICIPANT-RECORD
                        sub_Cite_Participant_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_F1001_Y_To_N_Ctr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #F1001-Y-TO-N-CTR
                        ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                             //Natural: ASSIGN TWRT-FLAG := 'C'
                        ldaTwrl810b.getPar_Twrparti_Citation_Ind().setValue("C");                                                                                         //Natural: ASSIGN PAR.TWRPARTI-CITATION-IND := 'C'
                        pnd_Citation_Message_Code.setValue(53);                                                                                                           //Natural: ASSIGN #CITATION-MESSAGE-CODE := 53
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                        sub_Update_Citation_Table();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM REPORT-FORM1001-Y-TO-N-ADDED
                        sub_Report_Form1001_Y_To_N_Added();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Store_Participant.setValue(true);                                                                                                                         //Natural: ASSIGN #STORE-PARTICIPANT := TRUE
            G2:                                                                                                                                                           //Natural: GET OLDPAR *ISN ( RL1. )
            vw_oldpar.readByID(ldaTwrl810b.getVw_par().getAstISN("RL1"), "G2");
            //* IF TWRT-PAYMT-DTE  =  OLDPAR.TWRPARTI-PART-EFF-START-DTE  /* RCC
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha().equals(oldpar_Twrparti_Part_Eff_Start_Dte)))                                                  //Natural: IF TWRT-PAYMT-DTE-ALPHA = OLDPAR.TWRPARTI-PART-EFF-START-DTE
            {
                pnd_Upd_Part_Eff_End_Dte.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                                           //Natural: ASSIGN #UPD-PART-EFF-END-DTE := TWRT-PAYMT-DTE
                oldpar_Twrparti_Part_Eff_End_Dte.setValue(pnd_Upd_Part_Eff_End_Dte);                                                                                      //Natural: ASSIGN OLDPAR.TWRPARTI-PART-EFF-END-DTE := #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Invrse_End_Dte.compute(new ComputeParameters(false, oldpar_Twrparti_Part_Invrse_End_Dte), DbsField.subtract(100000000,               //Natural: ASSIGN OLDPAR.TWRPARTI-PART-INVRSE-END-DTE := 100000000 - #UPD-PART-EFF-END-DTE-N
                    pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   MOVE EDITED  TWRT-PAYMT-DTE  TO   #DATE-D (EM=YYYYMMDD)     /* RCC
                //* PIN-EXP
                getReports().write(0, "TWRT-PAYMT-DTE-ALPHA:",ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha());                                                         //Natural: WRITE 'TWRT-PAYMT-DTE-ALPHA:' TWRT-PAYMT-DTE-ALPHA
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha());                                              //Natural: MOVE EDITED TWRT-PAYMT-DTE-ALPHA TO #DATE-D ( EM = YYYYMMDD )
                //* PIN-EXP
                getReports().write(0, "#DATE-D:",pnd_Date_D);                                                                                                             //Natural: WRITE '#DATE-D:' #DATE-D
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Date_D.nsubtract(1);                                                                                                                                  //Natural: ASSIGN #DATE-D := #DATE-D - 1
                pnd_Upd_Part_Eff_End_Dte.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Eff_End_Dte.setValue(pnd_Upd_Part_Eff_End_Dte);                                                                                      //Natural: ASSIGN OLDPAR.TWRPARTI-PART-EFF-END-DTE := #UPD-PART-EFF-END-DTE
                oldpar_Twrparti_Part_Invrse_End_Dte.compute(new ComputeParameters(false, oldpar_Twrparti_Part_Invrse_End_Dte), DbsField.subtract(100000000,               //Natural: ASSIGN OLDPAR.TWRPARTI-PART-INVRSE-END-DTE := 100000000 - #UPD-PART-EFF-END-DTE-N
                    pnd_Upd_Part_Eff_End_Dte_Pnd_Upd_Part_Eff_End_Dte_N));
            }                                                                                                                                                             //Natural: END-IF
            vw_oldpar.updateDBRow("G2");                                                                                                                                  //Natural: UPDATE ( G2. )
            pnd_Et_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CTR
            if (condition((ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("N") && ldaTwrl810b.getPar_Twrparti_Frm1078_Eff_Dte().notEquals("00000000")                   //Natural: IF ( PAR.TWRPARTI-FRM1078-IND = 'N' AND PAR.TWRPARTI-FRM1078-EFF-DTE NE '00000000' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y' ) OR ( PAR.TWRPARTI-FRM1001-IND = 'N' AND PAR.TWRPARTI-FRM1001-EFF-DTE NE '00000000' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y' ) OR ( PAR.TWRPARTI-FRM1001-IND = 'N' AND PAR.TWRPARTI-FRM1001-EFF-DTE NE '00000000' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y' ) OR ( PAR.TWRPARTI-FRM1078-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1078 = 'N' ) OR ( PAR.TWRPARTI-FRM1078-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y' ) OR ( PAR.TWRPARTI-FRM1001-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y' )
                && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")) || (ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("N") && ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().notEquals("00000000") 
                && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")) || (ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("N") && ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().notEquals("00000000") 
                && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")) || (ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("N")) 
                || (ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")) || (ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("Y") 
                && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y"))))
            {
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'N' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y'
                {
                    pnd_Citation_Message_Code.setValue(54);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 54
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("N")))                //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1078 = 'N'
                {
                    pnd_Citation_Message_Code.setValue(55);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 55
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'N' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
                {
                    pnd_Citation_Message_Code.setValue(56);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 56
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'N' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y'
                {
                    pnd_Citation_Message_Code.setValue(57);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 57
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
                {
                    pnd_Citation_Message_Code.setValue(58);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 58
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y'
                {
                    pnd_Citation_Message_Code.setValue(59);                                                                                                               //Natural: ASSIGN #CITATION-MESSAGE-CODE := 59
                }                                                                                                                                                         //Natural: END-IF
                pnd_Forms_Cited_Ctr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #FORMS-CITED-CTR
                ldaTwrl810b.getPar_Twrparti_Citation_Ind().setValue("C");                                                                                                 //Natural: ASSIGN PAR.TWRPARTI-CITATION-IND := 'C'
                ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                     //Natural: ASSIGN TWRT-FLAG := 'C'
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                sub_Update_Citation_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM REPORT-FORMS-CITED-ADDED
                sub_Report_Forms_Cited_Added();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("N") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                    //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'N' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
            {
                pnd_Report_Forms_Ctr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REPORT-FORMS-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-FORMS-ADDED
                sub_Report_Forms_Added();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RL1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                    //Natural: IF PAR.TWRPARTI-FRM1078-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
            {
                ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().setValue("N");                                                                                                  //Natural: ASSIGN PAR.TWRPARTI-FRM1078-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals("Y") && ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                    //Natural: IF PAR.TWRPARTI-FRM1001-IND = 'Y' AND TWRT-PYMNT-TAX-FORM-1078 = 'Y'
            {
                ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().setValue("N");                                                                                                  //Natural: ASSIGN PAR.TWRPARTI-FRM1001-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Residency_Code().equals(pnd_Twrparti_Residency_Code)))                                                              //Natural: IF PAR.TWRPARTI-RESIDENCY-CODE = #TWRPARTI-RESIDENCY-CODE
            {
                if (condition(pnd_Twrparti_Residency_Code.equals("35") && ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                                   //Natural: IF #TWRPARTI-RESIDENCY-CODE = '35' AND TWRT-LOCALITY-CDE = '3E'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                         //Natural: ASSIGN TWRT-LOCALITY-CDE := ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Residency_Code().setValue(pnd_Twrparti_Residency_Code);                                                                       //Natural: ASSIGN PAR.TWRPARTI-RESIDENCY-CODE := #TWRPARTI-RESIDENCY-CODE
                //*     IF PAR.TWRPARTI-CITIZEN-CDE  NE  '1'
                //*    AND #TWRPARTI-RESIDENCY-CODE  =  MASK (AA)
                //*        PERFORM  SELECT-MULTI-COUNTRY-CODES
                //*     END-IF
                if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"NN") || pnd_Twrparti_Residency_Code.equals("OC")))                                         //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( NN ) OR #TWRPARTI-RESIDENCY-CODE = 'OC'
                {
                    if (condition(pnd_Twrparti_Residency_Code.equals("35") && ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                               //Natural: IF #TWRPARTI-RESIDENCY-CODE = '35' AND TWRT-LOCALITY-CDE = '3E'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                     //Natural: ASSIGN TWRT-LOCALITY-CDE := PAR.TWRPARTI-LOCALITY-CDE := ' '
                        ldaTwrl810b.getPar_Twrparti_Locality_Cde().setValue(" ");
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                         //Natural: ASSIGN TWRT-LOCALITY-CDE := PAR.TWRPARTI-LOCALITY-CDE := ' '
                    ldaTwrl810b.getPar_Twrparti_Locality_Cde().setValue(" ");
                    if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"AA")))                                                                                 //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( AA )
                    {
                        pnd_Multi_Country.setValue(false);                                                                                                                //Natural: ASSIGN #MULTI-COUNTRY := FALSE
                        pnd_Locality_Found.setValue(false);                                                                                                               //Natural: ASSIGN #LOCALITY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM SELECT-MULTI-COUNTRY-CODES
                        sub_Select_Multi_Country_Codes();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Multi_Country.equals(true)))                                                                                                    //Natural: IF #MULTI-COUNTRY = TRUE
                        {
                            if (condition(pnd_Locality_Found.equals(true)))                                                                                               //Natural: IF #LOCALITY-FOUND = TRUE
                            {
                                //*            PRINT 'OLD'
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-FOUND
                                sub_Report_Locality_Found();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RL1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-NOT-FOUND
                                sub_Report_Locality_Not_Found();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RL1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Locality_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde())))                                             //Natural: IF PAR.TWRPARTI-LOCALITY-CDE = TWRT-LOCALITY-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                      //Natural: ASSIGN PAR.TWRPARTI-LOCALITY-CDE := TWRT-LOCALITY-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Citizen_Cde().equals(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship())))                                               //Natural: IF PAR.TWRPARTI-CITIZEN-CDE = TWRT-CITIZENSHIP
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Citizen_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship());                                                        //Natural: ASSIGN PAR.TWRPARTI-CITIZEN-CDE := TWRT-CITIZENSHIP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001())))                                       //Natural: IF PAR.TWRPARTI-FRM1001-IND = TWRT-PYMNT-TAX-FORM-1001
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Frm1001_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                                //Natural: ASSIGN PAR.TWRPARTI-FRM1001-IND := TWRT-PYMNT-TAX-FORM-1001
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                                                                         //Natural: IF TWRT-PYMNT-TAX-FORM-1001 = 'Y'
                {
                    ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                  //Natural: ASSIGN PAR.TWRPARTI-FRM1001-EFF-DTE := TWRT-PAYMT-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().equals("00000000")))                                                                      //Natural: IF PAR.TWRPARTI-FRM1001-EFF-DTE = '00000000'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl810b.getPar_Twrparti_Frm1001_Eff_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                              //Natural: ASSIGN PAR.TWRPARTI-FRM1001-EFF-DTE := TWRT-PAYMT-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078())))                                       //Natural: IF PAR.TWRPARTI-FRM1078-IND = TWRT-PYMNT-TAX-FORM-1078
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Frm1078_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078());                                                //Natural: ASSIGN PAR.TWRPARTI-FRM1078-IND := TWRT-PYMNT-TAX-FORM-1078
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                                                                         //Natural: IF TWRT-PYMNT-TAX-FORM-1078 = 'Y'
                {
                    ldaTwrl810b.getPar_Twrparti_Frm1078_Eff_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                  //Natural: ASSIGN PAR.TWRPARTI-FRM1078-EFF-DTE := TWRT-PAYMT-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl810b.getPar_Twrparti_Frm1078_Eff_Dte().equals("00000000")))                                                                      //Natural: IF PAR.TWRPARTI-FRM1078-EFF-DTE = '00000000'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl810b.getPar_Twrparti_Frm1078_Eff_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                              //Natural: ASSIGN PAR.TWRPARTI-FRM1078-EFF-DTE := TWRT-PAYMT-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Pin().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id())))                                                            //Natural: IF PAR.TWRPARTI-PIN = TWRT-PIN-ID
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Pin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                     //Natural: ASSIGN PAR.TWRPARTI-PIN := TWRT-PIN-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Participant_Name().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1))))                                  //Natural: IF PAR.TWRPARTI-PARTICIPANT-NAME = TWRT-NA-LINE ( 1 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Participant_Name().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1));                                           //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-NAME := TWRT-NA-LINE ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Participant_Dob().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A())))                                                 //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOB = TWRT-DOB-A
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Participant_Dob().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A());                                                          //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-DOB := TWRT-DOB-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Participant_Dod().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dod())))                                                   //Natural: IF PAR.TWRPARTI-PARTICIPANT-DOD = TWRT-DOD
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Participant_Dod().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dod());                                                            //Natural: ASSIGN PAR.TWRPARTI-PARTICIPANT-DOD := TWRT-DOD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln1().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN1 := TWRT-NA-LINE ( 2 )
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" ") && ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3).equals(" ")))    //Natural: IF TWRT-NA-LINE ( 2 ) = ' ' AND TWRT-NA-LINE ( 3 ) = ' '
                {
                    ldaTwrl810b.getPar_Twrparti_Addr_Source().setValue(" ");                                                                                              //Natural: ASSIGN PAR.TWRPARTI-ADDR-SOURCE := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl810b.getPar_Twrparti_Addr_Source().setValue("P");                                                                                              //Natural: ASSIGN PAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
                    sub_Report_Address_From_Feeder();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln2().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN2 = TWRT-NA-LINE ( 3 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln2().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN2 := TWRT-NA-LINE ( 3 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln3().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN3 = TWRT-NA-LINE ( 4 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln3().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN3 := TWRT-NA-LINE ( 4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln4().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN4 = TWRT-NA-LINE ( 5 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln4().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN4 := TWRT-NA-LINE ( 5 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln5().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN5 = TWRT-NA-LINE ( 6 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln5().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN5 := TWRT-NA-LINE ( 6 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl810b.getPar_Twrparti_Addr_Ln6().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7))))                                          //Natural: IF PAR.TWRPARTI-ADDR-LN6 = TWRT-NA-LINE ( 7 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810b.getPar_Twrparti_Addr_Ln6().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7));                                                   //Natural: ASSIGN PAR.TWRPARTI-ADDR-LN6 := TWRT-NA-LINE ( 7 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Store_New_Participant_From_Old() throws Exception                                                                                                    //Natural: STORE-NEW-PARTICIPANT-FROM-OLD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        ldaTwrl810c.getVw_newpar().setValuesByName(ldaTwrl810b.getVw_par());                                                                                              //Natural: MOVE BY NAME PAR TO NEWPAR
        ldaTwrl810c.getNewpar_Twrparti_Residency_Type().setValue(pnd_Twrparti_Residency_Type);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-TYPE := #TWRPARTI-RESIDENCY-TYPE
        ldaTwrl810c.getNewpar_Twrparti_Part_Eff_Start_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-START-DTE := TWRT-PAYMT-DTE
        ldaTwrl810c.getNewpar_Twrparti_Part_Eff_End_Dte().setValue("99999999");                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-END-DTE := '99999999'
        ldaTwrl810c.getNewpar_Twrparti_Part_Invrse_End_Dte().setValue(11111111);                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-PART-INVRSE-END-DTE := 11111111
        ldaTwrl810c.getNewpar_Twrparti_Origin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ORIGIN := TWRT-SOURCE
        ldaTwrl810c.getNewpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl810c.getNewpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl810c.getNewpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl810c.getNewpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN NEWPAR.TWRPARTI-LU-TS := *TIMX
        ST1:                                                                                                                                                              //Natural: STORE NEWPAR
        ldaTwrl810c.getVw_newpar().insertDBRow("ST1");
        pnd_Part_Isn.setValue(ldaTwrl810c.getVw_newpar().getAstISN("ST1"));                                                                                               //Natural: ASSIGN #PART-ISN := *ISN ( ST1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Old_Store_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #OLD-STORE-CTR
    }
    private void sub_Store_New_Participant() throws Exception                                                                                                             //Natural: STORE-NEW-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        //*   TWRT-CITIZENSHIP          =  '01'
        //*   TWRT-PYMNT-TAX-FORM-1078  =  'Y'
        if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"NN") || pnd_Twrparti_Residency_Code.equals("OC")))                                                 //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( NN ) OR #TWRPARTI-RESIDENCY-CODE = 'OC'
        {
            if (condition(pnd_Twrparti_Residency_Code.equals("35") && ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                                       //Natural: IF #TWRPARTI-RESIDENCY-CODE = '35' AND TWRT-LOCALITY-CDE = '3E'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                             //Natural: ASSIGN TWRT-LOCALITY-CDE := ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pnd_Twrparti_Residency_Code,"AA")))                                                                                         //Natural: IF #TWRPARTI-RESIDENCY-CODE = MASK ( AA )
            {
                pnd_Multi_Country.setValue(false);                                                                                                                        //Natural: ASSIGN #MULTI-COUNTRY := FALSE
                pnd_Locality_Found.setValue(false);                                                                                                                       //Natural: ASSIGN #LOCALITY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM SELECT-MULTI-COUNTRY-CODES
                sub_Select_Multi_Country_Codes();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Multi_Country.equals(true)))                                                                                                            //Natural: IF #MULTI-COUNTRY = TRUE
                {
                    if (condition(pnd_Locality_Found.equals(true)))                                                                                                       //Natural: IF #LOCALITY-FOUND = TRUE
                    {
                        //*            PRINT 'NEW'
                                                                                                                                                                          //Natural: PERFORM REPORT-LOCALITY-FOUND
                        sub_Report_Locality_Found();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Twrparti_Residency_Code,              //Natural: COMPRESS #TWRPARTI-RESIDENCY-CODE '1' INTO TWRT-LOCALITY-CDE LEAVING NO SPACE
                            "1"));
                        ldaTwrl0702.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                             //Natural: ASSIGN TWRT-FLAG := 'C'
                        pnd_Citation_Message_Code.setValue(37);                                                                                                           //Natural: ASSIGN #CITATION-MESSAGE-CODE := 37
                                                                                                                                                                          //Natural: PERFORM UPDATE-CITATION-TABLE
                        sub_Update_Citation_Table();
                        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-DEFAULT-LOCALITY-NEW
                        sub_Report_Default_Locality_New();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        pnd_Twrparti_Frm1078_Eff_Dte.setValue("00000000");                                                                                                                //Natural: ASSIGN #TWRPARTI-FRM1078-EFF-DTE := '00000000'
        pnd_Twrparti_Frm1001_Eff_Dte.setValue("00000000");                                                                                                                //Natural: ASSIGN #TWRPARTI-FRM1001-EFF-DTE := '00000000'
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                                                                                 //Natural: IF TWRT-PYMNT-TAX-FORM-1078 = 'Y'
        {
            pnd_Twrparti_Frm1078_Eff_Dte.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                                           //Natural: ASSIGN #TWRPARTI-FRM1078-EFF-DTE := TWRT-PAYMT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))                                                                                 //Natural: IF TWRT-PYMNT-TAX-FORM-1001 = 'Y'
        {
            pnd_Twrparti_Frm1001_Eff_Dte.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                                           //Natural: ASSIGN #TWRPARTI-FRM1001-EFF-DTE := TWRT-PAYMT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #TWRPARTI-RESIDENCY-CODE  =  'OC'
        //*     TWRT-CITATION-FLAG      :=  'C'
        //*     #CITATION-MESSAGE-CODE  :=  60
        //*     PERFORM  UPDATE-CITATION-TABLE
        //*  END-IF
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" ") && ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3).equals(" ")))            //Natural: IF TWRT-NA-LINE ( 2 ) = ' ' AND TWRT-NA-LINE ( 3 ) = ' '
        {
            ldaTwrl810c.getNewpar_Twrparti_Addr_Source().setValue(" ");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-SOURCE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810c.getNewpar_Twrparti_Addr_Source().setValue("P");                                                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
            sub_Report_Address_From_Feeder();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl810c.getNewpar_Twrparti_Status().setValue(" ");                                                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-STATUS := ' '
        ldaTwrl810c.getNewpar_Twrparti_Residency_Type().setValue(pnd_Twrparti_Residency_Type);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-TYPE := #TWRPARTI-RESIDENCY-TYPE
        ldaTwrl810c.getNewpar_Twrparti_Tax_Id_Type().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type());                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-TAX-ID-TYPE := TWRT-TAX-ID-TYPE
        ldaTwrl810c.getNewpar_Twrparti_Tax_Id().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-TAX-ID := TWRT-TAX-ID
        ldaTwrl810c.getNewpar_Twrparti_Citation_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Flag());                                                                   //Natural: ASSIGN NEWPAR.TWRPARTI-CITATION-IND := TWRT-FLAG
        ldaTwrl810c.getNewpar_Twrparti_Part_Eff_Start_Dte().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte());                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-START-DTE := TWRT-PAYMT-DTE
        ldaTwrl810c.getNewpar_Twrparti_Part_Eff_End_Dte().setValue("99999999");                                                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-PART-EFF-END-DTE := '99999999'
        ldaTwrl810c.getNewpar_Twrparti_Part_Invrse_End_Dte().setValue(11111111);                                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-PART-INVRSE-END-DTE := 11111111
        ldaTwrl810c.getNewpar_Twrparti_Pin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-PIN := TWRT-PIN-ID
        ldaTwrl810c.getNewpar_Twrparti_Participant_Name().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1));                                                //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-NAME := TWRT-NA-LINE ( 1 )
        ldaTwrl810c.getNewpar_Twrparti_Participant_Dob().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A());                                                               //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-DOB := TWRT-DOB-A
        ldaTwrl810c.getNewpar_Twrparti_Participant_Dod().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dod());                                                                 //Natural: ASSIGN NEWPAR.TWRPARTI-PARTICIPANT-DOD := TWRT-DOD
        ldaTwrl810c.getNewpar_Twrparti_Residency_Code().setValue(pnd_Twrparti_Residency_Code);                                                                            //Natural: ASSIGN NEWPAR.TWRPARTI-RESIDENCY-CODE := #TWRPARTI-RESIDENCY-CODE
        ldaTwrl810c.getNewpar_Twrparti_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde());                                                           //Natural: ASSIGN NEWPAR.TWRPARTI-LOCALITY-CDE := TWRT-LOCALITY-CDE
        ldaTwrl810c.getNewpar_Twrparti_Citizen_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship());                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-CITIZEN-CDE := TWRT-CITIZENSHIP
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln1().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN1 := TWRT-NA-LINE ( 2 )
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln2().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN2 := TWRT-NA-LINE ( 3 )
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln3().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN3 := TWRT-NA-LINE ( 4 )
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln4().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN4 := TWRT-NA-LINE ( 5 )
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln5().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN5 := TWRT-NA-LINE ( 6 )
        ldaTwrl810c.getNewpar_Twrparti_Addr_Ln6().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7));                                                        //Natural: ASSIGN NEWPAR.TWRPARTI-ADDR-LN6 := TWRT-NA-LINE ( 7 )
        ldaTwrl810c.getNewpar_Twrparti_Frm1001_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1001-IND := TWRT-PYMNT-TAX-FORM-1001
        ldaTwrl810c.getNewpar_Twrparti_Frm1001_Eff_Dte().setValue(pnd_Twrparti_Frm1001_Eff_Dte);                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1001-EFF-DTE := #TWRPARTI-FRM1001-EFF-DTE
        ldaTwrl810c.getNewpar_Twrparti_Frm1078_Ind().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078());                                                     //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1078-IND := TWRT-PYMNT-TAX-FORM-1078
        ldaTwrl810c.getNewpar_Twrparti_Frm1078_Eff_Dte().setValue(pnd_Twrparti_Frm1078_Eff_Dte);                                                                          //Natural: ASSIGN NEWPAR.TWRPARTI-FRM1078-EFF-DTE := #TWRPARTI-FRM1078-EFF-DTE
        ldaTwrl810c.getNewpar_Twrparti_Origin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-ORIGIN := TWRT-SOURCE
        ldaTwrl810c.getNewpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl810c.getNewpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl810c.getNewpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN NEWPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl810c.getNewpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN NEWPAR.TWRPARTI-LU-TS := *TIMX
        ST2:                                                                                                                                                              //Natural: STORE NEWPAR
        ldaTwrl810c.getVw_newpar().insertDBRow("ST2");
        pnd_Part_Isn.setValue(ldaTwrl810c.getVw_newpar().getAstISN("ST2"));                                                                                               //Natural: ASSIGN #PART-ISN := *ISN ( ST2. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Store_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #NEW-STORE-CTR
                                                                                                                                                                          //Natural: PERFORM REPORT-NEW-PARTICIPANTS-ADDED
        sub_Report_New_Participants_Added();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Update_Existing_Participant() throws Exception                                                                                                       //Natural: UPDATE-EXISTING-PARTICIPANT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        G3:                                                                                                                                                               //Natural: GET UPDPAR *ISN ( RL1. )
        ldaTwrl810d.getVw_updpar().readByID(ldaTwrl810b.getVw_par().getAstISN("RL1"), "G3");
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Pin().equals(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id())))                                                             //Natural: IF UPDPAR.TWRPARTI-PIN = TWRT-PIN-ID
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Pin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                      //Natural: ASSIGN UPDPAR.TWRPARTI-PIN := TWRT-PIN-ID
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Participant_Name().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1))))                                   //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-NAME = TWRT-NA-LINE ( 1 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Participant_Name().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1));                                            //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-NAME := TWRT-NA-LINE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Participant_Dob().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A())))                                                  //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-DOB = TWRT-DOB-A
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Participant_Dob().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dob_A());                                                           //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-DOB := TWRT-DOB-A
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Participant_Dod().equals(ldaTwrl0702.getTwrt_Record_Twrt_Dod())))                                                    //Natural: IF UPDPAR.TWRPARTI-PARTICIPANT-DOD = TWRT-DOD
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Participant_Dod().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Dod());                                                             //Natural: ASSIGN UPDPAR.TWRPARTI-PARTICIPANT-DOD := TWRT-DOD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln1().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN1 = TWRT-NA-LINE ( 2 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln1().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN1 := TWRT-NA-LINE ( 2 )
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(2).equals(" ") && ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3).equals(" ")))        //Natural: IF TWRT-NA-LINE ( 2 ) = ' ' AND TWRT-NA-LINE ( 3 ) = ' '
            {
                ldaTwrl810d.getUpdpar_Twrparti_Addr_Source().setValue(" ");                                                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-SOURCE := ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl810d.getUpdpar_Twrparti_Addr_Source().setValue("P");                                                                                               //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-SOURCE := 'P'
                                                                                                                                                                          //Natural: PERFORM REPORT-ADDRESS-FROM-FEEDER
                sub_Report_Address_From_Feeder();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln2().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN2 = TWRT-NA-LINE ( 3 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln2().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(3));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN2 := TWRT-NA-LINE ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln3().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN3 = TWRT-NA-LINE ( 4 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln3().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(4));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN3 := TWRT-NA-LINE ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln4().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN4 = TWRT-NA-LINE ( 5 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln4().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(5));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN4 := TWRT-NA-LINE ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln5().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN5 = TWRT-NA-LINE ( 6 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln5().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(6));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN5 := TWRT-NA-LINE ( 6 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln6().equals(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7))))                                           //Natural: IF UPDPAR.TWRPARTI-ADDR-LN6 = TWRT-NA-LINE ( 7 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl810d.getUpdpar_Twrparti_Addr_Ln6().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(7));                                                    //Natural: ASSIGN UPDPAR.TWRPARTI-ADDR-LN6 := TWRT-NA-LINE ( 7 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl810d.getUpdpar_Twrparti_Origin().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-ORIGIN := TWRT-SOURCE
        ldaTwrl810d.getUpdpar_Twrparti_Updt_User().setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-USER := *INIT-USER
        ldaTwrl810d.getUpdpar_Twrparti_Updt_Dte().setValue(Global.getDATN());                                                                                             //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-DTE := *DATN
        ldaTwrl810d.getUpdpar_Twrparti_Updt_Time().setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                       //Natural: ASSIGN UPDPAR.TWRPARTI-UPDT-TIME := #TIMN-A
        ldaTwrl810d.getUpdpar_Twrparti_Lu_Ts().setValue(Global.getTIMX());                                                                                                //Natural: ASSIGN UPDPAR.TWRPARTI-LU-TS := *TIMX
        ldaTwrl810d.getVw_updpar().updateDBRow("G3");                                                                                                                     //Natural: UPDATE ( G3. )
        pnd_Part_Isn.setValue(ldaTwrl810d.getVw_updpar().getAstISN("G3"));                                                                                                //Natural: ASSIGN #PART-ISN := *ISN ( G3. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Update_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #UPDATE-CTR
    }
    private void sub_Cite_Participant_Record() throws Exception                                                                                                           //Natural: CITE-PARTICIPANT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        G1:                                                                                                                                                               //Natural: GET OLDPAR *ISN ( RL1. )
        vw_oldpar.readByID(ldaTwrl810b.getVw_par().getAstISN("RL1"), "G1");
        oldpar_Twrparti_Citation_Ind.setValue("C");                                                                                                                       //Natural: ASSIGN OLDPAR.TWRPARTI-CITATION-IND := 'C'
        vw_oldpar.updateDBRow("G1");                                                                                                                                      //Natural: UPDATE ( G1. )
        pnd_Et_Ctr.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CTR
        if (condition(pnd_Et_Ctr.greater(pnd_Et_Const)))                                                                                                                  //Natural: IF #ET-CTR > #ET-CONST
        {
            pnd_Et_Ctr.setValue(0);                                                                                                                                       //Natural: ASSIGN #ET-CTR := 0
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Residency_Type() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrparti_Residency_Type.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := ' '
        //*  CANADA
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("CA")))                                                                                       //Natural: IF TWRT-STATE-RSDNCY = 'CA'
        {
            pnd_Twrparti_Residency_Type.setValue("4");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '4'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMONWEALTH OF PUERTO RICO
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ"))) //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
        {
            pnd_Twrparti_Residency_Type.setValue("3");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROVINCE OF CANADA
        if (condition(((ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("74") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("88"))          //Natural: IF TWRT-STATE-RSDNCY = '74' THRU '88' OR TWRT-STATE-RSDNCY = '96'
            || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("96"))))
        {
            pnd_Twrparti_Residency_Type.setValue("5");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '5'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  STATE
        if (condition((((ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("00") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("41"))         //Natural: IF TWRT-STATE-RSDNCY = '00' THRU '41' OR TWRT-STATE-RSDNCY = '43' THRU '57' OR TWRT-STATE-RSDNCY = '97' OR = 'US'
            || (ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("43") && ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("57"))) 
            || (ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("97") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("US")))))
        {
            pnd_Twrparti_Residency_Type.setValue("1");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '1'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COUNTRY
        if (condition(DbsUtil.maskMatches(ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"AA") || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("98")             //Natural: IF TWRT-STATE-RSDNCY = MASK ( AA ) OR = '98' OR = '99'
            || ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy().equals("99")))
        {
            pnd_Twrparti_Residency_Type.setValue("2");                                                                                                                    //Natural: ASSIGN #TWRPARTI-RESIDENCY-TYPE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Residency_Code() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Addrss_Geographic_Cde.setValue(" ");                                                                                                                          //Natural: ASSIGN #ADDRSS-GEOGRAPHIC-CDE := ' '
        if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Citizenship().equals("01") || ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                   //Natural: IF TWRT-CITIZENSHIP = '01' OR TWRT-PYMNT-TAX-FORM-1078 = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-AND-ADDRESS
            sub_Lookup_Name_And_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Naa_Found.equals(true)))                                                                                                                    //Natural: IF #NAA-FOUND = TRUE
            {
                if (condition(DbsUtil.maskMatches(pnd_Addrss_Geographic_Cde,"AA")))                                                                                       //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = MASK ( AA )
                {
                    pnd_Country_Found.setValue(false);                                                                                                                    //Natural: ASSIGN #COUNTRY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-COUNTRY-CODE
                    sub_Lookup_Country_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pnd_Country_Found.equals(true)))                                                                                                        //Natural: IF #COUNTRY-FOUND = TRUE
                    {
                        pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                  //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                        pnd_Residency_Found.setValue(true);                                                                                                               //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                        sub_Report_Converted_Residency_Code();
                        if (condition(Global.isEscape())) {return;}
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                      //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                    pnd_Residency_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                    sub_Report_Converted_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-NAME-AND-ADDRESS
            sub_Lookup_Name_And_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Naa_Found.equals(true)))                                                                                                                    //Natural: IF #NAA-FOUND = TRUE
            {
                if (condition(pnd_Addrss_Geographic_Cde.greaterOrEqual("74") && pnd_Addrss_Geographic_Cde.lessOrEqual("88")))                                             //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = '74' THRU '88'
                {
                    pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                                      //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                    pnd_Residency_Found.setValue(true);                                                                                                                   //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                    sub_Report_Converted_Residency_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(DbsUtil.maskMatches(pnd_Addrss_Geographic_Cde,"AA")))                                                                                   //Natural: IF #ADDRSS-GEOGRAPHIC-CDE = MASK ( AA )
                    {
                        pnd_Country_Found.setValue(false);                                                                                                                //Natural: ASSIGN #COUNTRY-FOUND := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-COUNTRY-CODE
                        sub_Lookup_Country_Code();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(pnd_Country_Found.equals(true)))                                                                                                    //Natural: IF #COUNTRY-FOUND = TRUE
                        {
                            pnd_Twrparti_Residency_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                              //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := #ADDRSS-GEOGRAPHIC-CDE
                            pnd_Residency_Found.setValue(true);                                                                                                           //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-CONVERTED-RESIDENCY-CODE
                            sub_Report_Converted_Residency_Code();
                            if (condition(Global.isEscape())) {return;}
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Twrparti_Residency_Code.setValue("OC");                                                                                                                   //Natural: ASSIGN #TWRPARTI-RESIDENCY-CODE := 'OC'
            pnd_Residency_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #RESIDENCY-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM REPORT-OC-RESIDENCY-CODE
            sub_Report_Oc_Residency_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Name_And_Address() throws Exception                                                                                                           //Natural: LOOKUP-NAME-AND-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                         //Natural: ASSIGN #PH-UNQUE-ID-NMBR := TWRT-PIN-ID
        pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind.setValue("B");                                                                                                             //Natural: ASSIGN #PH-BSE-ADDRSS-IND := 'B'
        //*  NA1.              /* FE201506 START COMMENT OUT
        //*  READ (1)   NAA   WITH   PIN-BASE-KEY  =  #PIN-BASE-KEY
        //*   IF PH-UNQUE-ID-NMBR  (NA1.)  =  #PH-UNQUE-ID-NMBR-N
        //*       AND PH-BSE-ADDRSS-IND (NA1.)  =  'B'
        //*     IF ADDRSS-GEOGRAPHIC-CDE (NA1.)  =  '  '  OR =  '00'  OR =  '96'
        //*         OR =  '97'  OR =  '98'  OR =  '99'
        //*       IGNORE
        //*     ELSE
        //*       #NAA-FOUND  :=  TRUE
        //*       #ADDRSS-GEOGRAPHIC-CDE  :=  ADDRSS-GEOGRAPHIC-CDE (NA1.)
        //*     END-IF
        //*     ESCAPE ROUTINE
        //*   ELSE
        //*     #PH-BSE-ADDRSS-IND  :=  ' '
        //*     NA2.
        //*     READ (1)   NAA   WITH   PIN-CNTRCT-KEY  =  #PIN-BASE-KEY
        //*       IF PH-UNQUE-ID-NMBR (NA2.)  =  #PH-UNQUE-ID-NMBR-N
        //*         IF ADDRSS-GEOGRAPHIC-CDE (NA2.)  =  '  '  OR =  '00'
        //*             OR =  '96'  OR =  '97'
        //*             OR =  '98'  OR =  '99'
        //*           IGNORE
        //*         ELSE
        //*           #NAA-FOUND  :=  TRUE
        //*           #ADDRSS-GEOGRAPHIC-CDE  :=  ADDRSS-GEOGRAPHIC-CDE (NA2.)
        //*         END-IF
        //*         ESCAPE ROUTINE
        //*       END-IF
        //*     END-READ
        //*   END-IF
        //*  END-READ                 /* FE201506 END
        //*                       /* FE201506 START
        //*  RESET #MDMA100
        //*   PIN EXP  08/2017
        //*   PIN EXP 08/2017
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_A12().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                     //Natural: ASSIGN #MDMA101.#I-PIN-A12 := TWRT-PIN-ID
        //*  CALLNAT 'MDMN100A' #MDMA100
        //*   PIN EXP 08/2017
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  IF #MDMA100.#O-RETURN-CODE EQ '0000'
        //*   PIN EXP  08/2017
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            //*  IF #MDMA100.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = '00' OR = '96'
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("00") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("96")  //Natural: IF #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = '00' OR = '96' OR = '97' OR = '98' OR = '99'
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("97") || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("98") 
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals("99")))
            {
                ignore();
                //*   PIN EXP 08/2017
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Naa_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #NAA-FOUND := TRUE
                pnd_Addrss_Geographic_Cde.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code());                                                       //Natural: ASSIGN #ADDRSS-GEOGRAPHIC-CDE := #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201506 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Country_Code() throws Exception                                                                                                               //Natural: LOOKUP-COUNTRY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr.setValue("3");                                                                                                    //Natural: ASSIGN #COUNTRY-TBL-NBR := '3'
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year());                                                            //Natural: ASSIGN #COUNTRY-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code.setValue(pnd_Addrss_Geographic_Cde);                                                                           //Natural: ASSIGN #COUNTRY-ALPHA-CODE := #ADDRSS-GEOGRAPHIC-CDE
        vw_country.createHistogram                                                                                                                                        //Natural: HISTOGRAM ( 1 ) COUNTRY TIRCNTL-NBR-YEAR-ALPHA-CDE STARTING FROM #COUNTRY-3-YR-ALPHA-CODE
        (
        "HIST01",
        "TIRCNTL_NBR_YEAR_ALPHA_CDE",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_ALPHA_CDE", ">=", pnd_Country_3_Yr_Alpha_Code, WcType.WITH) },
        1
        );
        HIST01:
        while (condition(vw_country.readNextRow("HIST01")))
        {
            if (condition(country_Country_Tbl_Nbr.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tbl_Nbr) && country_Country_Tax_Year.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Tax_Year)  //Natural: IF COUNTRY-TBL-NBR = #COUNTRY-TBL-NBR AND COUNTRY-TAX-YEAR = #COUNTRY-TAX-YEAR AND COUNTRY-ALPHA-CODE = #COUNTRY-ALPHA-CODE
                && country_Country_Alpha_Code.equals(pnd_Country_3_Yr_Alpha_Code_Pnd_Country_Alpha_Code)))
            {
                pnd_Country_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #COUNTRY-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
    }
    private void sub_Update_Citation_Table() throws Exception                                                                                                             //Natural: UPDATE-CITATION-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR Z = 1 TO 60
        for (z.setValue(1); condition(z.lessOrEqual(60)); z.nadd(1))
        {
            if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).equals(getZero())))                                                                   //Natural: IF TWRT-ERROR-TABLE ( Z ) = 0
            {
                ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(z).setValue(pnd_Citation_Message_Code);                                                            //Natural: ASSIGN TWRT-ERROR-TABLE ( Z ) := #CITATION-MESSAGE-CODE
                pnd_Citation_Message_Code.setValue(0);                                                                                                                    //Natural: ASSIGN #CITATION-MESSAGE-CODE := 0
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Select_Multi_Country_Codes() throws Exception                                                                                                        //Natural: SELECT-MULTI-COUNTRY-CODES
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(" ");                                                                                                     //Natural: ASSIGN TWRT-LOCALITY-CDE := ' '
        pnd_Multi_Country.setValue(false);                                                                                                                                //Natural: ASSIGN #MULTI-COUNTRY := FALSE
        pnd_Locality_Found.setValue(false);                                                                                                                               //Natural: ASSIGN #LOCALITY-FOUND := FALSE
        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        FOR02:                                                                                                                                                            //Natural: FOR W = 1 TO 12
        for (w.setValue(1); condition(w.lessOrEqual(12)); w.nadd(1))
        {
            if (condition(pnd_Twrparti_Residency_Code.equals(pnd_Country_Init_Pnd_Country.getValue(w))))                                                                  //Natural: IF #TWRPARTI-RESIDENCY-CODE = #COUNTRY ( W )
            {
                pnd_Multi_Country.setValue(true);                                                                                                                         //Natural: ASSIGN #MULTI-COUNTRY := TRUE
                //*  09/28/11
                //*  RC01
                //*  VIKRAM2
                if (condition(ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("SS") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("NZ") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("DC")  //Natural: IF TWRT-SOURCE = 'SS' OR = 'NZ' OR = 'DC' OR = 'AP' OR = 'SI' OR = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL'
                    || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("AP") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("SI") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("OP") 
                    || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("NV") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("AM") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("EW") 
                    || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("VL")))
                {
                    ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Annt_Locality_Code());                                        //Natural: ASSIGN TWRT-LOCALITY-CDE := TWRT-ANNT-LOCALITY-CODE
                    pnd_Locality_Found.setValue(true);                                                                                                                    //Natural: ASSIGN #LOCALITY-FOUND := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-LAST-ADDRESS-LINE
                sub_Lookup_Last_Address_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Naa_Found.equals(false) || pnd_Twrparti_Country_Desc.equals(" ")))                                                                      //Natural: IF #NAA-FOUND = FALSE OR #TWRPARTI-COUNTRY-DESC = ' '
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR03:                                                                                                                                                //Natural: FOR X = 1 TO 39
                    for (x.setValue(1); condition(x.lessOrEqual(39)); x.nadd(1))
                    {
                        if (condition(pnd_Twrparti_Country_Desc.contains (ldaTwrl810e.getPnd_Locality_Table_Pnd_Local_Name ().getValue(x))))                              //Natural: IF #TWRPARTI-COUNTRY-DESC = SCAN #LOCAL-NAME ( X )
                        {
                            pnd_Locality_Found.setValue(true);                                                                                                            //Natural: ASSIGN #LOCALITY-FOUND := TRUE
                            ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde().setValue(ldaTwrl810e.getPnd_Locality_Table_Pnd_Local_Code().getValue(x));                      //Natural: ASSIGN TWRT-LOCALITY-CDE := #LOCAL-CODE ( X )
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Lookup_Last_Address_Line() throws Exception                                                                                                          //Natural: LOOKUP-LAST-ADDRESS-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naa_Found.setValue(false);                                                                                                                                    //Natural: ASSIGN #NAA-FOUND := FALSE
        pnd_Twrparti_Country_Desc.setValue(" ");                                                                                                                          //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := ' '
        pnd_Pin_Base_Key_Pnd_Ph_Unque_Id_Nmbr.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                         //Natural: ASSIGN #PH-UNQUE-ID-NMBR := TWRT-PIN-ID
        pnd_Pin_Base_Key_Pnd_Ph_Bse_Addrss_Ind.setValue("B");                                                                                                             //Natural: ASSIGN #PH-BSE-ADDRSS-IND := 'B'
        //*  NA3.
        //*  READ (1)   NAA   WITH   PIN-BASE-KEY  =  #PIN-BASE-KEY
        //*   IF PH-UNQUE-ID-NMBR      (NA3.)  =  #PH-UNQUE-ID-NMBR-N
        //*       AND PH-BSE-ADDRSS-IND     (NA3.)  =  'B'
        //*       AND ADDRSS-GEOGRAPHIC-CDE (NA3.)  =  #TWRPARTI-RESIDENCY-CODE
        //*  INT '=' ADDRSS-GEOGRAPHIC-CDE (NA3.)  '='  #TWRPARTI-RESIDENCY-CODE
        //*      'ONE'
        //*     #NAA-FOUND  :=  TRUE
        //*                       /* FE201506 START
        //*  RESET #MDMA100
        //*  #MDMA100.#I-PIN-A7 := TWRT-PIN-ID
        //*  CALLNAT 'MDMN100A' #MDMA100
        //*  IF #MDMA100.#O-RETURN-CODE EQ '0000'
        //*  ND #MDMA100.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = #TWRPARTI-RESIDENCY-CODE
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_A12().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id());                                                                     //Natural: ASSIGN #MDMA101.#I-PIN-A12 := TWRT-PIN-ID
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000") && pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().equals(pnd_Twrparti_Residency_Code))) //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000' AND #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = #TWRPARTI-RESIDENCY-CODE
        {
            pnd_Naa_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #NAA-FOUND := TRUE
            //*    IF ADDRSS-LNE-6 (NA3.)  =  ' '
            //*      IGNORE
            //*    ELSE
            //*      #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-6 (NA3.)
            //*      ESCAPE ROUTINE
            //*    END-IF
            //*    IF ADDRSS-LNE-5 (NA3.)  =  ' '
            //*      IGNORE
            //*    ELSE
            //*      #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-5 (NA3.)
            //*      ESCAPE ROUTINE
            //*    END-IF
            //*    IF #MDMA100.#O-BASE-ADDRESS-LINE-4 = ' '
            //*  PIN EXP  08/2017
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-4 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #MDMA101.#O-BASE-ADDRESS-LINE-4
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*    IF #MDMA100.#O-BASE-ADDRESS-LINE-3 = ' '
            //*   PIN EXP 08/2017
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-3 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #MDMA101.#O-BASE-ADDRESS-LINE-3
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*    IF #MDMA100.#O-BASE-ADDRESS-LINE-2 = ' '
            //*   PIN EXP  08/2017
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-2 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #MDMA101.#O-BASE-ADDRESS-LINE-2
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*    IF #MDMA100.#O-BASE-ADDRESS-LINE-1 = ' '
            //*   PIN EXP 08/2017
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1().equals(" ")))                                                                             //Natural: IF #MDMA101.#O-BASE-ADDRESS-LINE-1 = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrparti_Country_Desc.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                //Natural: ASSIGN #TWRPARTI-COUNTRY-DESC := #MDMA101.#O-BASE-ADDRESS-LINE-1
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELSE
        //*    #PH-BSE-ADDRSS-IND  :=  ' '
        //*    NA4.
        //*    READ (1)   NAA   WITH   PIN-CNTRCT-KEY  =  #PIN-BASE-KEY
        //*      IF PH-UNQUE-ID-NMBR      (NA4.)  =  #PH-UNQUE-ID-NMBR-N
        //*          AND ADDRSS-GEOGRAPHIC-CDE (NA4.)  =  #TWRPARTI-RESIDENCY-CODE
        //*  INT '=' ADDRSS-GEOGRAPHIC-CDE (NA4.)  '='  #TWRPARTI-RESIDENCY-CODE
        //*      'TWO'
        //*        #NAA-FOUND  :=  TRUE
        //*        IF ADDRSS-LNE-6 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-6 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-5 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-5 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-4 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-4 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-3 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-3 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-2 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-2 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*        IF ADDRSS-LNE-1 (NA4.)  =  ' '
        //*          IGNORE
        //*        ELSE
        //*          #TWRPARTI-COUNTRY-DESC  :=  ADDRSS-LNE-1 (NA4.)
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*      END-IF
        //*    END-READ
        //*  END-IF
        //*  END-READ   /* FE201506 END COMMENT OUT
    }
    private void sub_Report_Tax_Transaction_Read() throws Exception                                                                                                       //Natural: REPORT-TAX-TRANSACTION-READ
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------
        getReports().display(1, "T/C",                                                                                                                                    //Natural: DISPLAY ( 01 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Tax_Transaction_Bypassed() throws Exception                                                                                                   //Natural: REPORT-TAX-TRANSACTION-BYPASSED
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------------
        getReports().display(2, "T/C",                                                                                                                                    //Natural: DISPLAY ( 02 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_New_Participants_Added() throws Exception                                                                                                     //Natural: REPORT-NEW-PARTICIPANTS-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        getReports().display(3, "T/C",                                                                                                                                    //Natural: DISPLAY ( 03 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Retroactive_Date_Bypass() throws Exception                                                                                                    //Natural: REPORT-RETROACTIVE-DATE-BYPASS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------------
        getReports().display(4, "T/C",                                                                                                                                    //Natural: DISPLAY ( 04 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Duplicate_Date_Bypass() throws Exception                                                                                                      //Natural: REPORT-DUPLICATE-DATE-BYPASS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        getReports().display(5, "T/C",                                                                                                                                    //Natural: DISPLAY ( 05 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
        getReports().print(5, "Citation Error Codes ...",ldaTwrl0702.getTwrt_Record_Twrt_Error_Table().getValue(1,":",30), new ReportEditMask ("ZZ"));                    //Natural: PRINT ( 05 ) 'Citation Error Codes ...' TWRT-ERROR-TABLE ( 1:30 ) ( EM = ZZ )
    }
    private void sub_Report_Form1001_Y_To_N_Added() throws Exception                                                                                                      //Natural: REPORT-FORM1001-Y-TO-N-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        getReports().display(6, "T/C",                                                                                                                                    //Natural: DISPLAY ( 06 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Forms_Cited_Added() throws Exception                                                                                                          //Natural: REPORT-FORMS-CITED-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        getReports().display(7, "T/C",                                                                                                                                    //Natural: DISPLAY ( 07 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Forms_Added() throws Exception                                                                                                                //Natural: REPORT-FORMS-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        getReports().display(8, "T/C",                                                                                                                                    //Natural: DISPLAY ( 08 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Oc_Residency_Code() throws Exception                                                                                                          //Natural: REPORT-OC-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        pnd_Oc_Residency_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #OC-RESIDENCY-CTR
        getReports().display(9, "T/C",                                                                                                                                    //Natural: DISPLAY ( 09 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Converted_Residency_Code() throws Exception                                                                                                   //Natural: REPORT-CONVERTED-RESIDENCY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------------
        pnd_Conv_Resdency_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CONV-RESDENCY-CTR
        getReports().display(10, "T/C",                                                                                                                                   //Natural: DISPLAY ( 10 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) ( AL = 30 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'RS/D2' #TWRPARTI-RESIDENCY-CODE 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1), new AlphanumericLength (30),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"RS/D2",
        		pnd_Twrparti_Residency_Code,"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Address_From_Feeder() throws Exception                                                                                                        //Natural: REPORT-ADDRESS-FROM-FEEDER
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------
        pnd_Feed_Address_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #FEED-ADDRESS-CTR
        getReports().display(11, "T/C",                                                                                                                                   //Natural: DISPLAY ( 11 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Form1001_Y_To_N_Bypass() throws Exception                                                                                                     //Natural: REPORT-FORM1001-Y-TO-N-BYPASS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        getReports().display(12, "T/C",                                                                                                                                   //Natural: DISPLAY ( 12 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        //*        'D-O-D'     TWRT-DOD    (EM=XXXX/XX/XX)
    }
    private void sub_Report_Locality_Found() throws Exception                                                                                                             //Natural: REPORT-LOCALITY-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_Country_Found_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTRY-FOUND-CTR
        getReports().display(13, "T/C",                                                                                                                                   //Natural: DISPLAY ( 13 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
        getReports().print(13, pnd_Twrparti_Country_Desc);                                                                                                                //Natural: PRINT ( 13 ) #TWRPARTI-COUNTRY-DESC
    }
    private void sub_Report_Default_Locality_New() throws Exception                                                                                                       //Natural: REPORT-DEFAULT-LOCALITY-NEW
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------
        pnd_Default_New_Ctr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DEFAULT-NEW-CTR
        getReports().display(14, "T/C",                                                                                                                                   //Natural: DISPLAY ( 14 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
    }
    private void sub_Report_Default_Locality_File() throws Exception                                                                                                      //Natural: REPORT-DEFAULT-LOCALITY-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        pnd_Default_File_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #DEFAULT-FILE-CTR
        getReports().display(15, "T/C",                                                                                                                                   //Natural: DISPLAY ( 15 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
    }
    private void sub_Report_Locality_Not_Found() throws Exception                                                                                                         //Natural: REPORT-LOCALITY-NOT-FOUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Country_Not_Found.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTRY-NOT-FOUND
        getReports().display(16, "T/C",                                                                                                                                   //Natural: DISPLAY ( 16 ) 'T/C' TWRT-COMPANY-CDE 'SR/CE' TWRT-SOURCE 'Contract' TWRT-CNTRCT-NBR 'PY/EE' TWRT-PAYEE-CDE 'Tax ID' TWRT-TAX-ID 'T/Y' TWRT-TAX-ID-TYPE 'Pin ID' TWRT-PIN-ID 'Name' TWRT-NA-LINE ( 1 ) 'Payment' TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) 'P/Y' TWRT-PAYMT-TYPE 'S/E' TWRT-SETTL-TYPE 'RS/DN' TWRT-STATE-RSDNCY 'Loc' TWRT-LOCALITY-CDE 'CI/TI' TWRT-CITIZENSHIP 'Tax/Year' TWRT-TAX-YEAR 'Ro/ll' TWRT-ROLLOVER-IND 'Ci/ta' TWRT-FLAG 'D-O-B' TWRT-DOB-A ( EM = XXXX/XX/XX ) '10/78' TWRT-PYMNT-TAX-FORM-1078 '10/01' TWRT-PYMNT-TAX-FORM-1001
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line().getValue(1),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        if (Global.isEscape()) return;
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, new TabSetting(1),"Tax Transaction Records Read...............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                            //Natural: WRITE ( 00 ) 01T 'Tax Transaction Records Read...............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Tax Transaction Records Bypassed...........",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 00 ) / 01T 'Tax Transaction Records Bypassed...........' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Duplicate Payment Dates Bypassed...........",pnd_Duplicate_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Duplicate Payment Dates Bypassed...........' #DUPLICATE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Retroactive Payment Records Bypassed.......",pnd_Retroactive_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 00 ) / 01T 'Retroactive Payment Records Bypassed.......' #RETROACTIVE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Form 1001 From 'Y' To 'N' Part. Added......",pnd_F1001_Y_To_N_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Form 1001 From "Y" To "N" Part. Added......' #F1001-Y-TO-N-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Added (First Time)............",pnd_New_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Participants Added (First Time)............' #NEW-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Added (Already On File).......",pnd_Old_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 00 ) / 01T 'Participants Added (Already On File).......' #OLD-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants Updated.......................",pnd_Update_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 00 ) / 01T 'Participants Updated.......................' #UPDATE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Forms Cited & Added (Already On File)......",pnd_Forms_Cited_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 00 ) / 01T 'Forms Cited & Added (Already On File)......' #FORMS-CITED-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Forms Reported & Added (Already On File)...",pnd_Report_Forms_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Forms Reported & Added (Already On File)...' #REPORT-FORMS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With 'OC' Residency Code......",pnd_Oc_Residency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Participants With "OC" Residency Code......' #OC-RESIDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With Converted Residency......",pnd_Conv_Resdency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Participants With Converted Residency......' #CONV-RESDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Participants With Feeder Address...........",pnd_Feed_Address_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Participants With Feeder Address...........' #FEED-ADDRESS-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Form 1001 From 'Y' To 'N' Part. Cited......",pnd_F1001_Y_To_N_Ctr2, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Form 1001 From "Y" To "N" Part. Cited......' #F1001-Y-TO-N-CTR2
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Found..........",pnd_Country_Found_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Found..........' #COUNTRY-FOUND-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default New....",pnd_Default_New_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Default New....' #DEFAULT-NEW-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default File...",pnd_Default_File_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Default File...' #DEFAULT-FILE-CTR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Not Found......",pnd_Country_Not_Found, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 00 ) / 01T 'Multi-Country Locality Code Not Found......' #COUNTRY-NOT-FOUND
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(1),"Reversals Found (Participant Not Updated)..",pnd_Reversal_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                //Natural: WRITE ( 00 ) / 01T 'Reversals Found (Participant Not Updated)..' #REVERSAL-CTR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,new TabSetting(1),"Tax Transaction Records Read...............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                    //Natural: WRITE ( 01 ) / 01T 'Tax Transaction Records Read...............' #READ-CTR
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,new TabSetting(1),"Tax Transaction Records Bypassed...........",pnd_Bypass_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));                  //Natural: WRITE ( 02 ) / 01T 'Tax Transaction Records Bypassed...........' #BYPASS-CTR
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,new TabSetting(1),"Participants Added (First Time)............",pnd_New_Store_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 03 ) / 01T 'Participants Added (First Time)............' #NEW-STORE-CTR
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,new TabSetting(1),"Retroactive Payment Records Bypassed.......",pnd_Retroactive_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 04 ) / 01T 'Retroactive Payment Records Bypassed.......' #RETROACTIVE-CTR
        if (Global.isEscape()) return;
        getReports().write(5, NEWLINE,new TabSetting(1),"Duplicate Payment Dates Bypassed...........",pnd_Duplicate_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));               //Natural: WRITE ( 05 ) / 01T 'Duplicate Payment Dates Bypassed...........' #DUPLICATE-CTR
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,new TabSetting(1),"Form 1001 From 'Y' To 'N' Part. Added......",pnd_F1001_Y_To_N_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 06 ) / 01T 'Form 1001 From "Y" To "N" Part. Added......' #F1001-Y-TO-N-CTR
        if (Global.isEscape()) return;
        getReports().write(7, NEWLINE,new TabSetting(1),"Forms Cited & Added (Already On File)......",pnd_Forms_Cited_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));             //Natural: WRITE ( 07 ) / 01T 'Forms Cited & Added (Already On File)......' #FORMS-CITED-CTR
        if (Global.isEscape()) return;
        getReports().write(8, NEWLINE,new TabSetting(1),"Forms Reported & Added (Already On File)...",pnd_Report_Forms_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 08 ) / 01T 'Forms Reported & Added (Already On File)...' #REPORT-FORMS-CTR
        if (Global.isEscape()) return;
        getReports().write(9, NEWLINE,new TabSetting(1),"Participants With 'OC' Residency Code......",pnd_Oc_Residency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 09 ) / 01T 'Participants With "OC" Residency Code......' #OC-RESIDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(10, NEWLINE,new TabSetting(1),"Participants With Converted Residency......",pnd_Conv_Resdency_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));          //Natural: WRITE ( 10 ) / 01T 'Participants With Converted Residency......' #CONV-RESDENCY-CTR
        if (Global.isEscape()) return;
        getReports().write(11, NEWLINE,new TabSetting(1),"Participants With Feeder Address...........",pnd_Feed_Address_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 11 ) / 01T 'Participants With Feeder Address...........' #FEED-ADDRESS-CTR
        if (Global.isEscape()) return;
        getReports().write(12, NEWLINE,new TabSetting(1),"Form 1001 From 'Y' To 'N' Part. Cited......",pnd_F1001_Y_To_N_Ctr2, new ReportEditMask ("Z,ZZZ,ZZ9"));          //Natural: WRITE ( 12 ) / 01T 'Form 1001 From "Y" To "N" Part. Cited......' #F1001-Y-TO-N-CTR2
        if (Global.isEscape()) return;
        getReports().write(13, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Found..........",pnd_Country_Found_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));          //Natural: WRITE ( 13 ) / 01T 'Multi-Country Locality Code Found..........' #COUNTRY-FOUND-CTR
        if (Global.isEscape()) return;
        getReports().write(14, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default New....",pnd_Default_New_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));            //Natural: WRITE ( 14 ) / 01T 'Multi-Country Locality Code Default New....' #DEFAULT-NEW-CTR
        if (Global.isEscape()) return;
        getReports().write(15, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Default File...",pnd_Default_File_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));           //Natural: WRITE ( 15 ) / 01T 'Multi-Country Locality Code Default File...' #DEFAULT-FILE-CTR
        if (Global.isEscape()) return;
        getReports().write(16, NEWLINE,new TabSetting(1),"Multi-Country Locality Code Not Found......",pnd_Country_Not_Found, new ReportEditMask ("Z,ZZZ,ZZ9"));          //Natural: WRITE ( 16 ) / 01T 'Multi-Country Locality Code Not Found......' #COUNTRY-NOT-FOUND
        if (Global.isEscape()) return;
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=133 ZP=ON");
        Global.format(4, "PS=60 LS=133 ZP=ON");
        Global.format(5, "PS=60 LS=133 ZP=ON");
        Global.format(6, "PS=60 LS=133 ZP=ON");
        Global.format(7, "PS=60 LS=133 ZP=ON");
        Global.format(8, "PS=60 LS=133 ZP=ON");
        Global.format(9, "PS=60 LS=133 ZP=ON");
        Global.format(10, "PS=60 LS=133 ZP=ON");
        Global.format(11, "PS=60 LS=133 ZP=ON");
        Global.format(12, "PS=60 LS=133 ZP=ON");
        Global.format(13, "PS=60 LS=133 ZP=ON");
        Global.format(14, "PS=60 LS=133 ZP=ON");
        Global.format(15, "PS=60 LS=133 ZP=ON");
        Global.format(16, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(52),"Tax Transactions Records Read",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Tax Transactions Records Bypassed",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"Participants Added For The First Time",new TabSetting(120),"REPORT: RPT3",NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Retroactive Participants Bypassed",new TabSetting(120),"REPORT: RPT4",NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"Participants Duplicate Date Bypassed",new TabSetting(120),"REPORT: RPT5",NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participants Added Form 1001 From 'Y' to 'N'",new TabSetting(120),"REPORT: RPT6",NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"Participants Added Forms Cited",new TabSetting(120),"REPORT: RPT7",NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Participants Added Forms Report",new TabSetting(120),"REPORT: RPT8",NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"Participants 'OC' Residency Code",new TabSetting(120),"REPORT: RPT9",NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participants Converted Residency Code",new TabSetting(120),"REPORT: RPT10",NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"Participants Address From Feeder",new TabSetting(120),"REPORT: RPT11",NEWLINE,NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participants Cited Form 1001 From 'Y' to 'N'",new TabSetting(120),"REPORT: RPT12",NEWLINE,NEWLINE);
        getReports().write(13, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(13), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"Participant Country Locality Code Found",new TabSetting(120),"REPORT: RPT13",NEWLINE,NEWLINE);
        getReports().write(14, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(14), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Participant Country Locality Default New",new TabSetting(120),"REPORT: RPT14",NEWLINE,NEWLINE);
        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"Participant File Country Locality Default",new TabSetting(120),"REPORT: RPT15",NEWLINE,NEWLINE);
        getReports().write(16, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(16), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"Participant Country Locality Code NOT Found",new TabSetting(120),"REPORT: RPT16");

        getReports().setDisplayColumns(1, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(2, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(3, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(4, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(5, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(6, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(7, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(8, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(9, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(10, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(), new AlphanumericLength (30),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"RS/D2",
        		pnd_Twrparti_Residency_Code,"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(11, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(12, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(13, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(14, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(15, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
        getReports().setDisplayColumns(16, "T/C",
        		ldaTwrl0702.getTwrt_Record_Twrt_Company_Cde(),"SR/CE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"PY/EE",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(),"Tax ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(),"T/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id_Type(),"Pin ID",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pin_Id(),"Name",
        		ldaTwrl0702.getTwrt_Record_Twrt_Na_Line(),"Payment",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"P/Y",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Type(),"S/E",
        		ldaTwrl0702.getTwrt_Record_Twrt_Settl_Type(),"RS/DN",
        		ldaTwrl0702.getTwrt_Record_Twrt_State_Rsdncy(),"Loc",
        		ldaTwrl0702.getTwrt_Record_Twrt_Locality_Cde(),"CI/TI",
        		ldaTwrl0702.getTwrt_Record_Twrt_Citizenship(),"Tax/Year",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),"Ro/ll",
        		ldaTwrl0702.getTwrt_Record_Twrt_Rollover_Ind(),"Ci/ta",
        		ldaTwrl0702.getTwrt_Record_Twrt_Flag(),"D-O-B",
        		ldaTwrl0702.getTwrt_Record_Twrt_Dob_A(), new ReportEditMask ("XXXX/XX/XX"),"10/78",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"10/01",
        		ldaTwrl0702.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());
    }
}
