/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:58 PM
**        * FROM NATURAL PROGRAM : Twrp0921
************************************************************
**        * FILE NAME            : Twrp0921.java
**        * CLASS NAME           : Twrp0921
**        * INSTANCE NAME        : Twrp0921
************************************************************
************************************************************************
*
*  PROGRAM : TWRP0921
*  SYSTEM  : TAXWARS
*  FUNCTION: ONE TIME RUN PROGRAM.
*
*            UPDATE 2014 PAYMENT RECORDS WITH SUBPLAN,
*            ORIGINATING CONTRACT, ORIGINATING SUBPLAN FROM OMNI
*
* HISTORY
* ------------------------------------
* 17/11/2014 - CTS - REMOVE HARDCODING OF TAX YEAR TO TAKE INPUT FROM
*                    JCL - TAG - CTS
* 19/10/2016 - SINHASN - STORING NEW 'SUNYCLN' RECORD IN CONTROL TABLE
*                    NUMBER 4 AFTER SUNY-CUNY CLEANUP - TAG - SINHASN
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0921 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9420 ldaTwrl9420;
    private LdaTwrl9425 ldaTwrl9425;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntl_S;
    private DbsField cntl_S_Tircntl_Tbl_Nbr;
    private DbsField cntl_S_Tircntl_Tax_Year;
    private DbsField cntl_S_Tircntl_Seq_Nbr;

    private DbsGroup cntl_S_Tircntl_Reporting_Tbl;
    private DbsField cntl_S_Tircntl_Rpt_Form_Name;
    private DbsField cntl_S_Tircntl_Future_Use_Date;
    private DbsField cntl_S_Tircntl_Rpt_2nd_Mass_Mail_Dte;
    private DbsField cntl_S_Tircntl_Rpt_Mass_Mail_Dte;
    private DbsField cntl_S_Tircntl_Eff_Dte;
    private DbsField cntl_S_Tircntl_Pr_Cntl_Start;
    private DbsField cntl_S_Tircntl_Pr_Cntl_End;
    private DbsField cntl_S_Tircntl_Pr_Cntl_Curr;
    private DbsField pnd_Input_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Con_Pay_Sd;

    private DbsGroup pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1;
    private DbsField pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Tax_Year;
    private DbsField pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Contract_Nbr;
    private DbsField pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Payee_Cde;

    private DbsGroup pnd_Omni_Parms;
    private DbsField pnd_Omni_Parms_Pnd_Omni_File_Id;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Plan;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Contract_Nbr;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Subplan;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Orig_Subplan;
    private DbsField pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr;

    private DbsGroup pnd_Ws_Omni;
    private DbsField pnd_Ws_Omni_Pnd_Ws_Omni_Plan;
    private DbsField pnd_Ws_Omni_Pnd_Ws_Omni_Subplan;
    private DbsField pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan;
    private DbsField pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr;

    private DbsGroup pnd_Sv_Fields;
    private DbsField pnd_Sv_Fields_Pnd_Sv_Plan;
    private DbsField pnd_Sv_Fields_Pnd_Sv_Tax_Id_Nbr;
    private DbsField pnd_Sv_Fields_Pnd_Sv_Contract_Nbr;
    private DbsField pnd_Contract_Nbr;
    private DbsField pnd_D;
    private DbsField pnd_Ws_Isn;
    private DbsField pnd_I;
    private DbsField pnd_Msg_Txt;

    private DbsGroup pnd_Report_Details;
    private DbsField pnd_Report_Details_Pnd_R_Pymnt_Dte;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Display_Cnt;
    private DbsField pnd_Omni_Call_Cnt;
    private DbsField pnd_No_Ssn_Cnt;
    private DbsField pnd_No_Contract_Cnt;
    private DbsField pnd_No_Plan_Cnt;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_No_Update_Cnt;
    private DbsField pnd_Error_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_First_Omni_Call;
    private DbsField pnd_Record_Changed;
    private DbsField pnd_Payment_Changed;
    private DbsField pnd_Debug;
    private int psg1048ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9420 = new LdaTwrl9420();
        registerRecord(ldaTwrl9420);
        registerRecord(ldaTwrl9420.getVw_payr());
        ldaTwrl9425 = new LdaTwrl9425();
        registerRecord(ldaTwrl9425);
        registerRecord(ldaTwrl9425.getVw_payu());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntl_S = new DataAccessProgramView(new NameInfo("vw_cntl_S", "CNTL-S"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        cntl_S_Tircntl_Tbl_Nbr = vw_cntl_S.getRecord().newFieldInGroup("cntl_S_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_S_Tircntl_Tax_Year = vw_cntl_S.getRecord().newFieldInGroup("cntl_S_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_S_Tircntl_Seq_Nbr = vw_cntl_S.getRecord().newFieldInGroup("cntl_S_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        cntl_S_Tircntl_Reporting_Tbl = vw_cntl_S.getRecord().newGroupInGroup("CNTL_S_TIRCNTL_REPORTING_TBL", "TIRCNTL-REPORTING-TBL");
        cntl_S_Tircntl_Rpt_Form_Name = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        cntl_S_Tircntl_Future_Use_Date = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Future_Use_Date", "TIRCNTL-FUTURE-USE-DATE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FUTURE_USE_DATE");
        cntl_S_Tircntl_Rpt_2nd_Mass_Mail_Dte = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Rpt_2nd_Mass_Mail_Dte", "TIRCNTL-RPT-2ND-MASS-MAIL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_2ND_MASS_MAIL_DTE");
        cntl_S_Tircntl_Rpt_Mass_Mail_Dte = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Rpt_Mass_Mail_Dte", "TIRCNTL-RPT-MASS-MAIL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_RPT_MASS_MAIL_DTE");
        cntl_S_Tircntl_Eff_Dte = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Eff_Dte", "TIRCNTL-EFF-DTE", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "TIRCNTL_EFF_DTE");
        cntl_S_Tircntl_Pr_Cntl_Start = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Pr_Cntl_Start", "TIRCNTL-PR-CNTL-START", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_START");
        cntl_S_Tircntl_Pr_Cntl_Start.setDdmHeader("PUERTORICO/CONTROL #/START");
        cntl_S_Tircntl_Pr_Cntl_End = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Pr_Cntl_End", "TIRCNTL-PR-CNTL-END", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_END");
        cntl_S_Tircntl_Pr_Cntl_End.setDdmHeader("PUERTO RICO/CONTROL #/END");
        cntl_S_Tircntl_Pr_Cntl_Curr = cntl_S_Tircntl_Reporting_Tbl.newFieldInGroup("cntl_S_Tircntl_Pr_Cntl_Curr", "TIRCNTL-PR-CNTL-CURR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TIRCNTL_PR_CNTL_CURR");
        cntl_S_Tircntl_Pr_Cntl_Curr.setDdmHeader("PUERTO RICO/CONTROL #/CURRENT");
        registerRecord(vw_cntl_S);

        pnd_Input_Tax_Year = localVariables.newFieldInRecord("pnd_Input_Tax_Year", "#INPUT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Con_Pay_Sd = localVariables.newFieldInRecord("pnd_S_Twrpymnt_Con_Pay_Sd", "#S-TWRPYMNT-CON-PAY-SD", FieldType.STRING, 14);

        pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1 = localVariables.newGroupInRecord("pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1", "REDEFINE", pnd_S_Twrpymnt_Con_Pay_Sd);
        pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Tax_Year = pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Tax_Year", 
            "#S-TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Contract_Nbr = pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Contract_Nbr", 
            "#S-TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Payee_Cde = pnd_S_Twrpymnt_Con_Pay_Sd__R_Field_1.newFieldInGroup("pnd_S_Twrpymnt_Con_Pay_Sd_Pnd_S_Twrpymnt_Payee_Cde", 
            "#S-TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);

        pnd_Omni_Parms = localVariables.newGroupInRecord("pnd_Omni_Parms", "#OMNI-PARMS");
        pnd_Omni_Parms_Pnd_Omni_File_Id = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_File_Id", "#OMNI-FILE-ID", FieldType.STRING, 1);
        pnd_Omni_Parms_Pnd_Omni_Plan = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Plan", "#OMNI-PLAN", FieldType.STRING, 6);
        pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr", "#OMNI-TAX-ID-NBR", FieldType.STRING, 
            9);
        pnd_Omni_Parms_Pnd_Omni_Contract_Nbr = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Contract_Nbr", "#OMNI-CONTRACT-NBR", FieldType.STRING, 
            10);
        pnd_Omni_Parms_Pnd_Omni_Subplan = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Subplan", "#OMNI-SUBPLAN", FieldType.STRING, 6);
        pnd_Omni_Parms_Pnd_Omni_Orig_Subplan = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Orig_Subplan", "#OMNI-ORIG-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr = pnd_Omni_Parms.newFieldInGroup("pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr", "#OMNI-ORIG-CONTRACT-NBR", 
            FieldType.STRING, 10);

        pnd_Ws_Omni = localVariables.newGroupInRecord("pnd_Ws_Omni", "#WS-OMNI");
        pnd_Ws_Omni_Pnd_Ws_Omni_Plan = pnd_Ws_Omni.newFieldInGroup("pnd_Ws_Omni_Pnd_Ws_Omni_Plan", "#WS-OMNI-PLAN", FieldType.STRING, 6);
        pnd_Ws_Omni_Pnd_Ws_Omni_Subplan = pnd_Ws_Omni.newFieldInGroup("pnd_Ws_Omni_Pnd_Ws_Omni_Subplan", "#WS-OMNI-SUBPLAN", FieldType.STRING, 6);
        pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan = pnd_Ws_Omni.newFieldInGroup("pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan", "#WS-OMNI-ORIG-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr = pnd_Ws_Omni.newFieldInGroup("pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr", "#WS-OMNI-ORIG-CONTRACT-NBR", 
            FieldType.STRING, 10);

        pnd_Sv_Fields = localVariables.newGroupInRecord("pnd_Sv_Fields", "#SV-FIELDS");
        pnd_Sv_Fields_Pnd_Sv_Plan = pnd_Sv_Fields.newFieldInGroup("pnd_Sv_Fields_Pnd_Sv_Plan", "#SV-PLAN", FieldType.STRING, 6);
        pnd_Sv_Fields_Pnd_Sv_Tax_Id_Nbr = pnd_Sv_Fields.newFieldInGroup("pnd_Sv_Fields_Pnd_Sv_Tax_Id_Nbr", "#SV-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_Sv_Fields_Pnd_Sv_Contract_Nbr = pnd_Sv_Fields.newFieldInGroup("pnd_Sv_Fields_Pnd_Sv_Contract_Nbr", "#SV-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Contract_Nbr = localVariables.newFieldInRecord("pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Ws_Isn = localVariables.newFieldInRecord("pnd_Ws_Isn", "#WS-ISN", FieldType.PACKED_DECIMAL, 13);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Msg_Txt = localVariables.newFieldInRecord("pnd_Msg_Txt", "#MSG-TXT", FieldType.STRING, 30);

        pnd_Report_Details = localVariables.newGroupInRecord("pnd_Report_Details", "#REPORT-DETAILS");
        pnd_Report_Details_Pnd_R_Pymnt_Dte = pnd_Report_Details.newFieldInGroup("pnd_Report_Details_Pnd_R_Pymnt_Dte", "#R-PYMNT-DTE", FieldType.STRING, 
            10);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Display_Cnt = localVariables.newFieldInRecord("pnd_Display_Cnt", "#DISPLAY-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Omni_Call_Cnt = localVariables.newFieldInRecord("pnd_Omni_Call_Cnt", "#OMNI-CALL-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Ssn_Cnt = localVariables.newFieldInRecord("pnd_No_Ssn_Cnt", "#NO-SSN-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Contract_Cnt = localVariables.newFieldInRecord("pnd_No_Contract_Cnt", "#NO-CONTRACT-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Plan_Cnt = localVariables.newFieldInRecord("pnd_No_Plan_Cnt", "#NO-PLAN-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_No_Update_Cnt = localVariables.newFieldInRecord("pnd_No_Update_Cnt", "#NO-UPDATE-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Error_Cnt = localVariables.newFieldInRecord("pnd_Error_Cnt", "#ERROR-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_First_Omni_Call = localVariables.newFieldInRecord("pnd_First_Omni_Call", "#FIRST-OMNI-CALL", FieldType.BOOLEAN, 1);
        pnd_Record_Changed = localVariables.newFieldInRecord("pnd_Record_Changed", "#RECORD-CHANGED", FieldType.BOOLEAN, 1);
        pnd_Payment_Changed = localVariables.newFieldInRecord("pnd_Payment_Changed", "#PAYMENT-CHANGED", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl_S.reset();

        ldaTwrl9420.initializeValues();
        ldaTwrl9425.initializeValues();

        localVariables.reset();
        pnd_First_Omni_Call.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0921() throws Exception
    {
        super("Twrp0921");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp0921|Main");
        OnErrorManager.pushEvent("TWRP0921", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                getReports().write(0, "Start Executing ",Global.getPROGRAM(),Global.getDATX(),Global.getTIMX(),Global.getINIT_USER());                                    //Natural: WRITE 'Start Executing ' *PROGRAM *DATX *TIMX *INIT-USER
                if (Global.isEscape()) return;
                //* CTS                                                                                                                                                   //Natural: FORMAT ( 1 ) PS = 60 LS = 133
                //*                                                                                                                                                       //Natural: FORMAT ( 2 ) PS = 60 LS = 133
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Tax_Year);                                                                                   //Natural: INPUT #INPUT-TAX-YEAR
                //* CTS
                ldaTwrl9420.getVw_payr().startDatabaseRead                                                                                                                //Natural: READ PAYR BY TWRPYMNT-CON-PAY-SD = #INPUT-TAX-YEAR
                (
                "READ01",
                new Wc[] { new Wc("TWRPYMNT_CON_PAY_SD", ">=", pnd_Input_Tax_Year, WcType.BY) },
                new Oc[] { new Oc("TWRPYMNT_CON_PAY_SD", "ASC") }
                );
                READ01:
                while (condition(ldaTwrl9420.getVw_payr().readNextRow("READ01")))
                {
                    //* CTS
                    if (condition(ldaTwrl9420.getPayr_Twrpymnt_Tax_Year().notEquals(pnd_Input_Tax_Year)))                                                                 //Natural: IF PAYR.TWRPYMNT-TAX-YEAR NE #INPUT-TAX-YEAR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CNT
                    pnd_Display_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #DISPLAY-CNT
                    if (condition(pnd_Display_Cnt.equals(5000)))                                                                                                          //Natural: IF #DISPLAY-CNT = 5000
                    {
                        pnd_Display_Cnt.reset();                                                                                                                          //Natural: RESET #DISPLAY-CNT
                        //*  TO PREVENT NAT3009
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        getReports().write(0, "Rec Cnt",pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"),ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr(),Global.getTIMX());    //Natural: WRITE 'Rec Cnt' #READ-CNT PAYR.TWRPYMNT-CONTRACT-NBR *TIMX
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Record_Changed.reset();                                                                                                                           //Natural: RESET #RECORD-CHANGED #REPORT-DETAILS #MSG-TXT #I
                    pnd_Report_Details.reset();
                    pnd_Msg_Txt.reset();
                    pnd_I.reset();
                    pnd_Ws_Isn.setValue(ldaTwrl9420.getVw_payr().getAstISN("Read01"));                                                                                    //Natural: MOVE *ISN TO #WS-ISN
                    pnd_Contract_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr());                                                                               //Natural: MOVE PAYR.TWRPYMNT-CONTRACT-NBR TO #CONTRACT-NBR
                    if (condition(ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Type().equals("5") || ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr().equals(" ")))                       //Natural: IF PAYR.TWRPYMNT-TAX-ID-TYPE = '5' OR PAYR.TWRPYMNT-TAX-ID-NBR = ' '
                    {
                        pnd_No_Ssn_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #NO-SSN-CNT
                        pnd_Msg_Txt.setValue("Missing SSN or Tax Id Type 5");                                                                                             //Natural: MOVE 'Missing SSN or Tax Id Type 5' TO #MSG-TXT
                                                                                                                                                                          //Natural: PERFORM S900-EXCEPTION-REPORT
                        sub_S900_Exception_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr().equals(" ")))                                                                               //Natural: IF PAYR.TWRPYMNT-CONTRACT-NBR = ' '
                    {
                        pnd_No_Contract_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NO-CONTRACT-CNT
                        pnd_Msg_Txt.setValue("Missing Contract Number");                                                                                                  //Natural: MOVE 'Missing Contract Number' TO #MSG-TXT
                                                                                                                                                                          //Natural: PERFORM S900-EXCEPTION-REPORT
                        sub_S900_Exception_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 PAYR.C*TWRPYMNT-PAYMENTS
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9420.getPayr_Count_Casttwrpymnt_Payments())); pnd_I.nadd(1))
                    {
                        //*  ACTIVE
                        if (condition(ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Status().getValue(pnd_I).equals(" ") || ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Status().getValue(pnd_I).equals("C"))) //Natural: IF PAYR.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9420.getPayr_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("OP") || ldaTwrl9420.getPayr_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("ML")  //Natural: IF ( PAYR.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'OP' OR = 'ML' OR = 'PL' OR = 'OL' OR = 'NL' ) OR ( PAYR.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'OP' OR = 'ML' OR = 'PL' OR = 'OL' OR = 'NL' )
                            || ldaTwrl9420.getPayr_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("PL") || ldaTwrl9420.getPayr_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("OL") 
                            || ldaTwrl9420.getPayr_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_I).equals("NL") || ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("OP") 
                            || ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("ML") || ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("PL") 
                            || ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("OL") || ldaTwrl9420.getPayr_Twrpymnt_Updte_Srce_Cde().getValue(pnd_I).equals("NL")))
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I).equals(" ")))                                                              //Natural: IF PAYR.TWRPYMNT-OMNI-PLAN ( #I ) = ' '
                        {
                            pnd_No_Plan_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NO-PLAN-CNT
                            pnd_Msg_Txt.setValue("Missing Plan");                                                                                                         //Natural: MOVE 'Missing Plan' TO #MSG-TXT
                                                                                                                                                                          //Natural: PERFORM S900-EXCEPTION-REPORT
                            sub_S900_Exception_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM S001-CALL-OMNI
                        sub_S001_Call_Omni();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Omni_Parms_Pnd_Omni_Subplan.equals(" ")))                                                                                       //Natural: IF #OMNI-SUBPLAN = ' '
                        {
                            pnd_No_Plan_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NO-PLAN-CNT
                            pnd_Msg_Txt.setValue("No Subplan in Omni");                                                                                                   //Natural: MOVE 'No Subplan in Omni' TO #MSG-TXT
                                                                                                                                                                          //Natural: PERFORM S900-EXCEPTION-REPORT
                            sub_S900_Exception_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM S010-CHECK-IF-PAYMENT-CHANGED
                        sub_S010_Check_If_Payment_Changed();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Payment_Changed.getBoolean()))                                                                                                  //Natural: IF #PAYMENT-CHANGED
                        {
                            pnd_Record_Changed.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #RECORD-CHANGED
                                                                                                                                                                          //Natural: PERFORM S020-REPORT-PAYMENT
                            sub_S020_Report_Payment();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Update_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #UPDATE-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_No_Update_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #NO-UPDATE-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Record_Changed.getBoolean()))                                                                                                       //Natural: IF #RECORD-CHANGED
                    {
                                                                                                                                                                          //Natural: PERFORM S030-UPDATE--RECORD
                        sub_S030_Update__Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM S002-CLOSE-OMNI
                sub_S002_Close_Omni();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM S910-CALC-TOTALS
                sub_S910_Calc_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM S920-WRITE-TOTALS-0
                sub_S920_Write_Totals_0();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM S921-WRITE-TOTALS-1
                sub_S921_Write_Totals_1();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM S922-WRITE-TOTALS-2
                sub_S922_Write_Totals_2();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  SINHASN
                                                                                                                                                                          //Natural: PERFORM STORE-CNTL-TABLE
                sub_Store_Cntl_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, "End Executing ",Global.getPROGRAM(),Global.getDATX(),Global.getTIMX());                                                            //Natural: WRITE 'End Executing ' *PROGRAM *DATX *TIMX
                if (Global.isEscape()) return;
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* *YR.TWRPYMNT-OMNI-PLAN         (#I) := #WS-OMNI-PLAN
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* *YU.TWRPYMNT-OMNI-PLAN        (*) := PAYR.TWRPYMNT-OMNI-PLAN        (*)
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //*  << SINHASN
                //* * --------------------------------------------------------------------
                //* * --------------------------------------------------------------------
                //*  >> SINHASN
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                //*                                                                                                                                                       //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_S001_Call_Omni() throws Exception                                                                                                                    //Natural: S001-CALL-OMNI
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I).equals(pnd_Sv_Fields_Pnd_Sv_Plan) && ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr().equals(pnd_Sv_Fields_Pnd_Sv_Tax_Id_Nbr)  //Natural: IF PAYR.TWRPYMNT-OMNI-PLAN ( #I ) = #SV-PLAN AND PAYR.TWRPYMNT-TAX-ID-NBR = #SV-TAX-ID-NBR AND PAYR.TWRPYMNT-CONTRACT-NBR = #SV-CONTRACT-NBR
            && ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr().equals(pnd_Sv_Fields_Pnd_Sv_Contract_Nbr)))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sv_Fields_Pnd_Sv_Plan.setValue(ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I));                                                                     //Natural: MOVE PAYR.TWRPYMNT-OMNI-PLAN ( #I ) TO #SV-PLAN
        pnd_Sv_Fields_Pnd_Sv_Tax_Id_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr());                                                                              //Natural: MOVE PAYR.TWRPYMNT-TAX-ID-NBR TO #SV-TAX-ID-NBR
        pnd_Sv_Fields_Pnd_Sv_Contract_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr());                                                                          //Natural: MOVE PAYR.TWRPYMNT-CONTRACT-NBR TO #SV-CONTRACT-NBR
        if (condition(pnd_First_Omni_Call.getBoolean()))                                                                                                                  //Natural: IF #FIRST-OMNI-CALL
        {
            pnd_First_Omni_Call.reset();                                                                                                                                  //Natural: RESET #FIRST-OMNI-CALL
            //*  OPEN
            pnd_Omni_Parms_Pnd_Omni_File_Id.setValue("O");                                                                                                                //Natural: MOVE 'O' TO #OMNI-FILE-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Omni_Parms_Pnd_Omni_File_Id.reset();                                                                                                                      //Natural: RESET #OMNI-FILE-ID
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Omni_Parms_Pnd_Omni_Plan.setValue(ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I));                                                                  //Natural: MOVE PAYR.TWRPYMNT-OMNI-PLAN ( #I ) TO #OMNI-PLAN
        pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr());                                                                           //Natural: MOVE PAYR.TWRPYMNT-TAX-ID-NBR TO #OMNI-TAX-ID-NBR
        pnd_Omni_Parms_Pnd_Omni_Contract_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr());                                                                       //Natural: MOVE PAYR.TWRPYMNT-CONTRACT-NBR TO #OMNI-CONTRACT-NBR
        pnd_Omni_Parms_Pnd_Omni_Subplan.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #OMNI-SUBPLAN
        pnd_Omni_Parms_Pnd_Omni_Orig_Subplan.setValue(" ");                                                                                                               //Natural: MOVE ' ' TO #OMNI-ORIG-SUBPLAN
        pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr.setValue(" ");                                                                                                          //Natural: MOVE ' ' TO #OMNI-ORIG-CONTRACT-NBR
                                                                                                                                                                          //Natural: PERFORM S003-CALL-OMNI-PROGRAM
        sub_S003_Call_Omni_Program();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  S001-CALL-OMNI
    }
    private void sub_S002_Close_Omni() throws Exception                                                                                                                   //Natural: S002-CLOSE-OMNI
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        //*  CLOSE
        pnd_Omni_Parms_Pnd_Omni_File_Id.setValue("C");                                                                                                                    //Natural: MOVE 'C' TO #OMNI-FILE-ID
        pnd_Omni_Parms_Pnd_Omni_Plan.reset();                                                                                                                             //Natural: RESET #OMNI-PLAN #OMNI-TAX-ID-NBR #OMNI-CONTRACT-NBR #OMNI-SUBPLAN #OMNI-ORIG-SUBPLAN #OMNI-ORIG-CONTRACT-NBR
        pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr.reset();
        pnd_Omni_Parms_Pnd_Omni_Contract_Nbr.reset();
        pnd_Omni_Parms_Pnd_Omni_Subplan.reset();
        pnd_Omni_Parms_Pnd_Omni_Orig_Subplan.reset();
        pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr.reset();
                                                                                                                                                                          //Natural: PERFORM S003-CALL-OMNI-PROGRAM
        sub_S003_Call_Omni_Program();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  S002-CLOSE-OMNI
    }
    private void sub_S003_Call_Omni_Program() throws Exception                                                                                                            //Natural: S003-CALL-OMNI-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        pnd_Omni_Call_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #OMNI-CALL-CNT
        psg1048ReturnCode = DbsUtil.callExternalProgram("PSG1048",pnd_Omni_Parms_Pnd_Omni_File_Id,pnd_Omni_Parms_Pnd_Omni_Plan,pnd_Omni_Parms_Pnd_Omni_Tax_Id_Nbr,        //Natural: CALL 'PSG1048' USING #OMNI-FILE-ID #OMNI-PLAN #OMNI-TAX-ID-NBR #OMNI-CONTRACT-NBR #OMNI-SUBPLAN #OMNI-ORIG-SUBPLAN #OMNI-ORIG-CONTRACT-NBR
            pnd_Omni_Parms_Pnd_Omni_Contract_Nbr,pnd_Omni_Parms_Pnd_Omni_Subplan,pnd_Omni_Parms_Pnd_Omni_Orig_Subplan,pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr);
        //*  S003-CALL-OMNI-PROGRAM
    }
    private void sub_S010_Check_If_Payment_Changed() throws Exception                                                                                                     //Natural: S010-CHECK-IF-PAYMENT-CHANGED
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        pnd_Payment_Changed.reset();                                                                                                                                      //Natural: RESET #PAYMENT-CHANGED
        pnd_Ws_Omni_Pnd_Ws_Omni_Plan.setValue(pnd_Omni_Parms_Pnd_Omni_Plan);                                                                                              //Natural: ASSIGN #WS-OMNI-PLAN := #OMNI-PLAN
        pnd_Ws_Omni_Pnd_Ws_Omni_Subplan.setValue(pnd_Omni_Parms_Pnd_Omni_Subplan);                                                                                        //Natural: ASSIGN #WS-OMNI-SUBPLAN := #OMNI-SUBPLAN
        pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan.setValue(pnd_Omni_Parms_Pnd_Omni_Orig_Subplan);                                                                              //Natural: ASSIGN #WS-OMNI-ORIG-SUBPLAN := #OMNI-ORIG-SUBPLAN
        pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr.setValue(pnd_Omni_Parms_Pnd_Omni_Orig_Contract_Nbr);                                                                    //Natural: ASSIGN #WS-OMNI-ORIG-CONTRACT-NBR := #OMNI-ORIG-CONTRACT-NBR
        if (condition(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan.equals(" ")))                                                                                                  //Natural: IF #WS-OMNI-ORIG-SUBPLAN = ' '
        {
            pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan.setValue(pnd_Ws_Omni_Pnd_Ws_Omni_Subplan);                                                                               //Natural: ASSIGN #WS-OMNI-ORIG-SUBPLAN := #WS-OMNI-SUBPLAN
            //* *  #OMNI-ORIG-SUBPLAN    := #WS-OMNI-SUBPLAN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr.equals(" ")))                                                                                             //Natural: IF #WS-OMNI-ORIG-CONTRACT-NBR = ' '
        {
            pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr.setValue(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr());                                                              //Natural: ASSIGN #WS-OMNI-ORIG-CONTRACT-NBR := PAYR.TWRPYMNT-CONTRACT-NBR
            //* *  #OMNI-ORIG-CONTRACT-NBR    := PAYR.TWRPYMNT-CONTRACT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //* *  PAYR.TWRPYMNT-OMNI-PLAN        (#I) = #WS-OMNI-PLAN              AND
        if (condition(ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue(pnd_I).equals(pnd_Ws_Omni_Pnd_Ws_Omni_Subplan) && ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue(pnd_I).equals(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan)  //Natural: IF PAYR.TWRPYMNT-SUBPLAN ( #I ) = #WS-OMNI-SUBPLAN AND PAYR.TWRPYMNT-ORIG-SUBPLAN ( #I ) = #WS-OMNI-ORIG-SUBPLAN AND PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( #I ) = #WS-OMNI-ORIG-CONTRACT-NBR
            && ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue(pnd_I).equals(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr)))
        {
            //*  NO CHANGE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Payment_Changed.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #PAYMENT-CHANGED
        //*  S010-CHECK-IF-PAYMENT-CHANGED
    }
    private void sub_S020_Report_Payment() throws Exception                                                                                                               //Natural: S020-REPORT-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        pnd_Report_Details.reset();                                                                                                                                       //Natural: RESET #REPORT-DETAILS
        if (condition(DbsUtil.maskMatches(ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_I),"YYYYMMDD")))                                                          //Natural: IF PAYR.TWRPYMNT-PYMNT-DTE ( #I ) = MASK ( YYYYMMDD )
        {
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                                //Natural: MOVE EDITED PAYR.TWRPYMNT-PYMNT-DTE ( #I ) TO #D ( EM = YYYYMMDD )
            pnd_Report_Details_Pnd_R_Pymnt_Dte.setValueEdited(pnd_D,new ReportEditMask("MM/DD/YYYY"));                                                                    //Natural: MOVE EDITED #D ( EM = MM/DD/YYYY ) TO #R-PYMNT-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Details_Pnd_R_Pymnt_Dte.setValue(ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_I));                                                        //Natural: MOVE PAYR.TWRPYMNT-PYMNT-DTE ( #I ) TO #R-PYMNT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(1, "Contract",                                                                                                                               //Natural: DISPLAY ( 1 ) 'Contract' PAYR.TWRPYMNT-CONTRACT-NBR 'Payee' PAYR.TWRPYMNT-PAYEE-CDE 'SSN' PAYR.TWRPYMNT-TAX-ID-NBR 'Pymt Date' #R-PYMNT-DTE 'New/Plan' #WS-OMNI-PLAN 'New/Subplan' #WS-OMNI-SUBPLAN 'New/Orig Subplan' #WS-OMNI-ORIG-SUBPLAN 'New/Orig Contract' #WS-OMNI-ORIG-CONTRACT-NBR 'Old/Plan' PAYR.TWRPYMNT-OMNI-PLAN ( #I ) 'Old/Subplan' PAYR.TWRPYMNT-SUBPLAN ( #I ) 'Old/Orig Subplan' PAYR.TWRPYMNT-ORIG-SUBPLAN ( #I ) 'Old/Orig Contract' PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( #I )
        		ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr(),"Payee",
        		ldaTwrl9420.getPayr_Twrpymnt_Payee_Cde(),"SSN",
        		ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr(),"Pymt Date",
        		pnd_Report_Details_Pnd_R_Pymnt_Dte,"New/Plan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Plan,"New/Subplan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Subplan,"New/Orig Subplan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan,"New/Orig Contract",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr,"Old/Plan",
        		ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I),"Old/Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue(pnd_I),"Old/Orig Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue(pnd_I),"Old/Orig Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue(pnd_I));
        if (Global.isEscape()) return;
        ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue(pnd_I).setValue(pnd_Ws_Omni_Pnd_Ws_Omni_Subplan);                                                                 //Natural: ASSIGN PAYR.TWRPYMNT-SUBPLAN ( #I ) := #WS-OMNI-SUBPLAN
        ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue(pnd_I).setValue(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan);                                                       //Natural: ASSIGN PAYR.TWRPYMNT-ORIG-SUBPLAN ( #I ) := #WS-OMNI-ORIG-SUBPLAN
        ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue(pnd_I).setValue(pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr);                                             //Natural: ASSIGN PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( #I ) := #WS-OMNI-ORIG-CONTRACT-NBR
        //*  S020-REPORT-PAYMENT
    }
    private void sub_S030_Update__Record() throws Exception                                                                                                               //Natural: S030-UPDATE--RECORD
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET PAYU #WS-ISN
        ldaTwrl9425.getVw_payu().readByID(pnd_Ws_Isn.getLong(), "G1");
        ldaTwrl9425.getPayu_Twrpymnt_Subplan().getValue("*").setValue(ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue("*"));                                              //Natural: ASSIGN PAYU.TWRPYMNT-SUBPLAN ( * ) := PAYR.TWRPYMNT-SUBPLAN ( * )
        ldaTwrl9425.getPayu_Twrpymnt_Orig_Subplan().getValue("*").setValue(ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue("*"));                                    //Natural: ASSIGN PAYU.TWRPYMNT-ORIG-SUBPLAN ( * ) := PAYR.TWRPYMNT-ORIG-SUBPLAN ( * )
        ldaTwrl9425.getPayu_Twrpymnt_Orig_Contract_Nbr().getValue("*").setValue(ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue("*"));                          //Natural: ASSIGN PAYU.TWRPYMNT-ORIG-CONTRACT-NBR ( * ) := PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( * )
        ldaTwrl9425.getVw_payu().updateDBRow("G1");                                                                                                                       //Natural: UPDATE ( G1. )
        pnd_Et_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Et_Cnt.greaterOrEqual(25)))                                                                                                                     //Natural: IF #ET-CNT GE 25
        {
            pnd_Et_Cnt.reset();                                                                                                                                           //Natural: RESET #ET-CNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  S030-UPDATE--RECORD
    }
    private void sub_S900_Exception_Report() throws Exception                                                                                                             //Natural: S900-EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(pnd_I.equals(getZero()) || pnd_I.greater(24)))                                                                                                      //Natural: IF #I = 0 OR #I GT 24
        {
            //*  DUMMY STATEMENT TO PREVENT ZERO INDEX
            pnd_I.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #I
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(2, "Contract",                                                                                                                               //Natural: DISPLAY ( 2 ) 'Contract' PAYR.TWRPYMNT-CONTRACT-NBR 'Payee' PAYR.TWRPYMNT-PAYEE-CDE 'SSN' PAYR.TWRPYMNT-TAX-ID-NBR 'Payment/Date' PAYR.TWRPYMNT-PYMNT-DTE ( #I ) 'Old/Plan' PAYR.TWRPYMNT-OMNI-PLAN ( #I ) 'Old/Subplan' PAYR.TWRPYMNT-SUBPLAN ( #I ) 'Old/Orig Subplan' PAYR.TWRPYMNT-ORIG-SUBPLAN ( #I ) 'Old/Orig Contract' PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( #I ) 'Msg Text' #MSG-TXT
        		ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr(),"Payee",
        		ldaTwrl9420.getPayr_Twrpymnt_Payee_Cde(),"SSN",
        		ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr(),"Payment/Date",
        		ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte().getValue(pnd_I),"Old/Plan",
        		ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_I),"Old/Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue(pnd_I),"Old/Orig Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue(pnd_I),"Old/Orig Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue(pnd_I),"Msg Text",
        		pnd_Msg_Txt);
        if (Global.isEscape()) return;
        //*  S900-EXCEPTION-REPORT
    }
    private void sub_S910_Calc_Totals() throws Exception                                                                                                                  //Natural: S910-CALC-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Cnt.compute(new ComputeParameters(false, pnd_Error_Cnt), pnd_No_Ssn_Cnt.add(pnd_No_Contract_Cnt).add(pnd_No_Plan_Cnt));                                 //Natural: ASSIGN #ERROR-CNT := #NO-SSN-CNT + #NO-CONTRACT-CNT + #NO-PLAN-CNT
        //*  S910-CALC-TOTALS
    }
    private void sub_S920_Write_Totals_0() throws Exception                                                                                                               //Natural: S920-WRITE-TOTALS-0
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getReports().write(0, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 0 ) //
        if (Global.isEscape()) return;
        getReports().write(0, "Records Read                 :",pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                          //Natural: WRITE ( 0 ) 'Records Read                 :' #READ-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Number of Calls to OMNI      :",pnd_Omni_Call_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 0 ) 'Number of Calls to OMNI      :' #OMNI-CALL-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Total Payments Updated       :",pnd_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 0 ) 'Total Payments Updated       :' #UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Total Payments Not Updated   :",pnd_No_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 0 ) 'Total Payments Not Updated   :' #NO-UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "A: Missing SSN/Tax Id Type 5 :",pnd_No_Ssn_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 0 ) 'A: Missing SSN/Tax Id Type 5 :' #NO-SSN-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "B: Missing Contract Number   :",pnd_No_Contract_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                   //Natural: WRITE ( 0 ) 'B: Missing Contract Number   :' #NO-CONTRACT-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "C: Missing Plan/Subplan      :",pnd_No_Plan_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                       //Natural: WRITE ( 0 ) 'C: Missing Plan/Subplan      :' #NO-PLAN-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "Total Exceptions (A+B+C)     :",pnd_Error_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                         //Natural: WRITE ( 0 ) 'Total Exceptions (A+B+C)     :' #ERROR-CNT
        if (Global.isEscape()) return;
        //*  S920-WRITE-TOTALS-0
    }
    private void sub_S921_Write_Totals_1() throws Exception                                                                                                               //Natural: S921-WRITE-TOTALS-1
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getReports().write(1, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) //
        if (Global.isEscape()) return;
        getReports().write(1, "Records Read                 :",pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                          //Natural: WRITE ( 1 ) 'Records Read                 :' #READ-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "Number of Calls to OMNI      :",pnd_Omni_Call_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 1 ) 'Number of Calls to OMNI      :' #OMNI-CALL-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "Total Payments Updated       :",pnd_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 1 ) 'Total Payments Updated       :' #UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "Total Payments Not Updated   :",pnd_No_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 1 ) 'Total Payments Not Updated   :' #NO-UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "A: Missing SSN/Tax Id Type 5 :",pnd_No_Ssn_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 1 ) 'A: Missing SSN/Tax Id Type 5 :' #NO-SSN-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "B: Missing Contract Number   :",pnd_No_Contract_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                   //Natural: WRITE ( 1 ) 'B: Missing Contract Number   :' #NO-CONTRACT-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "C: Missing Plan/Subplan      :",pnd_No_Plan_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                       //Natural: WRITE ( 1 ) 'C: Missing Plan/Subplan      :' #NO-PLAN-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "Total Exceptions (A+B+C)     :",pnd_Error_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                         //Natural: WRITE ( 1 ) 'Total Exceptions (A+B+C)     :' #ERROR-CNT
        if (Global.isEscape()) return;
        //*  S921-WRITE-TOTALS-1
    }
    private void sub_S922_Write_Totals_2() throws Exception                                                                                                               //Natural: S922-WRITE-TOTALS-2
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getReports().write(2, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 2 ) //
        if (Global.isEscape()) return;
        getReports().write(2, "Records Read                 :",pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                          //Natural: WRITE ( 2 ) 'Records Read                 :' #READ-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "Number of Calls to OMNI      :",pnd_Omni_Call_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 2 ) 'Number of Calls to OMNI      :' #OMNI-CALL-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "Total Payments Updated       :",pnd_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 2 ) 'Total Payments Updated       :' #UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "Total Payments Not Updated   :",pnd_No_Update_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                     //Natural: WRITE ( 2 ) 'Total Payments Not Updated   :' #NO-UPDATE-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "A: Missing SSN/Tax Id Type 5 :",pnd_No_Ssn_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                        //Natural: WRITE ( 2 ) 'A: Missing SSN/Tax Id Type 5 :' #NO-SSN-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "B: Missing Contract Number   :",pnd_No_Contract_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                   //Natural: WRITE ( 2 ) 'B: Missing Contract Number   :' #NO-CONTRACT-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "C: Missing Plan/Subplan      :",pnd_No_Plan_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                       //Natural: WRITE ( 2 ) 'C: Missing Plan/Subplan      :' #NO-PLAN-CNT
        if (Global.isEscape()) return;
        getReports().write(2, "Total Exceptions (A+B+C)     :",pnd_Error_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                         //Natural: WRITE ( 2 ) 'Total Exceptions (A+B+C)     :' #ERROR-CNT
        if (Global.isEscape()) return;
        //*  S922-WRITE-TOTALS-2
    }
    private void sub_Store_Cntl_Table() throws Exception                                                                                                                  //Natural: STORE-CNTL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        cntl_S_Tircntl_Tbl_Nbr.setValue(4);                                                                                                                               //Natural: ASSIGN CNTL-S.TIRCNTL-TBL-NBR := 4
        cntl_S_Tircntl_Tax_Year.setValue(pnd_Input_Tax_Year);                                                                                                             //Natural: ASSIGN CNTL-S.TIRCNTL-TAX-YEAR := #INPUT-TAX-YEAR
        cntl_S_Tircntl_Seq_Nbr.setValue(11);                                                                                                                              //Natural: ASSIGN CNTL-S.TIRCNTL-SEQ-NBR := 11
        cntl_S_Tircntl_Rpt_Form_Name.setValue("SUNYCLN");                                                                                                                 //Natural: ASSIGN CNTL-S.TIRCNTL-RPT-FORM-NAME := 'SUNYCLN'
        cntl_S_Tircntl_Future_Use_Date.setValue(99999999);                                                                                                                //Natural: ASSIGN CNTL-S.TIRCNTL-FUTURE-USE-DATE := 99999999
        cntl_S_Tircntl_Rpt_2nd_Mass_Mail_Dte.setValue(99999999);                                                                                                          //Natural: ASSIGN CNTL-S.TIRCNTL-RPT-2ND-MASS-MAIL-DTE := 99999999
        cntl_S_Tircntl_Rpt_Mass_Mail_Dte.setValue(99999999);                                                                                                              //Natural: ASSIGN CNTL-S.TIRCNTL-RPT-MASS-MAIL-DTE := 99999999
        cntl_S_Tircntl_Eff_Dte.setValue(Global.getDATN());                                                                                                                //Natural: ASSIGN CNTL-S.TIRCNTL-EFF-DTE := *DATN
        cntl_S_Tircntl_Pr_Cntl_Start.setValue(" ");                                                                                                                       //Natural: ASSIGN CNTL-S.TIRCNTL-PR-CNTL-START := ' '
        cntl_S_Tircntl_Pr_Cntl_End.setValue(" ");                                                                                                                         //Natural: ASSIGN CNTL-S.TIRCNTL-PR-CNTL-END := ' '
        cntl_S_Tircntl_Pr_Cntl_Curr.setValue(" ");                                                                                                                        //Natural: ASSIGN CNTL-S.TIRCNTL-PR-CNTL-CURR := ' '
        vw_cntl_S.insertDBRow();                                                                                                                                          //Natural: STORE CNTL-S
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().display(0, "SUNYCLN record has been added in Control table");                                                                                        //Natural: DISPLAY 'SUNYCLN record has been added in Control table'
        if (Global.isEscape()) return;
        //*  STORE-CNTL-TABLE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new TabSetting(30),"Updated Payment Records - OMNI Call","Subplan, Originating Subplan & Contract",new      //Natural: WRITE ( 1 ) *PROGRAM 030T 'Updated Payment Records - OMNI Call' 'Subplan, Originating Subplan & Contract' 110T *DATX *TIMX
                        TabSetting(110),Global.getDATX(),Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, Global.getPROGRAM(),new TabSetting(30),"Not Updated Payment Records - OMNI Call","Subplan, Originating Subplan & Contract",new  //Natural: WRITE ( 2 ) *PROGRAM 030T 'Not Updated Payment Records - OMNI Call' 'Subplan, Originating Subplan & Contract' 110T *DATX *TIMX
                        TabSetting(110),Global.getDATX(),Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "ON ERROR FOR CONTRACT ",pnd_Contract_Nbr);                                                                                                 //Natural: WRITE 'ON ERROR FOR CONTRACT ' #CONTRACT-NBR
        getReports().write(0, "RECORD COUNT",pnd_Read_Cnt, new ReportEditMask ("ZZ,ZZZ,ZZ9"));                                                                            //Natural: WRITE 'RECORD COUNT' #READ-CNT
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        getReports().write(0, "ELIN: ",Global.getERROR_LINE());                                                                                                           //Natural: WRITE '=' *ERROR-LINE
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr(),"Payee",
        		ldaTwrl9420.getPayr_Twrpymnt_Payee_Cde(),"SSN",
        		ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr(),"Pymt Date",
        		pnd_Report_Details_Pnd_R_Pymnt_Dte,"New/Plan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Plan,"New/Subplan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Subplan,"New/Orig Subplan",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Subplan,"New/Orig Contract",
        		pnd_Ws_Omni_Pnd_Ws_Omni_Orig_Contract_Nbr,"Old/Plan",
        		ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan(),"Old/Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Subplan(),"Old/Orig Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan(),"Old/Orig Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr());
        getReports().setDisplayColumns(2, "Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr(),"Payee",
        		ldaTwrl9420.getPayr_Twrpymnt_Payee_Cde(),"SSN",
        		ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr(),"Payment/Date",
        		ldaTwrl9420.getPayr_Twrpymnt_Pymnt_Dte(),"Old/Plan",
        		ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan(),"Old/Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Subplan(),"Old/Orig Subplan",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan(),"Old/Orig Contract",
        		ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr(),"Msg Text",
        		pnd_Msg_Txt);
        getReports().setDisplayColumns(0, "SUNYCLN record has been added in Control table");
    }
}
