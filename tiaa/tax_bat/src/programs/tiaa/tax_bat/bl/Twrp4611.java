/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:39:58 PM
**        * FROM NATURAL PROGRAM : Twrp4611
************************************************************
**        * FILE NAME            : Twrp4611.java
**        * CLASS NAME           : Twrp4611
**        * INSTANCE NAME        : Twrp4611
************************************************************
***********************************************************************
* PROGRAM  : TWRP4611
* SYSTEM   : TAX WITHHOLDING AND REPORTING SYSTEM.
* TITLE    : REPORTING PROGRAM.
* CREATED  : 04 / 03 / 2016.
*  BY      : RAHUL DAS
* FUNCTION : PROGRAM USES THE ACCEPTED RECORDS COMBINED FILE
*            AND USES IT FOR GENERATING UNIQUE TIN COUNT REPORT.
*
* HISTORY  : RAHUL DAS - INCORPORATE CHANGES TO HANDLE NON NUMERIC PIN
*                        TAG: DASRAH
*    09/27/16  JB - CPM ELIMINATION  SCAN 092716
*
*    04/27/17  JW - PIN EXPANSION  SCAN JW0427
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
**********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4611 extends BLNatBase
{
    // Data Areas
    private LdaTwrl462a ldaTwrl462a;
    private LdaTwrl462e ldaTwrl462e;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Accept_Cnt;
    private DbsField pnd_E_Del_Cnt;
    private DbsField pnd_Paper_Cnt;
    private DbsField pnd_Deceased_Cnt;
    private DbsField pnd_Txyr;
    private DbsField pnd_Customer_Id;

    private DbsGroup pnd_Customer_Id__R_Field_1;
    private DbsField pnd_Customer_Id_Pnd_Pin;

    private DataAccessProgramView vw_twrparti_Read;
    private DbsField twrparti_Read_Twrparti_Status;
    private DbsField twrparti_Read_Twrparti_Tax_Id_Type;
    private DbsField twrparti_Read_Twrparti_Tax_Id;
    private DbsField twrparti_Read_Twrparti_Pin;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Addr_Dte;
    private DbsField twrparti_Read_Twrparti_Rlup_Email_Pref_Dte;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_2;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl462a = new LdaTwrl462a();
        registerRecord(ldaTwrl462a);
        ldaTwrl462e = new LdaTwrl462e();
        registerRecord(ldaTwrl462e);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Accept_Cnt = localVariables.newFieldInRecord("pnd_Accept_Cnt", "#ACCEPT-CNT", FieldType.NUMERIC, 10);
        pnd_E_Del_Cnt = localVariables.newFieldInRecord("pnd_E_Del_Cnt", "#E-DEL-CNT", FieldType.NUMERIC, 10);
        pnd_Paper_Cnt = localVariables.newFieldInRecord("pnd_Paper_Cnt", "#PAPER-CNT", FieldType.NUMERIC, 10);
        pnd_Deceased_Cnt = localVariables.newFieldInRecord("pnd_Deceased_Cnt", "#DECEASED-CNT", FieldType.NUMERIC, 10);
        pnd_Txyr = localVariables.newFieldInRecord("pnd_Txyr", "#TXYR", FieldType.STRING, 4);
        pnd_Customer_Id = localVariables.newFieldInRecord("pnd_Customer_Id", "#CUSTOMER-ID", FieldType.STRING, 15);

        pnd_Customer_Id__R_Field_1 = localVariables.newGroupInRecord("pnd_Customer_Id__R_Field_1", "REDEFINE", pnd_Customer_Id);
        pnd_Customer_Id_Pnd_Pin = pnd_Customer_Id__R_Field_1.newFieldInGroup("pnd_Customer_Id_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);

        vw_twrparti_Read = new DataAccessProgramView(new NameInfo("vw_twrparti_Read", "TWRPARTI-READ"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        twrparti_Read_Twrparti_Status = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Read_Twrparti_Tax_Id_Type = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID_TYPE");
        twrparti_Read_Twrparti_Tax_Id = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Read_Twrparti_Pin = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, 
            RepeatingFieldStrategy.None, "TWRPARTI_PIN");
        twrparti_Read_Twrparti_Rlup_Email_Addr = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr", "TWRPARTI-RLUP-EMAIL-ADDR", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR");
        twrparti_Read_Twrparti_Rlup_Email_Pref = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref", "TWRPARTI-RLUP-EMAIL-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF");
        twrparti_Read_Twrparti_Rlup_Email_Addr_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Addr_Dte", "TWRPARTI-RLUP-EMAIL-ADDR-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR_DTE");
        twrparti_Read_Twrparti_Rlup_Email_Pref_Dte = vw_twrparti_Read.getRecord().newFieldInGroup("twrparti_Read_Twrparti_Rlup_Email_Pref_Dte", "TWRPARTI-RLUP-EMAIL-PREF-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF_DTE");
        registerRecord(vw_twrparti_Read);

        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_2 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_2", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id", 
            "#KEY-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status", 
            "#KEY-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_2.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte", 
            "#KEY-TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_twrparti_Read.reset();

        ldaTwrl462a.initializeValues();
        ldaTwrl462e.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4611() throws Exception
    {
        super("Twrp4611");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 01 RECORD IRA-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl462a.getIra_Record())))
        {
            pnd_Accept_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ACCEPT-CNT
            if (condition(pnd_Accept_Cnt.equals(1)))                                                                                                                      //Natural: IF #ACCEPT-CNT = 1
            {
                pnd_Txyr.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Year());                                                                                              //Natural: ASSIGN #TXYR := IRA-RECORD.IRA-TAX-YEAR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl462a.getIra_Record_Ira_Dod().notEquals(" ") && ldaTwrl462a.getIra_Record_Ira_Dod().notEquals("00000000")))                               //Natural: IF IRA-DOD NE ' ' AND IRA-DOD NE '00000000'
            {
                pnd_Deceased_Cnt.nadd(1);                                                                                                                                 //Natural: ASSIGN #DECEASED-CNT := #DECEASED-CNT + 1
                                                                                                                                                                          //Natural: PERFORM WRITE-DECEASED-RPT-DTL
                sub_Write_Deceased_Rpt_Dtl();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-EDELIVERY
            sub_Lookup_Edelivery();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-DECEASED-RPT-TLR
        sub_Write_Deceased_Rpt_Tlr();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-UNQTIN-REPORT
        sub_Write_Unqtin_Report();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //* **********************************************                                                                                                                //Natural: AT TOP OF PAGE ( 02 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DECEASED-RPT-DTL
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-EDELIVERY
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DECEASED-RPT-TLR
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-UNQTIN-REPORT
    }
    private void sub_Write_Deceased_Rpt_Dtl() throws Exception                                                                                                            //Natural: WRITE-DECEASED-RPT-DTL
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(25),ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                            //Natural: WRITE ( 02 ) NOTITLE 25T IRA-TAX-ID
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 02 ) 1
    }
    private void sub_Lookup_Edelivery() throws Exception                                                                                                                  //Natural: LOOKUP-EDELIVERY
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        //*  RESET #COMM-CPMN550 #CUSTOMER-ID       092716  START
        //*  DASRAH
        //*  JW0427
        if (condition(DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Pin(),"9999999") || DbsUtil.maskMatches(ldaTwrl462a.getIra_Record_Ira_Pin(),"999999999999")))     //Natural: IF IRA-PIN = MASK ( 9999999 ) OR IRA-PIN = MASK ( 999999999999 )
        {
            pnd_Customer_Id_Pnd_Pin.compute(new ComputeParameters(false, pnd_Customer_Id_Pnd_Pin), ldaTwrl462a.getIra_Record_Ira_Pin().val());                            //Natural: ASSIGN #PIN := VAL ( IRA-PIN )
            //*  DASRAH
            //*  DASRAH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Customer_Id_Pnd_Pin.setValue(0);                                                                                                                          //Natural: ASSIGN #PIN := 0
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        //*  #COMM-CPMN550.#COMM-CUSTOMER-PIN   := #CUSTOMER-ID
        //*  #COMM-CPMN550.#COMM-CUSTOMER-TYP   := 'PH'
        //*  #COMM-CPMN550.#COMM-CUSTOMER-FK    := 'PPPPPPPPPPPPPPP'
        //*  #COMM-CPMN550.#COMM-DOC-ID         := 'D35'
        //*  CALLNAT 'CPMN550' USING #COMM-CPMN550
        //*  IF #COMM-OUT-USER-ERROR-CODE = 0 AND ( IRA-DOD = ' ' OR= '00000000')
        pnd_Twrparti_Curr_Rec_Sd.reset();                                                                                                                                 //Natural: RESET #TWRPARTI-CURR-REC-SD
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id.setValue(ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                                //Natural: ASSIGN #KEY-TWRPARTI-TAX-ID := IRA-TAX-ID
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status.setValue(" ");                                                                                                   //Natural: ASSIGN #KEY-TWRPARTI-STATUS := ' '
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                                  //Natural: ASSIGN #KEY-TWRPARTI-PART-EFF-END-DTE := '99999999'
        vw_twrparti_Read.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) TWRPARTI-READ WITH TWRPARTI-CURR-REC-SD EQ #TWRPARTI-CURR-REC-SD
        (
        "READ01",
        new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
        1
        );
        READ01:
        while (condition(vw_twrparti_Read.readNextRow("READ01")))
        {
            if (condition(twrparti_Read_Twrparti_Tax_Id.notEquals(ldaTwrl462a.getIra_Record_Ira_Tax_Id())))                                                               //Natural: IF TWRPARTI-TAX-ID NE IRA-TAX-ID
            {
                getReports().write(0, "=",twrparti_Read_Twrparti_Tax_Id,"=",ldaTwrl462a.getIra_Record_Ira_Tax_Id());                                                      //Natural: WRITE '=' TWRPARTI-TAX-ID '=' IRA-TAX-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **");                                                                                 //Natural: WRITE '** SYSTEM ERROR: SSN NOT ON PARTICIPANT FILE **'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #COMM-OUT-USER-ERROR-CODE = 0 AND ( IRA-DOD = ' ' OR= '00000000')
            if (condition(((twrparti_Read_Twrparti_Rlup_Email_Pref.equals("Y") && (ldaTwrl462a.getIra_Record_Ira_Dod().equals(" ") || ldaTwrl462a.getIra_Record_Ira_Dod().equals("00000000")))  //Natural: IF TWRPARTI-RLUP-EMAIL-PREF = 'Y' AND ( IRA-DOD = ' ' OR = '00000000' ) AND #PIN NE 0
                && pnd_Customer_Id_Pnd_Pin.notEquals(getZero()))))
            {
                pnd_E_Del_Cnt.nadd(1);                                                                                                                                    //Natural: ASSIGN #E-DEL-CNT := #E-DEL-CNT + 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Paper_Cnt.nadd(1);                                                                                                                                    //Natural: ASSIGN #PAPER-CNT := #PAPER-CNT + 1
            }                                                                                                                                                             //Natural: END-IF
            //*   092716 END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Write_Deceased_Rpt_Tlr() throws Exception                                                                                                            //Natural: WRITE-DECEASED-RPT-TLR
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(96));                                                                                               //Natural: WRITE ( 02 ) NOTITLE '-' ( 96 )
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Total TIN Count",new TabSetting(22),pnd_Deceased_Cnt);                                                                //Natural: WRITE ( 02 ) NOTITLE 'Total TIN Count' 22T #DECEASED-CNT
        if (Global.isEscape()) return;
    }
    private void sub_Write_Unqtin_Report() throws Exception                                                                                                               //Natural: WRITE-UNQTIN-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(96));                                                                                               //Natural: WRITE ( 01 ) NOTITLE '-' ( 96 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TIN COUNT");                                                                                       //Natural: WRITE ( 01 ) NOTITLE 30T 'TIN COUNT'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(96));                                                                                               //Natural: WRITE ( 01 ) NOTITLE '-' ( 96 )
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Total Accepted      ",new TabSetting(25),pnd_Accept_Cnt,NEWLINE,NEWLINE);                                             //Natural: WRITE ( 01 ) NOTITLE 'Total Accepted      ' 25T #ACCEPT-CNT //
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"E-Del Accepted      ",new TabSetting(25),pnd_E_Del_Cnt,NEWLINE,NEWLINE);                                              //Natural: WRITE ( 01 ) NOTITLE 'E-Del Accepted      ' 25T #E-DEL-CNT //
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
        getReports().write(1, ReportOption.NOTITLE,"Paper Accepted      ",new TabSetting(25),pnd_Paper_Cnt,NEWLINE,NEWLINE);                                              //Natural: WRITE ( 01 ) NOTITLE 'Paper Accepted      ' 25T #PAPER-CNT //
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(35),"TAX WITHHOLDING & REPORTING SYSTEM",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 35T 'TAX WITHHOLDING & REPORTING SYSTEM' 85T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(85),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"Unique TIN count",NEWLINE,"Year : ",pnd_Txyr,new  //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 45T 'Unique TIN count'/ 'Year : ' #TXYR 85T 'REPORT: RPT1' //
                        TabSetting(85),"REPORT: RPT1",NEWLINE,NEWLINE);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(35),"TAX WITHHOLDING & REPORTING SYSTEM",new  //Natural: WRITE ( 02 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 35T 'TAX WITHHOLDING & REPORTING SYSTEM' 85T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 )
                        TabSetting(85),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(39),"Deceased Participant Report",NEWLINE,"YEAR : ",pnd_Txyr,new  //Natural: WRITE ( 02 ) NOTITLE *INIT-USER '-' *PROGRAM 39T 'Deceased Participant Report'/ 'YEAR : ' #TXYR 85T 'REPORT: RPT1' //
                        TabSetting(85),"REPORT: RPT1",NEWLINE,NEWLINE);
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 02 ) 1
                    getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(96));                                                                                   //Natural: WRITE ( 02 ) NOTITLE '-' ( 96 )
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(25),"TIN Number");                                                                          //Natural: WRITE ( 02 ) NOTITLE 25T 'TIN Number'
                    getReports().write(2, ReportOption.NOTITLE,"-",new RepeatItem(96));                                                                                   //Natural: WRITE ( 02 ) NOTITLE '-' ( 96 )
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
    }
}
