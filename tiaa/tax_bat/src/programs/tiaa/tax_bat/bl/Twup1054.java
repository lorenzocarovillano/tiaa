/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:45:37 PM
**        * FROM NATURAL PROGRAM : Twup1054
************************************************************
**        * FILE NAME            : Twup1054.java
**        * CLASS NAME           : Twup1054
**        * INSTANCE NAME        : Twup1054
************************************************************
**--------------------------------------------------------------------
** PROGRAM    :  TWUP1054
** APPLICATION:  TAXWARS SYSTEM
** DESCRIPTION:  REFFERENCE TABLE DATA DELETE PROGRAM.
**               RUNS YEARLY, TO REMOVE DATA MORE THAN 2 YEARS OLD.
**               REFFENCE TABLE DATA ORIGINATES FROM TWUP1053
**               (TAXWARS TO ODS INCOME SUMMARY) PROGRAM.
** AUTHOR     :  COGNIZANT
**
**--------------------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twup1054 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rt_Read;

    private DbsGroup rt_Read_Rt_Record;
    private DbsField rt_Read_Rt_A_I_Ind;
    private DbsField rt_Read_Rt_Table_Id;
    private DbsField rt_Read_Rt_Short_Key;
    private DbsField rt_Read_Rt_Long_Key;
    private DbsField rt_Read_Rt_Desc1;
    private DbsField rt_Read_Rt_Desc2;
    private DbsField rt_Read_Rt_Desc3;
    private DbsField rt_Read_Rt_Desc4;
    private DbsField rt_Read_Rt_Desc5;
    private DbsField rt_Read_Rt_Eff_From_Ccyymmdd;
    private DbsField rt_Read_Rt_Eff_To_Ccyymmdd;
    private DbsField rt_Read_Rt_Upd_Source;
    private DbsField rt_Read_Rt_Upd_User;
    private DbsField rt_Read_Rt_Upd_Ccyymmdd;
    private DbsField rt_Read_Rt_Upd_Timn;

    private DataAccessProgramView vw_rt_Find;

    private DbsGroup rt_Find_Rt_Record;
    private DbsField rt_Find_Rt_A_I_Ind;
    private DbsField rt_Find_Rt_Table_Id;
    private DbsField rt_Find_Rt_Short_Key;
    private DbsField rt_Find_Rt_Long_Key;
    private DbsField rt_Find_Rt_Desc1;
    private DbsField rt_Find_Rt_Desc2;
    private DbsField rt_Find_Rt_Desc3;
    private DbsField rt_Find_Rt_Desc4;
    private DbsField rt_Find_Rt_Desc5;
    private DbsField rt_Find_Rt_Eff_From_Ccyymmdd;
    private DbsField rt_Find_Rt_Eff_To_Ccyymmdd;
    private DbsField rt_Find_Rt_Upd_Source;
    private DbsField rt_Find_Rt_Upd_User;
    private DbsField rt_Find_Rt_Upd_Ccyymmdd;
    private DbsField rt_Find_Rt_Upd_Timn;
    private DbsField pnd_Rt_Super1_Start;

    private DbsGroup pnd_Rt_Super1_Start__R_Field_1;
    private DbsField pnd_Rt_Super1_Start_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Start_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Start_Pnd_Rt_Short_Key;
    private DbsField pnd_Rt_Super1_Start_Pnd_Rt_Long_Key;
    private DbsField pnd_Date_Start_D;

    private DbsGroup pnd_Date_Start_D__R_Field_2;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Ccyy;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Mm;
    private DbsField pnd_Date_Start_D_Pnd_Date_Start_Dd;
    private DbsField wk_Eff_To_Ccyymmdd;

    private DbsGroup wk_Eff_To_Ccyymmdd__R_Field_3;
    private DbsField wk_Eff_To_Ccyymmdd_Wk_Eff_To_Ccyy;
    private DbsField wk_Eff_To_Ccyymmdd_Wk_Eff_To_Mm;
    private DbsField wk_Eff_To_Ccyymmdd_Wk_Eff_To_Dd;
    private DbsField pnd_Timex;

    private DbsGroup pnd_Timex__R_Field_4;
    private DbsField pnd_Timex_Pnd_Timex_Hh;
    private DbsField pnd_Timex_Pnd_Timex_Ii;
    private DbsField pnd_Timex_Pnd_Timex_Ss;

    private DbsGroup pnd_Timex__R_Field_5;
    private DbsField pnd_Timex_Pnd_Timen;
    private DbsField pnd_Isn;
    private DbsField wk_Rt_Short_Key;
    private DbsField pnd_Year_Diff;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Del_Count;
    private DbsField pnd_Temp_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rt_Read = new DataAccessProgramView(new NameInfo("vw_rt_Read", "RT-READ"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt_Read_Rt_Record = vw_rt_Read.getRecord().newGroupInGroup("RT_READ_RT_RECORD", "RT-RECORD");
        rt_Read_Rt_A_I_Ind = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Read_Rt_Table_Id = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        rt_Read_Rt_Short_Key = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        rt_Read_Rt_Long_Key = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        rt_Read_Rt_Desc1 = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Read_Rt_Desc2 = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Read_Rt_Desc3 = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rt_Read_Rt_Desc4 = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rt_Read_Rt_Desc5 = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rt_Read_Rt_Eff_From_Ccyymmdd = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        rt_Read_Rt_Eff_To_Ccyymmdd = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rt_Read_Rt_Upd_Source = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        rt_Read_Rt_Upd_User = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        rt_Read_Rt_Upd_Ccyymmdd = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rt_Read_Rt_Upd_Timn = rt_Read_Rt_Record.newFieldInGroup("rt_Read_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_rt_Read);

        vw_rt_Find = new DataAccessProgramView(new NameInfo("vw_rt_Find", "RT-FIND"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt_Find_Rt_Record = vw_rt_Find.getRecord().newGroupInGroup("RT_FIND_RT_RECORD", "RT-RECORD");
        rt_Find_Rt_A_I_Ind = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Find_Rt_Table_Id = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        rt_Find_Rt_Short_Key = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        rt_Find_Rt_Long_Key = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        rt_Find_Rt_Desc1 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Find_Rt_Desc2 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Find_Rt_Desc3 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rt_Find_Rt_Desc4 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rt_Find_Rt_Desc5 = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rt_Find_Rt_Eff_From_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        rt_Find_Rt_Eff_To_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rt_Find_Rt_Upd_Source = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        rt_Find_Rt_Upd_User = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        rt_Find_Rt_Upd_Ccyymmdd = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rt_Find_Rt_Upd_Timn = rt_Find_Rt_Record.newFieldInGroup("rt_Find_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_rt_Find);

        pnd_Rt_Super1_Start = localVariables.newFieldInRecord("pnd_Rt_Super1_Start", "#RT-SUPER1-START", FieldType.STRING, 66);

        pnd_Rt_Super1_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Rt_Super1_Start__R_Field_1", "REDEFINE", pnd_Rt_Super1_Start);
        pnd_Rt_Super1_Start_Pnd_Rt_A_I_Ind = pnd_Rt_Super1_Start__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Start_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Rt_Super1_Start_Pnd_Rt_Table_Id = pnd_Rt_Super1_Start__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Start_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Rt_Super1_Start_Pnd_Rt_Short_Key = pnd_Rt_Super1_Start__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Start_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", 
            FieldType.STRING, 20);
        pnd_Rt_Super1_Start_Pnd_Rt_Long_Key = pnd_Rt_Super1_Start__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Start_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Date_Start_D = localVariables.newFieldInRecord("pnd_Date_Start_D", "#DATE-START-D", FieldType.STRING, 8);

        pnd_Date_Start_D__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Start_D__R_Field_2", "REDEFINE", pnd_Date_Start_D);
        pnd_Date_Start_D_Pnd_Date_Start_Ccyy = pnd_Date_Start_D__R_Field_2.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Ccyy", "#DATE-START-CCYY", 
            FieldType.NUMERIC, 4);
        pnd_Date_Start_D_Pnd_Date_Start_Mm = pnd_Date_Start_D__R_Field_2.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Mm", "#DATE-START-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Start_D_Pnd_Date_Start_Dd = pnd_Date_Start_D__R_Field_2.newFieldInGroup("pnd_Date_Start_D_Pnd_Date_Start_Dd", "#DATE-START-DD", FieldType.NUMERIC, 
            2);
        wk_Eff_To_Ccyymmdd = localVariables.newFieldInRecord("wk_Eff_To_Ccyymmdd", "WK-EFF-TO-CCYYMMDD", FieldType.STRING, 8);

        wk_Eff_To_Ccyymmdd__R_Field_3 = localVariables.newGroupInRecord("wk_Eff_To_Ccyymmdd__R_Field_3", "REDEFINE", wk_Eff_To_Ccyymmdd);
        wk_Eff_To_Ccyymmdd_Wk_Eff_To_Ccyy = wk_Eff_To_Ccyymmdd__R_Field_3.newFieldInGroup("wk_Eff_To_Ccyymmdd_Wk_Eff_To_Ccyy", "WK-EFF-TO-CCYY", FieldType.NUMERIC, 
            4);
        wk_Eff_To_Ccyymmdd_Wk_Eff_To_Mm = wk_Eff_To_Ccyymmdd__R_Field_3.newFieldInGroup("wk_Eff_To_Ccyymmdd_Wk_Eff_To_Mm", "WK-EFF-TO-MM", FieldType.NUMERIC, 
            2);
        wk_Eff_To_Ccyymmdd_Wk_Eff_To_Dd = wk_Eff_To_Ccyymmdd__R_Field_3.newFieldInGroup("wk_Eff_To_Ccyymmdd_Wk_Eff_To_Dd", "WK-EFF-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Timex = localVariables.newFieldInRecord("pnd_Timex", "#TIMEX", FieldType.STRING, 6);

        pnd_Timex__R_Field_4 = localVariables.newGroupInRecord("pnd_Timex__R_Field_4", "REDEFINE", pnd_Timex);
        pnd_Timex_Pnd_Timex_Hh = pnd_Timex__R_Field_4.newFieldInGroup("pnd_Timex_Pnd_Timex_Hh", "#TIMEX-HH", FieldType.STRING, 2);
        pnd_Timex_Pnd_Timex_Ii = pnd_Timex__R_Field_4.newFieldInGroup("pnd_Timex_Pnd_Timex_Ii", "#TIMEX-II", FieldType.STRING, 2);
        pnd_Timex_Pnd_Timex_Ss = pnd_Timex__R_Field_4.newFieldInGroup("pnd_Timex_Pnd_Timex_Ss", "#TIMEX-SS", FieldType.STRING, 2);

        pnd_Timex__R_Field_5 = localVariables.newGroupInRecord("pnd_Timex__R_Field_5", "REDEFINE", pnd_Timex);
        pnd_Timex_Pnd_Timen = pnd_Timex__R_Field_5.newFieldInGroup("pnd_Timex_Pnd_Timen", "#TIMEN", FieldType.NUMERIC, 6);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        wk_Rt_Short_Key = localVariables.newFieldInRecord("wk_Rt_Short_Key", "WK-RT-SHORT-KEY", FieldType.STRING, 4);
        pnd_Year_Diff = localVariables.newFieldInRecord("pnd_Year_Diff", "#YEAR-DIFF", FieldType.NUMERIC, 2);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 11);
        pnd_Del_Count = localVariables.newFieldInRecord("pnd_Del_Count", "#DEL-COUNT", FieldType.NUMERIC, 11);
        pnd_Temp_Count = localVariables.newFieldInRecord("pnd_Temp_Count", "#TEMP-COUNT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rt_Read.reset();
        vw_rt_Find.reset();

        localVariables.reset();
        pnd_Rt_Super1_Start.setInitialValue("A10000");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twup1054() throws Exception
    {
        super("Twup1054");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt15, 15);
        setupReports();
        //* *
        //* *                                                                                                                                                             //Natural: FORMAT PS = 23;//Natural: FORMAT LS = 130
        //* *                                                                                                                                                             //Natural: AT TOP OF PAGE ( 15 )
        //*    START OF LOGIC   */
        pnd_Date_Start_D.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE-START-D
        pnd_Timex.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISS"));                                                                                          //Natural: MOVE EDITED *TIMX ( EM = HHIISS ) TO #TIMEX
        getReports().write(0, "$$$ = RUN START DATE: ",pnd_Date_Start_D_Pnd_Date_Start_Ccyy,"/",pnd_Date_Start_D_Pnd_Date_Start_Mm,"/",pnd_Date_Start_D_Pnd_Date_Start_Dd); //Natural: WRITE '$$$ = RUN START DATE: ' #DATE-START-CCYY '/' #DATE-START-MM '/' #DATE-START-DD
        if (Global.isEscape()) return;
        getReports().write(0, "$$$ = RUN START TIME: ",pnd_Timex_Pnd_Timex_Hh,":",pnd_Timex_Pnd_Timex_Ii,":",pnd_Timex_Pnd_Timex_Ss);                                     //Natural: WRITE '$$$ = RUN START TIME: ' #TIMEX-HH ':' #TIMEX-II ':' #TIMEX-SS
        if (Global.isEscape()) return;
        vw_rt_Read.startDatabaseRead                                                                                                                                      //Natural: READ RT-READ BY RT-SUPER1 STARTING FROM #RT-SUPER1-START
        (
        "RD1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1_Start, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        RD1:
        while (condition(vw_rt_Read.readNextRow("RD1")))
        {
            if (condition(pnd_Et_Count.greater(1000)))                                                                                                                    //Natural: IF #ET-COUNT > 1000
            {
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Et_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ET-COUNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rt_Read_Rt_Table_Id.notEquals(pnd_Rt_Super1_Start_Pnd_Rt_Table_Id)))                                                                            //Natural: IF RT-READ.RT-TABLE-ID NE #RT-TABLE-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rt_Read_Rt_Upd_Source.notEquals("TWUP1053")))                                                                                                   //Natural: IF RT-READ.RT-UPD-SOURCE NE 'TWUP1053'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Isn.setValue(vw_rt_Read.getAstISN("RD1"));                                                                                                            //Natural: ASSIGN #ISN := *ISN ( RD1. )
                                                                                                                                                                          //Natural: PERFORM PREP-FOR-DELETE-REC
                sub_Prep_For_Delete_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "Total records Deleted: ",pnd_Del_Count);                                                                                                   //Natural: WRITE 'Total records Deleted: ' #DEL-COUNT
        if (Global.isEscape()) return;
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREP-FOR-DELETE-REC
        //* ************************************************
    }
    private void sub_Prep_For_Delete_Rec() throws Exception                                                                                                               //Natural: PREP-FOR-DELETE-REC
    {
        if (BLNatReinput.isReinput()) return;

        GET_REC:                                                                                                                                                          //Natural: GET RT-FIND #ISN
        vw_rt_Find.readByID(pnd_Isn.getLong(), "GET_REC");
        wk_Rt_Short_Key.setValue(rt_Find_Rt_Short_Key);                                                                                                                   //Natural: ASSIGN WK-RT-SHORT-KEY := RT-SHORT-KEY
        wk_Eff_To_Ccyymmdd.setValue(rt_Find_Rt_Eff_To_Ccyymmdd);                                                                                                          //Natural: ASSIGN WK-EFF-TO-CCYYMMDD := RT-FIND.RT-EFF-TO-CCYYMMDD
        pnd_Year_Diff.compute(new ComputeParameters(false, pnd_Year_Diff), wk_Eff_To_Ccyymmdd_Wk_Eff_To_Ccyy.subtract(pnd_Date_Start_D_Pnd_Date_Start_Ccyy));             //Natural: ASSIGN #YEAR-DIFF := WK-EFF-TO-CCYY - #DATE-START-CCYY
        if (condition(wk_Eff_To_Ccyymmdd_Wk_Eff_To_Ccyy.equals(pnd_Date_Start_D_Pnd_Date_Start_Ccyy)))                                                                    //Natural: IF WK-EFF-TO-CCYY EQ #DATE-START-CCYY
        {
            getReports().write(15, ReportOption.NOTITLE,new TabSetting(2),rt_Read_Rt_A_I_Ind,new TabSetting(5),rt_Read_Rt_Table_Id,new TabSetting(11),wk_Rt_Short_Key,new //Natural: WRITE ( 15 ) 002T RT-A-I-IND 005T RT-TABLE-ID 011T WK-RT-SHORT-KEY 016T RT-LONG-KEY 057T RT-EFF-FROM-CCYYMMDD 066T RT-EFF-TO-CCYYMMDD
                TabSetting(16),rt_Read_Rt_Long_Key,new TabSetting(57),rt_Read_Rt_Eff_From_Ccyymmdd,new TabSetting(66),rt_Read_Rt_Eff_To_Ccyymmdd);
            if (Global.isEscape()) return;
            vw_rt_Find.deleteDBRow("GET_REC");                                                                                                                            //Natural: DELETE ( GET-REC. )
            pnd_Temp_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TEMP-COUNT
            pnd_Del_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #DEL-COUNT
            if (condition(pnd_Temp_Count.equals(50000)))                                                                                                                  //Natural: IF #TEMP-COUNT = 50000
            {
                pnd_Temp_Count.reset();                                                                                                                                   //Natural: RESET #TEMP-COUNT
                getReports().display(0, pnd_Del_Count);                                                                                                                   //Natural: DISPLAY #DEL-COUNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PREP-FOR-DELETE-REC
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt15 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(15, ReportOption.NOTITLE,new TabSetting(2),Global.getPROGRAM(),new TabSetting(20),"FOLLOWING RECORDS WILL BE DELETED FROM REFERENCE-TABLE",NEWLINE,new  //Natural: WRITE ( 15 ) NOTITLE 002T *PROGRAM 020T 'FOLLOWING RECORDS WILL BE DELETED FROM REFERENCE-TABLE' / 001T 'A-I' 005T 'TABLE' 011T 'SHORT' 057T 'EFFECTVE' 066T 'EFFECTVE' / 001T 'IND' 005T ' ID  ' 012T 'KEY' 016T 'LONG - KEY ' 057T 'FRM-DATE' 067T 'TO-DATE' / 001T '---' 005T '-----' 011T '---------------------------------------------' 057T '--------' 066T '--------' //
                        TabSetting(1),"A-I",new TabSetting(5),"TABLE",new TabSetting(11),"SHORT",new TabSetting(57),"EFFECTVE",new TabSetting(66),"EFFECTVE",NEWLINE,new 
                        TabSetting(1),"IND",new TabSetting(5)," ID  ",new TabSetting(12),"KEY",new TabSetting(16),"LONG - KEY ",new TabSetting(57),"FRM-DATE",new 
                        TabSetting(67),"TO-DATE",NEWLINE,new TabSetting(1),"---",new TabSetting(5),"-----",new TabSetting(11),"---------------------------------------------",new 
                        TabSetting(57),"--------",new TabSetting(66),"--------",NEWLINE,NEWLINE);
                    //*       080T 'PAGE:' *PAGE-NUMBER (6) (EM=ZZZZ9) /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23");
        Global.format(0, "LS=130");

        getReports().setDisplayColumns(0, pnd_Del_Count);
    }
}
