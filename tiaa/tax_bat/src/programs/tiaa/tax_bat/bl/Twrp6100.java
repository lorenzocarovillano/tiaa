/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:43:32 PM
**        * FROM NATURAL PROGRAM : Twrp6100
************************************************************
**        * FILE NAME            : Twrp6100.java
**        * CLASS NAME           : Twrp6100
**        * INSTANCE NAME        : Twrp6100
************************************************************
************************************************************************
* PROGRAM...: TWRP6100
* FUNCTION..: THIS PROGRAM SEARCHES TABLE 4 FOR FORM ROLLUP AND MASS
*             MAILING DATES, THEN STORES THOSE DATES IN 3 WORK FILES,
*             WHICH WOULD BE ACCESSED BY SUBSEQUENT PROGRAMS IN ORDER TO
*             RUN MONTHLY MANAGERIAL REPORTS FOR 3 YEARS.
* PROGRAMMER: ROSE MA
* DATE......: 03/31/2005
* HISTORY...:
* 02/26/07  RM  FOR 2006, THERE're multiply form rollups. The dates on
*               TABLE ARE INVALID FOR FORM 1099 AND 5498, NEEDS TO HARD
*               CODE HERE TO OVERRIDE THE WORK FILES.
* 01/06/08  RM  THE 2007 DECEMBER RUN WAS DELAYED TILL JAN 2, 2008,
*               THEREFORE THE 1ST WORK FILE WAS FOR 2007 AND EMPTY.
*               ADD LOGIC TO PREVENT THIS FROM HAPPENNING AGAIN.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp6100 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Pnd_Tax_Yr;

    private DbsGroup pnd_Work_Record_Pnd_Create_Dates;
    private DbsField pnd_Work_Record_Pnd_Form_Id;
    private DbsField pnd_Work_Record_Pnd_Eff_Dte;
    private DbsField pnd_Work_Record_Pnd_Mass_Mail_Dte;
    private DbsField pnd_Work_Record_Pnd_2nd_Mm_Dte;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Curr_Dte;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Curr_Yr;
    private DbsField pnd_Ws_Pnd_Curr_Mm;
    private DbsField pnd_Ws_Pnd_Curr_Dd;
    private DbsField pnd_Ws_Pnd_Y;
    private DbsField pnd_Ws_Pnd_F;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);

        // Local Variables
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 200);

        pnd_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Tax_Yr = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Tax_Yr", "#TAX-YR", FieldType.NUMERIC, 4);

        pnd_Work_Record_Pnd_Create_Dates = pnd_Work_Record__R_Field_1.newGroupArrayInGroup("pnd_Work_Record_Pnd_Create_Dates", "#CREATE-DATES", new DbsArrayController(1, 
            7));
        pnd_Work_Record_Pnd_Form_Id = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Form_Id", "#FORM-ID", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Eff_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Eff_Dte", "#EFF-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Pnd_Mass_Mail_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Mass_Mail_Dte", "#MASS-MAIL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_Pnd_2nd_Mm_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_2nd_Mm_Dte", "#2ND-MM-DTE", FieldType.NUMERIC, 
            8);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Curr_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Curr_Dte", "#CURR-DTE", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Curr_Dte);
        pnd_Ws_Pnd_Curr_Yr = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Curr_Yr", "#CURR-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Curr_Mm = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Curr_Mm", "#CURR-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Curr_Dd = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Curr_Dd", "#CURR-DD", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Y = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 1);
        pnd_Ws_Pnd_F = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_F", "#F", FieldType.PACKED_DECIMAL, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp6100() throws Exception
    {
        super("Twrp6100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 40 LS = 80 KD = ON
        pnd_Ws_Pnd_Curr_Dte.setValue(Global.getDATN());                                                                                                                   //Natural: ASSIGN #CURR-DTE := *DATN
        //*  ADD LOGIC TO PREVENT RUN DATE OVERFLOW                    01/06/08  RM
        if (condition(pnd_Ws_Pnd_Curr_Mm.equals(1) && pnd_Ws_Pnd_Curr_Dd.less(15)))                                                                                       //Natural: IF #CURR-MM = 01 AND #CURR-DD < 15
        {
            pnd_Ws_Pnd_Curr_Yr.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #CURR-YR
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #Y = 1 TO 3
        for (pnd_Ws_Pnd_Y.setValue(1); condition(pnd_Ws_Pnd_Y.lessOrEqual(3)); pnd_Ws_Pnd_Y.nadd(1))
        {
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year()), pnd_Ws_Pnd_Curr_Yr.subtract(pnd_Ws_Pnd_Y)); //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #CURR-YR - #Y
            FOR02:                                                                                                                                                        //Natural: FOR #F = 1 TO 7
            for (pnd_Ws_Pnd_F.setValue(1); condition(pnd_Ws_Pnd_F.lessOrEqual(7)); pnd_Ws_Pnd_F.nadd(1))
            {
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_F);                                                                                        //Natural: ASSIGN #TWRATBL4.#FORM-IND := #F
                DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #RET-CODE
                {
                    //*   02/26/07  RM
                    if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().equals(2006)))                                                                               //Natural: IF #TWRATBL4.#TAX-YEAR = 2006
                    {
                        //*   02/26/07  RM
                        //*   02/26/07  RM
                        short decideConditionsMet135 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #F;//Natural: VALUE 1, 2
                        if (condition((pnd_Ws_Pnd_F.equals(1) || pnd_Ws_Pnd_F.equals(2))))
                        {
                            decideConditionsMet135++;
                            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Eff_Dte().setValue(20070110);                                                                             //Natural: ASSIGN #TWRATBL4.TIRCNTL-EFF-DTE := 20070110
                        }                                                                                                                                                 //Natural: VALUE 4
                        else if (condition((pnd_Ws_Pnd_F.equals(4))))
                        {
                            decideConditionsMet135++;
                            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Eff_Dte().setValue(20070108);                                                                             //Natural: ASSIGN #TWRATBL4.TIRCNTL-EFF-DTE := 20070108
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Record_Pnd_Tax_Yr.setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year());                                                                      //Natural: ASSIGN #TAX-YR := #TWRATBL4.#TAX-YEAR
                    pnd_Work_Record_Pnd_Form_Id.getValue(pnd_Ws_Pnd_F).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind());                                              //Natural: ASSIGN #FORM-ID ( #F ) := #TWRATBL4.#FORM-IND
                    pnd_Work_Record_Pnd_Eff_Dte.getValue(pnd_Ws_Pnd_F).setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Eff_Dte());                                           //Natural: ASSIGN #EFF-DTE ( #F ) := #TWRATBL4.TIRCNTL-EFF-DTE
                    pnd_Work_Record_Pnd_Mass_Mail_Dte.getValue(pnd_Ws_Pnd_F).setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Mass_Mail_Dte());                           //Natural: ASSIGN #MASS-MAIL-DTE ( #F ) := #TWRATBL4.TIRCNTL-RPT-MASS-MAIL-DTE
                    pnd_Work_Record_Pnd_2nd_Mm_Dte.getValue(pnd_Ws_Pnd_F).setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_2nd_Mass_Mail_Dte());                          //Natural: ASSIGN #2ND-MM-DTE ( #F ) := #TWRATBL4.TIRCNTL-RPT-2ND-MASS-MAIL-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "Error when CALLNAT 'TWRNTB4R', Form:",pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind(),"For year:",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), //Natural: WRITE 'Error when CALLNAT "TWRNTB4R", Form:' #TWRATBL4.#FORM-IND 'For year:' #TWRATBL4.#TAX-YEAR / '!!!!!!  Please notify Systems to correct the error  !!!!!!'
                        NEWLINE,"!!!!!!  Please notify Systems to correct the error  !!!!!!");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Ws_Pnd_Y.equals(1)))                                                                                                                        //Natural: IF #Y = 1
            {
                getWorkFiles().write(1, false, pnd_Work_Record);                                                                                                          //Natural: WRITE WORK FILE 01 #WORK-RECORD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Y.equals(2)))                                                                                                                        //Natural: IF #Y = 2
            {
                getWorkFiles().write(2, false, pnd_Work_Record);                                                                                                          //Natural: WRITE WORK FILE 02 #WORK-RECORD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Y.equals(3)))                                                                                                                        //Natural: IF #Y = 3
            {
                getWorkFiles().write(3, false, pnd_Work_Record);                                                                                                          //Natural: WRITE WORK FILE 03 #WORK-RECORD
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, pnd_Work_Record_Pnd_Tax_Yr,NEWLINE,pnd_Work_Record_Pnd_Form_Id.getValue("*"),NEWLINE,pnd_Work_Record_Pnd_Eff_Dte.getValue("*"),         //Natural: WRITE ( 0 ) #TAX-YR / #FORM-ID ( * ) / #EFF-DTE ( * ) / #MASS-MAIL-DTE ( * ) / #2ND-MM-DTE ( * ) //
                NEWLINE,pnd_Work_Record_Pnd_Mass_Mail_Dte.getValue("*"),NEWLINE,pnd_Work_Record_Pnd_2nd_Mm_Dte.getValue("*"),NEWLINE,NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=40 LS=80 KD=ON");
    }
}
