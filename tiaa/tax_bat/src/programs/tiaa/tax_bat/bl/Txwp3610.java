/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:06 PM
**        * FROM NATURAL PROGRAM : Txwp3610
************************************************************
**        * FILE NAME            : Txwp3610.java
**        * CLASS NAME           : Txwp3610
**        * INSTANCE NAME        : Txwp3610
************************************************************
************************************************************************
* PROGRAM   : TXWP3610
* FUNCTION  : THIS PROGRAM WILL DEACTIVATE RECORDS ON THE ELECTION FILE
*             THAT HAVE ELECTION CODES WITH ONE-TIME ELECTIONS.
* AUTHOR    : TED PROIMOS
* DATE      : 10/01
* ----------------------------------------------------------------------
*            MAINTENANCE LOG
*            ---------------
*   DATE   INIT DESCRIPTION
* -------- ---- --------------------------------------------------------
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp3610 extends BLNatBase
{
    // Data Areas
    private LdaTxwl3460 ldaTxwl3460;
    private PdaTwratbl2 pdaTwratbl2;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_elec;
    private DbsField elec_Tin_Type;
    private DbsField elec_Tin;
    private DbsField elec_Contract_Nbr;
    private DbsField elec_Payee_Cde;
    private DbsField elec_Create_User;
    private DbsField elec_Lu_User;
    private DbsField elec_Effective_Ts;
    private DbsField elec_Invrse_Effective_Ts;
    private DbsField elec_Elc_Cde;
    private DbsField elec_Elc_Type;
    private DbsField elec_Elc_Res;
    private DbsField elec_Use_Dte;
    private DbsField elec_Create_Ts;
    private DbsField elec_Lu_Ts;
    private DbsField elec_Active_Ind;
    private DbsField elec_Super_1;

    private DataAccessProgramView vw_elec2;
    private DbsField elec2_Tin_Type;
    private DbsField elec2_Tin;
    private DbsField elec2_Contract_Nbr;
    private DbsField elec2_Payee_Cde;
    private DbsField elec2_Create_User;
    private DbsField elec2_Lu_User;
    private DbsField elec2_Effective_Ts;
    private DbsField elec2_Invrse_Effective_Ts;
    private DbsField elec2_Elc_Cde;
    private DbsField elec2_Elc_Type;
    private DbsField elec2_Elc_Res;
    private DbsField elec2_Use_Dte;
    private DbsField elec2_Create_Ts;
    private DbsField elec2_Lu_Ts;
    private DbsField elec2_Active_Ind;
    private DbsField elec2_Super_1;

    private DataAccessProgramView vw_elec3;
    private DbsField elec3_Tin_Type;
    private DbsField elec3_Tin;
    private DbsField elec3_Contract_Nbr;
    private DbsField elec3_Payee_Cde;
    private DbsField elec3_Create_User;
    private DbsField elec3_Lu_User;
    private DbsField elec3_Effective_Ts;
    private DbsField elec3_Invrse_Effective_Ts;
    private DbsField elec3_Elc_Cde;
    private DbsField elec3_Elc_Type;
    private DbsField elec3_Elc_Res;
    private DbsField elec3_Use_Dte;
    private DbsField elec3_Create_Ts;
    private DbsField elec3_Lu_Ts;
    private DbsField elec3_Active_Ind;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Low_Val;
    private DbsField pnd_Ws_Const_Pnd_High_Val;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_S5_Start;
    private DbsField pnd_Ws_Pnd_S5_End;
    private DbsField pnd_Ws_Pnd_Ts;
    private DbsField pnd_Ws_Pnd_Ts_X;
    private DbsField pnd_Ws_Pnd_Hold_Ts;
    private DbsField pnd_Ws_Pnd_Hold_Super_1;
    private DbsField pnd_Ws_Pnd_Hold_Sec_Super_1;
    private DbsField pnd_Ws_Pnd_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Hold_Tin;
    private DbsField pnd_Ws_Pnd_Hold_Tin_Type;
    private DbsField pnd_Ws_Pnd_Hold_Contract_Nbr;
    private DbsField pnd_Ws_Pnd_Hold_Payee_Cde;
    private DbsField pnd_Ws_Pnd_Hold_Active_Ind;
    private DbsField pnd_Ws_Pnd_Hold_Elc_Cde;
    private DbsField pnd_Ws_Pnd_Hold_Type;
    private DbsField pnd_Ws_Pnd_Hold_Elc_Res;
    private DbsField pnd_Ws_Pnd_Hold_Invrse_Eff_Ts;
    private DbsField pnd_Ws_Pnd_Isn;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Ii;
    private DbsField pnd_Ws_Pnd_Cntr;
    private DbsField pnd_Ws_Pnd_Found_In_Table;
    private DbsField pnd_Ws_Pnd_First_Time;
    private DbsField pnd_Ws_Dat1;
    private DbsField pnd_Ws_Dat2;
    private DbsField pnd_Ws_Dat3;
    private DbsField pnd_Ws_Dat4;
    private DbsField pnd_Ws_Pnd_Eff_Field_Yyyymmdd;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Eff_Field_Num_Yyyymmdd;
    private DbsField pnd_Ws_Pnd_Use_Field_Mmddyyyy;
    private DbsField pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Date_Cc;
    private DbsField pnd_Ws_Pnd_Date_Yy;
    private DbsField pnd_Ws_Pnd_Date_Mm;
    private DbsField pnd_Ws_Pnd_Date_Dd;
    private DbsField pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc;
    private DbsField pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc1;
    private DbsField pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc2;
    private DbsField pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc3;
    private DbsField pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc4;
    private DbsField pnd_Ws_Pnd_Save_Election_Cd;
    private DbsField pnd_Ws_Pnd_Tab_Election_Cd;
    private DbsField pnd_Ws_Pnd_Tab_One_Time_Elc;

    private DbsGroup pnd_Ws_Pnd_State_Code_Ws;
    private DbsField pnd_Ws_Pnd_State_Code_Zero;
    private DbsField pnd_Ws_Pnd_State_Code_Rest;

    private DbsGroup pnd_Ws__R_Field_4;
    private DbsField pnd_Ws_Pnd_State_Code_Red;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTxwl3460 = new LdaTxwl3460();
        registerRecord(ldaTxwl3460);
        registerRecord(ldaTxwl3460.getVw_txwl3460());
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);

        // Local Variables

        vw_elec = new DataAccessProgramView(new NameInfo("vw_elec", "ELEC"), "TWR_ELECTION", "TWR_ELECTION");
        elec_Tin_Type = vw_elec.getRecord().newFieldInGroup("elec_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIN_TYPE");
        elec_Tin_Type.setDdmHeader("TIN/TYPE");
        elec_Tin = vw_elec.getRecord().newFieldInGroup("elec_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        elec_Tin.setDdmHeader("TAX/ID/NUMBER");
        elec_Contract_Nbr = vw_elec.getRecord().newFieldInGroup("elec_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        elec_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        elec_Payee_Cde = vw_elec.getRecord().newFieldInGroup("elec_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAYEE_CDE");
        elec_Payee_Cde.setDdmHeader("PAYEE/CODE");
        elec_Create_User = vw_elec.getRecord().newFieldInGroup("elec_Create_User", "CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CREATE_USER");
        elec_Create_User.setDdmHeader("CREATE/USER/ID");
        elec_Lu_User = vw_elec.getRecord().newFieldInGroup("elec_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LU_USER");
        elec_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        elec_Effective_Ts = vw_elec.getRecord().newFieldInGroup("elec_Effective_Ts", "EFFECTIVE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "EFFECTIVE_TS");
        elec_Effective_Ts.setDdmHeader("EFFECTIVE/TIMESTAMP");
        elec_Invrse_Effective_Ts = vw_elec.getRecord().newFieldInGroup("elec_Invrse_Effective_Ts", "INVRSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 13, 
            RepeatingFieldStrategy.None, "INVRSE_EFFECTIVE_TS");
        elec_Invrse_Effective_Ts.setDdmHeader("INVERSE/EFFECTIVE/TIMESTAMP");
        elec_Elc_Cde = vw_elec.getRecord().newFieldInGroup("elec_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ELC_CDE");
        elec_Elc_Cde.setDdmHeader("ELC/CDE");
        elec_Elc_Type = vw_elec.getRecord().newFieldInGroup("elec_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELC_TYPE");
        elec_Elc_Type.setDdmHeader("ELC/TYP");
        elec_Elc_Res = vw_elec.getRecord().newFieldInGroup("elec_Elc_Res", "ELC-RES", FieldType.STRING, 3, RepeatingFieldStrategy.None, "ELC_RES");
        elec_Elc_Res.setDdmHeader("ELC/RES");
        elec_Use_Dte = vw_elec.getRecord().newFieldInGroup("elec_Use_Dte", "USE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "USE_DTE");
        elec_Use_Dte.setDdmHeader("1ST/USE/DATE");
        elec_Create_Ts = vw_elec.getRecord().newFieldInGroup("elec_Create_Ts", "CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "CREATE_TS");
        elec_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        elec_Lu_Ts = vw_elec.getRecord().newFieldInGroup("elec_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        elec_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        elec_Active_Ind = vw_elec.getRecord().newFieldInGroup("elec_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ACTIVE_IND");
        elec_Active_Ind.setDdmHeader("ACT/IND");
        elec_Super_1 = vw_elec.getRecord().newFieldInGroup("elec_Super_1", "SUPER-1", FieldType.BINARY, 38, RepeatingFieldStrategy.None, "SUPER_1");
        elec_Super_1.setSuperDescriptor(true);
        registerRecord(vw_elec);

        vw_elec2 = new DataAccessProgramView(new NameInfo("vw_elec2", "ELEC2"), "TWR_ELECTION", "TWR_ELECTION");
        elec2_Tin_Type = vw_elec2.getRecord().newFieldInGroup("elec2_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIN_TYPE");
        elec2_Tin_Type.setDdmHeader("TIN/TYPE");
        elec2_Tin = vw_elec2.getRecord().newFieldInGroup("elec2_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        elec2_Tin.setDdmHeader("TAX/ID/NUMBER");
        elec2_Contract_Nbr = vw_elec2.getRecord().newFieldInGroup("elec2_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        elec2_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        elec2_Payee_Cde = vw_elec2.getRecord().newFieldInGroup("elec2_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAYEE_CDE");
        elec2_Payee_Cde.setDdmHeader("PAYEE/CODE");
        elec2_Create_User = vw_elec2.getRecord().newFieldInGroup("elec2_Create_User", "CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CREATE_USER");
        elec2_Create_User.setDdmHeader("CREATE/USER/ID");
        elec2_Lu_User = vw_elec2.getRecord().newFieldInGroup("elec2_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LU_USER");
        elec2_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        elec2_Effective_Ts = vw_elec2.getRecord().newFieldInGroup("elec2_Effective_Ts", "EFFECTIVE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "EFFECTIVE_TS");
        elec2_Effective_Ts.setDdmHeader("EFFECTIVE/TIMESTAMP");
        elec2_Invrse_Effective_Ts = vw_elec2.getRecord().newFieldInGroup("elec2_Invrse_Effective_Ts", "INVRSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_EFFECTIVE_TS");
        elec2_Invrse_Effective_Ts.setDdmHeader("INVERSE/EFFECTIVE/TIMESTAMP");
        elec2_Elc_Cde = vw_elec2.getRecord().newFieldInGroup("elec2_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ELC_CDE");
        elec2_Elc_Cde.setDdmHeader("ELC/CDE");
        elec2_Elc_Type = vw_elec2.getRecord().newFieldInGroup("elec2_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELC_TYPE");
        elec2_Elc_Type.setDdmHeader("ELC/TYP");
        elec2_Elc_Res = vw_elec2.getRecord().newFieldInGroup("elec2_Elc_Res", "ELC-RES", FieldType.STRING, 3, RepeatingFieldStrategy.None, "ELC_RES");
        elec2_Elc_Res.setDdmHeader("ELC/RES");
        elec2_Use_Dte = vw_elec2.getRecord().newFieldInGroup("elec2_Use_Dte", "USE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "USE_DTE");
        elec2_Use_Dte.setDdmHeader("1ST/USE/DATE");
        elec2_Create_Ts = vw_elec2.getRecord().newFieldInGroup("elec2_Create_Ts", "CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "CREATE_TS");
        elec2_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        elec2_Lu_Ts = vw_elec2.getRecord().newFieldInGroup("elec2_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        elec2_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        elec2_Active_Ind = vw_elec2.getRecord().newFieldInGroup("elec2_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ACTIVE_IND");
        elec2_Active_Ind.setDdmHeader("ACT/IND");
        elec2_Super_1 = vw_elec2.getRecord().newFieldInGroup("elec2_Super_1", "SUPER-1", FieldType.BINARY, 38, RepeatingFieldStrategy.None, "SUPER_1");
        elec2_Super_1.setSuperDescriptor(true);
        registerRecord(vw_elec2);

        vw_elec3 = new DataAccessProgramView(new NameInfo("vw_elec3", "ELEC3"), "TWR_ELECTION", "TWR_ELECTION");
        elec3_Tin_Type = vw_elec3.getRecord().newFieldInGroup("elec3_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIN_TYPE");
        elec3_Tin_Type.setDdmHeader("TIN/TYPE");
        elec3_Tin = vw_elec3.getRecord().newFieldInGroup("elec3_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        elec3_Tin.setDdmHeader("TAX/ID/NUMBER");
        elec3_Contract_Nbr = vw_elec3.getRecord().newFieldInGroup("elec3_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        elec3_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        elec3_Payee_Cde = vw_elec3.getRecord().newFieldInGroup("elec3_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "PAYEE_CDE");
        elec3_Payee_Cde.setDdmHeader("PAYEE/CODE");
        elec3_Create_User = vw_elec3.getRecord().newFieldInGroup("elec3_Create_User", "CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CREATE_USER");
        elec3_Create_User.setDdmHeader("CREATE/USER/ID");
        elec3_Lu_User = vw_elec3.getRecord().newFieldInGroup("elec3_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LU_USER");
        elec3_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        elec3_Effective_Ts = vw_elec3.getRecord().newFieldInGroup("elec3_Effective_Ts", "EFFECTIVE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "EFFECTIVE_TS");
        elec3_Effective_Ts.setDdmHeader("EFFECTIVE/TIMESTAMP");
        elec3_Invrse_Effective_Ts = vw_elec3.getRecord().newFieldInGroup("elec3_Invrse_Effective_Ts", "INVRSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "INVRSE_EFFECTIVE_TS");
        elec3_Invrse_Effective_Ts.setDdmHeader("INVERSE/EFFECTIVE/TIMESTAMP");
        elec3_Elc_Cde = vw_elec3.getRecord().newFieldInGroup("elec3_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ELC_CDE");
        elec3_Elc_Cde.setDdmHeader("ELC/CDE");
        elec3_Elc_Type = vw_elec3.getRecord().newFieldInGroup("elec3_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ELC_TYPE");
        elec3_Elc_Type.setDdmHeader("ELC/TYP");
        elec3_Elc_Res = vw_elec3.getRecord().newFieldInGroup("elec3_Elc_Res", "ELC-RES", FieldType.STRING, 3, RepeatingFieldStrategy.None, "ELC_RES");
        elec3_Elc_Res.setDdmHeader("ELC/RES");
        elec3_Use_Dte = vw_elec3.getRecord().newFieldInGroup("elec3_Use_Dte", "USE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "USE_DTE");
        elec3_Use_Dte.setDdmHeader("1ST/USE/DATE");
        elec3_Create_Ts = vw_elec3.getRecord().newFieldInGroup("elec3_Create_Ts", "CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, "CREATE_TS");
        elec3_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        elec3_Lu_Ts = vw_elec3.getRecord().newFieldInGroup("elec3_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        elec3_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        elec3_Active_Ind = vw_elec3.getRecord().newFieldInGroup("elec3_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ACTIVE_IND");
        elec3_Active_Ind.setDdmHeader("ACT/IND");
        registerRecord(vw_elec3);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Low_Val = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Low_Val", "#LOW-VAL", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_High_Val = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_High_Val", "#HIGH-VAL", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_S5_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S5_Start", "#S5-START", FieldType.STRING, 34);
        pnd_Ws_Pnd_S5_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S5_End", "#S5-END", FieldType.STRING, 27);
        pnd_Ws_Pnd_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ts", "#TS", FieldType.TIME);
        pnd_Ws_Pnd_Ts_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ts_X", "#TS-X", FieldType.STRING, 15);
        pnd_Ws_Pnd_Hold_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Ts", "#HOLD-TS", FieldType.STRING, 15);
        pnd_Ws_Pnd_Hold_Super_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Super_1", "#HOLD-SUPER-1", FieldType.STRING, 31);
        pnd_Ws_Pnd_Hold_Sec_Super_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Sec_Super_1", "#HOLD-SEC-SUPER-1", FieldType.STRING, 31);
        pnd_Ws_Pnd_Key = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Key", "#KEY", FieldType.STRING, 38);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Key);
        pnd_Ws_Pnd_Hold_Tin = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Tin", "#HOLD-TIN", FieldType.STRING, 10);
        pnd_Ws_Pnd_Hold_Tin_Type = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Tin_Type", "#HOLD-TIN-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Hold_Contract_Nbr = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Contract_Nbr", "#HOLD-CONTRACT-NBR", FieldType.STRING, 10);
        pnd_Ws_Pnd_Hold_Payee_Cde = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Payee_Cde", "#HOLD-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Hold_Active_Ind = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Active_Ind", "#HOLD-ACTIVE-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_Hold_Elc_Cde = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Elc_Cde", "#HOLD-ELC-CDE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Hold_Type = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Type", "#HOLD-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Hold_Elc_Res = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Elc_Res", "#HOLD-ELC-RES", FieldType.STRING, 3);
        pnd_Ws_Pnd_Hold_Invrse_Eff_Ts = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Invrse_Eff_Ts", "#HOLD-INVRSE-EFF-TS", FieldType.PACKED_DECIMAL, 
            13);
        pnd_Ws_Pnd_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 13);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Ii = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ii", "#II", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Cntr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntr", "#CNTR", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Found_In_Table = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Found_In_Table", "#FOUND-IN-TABLE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_First_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Time", "#FIRST-TIME", FieldType.STRING, 1);
        pnd_Ws_Dat1 = pnd_Ws.newFieldInGroup("pnd_Ws_Dat1", "DAT1", FieldType.DATE);
        pnd_Ws_Dat2 = pnd_Ws.newFieldInGroup("pnd_Ws_Dat2", "DAT2", FieldType.DATE);
        pnd_Ws_Dat3 = pnd_Ws.newFieldInGroup("pnd_Ws_Dat3", "DAT3", FieldType.DATE);
        pnd_Ws_Dat4 = pnd_Ws.newFieldInGroup("pnd_Ws_Dat4", "DAT4", FieldType.DATE);
        pnd_Ws_Pnd_Eff_Field_Yyyymmdd = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Eff_Field_Yyyymmdd", "#EFF-FIELD-YYYYMMDD", FieldType.STRING, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Eff_Field_Yyyymmdd);
        pnd_Ws_Pnd_Eff_Field_Num_Yyyymmdd = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Eff_Field_Num_Yyyymmdd", "#EFF-FIELD-NUM-YYYYMMDD", FieldType.NUMERIC, 
            8);
        pnd_Ws_Pnd_Use_Field_Mmddyyyy = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Use_Field_Mmddyyyy", "#USE-FIELD-MMDDYYYY", FieldType.STRING, 8);
        pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref", "#DATE-FIELD-YYYYMMDD-REF", FieldType.NUMERIC, 
            8);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref);
        pnd_Ws_Pnd_Date_Cc = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Date_Yy = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Date_Mm = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Date_Dd = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc", "#DATE-FIELD-YYYYMMDD-ELC", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc1", "#DATE-FIELD-MMDDYYYY-ELC1", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc2", "#DATE-FIELD-MMDDYYYY-ELC2", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc3 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc3", "#DATE-FIELD-MMDDYYYY-ELC3", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc4 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date_Field_Mmddyyyy_Elc4", "#DATE-FIELD-MMDDYYYY-ELC4", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Save_Election_Cd = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Save_Election_Cd", "#SAVE-ELECTION-CD", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Tab_Election_Cd = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Tab_Election_Cd", "#TAB-ELECTION-CD", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            50));
        pnd_Ws_Pnd_Tab_One_Time_Elc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Tab_One_Time_Elc", "#TAB-ONE-TIME-ELC", FieldType.STRING, 1, new DbsArrayController(1, 
            50));

        pnd_Ws_Pnd_State_Code_Ws = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_State_Code_Ws", "#STATE-CODE-WS");
        pnd_Ws_Pnd_State_Code_Zero = pnd_Ws_Pnd_State_Code_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Code_Zero", "#STATE-CODE-ZERO", FieldType.STRING, 1);
        pnd_Ws_Pnd_State_Code_Rest = pnd_Ws_Pnd_State_Code_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Code_Rest", "#STATE-CODE-REST", FieldType.STRING, 2);

        pnd_Ws__R_Field_4 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_4", "REDEFINE", pnd_Ws_Pnd_State_Code_Ws);
        pnd_Ws_Pnd_State_Code_Red = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws_Pnd_State_Code_Red", "#STATE-CODE-RED", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_elec.reset();
        vw_elec2.reset();
        vw_elec3.reset();

        ldaTxwl3460.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Low_Val.setInitialValue("H'00'");
        pnd_Ws_Const_Pnd_High_Val.setInitialValue("H'FF'");
        pnd_Ws_Pnd_S5_Start.setInitialValue("TX002A");
        pnd_Ws_Pnd_First_Time.setInitialValue("T");
        pnd_Ws_Pnd_State_Code_Zero.setInitialValue("0");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp3610() throws Exception
    {
        super("Txwp3610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 01 ) PS = 55 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 55 LS = 133 ZP = ON;//Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 58T 'TAXWARS' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'DEACTIVATING ONE-TIME ELECTIONS              '
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        //*  MOVE 20010917  TO  #DATE-FIELD-YYYYMMDD-REF
        //*  MOVE 20010917  TO  #DATE-FIELD-YYYYMMDD-ELC
        //* ****************************************************************
        //* ** CREATE DATE RANGES FOR ELECTION TABLE AND ELECTION FILE *****
        //* ****************************************************************
        pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref.setValue(Global.getDATN());                                                                                                    //Natural: MOVE *DATN TO #DATE-FIELD-YYYYMMDD-REF
        pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc.setValue(Global.getDATN());                                                                                                    //Natural: MOVE *DATN TO #DATE-FIELD-YYYYMMDD-ELC
        pnd_Ws_Dat1.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc);                                                                    //Natural: MOVE EDITED #DATE-FIELD-YYYYMMDD-ELC TO DAT1 ( EM = YYYYMMDD )
        pnd_Ws_Dat2.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_Field_Yyyymmdd_Elc);                                                                    //Natural: MOVE EDITED #DATE-FIELD-YYYYMMDD-ELC TO DAT2 ( EM = YYYYMMDD )
        pnd_Ws_Dat2.nsubtract(4);                                                                                                                                         //Natural: SUBTRACT 4 FROM DAT2
        //* *******************************************************************
        //* ** READ ELECTION TABLE AND LOAD ELECTION CODES WITH ONE-TIME ELEC *
        //* *******************************************************************
        ldaTxwl3460.getVw_txwl3460().startDatabaseRead                                                                                                                    //Natural: READ TXWL3460 BY RT-SUPER5 STARTING FROM #WS.#S5-START
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER5", ">=", pnd_Ws_Pnd_S5_Start, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER5", "ASC") }
        );
        READ01:
        while (condition(ldaTxwl3460.getVw_txwl3460().readNextRow("READ01")))
        {
            //*  AT START OF DATA
            //*     MOVE TXWL3460.#ELECTION-CD  TO  #SAVE-ELECTION-CD
            //*  END-START
            if (condition(!ldaTxwl3460.getTxwl3460_Rt_Super5().getSubstring(1,6).equals("TX002A")))                                                                       //Natural: IF SUBSTR ( RT-SUPER5,1,6 ) NE 'TX002A'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Eff_Field_Yyyymmdd.setValueEdited(ldaTxwl3460.getTxwl3460_Pnd_Eff_Ts(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED TXWL3460.#EFF-TS ( EM = YYYYMMDD ) TO #EFF-FIELD-YYYYMMDD
            if (condition(pnd_Ws_Pnd_Eff_Field_Num_Yyyymmdd.greater(pnd_Ws_Pnd_Date_Field_Yyyymmdd_Ref)))                                                                 //Natural: IF #EFF-FIELD-NUM-YYYYMMDD GT #DATE-FIELD-YYYYMMDD-REF
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Save_Election_Cd.equals(ldaTxwl3460.getTxwl3460_Pnd_Election_Cd())))                                                                 //Natural: IF #SAVE-ELECTION-CD EQ TXWL3460.#ELECTION-CD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_I.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #I
                pnd_Ws_Pnd_Tab_Election_Cd.getValue(pnd_Ws_Pnd_I).setValue(ldaTxwl3460.getTxwl3460_Pnd_Election_Cd());                                                    //Natural: MOVE TXWL3460.#ELECTION-CD TO #TAB-ELECTION-CD ( #I )
                pnd_Ws_Pnd_Tab_One_Time_Elc.getValue(pnd_Ws_Pnd_I).setValue(ldaTxwl3460.getTxwl3460_Pnd_One_Time_Elc());                                                  //Natural: MOVE TXWL3460.#ONE-TIME-ELC TO #TAB-ONE-TIME-ELC ( #I )
                pnd_Ws_Pnd_Save_Election_Cd.setValue(ldaTxwl3460.getTxwl3460_Pnd_Election_Cd());                                                                          //Natural: MOVE TXWL3460.#ELECTION-CD TO #SAVE-ELECTION-CD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *********************************************************************
        //* * READ ELECTION FILE FOR ACTIVE REC AND USE-DATE EQUAL TO DATE RANGE*
        //* *********************************************************************
        vw_elec.startDatabaseRead                                                                                                                                         //Natural: READ ELEC BY USE-DTE EQ DAT2 THRU DAT1 WHERE ACTIVE-IND = 'A'
        (
        "READ02",
        new Wc[] { new Wc("ACTIVE_IND", "=", "A", WcType.WHERE) ,
        new Wc("USE_DTE", ">=", pnd_Ws_Dat2, "And", WcType.BY) ,
        new Wc("USE_DTE", "<=", pnd_Ws_Dat1, WcType.BY) },
        new Oc[] { new Oc("USE_DTE", "ASC") }
        );
        READ02:
        while (condition(vw_elec.readNextRow("READ02")))
        {
            pnd_Ws_Pnd_Isn.setValue(vw_elec.getAstISN("Read02"));                                                                                                         //Natural: MOVE *ISN TO #ISN
                                                                                                                                                                          //Natural: PERFORM TABLE-LOOKUP
            sub_Table_Lookup();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *****************************
        //*  S U B R O U T I N E S      *
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TABLE-LOOKUP
        //* *****************************
        //* ***************************************************
        //* * THIS ROUTINE WILL SEARCH ARRAY FOR ELECTION CODE*
        //* ***************************************************
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RTN
        //* *****************************************************
        //* *****************************************************
        //* * THIS ROUTINE WILL DEACTIVATE ELECTION FILE RECORD *
        //* *****************************************************
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PREVIOUS-RECS-RTN
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-CODE
        //* ********************************************
        //* ****************************************************************
        //* * THIS ROUTINE WILL GET STATE CODE                             *
        //* ****************************************************************
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPT-RTN
        //* ********************************************
        //* ****************************************************************
        //* * THIS ROUTINE WILL REPORT ON ALL RECORDS THAT ARE DEACTIVATED *
        //* ****************************************************************
        //* ** '//ELC/RES'            ELEC3.ELC-RES                 (LC=�)
        //* ** '//ACTIVE/IND'         ELEC3.ACTIVE-IND              (LC=�)
        //* ** '//INVRSE/DATE'        ELEC3.INVRSE-EFFECTIVE-TS     (LC=�)
    }
    private void sub_Table_Lookup() throws Exception                                                                                                                      //Natural: TABLE-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #II = 1 TO #I
        for (pnd_Ws_Pnd_Ii.setValue(1); condition(pnd_Ws_Pnd_Ii.lessOrEqual(pnd_Ws_Pnd_I)); pnd_Ws_Pnd_Ii.nadd(1))
        {
            if (condition(elec_Elc_Cde.equals(pnd_Ws_Pnd_Tab_Election_Cd.getValue(pnd_Ws_Pnd_Ii)) && pnd_Ws_Pnd_Tab_One_Time_Elc.getValue(pnd_Ws_Pnd_Ii).equals("Y")))    //Natural: IF ELEC.ELC-CDE = #TAB-ELECTION-CD ( #II ) AND #TAB-ONE-TIME-ELC ( #II ) = 'Y'
            {
                pnd_Ws_Pnd_Hold_Super_1.setValue(elec_Super_1);                                                                                                           //Natural: MOVE ELEC.SUPER-1 TO #HOLD-SUPER-1
                pnd_Ws_Pnd_Hold_Tin.setValue(elec_Tin);                                                                                                                   //Natural: MOVE ELEC.TIN TO #HOLD-TIN
                pnd_Ws_Pnd_Hold_Tin_Type.setValue(elec_Tin_Type);                                                                                                         //Natural: MOVE ELEC.TIN-TYPE TO #HOLD-TIN-TYPE
                pnd_Ws_Pnd_Hold_Contract_Nbr.setValue(elec_Contract_Nbr);                                                                                                 //Natural: MOVE ELEC.CONTRACT-NBR TO #HOLD-CONTRACT-NBR
                pnd_Ws_Pnd_Hold_Payee_Cde.setValue(elec_Payee_Cde);                                                                                                       //Natural: MOVE ELEC.PAYEE-CDE TO #HOLD-PAYEE-CDE
                pnd_Ws_Pnd_Hold_Active_Ind.setValue(elec_Active_Ind);                                                                                                     //Natural: MOVE ELEC.ACTIVE-IND TO #HOLD-ACTIVE-IND
                pnd_Ws_Pnd_Hold_Elc_Cde.setValue(elec_Elc_Cde);                                                                                                           //Natural: MOVE ELEC.ELC-CDE TO #HOLD-ELC-CDE
                pnd_Ws_Pnd_Hold_Type.setValue(elec_Elc_Type);                                                                                                             //Natural: MOVE ELEC.ELC-TYPE TO #HOLD-TYPE
                pnd_Ws_Pnd_Hold_Elc_Res.setValue(elec_Elc_Res);                                                                                                           //Natural: MOVE ELEC.ELC-RES TO #HOLD-ELC-RES
                pnd_Ws_Pnd_Hold_Invrse_Eff_Ts.setValue(elec_Invrse_Effective_Ts);                                                                                         //Natural: MOVE ELEC.INVRSE-EFFECTIVE-TS TO #HOLD-INVRSE-EFF-TS
                                                                                                                                                                          //Natural: PERFORM UPDATE-RTN
                sub_Update_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-STATE-CODE
                sub_Get_State_Code();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM REPT-RTN
                sub_Rept_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CHECK-PREVIOUS-RECS-RTN
                sub_Check_Previous_Recs_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_Rtn() throws Exception                                                                                                                        //Natural: UPDATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L2170:                                                                                                                                                    //Natural: GET ELEC3 #ISN
        vw_elec3.readByID(pnd_Ws_Pnd_Isn.getLong(), "PND_PND_L2170");
        elec3_Active_Ind.setValue(" ");                                                                                                                                   //Natural: MOVE ' ' TO ELEC3.ACTIVE-IND
        elec3_Lu_Ts.setValue(Global.getTIMX());                                                                                                                           //Natural: MOVE *TIMX TO ELEC3.LU-TS
        elec3_Lu_User.setValue(Global.getUSER());                                                                                                                         //Natural: MOVE *USER TO ELEC3.LU-USER
        vw_elec3.updateDBRow("PND_PND_L2170");                                                                                                                            //Natural: UPDATE ( ##L2170. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Check_Previous_Recs_Rtn() throws Exception                                                                                                           //Natural: CHECK-PREVIOUS-RECS-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        //* ***************************************************
        //* * THIS ROUTINE WILL DEACTIVATE PRIOR RECORDS      *
        //* ***************************************************
        vw_elec2.startDatabaseRead                                                                                                                                        //Natural: READ ELEC2 BY SUPER-1 STARTING FROM #KEY
        (
        "READ03",
        new Wc[] { new Wc("SUPER_1", ">=", pnd_Ws_Pnd_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("SUPER_1", "ASC") }
        );
        READ03:
        while (condition(vw_elec2.readNextRow("READ03")))
        {
            if (condition(elec2_Super_1.equals(pnd_Ws_Pnd_Key)))                                                                                                          //Natural: IF SUPER-1 = #KEY
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Hold_Sec_Super_1.setValue(elec2_Super_1);                                                                                                          //Natural: MOVE SUPER-1 TO #HOLD-SEC-SUPER-1
            if (condition(pnd_Ws_Pnd_Hold_Super_1.equals(pnd_Ws_Pnd_Hold_Sec_Super_1)))                                                                                   //Natural: IF #HOLD-SUPER-1 = #HOLD-SEC-SUPER-1
            {
                pnd_Ws_Pnd_Isn.setValue(vw_elec2.getAstISN("Read03"));                                                                                                    //Natural: MOVE *ISN TO #ISN
                                                                                                                                                                          //Natural: PERFORM UPDATE-RTN
                sub_Update_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-STATE-CODE
                sub_Get_State_Code();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM REPT-RTN
                sub_Rept_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_State_Code() throws Exception                                                                                                                    //Natural: GET-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), Global.getDATN().divide(10000));             //Natural: ASSIGN TWRATBL2.#TAX-YEAR := *DATN / 10000
        if (condition(elec3_Elc_Res.equals("   ")))                                                                                                                       //Natural: IF ELEC3.ELC-RES = '   '
        {
            pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code().setValue("   ");                                                                                           //Natural: MOVE '   ' TO TIRCNTL-STATE-ALPHA-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_State_Code_Rest.setValue(elec3_Elc_Res.getSubstring(1,2));                                                                                         //Natural: MOVE SUBSTR ( ELEC3.ELC-RES,1,2 ) TO #STATE-CODE-REST
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_Ws_Pnd_State_Code_Red);                                                                                  //Natural: MOVE #STATE-CODE-RED TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code().setValue("   ");                                                                                       //Natural: MOVE '   ' TO TIRCNTL-STATE-ALPHA-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rept_Rtn() throws Exception                                                                                                                          //Natural: REPT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "//TIN/TYP",                                                                                                                              //Natural: DISPLAY ( 1 ) '//TIN/TYP' ELEC3.TIN-TYPE ( LC = � ) '///TIN' ELEC3.TIN '//CONTRACT/PAYEE' ELEC3.CONTRACT-NBR ( AL = 10 ) '//ELC/CODE' ELEC3.ELC-CDE ( LC = � ) '//PAYEE/CODE' ELEC3.PAYEE-CDE ( LC = � ) '//ELC/TYPE' ELEC3.ELC-TYPE ( LC = � ) '//ELC/RES' TIRCNTL-STATE-ALPHA-CODE ( LC = � ) '//USE/DATE' ELEC3.USE-DTE ( LC = � ) '//EFF/DATE' ELEC3.EFFECTIVE-TS ( LC = � )
        		elec3_Tin_Type, new FieldAttributes("LC=�"),"///TIN",
        		elec3_Tin,"//CONTRACT/PAYEE",
        		elec3_Contract_Nbr, new AlphanumericLength (10),"//ELC/CODE",
        		elec3_Elc_Cde, new FieldAttributes("LC=�"),"//PAYEE/CODE",
        		elec3_Payee_Cde, new FieldAttributes("LC=�"),"//ELC/TYPE",
        		elec3_Elc_Type, new FieldAttributes("LC=�"),"//ELC/RES",
        		pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(), new FieldAttributes("LC=�"),"//USE/DATE",
        		elec3_Use_Dte, new FieldAttributes("LC=�"),"//EFF/DATE",
        		elec3_Effective_Ts, new FieldAttributes("LC=�"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1 LINES
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=133 ZP=ON");
        Global.format(2, "PS=55 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(58),"TAXWARS",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"DEACTIVATING ONE-TIME ELECTIONS              ");

        getReports().setDisplayColumns(1, "//TIN/TYP",
        		elec3_Tin_Type, new FieldAttributes("LC=�"),"///TIN",
        		elec3_Tin,"//CONTRACT/PAYEE",
        		elec3_Contract_Nbr, new AlphanumericLength (10),"//ELC/CODE",
        		elec3_Elc_Cde, new FieldAttributes("LC=�"),"//PAYEE/CODE",
        		elec3_Payee_Cde, new FieldAttributes("LC=�"),"//ELC/TYPE",
        		elec3_Elc_Type, new FieldAttributes("LC=�"),"//ELC/RES",
        		pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code(), new FieldAttributes("LC=�"),"//USE/DATE",
        		elec3_Use_Dte, new FieldAttributes("LC=�"),"//EFF/DATE",
        		elec3_Effective_Ts, new FieldAttributes("LC=�"));
    }
}
