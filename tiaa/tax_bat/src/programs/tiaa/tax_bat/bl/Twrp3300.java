/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:02 PM
**        * FROM NATURAL PROGRAM : Twrp3300
************************************************************
**        * FILE NAME            : Twrp3300.java
**        * CLASS NAME           : Twrp3300
**        * INSTANCE NAME        : Twrp3300
************************************************************
*******************************************************************
*                                                                 *
*      PROJECT  - IRS ORIGINAL MAGNETIC MEDIA REPORTING           *
*                                                                 *
*      TWRP3300 - READ ADABAS FILE 097                            *
*                 AND CREATE EXTRACT FILE                         *
*                                                                 *
* HISTORY: J.ROTHOLZ 07/2002 - ADD LOGIC FOR TCII; ALSO ADDED     *
*            TCII FIELDS TO TABLE 4                               *
*                                                                 *
* 09/23/03 RM - 2003 1099 & 5498 SDR                              *
*               DISTRIBUTION CODE CONVERTED FROM 1 BYTE TO 2 BYTES*
*               USING TWRNDIST.                                   *
* 12/02/04 DA - MODIFIED FOR NEW IRS UNIQUE ID FOR ORIGINAL TRANS.*
* 12/02/04 DA - MODIFIED FOR TRUST AND TIAA COMPANY CODES.        *
* 04/18/05 MS - NEW PROVINCE CODE 88 - NU                         *
* 04/14/06 AY - CORRECT RECONCILIATION TOTALS BY REPLACING        *
*               #TCII-I AND #TCII-R WITH #TRST-I AND #TRST-R.     *
* 10/12/06 RM - CHANGE LDA TWRL3300 TO ADD A NEW FIELD - ROTH-YEAR*
*               FOR 2006 IRS LAYOUT.                              *
*               UPDATE THIS PGM TO POPULATE THE NEW FIELD.        *
* 12/05/06 JR - FIX ACCUMULATIONS FOR NEGATIVE AMOUNTS            *
* 01/30/08 AY - REVISED TO ACCOMMODATE FORM 480.7C, WHICH REPLACES*
*               FORMS 480.6A AND 480.6B AS OF TAX YEAR 2007.      *
*               REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.       *
* 10/18/11  JB NON-ERISA TDA - ADD COMPANY CODE 'F' SCAN JB1011  *
* 10/30/11 RC - COMPLIANCE CHANGES
* 11/27/13 RC - RECOMPILED - TWRL3300 CHANGES.
*               EXTRACT-TAXABLE-AMT EXPANDED FROM 7.2 TO 9.2
* 02/20/15 DUTTAD - RECOMPILED - FOR TWRL3300 RECOMPILATION.
* 04/01/15 MUKHERR- ADDED LOGIC TO WRITE THE CONTRACT-PAYEE PART  *
*                   IN IRS-FILE TO REPORT TO IRS INSTEAD OF REJECT*
*                   FILE.  TAGGED - MUKHERR                       *
* 11/21/17 SAI.K  - RESTOW FOR TWRL9705 CHANGES
* 10/05/2018 - KAUSHIK - EIN CHANGE - TAG: EINCHG                 *
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
*******************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3300 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3300 ldaTwrl3300;
    private PdaTwrafrmn pdaTwrafrmn;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private LdaTwrl9705 ldaTwrl9705;
    private PdaTwradist pdaTwradist;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Form;
    private DbsField pnd_Input_Parm_Pnd_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok;
    private DbsField pnd_Input_Parm_Pnd_Test_Run;
    private DbsField pnd_Ws_Save_Time_Stamp;
    private DbsField pnd_Ws_Save_Date;
    private DbsField pnd_Ws_Save_Date_N;
    private DbsField pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Year__R_Field_1;
    private DbsField pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A;
    private DbsField pnd_Y_Cnt;
    private DbsField pnd_Ws_Moved_Aa;
    private DbsField pnd_Ws_Reportable;
    private DbsField pnd_Zero_Form;
    private DbsField pnd_Ws_End;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Super_Start;
    private DbsField pnd_I;
    private DbsField pnd_J;

    private DbsGroup pnd_J__R_Field_2;
    private DbsField pnd_J_Pnd_K;
    private DbsField pnd_L;

    private DbsGroup pnd_L__R_Field_3;
    private DbsField pnd_L_Pnd_M;
    private DbsField pnd_State_Ndx;
    private DbsField pnd_State_Cnt;
    private DbsField pnd_Ws_State_Alpha;
    private DbsField pnd_Ws_State_Tax_Id_Tiaa;
    private DbsField pnd_Ws_State_Tax_Id_Cref;
    private DbsField pnd_Ws_State_Tax_Id_Life;
    private DbsField pnd_Ws_State_Tax_Id_Tcii;
    private DbsField pnd_Ws_State_Tax_Id_Trst;
    private DbsField pnd_Ws_State_Tax_Id_Fsb;
    private DbsField pnd_Ws_State_Comb_Fed_Ind;
    private DbsField pnd_Ws_Extract_Irs_Form_Tin;

    private DbsGroup pnd_Ws_Extract_Irs_Form_Tin__R_Field_4;
    private DbsField pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_1;
    private DbsField pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_Contr;

    private DbsGroup pnd_Ws_Extract_Irs_Form_Tin__R_Field_5;
    private DbsField pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_2;
    private DbsField pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_4;
    private DbsField pnd_Ws_Extract_Irs_Form_Loc_Cde;
    private DbsField pnd_Ws_Extract_Irs_Form_State_Cde;
    private DbsField pnd_Ws_1099r_Account_Num;

    private DbsGroup pnd_Ws_1099r_Account_Num__R_Field_6;
    private DbsField pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Tin_4;
    private DbsField pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Cntrct_Payee;
    private DbsField pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Seq_Num;
    private DbsField pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Res_Loc_Cde;
    private DbsField pnd_Ws_1099int_Account_Num;

    private DbsGroup pnd_Ws_1099int_Account_Num__R_Field_7;
    private DbsField pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Tin_4;
    private DbsField pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Cntrct_Payee;
    private DbsField pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Seq_Num;
    private DbsField pnd_Read_Key;

    private DbsGroup pnd_Read_Key__R_Field_8;
    private DbsField pnd_Read_Key_Pnd_Rk_1;
    private DbsField pnd_Read_Key_Pnd_Rk_2;
    private DbsField pnd_Read_Key_Pnd_Rk_3;
    private DbsField pnd_Dist4_Ctr;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Reac_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Zeroes_Ctr;
    private DbsField pnd_Cref_I;
    private DbsField pnd_Cref_I_St;
    private DbsField pnd_Cref_R;
    private DbsField pnd_Cref_R_St;
    private DbsField pnd_Group_I;
    private DbsField pnd_Group_I_St;
    private DbsField pnd_Group_R;
    private DbsField pnd_Group_R_St;
    private DbsField pnd_Tiaa_I;
    private DbsField pnd_Tiaa_I_St;
    private DbsField pnd_Tiaa_R;
    private DbsField pnd_Tiaa_R_St;
    private DbsField pnd_Life_I;
    private DbsField pnd_Life_I_St;
    private DbsField pnd_Life_R;
    private DbsField pnd_Life_R_St;
    private DbsField pnd_Tcii_I;
    private DbsField pnd_Tcii_I_St;
    private DbsField pnd_Tcii_R;
    private DbsField pnd_Tcii_R_St;
    private DbsField pnd_Trst_I;
    private DbsField pnd_Trst_I_St;
    private DbsField pnd_Trst_R;
    private DbsField pnd_Trst_R_St;
    private DbsField pnd_Fsb_I;
    private DbsField pnd_Fsb_I_St;
    private DbsField pnd_Fsb_R;
    private DbsField pnd_Fsb_R_St;
    private DbsField pnd_Grand_Tot;
    private DbsField pnd_Grand_Tot_St;
    private DbsField pnd_Cref_I_X;
    private DbsField pnd_Cref_R_X;
    private DbsField pnd_Group_I_X;
    private DbsField pnd_Group_R_X;
    private DbsField pnd_Tiaa_I_X;
    private DbsField pnd_Tiaa_R_X;
    private DbsField pnd_Life_I_X;
    private DbsField pnd_Life_R_X;
    private DbsField pnd_Tcii_I_X;
    private DbsField pnd_Tcii_R_X;
    private DbsField pnd_Trst_I_X;
    private DbsField pnd_Trst_R_X;
    private DbsField pnd_Fsb_I_X;
    private DbsField pnd_Fsb_R_X;
    private DbsField pnd_Grand_Tot_X;
    private DbsField pnd_Cntrct_Py;

    private DbsGroup pnd_Cntrct_Py__R_Field_9;
    private DbsField pnd_Cntrct_Py_Pnd_Cntrct;
    private DbsField pnd_Cntrct_Py_Pnd_Pye;
    private DbsField pnd_Cntrl_Ctgry;

    private DbsGroup pnd_Cntrl_Ctgry__R_Field_10;
    private DbsField pnd_Cntrl_Ctgry_Pnd_Cntrl;
    private DbsField pnd_Cntrl_Ctgry_Pnd_Ctgry;
    private DbsField pnd_Year;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Et_Limit;
    private DbsField pnd_Ws_Date_A8;

    private DbsGroup pnd_Ws_Date_A8__R_Field_11;
    private DbsField pnd_Ws_Date_A8_Pnd_Ws_Date_N8;
    private DbsField pnd_In_Dex;
    private DbsField pnd_Missing_Prtcpnt;

    private DbsGroup pnd_Wh_Recon_Accum;
    private DbsField pnd_Wh_Recon_Accum_Cref_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Cref_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Cref_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Cref_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Life_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Life_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Life_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Life_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Life_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Life_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Trst_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Trst_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Trst_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Trst_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Gross_Amt;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Int_Amt;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Taxable_Amt;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Ivc_Amt;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_R;
    private DbsField pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_I;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Gross;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Taxable;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Ivc;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I;
    private DbsField pnd_Wh_Recon_Accum_Pnd_Total_Interest;
    private DbsField pnd_Iri_Tot_Cnt;
    private DbsField pnd_Iri_Tot_Sta;
    private DbsField pnd_Iri_Tot_Loc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3300 = new LdaTwrl3300();
        registerRecord(ldaTwrl3300);
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());
        pdaTwradist = new PdaTwradist(localVariables);

        // Local Variables

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Form = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Input_Parm_Pnd_Tax_Year = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok", "#NOT-FIRST-TIME-RUN-OK", 
            FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Test_Run = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Test_Run", "#TEST-RUN", FieldType.STRING, 1);
        pnd_Ws_Save_Time_Stamp = localVariables.newFieldInRecord("pnd_Ws_Save_Time_Stamp", "#WS-SAVE-TIME-STAMP", FieldType.TIME);
        pnd_Ws_Save_Date = localVariables.newFieldInRecord("pnd_Ws_Save_Date", "#WS-SAVE-DATE", FieldType.DATE);
        pnd_Ws_Save_Date_N = localVariables.newFieldInRecord("pnd_Ws_Save_Date_N", "#WS-SAVE-DATE-N", FieldType.NUMERIC, 8);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Tax_Year__R_Field_1", "REDEFINE", pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A = pnd_Ws_Tax_Year__R_Field_1.newFieldInGroup("pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A", "#WS-TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Y_Cnt = localVariables.newFieldInRecord("pnd_Y_Cnt", "#Y-CNT", FieldType.NUMERIC, 2);
        pnd_Ws_Moved_Aa = localVariables.newFieldInRecord("pnd_Ws_Moved_Aa", "#WS-MOVED-AA", FieldType.STRING, 3);
        pnd_Ws_Reportable = localVariables.newFieldInRecord("pnd_Ws_Reportable", "#WS-REPORTABLE", FieldType.NUMERIC, 2);
        pnd_Zero_Form = localVariables.newFieldInRecord("pnd_Zero_Form", "#ZERO-FORM", FieldType.BOOLEAN, 1);
        pnd_Ws_End = localVariables.newFieldInRecord("pnd_Ws_End", "#WS-END", FieldType.NUMERIC, 2);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Super_Start = localVariables.newFieldInRecord("pnd_Super_Start", "#SUPER-START", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.STRING, 2);

        pnd_J__R_Field_2 = localVariables.newGroupInRecord("pnd_J__R_Field_2", "REDEFINE", pnd_J);
        pnd_J_Pnd_K = pnd_J__R_Field_2.newFieldInGroup("pnd_J_Pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.STRING, 3);

        pnd_L__R_Field_3 = localVariables.newGroupInRecord("pnd_L__R_Field_3", "REDEFINE", pnd_L);
        pnd_L_Pnd_M = pnd_L__R_Field_3.newFieldInGroup("pnd_L_Pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_State_Ndx = localVariables.newFieldInRecord("pnd_State_Ndx", "#STATE-NDX", FieldType.INTEGER, 2);
        pnd_State_Cnt = localVariables.newFieldInRecord("pnd_State_Cnt", "#STATE-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_State_Alpha = localVariables.newFieldArrayInRecord("pnd_Ws_State_Alpha", "#WS-STATE-ALPHA", FieldType.STRING, 2, new DbsArrayController(1, 
            100));
        pnd_Ws_State_Tax_Id_Tiaa = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Tiaa", "#WS-STATE-TAX-ID-TIAA", FieldType.STRING, 14, new 
            DbsArrayController(1, 100));
        pnd_Ws_State_Tax_Id_Cref = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Cref", "#WS-STATE-TAX-ID-CREF", FieldType.STRING, 14, new 
            DbsArrayController(1, 100));
        pnd_Ws_State_Tax_Id_Life = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Life", "#WS-STATE-TAX-ID-LIFE", FieldType.STRING, 14, new 
            DbsArrayController(1, 100));
        pnd_Ws_State_Tax_Id_Tcii = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Tcii", "#WS-STATE-TAX-ID-TCII", FieldType.STRING, 14, new 
            DbsArrayController(1, 100));
        pnd_Ws_State_Tax_Id_Trst = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Trst", "#WS-STATE-TAX-ID-TRST", FieldType.STRING, 14, new 
            DbsArrayController(1, 100));
        pnd_Ws_State_Tax_Id_Fsb = localVariables.newFieldArrayInRecord("pnd_Ws_State_Tax_Id_Fsb", "#WS-STATE-TAX-ID-FSB", FieldType.STRING, 14, new DbsArrayController(1, 
            100));
        pnd_Ws_State_Comb_Fed_Ind = localVariables.newFieldArrayInRecord("pnd_Ws_State_Comb_Fed_Ind", "#WS-STATE-COMB-FED-IND", FieldType.STRING, 14, 
            new DbsArrayController(1, 100));
        pnd_Ws_Extract_Irs_Form_Tin = localVariables.newFieldInRecord("pnd_Ws_Extract_Irs_Form_Tin", "#WS-EXTRACT-IRS-FORM-TIN", FieldType.STRING, 10);

        pnd_Ws_Extract_Irs_Form_Tin__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Extract_Irs_Form_Tin__R_Field_4", "REDEFINE", pnd_Ws_Extract_Irs_Form_Tin);
        pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_1 = pnd_Ws_Extract_Irs_Form_Tin__R_Field_4.newFieldInGroup("pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_1", 
            "#WS-EXTRACT-IRS-FILLER-1", FieldType.STRING, 6);
        pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_Contr = pnd_Ws_Extract_Irs_Form_Tin__R_Field_4.newFieldInGroup("pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_Contr", 
            "#WS-EXTRACT-IRS-FORM-TIN-CONTR", FieldType.STRING, 4);

        pnd_Ws_Extract_Irs_Form_Tin__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Extract_Irs_Form_Tin__R_Field_5", "REDEFINE", pnd_Ws_Extract_Irs_Form_Tin);
        pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_2 = pnd_Ws_Extract_Irs_Form_Tin__R_Field_5.newFieldInGroup("pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Filler_2", 
            "#WS-EXTRACT-IRS-FILLER-2", FieldType.STRING, 5);
        pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_4 = pnd_Ws_Extract_Irs_Form_Tin__R_Field_5.newFieldInGroup("pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_4", 
            "#WS-EXTRACT-IRS-FORM-TIN-4", FieldType.STRING, 4);
        pnd_Ws_Extract_Irs_Form_Loc_Cde = localVariables.newFieldInRecord("pnd_Ws_Extract_Irs_Form_Loc_Cde", "#WS-EXTRACT-IRS-FORM-LOC-CDE", FieldType.STRING, 
            3);
        pnd_Ws_Extract_Irs_Form_State_Cde = localVariables.newFieldInRecord("pnd_Ws_Extract_Irs_Form_State_Cde", "#WS-EXTRACT-IRS-FORM-STATE-CDE", FieldType.STRING, 
            2);
        pnd_Ws_1099r_Account_Num = localVariables.newFieldInRecord("pnd_Ws_1099r_Account_Num", "#WS-1099R-ACCOUNT-NUM", FieldType.STRING, 20);

        pnd_Ws_1099r_Account_Num__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_1099r_Account_Num__R_Field_6", "REDEFINE", pnd_Ws_1099r_Account_Num);
        pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Tin_4 = pnd_Ws_1099r_Account_Num__R_Field_6.newFieldInGroup("pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Tin_4", 
            "#WS-IRS-FORM-TIN-4", FieldType.STRING, 4);
        pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Cntrct_Payee = pnd_Ws_1099r_Account_Num__R_Field_6.newFieldInGroup("pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Cntrct_Payee", 
            "#WS-IRS-FORM-CNTRCT-PAYEE", FieldType.STRING, 10);
        pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Seq_Num = pnd_Ws_1099r_Account_Num__R_Field_6.newFieldInGroup("pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Seq_Num", 
            "#WS-IRS-FORM-SEQ-NUM", FieldType.NUMERIC, 3);
        pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Res_Loc_Cde = pnd_Ws_1099r_Account_Num__R_Field_6.newFieldInGroup("pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Res_Loc_Cde", 
            "#WS-IRS-FORM-RES-LOC-CDE", FieldType.STRING, 3);
        pnd_Ws_1099int_Account_Num = localVariables.newFieldInRecord("pnd_Ws_1099int_Account_Num", "#WS-1099INT-ACCOUNT-NUM", FieldType.STRING, 17);

        pnd_Ws_1099int_Account_Num__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_1099int_Account_Num__R_Field_7", "REDEFINE", pnd_Ws_1099int_Account_Num);
        pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Tin_4 = pnd_Ws_1099int_Account_Num__R_Field_7.newFieldInGroup("pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Tin_4", 
            "#WS-IRS-FORMI-TIN-4", FieldType.STRING, 4);
        pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Cntrct_Payee = pnd_Ws_1099int_Account_Num__R_Field_7.newFieldInGroup("pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Cntrct_Payee", 
            "#WS-IRS-FORMI-CNTRCT-PAYEE", FieldType.STRING, 10);
        pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Seq_Num = pnd_Ws_1099int_Account_Num__R_Field_7.newFieldInGroup("pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Seq_Num", 
            "#WS-IRS-FORMI-SEQ-NUM", FieldType.NUMERIC, 3);
        pnd_Read_Key = localVariables.newFieldInRecord("pnd_Read_Key", "#READ-KEY", FieldType.STRING, 25);

        pnd_Read_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Read_Key__R_Field_8", "REDEFINE", pnd_Read_Key);
        pnd_Read_Key_Pnd_Rk_1 = pnd_Read_Key__R_Field_8.newFieldInGroup("pnd_Read_Key_Pnd_Rk_1", "#RK-1", FieldType.STRING, 1);
        pnd_Read_Key_Pnd_Rk_2 = pnd_Read_Key__R_Field_8.newFieldInGroup("pnd_Read_Key_Pnd_Rk_2", "#RK-2", FieldType.STRING, 10);
        pnd_Read_Key_Pnd_Rk_3 = pnd_Read_Key__R_Field_8.newFieldInGroup("pnd_Read_Key_Pnd_Rk_3", "#RK-3", FieldType.STRING, 14);
        pnd_Dist4_Ctr = localVariables.newFieldInRecord("pnd_Dist4_Ctr", "#DIST4-CTR", FieldType.NUMERIC, 9);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.NUMERIC, 9);
        pnd_Reac_Ctr = localVariables.newFieldInRecord("pnd_Reac_Ctr", "#REAC-CTR", FieldType.NUMERIC, 9);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.NUMERIC, 9);
        pnd_Zeroes_Ctr = localVariables.newFieldInRecord("pnd_Zeroes_Ctr", "#ZEROES-CTR", FieldType.NUMERIC, 9);
        pnd_Cref_I = localVariables.newFieldInRecord("pnd_Cref_I", "#CREF-I", FieldType.NUMERIC, 9);
        pnd_Cref_I_St = localVariables.newFieldInRecord("pnd_Cref_I_St", "#CREF-I-ST", FieldType.NUMERIC, 9);
        pnd_Cref_R = localVariables.newFieldInRecord("pnd_Cref_R", "#CREF-R", FieldType.NUMERIC, 9);
        pnd_Cref_R_St = localVariables.newFieldInRecord("pnd_Cref_R_St", "#CREF-R-ST", FieldType.NUMERIC, 9);
        pnd_Group_I = localVariables.newFieldInRecord("pnd_Group_I", "#GROUP-I", FieldType.NUMERIC, 9);
        pnd_Group_I_St = localVariables.newFieldInRecord("pnd_Group_I_St", "#GROUP-I-ST", FieldType.NUMERIC, 9);
        pnd_Group_R = localVariables.newFieldInRecord("pnd_Group_R", "#GROUP-R", FieldType.NUMERIC, 9);
        pnd_Group_R_St = localVariables.newFieldInRecord("pnd_Group_R_St", "#GROUP-R-ST", FieldType.NUMERIC, 9);
        pnd_Tiaa_I = localVariables.newFieldInRecord("pnd_Tiaa_I", "#TIAA-I", FieldType.NUMERIC, 9);
        pnd_Tiaa_I_St = localVariables.newFieldInRecord("pnd_Tiaa_I_St", "#TIAA-I-ST", FieldType.NUMERIC, 9);
        pnd_Tiaa_R = localVariables.newFieldInRecord("pnd_Tiaa_R", "#TIAA-R", FieldType.NUMERIC, 9);
        pnd_Tiaa_R_St = localVariables.newFieldInRecord("pnd_Tiaa_R_St", "#TIAA-R-ST", FieldType.NUMERIC, 9);
        pnd_Life_I = localVariables.newFieldInRecord("pnd_Life_I", "#LIFE-I", FieldType.NUMERIC, 9);
        pnd_Life_I_St = localVariables.newFieldInRecord("pnd_Life_I_St", "#LIFE-I-ST", FieldType.NUMERIC, 9);
        pnd_Life_R = localVariables.newFieldInRecord("pnd_Life_R", "#LIFE-R", FieldType.NUMERIC, 9);
        pnd_Life_R_St = localVariables.newFieldInRecord("pnd_Life_R_St", "#LIFE-R-ST", FieldType.NUMERIC, 9);
        pnd_Tcii_I = localVariables.newFieldInRecord("pnd_Tcii_I", "#TCII-I", FieldType.NUMERIC, 9);
        pnd_Tcii_I_St = localVariables.newFieldInRecord("pnd_Tcii_I_St", "#TCII-I-ST", FieldType.NUMERIC, 9);
        pnd_Tcii_R = localVariables.newFieldInRecord("pnd_Tcii_R", "#TCII-R", FieldType.NUMERIC, 9);
        pnd_Tcii_R_St = localVariables.newFieldInRecord("pnd_Tcii_R_St", "#TCII-R-ST", FieldType.NUMERIC, 9);
        pnd_Trst_I = localVariables.newFieldInRecord("pnd_Trst_I", "#TRST-I", FieldType.NUMERIC, 9);
        pnd_Trst_I_St = localVariables.newFieldInRecord("pnd_Trst_I_St", "#TRST-I-ST", FieldType.NUMERIC, 9);
        pnd_Trst_R = localVariables.newFieldInRecord("pnd_Trst_R", "#TRST-R", FieldType.NUMERIC, 9);
        pnd_Trst_R_St = localVariables.newFieldInRecord("pnd_Trst_R_St", "#TRST-R-ST", FieldType.NUMERIC, 9);
        pnd_Fsb_I = localVariables.newFieldInRecord("pnd_Fsb_I", "#FSB-I", FieldType.NUMERIC, 9);
        pnd_Fsb_I_St = localVariables.newFieldInRecord("pnd_Fsb_I_St", "#FSB-I-ST", FieldType.NUMERIC, 9);
        pnd_Fsb_R = localVariables.newFieldInRecord("pnd_Fsb_R", "#FSB-R", FieldType.NUMERIC, 9);
        pnd_Fsb_R_St = localVariables.newFieldInRecord("pnd_Fsb_R_St", "#FSB-R-ST", FieldType.NUMERIC, 9);
        pnd_Grand_Tot = localVariables.newFieldInRecord("pnd_Grand_Tot", "#GRAND-TOT", FieldType.NUMERIC, 9);
        pnd_Grand_Tot_St = localVariables.newFieldInRecord("pnd_Grand_Tot_St", "#GRAND-TOT-ST", FieldType.NUMERIC, 9);
        pnd_Cref_I_X = localVariables.newFieldInRecord("pnd_Cref_I_X", "#CREF-I-X", FieldType.NUMERIC, 9);
        pnd_Cref_R_X = localVariables.newFieldInRecord("pnd_Cref_R_X", "#CREF-R-X", FieldType.NUMERIC, 9);
        pnd_Group_I_X = localVariables.newFieldInRecord("pnd_Group_I_X", "#GROUP-I-X", FieldType.NUMERIC, 9);
        pnd_Group_R_X = localVariables.newFieldInRecord("pnd_Group_R_X", "#GROUP-R-X", FieldType.NUMERIC, 9);
        pnd_Tiaa_I_X = localVariables.newFieldInRecord("pnd_Tiaa_I_X", "#TIAA-I-X", FieldType.NUMERIC, 9);
        pnd_Tiaa_R_X = localVariables.newFieldInRecord("pnd_Tiaa_R_X", "#TIAA-R-X", FieldType.NUMERIC, 9);
        pnd_Life_I_X = localVariables.newFieldInRecord("pnd_Life_I_X", "#LIFE-I-X", FieldType.NUMERIC, 9);
        pnd_Life_R_X = localVariables.newFieldInRecord("pnd_Life_R_X", "#LIFE-R-X", FieldType.NUMERIC, 9);
        pnd_Tcii_I_X = localVariables.newFieldInRecord("pnd_Tcii_I_X", "#TCII-I-X", FieldType.NUMERIC, 9);
        pnd_Tcii_R_X = localVariables.newFieldInRecord("pnd_Tcii_R_X", "#TCII-R-X", FieldType.NUMERIC, 9);
        pnd_Trst_I_X = localVariables.newFieldInRecord("pnd_Trst_I_X", "#TRST-I-X", FieldType.NUMERIC, 9);
        pnd_Trst_R_X = localVariables.newFieldInRecord("pnd_Trst_R_X", "#TRST-R-X", FieldType.NUMERIC, 9);
        pnd_Fsb_I_X = localVariables.newFieldInRecord("pnd_Fsb_I_X", "#FSB-I-X", FieldType.NUMERIC, 9);
        pnd_Fsb_R_X = localVariables.newFieldInRecord("pnd_Fsb_R_X", "#FSB-R-X", FieldType.NUMERIC, 9);
        pnd_Grand_Tot_X = localVariables.newFieldInRecord("pnd_Grand_Tot_X", "#GRAND-TOT-X", FieldType.NUMERIC, 9);
        pnd_Cntrct_Py = localVariables.newFieldInRecord("pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 10);

        pnd_Cntrct_Py__R_Field_9 = localVariables.newGroupInRecord("pnd_Cntrct_Py__R_Field_9", "REDEFINE", pnd_Cntrct_Py);
        pnd_Cntrct_Py_Pnd_Cntrct = pnd_Cntrct_Py__R_Field_9.newFieldInGroup("pnd_Cntrct_Py_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 8);
        pnd_Cntrct_Py_Pnd_Pye = pnd_Cntrct_Py__R_Field_9.newFieldInGroup("pnd_Cntrct_Py_Pnd_Pye", "#PYE", FieldType.STRING, 2);
        pnd_Cntrl_Ctgry = localVariables.newFieldInRecord("pnd_Cntrl_Ctgry", "#CNTRL-CTGRY", FieldType.STRING, 2);

        pnd_Cntrl_Ctgry__R_Field_10 = localVariables.newGroupInRecord("pnd_Cntrl_Ctgry__R_Field_10", "REDEFINE", pnd_Cntrl_Ctgry);
        pnd_Cntrl_Ctgry_Pnd_Cntrl = pnd_Cntrl_Ctgry__R_Field_10.newFieldInGroup("pnd_Cntrl_Ctgry_Pnd_Cntrl", "#CNTRL", FieldType.STRING, 1);
        pnd_Cntrl_Ctgry_Pnd_Ctgry = pnd_Cntrl_Ctgry__R_Field_10.newFieldInGroup("pnd_Cntrl_Ctgry_Pnd_Ctgry", "#CTGRY", FieldType.STRING, 1);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 9);
        pnd_Et_Limit = localVariables.newFieldInRecord("pnd_Et_Limit", "#ET-LIMIT", FieldType.NUMERIC, 4);
        pnd_Ws_Date_A8 = localVariables.newFieldInRecord("pnd_Ws_Date_A8", "#WS-DATE-A8", FieldType.STRING, 8);

        pnd_Ws_Date_A8__R_Field_11 = localVariables.newGroupInRecord("pnd_Ws_Date_A8__R_Field_11", "REDEFINE", pnd_Ws_Date_A8);
        pnd_Ws_Date_A8_Pnd_Ws_Date_N8 = pnd_Ws_Date_A8__R_Field_11.newFieldInGroup("pnd_Ws_Date_A8_Pnd_Ws_Date_N8", "#WS-DATE-N8", FieldType.NUMERIC, 
            8);
        pnd_In_Dex = localVariables.newFieldInRecord("pnd_In_Dex", "#IN-DEX", FieldType.NUMERIC, 3);
        pnd_Missing_Prtcpnt = localVariables.newFieldInRecord("pnd_Missing_Prtcpnt", "#MISSING-PRTCPNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Wh_Recon_Accum = localVariables.newGroupArrayInRecord("pnd_Wh_Recon_Accum", "#WH-RECON-ACCUM", new DbsArrayController(1, 2));
        pnd_Wh_Recon_Accum_Cref_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Gross_Amt", "CREF-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Cref_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Int_Amt", "CREF-INT-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Cref_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Taxable_Amt", "CREF-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Cref_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Ivc_Amt", "CREF-IVC-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R", "CREF-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I", "CREF-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Life_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Gross_Amt", "LIFE-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Life_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Int_Amt", "LIFE-INT-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Life_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Taxable_Amt", "LIFE-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Life_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Ivc_Amt", "LIFE-IVC-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Life_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Tax_Wthld_R", "LIFE-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Life_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Life_Tax_Wthld_I", "LIFE-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tiaa_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Gross_Amt", "TIAA-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tiaa_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Int_Amt", "TIAA-INT-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt", "TIAA-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt", "TIAA-IVC-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R", "TIAA-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I", "TIAA-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tcii_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Gross_Amt", "TCII-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tcii_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Int_Amt", "TCII-INT-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Tcii_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Taxable_Amt", "TCII-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tcii_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Ivc_Amt", "TCII-IVC-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R", "TCII-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I", "TCII-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Trst_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Gross_Amt", "TRST-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Trst_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Int_Amt", "TRST-INT-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Trst_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Taxable_Amt", "TRST-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Trst_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Ivc_Amt", "TRST-IVC-AMT", FieldType.NUMERIC, 15, 
            2);
        pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R", "TRST-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I", "TRST-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Fsb_Gross_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Gross_Amt", "FSB-GROSS-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Fsb_Int_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Int_Amt", "FSB-INT-AMT", FieldType.NUMERIC, 15, 2);
        pnd_Wh_Recon_Accum_Fsb_Taxable_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Taxable_Amt", "FSB-TAXABLE-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Fsb_Ivc_Amt = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Ivc_Amt", "FSB-IVC-AMT", FieldType.NUMERIC, 15, 2);
        pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_R", "FSB-TAX-WTHLD-R", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_I", "FSB-TAX-WTHLD-I", FieldType.NUMERIC, 
            15, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Gross = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Gross", "#TOTAL-GROSS", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Taxable = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Taxable", "#TOTAL-TAXABLE", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Ivc = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Ivc", "#TOTAL-IVC", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R", "#TOTAL-WITHHELD-R", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I", "#TOTAL-WITHHELD-I", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Wh_Recon_Accum_Pnd_Total_Interest = pnd_Wh_Recon_Accum.newFieldInGroup("pnd_Wh_Recon_Accum_Pnd_Total_Interest", "#TOTAL-INTEREST", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Iri_Tot_Cnt = localVariables.newFieldInRecord("pnd_Iri_Tot_Cnt", "#IRI-TOT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Iri_Tot_Sta = localVariables.newFieldInRecord("pnd_Iri_Tot_Sta", "#IRI-TOT-STA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Iri_Tot_Loc = localVariables.newFieldInRecord("pnd_Iri_Tot_Loc", "#IRI-TOT-LOC", FieldType.PACKED_DECIMAL, 13, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3300.initializeValues();
        ldaTwrl9705.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Year.setInitialValue(0);
        pnd_Et_Count.setInitialValue(0);
        pnd_Et_Limit.setInitialValue(50);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3300() throws Exception
    {
        super("Twrp3300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  RMAT (03)  PS=60  LS=133                                                                                                                                     //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: FORMAT ( 04 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  CHECK THAT MOORE 1099 HAS NOT RUN
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTROL-RECORD
        sub_Check_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //*  12-16-00 FRANK
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-RECORD
        sub_Update_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //*  08-11-99 FRANK
        //*  09-08-99 FRANK
        //*  09-08-99 FRANK
        //*  09-08-99 FRANK
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
        sub_Load_State_Table();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Save_Time_Stamp.setValue(Global.getTIMX());                                                                                                                //Natural: ASSIGN #WS-SAVE-TIME-STAMP := *TIMX
        pnd_Ws_Save_Date.setValue(Global.getDATX());                                                                                                                      //Natural: ASSIGN #WS-SAVE-DATE := *DATX
        pnd_Ws_Save_Date_N.setValue(Global.getDATN());                                                                                                                    //Natural: ASSIGN #WS-SAVE-DATE-N := *DATN
        setValueToSubstring(pnd_Ws_Tax_Year,pnd_Super_Start,1,4);                                                                                                         //Natural: MOVE #WS-TAX-YEAR TO SUBSTR ( #SUPER-START,1,4 )
        setValueToSubstring("A",pnd_Super_Start,5,1);                                                                                                                     //Natural: MOVE 'A' TO SUBSTR ( #SUPER-START,5,1 )
        setValueToSubstring("01",pnd_Super_Start,6,2);                                                                                                                    //Natural: MOVE '01' TO SUBSTR ( #SUPER-START,6,2 )
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Super_Start,8,1);                                                                                                 //Natural: MOVE LOW-VALUES TO SUBSTR ( #SUPER-START,8,1 )
        ldaTwrl9705.getVw_form_U().startDatabaseRead                                                                                                                      //Natural: READ FORM-U BY TIRF-SUPERDE-3 = #SUPER-START
        (
        "RD_1",
        new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Super_Start, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
        );
        RD_1:
        while (condition(ldaTwrl9705.getVw_form_U().readNextRow("RD_1")))
        {
            //* FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().greater(pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A) || ldaTwrl9705.getForm_U_Tirf_Form_Type().greater(2)))         //Natural: IF TIRF-TAX-YEAR > #WS-TAX-YEAR-A OR TIRF-FORM-TYPE > 02
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            //*    IF #READ-CTR > 50
            //*         TERMINATE 103
            //*     END-IF
            ldaTwrl3300.getExtract_Npd_File_0().reset();                                                                                                                  //Natural: RESET EXTRACT-NPD-FILE-0
            if (condition(ldaTwrl9705.getForm_U_Tirf_Empty_Form().equals(true)))                                                                                          //Natural: IF TIRF-EMPTY-FORM = TRUE
            {
                pnd_Zeroes_Ctr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ZEROES-CTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accept_Ctr.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ACCEPT-CTR
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Prod_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                      //Natural: ASSIGN EXTRACT-PROD-IND := TIRF-COMPANY-CDE
            //*  12-06-99 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                       //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
            {
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_Reject_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind());                                         //Natural: ASSIGN EXTRACT-IRS-REJECT-IND := TIRF-IRS-REJECT-IND
            }                                                                                                                                                             //Natural: END-IF
            //*  IF TIRF-FED-TAX-WTHLD  >  0
            //*  12/05/06 J.ROTHOLZ
            if (condition(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld().notEquals(getZero())))                                                                               //Natural: IF TIRF-FED-TAX-WTHLD NE 0
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-WTHHOLD-RECON
                sub_Accum_Wthhold_Recon();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("C")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'C'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Cref_I.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CREF-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cref_I_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #CREF-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cref_R.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CREF-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cref_R_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #CREF-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  EINCHG
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("T") || ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("A")))                                  //Natural: IF TIRF-COMPANY-CDE = 'T' OR = 'A'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Tiaa_I.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TIAA-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tiaa_I_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TIAA-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tiaa_R.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TIAA-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tiaa_R_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TIAA-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  07-19-99 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("L")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'L'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Life_I.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LIFE-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Life_I_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #LIFE-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Life_R.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LIFE-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Life_R_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #LIFE-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  07-15-02 J.ROTHOLZ
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("S")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'S'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Tcii_I.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TCII-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tcii_I_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TCII-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tcii_R.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TCII-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tcii_R_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TCII-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  JB1011 NXT 11 LINES
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("F")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'F'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Fsb_I.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #FSB-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Fsb_I_St.nadd(pnd_State_Cnt);                                                                                                                     //Natural: ADD #STATE-CNT TO #FSB-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fsb_R.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #FSB-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Fsb_R_St.nadd(pnd_State_Cnt);                                                                                                                     //Natural: ADD #STATE-CNT TO #FSB-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  11-22-04 D.APRIL
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("X")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'X'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Trst_I.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TRST-I
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Trst_I_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TRST-I-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Trst_R.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TRST-R
                                                                                                                                                                          //Natural: PERFORM STATE-COUNT
                    sub_State_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Trst_R_St.nadd(pnd_State_Cnt);                                                                                                                    //Natural: ADD #STATE-CNT TO #TRST-R-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet872 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
            if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("1") || ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("3"))))
            {
                decideConditionsMet872++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Ind().setValue("2");                                                                                         //Natural: ASSIGN EXTRACT-ID-IND := '2'
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                           //Natural: ASSIGN EXTRACT-ID-NMBR := TIRF-TIN
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("2"))))
            {
                decideConditionsMet872++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Ind().setValue("1");                                                                                         //Natural: ASSIGN EXTRACT-ID-IND := '1'
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                           //Natural: ASSIGN EXTRACT-ID-NMBR := TIRF-TIN
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet872 > 0))
            {
                pnd_Ws_Extract_Irs_Form_Tin.setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                                   //Natural: ASSIGN #WS-EXTRACT-IRS-FORM-TIN := TIRF-TIN
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Ws_Extract_Irs_Form_Tin.setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                                   //Natural: ASSIGN #WS-EXTRACT-IRS-FORM-TIN := TIRF-TIN
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Id_Nmbr().reset();                                                                                              //Natural: RESET EXTRACT-ID-NMBR
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),  //Natural: COMPRESS TIRF-CONTRACT-NBR TIRF-PAYEE-CDE INTO EXTRACT-CNTRCT-PY-NMBR LEAVING NO SPACE
                ldaTwrl9705.getForm_U_Tirf_Payee_Cde()));
            //* ** USE NEW ROUTINE TO CONVERT DISTRIBUTION CODE          9/24/03  RM
            pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();                                                                                                          //Natural: RESET #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
            pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
            pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
            pdaTwradist.getPnd_Twradist_Pnd_Tax_Year_A().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                                 //Natural: ASSIGN #TWRADIST.#TAX-YEAR-A := TIRF-TAX-YEAR
            pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9705.getForm_U_Tirf_Distribution_Cde());                                                            //Natural: ASSIGN #TWRADIST.#IN-DIST := TIRF-DISTRIBUTION-CDE
            DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist());                                                                    //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST
            if (condition(Global.isEscape())) return;
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                             //Natural: ASSIGN EXTRACT-CNTRL-CTGRY-CDE := #TWRADIST.#OUT-DIST
            //* ** THE FOLLOWING CODES ARE OBSOLETE                      9/24/03   RM
            //*   DECIDE ON FIRST VALUE OF TIRF-DISTRIBUTION-CDE
            //*     VALUE 'Z'
            //*       EXTRACT-CNTRL-CTGRY-CDE := '4G'
            //*     VALUE 'Y'
            //*       EXTRACT-CNTRL-CTGRY-CDE := '4J'
            //*    NONE
            //*      EXTRACT-CNTRL-CTGRY-CDE := TIRF-DISTRIBUTION-CDE
            //*  END-DECIDE
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                     //Natural: MOVE TIRF-COMPANY-CDE TO EXTRACT-PRDCT-CDE
            short decideConditionsMet912 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF TIRF-FORM-TYPE;//Natural: VALUE 2
            if (condition((ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2))))
            {
                decideConditionsMet912++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().setValue("I");                                                                                       //Natural: ASSIGN EXTRACT-TYPE-CDE := 'I'
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                 //Natural: ASSIGN EXTRACT-FED-GROSS-AMT := TIRF-INT-AMT
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Wthhld_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                          //Natural: ASSIGN EXTRACT-FED-WTHHLD-AMT := TIRF-FED-TAX-WTHLD
            }                                                                                                                                                             //Natural: VALUE 1
            else if (condition((ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1))))
            {
                decideConditionsMet912++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().setValue("R");                                                                                       //Natural: ASSIGN EXTRACT-TYPE-CDE := 'R'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaTwrl9705.getForm_U_Tirf_Ira_Distr().equals("Y")))                                                                                            //Natural: IF TIRF-IRA-DISTR = 'Y'
            {
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Ira_Sep_Ind().setValue("1");                                                                                    //Natural: ASSIGN EXTRACT-IRA-SEP-IND := '1'
                //*  10/12/06   RM
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Percent_Distrib().setValue(ldaTwrl9705.getForm_U_Tirf_Pct_Of_Tot());                                                //Natural: ASSIGN EXTRACT-PERCENT-DISTRIB := TIRF-PCT-OF-TOT
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Total_Distrib_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_Tot_Distr());                                               //Natural: ASSIGN EXTRACT-TOTAL-DISTRIB-IND := TIRF-TOT-DISTR
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Roth_Year().setValue(ldaTwrl9705.getForm_U_Tirf_Roth_Year());                                                       //Natural: ASSIGN EXTRACT-ROTH-YEAR := TIRF-ROTH-YEAR
            //*  09-14-99 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Moore_Test_Ind().equals("Y")))                                                                                       //Natural: IF TIRF-MOORE-TEST-IND = 'Y'
            {
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Moore_Test_Ind().setValue("X");                                                                                 //Natural: ASSIGN EXTRACT-MOORE-TEST-IND := 'X'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9705.getForm_U_Tirf_Moore_Hold_Ind().equals(" ") || ldaTwrl9705.getForm_U_Tirf_Moore_Hold_Ind().equals("N")))                            //Natural: IF TIRF-MOORE-HOLD-IND = ' ' OR = 'N'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl3300.getExtract_Npd_File_0_Extract_Hold_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Moore_Hold_Ind());                                               //Natural: ASSIGN EXTRACT-HOLD-CDE := TIRF-MOORE-HOLD-IND
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Name().setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(),  //Natural: COMPRESS TIRF-PART-LAST-NME TIRF-PART-FIRST-NME TIRF-PART-MDDLE-NME INTO EXTRACT-NAME
                ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme()));
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_1().setValue(ldaTwrl9705.getForm_U_Tirf_Street_Addr());                                                  //Natural: ASSIGN EXTRACT-ADDRSS-TXT-1 := TIRF-STREET-ADDR
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_2().setValue(ldaTwrl9705.getForm_U_Tirf_City());                                                         //Natural: ASSIGN EXTRACT-ADDRSS-TXT-2 := TIRF-CITY
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Apo_Geo_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde());                                                   //Natural: ASSIGN EXTRACT-APO-GEO-CDE := TIRF-APO-GEO-CDE
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Country_Name().setValue(ldaTwrl9705.getForm_U_Tirf_Country_Name());                                                 //Natural: ASSIGN EXTRACT-COUNTRY-NAME := TIRF-COUNTRY-NAME
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Province_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Province_Code());                                               //Natural: ASSIGN EXTRACT-PROVINCE-CODE := TIRF-PROVINCE-CODE
            //*  11-13-00 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Foreign_Addr().equals("Y")))                                                                                         //Natural: IF FORM-U.TIRF-FOREIGN-ADDR = 'Y'
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Province_Code().greaterOrEqual("074") && ldaTwrl9705.getForm_U_Tirf_Province_Code().lessOrEqual("088")           //Natural: IF FORM-U.TIRF-PROVINCE-CODE = '074' THRU '088' AND FORM-U.TIRF-PROVINCE-CODE = MASK ( 999 )
                    && DbsUtil.maskMatches(ldaTwrl9705.getForm_U_Tirf_Province_Code(),"999")))
                {
                    pnd_L.setValue(ldaTwrl9705.getForm_U_Tirf_Province_Code());                                                                                           //Natural: ASSIGN #L := FORM-U.TIRF-PROVINCE-CODE
                    ldaTwrl3300.getExtract_Npd_File_0_Extract_Province_Code().setValue(pnd_Ws_State_Alpha.getValue(pnd_L_Pnd_M.getInt() + 1));                            //Natural: ASSIGN EXTRACT-PROVINCE-CODE := #WS-STATE-ALPHA ( #M )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addrss_Txt_7().reset();                                                                                             //Natural: RESET EXTRACT-ADDRSS-TXT-7
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Zip().setValue(ldaTwrl9705.getForm_U_Tirf_Zip().getSubstring(1,5));                                          //Natural: ASSIGN EXTRACT-ADDR-7-ZIP := SUBSTR ( TIRF-ZIP,1,5 )
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Plus_4().setValue(ldaTwrl9705.getForm_U_Tirf_Zip().getSubstring(6,4));                                       //Natural: ASSIGN EXTRACT-ADDR-7-PLUS-4 := SUBSTR ( TIRF-ZIP,6,4 )
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Walk().setValue(ldaTwrl9705.getForm_U_Tirf_Walk_Rte());                                                      //Natural: ASSIGN EXTRACT-ADDR-7-WALK := TIRF-WALK-RTE
            //*  11-23-99 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Foreign_Addr().equals("Y")))                                                                                         //Natural: IF FORM-U.TIRF-FOREIGN-ADDR = 'Y'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (DbsUtil.maskMatches(ldaTwrl9705.getForm_U_Tirf_Geo_Cde(),"NN"))))                                                                        //Natural: IF FORM-U.TIRF-GEO-CDE NE MASK ( NN )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Geo_Cde().equals("00")))                                                                                     //Natural: IF FORM-U.TIRF-GEO-CDE = '00'
                    {
                        ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Geo_Cd().setValue(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde());                                     //Natural: ASSIGN EXTRACT-ADDR-7-GEO-CD := TIRF-APO-GEO-CDE
                        //*  03-19-03 FRANK
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                  //Natural: IF FORM-U.TIRF-FORM-TYPE = 02
                        {
                            ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde());                                     //Natural: ASSIGN EXTRACT-STATE-CDE := TIRF-APO-GEO-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_J.setValue(ldaTwrl9705.getForm_U_Tirf_Geo_Cde());                                                                                             //Natural: ASSIGN #J := FORM-U.TIRF-GEO-CDE
                        ldaTwrl3300.getExtract_Npd_File_0_Extract_Addr_7_Geo_Cd().setValue(pnd_Ws_State_Alpha.getValue(pnd_J_Pnd_K.getInt() + 1));                        //Natural: ASSIGN EXTRACT-ADDR-7-GEO-CD := #WS-STATE-ALPHA ( #K )
                        //*  03-19-03 FRANK
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                  //Natural: IF FORM-U.TIRF-FORM-TYPE = 02
                        {
                            ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().setValue(pnd_Ws_State_Alpha.getValue(pnd_J_Pnd_K.getInt() + 1));                        //Natural: ASSIGN EXTRACT-STATE-CDE := #WS-STATE-ALPHA ( #K )
                            if (condition(pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_J_Pnd_K.getInt() + 1).equals("C")))                                                      //Natural: IF #WS-STATE-COMB-FED-IND ( #K ) = 'C'
                            {
                                ldaTwrl3300.getExtract_Npd_File_0_Extract_Combine_Ind().setValue("Y");                                                                    //Natural: ASSIGN EXTRACT-COMBINE-IND := 'Y'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  12-06-99 FRANK
                //*  02-04-00 FRANK
                //*  02-04-00 FRANK
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Foreign_Addr().setValue(ldaTwrl9705.getForm_U_Tirf_Foreign_Addr());                                                 //Natural: ASSIGN EXTRACT-FOREIGN-ADDR := TIRF-FOREIGN-ADDR
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntr_Sys_Err().setValue(ldaTwrl9705.getForm_U_Count_Casttirf_Sys_Err());                                            //Natural: ASSIGN EXTRACT-CNTR-SYS-ERR := C*TIRF-SYS-ERR
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Sys_Err().getValue("*").setValue(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*"));                               //Natural: ASSIGN EXTRACT-SYS-ERR ( * ) := TIRF-SYS-ERR ( * )
                                                                                                                                                                          //Natural: PERFORM SET-IRS-UNIQUE-ID
            sub_Set_Irs_Unique_Id();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * PROCESS 1099-INT RECORDS                        /* 04-17-01 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 2
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                   //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
                {
                    getWorkFiles().write(2, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                                  //Natural: WRITE WORK FILE 02 EXTRACT-NPD-FILE-0
                    //*  MUKHERR
                    //*  MUKHERR
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(1) && ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("5")))                  //Natural: IF TIRF-SYS-ERR ( * ) EQ 1 AND FORM-U.TIRF-TAX-ID-TYPE = '5'
                    {
                        //*  MUKHERR
                        getWorkFiles().write(1, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                              //Natural: WRITE WORK FILE 01 EXTRACT-NPD-FILE-0
                        //*  MUKHERR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getWorkFiles().write(1, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                                  //Natural: WRITE WORK FILE 01 EXTRACT-NPD-FILE-0
                }                                                                                                                                                         //Natural: END-IF
                //*  08-11-99 FRANK
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO C*TIRF-1099-R-STATE-GRP
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_I.nadd(1))
            {
                //*  02-22-00 FRANK
                if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y") || ldaTwrl9705.getForm_U_Tirf_State_Irs_Rpt_Ind().getValue(pnd_I).equals("Y")))     //Natural: IF TIRF-IRS-REJECT-IND = 'Y' OR TIRF-STATE-IRS-RPT-IND ( #I ) = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-STATE
                    sub_Process_State();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  12-06-99 FRANK
            if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                       //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
            {
                ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Ind().setValue("H");                                                                                                   //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'H'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Ind().setValue("O");                                                                                                   //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'O'
            }                                                                                                                                                             //Natural: END-IF
            //*  MUKHERR
            //*  MUKHERR
            //*  MUKHERR
            if (condition(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(1) && ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("5")))                          //Natural: IF TIRF-SYS-ERR ( * ) EQ 1 AND TIRF-TAX-ID-TYPE = '5'
            {
                ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Ind().setValue("O");                                                                                                   //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'O'
                //*  MUKHERR
            }                                                                                                                                                             //Natural: END-IF
            //*  09-07-99 FRANK
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Date().setValue(pnd_Ws_Save_Date);                                                                                         //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #WS-SAVE-DATE
            ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Ws_Save_Time_Stamp);                                                                                          //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS-SAVE-TIME-STAMP
            ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_USER());                                                                                         //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
            //*  08-26-99
            ldaTwrl9705.getVw_form_U().updateDBRow("RD_1");                                                                                                               //Natural: UPDATE
            if (condition(pnd_Et_Count.greaterOrEqual(pnd_Et_Limit)))                                                                                                     //Natural: IF #ET-COUNT >= #ET-LIMIT
            {
                //*  08-26-99
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  08-11-99 FC
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM CONTROL-REPORTS
        sub_Control_Reports();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
        //* *---------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-STATE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-WTHHOLD-RECON
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDT-CNTRL-REC-WITH-WTHLD-RECON
        //* *TIRCNTL-RPT-TCII-GROSS-AMT    (1)  :=  TCII-GROSS-AMT(#I)
        //* *TIRCNTL-RPT-TCII-TAXABLE-AMT  (1)  :=  TCII-TAXABLE-AMT(#I)
        //* *TIRCNTL-RPT-TCII-IVC-AMT      (1)  :=  TCII-IVC-AMT(#I)
        //* *TIRCNTL-RPT-TCII-TAX-WTHLD    (1)  :=  TCII-TAX-WTHLD-R(#I)
        //* *TIRCNTL-RPT-TCII-INT-AMT      (1)  :=  TCII-INT-AMT(#I)
        //* *TIRCNTL-RPT-TCII-TAX-WTHLD    (1)  :=  TCII-TAX-WTHLD-I(#I)
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTROL-RECORD
        //* *-------------------------------------
        //*   CHECK CONTROL FILE
        //*  #TWRATBL4.TIRCNTL-RPT-DTE (1)
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-REPORTS
        //* */ 50X 'Extract Control Report' #WS-TAX-YEAR (EM=9999)
        //*  34T 'FORM'
        //*  / 'TOTALS'  27X 'COUNT'
        //*  /     '** CREF  **'
        //*  /     '1099-INT                    ' #CREF-I     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #CREF-I-ST (EM=ZZ,ZZZ,ZZ9)
        //*  /     '1099-R                      ' #CREF-R     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #CREF-R-ST (EM=ZZ,ZZZ,ZZ9)
        //*  //    '** LIFE **'
        //*  /     '1099-INT                    ' #LIFE-I     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #LIFE-I-ST (EM=ZZ,ZZZ,ZZ9)
        //*  /     '1099-R                      ' #LIFE-R     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #LIFE-R-ST (EM=ZZ,ZZZ,ZZ9)
        //*  //    '** TCII **'
        //*  /     '1099-INT                    ' #TCII-I     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #TCII-I-ST (EM=ZZ,ZZZ,ZZ9)
        //*  /     '1099-R                      ' #TCII-R     (EM=ZZ,ZZZ,ZZ9)
        //*  2X #TCII-R-ST (EM=ZZ,ZZZ,ZZ9)
        //*  /   'CREF GROSS      ' CREF-GROSS-AMT  (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF GROSS      ' CREF-GROSS-AMT  (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'CREF INTEREST   ' CREF-INT-AMT    (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF INTEREST   ' CREF-INT-AMT    (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'CREF TAXABLE    ' CREF-TAXABLE-AMT(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF TAXABLE    ' CREF-TAXABLE-AMT(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'CREF IVC        ' CREF-IVC-AMT    (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF IVC        ' CREF-IVC-AMT    (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'CREF WTHLD 1099R' CREF-TAX-WTHLD-R(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF WTHLD 1099R' CREF-TAX-WTHLD-R(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'CREF WTHLD 1099I' CREF-TAX-WTHLD-I(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'CREF WTHLD 1099I' CREF-TAX-WTHLD-I(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE GROSS      ' LIFE-GROSS-AMT  (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE GROSS      ' LIFE-GROSS-AMT  (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE INTEREST   ' LIFE-INT-AMT    (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE INTEREST   ' LIFE-INT-AMT    (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE TAXABLE    ' LIFE-TAXABLE-AMT(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE TAXABLE    ' LIFE-TAXABLE-AMT(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE IVC        ' LIFE-IVC-AMT    (1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE IVC        ' LIFE-IVC-AMT    (2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE WTHLD 1099R' LIFE-TAX-WTHLD-R(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE WTHLD 1099R' LIFE-TAX-WTHLD-R(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  /   'LIFE WTHLD 1099I' LIFE-TAX-WTHLD-I(1)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //*  42T 'LIFE WTHLD 1099I' LIFE-TAX-WTHLD-I(2)    (EM=ZZ,ZZZ,ZZZ,ZZ9.99)
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-RECORD
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-IRS-UNIQUE-ID
        //*     IF #READ-CTR      LT 40
        //*        WRITE (0) '=' FORM-U.TIRF-IRS-FORM-SEQ
        //*        WRITE (0) '=' EXTRACT-CNTRCT-PY-NMBR
        //*        WRITE (0) '=' #WS-EXTRACT-IRS-FORM-LOC-CDE
        //*        WRITE (0) '=' #WS-EXTRACT-IRS-FORM-STATE-CDE
        //*     END-IF
        //*     IF #READ-CTR      LT 40
        //*        WRITE (0) '=' FORM-U.TIRF-IRS-FORM-SEQ
        //*        WRITE (0) '=' EXTRACT-CNTRCT-PY-NMBR
        //*     END-IF
        //*     IF FORM-U.TIRF-IRS-FORM-SEQ EQ MASK(9999999)
        //*     END-IF
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-COUNT
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding & Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 40T 'Tax Withholding and Payment System - Tax Year Control Report' 120T 'REPORT: RPT1' / 55T 'IRS ORIGINAL REPORTING' //
        //* *-------------------
        //*  45T 'Tax Withholding and Payment System - ERROR REPORT'
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                                 //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        FOR02:                                                                                                                                                            //Natural: FOR #I = 0 TO 99
        for (pnd_I.setValue(0); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_I,new ReportEditMask("999"));                                                                      //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            //* 03-19-03 FRANK
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().equals(true) || ! (pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().getBoolean())))                           //Natural: IF #RETURN-CDE = TRUE OR NOT TWRATBL2.#DISPLAY-IND
            {
                pnd_Ws_State_Alpha.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                             //Natural: ASSIGN #WS-STATE-ALPHA ( #I ) := TIRCNTL-STATE-ALPHA-CODE
                pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                      //Natural: ASSIGN #WS-STATE-TAX-ID-TIAA ( #I ) := TIRCNTL-STATE-TAX-ID-TIAA
                pnd_Ws_State_Tax_Id_Cref.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Cref());                                      //Natural: ASSIGN #WS-STATE-TAX-ID-CREF ( #I ) := TIRCNTL-STATE-TAX-ID-CREF
                pnd_Ws_State_Tax_Id_Life.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Life());                                      //Natural: ASSIGN #WS-STATE-TAX-ID-LIFE ( #I ) := TIRCNTL-STATE-TAX-ID-LIFE
                pnd_Ws_State_Tax_Id_Trst.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                     //Natural: ASSIGN #WS-STATE-TAX-ID-TRST ( #I ) := TIRCNTL-STATE-TAX-ID-TRUST
                pnd_Ws_State_Comb_Fed_Ind.getValue(pnd_I.getInt() + 1).setValue(pdaTwratbl2.getTwratbl2_Tircntl_Comb_Fed_Ind());                                          //Natural: ASSIGN #WS-STATE-COMB-FED-IND ( #I ) := TIRCNTL-COMB-FED-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_State() throws Exception                                                                                                                     //Natural: PROCESS-STATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------
        //*  12-08-00 FRANK
        if (condition(ldaTwrl9705.getForm_U_Tirf_Res_Type().getValue(pnd_I).equals("1")))                                                                                 //Natural: IF TIRF-RES-TYPE ( #I ) = '1'
        {
            pnd_J.setValue(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I));                                                                                      //Natural: ASSIGN #J := FORM-U.TIRF-STATE-CODE ( #I )
            ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Cde().setValue(pnd_Ws_State_Alpha.getValue(pnd_J_Pnd_K.getInt() + 1));                                        //Natural: ASSIGN EXTRACT-STATE-CDE := #WS-STATE-ALPHA ( #K )
            pnd_Ws_Extract_Irs_Form_Loc_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Code().getValue(pnd_I));                                                              //Natural: ASSIGN #WS-EXTRACT-IRS-FORM-LOC-CDE := FORM-U.TIRF-LOC-CODE ( #I )
            pnd_Ws_Extract_Irs_Form_State_Cde.setValue(ldaTwrl9705.getForm_U_Tirf_State_Code().getValue(pnd_I));                                                          //Natural: ASSIGN #WS-EXTRACT-IRS-FORM-STATE-CDE := FORM-U.TIRF-STATE-CODE ( #I )
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Combine_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Cmb_Ind().getValue(pnd_I));                                   //Natural: ASSIGN EXTRACT-COMBINE-IND := FORM-U.TIRF-RES-CMB-IND ( #I )
            //*  EINCHG
            //*  JB1011 2 LINES
            short decideConditionsMet1224 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF EXTRACT-PRDCT-CDE;//Natural: VALUE 'C'
            if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("C"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Cref.getValue(pnd_J_Pnd_K.getInt() + 1));                            //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-CREF ( #K )
            }                                                                                                                                                             //Natural: VALUE 'T', 'A'
            else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("T") || ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("A"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Tiaa.getValue(pnd_J_Pnd_K.getInt() + 1));                            //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-TIAA ( #K )
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("L"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Life.getValue(pnd_J_Pnd_K.getInt() + 1));                            //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-LIFE ( #K )
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("S"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Tcii.getValue(pnd_J_Pnd_K.getInt() + 1));                            //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-TCII ( #K )
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("X"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Trst.getValue(pnd_J_Pnd_K.getInt() + 1));                            //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-TRST ( #K )
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Prdct_Cde().equals("F"))))
            {
                decideConditionsMet1224++;
                ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Taxid().setValue(pnd_Ws_State_Tax_Id_Fsb.getValue(pnd_J_Pnd_K.getInt() + 1));                             //Natural: ASSIGN EXTRACT-STATE-TAXID := #WS-STATE-TAX-ID-FSB ( #K )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  07-19-99 FRANK
        if (condition(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Not_Det().getValue(pnd_I).equals("Y")))                                                                      //Natural: IF TIRF-RES-TAXABLE-NOT-DET ( #I ) = 'Y'
        {
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Ivc_Ind().setValue("U");                                                                                            //Natural: ASSIGN EXTRACT-IVC-IND := 'U'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Ivc_Ind().setValue("K");                                                                                            //Natural: ASSIGN EXTRACT-IVC-IND := 'K'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Tot_Contractual_Ivc());                                     //Natural: ASSIGN EXTRACT-TOT-IVC-CNTRT-AMT := TIRF-TOT-CONTRACTUAL-IVC
            //*  RCC
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Gross_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Gross_Amt().getValue(pnd_I));                                   //Natural: ASSIGN EXTRACT-FED-GROSS-AMT := FORM-U.TIRF-RES-GROSS-AMT ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_Taxable_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Taxable_Amt().getValue(pnd_I));                                   //Natural: ASSIGN EXTRACT-TAXABLE-AMT := FORM-U.TIRF-RES-TAXABLE-AMT ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_Fed_Wthhld_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_I));                              //Natural: ASSIGN EXTRACT-FED-WTHHLD-AMT := FORM-U.TIRF-RES-FED-TAX-WTHLD ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Gross_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_State_Distr().getValue(pnd_I));                                      //Natural: ASSIGN EXTRACT-ST-GROSS-AMT := FORM-U.TIRF-STATE-DISTR ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Ivc_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Ivc_Amt().getValue(pnd_I));                                        //Natural: ASSIGN EXTRACT-ST-IVC-AMT := FORM-U.TIRF-RES-IVC-AMT ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Gross_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Distr().getValue(pnd_I));                                       //Natural: ASSIGN EXTRACT-LCL-GROSS-AMT := FORM-U.TIRF-LOC-DISTR ( #I )
        ldaTwrl3300.getExtract_Npd_File_0_Extract_Res_Irr_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Res_Irr_Amt().getValue(pnd_I));                                       //Natural: ASSIGN EXTRACT-RES-IRR-AMT := TIRF-RES-IRR-AMT ( #I )
        //*  12-08-00 FRANK
        if (condition(ldaTwrl9705.getForm_U_Tirf_Res_Type().getValue(pnd_I).equals("1")))                                                                                 //Natural: IF TIRF-RES-TYPE ( #I ) = '1'
        {
            ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Wthhld_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_State_Tax_Wthld().getValue(pnd_I));                             //Natural: ASSIGN EXTRACT-ST-WTHHLD-AMT := FORM-U.TIRF-STATE-TAX-WTHLD ( #I )
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Wthhld_Amt().setValue(ldaTwrl9705.getForm_U_Tirf_Loc_Tax_Wthld().getValue(pnd_I));                              //Natural: ASSIGN EXTRACT-LCL-WTHHLD-AMT := FORM-U.TIRF-LOC-TAX-WTHLD ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3300.getExtract_Npd_File_0_Extract_St_Wthhld_Amt().reset();                                                                                            //Natural: RESET EXTRACT-ST-WTHHLD-AMT EXTRACT-LCL-WTHHLD-AMT
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Lcl_Wthhld_Amt().reset();
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3300.getExtract_Npd_File_0_Extract_State_Hardcopy_Ind().setValue(ldaTwrl9705.getForm_U_Tirf_State_Hardcopy_Ind().getValue(pnd_I));                         //Natural: ASSIGN EXTRACT-STATE-HARDCOPY-IND := TIRF-STATE-HARDCOPY-IND ( #I )
        //*                                              /* 10-06-99 FRANK
        //*  12-06-99 FRANK
        if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                           //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
        {
            //*  12-06-99 FRANK
            getWorkFiles().write(2, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                                          //Natural: WRITE WORK FILE 02 EXTRACT-NPD-FILE-0
            //*  MUKHERR
            //*  MUKHERR
            if (condition(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue("*").equals(1) && ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().equals("5")))                          //Natural: IF TIRF-SYS-ERR ( * ) EQ 1 AND FORM-U.TIRF-TAX-ID-TYPE = '5'
            {
                //*  MUKHERR
                getWorkFiles().write(1, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                                      //Natural: WRITE WORK FILE 01 EXTRACT-NPD-FILE-0
                //*  MUKHERR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Read_Ctr.less(40)))                                                                                                                         //Natural: IF #READ-CTR LT 40
            {
                getReports().write(0, "STATE REJECT WORKFILE 2");                                                                                                         //Natural: WRITE ( 0 ) 'STATE REJECT WORKFILE 2'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(1, false, ldaTwrl3300.getExtract_Npd_File_0());                                                                                          //Natural: WRITE WORK FILE 01 EXTRACT-NPD-FILE-0
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Read_Ctr.less(40)))                                                                                                                             //Natural: IF #READ-CTR LT 40
        {
            getReports().write(0, "STATE GOOD WORKFILE 1");                                                                                                               //Natural: WRITE ( 0 ) 'STATE GOOD WORKFILE 1'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  02-24-00 FRANK
    private void sub_Accum_Wthhold_Recon() throws Exception                                                                                                               //Natural: ACCUM-WTHHOLD-RECON
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        short decideConditionsMet1300 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TIRF-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("C"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Cref_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                               //Natural: ADD TIRF-GROSS-AMT TO CREF-GROSS-AMT ( 1 )
                //*      ADD TIRF-GROSS-AMT      TO  CREF-GROSS-AMT(2)
                pnd_Wh_Recon_Accum_Cref_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                   //Natural: ADD TIRF-IVC-AMT TO CREF-IVC-AMT ( 1 )
                //*      ADD TIRF-IVC-AMT        TO  CREF-IVC-AMT(2)
                pnd_Wh_Recon_Accum_Cref_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                           //Natural: ADD TIRF-TAXABLE-AMT TO CREF-TAXABLE-AMT ( 1 )
                //*      ADD TIRF-TAXABLE-AMT    TO  CREF-TAXABLE-AMT(2)
                pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO CREF-TAX-WTHLD-R ( 1 )
                //*      ADD TIRF-FED-TAX-WTHLD  TO  CREF-TAX-WTHLD-R(2)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    //*        ADD TIRF-INT-AMT        TO  CREF-INT-AMT(1)
                    pnd_Wh_Recon_Accum_Cref_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                               //Natural: ADD TIRF-INT-AMT TO CREF-INT-AMT ( 2 )
                    //*        ADD TIRF-FED-TAX-WTHLD  TO  CREF-TAX-WTHLD-I(1)
                    pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO CREF-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("L"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Life_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                               //Natural: ADD TIRF-GROSS-AMT TO LIFE-GROSS-AMT ( 1 )
                //*      ADD  TIRF-GROSS-AMT      TO  LIFE-GROSS-AMT(2)
                pnd_Wh_Recon_Accum_Life_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                   //Natural: ADD TIRF-IVC-AMT TO LIFE-IVC-AMT ( 1 )
                //*      ADD  TIRF-IVC-AMT        TO  LIFE-IVC-AMT(2)
                pnd_Wh_Recon_Accum_Life_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                           //Natural: ADD TIRF-TAXABLE-AMT TO LIFE-TAXABLE-AMT ( 1 )
                //*      ADD  TIRF-TAXABLE-AMT    TO  LIFE-TAXABLE-AMT(2)
                pnd_Wh_Recon_Accum_Life_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO LIFE-TAX-WTHLD-R ( 1 )
                //*      ADD  TIRF-FED-TAX-WTHLD  TO  LIFE-TAX-WTHLD-R(2)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    //*        ADD  TIRF-INT-AMT        TO  LIFE-INT-AMT(1)
                    pnd_Wh_Recon_Accum_Life_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                               //Natural: ADD TIRF-INT-AMT TO LIFE-INT-AMT ( 2 )
                    //*        ADD  TIRF-FED-TAX-WTHLD  TO  LIFE-TAX-WTHLD-I(1)
                    pnd_Wh_Recon_Accum_Life_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO LIFE-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
                //*  EINCHG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'T','A'
        else if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("T") || ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("A"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                               //Natural: ADD TIRF-GROSS-AMT TO TIAA-GROSS-AMT ( 1 )
                //*      ADD  TIRF-GROSS-AMT      TO  TIAA-GROSS-AMT(2)
                pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                   //Natural: ADD TIRF-IVC-AMT TO TIAA-IVC-AMT ( 1 )
                //*      ADD  TIRF-IVC-AMT        TO  TIAA-IVC-AMT(2)
                pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                           //Natural: ADD TIRF-TAXABLE-AMT TO TIAA-TAXABLE-AMT ( 1 )
                //*      ADD  TIRF-TAXABLE-AMT    TO  TIAA-TAXABLE-AMT(2)
                pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO TIAA-TAX-WTHLD-R ( 1 )
                //*      ADD  TIRF-FED-TAX-WTHLD  TO  TIAA-TAX-WTHLD-R(2)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    //*        ADD  TIRF-INT-AMT        TO  TIAA-INT-AMT(1)
                    pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                               //Natural: ADD TIRF-INT-AMT TO TIAA-INT-AMT ( 2 )
                    //*        ADD  TIRF-FED-TAX-WTHLD  TO  TIAA-TAX-WTHLD-I(1)
                    pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO TIAA-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("S"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Tcii_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                               //Natural: ADD TIRF-GROSS-AMT TO TCII-GROSS-AMT ( 1 )
                //*      ADD  TIRF-GROSS-AMT      TO  TCII-GROSS-AMT(2)
                pnd_Wh_Recon_Accum_Tcii_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                   //Natural: ADD TIRF-IVC-AMT TO TCII-IVC-AMT ( 1 )
                //*      ADD  TIRF-IVC-AMT        TO  TCII-IVC-AMT(2)
                pnd_Wh_Recon_Accum_Tcii_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                           //Natural: ADD TIRF-TAXABLE-AMT TO TCII-TAXABLE-AMT ( 1 )
                //*      ADD  TIRF-TAXABLE-AMT    TO  TCII-TAXABLE-AMT(2)
                pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO TCII-TAX-WTHLD-R ( 1 )
                //*      ADD  TIRF-FED-TAX-WTHLD  TO  TCII-TAX-WTHLD-R(2)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    //*        ADD  TIRF-INT-AMT        TO  TCII-INT-AMT(1)
                    pnd_Wh_Recon_Accum_Tcii_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                               //Natural: ADD TIRF-INT-AMT TO TCII-INT-AMT ( 2 )
                    //*        ADD  TIRF-FED-TAX-WTHLD  TO  TCII-TAX-WTHLD-I(1)
                    pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO TCII-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("X"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Trst_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                               //Natural: ADD TIRF-GROSS-AMT TO TRST-GROSS-AMT ( 1 )
                //*      ADD  TIRF-GROSS-AMT      TO  TRST-GROSS-AMT(2)
                pnd_Wh_Recon_Accum_Trst_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                   //Natural: ADD TIRF-IVC-AMT TO TRST-IVC-AMT ( 1 )
                //*      ADD  TIRF-IVC-AMT        TO  TRST-IVC-AMT(2)
                pnd_Wh_Recon_Accum_Trst_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                           //Natural: ADD TIRF-TAXABLE-AMT TO TRST-TAXABLE-AMT ( 1 )
                //*      ADD  TIRF-TAXABLE-AMT    TO  TRST-TAXABLE-AMT(2)
                pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD TIRF-FED-TAX-WTHLD TO TRST-TAX-WTHLD-R ( 1 )
                //*      ADD  TIRF-FED-TAX-WTHLD  TO  TRST-TAX-WTHLD-R(2)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    //*        ADD  TIRF-INT-AMT        TO  TRST-INT-AMT(1)
                    pnd_Wh_Recon_Accum_Trst_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                               //Natural: ADD TIRF-INT-AMT TO TRST-INT-AMT ( 2 )
                    //*        ADD  TIRF-FED-TAX-WTHLD  TO  TRST-TAX-WTHLD-I(1)
                    pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                     //Natural: ADD TIRF-FED-TAX-WTHLD TO TRST-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
                //*  JB1011  NEXT 13 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("F"))))
        {
            decideConditionsMet1300++;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF TIRF-FORM-TYPE = 01
            {
                pnd_Wh_Recon_Accum_Fsb_Gross_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Gross_Amt());                                                                //Natural: ADD TIRF-GROSS-AMT TO FSB-GROSS-AMT ( 1 )
                pnd_Wh_Recon_Accum_Fsb_Ivc_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Ivc_Amt());                                                                    //Natural: ADD TIRF-IVC-AMT TO FSB-IVC-AMT ( 1 )
                pnd_Wh_Recon_Accum_Fsb_Taxable_Amt.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Taxable_Amt());                                                            //Natural: ADD TIRF-TAXABLE-AMT TO FSB-TAXABLE-AMT ( 1 )
                pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_R.getValue(1).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                          //Natural: ADD TIRF-FED-TAX-WTHLD TO FSB-TAX-WTHLD-R ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Form_Type().equals(2)))                                                                                          //Natural: IF TIRF-FORM-TYPE = 02
                {
                    pnd_Wh_Recon_Accum_Fsb_Int_Amt.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Int_Amt());                                                                //Natural: ADD TIRF-INT-AMT TO FSB-INT-AMT ( 2 )
                    pnd_Wh_Recon_Accum_Fsb_Tax_Wthld_I.getValue(2).nadd(ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld());                                                      //Natural: ADD TIRF-FED-TAX-WTHLD TO FSB-TAX-WTHLD-I ( 2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Updt_Cntrl_Rec_With_Wthld_Recon() throws Exception                                                                                                   //Natural: UPDT-CNTRL-REC-WITH-WTHLD-RECON
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------------
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Gross_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Gross_Amt.getValue(pnd_I));                             //Natural: ASSIGN TIRCNTL-RPT-CREF-GROSS-AMT ( 1 ) := CREF-GROSS-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Taxable_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Taxable_Amt.getValue(pnd_I));                         //Natural: ASSIGN TIRCNTL-RPT-CREF-TAXABLE-AMT ( 1 ) := CREF-TAXABLE-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Ivc_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Ivc_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-CREF-IVC-AMT ( 1 ) := CREF-IVC-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-CREF-TAX-WTHLD ( 1 ) := CREF-TAX-WTHLD-R ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Int_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Int_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-CREF-INT-AMT ( 1 ) := CREF-INT-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-CREF-TAX-WTHLD ( 1 ) := CREF-TAX-WTHLD-I ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Gross_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Gross_Amt.getValue(pnd_I));                             //Natural: ASSIGN TIRCNTL-RPT-LIFE-GROSS-AMT ( 1 ) := LIFE-GROSS-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Taxable_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Taxable_Amt.getValue(pnd_I));                         //Natural: ASSIGN TIRCNTL-RPT-LIFE-TAXABLE-AMT ( 1 ) := LIFE-TAXABLE-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Ivc_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Ivc_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-LIFE-IVC-AMT ( 1 ) := LIFE-IVC-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Tax_Wthld_R.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-LIFE-TAX-WTHLD ( 1 ) := LIFE-TAX-WTHLD-R ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Int_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Int_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-LIFE-INT-AMT ( 1 ) := LIFE-INT-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Life_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Life_Tax_Wthld_I.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-LIFE-TAX-WTHLD ( 1 ) := LIFE-TAX-WTHLD-I ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Gross_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(pnd_I));                             //Natural: ASSIGN TIRCNTL-RPT-TIAA-GROSS-AMT ( 1 ) := TIAA-GROSS-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Taxable_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(pnd_I));                         //Natural: ASSIGN TIRCNTL-RPT-TIAA-TAXABLE-AMT ( 1 ) := TIAA-TAXABLE-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Ivc_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-TIAA-IVC-AMT ( 1 ) := TIAA-IVC-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-TIAA-TAX-WTHLD ( 1 ) := TIAA-TAX-WTHLD-R ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Int_Amt().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(pnd_I));                                 //Natural: ASSIGN TIRCNTL-RPT-TIAA-INT-AMT ( 1 ) := TIAA-INT-AMT ( #I )
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Tiaa_Tax_Wthld().getValue(1).setValue(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(pnd_I));                           //Natural: ASSIGN TIRCNTL-RPT-TIAA-TAX-WTHLD ( 1 ) := TIAA-TAX-WTHLD-I ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        //* * COMMENTED-OUT FRANK 09-27-02
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Control_Record() throws Exception                                                                                                              //Natural: CHECK-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3300|sub_Check_Control_Record");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                getReports().display(0, pnd_Input_Parm_Pnd_Form,pnd_Input_Parm_Pnd_Tax_Year,pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok,pnd_Input_Parm_Pnd_Test_Run);        //Natural: DISPLAY #INPUT-PARM.#FORM #INPUT-PARM.#TAX-YEAR #INPUT-PARM.#NOT-FIRST-TIME-RUN-OK #INPUT-PARM.#TEST-RUN
                if (Global.isEscape()) return;
                if (condition(pnd_Input_Parm_Pnd_Tax_Year.equals(getZero())))                                                                                             //Natural: IF #INPUT-PARM.#TAX-YEAR = 0
                {
                    pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                   //Natural: ASSIGN #WS-TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Tax_Year.setValue(pnd_Input_Parm_Pnd_Tax_Year);                                                                                                //Natural: ASSIGN #WS-TAX-YEAR := #INPUT-PARM.#TAX-YEAR
                    //*  01/30/08 - AY
                    //*  1099-R
                }                                                                                                                                                         //Natural: END-IF
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                     //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS-TAX-YEAR
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(1);                                                                                                   //Natural: ASSIGN #TWRATBL4.#FORM-IND := 1
                DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                //*  01/30/08
                //*  01-18-02 J.ROTHOLZ
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Form...............: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(1 + 1),NEWLINE,new  //Natural: WRITE ( 01 ) / 10T 'Form...............: ' #TWRAFRMN.#FORM-NAME ( 1 ) / 10T 'Original Reporting.: ' #WS-TAX-YEAR
                    TabSetting(10),"Original Reporting.: ",pnd_Ws_Tax_Year);
                if (Global.isEscape()) return;
                //*  02-23-00 FRANK
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).greater(getZero())))                                                              //Natural: IF #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) > 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  01/30/08 - AY
                    //*  02-23-00
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(15),"Original Reporting process for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(1 + 1),new  //Natural: WRITE ( 01 ) '***' 15T 'Original Reporting process for' #TWRAFRMN.#FORM-NAME ( 1 ) 67T '***' / '***' 15T 'already executed on' #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) 67T '***'
                        TabSetting(67),"***",NEWLINE,"***",new TabSetting(15),"already executed on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1),new 
                        TabSetting(67),"***");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(102);  if (true) return;                                                                                                            //Natural: TERMINATE 102
                }                                                                                                                                                         //Natural: END-IF
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Control_Reports() throws Exception                                                                                                                   //Natural: CONTROL-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        getReports().newPage(new ReportSpecification(4));                                                                                                                 //Natural: NEWPAGE ( 04 )
        if (condition(Global.isEscape())){return;}
        getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Payment System",new  //Natural: WRITE ( 04 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Payment System' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' 'TWRP3300-01' 28X 'Extract Control Report' #WS-TAX-YEAR ( EM = 9999 ) / 51X 'IRS Original Reporting'
            TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-","TWRP3300-01",new 
            ColumnSpacing(28),"Extract Control Report",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,new ColumnSpacing(51),"IRS Original Reporting");
        if (Global.isEscape()) return;
        getReports().skip(4, 6);                                                                                                                                          //Natural: SKIP ( 04 ) 6
        pnd_Grand_Tot.compute(new ComputeParameters(false, pnd_Grand_Tot), pnd_Cref_I.add(pnd_Life_I).add(pnd_Tiaa_I).add(pnd_Tcii_I).add(pnd_Trst_I).add(pnd_Cref_R).add(pnd_Life_R).add(pnd_Tiaa_R).add(pnd_Tcii_R).add(pnd_Trst_R)); //Natural: ADD #CREF-I #LIFE-I #TIAA-I #TCII-I #TRST-I #CREF-R #LIFE-R #TIAA-R #TCII-R #TRST-R GIVING #GRAND-TOT
        pnd_Grand_Tot_St.compute(new ComputeParameters(false, pnd_Grand_Tot_St), pnd_Cref_I_St.add(pnd_Life_I_St).add(pnd_Tiaa_I_St).add(pnd_Tcii_I_St).add(pnd_Trst_I_St).add(pnd_Cref_R_St).add(pnd_Life_R_St).add(pnd_Tiaa_R_St).add(pnd_Tcii_R_St).add(pnd_Trst_R_St)); //Natural: ADD #CREF-I-ST #LIFE-I-ST #TIAA-I-ST #TCII-I-ST #TRST-I-ST #CREF-R-ST #LIFE-R-ST #TIAA-R-ST #TCII-R-ST #TRST-R-ST GIVING #GRAND-TOT-ST
        pnd_Grand_Tot_X.compute(new ComputeParameters(false, pnd_Grand_Tot_X), pnd_Cref_I_X.add(pnd_Life_I_X).add(pnd_Tiaa_I_X).add(pnd_Tcii_I_X).add(pnd_Trst_I_X).add(pnd_Cref_R_X).add(pnd_Life_R_X).add(pnd_Tiaa_R_X).add(pnd_Tcii_R_X).add(pnd_Trst_R_X)); //Natural: ADD #CREF-I-X #LIFE-I-X #TIAA-I-X #TCII-I-X #TRST-I-X #CREF-R-X #LIFE-R-X #TIAA-R-X #TCII-R-X #TRST-R-X GIVING #GRAND-TOT-X
        //*  04/14/06
        //*  04/14/06
        //*  02-29-00 FRANK
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(34),"FORM        STATE",NEWLINE,"TOTALS",new ColumnSpacing(27),"COUNT       COUNT",NEWLINE,"----------------------------    -------     -------",NEWLINE,NEWLINE,"DATABASE FORM RECORDS READ  ",pnd_Read_Ctr,  //Natural: WRITE ( 04 ) 34T 'FORM        STATE' / 'TOTALS' 27X 'COUNT       COUNT' / '----------------------------    -------     -------' // 'DATABASE FORM RECORDS READ  ' #READ-CTR ( EM = ZZ,ZZZ,ZZ9 ) // 'ACTIVE    FORM RECS SELECTED' #ACCEPT-CTR ( EM = ZZ,ZZZ,ZZ9 ) / 'ZERO AMT  FORM RECS BYPASSED' #ZEROES-CTR ( EM = ZZ,ZZZ,ZZ9 ) /// 'ACTIVE FORM RECORD' 34T 'FORM        STATE' / 'TOTALS BY PRODUCT/TYPE' 11X 'COUNT       COUNT' / '----------------------------    -------     -------' // '** TIAA **' / '1099-INT                    ' #TIAA-I ( EM = ZZ,ZZZ,ZZ9 ) 2X #TIAA-I-ST ( EM = ZZ,ZZZ,ZZ9 ) / '1099-R                      ' #TIAA-R ( EM = ZZ,ZZZ,ZZ9 ) 2X #TIAA-R-ST ( EM = ZZ,ZZZ,ZZ9 ) // '** TRST **' / '1099-INT                    ' #TRST-I ( EM = ZZ,ZZZ,ZZ9 ) 2X #TRST-I-ST ( EM = ZZ,ZZZ,ZZ9 ) / '1099-R                      ' #TRST-R ( EM = ZZ,ZZZ,ZZ9 ) 2X #TRST-R-ST ( EM = ZZ,ZZZ,ZZ9 ) // 32X '-------     -------' / 'GRAND TOTALS                ' #GRAND-TOT ( EM = ZZ,ZZZ,ZZ9 ) 2X #GRAND-TOT-ST ( EM = ZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"ACTIVE    FORM RECS SELECTED",pnd_Accept_Ctr, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"ZERO AMT  FORM RECS BYPASSED",pnd_Zeroes_Ctr, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"ACTIVE FORM RECORD",new TabSetting(34),"FORM        STATE",NEWLINE,"TOTALS BY PRODUCT/TYPE",new 
            ColumnSpacing(11),"COUNT       COUNT",NEWLINE,"----------------------------    -------     -------",NEWLINE,NEWLINE,"** TIAA **",NEWLINE,"1099-INT                    ",pnd_Tiaa_I, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Tiaa_I_St, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"1099-R                      ",pnd_Tiaa_R, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Tiaa_R_St, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"** TRST **",NEWLINE,"1099-INT                    ",pnd_Trst_I, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Trst_I_St, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"1099-R                      ",pnd_Trst_R, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Trst_R_St, new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new ColumnSpacing(32),"-------     -------",NEWLINE,"GRAND TOTALS                ",pnd_Grand_Tot, 
            new ReportEditMask ("ZZ,ZZZ,ZZ9"),new ColumnSpacing(2),pnd_Grand_Tot_St, new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                             //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(1)), pnd_Wh_Recon_Accum_Cref_Gross_Amt.getValue(1).add(pnd_Wh_Recon_Accum_Life_Gross_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Gross_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Gross_Amt.getValue(1))); //Natural: ASSIGN #TOTAL-GROSS ( 1 ) := CREF-GROSS-AMT ( 1 ) + LIFE-GROSS-AMT ( 1 ) + TIAA-GROSS-AMT ( 1 ) + TCII-GROSS-AMT ( 1 ) + TRST-GROSS-AMT ( 1 )
        pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(1)), pnd_Wh_Recon_Accum_Cref_Int_Amt.getValue(1).add(pnd_Wh_Recon_Accum_Life_Int_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Int_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Int_Amt.getValue(1))); //Natural: ASSIGN #TOTAL-INTEREST ( 1 ) := CREF-INT-AMT ( 1 ) + LIFE-INT-AMT ( 1 ) + TIAA-INT-AMT ( 1 ) + TCII-INT-AMT ( 1 ) + TRST-INT-AMT ( 1 )
        pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(1)), pnd_Wh_Recon_Accum_Cref_Taxable_Amt.getValue(1).add(pnd_Wh_Recon_Accum_Life_Taxable_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Taxable_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Taxable_Amt.getValue(1))); //Natural: ASSIGN #TOTAL-TAXABLE ( 1 ) := CREF-TAXABLE-AMT ( 1 ) + LIFE-TAXABLE-AMT ( 1 ) + TIAA-TAXABLE-AMT ( 1 ) + TCII-TAXABLE-AMT ( 1 ) + TRST-TAXABLE-AMT ( 1 )
        pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(1)), pnd_Wh_Recon_Accum_Cref_Ivc_Amt.getValue(1).add(pnd_Wh_Recon_Accum_Life_Ivc_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Ivc_Amt.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Ivc_Amt.getValue(1))); //Natural: ASSIGN #TOTAL-IVC ( 1 ) := CREF-IVC-AMT ( 1 ) + LIFE-IVC-AMT ( 1 ) + TIAA-IVC-AMT ( 1 ) + TCII-IVC-AMT ( 1 ) + TRST-IVC-AMT ( 1 )
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(1)),                    //Natural: ASSIGN #TOTAL-WITHHELD-R ( 1 ) := CREF-TAX-WTHLD-R ( 1 ) + LIFE-TAX-WTHLD-R ( 1 ) + TIAA-TAX-WTHLD-R ( 1 ) + TCII-TAX-WTHLD-R ( 1 ) + TRST-TAX-WTHLD-R ( 1 )
            pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R.getValue(1).add(pnd_Wh_Recon_Accum_Life_Tax_Wthld_R.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R.getValue(1)));
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I.getValue(1).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I.getValue(1)),                    //Natural: ASSIGN #TOTAL-WITHHELD-I ( 1 ) := CREF-TAX-WTHLD-I ( 1 ) + LIFE-TAX-WTHLD-I ( 1 ) + TIAA-TAX-WTHLD-I ( 1 ) + TCII-TAX-WTHLD-I ( 1 ) + TRST-TAX-WTHLD-I ( 1 )
            pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I.getValue(1).add(pnd_Wh_Recon_Accum_Life_Tax_Wthld_I.getValue(1)).add(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(1)).add(pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I.getValue(1)).add(pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I.getValue(1)));
        pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(2)), pnd_Wh_Recon_Accum_Cref_Gross_Amt.getValue(2).add(pnd_Wh_Recon_Accum_Life_Gross_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Gross_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Gross_Amt.getValue(2))); //Natural: ASSIGN #TOTAL-GROSS ( 2 ) := CREF-GROSS-AMT ( 2 ) + LIFE-GROSS-AMT ( 2 ) + TIAA-GROSS-AMT ( 2 ) + TCII-GROSS-AMT ( 2 ) + TRST-GROSS-AMT ( 2 )
        pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(2)), pnd_Wh_Recon_Accum_Cref_Int_Amt.getValue(2).add(pnd_Wh_Recon_Accum_Life_Int_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Int_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Int_Amt.getValue(2))); //Natural: ASSIGN #TOTAL-INTEREST ( 2 ) := CREF-INT-AMT ( 2 ) + LIFE-INT-AMT ( 2 ) + TIAA-INT-AMT ( 2 ) + TCII-INT-AMT ( 2 ) + TRST-INT-AMT ( 2 )
        pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(2)), pnd_Wh_Recon_Accum_Cref_Taxable_Amt.getValue(2).add(pnd_Wh_Recon_Accum_Life_Taxable_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Taxable_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Taxable_Amt.getValue(2))); //Natural: ASSIGN #TOTAL-TAXABLE ( 2 ) := CREF-TAXABLE-AMT ( 2 ) + LIFE-TAXABLE-AMT ( 2 ) + TIAA-TAXABLE-AMT ( 2 ) + TCII-TAXABLE-AMT ( 2 ) + TRST-TAXABLE-AMT ( 2 )
        pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(2)), pnd_Wh_Recon_Accum_Cref_Ivc_Amt.getValue(2).add(pnd_Wh_Recon_Accum_Life_Ivc_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Ivc_Amt.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Ivc_Amt.getValue(2))); //Natural: ASSIGN #TOTAL-IVC ( 2 ) := CREF-IVC-AMT ( 2 ) + LIFE-IVC-AMT ( 2 ) + TIAA-IVC-AMT ( 2 ) + TCII-IVC-AMT ( 2 ) + TRST-IVC-AMT ( 2 )
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(2)),                    //Natural: ASSIGN #TOTAL-WITHHELD-R ( 2 ) := CREF-TAX-WTHLD-R ( 2 ) + LIFE-TAX-WTHLD-R ( 2 ) + TIAA-TAX-WTHLD-R ( 2 ) + TCII-TAX-WTHLD-R ( 2 ) + TRST-TAX-WTHLD-R ( 2 )
            pnd_Wh_Recon_Accum_Cref_Tax_Wthld_R.getValue(2).add(pnd_Wh_Recon_Accum_Life_Tax_Wthld_R.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R.getValue(2)));
        pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I.getValue(2).compute(new ComputeParameters(false, pnd_Wh_Recon_Accum_Pnd_Total_Withheld_I.getValue(2)),                    //Natural: ASSIGN #TOTAL-WITHHELD-I ( 2 ) := CREF-TAX-WTHLD-I ( 2 ) + LIFE-TAX-WTHLD-I ( 2 ) + TIAA-TAX-WTHLD-I ( 2 ) + TCII-TAX-WTHLD-I ( 2 ) + TRST-TAX-WTHLD-I ( 2 )
            pnd_Wh_Recon_Accum_Cref_Tax_Wthld_I.getValue(2).add(pnd_Wh_Recon_Accum_Life_Tax_Wthld_I.getValue(2)).add(pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(2)).add(pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I.getValue(2)).add(pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I.getValue(2)));
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(53),"Tax Withholding Reporting System",new  //Natural: WRITE ( 02 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Tax Withholding Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 54T 'WITHHOLDING RECONCILIATION' / 62T 'Tax year:' #WS-TAX-YEAR //
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(54),"WITHHOLDING RECONCILIATION",NEWLINE,new TabSetting(62),"Tax year:",pnd_Ws_Tax_Year,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(15),"1099-R",new TabSetting(55),"1099-INT",NEWLINE,"-",new RepeatItem(34),new                   //Natural: WRITE ( 02 ) / 15T '1099-R' 55T '1099-INT' / '-' ( 34 ) 42T '-' ( 34 ) / 'TIAA GROSS      ' TIAA-GROSS-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA GROSS      ' TIAA-GROSS-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA INTEREST   ' TIAA-INT-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA INTEREST   ' TIAA-INT-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA TAXABLE    ' TIAA-TAXABLE-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA TAXABLE    ' TIAA-TAXABLE-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA IVC        ' TIAA-IVC-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA IVC        ' TIAA-IVC-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA WTHLD 1099R' TIAA-TAX-WTHLD-R ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA WTHLD 1099R' TIAA-TAX-WTHLD-R ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TIAA WTHLD 1099I' TIAA-TAX-WTHLD-I ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TIAA WTHLD 1099I' TIAA-TAX-WTHLD-I ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII GROSS      ' TCII-GROSS-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII GROSS      ' TCII-GROSS-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII INTEREST   ' TCII-INT-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII INTEREST   ' TCII-INT-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII TAXABLE    ' TCII-TAXABLE-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII TAXABLE    ' TCII-TAXABLE-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII IVC        ' TCII-IVC-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII IVC        ' TCII-IVC-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII WTHLD 1099R' TCII-TAX-WTHLD-R ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII WTHLD 1099R' TCII-TAX-WTHLD-R ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TCII WTHLD 1099I' TCII-TAX-WTHLD-I ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TCII WTHLD 1099I' TCII-TAX-WTHLD-I ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST GROSS      ' TRST-GROSS-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST GROSS      ' TRST-GROSS-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST INTEREST   ' TRST-INT-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST INTEREST   ' TRST-INT-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST TAXABLE    ' TRST-TAXABLE-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST TAXABLE    ' TRST-TAXABLE-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST IVC        ' TRST-IVC-AMT ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST IVC        ' TRST-IVC-AMT ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST WTHLD 1099R' TRST-TAX-WTHLD-R ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST WTHLD 1099R' TRST-TAX-WTHLD-R ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TRST WTHLD 1099I' TRST-TAX-WTHLD-I ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TRST WTHLD 1099I' TRST-TAX-WTHLD-I ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) // 'TOTAL GROSS     ' #TOTAL-GROSS ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TOTAL GROSS     ' #TOTAL-GROSS ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL INTEREST  ' #TOTAL-INTEREST ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TOTAL INTEREST  ' #TOTAL-INTEREST ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL TAXABLE   ' #TOTAL-TAXABLE ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TOTAL TAXABLE   ' #TOTAL-TAXABLE ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL IVC       ' #TOTAL-IVC ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TOTAL IVC       ' #TOTAL-IVC ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL WITHHELD  ' #TOTAL-WITHHELD-R ( 1 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 42T 'TOTAL WITHHELD  ' #TOTAL-WITHHELD-R ( 2 ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 )
            TabSetting(42),"-",new RepeatItem(34),NEWLINE,"TIAA GROSS      ",pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TIAA GROSS      ",pnd_Wh_Recon_Accum_Tiaa_Gross_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA INTEREST   ",pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TIAA INTEREST   ",pnd_Wh_Recon_Accum_Tiaa_Int_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA TAXABLE    ",pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TIAA TAXABLE    ",pnd_Wh_Recon_Accum_Tiaa_Taxable_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA IVC        ",pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TIAA IVC        ",pnd_Wh_Recon_Accum_Tiaa_Ivc_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA WTHLD 1099R",pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TIAA WTHLD 1099R",pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_R.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TIAA WTHLD 1099I",pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TIAA WTHLD 1099I",pnd_Wh_Recon_Accum_Tiaa_Tax_Wthld_I.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII GROSS      ",pnd_Wh_Recon_Accum_Tcii_Gross_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TCII GROSS      ",pnd_Wh_Recon_Accum_Tcii_Gross_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII INTEREST   ",pnd_Wh_Recon_Accum_Tcii_Int_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TCII INTEREST   ",pnd_Wh_Recon_Accum_Tcii_Int_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII TAXABLE    ",pnd_Wh_Recon_Accum_Tcii_Taxable_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TCII TAXABLE    ",pnd_Wh_Recon_Accum_Tcii_Taxable_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII IVC        ",pnd_Wh_Recon_Accum_Tcii_Ivc_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TCII IVC        ",pnd_Wh_Recon_Accum_Tcii_Ivc_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII WTHLD 1099R",pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TCII WTHLD 1099R",pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_R.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TCII WTHLD 1099I",pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TCII WTHLD 1099I",pnd_Wh_Recon_Accum_Tcii_Tax_Wthld_I.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST GROSS      ",pnd_Wh_Recon_Accum_Trst_Gross_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TRST GROSS      ",pnd_Wh_Recon_Accum_Trst_Gross_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST INTEREST   ",pnd_Wh_Recon_Accum_Trst_Int_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TRST INTEREST   ",pnd_Wh_Recon_Accum_Trst_Int_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST TAXABLE    ",pnd_Wh_Recon_Accum_Trst_Taxable_Amt.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TRST TAXABLE    ",pnd_Wh_Recon_Accum_Trst_Taxable_Amt.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST IVC        ",pnd_Wh_Recon_Accum_Trst_Ivc_Amt.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TRST IVC        ",pnd_Wh_Recon_Accum_Trst_Ivc_Amt.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST WTHLD 1099R",pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TRST WTHLD 1099R",pnd_Wh_Recon_Accum_Trst_Tax_Wthld_R.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TRST WTHLD 1099I",pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TRST WTHLD 1099I",pnd_Wh_Recon_Accum_Trst_Tax_Wthld_I.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"TOTAL GROSS     ",pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TOTAL GROSS     ",pnd_Wh_Recon_Accum_Pnd_Total_Gross.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL INTEREST  ",pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TOTAL INTEREST  ",pnd_Wh_Recon_Accum_Pnd_Total_Interest.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL TAXABLE   ",pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TOTAL TAXABLE   ",pnd_Wh_Recon_Accum_Pnd_Total_Taxable.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL IVC       ",pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(42),"TOTAL IVC       ",pnd_Wh_Recon_Accum_Pnd_Total_Ivc.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL WITHHELD  ",pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(42),"TOTAL WITHHELD  ",pnd_Wh_Recon_Accum_Pnd_Total_Withheld_R.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }
    //*  12-16-00 FRANK
    //*  02-29-00 FRANK
    private void sub_Update_Control_Record() throws Exception                                                                                                             //Natural: UPDATE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                             //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_I);                                                                                                   //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
            //*  02-23-00 FRANK
            DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(Global.getDATX());                                                                         //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := *DATX
            //*  02-24-00 FRANK
                                                                                                                                                                          //Natural: PERFORM UPDT-CNTRL-REC-WITH-WTHLD-RECON
            sub_Updt_Cntrl_Rec_With_Wthld_Recon();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Set_Irs_Unique_Id() throws Exception                                                                                                                 //Natural: SET-IRS-UNIQUE-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        short decideConditionsMet1550 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF EXTRACT-TYPE-CDE;//Natural: VALUE 'R'
        if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("R"))))
        {
            decideConditionsMet1550++;
            if (condition(!pnd_Ws_Extract_Irs_Form_Tin.getSubstring(10,1).equals(" ")))                                                                                   //Natural: IF SUBSTRING ( #WS-EXTRACT-IRS-FORM-TIN,10,1 ) NE ' '
            {
                pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Tin_4.setValue(pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_Contr);                                   //Natural: ASSIGN #WS-IRS-FORM-TIN-4 := #WS-EXTRACT-IRS-FORM-TIN-CONTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Tin_4.setValue(pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_4);                                       //Natural: ASSIGN #WS-IRS-FORM-TIN-4 := #WS-EXTRACT-IRS-FORM-TIN-4
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Cntrct_Payee.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                                   //Natural: ASSIGN #WS-IRS-FORM-CNTRCT-PAYEE := EXTRACT-CNTRCT-PY-NMBR
            pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Seq_Num.setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Form_Seq());                                                         //Natural: ASSIGN #WS-IRS-FORM-SEQ-NUM := FORM-U.TIRF-IRS-FORM-SEQ
            if (condition(pnd_Ws_Extract_Irs_Form_Loc_Cde.notEquals("   ")))                                                                                              //Natural: IF #WS-EXTRACT-IRS-FORM-LOC-CDE NE '   '
            {
                pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Res_Loc_Cde.setValue(pnd_Ws_Extract_Irs_Form_Loc_Cde);                                                           //Natural: ASSIGN #WS-IRS-FORM-RES-LOC-CDE := #WS-EXTRACT-IRS-FORM-LOC-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_1099r_Account_Num_Pnd_Ws_Irs_Form_Res_Loc_Cde.setValue(pnd_Ws_Extract_Irs_Form_State_Cde);                                                         //Natural: ASSIGN #WS-IRS-FORM-RES-LOC-CDE := #WS-EXTRACT-IRS-FORM-STATE-CDE
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_B_Contract_Payee().setValue(pnd_Ws_1099r_Account_Num);                                                          //Natural: ASSIGN EXTRACT-IRS-B-CONTRACT-PAYEE := #WS-1099R-ACCOUNT-NUM
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaTwrl3300.getExtract_Npd_File_0_Extract_Type_Cde().equals("I"))))
        {
            decideConditionsMet1550++;
            if (condition(!pnd_Ws_Extract_Irs_Form_Tin.getSubstring(10,1).equals(" ")))                                                                                   //Natural: IF SUBSTRING ( #WS-EXTRACT-IRS-FORM-TIN,10,1 ) NE ' '
            {
                pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Tin_4.setValue(pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_Contr);                                //Natural: ASSIGN #WS-IRS-FORMI-TIN-4 := #WS-EXTRACT-IRS-FORM-TIN-CONTR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Tin_4.setValue(pnd_Ws_Extract_Irs_Form_Tin_Pnd_Ws_Extract_Irs_Form_Tin_4);                                    //Natural: ASSIGN #WS-IRS-FORMI-TIN-4 := #WS-EXTRACT-IRS-FORM-TIN-4
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Cntrct_Payee.setValue(ldaTwrl3300.getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr());                                //Natural: ASSIGN #WS-IRS-FORMI-CNTRCT-PAYEE := EXTRACT-CNTRCT-PY-NMBR
            pnd_Ws_1099int_Account_Num_Pnd_Ws_Irs_Formi_Seq_Num.setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Form_Seq());                                                      //Natural: ASSIGN #WS-IRS-FORMI-SEQ-NUM := FORM-U.TIRF-IRS-FORM-SEQ
            ldaTwrl3300.getExtract_Npd_File_0_Extract_Irs_B_Contract_Payee().setValue(pnd_Ws_1099int_Account_Num);                                                        //Natural: ASSIGN EXTRACT-IRS-B-CONTRACT-PAYEE := #WS-1099INT-ACCOUNT-NUM
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_State_Count() throws Exception                                                                                                                       //Natural: STATE-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_State_Cnt.reset();                                                                                                                                            //Natural: RESET #STATE-CNT
        FOR04:                                                                                                                                                            //Natural: FOR #STATE-NDX = 1 TO FORM-U.C*TIRF-1099-R-STATE-GRP
        for (pnd_State_Ndx.setValue(1); condition(pnd_State_Ndx.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp())); pnd_State_Ndx.nadd(1))
        {
            if (condition(ldaTwrl9705.getForm_U_Tirf_Res_Type().getValue(pnd_State_Ndx).equals("1")))                                                                     //Natural: IF TIRF-RES-TYPE ( #STATE-NDX ) = '1'
            {
                pnd_State_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #STATE-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"Tax Withholding & Payment System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(40),"Tax Withholding and Payment System - Tax Year Control Report",new 
            TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(55),"IRS ORIGINAL REPORTING",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, pnd_Input_Parm_Pnd_Form,pnd_Input_Parm_Pnd_Tax_Year,pnd_Input_Parm_Pnd_Not_First_Time_Run_Ok,pnd_Input_Parm_Pnd_Test_Run);
    }
}
