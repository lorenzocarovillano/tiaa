/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:24 PM
**        * FROM NATURAL PROGRAM : Twrp5000
************************************************************
**        * FILE NAME            : Twrp5000.java
**        * CLASS NAME           : Twrp5000
**        * INSTANCE NAME        : Twrp5000
************************************************************
************************************************************************
** PROGRAM : TWRP5000
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: DRIVER FOR BATCH FORM ROLLUP - MASS MAILING
** HISTORY.....:
**    07/24/2012 MS SUNY CHANGES
**    09/12/2005 MS TOPS RELEASE 6.5 - SEP IRA
**    03/20/2003 RM DECREASE ET-LIMIT FOR NR4 AND 480.6 TO AVOID TIME-
**                  OUT PROBLEM.
**    11/06/2002 MS INCLUDE AN ADDITIONAL PARM FOR MULTI FORM ROLLUP
**    11/02/2001 MS MODIFIED FOR 2001 REPORTING.
**    11/30/2000 MS MODIFIED FOR 2000 REPORTING.
* 08/2016  F.ENDAYA    COR/NAS SUNSET. FE201608
* 24/10/2016 - SINHASN - VALIDATING IF SUNY-CUNY CLEANUP HAS PERFORMED
*                        BEFORE 1099R-INT FORM ROLLUP ELSE
*                        TERMINATE 1099R-INT FORM ROLLUP - TAG - SINHASN
**
* 4/7/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
* 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 - 5498 CHANGES
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5000 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaTwrapart pdaTwrapart;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwra5000 pdaTwra5000;
    private PdaTwra5006 pdaTwra5006;
    private PdaTwra5008 pdaTwra5008;
    private LdaTwrl5000 ldaTwrl5000;
    private LdaTwrl5009 ldaTwrl5009;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntl_R;
    private DbsField cntl_R_Tircntl_Tbl_Nbr;
    private DbsField cntl_R_Tircntl_Tax_Year;
    private DbsField cntl_R_Tircntl_Seq_Nbr;
    private DbsField cntl_R_Tircntl_Rpt_Form_Name;
    private DbsField pnd_Super_Cntl;

    private DbsGroup pnd_Super_Cntl__R_Field_1;
    private DbsField pnd_Super_Cntl_Pnd_S_Tbl;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year;

    private DbsGroup pnd_Super_Cntl__R_Field_2;
    private DbsField pnd_Super_Cntl_Pnd_S_Tax_Year_N;
    private DbsField pnd_Super_Cntl_Pnd_S_Form;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Multi_Form_Rollup;
    private DbsField pnd_Ws_Pnd_Not_First_Time_Run_Ok;
    private DbsField pnd_Ws_Pnd_Eff_Date;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_First_Time_Run_Only;
    private DbsField pnd_Ws_Pnd_Lib;
    private DbsField pnd_Job_Name;
    private DbsField pnd_Mq_Open;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;
    private DbsField pnd_Curr_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTwrapart = new PdaTwrapart(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwra5000 = new PdaTwra5000(localVariables);
        pdaTwra5006 = new PdaTwra5006(localVariables);
        pdaTwra5008 = new PdaTwra5008(localVariables);
        ldaTwrl5000 = new LdaTwrl5000();
        registerRecord(ldaTwrl5000);
        ldaTwrl5009 = new LdaTwrl5009();
        registerRecord(ldaTwrl5009);

        // Local Variables

        vw_cntl_R = new DataAccessProgramView(new NameInfo("vw_cntl_R", "CNTL-R"), "TIRCNTL_REPORTING_TBL_VIEW", "TIR_CONTROL");
        cntl_R_Tircntl_Tbl_Nbr = vw_cntl_R.getRecord().newFieldInGroup("cntl_R_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_R_Tircntl_Tax_Year = vw_cntl_R.getRecord().newFieldInGroup("cntl_R_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_R_Tircntl_Seq_Nbr = vw_cntl_R.getRecord().newFieldInGroup("cntl_R_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");
        cntl_R_Tircntl_Rpt_Form_Name = vw_cntl_R.getRecord().newFieldInGroup("cntl_R_Tircntl_Rpt_Form_Name", "TIRCNTL-RPT-FORM-NAME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "TIRCNTL_RPT_FORM_NAME");
        registerRecord(vw_cntl_R);

        pnd_Super_Cntl = localVariables.newFieldInRecord("pnd_Super_Cntl", "#SUPER-CNTL", FieldType.STRING, 12);

        pnd_Super_Cntl__R_Field_1 = localVariables.newGroupInRecord("pnd_Super_Cntl__R_Field_1", "REDEFINE", pnd_Super_Cntl);
        pnd_Super_Cntl_Pnd_S_Tbl = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tbl", "#S-TBL", FieldType.NUMERIC, 1);
        pnd_Super_Cntl_Pnd_S_Tax_Year = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.STRING, 4);

        pnd_Super_Cntl__R_Field_2 = pnd_Super_Cntl__R_Field_1.newGroupInGroup("pnd_Super_Cntl__R_Field_2", "REDEFINE", pnd_Super_Cntl_Pnd_S_Tax_Year);
        pnd_Super_Cntl_Pnd_S_Tax_Year_N = pnd_Super_Cntl__R_Field_2.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Tax_Year_N", "#S-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super_Cntl_Pnd_S_Form = pnd_Super_Cntl__R_Field_1.newFieldInGroup("pnd_Super_Cntl_Pnd_S_Form", "#S-FORM", FieldType.STRING, 7);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Multi_Form_Rollup = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Multi_Form_Rollup", "#MULTI-FORM-ROLLUP", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Not_First_Time_Run_Ok = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Not_First_Time_Run_Ok", "#NOT-FIRST-TIME-RUN-OK", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Eff_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Eff_Date", "#EFF-DATE", FieldType.PACKED_DECIMAL, 8);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_First_Time_Run_Only = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Time_Run_Only", "#FIRST-TIME-RUN-ONLY", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Lib = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lib", "#LIB", FieldType.STRING, 4);
        pnd_Job_Name = localVariables.newFieldInRecord("pnd_Job_Name", "#JOB-NAME", FieldType.STRING, 8);
        pnd_Mq_Open = localVariables.newFieldInRecord("pnd_Mq_Open", "#MQ-OPEN", FieldType.BOOLEAN, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_Curr_Year = localVariables.newFieldInRecord("pnd_Curr_Year", "#CURR-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl_R.reset();

        ldaTwrl5000.initializeValues();
        ldaTwrl5009.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Mq_Open.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5000() throws Exception
    {
        super("Twrp5000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  FE201608 START
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  $$$$
        //*  #TWRA5000.#DEBUG-IND := TRUE
        pnd_Job_Name.setValue(Global.getINIT_USER());                                                                                                                     //Natural: ASSIGN #JOB-NAME := *INIT-USER
        pnd_Mq_Open.setValue(true);                                                                                                                                       //Natural: ASSIGN #MQ-OPEN := TRUE
        if (condition(pnd_Job_Name.getSubstring(2,7).equals("1600TWA") || pnd_Job_Name.getSubstring(2,7).equals("1601TWA") || pnd_Job_Name.getSubstring(2,7).equals("1602TWA")  //Natural: IF SUBSTR ( #JOB-NAME,2,7 ) = '1600TWA' OR = '1601TWA' OR = '1602TWA' OR = '1603TWA' OR = '1604TWA'
            || pnd_Job_Name.getSubstring(2,7).equals("1603TWA") || pnd_Job_Name.getSubstring(2,7).equals("1604TWA")))
        {
            pnd_Mq_Open.setValue(false);                                                                                                                                  //Natural: ASSIGN #MQ-OPEN := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
            if (condition(pnd_Mq_Open.getBoolean()))                                                                                                                      //Natural: IF #MQ-OPEN
            {
                //*  FE201608
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
                sub_Open_Mq();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                          //Natural: IF #TWRA5000.#DEBUG-IND
        {
            getReports().write(0, "Starting Program:",Global.getPROGRAM(),"- Mass Mailing");                                                                              //Natural: WRITE ( 0 ) 'Starting Program:' *PROGRAM '- Mass Mailing'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TABLE
        sub_Update_Control_Table();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.callnat(Twrn5000.class , getCurrentProcessState(), pdaTwrapart.getPnd_Twrapart(), pdaTwra5000.getPnd_Twra5000(), ldaTwrl5009.getPnd_Twrl5009(),           //Natural: CALLNAT 'TWRN5000' USING #TWRAPART #TWRA5000 #TWRL5009 #TWRA5006 #TWRA5008
            pdaTwra5006.getPnd_Twra5006(), pdaTwra5008.getPnd_Twra5008());
        if (condition(Global.isEscape())) return;
        //*  CONTROL REPORT
        DbsUtil.callnat(Twrn5006.class , getCurrentProcessState(), pdaTwra5000.getPnd_Twra5000(), pdaTwra5006.getPnd_Twra5006());                                         //Natural: CALLNAT 'TWRN5006' USING #TWRA5000 #TWRA5006
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Code().getBoolean()))                                                                                           //Natural: IF #TWRA5000.#RET-CODE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Msg(),new TabSetting(77),"***");                      //Natural: WRITE ( 1 ) '***' 25T #TWRA5000.#RET-MSG 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            //*  TERMINATE 103
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************
        //*  S U B R O U T I N E S
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //*  << SINHASN
        //*  >> SINHASN
        //*  << SINHASN
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-TABLE
        //* *************************************
        //*  >> SINHASN
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-TABLE
        //* *************************************
        //*  ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  *****************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        if (condition(pnd_Mq_Open.getBoolean()))                                                                                                                          //Natural: IF #MQ-OPEN
        {
            //*  CLOSE MQ FE201504 END
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0012'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP5000|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year()), Global.getDATN().divide(10000).subtract(1)); //Natural: ASSIGN #TWRA5000.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                             //Natural: ASSIGN #TWRA5000.#TAX-YEAR := #WS.#TAX-YEAR
                }                                                                                                                                                         //Natural: END-IF
                //*  FOR SUNY
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2012)))                                                                           //Natural: IF #TWRA5000.#TAX-YEAR GE 2012
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit().setValue(10);                                                                                            //Natural: ASSIGN #TWRA5000.#FORM-LIMIT := 10
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit().setValue(7);                                                                                             //Natural: ASSIGN #TWRA5000.#FORM-LIMIT := 7
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet494 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM;//Natural: VALUE '1099'
                if (condition((pnd_Ws_Pnd_Form.equals("1099"))))
                {
                    decideConditionsMet494++;
                    pnd_Curr_Year.compute(new ComputeParameters(false, pnd_Curr_Year), Global.getDATN().divide(10000));                                                   //Natural: ASSIGN #CURR-YEAR := *DATN / 10000
                    if (condition(Global.getDEVICE().equals("BATCH") && pnd_Job_Name.equals("P1600TWA") && pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().equals(pnd_Curr_Year))) //Natural: IF *DEVICE = 'BATCH' AND #JOB-NAME = 'P1600TWA' AND #TWRA5000.#TAX-YEAR = #CURR-YEAR
                    {
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-TABLE
                        sub_Read_Control_Table();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(1).setValue(true);                                                                            //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 1 ) := #TWRA5000.#OK-TO-ROLLUP ( 2 ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(2).setValue(true);
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(100);                                                                                             //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 100
                    //*  SUNY
                    if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2012)))                                                                       //Natural: IF #TWRA5000.#TAX-YEAR GE 2012
                    {
                        pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(10).setValue(true);                                                                       //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 10 ) := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE '1042-S'
                else if (condition((pnd_Ws_Pnd_Form.equals("1042-S"))))
                {
                    decideConditionsMet494++;
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(3).setValue(true);                                                                            //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 3 ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(75);                                                                                              //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 75
                }                                                                                                                                                         //Natural: VALUE '5498'
                else if (condition((pnd_Ws_Pnd_Form.equals("5498"))))
                {
                    decideConditionsMet494++;
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(4).setValue(true);                                                                            //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 4 ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(100);                                                                                             //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 100
                }                                                                                                                                                         //Natural: VALUE '480.6'
                else if (condition((pnd_Ws_Pnd_Form.equals("480.6"))))
                {
                    decideConditionsMet494++;
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(5).setValue(true);                                                                            //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 5 ) := #TWRA5000.#OK-TO-ROLLUP ( 6 ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(6).setValue(true);
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(25);                                                                                              //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 25
                }                                                                                                                                                         //Natural: VALUE 'NR4'
                else if (condition((pnd_Ws_Pnd_Form.equals("NR4"))))
                {
                    decideConditionsMet494++;
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(7).setValue(true);                                                                            //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 7 ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(25);                                                                                              //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 25
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown input parmameter:",pnd_Ws_Pnd_Form,new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Unknown input parmameter:' #WS.#FORM 77T '***' / '***' 25T 'Valid values are:.......:"1099" - 1099-R & 1099-INT' 77T '***' / '***' 25T '                         "1042-S"' 77T '***' / '***' 25T '                         "5498"' 77T '***' / '***' 25T '                         "480.6" - 480.6A & 480.6B' 77T '***' / '***' 25T '                         "NR4"' 77T '***'
                        TabSetting(25),"Valid values are:.......:'1099' - 1099-R & 1099-INT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"                         '1042-S'",new 
                        TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"                         '5498'",new TabSetting(77),"***",NEWLINE,"***",new 
                        TabSetting(25),"                         '480.6' - 480.6A & 480.6B",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"                         'NR4'",new 
                        TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(101);  if (true) return;                                                                                                            //Natural: TERMINATE 101
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Ws_Pnd_Not_First_Time_Run_Ok.notEquals("Y")))                                                                                           //Natural: IF #NOT-FIRST-TIME-RUN-OK NE 'Y'
                {
                    pnd_Ws_Pnd_First_Time_Run_Only.setValue(true);                                                                                                        //Natural: ASSIGN #WS.#FIRST-TIME-RUN-ONLY := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Multi_Form_Rollup.equals("F") && pnd_Ws_Pnd_Form.notEquals("5498")))                                                             //Natural: IF #WS.#MULTI-FORM-ROLLUP = 'F' AND #WS.#FORM NE '5498'
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Multi_Form_Rollup().setValue(true);                                                                                   //Natural: ASSIGN #TWRA5000.#MULTI-FORM-ROLLUP := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").setValue(true);                                                                          //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( * ) := TRUE
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(4).reset();                                                                                   //Natural: RESET #TWRA5000.#OK-TO-ROLLUP ( 4 ) #TWRA5000.#OK-TO-ROLLUP ( 8 ) #TWRA5000.#OK-TO-ROLLUP ( 9 )
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(8).reset();
                    pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(9).reset();
                }                                                                                                                                                         //Natural: END-IF
                //*  $$$$
                //*     #TWRA5000.#OK-TO-ROLLUP(*)    := FALSE
                //*     #TWRA5000.#OK-TO-ROLLUP(3)    := TRUE
                //*     #TWRA5000.#OK-TO-ROLLUP(5:7)  := TRUE
                //*     #TWRA5000.#OK-TO-ROLLUP(10)   := TRUE
                //*     #TWRA5000.#OK-TO-ROLLUP(7)    := TRUE
                //*     #TWRA5000.#OK-TO-ROLLUP(*)    := TRUE
                //*     #TWRA5000.#OK-TO-ROLLUP(4)    := FALSE    /* 5498
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                  //Natural: IF #TWRA5000.#DEBUG-IND
                {
                    getReports().write(0, Global.getPROGRAM(),"=",pnd_Ws_Pnd_Form,"=",pnd_Ws_Pnd_Multi_Form_Rollup,NEWLINE,"=",pdaTwra5000.getPnd_Twra5000_Pnd_Multi_Form_Rollup(),  //Natural: WRITE ( 0 ) *PROGRAM '=' #WS.#FORM '=' #WS.#MULTI-FORM-ROLLUP / '=' #TWRA5000.#MULTI-FORM-ROLLUP ( EM = N/Y )
                        new ReportEditMask ("N/Y"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Mass Mailing form rollup parameters:",NEWLINE,new TabSetting(23),"Tax Year...........:",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(),NEWLINE,new  //Natural: WRITE ( 1 ) 7T 'Mass Mailing form rollup parameters:' / 23T 'Tax Year...........:' #TWRA5000.#TAX-YEAR / 23T 'First time run only: ' #WS.#FIRST-TIME-RUN-ONLY ( EM = N/Y )
                    TabSetting(23),"First time run only: ",pnd_Ws_Pnd_First_Time_Run_Only, new ReportEditMask ("N/Y"));
                if (Global.isEscape()) return;
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                          //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRA5000.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO #TWRA5000.#FORM-LIMIT
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit())); pnd_Ws_Pnd_I.nadd(1))
                {
                    //*   NOT FORM 8 AND 9
                    if (condition(pnd_Ws_Pnd_I.equals(8) || pnd_Ws_Pnd_I.equals(9)))                                                                                      //Natural: IF #I = 8 OR = 9
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                    //Natural: IF #TWRA5000.#OK-TO-ROLLUP ( #I )
                    {
                        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                            pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        pdaTwra5000.getPnd_Twra5000_Pnd_First_Time_Rollup().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_First_Time_Forms().getBoolean()); //Natural: ASSIGN #TWRA5000.#FIRST-TIME-ROLLUP ( #I ) := #TWRATBL4.#TIRCNTL-FIRST-TIME-FORMS
                        pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Mass_Mailing().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Mass_Mailing().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-MASS-MAILING ( #I ) := #TWRATBL4.#TIRCNTL-MASS-MAILING
                        pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_2nd_Mass_Mailing().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_2nd_Mass_Mailing().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-2ND-MASS-MAILING ( #I ) := #TWRATBL4.#TIRCNTL-2ND-MASS-MAILING
                        pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Irs_Orig().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-IRS-ORIG ( #I ) := #TWRATBL4.#TIRCNTL-IRS-ORIG
                        pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Irs_Corr().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Corr().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-IRS-CORR ( #I ) := #TWRATBL4.#TIRCNTL-IRS-CORR
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(23),"Form...............: ",ldaTwrl5000.getPnd_Twrl5000_Pnd_Form_Name().getValue(2 + 1,pnd_Ws_Pnd_I),"First time rollup: ",pdaTwra5000.getPnd_Twra5000_Pnd_First_Time_Rollup().getValue(pnd_Ws_Pnd_I),  //Natural: WRITE ( 1 ) / 23T 'Form...............: ' #TWRL5000.#FORM-NAME ( 2,#I ) 'First time rollup: ' #TWRA5000.#FIRST-TIME-ROLLUP ( #I ) ( EM = N/Y )
                            new ReportEditMask ("N/Y"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Ws_Pnd_First_Time_Run_Only.getBoolean() && ! (pdaTwra5000.getPnd_Twra5000_Pnd_First_Time_Rollup().getValue(pnd_Ws_Pnd_I).equals(true)))) //Natural: IF #WS.#FIRST-TIME-RUN-ONLY AND NOT #TWRA5000.#FIRST-TIME-ROLLUP ( #I )
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                            sub_Error_Display_Start();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Mass Mailing process for",ldaTwrl5000.getPnd_Twrl5000_Pnd_Form_Name().getValue(2 + 1,pnd_Ws_Pnd_I),new  //Natural: WRITE ( 1 ) '***' 25T 'Mass Mailing process for' #TWRL5000.#FORM-NAME ( 2,#I ) 77T '***' / '***' 25T 'had been already run on' #TWRATBL4.TIRCNTL-EFF-DTE ( EM = 9999/99/99 ) 77T '***'
                                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"had been already run on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Eff_Dte(), 
                                new ReportEditMask ("9999/99/99"),new TabSetting(77),"***");
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                            sub_Error_Display_End();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            DbsUtil.terminate(102);  if (true) return;                                                                                                    //Natural: TERMINATE 102
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pdaTwra5000.getPnd_Twra5000_Pnd_Process_Ind().setValue("1");                                                                                              //Natural: ASSIGN #TWRA5000.#PROCESS-IND := '1'
                pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                   //Natural: ASSIGN #TWRA5000.#PYMNT-START-KEY := #TWRA5000.#PYMNT-END-KEY := #TWRA5000.#IRA-START-KEY := #TWRA5000.#IRA-END-KEY := #TWRA5000.#TAX-YEAR
                pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());
                pdaTwra5000.getPnd_Twra5000_Pnd_Ira_Start_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());
                pdaTwra5000.getPnd_Twra5000_Pnd_Ira_End_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());
                setValueToSubstring(pnd_Ws_Const_Low_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),5,1);                                                       //Natural: MOVE LOW-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,5,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key(),5,1);                                                        //Natural: MOVE HIGH-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-END-KEY,5,1 )
                setValueToSubstring(pnd_Ws_Const_Low_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Ira_Start_Key(),6,1);                                                         //Natural: MOVE LOW-VALUES TO SUBSTR ( #TWRA5000.#IRA-START-KEY,6,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Ira_End_Key(),6,1);                                                          //Natural: MOVE HIGH-VALUES TO SUBSTR ( #TWRA5000.#IRA-END-KEY,6,1 )
                pnd_Ws_Pnd_Lib.setValue(Global.getLIBRARY_ID());                                                                                                          //Natural: ASSIGN #WS.#LIB := *LIBRARY-ID
                if (condition(pnd_Ws_Pnd_Lib.equals("PROJ") || pnd_Ws_Pnd_Lib.equals("ACPT")))                                                                            //Natural: IF #WS.#LIB = 'PROJ' OR = 'ACPT'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(100);                                                                                             //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 100
                }                                                                                                                                                         //Natural: END-IF
                pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(1 + 1,":",10 + 1).setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").getBoolean()); //Natural: ASSIGN #TWRA5006.#FORMS-TO-REPORT ( 1:10 ) := #TWRA5000.#OK-TO-ROLLUP ( * )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Control_Table() throws Exception                                                                                                                //Natural: READ-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Super_Cntl_Pnd_S_Tbl.setValue(4);                                                                                                                             //Natural: ASSIGN #S-TBL := 4
        pnd_Super_Cntl_Pnd_S_Tax_Year_N.setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                                             //Natural: ASSIGN #S-TAX-YEAR-N := #TWRA5000.#TAX-YEAR
        pnd_Super_Cntl_Pnd_S_Form.setValue("SUNYCLN");                                                                                                                    //Natural: ASSIGN #S-FORM := 'SUNYCLN'
        vw_cntl_R.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) CNTL-R WITH TIRCNTL-NBR-YEAR-FORM-NAME = #SUPER-CNTL
        (
        "READ01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_FORM_NAME", ">=", pnd_Super_Cntl, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_FORM_NAME", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cntl_R.readNextRow("READ01")))
        {
            if (condition(cntl_R_Tircntl_Tbl_Nbr.equals(pnd_Super_Cntl_Pnd_S_Tbl) && cntl_R_Tircntl_Tax_Year.equals(pnd_Super_Cntl_Pnd_S_Tax_Year_N) &&                   //Natural: IF CNTL-R.TIRCNTL-TBL-NBR = #S-TBL AND CNTL-R.TIRCNTL-TAX-YEAR = #S-TAX-YEAR-N AND CNTL-R.TIRCNTL-RPT-FORM-NAME = #S-FORM
                cntl_R_Tircntl_Rpt_Form_Name.equals(pnd_Super_Cntl_Pnd_S_Form)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Msg().setValue("Suny-Cuny Cleanup has not performed");                                                                //Natural: MOVE 'Suny-Cuny Cleanup has not performed' TO #TWRA5000.#RET-MSG
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Msg(),new TabSetting(77),"***");                  //Natural: WRITE ( 1 ) '***' 25T #TWRA5000.#RET-MSG 77T '***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-CONTROL-TABLE
    }
    private void sub_Update_Control_Table() throws Exception                                                                                                              //Natural: UPDATE-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Eff_Date.setValue(Global.getDATN());                                                                                                                   //Natural: ASSIGN #WS.#EFF-DATE := *DATN
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #TWRA5000.#FORM-LIMIT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit())); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  NO FORMS FOR 8 AND 9
            if (condition(pnd_Ws_Pnd_I.equals(8) || pnd_Ws_Pnd_I.equals(9)))                                                                                              //Natural: IF #I = 8 OR = 9
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                            //Natural: IF #TWRA5000.#OK-TO-ROLLUP ( #I )
            {
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                        //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                pdaTwratbl4.getPnd_Twratbl4_Tircntl_Eff_Dte().setValue(pnd_Ws_Pnd_Eff_Date);                                                                              //Natural: ASSIGN #TWRATBL4.TIRCNTL-EFF-DTE := #WS.#EFF-DATE
                DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    //*  FE201608 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
