/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:19 PM
**        * FROM NATURAL PROGRAM : Twrp0705
************************************************************
**        * FILE NAME            : Twrp0705.java
**        * CLASS NAME           : Twrp0705
**        * INSTANCE NAME        : Twrp0705
************************************************************
**************************************************************
** PROGRAM NAME:  TWRP0705
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  ANATOLY
** PURPOSE     :  TRANSACTION FILE EDIT\SUMMARY
**             :  REPORT
** INPUT       :  1.TRANSACTION FILE EXTRACT FROM FEEDERS
**             :    (DETAIL)
** OUTPUT      :  2.TRANSACTION FILE CONTROL REPORT
**             :    (SUMMARY)
** DATE        :  01/15/99
**-----------------------------------------------------------
** UPDATES -
** 01/30/12 - RCC - RE-STOW CUNY-SUNY
** 09/28/98 - AVS - INSTEAD OF BREAKING BY REFERENCE NUMBER
**                  GENERATED THE REPORT BREAKING BY
**                  PAYMENT AND SETTLEMENT TYPES(LOU's request)
** 10/06/98 - AVS - ADDED CAN. AND P.-R. AMOUNTS TO THE REPORT.
** 10/14/98 - AVS - MADE DISTR. CODE DESCRIPTIONS SHORTER.
** 10/15/98 - AVS - INCREASED THE FORMAT OF SOME FIELDS.
** 10/20/98 - AVS - SWITCHED TAX FREE IVC AND TAXABLE AMOUNT
**                  ON THE REPORT.
** 10/24/98 - AVS - ADDED SOME NEW DISTR.CODE ABBREVIATIONS.
** 01/15/99 - AVS - ADDED A JOB NAME TO THE REPORT.
** 09/21/99 - JH  - ADDED 'Roth Death W/D' TO DESCRIPTIONS
** 09/20/04 - MS  - ADDED TRUST COMPANY FOR TOPS.
** 12/06/05 - BK. RESTOWED FOR NEW  TWRNCOMP PARMS
** 05/30/06 - AAY - REVISED TO ACCOMMODATE ROTH SURVIVOR DESCRIPTIONS.
** 09/20/06 - AAY - REVISED TO ACCOMMODATE VARIABLE UNIVERSAL LIFE (VL).
** 05/07/08 - RM  - ROTH 401K/403B PROJECT
**                  RECOMPILED DUE TO UPDATED TWRL0700.
** 04/12/17 - J BREMER PIN EXP  STOW FOR TWRL0700
* 07/11/19    TWRT-LOB-CDE = VT IS NOT PROCESSED -CPS SUNSET - SAURAV
**-----------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0705 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl9803 ldaTwrl9803;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Types_Break;
    private DbsField pnd_Tax_Amount;
    private DbsField pnd_Distr_Code_Descr;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Pnd_Tbl_Nbr;
    private DbsField pnd_Key_Pnd_T_Y;
    private DbsField pnd_Key_Pnd_Type_Refer;
    private DbsField pnd_Cnt;
    private DbsField pnd_Distr_Code;
    private DbsField pnd_Can_Amt;
    private DbsField pnd_State_Amt;
    private DbsField pnd_Pr_Amt;
    private DbsField pnd_Sum_Can_Amt;
    private DbsField pnd_Sum_State_Amt;
    private DbsField pnd_Sum_Pr_Amt;
    private DbsField pnd_Tot_Can_Amt;
    private DbsField pnd_Tot_State_Amt;
    private DbsField pnd_Tot_Pr_Amt;
    private DbsField pnd_Grd_Can_Amt;
    private DbsField pnd_Grd_State_Amt;
    private DbsField pnd_Grd_Pr_Amt;
    private DbsField pnd_Twrt_Gross_Amt;
    private DbsField pnd_Twrt_Fed_Amt;
    private DbsField pnd_Twrt_Ivc_Amt;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrt_Tax_YearOld;
    private DbsField readWork01Twrt_Type_ReferOld;
    private DbsField readWork01Twrt_Pay_Set_TypesOld;
    private DbsField readWork01Pnd_Twrt_Gross_AmtSum292;
    private DbsField readWork01Pnd_Twrt_Gross_AmtSum;
    private DbsField readWork01Pnd_Twrt_Ivc_AmtSum292;
    private DbsField readWork01Pnd_Twrt_Ivc_AmtSum;
    private DbsField readWork01Twrt_Type_ReferNcount292;
    private DbsField readWork01Twrt_Type_ReferNcount;
    private DbsField readWork01Twrt_Interest_AmtSum292;
    private DbsField readWork01Twrt_Interest_AmtSum;
    private DbsField readWork01Pnd_Twrt_Fed_AmtSum292;
    private DbsField readWork01Pnd_Twrt_Fed_AmtSum;
    private DbsField readWork01Twrt_Nra_Whhld_AmtSum292;
    private DbsField readWork01Twrt_Nra_Whhld_AmtSum;
    private DbsField readWork01Twrt_Local_Whhld_AmtSum292;
    private DbsField readWork01Twrt_Local_Whhld_AmtSum;
    private DbsField readWork01Pnd_Twrt_Gross_AmtSum314;
    private DbsField readWork01Pnd_Twrt_Ivc_AmtSum314;
    private DbsField readWork01Twrt_Type_ReferNcount314;
    private DbsField readWork01Twrt_SourceOld;
    private DbsField readWork01Twrt_Interest_AmtSum314;
    private DbsField readWork01Pnd_Twrt_Fed_AmtSum314;
    private DbsField readWork01Twrt_Nra_Whhld_AmtSum314;
    private DbsField readWork01Twrt_Local_Whhld_AmtSum314;
    private DbsField readWork01Twrt_Company_CdeOld;
    private DbsField readWork01Pnd_Twrt_Gross_AmtSum334;
    private DbsField readWork01Pnd_Twrt_Ivc_AmtSum334;
    private DbsField readWork01Twrt_Type_ReferNcount334;
    private DbsField readWork01Twrt_Interest_AmtSum334;
    private DbsField readWork01Pnd_Twrt_Fed_AmtSum334;
    private DbsField readWork01Twrt_Nra_Whhld_AmtSum334;
    private DbsField readWork01Twrt_Local_Whhld_AmtSum334;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl9803 = new LdaTwrl9803();
        registerRecord(ldaTwrl9803);
        registerRecord(ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View());

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Types_Break = localVariables.newFieldInRecord("pnd_Types_Break", "#TYPES-BREAK", FieldType.STRING, 2);
        pnd_Tax_Amount = localVariables.newFieldInRecord("pnd_Tax_Amount", "#TAX-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Distr_Code_Descr = localVariables.newFieldInRecord("pnd_Distr_Code_Descr", "#DISTR-CODE-DESCR", FieldType.STRING, 16);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 7);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Tbl_Nbr = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Tbl_Nbr", "#TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Key_Pnd_T_Y = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_T_Y", "#T-Y", FieldType.NUMERIC, 4);
        pnd_Key_Pnd_Type_Refer = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Type_Refer", "#TYPE-REFER", FieldType.STRING, 2);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 9);
        pnd_Distr_Code = localVariables.newFieldInRecord("pnd_Distr_Code", "#DISTR-CODE", FieldType.STRING, 1);
        pnd_Can_Amt = localVariables.newFieldInRecord("pnd_Can_Amt", "#CAN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_State_Amt = localVariables.newFieldInRecord("pnd_State_Amt", "#STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Pr_Amt = localVariables.newFieldInRecord("pnd_Pr_Amt", "#PR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sum_Can_Amt = localVariables.newFieldInRecord("pnd_Sum_Can_Amt", "#SUM-CAN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sum_State_Amt = localVariables.newFieldInRecord("pnd_Sum_State_Amt", "#SUM-STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sum_Pr_Amt = localVariables.newFieldInRecord("pnd_Sum_Pr_Amt", "#SUM-PR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Can_Amt = localVariables.newFieldInRecord("pnd_Tot_Can_Amt", "#TOT-CAN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_State_Amt = localVariables.newFieldInRecord("pnd_Tot_State_Amt", "#TOT-STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tot_Pr_Amt = localVariables.newFieldInRecord("pnd_Tot_Pr_Amt", "#TOT-PR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grd_Can_Amt = localVariables.newFieldInRecord("pnd_Grd_Can_Amt", "#GRD-CAN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grd_State_Amt = localVariables.newFieldInRecord("pnd_Grd_State_Amt", "#GRD-STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Grd_Pr_Amt = localVariables.newFieldInRecord("pnd_Grd_Pr_Amt", "#GRD-PR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Twrt_Gross_Amt = localVariables.newFieldInRecord("pnd_Twrt_Gross_Amt", "#TWRT-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Twrt_Fed_Amt = localVariables.newFieldInRecord("pnd_Twrt_Fed_Amt", "#TWRT-FED-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Twrt_Ivc_Amt = localVariables.newFieldInRecord("pnd_Twrt_Ivc_Amt", "#TWRT-IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Tax_Year_OLD", "Twrt_Tax_Year_OLD", FieldType.NUMERIC, 4);
        readWork01Twrt_Type_ReferOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Type_Refer_OLD", "Twrt_Type_Refer_OLD", FieldType.STRING, 
            2);
        readWork01Twrt_Pay_Set_TypesOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Pay_Set_Types_OLD", "Twrt_Pay_Set_Types_OLD", FieldType.STRING, 
            2);
        readWork01Pnd_Twrt_Gross_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_Amt_SUM_292", "Pnd_Twrt_Gross_Amt_SUM_292", 
            FieldType.PACKED_DECIMAL, 12, 2);
        readWork01Pnd_Twrt_Gross_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_Amt_SUM", "Pnd_Twrt_Gross_Amt_SUM", FieldType.PACKED_DECIMAL, 
            12, 2);
        readWork01Pnd_Twrt_Ivc_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_Amt_SUM_292", "Pnd_Twrt_Ivc_Amt_SUM_292", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Pnd_Twrt_Ivc_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_Amt_SUM", "Pnd_Twrt_Ivc_Amt_SUM", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Twrt_Type_ReferNcount292 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Type_Refer_NCOUNT_292", "Twrt_Type_Refer_NCOUNT_292", 
            FieldType.STRING, 2);
        readWork01Twrt_Type_ReferNcount = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Type_Refer_NCOUNT", "Twrt_Type_Refer_NCOUNT", FieldType.STRING, 
            2);
        readWork01Twrt_Interest_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Interest_Amt_SUM_292", "Twrt_Interest_Amt_SUM_292", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Twrt_Interest_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Interest_Amt_SUM", "Twrt_Interest_Amt_SUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Pnd_Twrt_Fed_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_Amt_SUM_292", "Pnd_Twrt_Fed_Amt_SUM_292", FieldType.PACKED_DECIMAL, 
            12, 2);
        readWork01Pnd_Twrt_Fed_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_Amt_SUM", "Pnd_Twrt_Fed_Amt_SUM", FieldType.PACKED_DECIMAL, 
            12, 2);
        readWork01Twrt_Nra_Whhld_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Nra_Whhld_Amt_SUM_292", "Twrt_Nra_Whhld_Amt_SUM_292", 
            FieldType.PACKED_DECIMAL, 9, 2);
        readWork01Twrt_Nra_Whhld_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Nra_Whhld_Amt_SUM", "Twrt_Nra_Whhld_Amt_SUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Twrt_Local_Whhld_AmtSum292 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Local_Whhld_Amt_SUM_292", "Twrt_Local_Whhld_Amt_SUM_292", 
            FieldType.PACKED_DECIMAL, 9, 2);
        readWork01Twrt_Local_Whhld_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Local_Whhld_Amt_SUM", "Twrt_Local_Whhld_Amt_SUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Pnd_Twrt_Gross_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_Amt_SUM_314", "Pnd_Twrt_Gross_Amt_SUM_314", 
            FieldType.PACKED_DECIMAL, 12, 2);
        readWork01Pnd_Twrt_Ivc_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_Amt_SUM_314", "Pnd_Twrt_Ivc_Amt_SUM_314", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Twrt_Type_ReferNcount314 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Type_Refer_NCOUNT_314", "Twrt_Type_Refer_NCOUNT_314", 
            FieldType.STRING, 2);
        readWork01Twrt_SourceOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Source_OLD", "Twrt_Source_OLD", FieldType.STRING, 2);
        readWork01Twrt_Interest_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Interest_Amt_SUM_314", "Twrt_Interest_Amt_SUM_314", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Pnd_Twrt_Fed_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_Amt_SUM_314", "Pnd_Twrt_Fed_Amt_SUM_314", FieldType.PACKED_DECIMAL, 
            12, 2);
        readWork01Twrt_Nra_Whhld_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Nra_Whhld_Amt_SUM_314", "Twrt_Nra_Whhld_Amt_SUM_314", 
            FieldType.PACKED_DECIMAL, 9, 2);
        readWork01Twrt_Local_Whhld_AmtSum314 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Local_Whhld_Amt_SUM_314", "Twrt_Local_Whhld_Amt_SUM_314", 
            FieldType.PACKED_DECIMAL, 9, 2);
        readWork01Twrt_Company_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Company_Cde_OLD", "Twrt_Company_Cde_OLD", FieldType.STRING, 
            1);
        readWork01Pnd_Twrt_Gross_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Gross_Amt_SUM_334", "Pnd_Twrt_Gross_Amt_SUM_334", 
            FieldType.PACKED_DECIMAL, 12, 2);
        readWork01Pnd_Twrt_Ivc_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Ivc_Amt_SUM_334", "Pnd_Twrt_Ivc_Amt_SUM_334", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Twrt_Type_ReferNcount334 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Type_Refer_NCOUNT_334", "Twrt_Type_Refer_NCOUNT_334", 
            FieldType.STRING, 2);
        readWork01Twrt_Interest_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Interest_Amt_SUM_334", "Twrt_Interest_Amt_SUM_334", FieldType.PACKED_DECIMAL, 
            9, 2);
        readWork01Pnd_Twrt_Fed_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Twrt_Fed_Amt_SUM_334", "Pnd_Twrt_Fed_Amt_SUM_334", FieldType.PACKED_DECIMAL, 
            12, 2);
        readWork01Twrt_Nra_Whhld_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Nra_Whhld_Amt_SUM_334", "Twrt_Nra_Whhld_Amt_SUM_334", 
            FieldType.PACKED_DECIMAL, 9, 2);
        readWork01Twrt_Local_Whhld_AmtSum334 = internalLoopRecord.newFieldInRecord("ReadWork01_Twrt_Local_Whhld_Amt_SUM_334", "Twrt_Local_Whhld_Amt_SUM_334", 
            FieldType.PACKED_DECIMAL, 9, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0700.initializeValues();
        ldaTwrl9803.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0705() throws Exception
    {
        super("Twrp0705");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  SUMMARY TOTALS REPORT
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 133 PS = 61
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().reset();                                                                                                              //Natural: RESET #TWRACOMP.#ABEND-IND #TWRACOMP.#DISPLAY-IND
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().reset();
        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(pnd_Ws_Const_Low_Values);                                                                                    //Natural: ASSIGN #TWRACOMP.#COMP-CODE := LOW-VALUES
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            CheckAtStartofData278();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*  SAURAV
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().notEquals("VT")))                                                                                     //Natural: IF TWRT-LOB-CDE NE 'VT'
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("DC")))                                                                              //Natural: IF TWRT-PAY-SET-TYPES = 'DC'
                {
                    //*  JH 9/99 - DY OR D4
                    ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde());                                                 //Natural: MOVE TWRT-DISTRIB-CDE TO TWRT-SETTL-TYPE
                }                                                                                                                                                         //Natural: END-IF
                //*  SAURAV
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*  ----------------------------------                                                                                                                       //Natural: AT BREAK OF TWRT-PAY-SET-TYPES
            //*  ----------------------------------                                                                                                                       //Natural: AT BREAK OF TWRT-SOURCE
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-COMPANY-CDE
            //*  07-12-99 FRANK
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("C") && ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("IS")))                            //Natural: IF TWRT-COMPANY-CDE = 'C' AND TWRT-SOURCE = 'IS'
            {
                getReports().write(0, "REJECTED==> SOURCE = 'IS' AND COMPANY-CODE = 'C'","CONTRACT NO.=",ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),                    //Natural: WRITE 'REJECTED==> SOURCE = "IS" AND COMPANY-CODE = "C"' 'CONTRACT NO.=' TWRT-CNTRCT-NBR 'PAYEE CODE =' TWRT-PAYEE-CDE
                    "PAYEE CODE =",ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Twrt_Ivc_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                                                         //Natural: MOVE TWRT-IVC-AMT TO #TWRT-IVC-AMT
            pnd_Twrt_Gross_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                                     //Natural: MOVE TWRT-GROSS-AMT TO #TWRT-GROSS-AMT
            pnd_Twrt_Fed_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                                                                   //Natural: MOVE TWRT-FED-WHHLD-AMT TO #TWRT-FED-AMT
            //* *
            pnd_Can_Amt.reset();                                                                                                                                          //Natural: RESET #CAN-AMT #STATE-AMT #PR-AMT
            pnd_State_Amt.reset();
            pnd_Pr_Amt.reset();
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            pnd_State_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                    //Natural: MOVE TWRT-STATE-WHHLD-AMT TO #STATE-AMT
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ")))                                                                                   //Natural: IF TWRT-STATE-RSDNCY = 'RQ'
            {
                pnd_Pr_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                                   //Natural: MOVE TWRT-STATE-WHHLD-AMT TO #PR-AMT
                pnd_State_Amt.setValue(0);                                                                                                                                //Natural: MOVE 0 TO #STATE-AMT
            }                                                                                                                                                             //Natural: END-IF
            //* *
            pnd_Sum_Pr_Amt.nadd(pnd_Pr_Amt);                                                                                                                              //Natural: COMPUTE #SUM-PR-AMT = #SUM-PR-AMT + #PR-AMT
            pnd_Sum_State_Amt.nadd(pnd_State_Amt);                                                                                                                        //Natural: COMPUTE #SUM-STATE-AMT = #SUM-STATE-AMT + #STATE-AMT
            //* *
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Ind().equals("Y")))                                                                                   //Natural: IF TWRT-CAN-WHHLD-IND = 'Y'
            {
                pnd_Can_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                                                                    //Natural: MOVE TWRT-CAN-WHHLD-AMT TO #CAN-AMT
                pnd_Sum_Can_Amt.nadd(pnd_Can_Amt);                                                                                                                        //Natural: COMPUTE #SUM-CAN-AMT = #SUM-CAN-AMT + #CAN-AMT
            }                                                                                                                                                             //Natural: END-IF
            //* *
            readWork01Twrt_Tax_YearOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                              //Natural: END-WORK
            readWork01Twrt_Type_ReferOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Type_Refer());
            readWork01Twrt_Pay_Set_TypesOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types());
            readWork01Pnd_Twrt_Gross_AmtSum292.nadd(readWork01Pnd_Twrt_Gross_AmtSum292,pnd_Twrt_Gross_Amt);
            readWork01Pnd_Twrt_Gross_AmtSum.nadd(readWork01Pnd_Twrt_Gross_AmtSum,pnd_Twrt_Gross_Amt);
            readWork01Pnd_Twrt_Ivc_AmtSum292.nadd(readWork01Pnd_Twrt_Ivc_AmtSum292,pnd_Twrt_Ivc_Amt);
            readWork01Pnd_Twrt_Ivc_AmtSum.nadd(readWork01Pnd_Twrt_Ivc_AmtSum,pnd_Twrt_Ivc_Amt);
            readWork01Twrt_Interest_AmtSum292.nadd(readWork01Twrt_Interest_AmtSum292,ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());
            readWork01Twrt_Interest_AmtSum.nadd(readWork01Twrt_Interest_AmtSum,ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());
            readWork01Pnd_Twrt_Fed_AmtSum292.nadd(readWork01Pnd_Twrt_Fed_AmtSum292,pnd_Twrt_Fed_Amt);
            readWork01Pnd_Twrt_Fed_AmtSum.nadd(readWork01Pnd_Twrt_Fed_AmtSum,pnd_Twrt_Fed_Amt);
            readWork01Twrt_Nra_Whhld_AmtSum292.nadd(readWork01Twrt_Nra_Whhld_AmtSum292,ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());
            readWork01Twrt_Nra_Whhld_AmtSum.nadd(readWork01Twrt_Nra_Whhld_AmtSum,ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());
            readWork01Twrt_Local_Whhld_AmtSum292.nadd(readWork01Twrt_Local_Whhld_AmtSum292,ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());
            readWork01Twrt_Local_Whhld_AmtSum.nadd(readWork01Twrt_Local_Whhld_AmtSum,ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());
            readWork01Pnd_Twrt_Gross_AmtSum314.nadd(readWork01Pnd_Twrt_Gross_AmtSum314,pnd_Twrt_Gross_Amt);
            readWork01Pnd_Twrt_Ivc_AmtSum314.nadd(readWork01Pnd_Twrt_Ivc_AmtSum314,pnd_Twrt_Ivc_Amt);
            readWork01Twrt_SourceOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Source());
            readWork01Twrt_Interest_AmtSum314.nadd(readWork01Twrt_Interest_AmtSum314,ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());
            readWork01Pnd_Twrt_Fed_AmtSum314.nadd(readWork01Pnd_Twrt_Fed_AmtSum314,pnd_Twrt_Fed_Amt);
            readWork01Twrt_Nra_Whhld_AmtSum314.nadd(readWork01Twrt_Nra_Whhld_AmtSum314,ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());
            readWork01Twrt_Local_Whhld_AmtSum314.nadd(readWork01Twrt_Local_Whhld_AmtSum314,ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());
            readWork01Twrt_Company_CdeOld.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());
            readWork01Pnd_Twrt_Gross_AmtSum334.nadd(readWork01Pnd_Twrt_Gross_AmtSum334,pnd_Twrt_Gross_Amt);
            readWork01Pnd_Twrt_Ivc_AmtSum334.nadd(readWork01Pnd_Twrt_Ivc_AmtSum334,pnd_Twrt_Ivc_Amt);
            readWork01Twrt_Interest_AmtSum334.nadd(readWork01Twrt_Interest_AmtSum334,ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());
            readWork01Pnd_Twrt_Fed_AmtSum334.nadd(readWork01Pnd_Twrt_Fed_AmtSum334,pnd_Twrt_Fed_Amt);
            readWork01Twrt_Nra_Whhld_AmtSum334.nadd(readWork01Twrt_Nra_Whhld_AmtSum334,ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());
            readWork01Twrt_Local_Whhld_AmtSum334.nadd(readWork01Twrt_Local_Whhld_AmtSum334,ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  -- - - - - - - - - - - - - - - - - - - - - - -
        //* *  CREATE EDIT CONTROL REPORT                **
        //* *
        //* **********************                                                                                                                                        //Natural: AT TOP OF PAGE ( 01 )
        //*  S U B R O U T I N E S
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-NAME
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-NAME-BY-NO
        //*  -------------------------------------  JH 9/21/99 ADD 'Roth' AND
        //*  VALUE 'DC'                                        SUBDIVIDE 'DC'
        //*  ----------------------------------------------
    }
    private void sub_Get_Company_Name() throws Exception                                                                                                                  //Natural: GET-COMPANY-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().notEquals(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde())))                                              //Natural: IF #TWRACOMP.#COMP-CODE NE TWRT-COMPANY-CDE
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());                                                          //Natural: ASSIGN #TWRACOMP.#COMP-CODE := TWRT-COMPANY-CDE
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());                                                          //Natural: ASSIGN #TWRACOMP.#COMP-CODE := TWRT-COMPANY-CDE
            DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Find_Name_By_No() throws Exception                                                                                                                   //Natural: FIND-NAME-BY-NO
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View().startDatabaseFind                                                                                             //Natural: FIND TIRCNTL-DIST-CODE-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-TYPE-REFER = #KEY
        (
        "FIND01",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_TYPE_REFER", "=", pnd_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View().readNextRow("FIND01")))
        {
            ldaTwrl9803.getVw_tircntl_Dist_Code_Tbl_View_View().setIfNotFoundControlFlag(false);
            pnd_Distr_Code_Descr.setValue(ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Settl_Paym_Descr());                                                     //Natural: MOVE TIRCNTL-SETTL-PAYM-DESCR TO #DISTR-CODE-DESCR
            pnd_Distr_Code.setValue(ldaTwrl9803.getTircntl_Dist_Code_Tbl_View_View_Tircntl_Dist_Code());                                                                  //Natural: MOVE TIRCNTL-DIST-CODE TO #DISTR-CODE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* *
        //*  JH - 9/21/99 - DES THIS WORK??
        short decideConditionsMet414 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TYPES-BREAK;//Natural: VALUE 'AC'
        if (condition((pnd_Types_Break.equals("AC"))))
        {
            decideConditionsMet414++;
            if (condition(pnd_Distr_Code.equals("4")))                                                                                                                    //Natural: IF #DISTR-CODE = '4'
            {
                pnd_Distr_Code_Descr.setValue("Death Ann.- Cash");                                                                                                        //Natural: MOVE 'Death Ann.- Cash' TO #DISTR-CODE-DESCR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Distr_Code_Descr.setValue("AP Cash");                                                                                                                 //Natural: MOVE 'AP Cash' TO #DISTR-CODE-DESCR
                //*  05/30/06
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AD'
        else if (condition((pnd_Types_Break.equals("AD"))))
        {
            decideConditionsMet414++;
            //*  DEFAULT
            if (condition(pnd_Distr_Code.equals("T")))                                                                                                                    //Natural: IF #DISTR-CODE = 'T'
            {
                pnd_Distr_Code_Descr.setValue("Roth Surv UnQlfd");                                                                                                        //Natural: MOVE 'Roth Surv UnQlfd' TO #DISTR-CODE-DESCR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Distr_Code_Descr.setValue("Roth Surv Qlfd  ");                                                                                                        //Natural: MOVE 'Roth Surv Qlfd  ' TO #DISTR-CODE-DESCR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'DI'
        else if (condition((pnd_Types_Break.equals("DI"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Death Roll to IRA");                                                                                                           //Natural: MOVE 'Death Roll to IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'DL'
        else if (condition((pnd_Types_Break.equals("DL"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Death RO Non-IRA");                                                                                                            //Natural: MOVE 'Death RO Non-IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'DO'
        else if (condition((pnd_Types_Break.equals("DO"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Death Min Dist");                                                                                                              //Natural: MOVE 'Death Min Dist' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NT'
        else if (condition((pnd_Types_Break.equals("NT"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("DCI/DPI,Grp Int");                                                                                                             //Natural: MOVE 'DCI/DPI,Grp Int' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'LD'
        else if (condition((pnd_Types_Break.equals("LD"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Loan Default");                                                                                                                //Natural: MOVE 'Loan Default' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'LT'
        else if (condition((pnd_Types_Break.equals("LT"))))
        {
            decideConditionsMet414++;
            //*  JH 9/21/99 - NM NOT FOUND
            pnd_Distr_Code_Descr.setValue("Loan Default Int");                                                                                                            //Natural: MOVE 'Loan Default Int' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NK','NM'
        else if (condition((pnd_Types_Break.equals("NK") || pnd_Types_Break.equals("NM"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Cash Surr/Mat End");                                                                                                           //Natural: MOVE 'Cash Surr/Mat End' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'IT'
        else if (condition((pnd_Types_Break.equals("IT"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Insurance Int");                                                                                                               //Natural: MOVE 'Insurance Int' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NI'
        else if (condition((pnd_Types_Break.equals("NI"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("IRA Excess Cont");                                                                                                             //Natural: MOVE 'IRA Excess Cont' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NO'
        else if (condition((pnd_Types_Break.equals("NO"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Minimum Dist");                                                                                                                //Natural: MOVE 'Minimum Dist' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'CT'
        else if (condition((pnd_Types_Break.equals("CT"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("IPRO P&I NQDA Int");                                                                                                           //Natural: MOVE 'IPRO P&I NQDA Int' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'D4'
        else if (condition((pnd_Types_Break.equals("D4"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Death Lump Sum");                                                                                                              //Natural: MOVE 'Death Lump Sum' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'DY'
        else if (condition((pnd_Types_Break.equals("DY"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Roth Death W/D");                                                                                                              //Natural: MOVE 'Roth Death W/D' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'CX'
        else if (condition((pnd_Types_Break.equals("CX"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Tax Free Exchange");                                                                                                           //Natural: MOVE 'Tax Free Exchange' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'RL','RB','RQ'
        else if (condition((pnd_Types_Break.equals("RL") || pnd_Types_Break.equals("RB") || pnd_Types_Break.equals("RQ"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("RO to Non-IRA");                                                                                                               //Natural: MOVE 'RO to Non-IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'AI'
        else if (condition((pnd_Types_Break.equals("AI"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Roll AP to IRA");                                                                                                              //Natural: MOVE 'Roll AP to IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'AL','AB','AQ'
        else if (condition((pnd_Types_Break.equals("AL") || pnd_Types_Break.equals("AB") || pnd_Types_Break.equals("AQ"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("RO AP to Non-IRA");                                                                                                            //Natural: MOVE 'RO AP to Non-IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NV'
        else if (condition((pnd_Types_Break.equals("NV"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Roth Conv WD<5yr");                                                                                                            //Natural: MOVE 'Roth Conv WD<5yr' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NH'
        else if (condition((pnd_Types_Break.equals("NH"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("REAC");                                                                                                                        //Natural: MOVE 'REAC' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NS'
        else if (condition((pnd_Types_Break.equals("NS"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Straight Refund");                                                                                                             //Natural: MOVE 'Straight Refund' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NP'
        else if (condition((pnd_Types_Break.equals("NP"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Premium Refund");                                                                                                              //Natural: MOVE 'Premium Refund' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'NR'
        else if (condition((pnd_Types_Break.equals("NR"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("Roth IRA WD");                                                                                                                 //Natural: MOVE 'Roth IRA WD' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: VALUE 'BL','BB','BQ'
        else if (condition((pnd_Types_Break.equals("BL") || pnd_Types_Break.equals("BB") || pnd_Types_Break.equals("BQ"))))
        {
            decideConditionsMet414++;
            pnd_Distr_Code_Descr.setValue("RTB to Non-IRA");                                                                                                              //Natural: MOVE 'RTB to Non-IRA' TO #DISTR-CODE-DESCR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP0705-01",new                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP0705-01' 23X 'Tax Reporting And Withholding System' 31X 'Page:   ' *PAGE-NUMBER ( 01 ) / 42X 'Transactions By Systems Within Company' 30X 'Date: '*DATU / 42X ' Accepted + Cited Transactions (EDITS)' /'Company -' #TWRACOMP.#COMP-SHORT-NAME // '    Data Case          Trans       Gross       Tax Free     ' 'Taxable    Interest     Federal Tax      NRA       State      Local' / '       Type            Count      Amount          IVC       ' ' Amount      Amount      Wthhld        Wthhld      Wthhld     Wthhld' / '                                                            ' '                                                /Puerto-Rico /Canada' / '_' ( 132 )
                        ColumnSpacing(23),"Tax Reporting And Withholding System",new ColumnSpacing(31),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE,new 
                        ColumnSpacing(42),"Transactions By Systems Within Company",new ColumnSpacing(30),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(42)," Accepted + Cited Transactions (EDITS)",NEWLINE,"Company -",pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),NEWLINE,NEWLINE,"    Data Case          Trans       Gross       Tax Free     ","Taxable    Interest     Federal Tax      NRA       State      Local",NEWLINE,"       Type            Count      Amount          IVC       "," Amount      Amount      Wthhld        Wthhld      Wthhld     Wthhld",NEWLINE,"                                                            ","                                                /Puerto-Rico /Canada",NEWLINE,"_",new 
                        RepeatItem(132));
                    //* *
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0700_getTwrt_Record_Twrt_Pay_Set_TypesIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().isBreak(endOfData);
        boolean ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Source().isBreak(endOfData);
        boolean ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().isBreak(endOfData);
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_Pay_Set_TypesIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak))
        {
            pnd_Key_Pnd_Tbl_Nbr.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #TBL-NBR
            pnd_Key_Pnd_T_Y.setValue(readWork01Twrt_Tax_YearOld);                                                                                                         //Natural: MOVE OLD ( TWRT-TAX-YEAR ) TO #T-Y
            pnd_Key_Pnd_Type_Refer.setValue(readWork01Twrt_Type_ReferOld);                                                                                                //Natural: MOVE OLD ( TWRT-TYPE-REFER ) TO #TYPE-REFER
            pnd_Types_Break.setValue(readWork01Twrt_Pay_Set_TypesOld);                                                                                                    //Natural: MOVE OLD ( TWRT-PAY-SET-TYPES ) TO #TYPES-BREAK
            //*  DISTRIBUTION DESCR. BY REFER. NO
                                                                                                                                                                          //Natural: PERFORM FIND-NAME-BY-NO
            sub_Find_Name_By_No();
            if (condition(Global.isEscape())) {return;}
            pnd_Tax_Amount.compute(new ComputeParameters(false, pnd_Tax_Amount), readWork01Pnd_Twrt_Gross_AmtSum292.subtract(readWork01Pnd_Twrt_Ivc_AmtSum292));          //Natural: COMPUTE #TAX-AMOUNT = SUM ( #TWRT-GROSS-AMT ) - SUM ( #TWRT-IVC-AMT )
            //*  TRANSACTION COUNT
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Distr_Code_Descr,readWork01Twrt_Type_ReferNcount,readWork01Pnd_Twrt_Gross_AmtSum292,                   //Natural: WRITE ( 01 ) / #DISTR-CODE-DESCR NCOUNT ( TWRT-TYPE-REFER ) SUM ( #TWRT-GROSS-AMT ) SUM ( #TWRT-IVC-AMT ) #TAX-AMOUNT SUM ( TWRT-INTEREST-AMT ) SUM ( #TWRT-FED-AMT ) SUM ( TWRT-NRA-WHHLD-AMT ) #SUM-STATE-AMT SUM ( TWRT-LOCAL-WHHLD-AMT )
                readWork01Pnd_Twrt_Ivc_AmtSum292,pnd_Tax_Amount,readWork01Twrt_Interest_AmtSum292,readWork01Pnd_Twrt_Fed_AmtSum292,readWork01Twrt_Nra_Whhld_AmtSum292,
                pnd_Sum_State_Amt,readWork01Twrt_Local_Whhld_AmtSum292);
            if (condition(Global.isEscape())) return;
            //*      #DISTR-CODE
            //* *    SUM    (TWRT-STATE-WHHLD-AMT)
            if (condition(pnd_Sum_Can_Amt.greater(getZero()) || pnd_Sum_Pr_Amt.greater(getZero())))                                                                       //Natural: IF #SUM-CAN-AMT > 0 OR #SUM-PR-AMT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(109),pnd_Sum_Pr_Amt,pnd_Sum_Can_Amt);                                                        //Natural: WRITE ( 01 ) 109X #SUM-PR-AMT #SUM-CAN-AMT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //* *
            pnd_Tot_Pr_Amt.nadd(pnd_Sum_Pr_Amt);                                                                                                                          //Natural: COMPUTE #TOT-PR-AMT = #TOT-PR-AMT + #SUM-PR-AMT
            pnd_Tot_State_Amt.nadd(pnd_Sum_State_Amt);                                                                                                                    //Natural: COMPUTE #TOT-STATE-AMT = #TOT-STATE-AMT + #SUM-STATE-AMT
            pnd_Tot_Can_Amt.nadd(pnd_Sum_Can_Amt);                                                                                                                        //Natural: COMPUTE #TOT-CAN-AMT = #TOT-CAN-AMT + #SUM-CAN-AMT
            pnd_Sum_Can_Amt.reset();                                                                                                                                      //Natural: RESET #SUM-CAN-AMT #SUM-STATE-AMT #SUM-PR-AMT
            pnd_Sum_State_Amt.reset();
            pnd_Sum_Pr_Amt.reset();
            readWork01Pnd_Twrt_Gross_AmtSum292.setDec(new DbsDecimal(0));                                                                                                 //Natural: END-BREAK
            readWork01Pnd_Twrt_Ivc_AmtSum292.setDec(new DbsDecimal(0));
            readWork01Twrt_Type_ReferNcount292.setDec(new DbsDecimal(0));
            readWork01Twrt_Interest_AmtSum292.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Fed_AmtSum292.setDec(new DbsDecimal(0));
            readWork01Twrt_Nra_Whhld_AmtSum292.setDec(new DbsDecimal(0));
            readWork01Twrt_Local_Whhld_AmtSum292.setDec(new DbsDecimal(0));
        }
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak))
        {
            pnd_Tax_Amount.compute(new ComputeParameters(false, pnd_Tax_Amount), readWork01Pnd_Twrt_Gross_AmtSum292.subtract(readWork01Pnd_Twrt_Ivc_AmtSum292));          //Natural: COMPUTE #TAX-AMOUNT = SUM ( #TWRT-GROSS-AMT ) - SUM ( #TWRT-IVC-AMT )
            //* *
            //*  TRANSACTION COUNT
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(132),NEWLINE,new ColumnSpacing(3),"Total(",readWork01Twrt_SourceOld,")",new             //Natural: WRITE ( 01 ) / '*' ( 132 ) / 3X 'Total('OLD ( TWRT-SOURCE ) ')' 3X NCOUNT ( TWRT-TYPE-REFER ) SUM ( #TWRT-GROSS-AMT ) SUM ( #TWRT-IVC-AMT ) #TAX-AMOUNT SUM ( TWRT-INTEREST-AMT ) SUM ( #TWRT-FED-AMT ) SUM ( TWRT-NRA-WHHLD-AMT ) #TOT-STATE-AMT SUM ( TWRT-LOCAL-WHHLD-AMT )
                ColumnSpacing(3),readWork01Twrt_Type_ReferNcount,readWork01Pnd_Twrt_Gross_AmtSum314,readWork01Pnd_Twrt_Ivc_AmtSum314,pnd_Tax_Amount,readWork01Twrt_Interest_AmtSum314,
                readWork01Pnd_Twrt_Fed_AmtSum314,readWork01Twrt_Nra_Whhld_AmtSum314,pnd_Tot_State_Amt,readWork01Twrt_Local_Whhld_AmtSum314);
            if (condition(Global.isEscape())) return;
            //* *    SUM    (TWRT-STATE-WHHLD-AMT)
            if (condition(pnd_Tot_Can_Amt.greater(getZero()) || pnd_Tot_Pr_Amt.greater(getZero())))                                                                       //Natural: IF #TOT-CAN-AMT > 0 OR #TOT-PR-AMT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(109),pnd_Tot_Pr_Amt,pnd_Tot_Can_Amt);                                                        //Natural: WRITE ( 01 ) 109X #TOT-PR-AMT #TOT-CAN-AMT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //* *
            pnd_Grd_Pr_Amt.nadd(pnd_Tot_Pr_Amt);                                                                                                                          //Natural: COMPUTE #GRD-PR-AMT = #GRD-PR-AMT + #TOT-PR-AMT
            pnd_Grd_State_Amt.nadd(pnd_Tot_State_Amt);                                                                                                                    //Natural: COMPUTE #GRD-STATE-AMT = #GRD-STATE-AMT + #TOT-STATE-AMT
            pnd_Grd_Can_Amt.nadd(pnd_Tot_Can_Amt);                                                                                                                        //Natural: COMPUTE #GRD-CAN-AMT = #GRD-CAN-AMT + #TOT-CAN-AMT
            pnd_Tot_Can_Amt.reset();                                                                                                                                      //Natural: RESET #TOT-CAN-AMT #TOT-STATE-AMT #TOT-PR-AMT
            pnd_Tot_State_Amt.reset();
            pnd_Tot_Pr_Amt.reset();
            //* *
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().greater(" ") && ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals(readWork01Twrt_Company_CdeOld)))  //Natural: IF TWRT-SOURCE > ' ' AND TWRT-COMPANY-CDE = OLD ( TWRT-COMPANY-CDE )
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Source code:",ldaTwrl0700.getTwrt_Record_Twrt_Source(),NEWLINE);                              //Natural: WRITE ( 01 ) // 'Source code:' TWRT-SOURCE /
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Twrt_Gross_AmtSum314.setDec(new DbsDecimal(0));                                                                                                 //Natural: END-BREAK
            readWork01Pnd_Twrt_Ivc_AmtSum314.setDec(new DbsDecimal(0));
            readWork01Twrt_Type_ReferNcount314.setDec(new DbsDecimal(0));
            readWork01Twrt_Interest_AmtSum314.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Fed_AmtSum314.setDec(new DbsDecimal(0));
            readWork01Twrt_Nra_Whhld_AmtSum314.setDec(new DbsDecimal(0));
            readWork01Twrt_Local_Whhld_AmtSum314.setDec(new DbsDecimal(0));
        }
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-NAME
            sub_Get_Company_Name();
            if (condition(Global.isEscape())) {return;}
            pnd_Tax_Amount.compute(new ComputeParameters(false, pnd_Tax_Amount), readWork01Pnd_Twrt_Gross_AmtSum292.subtract(readWork01Pnd_Twrt_Ivc_AmtSum292));          //Natural: COMPUTE #TAX-AMOUNT = SUM ( #TWRT-GROSS-AMT ) - SUM ( #TWRT-IVC-AMT )
            //* *
            //*  TRANSACTION COUNT
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(132),NEWLINE,"Company Totals: ",readWork01Twrt_Type_ReferNcount,readWork01Pnd_Twrt_Gross_AmtSum334, //Natural: WRITE ( 01 ) / '*' ( 132 ) / 'Company Totals: ' NCOUNT ( TWRT-TYPE-REFER ) SUM ( #TWRT-GROSS-AMT ) SUM ( #TWRT-IVC-AMT ) #TAX-AMOUNT SUM ( TWRT-INTEREST-AMT ) SUM ( #TWRT-FED-AMT ) SUM ( TWRT-NRA-WHHLD-AMT ) #GRD-STATE-AMT SUM ( TWRT-LOCAL-WHHLD-AMT )
                readWork01Pnd_Twrt_Ivc_AmtSum334,pnd_Tax_Amount,readWork01Twrt_Interest_AmtSum334,readWork01Pnd_Twrt_Fed_AmtSum334,readWork01Twrt_Nra_Whhld_AmtSum334,
                pnd_Grd_State_Amt,readWork01Twrt_Local_Whhld_AmtSum334);
            if (condition(Global.isEscape())) return;
            //* *    SUM    (TWRT-STATE-WHHLD-AMT)
            if (condition(pnd_Grd_Can_Amt.greater(getZero()) || pnd_Grd_Pr_Amt.greater(getZero())))                                                                       //Natural: IF #GRD-CAN-AMT > 0 OR #GRD-PR-AMT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(109),pnd_Grd_Pr_Amt,pnd_Grd_Can_Amt);                                                        //Natural: WRITE ( 01 ) 109X #GRD-PR-AMT #GRD-CAN-AMT
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
            pnd_Grd_Can_Amt.reset();                                                                                                                                      //Natural: RESET #GRD-CAN-AMT #GRD-STATE-AMT #GRD-PR-AMT
            pnd_Grd_State_Amt.reset();
            pnd_Grd_Pr_Amt.reset();
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().notEquals(" ")))                                                                                  //Natural: IF TWRT-COMPANY-CDE NE ' '
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Source code:",ldaTwrl0700.getTwrt_Record_Twrt_Source(),NEWLINE);                              //Natural: WRITE ( 01 ) // 'Source code:' TWRT-SOURCE /
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Twrt_Gross_AmtSum334.setDec(new DbsDecimal(0));                                                                                                 //Natural: END-BREAK
            readWork01Pnd_Twrt_Ivc_AmtSum334.setDec(new DbsDecimal(0));
            readWork01Twrt_Type_ReferNcount334.setDec(new DbsDecimal(0));
            readWork01Twrt_Interest_AmtSum334.setDec(new DbsDecimal(0));
            readWork01Pnd_Twrt_Fed_AmtSum334.setDec(new DbsDecimal(0));
            readWork01Twrt_Nra_Whhld_AmtSum334.setDec(new DbsDecimal(0));
            readWork01Twrt_Local_Whhld_AmtSum334.setDec(new DbsDecimal(0));
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=61");
    }
    private void CheckAtStartofData278() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-NAME
            sub_Get_Company_Name();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Source code:",ldaTwrl0700.getTwrt_Record_Twrt_Source(),NEWLINE);                                  //Natural: WRITE ( 01 ) // 'Source code:' TWRT-SOURCE /
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-START
    }
}
