/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:21 PM
**        * FROM NATURAL PROGRAM : Twrp7200
************************************************************
**        * FILE NAME            : Twrp7200.java
**        * CLASS NAME           : Twrp7200
**        * INSTANCE NAME        : Twrp7200
************************************************************
************************************************************************
** PROGRAM : TWRP7200
** SYSTEM  : TAXWARS
** AUTHOR  : FRANCIS ENDAYA
** FUNCTION: UPDATE PARTICIPANT's File with information coming from
**           MDM ETL.
** HISTORY.....:
**    02/12/2016 FENDAYA NEW
**    11/30/2016 RAHUL DAS - COR NAS DEFECT FIX
**                           I) INTRODUCE RLUP-PIN
**                          II) REMOVING REDUNDANT LOGIC FOR POPULATING
**                              WK-MDM-NAMES & WK-MDM-ADDRESS
**                         III) CORRECTING BYPASS LOGIC FOR ZERO PIN TO
**                              HANDLE VALID NAME AND ADDRESS CASES
**                          IV) UPDATE USER AND UPDATE TIME FIX
**               TAG - DASRAH
**    06/14/2017 COGNIZANT - PIN EXPANSION TAG - PIN-NBR EXPANSION
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp7200 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9501 ldaTwrl9501;
    private LdaTwrliso ldaTwrliso;
    private LdaTwrl9802 ldaTwrl9802;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_twrparti_Upd;
    private DbsField twrparti_Upd_Twrparti_Status;
    private DbsField twrparti_Upd_Twrparti_Tax_Id_Type;
    private DbsField twrparti_Upd_Twrparti_Tax_Id;
    private DbsField twrparti_Upd_Twrparti_Pin;
    private DbsField twrparti_Upd_Twrparti_Participant_Name;
    private DbsField twrparti_Upd_Twrparti_Addr_Ln1;
    private DbsField twrparti_Upd_Twrparti_Participant_Dob;
    private DbsField twrparti_Upd_Twrparti_Participant_Dod;
    private DbsField twrparti_Upd_Twrparti_Residency_Type;
    private DbsField twrparti_Upd_Twrparti_Residency_Code;
    private DbsField twrparti_Upd_Twrparti_Locality_Cde;
    private DbsField twrparti_Upd_Twrparti_Citizen_Cde;
    private DbsField twrparti_Upd_Twrparti_Updt_User;
    private DbsField twrparti_Upd_Twrparti_Updt_Dte;
    private DbsField twrparti_Upd_Twrparti_Updt_Time;
    private DbsField twrparti_Upd_Twrparti_Lu_Ts;
    private DbsField twrparti_Upd_Twrparti_Rlup_Last_Name;
    private DbsField twrparti_Upd_Twrparti_Rlup_First_Name;
    private DbsField twrparti_Upd_Twrparti_Rlup_Middle_Name;
    private DbsField twrparti_Upd_Twrparti_Rlup_Part_Name;
    private DbsField twrparti_Upd_Twrparti_Rlup_Dob;
    private DbsField twrparti_Upd_Twrparti_Rlup_Dod;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln1;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln2;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln3;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln4;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln5;
    private DbsField twrparti_Upd_Twrparti_Rlup_Addr_Ln6;
    private DbsField twrparti_Upd_Twrparti_Rlup_Postal_Data;
    private DbsField twrparti_Upd_Twrparti_Rlup_Type_Cde;
    private DbsField twrparti_Upd_Twrparti_Rlup_Geo_Code;
    private DbsField twrparti_Upd_Twrparti_Rlup_Email_Addr;
    private DbsField twrparti_Upd_Twrparti_Rlup_Email_Pref;
    private DbsField twrparti_Upd_Twrparti_Rlup_Email_Addr_Dte;
    private DbsField twrparti_Upd_Twrparti_Rlup_Email_Pref_Dte;
    private DbsField twrparti_Upd_Twrparti_Rlup_Pin;

    private DataAccessProgramView vw_ref_Read;

    private DbsGroup ref_Read_Rt_Record;
    private DbsField ref_Read_Rt_A_I_Ind;
    private DbsField ref_Read_Rt_Table_Id;
    private DbsField ref_Read_Rt_Short_Key;
    private DbsField ref_Read_Rt_Long_Key;
    private DbsField ref_Read_Rt_Desc1;
    private DbsField ref_Read_Rt_Desc2;
    private DbsField ref_Read_Rt_Desc3;
    private DbsField ref_Read_Rt_Desc4;
    private DbsField ref_Read_Rt_Desc5;
    private DbsGroup ref_Read_Rt_DescriptionMuGroup;
    private DbsField ref_Read_Rt_Description;
    private DbsField ref_Read_Rt_Eff_From_Ccyymmdd;
    private DbsField ref_Read_Rt_Eff_To_Ccyymmdd;
    private DbsField ref_Read_Rt_Upd_Source;
    private DbsField ref_Read_Rt_Upd_User;
    private DbsField ref_Read_Rt_Upd_Ccyymmdd;
    private DbsField ref_Read_Rt_Upd_Timn;

    private DataAccessProgramView vw_ref_Upd;

    private DbsGroup ref_Upd_Rt_Record;
    private DbsField ref_Upd_Rt_A_I_Ind;
    private DbsField ref_Upd_Rt_Table_Id;
    private DbsField ref_Upd_Rt_Short_Key;
    private DbsField ref_Upd_Rt_Long_Key;
    private DbsField ref_Upd_Rt_Desc1;
    private DbsField ref_Upd_Rt_Desc2;
    private DbsField ref_Upd_Rt_Desc3;
    private DbsField ref_Upd_Rt_Desc4;
    private DbsField ref_Upd_Rt_Desc5;
    private DbsGroup ref_Upd_Rt_DescriptionMuGroup;
    private DbsField ref_Upd_Rt_Description;
    private DbsField ref_Upd_Rt_Eff_From_Ccyymmdd;
    private DbsField ref_Upd_Rt_Eff_To_Ccyymmdd;
    private DbsField ref_Upd_Rt_Upd_Source;
    private DbsField ref_Upd_Rt_Upd_User;
    private DbsField ref_Upd_Rt_Upd_Ccyymmdd;
    private DbsField ref_Upd_Rt_Upd_Timn;

    private DataAccessProgramView vw_ref_Stor;

    private DbsGroup ref_Stor_Rt_Record;
    private DbsField ref_Stor_Rt_A_I_Ind;
    private DbsField ref_Stor_Rt_Table_Id;
    private DbsField ref_Stor_Rt_Short_Key;
    private DbsField ref_Stor_Rt_Long_Key;
    private DbsField ref_Stor_Rt_Desc1;
    private DbsField ref_Stor_Rt_Desc2;
    private DbsField ref_Stor_Rt_Desc3;
    private DbsField ref_Stor_Rt_Desc4;
    private DbsField ref_Stor_Rt_Desc5;
    private DbsGroup ref_Stor_Rt_DescriptionMuGroup;
    private DbsField ref_Stor_Rt_Description;
    private DbsField ref_Stor_Rt_Eff_From_Ccyymmdd;
    private DbsField ref_Stor_Rt_Eff_To_Ccyymmdd;
    private DbsField ref_Stor_Rt_Upd_Source;
    private DbsField ref_Stor_Rt_Upd_User;
    private DbsField ref_Stor_Rt_Upd_Ccyymmdd;
    private DbsField ref_Stor_Rt_Upd_Timn;

    private DbsGroup pnd_Mdm_File;
    private DbsField pnd_Mdm_File_Pnd_Twrpymnt_Tax_Year;
    private DbsField pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Type;
    private DbsField pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Mdm_File_Pnd_Twrpymnt_Payee_Cde;
    private DbsField pnd_Mdm_File_Pnd_Pin_X;

    private DbsGroup pnd_Mdm_File__R_Field_1;
    private DbsField pnd_Mdm_File_Pnd_Pin;
    private DbsField pnd_Mdm_File_Pnd_Names;

    private DbsGroup pnd_Mdm_File__R_Field_2;
    private DbsField pnd_Mdm_File_Pnd_Last_Name;
    private DbsField pnd_Mdm_File_Pnd_First_Name;
    private DbsField pnd_Mdm_File_Pnd_Middle_Name;
    private DbsField pnd_Mdm_File_Pnd_Dob;
    private DbsField pnd_Mdm_File_Pnd_Dod;
    private DbsField pnd_Mdm_File_Pnd_Part_Name;
    private DbsField pnd_Mdm_File_Pnd_Address_Lines;

    private DbsGroup pnd_Mdm_File__R_Field_3;
    private DbsField pnd_Mdm_File_Pnd_Address_Line;

    private DbsGroup pnd_Mdm_File__R_Field_4;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_1;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_2;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_3;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_4;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_5;
    private DbsField pnd_Mdm_File_Pnd_Addr_Lne_6;
    private DbsField pnd_Mdm_File_Pnd_Addr_Postal_Data;
    private DbsField pnd_Mdm_File_Pnd_Addrss_Type_Cde;
    private DbsField pnd_Mdm_File_Pnd_Geo;
    private DbsField pnd_Mdm_File_Pnd_Email_Pref;
    private DbsField pnd_Mdm_File_Pnd_Email_Pref_Dte;
    private DbsField pnd_Mdm_File_Pnd_Email_Addr;
    private DbsField pnd_Mdm_File_Pnd_Email_Addr_Dte;
    private DbsField pnd_Mdm_File_Pnd_Filler;

    private DbsGroup pnd_Work_Areas;
    private DbsField pnd_Work_Areas_Pnd_Read_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Valid_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Upd_Ref_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Upd_Part_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Add_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Rt_Add_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Bypass_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Reject_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Et_Cnt;
    private DbsField pnd_Work_Areas_Pnd_Et_Limit;
    private DbsField pnd_Work_Areas_Pnd_Wk_Address;
    private DbsField pnd_Work_Areas_Pnd_Wk_Names;
    private DbsField pnd_Work_Areas_Pnd_Wk_Isn;
    private DbsField pnd_Work_Areas_Pnd_Wk_Rt_Isn;
    private DbsField pnd_Work_Areas_Pnd_Work_Date;
    private DbsField pnd_Work_Areas_Pnd_Wk_Dob;
    private DbsField pnd_Work_Areas_Pnd_Wk_Dod;
    private DbsField pnd_Twrparti_Curr_Rec_Sd;

    private DbsGroup pnd_Twrparti_Curr_Rec_Sd__R_Field_5;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status;
    private DbsField pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte;
    private DbsField pnd_Key_Rt_Super2;

    private DbsGroup pnd_Key_Rt_Super2__R_Field_6;
    private DbsField pnd_Key_Rt_Super2_Pnd_Key_Rt_A_I_Ind;
    private DbsField pnd_Key_Rt_Super2_Pnd_Key_Rt_Table_Id;
    private DbsField pnd_Key_Rt_Super2_Pnd_Key_Rt_Long_Key;
    private DbsField pnd_Key_Rt_Super2_Pnd_Key_Rt_Short_Key;
    private DbsField pnd_Rt_Short_Key;

    private DbsGroup pnd_Rt_Short_Key__R_Field_7;
    private DbsField pnd_Rt_Short_Key_Pnd_Rt_Pin;
    private DbsField pnd_Rt_Short_Key_Pnd_Rt_Contract_Number;
    private DbsField pnd_Rt_Short_Key_Pnd_Rt_Payee_Code;
    private DbsField pnd_Rt_Short_Key_Pnd_Rt_Filler1;
    private DbsField pnd_Rt_Long_Key;

    private DbsGroup pnd_Rt_Long_Key__R_Field_8;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Ssn;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Contract_Number;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Payee_Code;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Tax_Year;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Tax_Id_Type;
    private DbsField pnd_Rt_Long_Key_Pnd_Rtl_Filler2;
    private DbsField pnd_Rtd1_Desc1;

    private DbsGroup pnd_Rtd1_Desc1__R_Field_9;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_Last_Name;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_First_Name;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_Middle_Name;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_Part_Name;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_Dob;
    private DbsField pnd_Rtd1_Desc1_Pnd_Rtd1_Dod;
    private DbsField pnd_Rtd1_Desc1_Pnd_Filler3;
    private DbsField pnd_Rtd2_Desc2;

    private DbsGroup pnd_Rtd2_Desc2__R_Field_10;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_1;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_2;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_3;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_4;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_5;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_6;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Postal_Data;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Addrss_Type_Cde;
    private DbsField pnd_Rtd2_Desc2_Pnd_Rtd2_Geo_Code;
    private DbsField pnd_Rtd2_Desc2_Pnd_Filler4;
    private DbsField pnd_No_Pin;
    private DbsField pnd_Date;
    private DbsField pnd_Irs_Names;
    private DbsField pnd_Country_Name;
    private DbsField pnd_Date_Yr;

    private DbsGroup pnd_Date_Yr__R_Field_11;
    private DbsField pnd_Date_Yr_Pnd_Date_Yr_N;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Country;
    private DbsField pnd_Aliases;
    private DbsField pnd_Fill;
    private DbsField pnd_Country_Nf_Ind;
    private DbsField pnd_Index;
    private DbsField pnd_I;
    private DbsField pnd_K;
    private DbsField pnd_Count;
    private DbsField pnd_Country_Not_Found;
    private DbsField pnd_Addr_Found;
    private DbsField pnd_Addr_Not_Found;
    private DbsField pnd_Rt_Rec_Exist;
    private DbsField pnd_Cn_Su;

    private DbsGroup pnd_Cn_Su__R_Field_12;
    private DbsField pnd_Cn_Su__Filler1;
    private DbsField pnd_Cn_Su_Pnd_Cn_Yr;
    private DbsField pnd_Cn_Su_Pnd_Cn_Code;
    private DbsField pnd_Timn_N;

    private DbsGroup pnd_Timn_N__R_Field_13;
    private DbsField pnd_Timn_N_Pnd_Timn_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9501 = new LdaTwrl9501();
        registerRecord(ldaTwrl9501);
        registerRecord(ldaTwrl9501.getVw_twrparti_Part());
        ldaTwrliso = new LdaTwrliso();
        registerRecord(ldaTwrliso);
        ldaTwrl9802 = new LdaTwrl9802();
        registerRecord(ldaTwrl9802);
        registerRecord(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi());

        // Local Variables
        localVariables = new DbsRecord();

        vw_twrparti_Upd = new DataAccessProgramView(new NameInfo("vw_twrparti_Upd", "TWRPARTI-UPD"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        twrparti_Upd_Twrparti_Status = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_STATUS");
        twrparti_Upd_Twrparti_Tax_Id_Type = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID_TYPE");
        twrparti_Upd_Twrparti_Tax_Id = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID");
        twrparti_Upd_Twrparti_Pin = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "TWRPARTI_PIN");
        twrparti_Upd_Twrparti_Participant_Name = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        twrparti_Upd_Twrparti_Addr_Ln1 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Addr_Ln1", "TWRPARTI-ADDR-LN1", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ADDR_LN1");
        twrparti_Upd_Twrparti_Participant_Dob = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Participant_Dob", "TWRPARTI-PARTICIPANT-DOB", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOB");
        twrparti_Upd_Twrparti_Participant_Dod = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Participant_Dod", "TWRPARTI-PARTICIPANT-DOD", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOD");
        twrparti_Upd_Twrparti_Residency_Type = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Residency_Type", "TWRPARTI-RESIDENCY-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_TYPE");
        twrparti_Upd_Twrparti_Residency_Code = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Residency_Code", "TWRPARTI-RESIDENCY-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_CODE");
        twrparti_Upd_Twrparti_Locality_Cde = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Locality_Cde", "TWRPARTI-LOCALITY-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TWRPARTI_LOCALITY_CDE");
        twrparti_Upd_Twrparti_Citizen_Cde = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Citizen_Cde", "TWRPARTI-CITIZEN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPARTI_CITIZEN_CDE");
        twrparti_Upd_Twrparti_Updt_User = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Updt_User", "TWRPARTI-UPDT-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_UPDT_USER");
        twrparti_Upd_Twrparti_Updt_Dte = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Updt_Dte", "TWRPARTI-UPDT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_UPDT_DTE");
        twrparti_Upd_Twrparti_Updt_Time = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Updt_Time", "TWRPARTI-UPDT-TIME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "TWRPARTI_UPDT_TIME");
        twrparti_Upd_Twrparti_Lu_Ts = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Lu_Ts", "TWRPARTI-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TWRPARTI_LU_TS");
        twrparti_Upd_Twrparti_Rlup_Last_Name = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Last_Name", "TWRPARTI-RLUP-LAST-NAME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_LAST_NAME");
        twrparti_Upd_Twrparti_Rlup_First_Name = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_First_Name", "TWRPARTI-RLUP-FIRST-NAME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_FIRST_NAME");
        twrparti_Upd_Twrparti_Rlup_Middle_Name = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Middle_Name", "TWRPARTI-RLUP-MIDDLE-NAME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_MIDDLE_NAME");
        twrparti_Upd_Twrparti_Rlup_Part_Name = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Part_Name", "TWRPARTI-RLUP-PART-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_PART_NAME");
        twrparti_Upd_Twrparti_Rlup_Dob = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Dob", "TWRPARTI-RLUP-DOB", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_DOB");
        twrparti_Upd_Twrparti_Rlup_Dod = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Dod", "TWRPARTI-RLUP-DOD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_DOD");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln1 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln1", "TWRPARTI-RLUP-ADDR-LN1", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN1");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln2 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln2", "TWRPARTI-RLUP-ADDR-LN2", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN2");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln3 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln3", "TWRPARTI-RLUP-ADDR-LN3", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN3");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln4 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln4", "TWRPARTI-RLUP-ADDR-LN4", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN4");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln5 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln5", "TWRPARTI-RLUP-ADDR-LN5", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN5");
        twrparti_Upd_Twrparti_Rlup_Addr_Ln6 = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Addr_Ln6", "TWRPARTI-RLUP-ADDR-LN6", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_ADDR_LN6");
        twrparti_Upd_Twrparti_Rlup_Postal_Data = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Postal_Data", "TWRPARTI-RLUP-POSTAL-DATA", 
            FieldType.STRING, 32, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_POSTAL_DATA");
        twrparti_Upd_Twrparti_Rlup_Type_Cde = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Type_Cde", "TWRPARTI-RLUP-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_TYPE_CDE");
        twrparti_Upd_Twrparti_Rlup_Geo_Code = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Geo_Code", "TWRPARTI-RLUP-GEO-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_GEO_CODE");
        twrparti_Upd_Twrparti_Rlup_Email_Addr = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Email_Addr", "TWRPARTI-RLUP-EMAIL-ADDR", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR");
        twrparti_Upd_Twrparti_Rlup_Email_Pref = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Email_Pref", "TWRPARTI-RLUP-EMAIL-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF");
        twrparti_Upd_Twrparti_Rlup_Email_Addr_Dte = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Email_Addr_Dte", "TWRPARTI-RLUP-EMAIL-ADDR-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_ADDR_DTE");
        twrparti_Upd_Twrparti_Rlup_Email_Pref_Dte = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Email_Pref_Dte", "TWRPARTI-RLUP-EMAIL-PREF-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_EMAIL_PREF_DTE");
        twrparti_Upd_Twrparti_Rlup_Pin = vw_twrparti_Upd.getRecord().newFieldInGroup("twrparti_Upd_Twrparti_Rlup_Pin", "TWRPARTI-RLUP-PIN", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TWRPARTI_RLUP_PIN");
        registerRecord(vw_twrparti_Upd);

        vw_ref_Read = new DataAccessProgramView(new NameInfo("vw_ref_Read", "REF-READ"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        ref_Read_Rt_Record = vw_ref_Read.getRecord().newGroupInGroup("REF_READ_RT_RECORD", "RT-RECORD");
        ref_Read_Rt_A_I_Ind = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RT_A_I_IND");
        ref_Read_Rt_Table_Id = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        ref_Read_Rt_Short_Key = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        ref_Read_Rt_Long_Key = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        ref_Read_Rt_Desc1 = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ref_Read_Rt_Desc2 = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        ref_Read_Rt_Desc3 = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        ref_Read_Rt_Desc4 = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        ref_Read_Rt_Desc5 = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ref_Read_Rt_DescriptionMuGroup = ref_Read_Rt_Record.newGroupInGroup("REF_READ_RT_DESCRIPTIONMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Read_Rt_Description = ref_Read_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Read_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, 
            new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Read_Rt_Eff_From_Ccyymmdd = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        ref_Read_Rt_Eff_To_Ccyymmdd = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ref_Read_Rt_Upd_Source = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        ref_Read_Rt_Upd_User = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        ref_Read_Rt_Upd_Ccyymmdd = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        ref_Read_Rt_Upd_Timn = ref_Read_Rt_Record.newFieldInGroup("ref_Read_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_ref_Read);

        vw_ref_Upd = new DataAccessProgramView(new NameInfo("vw_ref_Upd", "REF-UPD"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        ref_Upd_Rt_Record = vw_ref_Upd.getRecord().newGroupInGroup("REF_UPD_RT_RECORD", "RT-RECORD");
        ref_Upd_Rt_A_I_Ind = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Upd_Rt_Table_Id = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        ref_Upd_Rt_Short_Key = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        ref_Upd_Rt_Long_Key = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        ref_Upd_Rt_Desc1 = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ref_Upd_Rt_Desc2 = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        ref_Upd_Rt_Desc3 = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        ref_Upd_Rt_Desc4 = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        ref_Upd_Rt_Desc5 = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ref_Upd_Rt_DescriptionMuGroup = ref_Upd_Rt_Record.newGroupInGroup("REF_UPD_RT_DESCRIPTIONMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Upd_Rt_Description = ref_Upd_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Upd_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, 
            new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Upd_Rt_Eff_From_Ccyymmdd = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        ref_Upd_Rt_Eff_To_Ccyymmdd = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ref_Upd_Rt_Upd_Source = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        ref_Upd_Rt_Upd_User = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        ref_Upd_Rt_Upd_Ccyymmdd = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        ref_Upd_Rt_Upd_Timn = ref_Upd_Rt_Record.newFieldInGroup("ref_Upd_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_ref_Upd);

        vw_ref_Stor = new DataAccessProgramView(new NameInfo("vw_ref_Stor", "REF-STOR"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        ref_Stor_Rt_Record = vw_ref_Stor.getRecord().newGroupInGroup("REF_STOR_RT_RECORD", "RT-RECORD");
        ref_Stor_Rt_A_I_Ind = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RT_A_I_IND");
        ref_Stor_Rt_Table_Id = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        ref_Stor_Rt_Short_Key = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        ref_Stor_Rt_Long_Key = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        ref_Stor_Rt_Desc1 = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ref_Stor_Rt_Desc2 = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        ref_Stor_Rt_Desc3 = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        ref_Stor_Rt_Desc4 = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        ref_Stor_Rt_Desc5 = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ref_Stor_Rt_DescriptionMuGroup = ref_Stor_Rt_Record.newGroupInGroup("REF_STOR_RT_DESCRIPTIONMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Stor_Rt_Description = ref_Stor_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Stor_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, 
            new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Stor_Rt_Eff_From_Ccyymmdd = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RT_EFF_FROM_CCYYMMDD");
        ref_Stor_Rt_Eff_To_Ccyymmdd = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ref_Stor_Rt_Upd_Source = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_SOURCE");
        ref_Stor_Rt_Upd_User = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_USER");
        ref_Stor_Rt_Upd_Ccyymmdd = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        ref_Stor_Rt_Upd_Timn = ref_Stor_Rt_Record.newFieldInGroup("ref_Stor_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "RT_UPD_TIMN");
        registerRecord(vw_ref_Stor);

        pnd_Mdm_File = localVariables.newGroupInRecord("pnd_Mdm_File", "#MDM-FILE");
        pnd_Mdm_File_Pnd_Twrpymnt_Tax_Year = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Twrpymnt_Tax_Year", "#TWRPYMNT-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Type = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Type", "#TWRPYMNT-TAX-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr", "#TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 
            10);
        pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr", "#TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Mdm_File_Pnd_Twrpymnt_Payee_Cde = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Twrpymnt_Payee_Cde", "#TWRPYMNT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Mdm_File_Pnd_Pin_X = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Pin_X", "#PIN-X", FieldType.STRING, 12);

        pnd_Mdm_File__R_Field_1 = pnd_Mdm_File.newGroupInGroup("pnd_Mdm_File__R_Field_1", "REDEFINE", pnd_Mdm_File_Pnd_Pin_X);
        pnd_Mdm_File_Pnd_Pin = pnd_Mdm_File__R_Field_1.newFieldInGroup("pnd_Mdm_File_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Mdm_File_Pnd_Names = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Names", "#NAMES", FieldType.STRING, 90);

        pnd_Mdm_File__R_Field_2 = pnd_Mdm_File.newGroupInGroup("pnd_Mdm_File__R_Field_2", "REDEFINE", pnd_Mdm_File_Pnd_Names);
        pnd_Mdm_File_Pnd_Last_Name = pnd_Mdm_File__R_Field_2.newFieldInGroup("pnd_Mdm_File_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_Mdm_File_Pnd_First_Name = pnd_Mdm_File__R_Field_2.newFieldInGroup("pnd_Mdm_File_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        pnd_Mdm_File_Pnd_Middle_Name = pnd_Mdm_File__R_Field_2.newFieldInGroup("pnd_Mdm_File_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);
        pnd_Mdm_File_Pnd_Dob = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Dob", "#DOB", FieldType.STRING, 8);
        pnd_Mdm_File_Pnd_Dod = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Dod", "#DOD", FieldType.STRING, 8);
        pnd_Mdm_File_Pnd_Part_Name = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Part_Name", "#PART-NAME", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Address_Lines = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Address_Lines", "#ADDRESS-LINES", FieldType.STRING, 210);

        pnd_Mdm_File__R_Field_3 = pnd_Mdm_File.newGroupInGroup("pnd_Mdm_File__R_Field_3", "REDEFINE", pnd_Mdm_File_Pnd_Address_Lines);
        pnd_Mdm_File_Pnd_Address_Line = pnd_Mdm_File__R_Field_3.newFieldArrayInGroup("pnd_Mdm_File_Pnd_Address_Line", "#ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 6));

        pnd_Mdm_File__R_Field_4 = pnd_Mdm_File.newGroupInGroup("pnd_Mdm_File__R_Field_4", "REDEFINE", pnd_Mdm_File_Pnd_Address_Lines);
        pnd_Mdm_File_Pnd_Addr_Lne_1 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_1", "#ADDR-LNE-1", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Lne_2 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_2", "#ADDR-LNE-2", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Lne_3 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_3", "#ADDR-LNE-3", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Lne_4 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_4", "#ADDR-LNE-4", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Lne_5 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_5", "#ADDR-LNE-5", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Lne_6 = pnd_Mdm_File__R_Field_4.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Lne_6", "#ADDR-LNE-6", FieldType.STRING, 35);
        pnd_Mdm_File_Pnd_Addr_Postal_Data = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Addr_Postal_Data", "#ADDR-POSTAL-DATA", FieldType.STRING, 32);
        pnd_Mdm_File_Pnd_Addrss_Type_Cde = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Addrss_Type_Cde", "#ADDRSS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Mdm_File_Pnd_Geo = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Geo", "#GEO", FieldType.STRING, 2);
        pnd_Mdm_File_Pnd_Email_Pref = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Email_Pref", "#EMAIL-PREF", FieldType.STRING, 1);
        pnd_Mdm_File_Pnd_Email_Pref_Dte = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Email_Pref_Dte", "#EMAIL-PREF-DTE", FieldType.STRING, 8);
        pnd_Mdm_File_Pnd_Email_Addr = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Email_Addr", "#EMAIL-ADDR", FieldType.STRING, 70);
        pnd_Mdm_File_Pnd_Email_Addr_Dte = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Email_Addr_Dte", "#EMAIL-ADDR-DTE", FieldType.STRING, 8);
        pnd_Mdm_File_Pnd_Filler = pnd_Mdm_File.newFieldInGroup("pnd_Mdm_File_Pnd_Filler", "#FILLER", FieldType.STRING, 1);

        pnd_Work_Areas = localVariables.newGroupInRecord("pnd_Work_Areas", "#WORK-AREAS");
        pnd_Work_Areas_Pnd_Read_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Valid_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Valid_Cnt", "#VALID-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Upd_Ref_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Upd_Ref_Cnt", "#UPD-REF-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Upd_Part_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Upd_Part_Cnt", "#UPD-PART-CNT", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Work_Areas_Pnd_Add_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Add_Cnt", "#ADD-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Rt_Add_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Rt_Add_Cnt", "#RT-ADD-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Bypass_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Bypass_Cnt", "#BYPASS-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Reject_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Reject_Cnt", "#REJECT-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Et_Cnt = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Et_Limit = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Et_Limit", "#ET-LIMIT", FieldType.PACKED_DECIMAL, 12);
        pnd_Work_Areas_Pnd_Wk_Address = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Address", "#WK-ADDRESS", FieldType.STRING, 210);
        pnd_Work_Areas_Pnd_Wk_Names = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Names", "#WK-NAMES", FieldType.STRING, 90);
        pnd_Work_Areas_Pnd_Wk_Isn = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Isn", "#WK-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_Areas_Pnd_Wk_Rt_Isn = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Rt_Isn", "#WK-RT-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_Areas_Pnd_Work_Date = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);
        pnd_Work_Areas_Pnd_Wk_Dob = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Dob", "#WK-DOB", FieldType.STRING, 8);
        pnd_Work_Areas_Pnd_Wk_Dod = pnd_Work_Areas.newFieldInGroup("pnd_Work_Areas_Pnd_Wk_Dod", "#WK-DOD", FieldType.STRING, 8);
        pnd_Twrparti_Curr_Rec_Sd = localVariables.newFieldInRecord("pnd_Twrparti_Curr_Rec_Sd", "#TWRPARTI-CURR-REC-SD", FieldType.STRING, 19);

        pnd_Twrparti_Curr_Rec_Sd__R_Field_5 = localVariables.newGroupInRecord("pnd_Twrparti_Curr_Rec_Sd__R_Field_5", "REDEFINE", pnd_Twrparti_Curr_Rec_Sd);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id", 
            "#KEY-TWRPARTI-TAX-ID", FieldType.STRING, 10);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status", 
            "#KEY-TWRPARTI-STATUS", FieldType.STRING, 1);
        pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte = pnd_Twrparti_Curr_Rec_Sd__R_Field_5.newFieldInGroup("pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte", 
            "#KEY-TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 8);
        pnd_Key_Rt_Super2 = localVariables.newFieldInRecord("pnd_Key_Rt_Super2", "#KEY-RT-SUPER2", FieldType.STRING, 66);

        pnd_Key_Rt_Super2__R_Field_6 = localVariables.newGroupInRecord("pnd_Key_Rt_Super2__R_Field_6", "REDEFINE", pnd_Key_Rt_Super2);
        pnd_Key_Rt_Super2_Pnd_Key_Rt_A_I_Ind = pnd_Key_Rt_Super2__R_Field_6.newFieldInGroup("pnd_Key_Rt_Super2_Pnd_Key_Rt_A_I_Ind", "#KEY-RT-A-I-IND", 
            FieldType.STRING, 1);
        pnd_Key_Rt_Super2_Pnd_Key_Rt_Table_Id = pnd_Key_Rt_Super2__R_Field_6.newFieldInGroup("pnd_Key_Rt_Super2_Pnd_Key_Rt_Table_Id", "#KEY-RT-TABLE-ID", 
            FieldType.STRING, 5);
        pnd_Key_Rt_Super2_Pnd_Key_Rt_Long_Key = pnd_Key_Rt_Super2__R_Field_6.newFieldInGroup("pnd_Key_Rt_Super2_Pnd_Key_Rt_Long_Key", "#KEY-RT-LONG-KEY", 
            FieldType.STRING, 40);
        pnd_Key_Rt_Super2_Pnd_Key_Rt_Short_Key = pnd_Key_Rt_Super2__R_Field_6.newFieldInGroup("pnd_Key_Rt_Super2_Pnd_Key_Rt_Short_Key", "#KEY-RT-SHORT-KEY", 
            FieldType.STRING, 20);
        pnd_Rt_Short_Key = localVariables.newFieldInRecord("pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 25);

        pnd_Rt_Short_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Rt_Short_Key__R_Field_7", "REDEFINE", pnd_Rt_Short_Key);
        pnd_Rt_Short_Key_Pnd_Rt_Pin = pnd_Rt_Short_Key__R_Field_7.newFieldInGroup("pnd_Rt_Short_Key_Pnd_Rt_Pin", "#RT-PIN", FieldType.NUMERIC, 12);
        pnd_Rt_Short_Key_Pnd_Rt_Contract_Number = pnd_Rt_Short_Key__R_Field_7.newFieldInGroup("pnd_Rt_Short_Key_Pnd_Rt_Contract_Number", "#RT-CONTRACT-NUMBER", 
            FieldType.STRING, 8);
        pnd_Rt_Short_Key_Pnd_Rt_Payee_Code = pnd_Rt_Short_Key__R_Field_7.newFieldInGroup("pnd_Rt_Short_Key_Pnd_Rt_Payee_Code", "#RT-PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_Rt_Short_Key_Pnd_Rt_Filler1 = pnd_Rt_Short_Key__R_Field_7.newFieldInGroup("pnd_Rt_Short_Key_Pnd_Rt_Filler1", "#RT-FILLER1", FieldType.STRING, 
            3);
        pnd_Rt_Long_Key = localVariables.newFieldInRecord("pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 40);

        pnd_Rt_Long_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Rt_Long_Key__R_Field_8", "REDEFINE", pnd_Rt_Long_Key);
        pnd_Rt_Long_Key_Pnd_Rtl_Ssn = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Ssn", "#RTL-SSN", FieldType.STRING, 10);
        pnd_Rt_Long_Key_Pnd_Rtl_Contract_Number = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Contract_Number", "#RTL-CONTRACT-NUMBER", 
            FieldType.STRING, 8);
        pnd_Rt_Long_Key_Pnd_Rtl_Payee_Code = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Payee_Code", "#RTL-PAYEE-CODE", FieldType.STRING, 
            2);
        pnd_Rt_Long_Key_Pnd_Rtl_Tax_Year = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Tax_Year", "#RTL-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Rt_Long_Key_Pnd_Rtl_Tax_Id_Type = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Tax_Id_Type", "#RTL-TAX-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Rt_Long_Key_Pnd_Rtl_Filler2 = pnd_Rt_Long_Key__R_Field_8.newFieldInGroup("pnd_Rt_Long_Key_Pnd_Rtl_Filler2", "#RTL-FILLER2", FieldType.STRING, 
            15);
        pnd_Rtd1_Desc1 = localVariables.newFieldInRecord("pnd_Rtd1_Desc1", "#RTD1-DESC1", FieldType.STRING, 250);

        pnd_Rtd1_Desc1__R_Field_9 = localVariables.newGroupInRecord("pnd_Rtd1_Desc1__R_Field_9", "REDEFINE", pnd_Rtd1_Desc1);
        pnd_Rtd1_Desc1_Pnd_Rtd1_Last_Name = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_Last_Name", "#RTD1-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Rtd1_Desc1_Pnd_Rtd1_First_Name = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_First_Name", "#RTD1-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Rtd1_Desc1_Pnd_Rtd1_Middle_Name = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_Middle_Name", "#RTD1-MIDDLE-NAME", FieldType.STRING, 
            30);
        pnd_Rtd1_Desc1_Pnd_Rtd1_Part_Name = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_Part_Name", "#RTD1-PART-NAME", FieldType.STRING, 
            35);
        pnd_Rtd1_Desc1_Pnd_Rtd1_Dob = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_Dob", "#RTD1-DOB", FieldType.STRING, 8);
        pnd_Rtd1_Desc1_Pnd_Rtd1_Dod = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Rtd1_Dod", "#RTD1-DOD", FieldType.STRING, 8);
        pnd_Rtd1_Desc1_Pnd_Filler3 = pnd_Rtd1_Desc1__R_Field_9.newFieldInGroup("pnd_Rtd1_Desc1_Pnd_Filler3", "#FILLER3", FieldType.STRING, 109);
        pnd_Rtd2_Desc2 = localVariables.newFieldInRecord("pnd_Rtd2_Desc2", "#RTD2-DESC2", FieldType.STRING, 250);

        pnd_Rtd2_Desc2__R_Field_10 = localVariables.newGroupInRecord("pnd_Rtd2_Desc2__R_Field_10", "REDEFINE", pnd_Rtd2_Desc2);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_1 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_1", "#RTD2-ADDR-LNE-1", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_2 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_2", "#RTD2-ADDR-LNE-2", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_3 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_3", "#RTD2-ADDR-LNE-3", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_4 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_4", "#RTD2-ADDR-LNE-4", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_5 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_5", "#RTD2-ADDR-LNE-5", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_6 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Lne_6", "#RTD2-ADDR-LNE-6", FieldType.STRING, 
            35);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Postal_Data = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Postal_Data", "#RTD2-ADDR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Addrss_Type_Cde = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Addrss_Type_Cde", "#RTD2-ADDRSS-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Rtd2_Desc2_Pnd_Rtd2_Geo_Code = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Rtd2_Geo_Code", "#RTD2-GEO-CODE", FieldType.STRING, 
            2);
        pnd_Rtd2_Desc2_Pnd_Filler4 = pnd_Rtd2_Desc2__R_Field_10.newFieldInGroup("pnd_Rtd2_Desc2_Pnd_Filler4", "#FILLER4", FieldType.STRING, 5);
        pnd_No_Pin = localVariables.newFieldInRecord("pnd_No_Pin", "#NO-PIN", FieldType.NUMERIC, 12);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Irs_Names = localVariables.newFieldArrayInRecord("pnd_Irs_Names", "#IRS-NAMES", FieldType.STRING, 35, new DbsArrayController(1, 10));
        pnd_Country_Name = localVariables.newFieldInRecord("pnd_Country_Name", "#COUNTRY-NAME", FieldType.STRING, 35);
        pnd_Date_Yr = localVariables.newFieldInRecord("pnd_Date_Yr", "#DATE-YR", FieldType.STRING, 4);

        pnd_Date_Yr__R_Field_11 = localVariables.newGroupInRecord("pnd_Date_Yr__R_Field_11", "REDEFINE", pnd_Date_Yr);
        pnd_Date_Yr_Pnd_Date_Yr_N = pnd_Date_Yr__R_Field_11.newFieldInGroup("pnd_Date_Yr_Pnd_Date_Yr_N", "#DATE-YR-N", FieldType.NUMERIC, 4);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 30);
        pnd_Country = localVariables.newFieldInRecord("pnd_Country", "#COUNTRY", FieldType.STRING, 2);
        pnd_Aliases = localVariables.newFieldInRecord("pnd_Aliases", "#ALIASES", FieldType.STRING, 197);
        pnd_Fill = localVariables.newFieldInRecord("pnd_Fill", "#FILL", FieldType.STRING, 1);
        pnd_Country_Nf_Ind = localVariables.newFieldInRecord("pnd_Country_Nf_Ind", "#COUNTRY-NF-IND", FieldType.STRING, 1);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 3);
        pnd_Country_Not_Found = localVariables.newFieldInRecord("pnd_Country_Not_Found", "#COUNTRY-NOT-FOUND", FieldType.NUMERIC, 7);
        pnd_Addr_Found = localVariables.newFieldInRecord("pnd_Addr_Found", "#ADDR-FOUND", FieldType.NUMERIC, 7);
        pnd_Addr_Not_Found = localVariables.newFieldInRecord("pnd_Addr_Not_Found", "#ADDR-NOT-FOUND", FieldType.NUMERIC, 7);
        pnd_Rt_Rec_Exist = localVariables.newFieldInRecord("pnd_Rt_Rec_Exist", "#RT-REC-EXIST", FieldType.BOOLEAN, 1);
        pnd_Cn_Su = localVariables.newFieldInRecord("pnd_Cn_Su", "#CN-SU", FieldType.STRING, 7);

        pnd_Cn_Su__R_Field_12 = localVariables.newGroupInRecord("pnd_Cn_Su__R_Field_12", "REDEFINE", pnd_Cn_Su);
        pnd_Cn_Su__Filler1 = pnd_Cn_Su__R_Field_12.newFieldInGroup("pnd_Cn_Su__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Cn_Su_Pnd_Cn_Yr = pnd_Cn_Su__R_Field_12.newFieldInGroup("pnd_Cn_Su_Pnd_Cn_Yr", "#CN-YR", FieldType.NUMERIC, 4);
        pnd_Cn_Su_Pnd_Cn_Code = pnd_Cn_Su__R_Field_12.newFieldInGroup("pnd_Cn_Su_Pnd_Cn_Code", "#CN-CODE", FieldType.STRING, 2);
        pnd_Timn_N = localVariables.newFieldInRecord("pnd_Timn_N", "#TIMN-N", FieldType.NUMERIC, 7);

        pnd_Timn_N__R_Field_13 = localVariables.newGroupInRecord("pnd_Timn_N__R_Field_13", "REDEFINE", pnd_Timn_N);
        pnd_Timn_N_Pnd_Timn_A = pnd_Timn_N__R_Field_13.newFieldInGroup("pnd_Timn_N_Pnd_Timn_A", "#TIMN-A", FieldType.STRING, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_twrparti_Upd.reset();
        vw_ref_Read.reset();
        vw_ref_Upd.reset();
        vw_ref_Stor.reset();

        ldaTwrl9501.initializeValues();
        ldaTwrliso.initializeValues();
        ldaTwrl9802.initializeValues();

        localVariables.reset();
        pnd_Work_Areas_Pnd_Et_Limit.setInitialValue(150);
        pnd_Cn_Su.setInitialValue("3");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp7200() throws Exception
    {
        super("Twrp7200");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 23 LS = 133 ZP = ON;//Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'TAX REPORTING AND WITHHOLDING SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Foreign Address Country Code Correction' 120T 'REPORT: RPT1' //
        //* *-------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 48T 'TAX REPORTING AND WITHHOLDING SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Foreign Country Code Error' 120T 'REPORT: RPT1' //
        //* *-------------------
        //*  LOAD COUNTY CODE ALIASES
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #COUNTRY #FILL #ALIASES
        while (condition(getWorkFiles().read(2, pnd_Country, pnd_Fill, pnd_Aliases)))
        {
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            ldaTwrliso.getPnd_Iso_Countries().getValue(pnd_I).setValue(pnd_Country);                                                                                      //Natural: ASSIGN #ISO-COUNTRIES ( #I ) := #COUNTRY
            pnd_Aliases.separate(EnumSet.of(SeparateOption.Ignore,SeparateOption.WithAnyDelimiters), "|", ldaTwrliso.getPnd_Country_Names_Pnd_Country_Alias().getValue(pnd_I, //Natural: SEPARATE #ALIASES LEFT JUSTIFIED INTO #COUNTRY-ALIAS ( #I,* ) IGNORE WITH DELIMITER '|'
                "*"));
            getReports().write(0, "ALIAS COUNTRY:",ldaTwrliso.getPnd_Iso_Countries().getValue(pnd_I));                                                                    //Natural: WRITE 'ALIAS COUNTRY:' #ISO-COUNTRIES ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "ALIAS NAMES:",ldaTwrliso.getPnd_Country_Names_Pnd_Country_Alias().getValue(pnd_I,"*"));                                                //Natural: WRITE 'ALIAS NAMES:' #COUNTRY-ALIAS ( #I,* )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "--------------------------------------------------------------");                                                                      //Natural: WRITE '--------------------------------------------------------------'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        RDW:                                                                                                                                                              //Natural: READ WORK FILE 1 #MDM-FILE
        while (condition(getWorkFiles().read(1, pnd_Mdm_File)))
        {
            pnd_Work_Areas_Pnd_Read_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #READ-CNT
            //*  PIN-NBR EXPANSION START
            //*  DASRAH
            if (condition((((pnd_Mdm_File_Pnd_Pin_X.equals(" ") || pnd_Mdm_File_Pnd_Pin_X.equals("0000000")) && pnd_Mdm_File_Pnd_Names.equals(" ")) ||                    //Natural: IF ( ( #PIN-X = ' ' OR = '0000000' ) AND #NAMES = ' ' ) OR ( ( #PIN-X = ' ' OR = '000000000000' ) AND #NAMES = ' ' ) AND #ADDRESS-LINES = ' '
                (((pnd_Mdm_File_Pnd_Pin_X.equals(" ") || pnd_Mdm_File_Pnd_Pin_X.equals("000000000000")) && pnd_Mdm_File_Pnd_Names.equals(" ")) && pnd_Mdm_File_Pnd_Address_Lines.equals(" ")))))
            {
                //*  PIN-NBR EXPANSION END
                pnd_Work_Areas_Pnd_Reject_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #REJECT-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Areas_Pnd_Wk_Names.reset();                                                                                                                          //Natural: RESET #WK-NAMES #WK-ADDRESS #TWRPARTI-CURR-REC-SD
            pnd_Work_Areas_Pnd_Wk_Address.reset();
            pnd_Twrparti_Curr_Rec_Sd.reset();
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Tax_Id.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr);                                                              //Natural: ASSIGN #KEY-TWRPARTI-TAX-ID := #TWRPYMNT-TAX-ID-NBR
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Status.setValue(" ");                                                                                               //Natural: ASSIGN #KEY-TWRPARTI-STATUS := ' '
            pnd_Twrparti_Curr_Rec_Sd_Pnd_Key_Twrparti_Part_Eff_End_Dte.setValue("99999999");                                                                              //Natural: ASSIGN #KEY-TWRPARTI-PART-EFF-END-DTE := '99999999'
            if (condition(pnd_Mdm_File_Pnd_Addrss_Type_Cde.equals("F")))                                                                                                  //Natural: IF #ADDRSS-TYPE-CDE = 'F'
            {
                                                                                                                                                                          //Natural: PERFORM FOREIGN-ADDRESS
                sub_Foreign_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RDW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RDW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl9501.getVw_twrparti_Part().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) TWRPARTI-PART WITH TWRPARTI-CURR-REC-SD EQ #TWRPARTI-CURR-REC-SD
            (
            "RD2",
            new Wc[] { new Wc("TWRPARTI_CURR_REC_SD", ">=", pnd_Twrparti_Curr_Rec_Sd, WcType.BY) },
            new Oc[] { new Oc("TWRPARTI_CURR_REC_SD", "ASC") },
            1
            );
            RD2:
            while (condition(ldaTwrl9501.getVw_twrparti_Part().readNextRow("RD2")))
            {
                pnd_Work_Areas_Pnd_Valid_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #VALID-CNT
                pnd_Work_Areas_Pnd_Wk_Isn.reset();                                                                                                                        //Natural: RESET #WK-ISN #WK-NAMES #WK-ADDRESS
                pnd_Work_Areas_Pnd_Wk_Names.reset();
                pnd_Work_Areas_Pnd_Wk_Address.reset();
                if (condition(ldaTwrl9501.getTwrparti_Part_Twrparti_Tax_Id().notEquals(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr)))                                            //Natural: IF TWRPARTI-PART.TWRPARTI-TAX-ID NE #TWRPYMNT-TAX-ID-NBR
                {
                    pnd_Rt_Short_Key.reset();                                                                                                                             //Natural: RESET #RT-SHORT-KEY #RT-LONG-KEY #RTD1-DESC1 #RTD2-DESC2
                    pnd_Rt_Long_Key.reset();
                    pnd_Rtd1_Desc1.reset();
                    pnd_Rtd2_Desc2.reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Areas_Pnd_Wk_Isn.setValue(ldaTwrl9501.getVw_twrparti_Part().getAstISN("RD2"));                                                               //Natural: ASSIGN #WK-ISN := *ISN ( RD2. )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Areas_Pnd_Wk_Names.setValue(pnd_Mdm_File_Pnd_Names);                                                                                             //Natural: ASSIGN #WK-NAMES := #NAMES
                pnd_Work_Areas_Pnd_Wk_Address.setValue(pnd_Mdm_File_Pnd_Address_Lines);                                                                                   //Natural: ASSIGN #WK-ADDRESS := #ADDRESS-LINES
                if (condition(pnd_Work_Areas_Pnd_Wk_Isn.greater(getZero())))                                                                                              //Natural: IF #WK-ISN GT 0
                {
                    pnd_Work_Areas_Pnd_Wk_Dob.setValue("00000000");                                                                                                       //Natural: ASSIGN #WK-DOB := '00000000'
                    pnd_Work_Areas_Pnd_Wk_Dod.setValue("00000000");                                                                                                       //Natural: ASSIGN #WK-DOD := '00000000'
                    if (condition(pnd_Mdm_File_Pnd_Dob.equals("00000000") || pnd_Mdm_File_Pnd_Dob.equals(" ")))                                                           //Natural: IF #DOB EQ '00000000' OR = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Areas_Pnd_Wk_Dob.setValue(pnd_Mdm_File_Pnd_Dob);                                                                                         //Natural: ASSIGN #WK-DOB := #DOB
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mdm_File_Pnd_Dod.equals("00000000") || pnd_Mdm_File_Pnd_Dod.equals(" ")))                                                           //Natural: IF #DOD EQ '00000000' OR = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Areas_Pnd_Wk_Dod.setValue(pnd_Mdm_File_Pnd_Dod);                                                                                         //Natural: ASSIGN #WK-DOD := #DOD
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-PART-FILE
                    sub_Update_Part_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*       END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Key_Rt_Super2_Pnd_Key_Rt_A_I_Ind.setValue("A");                                                                                                   //Natural: ASSIGN #KEY-RT-A-I-IND := 'A'
                    pnd_Key_Rt_Super2_Pnd_Key_Rt_Table_Id.setValue("TPART");                                                                                              //Natural: ASSIGN #KEY-RT-TABLE-ID := 'TPART'
                    pnd_Key_Rt_Super2_Pnd_Key_Rt_Long_Key.reset();                                                                                                        //Natural: RESET #KEY-RT-LONG-KEY #KEY-RT-SHORT-KEY #RT-SHORT-KEY #RT-LONG-KEY #RTD1-DESC1 #RTD2-DESC2 #RT-REC-EXIST
                    pnd_Key_Rt_Super2_Pnd_Key_Rt_Short_Key.reset();
                    pnd_Rt_Short_Key.reset();
                    pnd_Rt_Long_Key.reset();
                    pnd_Rtd1_Desc1.reset();
                    pnd_Rtd2_Desc2.reset();
                    pnd_Rt_Rec_Exist.reset();
                    setValueToSubstring(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr,pnd_Key_Rt_Super2_Pnd_Key_Rt_Long_Key,1,10);                                                 //Natural: MOVE #TWRPYMNT-TAX-ID-NBR TO SUBSTR ( #KEY-RT-LONG-KEY,1,10 )
                    vw_ref_Read.startDatabaseRead                                                                                                                         //Natural: READ ( 1 ) REF-READ WITH RT-SUPER2 = #KEY-RT-SUPER2
                    (
                    "RT1",
                    new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Key_Rt_Super2, WcType.BY) },
                    new Oc[] { new Oc("RT_SUPER2", "ASC") },
                    1
                    );
                    RT1:
                    while (condition(vw_ref_Read.readNextRow("RT1")))
                    {
                        pnd_Rt_Short_Key.setValue(ref_Read_Rt_Short_Key);                                                                                                 //Natural: ASSIGN #RT-SHORT-KEY := REF-READ.RT-SHORT-KEY
                        pnd_Rt_Long_Key.setValue(ref_Read_Rt_Long_Key);                                                                                                   //Natural: ASSIGN #RT-LONG-KEY := REF-READ.RT-LONG-KEY
                        if (condition(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr.equals(pnd_Rt_Long_Key_Pnd_Rtl_Ssn)))                                                          //Natural: IF #TWRPYMNT-TAX-ID-NBR = #RTL-SSN
                        {
                            pnd_Rt_Rec_Exist.setValue(true);                                                                                                              //Natural: ASSIGN #RT-REC-EXIST := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Rt_Short_Key_Pnd_Rt_Pin.setValue(pnd_Mdm_File_Pnd_Pin);                                                                                       //Natural: ASSIGN #RT-PIN := #PIN
                        pnd_Rt_Short_Key_Pnd_Rt_Contract_Number.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr);                                                         //Natural: ASSIGN #RT-CONTRACT-NUMBER := #TWRPYMNT-CONTRACT-NBR
                        pnd_Rt_Short_Key_Pnd_Rt_Payee_Code.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Payee_Cde);                                                                 //Natural: ASSIGN #RT-PAYEE-CODE := #TWRPYMNT-PAYEE-CDE
                        pnd_Rt_Long_Key_Pnd_Rtl_Ssn.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr);                                                                       //Natural: ASSIGN #RTL-SSN := #TWRPYMNT-TAX-ID-NBR
                        pnd_Rt_Long_Key_Pnd_Rtl_Contract_Number.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr);                                                         //Natural: ASSIGN #RTL-CONTRACT-NUMBER := #TWRPYMNT-CONTRACT-NBR
                        pnd_Rt_Long_Key_Pnd_Rtl_Payee_Code.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Payee_Cde);                                                                 //Natural: ASSIGN #RTL-PAYEE-CODE := #TWRPYMNT-PAYEE-CDE
                        pnd_Rt_Long_Key_Pnd_Rtl_Tax_Year.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Year);                                                                    //Natural: ASSIGN #RTL-TAX-YEAR := #TWRPYMNT-TAX-YEAR
                        pnd_Rt_Long_Key_Pnd_Rtl_Tax_Id_Type.setValue(pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Type);                                                              //Natural: ASSIGN #RTL-TAX-ID-TYPE := #TWRPYMNT-TAX-ID-TYPE
                        setValueToSubstring(pnd_Mdm_File_Pnd_Names,pnd_Rtd1_Desc1,1,90);                                                                                  //Natural: MOVE #NAMES TO SUBSTR ( #RTD1-DESC1,1,90 )
                        pnd_Rtd1_Desc1_Pnd_Rtd1_Part_Name.setValue(pnd_Mdm_File_Pnd_Part_Name);                                                                           //Natural: ASSIGN #RTD1-PART-NAME := #PART-NAME
                        pnd_Rtd1_Desc1_Pnd_Rtd1_Dob.setValue(pnd_Mdm_File_Pnd_Dob);                                                                                       //Natural: ASSIGN #RTD1-DOB := #DOB
                        pnd_Rtd1_Desc1_Pnd_Rtd1_Dod.setValue(pnd_Mdm_File_Pnd_Dod);                                                                                       //Natural: ASSIGN #RTD1-DOD := #DOD
                        setValueToSubstring(pnd_Mdm_File_Pnd_Address_Lines,pnd_Rtd2_Desc2,1,210);                                                                         //Natural: MOVE #ADDRESS-LINES TO SUBSTR ( #RTD2-DESC2,1,210 )
                        pnd_Rtd2_Desc2_Pnd_Rtd2_Addr_Postal_Data.setValue(pnd_Mdm_File_Pnd_Addr_Postal_Data);                                                             //Natural: ASSIGN #RTD2-ADDR-POSTAL-DATA := #ADDR-POSTAL-DATA
                        pnd_Rtd2_Desc2_Pnd_Rtd2_Addrss_Type_Cde.setValue(pnd_Mdm_File_Pnd_Addrss_Type_Cde);                                                               //Natural: ASSIGN #RTD2-ADDRSS-TYPE-CDE := #ADDRSS-TYPE-CDE
                        pnd_Rtd2_Desc2_Pnd_Rtd2_Geo_Code.setValue(pnd_Mdm_File_Pnd_Geo);                                                                                  //Natural: ASSIGN #RTD2-GEO-CODE := #GEO
                        pnd_Rtd1_Desc1_Pnd_Filler3.reset();                                                                                                               //Natural: RESET #FILLER3
                        if (condition(pnd_Rt_Rec_Exist.getBoolean()))                                                                                                     //Natural: IF #RT-REC-EXIST
                        {
                            pnd_Work_Areas_Pnd_Wk_Rt_Isn.setValue(vw_ref_Read.getAstISN("RT1"));                                                                          //Natural: ASSIGN #WK-RT-ISN := *ISN ( RT1. )
                                                                                                                                                                          //Natural: PERFORM UPDATE-REC-REF-TABLE
                            sub_Update_Rec_Ref_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RT1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RT1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM STORE-REC-REF-TABLE
                            sub_Store_Rec_Ref_Table();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RT1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RT1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RDW"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RDW"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Areas_Pnd_Et_Cnt.greaterOrEqual(pnd_Work_Areas_Pnd_Et_Limit)))                                                                         //Natural: IF #ET-CNT GE #ET-LIMIT
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Work_Areas_Pnd_Et_Cnt.setValue(0);                                                                                                                    //Natural: ASSIGN #ET-CNT := 0
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RDW_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ",new TabSetting(45),pnd_Work_Areas_Pnd_Read_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Valid Records for Processing",pnd_Work_Areas_Pnd_Valid_Cnt,  //Natural: WRITE 'NUMBER OF RECORDS READ' 45T #READ-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Valid Records for Processing' #VALID-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Number of Records Added' 45T #ADD-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Number of Updated records in Ref Table' 45T #UPD-REF-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Number of Updated records in Part Table' 45T #UPD-PART-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Number of Bypass Records' 45T #BYPASS-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Number of Rejected Records' 45T #REJECT-CNT ( EM = ZZZ,ZZZ,ZZZ,Z99 ) / 'Foreign address correct' 45T #ADDR-FOUND ( EM = ZZZ,ZZZ,ZZZ,Z99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Number of Records Added",new TabSetting(45),pnd_Work_Areas_Pnd_Add_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Number of Updated records in Ref Table",new 
            TabSetting(45),pnd_Work_Areas_Pnd_Upd_Ref_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Number of Updated records in Part Table",new 
            TabSetting(45),pnd_Work_Areas_Pnd_Upd_Part_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Number of Bypass Records",new TabSetting(45),pnd_Work_Areas_Pnd_Bypass_Cnt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Number of Rejected Records",new TabSetting(45),pnd_Work_Areas_Pnd_Reject_Cnt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZZ,Z99"),NEWLINE,"Foreign address correct",new TabSetting(45),pnd_Addr_Found, new ReportEditMask ("Z,ZZZ,Z99"));
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,"Country not found",new TabSetting(45),pnd_Country_Not_Found, new ReportEditMask ("Z,ZZZ,Z99"));                                    //Natural: WRITE ( 2 ) / 'Country not found' 45T #COUNTRY-NOT-FOUND ( EM = ZZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"Foreign address corrected",new TabSetting(45),pnd_Addr_Not_Found, new ReportEditMask ("Z,ZZZ,Z99"));                               //Natural: WRITE ( 1 ) / 'Foreign address corrected' 45T #ADDR-NOT-FOUND ( EM = ZZZ,ZZZ,ZZZ,Z99 )
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PART-FILE
        //*  **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-REC-REF-TABLE
        //*  **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-REC-REF-TABLE
        //*  **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FOREIGN-ADDRESS
        //*  #CN-YR  := 2016
        //*    #ADDRESS-LINE(#K+1) := TIRCNTL-CNTRY-FULL-NAME
        //*      '=' #GEO '=' TIRCNTL-CNTRY-FULL-NAME /
    }
    private void sub_Update_Part_File() throws Exception                                                                                                                  //Natural: UPDATE-PART-FILE
    {
        if (BLNatReinput.isReinput()) return;

        GT1:                                                                                                                                                              //Natural: GET TWRPARTI-UPD #WK-ISN
        vw_twrparti_Upd.readByID(pnd_Work_Areas_Pnd_Wk_Isn.getLong(), "GT1");
        twrparti_Upd_Twrparti_Rlup_Last_Name.setValue(pnd_Mdm_File_Pnd_Last_Name);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-LAST-NAME := #LAST-NAME
        twrparti_Upd_Twrparti_Rlup_First_Name.setValue(pnd_Mdm_File_Pnd_First_Name);                                                                                      //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-FIRST-NAME := #FIRST-NAME
        twrparti_Upd_Twrparti_Rlup_Middle_Name.setValue(pnd_Mdm_File_Pnd_Middle_Name);                                                                                    //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-MIDDLE-NAME := #MIDDLE-NAME
        twrparti_Upd_Twrparti_Rlup_Part_Name.setValue(pnd_Mdm_File_Pnd_Part_Name);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-PART-NAME := #PART-NAME
        twrparti_Upd_Twrparti_Rlup_Addr_Ln1.setValue(pnd_Mdm_File_Pnd_Addr_Lne_1);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN1 := #ADDR-LNE-1
        twrparti_Upd_Twrparti_Rlup_Addr_Ln2.setValue(pnd_Mdm_File_Pnd_Addr_Lne_2);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN2 := #ADDR-LNE-2
        twrparti_Upd_Twrparti_Rlup_Addr_Ln3.setValue(pnd_Mdm_File_Pnd_Addr_Lne_3);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN3 := #ADDR-LNE-3
        twrparti_Upd_Twrparti_Rlup_Addr_Ln4.setValue(pnd_Mdm_File_Pnd_Addr_Lne_4);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN4 := #ADDR-LNE-4
        twrparti_Upd_Twrparti_Rlup_Addr_Ln5.setValue(pnd_Mdm_File_Pnd_Addr_Lne_5);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN5 := #ADDR-LNE-5
        twrparti_Upd_Twrparti_Rlup_Addr_Ln6.setValue(pnd_Mdm_File_Pnd_Addr_Lne_6);                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-ADDR-LN6 := #ADDR-LNE-6
        twrparti_Upd_Twrparti_Rlup_Postal_Data.setValue(pnd_Mdm_File_Pnd_Addr_Postal_Data);                                                                               //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-POSTAL-DATA := #ADDR-POSTAL-DATA
        twrparti_Upd_Twrparti_Rlup_Type_Cde.setValue(pnd_Mdm_File_Pnd_Addrss_Type_Cde);                                                                                   //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-TYPE-CDE := #ADDRSS-TYPE-CDE
        twrparti_Upd_Twrparti_Rlup_Geo_Code.setValue(pnd_Mdm_File_Pnd_Geo);                                                                                               //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-GEO-CODE := #GEO
        twrparti_Upd_Twrparti_Rlup_Email_Pref.setValue(pnd_Mdm_File_Pnd_Email_Pref);                                                                                      //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-EMAIL-PREF := #EMAIL-PREF
        twrparti_Upd_Twrparti_Rlup_Email_Pref_Dte.setValue(pnd_Mdm_File_Pnd_Email_Pref_Dte);                                                                              //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-EMAIL-PREF-DTE := #EMAIL-PREF-DTE
        twrparti_Upd_Twrparti_Rlup_Email_Addr.setValue(pnd_Mdm_File_Pnd_Email_Addr);                                                                                      //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-EMAIL-ADDR := #EMAIL-ADDR
        twrparti_Upd_Twrparti_Rlup_Email_Addr_Dte.setValue(pnd_Mdm_File_Pnd_Email_Addr_Dte);                                                                              //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-EMAIL-ADDR-DTE := #EMAIL-ADDR-DTE
        if (condition(pnd_Work_Areas_Pnd_Wk_Dob.notEquals("00000000")))                                                                                                   //Natural: IF #WK-DOB NE '00000000'
        {
            twrparti_Upd_Twrparti_Rlup_Dob.setValue(pnd_Work_Areas_Pnd_Wk_Dob);                                                                                           //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-DOB := #WK-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Areas_Pnd_Wk_Dod.notEquals("00000000")))                                                                                                   //Natural: IF #WK-DOD NE '00000000'
        {
            twrparti_Upd_Twrparti_Rlup_Dod.setValue(pnd_Work_Areas_Pnd_Wk_Dod);                                                                                           //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-DOD := #WK-DOD
        }                                                                                                                                                                 //Natural: END-IF
        //*  DASRAH
        //*  DASRAH
        if (condition(pnd_Mdm_File_Pnd_Pin.greater(getZero())))                                                                                                           //Natural: IF #PIN GT 0
        {
            twrparti_Upd_Twrparti_Rlup_Pin.setValue(pnd_Mdm_File_Pnd_Pin_X);                                                                                              //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-RLUP-PIN := #PIN-X
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        //*  PIN-NBR EXPANSION START
        if (condition(twrparti_Upd_Twrparti_Pin.equals(" ") || twrparti_Upd_Twrparti_Pin.equals("0000000") || twrparti_Upd_Twrparti_Pin.equals(" ") ||                    //Natural: IF ( TWRPARTI-UPD.TWRPARTI-PIN = ' ' OR = '0000000' ) OR ( TWRPARTI-UPD.TWRPARTI-PIN = ' ' OR = '000000000000' )
            twrparti_Upd_Twrparti_Pin.equals("000000000000")))
        {
            //*  PIN-NBR EXPANSION END
            pnd_No_Pin.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #NO-PIN
            //*  DASRAH
            //*  DASRAH
            //*  DASRAH
            //*  DASRAH
            //*  DASRAH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Timn_N.setValue(Global.getTIMN());                                                                                                                            //Natural: ASSIGN #TIMN-N := *TIMN
        twrparti_Upd_Twrparti_Updt_User.setValue(Global.getINIT_USER());                                                                                                  //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-UPDT-USER := *INIT-USER
        twrparti_Upd_Twrparti_Updt_Dte.setValue(Global.getDATN());                                                                                                        //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-UPDT-DTE := *DATN
        twrparti_Upd_Twrparti_Updt_Time.setValue(pnd_Timn_N_Pnd_Timn_A);                                                                                                  //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-UPDT-TIME := #TIMN-A
        twrparti_Upd_Twrparti_Lu_Ts.setValue(Global.getTIMX());                                                                                                           //Natural: ASSIGN TWRPARTI-UPD.TWRPARTI-LU-TS := *TIMX
        vw_twrparti_Upd.updateDBRow("GT1");                                                                                                                               //Natural: UPDATE ( GT1. )
        pnd_Work_Areas_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
        pnd_Work_Areas_Pnd_Upd_Part_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #UPD-PART-CNT
        //*  ITE 'Update Participant, Seq.=' #READ-CNT 'TIN=' #TWRPYMNT-TAX-ID-NBR
        //*  UPDATE-PART-FILE
    }
    private void sub_Update_Rec_Ref_Table() throws Exception                                                                                                              //Natural: UPDATE-REC-REF-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        GT2:                                                                                                                                                              //Natural: GET REF-UPD #WK-RT-ISN
        vw_ref_Upd.readByID(pnd_Work_Areas_Pnd_Wk_Rt_Isn.getLong(), "GT2");
        ref_Upd_Rt_Short_Key.setValue(pnd_Rt_Short_Key);                                                                                                                  //Natural: ASSIGN REF-UPD.RT-SHORT-KEY := #RT-SHORT-KEY
        ref_Upd_Rt_Long_Key.setValue(pnd_Rt_Long_Key);                                                                                                                    //Natural: ASSIGN REF-UPD.RT-LONG-KEY := #RT-LONG-KEY
        ref_Upd_Rt_Desc1.setValue(pnd_Rtd1_Desc1);                                                                                                                        //Natural: ASSIGN REF-UPD.RT-DESC1 := #RTD1-DESC1
        ref_Upd_Rt_Desc2.setValue(pnd_Rtd2_Desc2);                                                                                                                        //Natural: ASSIGN REF-UPD.RT-DESC2 := #RTD2-DESC2
        ref_Upd_Rt_Eff_From_Ccyymmdd.setValue(Global.getDATN());                                                                                                          //Natural: ASSIGN REF-UPD.RT-EFF-FROM-CCYYMMDD := *DATN
        ref_Upd_Rt_Eff_To_Ccyymmdd.setValue(Global.getDATN());                                                                                                            //Natural: ASSIGN REF-UPD.RT-EFF-TO-CCYYMMDD := *DATN
        ref_Upd_Rt_Upd_Source.setValue(Global.getPROGRAM());                                                                                                              //Natural: ASSIGN REF-UPD.RT-UPD-SOURCE := *PROGRAM
        ref_Upd_Rt_Upd_User.setValue(Global.getINIT_USER());                                                                                                              //Natural: ASSIGN REF-UPD.RT-UPD-USER := *INIT-USER
        ref_Upd_Rt_Upd_Ccyymmdd.setValue(Global.getDATN());                                                                                                               //Natural: ASSIGN REF-UPD.RT-UPD-CCYYMMDD := *DATN
        ref_Upd_Rt_Upd_Timn.setValue(Global.getTIMN());                                                                                                                   //Natural: ASSIGN REF-UPD.RT-UPD-TIMN := *TIMN
        vw_ref_Upd.updateDBRow("GT2");                                                                                                                                    //Natural: UPDATE ( GT2. )
        pnd_Work_Areas_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
        pnd_Work_Areas_Pnd_Upd_Ref_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #UPD-REF-CNT
        //*  WRITE 'Update Ref Table, Seq.=' #READ-CNT 'TIN=' #TWRPYMNT-TAX-ID-NBR
        //*  UPDATE-REC-REF-TABLE
    }
    private void sub_Store_Rec_Ref_Table() throws Exception                                                                                                               //Natural: STORE-REC-REF-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        ref_Stor_Rt_A_I_Ind.setValue("A");                                                                                                                                //Natural: ASSIGN REF-STOR.RT-A-I-IND := 'A'
        ref_Stor_Rt_Table_Id.setValue("TPART");                                                                                                                           //Natural: ASSIGN REF-STOR.RT-TABLE-ID := 'TPART'
        ref_Stor_Rt_Eff_From_Ccyymmdd.setValue(Global.getDATN());                                                                                                         //Natural: ASSIGN REF-STOR.RT-EFF-FROM-CCYYMMDD := *DATN
        ref_Stor_Rt_Eff_To_Ccyymmdd.setValue(Global.getDATN());                                                                                                           //Natural: ASSIGN REF-STOR.RT-EFF-TO-CCYYMMDD := *DATN
        ref_Stor_Rt_Upd_Source.setValue(Global.getPROGRAM());                                                                                                             //Natural: ASSIGN REF-STOR.RT-UPD-SOURCE := *PROGRAM
        ref_Stor_Rt_Upd_User.setValue(Global.getINIT_USER());                                                                                                             //Natural: ASSIGN REF-STOR.RT-UPD-USER := *INIT-USER
        ref_Stor_Rt_Upd_Ccyymmdd.setValue(Global.getDATN());                                                                                                              //Natural: ASSIGN REF-STOR.RT-UPD-CCYYMMDD := *DATN
        ref_Stor_Rt_Upd_Timn.setValue(Global.getTIMN());                                                                                                                  //Natural: ASSIGN REF-STOR.RT-UPD-TIMN := *TIMN
        ref_Stor_Rt_Short_Key.setValue(pnd_Rt_Short_Key);                                                                                                                 //Natural: ASSIGN REF-STOR.RT-SHORT-KEY := #RT-SHORT-KEY
        ref_Stor_Rt_Long_Key.setValue(pnd_Rt_Long_Key);                                                                                                                   //Natural: ASSIGN REF-STOR.RT-LONG-KEY := #RT-LONG-KEY
        ref_Stor_Rt_Desc1.setValue(pnd_Rtd1_Desc1);                                                                                                                       //Natural: ASSIGN REF-STOR.RT-DESC1 := #RTD1-DESC1
        ref_Stor_Rt_Desc2.setValue(pnd_Rtd2_Desc2);                                                                                                                       //Natural: ASSIGN REF-STOR.RT-DESC2 := #RTD2-DESC2
        vw_ref_Stor.insertDBRow();                                                                                                                                        //Natural: STORE REF-STOR
        pnd_Work_Areas_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
        pnd_Work_Areas_Pnd_Add_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ADD-CNT
        //*  ITE 'Store Ref. Table, Seq.=' #READ-CNT 'TIN=' #TWRPYMNT-TAX-ID-NBR
        //*  STORE-REC-REF-TABLE
    }
    private void sub_Foreign_Address() throws Exception                                                                                                                   //Natural: FOREIGN-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet578 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #GEO;//Natural: VALUE 'GE'
        if (condition((pnd_Mdm_File_Pnd_Geo.equals("GE"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("GM");                                                                                                                          //Natural: ASSIGN #GEO := 'GM'
        }                                                                                                                                                                 //Natural: VALUE 'CZ'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("CZ"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("EZ");                                                                                                                          //Natural: ASSIGN #GEO := 'EZ'
        }                                                                                                                                                                 //Natural: VALUE 'GC'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("GC"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("GM");                                                                                                                          //Natural: ASSIGN #GEO := 'GM'
        }                                                                                                                                                                 //Natural: VALUE 'FA'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("FA"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("FK");                                                                                                                          //Natural: ASSIGN #GEO := 'FK'
        }                                                                                                                                                                 //Natural: VALUE 'NA'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("NA"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("NT");                                                                                                                          //Natural: ASSIGN #GEO := 'NT'
        }                                                                                                                                                                 //Natural: VALUE 'YE'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("YE"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("YM");                                                                                                                          //Natural: ASSIGN #GEO := 'YM'
        }                                                                                                                                                                 //Natural: VALUE 'YS'
        else if (condition((pnd_Mdm_File_Pnd_Geo.equals("YS"))))
        {
            decideConditionsMet578++;
            pnd_Mdm_File_Pnd_Geo.setValue("YM");                                                                                                                          //Natural: ASSIGN #GEO := 'YM'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(pnd_Mdm_File_Pnd_Address_Line.getValue(pnd_I).equals(" ")))                                                                                     //Natural: IF #ADDRESS-LINE ( #I ) = ' '
            {
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_I.subtract(1));                                                                                    //Natural: ASSIGN #K := #I - 1
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(ldaTwrliso.getPnd_Iso_Countries().getValue("*")), new ExamineSearch(pnd_Mdm_File_Pnd_Geo), new ExamineGivingIndex(pnd_Index));  //Natural: EXAMINE #ISO-COUNTRIES ( * ) FOR #GEO GIVING INDEX #INDEX
        if (condition(pnd_Index.equals(getZero())))                                                                                                                       //Natural: IF #INDEX = 0
        {
            pnd_Date.setValue(Global.getDATX());                                                                                                                          //Natural: MOVE *DATX TO #DATE
            pnd_Date_Yr.setValueEdited(pnd_Date,new ReportEditMask("YYYY"));                                                                                              //Natural: MOVE EDITED #DATE ( EM = YYYY ) TO #DATE-YR
            pnd_Cn_Su_Pnd_Cn_Yr.setValue(pnd_Date_Yr_Pnd_Date_Yr_N);                                                                                                      //Natural: ASSIGN #CN-YR := #DATE-YR-N
            pnd_Cn_Su_Pnd_Cn_Code.setValue(pnd_Mdm_File_Pnd_Geo);                                                                                                         //Natural: ASSIGN #CN-CODE := #GEO
            pnd_Country_Nf_Ind.reset();                                                                                                                                   //Natural: RESET #COUNTRY-NF-IND #IRS-NAMES ( * ) #I #COUNTRY-NAME
            pnd_Irs_Names.getValue("*").reset();
            pnd_I.reset();
            pnd_Country_Name.reset();
            ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().startDatabaseRead                                                                                        //Natural: READ TIRCNTL-COUNTRY-CODE-TBL-VIEW-VI BY TIRCNTL-NBR-YEAR-ALPHA-CDE = #CN-SU
            (
            "READ02",
            new Wc[] { new Wc("TIRCNTL_NBR_YEAR_ALPHA_CDE", ">=", pnd_Cn_Su, WcType.BY) },
            new Oc[] { new Oc("TIRCNTL_NBR_YEAR_ALPHA_CDE", "ASC") }
            );
            READ02:
            while (condition(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().readNextRow("READ02")))
            {
                if (condition(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tbl_Nbr().equals(3) && ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tax_Year().equals(pnd_Cn_Su_Pnd_Cn_Yr)  //Natural: IF TIRCNTL-TBL-NBR = 3 AND TIRCNTL-TAX-YEAR = #CN-YR AND TIRCNTL-CNTRY-ALPHA-CODE = #GEO
                    && ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Alpha_Code().equals(pnd_Mdm_File_Pnd_Geo)))
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    if (condition(pnd_I.equals(1)))                                                                                                                       //Natural: IF #I = 1
                    {
                        pnd_Country_Name.setValue(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Full_Name());                                             //Natural: ASSIGN #COUNTRY-NAME := TIRCNTL-CNTRY-FULL-NAME
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Irs_Names.getValue(pnd_I).setValue(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Full_Name());                                    //Natural: ASSIGN #IRS-NAMES ( #I ) := TIRCNTL-CNTRY-FULL-NAME
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Irs_Names.getValue("*").greater(" ")))                                                                                              //Natural: IF #IRS-NAMES ( * ) > ' '
                    {
                        //*        WRITE '=' #IRS-NAMES(*)
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mdm_File_Pnd_Geo.equals("  ")))                                                                                                     //Natural: IF #GEO = '  '
                    {
                        pnd_Error_Msg.setValue("Geo Code is spaces");                                                                                                     //Natural: ASSIGN #ERROR-MSG := 'Geo Code is spaces'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Error_Msg.setValue("Geo Code not on Country Code table");                                                                                     //Natural: ASSIGN #ERROR-MSG := 'Geo Code not on Country Code table'
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(2, "Geo Code",                                                                                                                   //Natural: DISPLAY ( 2 ) 'Geo Code' #GEO 'SSN' #TWRPYMNT-TAX-ID-NBR 'Contract' #TWRPYMNT-CONTRACT-NBR 'Error Message' #ERROR-MSG
                    		pnd_Mdm_File_Pnd_Geo,"SSN",
                    		pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr,"Contract",
                    		pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr,"Error Message",
                    		pnd_Error_Msg);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Country_Nf_Ind.setValue("Y");                                                                                                                     //Natural: ASSIGN #COUNTRY-NF-IND := 'Y'
                    getReports().write(2, "=",pnd_Mdm_File_Pnd_Address_Line.getValue(pnd_K));                                                                             //Natural: WRITE ( 2 ) '=' #ADDRESS-LINE ( #K )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pnd_Country_Nf_Ind.equals("Y")))                                                                                                                //Natural: IF #COUNTRY-NF-IND = 'Y'
            {
                pnd_Country_Not_Found.nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNTRY-NOT-FOUND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count.reset();                                                                                                                                            //Natural: RESET #COUNT
            if (condition(pnd_Index.greater(getZero())))                                                                                                                  //Natural: IF #INDEX > 0
            {
                //*  WRITE 'ALIAS' '=' #ADDRESS-LINE(#K) '=' #COUNTRY-NAME(#INDEX,*)
                //*     EXAMINE FULL #ADDRESS-LINE(#K) FOR
                //*       #COUNTRY-ALIAS(#INDEX,*) GIVING NUMBER IN #COUNT
                if (condition(pnd_Mdm_File_Pnd_Address_Line.getValue(pnd_K).equals(ldaTwrliso.getPnd_Country_Names_Pnd_Country_Alias().getValue(pnd_Index,                //Natural: IF #ADDRESS-LINE ( #K ) = #COUNTRY-ALIAS ( #INDEX,* )
                    "*"))))
                {
                    pnd_Count.setValue(1);                                                                                                                                //Natural: ASSIGN #COUNT := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  WRITE 'ON TABLE' #ADDRESS-LINE(#K) '=' TIRCNTL-CNTRY-FULL-NAME
                //*     EXAMINE FULL #ADDRESS-LINE(#K) FOR
                //*       TIRCNTL-CNTRY-FULL-NAME GIVING NUMBER IN #COUNT
                //*    IF #ADDRESS-LINE(#K) = TIRCNTL-CNTRY-FULL-NAME
                if (condition(pnd_Mdm_File_Pnd_Address_Line.getValue(pnd_K).equals(pnd_Irs_Names.getValue("*"))))                                                         //Natural: IF #ADDRESS-LINE ( #K ) = #IRS-NAMES ( * )
                {
                    pnd_Count.setValue(1);                                                                                                                                //Natural: ASSIGN #COUNT := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Count.greater(getZero())))                                                                                                                  //Natural: IF #COUNT > 0
            {
                pnd_Addr_Found.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ADDR-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Addr_Not_Found.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ADDR-NOT-FOUND
                pnd_Mdm_File_Pnd_Address_Line.getValue(pnd_K.getDec().add(1)).setValue(pnd_Country_Name);                                                                 //Natural: ASSIGN #ADDRESS-LINE ( #K+1 ) := #COUNTRY-NAME
                getReports().display(1, "SSN",                                                                                                                            //Natural: DISPLAY ( 1 ) 'SSN' #TWRPYMNT-TAX-ID-NBR 'Contract' #TWRPYMNT-CONTRACT-NBR /
                		pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr,"Contract",
                		pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr,NEWLINE);
                if (Global.isEscape()) return;
                getReports().write(1, "=",pnd_Mdm_File_Pnd_Address_Line.getValue(1),NEWLINE,"=",pnd_Mdm_File_Pnd_Address_Line.getValue(2),NEWLINE,"=",                    //Natural: WRITE ( 1 ) '=' #ADDRESS-LINE ( 1 ) / '=' #ADDRESS-LINE ( 2 ) / '=' #ADDRESS-LINE ( 3 ) / '=' #ADDRESS-LINE ( 4 ) / '=' #ADDRESS-LINE ( 5 ) / '=' #ADDRESS-LINE ( 6 ) / '----------------------------------------------' //
                    pnd_Mdm_File_Pnd_Address_Line.getValue(3),NEWLINE,"=",pnd_Mdm_File_Pnd_Address_Line.getValue(4),NEWLINE,"=",pnd_Mdm_File_Pnd_Address_Line.getValue(5),
                    NEWLINE,"=",pnd_Mdm_File_Pnd_Address_Line.getValue(6),NEWLINE,"----------------------------------------------",NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOREIGN-ADDRESS
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=23 LS=133 ZP=ON");
        Global.format(2, "PS=23 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(48),"TAX REPORTING AND WITHHOLDING SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Foreign Address Country Code Correction",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(48),"TAX REPORTING AND WITHHOLDING SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Foreign Country Code Error",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, "Geo Code",
        		pnd_Mdm_File_Pnd_Geo,"SSN",
        		pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr,"Contract",
        		pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr,"Error Message",
        		pnd_Error_Msg);
        getReports().setDisplayColumns(1, "SSN",
        		pnd_Mdm_File_Pnd_Twrpymnt_Tax_Id_Nbr,"Contract",
        		pnd_Mdm_File_Pnd_Twrpymnt_Contract_Nbr,NEWLINE);
    }
}
