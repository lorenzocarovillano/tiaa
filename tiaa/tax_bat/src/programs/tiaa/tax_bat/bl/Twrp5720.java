/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:50 PM
**        * FROM NATURAL PROGRAM : Twrp5720
************************************************************
**        * FILE NAME            : Twrp5720.java
**        * CLASS NAME           : Twrp5720
**        * INSTANCE NAME        : Twrp5720
************************************************************
************************************************************************
* PROGRAM : TWRP5720
* SYSTEM  : TAXWARS
* FUNCTION: BASED ON YEAR FOUND IN PARM CARD, PROCESSES W2 CORRECTIONS
*           FOR TAX YEAR. PRODUCES REPORTS USED FOR MANUAL UPDATES TO
*           FILE TRANSMITTED TO CONVEY.
* HISTORY : 05/19/10  J.ROTHOLZ
*
* INPUT   : SORTED PAYMENTS EXTRACT PRODUCED BY PTW1071D (TWRP5706),
*           INCLUDES YTD W2 DATA FOR PREVIOUS TAX YEAR.  DATA IN
*           SEQUENCE BY COMPANY/RESIDENCY/TIN.
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5720 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwrafrm2 pdaTwrafrm2;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;

    private DbsGroup pnd_W2_Record;
    private DbsField pnd_W2_Record_Pnd_Tax_Year;

    private DbsGroup pnd_W2_Record__R_Field_1;
    private DbsField pnd_W2_Record_Pnd_Tax_Year_N;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Type;
    private DbsField pnd_W2_Record_Pnd_Tax_Id_Nbr;
    private DbsField pnd_W2_Record_Pnd_Pin_Nbr;
    private DbsField pnd_W2_Record_Pnd_Company_Code;
    private DbsField pnd_W2_Record_Pnd_Contract_Nbr;
    private DbsField pnd_W2_Record_Pnd_Payee_Cde;
    private DbsField pnd_W2_Record_Pnd_Residency_Type;
    private DbsField pnd_W2_Record_Pnd_Residency_Code;
    private DbsField pnd_W2_Record_Pnd_Gross_Amount;
    private DbsField pnd_W2_Record_Pnd_Federal_Tax;
    private DbsField pnd_W2_Record_Pnd_State_Tax;
    private DbsField pnd_W2_Record_Pnd_Name_And_Address;

    private DbsGroup pnd_W2_Record__R_Field_2;
    private DbsField pnd_W2_Record_Pnd_Name;
    private DbsField pnd_W2_Record_Pnd_Address_1;
    private DbsField pnd_W2_Record_Pnd_Address_2;
    private DbsField pnd_W2_Record_Pnd_Address_3;
    private DbsField pnd_W2_Record_Pnd_Address_4;
    private DbsField pnd_W2_Record_Pnd_Address_5;
    private DbsField pnd_W2_Record_Pnd_Employer_State_Id;

    private DbsGroup pnd_W2_Record__R_Field_3;
    private DbsField pnd_W2_Record_Pnd_State_Code;
    private DbsField pnd_W2_Record_Pnd_State_Id;
    private DbsField pnd_W2_Record_Pnd_Transaction_Dt;
    private DbsField pnd_W2_Record_Pnd_W2_Op_Ia_Ind;
    private DbsField pnd_W2_Record_Pnd_Record_Type;
    private DbsField pnd_W2_Record_Pnd_Sys_Dte_Time;
    private DbsField pnd_Old_Company;
    private DbsField pnd_Old_Tax_Id;
    private DbsField pnd_Old_State_Code;
    private DbsField pnd_Old_Name;
    private DbsField pnd_Old_Addr_1;
    private DbsField pnd_Old_Addr_2;
    private DbsField pnd_Old_Addr_3;
    private DbsField pnd_Old_Addr_4;
    private DbsField pnd_Old_Addr_5;
    private DbsField pnd_Init_Tax_Yr;

    private DbsGroup pnd_Init_Tax_Yr__R_Field_4;
    private DbsField pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N;
    private DbsField pnd_Corr_Seq;
    private DbsField pnd_Dt_Tm_Compare;
    private DbsField pnd_Tbl_Dt_Tm;

    private DbsGroup pnd_Tbl_Dt_Tm__R_Field_5;
    private DbsField pnd_Tbl_Dt_Tm_Pnd_Tbl_Dt;
    private DbsField pnd_Tbl_Dt_Tm_Pnd_Tbl_Tm;
    private DbsField pnd_Time_Convert_P;
    private DbsField pnd_Time_Convert_N;

    private DbsGroup pnd_Time_Convert_N__R_Field_6;
    private DbsField pnd_Time_Convert_N_Pnd_Fill4;
    private DbsField pnd_Time_Convert_N_Pnd_Time_7;
    private DbsField pnd_Time_Convert_N_Pnd_Fill2;
    private DbsField pnd_Rpt_Title;
    private DbsField pnd_P_Gross_Accum;
    private DbsField pnd_P_Fed_Accum;
    private DbsField pnd_P_St_Accum;
    private DbsField pnd_S_Gross_Accum;
    private DbsField pnd_S_Fed_Accum;
    private DbsField pnd_S_St_Accum;
    private DbsField pnd_S_Forms_Accum;
    private DbsField pnd_C_Gross_Accum;
    private DbsField pnd_C_Fed_Accum;
    private DbsField pnd_C_St_Accum;
    private DbsField pnd_C_Forms_Accum;
    private DbsField pnd_T_Gross_Accum;
    private DbsField pnd_T_Fed_Accum;
    private DbsField pnd_T_St_Accum;
    private DbsField pnd_T_Forms_Accum;
    private DbsField pnd_Co;
    private DbsField pnd_Co_Ndx;
    private DbsField pnd_Co_Line;
    private DbsField pnd_Co_Lit;
    private DbsField pnd_Det1_Lit;
    private DbsField pnd_Det2_Lit;
    private DbsField pnd_Det3_Lit;

    private DbsGroup pnd_Tot_Array;
    private DbsField pnd_Tot_Array_Pnd_Co_Name;

    private DbsGroup pnd_Tot_Array__R_Field_7;
    private DbsField pnd_Tot_Array_Pnd_Co_Short;
    private DbsField pnd_Co_Hold;
    private DbsField pnd_I;
    private DbsField pnd_Cntl_Ndx;
    private DbsField pnd_Sys_Date;

    private DbsRecord internalLoopRecord;
    private DbsField rD1Pnd_Tax_Id_NbrOld;
    private DbsField rD1Pnd_State_CodeOld;
    private DbsField rD1Pnd_NameOld;
    private DbsField rD1Pnd_Address_1Old;
    private DbsField rD1Pnd_Address_2Old;
    private DbsField rD1Pnd_Address_3Old;
    private DbsField rD1Pnd_Address_4Old;
    private DbsField rD1Pnd_Address_5Old;
    private DbsField rD1Pnd_Company_CodeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwrafrm2 = new PdaTwrafrm2(localVariables);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);

        pnd_W2_Record = localVariables.newGroupInRecord("pnd_W2_Record", "#W2-RECORD");
        pnd_W2_Record_Pnd_Tax_Year = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_W2_Record__R_Field_1 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_1", "REDEFINE", pnd_W2_Record_Pnd_Tax_Year);
        pnd_W2_Record_Pnd_Tax_Year_N = pnd_W2_Record__R_Field_1.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_W2_Record_Pnd_Tax_Id_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Type", "#TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Tax_Id_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 10);
        pnd_W2_Record_Pnd_Pin_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.STRING, 12);
        pnd_W2_Record_Pnd_Company_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Contract_Nbr = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_Payee_Cde = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Residency_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Type", "#RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Residency_Code = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Residency_Code", "#RESIDENCY-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_Gross_Amount = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_W2_Record_Pnd_Federal_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_State_Tax = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_W2_Record_Pnd_Name_And_Address = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Name_And_Address", "#NAME-AND-ADDRESS", FieldType.STRING, 
            210);

        pnd_W2_Record__R_Field_2 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_2", "REDEFINE", pnd_W2_Record_Pnd_Name_And_Address);
        pnd_W2_Record_Pnd_Name = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Name", "#NAME", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_1 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_1", "#ADDRESS-1", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_2 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_2", "#ADDRESS-2", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_3 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_3", "#ADDRESS-3", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_4 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_4", "#ADDRESS-4", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Address_5 = pnd_W2_Record__R_Field_2.newFieldInGroup("pnd_W2_Record_Pnd_Address_5", "#ADDRESS-5", FieldType.STRING, 35);
        pnd_W2_Record_Pnd_Employer_State_Id = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Employer_State_Id", "#EMPLOYER-STATE-ID", FieldType.STRING, 
            16);

        pnd_W2_Record__R_Field_3 = pnd_W2_Record.newGroupInGroup("pnd_W2_Record__R_Field_3", "REDEFINE", pnd_W2_Record_Pnd_Employer_State_Id);
        pnd_W2_Record_Pnd_State_Code = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_W2_Record_Pnd_State_Id = pnd_W2_Record__R_Field_3.newFieldInGroup("pnd_W2_Record_Pnd_State_Id", "#STATE-ID", FieldType.STRING, 14);
        pnd_W2_Record_Pnd_Transaction_Dt = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Transaction_Dt", "#TRANSACTION-DT", FieldType.STRING, 8);
        pnd_W2_Record_Pnd_W2_Op_Ia_Ind = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_W2_Op_Ia_Ind", "#W2-OP-IA-IND", FieldType.STRING, 1);
        pnd_W2_Record_Pnd_Record_Type = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 1);
        pnd_W2_Record_Pnd_Sys_Dte_Time = pnd_W2_Record.newFieldInGroup("pnd_W2_Record_Pnd_Sys_Dte_Time", "#SYS-DTE-TIME", FieldType.TIME);
        pnd_Old_Company = localVariables.newFieldInRecord("pnd_Old_Company", "#OLD-COMPANY", FieldType.STRING, 1);
        pnd_Old_Tax_Id = localVariables.newFieldInRecord("pnd_Old_Tax_Id", "#OLD-TAX-ID", FieldType.STRING, 10);
        pnd_Old_State_Code = localVariables.newFieldInRecord("pnd_Old_State_Code", "#OLD-STATE-CODE", FieldType.STRING, 2);
        pnd_Old_Name = localVariables.newFieldInRecord("pnd_Old_Name", "#OLD-NAME", FieldType.STRING, 35);
        pnd_Old_Addr_1 = localVariables.newFieldInRecord("pnd_Old_Addr_1", "#OLD-ADDR-1", FieldType.STRING, 35);
        pnd_Old_Addr_2 = localVariables.newFieldInRecord("pnd_Old_Addr_2", "#OLD-ADDR-2", FieldType.STRING, 35);
        pnd_Old_Addr_3 = localVariables.newFieldInRecord("pnd_Old_Addr_3", "#OLD-ADDR-3", FieldType.STRING, 35);
        pnd_Old_Addr_4 = localVariables.newFieldInRecord("pnd_Old_Addr_4", "#OLD-ADDR-4", FieldType.STRING, 35);
        pnd_Old_Addr_5 = localVariables.newFieldInRecord("pnd_Old_Addr_5", "#OLD-ADDR-5", FieldType.STRING, 35);
        pnd_Init_Tax_Yr = localVariables.newFieldInRecord("pnd_Init_Tax_Yr", "#INIT-TAX-YR", FieldType.STRING, 4);

        pnd_Init_Tax_Yr__R_Field_4 = localVariables.newGroupInRecord("pnd_Init_Tax_Yr__R_Field_4", "REDEFINE", pnd_Init_Tax_Yr);
        pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N = pnd_Init_Tax_Yr__R_Field_4.newFieldInGroup("pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N", "#INIT-TAX-YR-N", FieldType.NUMERIC, 
            4);
        pnd_Corr_Seq = localVariables.newFieldInRecord("pnd_Corr_Seq", "#CORR-SEQ", FieldType.PACKED_DECIMAL, 3);
        pnd_Dt_Tm_Compare = localVariables.newFieldInRecord("pnd_Dt_Tm_Compare", "#DT-TM-COMPARE", FieldType.STRING, 15);
        pnd_Tbl_Dt_Tm = localVariables.newFieldInRecord("pnd_Tbl_Dt_Tm", "#TBL-DT-TM", FieldType.STRING, 15);

        pnd_Tbl_Dt_Tm__R_Field_5 = localVariables.newGroupInRecord("pnd_Tbl_Dt_Tm__R_Field_5", "REDEFINE", pnd_Tbl_Dt_Tm);
        pnd_Tbl_Dt_Tm_Pnd_Tbl_Dt = pnd_Tbl_Dt_Tm__R_Field_5.newFieldInGroup("pnd_Tbl_Dt_Tm_Pnd_Tbl_Dt", "#TBL-DT", FieldType.STRING, 8);
        pnd_Tbl_Dt_Tm_Pnd_Tbl_Tm = pnd_Tbl_Dt_Tm__R_Field_5.newFieldInGroup("pnd_Tbl_Dt_Tm_Pnd_Tbl_Tm", "#TBL-TM", FieldType.NUMERIC, 7);
        pnd_Time_Convert_P = localVariables.newFieldInRecord("pnd_Time_Convert_P", "#TIME-CONVERT-P", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Time_Convert_N = localVariables.newFieldInRecord("pnd_Time_Convert_N", "#TIME-CONVERT-N", FieldType.NUMERIC, 13, 2);

        pnd_Time_Convert_N__R_Field_6 = localVariables.newGroupInRecord("pnd_Time_Convert_N__R_Field_6", "REDEFINE", pnd_Time_Convert_N);
        pnd_Time_Convert_N_Pnd_Fill4 = pnd_Time_Convert_N__R_Field_6.newFieldInGroup("pnd_Time_Convert_N_Pnd_Fill4", "#FILL4", FieldType.STRING, 4);
        pnd_Time_Convert_N_Pnd_Time_7 = pnd_Time_Convert_N__R_Field_6.newFieldInGroup("pnd_Time_Convert_N_Pnd_Time_7", "#TIME-7", FieldType.NUMERIC, 7);
        pnd_Time_Convert_N_Pnd_Fill2 = pnd_Time_Convert_N__R_Field_6.newFieldInGroup("pnd_Time_Convert_N_Pnd_Fill2", "#FILL2", FieldType.STRING, 2);
        pnd_Rpt_Title = localVariables.newFieldInRecord("pnd_Rpt_Title", "#RPT-TITLE", FieldType.STRING, 44);
        pnd_P_Gross_Accum = localVariables.newFieldArrayInRecord("pnd_P_Gross_Accum", "#P-GROSS-ACCUM", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_P_Fed_Accum = localVariables.newFieldArrayInRecord("pnd_P_Fed_Accum", "#P-FED-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_P_St_Accum = localVariables.newFieldArrayInRecord("pnd_P_St_Accum", "#P-ST-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_S_Gross_Accum = localVariables.newFieldArrayInRecord("pnd_S_Gross_Accum", "#S-GROSS-ACCUM", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_S_Fed_Accum = localVariables.newFieldArrayInRecord("pnd_S_Fed_Accum", "#S-FED-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_S_St_Accum = localVariables.newFieldArrayInRecord("pnd_S_St_Accum", "#S-ST-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_S_Forms_Accum = localVariables.newFieldInRecord("pnd_S_Forms_Accum", "#S-FORMS-ACCUM", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Gross_Accum = localVariables.newFieldArrayInRecord("pnd_C_Gross_Accum", "#C-GROSS-ACCUM", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_C_Fed_Accum = localVariables.newFieldArrayInRecord("pnd_C_Fed_Accum", "#C-FED-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_C_St_Accum = localVariables.newFieldArrayInRecord("pnd_C_St_Accum", "#C-ST-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_C_Forms_Accum = localVariables.newFieldInRecord("pnd_C_Forms_Accum", "#C-FORMS-ACCUM", FieldType.PACKED_DECIMAL, 7);
        pnd_T_Gross_Accum = localVariables.newFieldArrayInRecord("pnd_T_Gross_Accum", "#T-GROSS-ACCUM", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            3));
        pnd_T_Fed_Accum = localVariables.newFieldArrayInRecord("pnd_T_Fed_Accum", "#T-FED-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_T_St_Accum = localVariables.newFieldArrayInRecord("pnd_T_St_Accum", "#T-ST-ACCUM", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            3));
        pnd_T_Forms_Accum = localVariables.newFieldInRecord("pnd_T_Forms_Accum", "#T-FORMS-ACCUM", FieldType.PACKED_DECIMAL, 7);
        pnd_Co = localVariables.newFieldInRecord("pnd_Co", "#CO", FieldType.STRING, 1);
        pnd_Co_Ndx = localVariables.newFieldInRecord("pnd_Co_Ndx", "#CO-NDX", FieldType.NUMERIC, 2);
        pnd_Co_Line = localVariables.newFieldInRecord("pnd_Co_Line", "#CO-LINE", FieldType.STRING, 88);
        pnd_Co_Lit = localVariables.newFieldInRecord("pnd_Co_Lit", "#CO-LIT", FieldType.STRING, 9);
        pnd_Det1_Lit = localVariables.newFieldInRecord("pnd_Det1_Lit", "#DET1-LIT", FieldType.STRING, 10);
        pnd_Det2_Lit = localVariables.newFieldInRecord("pnd_Det2_Lit", "#DET2-LIT", FieldType.STRING, 10);
        pnd_Det3_Lit = localVariables.newFieldInRecord("pnd_Det3_Lit", "#DET3-LIT", FieldType.STRING, 10);

        pnd_Tot_Array = localVariables.newGroupArrayInRecord("pnd_Tot_Array", "#TOT-ARRAY", new DbsArrayController(1, 3));
        pnd_Tot_Array_Pnd_Co_Name = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Co_Name", "#CO-NAME", FieldType.STRING, 60);

        pnd_Tot_Array__R_Field_7 = pnd_Tot_Array.newGroupInGroup("pnd_Tot_Array__R_Field_7", "REDEFINE", pnd_Tot_Array_Pnd_Co_Name);
        pnd_Tot_Array_Pnd_Co_Short = pnd_Tot_Array__R_Field_7.newFieldInGroup("pnd_Tot_Array_Pnd_Co_Short", "#CO-SHORT", FieldType.STRING, 4);
        pnd_Co_Hold = localVariables.newFieldInRecord("pnd_Co_Hold", "#CO-HOLD", FieldType.STRING, 4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Cntl_Ndx = localVariables.newFieldInRecord("pnd_Cntl_Ndx", "#CNTL-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rD1Pnd_Tax_Id_NbrOld = internalLoopRecord.newFieldInRecord("RD1_Pnd_Tax_Id_Nbr_OLD", "Pnd_Tax_Id_Nbr_OLD", FieldType.STRING, 10);
        rD1Pnd_State_CodeOld = internalLoopRecord.newFieldInRecord("RD1_Pnd_State_Code_OLD", "Pnd_State_Code_OLD", FieldType.STRING, 2);
        rD1Pnd_NameOld = internalLoopRecord.newFieldInRecord("RD1_Pnd_Name_OLD", "Pnd_Name_OLD", FieldType.STRING, 35);
        rD1Pnd_Address_1Old = internalLoopRecord.newFieldInRecord("RD1_Pnd_Address_1_OLD", "Pnd_Address_1_OLD", FieldType.STRING, 35);
        rD1Pnd_Address_2Old = internalLoopRecord.newFieldInRecord("RD1_Pnd_Address_2_OLD", "Pnd_Address_2_OLD", FieldType.STRING, 35);
        rD1Pnd_Address_3Old = internalLoopRecord.newFieldInRecord("RD1_Pnd_Address_3_OLD", "Pnd_Address_3_OLD", FieldType.STRING, 35);
        rD1Pnd_Address_4Old = internalLoopRecord.newFieldInRecord("RD1_Pnd_Address_4_OLD", "Pnd_Address_4_OLD", FieldType.STRING, 35);
        rD1Pnd_Address_5Old = internalLoopRecord.newFieldInRecord("RD1_Pnd_Address_5_OLD", "Pnd_Address_5_OLD", FieldType.STRING, 35);
        rD1Pnd_Company_CodeOld = internalLoopRecord.newFieldInRecord("RD1_Pnd_Company_Code_OLD", "Pnd_Company_Code_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
        pnd_Co_Lit.setInitialValue("COMPANY: ");
        pnd_Det1_Lit.setInitialValue("*OLD FORM*");
        pnd_Det2_Lit.setInitialValue("*NEW FORM*");
        pnd_Det3_Lit.setInitialValue("*NET CHG*");
        pnd_Tot_Array_Pnd_Co_Name.getValue(1).setInitialValue("TIAA - TEACHERS INSURANCE AND ANNUITY ASSOC.");
        pnd_Tot_Array_Pnd_Co_Name.getValue(2).setInitialValue("TRST - TIAA-CREF AS AGENT FOR JPMORGAN CHASE BK RET PLNS TR");
        pnd_Tot_Array_Pnd_Co_Name.getValue(3).setInitialValue("GRAND TOTALS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5720() throws Exception
    {
        super("Twrp5720");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP5720", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 60
        pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #SYS-DATE := *DATX
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //* *******************************
        //*   BEGIN MAIN PROCESSING LOOP
        //* *******************************
        boolean endOfDataRd1 = true;                                                                                                                                      //Natural: READ WORK FILE 1 RECORD #W2-RECORD
        boolean firstRd1 = true;
        RD1:
        while (condition(getWorkFiles().read(1, pnd_W2_Record)))
        {
            CheckAtStartofData232();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRd1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRd1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //* *********************************
            //*  BREAK OF TIN - LOW LEVEL BREAK *
            //* *********************************
            //* ***************************************                                                                                                                   //Natural: AT BREAK OF #W2-RECORD.#TAX-ID-NBR
            //*  BREAK OF RESIDENCY - MID LEVEL BREAK *
            //* ***************************************
            //*                                                                                                                                                           //Natural: AT BREAK OF #W2-RECORD.#RESIDENCY-CODE
            //* **************************************
            //*  BREAK OF COMPANY - HIGH LEVEL BREAK *
            //* **************************************
            //* ***********************                                                                                                                                   //Natural: AT BREAK OF #W2-RECORD.#COMPANY-CODE
            //*  END BREAK PROCESSING *
            //* ***********************
            pnd_Dt_Tm_Compare.setValueEdited(pnd_W2_Record_Pnd_Sys_Dte_Time,new ReportEditMask("YYYYMMDDHHIISST"));                                                       //Natural: MOVE EDITED #W2-RECORD.#SYS-DTE-TIME ( EM = YYYYMMDDHHIISST ) TO #DT-TM-COMPARE
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, NEWLINE,"=",pnd_Dt_Tm_Compare,NEWLINE);                                                                                             //Natural: WRITE / '=' #DT-TM-COMPARE /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W2_Record_Pnd_Company_Code.notEquals(pnd_Old_Company)))                                                                                     //Natural: IF #W2-RECORD.#COMPANY-CODE NE #OLD-COMPANY
            {
                short decideConditionsMet333 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #W2-RECORD.#COMPANY-CODE;//Natural: VALUE 'T'
                if (condition((pnd_W2_Record_Pnd_Company_Code.equals("T"))))
                {
                    decideConditionsMet333++;
                    pnd_Co_Ndx.setValue(1);                                                                                                                               //Natural: ASSIGN #CO-NDX := 1
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_W2_Record_Pnd_Company_Code.equals("X"))))
                {
                    decideConditionsMet333++;
                    pnd_Co_Ndx.setValue(2);                                                                                                                               //Natural: ASSIGN #CO-NDX := 2
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet333 > 0))
                {
                    pnd_Co_Line.setValue(DbsUtil.compress(pnd_Co_Lit, pnd_Tot_Array_Pnd_Co_Name.getValue(pnd_Co_Ndx)));                                                   //Natural: COMPRESS #CO-LIT #CO-NAME ( #CO-NDX ) INTO #CO-LINE
                    pnd_Co_Hold.setValue(pnd_Tot_Array_Pnd_Co_Short.getValue(pnd_Co_Ndx));                                                                                //Natural: ASSIGN #CO-HOLD := #CO-SHORT ( #CO-NDX )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet346 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DT-TM-COMPARE <= #TBL-DT-TM
            if (condition(pnd_Dt_Tm_Compare.lessOrEqual(pnd_Tbl_Dt_Tm)))
            {
                decideConditionsMet346++;
                pnd_P_Gross_Accum.getValue(1).nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                                                       //Natural: ADD #W2-RECORD.#GROSS-AMOUNT TO #P-GROSS-ACCUM ( 1 )
                pnd_P_Fed_Accum.getValue(1).nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                                                          //Natural: ADD #W2-RECORD.#FEDERAL-TAX TO #P-FED-ACCUM ( 1 )
                pnd_P_St_Accum.getValue(1).nadd(pnd_W2_Record_Pnd_State_Tax);                                                                                             //Natural: ADD #W2-RECORD.#STATE-TAX TO #P-ST-ACCUM ( 1 )
                pnd_P_Gross_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                                                       //Natural: ADD #W2-RECORD.#GROSS-AMOUNT TO #P-GROSS-ACCUM ( 2 )
                pnd_P_Fed_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                                                          //Natural: ADD #W2-RECORD.#FEDERAL-TAX TO #P-FED-ACCUM ( 2 )
                pnd_P_St_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_State_Tax);                                                                                             //Natural: ADD #W2-RECORD.#STATE-TAX TO #P-ST-ACCUM ( 2 )
            }                                                                                                                                                             //Natural: WHEN #DT-TM-COMPARE > #TBL-DT-TM
            else if (condition(pnd_Dt_Tm_Compare.greater(pnd_Tbl_Dt_Tm)))
            {
                decideConditionsMet346++;
                pnd_P_Gross_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_Gross_Amount);                                                                                       //Natural: ADD #W2-RECORD.#GROSS-AMOUNT TO #P-GROSS-ACCUM ( 2 )
                pnd_P_Fed_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_Federal_Tax);                                                                                          //Natural: ADD #W2-RECORD.#FEDERAL-TAX TO #P-FED-ACCUM ( 2 )
                pnd_P_St_Accum.getValue(2).nadd(pnd_W2_Record_Pnd_State_Tax);                                                                                             //Natural: ADD #W2-RECORD.#STATE-TAX TO #P-ST-ACCUM ( 2 )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            rD1Pnd_Tax_Id_NbrOld.setValue(pnd_W2_Record_Pnd_Tax_Id_Nbr);                                                                                                  //Natural: END-WORK
            rD1Pnd_State_CodeOld.setValue(pnd_W2_Record_Pnd_State_Code);
            rD1Pnd_NameOld.setValue(pnd_W2_Record_Pnd_Name);
            rD1Pnd_Address_1Old.setValue(pnd_W2_Record_Pnd_Address_1);
            rD1Pnd_Address_2Old.setValue(pnd_W2_Record_Pnd_Address_2);
            rD1Pnd_Address_3Old.setValue(pnd_W2_Record_Pnd_Address_3);
            rD1Pnd_Address_4Old.setValue(pnd_W2_Record_Pnd_Address_4);
            rD1Pnd_Address_5Old.setValue(pnd_W2_Record_Pnd_Address_5);
            rD1Pnd_Company_CodeOld.setValue(pnd_W2_Record_Pnd_Company_Code);
        }
        RD1_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRd1(endOfDataRd1);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TABLE
        sub_Update_Control_Table();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
        sub_Write_Grand_Totals();
        if (condition(Global.isEscape())) {return;}
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W2-CORR-DETAIL
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //* **************************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-CONTROL-LOOKUP
        //* **************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-TABLE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
        //* **
        //* **                                                                                                                                                            //Natural: ON ERROR
        //* **
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_W2_Corr_Detail() throws Exception                                                                                                              //Natural: WRITE-W2-CORR-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(getReports().getAstLineCount(1).greater(49)))                                                                                                       //Natural: IF *LINE-COUNT ( 01 ) > 49
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_C_Forms_Accum.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-FORMS-ACCUM
        pnd_T_Forms_Accum.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #T-FORMS-ACCUM
        pnd_S_Forms_Accum.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #S-FORMS-ACCUM
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Old_Tax_Id, new ReportEditMask ("XXX-XX-XXXX"),new TabSetting(13),pnd_Old_Name,          //Natural: WRITE ( 01 ) / 1T #OLD-TAX-ID ( EM = XXX-XX-XXXX ) 13T #OLD-NAME ( AL = 30 ) 44T #P-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #P-FED-ACCUM ( 1 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #P-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 95T #OLD-STATE-CODE ( EM = XX ) 98T #P-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #P-ST-ACCUM ( 1 ) ( EM = -Z,ZZZ,ZZ9.99 )
            new AlphanumericLength (30),new TabSetting(44),pnd_P_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_P_Fed_Accum.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_P_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Old_State_Code, 
            new ReportEditMask ("XX"),new TabSetting(98),pnd_P_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_P_St_Accum.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        short decideConditionsMet407 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #OLD-ADDR-1 NE ' '
        if (condition(pnd_Old_Addr_1.notEquals(" ")))
        {
            decideConditionsMet407++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Det1_Lit,new TabSetting(13),pnd_Old_Addr_1);                                                 //Natural: WRITE ( 01 ) 1T #DET1-LIT 13T #OLD-ADDR-1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #OLD-ADDR-2 NE ' '
        if (condition(pnd_Old_Addr_2.notEquals(" ")))
        {
            decideConditionsMet407++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_2);                                                                                //Natural: WRITE ( 01 ) 13T #OLD-ADDR-2
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #OLD-ADDR-3 NE ' '
        if (condition(pnd_Old_Addr_3.notEquals(" ")))
        {
            decideConditionsMet407++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_3);                                                                                //Natural: WRITE ( 01 ) 13T #OLD-ADDR-3
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #OLD-ADDR-4 NE ' '
        if (condition(pnd_Old_Addr_4.notEquals(" ")))
        {
            decideConditionsMet407++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_4);                                                                                //Natural: WRITE ( 01 ) 13T #OLD-ADDR-4
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #OLD-ADDR-5 NE ' '
        if (condition(pnd_Old_Addr_5.notEquals(" ")))
        {
            decideConditionsMet407++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),pnd_Old_Addr_5);                                                                                //Natural: WRITE ( 01 ) 13T #OLD-ADDR-5
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet407 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Det2_Lit,new TabSetting(44),pnd_P_Gross_Accum.getValue(2), new ReportEditMask            //Natural: WRITE ( 01 ) / 1T #DET2-LIT 44T #P-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #P-FED-ACCUM ( 2 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #P-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 95T #OLD-STATE-CODE ( EM = XX ) 98T #P-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #P-ST-ACCUM ( 2 ) ( EM = -Z,ZZZ,ZZ9.99 )
            ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_P_Fed_Accum.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_P_Gross_Accum.getValue(2), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Old_State_Code, new ReportEditMask ("XX"),new TabSetting(98),pnd_P_Gross_Accum.getValue(2), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_P_St_Accum.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),pnd_Det3_Lit,new TabSetting(44),pnd_P_Gross_Accum.getValue(3), new ReportEditMask            //Natural: WRITE ( 01 ) / 1T #DET3-LIT 44T #P-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #P-FED-ACCUM ( 3 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #P-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 95T #OLD-STATE-CODE ( EM = XX ) 98T #P-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 118T #P-ST-ACCUM ( 3 ) ( EM = -Z,ZZZ,ZZ9.99 )
            ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_P_Fed_Accum.getValue(3), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_P_Gross_Accum.getValue(3), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_Old_State_Code, new ReportEditMask ("XX"),new TabSetting(98),pnd_P_Gross_Accum.getValue(3), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(118),pnd_P_St_Accum.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 01 ) 1
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Co_Line.setValue(pnd_Tot_Array_Pnd_Co_Name.getValue(3));                                                                                                      //Natural: ASSIGN #CO-LINE := #CO-NAME ( 3 )
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Total Form Count",new TabSetting(31),pnd_T_Forms_Accum, new ReportEditMask                  //Natural: WRITE ( 01 ) / 1T 'Total Form Count' 31T #T-FORMS-ACCUM ( EM = ZZZ,ZZ9 ) 44T #T-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #T-FED-ACCUM ( 1 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #T-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #T-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #T-ST-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #T-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #T-FED-ACCUM ( 2 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #T-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #T-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #T-ST-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #T-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #T-FED-ACCUM ( 3 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #T-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #T-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #T-ST-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) /
            ("ZZZ,ZZ9"),new TabSetting(44),pnd_T_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_T_Fed_Accum.getValue(1), 
            new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_T_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_T_Gross_Accum.getValue(1), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_T_St_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_T_Gross_Accum.getValue(2), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_T_Fed_Accum.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_T_Gross_Accum.getValue(2), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_T_Gross_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_T_St_Accum.getValue(2), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_T_Gross_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(62),pnd_T_Fed_Accum.getValue(3), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_T_Gross_Accum.getValue(3), new ReportEditMask 
            ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_T_Gross_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_T_St_Accum.getValue(3), 
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
    }
    //*  W-2
    private void sub_Form_Control_Lookup() throws Exception                                                                                                               //Natural: FORM-CONTROL-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N);                                                                           //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #INIT-TAX-YR-N
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(8);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 8
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Twrnfrm2.class , getCurrentProcessState(), pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRM2' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean())))                                                                               //Natural: IF NOT #TWRATBL4.#TIRCNTL-IRS-ORIG
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(15),"ORIGINAL REPORTING PROCESS FOR",pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Form_Name().getValue(8 + 1),new  //Natural: WRITE ( 1 ) '***' 15T 'ORIGINAL REPORTING PROCESS FOR' #TWRAFRMN.#FORM-NAME ( 8 ) 77T '***' / '***' 15T 'HAS NOT BEEN RUN YET' 77T '***' / '***' 15T 'ORIGINAL REPORTING MUST BE RUN BEFORE' 77T '***' / '***' 15T 'CORRECTIONS REPORTING - CHECK SCHEDULE' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(15),"HAS NOT BEEN RUN YET",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(15),"ORIGINAL REPORTING MUST BE RUN BEFORE",new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(15),"CORRECTIONS REPORTING - CHECK SCHEDULE",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            if (condition(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pnd_I).greater(getZero())))                                                              //Natural: IF TIRCNTL-RPT-DTE ( #I ) > 0
            {
                pnd_Corr_Seq.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CORR-SEQ
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tbl_Dt_Tm_Pnd_Tbl_Dt.setValueEdited(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pnd_Corr_Seq),new ReportEditMask("YYYYMMDD"));                     //Natural: MOVE EDITED TIRCNTL-RPT-DTE ( #CORR-SEQ ) ( EM = YYYYMMDD ) TO #TBL-DT
        pnd_Time_Convert_N.setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Gross_Amt().getValue(pnd_Corr_Seq));                                                     //Natural: ASSIGN #TIME-CONVERT-N := TIRCNTL-RPT-CREF-GROSS-AMT ( #CORR-SEQ )
        pnd_Tbl_Dt_Tm_Pnd_Tbl_Tm.setValue(pnd_Time_Convert_N_Pnd_Time_7);                                                                                                 //Natural: ASSIGN #TBL-TM := #TIME-7
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, NEWLINE,NEWLINE,"=",pnd_Tbl_Dt_Tm,new ColumnSpacing(5),"=",pnd_Corr_Seq);                                                               //Natural: WRITE // '=' #TBL-DT-TM 5X '=' #CORR-SEQ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Control_Table() throws Exception                                                                                                              //Natural: UPDATE-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getReports().write(0, "Begin Subroutine: UPDATE-CONTROL-TABLE");                                                                                                  //Natural: WRITE 'Begin Subroutine: UPDATE-CONTROL-TABLE'
        if (Global.isEscape()) return;
        pnd_Cntl_Ndx.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #CNTL-NDX
        pnd_Corr_Seq.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CORR-SEQ
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pnd_Corr_Seq).setValue(pnd_Sys_Date);                                                                      //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( #CORR-SEQ ) := #SYS-DATE
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Gross_Amt().getValue(pnd_Corr_Seq).setValue(Global.getTIMN());                                                       //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-CREF-GROSS-AMT ( #CORR-SEQ ) := *TIMN
        if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Corr().getBoolean())))                                                                               //Natural: IF NOT #TIRCNTL-IRS-CORR
        {
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Corr().setValue(true);                                                                                            //Natural: ASSIGN #TWRATBL4.#TIRCNTL-IRS-CORR := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, NEWLINE,NEWLINE,"=",pnd_Corr_Seq,NEWLINE,"=",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pnd_Corr_Seq),NEWLINE,"=",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Gross_Amt().getValue(pnd_Corr_Seq),NEWLINE,"=",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Corr(),  //Natural: WRITE // '=' #CORR-SEQ / '=' #TWRATBL4.TIRCNTL-RPT-DTE ( #CORR-SEQ ) / '=' #TWRATBL4.TIRCNTL-RPT-CREF-GROSS-AMT ( #CORR-SEQ ) / '=' #TWRATBL4.#TIRCNTL-IRS-CORR ( EM = N/Y )
                new ReportEditMask ("N/Y"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new     //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 43T #RPT-TITLE 120T 'REPORT: RPT1' / 60T 'Tax Year:' #INIT-TAX-YR-N ( EM = 9999 ) / #CO-LINE // 3T 'Social' 15T 'Name and Address' 52T 'Box 1' 68T 'Box 2' 84T 'Box 11' 94T 'Box 15' 105T 'Box 16' 123T 'Box 17' / 1T 'Security No' 50T 'Gross Amt' 65T 'Federal Tax' 83T 'Gross Amt' 94T 'State' 104T 'Gross Amt' 122T 'State Tax'
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(43),pnd_Rpt_Title,new TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N, 
                        new ReportEditMask ("9999"),NEWLINE,pnd_Co_Line,NEWLINE,NEWLINE,new TabSetting(3),"Social",new TabSetting(15),"Name and Address",new 
                        TabSetting(52),"Box 1",new TabSetting(68),"Box 2",new TabSetting(84),"Box 11",new TabSetting(94),"Box 15",new TabSetting(105),"Box 16",new 
                        TabSetting(123),"Box 17",NEWLINE,new TabSetting(1),"Security No",new TabSetting(50),"Gross Amt",new TabSetting(65),"Federal Tax",new 
                        TabSetting(83),"Gross Amt",new TabSetting(94),"State",new TabSetting(104),"Gross Amt",new TabSetting(122),"State Tax");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
        sub_Error_Routine();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventRd1() throws Exception {atBreakEventRd1(false);}
    private void atBreakEventRd1(boolean endOfData) throws Exception
    {
        boolean pnd_W2_Record_Pnd_Tax_Id_NbrIsBreak = pnd_W2_Record_Pnd_Tax_Id_Nbr.isBreak(endOfData);
        boolean pnd_W2_Record_Pnd_Residency_CodeIsBreak = pnd_W2_Record_Pnd_Residency_Code.isBreak(endOfData);
        boolean pnd_W2_Record_Pnd_Company_CodeIsBreak = pnd_W2_Record_Pnd_Company_Code.isBreak(endOfData);
        if (condition(pnd_W2_Record_Pnd_Tax_Id_NbrIsBreak || pnd_W2_Record_Pnd_Residency_CodeIsBreak || pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            pnd_Old_Tax_Id.setValue(rD1Pnd_Tax_Id_NbrOld);                                                                                                                //Natural: ASSIGN #OLD-TAX-ID := OLD ( #TAX-ID-NBR )
            pnd_Old_State_Code.setValue(rD1Pnd_State_CodeOld);                                                                                                            //Natural: ASSIGN #OLD-STATE-CODE := OLD ( #STATE-CODE )
            pnd_Old_Name.setValue(rD1Pnd_NameOld);                                                                                                                        //Natural: ASSIGN #OLD-NAME := OLD ( #NAME )
            pnd_Old_Addr_1.setValue(rD1Pnd_Address_1Old);                                                                                                                 //Natural: ASSIGN #OLD-ADDR-1 := OLD ( #ADDRESS-1 )
            pnd_Old_Addr_2.setValue(rD1Pnd_Address_2Old);                                                                                                                 //Natural: ASSIGN #OLD-ADDR-2 := OLD ( #ADDRESS-2 )
            pnd_Old_Addr_3.setValue(rD1Pnd_Address_3Old);                                                                                                                 //Natural: ASSIGN #OLD-ADDR-3 := OLD ( #ADDRESS-3 )
            pnd_Old_Addr_4.setValue(rD1Pnd_Address_4Old);                                                                                                                 //Natural: ASSIGN #OLD-ADDR-4 := OLD ( #ADDRESS-4 )
            pnd_Old_Addr_5.setValue(rD1Pnd_Address_5Old);                                                                                                                 //Natural: ASSIGN #OLD-ADDR-5 := OLD ( #ADDRESS-5 )
            //*  OR #DEBUG
            if (condition((pnd_P_Gross_Accum.getValue(1).notEquals(pnd_P_Gross_Accum.getValue(2)) || pnd_P_Fed_Accum.getValue(1).notEquals(pnd_P_Fed_Accum.getValue(2))   //Natural: IF ( #P-GROSS-ACCUM ( 1 ) NE #P-GROSS-ACCUM ( 2 ) OR #P-FED-ACCUM ( 1 ) NE #P-FED-ACCUM ( 2 ) OR #P-ST-ACCUM ( 1 ) NE #P-ST-ACCUM ( 2 ) )
                || pnd_P_St_Accum.getValue(1).notEquals(pnd_P_St_Accum.getValue(2)))))
            {
                pnd_P_Gross_Accum.getValue(3).compute(new ComputeParameters(false, pnd_P_Gross_Accum.getValue(3)), pnd_P_Gross_Accum.getValue(2).subtract(pnd_P_Gross_Accum.getValue(1))); //Natural: COMPUTE #P-GROSS-ACCUM ( 3 ) = #P-GROSS-ACCUM ( 2 ) - #P-GROSS-ACCUM ( 1 )
                pnd_P_Fed_Accum.getValue(3).compute(new ComputeParameters(false, pnd_P_Fed_Accum.getValue(3)), pnd_P_Fed_Accum.getValue(2).subtract(pnd_P_Fed_Accum.getValue(1))); //Natural: COMPUTE #P-FED-ACCUM ( 3 ) = #P-FED-ACCUM ( 2 ) - #P-FED-ACCUM ( 1 )
                pnd_P_St_Accum.getValue(3).compute(new ComputeParameters(false, pnd_P_St_Accum.getValue(3)), pnd_P_St_Accum.getValue(2).subtract(pnd_P_St_Accum.getValue(1))); //Natural: COMPUTE #P-ST-ACCUM ( 3 ) = #P-ST-ACCUM ( 2 ) - #P-ST-ACCUM ( 1 )
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-CORR-DETAIL
                sub_Write_W2_Corr_Detail();
                if (condition(Global.isEscape())) {return;}
                pnd_S_Gross_Accum.getValue(1).nadd(pnd_P_Gross_Accum.getValue(1));                                                                                        //Natural: ADD #P-GROSS-ACCUM ( 1 ) TO #S-GROSS-ACCUM ( 1 )
                pnd_S_Fed_Accum.getValue(1).nadd(pnd_P_Fed_Accum.getValue(1));                                                                                            //Natural: ADD #P-FED-ACCUM ( 1 ) TO #S-FED-ACCUM ( 1 )
                pnd_S_St_Accum.getValue(1).nadd(pnd_P_St_Accum.getValue(1));                                                                                              //Natural: ADD #P-ST-ACCUM ( 1 ) TO #S-ST-ACCUM ( 1 )
                pnd_S_Gross_Accum.getValue(2).nadd(pnd_P_Gross_Accum.getValue(2));                                                                                        //Natural: ADD #P-GROSS-ACCUM ( 2 ) TO #S-GROSS-ACCUM ( 2 )
                pnd_S_Fed_Accum.getValue(2).nadd(pnd_P_Fed_Accum.getValue(2));                                                                                            //Natural: ADD #P-FED-ACCUM ( 2 ) TO #S-FED-ACCUM ( 2 )
                pnd_S_St_Accum.getValue(2).nadd(pnd_P_St_Accum.getValue(2));                                                                                              //Natural: ADD #P-ST-ACCUM ( 2 ) TO #S-ST-ACCUM ( 2 )
                pnd_S_Gross_Accum.getValue(3).nadd(pnd_P_Gross_Accum.getValue(3));                                                                                        //Natural: ADD #P-GROSS-ACCUM ( 3 ) TO #S-GROSS-ACCUM ( 3 )
                pnd_S_Fed_Accum.getValue(3).nadd(pnd_P_Fed_Accum.getValue(3));                                                                                            //Natural: ADD #P-FED-ACCUM ( 3 ) TO #S-FED-ACCUM ( 3 )
                pnd_S_St_Accum.getValue(3).nadd(pnd_P_St_Accum.getValue(3));                                                                                              //Natural: ADD #P-ST-ACCUM ( 3 ) TO #S-ST-ACCUM ( 3 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_P_Gross_Accum.getValue("*").reset();                                                                                                                      //Natural: RESET #P-GROSS-ACCUM ( * ) #P-FED-ACCUM ( * ) #P-ST-ACCUM ( * )
            pnd_P_Fed_Accum.getValue("*").reset();
            pnd_P_St_Accum.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_W2_Record_Pnd_Residency_CodeIsBreak || pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            if (condition(pnd_S_Forms_Accum.greater(getZero())))                                                                                                          //Natural: IF #S-FORMS-ACCUM > 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Total Form Count for State",rD1Pnd_State_CodeOld, new AlphanumericLength            //Natural: WRITE ( 01 ) / 1T 'Total Form Count for State' OLD ( #STATE-CODE ) ( AL = 2 ) 31T #S-FORMS-ACCUM ( EM = ZZZ,ZZ9 ) 44T #S-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #S-FED-ACCUM ( 1 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #S-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #S-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #S-ST-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #S-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #S-FED-ACCUM ( 2 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #S-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #S-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #S-ST-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #S-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #S-FED-ACCUM ( 3 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #S-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #S-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #S-ST-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) /
                    (2),new TabSetting(31),pnd_S_Forms_Accum, new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_S_Gross_Accum.getValue(1), new ReportEditMask 
                    ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_S_Fed_Accum.getValue(1), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_S_Gross_Accum.getValue(1), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_S_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(116),pnd_S_St_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_S_Gross_Accum.getValue(2), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_S_Fed_Accum.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_S_Gross_Accum.getValue(2), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_S_Gross_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(116),pnd_S_St_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(44),pnd_S_Gross_Accum.getValue(3), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_S_Fed_Accum.getValue(3), new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_S_Gross_Accum.getValue(3), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_S_Gross_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(116),pnd_S_St_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE);
                if (condition(Global.isEscape())) return;
                pnd_C_Gross_Accum.getValue(1).nadd(pnd_S_Gross_Accum.getValue(1));                                                                                        //Natural: ADD #S-GROSS-ACCUM ( 1 ) TO #C-GROSS-ACCUM ( 1 )
                pnd_C_Fed_Accum.getValue(1).nadd(pnd_S_Fed_Accum.getValue(1));                                                                                            //Natural: ADD #S-FED-ACCUM ( 1 ) TO #C-FED-ACCUM ( 1 )
                pnd_C_St_Accum.getValue(1).nadd(pnd_S_St_Accum.getValue(1));                                                                                              //Natural: ADD #S-ST-ACCUM ( 1 ) TO #C-ST-ACCUM ( 1 )
                pnd_C_Gross_Accum.getValue(2).nadd(pnd_S_Gross_Accum.getValue(2));                                                                                        //Natural: ADD #S-GROSS-ACCUM ( 2 ) TO #C-GROSS-ACCUM ( 2 )
                pnd_C_Fed_Accum.getValue(2).nadd(pnd_S_Fed_Accum.getValue(2));                                                                                            //Natural: ADD #S-FED-ACCUM ( 2 ) TO #C-FED-ACCUM ( 2 )
                pnd_C_St_Accum.getValue(2).nadd(pnd_S_St_Accum.getValue(2));                                                                                              //Natural: ADD #S-ST-ACCUM ( 2 ) TO #C-ST-ACCUM ( 2 )
                pnd_C_Gross_Accum.getValue(3).nadd(pnd_S_Gross_Accum.getValue(3));                                                                                        //Natural: ADD #S-GROSS-ACCUM ( 3 ) TO #C-GROSS-ACCUM ( 3 )
                pnd_C_Fed_Accum.getValue(3).nadd(pnd_S_Fed_Accum.getValue(3));                                                                                            //Natural: ADD #S-FED-ACCUM ( 3 ) TO #C-FED-ACCUM ( 3 )
                pnd_C_St_Accum.getValue(3).nadd(pnd_S_St_Accum.getValue(3));                                                                                              //Natural: ADD #S-ST-ACCUM ( 3 ) TO #C-ST-ACCUM ( 3 )
                pnd_S_Forms_Accum.reset();                                                                                                                                //Natural: RESET #S-FORMS-ACCUM #S-GROSS-ACCUM ( * ) #S-FED-ACCUM ( * ) #S-ST-ACCUM ( * )
                pnd_S_Gross_Accum.getValue("*").reset();
                pnd_S_Fed_Accum.getValue("*").reset();
                pnd_S_St_Accum.getValue("*").reset();
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_W2_Record_Pnd_Company_CodeIsBreak))
        {
            pnd_Old_Company.setValue(rD1Pnd_Company_CodeOld);                                                                                                             //Natural: ASSIGN #OLD-COMPANY := OLD ( #W2-RECORD.#COMPANY-CODE )
            if (condition(pnd_C_Forms_Accum.greater(getZero())))                                                                                                          //Natural: IF #C-FORMS-ACCUM > 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"TOTAL FORM COUNT FOR",pnd_Co_Hold,new TabSetting(31),pnd_C_Forms_Accum,             //Natural: WRITE ( 01 ) / 1T 'TOTAL FORM COUNT FOR' #CO-HOLD 31T #C-FORMS-ACCUM ( EM = ZZZ,ZZ9 ) 44T #C-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #C-FED-ACCUM ( 1 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #C-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #C-GROSS-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #C-ST-ACCUM ( 1 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #C-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #C-FED-ACCUM ( 2 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #C-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #C-GROSS-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #C-ST-ACCUM ( 2 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) / 44T #C-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 62T #C-FED-ACCUM ( 3 ) ( EM = -ZZ,ZZZ,ZZ9.99 ) 77T #C-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 98T #C-GROSS-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) 116T #C-ST-ACCUM ( 3 ) ( EM = -ZZZ,ZZZ,ZZ9.99 ) /
                    new ReportEditMask ("ZZZ,ZZ9"),new TabSetting(44),pnd_C_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_C_Fed_Accum.getValue(1), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_C_Gross_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_C_Gross_Accum.getValue(1), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_C_St_Accum.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                    TabSetting(44),pnd_C_Gross_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_C_Fed_Accum.getValue(2), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_C_Gross_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_C_Gross_Accum.getValue(2), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_C_St_Accum.getValue(2), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                    TabSetting(44),pnd_C_Gross_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(62),pnd_C_Fed_Accum.getValue(3), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_C_Gross_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(98),pnd_C_Gross_Accum.getValue(3), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(116),pnd_C_St_Accum.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),NEWLINE);
                if (condition(Global.isEscape())) return;
                pnd_T_Gross_Accum.getValue(1).nadd(pnd_C_Gross_Accum.getValue(1));                                                                                        //Natural: ADD #C-GROSS-ACCUM ( 1 ) TO #T-GROSS-ACCUM ( 1 )
                pnd_T_Fed_Accum.getValue(1).nadd(pnd_C_Fed_Accum.getValue(1));                                                                                            //Natural: ADD #C-FED-ACCUM ( 1 ) TO #T-FED-ACCUM ( 1 )
                pnd_T_St_Accum.getValue(1).nadd(pnd_C_St_Accum.getValue(1));                                                                                              //Natural: ADD #C-ST-ACCUM ( 1 ) TO #T-ST-ACCUM ( 1 )
                pnd_T_Gross_Accum.getValue(2).nadd(pnd_C_Gross_Accum.getValue(2));                                                                                        //Natural: ADD #C-GROSS-ACCUM ( 2 ) TO #T-GROSS-ACCUM ( 2 )
                pnd_T_Fed_Accum.getValue(2).nadd(pnd_C_Fed_Accum.getValue(2));                                                                                            //Natural: ADD #C-FED-ACCUM ( 2 ) TO #T-FED-ACCUM ( 2 )
                pnd_T_St_Accum.getValue(2).nadd(pnd_C_St_Accum.getValue(2));                                                                                              //Natural: ADD #C-ST-ACCUM ( 2 ) TO #T-ST-ACCUM ( 2 )
                pnd_T_Gross_Accum.getValue(3).nadd(pnd_C_Gross_Accum.getValue(3));                                                                                        //Natural: ADD #C-GROSS-ACCUM ( 3 ) TO #T-GROSS-ACCUM ( 3 )
                pnd_T_Fed_Accum.getValue(3).nadd(pnd_C_Fed_Accum.getValue(3));                                                                                            //Natural: ADD #C-FED-ACCUM ( 3 ) TO #T-FED-ACCUM ( 3 )
                pnd_T_St_Accum.getValue(3).nadd(pnd_C_St_Accum.getValue(3));                                                                                              //Natural: ADD #C-ST-ACCUM ( 3 ) TO #T-ST-ACCUM ( 3 )
                pnd_C_Forms_Accum.reset();                                                                                                                                //Natural: RESET #C-FORMS-ACCUM #C-GROSS-ACCUM ( * ) #C-FED-ACCUM ( * ) #C-ST-ACCUM ( * )
                pnd_C_Gross_Accum.getValue("*").reset();
                pnd_C_Fed_Accum.getValue("*").reset();
                pnd_C_St_Accum.getValue("*").reset();
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
    private void CheckAtStartofData232() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Init_Tax_Yr.setValue(pnd_W2_Record_Pnd_Tax_Year);                                                                                                         //Natural: ASSIGN #INIT-TAX-YR := #W2-RECORD.#TAX-YEAR
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, NEWLINE,"=",pnd_Init_Tax_Yr,NEWLINE);                                                                                               //Natural: WRITE / '=' #INIT-TAX-YR /
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORM-CONTROL-LOOKUP
            sub_Form_Control_Lookup();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Corr_Seq.equals(1)))                                                                                                                        //Natural: IF #CORR-SEQ = 1
            {
                pnd_Rpt_Title.setValue("Participant Amended Forms Before SSA Orig");                                                                                      //Natural: ASSIGN #RPT-TITLE := 'Participant Amended Forms Before SSA Orig'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rpt_Title.setValue("Participant Correction Forms After SSA Orig");                                                                                    //Natural: ASSIGN #RPT-TITLE := 'Participant Correction Forms After SSA Orig'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
