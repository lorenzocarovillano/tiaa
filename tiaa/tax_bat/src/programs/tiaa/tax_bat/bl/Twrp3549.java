/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:37:12 PM
**        * FROM NATURAL PROGRAM : Twrp3549
************************************************************
**        * FILE NAME            : Twrp3549.java
**        * CLASS NAME           : Twrp3549
**        * INSTANCE NAME        : Twrp3549
************************************************************
************************************************************************
** PROGRAM : TWRP3549
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: 1099 IRS CORRECTION REPORTING FOR YEAR >= 2004
** HISTORY.:
**    11/10/2005 MS RECOMPILED - CHANGE IN TWRL5001
**    04/18/05   MS - NEW PROVINCE CODE 88 - NU
**    03/06/07   AY - REVISED TO CALL NAME CONTROL SUB-ROUTINE
**    05/01/07        'TWRN5052' TO POPULATE IRS FIELD
**                    IRS-B-NAME-CONTROL.
**    06/29/07   RM - RECOMPILED THIS PROGRAM FOR TWRG3537 CHANGES.
**                    UPDATED LOGIC TO LIMIT INDEX FOR STATE TABLE, IN
**                    ORDER TO PREVENT OUT OF BOUNDRY PROBLEM.
**    10/10/07   AY - REVISED FOR 2007 LAYOUT CHANGES.
**    01/30/08   AY - REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
**    11/17/09   AY - RE-COMPILED TO ACCOMMODATE 2009 IRS CHANGES TO
**                    'B', 'C', AND 'K' RECS.
**    10/01/10   AY - RE-COMPILED TO ACCOMMODATE 2010 IRS CHANGES TO
**                    'A' AND 'B' RECORDS.
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3549 extends BLNatBase
{
    // Data Areas
    private GdaTwrg3537 gdaTwrg3537;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwradcat pdaTwradcat;
    private PdaTwradist pdaTwradist;
    private PdaTwraform pdaTwraform;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratin pdaTwratin;
    private PdaTwra5052 pdaTwra5052;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;
    private LdaTwrl3508 ldaTwrl3508;
    private LdaTwrl3321 ldaTwrl3321;
    private LdaTwrl3507 ldaTwrl3507;
    private LdaTwrl3323 ldaTwrl3323;
    private LdaTwrl3324 ldaTwrl3324;
    private LdaTwrl3325 ldaTwrl3325;
    private LdaTwrl3326 ldaTwrl3326;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Tirf_Irs_Rpt_Ind;
    private DbsField form_U_Tirf_Irs_Rpt_Date;
    private DbsField form_U_Tirf_Link_Tin;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Tima;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_System_Year;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Form_Typ;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Err;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_S3_Link;
    private DbsField pnd_Ws_Pnd_S3_Start;
    private DbsField pnd_Ws_Pnd_S3_End;
    private DbsField pnd_Ws_Pnd_S4_Start;
    private DbsField pnd_Ws_Pnd_S4_End;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_Seq_Num;
    private DbsField pnd_Ws_Pnd_Corr_Idx;
    private DbsField pnd_Ws_Pnd_Temp_Seq;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Temp_Seq_N;
    private DbsField pnd_Ws_Pnd_Temp_Corr_Ind;
    private DbsField pnd_Ws_Pnd_New_Res;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_State_Dist;
    private DbsField pnd_Case_Fields_Pnd_Active_Isn;
    private DbsField pnd_Case_Fields_Pnd_Prior_Held;
    private DbsField pnd_Case_Fields_Pnd_Prior_Reported;
    private DbsField pnd_Case_Fields_Pnd_Prior_Record;
    private DbsField pnd_Case_Fields_Pnd_Latest_Form;
    private DbsField pnd_Case_Fields_Pnd_Two_Part_Tran;
    private DbsField pnd_Case_Fields_Pnd_No_Change;
    private DbsField pnd_Case_Fields_Pnd_Link_Tin;
    private DbsField pnd_Case_Fields_Pnd_No_States;
    private DbsField pnd_Case_Fields_Pnd_Recon_Summary;
    private DbsField pnd_Case_Fields_Pnd_Recon_Detail;
    private DbsField pnd_Case_Fields_Pnd_Irs_Reported;
    private DbsField pnd_Case_Fields_Pnd_St_Max;
    private DbsField pnd_Case_Fields_Pnd_Fed_Disp_Cv;
    private DbsField pnd_Case_Fields_Pnd_State_Disp_Cv;
    private DbsField pnd_Case_Fields_Pnd_Local_Disp_Cv;
    private DbsField pnd_Case_Fields_Pnd_Update_Ind;
    private DbsField pnd_Case_Fields_Pnd_Geo;
    private DbsField pnd_Case_Fields_Pnd_Link_Tin_Value;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaTwrg3537 = GdaTwrg3537.getInstance(getCallnatLevel());
        registerRecord(gdaTwrg3537);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwradcat = new PdaTwradcat(localVariables);
        pdaTwradist = new PdaTwradist(localVariables);
        pdaTwraform = new PdaTwraform(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwra5052 = new PdaTwra5052(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());
        ldaTwrl3508 = new LdaTwrl3508();
        registerRecord(ldaTwrl3508);
        ldaTwrl3321 = new LdaTwrl3321();
        registerRecord(ldaTwrl3321);
        ldaTwrl3507 = new LdaTwrl3507();
        registerRecord(ldaTwrl3507);
        ldaTwrl3323 = new LdaTwrl3323();
        registerRecord(ldaTwrl3323);
        ldaTwrl3324 = new LdaTwrl3324();
        registerRecord(ldaTwrl3324);
        ldaTwrl3325 = new LdaTwrl3325();
        registerRecord(ldaTwrl3325);
        ldaTwrl3326 = new LdaTwrl3326();
        registerRecord(ldaTwrl3326);

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Tirf_Irs_Rpt_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_U_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_U_Tirf_Irs_Rpt_Date = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_U_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form_U_Tirf_Link_Tin = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Link_Tin", "TIRF-LINK-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_LINK_TIN");
        form_U_Tirf_Link_Tin.setDdmHeader("LINK/TIN");
        registerRecord(vw_form_U);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Tima = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tima", "#TIMA", FieldType.STRING, 15);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE, new DbsArrayController(1, 
            2));
        pnd_Ws_Pnd_System_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_System_Year", "#SYSTEM-YEAR", FieldType.STRING, 4);
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Form_Typ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Typ", "#FORM-TYP", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Err = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Err", "#ERR", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S3_Link = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Link", "#S3-LINK", FieldType.STRING, 33);
        pnd_Ws_Pnd_S3_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_Start", "#S3-START", FieldType.STRING, 8);
        pnd_Ws_Pnd_S3_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S3_End", "#S3-END", FieldType.STRING, 8);
        pnd_Ws_Pnd_S4_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_Start", "#S4-START", FieldType.STRING, 32);
        pnd_Ws_Pnd_S4_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_End", "#S4-END", FieldType.STRING, 32);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_Seq_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Seq_Num", "#SEQ-NUM", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Pnd_Corr_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Corr_Idx", "#CORR-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Temp_Seq = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Seq", "#TEMP-SEQ", FieldType.STRING, 3);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Temp_Seq);
        pnd_Ws_Pnd_Temp_Seq_N = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Temp_Seq_N", "#TEMP-SEQ-N", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Temp_Corr_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Corr_Ind", "#TEMP-CORR-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_New_Res = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_New_Res", "#NEW-RES", FieldType.BOOLEAN, 1);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_State_Dist = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_State_Dist", "#STATE-DIST", FieldType.NUMERIC, 18, 2);
        pnd_Case_Fields_Pnd_Active_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Active_Isn", "#ACTIVE-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Case_Fields_Pnd_Prior_Held = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Held", "#PRIOR-HELD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Prior_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Reported", "#PRIOR-REPORTED", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Prior_Record = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Prior_Record", "#PRIOR-RECORD", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Latest_Form = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Latest_Form", "#LATEST-FORM", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Two_Part_Tran = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Two_Part_Tran", "#TWO-PART-TRAN", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_No_Change = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_Change", "#NO-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Link_Tin = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Link_Tin", "#LINK-TIN", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_No_States = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_No_States", "#NO-STATES", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Recon_Summary = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Summary", "#RECON-SUMMARY", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Recon_Detail = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Recon_Detail", "#RECON-DETAIL", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Irs_Reported = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Irs_Reported", "#IRS-REPORTED", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_St_Max = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_St_Max", "#ST-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Case_Fields_Pnd_Fed_Disp_Cv = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Fed_Disp_Cv", "#FED-DISP-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Case_Fields_Pnd_State_Disp_Cv = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_State_Disp_Cv", "#STATE-DISP-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Case_Fields_Pnd_Local_Disp_Cv = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Local_Disp_Cv", "#LOCAL-DISP-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Case_Fields_Pnd_Update_Ind = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Update_Ind", "#UPDATE-IND", FieldType.STRING, 1);
        pnd_Case_Fields_Pnd_Geo = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Geo", "#GEO", FieldType.STRING, 2);
        pnd_Case_Fields_Pnd_Link_Tin_Value = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Link_Tin_Value", "#LINK-TIN-VALUE", FieldType.STRING, 
            10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();

        ldaTwrl5001.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();
        ldaTwrl3508.initializeValues();
        ldaTwrl3321.initializeValues();
        ldaTwrl3507.initializeValues();
        ldaTwrl3323.initializeValues();
        ldaTwrl3324.initializeValues();
        ldaTwrl3325.initializeValues();
        ldaTwrl3326.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Case_Fields_Pnd_Fed_Disp_Cv.setInitialAttributeValue("AD=I");
        pnd_Case_Fields_Pnd_State_Disp_Cv.setInitialAttributeValue("AD=I");
        pnd_Case_Fields_Pnd_Local_Disp_Cv.setInitialAttributeValue("AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3549() throws Exception
    {
        super("Twrp3549");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3549|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 11 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 12 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 13 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 14 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 16 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 17 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 18 ) PS = 58 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                    //*  01/30/08 - AY
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'IRS Correction Reporting Summary' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 50T 'IRS Correction Reporting Summary' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS Correction Reporting Rejected Detail' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS Correction Reporting Rejected Detail' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS Correction Reporting Accepted Detail' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS Correction Reporting Accepted Detail' 120T 'Report: RPT7' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 42T 'IRS Correction Withholding Reconciliation Summary' 120T 'Report: RPT8' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 42T 'IRS Correction Withholding Reconciliation Summary' 120T 'Report: RPT9' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 10 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 10 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'IRS Correction Withholding Reconciliation Accepted Detail' 118T 'Report:  RPT10' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 11 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 11 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'IRS Correction Withholding Reconciliation Accepted Detail' 118T 'Report:  RPT11' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 12 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 12 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'IRS Correction Withholding Reconciliation Rejected Detail' 118T 'Report:  RPT12' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 13 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 13 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'IRS Correction Withholding Reconciliation Rejected Detail' 118T 'Report:  RPT13' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 14 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 14 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS General Ledger Reconciliation Summary' 118T 'Report:  RPT14' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS General Ledger Reconciliation Summary' 118T 'Report:  RPT15' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 16 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 16 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS General Ledger Reconciliation Detail' 118T 'Report:  RPT16' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 17 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 118T 'Page  :' *PAGE-NUMBER ( 17 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'IRS General Ledger Reconciliation Detail' 118T 'Report:  RPT17' / 43T 'Reporting period' #WS.#PREV-RPT-DATE ( #FORM-TYP ) ( EM = MM/DD/YYYY ) 'through' #WS.#DATX ( EM = MM/DD/YYYY ) / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYP ) / #WS.#COMPANY-LINE //
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Tax_Year,pnd_Ws_Pnd_System_Year,pnd_Ws_Pnd_Datx,pnd_Ws_Pnd_Tima,pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*")); //Natural: INPUT #WS.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                //*  01/30/08 - AY
                pnd_Ws_Pnd_Timx.setValueEdited(new ReportEditMask("MMDDYYYYHHIISST"),pnd_Ws_Pnd_Tima);                                                                    //Natural: MOVE EDITED #WS.#TIMA TO #WS.#TIMX ( EM = MMDDYYYYHHIISST )
                pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                     //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
                pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                                 //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
                pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                     //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #TWRADIST.#TAX-YEAR := #TWRACOM2.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwradist.getPnd_Twradist_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
                pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().reset();                                                                                                          //Natural: RESET TWRATBL2.#ABEND-IND TWRATBL2.#DISPLAY-IND #TWRADIST.#ABEND-IND #TWRADIST.#DISPLAY-IND
                pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().reset();
                pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().reset();
                pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().reset();
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
                sub_Load_State_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-PROVINCE-TABLE
                sub_Load_Province_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  01/30/08 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                pnd_Ws_Pnd_S3_Start.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                       //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #WS.#S3-START
                pnd_Ws_Pnd_S3_End.setValue(pnd_Ws_Pnd_S3_Start);                                                                                                          //Natural: ASSIGN #WS.#S3-END := #WS.#S3-START
                setValueToSubstring("A01",pnd_Ws_Pnd_S3_Start,5,3);                                                                                                       //Natural: MOVE 'A01' TO SUBSTR ( #WS.#S3-START,5,3 )
                setValueToSubstring("A02",pnd_Ws_Pnd_S3_End,5,3);                                                                                                         //Natural: MOVE 'A02' TO SUBSTR ( #WS.#S3-END,5,3 )
                setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S3_Start,8,1);                                                                                     //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S3-START,8,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S3_End,8,1);                                                                                      //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S3-END,8,1 )
                ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                //Natural: READ FORM BY TIRF-SUPERDE-3 = #WS.#S3-START THRU #WS.#S3-END
                (
                "RD_FORM",
                new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Ws_Pnd_S3_Start, "And", WcType.BY) ,
                new Wc("TIRF_SUPERDE_3", "<=", pnd_Ws_Pnd_S3_End, WcType.BY) },
                new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
                );
                boolean endOfDataRdForm = true;
                boolean firstRdForm = true;
                RD_FORM:
                while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
                {
                    CheckAtStartofData1562();

                    if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd_Form();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRdForm = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))                                                                                                 //Natural: AT START OF DATA;//Natural: AT BREAK OF FORM.TIRF-COMPANY-CDE;//Natural: AT BREAK OF FORM.TIRF-FORM-TYPE;//Natural: IF #WS.#COMPANY-BREAK
                    {
                        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                     //Natural: ASSIGN #TWRACOM2.#COMP-CODE := FORM.TIRF-COMPANY-CDE
                        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
                        sub_Process_Company();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                     //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
                        setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                              //Natural: MOVE #TWRACOM2.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
                        setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                            //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
                        setValueToSubstring(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                   //Natural: MOVE #TWRACOM2.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
                        pnd_Ws_Pnd_Company_Break.reset();                                                                                                                 //Natural: RESET #WS.#COMPANY-BREAK
                    }                                                                                                                                                     //Natural: END-IF
                    gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(1);                                                                                            //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                    sub_Accum_Irsr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  ALREADY RECONCILED
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().notEquals(" ")))                                                                                 //Natural: IF FORM.TIRF-IRS-RPT-IND NE ' '
                    {
                        //*  FOR GENERAL LEDGER
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG
                        sub_Accum_Irsg();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ALREADY REPORTED
                    if (condition(! (ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals(" ") || ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H"))))                          //Natural: IF NOT FORM.TIRF-IRS-RPT-IND = ' ' OR = 'H'
                    {
                        gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(2);                                                                                        //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 2
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean() && ldaTwrl9710.getForm_Tirf_Link_Tin().notEquals(" ")))                              //Natural: IF FORM.TIRF-EMPTY-FORM AND FORM.TIRF-LINK-TIN NE ' '
                    {
                        gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(5);                                                                                        //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields.resetInitial();                                                                                                                       //Natural: RESET INITIAL #CASE-FIELDS
                    pnd_Case_Fields_Pnd_Active_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                               //Natural: ASSIGN #CASE-FIELDS.#ACTIVE-ISN := *ISN ( RD-FORM. )
                    pnd_Ws_Pnd_S4_End.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());                                                                                     //Natural: ASSIGN #WS.#S4-END := #WS.#S4-START := FORM.TIRF-SUPERDE-4
                    pnd_Ws_Pnd_S4_Start.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());
                                                                                                                                                                          //Natural: PERFORM PRIOR-REPORTED
                    sub_Prior_Reported();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(ldaTwrl9710.getForm_Tirf_Link_Tin().notEquals(" ")))                                                                                    //Natural: IF FORM.TIRF-LINK-TIN NE ' '
                    {
                        pnd_Ws_Pnd_S4_Start.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());                                                                               //Natural: ASSIGN #WS.#S4-START := FORM.TIRF-SUPERDE-4
                        setValueToSubstring(ldaTwrl9710.getForm_Tirf_Link_Tin(),pnd_Ws_Pnd_S4_Start,5,10);                                                                //Natural: MOVE FORM.TIRF-LINK-TIN TO SUBSTR ( #WS.#S4-START,5,10 )
                        pnd_Ws_Pnd_S4_End.setValue(pnd_Ws_Pnd_S4_Start);                                                                                                  //Natural: ASSIGN #WS.#S4-END := #WS.#S4-START
                                                                                                                                                                          //Natural: PERFORM PRIOR-REPORTED
                        sub_Prior_Reported();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean() && ! (pnd_Case_Fields_Pnd_Prior_Reported.getBoolean())))                             //Natural: IF FORM.TIRF-EMPTY-FORM AND NOT #CASE-FIELDS.#PRIOR-REPORTED
                    {
                        gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(4);                                                                                        //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 4
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                        sub_Accum_Irsr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))                                                                                       //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED
                    {
                        if (condition(ldaTwrl9710.getForm_Tirf_Link_Tin().equals(" ")))                                                                                   //Natural: IF FORM.TIRF-LINK-TIN = ' '
                        {
                            if (condition(ldaTwrl9715.getForm_R_Tirf_Link_Tin().equals(" ")))                                                                             //Natural: IF FORM-R.TIRF-LINK-TIN = ' '
                            {
                                pnd_Case_Fields_Pnd_Link_Tin_Value.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                              //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN-VALUE := FORM.TIRF-TIN
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Case_Fields_Pnd_Link_Tin_Value.setValue(ldaTwrl9715.getForm_R_Tirf_Link_Tin());                                                       //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN-VALUE := FORM-R.TIRF-LINK-TIN
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Case_Fields_Pnd_Link_Tin_Value.setValue(ldaTwrl9710.getForm_Tirf_Link_Tin());                                                             //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN-VALUE := FORM.TIRF-LINK-TIN
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Case_Fields_Pnd_Link_Tin_Value.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                      //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN-VALUE := FORM.TIRF-TIN
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields_Pnd_Prior_Record.setValue(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean());                                                           //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := #CASE-FIELDS.#PRIOR-REPORTED
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                    sub_Setup_Recon_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                       //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                    {
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RECORDS IN ERROR
                    if (condition(ldaTwrl9710.getForm_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                 //Natural: IF FORM.TIRF-IRS-REJECT-IND = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-REJECT
                        sub_Irsr_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-REJECT
                        sub_Irsw_Detail_Reject();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  OLD REJECTS
                        if (condition(ldaTwrl9710.getForm_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                //Natural: IF FORM.TIRF-IRS-RPT-IND = 'H'
                        {
                            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(6);                                                                                    //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  FIRST TIME REJECT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(7);                                                                                    //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 7
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Case_Fields_Pnd_Update_Ind.setValue("H");                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'H'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
                            sub_Update_Form();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  1099-R
                    if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                        //Natural: IF FORM.TIRF-FORM-TYPE = 01
                    {
                        pnd_Case_Fields_Pnd_Irs_Reported.setValue(false);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#IRS-REPORTED := FALSE
                        FOR01:                                                                                                                                            //Natural: FOR #WS.#I = 1 TO FORM.C*TIRF-1099-R-STATE-GRP
                        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp())); pnd_Ws_Pnd_I.nadd(1))
                        {
                            if (condition(ldaTwrl9710.getForm_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))                                               //Natural: IF FORM.TIRF-STATE-IRS-RPT-IND ( #I ) = 'Y'
                            {
                                pnd_Case_Fields_Pnd_Irs_Reported.setValue(true);                                                                                          //Natural: ASSIGN #CASE-FIELDS.#IRS-REPORTED := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Case_Fields_Pnd_Irs_Reported.getBoolean()))                                                                                     //Natural: IF #CASE-FIELDS.#IRS-REPORTED
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(3);                                                                                    //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))                                                                                       //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED
                    {
                                                                                                                                                                          //Natural: PERFORM COMPARE-IRSR-RECORDS
                        sub_Compare_Irsr_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Case_Fields_Pnd_No_Change.getBoolean()))                                                                                        //Natural: IF #CASE-FIELDS.#NO-CHANGE
                        {
                            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(3);                                                                                    //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
                            sub_Accum_Irsr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Two_Part_Tran.getBoolean()))                                                                                        //Natural: IF #CASE-FIELDS.#TWO-PART-TRAN
                    {
                                                                                                                                                                          //Natural: PERFORM FORMAT-TWO-PART-TRAN
                        sub_Format_Two_Part_Tran();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-ONE-PART-TRAN
                    sub_Format_One_Part_Tran();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd_Form(endOfDataRdForm);
                }
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                if (condition(pnd_Ws_Pnd_Accepted_Form_Cnt.equals(getZero())))                                                                                            //Natural: IF #WS.#ACCEPTED-FORM-CNT = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new       //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'IRS Correction Reporting' 77T '***'
                        TabSetting(25),"IRS Correction Reporting",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Ws_Pnd_Terminate.setValue(105);                                                                                                                   //Natural: ASSIGN #WS.#TERMINATE := 105
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                    sub_Terminate_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-T-RECORD
                sub_Create_T_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-F-RECORD
                sub_Create_F_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ************************
                //*  S U B R O U T I N E S
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSR
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSW
                //* ***************************
                //* ***************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-IRSG1
                //* ****************************
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRIOR-REPORTED
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-IRSR-RECORDS
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-TWO-PART-TRAN
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ONE-PART-TRAN
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-B-RECORD
                //*  IRS-B-NAME-CONTROL := #TWRAFORM.TIRF-PART-LAST-NME
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-B-REC
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM
                //* ****************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-LINK-TIN-FORM
                //* *************************************
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORTS
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-ACCEPT
                //*    '//Tot/Dist'           IRS-B-TOTAL-DIST         (LC=�)
                //*    '%/of/Tot/Dist'        IRS-B-TOTAL-DIST-PCT     (LC=�)
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSR-DETAIL-REJECT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-REJECT
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSW-DETAIL-ACCEPT
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRSG-DETAIL
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DISTRIB
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DISTCAT
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-TIN
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-RECON-CASE
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-A-RECORDS
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-C-RECORDS
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-K-RECORDS
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T-RECORD
                //* ********************************
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-F-RECORD
                //* ********************************
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
                //* *********************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PROVINCE-TABLE
                //* ************************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Accum_Irsr() throws Exception                                                                                                                        //Natural: ACCUM-IRSR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(1);                                                         //Natural: ADD 1 TO #IRSR.#FORM-CNT ( #IRSR1,1 )
        gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                   //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSR.#FED-TAX ( #IRSR1,1 )
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                 //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSR.#GROSS-AMT ( #IRSR1,1 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                     //Natural: ADD FORM.TIRF-IVC-AMT TO #IRSR.#IVC-AMT ( #IRSR1,1 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());             //Natural: ADD FORM.TIRF-TAXABLE-AMT TO #IRSR.#TAXABLE-AMT ( #IRSR1,1 )
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO FORM.C*TIRF-1099-R-STATE-GRP
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(8) && ldaTwrl9710.getForm_Tirf_Res_Cmb_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))      //Natural: IF #IRSR1 = 8 AND FORM.TIRF-RES-CMB-IND ( #I ) = 'Y'
                {
                    gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(1).nadd(1);                                                                                      //Natural: ADD 1 TO #IRSR.#CMB-STATE-CNT ( 1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(1).nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( #I ) TO #IRSR.#CMB-STATE-TAX ( 1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(1).nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                        //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( #I ) TO #IRSR.#CMB-LOCAL-TAX ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(8) && ldaTwrl9710.getForm_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Ws_Pnd_I).equals("Y"))) //Natural: IF #IRSR1 = 8 AND FORM.TIRF-STATE-IRS-RPT-IND ( #I ) = 'Y'
                {
                    gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(9,1).nadd(1);                                                                                         //Natural: ADD 1 TO #IRSR.#FORM-CNT ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_I));                          //Natural: ADD FORM.TIRF-RES-GROSS-AMT ( #I ) TO #IRSR.#GROSS-AMT ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_Res_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                              //Natural: ADD FORM.TIRF-RES-IVC-AMT ( #I ) TO #IRSR.#IVC-AMT ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD FORM.TIRF-RES-TAXABLE-AMT ( #I ) TO #IRSR.#TAXABLE-AMT ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                        //Natural: ADD FORM.TIRF-RES-FED-TAX-WTHLD ( #I ) TO #IRSR.#FED-TAX ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                        //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( #I ) TO #IRSR.#STATE-TAX ( 9,1 )
                    gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue(9,1).nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I));                          //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( #I ) TO #IRSR.#LOCAL-TAX ( 9,1 )
                }                                                                                                                                                         //Natural: END-IF
                gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I)); //Natural: ADD FORM.TIRF-STATE-TAX-WTHLD ( #I ) TO #IRSR.#STATE-TAX ( #IRSR1,1 )
                gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I)); //Natural: ADD FORM.TIRF-LOC-TAX-WTHLD ( #I ) TO #IRSR.#LOCAL-TAX ( #IRSR1,1 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaTwrg3537.getPnd_Irsr_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                     //Natural: ADD FORM.TIRF-INT-AMT TO #IRSR.#INT-AMT ( #IRSR1,1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Irsw() throws Exception                                                                                                                        //Natural: ACCUM-IRSW
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2().compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2()), gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().add(pnd_Ws_Pnd_K)); //Natural: ASSIGN #REPORT-INDEXES.#IRSW2 := #REPORT-INDEXES.#IRSW1 + #WS.#K
            gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#FORM-CNT ( #K ) TO #IRSW.#FORM-CNT ( #IRSW2,1 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#FED-TAX ( #K ) TO #IRSW.#FED-TAX ( #IRSW2,1 )
            //*  1099-R
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                gdaTwrg3537.getPnd_Irsw_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#GROSS-AMT ( #K ) TO #IRSW.#GROSS-AMT ( #IRSW2,1 )
                gdaTwrg3537.getPnd_Irsw_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#IVC-AMT ( #K ) TO #IRSW.#IVC-AMT ( #IRSW2,1 )
                gdaTwrg3537.getPnd_Irsw_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#TAXABLE-AMT ( #K ) TO #IRSW.#TAXABLE-AMT ( #IRSW2,1 )
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaTwrg3537.getPnd_Irsw_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw2(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(pnd_Ws_Pnd_K)); //Natural: ADD #RECON-CASE.#INT-AMT ( #K ) TO #IRSW.#INT-AMT ( #IRSW2,1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Irsg() throws Exception                                                                                                                        //Natural: ACCUM-IRSG
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                                                                           //Natural: IF FORM.TIRF-FED-TAX-WTHLD = 0.00
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue(1,":",2,1).nadd(1);                                                                                               //Natural: ADD 1 TO #IRSG.#FORM-CNT ( 1:2,1 )
        gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                         //Natural: ADD FORM.TIRF-FED-TAX-WTHLD TO #IRSG.#FED-TAX ( 1:2,1 )
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt().getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                       //Natural: ADD FORM.TIRF-GROSS-AMT TO #IRSG.#GROSS-AMT ( 1:2,1 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt().getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                           //Natural: ADD FORM.TIRF-IVC-AMT TO #IRSG.#IVC-AMT ( 1:2,1 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt().getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                   //Natural: ADD FORM.TIRF-TAXABLE-AMT TO #IRSG.#TAXABLE-AMT ( 1:2,1 )
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt().getValue(1,":",2,1).nadd(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                           //Natural: ADD FORM.TIRF-INT-AMT TO #IRSG.#INT-AMT ( 1:2,1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Irsg1() throws Exception                                                                                                                       //Natural: ACCUM-IRSG1
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #REPORT-INDEXES.#IRSG1 = 1 TO 3
        for (gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().setValue(1); condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().lessOrEqual(3)); gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().nadd(1))
        {
            gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#FORM-CNT ( #IRSG1 ) TO #IRSG.#FORM-CNT ( #IRSG1,1 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#FED-TAX ( #IRSG1 ) TO #IRSG.#FED-TAX ( #IRSG1,1 )
            //*  1099-R
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#GROSS-AMT ( #IRSG1 ) TO #IRSG.#GROSS-AMT ( #IRSG1,1 )
                gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#IVC-AMT ( #IRSG1 ) TO #IRSG.#IVC-AMT ( #IRSG1,1 )
                gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#TAXABLE-AMT ( #IRSG1 ) TO #IRSG.#TAXABLE-AMT ( #IRSG1,1 )
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),1).nadd(gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1())); //Natural: ADD #RECON-CASE.#INT-AMT ( #IRSG1 ) TO #IRSG.#INT-AMT ( #IRSG1,1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Prior_Reported() throws Exception                                                                                                                    //Natural: PRIOR-REPORTED
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S4_Start,32,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S4-START,32,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S4_End,32,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S4-END,32,1 )
        ldaTwrl9715.getVw_form_R().startDatabaseRead                                                                                                                      //Natural: READ FORM-R BY FORM-R.TIRF-SUPERDE-4 = #WS.#S4-START THRU #WS.#S4-END WHERE FORM-R.TIRF-COMPANY-CDE = FORM.TIRF-COMPANY-CDE
        (
        "RD1_FORM",
        new Wc[] { new Wc("TIRF_COMPANY_CDE", "=", ldaTwrl9710.getForm_Tirf_Company_Cde(), WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_4", ">=", pnd_Ws_Pnd_S4_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_4", "<=", pnd_Ws_Pnd_S4_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") }
        );
        RD1_FORM:
        while (condition(ldaTwrl9715.getVw_form_R().readNextRow("RD1_FORM")))
        {
            if (condition(ldaTwrl9715.getForm_R_Tirf_Irs_Rpt_Ind().equals("H")))                                                                                          //Natural: IF FORM-R.TIRF-IRS-RPT-IND = 'H'
            {
                if (condition(! (pnd_Case_Fields_Pnd_Prior_Held.getBoolean())))                                                                                           //Natural: IF NOT #CASE-FIELDS.#PRIOR-HELD
                {
                    pnd_Case_Fields_Pnd_Prior_Held.setValue(true);                                                                                                        //Natural: ASSIGN #CASE-FIELDS.#PRIOR-HELD := TRUE
                    //*  INACTIVE
                    if (condition(ldaTwrl9715.getForm_R_Tirf_Active_Ind().equals(" ")))                                                                                   //Natural: IF FORM-R.TIRF-ACTIVE-IND = ' '
                    {
                        pnd_Case_Fields_Pnd_Prior_Record.setValue(true);                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#PRIOR-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM SETUP-RECON-CASE
                        sub_Setup_Recon_Case();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSG-DETAIL
                        sub_Irsg_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1_FORM"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Case_Fields_Pnd_Prior_Reported.setValue(true);                                                                                                        //Natural: ASSIGN #CASE-FIELDS.#PRIOR-REPORTED := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Compare_Irsr_Records() throws Exception                                                                                                              //Natural: COMPARE-IRSR-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 1
        {
            FOR05:                                                                                                                                                        //Natural: FOR #WS.#I = 1 TO FORM.C*TIRF-1099-R-STATE-GRP
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(ldaTwrl9710.getForm_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                           //Natural: IF FORM.TIRF-STATE-IRS-RPT-IND ( #I ) = 'Y'
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_Link_Tin().notEquals(" ")))                                                                                    //Natural: IF FORM.TIRF-LINK-TIN NE ' '
                    {
                        pnd_Case_Fields_Pnd_Two_Part_Tran.setValue(true);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#TWO-PART-TRAN := TRUE
                        pnd_Case_Fields_Pnd_Link_Tin.setValue(true);                                                                                                      //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Part_Last_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_Last_Nme()) || ldaTwrl9710.getForm_Tirf_Part_First_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_First_Nme())  //Natural: IF FORM.TIRF-PART-LAST-NME NE FORM-R.TIRF-PART-LAST-NME OR FORM.TIRF-PART-FIRST-NME NE FORM-R.TIRF-PART-FIRST-NME OR FORM.TIRF-PART-MDDLE-NME NE FORM-R.TIRF-PART-MDDLE-NME
                        || ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_Mddle_Nme())))
                    {
                        pnd_Case_Fields_Pnd_Two_Part_Tran.setValue(true);                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#TWO-PART-TRAN := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaTwrl9710.getForm_Tirf_Link_Tin().notEquals(" ")))                                                                                            //Natural: IF FORM.TIRF-LINK-TIN NE ' '
            {
                pnd_Case_Fields_Pnd_Two_Part_Tran.setValue(true);                                                                                                         //Natural: ASSIGN #CASE-FIELDS.#TWO-PART-TRAN := TRUE
                pnd_Case_Fields_Pnd_Link_Tin.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#LINK-TIN := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Part_Last_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_Last_Nme()) || ldaTwrl9710.getForm_Tirf_Part_First_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_First_Nme())  //Natural: IF FORM.TIRF-PART-LAST-NME NE FORM-R.TIRF-PART-LAST-NME OR FORM.TIRF-PART-FIRST-NME NE FORM-R.TIRF-PART-FIRST-NME OR FORM.TIRF-PART-MDDLE-NME NE FORM-R.TIRF-PART-MDDLE-NME
                    || ldaTwrl9710.getForm_Tirf_Part_Mddle_Nme().notEquals(ldaTwrl9715.getForm_R_Tirf_Part_Mddle_Nme())))
                {
                    pnd_Case_Fields_Pnd_Two_Part_Tran.setValue(true);                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#TWO-PART-TRAN := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9710.getForm_Tirf_Tax_Id_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || ldaTwrl9710.getForm_Tirf_Company_Cde().notEquals(ldaTwrl9715.getForm_R_Tirf_Company_Cde())  //Natural: IF FORM.TIRF-TAX-ID-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR FORM.TIRF-COMPANY-CDE NE FORM-R.TIRF-COMPANY-CDE OR FORM.TIRF-FOREIGN-ADDR NE FORM-R.TIRF-FOREIGN-ADDR OR FORM.TIRF-STREET-ADDR NE FORM-R.TIRF-STREET-ADDR OR FORM.TIRF-CITY NE FORM-R.TIRF-CITY OR FORM.TIRF-GEO-CDE NE FORM-R.TIRF-GEO-CDE OR FORM.TIRF-APO-GEO-CDE NE FORM-R.TIRF-APO-GEO-CDE OR FORM.TIRF-ZIP NE FORM-R.TIRF-ZIP OR FORM.TIRF-PROVINCE-CODE NE FORM-R.TIRF-PROVINCE-CODE OR FORM.TIRF-COUNTRY-NAME NE FORM-R.TIRF-COUNTRY-NAME OR FORM.TIRF-INT-AMT NE FORM-R.TIRF-INT-AMT OR FORM.TIRF-FED-TAX-WTHLD NE FORM-R.TIRF-FED-TAX-WTHLD
                || ldaTwrl9710.getForm_Tirf_Foreign_Addr().notEquals(ldaTwrl9715.getForm_R_Tirf_Foreign_Addr()) || ldaTwrl9710.getForm_Tirf_Street_Addr().notEquals(ldaTwrl9715.getForm_R_Tirf_Street_Addr()) 
                || ldaTwrl9710.getForm_Tirf_City().notEquals(ldaTwrl9715.getForm_R_Tirf_City()) || ldaTwrl9710.getForm_Tirf_Geo_Cde().notEquals(ldaTwrl9715.getForm_R_Tirf_Geo_Cde()) 
                || ldaTwrl9710.getForm_Tirf_Apo_Geo_Cde().notEquals(ldaTwrl9715.getForm_R_Tirf_Apo_Geo_Cde()) || ldaTwrl9710.getForm_Tirf_Zip().notEquals(ldaTwrl9715.getForm_R_Tirf_Zip()) 
                || ldaTwrl9710.getForm_Tirf_Province_Code().notEquals(ldaTwrl9715.getForm_R_Tirf_Province_Code()) || ldaTwrl9710.getForm_Tirf_Country_Name().notEquals(ldaTwrl9715.getForm_R_Tirf_Country_Name()) 
                || ldaTwrl9710.getForm_Tirf_Int_Amt().notEquals(ldaTwrl9715.getForm_R_Tirf_Int_Amt()) || ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld())))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Case_Fields_Pnd_No_Change.setValue(true);                                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#NO-CHANGE := TRUE
    }
    private void sub_Format_Two_Part_Tran() throws Exception                                                                                                              //Natural: FORMAT-TWO-PART-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9715.getVw_form_R());                                                                       //Natural: MOVE BY NAME FORM-R TO #TWRAFORM-DET
        pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp());                                                         //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM-R.C*TIRF-1099-R-STATE-GRP
        ldaTwrl3508.getPnd_Twrl3508().resetInitial();                                                                                                                     //Natural: RESET INITIAL #TWRL3508 IRS-B-REC
        ldaTwrl3507.getIrs_B_Rec().resetInitial();
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue("G");                                                                                                         //Natural: ASSIGN #TWRL3508.#CORR-IND := 'G'
                                                                                                                                                                          //Natural: PERFORM FORMAT-B-RECORD
        sub_Format_B_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(10,1).nadd(1);                                                                                                    //Natural: ADD 1 TO #IRSR.#FORM-CNT ( 10,1 )
    }
    private void sub_Format_One_Part_Tran() throws Exception                                                                                                              //Natural: FORMAT-ONE-PART-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pdaTwraform.getPnd_Twraform_Pnd_Twraform_Det().setValuesByName(ldaTwrl9710.getVw_form());                                                                         //Natural: MOVE BY NAME FORM TO #TWRAFORM-DET
        pdaTwraform.getPnd_Twraform_C_1099_R().setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                           //Natural: ASSIGN #TWRAFORM.C-1099-R := FORM.C*TIRF-1099-R-STATE-GRP
        ldaTwrl3508.getPnd_Twrl3508().resetInitial();                                                                                                                     //Natural: RESET INITIAL #TWRL3508 IRS-B-REC
        ldaTwrl3507.getIrs_B_Rec().resetInitial();
        short decideConditionsMet1971 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CASE-FIELDS.#TWO-PART-TRAN
        if (condition(pnd_Case_Fields_Pnd_Two_Part_Tran.getBoolean()))
        {
            decideConditionsMet1971++;
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue("C");                                                                                                     //Natural: ASSIGN #TWRL3508.#CORR-IND := 'C'
        }                                                                                                                                                                 //Natural: WHEN #CASE-FIELDS.#PRIOR-REPORTED
        else if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))
        {
            decideConditionsMet1971++;
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue("G");                                                                                                     //Natural: ASSIGN #TWRL3508.#CORR-IND := 'G'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  NEW CASE
            ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().reset();                                                                                                           //Natural: RESET #TWRL3508.#CORR-IND
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Case_Fields_Pnd_Latest_Form.setValue(true);                                                                                                                   //Natural: ASSIGN #CASE-FIELDS.#LATEST-FORM := TRUE
                                                                                                                                                                          //Natural: PERFORM FORMAT-B-RECORD
        sub_Format_B_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(8);                                                                                                        //Natural: ASSIGN #REPORT-INDEXES.#IRSR1 := 8
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSR
        sub_Accum_Irsr();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Format_B_Record() throws Exception                                                                                                                   //Natural: FORMAT-B-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  1099-R
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Form_Type().equals(1)))                                                                                            //Natural: IF #TWRAFORM.TIRF-FORM-TYPE = 1
        {
            pnd_Case_Fields_Pnd_State_Dist.reset();                                                                                                                       //Natural: RESET #CASE-FIELDS.#STATE-DIST IRS-B-REC.IRS-B-1099-R-AREA
            ldaTwrl3507.getIrs_B_Rec_Irs_B_1099_R_Area().reset();
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_1099_Int_Area().reset();                                                                                                       //Natural: RESET IRS-B-REC.IRS-B-1099-INT-AREA
        }                                                                                                                                                                 //Natural: END-IF
        //*  03/06/07
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Year().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tax_Year());                                                                      //Natural: MOVE #TWRAFORM.TIRF-TAX-YEAR TO IRS-B-YEAR
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year().setValue(ldaTwrl3507.getIrs_B_Rec_Irs_B_Year());                                                                       //Natural: ASSIGN #TWRL3508.#TAX-YEAR := IRS-B-YEAR
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pdaTwraform.getPnd_Twraform_Tirf_Company_Cde());                                                          //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := #TWRAFORM.TIRF-COMPANY-CDE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Form().setValue(pdaTwraform.getPnd_Twraform_Tirf_Form_Type());                                                                    //Natural: ASSIGN #TWRL3508.#FORM := #TWRAFORM.TIRF-FORM-TYPE
        pdaTwra5052.getPnd_Twra5052_Pnd_Lname().setValue(pdaTwraform.getPnd_Twraform_Tirf_Part_Last_Nme());                                                               //Natural: ASSIGN #TWRA5052.#LNAME := #TWRAFORM.TIRF-PART-LAST-NME
        //*  03/06/07
        DbsUtil.callnat(Twrn5052.class , getCurrentProcessState(), pdaTwra5052.getPnd_Twra5052());                                                                        //Natural: CALLNAT 'TWRN5052' #TWRA5052
        if (condition(Global.isEscape())) return;
        //*  05/01/07
        if (condition(pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #TWRA5052.#RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Abend In Name Control Routine 'TWRN5052'",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Return Message...:",pdaTwra5052.getPnd_Twra5052_Pnd_Ret_Msg(),new  //Natural: WRITE ( 00 ) '***' 06T 'Abend In Name Control Routine "TWRN5052"' 77T '***' / '***' 06T 'Return Message...:' #TWRA5052.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(93);  if (true) return;                                                                                                                     //Natural: TERMINATE 93
            //*  05/01/07
            //*  03/06/07
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Control().setValue(pdaTwra5052.getPnd_Twra5052_Pnd_Lname_Control());                                                          //Natural: ASSIGN IRS-B-NAME-CONTROL := #TWRA5052.#LNAME-CONTROL
        short decideConditionsMet2020 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #TWRAFORM.TIRF-TAX-ID-TYPE;//Natural: VALUE '1', '3'
        if (condition((pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("1") || pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("3"))))
        {
            decideConditionsMet2020++;
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type().setValue("2");                                                                                                      //Natural: ASSIGN IRS-B-TIN-TYPE := '2'
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                        //Natural: ASSIGN IRS-B-TIN := #TWRAFORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaTwraform.getPnd_Twraform_Tirf_Tax_Id_Type().equals("2"))))
        {
            decideConditionsMet2020++;
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type().setValue("1");                                                                                                      //Natural: ASSIGN IRS-B-TIN-TYPE := '1'
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tin());                                                                        //Natural: ASSIGN IRS-B-TIN := #TWRAFORM.TIRF-TIN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Case_Fields_Pnd_Link_Tin_Value.getSubstring(10,1).equals(" ")))                                                                                 //Natural: IF SUBSTR ( #CASE-FIELDS.#LINK-TIN-VALUE,10,1 ) = ' '
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee().setValue(pnd_Case_Fields_Pnd_Link_Tin_Value.getSubstring(6,4));                                               //Natural: MOVE SUBSTR ( #CASE-FIELDS.#LINK-TIN-VALUE, 6,4 ) TO IRS-B-CONTRACT-PAYEE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee().setValue(pnd_Case_Fields_Pnd_Link_Tin_Value.getSubstring(7,4));                                               //Natural: MOVE SUBSTR ( #CASE-FIELDS.#LINK-TIN-VALUE, 7,4 ) TO IRS-B-CONTRACT-PAYEE
        }                                                                                                                                                                 //Natural: END-IF
        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Contract_Nbr(),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),5,8);                                         //Natural: MOVE #TWRAFORM.TIRF-CONTRACT-NBR TO SUBSTR ( IRS-B-CONTRACT-PAYEE, 5,8 )
        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Payee_Cde(),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),13,2);                                           //Natural: MOVE #TWRAFORM.TIRF-PAYEE-CDE TO SUBSTR ( IRS-B-CONTRACT-PAYEE,13,2 )
        pnd_Ws_Pnd_Temp_Seq_N.setValue(ldaTwrl9710.getForm_Tirf_Irs_Form_Seq());                                                                                          //Natural: ASSIGN #WS.#TEMP-SEQ-N := FORM.TIRF-IRS-FORM-SEQ
        setValueToSubstring(pnd_Ws_Pnd_Temp_Seq,ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),15,3);                                                                    //Natural: MOVE #WS.#TEMP-SEQ TO SUBSTR ( IRS-B-CONTRACT-PAYEE,15,3 )
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1().setValue(DbsUtil.compress(pdaTwraform.getPnd_Twraform_Tirf_Part_Last_Nme(), pdaTwraform.getPnd_Twraform_Tirf_Part_First_Nme(),  //Natural: COMPRESS #TWRAFORM.TIRF-PART-LAST-NME #TWRAFORM.TIRF-PART-FIRST-NME #TWRAFORM.TIRF-PART-MDDLE-NME INTO IRS-B-NAME-LINE-1
            pdaTwraform.getPnd_Twraform_Tirf_Part_Mddle_Nme()));
        ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line().setValue(pdaTwraform.getPnd_Twraform_Tirf_Street_Addr());                                                           //Natural: ASSIGN IRS-B-ADDRESS-LINE := #TWRAFORM.TIRF-STREET-ADDR
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().equals("00")))                                                                                           //Natural: IF #TWRAFORM.TIRF-GEO-CDE = '00'
        {
            pnd_Case_Fields_Pnd_Geo.setValue(pdaTwraform.getPnd_Twraform_Tirf_Apo_Geo_Cde());                                                                             //Natural: ASSIGN #CASE-FIELDS.#GEO := #TWRAFORM.TIRF-APO-GEO-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().greaterOrEqual("01") && pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_State_Max_Alpha())  //Natural: IF #TWRAFORM.TIRF-GEO-CDE = '01' THRU #STATE-MAX-ALPHA AND #TWRAFORM.TIRF-GEO-CDE = MASK ( 99 )
                && DbsUtil.maskMatches(pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde(),"99")))
            {
                pnd_Ws_Pnd_I.compute(new ComputeParameters(false, pnd_Ws_Pnd_I), pdaTwraform.getPnd_Twraform_Tirf_Geo_Cde().val());                                       //Natural: ASSIGN #WS.#I := VAL ( #TWRAFORM.TIRF-GEO-CDE )
                pnd_Case_Fields_Pnd_Geo.setValue(gdaTwrg3537.getPnd_State_Table_Tircntl_State_Alpha_Code().getValue(pnd_Ws_Pnd_I.getInt() + 2));                          //Natural: ASSIGN #CASE-FIELDS.#GEO := #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Foreign_Addr().equals("Y")))                                                                                       //Natural: IF #TWRAFORM.TIRF-FOREIGN-ADDR = 'Y'
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind().setValue("1");                                                                                                   //Natural: ASSIGN IRS-B-FOREIGN-IND := '1'
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Province_Code().greaterOrEqual("074") && pdaTwraform.getPnd_Twraform_Tirf_Province_Code().lessOrEqual("088")   //Natural: IF #TWRAFORM.TIRF-PROVINCE-CODE = '074' THRU '088' AND #TWRAFORM.TIRF-PROVINCE-CODE = MASK ( 999 )
                && DbsUtil.maskMatches(pdaTwraform.getPnd_Twraform_Tirf_Province_Code(),"999")))
            {
                pnd_Ws_Pnd_I.compute(new ComputeParameters(false, pnd_Ws_Pnd_I), pdaTwraform.getPnd_Twraform_Tirf_Province_Code().val());                                 //Natural: ASSIGN #WS.#I := VAL ( #TWRAFORM.TIRF-PROVINCE-CODE )
                pdaTwraform.getPnd_Twraform_Tirf_Province_Code().setValue(gdaTwrg3537.getPnd_Prov_Table_Tircntl_State_Alpha_Code().getValue(pnd_Ws_Pnd_I));               //Natural: ASSIGN #TWRAFORM.TIRF-PROVINCE-CODE := #PROV-TABLE.TIRCNTL-STATE-ALPHA-CODE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3507.getIrs_B_Rec_Irs_B_City_State_Zip().setValue(DbsUtil.compress(pdaTwraform.getPnd_Twraform_Tirf_City(), pdaTwraform.getPnd_Twraform_Tirf_Province_Code(),  //Natural: COMPRESS #TWRAFORM.TIRF-CITY #TWRAFORM.TIRF-PROVINCE-CODE #TWRAFORM.TIRF-ZIP #TWRAFORM.TIRF-COUNTRY-NAME INTO IRS-B-CITY-STATE-ZIP
                pdaTwraform.getPnd_Twraform_Tirf_Zip(), pdaTwraform.getPnd_Twraform_Tirf_Country_Name()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_City().setValue(pdaTwraform.getPnd_Twraform_Tirf_City());                                                                      //Natural: ASSIGN IRS-B-CITY := #TWRAFORM.TIRF-CITY
            ldaTwrl3507.getIrs_B_Rec_Irs_B_State().setValue(pnd_Case_Fields_Pnd_Geo);                                                                                     //Natural: ASSIGN IRS-B-STATE := #CASE-FIELDS.#GEO
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip().setValue(pdaTwraform.getPnd_Twraform_Tirf_Zip());                                                                        //Natural: ASSIGN IRS-B-ZIP := #TWRAFORM.TIRF-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-R
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Form_Type().equals(1)))                                                                                            //Natural: IF #TWRAFORM.TIRF-FORM-TYPE = 1
        {
            if (condition(pdaTwraform.getPnd_Twraform_Tirf_Ira_Distr().equals("Y")))                                                                                      //Natural: IF #TWRAFORM.TIRF-IRA-DISTR = 'Y'
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Ira_Sep_Ind().setValue("1");                                                                                               //Natural: ASSIGN IRS-B-IRA-SEP-IND := '1'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(pdaTwraform.getPnd_Twraform_Tirf_Distribution_Cde())))                                      //Natural: IF #TWRADIST.#IN-DIST NE #TWRAFORM.TIRF-DISTRIBUTION-CDE
            {
                pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(pdaTwraform.getPnd_Twraform_Tirf_Distribution_Cde());                                                  //Natural: ASSIGN #TWRADIST.#IN-DIST := #TWRAFORM.TIRF-DISTRIBUTION-CDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTRIB
                sub_Process_Distrib();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Dist_Code().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                                              //Natural: ASSIGN IRS-B-DIST-CODE := #TWRADIST.#OUT-DIST
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9().setValue(pdaTwraform.getPnd_Twraform_Tirf_Tot_Contractual_Ivc());                                              //Natural: ASSIGN IRS-B-PAYMENT-AMT-9 := #TWRAFORM.TIRF-TOT-CONTRACTUAL-IVC
            FOR06:                                                                                                                                                        //Natural: FOR #I = 1 TO #TWRAFORM.C-1099-R
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaTwraform.getPnd_Twraform_C_1099_R())); pnd_Ws_Pnd_I.nadd(1))
            {
                pnd_Ws_Pnd_Temp_Corr_Ind.setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind());                                                                            //Natural: ASSIGN #WS.#TEMP-CORR-IND := IRS-B-CORR-IND := #TWRL3508.#CORR-IND
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind());
                if (condition(pdaTwraform.getPnd_Twraform_Tirf_State_Irs_Rpt_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                   //Natural: IF #TWRAFORM.TIRF-STATE-IRS-RPT-IND ( #I ) = 'Y'
                {
                    if (condition(pnd_Case_Fields_Pnd_Latest_Form.getBoolean()))                                                                                          //Natural: IF #CASE-FIELDS.#LATEST-FORM
                    {
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_I));                 //Natural: ASSIGN IRS-B-PAYMENT-AMT-1 := #TWRAFORM.TIRF-RES-GROSS-AMT ( #I )
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Taxable_Amt().getValue(pnd_Ws_Pnd_I));               //Natural: ASSIGN IRS-B-PAYMENT-AMT-2 := #TWRAFORM.TIRF-RES-TAXABLE-AMT ( #I )
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Fed_Tax_Wthld().getValue(pnd_Ws_Pnd_I));             //Natural: ASSIGN IRS-B-PAYMENT-AMT-4 := #TWRAFORM.TIRF-RES-FED-TAX-WTHLD ( #I )
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                   //Natural: ASSIGN IRS-B-PAYMENT-AMT-5 := #TWRAFORM.TIRF-RES-IVC-AMT ( #I )
                        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Ira_Distr().equals("Y")))                                                                          //Natural: IF #TWRAFORM.TIRF-IRA-DISTR = 'Y'
                        {
                            ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A().setValue(pdaTwraform.getPnd_Twraform_Tirf_Res_Gross_Amt().getValue(pnd_Ws_Pnd_I));             //Natural: ASSIGN IRS-B-PAYMENT-AMT-A := #TWRAFORM.TIRF-RES-GROSS-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Case_Fields_Pnd_Prior_Reported.getBoolean()))                                                                                   //Natural: IF #CASE-FIELDS.#PRIOR-REPORTED
                        {
                            pnd_Ws_Pnd_New_Res.setValue(true);                                                                                                            //Natural: ASSIGN #WS.#NEW-RES := TRUE
                            FOR07:                                                                                                                                        //Natural: FOR #J = 1 TO FORM-R.C*TIRF-1099-R-STATE-GRP
                            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(ldaTwrl9715.getForm_R_Count_Casttirf_1099_R_State_Grp())); 
                                pnd_Ws_Pnd_J.nadd(1))
                            {
                                if (condition(pdaTwraform.getPnd_Twraform_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).equals(ldaTwrl9715.getForm_R_Tirf_State_Code().getValue(pnd_Ws_Pnd_J))  //Natural: IF #TWRAFORM.TIRF-STATE-CODE ( #I ) = FORM-R.TIRF-STATE-CODE ( #J ) AND #TWRAFORM.TIRF-LOC-CODE ( #I ) = FORM-R.TIRF-LOC-CODE ( #J )
                                    && pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).equals(ldaTwrl9715.getForm_R_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_J))))
                                {
                                    pnd_Ws_Pnd_New_Res.reset();                                                                                                           //Natural: RESET #WS.#NEW-RES
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(pnd_Ws_Pnd_New_Res.getBoolean()))                                                                                               //Natural: IF #WS.#NEW-RES
                            {
                                ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind().reset();                                                                                        //Natural: RESET IRS-B-CORR-IND #TWRL3508.#CORR-IND
                                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().reset();
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).equals(" ")))                                                        //Natural: IF #TWRAFORM.TIRF-LOC-CODE ( #I ) = ' '
                    {
                        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_State_Code().getValue(pnd_Ws_Pnd_I),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),         //Natural: MOVE #TWRAFORM.TIRF-STATE-CODE ( #I ) TO SUBSTR ( IRS-B-CONTRACT-PAYEE,18,2 )
                            18,2);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        setValueToSubstring(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I),ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),           //Natural: MOVE #TWRAFORM.TIRF-LOC-CODE ( #I ) TO SUBSTR ( IRS-B-CONTRACT-PAYEE,18,3 )
                            18,3);
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Case_Fields_Pnd_St_Max.reset();                                                                                                                   //Natural: RESET #CASE-FIELDS.#ST-MAX
                    if (condition(pdaTwraform.getPnd_Twraform_Tirf_Res_Cmb_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                     //Natural: IF #TWRAFORM.TIRF-RES-CMB-IND ( #I ) = 'Y'
                    {
                        pnd_Case_Fields_Pnd_St_Max.nadd(1);                                                                                                               //Natural: ADD 1 TO #CASE-FIELDS.#ST-MAX
                        pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaTwraform.getPnd_Twraform_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).val());     //Natural: ASSIGN #WS.#J := VAL ( #TWRAFORM.TIRF-STATE-CODE ( #I ) )
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValueEdited(gdaTwrg3537.getPnd_State_Table_Tircntl_Seq_Nbr().getValue(pnd_Ws_Pnd_J.getInt() + 2),new  //Natural: MOVE EDITED #STATE-TABLE.TIRCNTL-SEQ-NBR ( #J ) ( EM = 99 ) TO IRS-B-FEDERAL-STATE-CODE
                            ReportEditMask("99"));
                        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().setValue(gdaTwrg3537.getPnd_State_Table_Tircntl_Seq_Nbr().getValue(pnd_Ws_Pnd_J.getInt()            //Natural: ASSIGN #TWRL3508.#IRS-B-STATE := #STATE-TABLE.TIRCNTL-SEQ-NBR ( #J )
                            + 2));
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Case_Fields_Pnd_Latest_Form.getBoolean()))                                                                                          //Natural: IF #CASE-FIELDS.#LATEST-FORM
                    {
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num().setValue(pdaTwraform.getPnd_Twraform_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I));               //Natural: ASSIGN IRS-B-STATE-TAX-NUM := #TWRAFORM.TIRF-STATE-TAX-WTHLD ( #I )
                        pnd_Case_Fields_Pnd_State_Dist.setValue(pdaTwraform.getPnd_Twraform_Tirf_State_Distr().getValue(pnd_Ws_Pnd_I));                                   //Natural: ASSIGN #CASE-FIELDS.#STATE-DIST := #TWRAFORM.TIRF-STATE-DISTR ( #I )
                        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).notEquals(" ") || pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I).notEquals(new  //Natural: IF #TWRAFORM.TIRF-LOC-CODE ( #I ) NE ' ' OR #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #I ) NE 0.00
                            DbsDecimal("0.00"))))
                        {
                            ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num().setValue(pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I));             //Natural: ASSIGN IRS-B-LOCAL-TAX-NUM := #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Case_Fields_Pnd_State_Dist.reset();                                                                                                           //Natural: RESET #CASE-FIELDS.#STATE-DIST IRS-B-STATE-TAX-NUM
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num().reset();
                        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Loc_Code().getValue(pnd_Ws_Pnd_I).notEquals(" ") || pdaTwraform.getPnd_Twraform_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I).notEquals(new  //Natural: IF #TWRAFORM.TIRF-LOC-CODE ( #I ) NE ' ' OR #TWRAFORM.TIRF-LOC-TAX-WTHLD ( #I ) NE 0.00
                            DbsDecimal("0.00"))))
                        {
                            ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num().reset();                                                                                       //Natural: RESET IRS-B-LOCAL-TAX-NUM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwraform.getPnd_Twraform_Tirf_Res_Taxable_Not_Det().getValue(pnd_Ws_Pnd_I).equals("Y")))                                             //Natural: IF #TWRAFORM.TIRF-RES-TAXABLE-NOT-DET ( #I ) = 'Y'
                    {
                        ldaTwrl3507.getIrs_B_Rec_Irs_B_Taxable_Not_Det().setValue("1");                                                                                   //Natural: ASSIGN IRS-B-TAXABLE-NOT-DET := '1'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-B-REC
                    sub_Write_B_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(pnd_Ws_Pnd_Temp_Corr_Ind);                                                                        //Natural: ASSIGN #TWRL3508.#CORR-IND := #WS.#TEMP-CORR-IND
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1().reset();                                                                                               //Natural: RESET IRS-B-PAYMENT-AMT-1 IRS-B-PAYMENT-AMT-2 IRS-B-PAYMENT-AMT-4 IRS-B-PAYMENT-AMT-5 IRS-B-PAYMENT-AMT-9 IRS-B-PAYMENT-AMT-A IRS-B-STATE-TAX-NUM IRS-B-LOCAL-TAX-NUM IRS-B-FEDERAL-STATE-CODE #IRS-B-STATE
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num().reset();
                    ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().reset();
                    ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind());                                                               //Natural: ASSIGN IRS-B-CORR-IND := #TWRL3508.#CORR-IND
            //*  TO LIMIT INDEX       06/29/07  RM
            if (condition(pnd_Ws_Pnd_I.lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_State_Max()) && gdaTwrg3537.getPnd_State_Table_Tircntl_Comb_Fed_Ind().getValue(pnd_Ws_Pnd_I.getInt()  //Natural: IF #I LE #STATE-MAX AND #STATE-TABLE.TIRCNTL-COMB-FED-IND ( #I ) = 'C'
                + 2).equals("C")))
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().setValueEdited(gdaTwrg3537.getPnd_State_Table_Tircntl_Seq_Nbr().getValue(pnd_Ws_Pnd_I.getInt() + 2),new  //Natural: MOVE EDITED #STATE-TABLE.TIRCNTL-SEQ-NBR ( #I ) ( EM = 99 ) TO IRS-B-FEDERAL-STATE-CODE
                    ReportEditMask("99"));
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().setValue(gdaTwrg3537.getPnd_State_Table_Tircntl_Seq_Nbr().getValue(pnd_Ws_Pnd_I.getInt()                    //Natural: ASSIGN #TWRL3508.#IRS-B-STATE := #STATE-TABLE.TIRCNTL-SEQ-NBR ( #I )
                    + 2));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Case_Fields_Pnd_Latest_Form.getBoolean()))                                                                                                  //Natural: IF #CASE-FIELDS.#LATEST-FORM
            {
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1().setValue(pdaTwraform.getPnd_Twraform_Tirf_Int_Amt());                                                      //Natural: ASSIGN IRS-B-PAYMENT-AMT-1 := #TWRAFORM.TIRF-INT-AMT
                ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4().setValue(pdaTwraform.getPnd_Twraform_Tirf_Fed_Tax_Wthld());                                                //Natural: ASSIGN IRS-B-PAYMENT-AMT-4 := #TWRAFORM.TIRF-FED-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-B-REC
            sub_Write_B_Rec();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Latest_Form.getBoolean()))                                                                                                      //Natural: IF #CASE-FIELDS.#LATEST-FORM
        {
            if (condition(pnd_Case_Fields_Pnd_Link_Tin.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#LINK-TIN
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-LINK-TIN-FORM
                sub_Update_Link_Tin_Form();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Case_Fields_Pnd_Update_Ind.setValue("C");                                                                                                                 //Natural: ASSIGN #CASE-FIELDS.#UPDATE-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM
            sub_Update_Form();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM IRSW-DETAIL-ACCEPT
            sub_Irsw_Detail_Accept();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_B_Rec() throws Exception                                                                                                                       //Natural: WRITE-B-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Ws_Pnd_Seq_Num.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WS.#SEQ-NUM
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().setValue(pnd_Ws_Pnd_Seq_Num);                                                                                           //Natural: ASSIGN #TWRL3508.#SEQ-NUM := #WS.#SEQ-NUM
        getWorkFiles().write(1, false, ldaTwrl3507.getIrs_B_Rec(), ldaTwrl3508.getPnd_Twrl3508());                                                                        //Natural: WRITE WORK FILE 1 IRS-B-REC #TWRL3508
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_B_State().reset();                                                                                                            //Natural: RESET #TWRL3508.#IRS-B-STATE
                                                                                                                                                                          //Natural: PERFORM IRSR-DETAIL-ACCEPT
        sub_Irsr_Detail_Accept();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        short decideConditionsMet2159 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #TWRL3508.#CORR-IND;//Natural: VALUE ' '
        if (condition((ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().equals(" "))))
        {
            decideConditionsMet2159++;
            pnd_Ws_Pnd_Corr_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #WS.#CORR-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().equals("C"))))
        {
            decideConditionsMet2159++;
            pnd_Ws_Pnd_Corr_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #WS.#CORR-IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'G'
        else if (condition((ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().equals("G"))))
        {
            decideConditionsMet2159++;
            pnd_Ws_Pnd_Corr_Idx.setValue(3);                                                                                                                              //Natural: ASSIGN #WS.#CORR-IDX := 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        gdaTwrg3537.getPnd_A_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);                                                                             //Natural: ASSIGN #A-REC.#GENERATE ( #CORR-IDX ) := #C-REC.#GENERATE ( #CORR-IDX ) := TRUE
        gdaTwrg3537.getPnd_C_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);
        gdaTwrg3537.getPnd_C_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(1);                                                                                  //Natural: ADD 1 TO #C-REC.#NUM-PAYEES ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_1().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                                          //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #C-REC.#AMT-1 ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_2().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());                                          //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #C-REC.#AMT-2 ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_4().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());                                          //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #C-REC.#AMT-4 ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_5().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());                                          //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #C-REC.#AMT-5 ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_9().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());                                          //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #C-REC.#AMT-9 ( #CORR-IDX )
        gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_A().getValue(pnd_Ws_Pnd_Corr_Idx).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());                                          //Natural: ADD IRS-B-PAYMENT-AMT-A TO #C-REC.#AMT-A ( #CORR-IDX )
        //* *IF      #TWRL3508.#FORM            = 01            /* 1099-R
        if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().notEquals(" ")))                                                                                //Natural: IF IRS-B-FEDERAL-STATE-CODE NE ' '
        {
            gdaTwrg3537.getPnd_K_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_Corr_Idx).setValue(true);                                                                         //Natural: ASSIGN #K-REC.#GENERATE ( #CORR-IDX ) := TRUE
            pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code().val());                                  //Natural: ASSIGN #WS.#J := VAL ( IRS-B-FEDERAL-STATE-CODE )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(1);                                                                 //Natural: ADD 1 TO #K-REC.#NUM-PAYEES ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_1().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1());                         //Natural: ADD IRS-B-PAYMENT-AMT-1 TO #K-REC.#AMT-1 ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_2().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2());                         //Natural: ADD IRS-B-PAYMENT-AMT-2 TO #K-REC.#AMT-2 ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_4().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4());                         //Natural: ADD IRS-B-PAYMENT-AMT-4 TO #K-REC.#AMT-4 ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_5().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5());                         //Natural: ADD IRS-B-PAYMENT-AMT-5 TO #K-REC.#AMT-5 ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_9().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9());                         //Natural: ADD IRS-B-PAYMENT-AMT-9 TO #K-REC.#AMT-9 ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_A().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A());                         //Natural: ADD IRS-B-PAYMENT-AMT-A TO #K-REC.#AMT-A ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_State_Tax().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num());                     //Natural: ADD IRS-B-STATE-TAX-NUM TO #K-REC.#STATE-TAX ( #CORR-IDX,#J )
            gdaTwrg3537.getPnd_K_Rec_Pnd_Local_Tax().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_J).nadd(ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num());                     //Natural: ADD IRS-B-LOCAL-TAX-NUM TO #K-REC.#LOCAL-TAX ( #CORR-IDX,#J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Form() throws Exception                                                                                                                       //Natural: UPDATE-FORM
    {
        if (BLNatReinput.isReinput()) return;

        G_FORM:                                                                                                                                                           //Natural: GET FORM-U #CASE-FIELDS.#ACTIVE-ISN
        vw_form_U.readByID(pnd_Case_Fields_Pnd_Active_Isn.getLong(), "G_FORM");
        form_U_Tirf_Irs_Rpt_Ind.setValue(pnd_Case_Fields_Pnd_Update_Ind);                                                                                                 //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := #CASE-FIELDS.#UPDATE-IND
        form_U_Tirf_Irs_Rpt_Date.setValue(pnd_Ws_Pnd_Datx);                                                                                                               //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #WS.#DATX
        form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                              //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
        form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
        form_U_Tirf_Link_Tin.setValue(pnd_Case_Fields_Pnd_Link_Tin_Value);                                                                                                //Natural: ASSIGN FORM-U.TIRF-LINK-TIN := #CASE-FIELDS.#LINK-TIN-VALUE
        vw_form_U.updateDBRow("G_FORM");                                                                                                                                  //Natural: UPDATE ( G-FORM. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Link_Tin_Form() throws Exception                                                                                                              //Natural: UPDATE-LINK-TIN-FORM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_S3_Link.setValue(ldaTwrl9710.getForm_Tirf_Superde_3());                                                                                                //Natural: ASSIGN #WS.#S3-LINK := FORM.TIRF-SUPERDE-3
        setValueToSubstring(ldaTwrl9710.getForm_Tirf_Link_Tin(),pnd_Ws_Pnd_S3_Link,9,10);                                                                                 //Natural: MOVE FORM.TIRF-LINK-TIN TO SUBSTR ( #WS.#S3-LINK,9,10 )
        vw_form_U.startDatabaseFind                                                                                                                                       //Natural: FIND ( 1 ) FORM-U WITH FORM-U.TIRF-SUPERDE-3 = #WS.#S3-LINK
        (
        "F_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_3", "=", pnd_Ws_Pnd_S3_Link, WcType.WITH) },
        1
        );
        F_FORM:
        while (condition(vw_form_U.readNextRow("F_FORM")))
        {
            vw_form_U.setIfNotFoundControlFlag(false);
            form_U_Tirf_Irs_Rpt_Ind.setValue("L");                                                                                                                        //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'L'
            form_U_Tirf_Irs_Rpt_Date.setValue(pnd_Ws_Pnd_Datx);                                                                                                           //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #WS.#DATX
            form_U_Tirf_Lu_User.setValue(Global.getINIT_USER());                                                                                                          //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
            form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                  //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#TIMX
            vw_form_U.updateDBRow("F_FORM");                                                                                                                              //Natural: UPDATE ( F-FORM. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Reports() throws Exception                                                                                                                   //Natural: SUMMARY-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  1099-R
        if (condition(pnd_Ws_Pnd_Form_Typ.equals(1)))                                                                                                                     //Natural: IF #WS.#FORM-TYP = 01
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 6 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(8));                                                                                                             //Natural: NEWPAGE ( 8 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(10));                                                                                                            //Natural: NEWPAGE ( 10 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(12));                                                                                                            //Natural: NEWPAGE ( 12 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(14));                                                                                                            //Natural: NEWPAGE ( 14 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(16));                                                                                                            //Natural: NEWPAGE ( 16 )
            if (condition(Global.isEscape())){return;}
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(5));                                                                                                             //Natural: NEWPAGE ( 5 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(7));                                                                                                             //Natural: NEWPAGE ( 7 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(9));                                                                                                             //Natural: NEWPAGE ( 9 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(11));                                                                                                            //Natural: NEWPAGE ( 11 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(13));                                                                                                            //Natural: NEWPAGE ( 13 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(15));                                                                                                            //Natural: NEWPAGE ( 15 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(17));                                                                                                            //Natural: NEWPAGE ( 17 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #IRSR1 = 1 TO #IRSR-MAX-LINES
        for (gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().setValue(1); condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_Irsr_Max_Lines())); 
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().nadd(1))
        {
            //*  1099-R
            if (condition(pnd_Ws_Pnd_Form_Typ.equals(1)))                                                                                                                 //Natural: IF #WS.#FORM-TYP = 01
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(8)))                                                                                   //Natural: IF #IRSR1 = 8
                {
                    getReports().write(2, "=",new RepeatItem(132));                                                                                                       //Natural: WRITE ( 2 ) '=' ( 132 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 2 ) ( ES = ON HC = R ) '/' #IRSR.#HEADER1 ( #IRSR1 ) / '/' #IRSR.#HEADER2 ( #IRSR1 ) 'Form/Count' #IRSR.#FORM-CNT ( #IRSR1,#S2 ) '/Gross Amount' #IRSR.#GROSS-AMT ( #IRSR1,#S2 ) '/IVC Amount' #IRSR.#IVC-AMT ( #IRSR1,#S2 ) '/Taxable Amount' #IRSR.#TAXABLE-AMT ( #IRSR1,#S2 ) 'Federal/Withholding' #IRSR.#FED-TAX ( #IRSR1,#S2 ) 'State/Withholding' #IRSR.#STATE-TAX ( #IRSR1,#S2 ) 'Local/Withholding' #IRSR.#LOCAL-TAX ( #IRSR1,#S2 )
                		gdaTwrg3537.getPnd_Irsr_Pnd_Header1().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1()),NEWLINE,"/",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Header2().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"/Gross Amount",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"/IVC Amount",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"/Taxable Amount",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"Federal/Withholding",
                    
                		gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"State/Withholding",
                		gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"Local/Withholding",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(8)))                                                                                   //Natural: IF #IRSR1 = 8
                {
                    getReports().write(2, "                     Number Of B Recs");                                                                                       //Natural: WRITE ( 2 ) '                     Number Of B Recs'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(2, new ReportTAsterisk(gdaTwrg3537.getPnd_Irsr_Pnd_Header1().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1())),"Cmb Program States  ",gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(pnd_Ws_Pnd_S2),new  //Natural: WRITE ( 2 ) T*#IRSR.#HEADER1 ( #IRSR1 ) 'Cmb Program States  ' #IRSR.#CMB-STATE-CNT ( #S2 ) T*#IRSR.#STATE-TAX ( #IRSR1,#S2 ) #IRSR.#CMB-STATE-TAX ( #S2 ) T*#IRSR.#LOCAL-TAX ( #IRSR1,#S2 ) #IRSR.#CMB-LOCAL-TAX ( #S2 )
                        ReportTAsterisk(gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2)),gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(pnd_Ws_Pnd_S2), 
                        new ReportEditMask ("-ZZZZZZ,ZZ9.99"),new ReportTAsterisk(gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2)),gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(pnd_Ws_Pnd_S2), 
                        new ReportEditMask ("-ZZZZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(8)))                                                                                   //Natural: IF #IRSR1 = 8
                {
                    getReports().write(3, "=",new RepeatItem(61));                                                                                                        //Natural: WRITE ( 3 ) '=' ( 61 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(3, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 3 ) ( ES = ON HC = R ) '/' #IRSR.#HEADER1 ( #IRSR1 ) / '/' #IRSR.#HEADER2 ( #IRSR1 ) 'Form/Count' #IRSR.#FORM-CNT ( #IRSR1,#S2 ) 'Interest/Amount' #IRSR.#INT-AMT ( #IRSR1,#S2 ) 'Backup/Withholding' #IRSR.#FED-TAX ( #IRSR1,#S2 )
                		gdaTwrg3537.getPnd_Irsr_Pnd_Header1().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1()),NEWLINE,"/",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Header2().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"Interest/Amount",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2),"Backup/Withholding",
                		gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #IRSW1 = 1 TO #IRSW-MAX-LINES
        for (gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().setValue(1); condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_Irsw_Max_Lines())); 
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().nadd(1))
        {
            //*  1099-R
            if (condition(pnd_Ws_Pnd_Form_Typ.equals(1)))                                                                                                                 //Natural: IF #WS.#FORM-TYP = 01
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(3) || gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(6)))                        //Natural: IF #IRSW1 = 3 OR = 6
                {
                    getReports().write(8, "=",new RepeatItem(96));                                                                                                        //Natural: WRITE ( 8 ) '=' ( 96 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 8 ) ( ES = ON HC = R ) '/' #IRSW.#HEADER ( #IRSW1 ) 'Form/Count' #IRSW.#FORM-CNT ( #IRSW1,#S2 ) '/Gross Amount' #IRSW.#GROSS-AMT ( #IRSW1,#S2 ) '/IVC Amount' #IRSW.#IVC-AMT ( #IRSW1,#S2 ) '/Taxable Amount' #IRSW.#TAXABLE-AMT ( #IRSW1,#S2 ) 'Federal/Withholding' #IRSW.#FED-TAX ( #IRSW1,#S2 )
                		gdaTwrg3537.getPnd_Irsw_Pnd_Header().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"/Gross Amount",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"/IVC Amount",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"/Taxable Amount",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"Federal/Withholding",
                    
                		gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(3)))                                                                                   //Natural: IF #IRSW1 = 3
                {
                    getReports().skip(8, 4);                                                                                                                              //Natural: SKIP ( 8 ) 4 LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(3) || gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(6)))                        //Natural: IF #IRSW1 = 3 OR = 6
                {
                    getReports().write(9, "=",new RepeatItem(56));                                                                                                        //Natural: WRITE ( 9 ) '=' ( 56 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(9, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                     //Natural: DISPLAY ( 9 ) ( ES = ON HC = R ) '/' #IRSW.#HEADER ( #IRSW1 ) 'Form/Count' #IRSW.#FORM-CNT ( #IRSW1,#S2 ) 'Interest/Amount' #IRSW.#INT-AMT ( #IRSW1,#S2 ) 'Backup/Withholding' #IRSW.#FED-TAX ( #IRSW1,#S2 )
                		gdaTwrg3537.getPnd_Irsw_Pnd_Header().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"Interest/Amount",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2),"Backup/Withholding",
                		gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().equals(3)))                                                                                   //Natural: IF #IRSW1 = 3
                {
                    getReports().skip(9, 4);                                                                                                                              //Natural: SKIP ( 9 ) 4 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #IRSG1 = 1 TO #IRSG-MAX-LINES
        for (gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().setValue(1); condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_Irsg_Max_Lines())); 
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().nadd(1))
        {
            //*  1099-R
            if (condition(pnd_Ws_Pnd_Form_Typ.equals(1)))                                                                                                                 //Natural: IF #WS.#FORM-TYP = 01
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().equals(3)))                                                                                   //Natural: IF #IRSG1 = 3
                {
                    getReports().write(14, "=",new RepeatItem(101));                                                                                                      //Natural: WRITE ( 14 ) '=' ( 101 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(14, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                    //Natural: DISPLAY ( 14 ) ( ES = ON HC = R ) '/' #IRSG.#HEADER ( #IRSG1 ) 'Form/Count' #IRSG.#FORM-CNT ( #IRSG1,#S2 ) '/Gross Amount' #IRSG.#GROSS-AMT ( #IRSG1,#S2 ) '/IVC Amount' #IRSG.#IVC-AMT ( #IRSG1,#S2 ) '/Taxable Amount' #IRSG.#TAXABLE-AMT ( #IRSG1,#S2 ) 'Federal/Withholding' #IRSG.#FED-TAX ( #IRSG1,#S2 )
                		gdaTwrg3537.getPnd_Irsg_Pnd_Header().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"/Gross Amount",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"/IVC Amount",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"/Taxable Amount",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"Federal/Withholding",
                    
                		gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsr1().equals(3)))                                                                                   //Natural: IF #IRSR1 = 3
                {
                    getReports().skip(14, 4);                                                                                                                             //Natural: SKIP ( 14 ) 4 LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1().equals(3)))                                                                                   //Natural: IF #IRSG1 = 3
                {
                    getReports().write(15, "=",new RepeatItem(61));                                                                                                       //Natural: WRITE ( 15 ) '=' ( 61 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(15, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",                    //Natural: DISPLAY ( 15 ) ( ES = ON HC = R ) '/' #IRSG.#HEADER ( #IRSG1 ) 'Form/Count' #IRSG.#FORM-CNT ( #IRSG1,#S2 ) 'Interest/Amount' #IRSG.#INT-AMT ( #IRSG1,#S2 ) 'Backup/Withholding' #IRSG.#FED-TAX ( #IRSG1,#S2 )
                		gdaTwrg3537.getPnd_Irsg_Pnd_Header().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1()),"Form/Count",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"Interest/Amount",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2),"Backup/Withholding",
                		gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue(gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsg1(),pnd_Ws_Pnd_S2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Irsr_Detail_Accept() throws Exception                                                                                                                //Natural: IRSR-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  1099-R
        if (condition(pdaTwraform.getPnd_Twraform_Tirf_Form_Type().equals(1)))                                                                                            //Natural: IF #TWRAFORM.TIRF-FORM-TYPE = 01
        {
            if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax().equals(" ")))                                                                                        //Natural: IF IRS-B-STATE-TAX = ' '
            {
                pnd_Case_Fields_Pnd_State_Disp_Cv.setValue("AD=N");                                                                                                       //Natural: ASSIGN #CASE-FIELDS.#STATE-DISP-CV := ( AD = N )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Case_Fields_Pnd_State_Disp_Cv.resetInitial();                                                                                                         //Natural: RESET INITIAL #CASE-FIELDS.#STATE-DISP-CV
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax().equals(" ")))                                                                                        //Natural: IF IRS-B-LOCAL-TAX = ' '
            {
                pnd_Case_Fields_Pnd_Local_Disp_Cv.setValue("AD=N");                                                                                                       //Natural: ASSIGN #CASE-FIELDS.#LOCAL-DISP-CV := ( AD = N )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Case_Fields_Pnd_Local_Disp_Cv.resetInitial();                                                                                                         //Natural: RESET INITIAL #CASE-FIELDS.#LOCAL-DISP-CV
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(6, "//Cor/Ind",                                                                                                                          //Natural: DISPLAY ( 6 ) '//Cor/Ind' IRS-B-CORR-IND ( LC = � ) '//TIN/Typ' IRS-B-TIN-TYPE ( LC = � ) '///TIN' IRS-B-TIN '///Account' IRS-B-CONTRACT-PAYEE '//Dist/Code' IRS-B-DIST-CODE ( LC = � ) 'F/o/r/n' IRS-B-FOREIGN-IND '/Name' IRS-B-NAME-LINE-1 ( HC = L ) / 'Address' IRS-B-ADDRESS-LINE ( HC = L ) / '/' IRS-B-CITY ( HC = L ) / '/' IRS-B-STATE ( HC = L ) / '/' IRS-B-ZIP ( HC = L ) '/I/R/A' IRS-B-IRA-SEP-IND 'T/x/n/d' IRS-B-TAXABLE-NOT-DET '(1)' '///IRS Amounts' IRS-B-PAYMENT-AMT-1 ( HC = R ) / '(2)' '/' IRS-B-PAYMENT-AMT-2 ( HC = R ) / '(4)' '/' IRS-B-PAYMENT-AMT-4 ( HC = R ) / '(5)' '/' IRS-B-PAYMENT-AMT-5 ( HC = R ) / '(9)' '/' IRS-B-PAYMENT-AMT-9 ( HC = R ) / '(A)' '/' IRS-B-PAYMENT-AMT-A ( HC = R ) '/IRS/State/Code' IRS-B-FEDERAL-STATE-CODE ( LC = � HC = L ) '//State Tax' IRS-B-STATE-TAX-NUM ( CV = #STATE-DISP-CV HC = R ) / 'Local Tax' IRS-B-LOCAL-TAX-NUM ( CV = #LOCAL-DISP-CV HC = R )
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind(), new FieldAttributes("LC=�"),"//TIN/Typ",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type(), new FieldAttributes("LC=�"),"///TIN",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin(),"///Account",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),"//Dist/Code",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Dist_Code(), new FieldAttributes("LC=�"),"F/o/r/n",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind(),"/Name",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_City(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_State(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/I/R/A",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Ira_Sep_Ind(),"T/x/n/d",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Taxable_Not_Det(),"(1)",
            		"///IRS Amounts",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(2)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(4)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(5)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(9)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(A)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/IRS/State/Code",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code(), new FieldAttributes("LC=�"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
                "//State Tax",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                NEWLINE,"Local Tax",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num(), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1 LINES
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(7, "//Cor/Ind",                                                                                                                          //Natural: DISPLAY ( 7 ) '//Cor/Ind' IRS-B-CORR-IND ( LC = � ) '//TIN/Typ' IRS-B-TIN-TYPE ( LC = � ) '///TIN' IRS-B-TIN '///Account' IRS-B-CONTRACT-PAYEE 'F/O/R/N' IRS-B-FOREIGN-IND '//Name' IRS-B-NAME-LINE-1 ( HC = L ) / 'Address' IRS-B-ADDRESS-LINE ( HC = L ) / '/' IRS-B-CITY ( HC = L ) / '/' IRS-B-STATE ( HC = L ) / '/' IRS-B-ZIP ( HC = L ) '(1)' '///IRS Amounts' IRS-B-PAYMENT-AMT-1 ( HC = R ) / '(4)' '/' IRS-B-PAYMENT-AMT-4 ( HC = R )
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind(), new FieldAttributes("LC=�"),"//TIN/Typ",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type(), new FieldAttributes("LC=�"),"///TIN",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin(),"///Account",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),"F/O/R/N",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind(),"//Name",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_City(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_State(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"(1)",
            		"///IRS Amounts",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(4)",
            		"/",
            		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsr_Detail_Reject() throws Exception                                                                                                                //Natural: IRSR-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(ldaTwrl9710.getForm_Tirf_Distribution_Cde())))                                              //Natural: IF #TWRADIST.#IN-DIST NE FORM.TIRF-DISTRIBUTION-CDE
            {
                pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                          //Natural: ASSIGN #TWRADIST.#IN-DIST := FORM.TIRF-DISTRIBUTION-CDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTRIB
                sub_Process_Distrib();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().notEquals(ldaTwrl9710.getForm_Tirf_Distr_Category())))                                              //Natural: IF #TWRADCAT.#DCAT-CODE NE FORM.TIRF-DISTR-CATEGORY
            {
                pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().setValue(ldaTwrl9710.getForm_Tirf_Distr_Category());                                                          //Natural: ASSIGN #TWRADCAT.#DCAT-CODE := FORM.TIRF-DISTR-CATEGORY
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTCAT
                sub_Process_Distcat();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp().equals(getZero())))                                                                       //Natural: IF FORM.C*TIRF-1099-R-STATE-GRP = 0
            {
                pnd_Case_Fields_Pnd_No_States.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#NO-STATES := TRUE
                pnd_Case_Fields_Pnd_St_Max.setValue(1);                                                                                                                   //Natural: ASSIGN #CASE-FIELDS.#ST-MAX := 1
                pnd_Case_Fields_Pnd_State_Disp_Cv.setValue("AD=N");                                                                                                       //Natural: ASSIGN #CASE-FIELDS.#STATE-DISP-CV := #CASE-FIELDS.#LOCAL-DISP-CV := ( AD = N )
                pnd_Case_Fields_Pnd_Local_Disp_Cv.setValue("AD=N");
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Case_Fields_Pnd_St_Max.setValue(ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp());                                                               //Natural: ASSIGN #CASE-FIELDS.#ST-MAX := FORM.C*TIRF-1099-R-STATE-GRP
            }                                                                                                                                                             //Natural: END-IF
            FOR11:                                                                                                                                                        //Natural: FOR #I = 1 TO #CASE-FIELDS.#ST-MAX
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Case_Fields_Pnd_St_Max)); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(! (pnd_Case_Fields_Pnd_No_States.getBoolean())))                                                                                            //Natural: IF NOT #CASE-FIELDS.#NO-STATES
                {
                    if (condition(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).greaterOrEqual("00") && ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_State_Max_Alpha())  //Natural: IF FORM.TIRF-STATE-CODE ( #I ) = '00' THRU #STATE-MAX-ALPHA AND FORM.TIRF-STATE-CODE ( #I ) = MASK ( 99 )
                        && DbsUtil.maskMatches(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Pnd_I),"99")))
                    {
                        pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Pnd_I).val());             //Natural: ASSIGN #WS.#J := VAL ( FORM.TIRF-STATE-CODE ( #I ) )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_J.setValue(-1);                                                                                                                        //Natural: ASSIGN #WS.#J := -1
                        gdaTwrg3537.getPnd_State_Table_Tircntl_State_Alpha_Code().getValue(-1 + 2).setValue(ldaTwrl9710.getForm_Tirf_State_Code().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ( -1 ) := FORM.TIRF-STATE-CODE ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_I).equals(new DbsDecimal("0.00")) || ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I).equals(new  //Natural: IF FORM.TIRF-LOC-DISTR ( #I ) = 0.00 OR FORM.TIRF-LOC-TAX-WTHLD ( #I ) = 0.00
                        DbsDecimal("0.00"))))
                    {
                        pnd_Case_Fields_Pnd_Local_Disp_Cv.setValue("AD=N");                                                                                               //Natural: ASSIGN #CASE-FIELDS.#LOCAL-DISP-CV := ( AD = N )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Case_Fields_Pnd_Local_Disp_Cv.resetInitial();                                                                                                 //Natural: RESET INITIAL #CASE-FIELDS.#LOCAL-DISP-CV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_I.greater(1)))                                                                                                                   //Natural: IF #I > 1
                {
                    pnd_Case_Fields_Pnd_Fed_Disp_Cv.setValue("AD=N");                                                                                                     //Natural: ASSIGN #CASE-FIELDS.#FED-DISP-CV := ( AD = N )
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(4, new ReportEmptyLineSuppression(true),"///TIN",                                                                                    //Natural: DISPLAY ( 4 ) ( ES = ON ) '///TIN' TWRATIN.#O-TIN ( CV = #FED-DISP-CV ) / '/' TWRATIN.#O-TIN-TYPE-DESC ( CV = #FED-DISP-CV ) '///Contract' FORM.TIRF-CONTRACT-NBR ( CV = #FED-DISP-CV ) 'Pa/y/e/e' FORM.TIRF-PAYEE-CDE ( CV = #FED-DISP-CV ) 'D/i/s/t' #TWRADIST.#OUT-DIST ( CV = #FED-DISP-CV ) '/C/a/t' #TWRADCAT.#DCAT-SHORT ( CV = #FED-DISP-CV ) 'F/o/r/n' FORM.TIRF-FOREIGN-ADDR ( CV = #FED-DISP-CV ) '//Name' FORM.TIRF-PARTICIPANT-NAME ( HC = L CV = #FED-DISP-CV ) / 'Address' FORM.TIRF-ADDR-LN1 ( HC = L CV = #FED-DISP-CV ) / '/' FORM.TIRF-ADDR-LN2 ( HC = L CV = #FED-DISP-CV ) / '/' FORM.TIRF-ADDR-LN3 ( HC = L CV = #FED-DISP-CV ) / '/' FORM.TIRF-ADDR-LN4 ( HC = L CV = #FED-DISP-CV ) / '/' FORM.TIRF-ADDR-LN5 ( HC = L CV = #FED-DISP-CV ) / '/' FORM.TIRF-ADDR-LN6 ( HC = L CV = #FED-DISP-CV ) 'T/X/N/D' FORM.TIRF-TAXABLE-NOT-DET ( CV = #FED-DISP-CV ) 'Gross Amount' FORM.TIRF-GROSS-AMT ( HC = R CV = #FED-DISP-CV ) / 2X 'Taxable Amount' FORM.TIRF-TAXABLE-AMT ( HC = R CV = #FED-DISP-CV ) / 2X 'IVC Amount' FORM.TIRF-IVC-AMT ( HC = R CV = #FED-DISP-CV ) / 2X 'Federal Tax' FORM.TIRF-FED-TAX-WTHLD ( HC = R CV = #FED-DISP-CV ) '#/of/Res/ide/ncy' FORM.C*TIRF-1099-R-STATE-GRP ( EM = ZZ9 HC = R CV = #FED-DISP-CV ) '/Res/ide/ncy/Code' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE ( #J ) ( LC = �� CV = #STATE-DISP-CV ) '/C/M/B' #STATE-TABLE.TIRCNTL-COMB-FED-IND ( #J ) ( CV = #STATE-DISP-CV HC = R ) '/I/R/S' FORM.TIRF-RES-CMB-IND ( #I ) ( CV = #STATE-DISP-CV HC = R ) '//State Distrib' FORM.TIRF-STATE-DISTR ( #I ) ( CV = #STATE-DISP-CV HC = R ) / 'Local Distrib' FORM.TIRF-LOC-DISTR ( #I ) ( CV = #LOCAL-DISP-CV HC = R ) '//State Tax' FORM.TIRF-STATE-TAX-WTHLD ( #I ) ( CV = #STATE-DISP-CV HC = R ) / 'Local Tax' FORM.TIRF-LOC-TAX-WTHLD ( #I ) ( CV = #LOCAL-DISP-CV HC = R )
                		pdaTwratin.getTwratin_Pnd_O_Tin(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,"/",
                		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"///Contract",
                		ldaTwrl9710.getForm_Tirf_Contract_Nbr(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"Pa/y/e/e",
                		ldaTwrl9710.getForm_Tirf_Payee_Cde(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"D/i/s/t",
                		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"/C/a/t",
                		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"F/o/r/n",
                		ldaTwrl9710.getForm_Tirf_Foreign_Addr(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"//Name",
                		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"Address",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"/",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"/",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"/",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"/",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    NEWLINE,"/",
                		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    "T/X/N/D",
                		ldaTwrl9710.getForm_Tirf_Taxable_Not_Det(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"Gross Amount",
                		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
                    ColumnSpacing(2),"Taxable Amount",
                		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
                    ColumnSpacing(2),"IVC Amount",
                		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
                    ColumnSpacing(2),"Federal Tax",
                		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
                    "#/of/Res/ide/ncy",
                		ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
                    pnd_Case_Fields_Pnd_Fed_Disp_Cv,"/Res/ide/ncy/Code",
                		gdaTwrg3537.getPnd_State_Table_Tircntl_State_Alpha_Code().getValue(pnd_Ws_Pnd_J.getInt() + 2), new FieldAttributes("LC=��"), pnd_Case_Fields_Pnd_State_Disp_Cv,
                    "/C/M/B",
                		gdaTwrg3537.getPnd_State_Table_Tircntl_Comb_Fed_Ind().getValue(pnd_Ws_Pnd_J.getInt() + 2), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/I/R/S",
                		ldaTwrl9710.getForm_Tirf_Res_Cmb_Ind().getValue(pnd_Ws_Pnd_I), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "//State Distrib",
                		ldaTwrl9710.getForm_Tirf_State_Distr().getValue(pnd_Ws_Pnd_I), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    NEWLINE,"Local Distrib",
                		ldaTwrl9710.getForm_Tirf_Loc_Distr().getValue(pnd_Ws_Pnd_I), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "//State Tax",
                		ldaTwrl9710.getForm_Tirf_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    NEWLINE,"Local Tax",
                		ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR12:                                                                                                                                                        //Natural: FOR #WS.#J = 1 TO C*FORM.TIRF-SYS-ERR
            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Ws_Pnd_J.nadd(1))
            {
                pnd_Ws_Pnd_Err.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Ws_Pnd_J));                                                                       //Natural: ASSIGN #WS.#ERR := FORM.TIRF-SYS-ERR ( #J )
                getReports().write(4, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Participant_Name()),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ws_Pnd_Err.getInt() + 1)); //Natural: WRITE ( 4 ) T*FORM.TIRF-PARTICIPANT-NAME #TWRL5001.#ERR-DESC ( #ERR )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(4, 1);                                                                                                                                      //Natural: SKIP ( 4 ) 1 LINES
            //*  1099-INT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"//TIN/Type",                 //Natural: DISPLAY ( 5 ) ( HC = L ES = ON ) '//TIN/Type' TWRATIN.#O-TIN-TYPE-DESC '///TIN' TWRATIN.#O-TIN ( HC = C ) '///Contract' FORM.TIRF-CONTRACT-NBR '//Pay/ee' FORM.TIRF-PAYEE-CDE 'F/o/r/n' FORM.TIRF-FOREIGN-ADDR '//Name' FORM.TIRF-PARTICIPANT-NAME / 'Address' FORM.TIRF-ADDR-LN1 / '/' FORM.TIRF-ADDR-LN2 / '/' FORM.TIRF-ADDR-LN3 / '/' FORM.TIRF-ADDR-LN4 / '/' FORM.TIRF-ADDR-LN5 / '/' FORM.TIRF-ADDR-LN6 '//Interest/Amount' FORM.TIRF-INT-AMT ( HC = R ) '//Backup/Withholding' FORM.TIRF-FED-TAX-WTHLD ( HC = R )
                
            		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///TIN",
            		pdaTwratin.getTwratin_Pnd_O_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"///Contract",
            		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"//Pay/ee",
            		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"F/o/r/n",
            		ldaTwrl9710.getForm_Tirf_Foreign_Addr(),"//Name",
            		ldaTwrl9710.getForm_Tirf_Participant_Name(),NEWLINE,"Address",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln1(),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln2(),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln3(),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln4(),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln5(),NEWLINE,"/",
            		ldaTwrl9710.getForm_Tirf_Addr_Ln6(),"//Interest/Amount",
            		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Backup/Withholding",
            		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            FOR13:                                                                                                                                                        //Natural: FOR #WS.#J = 1 TO C*FORM.TIRF-SYS-ERR
            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_Sys_Err())); pnd_Ws_Pnd_J.nadd(1))
            {
                pnd_Ws_Pnd_Err.setValue(ldaTwrl9710.getForm_Tirf_Sys_Err().getValue(pnd_Ws_Pnd_J));                                                                       //Natural: ASSIGN #WS.#ERR := FORM.TIRF-SYS-ERR ( #J )
                getReports().write(5, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Participant_Name()),ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Ws_Pnd_Err.getInt() + 1)); //Natural: WRITE ( 5 ) T*FORM.TIRF-PARTICIPANT-NAME #TWRL5001.#ERR-DESC ( #ERR )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(5, 1);                                                                                                                                      //Natural: SKIP ( 5 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Irsw_Detail_Reject() throws Exception                                                                                                                //Natural: IRSW-DETAIL-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().setValue(3);                                                                                                    //Natural: ASSIGN #REPORT-INDEXES.#IRSW1 := 3
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Case_Fields_Pnd_Recon_Detail.getBoolean())))                                                                                                 //Natural: IF NOT #CASE-FIELDS.#RECON-DETAIL
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 12 )
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 13 )
        FOR14:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                                   //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  1099-R
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                getReports().display(12, "/TIN",                                                                                                                          //Natural: DISPLAY ( 12 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) 'Di/st' #RECON-CASE.#DIST-CODE ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Gross Amount' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) '/IVC Amount' #RECON-CASE.#IVC-AMT ( #K ) ( HC = R ) '/Taxable Amount' #RECON-CASE.#TAXABLE-AMT ( #K ) ( HC = R ) 'Federal/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Di/st",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Gross Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/IVC Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Taxable Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Federal/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(12, 1);                                                                                                                             //Natural: SKIP ( 12 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(13, "/TIN",                                                                                                                          //Natural: DISPLAY ( 13 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'Interest/Amount' #RECON-CASE.#INT-AMT ( #K ) ( HC = R ) 'Backup/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Interest/Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Backup/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(13, 1);                                                                                                                             //Natural: SKIP ( 13 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Irsw_Detail_Accept() throws Exception                                                                                                                //Natural: IRSW-DETAIL-ACCEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
            gdaTwrg3537.getPnd_Report_Indexes_Pnd_Irsw1().reset();                                                                                                        //Natural: RESET #REPORT-INDEXES.#IRSW1
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSW
            sub_Accum_Irsw();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Case_Fields_Pnd_Recon_Detail.getBoolean())))                                                                                                 //Natural: IF NOT #CASE-FIELDS.#RECON-DETAIL
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 10 )
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 11 )
        FOR15:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                                   //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  1099-R
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                getReports().display(10, "/TIN",                                                                                                                          //Natural: DISPLAY ( 10 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) 'Di/st' #RECON-CASE.#DIST-CODE ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Gross Amount' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) '/IVC Amount' #RECON-CASE.#IVC-AMT ( #K ) ( HC = R ) '/Taxable Amount' #RECON-CASE.#TAXABLE-AMT ( #K ) ( HC = R ) 'Federal/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Di/st",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Gross Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/IVC Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Taxable Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Federal/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(10, 1);                                                                                                                             //Natural: SKIP ( 10 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(11, "/TIN",                                                                                                                          //Natural: DISPLAY ( 11 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'Interest/Amount' #RECON-CASE.#INT-AMT ( #K ) ( HC = R ) 'Backup/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Interest/Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Backup/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(11, 1);                                                                                                                             //Natural: SKIP ( 11 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Irsg_Detail() throws Exception                                                                                                                       //Natural: IRSG-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Case_Fields_Pnd_Recon_Summary.getBoolean()))                                                                                                    //Natural: IF #CASE-FIELDS.#RECON-SUMMARY
        {
                                                                                                                                                                          //Natural: PERFORM ACCUM-IRSG1
            sub_Accum_Irsg1();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Case_Fields_Pnd_Recon_Detail.getBoolean())))                                                                                                 //Natural: IF NOT #CASE-FIELDS.#RECON-DETAIL
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 16 )
        Global.format("IS=OFF");                                                                                                                                          //Natural: SUSPEND IDENTICAL SUPPRESS ( 17 )
        FOR16:                                                                                                                                                            //Natural: FOR #WS.#K = 1 TO 3
        for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(3)); pnd_Ws_Pnd_K.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_K.equals(2) && ! (pnd_Case_Fields_Pnd_Prior_Record.getBoolean())))                                                                   //Natural: IF #WS.#K = 2 AND NOT #CASE-FIELDS.#PRIOR-RECORD
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  1099-R
            if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                //Natural: IF FORM.TIRF-FORM-TYPE = 01
            {
                getReports().display(16, "/TIN",                                                                                                                          //Natural: DISPLAY ( 16 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) 'Di/st' #RECON-CASE.#DIST-CODE ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) '/Gross Amount' #RECON-CASE.#GROSS-AMT ( #K ) ( HC = R ) '/IVC Amount' #RECON-CASE.#IVC-AMT ( #K ) ( HC = R ) '/Taxable Amount' #RECON-CASE.#TAXABLE-AMT ( #K ) ( HC = R ) 'Federal/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Di/st",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Gross Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/IVC Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/Taxable Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Federal/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(16, 1);                                                                                                                             //Natural: SKIP ( 16 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
                //*  1099-INT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(17, "/TIN",                                                                                                                          //Natural: DISPLAY ( 17 ) '/TIN' #RECON-CASE.#TIN ( #K ) ( IS = ON ) 'Contract/Payee' #RECON-CASE.#CNTRCT-PY ( #K ) ( IS = ON ) '/' #RECON-CASE.#HEADER ( #K ) '/Form Count' #RECON-CASE.#FORM-CNT ( #K ) ( HC = R ) 'Interest/Amount' #RECON-CASE.#INT-AMT ( #K ) ( HC = R ) 'Backup/Withholding' #RECON-CASE.#FED-TAX ( #K ) ( HC = R )
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"Contract/Payee",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(pnd_Ws_Pnd_K), new IdenticalSuppress(true),"/",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header().getValue(pnd_Ws_Pnd_K),"/Form Count",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Interest/Amount",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Backup/Withholding",
                		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(pnd_Ws_Pnd_K), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_K.equals(3)))                                                                                                                    //Natural: IF #WS.#K = 3
                {
                    getReports().skip(17, 1);                                                                                                                             //Natural: SKIP ( 17 ) 1 LINES
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Distrib() throws Exception                                                                                                                   //Natural: PROCESS-DISTRIB
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradist.getPnd_Twradist_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST.#INPUT-PARMS ( AD = O ) #TWRADIST.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Distcat() throws Exception                                                                                                                   //Natural: PROCESS-DISTCAT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrndcat.class , getCurrentProcessState(), pdaTwradcat.getPnd_Twradcat_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradcat.getPnd_Twradcat_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDCAT' USING #TWRADCAT.#INPUT-PARMS ( AD = O ) #TWRADCAT.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Tin() throws Exception                                                                                                                       //Natural: PROCESS-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Setup_Recon_Case() throws Exception                                                                                                                  //Natural: SETUP-RECON-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Case_Fields_Pnd_Recon_Summary.reset();                                                                                                                        //Natural: RESET #CASE-FIELDS.#RECON-SUMMARY #CASE-FIELDS.#RECON-DETAIL
        pnd_Case_Fields_Pnd_Recon_Detail.reset();
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().equals(new DbsDecimal("0.00"))))                        //Natural: IF #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD = 0.00
        {
            pnd_Case_Fields_Pnd_Prior_Record.reset();                                                                                                                     //Natural: RESET #CASE-FIELDS.#PRIOR-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld().notEquals(new DbsDecimal("0.00")) || pnd_Case_Fields_Pnd_Prior_Record.getBoolean() && ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld().notEquals(new  //Natural: IF FORM.TIRF-FED-TAX-WTHLD NE 0.00 OR #CASE-FIELDS.#PRIOR-RECORD AND FORM-R.TIRF-FED-TAX-WTHLD NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Case_Fields_Pnd_Recon_Summary.setValue(true);                                                                                                             //Natural: ASSIGN #CASE-FIELDS.#RECON-SUMMARY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        gdaTwrg3537.getPnd_Recon_Case().getValue("*").resetInitial();                                                                                                     //Natural: RESET INITIAL #RECON-CASE ( * )
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                      //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                                   //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
            sub_Process_Tin();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(1).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                                  //Natural: ASSIGN #RECON-CASE.#TIN ( 1 ) := TWRATIN.#O-TIN
        //*  1099-R
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(1)))                                                                                                    //Natural: IF FORM.TIRF-FORM-TYPE = 01
        {
            if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(ldaTwrl9710.getForm_Tirf_Distribution_Cde())))                                              //Natural: IF #TWRADIST.#IN-DIST NE FORM.TIRF-DISTRIBUTION-CDE
            {
                pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9710.getForm_Tirf_Distribution_Cde());                                                          //Natural: ASSIGN #TWRADIST.#IN-DIST := FORM.TIRF-DISTRIBUTION-CDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTRIB
                sub_Process_Distrib();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code().getValue(1).setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                               //Natural: ASSIGN #RECON-CASE.#DIST-CODE ( 1 ) := #TWRADIST.#OUT-DIST
        }                                                                                                                                                                 //Natural: END-IF
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(1).setValueEdited(ldaTwrl9710.getForm_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));               //Natural: MOVE EDITED FORM.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY ( 1 )
        setValueToSubstring(ldaTwrl9710.getForm_Tirf_Payee_Cde(),gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(1),11,2);                                         //Natural: MOVE FORM.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY ( 1 ) ,11,2 )
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(1).nadd(1);                                                                                                 //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 1 )
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Gross_Amt());                                                         //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 1 ) := FORM.TIRF-GROSS-AMT
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Ivc_Amt());                                                             //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 1 ) := FORM.TIRF-IVC-AMT
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Taxable_Amt());                                                     //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 1 ) := FORM.TIRF-TAXABLE-AMT
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Int_Amt());                                                             //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 1 ) := FORM.TIRF-INT-AMT
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(1).setValue(ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld());                                                       //Natural: ASSIGN #RECON-CASE.#FED-TAX ( 1 ) := FORM.TIRF-FED-TAX-WTHLD
        if (condition(pnd_Case_Fields_Pnd_Prior_Record.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#PRIOR-RECORD
        {
            if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9715.getForm_R_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-R.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-R.TIRF-TIN
            {
                pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Tax_Id_Type());                                                                //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-R.TIRF-TAX-ID-TYPE
                pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                             //Natural: ASSIGN TWRATIN.#I-TIN := FORM-R.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM PROCESS-TIN
                sub_Process_Tin();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin().getValue(2).setValue(pdaTwratin.getTwratin_Pnd_O_Tin());                                                              //Natural: ASSIGN #RECON-CASE.#TIN ( 2 ) := TWRATIN.#O-TIN
            //*  1099-R
            if (condition(ldaTwrl9715.getForm_R_Tirf_Form_Type().equals(1)))                                                                                              //Natural: IF FORM-R.TIRF-FORM-TYPE = 01
            {
                if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(ldaTwrl9715.getForm_R_Tirf_Distribution_Cde())))                                        //Natural: IF #TWRADIST.#IN-DIST NE FORM-R.TIRF-DISTRIBUTION-CDE
                {
                    pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl9715.getForm_R_Tirf_Distribution_Cde());                                                    //Natural: ASSIGN #TWRADIST.#IN-DIST := FORM-R.TIRF-DISTRIBUTION-CDE
                                                                                                                                                                          //Natural: PERFORM PROCESS-DISTRIB
                    sub_Process_Distrib();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code().getValue(2).setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                           //Natural: ASSIGN #RECON-CASE.#DIST-CODE ( 2 ) := #TWRADIST.#OUT-DIST
            }                                                                                                                                                             //Natural: END-IF
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(2).setValueEdited(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),new ReportEditMask("XXXXXXX-X/"));         //Natural: MOVE EDITED FORM-R.TIRF-CONTRACT-NBR ( EM = XXXXXXX-X/ ) TO #RECON-CASE.#CNTRCT-PY ( 2 )
            setValueToSubstring(ldaTwrl9715.getForm_R_Tirf_Payee_Cde(),gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py().getValue(2),11,2);                                   //Natural: MOVE FORM-R.TIRF-PAYEE-CDE TO SUBSTR ( #RECON-CASE.#CNTRCT-PY ( 2 ) ,11,2 )
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(2).nadd(1);                                                                                             //Natural: ADD 1 TO #RECON-CASE.#FORM-CNT ( 2 )
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Gross_Amt());                                                   //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 2 ) := FORM-R.TIRF-GROSS-AMT
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Ivc_Amt());                                                       //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 2 ) := FORM-R.TIRF-IVC-AMT
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Taxable_Amt());                                               //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 2 ) := FORM-R.TIRF-TAXABLE-AMT
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Int_Amt());                                                       //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 2 ) := FORM-R.TIRF-INT-AMT
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(2).setValue(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld());                                                 //Natural: ASSIGN #RECON-CASE.#FED-TAX ( 2 ) := FORM-R.TIRF-FED-TAX-WTHLD
        }                                                                                                                                                                 //Natural: END-IF
        gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(3)),          //Natural: ASSIGN #RECON-CASE.#FORM-CNT ( 3 ) := #RECON-CASE.#FORM-CNT ( 1 ) - #RECON-CASE.#FORM-CNT ( 2 )
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(2)));
        if (condition(gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(1).equals(new DbsDecimal("0.00"))))                                                            //Natural: IF #RECON-CASE.#FED-TAX ( 1 ) = 0.00
        {
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(3)),    //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 2 ) * -1
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(2).multiply(-1));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 3 ) := #RECON-CASE.#IVC-AMT ( 2 ) * -1
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(2).multiply(-1));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(3)),  //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 3 ) := #RECON-CASE.#TAXABLE-AMT ( 2 ) * -1
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(2).multiply(-1));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 3 ) := #RECON-CASE.#INT-AMT ( 2 ) * -1
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(2).multiply(-1));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#FED-TAX ( 3 ) := #RECON-CASE.#FED-TAX ( 2 ) * -1
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(2).multiply(-1));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(3)),    //Natural: ASSIGN #RECON-CASE.#GROSS-AMT ( 3 ) := #RECON-CASE.#GROSS-AMT ( 1 ) - #RECON-CASE.#GROSS-AMT ( 2 )
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(2)));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#IVC-AMT ( 3 ) := #RECON-CASE.#IVC-AMT ( 1 ) - #RECON-CASE.#IVC-AMT ( 2 )
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(2)));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(3)),  //Natural: ASSIGN #RECON-CASE.#TAXABLE-AMT ( 3 ) := #RECON-CASE.#TAXABLE-AMT ( 1 ) - #RECON-CASE.#TAXABLE-AMT ( 2 )
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(2)));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#INT-AMT ( 3 ) := #RECON-CASE.#INT-AMT ( 1 ) - #RECON-CASE.#INT-AMT ( 2 )
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(2)));
            gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(3).compute(new ComputeParameters(false, gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(3)),        //Natural: ASSIGN #RECON-CASE.#FED-TAX ( 3 ) := #RECON-CASE.#FED-TAX ( 1 ) - #RECON-CASE.#FED-TAX ( 2 )
                gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(1).subtract(gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(2)));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt().getValue(3).equals(new DbsDecimal("0.00")) && gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt().getValue(3).equals(new  //Natural: IF #RECON-CASE.#FORM-CNT ( 3 ) = 0.00 AND #RECON-CASE.#GROSS-AMT ( 3 ) = 0.00 AND #RECON-CASE.#IVC-AMT ( 3 ) = 0.00 AND #RECON-CASE.#TAXABLE-AMT ( 3 ) = 0.00 AND #RECON-CASE.#INT-AMT ( 3 ) = 0.00 AND #RECON-CASE.#FED-TAX ( 3 ) = 0.00
            DbsDecimal("0.00")) && gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt().getValue(3).equals(new DbsDecimal("0.00")) && gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt().getValue(3).equals(new 
            DbsDecimal("0.00")) && gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt().getValue(3).equals(new DbsDecimal("0.00")) && gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax().getValue(3).equals(new 
            DbsDecimal("0.00"))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Case_Fields_Pnd_Recon_Detail.setValue(true);                                                                                                              //Natural: ASSIGN #CASE-FIELDS.#RECON-DETAIL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_A_Records() throws Exception                                                                                                                  //Natural: CREATE-A-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        FOR17:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(gdaTwrg3537.getPnd_A_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                   //Natural: IF #A-REC.#GENERATE ( #I )
            {
                ldaTwrl3321.getIrs_A_Out_Tape().resetInitial();                                                                                                           //Natural: RESET INITIAL IRS-A-OUT-TAPE
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(gdaTwrg3537.getPnd_Gl_Const_Pnd_Corr_Ind().getValue(pnd_Ws_Pnd_I));                                   //Natural: ASSIGN #TWRL3508.#CORR-IND := #GL-CONST.#CORR-IND ( #I )
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payment_Year().setValue(ldaTwrl3508.getPnd_Twrl3508_Pnd_Tax_Year());                                                  //Natural: ASSIGN IRS-A-PAYMENT-YEAR := #TWRL3508.#TAX-YEAR
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Ein().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                       //Natural: ASSIGN IRS-A-PAYER-EIN := #TWRACOM2.#FED-ID
                //*  1099-R
                if (condition(ldaTwrl3508.getPnd_Twrl3508_Pnd_Form().equals(1)))                                                                                          //Natural: IF #TWRL3508.#FORM = 01
                {
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("9");                                                                                   //Natural: ASSIGN IRS-A-TYPE-OF-RETURN := '9'
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12459A");                                                                           //Natural: ASSIGN IRS-A-AMOUNT-INDICATORS := '12459A'
                    //*  1099-INT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Type_Of_Return().setValue("6");                                                                                   //Natural: ASSIGN IRS-A-TYPE-OF-RETURN := '6'
                    ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Amount_Indicators().setValue("12345");                                                                            //Natural: ASSIGN IRS-A-AMOUNT-INDICATORS := '12345'
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #TWRL3508.#CORR-IND         = ' '                   /* 10/10/07
                //*      IRS-A-ORIGINAL-FILE-IND     := '1'
                //*      RESET                          IRS-A-CORRECTION-FILE-IND
                //*    ELSE                                                   /* 10/10/07
                //*      RESET                          IRS-A-ORIGINAL-FILE-IND
                //*      IRS-A-CORRECTION-FILE-IND   := '1'
                //*    END-IF                                                 /* 10/10/07
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_Control().reset();                                                                                         //Natural: RESET IRS-A-PAYER-NAME-CONTROL
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                              //Natural: ASSIGN IRS-A-PAYER-NAME-1 := #TWRACOM2.#COMP-PAYER-A
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_B());                                              //Natural: ASSIGN IRS-A-PAYER-NAME-2 := #TWRACOM2.#COMP-PAYER-B
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Shipping_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                             //Natural: ASSIGN IRS-A-PAYER-SHIPPING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Phone_No().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                   //Natural: ASSIGN IRS-A-PAYER-PHONE-NO := #TWRACOM2.#PHONE
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                        //Natural: ASSIGN IRS-A-PAYER-CITY := #TWRACOM2.#CITY
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                      //Natural: ASSIGN IRS-A-PAYER-STATE := #TWRACOM2.#STATE
                ldaTwrl3321.getIrs_A_Out_Tape_Irs_A_Payer_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                        //Natural: ASSIGN IRS-A-PAYER-ZIP := #TWRACOM2.#ZIP-9
                getWorkFiles().write(1, false, ldaTwrl3321.getIrs_A_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                           //Natural: WRITE WORK FILE 1 IRS-A-OUT-TAPE #TWRL3508
                ldaTwrl3324.getIrs_F_Out_Tape_Irs_F_Number_Of_A_Records().nadd(1);                                                                                        //Natural: ADD 1 TO IRS-F-NUMBER-OF-A-RECORDS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gdaTwrg3537.getPnd_A_Rec().getValue("*").reset();                                                                                                                 //Natural: RESET #A-REC ( * )
    }
    private void sub_Create_C_Records() throws Exception                                                                                                                  //Natural: CREATE-C-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        FOR18:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(gdaTwrg3537.getPnd_C_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                   //Natural: IF #C-REC.#GENERATE ( #I )
            {
                ldaTwrl3323.getIrs_C_Out_Tape().resetInitial();                                                                                                           //Natural: RESET INITIAL IRS-C-OUT-TAPE
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(gdaTwrg3537.getPnd_Gl_Const_Pnd_Corr_Ind().getValue(pnd_Ws_Pnd_I));                                   //Natural: ASSIGN #TWRL3508.#CORR-IND := #GL-CONST.#CORR-IND ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Number_Of_Payees().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_I));                        //Natural: ASSIGN IRS-C-NUMBER-OF-PAYEES := #C-REC.#NUM-PAYEES ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_1().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_1().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-1 := #C-REC.#AMT-1 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_2().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_2().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-2 := #C-REC.#AMT-2 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_4().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_4().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-4 := #C-REC.#AMT-4 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_5().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_5().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-5 := #C-REC.#AMT-5 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_9().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_9().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-9 := #C-REC.#AMT-9 ( #I )
                ldaTwrl3323.getIrs_C_Out_Tape_Irs_C_Control_Total_A().setValue(gdaTwrg3537.getPnd_C_Rec_Pnd_Amt_A().getValue(pnd_Ws_Pnd_I));                              //Natural: ASSIGN IRS-C-CONTROL-TOTAL-A := #C-REC.#AMT-A ( #I )
                getWorkFiles().write(1, false, ldaTwrl3323.getIrs_C_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                           //Natural: WRITE WORK FILE 1 IRS-C-OUT-TAPE #TWRL3508
                ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees().nadd(gdaTwrg3537.getPnd_C_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_I));                      //Natural: ADD #C-REC.#NUM-PAYEES ( #I ) TO IRS-T-TOTAL-NUMBER-OF-PAYEES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gdaTwrg3537.getPnd_C_Rec().getValue("*").reset();                                                                                                                 //Natural: RESET #C-REC ( * )
    }
    private void sub_Create_K_Records() throws Exception                                                                                                                  //Natural: CREATE-K-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Seq_Num().reset();                                                                                                                //Natural: RESET #TWRL3508.#SEQ-NUM
        FOR19:                                                                                                                                                            //Natural: FOR #WS.#I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(gdaTwrg3537.getPnd_K_Rec_Pnd_Generate().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                   //Natural: IF #K-REC.#GENERATE ( #I )
            {
                ldaTwrl3508.getPnd_Twrl3508_Pnd_Corr_Ind().setValue(gdaTwrg3537.getPnd_Gl_Const_Pnd_Corr_Ind().getValue(pnd_Ws_Pnd_I));                                   //Natural: ASSIGN #TWRL3508.#CORR-IND := #GL-CONST.#CORR-IND ( #I )
                FOR20:                                                                                                                                                    //Natural: FOR #WS.#J = 1 TO #STATE-MAX
                for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_State_Max())); pnd_Ws_Pnd_J.nadd(1))
                {
                    if (condition(gdaTwrg3537.getPnd_K_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J).notEquals(getZero())))                                    //Natural: IF #K-REC.#NUM-PAYEES ( #I,#J ) NE 0
                    {
                        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State().setValue(pnd_Ws_Pnd_J);                                                                             //Natural: ASSIGN #TWRL3508.#IRS-K-STATE := #WS.#J
                        ldaTwrl3325.getIrs_K_Out_Tape().resetInitial();                                                                                                   //Natural: RESET INITIAL IRS-K-OUT-TAPE
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_State_Code().setValueEdited(pnd_Ws_Pnd_J,new ReportEditMask("99"));                                           //Natural: MOVE EDITED #WS.#J ( EM = 99 ) TO IRS-K-STATE-CODE
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Number_Of_Payees().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Num_Payees().getValue(pnd_Ws_Pnd_I,                  //Natural: ASSIGN IRS-K-NUMBER-OF-PAYEES := #K-REC.#NUM-PAYEES ( #I,#J )
                            pnd_Ws_Pnd_J));
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_1().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_1().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-1 := #K-REC.#AMT-1 ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_2().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_2().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-2 := #K-REC.#AMT-2 ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_4().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_4().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-4 := #K-REC.#AMT-4 ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_5().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_5().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-5 := #K-REC.#AMT-5 ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_9().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_9().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-9 := #K-REC.#AMT-9 ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Control_Total_A().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Amt_A().getValue(pnd_Ws_Pnd_I,pnd_Ws_Pnd_J));         //Natural: ASSIGN IRS-K-CONTROL-TOTAL-A := #K-REC.#AMT-A ( #I,#J )
                        ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_State_Tax().getValue(pnd_Ws_Pnd_I,                    //Natural: ASSIGN IRS-K-ST-TAX-WITHHELD := #K-REC.#STATE-TAX ( #I,#J )
                            pnd_Ws_Pnd_J));
                        if (condition(gdaTwrg3537.getPnd_K_Rec_Pnd_Local_Tax().getValue(pnd_Ws_Pnd_Corr_Idx,pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))             //Natural: IF #K-REC.#LOCAL-TAX ( #CORR-IDX,#I ) NE 0.00
                        {
                            ldaTwrl3325.getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld().setValue(gdaTwrg3537.getPnd_K_Rec_Pnd_Local_Tax().getValue(pnd_Ws_Pnd_I,                //Natural: ASSIGN IRS-K-LC-TAX-WITHHELD := #K-REC.#LOCAL-TAX ( #I,#J )
                                pnd_Ws_Pnd_J));
                        }                                                                                                                                                 //Natural: END-IF
                        getWorkFiles().write(1, false, ldaTwrl3325.getIrs_K_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                   //Natural: WRITE WORK FILE 1 IRS-K-OUT-TAPE #TWRL3508
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gdaTwrg3537.getPnd_K_Rec().getValue("*").reset();                                                                                                                 //Natural: RESET #K-REC ( * ) #TWRL3508.#IRS-K-STATE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Irs_K_State().reset();
    }
    private void sub_Create_T_Record() throws Exception                                                                                                                   //Natural: CREATE-T-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                        //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        //*  MARINA
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
        sub_Process_Company();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitters_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                        //Natural: ASSIGN IRS-T-TRANSMITTERS-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Control_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                              //Natural: ASSIGN IRS-T-TRANSMITTER-CONTROL-CODE := #TWRACOM2.#COMP-TCC
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                  //Natural: ASSIGN IRS-T-TRANSMITTER-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Transmitter_Name_Continued().reset();                                                                                         //Natural: RESET IRS-T-TRANSMITTER-NAME-CONTINUED IRS-T-MAGNETIC-TAPE-FILE-IND
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind().reset();
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                                      //Natural: ASSIGN IRS-T-COMPANY-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Name_Continued().reset();                                                                                             //Natural: RESET IRS-T-COMPANY-NAME-CONTINUED
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Mailing_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                    //Natural: ASSIGN IRS-T-COMPANY-MAILING-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                              //Natural: ASSIGN IRS-T-COMPANY-CITY := #TWRACOM2.#CITY
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                            //Natural: ASSIGN IRS-T-COMPANY-STATE := #TWRACOM2.#STATE
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Company_Zip_Code().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                         //Natural: ASSIGN IRS-T-COMPANY-ZIP-CODE := #TWRACOM2.#ZIP-9
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                                      //Natural: ASSIGN IRS-T-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Email_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Email_Addr());                                       //Natural: ASSIGN IRS-T-CONTACT-EMAIL-ADDRESS := #TWRACOM2.#CONTACT-EMAIL-ADDR
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                //Natural: ASSIGN IRS-T-CONTACT-PHONE-N-EXTENSION := #TWRACOM2.#PHONE
        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pnd_Ws_Const_Low_Values);                                                                                 //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := LOW-VALUES
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Payment_Year().setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO IRS-T-PAYMENT-YEAR
        if (condition(pnd_Ws_Pnd_System_Year.notEquals(ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Payment_Year())))                                                              //Natural: IF #WS.#SYSTEM-YEAR NE IRS-T-PAYMENT-YEAR
        {
            ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind().setValue("P");                                                                                        //Natural: ASSIGN IRS-T-PRIOR-YR-DATA-IND := 'P'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl3326.getIrs_T_Out_Tape_Irs_T_Vendor_Ind().setValue("I");                                                                                                   //Natural: ASSIGN IRS-T-VENDOR-IND := 'I'
        getWorkFiles().write(1, false, ldaTwrl3326.getIrs_T_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 1 IRS-T-OUT-TAPE #TWRL3508
    }
    private void sub_Create_F_Record() throws Exception                                                                                                                   //Natural: CREATE-F-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl3508.getPnd_Twrl3508_Pnd_Company_Code().setValue(pnd_Ws_Const_High_Values);                                                                                //Natural: ASSIGN #TWRL3508.#COMPANY-CODE := HIGH-VALUES
        getWorkFiles().write(1, false, ldaTwrl3324.getIrs_F_Out_Tape(), ldaTwrl3508.getPnd_Twrl3508());                                                                   //Natural: WRITE WORK FILE 1 IRS-F-OUT-TAPE #TWRL3508
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR21:                                                                                                                                                            //Natural: FOR #I = 0 TO #STATE-MAX
        for (pnd_Ws_Pnd_I.setValue(0); condition(pnd_Ws_Pnd_I.lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_State_Max())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                               //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                gdaTwrg3537.getPnd_State_Table().getValue(pnd_Ws_Pnd_I.getInt() + 2).setValuesByName(pdaTwratbl2.getTwratbl2());                                          //Natural: MOVE BY NAME TWRATBL2 TO #STATE-TABLE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Load_Province_Table() throws Exception                                                                                                               //Natural: LOAD-PROVINCE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR22:                                                                                                                                                            //Natural: FOR #I = #PROV-MIN TO #PROV-MAX
        for (pnd_Ws_Pnd_I.setValue(gdaTwrg3537.getPnd_Gl_Const_Pnd_Prov_Min()); condition(pnd_Ws_Pnd_I.lessOrEqual(gdaTwrg3537.getPnd_Gl_Const_Pnd_Prov_Max())); 
            pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                               //Natural: MOVE EDITED #I ( EM = 999 ) TO TWRATBL2.#STATE-CDE
            DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean()))                                                                                         //Natural: IF #RETURN-CDE
            {
                gdaTwrg3537.getPnd_Prov_Table().getValue(pnd_Ws_Pnd_I).setValuesByName(pdaTwratbl2.getTwratbl2());                                                        //Natural: MOVE BY NAME TWRATBL2 TO #PROV-TABLE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                       //Natural: TERMINATE #WS.#TERMINATE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRd_Form() throws Exception {atBreakEventRd_Form(false);}
    private void atBreakEventRd_Form(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak = ldaTwrl9710.getForm_Tirf_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak = ldaTwrl9710.getForm_Tirf_Form_Type().isBreak(endOfData);
        if (condition(ldaTwrl9710_getForm_Tirf_Company_CdeIsBreak || ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue("*",1));                                          //Natural: ADD #IRSR.#FORM-CNT ( *,1 ) TO #IRSR.#FORM-CNT ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt().getValue("*",1));                                        //Natural: ADD #IRSR.#GROSS-AMT ( *,1 ) TO #IRSR.#GROSS-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt().getValue("*",1));                                            //Natural: ADD #IRSR.#IVC-AMT ( *,1 ) TO #IRSR.#IVC-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt().getValue("*",1));                                    //Natural: ADD #IRSR.#TAXABLE-AMT ( *,1 ) TO #IRSR.#TAXABLE-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Int_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Int_Amt().getValue("*",1));                                            //Natural: ADD #IRSR.#INT-AMT ( *,1 ) TO #IRSR.#INT-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax().getValue("*",1));                                            //Natural: ADD #IRSR.#FED-TAX ( *,1 ) TO #IRSR.#FED-TAX ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax().getValue("*",1));                                        //Natural: ADD #IRSR.#STATE-TAX ( *,1 ) TO #IRSR.#STATE-TAX ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax().getValue("*",1));                                        //Natural: ADD #IRSR.#LOCAL-TAX ( *,1 ) TO #IRSR.#LOCAL-TAX ( *,2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(1));                                        //Natural: ADD #IRSR.#CMB-STATE-CNT ( 1 ) TO #IRSR.#CMB-STATE-CNT ( 2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(1));                                        //Natural: ADD #IRSR.#CMB-STATE-TAX ( 1 ) TO #IRSR.#CMB-STATE-TAX ( 2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(2).nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(1));                                        //Natural: ADD #IRSR.#CMB-LOCAL-TAX ( 1 ) TO #IRSR.#CMB-LOCAL-TAX ( 2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Totals().getValue("*",1).reset();                                                                                                 //Natural: RESET #IRSR.#TOTALS ( *,1 ) #IRSR.#CMB-STATE-TAX ( 1 ) #IRSR.#CMB-LOCAL-TAX ( 1 ) #IRSR.#CMB-STATE-CNT ( 1 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(1).reset();
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(1).reset();
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(1).reset();
            gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt().getValue("*",1));                                          //Natural: ADD #IRSW.#FORM-CNT ( *,1 ) TO #IRSW.#FORM-CNT ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Gross_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Gross_Amt().getValue("*",1));                                        //Natural: ADD #IRSW.#GROSS-AMT ( *,1 ) TO #IRSW.#GROSS-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Ivc_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Ivc_Amt().getValue("*",1));                                            //Natural: ADD #IRSW.#IVC-AMT ( *,1 ) TO #IRSW.#IVC-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Taxable_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Taxable_Amt().getValue("*",1));                                    //Natural: ADD #IRSW.#TAXABLE-AMT ( *,1 ) TO #IRSW.#TAXABLE-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Int_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Int_Amt().getValue("*",1));                                            //Natural: ADD #IRSW.#INT-AMT ( *,1 ) TO #IRSW.#INT-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax().getValue("*",1));                                            //Natural: ADD #IRSW.#FED-TAX ( *,1 ) TO #IRSW.#FED-TAX ( *,2 )
            gdaTwrg3537.getPnd_Irsw_Pnd_Totals().getValue("*",1).reset();                                                                                                 //Natural: RESET #IRSW.#TOTALS ( *,1 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt().getValue("*",1));                                          //Natural: ADD #IRSG.#FORM-CNT ( *,1 ) TO #IRSG.#FORM-CNT ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt().getValue("*",1));                                        //Natural: ADD #IRSG.#GROSS-AMT ( *,1 ) TO #IRSG.#GROSS-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt().getValue("*",1));                                            //Natural: ADD #IRSG.#IVC-AMT ( *,1 ) TO #IRSG.#IVC-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt().getValue("*",1));                                    //Natural: ADD #IRSG.#TAXABLE-AMT ( *,1 ) TO #IRSG.#TAXABLE-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt().getValue("*",1));                                            //Natural: ADD #IRSG.#INT-AMT ( *,1 ) TO #IRSG.#INT-AMT ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue("*",2).nadd(gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax().getValue("*",1));                                            //Natural: ADD #IRSG.#FED-TAX ( *,1 ) TO #IRSG.#FED-TAX ( *,2 )
            gdaTwrg3537.getPnd_Irsg_Pnd_Totals().getValue("*",1).reset();                                                                                                 //Natural: RESET #IRSG.#TOTALS ( *,1 )
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORDS
            sub_Create_A_Records();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORDS
            sub_Create_C_Records();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-K-RECORDS
            sub_Create_K_Records();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9710_getForm_Tirf_Form_TypeIsBreak))
        {
            pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 2
            pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                             //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORTS
            sub_Summary_Reports();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Accepted_Form_Cnt.nadd(gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt().getValue(8,2));                                                                      //Natural: ADD #IRSR.#FORM-CNT ( 8,2 ) TO #WS.#ACCEPTED-FORM-CNT
            gdaTwrg3537.getPnd_Irsr_Pnd_Totals().getValue("*",2).reset();                                                                                                 //Natural: RESET #IRSR.#TOTALS ( *,2 ) #IRSR.#CMB-STATE-TAX ( 2 ) #IRSR.#CMB-LOCAL-TAX ( 2 ) #IRSW.#TOTALS ( *,2 ) #IRSG.#TOTALS ( *,2 ) #IRSR.#CMB-STATE-CNT ( 2 )
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Tax().getValue(2).reset();
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_Local_Tax().getValue(2).reset();
            gdaTwrg3537.getPnd_Irsw_Pnd_Totals().getValue("*",2).reset();
            gdaTwrg3537.getPnd_Irsg_Pnd_Totals().getValue("*",2).reset();
            gdaTwrg3537.getPnd_Irsr_Pnd_Cmb_State_Cnt().getValue(2).reset();
            pnd_Ws_Pnd_Form_Typ.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                           //Natural: ASSIGN #WS.#FORM-TYP := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=133 ZP=ON");
        Global.format(11, "PS=58 LS=133 ZP=ON");
        Global.format(12, "PS=58 LS=133 ZP=ON");
        Global.format(13, "PS=58 LS=133 ZP=ON");
        Global.format(14, "PS=58 LS=133 ZP=ON");
        Global.format(15, "PS=58 LS=133 ZP=ON");
        Global.format(16, "PS=58 LS=133 ZP=ON");
        Global.format(17, "PS=58 LS=133 ZP=ON");
        Global.format(18, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"IRS Correction Reporting Summary",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(50),"IRS Correction Reporting Summary",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS Correction Reporting Rejected Detail",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS Correction Reporting Accepted Detail",new TabSetting(120),"Report: RPT7",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(42),"IRS Correction Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(42),"IRS Correction Withholding Reconciliation Summary",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(10, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"IRS Correction Withholding Reconciliation Accepted Detail",new TabSetting(118),"Report:  RPT10",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(11, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"IRS Correction Withholding Reconciliation Accepted Detail",new TabSetting(118),"Report:  RPT11",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(12, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"IRS Correction Withholding Reconciliation Rejected Detail",new TabSetting(118),"Report:  RPT12",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(13, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(13), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"IRS Correction Withholding Reconciliation Rejected Detail",new TabSetting(118),"Report:  RPT13",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(14, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(14), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS General Ledger Reconciliation Summary",new TabSetting(118),"Report:  RPT14",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS General Ledger Reconciliation Summary",new TabSetting(118),"Report:  RPT15",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(16, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(16), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS General Ledger Reconciliation Detail",new TabSetting(118),"Report:  RPT16",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);
        getReports().write(17, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(118),"Page  :",getReports().getPageNumberDbs(17), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"IRS General Ledger Reconciliation Detail",new TabSetting(118),"Report:  RPT17",NEWLINE,new TabSetting(43),"Reporting period",pnd_Ws_Pnd_Prev_Rpt_Date, 
            new ReportEditMask ("MM/DD/YYYY"),"through",pnd_Ws_Pnd_Datx, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,pnd_Ws_Pnd_Company_Line,
            NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsr_Pnd_Header1(),NEWLINE,"/",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Header2(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt(),"/Gross Amount",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Gross_Amt(),"/IVC Amount",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Ivc_Amt(),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Taxable_Amt(),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax(),"State/Withholding",
        		gdaTwrg3537.getPnd_Irsr_Pnd_State_Tax(),"Local/Withholding",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Local_Tax());
        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsr_Pnd_Header1(),NEWLINE,"/",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Header2(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Form_Cnt(),"Interest/Amount",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Int_Amt(),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Irsr_Pnd_Fed_Tax());
        getReports().setDisplayColumns(8, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsw_Pnd_Header(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt(),"/Gross Amount",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Gross_Amt(),"/IVC Amount",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Ivc_Amt(),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Taxable_Amt(),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax());
        getReports().setDisplayColumns(9, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsw_Pnd_Header(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Form_Cnt(),"Interest/Amount",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Int_Amt(),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Irsw_Pnd_Fed_Tax());
        getReports().setDisplayColumns(14, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsg_Pnd_Header(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt(),"/Gross Amount",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Gross_Amt(),"/IVC Amount",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Ivc_Amt(),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Taxable_Amt(),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax());
        getReports().setDisplayColumns(15, new ReportEmptyLineSuppression(true),new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/",
            
        		gdaTwrg3537.getPnd_Irsg_Pnd_Header(),"Form/Count",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Form_Cnt(),"Interest/Amount",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Int_Amt(),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Irsg_Pnd_Fed_Tax());
        getReports().setDisplayColumns(6, "//Cor/Ind",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind(), new FieldAttributes("LC=�"),"//TIN/Typ",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type(), new FieldAttributes("LC=�"),"///TIN",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin(),"///Account",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),"//Dist/Code",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Dist_Code(), new FieldAttributes("LC=�"),"F/o/r/n",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind(),"/Name",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_City(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_State(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/I/R/A",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Ira_Sep_Ind(),"T/x/n/d",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Taxable_Not_Det(),"(1)",
        		"///IRS Amounts",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(2)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(4)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(5)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(9)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_9(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(A)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_A(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/IRS/State/Code",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Federal_State_Code(), new FieldAttributes("LC=�"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
            "//State Tax",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_State_Tax_Num(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            NEWLINE,"Local Tax",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Local_Tax_Num(), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(7, "//Cor/Ind",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Corr_Ind(), new FieldAttributes("LC=�"),"//TIN/Typ",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin_Type(), new FieldAttributes("LC=�"),"///TIN",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Tin(),"///Account",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Contract_Payee(),"F/O/R/N",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Foreign_Ind(),"//Name",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Name_Line_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"Address",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Address_Line(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_City(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_State(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),NEWLINE,"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Zip(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"(1)",
        		"///IRS Amounts",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),NEWLINE,"(4)",
        		"/",
        		ldaTwrl3507.getIrs_B_Rec_Irs_B_Payment_Amt_4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,"/",
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"Pa/y/e/e",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"D/i/s/t",
        		pdaTwradist.getPnd_Twradist_Pnd_Out_Dist(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"/C/a/t",
        		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"F/o/r/n",
        		ldaTwrl9710.getForm_Tirf_Foreign_Addr(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
            NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,
            "/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,
            "/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,
            "/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,
            "/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,
            "/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"T/X/N/D",
            
        		ldaTwrl9710.getForm_Tirf_Taxable_Not_Det(), pnd_Case_Fields_Pnd_Fed_Disp_Cv,"Gross Amount",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
            ColumnSpacing(2),"Taxable Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
            ColumnSpacing(2),"IVC Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,NEWLINE,new 
            ColumnSpacing(2),"Federal Tax",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), pnd_Case_Fields_Pnd_Fed_Disp_Cv,
            "#/of/Res/ide/ncy",
        		ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp(), new ReportEditMask ("ZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), 
            pnd_Case_Fields_Pnd_Fed_Disp_Cv,"/Res/ide/ncy/Code",
        		gdaTwrg3537.getPnd_State_Table_Tircntl_State_Alpha_Code(), new FieldAttributes("LC=��"), pnd_Case_Fields_Pnd_State_Disp_Cv,"/C/M/B",
        		gdaTwrg3537.getPnd_State_Table_Tircntl_Comb_Fed_Ind(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/I/R/S",
        		ldaTwrl9710.getForm_Tirf_Res_Cmb_Ind(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "//State Distrib",
        		ldaTwrl9710.getForm_Tirf_State_Distr(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            NEWLINE,"Local Distrib",
        		ldaTwrl9710.getForm_Tirf_Loc_Distr(), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "//State Tax",
        		ldaTwrl9710.getForm_Tirf_State_Tax_Wthld(), pnd_Case_Fields_Pnd_State_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            NEWLINE,"Local Tax",
        		ldaTwrl9710.getForm_Tirf_Loc_Tax_Wthld(), pnd_Case_Fields_Pnd_Local_Disp_Cv, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(5, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ReportEmptyLineSuppression(true),"//TIN/Type",
            
        		pdaTwratin.getTwratin_Pnd_O_Tin_Type_Desc(),"///TIN",
        		pdaTwratin.getTwratin_Pnd_O_Tin(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"///Contract",
        		ldaTwrl9710.getForm_Tirf_Contract_Nbr(),"//Pay/ee",
        		ldaTwrl9710.getForm_Tirf_Payee_Cde(),"F/o/r/n",
        		ldaTwrl9710.getForm_Tirf_Foreign_Addr(),"//Name",
        		ldaTwrl9710.getForm_Tirf_Participant_Name(),NEWLINE,"Address",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln1(),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln2(),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln3(),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln4(),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln5(),NEWLINE,"/",
        		ldaTwrl9710.getForm_Tirf_Addr_Ln6(),"//Interest/Amount",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//Backup/Withholding",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(12, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"Di/st",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Gross Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/IVC Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(13, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Interest/Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(10, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"Di/st",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Gross Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/IVC Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(11, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Interest/Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(16, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"Di/st",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Dist_Code(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Gross Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/IVC Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/Taxable Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Federal/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(17, "/TIN",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Tin(), new IdenticalSuppress(true),"Contract/Payee",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Cntrct_Py(), new IdenticalSuppress(true),"/",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Header(),"/Form Count",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Form_Cnt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Interest/Amount",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
        		gdaTwrg3537.getPnd_Recon_Case_Pnd_Fed_Tax(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData1562() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Form_Typ.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                           //Natural: ASSIGN #WS.#FORM-TYP := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
}
