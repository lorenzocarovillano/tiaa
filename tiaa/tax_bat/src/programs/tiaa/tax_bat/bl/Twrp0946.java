/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:07 PM
**        * FROM NATURAL PROGRAM : Twrp0946
************************************************************
**        * FILE NAME            : Twrp0946.java
**        * CLASS NAME           : Twrp0946
**        * INSTANCE NAME        : Twrp0946
************************************************************
* PROGRAM  : TWRP0946
* FUNCTION : PRINTS CANADIAN TRANSACTIONS (CLONE OF TWRP0945)
* INPUT    : FLAT-FILE1 & 3
* WRITTEN BY : EDITH
* HISTORY
* -------------------------------------------------
*          : TO PROCESS 'APTM' & 'IATM'  3/3/99
*          : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*              IVC-IND = K BUT NOT ROLL-OVER  (EDITH 6/17/99)
*          : NEW DEFINITION OF ROLLOVER BASED ON DISTRIBUTION CODE
*               AND INCLUSION OF NEW CO., TIAA-LIFE (EDITH 10/8/99)
* 12/13/99 : DISTRIBUTION CODE '6' IS NOT ROLLOVER ANYMORE
* 12/14/99 : DISTRIBUTION CODE 'Y' IS NOT ROLLOVER ANYMORE
* 12/15/99 : UPDATED DUE TO DELETION OF IA MAINTENANCE (TMAL,TMIL)
* 02/27/02 : FIXED CANADIAN WITHHOLDING SELECTION LOGIC (J.ROTHOLZ)
* 04/02/02 : ADDED NEW COMPANY - TCII (J.ROTHOLZ)
* 05/30/02 : ALLOW IVC AMOUNTS WITH ROLLOVERS (JH)
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 07/10/03 RM INVESTMENT SOLUTIONS - AUTOMATE SSSS PROJECT
*             NEW SOURCE CODE 'SI' FOR INVESTMENT SOLUTIONS SHOULD BE
*             ADDED FOR THIS PROJECT, BUT NOT IN THIS PROGRAM. THERE IS
*             NO NEED FOR CHANGES IN HERE.
* 10/06/03 RM - 2003 1099 & 5498 SDR
*               DISTRIBUTION CODE 'X' REMOVED.
* 09/21/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'OP', 'NL', 'PL'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGE THIS TIME.
* 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*              'Roll Roth 403 to Roth IRA - Death'
* 10/18/11  MB - ADDED NEW COMPANY F TO SUBROUTINE CO-DESC /* 10/18/11
* 12/01/14  O SOTTO FATCA CHANGES MARKED 11/2014.
******************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0946 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Trans_Cnt_F;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Gross_Amt_F;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt_F;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt_F;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt_F;
    private DbsField pnd_Counters_Pnd_Fed_Amt;
    private DbsField pnd_Counters_Pnd_Fed_Amt_F;
    private DbsField pnd_Counters_Pnd_Nra_Amt;
    private DbsField pnd_Counters_Pnd_Nra_Amt_F;
    private DbsField pnd_Counters_Pnd_Can_Amt;
    private DbsField pnd_Counters_Pnd_Can_Amt_F;
    private DbsField pnd_Counters_Pnd_Pr_Amt;
    private DbsField pnd_Counters_Pnd_Pr_Amt_F;

    private DbsGroup pnd_Sv_Prt_Fields;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld1;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld2;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld3;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld4;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld5;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld6;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld7;
    private DbsField pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld8;

    private DbsGroup pnd_Sv_Print0;
    private DbsField pnd_Sv_Print0_Pnd_Sv_Prtfld9;
    private DbsField pnd_Sv_Print0_Pnd_Sv_Prtfld10;
    private DbsField pnd_Sv_Cnt;
    private DbsField pnd_D;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Tot_Per_Cmpny;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt;
    private DbsField pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Grand_Tot;

    private DbsGroup w_Hdg_Vars;

    private DbsGroup w_Hdg_Vars_Pnd_W_Hdg_1;
    private DbsField w_Hdg_Vars_Pnd_W_Syst;
    private DbsField w_Hdg_Vars_Pnd_W_Fill1;
    private DbsField w_Hdg_Vars_Pnd_W_Trans;
    private DbsField w_Hdg_Vars_Pnd_W_Fill2;
    private DbsField w_Hdg_Vars_Pnd_W_Gross;
    private DbsField w_Hdg_Vars_Pnd_W_Fill3;
    private DbsField w_Hdg_Vars_Pnd_W_Tax_Free;
    private DbsField w_Hdg_Vars_Pnd_W_Fill4;
    private DbsField w_Hdg_Vars_Pnd_W_Taxable;
    private DbsField w_Hdg_Vars_Pnd_W_Fill5;
    private DbsField w_Hdg_Vars_Pnd_W_Interest;
    private DbsField w_Hdg_Vars_Pnd_W_Fill6;
    private DbsField w_Hdg_Vars_Pnd_W_Federal;
    private DbsField w_Hdg_Vars_Pnd_W_Fill7;
    private DbsField w_Hdg_Vars_Pnd_W_Nra;

    private DbsGroup w_Hdg_Vars__R_Field_1;
    private DbsField w_Hdg_Vars_Pnd_W_Hdg1;

    private DbsGroup w_Hdg_Vars_Pnd_W_Hdg_2;
    private DbsField w_Hdg_Vars_Pnd_W_Srce;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill1;
    private DbsField w_Hdg_Vars_Pnd_W_Count;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill2;
    private DbsField w_Hdg_Vars_Pnd_W_Amount;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill3;
    private DbsField w_Hdg_Vars_Pnd_W_Ivc;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill4;
    private DbsField w_Hdg_Vars_Pnd_W_Amt2;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill5;
    private DbsField w_Hdg_Vars_Pnd_W_Amt3;
    private DbsField w_Hdg_Vars_Pnd_W_Hd2_Fill6;
    private DbsField w_Hdg_Vars_Pnd_W_With;

    private DbsGroup w_Hdg_Vars__R_Field_2;
    private DbsField w_Hdg_Vars_Pnd_W_Hdg2;

    private DbsGroup w_Hdg_Vars_Pnd_W_Hdg_3;
    private DbsField w_Hdg_Vars_Pnd_W_Hd3_Fill1;
    private DbsField w_Hdg_Vars_Pnd_W_Paymt;
    private DbsField w_Hdg_Vars_Pnd_W_Hd3_Fill2;
    private DbsField w_Hdg_Vars_Pnd_W_Slsh1;
    private DbsField w_Hdg_Vars_Pnd_W_Hd3_Fill3;
    private DbsField w_Hdg_Vars_Pnd_W_Slsh2;

    private DbsGroup w_Hdg_Vars__R_Field_3;
    private DbsField w_Hdg_Vars_Pnd_W_Hdg3;

    private DbsGroup w_Hdg_Vars_Pnd_W_Hdg_4;
    private DbsField w_Hdg_Vars_Pnd_W_Hd4_Fill1;
    private DbsField w_Hdg_Vars_Pnd_W_Canad;
    private DbsField w_Hdg_Vars_Pnd_W_Hd4_Fill2;
    private DbsField w_Hdg_Vars_Pnd_W_Cand2;
    private DbsField w_Hdg_Vars_Pnd_W_Hd4_Fill3;
    private DbsField w_Hdg_Vars_Pnd_W_Puerto;

    private DbsGroup w_Hdg_Vars__R_Field_4;
    private DbsField w_Hdg_Vars_Pnd_W_Hdg4;

    private DbsGroup w_Hdg_Vars_Pnd_W_Hdg_5;
    private DbsField w_Hdg_Vars_Pnd_W_Hd5_Fill1;
    private DbsField w_Hdg_Vars_Pnd_W_Withd;
    private DbsField w_Hdg_Vars_Pnd_W_Hd5_Fill2;
    private DbsField w_Hdg_Vars_Pnd_W_With2;
    private DbsField w_Hdg_Vars_Pnd_W_Hd5_Fill3;
    private DbsField w_Hdg_Vars_Pnd_W_With3;

    private DbsGroup w_Hdg_Vars__R_Field_5;
    private DbsField w_Hdg_Vars_Pnd_W_Hdg5;

    private DbsGroup pnd_Print_Fields;
    private DbsField pnd_Print_Fields_Pnd_Prtfld1;
    private DbsField pnd_Print_Fields_Pnd_Prtfld2;
    private DbsField pnd_Print_Fields_Pnd_Prtfld3;
    private DbsField pnd_Print_Fields_Pnd_Prtfld4;
    private DbsField pnd_Print_Fields_Pnd_Prtfld5;
    private DbsField pnd_Print_Fields_Pnd_Prtfld6;
    private DbsField pnd_Print_Fields_Pnd_Prtfld7;
    private DbsField pnd_Print_Fields_Pnd_Prtfld8;

    private DbsGroup pnd_Print0_Fields;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld9;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld10;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Source;
    private DbsField pnd_Sv_Source;
    private DbsField pnd_New_Source;
    private DbsField pnd_Occ;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_First_Rec;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;
    private DbsField pnd_Var_File1_Pnd_Start_Date;
    private DbsField pnd_Var_File1_Pnd_End_Date;
    private DbsField pnd_A;
    private DbsField pnd_Page;
    private DbsField pnd_Sub_Skip;
    private DbsField pnd_Sv_Srce12;
    private DbsField pnd_Source12;
    private DbsField pnd_Source34;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Company;
    private DbsField pnd_Savers_Pnd_Sv_Srce_Code;
    private DbsField pnd_Tot_Prt;
    private DbsField pnd_Tax_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Trans_Cnt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt_F", "#TRANS-CNT-F", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Gross_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt_F", "#GROSS-AMT-F", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Ivc_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt_F", "#IVC-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Taxable_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt_F", "#TAXABLE-AMT-F", FieldType.PACKED_DECIMAL, 14, 
            2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Int_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt_F", "#INT-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Fed_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Amt", "#FED-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Fed_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Amt_F", "#FED-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Nra_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Amt", "#NRA-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Nra_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Amt_F", "#NRA-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Can_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Amt", "#CAN-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Can_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Amt_F", "#CAN-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Pr_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Amt", "#PR-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Pr_Amt_F = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Amt_F", "#PR-AMT-F", FieldType.PACKED_DECIMAL, 12, 2);

        pnd_Sv_Prt_Fields = localVariables.newGroupArrayInRecord("pnd_Sv_Prt_Fields", "#SV-PRT-FIELDS", new DbsArrayController(1, 200));
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld1 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld1", "#SV-PRTFLD1", FieldType.STRING, 17);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld2 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld2", "#SV-PRTFLD2", FieldType.STRING, 8);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld3 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld3", "#SV-PRTFLD3", FieldType.STRING, 18);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld4 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld4", "#SV-PRTFLD4", FieldType.STRING, 15);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld5 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld5", "#SV-PRTFLD5", FieldType.STRING, 18);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld6 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld6", "#SV-PRTFLD6", FieldType.STRING, 15);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld7 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld7", "#SV-PRTFLD7", FieldType.STRING, 15);
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld8 = pnd_Sv_Prt_Fields.newFieldInGroup("pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld8", "#SV-PRTFLD8", FieldType.STRING, 15);

        pnd_Sv_Print0 = localVariables.newGroupArrayInRecord("pnd_Sv_Print0", "#SV-PRINT0", new DbsArrayController(1, 200));
        pnd_Sv_Print0_Pnd_Sv_Prtfld9 = pnd_Sv_Print0.newFieldInGroup("pnd_Sv_Print0_Pnd_Sv_Prtfld9", "#SV-PRTFLD9", FieldType.STRING, 15);
        pnd_Sv_Print0_Pnd_Sv_Prtfld10 = pnd_Sv_Print0.newFieldInGroup("pnd_Sv_Print0_Pnd_Sv_Prtfld10", "#SV-PRTFLD10", FieldType.STRING, 15);
        pnd_Sv_Cnt = localVariables.newFieldInRecord("pnd_Sv_Cnt", "#SV-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Tot_Per_Cmpny = localVariables.newGroupArrayInRecord("pnd_Tot_Per_Cmpny", "#TOT-PER-CMPNY", new DbsArrayController(1, 7));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name", "#TOT-CMPY-NAME", FieldType.STRING, 
            4);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", FieldType.NUMERIC, 
            6);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt", "#TOT-INT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt", "#TOT-FED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt", "#TOT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt", "#TOT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt = pnd_Tot_Per_Cmpny.newFieldInGroup("pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt", "#TOT-PR-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Grand_Tot = localVariables.newFieldInRecord("pnd_Grand_Tot", "#GRAND-TOT", FieldType.BOOLEAN, 1);

        w_Hdg_Vars = localVariables.newGroupInRecord("w_Hdg_Vars", "W-HDG-VARS");

        w_Hdg_Vars_Pnd_W_Hdg_1 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars_Pnd_W_Hdg_1", "#W-HDG-1");
        w_Hdg_Vars_Pnd_W_Syst = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Syst", "#W-SYST", FieldType.STRING, 6);
        w_Hdg_Vars_Pnd_W_Fill1 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill1", "#W-FILL1", FieldType.STRING, 14);
        w_Hdg_Vars_Pnd_W_Trans = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Trans", "#W-TRANS", FieldType.STRING, 5);
        w_Hdg_Vars_Pnd_W_Fill2 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill2", "#W-FILL2", FieldType.STRING, 10);
        w_Hdg_Vars_Pnd_W_Gross = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Gross", "#W-GROSS", FieldType.STRING, 5);
        w_Hdg_Vars_Pnd_W_Fill3 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill3", "#W-FILL3", FieldType.STRING, 11);
        w_Hdg_Vars_Pnd_W_Tax_Free = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Tax_Free", "#W-TAX-FREE", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Fill4 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill4", "#W-FILL4", FieldType.STRING, 10);
        w_Hdg_Vars_Pnd_W_Taxable = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Taxable", "#W-TAXABLE", FieldType.STRING, 7);
        w_Hdg_Vars_Pnd_W_Fill5 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill5", "#W-FILL5", FieldType.STRING, 9);
        w_Hdg_Vars_Pnd_W_Interest = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Interest", "#W-INTEREST", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Fill6 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill6", "#W-FILL6", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Federal = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Federal", "#W-FEDERAL", FieldType.STRING, 7);
        w_Hdg_Vars_Pnd_W_Fill7 = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Fill7", "#W-FILL7", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Nra = w_Hdg_Vars_Pnd_W_Hdg_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Nra", "#W-NRA", FieldType.STRING, 9);

        w_Hdg_Vars__R_Field_1 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars__R_Field_1", "REDEFINE", w_Hdg_Vars_Pnd_W_Hdg_1);
        w_Hdg_Vars_Pnd_W_Hdg1 = w_Hdg_Vars__R_Field_1.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hdg1", "#W-HDG1", FieldType.STRING, 125);

        w_Hdg_Vars_Pnd_W_Hdg_2 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars_Pnd_W_Hdg_2", "#W-HDG-2");
        w_Hdg_Vars_Pnd_W_Srce = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Srce", "#W-SRCE", FieldType.STRING, 4);
        w_Hdg_Vars_Pnd_W_Hd2_Fill1 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill1", "#W-HD2-FILL1", FieldType.STRING, 16);
        w_Hdg_Vars_Pnd_W_Count = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Count", "#W-COUNT", FieldType.STRING, 5);
        w_Hdg_Vars_Pnd_W_Hd2_Fill2 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill2", "#W-HD2-FILL2", FieldType.STRING, 10);
        w_Hdg_Vars_Pnd_W_Amount = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Amount", "#W-AMOUNT", FieldType.STRING, 6);
        w_Hdg_Vars_Pnd_W_Hd2_Fill3 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill3", "#W-HD2-FILL3", FieldType.STRING, 13);
        w_Hdg_Vars_Pnd_W_Ivc = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Ivc", "#W-IVC", FieldType.STRING, 3);
        w_Hdg_Vars_Pnd_W_Hd2_Fill4 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill4", "#W-HD2-FILL4", FieldType.STRING, 13);
        w_Hdg_Vars_Pnd_W_Amt2 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Amt2", "#W-AMT2", FieldType.STRING, 6);
        w_Hdg_Vars_Pnd_W_Hd2_Fill5 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill5", "#W-HD2-FILL5", FieldType.STRING, 10);
        w_Hdg_Vars_Pnd_W_Amt3 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Amt3", "#W-AMT3", FieldType.STRING, 6);
        w_Hdg_Vars_Pnd_W_Hd2_Fill6 = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd2_Fill6", "#W-HD2-FILL6", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_With = w_Hdg_Vars_Pnd_W_Hdg_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_With", "#W-WITH", FieldType.STRING, 8);

        w_Hdg_Vars__R_Field_2 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars__R_Field_2", "REDEFINE", w_Hdg_Vars_Pnd_W_Hdg_2);
        w_Hdg_Vars_Pnd_W_Hdg2 = w_Hdg_Vars__R_Field_2.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hdg2", "#W-HDG2", FieldType.STRING, 108);

        w_Hdg_Vars_Pnd_W_Hdg_3 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars_Pnd_W_Hdg_3", "#W-HDG-3");
        w_Hdg_Vars_Pnd_W_Hd3_Fill1 = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd3_Fill1", "#W-HD3-FILL1", FieldType.STRING, 31);
        w_Hdg_Vars_Pnd_W_Paymt = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Paymt", "#W-PAYMT", FieldType.STRING, 17);
        w_Hdg_Vars_Pnd_W_Hd3_Fill2 = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd3_Fill2", "#W-HD3-FILL2", FieldType.STRING, 50);
        w_Hdg_Vars_Pnd_W_Slsh1 = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Slsh1", "#W-SLSH1", FieldType.STRING, 14);
        w_Hdg_Vars_Pnd_W_Hd3_Fill3 = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd3_Fill3", "#W-HD3-FILL3", FieldType.STRING, 2);
        w_Hdg_Vars_Pnd_W_Slsh2 = w_Hdg_Vars_Pnd_W_Hdg_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Slsh2", "#W-SLSH2", FieldType.STRING, 14);

        w_Hdg_Vars__R_Field_3 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars__R_Field_3", "REDEFINE", w_Hdg_Vars_Pnd_W_Hdg_3);
        w_Hdg_Vars_Pnd_W_Hdg3 = w_Hdg_Vars__R_Field_3.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hdg3", "#W-HDG3", FieldType.STRING, 128);

        w_Hdg_Vars_Pnd_W_Hdg_4 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars_Pnd_W_Hdg_4", "#W-HDG-4");
        w_Hdg_Vars_Pnd_W_Hd4_Fill1 = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd4_Fill1", "#W-HD4-FILL1", FieldType.STRING, 35);
        w_Hdg_Vars_Pnd_W_Canad = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Canad", "#W-CANAD", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Hd4_Fill2 = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd4_Fill2", "#W-HD4-FILL2", FieldType.STRING, 58);
        w_Hdg_Vars_Pnd_W_Cand2 = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Cand2", "#W-CAND2", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Hd4_Fill3 = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd4_Fill3", "#W-HD4-FILL3", FieldType.STRING, 11);
        w_Hdg_Vars_Pnd_W_Puerto = w_Hdg_Vars_Pnd_W_Hdg_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Puerto", "#W-PUERTO", FieldType.STRING, 2);

        w_Hdg_Vars__R_Field_4 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars__R_Field_4", "REDEFINE", w_Hdg_Vars_Pnd_W_Hdg_4);
        w_Hdg_Vars_Pnd_W_Hdg4 = w_Hdg_Vars__R_Field_4.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hdg4", "#W-HDG4", FieldType.STRING, 122);

        w_Hdg_Vars_Pnd_W_Hdg_5 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars_Pnd_W_Hdg_5", "#W-HDG-5");
        w_Hdg_Vars_Pnd_W_Hd5_Fill1 = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd5_Fill1", "#W-HD5-FILL1", FieldType.STRING, 34);
        w_Hdg_Vars_Pnd_W_Withd = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_Withd", "#W-WITHD", FieldType.STRING, 11);
        w_Hdg_Vars_Pnd_W_Hd5_Fill2 = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd5_Fill2", "#W-HD5-FILL2", FieldType.STRING, 56);
        w_Hdg_Vars_Pnd_W_With2 = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_With2", "#W-WITH2", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_Hd5_Fill3 = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hd5_Fill3", "#W-HD5-FILL3", FieldType.STRING, 8);
        w_Hdg_Vars_Pnd_W_With3 = w_Hdg_Vars_Pnd_W_Hdg_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_With3", "#W-WITH3", FieldType.STRING, 8);

        w_Hdg_Vars__R_Field_5 = w_Hdg_Vars.newGroupInGroup("w_Hdg_Vars__R_Field_5", "REDEFINE", w_Hdg_Vars_Pnd_W_Hdg_5);
        w_Hdg_Vars_Pnd_W_Hdg5 = w_Hdg_Vars__R_Field_5.newFieldInGroup("w_Hdg_Vars_Pnd_W_Hdg5", "#W-HDG5", FieldType.STRING, 125);

        pnd_Print_Fields = localVariables.newGroupInRecord("pnd_Print_Fields", "#PRINT-FIELDS");
        pnd_Print_Fields_Pnd_Prtfld1 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld1", "#PRTFLD1", FieldType.STRING, 17);
        pnd_Print_Fields_Pnd_Prtfld2 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld2", "#PRTFLD2", FieldType.STRING, 8);
        pnd_Print_Fields_Pnd_Prtfld3 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld3", "#PRTFLD3", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld4 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld4", "#PRTFLD4", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld5 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld5", "#PRTFLD5", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld6 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld6", "#PRTFLD6", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld7 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld7", "#PRTFLD7", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld8 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld8", "#PRTFLD8", FieldType.STRING, 15);

        pnd_Print0_Fields = localVariables.newGroupInRecord("pnd_Print0_Fields", "#PRINT0-FIELDS");
        pnd_Print0_Fields_Pnd_Prtfld9 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld9", "#PRTFLD9", FieldType.STRING, 15);
        pnd_Print0_Fields_Pnd_Prtfld10 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld10", "#PRTFLD10", FieldType.STRING, 15);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_Sv_Source = localVariables.newFieldInRecord("pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 2);
        pnd_New_Source = localVariables.newFieldInRecord("pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Occ = localVariables.newFieldInRecord("pnd_Occ", "#OCC", FieldType.NUMERIC, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Start_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_End_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.BOOLEAN, 1);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.BOOLEAN, 1);
        pnd_Sub_Skip = localVariables.newFieldInRecord("pnd_Sub_Skip", "#SUB-SKIP", FieldType.BOOLEAN, 1);
        pnd_Sv_Srce12 = localVariables.newFieldInRecord("pnd_Sv_Srce12", "#SV-SRCE12", FieldType.STRING, 2);
        pnd_Source12 = localVariables.newFieldInRecord("pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Source34 = localVariables.newFieldInRecord("pnd_Source34", "#SOURCE34", FieldType.STRING, 2);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Company = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Srce_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 4);
        pnd_Tot_Prt = localVariables.newFieldInRecord("pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_Cmpy_Cnt.setInitialValue(7);
        pnd_Grand_Tot.setInitialValue(false);
        w_Hdg_Vars_Pnd_W_Syst.setInitialValue("SYSTEM");
        w_Hdg_Vars_Pnd_W_Trans.setInitialValue("TRANS");
        w_Hdg_Vars_Pnd_W_Gross.setInitialValue("GROSS");
        w_Hdg_Vars_Pnd_W_Tax_Free.setInitialValue("TAX FREE");
        w_Hdg_Vars_Pnd_W_Taxable.setInitialValue("TAXABLE");
        w_Hdg_Vars_Pnd_W_Interest.setInitialValue("INTEREST");
        w_Hdg_Vars_Pnd_W_Federal.setInitialValue("FEDERAL");
        w_Hdg_Vars_Pnd_W_Nra.setInitialValue("NON-FATCA");
        w_Hdg_Vars_Pnd_W_Srce.setInitialValue("SRCE");
        w_Hdg_Vars_Pnd_W_Count.setInitialValue("COUNT");
        w_Hdg_Vars_Pnd_W_Amount.setInitialValue("AMOUNT");
        w_Hdg_Vars_Pnd_W_Ivc.setInitialValue("IVC");
        w_Hdg_Vars_Pnd_W_Amt2.setInitialValue("AMOUNT");
        w_Hdg_Vars_Pnd_W_Amt3.setInitialValue("AMOUNT");
        w_Hdg_Vars_Pnd_W_With.setInitialValue("WITHHELD");
        w_Hdg_Vars_Pnd_W_Paymt.setInitialValue("FOR PAYMENTS WITH");
        w_Hdg_Vars_Pnd_W_Slsh1.setInitialValue("--------------");
        w_Hdg_Vars_Pnd_W_Slsh2.setInitialValue("--------------");
        w_Hdg_Vars_Pnd_W_Canad.setInitialValue("CANADIAN");
        w_Hdg_Vars_Pnd_W_Cand2.setInitialValue("CANADIAN");
        w_Hdg_Vars_Pnd_W_Puerto.setInitialValue("PR");
        w_Hdg_Vars_Pnd_W_Withd.setInitialValue("WITHHOLDING");
        w_Hdg_Vars_Pnd_W_With2.setInitialValue("WITHHELD");
        w_Hdg_Vars_Pnd_W_With3.setInitialValue("WITHHELD");
        pnd_First_Rec.setInitialValue(true);
        pnd_A.setInitialValue(true);
        pnd_Page.setInitialValue(false);
        pnd_Sub_Skip.setInitialValue(false);
        pnd_Tot_Prt.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0946() throws Exception
    {
        super("Twrp0946");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  ------------------------------------------
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Var_File1_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Var_File1_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *READ WORK FILE 2  RECORD #FLAT-FILE3 /* 11/2014
        //*  11/2014
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            //*  ACCEPT IF #FF3-CAN-WTHLD-AMT > 0
            //*  J.ROTHOLZ 02/2002
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().notEquals(getZero()))))                                                                 //Natural: ACCEPT IF #FF3-CAN-WTHLD-AMT NE 0
            {
                continue;
            }
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
            if (condition(pnd_First_Rec.getBoolean()))                                                                                                                    //Natural: IF #FIRST-REC
            {
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                                   //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                    sub_Source_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                sub_Co_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ACCUM-COMPANY-TOTALS
                sub_Accum_Company_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  11/2014 END
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  11/2014
                //*  11/2014
                pnd_Sv_Cnt.reset();                                                                                                                                       //Natural: RESET #SV-CNT #SV-PRT-FIELDS ( * )
                pnd_Sv_Prt_Fields.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- PROCESS-RECORD
            //*                  -------------
            pnd_Tax_Amt.reset();                                                                                                                                          //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet298 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet298++;
                //*                                           BG
                //*        OR = '6')     H4
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet298++;
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR01:                                                                                                                                                        //Natural: FOR #K 1 3
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(3)); pnd_K.nadd(1))
            {
                //*  11/2014 START
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))                                                                    //Natural: IF #FF3-TWRPYMNT-FATCA-IND = 'Y'
                {
                    pnd_Counters_Pnd_Trans_Cnt_F.getValue(pnd_K).nadd(1);                                                                                                 //Natural: ADD 1 TO #TRANS-CNT-F ( #K )
                    pnd_Counters_Pnd_Gross_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                 //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT-F ( #K )
                    pnd_Counters_Pnd_Ivc_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                     //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT-F ( #K )
                    pnd_Counters_Pnd_Taxable_Amt_F.getValue(pnd_K).nadd(pnd_Tax_Amt);                                                                                     //Natural: ADD #TAX-AMT TO #TAXABLE-AMT-F ( #K )
                    pnd_Counters_Pnd_Int_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                     //Natural: ADD #FF3-INT-AMT TO #INT-AMT-F ( #K )
                    pnd_Counters_Pnd_Fed_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                               //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-AMT-F ( #K )
                    pnd_Counters_Pnd_Nra_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                               //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-AMT-F ( #K )
                    pnd_Counters_Pnd_Can_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                               //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-AMT-F ( #K )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_K).nadd(1);                                                                                                   //Natural: ADD 1 TO #TRANS-CNT ( #K )
                    pnd_Counters_Pnd_Gross_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                   //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                    pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                       //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                    pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_K).nadd(pnd_Tax_Amt);                                                                                       //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                    pnd_Counters_Pnd_Int_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                       //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                    pnd_Counters_Pnd_Fed_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                                 //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-AMT ( #K )
                    pnd_Counters_Pnd_Nra_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                                 //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-AMT ( #K )
                    pnd_Counters_Pnd_Can_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                                 //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-AMT ( #K )
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                if (condition((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ"))  //Natural: IF ( #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42' ) AND #FF3-STATE-WTHLD > 0
                    || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().greater(getZero()))))
                {
                    //*  11/2014 START
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))                                                                //Natural: IF #FF3-TWRPYMNT-FATCA-IND = 'Y'
                    {
                        pnd_Counters_Pnd_Pr_Amt_F.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                              //Natural: ADD #FF3-STATE-WTHLD TO #PR-AMT-F ( #K )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Counters_Pnd_Pr_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                //Natural: ADD #FF3-STATE-WTHLD TO #PR-AMT ( #K )
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
            sub_Co_Desc();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-COMPANY-TOTALS
            sub_Accum_Company_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  11/2014 END
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/2014 START
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Grand_Tot.setValue(true);                                                                                                                                     //Natural: ASSIGN #GRAND-TOT := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-GRAND-ACCUM
        sub_Print_Grand_Accum();
        if (condition(Global.isEscape())) {return;}
        //*  11/2014 END
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM()," -- CONTROL TOTAL",NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS READ      : ", //Natural: WRITE ( 1 ) *INIT-USER '-' *PROGRAM ' -- CONTROL TOTAL' // 'TOTAL NUMBER OF RECORDS READ      : ' #REC-READ / 'TOTAL NUMBER OF RECORDS PROCESSED : ' #REC-PROCESS
            pnd_Rec_Read,NEWLINE,"TOTAL NUMBER OF RECORDS PROCESSED : ",pnd_Rec_Process);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  ------------------------------------------
        //*  ------------------------------------------
        //*  ------------------------------------------
        //*  ------------------------------------------
        //*                  --------
        //*  ------------------------------------------
        //*                  ----------
        //*  ------------------------------------------
        //*  11/2014 START
        //* ***********************************************************************
        //* ***********************************************************************
        //*  11/2014 END
        //*  ------------------------------------------
        //*  ------------------------------------------
        //*  ------------------------------------------
        //* ***********************************************************************
        //* ***********************************************************************
        //*  ------------------------------------------
        //*                    -------------
        //*  ------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }
    private void sub_Accum_Company_Totals() throws Exception                                                                                                              //Natural: ACCUM-COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue("*").equals(pnd_Print_Com)))                                                                           //Natural: IF #TOT-CMPY-NAME ( * ) = #PRINT-COM
        {
            DbsUtil.examine(new ExamineSource(pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue("*")), new ExamineSearch(pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPY-NAME ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR02:                                                                                                                                                        //Natural: FOR #TOT-NDX 1 #CMPY-CNT
            for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
            {
                if (condition(pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue(pnd_Tot_Ndx).equals(" ")))                                                                     //Natural: IF #TOT-CMPY-NAME ( #TOT-NDX ) = ' '
                {
                    pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue(pnd_Tot_Ndx).setValue(pnd_Print_Com);                                                                    //Natural: ASSIGN #TOT-CMPY-NAME ( #TOT-NDX ) := #PRINT-COM
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx)),        //Natural: ASSIGN #TOT-TRANS-CNT ( #TOT-NDX ) := #TOT-TRANS-CNT ( #TOT-NDX ) + #TRANS-CNT ( 3 ) + #TRANS-CNT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Trans_Cnt.getValue(3)).add(pnd_Counters_Pnd_Trans_Cnt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx)),        //Natural: ASSIGN #TOT-GROSS-AMT ( #TOT-NDX ) := #TOT-GROSS-AMT ( #TOT-NDX ) + #GROSS-AMT ( 3 ) + #GROSS-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Gross_Amt.getValue(3)).add(pnd_Counters_Pnd_Gross_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx)),            //Natural: ASSIGN #TOT-IVC-AMT ( #TOT-NDX ) := #TOT-IVC-AMT ( #TOT-NDX ) + #IVC-AMT ( 3 ) + #IVC-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Ivc_Amt.getValue(3)).add(pnd_Counters_Pnd_Ivc_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx)),    //Natural: ASSIGN #TOT-TAXABLE-AMT ( #TOT-NDX ) := #TOT-TAXABLE-AMT ( #TOT-NDX ) + #TAXABLE-AMT ( 3 ) + #TAXABLE-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Taxable_Amt.getValue(3)).add(pnd_Counters_Pnd_Taxable_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx)),            //Natural: ASSIGN #TOT-INT-AMT ( #TOT-NDX ) := #TOT-INT-AMT ( #TOT-NDX ) + #INT-AMT ( 3 ) + #INT-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Int_Amt.getValue(3)).add(pnd_Counters_Pnd_Int_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt.getValue(pnd_Tot_Ndx)),            //Natural: ASSIGN #TOT-FED-AMT ( #TOT-NDX ) := #TOT-FED-AMT ( #TOT-NDX ) + #FED-AMT ( 3 ) + #FED-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Fed_Amt.getValue(3)).add(pnd_Counters_Pnd_Fed_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt.getValue(pnd_Tot_Ndx)),            //Natural: ASSIGN #TOT-NRA-AMT ( #TOT-NDX ) := #TOT-NRA-AMT ( #TOT-NDX ) + #NRA-AMT ( 3 ) + #NRA-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Nra_Amt.getValue(3)).add(pnd_Counters_Pnd_Nra_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt.getValue(pnd_Tot_Ndx)),            //Natural: ASSIGN #TOT-CAN-AMT ( #TOT-NDX ) := #TOT-CAN-AMT ( #TOT-NDX ) + #CAN-AMT ( 3 ) + #CAN-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Can_Amt.getValue(3)).add(pnd_Counters_Pnd_Can_Amt_F.getValue(3)));
        pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt.getValue(pnd_Tot_Ndx).compute(new ComputeParameters(false, pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt.getValue(pnd_Tot_Ndx)),              //Natural: ASSIGN #TOT-PR-AMT ( #TOT-NDX ) := #TOT-PR-AMT ( #TOT-NDX ) + #PR-AMT ( 3 ) + #PR-AMT-F ( 3 )
            pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt.getValue(pnd_Tot_Ndx).add(pnd_Counters_Pnd_Pr_Amt.getValue(3)).add(pnd_Counters_Pnd_Pr_Amt_F.getValue(3)));
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
        sub_Source_To_Prt();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                       //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                          //Natural: IF #TOT-PRT
        {
            ignore();
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
            pnd_Occ.setValue(4);                                                                                                                                          //Natural: ASSIGN #OCC := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT GRANDTOTAL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Occ.setValue(3);                                                                                                                                              //Natural: ASSIGN #OCC := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                         //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        pnd_Counters.getValue("*").reset();                                                                                                                               //Natural: RESET #COUNTERS ( * )
        pnd_A.setValue(true);                                                                                                                                             //Natural: ASSIGN #A := TRUE
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*  10/18/11
        short decideConditionsMet483 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("TRST");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet483++;
            pnd_Print_Com.setValue("FSB");                                                                                                                                //Natural: ASSIGN #PRINT-COM := 'FSB'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Print_Com.setValue("    ");                                                                                                                               //Natural: ASSIGN #PRINT-COM := '    '
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Ctr2_Ctr4() throws Exception                                                                                                                    //Natural: MOVE-CTR2-CTR4
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue(4).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue(2));                                                                          //Natural: ASSIGN #TRANS-CNT ( 4 ) := #TRANS-CNT ( 2 )
        pnd_Counters_Pnd_Gross_Amt.getValue(4).setValue(pnd_Counters_Pnd_Gross_Amt.getValue(2));                                                                          //Natural: ASSIGN #GROSS-AMT ( 4 ) := #GROSS-AMT ( 2 )
        pnd_Counters_Pnd_Ivc_Amt.getValue(4).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue(2));                                                                              //Natural: ASSIGN #IVC-AMT ( 4 ) := #IVC-AMT ( 2 )
        pnd_Counters_Pnd_Taxable_Amt.getValue(4).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue(2));                                                                      //Natural: ASSIGN #TAXABLE-AMT ( 4 ) := #TAXABLE-AMT ( 2 )
        pnd_Counters_Pnd_Int_Amt.getValue(4).setValue(pnd_Counters_Pnd_Int_Amt.getValue(2));                                                                              //Natural: ASSIGN #INT-AMT ( 4 ) := #INT-AMT ( 2 )
        pnd_Counters_Pnd_Fed_Amt.getValue(4).setValue(pnd_Counters_Pnd_Fed_Amt.getValue(2));                                                                              //Natural: ASSIGN #FED-AMT ( 4 ) := #FED-AMT ( 2 )
        pnd_Counters_Pnd_Nra_Amt.getValue(4).setValue(pnd_Counters_Pnd_Nra_Amt.getValue(2));                                                                              //Natural: ASSIGN #NRA-AMT ( 4 ) := #NRA-AMT ( 2 )
        pnd_Counters_Pnd_Can_Amt.getValue(4).setValue(pnd_Counters_Pnd_Can_Amt.getValue(2));                                                                              //Natural: ASSIGN #CAN-AMT ( 4 ) := #CAN-AMT ( 2 )
        pnd_Counters_Pnd_Pr_Amt.getValue(4).setValue(pnd_Counters_Pnd_Pr_Amt.getValue(2));                                                                                //Natural: ASSIGN #PR-AMT ( 4 ) := #PR-AMT ( 2 )
        pnd_Counters_Pnd_Trans_Cnt_F.getValue(4).setValue(pnd_Counters_Pnd_Trans_Cnt_F.getValue(2));                                                                      //Natural: ASSIGN #TRANS-CNT-F ( 4 ) := #TRANS-CNT-F ( 2 )
        pnd_Counters_Pnd_Gross_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Gross_Amt_F.getValue(2));                                                                      //Natural: ASSIGN #GROSS-AMT-F ( 4 ) := #GROSS-AMT-F ( 2 )
        pnd_Counters_Pnd_Ivc_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Ivc_Amt_F.getValue(2));                                                                          //Natural: ASSIGN #IVC-AMT-F ( 4 ) := #IVC-AMT-F ( 2 )
        pnd_Counters_Pnd_Taxable_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Taxable_Amt_F.getValue(2));                                                                  //Natural: ASSIGN #TAXABLE-AMT-F ( 4 ) := #TAXABLE-AMT-F ( 2 )
        pnd_Counters_Pnd_Int_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Int_Amt_F.getValue(2));                                                                          //Natural: ASSIGN #INT-AMT-F ( 4 ) := #INT-AMT-F ( 2 )
        pnd_Counters_Pnd_Fed_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Fed_Amt_F.getValue(2));                                                                          //Natural: ASSIGN #FED-AMT-F ( 4 ) := #FED-AMT-F ( 2 )
        pnd_Counters_Pnd_Nra_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Nra_Amt_F.getValue(2));                                                                          //Natural: ASSIGN #NRA-AMT-F ( 4 ) := #NRA-AMT-F ( 2 )
        pnd_Counters_Pnd_Can_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Can_Amt_F.getValue(2));                                                                          //Natural: ASSIGN #CAN-AMT-F ( 4 ) := #CAN-AMT-F ( 2 )
        pnd_Counters_Pnd_Pr_Amt_F.getValue(4).setValue(pnd_Counters_Pnd_Pr_Amt_F.getValue(2));                                                                            //Natural: ASSIGN #PR-AMT-F ( 4 ) := #PR-AMT-F ( 2 )
        pnd_Occ.setValue(2);                                                                                                                                              //Natural: ASSIGN #OCC := 2
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Occ)); pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_J).reset();                                                                                                           //Natural: RESET #TRANS-CNT ( #J ) #GROSS-AMT ( #J ) #IVC-AMT ( #J ) #TAXABLE-AMT ( #J ) #INT-AMT ( #J ) #FED-AMT ( #J ) #NRA-AMT ( #J ) #CAN-AMT ( #J ) #PR-AMT ( #J ) #TRANS-CNT-F ( #J ) #GROSS-AMT-F ( #J ) #IVC-AMT-F ( #J ) #TAXABLE-AMT-F ( #J ) #INT-AMT-F ( #J ) #FED-AMT-F ( #J ) #NRA-AMT-F ( #J ) #CAN-AMT-F ( #J ) #PR-AMT-F ( #J )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Fed_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Nra_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Can_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Pr_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Trans_Cnt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Gross_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Fed_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Nra_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Can_Amt_F.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Pr_Amt_F.getValue(pnd_J).reset();
            //*  11/2014 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        //*  FOR DETAIL PER SOURCE-CODE
        short decideConditionsMet530 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 1
        if (condition((pnd_Occ.equals(1))))
        {
            decideConditionsMet530++;
            pnd_Print_Fields_Pnd_Prtfld1.setValue(pnd_New_Source);                                                                                                        //Natural: MOVE #NEW-SOURCE TO #PRTFLD1
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
            //*  FOR TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Prt.setValue(false);                                                                                                                                  //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Occ.equals(4))))
        {
            decideConditionsMet530++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            //*  09/21/04  RM
            if (condition(pnd_Sv_Srce12.equals("YY")))                                                                                                                    //Natural: IF #SV-SRCE12 = 'YY'
            {
                pnd_Sv_Srce12.setValue("NL");                                                                                                                             //Natural: ASSIGN #SV-SRCE12 := 'NL'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sv_Srce12.equals("ZZ")))                                                                                                                    //Natural: IF #SV-SRCE12 = 'ZZ'
            {
                pnd_Sv_Srce12.setValue("ML");                                                                                                                             //Natural: ASSIGN #SV-SRCE12 := 'ML'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      (", pnd_Sv_Srce12, ")"));                                   //Natural: COMPRESS 'TOTAL      (' #SV-SRCE12 ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Sub_Skip.setValue(true);                                                                                                                                  //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Counters.getValue(4).reset();                                                                                                                             //Natural: RESET #COUNTERS ( 4 )
            pnd_Tot_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Occ.equals(3))))
        {
            decideConditionsMet530++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL (", pnd_Print_Com, ")"));                                   //Natural: COMPRESS 'GRANDTOTAL (' #PRINT-COM ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Page.setValue(true);                                                                                                                                      //Natural: ASSIGN #PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Grand_Accum() throws Exception                                                                                                                 //Natural: PRINT-GRAND-ACCUM
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #OCC 1 #CMPY-CNT
        for (pnd_Occ.setValue(1); condition(pnd_Occ.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Occ.nadd(1))
        {
            if (condition(pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue(pnd_Occ).equals(" ")))                                                                             //Natural: IF #TOT-CMPY-NAME ( #OCC ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress("Grand Total", pnd_Tot_Per_Cmpny_Pnd_Tot_Cmpy_Name.getValue(pnd_Occ)));                                //Natural: COMPRESS 'Grand Total' #TOT-CMPY-NAME ( #OCC ) INTO #PRTFLD1
            pnd_Print_Fields_Pnd_Prtfld2.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Trans_Cnt.getValue(pnd_Occ),new ReportEditMask("ZZZ,ZZ9"));                             //Natural: MOVE EDITED #TOT-TRANS-CNT ( #OCC ) ( EM = ZZZ,ZZ9 ) TO #PRTFLD2
            pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Gross_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZZ,ZZ9.99-"));                   //Natural: MOVE EDITED #TOT-GROSS-AMT ( #OCC ) ( EM = ZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD3
            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Ivc_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                         //Natural: MOVE EDITED #TOT-IVC-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD4
            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Taxable_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZZ,ZZ9.99-"));                 //Natural: MOVE EDITED #TOT-TAXABLE-AMT ( #OCC ) ( EM = ZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD5
            pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Int_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                         //Natural: MOVE EDITED #TOT-INT-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD6
            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Fed_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                         //Natural: MOVE EDITED #TOT-FED-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD7
            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Nra_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                         //Natural: MOVE EDITED #TOT-NRA-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD8
            pnd_Print0_Fields_Pnd_Prtfld9.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Can_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                        //Natural: MOVE EDITED #TOT-CAN-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD9
            pnd_Print0_Fields_Pnd_Prtfld10.setValueEdited(pnd_Tot_Per_Cmpny_Pnd_Tot_Pr_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZ,ZZ9.99-"));                        //Natural: MOVE EDITED #TOT-PR-AMT ( #OCC ) ( EM = ZZZZZ,ZZ9.99- ) TO #PRTFLD10
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 98T #PRINT0-FIELDS
                TabSetting(98),pnd_Print0_Fields_Pnd_Prtfld9,pnd_Print0_Fields_Pnd_Prtfld10);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Rtn() throws Exception                                                                                                                         //Natural: PRINT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        pnd_Print_Fields_Pnd_Prtfld2.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Occ),new ReportEditMask("ZZZZ,ZZ9"));                                         //Natural: MOVE EDITED #TRANS-CNT ( #OCC ) ( EM = ZZZZ,ZZ9 ) TO #PRTFLD2
        pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                               //Natural: MOVE EDITED #GROSS-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD3
        pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #IVC-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD4
        pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                             //Natural: MOVE EDITED #TAXABLE-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD5
        pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #INT-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD6
        pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #FED-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD7
        pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                    //Natural: MOVE EDITED #NRA-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD8
        //*  11/2014 START
        pnd_Sv_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #SV-CNT
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld1.getValue(pnd_Sv_Cnt).setValue(pnd_Print_Fields_Pnd_Prtfld1);                                                                     //Natural: ASSIGN #SV-PRTFLD1 ( #SV-CNT ) := #PRTFLD1
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld2.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Trans_Cnt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZ,ZZ9"));              //Natural: MOVE EDITED #TRANS-CNT-F ( #OCC ) ( EM = ZZZZ,ZZ9 ) TO #SV-PRTFLD2 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld3.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Gross_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));    //Natural: MOVE EDITED #GROSS-AMT-F ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #SV-PRTFLD3 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld4.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Ivc_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));         //Natural: MOVE EDITED #IVC-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD4 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld5.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Taxable_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));  //Natural: MOVE EDITED #TAXABLE-AMT-F ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #SV-PRTFLD5 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld6.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Int_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));         //Natural: MOVE EDITED #INT-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD6 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld7.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Fed_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));         //Natural: MOVE EDITED #FED-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD7 ( #SV-CNT )
        pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld8.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Nra_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));         //Natural: MOVE EDITED #NRA-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD8 ( #SV-CNT )
        //*  11/2014 END
    }
    private void sub_Print_Rtn0() throws Exception                                                                                                                        //Natural: PRINT-RTN0
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print0_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Can_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                   //Natural: MOVE EDITED #CAN-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD9
        pnd_Print0_Fields_Pnd_Prtfld10.setValueEdited(pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                                   //Natural: MOVE EDITED #PR-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD10
        //*  11/2014 START
        pnd_Sv_Print0_Pnd_Sv_Prtfld9.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Can_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));             //Natural: MOVE EDITED #CAN-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD9 ( #SV-CNT )
        pnd_Sv_Print0_Pnd_Sv_Prtfld10.getValue(pnd_Sv_Cnt).setValueEdited(pnd_Counters_Pnd_Pr_Amt_F.getValue(pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));             //Natural: MOVE EDITED #PR-AMT-F ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #SV-PRTFLD10 ( #SV-CNT )
        //*  11/2014 END
    }
    private void sub_Print_Rtn1() throws Exception                                                                                                                        //Natural: PRINT-RTN1
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
        //*  11/2014
        w_Hdg_Vars_Pnd_W_Hdg_1.resetInitial();                                                                                                                            //Natural: RESET INITIAL #W-HDG-1
        getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 98T #PRINT0-FIELDS
            TabSetting(98),pnd_Print0_Fields_Pnd_Prtfld9,pnd_Print0_Fields_Pnd_Prtfld10);
        if (Global.isEscape()) return;
        pnd_Print_Fields.reset();                                                                                                                                         //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
        pnd_Print0_Fields.reset();
        if (condition(pnd_Sub_Skip.getBoolean()))                                                                                                                         //Natural: IF #SUB-SKIP
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Sub_Skip.setValue(false);                                                                                                                                 //Natural: ASSIGN #SUB-SKIP := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/2014 START
        if (condition(pnd_Page.getBoolean()))                                                                                                                             //Natural: IF #PAGE
        {
            w_Hdg_Vars_Pnd_W_Nra.setValue("  FATCA  ");                                                                                                                   //Natural: ASSIGN #W-NRA := '  FATCA  '
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SAVED-FIELDS
            sub_Print_Saved_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  11/2014 END
            w_Hdg_Vars_Pnd_W_Hdg_1.resetInitial();                                                                                                                        //Natural: RESET INITIAL #W-HDG-1
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Page.setValue(false);                                                                                                                                     //Natural: ASSIGN #PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Saved_Fields() throws Exception                                                                                                                //Natural: PRINT-SAVED-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #D 1 #SV-CNT
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(pnd_Sv_Cnt)); pnd_D.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld1.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld2.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld3.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld4.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld5.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld6.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld7.getValue(pnd_D),pnd_Sv_Prt_Fields_Pnd_Sv_Prtfld8.getValue(pnd_D),NEWLINE,new  //Natural: WRITE ( 1 ) #SV-PRT-FIELDS ( #D ) / 98T #SV-PRINT0 ( #D )
                TabSetting(98),pnd_Sv_Print0_Pnd_Sv_Prtfld9.getValue(pnd_D),pnd_Sv_Print0_Pnd_Sv_Prtfld10.getValue(pnd_D));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source12.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                                             //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Source34.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                                             //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        //*  09/21/04  RM
        short decideConditionsMet645 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Source34.equals("  "))))
        {
            decideConditionsMet645++;
            pnd_Source.setValue(pnd_Source12);                                                                                                                            //Natural: ASSIGN #SOURCE := #SOURCE12
        }                                                                                                                                                                 //Natural: VALUE 'OL', 'TM', 'ML', 'AL', 'IL', 'PL', 'NL'
        else if (condition((pnd_Source34.equals("OL") || pnd_Source34.equals("TM") || pnd_Source34.equals("ML") || pnd_Source34.equals("AL") || pnd_Source34.equals("IL") 
            || pnd_Source34.equals("PL") || pnd_Source34.equals("NL"))))
        {
            decideConditionsMet645++;
            pnd_Source.setValue(pnd_Source34);                                                                                                                            //Natural: ASSIGN #SOURCE := #SOURCE34
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Source.setValue(" ");                                                                                                                                     //Natural: ASSIGN #SOURCE := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet654 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Source.equals("DS"))))
        {
            decideConditionsMet654++;
            pnd_New_Source.setValue("DSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Source.equals("GS"))))
        {
            decideConditionsMet654++;
            pnd_New_Source.setValue("GSRA");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Source.equals("MS"))))
        {
            decideConditionsMet654++;
            pnd_New_Source.setValue("MSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Source.equals("SS"))))
        {
            decideConditionsMet654++;
            pnd_New_Source.setValue("SSSS");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_New_Source.setValue(pnd_Source);                                                                                                                          //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_A.getBoolean()))                                                                                                                                //Natural: IF #A
        {
            pnd_Sv_Srce12.setValue(pnd_Source12);                                                                                                                         //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
            pnd_A.setValue(false);                                                                                                                                        //Natural: ASSIGN #A := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Source12.equals(pnd_Sv_Srce12)))                                                                                                                //Natural: IF #SOURCE12 = #SV-SRCE12
        {
            pnd_Occ.setValue(1);                                                                                                                                          //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Occ.setValue(4);                                                                                                                                          //Natural: ASSIGN #OCC := 4
            //*  PRINT DETAIL OF SV-SRCE-CDE
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Occ.setValue(1);                                                                                                                                          //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Sv_Srce12.setValue(pnd_Source12);                                                                                                                         //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  11/2014 START
                    if (condition(pnd_Grand_Tot.getBoolean()))                                                                                                            //Natural: IF #GRAND-TOT
                    {
                        pnd_Print_Com.setValue("ALL ");                                                                                                                   //Natural: ASSIGN #PRINT-COM := 'ALL '
                        w_Hdg_Vars_Pnd_W_Nra.setValue("   NRA   ");                                                                                                       //Natural: ASSIGN #W-NRA := '   NRA   '
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                        sub_Co_Desc();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 116T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 52T 'CANADIAN TRANSACTIONS' / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 92T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY : ' #PRINT-COM // #W-HDG1 / #W-HDG2 / #W-HDG3 / #W-HDG4 / #W-HDG5 //
                        TabSetting(116),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(52),"CANADIAN TRANSACTIONS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new 
                        TabSetting(92),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",
                        pnd_Print_Com,NEWLINE,NEWLINE,w_Hdg_Vars_Pnd_W_Hdg1,NEWLINE,w_Hdg_Vars_Pnd_W_Hdg2,NEWLINE,w_Hdg_Vars_Pnd_W_Hdg3,NEWLINE,w_Hdg_Vars_Pnd_W_Hdg4,
                        NEWLINE,w_Hdg_Vars_Pnd_W_Hdg5,NEWLINE,NEWLINE);
                    //*    'SYSTEM' 14X 'TRANS'    10X 'GROSS'   11X 'TAX FREE' 10X 'TAXABLE'
                    //*    9X   'INTEREST'  8X 'FEDERAL' 11X 'NRA' /
                    //*    'SRCE'   16X 'COUNT'    10X 'AMOUNT'  13X 'IVC'      13X 'AMOUNT'
                    //*    10X  'AMOUNT'    8X 'WITHHELD'   8X 'WITHHELD'  /
                    //*    31T  'FOR PAYMENTS WITH'   98T '--------------'  2X '--------------'
                    //*    /  35T     'CANADIAN'        101T 'CANADIAN'  11X 'PR'
                    //*    /  34T    'WITHHOLDING'      101T 'WITHHELD'  8X  'WITHHELD'  //
                    //*  11/2014 END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
