/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:16 PM
**        * FROM NATURAL PROGRAM : Twrp3619
************************************************************
**        * FILE NAME            : Twrp3619.java
**        * CLASS NAME           : Twrp3619
**        * INSTANCE NAME        : Twrp3619
************************************************************
************************************************************************
** PROGRAM : TWRP3619
** SYSTEM  : TAXWARS
** AUTHOR  : JULIUS BREMER
** FUNCTION: NEW YORK STATE CUNU/SUNY FILE EXTRACT FOR DTF
**  HISTORY
** 08/08/12    J. BREMER  -  CUNY/SUNY PHASE 2
** 06/01/17    RAHUL DAS  -  SUPPRESSING PA COTRACTS FROM BEING
**                           APPEARED INTO THE REJECT REPORT, AS THEY
**                           ARE VALID CASES WHERE SUPPLEMENTS SHOULD
**                           NOT GET GENERATED - PRB81266 TAG: DASRAH
** 10/05/18    ARIVU     -   EIN CHANGE - TAG: EINCHG
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3619 extends BLNatBase
{
    // Data Areas
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwratin pdaTwratin;
    private LdaTwrl9715 ldaTwrl9715;
    private LdaTwrl3620 ldaTwrl3620;
    private PdaTwraclr2 pdaTwraclr2;
    private PdaTwraplr2 pdaTwraplr2;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_form_U;
    private DbsField form_U_Tirf_Tax_Year;
    private DbsField form_U_Tirf_Contract_Nbr;
    private DbsField form_U_Tirf_Tin;
    private DbsField form_U_Tirf_Lu_User;
    private DbsField form_U_Tirf_Lu_Ts;
    private DbsField form_U_Tirf_Irs_Reject_Ind;
    private DbsField form_U_Count_Casttirf_1099_R_State_Grp;
    private DbsField form_U_Count_Casttirf_Ltr_Grp;
    private DbsField form_U_Tirf_Irs_Rpt_Ind;
    private DbsField form_U_Tirf_Irs_Rpt_Date;

    private DbsGroup form_U_Tirf_Ltr_Grp;
    private DbsField form_U_Tirf_Plan_Type;
    private DbsField form_U_Tirf_Plan;

    private DbsGroup form_U_Tirf_1099_R_State_Grp;
    private DbsField form_U_Tirf_State_Code;
    private DbsField form_U_Tirf_State_Auth_Rpt_Ind;
    private DbsField form_U_Tirf_State_Auth_Rpt_Date;

    private DataAccessProgramView vw_twr_Frm;
    private DbsField twr_Frm_Tirf_Tax_Year;
    private DbsField twr_Frm_Tirf_Form_Type;
    private DbsField twr_Frm_Tirf_Company_Cde;
    private DbsField twr_Frm_Tirf_Tax_Id_Type;
    private DbsField twr_Frm_Tirf_Tin;
    private DbsField twr_Frm_Tirf_Contract_Nbr;
    private DbsField twr_Frm_Tirf_Payee_Cde;
    private DbsField twr_Frm_Tirf_Ltr_Data;
    private DbsField twr_Frm_Count_Casttirf_Ltr_Grp;

    private DbsGroup twr_Frm_Tirf_Ltr_Grp;
    private DbsField twr_Frm_Tirf_Ltr_Gross_Amt;
    private DbsField twr_Frm_Tirf_Ltr_Per;
    private DbsField twr_Frm_Tirf_Ltr_Fed_Tax_Wthld;
    private DbsField twr_Frm_Tirf_Ltr_State_Tax_Wthld;
    private DbsField twr_Frm_Tirf_Ltr_Loc_Tax_Wthld;

    private DataAccessProgramView vw_twr_Frm1;
    private DbsField twr_Frm1_Tirf_Tax_Year;
    private DbsField twr_Frm1_Tirf_Form_Type;
    private DbsField twr_Frm1_Tirf_Company_Cde;
    private DbsField twr_Frm1_Tirf_Tax_Id_Type;
    private DbsField twr_Frm1_Tirf_Tin;
    private DbsField twr_Frm1_Tirf_Active_Ind;
    private DbsField twr_Frm1_Tirf_Contract_Nbr;
    private DbsField twr_Frm1_Tirf_Payee_Cde;
    private DbsField twr_Frm1_Tirf_Ltr_Data;
    private DbsField twr_Frm1_Count_Casttirf_Ltr_Grp;

    private DbsGroup twr_Frm1_Tirf_Ltr_Grp;
    private DbsField twr_Frm1_Tirf_Ltr_Gross_Amt;
    private DbsField twr_Frm1_Tirf_Ltr_Per;
    private DbsField twr_Frm1_Tirf_Ltr_Fed_Tax_Wthld;
    private DbsField twr_Frm1_Tirf_Ltr_State_Tax_Wthld;
    private DbsField twr_Frm1_Tirf_Ltr_Loc_Tax_Wthld;

    private DataAccessProgramView vw_twr_Frm2;
    private DbsField twr_Frm2_Tirf_Tax_Year;
    private DbsField twr_Frm2_Tirf_Form_Type;
    private DbsField twr_Frm2_Tirf_Company_Cde;
    private DbsField twr_Frm2_Tirf_Tax_Id_Type;
    private DbsField twr_Frm2_Tirf_Tin;
    private DbsField twr_Frm2_Tirf_Active_Ind;
    private DbsField twr_Frm2_Tirf_Contract_Nbr;
    private DbsField twr_Frm2_Tirf_Payee_Cde;
    private DbsField twr_Frm2_Tirf_Ltr_Data;
    private DbsField twr_Frm2_Count_Casttirf_Ltr_Grp;

    private DbsGroup twr_Frm2_Tirf_Ltr_Grp;
    private DbsField twr_Frm2_Tirf_Ltr_Gross_Amt;
    private DbsField twr_Frm2_Tirf_Ltr_Per;
    private DbsField twr_Frm2_Tirf_Ltr_Fed_Tax_Wthld;
    private DbsField twr_Frm2_Tirf_Ltr_State_Tax_Wthld;
    private DbsField twr_Frm2_Tirf_Ltr_Loc_Tax_Wthld;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Hold_Tin;
    private DbsField pnd_Hold_Contract;
    private DbsField pnd_Mid_Name;

    private DbsGroup pnd_Mid_Name__R_Field_1;
    private DbsField pnd_Mid_Name_Pnd_Mid_Name1;
    private DbsField pnd_Daten;

    private DbsGroup pnd_Daten__R_Field_2;
    private DbsField pnd_Daten_Pnd_Daten_Cc;
    private DbsField pnd_Daten_Pnd_Daten_Yy;
    private DbsField pnd_Daten_Pnd_Daten_Mm;
    private DbsField pnd_Daten_Pnd_Daten_Dd;
    private DbsField pnd_Tirf_Superde_1;

    private DbsGroup pnd_Tirf_Superde_1__R_Field_3;
    private DbsField pnd_Tirf_Superde_1_Tirf_Tax_Year;
    private DbsField pnd_Tirf_Superde_1_Tirf_Tin;
    private DbsField pnd_Tirf_Superde_1_Tirf_Form_Type;
    private DbsField pnd_Tirf_Superde_1_Tirf_Contract_Nbr;
    private DbsField pnd_Tirf_Superde_1_Tirf_Payee_Cde;
    private DbsField pnd_Tirf_Superde_1_Tirf_Key;
    private DbsField pnd_Tirf_Superde_1_Tirf_Invrse_Create_Ts;
    private DbsField pnd_Tirf2_Superde_1;

    private DbsGroup pnd_Tirf2_Superde_1__R_Field_4;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Tax_Year;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Tin;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Form_Type;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Contract_Nbr;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Payee_Cde;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Key;
    private DbsField pnd_Tirf2_Superde_1_Tirf_Invrse_Create_Ts;
    private DbsField pnd_Actual_Reject;
    private DbsField pnd_Pa_Select;
    private DbsField pnd_Any_Ltr;
    private DbsField pnd_Super_Start;
    private DbsField pnd_Super2_Start;
    private DbsField pnd_Tirf_Superde_3;

    private DbsGroup pnd_Tax_Tape_Totals;
    private DbsField pnd_Tax_Tape_Totals_Pnd_Tot_Ny_Gross;
    private DbsField pnd_Tax_Tape_Totals_Pnd_Grand_1w;
    private DbsField pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc;
    private DbsField pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej;
    private DbsField pnd_Tax_Tape_Totals_Pnd_Grand_1e;
    private DbsField pnd_Plan_Name;

    private DbsGroup pnd_Plan_Name__R_Field_5;
    private DbsField pnd_Plan_Name_Pnd_Plan_Name1;
    private DbsField pnd_Plan_Name_Pnd_Plan_Name2;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Pnd_State_Max;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Tax_Year;

    private DbsGroup pnd_Ws__R_Field_6;
    private DbsField pnd_Ws_Pnd_Tax_Year_A;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_I1;
    private DbsField pnd_Ws_Pnd_J1;
    private DbsField pnd_Ws_Pnd_K1;
    private DbsField pnd_Ws_Pnd_L1;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Index;
    private DbsField pnd_Ws_Pnd_Num;
    private DbsField pnd_Ws_Pnd_Out_Ny_Tin_Nohyph;

    private DbsGroup pnd_Ws__R_Field_7;
    private DbsField pnd_Ws_First_3;
    private DbsField pnd_Ws_First_H;
    private DbsField pnd_Ws_Sec_2;
    private DbsField pnd_Ws_Sec_H;
    private DbsField pnd_Ws_Last_4;
    private DbsField pnd_Ws_Pnd_Hold_Tax_Year;

    private DbsGroup pnd_Ws__R_Field_8;
    private DbsField pnd_Ws_Pnd_Hold_Cent;
    private DbsField pnd_Ws_Pnd_Hold_Year;
    private DbsField pnd_Ws_Pnd_St_Occ;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_Valid_State;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Accepted_Form_Cnt;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_Disp_Rec;
    private DbsField pnd_Ws_Pnd_S8;
    private DbsField pnd_Ws_Pnd_Empty_Accept_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Reject_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Cnt;
    private DbsField pnd_Ws_Pnd_Empty_Frm2;
    private DbsField pnd_Ws_Pnd_Empty_Tot_Frm2;

    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Tircntl_State_Old_Code;
    private DbsField pnd_State_Table_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_Tircntl_State_Full_Name;
    private DbsField pnd_State_Table_Tircntl_Comb_Fed_Ind;
    private DbsField pnd_State_Table_Tircntl_State_Ind;
    private DbsField pnd_State_Table_Tircntl_Media_Code;
    private DbsField pnd_S1_End;

    private DbsGroup pnd_Hold_Fields;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Rej_Ind;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Cuny_Cont;
    private DbsField pnd_Hold_Fields_Pnd_Hold_Suny_Cont;
    private DbsField pnd_Plan_Table_Key;

    private DbsGroup pnd_Plan_Table_Key__R_Field_9;
    private DbsField pnd_Plan_Table_Key_Pnd_Pt_A_I_Ind;
    private DbsField pnd_Plan_Table_Key_Pnd_Pt_Table_Id;
    private DbsField pnd_Plan_Table_Key_Pnd_Pt_Plan_Id;
    private DbsField pnd_Header_Record1;
    private DbsField pnd_Header_Record2;
    private DbsField pnd_Sub_Head;
    private DbsField pnd_Sub_Head2;
    private DbsField pnd_Extract_Reject_Ind;
    private DbsField pnd_Extract_Name;
    private DbsField pnd_Extract_Gross_Amt_Header;
    private DbsField pnd_Extract_Fed_Tax_Header_N;
    private DbsField pnd_Extract_Stat_Tax_Header_N;
    private DbsField pnd_Extract_Local_Tax_Header_N;
    private DbsField pnd_Tin_Gross_Acc;
    private DbsField pnd_Tin_Fed_Tax_Acc;
    private DbsField pnd_Tin_State_Tax_Acc;
    private DbsField pnd_Tin_Local_Tax_Acc;
    private DbsField pnd_Tin_Gross_Rej;
    private DbsField pnd_Tin_Fed_Tax_Rej;
    private DbsField pnd_Tin_State_Tax_Rej;
    private DbsField pnd_Tin_Local_Tax_Rej;
    private DbsField pnd_Grand_Gross;
    private DbsField pnd_Grand_Fed_Tax;
    private DbsField pnd_Grand_State_Tax;
    private DbsField pnd_Grand_Local_Tax;
    private DbsField pnd_Cuny_Part_Cnt;
    private DbsField pnd_Suny_Part_Cnt;
    private DbsField pnd_Total_Forms;
    private DbsField pnd_Accepted_Forms;
    private DbsField pnd_Rejected_Forms;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Employee_Desc;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Et_Limit;
    private DbsField pnd_Cuny_Suny_Plan;
    private DbsField pnd_Client_Ids;
    private DbsField pnd_Plan_Type;
    private DbsField pnd_Report_Type;
    private DbsField pnd_Gross_Disp;
    private DbsField pnd_Fill;
    private DbsField pnd_Print_Suny;
    private DbsField pnd_Print_Cuny;
    private DbsField pnd_Print_Both;

    private DbsGroup pnd_Client_Table;
    private DbsField pnd_Client_Table_Pnd_Ct_Data;

    private DbsGroup pnd_Client_Table__R_Field_10;

    private DbsGroup pnd_Client_Table_Pnd_Ct_Keys;
    private DbsField pnd_Client_Table_Pnd_Ct_Key;

    private DbsGroup pnd_Client_Table__R_Field_11;
    private DbsField pnd_Client_Table_Pnd_Ct_Bad_Cl;
    private DbsField pnd_Client_Table_Pnd_Ct_Tin;
    private DbsField pnd_Client_Table_Pnd_Ct_Fix_Cl;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_12;
    private DbsField pnd_Key_Pnd_Key_Cl;
    private DbsField pnd_Key_Pnd_Key_Tin;
    private DbsField pnd_Num_X;
    private DbsField pnd_Idx_X;
    private DbsField pnd_No_Suny;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());
        ldaTwrl3620 = new LdaTwrl3620();
        registerRecord(ldaTwrl3620);
        pdaTwraclr2 = new PdaTwraclr2(localVariables);
        pdaTwraplr2 = new PdaTwraplr2(localVariables);

        // Local Variables

        vw_form_U = new DataAccessProgramView(new NameInfo("vw_form_U", "FORM-U"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        form_U_Tirf_Tax_Year = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_U_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_U_Tirf_Contract_Nbr = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_U_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_U_Tirf_Tin = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_U_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_U_Tirf_Lu_User = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_U_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_U_Tirf_Lu_Ts = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_U_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_U_Tirf_Irs_Reject_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Reject_Ind", "TIRF-IRS-REJECT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_REJECT_IND");
        form_U_Tirf_Irs_Reject_Ind.setDdmHeader("IRS/REJ/IND");
        form_U_Count_Casttirf_1099_R_State_Grp = vw_form_U.getRecord().newFieldInGroup("form_U_Count_Casttirf_1099_R_State_Grp", "C*TIRF-1099-R-STATE-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Count_Casttirf_Ltr_Grp = vw_form_U.getRecord().newFieldInGroup("form_U_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_U_Tirf_Irs_Rpt_Ind = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_U_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_U_Tirf_Irs_Rpt_Date = vw_form_U.getRecord().newFieldInGroup("form_U_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_U_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");

        form_U_Tirf_Ltr_Grp = vw_form_U.getRecord().newGroupInGroup("form_U_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_U_Tirf_Plan_Type = form_U_Tirf_Ltr_Grp.newFieldArrayInGroup("form_U_Tirf_Plan_Type", "TIRF-PLAN-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_PLAN_TYPE", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_U_Tirf_Plan_Type.setDdmHeader("PLAN/TYPE");
        form_U_Tirf_Plan = form_U_Tirf_Ltr_Grp.newFieldArrayInGroup("form_U_Tirf_Plan", "TIRF-PLAN", FieldType.STRING, 6, new DbsArrayController(1, 12) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_PLAN", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        form_U_Tirf_Plan.setDdmHeader("PLAN/NUM-/BER");

        form_U_Tirf_1099_R_State_Grp = vw_form_U.getRecord().newGroupInGroup("form_U_Tirf_1099_R_State_Grp", "TIRF-1099-R-STATE-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_1099_R_State_Grp.setDdmHeader("1099-R/STATE/GROUP");
        form_U_Tirf_State_Code = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Code", "TIRF-STATE-CODE", FieldType.STRING, 2, new 
            DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_CODE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Code.setDdmHeader("RESI-/DENCY/CODE");
        form_U_Tirf_State_Auth_Rpt_Ind = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Ind", "TIRF-STATE-AUTH-RPT-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_IND", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Ind.setDdmHeader("STATE/RPT/IND");
        form_U_Tirf_State_Auth_Rpt_Date = form_U_Tirf_1099_R_State_Grp.newFieldArrayInGroup("form_U_Tirf_State_Auth_Rpt_Date", "TIRF-STATE-AUTH-RPT-DATE", 
            FieldType.DATE, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_STATE_AUTH_RPT_DATE", "TWRFRM_FORM_FILE_TIRF_1099_R_STATE_GRP");
        form_U_Tirf_State_Auth_Rpt_Date.setDdmHeader("STATE/AUTH RPT/DATE");
        registerRecord(vw_form_U);

        vw_twr_Frm = new DataAccessProgramView(new NameInfo("vw_twr_Frm", "TWR-FRM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        twr_Frm_Tirf_Tax_Year = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        twr_Frm_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        twr_Frm_Tirf_Form_Type = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        twr_Frm_Tirf_Form_Type.setDdmHeader("FORM");
        twr_Frm_Tirf_Company_Cde = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        twr_Frm_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        twr_Frm_Tirf_Tax_Id_Type = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        twr_Frm_Tirf_Tin = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        twr_Frm_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        twr_Frm_Tirf_Contract_Nbr = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        twr_Frm_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        twr_Frm_Tirf_Payee_Cde = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        twr_Frm_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        twr_Frm_Tirf_Ltr_Data = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Tirf_Ltr_Data", "TIRF-LTR-DATA", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_LTR_DATA");
        twr_Frm_Tirf_Ltr_Data.setDdmHeader("LTR/DATA");
        twr_Frm_Count_Casttirf_Ltr_Grp = vw_twr_Frm.getRecord().newFieldInGroup("twr_Frm_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");

        twr_Frm_Tirf_Ltr_Grp = vw_twr_Frm.getRecord().newGroupInGroup("twr_Frm_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_Gross_Amt = twr_Frm_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm_Tirf_Ltr_Gross_Amt", "TIRF-LTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_GROSS_AMT", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_Gross_Amt.setDdmHeader("LETTER/GROSS/AMOUNT");
        twr_Frm_Tirf_Ltr_Per = twr_Frm_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm_Tirf_Ltr_Per", "TIRF-LTR-PER", FieldType.PACKED_DECIMAL, 8, 5, new DbsArrayController(1, 
            12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_PER", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_Per.setDdmHeader("LTR/PCT");
        twr_Frm_Tirf_Ltr_Fed_Tax_Wthld = twr_Frm_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm_Tirf_Ltr_Fed_Tax_Wthld", "TIRF-LTR-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_FED_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_Fed_Tax_Wthld.setDdmHeader("LTR/FED/TAX");
        twr_Frm_Tirf_Ltr_State_Tax_Wthld = twr_Frm_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm_Tirf_Ltr_State_Tax_Wthld", "TIRF-LTR-STATE-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_State_Tax_Wthld.setDdmHeader("LTR/STATE/TAX");
        twr_Frm_Tirf_Ltr_Loc_Tax_Wthld = twr_Frm_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm_Tirf_Ltr_Loc_Tax_Wthld", "TIRF-LTR-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm_Tirf_Ltr_Loc_Tax_Wthld.setDdmHeader("LTR/LOCAL/TAX");
        registerRecord(vw_twr_Frm);

        vw_twr_Frm1 = new DataAccessProgramView(new NameInfo("vw_twr_Frm1", "TWR-FRM1"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        twr_Frm1_Tirf_Tax_Year = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        twr_Frm1_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        twr_Frm1_Tirf_Form_Type = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        twr_Frm1_Tirf_Form_Type.setDdmHeader("FORM");
        twr_Frm1_Tirf_Company_Cde = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        twr_Frm1_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        twr_Frm1_Tirf_Tax_Id_Type = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        twr_Frm1_Tirf_Tin = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        twr_Frm1_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        twr_Frm1_Tirf_Active_Ind = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        twr_Frm1_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        twr_Frm1_Tirf_Contract_Nbr = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        twr_Frm1_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        twr_Frm1_Tirf_Payee_Cde = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        twr_Frm1_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        twr_Frm1_Tirf_Ltr_Data = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Tirf_Ltr_Data", "TIRF-LTR-DATA", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_LTR_DATA");
        twr_Frm1_Tirf_Ltr_Data.setDdmHeader("LTR/DATA");
        twr_Frm1_Count_Casttirf_Ltr_Grp = vw_twr_Frm1.getRecord().newFieldInGroup("twr_Frm1_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");

        twr_Frm1_Tirf_Ltr_Grp = vw_twr_Frm1.getRecord().newGroupInGroup("twr_Frm1_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_Gross_Amt = twr_Frm1_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm1_Tirf_Ltr_Gross_Amt", "TIRF-LTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_GROSS_AMT", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_Gross_Amt.setDdmHeader("LETTER/GROSS/AMOUNT");
        twr_Frm1_Tirf_Ltr_Per = twr_Frm1_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm1_Tirf_Ltr_Per", "TIRF-LTR-PER", FieldType.PACKED_DECIMAL, 8, 5, new 
            DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_PER", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_Per.setDdmHeader("LTR/PCT");
        twr_Frm1_Tirf_Ltr_Fed_Tax_Wthld = twr_Frm1_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm1_Tirf_Ltr_Fed_Tax_Wthld", "TIRF-LTR-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_FED_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_Fed_Tax_Wthld.setDdmHeader("LTR/FED/TAX");
        twr_Frm1_Tirf_Ltr_State_Tax_Wthld = twr_Frm1_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm1_Tirf_Ltr_State_Tax_Wthld", "TIRF-LTR-STATE-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_State_Tax_Wthld.setDdmHeader("LTR/STATE/TAX");
        twr_Frm1_Tirf_Ltr_Loc_Tax_Wthld = twr_Frm1_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm1_Tirf_Ltr_Loc_Tax_Wthld", "TIRF-LTR-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm1_Tirf_Ltr_Loc_Tax_Wthld.setDdmHeader("LTR/LOCAL/TAX");
        registerRecord(vw_twr_Frm1);

        vw_twr_Frm2 = new DataAccessProgramView(new NameInfo("vw_twr_Frm2", "TWR-FRM2"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE", DdmPeriodicGroups.getInstance().getGroups("TWRFRM_FORM_FILE"));
        twr_Frm2_Tirf_Tax_Year = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        twr_Frm2_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        twr_Frm2_Tirf_Form_Type = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        twr_Frm2_Tirf_Form_Type.setDdmHeader("FORM");
        twr_Frm2_Tirf_Company_Cde = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        twr_Frm2_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        twr_Frm2_Tirf_Tax_Id_Type = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        twr_Frm2_Tirf_Tin = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TIRF_TIN");
        twr_Frm2_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        twr_Frm2_Tirf_Active_Ind = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        twr_Frm2_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        twr_Frm2_Tirf_Contract_Nbr = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        twr_Frm2_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        twr_Frm2_Tirf_Payee_Cde = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        twr_Frm2_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        twr_Frm2_Tirf_Ltr_Data = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Tirf_Ltr_Data", "TIRF-LTR-DATA", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_LTR_DATA");
        twr_Frm2_Tirf_Ltr_Data.setDdmHeader("LTR/DATA");
        twr_Frm2_Count_Casttirf_Ltr_Grp = vw_twr_Frm2.getRecord().newFieldInGroup("twr_Frm2_Count_Casttirf_Ltr_Grp", "C*TIRF-LTR-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");

        twr_Frm2_Tirf_Ltr_Grp = vw_twr_Frm2.getRecord().newGroupInGroup("twr_Frm2_Tirf_Ltr_Grp", "TIRF-LTR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_Gross_Amt = twr_Frm2_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm2_Tirf_Ltr_Gross_Amt", "TIRF-LTR-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_GROSS_AMT", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_Gross_Amt.setDdmHeader("LETTER/GROSS/AMOUNT");
        twr_Frm2_Tirf_Ltr_Per = twr_Frm2_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm2_Tirf_Ltr_Per", "TIRF-LTR-PER", FieldType.PACKED_DECIMAL, 8, 5, new 
            DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_PER", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_Per.setDdmHeader("LTR/PCT");
        twr_Frm2_Tirf_Ltr_Fed_Tax_Wthld = twr_Frm2_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm2_Tirf_Ltr_Fed_Tax_Wthld", "TIRF-LTR-FED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_FED_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_Fed_Tax_Wthld.setDdmHeader("LTR/FED/TAX");
        twr_Frm2_Tirf_Ltr_State_Tax_Wthld = twr_Frm2_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm2_Tirf_Ltr_State_Tax_Wthld", "TIRF-LTR-STATE-TAX-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_STATE_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_State_Tax_Wthld.setDdmHeader("LTR/STATE/TAX");
        twr_Frm2_Tirf_Ltr_Loc_Tax_Wthld = twr_Frm2_Tirf_Ltr_Grp.newFieldArrayInGroup("twr_Frm2_Tirf_Ltr_Loc_Tax_Wthld", "TIRF-LTR-LOC-TAX-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 12) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIRF_LTR_LOC_TAX_WTHLD", "TWRFRM_FORM_FILE_TIRF_LTR_GRP");
        twr_Frm2_Tirf_Ltr_Loc_Tax_Wthld.setDdmHeader("LTR/LOCAL/TAX");
        registerRecord(vw_twr_Frm2);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        registerRecord(vw_iaa_Cntrct);

        pnd_Hold_Tin = localVariables.newFieldInRecord("pnd_Hold_Tin", "#HOLD-TIN", FieldType.STRING, 10);
        pnd_Hold_Contract = localVariables.newFieldInRecord("pnd_Hold_Contract", "#HOLD-CONTRACT", FieldType.STRING, 10);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 30);

        pnd_Mid_Name__R_Field_1 = localVariables.newGroupInRecord("pnd_Mid_Name__R_Field_1", "REDEFINE", pnd_Mid_Name);
        pnd_Mid_Name_Pnd_Mid_Name1 = pnd_Mid_Name__R_Field_1.newFieldInGroup("pnd_Mid_Name_Pnd_Mid_Name1", "#MID-NAME1", FieldType.STRING, 1);
        pnd_Daten = localVariables.newFieldInRecord("pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Daten__R_Field_2 = localVariables.newGroupInRecord("pnd_Daten__R_Field_2", "REDEFINE", pnd_Daten);
        pnd_Daten_Pnd_Daten_Cc = pnd_Daten__R_Field_2.newFieldInGroup("pnd_Daten_Pnd_Daten_Cc", "#DATEN-CC", FieldType.STRING, 2);
        pnd_Daten_Pnd_Daten_Yy = pnd_Daten__R_Field_2.newFieldInGroup("pnd_Daten_Pnd_Daten_Yy", "#DATEN-YY", FieldType.STRING, 2);
        pnd_Daten_Pnd_Daten_Mm = pnd_Daten__R_Field_2.newFieldInGroup("pnd_Daten_Pnd_Daten_Mm", "#DATEN-MM", FieldType.STRING, 2);
        pnd_Daten_Pnd_Daten_Dd = pnd_Daten__R_Field_2.newFieldInGroup("pnd_Daten_Pnd_Daten_Dd", "#DATEN-DD", FieldType.STRING, 2);
        pnd_Tirf_Superde_1 = localVariables.newFieldInRecord("pnd_Tirf_Superde_1", "#TIRF-SUPERDE-1", FieldType.STRING, 38);

        pnd_Tirf_Superde_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Tirf_Superde_1__R_Field_3", "REDEFINE", pnd_Tirf_Superde_1);
        pnd_Tirf_Superde_1_Tirf_Tax_Year = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Tirf_Superde_1_Tirf_Tin = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_Tirf_Superde_1_Tirf_Form_Type = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Tirf_Superde_1_Tirf_Contract_Nbr = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Tirf_Superde_1_Tirf_Payee_Cde = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Tirf_Superde_1_Tirf_Key = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_Tirf_Superde_1_Tirf_Invrse_Create_Ts = pnd_Tirf_Superde_1__R_Field_3.newFieldInGroup("pnd_Tirf_Superde_1_Tirf_Invrse_Create_Ts", "TIRF-INVRSE-CREATE-TS", 
            FieldType.PACKED_DECIMAL, 13);
        pnd_Tirf2_Superde_1 = localVariables.newFieldInRecord("pnd_Tirf2_Superde_1", "#TIRF2-SUPERDE-1", FieldType.STRING, 38);

        pnd_Tirf2_Superde_1__R_Field_4 = localVariables.newGroupInRecord("pnd_Tirf2_Superde_1__R_Field_4", "REDEFINE", pnd_Tirf2_Superde_1);
        pnd_Tirf2_Superde_1_Tirf_Tax_Year = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Tirf2_Superde_1_Tirf_Tin = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10);
        pnd_Tirf2_Superde_1_Tirf_Form_Type = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Tirf2_Superde_1_Tirf_Contract_Nbr = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Tirf2_Superde_1_Tirf_Payee_Cde = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Tirf2_Superde_1_Tirf_Key = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5);
        pnd_Tirf2_Superde_1_Tirf_Invrse_Create_Ts = pnd_Tirf2_Superde_1__R_Field_4.newFieldInGroup("pnd_Tirf2_Superde_1_Tirf_Invrse_Create_Ts", "TIRF-INVRSE-CREATE-TS", 
            FieldType.PACKED_DECIMAL, 13);
        pnd_Actual_Reject = localVariables.newFieldInRecord("pnd_Actual_Reject", "#ACTUAL-REJECT", FieldType.STRING, 1);
        pnd_Pa_Select = localVariables.newFieldInRecord("pnd_Pa_Select", "#PA-SELECT", FieldType.BOOLEAN, 1);
        pnd_Any_Ltr = localVariables.newFieldInRecord("pnd_Any_Ltr", "#ANY-LTR", FieldType.STRING, 1);
        pnd_Super_Start = localVariables.newFieldInRecord("pnd_Super_Start", "#SUPER-START", FieldType.STRING, 8);
        pnd_Super2_Start = localVariables.newFieldInRecord("pnd_Super2_Start", "#SUPER2-START", FieldType.STRING, 15);
        pnd_Tirf_Superde_3 = localVariables.newFieldInRecord("pnd_Tirf_Superde_3", "#TIRF-SUPERDE-3", FieldType.STRING, 33);

        pnd_Tax_Tape_Totals = localVariables.newGroupInRecord("pnd_Tax_Tape_Totals", "#TAX-TAPE-TOTALS");
        pnd_Tax_Tape_Totals_Pnd_Tot_Ny_Gross = pnd_Tax_Tape_Totals.newFieldInGroup("pnd_Tax_Tape_Totals_Pnd_Tot_Ny_Gross", "#TOT-NY-GROSS", FieldType.NUMERIC, 
            14, 2);
        pnd_Tax_Tape_Totals_Pnd_Grand_1w = pnd_Tax_Tape_Totals.newFieldInGroup("pnd_Tax_Tape_Totals_Pnd_Grand_1w", "#GRAND-1W", FieldType.NUMERIC, 7);
        pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc = pnd_Tax_Tape_Totals.newFieldInGroup("pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc", "#TOT-1W-ACC", FieldType.NUMERIC, 
            7);
        pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej = pnd_Tax_Tape_Totals.newFieldInGroup("pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej", "#TOT-1W-REJ", FieldType.NUMERIC, 
            7);
        pnd_Tax_Tape_Totals_Pnd_Grand_1e = pnd_Tax_Tape_Totals.newFieldInGroup("pnd_Tax_Tape_Totals_Pnd_Grand_1e", "#GRAND-1E", FieldType.NUMERIC, 14, 
            2);
        pnd_Plan_Name = localVariables.newFieldInRecord("pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 64);

        pnd_Plan_Name__R_Field_5 = localVariables.newGroupInRecord("pnd_Plan_Name__R_Field_5", "REDEFINE", pnd_Plan_Name);
        pnd_Plan_Name_Pnd_Plan_Name1 = pnd_Plan_Name__R_Field_5.newFieldInGroup("pnd_Plan_Name_Pnd_Plan_Name1", "#PLAN-NAME1", FieldType.STRING, 32);
        pnd_Plan_Name_Pnd_Plan_Name2 = pnd_Plan_Name__R_Field_5.newFieldInGroup("pnd_Plan_Name_Pnd_Plan_Name2", "#PLAN-NAME2", FieldType.STRING, 32);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Pnd_State_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_State_Max", "#STATE-MAX", FieldType.STRING, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws__R_Field_6 = pnd_Ws_Pnd_Input_Parms.newGroupInGroup("pnd_Ws__R_Field_6", "REDEFINE", pnd_Ws_Pnd_Tax_Year);
        pnd_Ws_Pnd_Tax_Year_A = pnd_Ws__R_Field_6.newFieldInGroup("pnd_Ws_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_I1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J1", "#J1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K1", "#K1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_L1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_L1", "#L1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Index = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Num", "#NUM", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Out_Ny_Tin_Nohyph = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Out_Ny_Tin_Nohyph", "#OUT-NY-TIN-NOHYPH", FieldType.STRING, 11);

        pnd_Ws__R_Field_7 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_7", "REDEFINE", pnd_Ws_Pnd_Out_Ny_Tin_Nohyph);
        pnd_Ws_First_3 = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_First_3", "FIRST-3", FieldType.STRING, 3);
        pnd_Ws_First_H = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_First_H", "FIRST-H", FieldType.STRING, 1);
        pnd_Ws_Sec_2 = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_Sec_2", "SEC-2", FieldType.STRING, 2);
        pnd_Ws_Sec_H = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_Sec_H", "SEC-H", FieldType.STRING, 1);
        pnd_Ws_Last_4 = pnd_Ws__R_Field_7.newFieldInGroup("pnd_Ws_Last_4", "LAST-4", FieldType.STRING, 4);
        pnd_Ws_Pnd_Hold_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hold_Tax_Year", "#HOLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Ws__R_Field_8 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_8", "REDEFINE", pnd_Ws_Pnd_Hold_Tax_Year);
        pnd_Ws_Pnd_Hold_Cent = pnd_Ws__R_Field_8.newFieldInGroup("pnd_Ws_Pnd_Hold_Cent", "#HOLD-CENT", FieldType.STRING, 2);
        pnd_Ws_Pnd_Hold_Year = pnd_Ws__R_Field_8.newFieldInGroup("pnd_Ws_Pnd_Hold_Year", "#HOLD-YEAR", FieldType.STRING, 2);
        pnd_Ws_Pnd_St_Occ = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Occ", "#ST-OCC", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Valid_State = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Valid_State", "#VALID-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Accepted_Form_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accepted_Form_Cnt", "#ACCEPTED-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 59);
        pnd_Ws_Pnd_Disp_Rec = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Rec", "#DISP-REC", FieldType.STRING, 5);
        pnd_Ws_Pnd_S8 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S8", "#S8", FieldType.STRING, 8);
        pnd_Ws_Pnd_Empty_Accept_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Accept_Cnt", "#EMPTY-ACCEPT-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Empty_Reject_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Reject_Cnt", "#EMPTY-REJECT-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Empty_Tot_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Cnt", "#EMPTY-TOT-CNT", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Empty_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Frm2", "#EMPTY-FRM2", FieldType.NUMERIC, 5);
        pnd_Ws_Pnd_Empty_Tot_Frm2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Empty_Tot_Frm2", "#EMPTY-TOT-FRM2", FieldType.NUMERIC, 5);

        pnd_State_Table = localVariables.newGroupInRecord("pnd_State_Table", "#STATE-TABLE");
        pnd_State_Table_Tircntl_State_Old_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Old_Code", "TIRCNTL-STATE-OLD-CODE", FieldType.STRING, 
            3);
        pnd_State_Table_Tircntl_State_Alpha_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_Tircntl_State_Full_Name = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Full_Name", "TIRCNTL-STATE-FULL-NAME", 
            FieldType.STRING, 19);
        pnd_State_Table_Tircntl_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_State_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Ind", "TIRCNTL-STATE-IND", FieldType.STRING, 
            1);
        pnd_State_Table_Tircntl_Media_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Media_Code", "TIRCNTL-MEDIA-CODE", FieldType.STRING, 
            1);
        pnd_S1_End = localVariables.newFieldInRecord("pnd_S1_End", "#S1-END", FieldType.STRING, 32);

        pnd_Hold_Fields = localVariables.newGroupInRecord("pnd_Hold_Fields", "#HOLD-FIELDS");
        pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind", "#HOLD-COMB-FED-IND", FieldType.STRING, 
            1);
        pnd_Hold_Fields_Pnd_Hold_Rej_Ind = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Rej_Ind", "#HOLD-REJ-IND", FieldType.STRING, 1);
        pnd_Hold_Fields_Pnd_Hold_Cuny_Cont = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Cuny_Cont", "#HOLD-CUNY-CONT", FieldType.STRING, 
            10);
        pnd_Hold_Fields_Pnd_Hold_Suny_Cont = pnd_Hold_Fields.newFieldInGroup("pnd_Hold_Fields_Pnd_Hold_Suny_Cont", "#HOLD-SUNY-CONT", FieldType.STRING, 
            10);
        pnd_Plan_Table_Key = localVariables.newFieldInRecord("pnd_Plan_Table_Key", "#PLAN-TABLE-KEY", FieldType.STRING, 26);

        pnd_Plan_Table_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Plan_Table_Key__R_Field_9", "REDEFINE", pnd_Plan_Table_Key);
        pnd_Plan_Table_Key_Pnd_Pt_A_I_Ind = pnd_Plan_Table_Key__R_Field_9.newFieldInGroup("pnd_Plan_Table_Key_Pnd_Pt_A_I_Ind", "#PT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Plan_Table_Key_Pnd_Pt_Table_Id = pnd_Plan_Table_Key__R_Field_9.newFieldInGroup("pnd_Plan_Table_Key_Pnd_Pt_Table_Id", "#PT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Plan_Table_Key_Pnd_Pt_Plan_Id = pnd_Plan_Table_Key__R_Field_9.newFieldInGroup("pnd_Plan_Table_Key_Pnd_Pt_Plan_Id", "#PT-PLAN-ID", FieldType.STRING, 
            20);
        pnd_Header_Record1 = localVariables.newFieldInRecord("pnd_Header_Record1", "#HEADER-RECORD1", FieldType.STRING, 65);
        pnd_Header_Record2 = localVariables.newFieldInRecord("pnd_Header_Record2", "#HEADER-RECORD2", FieldType.STRING, 70);
        pnd_Sub_Head = localVariables.newFieldInRecord("pnd_Sub_Head", "#SUB-HEAD", FieldType.STRING, 30);
        pnd_Sub_Head2 = localVariables.newFieldInRecord("pnd_Sub_Head2", "#SUB-HEAD2", FieldType.STRING, 15);
        pnd_Extract_Reject_Ind = localVariables.newFieldInRecord("pnd_Extract_Reject_Ind", "#EXTRACT-REJECT-IND", FieldType.STRING, 1);
        pnd_Extract_Name = localVariables.newFieldInRecord("pnd_Extract_Name", "#EXTRACT-NAME", FieldType.STRING, 64);
        pnd_Extract_Gross_Amt_Header = localVariables.newFieldInRecord("pnd_Extract_Gross_Amt_Header", "#EXTRACT-GROSS-AMT-HEADER", FieldType.NUMERIC, 
            14, 2);
        pnd_Extract_Fed_Tax_Header_N = localVariables.newFieldInRecord("pnd_Extract_Fed_Tax_Header_N", "#EXTRACT-FED-TAX-HEADER-N", FieldType.NUMERIC, 
            11, 2);
        pnd_Extract_Stat_Tax_Header_N = localVariables.newFieldInRecord("pnd_Extract_Stat_Tax_Header_N", "#EXTRACT-STAT-TAX-HEADER-N", FieldType.NUMERIC, 
            11, 2);
        pnd_Extract_Local_Tax_Header_N = localVariables.newFieldInRecord("pnd_Extract_Local_Tax_Header_N", "#EXTRACT-LOCAL-TAX-HEADER-N", FieldType.NUMERIC, 
            11, 2);
        pnd_Tin_Gross_Acc = localVariables.newFieldInRecord("pnd_Tin_Gross_Acc", "#TIN-GROSS-ACC", FieldType.NUMERIC, 14, 2);
        pnd_Tin_Fed_Tax_Acc = localVariables.newFieldInRecord("pnd_Tin_Fed_Tax_Acc", "#TIN-FED-TAX-ACC", FieldType.NUMERIC, 14, 2);
        pnd_Tin_State_Tax_Acc = localVariables.newFieldInRecord("pnd_Tin_State_Tax_Acc", "#TIN-STATE-TAX-ACC", FieldType.NUMERIC, 14, 2);
        pnd_Tin_Local_Tax_Acc = localVariables.newFieldInRecord("pnd_Tin_Local_Tax_Acc", "#TIN-LOCAL-TAX-ACC", FieldType.NUMERIC, 14, 2);
        pnd_Tin_Gross_Rej = localVariables.newFieldInRecord("pnd_Tin_Gross_Rej", "#TIN-GROSS-REJ", FieldType.NUMERIC, 14, 2);
        pnd_Tin_Fed_Tax_Rej = localVariables.newFieldInRecord("pnd_Tin_Fed_Tax_Rej", "#TIN-FED-TAX-REJ", FieldType.NUMERIC, 14, 2);
        pnd_Tin_State_Tax_Rej = localVariables.newFieldInRecord("pnd_Tin_State_Tax_Rej", "#TIN-STATE-TAX-REJ", FieldType.NUMERIC, 14, 2);
        pnd_Tin_Local_Tax_Rej = localVariables.newFieldInRecord("pnd_Tin_Local_Tax_Rej", "#TIN-LOCAL-TAX-REJ", FieldType.NUMERIC, 14, 2);
        pnd_Grand_Gross = localVariables.newFieldInRecord("pnd_Grand_Gross", "#GRAND-GROSS", FieldType.NUMERIC, 14, 2);
        pnd_Grand_Fed_Tax = localVariables.newFieldInRecord("pnd_Grand_Fed_Tax", "#GRAND-FED-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Grand_State_Tax = localVariables.newFieldInRecord("pnd_Grand_State_Tax", "#GRAND-STATE-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Grand_Local_Tax = localVariables.newFieldInRecord("pnd_Grand_Local_Tax", "#GRAND-LOCAL-TAX", FieldType.NUMERIC, 14, 2);
        pnd_Cuny_Part_Cnt = localVariables.newFieldInRecord("pnd_Cuny_Part_Cnt", "#CUNY-PART-CNT", FieldType.NUMERIC, 7);
        pnd_Suny_Part_Cnt = localVariables.newFieldInRecord("pnd_Suny_Part_Cnt", "#SUNY-PART-CNT", FieldType.NUMERIC, 7);
        pnd_Total_Forms = localVariables.newFieldInRecord("pnd_Total_Forms", "#TOTAL-FORMS", FieldType.NUMERIC, 7);
        pnd_Accepted_Forms = localVariables.newFieldInRecord("pnd_Accepted_Forms", "#ACCEPTED-FORMS", FieldType.NUMERIC, 7);
        pnd_Rejected_Forms = localVariables.newFieldInRecord("pnd_Rejected_Forms", "#REJECTED-FORMS", FieldType.NUMERIC, 7);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.NUMERIC, 7);
        pnd_Employee_Desc = localVariables.newFieldInRecord("pnd_Employee_Desc", "#EMPLOYEE-DESC", FieldType.STRING, 13);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 9);
        pnd_Et_Limit = localVariables.newFieldInRecord("pnd_Et_Limit", "#ET-LIMIT", FieldType.NUMERIC, 4);
        pnd_Cuny_Suny_Plan = localVariables.newFieldArrayInRecord("pnd_Cuny_Suny_Plan", "#CUNY-SUNY-PLAN", FieldType.STRING, 6, new DbsArrayController(1, 
            30));
        pnd_Client_Ids = localVariables.newFieldArrayInRecord("pnd_Client_Ids", "#CLIENT-IDS", FieldType.STRING, 6, new DbsArrayController(1, 26));
        pnd_Plan_Type = localVariables.newFieldInRecord("pnd_Plan_Type", "#PLAN-TYPE", FieldType.STRING, 1);
        pnd_Report_Type = localVariables.newFieldInRecord("pnd_Report_Type", "#REPORT-TYPE", FieldType.STRING, 1);
        pnd_Gross_Disp = localVariables.newFieldInRecord("pnd_Gross_Disp", "#GROSS-DISP", FieldType.STRING, 20);
        pnd_Fill = localVariables.newFieldInRecord("pnd_Fill", "#FILL", FieldType.STRING, 238);
        pnd_Print_Suny = localVariables.newFieldInRecord("pnd_Print_Suny", "#PRINT-SUNY", FieldType.STRING, 1);
        pnd_Print_Cuny = localVariables.newFieldInRecord("pnd_Print_Cuny", "#PRINT-CUNY", FieldType.STRING, 1);
        pnd_Print_Both = localVariables.newFieldInRecord("pnd_Print_Both", "#PRINT-BOTH", FieldType.STRING, 1);

        pnd_Client_Table = localVariables.newGroupInRecord("pnd_Client_Table", "#CLIENT-TABLE");
        pnd_Client_Table_Pnd_Ct_Data = pnd_Client_Table.newFieldArrayInGroup("pnd_Client_Table_Pnd_Ct_Data", "#CT-DATA", FieldType.STRING, 21, new DbsArrayController(1, 
            200));

        pnd_Client_Table__R_Field_10 = localVariables.newGroupInRecord("pnd_Client_Table__R_Field_10", "REDEFINE", pnd_Client_Table);

        pnd_Client_Table_Pnd_Ct_Keys = pnd_Client_Table__R_Field_10.newGroupArrayInGroup("pnd_Client_Table_Pnd_Ct_Keys", "#CT-KEYS", new DbsArrayController(1, 
            200));
        pnd_Client_Table_Pnd_Ct_Key = pnd_Client_Table_Pnd_Ct_Keys.newFieldInGroup("pnd_Client_Table_Pnd_Ct_Key", "#CT-KEY", FieldType.STRING, 15);

        pnd_Client_Table__R_Field_11 = pnd_Client_Table_Pnd_Ct_Keys.newGroupInGroup("pnd_Client_Table__R_Field_11", "REDEFINE", pnd_Client_Table_Pnd_Ct_Key);
        pnd_Client_Table_Pnd_Ct_Bad_Cl = pnd_Client_Table__R_Field_11.newFieldInGroup("pnd_Client_Table_Pnd_Ct_Bad_Cl", "#CT-BAD-CL", FieldType.STRING, 
            6);
        pnd_Client_Table_Pnd_Ct_Tin = pnd_Client_Table__R_Field_11.newFieldInGroup("pnd_Client_Table_Pnd_Ct_Tin", "#CT-TIN", FieldType.STRING, 9);
        pnd_Client_Table_Pnd_Ct_Fix_Cl = pnd_Client_Table_Pnd_Ct_Keys.newFieldInGroup("pnd_Client_Table_Pnd_Ct_Fix_Cl", "#CT-FIX-CL", FieldType.STRING, 
            6);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 15);

        pnd_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Key__R_Field_12", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Key_Cl = pnd_Key__R_Field_12.newFieldInGroup("pnd_Key_Pnd_Key_Cl", "#KEY-CL", FieldType.STRING, 6);
        pnd_Key_Pnd_Key_Tin = pnd_Key__R_Field_12.newFieldInGroup("pnd_Key_Pnd_Key_Tin", "#KEY-TIN", FieldType.STRING, 9);
        pnd_Num_X = localVariables.newFieldInRecord("pnd_Num_X", "#NUM-X", FieldType.NUMERIC, 3);
        pnd_Idx_X = localVariables.newFieldInRecord("pnd_Idx_X", "#IDX-X", FieldType.NUMERIC, 3);
        pnd_No_Suny = localVariables.newFieldInRecord("pnd_No_Suny", "#NO-SUNY", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form_U.reset();
        vw_twr_Frm.reset();
        vw_twr_Frm1.reset();
        vw_twr_Frm2.reset();
        vw_iaa_Cntrct.reset();

        ldaTwrl9715.initializeValues();
        ldaTwrl3620.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Pnd_State_Max.setInitialValue("57");
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Header_Record1.setInitialValue("TIN|Name|Contract|Contract Type|Orig Contract|Orig Contract Type|");
        pnd_Header_Record2.setInitialValue("Plan|Plan Name|Employer Type|Total Income|Plan Income|Percent");
        pnd_Et_Count.setInitialValue(0);
        pnd_Et_Limit.setInitialValue(100);
        pnd_Cuny_Suny_Plan.getValue(1).setInitialValue("151166");
        pnd_Cuny_Suny_Plan.getValue(2).setInitialValue("151212");
        pnd_Cuny_Suny_Plan.getValue(3).setInitialValue("151168");
        pnd_Cuny_Suny_Plan.getValue(4).setInitialValue("151211");
        pnd_Cuny_Suny_Plan.getValue(5).setInitialValue("151167");
        pnd_Cuny_Suny_Plan.getValue(6).setInitialValue("151169");
        pnd_Cuny_Suny_Plan.getValue(7).setInitialValue("151170");
        pnd_Cuny_Suny_Plan.getValue(8).setInitialValue("151596");
        pnd_Cuny_Suny_Plan.getValue(9).setInitialValue("151597");
        pnd_Cuny_Suny_Plan.getValue(10).setInitialValue("151598");
        pnd_Cuny_Suny_Plan.getValue(11).setInitialValue("151599");
        pnd_Cuny_Suny_Plan.getValue(12).setInitialValue("151600");
        pnd_Cuny_Suny_Plan.getValue(13).setInitialValue("151601");
        pnd_Cuny_Suny_Plan.getValue(14).setInitialValue("151602");
        pnd_Cuny_Suny_Plan.getValue(15).setInitialValue("151603");
        pnd_Cuny_Suny_Plan.getValue(16).setInitialValue("151172");
        pnd_Client_Ids.getValue(1).setInitialValue("009836");
        pnd_Client_Ids.getValue(2).setInitialValue("001478");
        pnd_Client_Ids.getValue(3).setInitialValue("002493");
        pnd_Client_Ids.getValue(4).setInitialValue("002502");
        pnd_Client_Ids.getValue(5).setInitialValue("002543");
        pnd_Client_Ids.getValue(6).setInitialValue("002595");
        pnd_Client_Ids.getValue(7).setInitialValue("003603");
        pnd_Client_Ids.getValue(8).setInitialValue("003879");
        pnd_Client_Ids.getValue(9).setInitialValue("005948");
        pnd_Client_Ids.getValue(10).setInitialValue("007270");
        pnd_Client_Ids.getValue(11).setInitialValue("008353");
        pnd_Client_Ids.getValue(12).setInitialValue("009845");
        pnd_Client_Ids.getValue(13).setInitialValue("009847");
        pnd_Client_Ids.getValue(14).setInitialValue("009855");
        pnd_Client_Ids.getValue(15).setInitialValue("009987");
        pnd_Client_Ids.getValue(16).setInitialValue("010157");
        pnd_Client_Ids.getValue(17).setInitialValue("010229");
        pnd_Client_Ids.getValue(18).setInitialValue("010250");
        pnd_Client_Ids.getValue(19).setInitialValue("011112");
        pnd_Client_Ids.getValue(20).setInitialValue("011113");
        pnd_Client_Ids.getValue(21).setInitialValue("011119");
        pnd_Client_Ids.getValue(22).setInitialValue("011133");
        pnd_Client_Ids.getValue(23).setInitialValue("011351");
        pnd_Client_Ids.getValue(24).setInitialValue("011402");
        pnd_Client_Ids.getValue(25).setInitialValue("013469");
        pnd_Client_Ids.getValue(26).setInitialValue("054303");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3619() throws Exception
    {
        super("Twrp3619");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ZP=ON
        //*  ZP=ON
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133;//Natural: FORMAT ( 03 ) PS = 58 LS = 133;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 60T 'DTF File Report - Accepted Records' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR-A / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 60T 'DTF File Report - Rejected Records' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR-A / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 60T 'DTF File Report - Summary Report' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR-A / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 60T 'CUNY Contracts' 120T 'REPORT: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR-A / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 60T 'SUNY Contracts' 120T 'REPORT: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR-A / //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-CLIENT-TABLE
        sub_Load_Client_Table();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
        setValueToSubstring(pnd_Ws_Pnd_Tax_Year,pnd_Super_Start,1,4);                                                                                                     //Natural: MOVE #WS.#TAX-YEAR TO SUBSTR ( #SUPER-START,1,4 )
        setValueToSubstring("A",pnd_Super_Start,5,1);                                                                                                                     //Natural: MOVE 'A' TO SUBSTR ( #SUPER-START,5,1 )
        setValueToSubstring("10",pnd_Super_Start,6,2);                                                                                                                    //Natural: MOVE '10' TO SUBSTR ( #SUPER-START,6,2 )
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Super_Start,8,1);                                                                                                 //Natural: MOVE LOW-VALUES TO SUBSTR ( #SUPER-START,8,1 )
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outa_Ny_Blank().setValue(DbsUtil.compress(pnd_Header_Record1, pnd_Header_Record2));                                          //Natural: COMPRESS #HEADER-RECORD1 #HEADER-RECORD2 INTO #OUTA-NY-BLANK
        getWorkFiles().write(1, false, ldaTwrl3620.getPnd_Out_Ny_Record());                                                                                               //Natural: WRITE WORK FILE 01 #OUT-NY-RECORD
        ldaTwrl9715.getVw_form_R().startDatabaseRead                                                                                                                      //Natural: READ FORM-R BY TIRF-SUPERDE-3 = #SUPER-START
        (
        "RD_1",
        new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Super_Start, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
        );
        RD_1:
        while (condition(ldaTwrl9715.getVw_form_R().readNextRow("RD_1")))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            //*  IF #READ-CNT = 1
            //*    #HOLD-TIN := FORM-R.TIRF-TIN
            //*  END-IF
            //*  ONLY SUNY
            //*  ONLY SUNY
            if (condition(ldaTwrl9715.getForm_R_Tirf_Form_Type().notEquals(10) || ldaTwrl9715.getForm_R_Tirf_Active_Ind().notEquals("A")))                                //Natural: IF FORM-R.TIRF-FORM-TYPE NE 10 OR FORM-R.TIRF-ACTIVE-IND NE 'A'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9715.getForm_R_Tirf_Tax_Year().greater(pnd_Ws_Pnd_Tax_Year_A)))                                                                          //Natural: IF FORM-R.TIRF-TAX-YEAR > #WS.#TAX-YEAR-A
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*                                       PROCESS HEADER RECORD / TIN
            if (condition(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr().equals(" ")))                                                                                         //Natural: IF FORM-R.TIRF-CONTRACT-NBR = ' '
            {
                pnd_Total_Forms.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-FORMS
                pnd_Print_Suny.reset();                                                                                                                                   //Natural: RESET #PRINT-SUNY #PRINT-CUNY #PRINT-BOTH
                pnd_Print_Cuny.reset();
                pnd_Print_Both.reset();
                                                                                                                                                                          //Natural: PERFORM CUNY-SUNY-CHECK
                sub_Cuny_Suny_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaTwrl9715.getForm_R_Tirf_St_Reject_Ind().equals("Y")))                                                                                    //Natural: IF FORM-R.TIRF-ST-REJECT-IND = 'Y'
                {
                    //*  DASRAH
                                                                                                                                                                          //Natural: PERFORM FORM-LOOKUP-TIRF-LTR
                    sub_Form_Lookup_Tirf_Ltr();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DASRAH
                    if (condition(pnd_Actual_Reject.equals("Y")))                                                                                                         //Natural: IF #ACTUAL-REJECT = 'Y'
                    {
                        pnd_Rejected_Forms.nadd(1);                                                                                                                       //Natural: ADD 1 TO #REJECTED-FORMS
                        if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                              //Natural: IF FORM-R.TIRF-EMPTY-FORM
                        {
                            pnd_Ws_Pnd_Empty_Reject_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #EMPTY-REJECT-CNT
                            //*        WRITE 'EMPTY REJ'  FORM-R.TIRF-TIN
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Accepted_Forms.nadd(1);                                                                                                                       //Natural: ADD 1 TO #ACCEPTED-FORMS
                        if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                              //Natural: IF FORM-R.TIRF-EMPTY-FORM
                        {
                            pnd_Ws_Pnd_Empty_Accept_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #EMPTY-ACCEPT-CNT
                            //*        WRITE 'EMPTY ACC'  #HOLD-TIN
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accepted_Forms.nadd(1);                                                                                                                           //Natural: ADD 1 TO #ACCEPTED-FORMS
                    if (condition(ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean()))                                                                                  //Natural: IF FORM-R.TIRF-EMPTY-FORM
                    {
                        pnd_Ws_Pnd_Empty_Accept_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #EMPTY-ACCEPT-CNT
                        getReports().write(0, "EMPTY ACC",pnd_Hold_Tin);                                                                                                  //Natural: WRITE 'EMPTY ACC' #HOLD-TIN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  DASRAH
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DASRAH
                }                                                                                                                                                         //Natural: END-IF
                pnd_Extract_Reject_Ind.setValue(ldaTwrl9715.getForm_R_Tirf_St_Reject_Ind());                                                                              //Natural: ASSIGN #EXTRACT-REJECT-IND := FORM-R.TIRF-ST-REJECT-IND
                //*  DASRAH
                //*  DASRAH
                if (condition(pnd_Extract_Reject_Ind.equals("Y")))                                                                                                        //Natural: IF #EXTRACT-REJECT-IND = 'Y'
                {
                    pnd_Extract_Reject_Ind.setValue(pnd_Actual_Reject);                                                                                                   //Natural: ASSIGN #EXTRACT-REJECT-IND := #ACTUAL-REJECT
                    //*  DASRAH
                }                                                                                                                                                         //Natural: END-IF
                pnd_Extract_Name.setValue(ldaTwrl9715.getForm_R_Tirf_Participant_Name());                                                                                 //Natural: ASSIGN #EXTRACT-NAME := FORM-R.TIRF-PARTICIPANT-NAME
                pnd_Extract_Gross_Amt_Header.setValue(ldaTwrl9715.getForm_R_Tirf_Gross_Amt());                                                                            //Natural: ASSIGN #EXTRACT-GROSS-AMT-HEADER := FORM-R.TIRF-GROSS-AMT
                pnd_Extract_Fed_Tax_Header_N.setValue(ldaTwrl9715.getForm_R_Tirf_Fed_Tax_Wthld());                                                                        //Natural: ASSIGN #EXTRACT-FED-TAX-HEADER-N := FORM-R.TIRF-FED-TAX-WTHLD
                pnd_Extract_Stat_Tax_Header_N.setValue(ldaTwrl9715.getForm_R_Tirf_Summ_State_Tax_Wthld());                                                                //Natural: ASSIGN #EXTRACT-STAT-TAX-HEADER-N := FORM-R.TIRF-SUMM-STATE-TAX-WTHLD
                pnd_Extract_Local_Tax_Header_N.setValue(ldaTwrl9715.getForm_R_Tirf_Summ_Local_Tax_Wthld());                                                               //Natural: ASSIGN #EXTRACT-LOCAL-TAX-HEADER-N := FORM-R.TIRF-SUMM-LOCAL-TAX-WTHLD
                if (condition(! (ldaTwrl9715.getForm_R_Tirf_Empty_Form().getBoolean())))                                                                                  //Natural: IF NOT FORM-R.TIRF-EMPTY-FORM
                {
                    pnd_Sub_Head2.setValue(DbsUtil.compress("TIN ", ldaTwrl9715.getForm_R_Tirf_Tin(), "/"));                                                              //Natural: COMPRESS 'TIN ' FORM-R.TIRF-TIN '/' INTO #SUB-HEAD2
                    if (condition(pnd_Extract_Reject_Ind.notEquals("Y")))                                                                                                 //Natural: IF #EXTRACT-REJECT-IND NE 'Y'
                    {
                        getReports().write(2, NEWLINE," ");                                                                                                               //Natural: WRITE ( 2 ) / ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, NEWLINE,NEWLINE,pnd_Sub_Head2,pnd_Extract_Name, new AlphanumericLength (28),"Total Income:",pnd_Extract_Gross_Amt_Header,   //Natural: WRITE ( 2 ) // #SUB-HEAD2 #EXTRACT-NAME ( AL = 28 ) 'Total Income:' #EXTRACT-GROSS-AMT-HEADER 09X
                            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(9));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*          #EXTRACT-FED-TAX-HEADER-N
                        //*          #EXTRACT-STAT-TAX-HEADER-N
                        //*          #EXTRACT-LOCAL-TAX-HEADER-N
                        getReports().write(2, NEWLINE,NEWLINE," ");                                                                                                       //Natural: WRITE ( 2 ) //' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Tin_Gross_Acc.nadd(pnd_Extract_Gross_Amt_Header);                                                                                             //Natural: ADD #EXTRACT-GROSS-AMT-HEADER TO #TIN-GROSS-ACC
                        pnd_Tin_Fed_Tax_Acc.nadd(pnd_Extract_Fed_Tax_Header_N);                                                                                           //Natural: ADD #EXTRACT-FED-TAX-HEADER-N TO #TIN-FED-TAX-ACC
                        pnd_Tin_State_Tax_Acc.nadd(pnd_Extract_Stat_Tax_Header_N);                                                                                        //Natural: ADD #EXTRACT-STAT-TAX-HEADER-N TO #TIN-STATE-TAX-ACC
                        pnd_Tin_Local_Tax_Acc.nadd(pnd_Extract_Local_Tax_Header_N);                                                                                       //Natural: ADD #EXTRACT-LOCAL-TAX-HEADER-N TO #TIN-LOCAL-TAX-ACC
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(3, NEWLINE," ");                                                                                                               //Natural: WRITE ( 3 ) / ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(3, NEWLINE,NEWLINE,pnd_Sub_Head2,pnd_Extract_Name, new AlphanumericLength (28),"Total Income:",pnd_Extract_Gross_Amt_Header,   //Natural: WRITE ( 3 ) // #SUB-HEAD2 #EXTRACT-NAME ( AL = 28 ) 'Total Income:' #EXTRACT-GROSS-AMT-HEADER 9X
                            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(9));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*          #EXTRACT-FED-TAX-HEADER-N
                        //*          #EXTRACT-STAT-TAX-HEADER-N
                        //*          #EXTRACT-LOCAL-TAX-HEADER-N
                        getReports().write(3, NEWLINE,NEWLINE," ");                                                                                                       //Natural: WRITE ( 3 ) // ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Tin_Gross_Rej.nadd(pnd_Extract_Gross_Amt_Header);                                                                                             //Natural: ADD #EXTRACT-GROSS-AMT-HEADER TO #TIN-GROSS-REJ
                        pnd_Tin_Fed_Tax_Rej.nadd(pnd_Extract_Fed_Tax_Header_N);                                                                                           //Natural: ADD #EXTRACT-FED-TAX-HEADER-N TO #TIN-FED-TAX-REJ
                        pnd_Tin_State_Tax_Rej.nadd(pnd_Extract_Stat_Tax_Header_N);                                                                                        //Natural: ADD #EXTRACT-STAT-TAX-HEADER-N TO #TIN-STATE-TAX-REJ
                        pnd_Tin_Local_Tax_Rej.nadd(pnd_Extract_Local_Tax_Header_N);                                                                                       //Natural: ADD #EXTRACT-LOCAL-TAX-HEADER-N TO #TIN-LOCAL-TAX-REJ
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Grand_Gross.nadd(pnd_Extract_Gross_Amt_Header);                                                                                                   //Natural: ADD #EXTRACT-GROSS-AMT-HEADER TO #GRAND-GROSS
                    pnd_Grand_Fed_Tax.nadd(pnd_Extract_Fed_Tax_Header_N);                                                                                                 //Natural: ADD #EXTRACT-FED-TAX-HEADER-N TO #GRAND-FED-TAX
                    pnd_Grand_State_Tax.nadd(pnd_Extract_Stat_Tax_Header_N);                                                                                              //Natural: ADD #EXTRACT-STAT-TAX-HEADER-N TO #GRAND-STATE-TAX
                    pnd_Grand_Local_Tax.nadd(pnd_Extract_Local_Tax_Header_N);                                                                                             //Natural: ADD #EXTRACT-LOCAL-TAX-HEADER-N TO #GRAND-LOCAL-TAX
                }                                                                                                                                                         //Natural: END-IF
                //*  PROCESS DETAIL RECORD/CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-OUT-TO-TAPE-NEW
                sub_Write_Out_To_Tape_New();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Tin.setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                                  //Natural: ASSIGN #HOLD-TIN := FORM-R.TIRF-TIN
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-IRS-STATUS
            sub_Update_Irs_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Report_Type.reset();                                                                                                                                      //Natural: RESET #REPORT-TYPE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Gross_Disp.setValueEdited(pnd_Tin_Gross_Acc,new ReportEditMask("-ZZZ,ZZZ,ZZZ,ZZ9.99"));                                                                       //Natural: MOVE EDITED #TIN-GROSS-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #GROSS-DISP
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outa_Ny_Blank().setValue(DbsUtil.compress("TOTAL DETAIL RECORDS:    ", pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc));                 //Natural: COMPRESS 'TOTAL DETAIL RECORDS:    ' #TOT-1W-ACC INTO #OUTA-NY-BLANK
        getWorkFiles().write(1, false, ldaTwrl3620.getPnd_Out_Ny_Record());                                                                                               //Natural: WRITE WORK FILE 1 #OUT-NY-RECORD
        if (condition(pnd_Read_Cnt.equals(getZero())))                                                                                                                    //Natural: IF #READ-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"No Forms were selected for the",new TabSetting(77),"***",NEWLINE,"***",new               //Natural: WRITE ( 1 ) '***' 25T 'No Forms were selected for the' 77T '***' / '***' 25T 'NYS FDT extract file' 77T '***'
                TabSetting(25),"NYS FDT extract file",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE TOTALS FOR 3 REPORTS: ACCEPTED, REJECTED, SUMMARY
        getReports().write(2, NEWLINE,NEWLINE,"Totals:",NEWLINE,NEWLINE,new ColumnSpacing(3),"TOTAL DETAIL RECORDS:    ",new ColumnSpacing(9),pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc,NEWLINE,new  //Natural: WRITE ( 2 ) // 'Totals:' // 3X 'TOTAL DETAIL RECORDS:    ' 9X #TOT-1W-ACC / 3X 'TOTAL EMPTY FORMS:       ' 9X #EMPTY-ACCEPT-CNT / 3X 'TOTAL GROSS:          ' #TIN-GROSS-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL FED TAX:        ' #TIN-FED-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL STATE TAX:      ' #TIN-STATE-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL LOCAL TAX:      ' #TIN-LOCAL-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) /
            ColumnSpacing(3),"TOTAL EMPTY FORMS:       ",new ColumnSpacing(9),pnd_Ws_Pnd_Empty_Accept_Cnt,NEWLINE,new ColumnSpacing(3),"TOTAL GROSS:          ",pnd_Tin_Gross_Acc, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL FED TAX:        ",pnd_Tin_Fed_Tax_Acc, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(3),"TOTAL STATE TAX:      ",pnd_Tin_State_Tax_Acc, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL LOCAL TAX:      ",pnd_Tin_Local_Tax_Acc, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,"Totals:",NEWLINE,NEWLINE,new ColumnSpacing(3),"TOTAL DETAIL RECORDS:    ",new ColumnSpacing(9),pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej,NEWLINE,new  //Natural: WRITE ( 3 ) // 'Totals:' // 3X 'TOTAL DETAIL RECORDS:    ' 9X #TOT-1W-REJ / 3X 'TOTAL EMPTY FORMS:       ' 9X #EMPTY-REJECT-CNT / 3X 'TOTAL GROSS:          ' #TIN-GROSS-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL FED TAX:        ' #TIN-FED-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL STATE TAX:      ' #TIN-STATE-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL LOCAL TAX:      ' #TIN-LOCAL-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) /
            ColumnSpacing(3),"TOTAL EMPTY FORMS:       ",new ColumnSpacing(9),pnd_Ws_Pnd_Empty_Reject_Cnt,NEWLINE,new ColumnSpacing(3),"TOTAL GROSS:          ",pnd_Tin_Gross_Rej, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL FED TAX:        ",pnd_Tin_Fed_Tax_Rej, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(3),"TOTAL STATE TAX:      ",pnd_Tin_State_Tax_Rej, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL LOCAL TAX:      ",pnd_Tin_Local_Tax_Rej, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        //*  3X 'TOTAL FORMS:             ' 9X #TOTAL-FORMS /
        getReports().write(4, NEWLINE,NEWLINE,"Accepted Totals:",NEWLINE,NEWLINE,new ColumnSpacing(3),"TOTAL DETAIL RECORDS:    ",new ColumnSpacing(9),pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc,NEWLINE,new  //Natural: WRITE ( 4 ) // 'Accepted Totals:' // 3X 'TOTAL DETAIL RECORDS:    ' 9X #TOT-1W-ACC / 3X 'TOTAL FORMS:             ' 9X #ACCEPTED-FORMS / 3X 'TOTAL EMPTY FORMS:       ' 9X #EMPTY-ACCEPT-CNT / 3X 'TOTAL GROSS:          ' #TIN-GROSS-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL FED TAX:        ' #TIN-FED-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL STATE TAX:      ' #TIN-STATE-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL LOCAL TAX:      ' #TIN-LOCAL-TAX-ACC ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) //
            ColumnSpacing(3),"TOTAL FORMS:             ",new ColumnSpacing(9),pnd_Accepted_Forms,NEWLINE,new ColumnSpacing(3),"TOTAL EMPTY FORMS:       ",new 
            ColumnSpacing(9),pnd_Ws_Pnd_Empty_Accept_Cnt,NEWLINE,new ColumnSpacing(3),"TOTAL GROSS:          ",pnd_Tin_Gross_Acc, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(3),"TOTAL FED TAX:        ",pnd_Tin_Fed_Tax_Acc, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL STATE TAX:      ",pnd_Tin_State_Tax_Acc, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL LOCAL TAX:      ",pnd_Tin_Local_Tax_Acc, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(4, NEWLINE,NEWLINE,"Rejected Totals:",NEWLINE,NEWLINE,new ColumnSpacing(3),"TOTAL DETAIL RECORDS:    ",new ColumnSpacing(9),pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej,NEWLINE,new  //Natural: WRITE ( 4 ) // 'Rejected Totals:' // 3X 'TOTAL DETAIL RECORDS:    ' 9X #TOT-1W-REJ / 3X 'TOTAL FORMS:             ' 9X #REJECTED-FORMS / 3X 'TOTAL EMPTY FORMS:       ' 9X #EMPTY-REJECT-CNT / 3X 'TOTAL GROSS:          ' #TIN-GROSS-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL FED TAX:        ' #TIN-FED-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL STATE TAX:      ' #TIN-STATE-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL LOCAL TAX:      ' #TIN-LOCAL-TAX-REJ ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) //
            ColumnSpacing(3),"TOTAL FORMS:             ",new ColumnSpacing(9),pnd_Rejected_Forms,NEWLINE,new ColumnSpacing(3),"TOTAL EMPTY FORMS:       ",new 
            ColumnSpacing(9),pnd_Ws_Pnd_Empty_Reject_Cnt,NEWLINE,new ColumnSpacing(3),"TOTAL GROSS:          ",pnd_Tin_Gross_Rej, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(3),"TOTAL FED TAX:        ",pnd_Tin_Fed_Tax_Rej, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL STATE TAX:      ",pnd_Tin_State_Tax_Rej, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL LOCAL TAX:      ",pnd_Tin_Local_Tax_Rej, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Empty_Tot_Cnt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Empty_Tot_Cnt), pnd_Ws_Pnd_Empty_Accept_Cnt.add(pnd_Ws_Pnd_Empty_Reject_Cnt));           //Natural: ASSIGN #EMPTY-TOT-CNT := #EMPTY-ACCEPT-CNT + #EMPTY-REJECT-CNT
        getReports().write(4, NEWLINE,NEWLINE,"Grand Totals:",NEWLINE,NEWLINE,new ColumnSpacing(3),"TOTAL DETAIL RECORDS:    ",new ColumnSpacing(9),pnd_Tax_Tape_Totals_Pnd_Grand_1w,NEWLINE,new  //Natural: WRITE ( 4 ) // 'Grand Totals:' // 3X 'TOTAL DETAIL RECORDS:    ' 9X #GRAND-1W / 3X 'TOTAL FORMS:             ' 9X #TOTAL-FORMS / 3X 'TOTAL EMPTY FORMS:       ' 9X #EMPTY-TOT-CNT / 3X 'TOTAL GROSS:          ' #GRAND-GROSS ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL FED TAX:        ' #GRAND-FED-TAX ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL STATE TAX:      ' #GRAND-STATE-TAX ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 3X 'TOTAL LOCAL TAX:      ' #GRAND-LOCAL-TAX ( EM = -ZZZ,ZZZ,ZZZ,ZZ9.99 ) /
            ColumnSpacing(3),"TOTAL FORMS:             ",new ColumnSpacing(9),pnd_Total_Forms,NEWLINE,new ColumnSpacing(3),"TOTAL EMPTY FORMS:       ",new 
            ColumnSpacing(9),pnd_Ws_Pnd_Empty_Tot_Cnt,NEWLINE,new ColumnSpacing(3),"TOTAL GROSS:          ",pnd_Grand_Gross, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(3),"TOTAL FED TAX:        ",pnd_Grand_Fed_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL STATE TAX:      ",pnd_Grand_State_Tax, 
            new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(3),"TOTAL LOCAL TAX:      ",pnd_Grand_Local_Tax, new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9.99"),
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, NEWLINE,NEWLINE,"Total Participants: ",pnd_Cuny_Part_Cnt);                                                                                  //Natural: WRITE ( 5 ) // 'Total Participants: ' #CUNY-PART-CNT
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,NEWLINE,"Total Participants: ",pnd_Suny_Part_Cnt);                                                                                  //Natural: WRITE ( 6 ) // 'Total Participants: ' #SUNY-PART-CNT
        if (Global.isEscape()) return;
        //*  G2.
        //*  GET TIRCNTL-RPT-U #TWRATBL4.#ISN
        //*                                     UPDATE CONTROL RECORDS
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(pnd_Ws_Pnd_Datx);                                                                              //Natural: ASSIGN TIRCNTL-RPT-DTE ( 1 ) := #DATX
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
        sub_Update_Control();
        if (condition(Global.isEscape())) {return;}
        //*  TIRCNTL-RPT-U.TIRCNTL-RPT-DTE (1)  := #DATX
        //*  UPDATE (G2.)
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ************************
        //*  S U B R O U T I N E S
        //* **************************
        //* ********************************
        //* * << DASRAH START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-LOOKUP-TIRF-LTR
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AGAIN-LOOKUP
        //* ***************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PA-CONTRACT-LOOKUP
        //* * DASRAH END >>
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUT-TO-TAPE-NEW
        //*   IF #EMPLOYEE-DESC = "Public"
        //*         '/Name'  #EXTRACT-NAME (AL=20)
        //*         '/Name'  #EXTRACT-NAME (AL=20)
        //* ***************************************
        //* **********************************
        //* ****************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //*  #TWRATBL4.#REC-TYPE                := '9ST0'
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLAN-DATA
        //* *******************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CLIENT-DATA
        //*  #ABEND-IND
        //*  TWRACLR2.#DISPLAY-IND
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-IRS-STATUS
        //* ***********************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CUNY-SUNY-CHECK
        //*        #OUTW-NY-PLAN GIVING NUMBER IN #NUM INDEX IN #INDEX
        //*         WRITE 'CUNY/SUNY PLAN NOT FOUND' #OUTW-NY-PLAN
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRAP-NAME
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CORRECT-CLIENT-ID
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CORRECT-CLIENT-ID2
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-CLIENT-TABLE
        //* ***********************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Form_Lookup_Tirf_Ltr() throws Exception                                                                                                              //Natural: FORM-LOOKUP-TIRF-LTR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Tirf_Superde_1.reset();                                                                                                                                       //Natural: RESET #TIRF-SUPERDE-1
        pnd_Actual_Reject.setValue("N");                                                                                                                                  //Natural: ASSIGN #ACTUAL-REJECT := 'N'
        pnd_Tirf_Superde_1_Tirf_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                   //Natural: MOVE #WS.#TAX-YEAR TO #TIRF-SUPERDE-1.TIRF-TAX-YEAR
        pnd_Tirf_Superde_1_Tirf_Tin.setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                           //Natural: MOVE FORM-R.TIRF-TIN TO #TIRF-SUPERDE-1.TIRF-TIN
        pnd_Tirf_Superde_1_Tirf_Form_Type.setValue(1);                                                                                                                    //Natural: MOVE 01 TO #TIRF-SUPERDE-1.TIRF-FORM-TYPE
        vw_twr_Frm.startDatabaseRead                                                                                                                                      //Natural: READ TWR-FRM WITH TIRF-SUPERDE-1 = #TIRF-SUPERDE-1
        (
        "READ01",
        new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Tirf_Superde_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
        );
        READ01:
        while (condition(vw_twr_Frm.readNextRow("READ01")))
        {
            if (condition(twr_Frm_Tirf_Tax_Year.notEquals(pnd_Tirf_Superde_1_Tirf_Tax_Year) || twr_Frm_Tirf_Tin.notEquals(pnd_Tirf_Superde_1_Tirf_Tin)                    //Natural: IF TWR-FRM.TIRF-TAX-YEAR NE #TIRF-SUPERDE-1.TIRF-TAX-YEAR OR TWR-FRM.TIRF-TIN NE #TIRF-SUPERDE-1.TIRF-TIN OR TWR-FRM.TIRF-FORM-TYPE NE #TIRF-SUPERDE-1.TIRF-FORM-TYPE
                || twr_Frm_Tirf_Form_Type.notEquals(pnd_Tirf_Superde_1_Tirf_Form_Type)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* * SPECIAL CHK TO SEE IF ANY CONTRACT BELONGS TO SUNY
            setValueToSubstring(pnd_Ws_Pnd_Tax_Year_A,pnd_Tirf_Superde_3,1,4);                                                                                            //Natural: MOVE #WS.#TAX-YEAR-A TO SUBSTR ( #TIRF-SUPERDE-3,1,4 )
            setValueToSubstring(twr_Frm_Tirf_Tin,pnd_Tirf_Superde_3,5,10);                                                                                                //Natural: MOVE TWR-FRM.TIRF-TIN TO SUBSTR ( #TIRF-SUPERDE-3,5,10 )
            setValueToSubstring("10",pnd_Tirf_Superde_3,15,2);                                                                                                            //Natural: MOVE '10' TO SUBSTR ( #TIRF-SUPERDE-3,15,2 )
            setValueToSubstring(twr_Frm_Tirf_Contract_Nbr,pnd_Tirf_Superde_3,17,8);                                                                                       //Natural: MOVE TWR-FRM.TIRF-CONTRACT-NBR TO SUBSTR ( #TIRF-SUPERDE-3,17,8 )
            setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Tirf_Superde_3,25,1);                                                                                         //Natural: MOVE LOW-VALUES TO SUBSTR ( #TIRF-SUPERDE-3,25,1 )
            pnd_No_Suny.setValue(false);                                                                                                                                  //Natural: ASSIGN #NO-SUNY := FALSE
            vw_twr_Frm1.startDatabaseRead                                                                                                                                 //Natural: READ ( 1 ) TWR-FRM1 WITH TIRF-SUPERDE-1 = #TIRF-SUPERDE-3
            (
            "READ02",
            new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Tirf_Superde_3.getBinary(), WcType.BY) },
            new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") },
            1
            );
            READ02:
            while (condition(vw_twr_Frm1.readNextRow("READ02")))
            {
                if (condition(twr_Frm1_Tirf_Tax_Year.notEquals(pnd_Ws_Pnd_Tax_Year_A) || twr_Frm1_Tirf_Form_Type.notEquals(10) || twr_Frm1_Tirf_Tin.notEquals(twr_Frm_Tirf_Tin)  //Natural: IF TWR-FRM1.TIRF-TAX-YEAR NE #WS.#TAX-YEAR-A OR TWR-FRM1.TIRF-FORM-TYPE NE 10 OR TWR-FRM1.TIRF-TIN NE TWR-FRM.TIRF-TIN OR TWR-FRM1.TIRF-CONTRACT-NBR NE TWR-FRM.TIRF-CONTRACT-NBR
                    || twr_Frm1_Tirf_Contract_Nbr.notEquals(twr_Frm_Tirf_Contract_Nbr)))
                {
                    pnd_No_Suny.setValue(true);                                                                                                                           //Natural: ASSIGN #NO-SUNY := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_No_Suny.getBoolean()))                                                                                                                      //Natural: IF #NO-SUNY
            {
                pnd_Actual_Reject.setValue("N");                                                                                                                          //Natural: ASSIGN #ACTUAL-REJECT := 'N'
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PA-CONTRACT-LOOKUP
            sub_Pa_Contract_Lookup();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Pa_Select.getBoolean()))                                                                                                                    //Natural: IF #PA-SELECT
            {
                pnd_Actual_Reject.setValue("N");                                                                                                                          //Natural: ASSIGN #ACTUAL-REJECT := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(twr_Frm_Tirf_Ltr_Data.equals("Y")))                                                                                                         //Natural: IF TWR-FRM.TIRF-LTR-DATA = 'Y'
                {
                    pnd_Actual_Reject.setValue("N");                                                                                                                      //Natural: ASSIGN #ACTUAL-REJECT := 'N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM AGAIN-LOOKUP
                    sub_Again_Lookup();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Any_Ltr.equals("Y")))                                                                                                               //Natural: IF #ANY-LTR = 'Y'
                    {
                        pnd_Actual_Reject.setValue("N");                                                                                                                  //Natural: ASSIGN #ACTUAL-REJECT := 'N'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Actual_Reject.setValue("Y");                                                                                                                  //Natural: ASSIGN #ACTUAL-REJECT := 'Y'
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Again_Lookup() throws Exception                                                                                                                      //Natural: AGAIN-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Any_Ltr.setValue("N");                                                                                                                                        //Natural: ASSIGN #ANY-LTR := 'N'
        pnd_Tirf2_Superde_1_Tirf_Tax_Year.setValue(pnd_Ws_Pnd_Tax_Year);                                                                                                  //Natural: MOVE #WS.#TAX-YEAR TO #TIRF2-SUPERDE-1.TIRF-TAX-YEAR
        pnd_Tirf2_Superde_1_Tirf_Tin.setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                          //Natural: MOVE FORM-R.TIRF-TIN TO #TIRF2-SUPERDE-1.TIRF-TIN
        pnd_Tirf2_Superde_1_Tirf_Form_Type.setValue(1);                                                                                                                   //Natural: MOVE 01 TO #TIRF2-SUPERDE-1.TIRF-FORM-TYPE
        setValueToSubstring(twr_Frm_Tirf_Contract_Nbr,pnd_Tirf2_Superde_1,17,8);                                                                                          //Natural: MOVE TWR-FRM.TIRF-CONTRACT-NBR TO SUBSTR ( #TIRF2-SUPERDE-1,17,8 )
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Tirf2_Superde_1,25,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #TIRF2-SUPERDE-1,25,1 )
        vw_twr_Frm2.startDatabaseRead                                                                                                                                     //Natural: READ TWR-FRM2 WITH TIRF-SUPERDE-1 = #TIRF2-SUPERDE-1
        (
        "READ03",
        new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Tirf2_Superde_1.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
        );
        READ03:
        while (condition(vw_twr_Frm2.readNextRow("READ03")))
        {
            if (condition(!(twr_Frm2_Tirf_Active_Ind.equals("A"))))                                                                                                       //Natural: ACCEPT IF TWR-FRM2.TIRF-ACTIVE-IND = 'A'
            {
                continue;
            }
            if (condition(twr_Frm2_Tirf_Tax_Year.notEquals(pnd_Tirf2_Superde_1_Tirf_Tax_Year) || twr_Frm2_Tirf_Tin.notEquals(pnd_Tirf2_Superde_1_Tirf_Tin)                //Natural: IF TWR-FRM2.TIRF-TAX-YEAR NE #TIRF2-SUPERDE-1.TIRF-TAX-YEAR OR TWR-FRM2.TIRF-TIN NE #TIRF2-SUPERDE-1.TIRF-TIN OR TWR-FRM2.TIRF-FORM-TYPE NE 01 OR TWR-FRM2.TIRF-CONTRACT-NBR NE SUBSTR ( #TIRF2-SUPERDE-1,17,8 )
                || twr_Frm2_Tirf_Form_Type.notEquals(1) || twr_Frm2_Tirf_Contract_Nbr.notEquals(pnd_Tirf2_Superde_1.getSubstring(17,8))))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(twr_Frm2_Tirf_Ltr_Data.equals("Y")))                                                                                                            //Natural: IF TWR-FRM2.TIRF-LTR-DATA = 'Y'
            {
                pnd_Any_Ltr.setValue("Y");                                                                                                                                //Natural: ASSIGN #ANY-LTR := 'Y'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Pa_Contract_Lookup() throws Exception                                                                                                                //Natural: PA-CONTRACT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Pa_Select.reset();                                                                                                                                            //Natural: RESET #PA-SELECT
        if (condition(twr_Frm_Tirf_Contract_Nbr.getSubstring(1,2).equals("00") || twr_Frm_Tirf_Contract_Nbr.getSubstring(1,2).equals("08")))                              //Natural: IF SUBSTR ( TWR-FRM.TIRF-CONTRACT-NBR,1,2 ) = '00' OR = '08'
        {
            pnd_Pa_Select.setValue(true);                                                                                                                                 //Natural: ASSIGN #PA-SELECT := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(twr_Frm_Tirf_Contract_Nbr.getSubstring(1,2).equals("IG")))                                                                                          //Natural: IF SUBSTR ( TWR-FRM.TIRF-CONTRACT-NBR,1,2 ) = 'IG'
        {
            vw_iaa_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = TWR-FRM.TIRF-CONTRACT-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", twr_Frm_Tirf_Contract_Nbr, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_iaa_Cntrct.readNextRow("FIND01")))
            {
                vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
                if (condition(iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr.getSubstring(1,2).equals("08") || iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr.getSubstring(1,                 //Natural: IF SUBSTR ( IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR,1,2 ) = '08' OR = 'G3'
                    2).equals("G3")))
                {
                    pnd_Pa_Select.setValue(true);                                                                                                                         //Natural: ASSIGN #PA-SELECT := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(twr_Frm_Tirf_Contract_Nbr.getSubstring(1,2).equals("S0")))                                                                                          //Natural: IF SUBSTR ( TWR-FRM.TIRF-CONTRACT-NBR,1,2 ) = 'S0'
        {
            vw_iaa_Cntrct.startDatabaseFind                                                                                                                               //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = TWR-FRM.TIRF-CONTRACT-NBR
            (
            "FIND02",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", twr_Frm_Tirf_Contract_Nbr, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(vw_iaa_Cntrct.readNextRow("FIND02")))
            {
                vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
                if (condition(iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr.getSubstring(1,2).equals("08")))                                                                       //Natural: IF SUBSTR ( IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR,1,2 ) = '08'
                {
                    pnd_Pa_Select.setValue(true);                                                                                                                         //Natural: ASSIGN #PA-SELECT := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Out_To_Tape_New() throws Exception                                                                                                             //Natural: WRITE-OUT-TO-TAPE-NEW
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaTwrl3620.getPnd_Out_Ny_Record().reset();                                                                                                                       //Natural: RESET #OUT-NY-RECORD
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter1().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER1 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter2().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER2 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter3().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER3 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter4().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER4 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter5().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER5 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter6().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER6 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter7().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER7 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter8().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER8 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter9().setValue("|");                                                                                                  //Natural: ASSIGN #DELIMITER9 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter10().setValue("|");                                                                                                 //Natural: ASSIGN #DELIMITER10 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Delimiter11().setValue("|");                                                                                                 //Natural: ASSIGN #DELIMITER11 := '|'
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin().setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                    //Natural: ASSIGN #OUTW-NY-TIN := FORM-R.TIRF-TIN
        ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Name().setValue(pnd_Extract_Name);                                                                                   //Natural: ASSIGN #OUTW-NY-NAME := #EXTRACT-NAME
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO C*FORM-R.TIRF-LTR-GRP
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl9715.getForm_R_Count_Casttirf_Ltr_Grp())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I).equals(getZero()) && ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I).equals(getZero())  //Natural: IF FORM-R.TIRF-LTR-GROSS-AMT ( #I ) = 0 AND FORM-R.TIRF-LTR-PER ( #I ) = 0 AND FORM-R.TIRF-LTR-FED-TAX-WTHLD ( #I ) = 0 AND FORM-R.TIRF-LTR-STATE-TAX-WTHLD ( #I ) = 0 AND FORM-R.TIRF-LTR-LOC-TAX-WTHLD ( #I ) = 0
                && ldaTwrl9715.getForm_R_Tirf_Ltr_Fed_Tax_Wthld().getValue(pnd_Ws_Pnd_I).equals(getZero()) && ldaTwrl9715.getForm_R_Tirf_Ltr_State_Tax_Wthld().getValue(pnd_Ws_Pnd_I).equals(getZero()) 
                && ldaTwrl9715.getForm_R_Tirf_Ltr_Loc_Tax_Wthld().getValue(pnd_Ws_Pnd_I).equals(getZero())))
            {
                getReports().write(0, "ALL ZERO ",ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                      //Natural: WRITE 'ALL ZERO ' FORM-R.TIRF-TIN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Tin.setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                                  //Natural: ASSIGN #HOLD-TIN := FORM-R.TIRF-TIN
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  SHOULD ONLY BE NEEDED FOR 2012
                                                                                                                                                                          //Natural: PERFORM CORRECT-CLIENT-ID
            sub_Correct_Client_Id();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  #OUTW-TIN-GROSS        :=  #EXTRACT-GROSS-AMT-HEADER
            //*  WRITE '=' #HOLD-TIN '=' FORM-R.TIRF-TIN
            if (condition(pnd_Hold_Tin.notEquals(ldaTwrl9715.getForm_R_Tirf_Tin())))                                                                                      //Natural: IF #HOLD-TIN NE FORM-R.TIRF-TIN
            {
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Tin_Gross().setValueEdited(pnd_Extract_Gross_Amt_Header,new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99-"));             //Natural: MOVE EDITED #EXTRACT-GROSS-AMT-HEADER ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99- ) TO #OUTW-TIN-GROSS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Tin_Gross().reset();                                                                                            //Natural: RESET #OUTW-TIN-GROSS
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan().setValue(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I));                                       //Natural: ASSIGN #OUTW-NY-PLAN := FORM-R.TIRF-PLAN ( #I )
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract().setValue(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr());                                                  //Natural: ASSIGN #OUTW-NY-CONTRACT := FORM-R.TIRF-CONTRACT-NBR
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract().setValue(ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_I));          //Natural: ASSIGN #OUTW-NY-ORIGINATING-CONTRACT := FORM-R.TIRF-ORIG-CONTRACT-NBR ( #I )
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Contract_Typ().getValue(pnd_Ws_Pnd_I));                      //Natural: ASSIGN #OUTW-NY-CONTRACT-TYPE := FORM-R.TIRF-CONTRACT-TYP ( #I )
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type().setValue(ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ().getValue(pnd_Ws_Pnd_I));            //Natural: ASSIGN #OUTW-NY-ORIG-CONTRACT-TYPE := FORM-R.TIRF-ORIG-CONTRACT-TYP ( #I )
            //*   #OUTW-NY-GROSS-WAGE := FORM-R.TIRF-LTR-GROSS-AMT(#I)
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage().setValueEdited(ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I),new                //Natural: MOVE EDITED FORM-R.TIRF-LTR-GROSS-AMT ( #I ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99- ) TO #OUTW-NY-GROSS-WAGE
                ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99-"));
            //*  #OUTW-NY-PERC-CONTRACT-TOT := FORM-R.TIRF-LTR-PER(#I)
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot().setValueEdited(ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I),new               //Natural: MOVE EDITED FORM-R.TIRF-LTR-PER ( #I ) ( EM = ZZ9.9999 ) TO #OUTW-NY-PERC-CONTRACT-TOT
                ReportEditMask("ZZ9.9999"));
            pnd_Plan_Type.reset();                                                                                                                                        //Natural: RESET #PLAN-TYPE
            if (condition(ldaTwrl9715.getForm_R_Tirf_Plan_Type().getValue(pnd_Ws_Pnd_I).equals("P")))                                                                     //Natural: IF FORM-R.TIRF-PLAN-TYPE ( #I ) = 'P'
            {
                //*     EXAMINE FULL  #CUNY-SUNY-PLAN(*) FOR FULL
                //*       #OUTW-NY-PLAN GIVING NUMBER IN #NUM INDEX IN #INDEX
                //*     WRITE #OUTW-NY-PLAN '=' #NUM '=' #INDEX
                //*     IF #NUM = 0
                //*       WRITE 'CUNY/SUNY PLAN NOT FOUND' #OUTW-NY-PLAN
                //*       #PLAN-TYPE := 'Z'
                //*     ELSE
                //*       IF #INDEX < 6
                //*         #PLAN-TYPE := 'S'
                //*         #REPORT-TYPE := 'S'
                //*       ELSE
                //*         #PLAN-TYPE := 'C'
                //*         #REPORT-TYPE := 'C'
                //*       END-IF
                //*     END-IF
                if (condition(pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().notEquals(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I))))                                //Natural: IF #PLAN-NUM NE FORM-R.TIRF-PLAN ( #I )
                {
                                                                                                                                                                          //Natural: PERFORM GET-PLAN-DATA
                    sub_Get_Plan_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*     #PLAN-TYPE := 'X'
                if (condition(pdaTwraclr2.getTwraclr2_Pnd_Client_Id().notEquals(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I))))                               //Natural: IF TWRACLR2.#CLIENT-ID NE FORM-R.TIRF-PLAN ( #I )
                {
                                                                                                                                                                          //Natural: PERFORM GET-CLIENT-DATA
                    sub_Get_Client_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE '=' #PLAN-TYPE '=' #REPORT-TYPE
            if (condition(ldaTwrl9715.getForm_R_Tirf_Plan_Type().getValue(pnd_Ws_Pnd_I).equals("P")))                                                                     //Natural: IF FORM-R.TIRF-PLAN-TYPE ( #I ) = 'P'
            {
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1().setValue(pdaTwraplr2.getTwraplr2_Rt_Pl_Name_1());                                               //Natural: ASSIGN #OUTW-NY-PLAN-NAME1 := RT-PL-NAME-1
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2().setValue(pdaTwraplr2.getTwraplr2_Rt_Pl_Name_2());                                               //Natural: ASSIGN #OUTW-NY-PLAN-NAME2 := RT-PL-NAME-2
                //*    #OUTW-NY-EMPL-TYPE := RT-PL-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1().setValue(pdaTwraclr2.getTwraclr2_Rt_Ci_Name_1());                                               //Natural: ASSIGN #OUTW-NY-PLAN-NAME1 := RT-CI-NAME-1
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2().setValue(pdaTwraclr2.getTwraclr2_Rt_Ci_Name_2());                                               //Natural: ASSIGN #OUTW-NY-PLAN-NAME2 := RT-CI-NAME-2
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type().setValue(" ");                                                                                   //Natural: ASSIGN #OUTW-NY-EMPL-TYPE := ' '
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRAP-NAME
            sub_Wrap_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DECIDE ON FIRST VALUE OF #OUTW-NY-EMPL-TYPE
            pnd_Employee_Desc.reset();                                                                                                                                    //Natural: RESET #EMPLOYEE-DESC
            short decideConditionsMet1234 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF RT-PL-TYPE;//Natural: VALUE '01'
            if (condition((pdaTwraplr2.getTwraplr2_Rt_Pl_Type().equals("01"))))
            {
                decideConditionsMet1234++;
                pnd_Employee_Desc.setValue("Public");                                                                                                                     //Natural: ASSIGN #EMPLOYEE-DESC := "Public"
            }                                                                                                                                                             //Natural: VALUE '02'
            else if (condition((pdaTwraplr2.getTwraplr2_Rt_Pl_Type().equals("02"))))
            {
                decideConditionsMet1234++;
                pnd_Employee_Desc.setValue("Private");                                                                                                                    //Natural: ASSIGN #EMPLOYEE-DESC := "Private"
            }                                                                                                                                                             //Natural: VALUE '03'
            else if (condition((pdaTwraplr2.getTwraplr2_Rt_Pl_Type().equals("03"))))
            {
                decideConditionsMet1234++;
                pnd_Employee_Desc.setValue("Religious");                                                                                                                  //Natural: ASSIGN #EMPLOYEE-DESC := "Religious"
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Employee_Desc.setValue("Unknown");                                                                                                                    //Natural: ASSIGN #EMPLOYEE-DESC := "Unknown"
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaTwrl9715.getForm_R_Tirf_Plan_Type().getValue(pnd_Ws_Pnd_I).notEquals("P")))                                                                  //Natural: IF FORM-R.TIRF-PLAN-TYPE ( #I ) NE 'P'
            {
                pnd_Employee_Desc.setValue("Non-SUNY/CUNY");                                                                                                              //Natural: ASSIGN #EMPLOYEE-DESC := 'Non-SUNY/CUNY'
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type().setValue(pnd_Employee_Desc);                                                                         //Natural: ASSIGN #OUTW-NY-EMPL-TYPE := #EMPLOYEE-DESC
            //*   ELSE
            //*     #OUTW-NY-EMPL-TYPE := 'Non-SUNY/CUNY'
            //*   END-IF
            pnd_Tax_Tape_Totals_Pnd_Grand_1w.nadd(1);                                                                                                                     //Natural: ADD 1 TO #GRAND-1W
            if (condition(pnd_Extract_Reject_Ind.notEquals("Y")))                                                                                                         //Natural: IF #EXTRACT-REJECT-IND NE 'Y'
            {
                if (condition(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract().equals(pnd_Hold_Contract)))                                                         //Natural: IF #OUTW-NY-CONTRACT = #HOLD-CONTRACT
                {
                    ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Tin_Gross().reset();                                                                                        //Natural: RESET #OUTW-TIN-GROSS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Hold_Contract.setValue(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract());                                                                      //Natural: ASSIGN #HOLD-CONTRACT := #OUTW-NY-CONTRACT
                pnd_Tax_Tape_Totals_Pnd_Tot_1w_Acc.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOT-1W-ACC
                getReports().display(2, "/Contract",                                                                                                                      //Natural: DISPLAY ( 2 ) '/Contract' FORM-R.TIRF-CONTRACT-NBR 'Contract/Type' FORM-R.TIRF-CONTRACT-TYP ( #I ) 'Orig DA/Contract' FORM-R.TIRF-ORIG-CONTRACT-NBR ( #I ) 'Orig Cont/Type' FORM-R.TIRF-ORIG-CONTRACT-TYP ( #I ) '/Plan' FORM-R.TIRF-PLAN ( #I ) 'Plan/Name' #OUTW-NY-PLAN-NAME1 / ' ' #OUTW-NY-PLAN-NAME2 'Employer/Type' #EMPLOYEE-DESC 'Plan/Income' FORM-R.TIRF-LTR-GROSS-AMT ( #I ) '/Percent' FORM-R.TIRF-LTR-PER ( #I ) ( EM = ZZ9.9999 )
                		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
                		ldaTwrl9715.getForm_R_Tirf_Contract_Typ().getValue(pnd_Ws_Pnd_I),"Orig DA/Contract",
                		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_I),"Orig Cont/Type",
                		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ().getValue(pnd_Ws_Pnd_I),"/Plan",
                		ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I),"Plan/Name",
                		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
                		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
                		pnd_Employee_Desc,"Plan/Income",
                		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I),"/Percent",
                		ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("ZZ9.9999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      'FED TAX/WITHHELD' FORM-R.TIRF-LTR-FED-TAX-WTHLD(#I) 2X
                //*      'STATE TAX/WITHHELD' FORM-R.TIRF-LTR-STATE-TAX-WTHLD(#I) 2X
                //*       'Local Tax/Withheld' FORM-R.TIRF-LTR-LOC-TAX-WTHLD(#I)
                //*     IF #PLAN-TYPE = 'C' OR #REPORT-TYPE = 'C'
                if (condition(pnd_Print_Cuny.equals("Y") || pnd_Print_Both.equals("Y")))                                                                                  //Natural: IF #PRINT-CUNY = 'Y' OR #PRINT-BOTH = 'Y'
                {
                    //*      WRITE '=' #HOLD-CUNY-CONT '=' FORM-R.TIRF-CONTRACT-NBR
                    if (condition(pnd_Hold_Fields_Pnd_Hold_Cuny_Cont.notEquals(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr())))                                               //Natural: IF #HOLD-CUNY-CONT NE FORM-R.TIRF-CONTRACT-NBR
                    {
                        getReports().write(5, " ");                                                                                                                       //Natural: WRITE ( 05 ) ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Hold_Fields_Pnd_Hold_Cuny_Cont.setValue(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr());                                                               //Natural: ASSIGN #HOLD-CUNY-CONT := FORM-R.TIRF-CONTRACT-NBR
                    //*      WRITE 'REJ ' '=' #HOLD-TIN '=' FORM-R.TIRF-TIN
                    if (condition(pnd_Hold_Tin.notEquals(ldaTwrl9715.getForm_R_Tirf_Tin())))                                                                              //Natural: IF #HOLD-TIN NE FORM-R.TIRF-TIN
                    {
                        pnd_Cuny_Part_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CUNY-PART-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(5, "/TIN",                                                                                                                       //Natural: DISPLAY ( 5 ) '/TIN' FORM-R.TIRF-TIN '/Contract' FORM-R.TIRF-CONTRACT-NBR 'Contract/Type' FORM-R.TIRF-CONTRACT-TYP ( #I ) 'Orig DA/Contract' FORM-R.TIRF-ORIG-CONTRACT-NBR ( #I ) 'Orig Cont/Type' FORM-R.TIRF-ORIG-CONTRACT-TYP ( #I ) '/Plan' FORM-R.TIRF-PLAN ( #I ) 'Plan/Name' #OUTW-NY-PLAN-NAME1 / ' ' #OUTW-NY-PLAN-NAME2 'Employer/Type' #EMPLOYEE-DESC 'Plan/Income' FORM-R.TIRF-LTR-GROSS-AMT ( #I ) '/Percent' FORM-R.TIRF-LTR-PER ( #I ) ( EM = ZZ9.9999 )
                    		ldaTwrl9715.getForm_R_Tirf_Tin(),"/Contract",
                    		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
                    		ldaTwrl9715.getForm_R_Tirf_Contract_Typ().getValue(pnd_Ws_Pnd_I),"Orig DA/Contract",
                    		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_I),"Orig Cont/Type",
                    		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ().getValue(pnd_Ws_Pnd_I),"/Plan",
                    		ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I),"Plan/Name",
                    		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
                    		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
                    		pnd_Employee_Desc,"Plan/Income",
                    		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I),"/Percent",
                    		ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("ZZ9.9999"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #PLAN-TYPE = 'S' OR #REPORT-TYPE = 'S'
                if (condition(pnd_Print_Suny.equals("Y") || pnd_Print_Both.equals("Y")))                                                                                  //Natural: IF #PRINT-SUNY = 'Y' OR #PRINT-BOTH = 'Y'
                {
                    if (condition(pnd_Hold_Fields_Pnd_Hold_Suny_Cont.notEquals(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr())))                                               //Natural: IF #HOLD-SUNY-CONT NE FORM-R.TIRF-CONTRACT-NBR
                    {
                        getReports().write(6, " ");                                                                                                                       //Natural: WRITE ( 06 ) ' '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Hold_Fields_Pnd_Hold_Suny_Cont.setValue(ldaTwrl9715.getForm_R_Tirf_Contract_Nbr());                                                               //Natural: ASSIGN #HOLD-SUNY-CONT := FORM-R.TIRF-CONTRACT-NBR
                    if (condition(pnd_Hold_Tin.notEquals(ldaTwrl9715.getForm_R_Tirf_Tin())))                                                                              //Natural: IF #HOLD-TIN NE FORM-R.TIRF-TIN
                    {
                        pnd_Suny_Part_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #SUNY-PART-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(6, "/TIN",                                                                                                                       //Natural: DISPLAY ( 6 ) '/TIN' FORM-R.TIRF-TIN '/Contract' FORM-R.TIRF-CONTRACT-NBR 'Contract/Type' FORM-R.TIRF-CONTRACT-TYP ( #I ) 'Orig DA/Contract' FORM-R.TIRF-ORIG-CONTRACT-NBR ( #I ) 'Orig Cont/Type' FORM-R.TIRF-ORIG-CONTRACT-TYP ( #I ) '/Plan' FORM-R.TIRF-PLAN ( #I ) 'Plan/Name' #OUTW-NY-PLAN-NAME1 / ' ' #OUTW-NY-PLAN-NAME2 'Employer/Type' #EMPLOYEE-DESC 'Plan/Income' FORM-R.TIRF-LTR-GROSS-AMT ( #I ) '/Percent' FORM-R.TIRF-LTR-PER ( #I ) ( EM = ZZ9.9999 )
                    		ldaTwrl9715.getForm_R_Tirf_Tin(),"/Contract",
                    		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
                    		ldaTwrl9715.getForm_R_Tirf_Contract_Typ().getValue(pnd_Ws_Pnd_I),"Orig DA/Contract",
                    		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_I),"Orig Cont/Type",
                    		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ().getValue(pnd_Ws_Pnd_I),"/Plan",
                    		ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I),"Plan/Name",
                    		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
                    		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
                    		pnd_Employee_Desc,"Plan/Income",
                    		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I),"/Percent",
                    		ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("ZZ9.9999"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Plan_Name.setValue(DbsUtil.compress(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(), ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2())); //Natural: COMPRESS #OUTW-NY-PLAN-NAME1 #OUTW-NY-PLAN-NAME2 INTO #PLAN-NAME
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1().setValue(pnd_Plan_Name_Pnd_Plan_Name1);                                                         //Natural: ASSIGN #OUTW-NY-PLAN-NAME1 := #PLAN-NAME1
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2().setValue(pnd_Plan_Name_Pnd_Plan_Name2);                                                         //Natural: ASSIGN #OUTW-NY-PLAN-NAME2 := #PLAN-NAME2
                getWorkFiles().write(1, false, ldaTwrl3620.getPnd_Out_Ny_Record());                                                                                       //Natural: WRITE WORK FILE 01 #OUT-NY-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tax_Tape_Totals_Pnd_Tot_1w_Rej.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOT-1W-REJ
                getReports().display(3, "/Contract",                                                                                                                      //Natural: DISPLAY ( 3 ) '/Contract' FORM-R.TIRF-CONTRACT-NBR 'Contract/Type' FORM-R.TIRF-CONTRACT-TYP ( #I ) 'Orig DA/Contract' FORM-R.TIRF-ORIG-CONTRACT-NBR ( #I ) 'Orig Cont/Type' FORM-R.TIRF-ORIG-CONTRACT-TYP ( #I ) '/Plan' FORM-R.TIRF-PLAN ( #I ) 'Plan/Name' #OUTW-NY-PLAN-NAME1 / ' ' #OUTW-NY-PLAN-NAME2 'Employer/Type' #EMPLOYEE-DESC 'Plan/Income' FORM-R.TIRF-LTR-GROSS-AMT ( #I ) '/Percent' FORM-R.TIRF-LTR-PER ( #I ) ( EM = ZZ9.9999 )
                		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
                		ldaTwrl9715.getForm_R_Tirf_Contract_Typ().getValue(pnd_Ws_Pnd_I),"Orig DA/Contract",
                		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_I),"Orig Cont/Type",
                		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ().getValue(pnd_Ws_Pnd_I),"/Plan",
                		ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I),"Plan/Name",
                		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
                		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
                		pnd_Employee_Desc,"Plan/Income",
                		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I),"/Percent",
                		ldaTwrl9715.getForm_R_Tirf_Ltr_Per().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("ZZ9.9999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      'Fed Tax/Withheld' FORM-R.TIRF-LTR-FED-TAX-WTHLD(#I) 2X
                //*      'State Tax/Withheld' FORM-R.TIRF-LTR-STATE-TAX-WTHLD(#I) 2X
                //*      'Local Tax/Withheld' FORM-R.TIRF-LTR-LOC-TAX-WTHLD(#I)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ADD #OUTW-NY-GROSS-WAGE   TO  #TOT-NY-GROSS
        pnd_Tax_Tape_Totals_Pnd_Tot_Ny_Gross.nadd(ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                     //Natural: ADD FORM-R.TIRF-LTR-GROSS-AMT ( #I ) TO #TOT-NY-GROSS
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3619|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"SUNY DTF file creation parameters:",NEWLINE,new TabSetting(26),"Tax Year...........:",      //Natural: WRITE ( 1 ) 7T 'SUNY DTF file creation parameters:' / 26T 'Tax Year...........:' #WS.#TAX-YEAR
                    pnd_Ws_Pnd_Tax_Year);
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #DATX := *DATX
                pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                               //Natural: ASSIGN #TIMX := *TIMX
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(false);                                                                                              //Natural: ASSIGN #TWRATBL4.#ABEND-IND := #TWRATBL4.#DISPLAY-IND := FALSE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(false);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(10);                                                                                                  //Natural: ASSIGN #TWRATBL4.#FORM-IND := 10
                //*  MOVE '35'                          TO SUBSTR(#TWRATBL4.#REC-TYPE,5,2)
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
                sub_Read_Control();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  DID FORM ROLL-UP RUN?
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_First_Time_Forms().getBoolean()))                                                                   //Natural: IF #TWRATBL4.#TIRCNTL-FIRST-TIME-FORMS
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"SUNY  Form Rollup has not been run",new TabSetting(77),"***");                   //Natural: WRITE ( 1 ) '***' 25T 'SUNY  Form Rollup has not been run' 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(102);  if (true) return;                                                                                                            //Natural: TERMINATE 102
                }                                                                                                                                                         //Natural: END-IF
                //*  DID DTF RUN ALREADY?
                if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                                                                           //Natural: IF #TWRATBL4.#TIRCNTL-IRS-ORIG
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"DTF file creation run already occurred",new TabSetting(77),"***",                //Natural: WRITE ( 1 ) '***' 25T 'DTF file creation run already occurred' 77T '***' /
                        NEWLINE);
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(103);  if (true) return;                                                                                                            //Natural: TERMINATE 103
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(26),"STATE..............: ",pnd_State_Table_Tircntl_State_Alpha_Code,"-",pnd_State_Table_Tircntl_State_Full_Name,NEWLINE,new  //Natural: WRITE ( 1 ) / 26T 'STATE..............: ' #STATE-TABLE.TIRCNTL-STATE-ALPHA-CODE '-' #STATE-TABLE.TIRCNTL-STATE-FULL-NAME / 26T 'TIAA STATE CODE....: ' #STATE-TABLE.TIRCNTL-STATE-OLD-CODE / 26T 'STATE IND..........: ' TWRATBL2.TIRCNTL-STATE-IND / 26T 'CMB IND............: ' #HOLD-COMB-FED-IND / 26T 'MEDIA IND..........: ' TWRATBL2.TIRCNTL-CORR-MEDIA-CODE
                    TabSetting(26),"TIAA STATE CODE....: ",pnd_State_Table_Tircntl_State_Old_Code,NEWLINE,new TabSetting(26),"STATE IND..........: ",pdaTwratbl2.getTwratbl2_Tircntl_State_Ind(),NEWLINE,new 
                    TabSetting(26),"CMB IND............: ",pnd_Hold_Fields_Pnd_Hold_Comb_Fed_Ind,NEWLINE,new TabSetting(26),"MEDIA IND..........: ",pdaTwratbl2.getTwratbl2_Tircntl_Corr_Media_Code());
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(16),"SYSTEM DATE USED FOR THIS RUN:",pnd_Ws_Pnd_Datx, new ReportEditMask                //Natural: WRITE ( 1 ) / 16T 'SYSTEM DATE USED FOR THIS RUN:' #WS.#DATX ( EM = MM/DD/YYYY ) / 16T 'SYSTEM TIME USED FOR THIS RUN:' #WS.#TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    ("MM/DD/YYYY"),NEWLINE,new TabSetting(16),"SYSTEM TIME USED FOR THIS RUN:",pnd_Ws_Pnd_Timx, new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
                pdaTwraplr2.getTwraplr2_Pnd_Function().setValue(1);                                                                                                       //Natural: ASSIGN TWRAPLR2.#FUNCTION := TWRACLR2.#FUNCTION := 1
                pdaTwraclr2.getTwraclr2_Pnd_Function().setValue(1);
                pdaTwraplr2.getTwraplr2_Pnd_Act_Ind().setValue("A");                                                                                                      //Natural: ASSIGN TWRAPLR2.#ACT-IND := TWRACLR2.#ACT-IND := 'A'
                pdaTwraclr2.getTwraclr2_Pnd_Act_Ind().setValue("A");
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  WRITE '=' #TWRATBL4.#INPUT-PARMS
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  EINCHG
        //*  EINCHG
        if (condition(pnd_Ws_Pnd_Tax_Year.greaterOrEqual(2018)))                                                                                                          //Natural: IF #WS.#TAX-YEAR GE 2018
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("A");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'A'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue("T");                                                                                                    //Natural: ASSIGN #TWRACOM2.#COMP-CODE := 'T'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        pnd_Daten.setValue(Global.getDATN());                                                                                                                             //Natural: ASSIGN #DATEN := *DATN
    }
    private void sub_Get_Plan_Data() throws Exception                                                                                                                     //Natural: GET-PLAN-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().setValue(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I));                                                        //Natural: ASSIGN #PLAN-NUM := FORM-R.TIRF-PLAN ( #I )
        DbsUtil.callnat(Twrnplr2.class , getCurrentProcessState(), pdaTwraplr2.getTwraplr2_Input_Parms(), new AttributeParameter("O"), pdaTwraplr2.getTwraplr2_Output_Data(),  //Natural: CALLNAT 'TWRNPLR2' USING TWRAPLR2.INPUT-PARMS ( AD = O ) TWRAPLR2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Client_Data() throws Exception                                                                                                                   //Natural: GET-CLIENT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.examine(new ExamineSource(pnd_Client_Ids.getValue("*"),true), new ExamineSearch(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I),                 //Natural: EXAMINE FULL #CLIENT-IDS ( * ) FOR FULL FORM-R.TIRF-PLAN ( #I ) GIVING NUMBER IN #NUM INDEX IN #INDEX
            true), new ExamineGivingNumber(pnd_Ws_Pnd_Num), new ExamineGivingIndex(pnd_Ws_Pnd_Index));
        if (condition(pnd_Ws_Pnd_Num.greater(getZero())))                                                                                                                 //Natural: IF #NUM > 0
        {
            getReports().write(0, "SPECIAL CLIENT ID ",ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I));                                                         //Natural: WRITE 'SPECIAL CLIENT ID ' FORM-R.TIRF-PLAN ( #I )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwraclr2.getTwraclr2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRACLR2.#FUNCTION := '1'
        pdaTwraclr2.getTwraclr2_Pnd_Act_Ind().setValue("A");                                                                                                              //Natural: ASSIGN TWRACLR2.#ACT-IND := 'A'
        pdaTwraclr2.getTwraclr2_Pnd_Client_Id().setValue(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I));                                                       //Natural: ASSIGN TWRACLR2.#CLIENT-ID := FORM-R.TIRF-PLAN ( #I )
        DbsUtil.callnat(Twrnclr2.class , getCurrentProcessState(), pdaTwraclr2.getTwraclr2_Input_Parms(), new AttributeParameter("O"), pdaTwraclr2.getTwraclr2_Output_Data(),  //Natural: CALLNAT 'TWRNCLR2' USING TWRACLR2.INPUT-PARMS ( AD = O ) TWRACLR2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Irs_Status() throws Exception                                                                                                                 //Natural: UPDATE-IRS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET FORM-U *ISN ( RD-1. )
        vw_form_U.readByID(ldaTwrl9715.getVw_form_R().getAstISN("RD_1"), "G1");
        if (condition(pnd_Extract_Reject_Ind.notEquals("Y")))                                                                                                             //Natural: IF #EXTRACT-REJECT-IND NE 'Y'
        {
            form_U_Tirf_Irs_Rpt_Ind.setValue("O");                                                                                                                        //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'O'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            form_U_Tirf_Irs_Rpt_Ind.setValue("H");                                                                                                                        //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-IND := 'H'
        }                                                                                                                                                                 //Natural: END-IF
        form_U_Tirf_Irs_Rpt_Date.setValue(pnd_Ws_Pnd_Datx);                                                                                                               //Natural: ASSIGN FORM-U.TIRF-IRS-RPT-DATE := #DATX
        form_U_Tirf_Lu_User.setValue(Global.getINIT_PROGRAM());                                                                                                           //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-PROGRAM
        form_U_Tirf_Lu_Ts.setValue(pnd_Ws_Pnd_Timx);                                                                                                                      //Natural: ASSIGN FORM-U.TIRF-LU-TS := #TIMX
        vw_form_U.updateDBRow("G1");                                                                                                                                      //Natural: UPDATE ( G1. )
        //*                                 COMMIT EVERY 100TH RECORD TO DB
        pnd_Et_Count.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
        if (condition(pnd_Et_Count.greaterOrEqual(pnd_Et_Limit)))                                                                                                         //Natural: IF #ET-COUNT GE #ET-LIMIT
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Count.reset();                                                                                                                                         //Natural: RESET #ET-COUNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cuny_Suny_Check() throws Exception                                                                                                                   //Natural: CUNY-SUNY-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        setValueToSubstring(pnd_Ws_Pnd_Tax_Year,pnd_Super2_Start,1,4);                                                                                                    //Natural: MOVE #WS.#TAX-YEAR TO SUBSTR ( #SUPER2-START,1,4 )
        setValueToSubstring("A",pnd_Super2_Start,5,1);                                                                                                                    //Natural: MOVE 'A' TO SUBSTR ( #SUPER2-START,5,1 )
        setValueToSubstring(ldaTwrl9715.getForm_R_Tirf_Tin(),pnd_Super2_Start,6,10);                                                                                      //Natural: MOVE FORM-R.TIRF-TIN TO SUBSTR ( #SUPER2-START,6,10 )
        vw_form_U.startDatabaseRead                                                                                                                                       //Natural: READ FORM-U BY TIRF-SUPERDE-2 = #SUPER2-START
        (
        "READ04",
        new Wc[] { new Wc("TIRF_SUPERDE_2", ">=", pnd_Super2_Start, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_2", "ASC") }
        );
        READ04:
        while (condition(vw_form_U.readNextRow("READ04")))
        {
            if (condition(form_U_Tirf_Tax_Year.notEquals(pnd_Ws_Pnd_Tax_Year_A) || form_U_Tirf_Tin.notEquals(ldaTwrl9715.getForm_R_Tirf_Tin())))                          //Natural: IF FORM-U.TIRF-TAX-YEAR NE #WS.#TAX-YEAR-A OR FORM-U.TIRF-TIN NE FORM-R.TIRF-TIN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(form_U_Tirf_Contract_Nbr.equals(" ") || form_U_Tirf_Irs_Reject_Ind.equals("Y")))                                                                //Natural: IF FORM-U.TIRF-CONTRACT-NBR = ' ' OR FORM-U.TIRF-IRS-REJECT-IND = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO C*FORM-U.TIRF-LTR-GRP
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(form_U_Count_Casttirf_Ltr_Grp)); pnd_Ws_Pnd_I.nadd(1))
            {
                //*  SHOULD ONLY BE NEEDED FOR 2012
                                                                                                                                                                          //Natural: PERFORM CORRECT-CLIENT-ID2
                sub_Correct_Client_Id2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                //*     WRITE TIRF-TIN '=' FORM-U.TIRF-PLAN-TYPE (#I)
                if (condition(form_U_Tirf_Plan_Type.getValue(pnd_Ws_Pnd_I).equals("P")))                                                                                  //Natural: IF FORM-U.TIRF-PLAN-TYPE ( #I ) = 'P'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Cuny_Suny_Plan.getValue("*"),true), new ExamineSearch(form_U_Tirf_Plan.getValue(pnd_Ws_Pnd_I),                  //Natural: EXAMINE FULL #CUNY-SUNY-PLAN ( * ) FOR FULL FORM-U.TIRF-PLAN ( #I ) GIVING NUMBER IN #NUM INDEX IN #INDEX
                        true), new ExamineGivingNumber(pnd_Ws_Pnd_Num), new ExamineGivingIndex(pnd_Ws_Pnd_Index));
                    //*       WRITE FORM-U.TIRF-PLAN     (#I) '=' #NUM '=' #INDEX
                    if (condition(pnd_Ws_Pnd_Num.equals(getZero())))                                                                                                      //Natural: IF #NUM = 0
                    {
                        pnd_Plan_Type.setValue("Z");                                                                                                                      //Natural: ASSIGN #PLAN-TYPE := 'Z'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ws_Pnd_Index.less(6)))                                                                                                          //Natural: IF #INDEX < 6
                        {
                            pnd_Print_Suny.setValue("Y");                                                                                                                 //Natural: ASSIGN #PRINT-SUNY := 'Y'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Print_Cuny.setValue("Y");                                                                                                                 //Natural: ASSIGN #PRINT-CUNY := 'Y'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Print_Suny.equals("Y") && pnd_Print_Cuny.equals("Y")))                                                                                      //Natural: IF #PRINT-SUNY = 'Y' AND #PRINT-CUNY = 'Y'
            {
                pnd_Print_Both.setValue("Y");                                                                                                                             //Natural: ASSIGN #PRINT-BOTH := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*   WRITE '=' TIRF-TIN '='  #PRINT-SUNY '=' #PRINT-CUNY
            //*     '=' #PRINT-BOTH
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Wrap_Name() throws Exception                                                                                                                         //Natural: WRAP-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        if (condition(!ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1().getSubstring(32,1).equals(" ") && !ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2().getSubstring(1, //Natural: IF SUBSTRING ( #OUTW-NY-PLAN-NAME1,32,1 ) NE ' ' AND SUBSTRING ( #OUTW-NY-PLAN-NAME2,1,1 ) NE ' '
            1).equals(" ")))
        {
            pnd_Ws_Pnd_I1.setValue(32);                                                                                                                                   //Natural: ASSIGN #I1 := 32
            pnd_Ws_Pnd_L1.setValue(1);                                                                                                                                    //Natural: ASSIGN #L1 := 1
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name1_A().getValue(pnd_Ws_Pnd_I1).equals(" "))) {break;}                                               //Natural: UNTIL #NAME1-A ( #I1 ) = ' '
                pnd_Ws_Pnd_I1.nsubtract(1);                                                                                                                               //Natural: ASSIGN #I1 := #I1 -1
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*   WRITE '=' #I
            pnd_Ws_Pnd_I1.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I1
            REPEAT02:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Ws_Pnd_I1.greater(32))) {break;}                                                                                                        //Natural: UNTIL #I1 > 32
                getReports().write(0, "=",pnd_Ws_Pnd_J1,pnd_Ws_Pnd_K1);                                                                                                   //Natural: WRITE '=' #J1 #K1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_J1.setValue(31);                                                                                                                               //Natural: ASSIGN #J1 := 31
                pnd_Ws_Pnd_K1.setValue(32);                                                                                                                               //Natural: ASSIGN #K1 := 32
                REPEAT03:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pnd_Ws_Pnd_J1.equals(pnd_Ws_Pnd_L1.subtract(1)))) {break;}                                                                              //Natural: UNTIL #J1 = #L1 - 1
                    ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name2_A().getValue(pnd_Ws_Pnd_K1).setValue(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name2_A().getValue(pnd_Ws_Pnd_J1)); //Natural: MOVE #NAME2-A ( #J1 ) TO #NAME2-A ( #K1 )
                    pnd_Ws_Pnd_J1.nsubtract(1);                                                                                                                           //Natural: SUBTRACT 1 FROM #J1
                    pnd_Ws_Pnd_K1.nsubtract(1);                                                                                                                           //Natural: SUBTRACT 1 FROM #K1
                }                                                                                                                                                         //Natural: END-REPEAT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name2_A().getValue(pnd_Ws_Pnd_L1).setValue(ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name1_A().getValue(pnd_Ws_Pnd_I1));  //Natural: MOVE #NAME1-A ( #I1 ) TO #NAME2-A ( #L1 )
                ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Name1_A().getValue(pnd_Ws_Pnd_I1).setValue(" ");                                                                     //Natural: MOVE ' ' TO #NAME1-A ( #I1 )
                pnd_Ws_Pnd_I1.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #I1
                pnd_Ws_Pnd_L1.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #L1
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            //*   WRITE #LINE1
            //*   WRITE #LINE2
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Correct_Client_Id() throws Exception                                                                                                                 //Natural: CORRECT-CLIENT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Key_Pnd_Key_Tin.setValue(ldaTwrl9715.getForm_R_Tirf_Tin());                                                                                                   //Natural: MOVE FORM-R.TIRF-TIN TO #KEY-TIN
        pnd_Key_Pnd_Key_Cl.setValue(ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I));                                                                            //Natural: MOVE FORM-R.TIRF-PLAN ( #I ) TO #KEY-CL
        DbsUtil.examine(new ExamineSource(pnd_Client_Table_Pnd_Ct_Key.getValue("*")), new ExamineSearch(pnd_Key), new ExamineGivingNumber(pnd_Num_X),                     //Natural: EXAMINE #CT-KEY ( * ) FOR #KEY GIVING NUMBER IN #NUM-X INDEX IN #IDX-X
            new ExamineGivingIndex(pnd_Idx_X));
        getReports().write(0, "FORM-R",pnd_Key,pnd_Num_X);                                                                                                                //Natural: WRITE 'FORM-R' #KEY #NUM-X
        if (Global.isEscape()) return;
        if (condition(pnd_Num_X.greater(getZero())))                                                                                                                      //Natural: IF #NUM-X > 0
        {
            //*   WRITE '=' #KEY
            ldaTwrl9715.getForm_R_Tirf_Plan().getValue(pnd_Ws_Pnd_I).setValue(pnd_Client_Table_Pnd_Ct_Fix_Cl.getValue(pnd_Idx_X));                                        //Natural: MOVE #CT-FIX-CL ( #IDX-X ) TO FORM-R.TIRF-PLAN ( #I )
            if (condition(pnd_Client_Table_Pnd_Ct_Fix_Cl.getValue(pnd_Idx_X).getSubstring(1,1).equals("1")))                                                              //Natural: IF SUBSTRING ( #CT-FIX-CL ( #IDX-X ) ,1,1 ) = '1'
            {
                ldaTwrl9715.getForm_R_Tirf_Plan_Type().getValue(pnd_Ws_Pnd_I).setValue("P");                                                                              //Natural: MOVE 'P' TO FORM-R.TIRF-PLAN-TYPE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Correct_Client_Id2() throws Exception                                                                                                                //Natural: CORRECT-CLIENT-ID2
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Key_Pnd_Key_Tin.setValue(form_U_Tirf_Tin);                                                                                                                    //Natural: MOVE FORM-U.TIRF-TIN TO #KEY-TIN
        pnd_Key_Pnd_Key_Cl.setValue(form_U_Tirf_Plan.getValue(pnd_Ws_Pnd_I));                                                                                             //Natural: MOVE FORM-U.TIRF-PLAN ( #I ) TO #KEY-CL
        DbsUtil.examine(new ExamineSource(pnd_Client_Table_Pnd_Ct_Key.getValue("*")), new ExamineSearch(pnd_Key), new ExamineGivingNumber(pnd_Num_X),                     //Natural: EXAMINE #CT-KEY ( * ) FOR #KEY GIVING NUMBER IN #NUM-X INDEX IN #IDX-X
            new ExamineGivingIndex(pnd_Idx_X));
        getReports().write(0, "FORM-U",pnd_Key,pnd_Num_X);                                                                                                                //Natural: WRITE 'FORM-U' #KEY #NUM-X
        if (Global.isEscape()) return;
        if (condition(pnd_Num_X.greater(getZero())))                                                                                                                      //Natural: IF #NUM-X > 0
        {
            //*   WRITE '=' #KEY
            form_U_Tirf_Plan.getValue(pnd_Ws_Pnd_I).setValue(pnd_Client_Table_Pnd_Ct_Fix_Cl.getValue(pnd_Idx_X));                                                         //Natural: MOVE #CT-FIX-CL ( #IDX-X ) TO FORM-U.TIRF-PLAN ( #I )
            if (condition(pnd_Client_Table_Pnd_Ct_Fix_Cl.getValue(pnd_Idx_X).getSubstring(1,1).equals("1")))                                                              //Natural: IF SUBSTRING ( #CT-FIX-CL ( #IDX-X ) ,1,1 ) = '1'
            {
                form_U_Tirf_Plan_Type.getValue(pnd_Ws_Pnd_I).setValue("P");                                                                                               //Natural: MOVE 'P' TO FORM-U.TIRF-PLAN-TYPE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Client_Table() throws Exception                                                                                                                 //Natural: LOAD-CLIENT-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Client_Table_Pnd_Ct_Data.getValue(1).setValue("005974004307656009836");                                                                                       //Natural: ASSIGN #CT-DATA ( 001 ) := '005974004307656009836'
        pnd_Client_Table_Pnd_Ct_Data.getValue(2).setValue("001619016304035009847");                                                                                       //Natural: ASSIGN #CT-DATA ( 002 ) := '001619016304035009847'
        pnd_Client_Table_Pnd_Ct_Data.getValue(3).setValue("009000019348947011402");                                                                                       //Natural: ASSIGN #CT-DATA ( 003 ) := '009000019348947011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(4).setValue("004561022247261151168");                                                                                       //Natural: ASSIGN #CT-DATA ( 004 ) := '004561022247261151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(5).setValue("006760022247261151166");                                                                                       //Natural: ASSIGN #CT-DATA ( 005 ) := '006760022247261151166'
        pnd_Client_Table_Pnd_Ct_Data.getValue(6).setValue("003744026124431011113");                                                                                       //Natural: ASSIGN #CT-DATA ( 006 ) := '003744026124431011113'
        pnd_Client_Table_Pnd_Ct_Data.getValue(7).setValue("008599029326530151168");                                                                                       //Natural: ASSIGN #CT-DATA ( 007 ) := '008599029326530151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(8).setValue("013614032264397011113");                                                                                       //Natural: ASSIGN #CT-DATA ( 008 ) := '013614032264397011113'
        pnd_Client_Table_Pnd_Ct_Data.getValue(9).setValue("050506032264397010157");                                                                                       //Natural: ASSIGN #CT-DATA ( 009 ) := '050506032264397010157'
        pnd_Client_Table_Pnd_Ct_Data.getValue(10).setValue("003763034281410010229");                                                                                      //Natural: ASSIGN #CT-DATA ( 010 ) := '003763034281410010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(11).setValue("008344044268160002502");                                                                                      //Natural: ASSIGN #CT-DATA ( 011 ) := '008344044268160002502'
        pnd_Client_Table_Pnd_Ct_Data.getValue(12).setValue("051904044268160011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 012 ) := '051904044268160011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(13).setValue("010229044488588011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 013 ) := '010229044488588011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(14).setValue("051156050340429151170");                                                                                      //Natural: ASSIGN #CT-DATA ( 014 ) := '051156050340429151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(15).setValue("001736054229586009845");                                                                                      //Natural: ASSIGN #CT-DATA ( 015 ) := '001736054229586009845'
        pnd_Client_Table_Pnd_Ct_Data.getValue(16).setValue("009000054248875011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 016 ) := '009000054248875011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(17).setValue("009000056208999011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 017 ) := '009000056208999011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(18).setValue("009000057229365011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 018 ) := '009000057229365011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(19).setValue("010229058261510011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 019 ) := '010229058261510011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(20).setValue("009000058325097011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 020 ) := '009000058325097011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(21).setValue("009001059227431011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 021 ) := '009001059227431011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(22).setValue("009000059244991011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 022 ) := '009000059244991011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(23).setValue("009000059262945011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 023 ) := '009000059262945011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(24).setValue("009009059382555011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 024 ) := '009009059382555011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(25).setValue("052467060245083151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 025 ) := '052467060245083151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(26).setValue("000610060302992002543");                                                                                      //Natural: ASSIGN #CT-DATA ( 026 ) := '000610060302992002543'
        pnd_Client_Table_Pnd_Ct_Data.getValue(27).setValue("010229060345304011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 027 ) := '010229060345304011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(28).setValue("012066060463731151170");                                                                                      //Natural: ASSIGN #CT-DATA ( 028 ) := '012066060463731151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(29).setValue("000648061207119011133");                                                                                      //Natural: ASSIGN #CT-DATA ( 029 ) := '000648061207119011133'
        pnd_Client_Table_Pnd_Ct_Data.getValue(30).setValue("008120061207119011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 030 ) := '008120061207119011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(31).setValue("008120061329799011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 031 ) := '008120061329799011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(32).setValue("010229062148674011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 032 ) := '010229062148674011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(33).setValue("001573062308300013469");                                                                                      //Natural: ASSIGN #CT-DATA ( 033 ) := '001573062308300013469'
        pnd_Client_Table_Pnd_Ct_Data.getValue(34).setValue("008120063362696011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 034 ) := '008120063362696011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(35).setValue("010229064208207011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 035 ) := '010229064208207011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(36).setValue("007155066240881002493");                                                                                      //Natural: ASSIGN #CT-DATA ( 036 ) := '007155066240881002493'
        pnd_Client_Table_Pnd_Ct_Data.getValue(37).setValue("009000066302056011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 037 ) := '009000066302056011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(38).setValue("009000067225878011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 038 ) := '009000067225878011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(39).setValue("009001067227227011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 039 ) := '009001067227227011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(40).setValue("053618068181011151170");                                                                                      //Natural: ASSIGN #CT-DATA ( 040 ) := '053618068181011151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(41).setValue("009000068184016011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 041 ) := '009000068184016011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(42).setValue("010229068204655011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 042 ) := '010229068204655011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(43).setValue("008120069189720011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 043 ) := '008120069189720011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(44).setValue("010229069189720011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 044 ) := '010229069189720011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(45).setValue("009002069385528011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 045 ) := '009002069385528011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(46).setValue("009000071348287011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 046 ) := '009000071348287011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(47).setValue("009000071485958011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 047 ) := '009000071485958011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(48).setValue("009000073149033011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 048 ) := '009000073149033011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(49).setValue("009002073308899011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 049 ) := '009002073308899011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(50).setValue("008300075289410010018");                                                                                      //Natural: ASSIGN #CT-DATA ( 050 ) := '008300075289410010018'
        pnd_Client_Table_Pnd_Ct_Data.getValue(51).setValue("003603076269742003603");                                                                                      //Natural: ASSIGN #CT-DATA ( 051 ) := '003603076269742003603'
        pnd_Client_Table_Pnd_Ct_Data.getValue(52).setValue("009009076328631011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 052 ) := '009009076328631011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(53).setValue("010074077468455011119");                                                                                      //Natural: ASSIGN #CT-DATA ( 053 ) := '010074077468455011119'
        pnd_Client_Table_Pnd_Ct_Data.getValue(54).setValue("010901077468455011119");                                                                                      //Natural: ASSIGN #CT-DATA ( 054 ) := '010901077468455011119'
        pnd_Client_Table_Pnd_Ct_Data.getValue(55).setValue("010068078325705151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 055 ) := '010068078325705151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(56).setValue("008120079184452011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 056 ) := '008120079184452011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(57).setValue("009001080245338011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 057 ) := '009001080245338011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(58).setValue("010229080260206011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 058 ) := '010229080260206011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(59).setValue("009001080309875011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 059 ) := '009001080309875011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(60).setValue("009001081204775011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 060 ) := '009001081204775011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(61).setValue("009000081224790011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 061 ) := '009000081224790011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(62).setValue("009001081306413011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 062 ) := '009001081306413011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(63).setValue("008120083249018011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 063 ) := '008120083249018011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(64).setValue("013755088147298151166");                                                                                      //Natural: ASSIGN #CT-DATA ( 064 ) := '013755088147298151166'
        pnd_Client_Table_Pnd_Ct_Data.getValue(65).setValue("006760089181191151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 065 ) := '006760089181191151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(66).setValue("009002090180816011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 066 ) := '009002090180816011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(67).setValue("003763090220549010229");                                                                                      //Natural: ASSIGN #CT-DATA ( 067 ) := '003763090220549010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(68).setValue("004449092242927151166");                                                                                      //Natural: ASSIGN #CT-DATA ( 068 ) := '004449092242927151166'
        pnd_Client_Table_Pnd_Ct_Data.getValue(69).setValue("009003092248791011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 069 ) := '009003092248791011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(70).setValue("009000093200377011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 070 ) := '009000093200377011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(71).setValue("009001093200377011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 071 ) := '009001093200377011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(72).setValue("007383093249198151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 072 ) := '007383093249198151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(73).setValue("009009094322884011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 073 ) := '009009094322884011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(74).setValue("009259096141211008353");                                                                                      //Natural: ASSIGN #CT-DATA ( 074 ) := '009259096141211008353'
        pnd_Client_Table_Pnd_Ct_Data.getValue(75).setValue("006066096602170151169");                                                                                      //Natural: ASSIGN #CT-DATA ( 075 ) := '006066096602170151169'
        pnd_Client_Table_Pnd_Ct_Data.getValue(76).setValue("011351096602170151170");                                                                                      //Natural: ASSIGN #CT-DATA ( 076 ) := '011351096602170151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(77).setValue("013906096602170151169");                                                                                      //Natural: ASSIGN #CT-DATA ( 077 ) := '013906096602170151169'
        pnd_Client_Table_Pnd_Ct_Data.getValue(78).setValue("151168096602170151170");                                                                                      //Natural: ASSIGN #CT-DATA ( 078 ) := '151168096602170151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(79).setValue("009000097260669011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 079 ) := '009000097260669011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(80).setValue("008306099208521151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 080 ) := '008306099208521151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(81).setValue("009870099302216010250");                                                                                      //Natural: ASSIGN #CT-DATA ( 081 ) := '009870099302216010250'
        pnd_Client_Table_Pnd_Ct_Data.getValue(82).setValue("009999103245978151166");                                                                                      //Natural: ASSIGN #CT-DATA ( 082 ) := '009999103245978151166'
        pnd_Client_Table_Pnd_Ct_Data.getValue(83).setValue("009000104248760011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 083 ) := '009000104248760011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(84).setValue("009000105168296011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 084 ) := '009000105168296011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(85).setValue("008120105201162011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 085 ) := '008120105201162011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(86).setValue("002416105442685151168");                                                                                      //Natural: ASSIGN #CT-DATA ( 086 ) := '002416105442685151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(88).setValue("007183107166741011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 088 ) := '007183107166741011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(89).setValue("007184107166741011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 089 ) := '007184107166741011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(90).setValue("009856107166741009855");                                                                                      //Natural: ASSIGN #CT-DATA ( 090 ) := '009856107166741009855'
        pnd_Client_Table_Pnd_Ct_Data.getValue(91).setValue("010017107166741011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 091 ) := '010017107166741011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(92).setValue("005971108307338011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 092 ) := '005971108307338011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(93).setValue("009009112286317011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 093 ) := '009009112286317011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(94).setValue("003672114340576001478");                                                                                      //Natural: ASSIGN #CT-DATA ( 094 ) := '003672114340576001478'
        pnd_Client_Table_Pnd_Ct_Data.getValue(95).setValue("009000114340576011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 095 ) := '009000114340576011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(96).setValue("009002115167184011402");                                                                                      //Natural: ASSIGN #CT-DATA ( 096 ) := '009002115167184011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(97).setValue("008120117303871011351");                                                                                      //Natural: ASSIGN #CT-DATA ( 097 ) := '008120117303871011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(98).setValue("002496117305237151169");                                                                                      //Natural: ASSIGN #CT-DATA ( 098 ) := '002496117305237151169'
        pnd_Client_Table_Pnd_Ct_Data.getValue(99).setValue("009306117305237151169");                                                                                      //Natural: ASSIGN #CT-DATA ( 099 ) := '009306117305237151169'
        pnd_Client_Table_Pnd_Ct_Data.getValue(100).setValue("007222118163619151170");                                                                                     //Natural: ASSIGN #CT-DATA ( 100 ) := '007222118163619151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(101).setValue("009000118163619151170");                                                                                     //Natural: ASSIGN #CT-DATA ( 101 ) := '009000118163619151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(101).setValue("005400118404046009987");                                                                                     //Natural: ASSIGN #CT-DATA ( 101 ) := '005400118404046009987'
        pnd_Client_Table_Pnd_Ct_Data.getValue(102).setValue("003763121482939010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 102 ) := '003763121482939010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(103).setValue("003769121482939010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 103 ) := '003769121482939010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(104).setValue("003763127262760010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 104 ) := '003763127262760010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(105).setValue("009009127266695011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 105 ) := '009009127266695011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(106).setValue("009719128267583151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 106 ) := '009719128267583151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(107).setValue("009009132288463011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 107 ) := '009009132288463011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(108).setValue("009000141019206011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 108 ) := '009000141019206011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(109).setValue("009687141019206011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 109 ) := '009687141019206011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(110).setValue("011084141247708002595");                                                                                     //Natural: ASSIGN #CT-DATA ( 110 ) := '011084141247708002595'
        pnd_Client_Table_Pnd_Ct_Data.getValue(111).setValue("009001144229037011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 111 ) := '009001144229037011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(112).setValue("009003144229037011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 112 ) := '009003144229037011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(113).setValue("000621145346178009855");                                                                                     //Natural: ASSIGN #CT-DATA ( 113 ) := '000621145346178009855'
        pnd_Client_Table_Pnd_Ct_Data.getValue(114).setValue("002503151223467151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 114 ) := '002503151223467151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(115).setValue("009000152226792011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 115 ) := '009000152226792011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(116).setValue("009879152226792151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 116 ) := '009879152226792151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(117).setValue("005967156303951011112");                                                                                     //Natural: ASSIGN #CT-DATA ( 117 ) := '005967156303951011112'
        pnd_Client_Table_Pnd_Ct_Data.getValue(118).setValue("003763188321184010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 118 ) := '003763188321184010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(119).setValue("009000200164094011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 119 ) := '009000200164094011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(120).setValue("009845200164094011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 120 ) := '009845200164094011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(121).setValue("009000207309845011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 121 ) := '009000207309845011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(122).setValue("009002210220672011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 122 ) := '009002210220672011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(123).setValue("009001221145042011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 123 ) := '009001221145042011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(124).setValue("009000239407437011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 124 ) := '009000239407437011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(125).setValue("005833241368421011351");                                                                                     //Natural: ASSIGN #CT-DATA ( 125 ) := '005833241368421011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(126).setValue("008300243548979010018");                                                                                     //Natural: ASSIGN #CT-DATA ( 126 ) := '008300243548979010018'
        pnd_Client_Table_Pnd_Ct_Data.getValue(127).setValue("002634260689983151170");                                                                                     //Natural: ASSIGN #CT-DATA ( 127 ) := '002634260689983151170'
        pnd_Client_Table_Pnd_Ct_Data.getValue(128).setValue("003600288281715005948");                                                                                     //Natural: ASSIGN #CT-DATA ( 128 ) := '003600288281715005948'
        pnd_Client_Table_Pnd_Ct_Data.getValue(129).setValue("008120297443898011351");                                                                                     //Natural: ASSIGN #CT-DATA ( 129 ) := '008120297443898011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(130).setValue("005451304307378151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 130 ) := '005451304307378151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(131).setValue("004510356223445003879");                                                                                     //Natural: ASSIGN #CT-DATA ( 131 ) := '004510356223445003879'
        pnd_Client_Table_Pnd_Ct_Data.getValue(132).setValue("007383369500142151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 132 ) := '007383369500142151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(133).setValue("003763372382203010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 133 ) := '003763372382203010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(134).setValue("009001374249746011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 134 ) := '009001374249746011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(135).setValue("009000385381833011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 135 ) := '009000385381833011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(136).setValue("009009409342947011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 136 ) := '009009409342947011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(137).setValue("009000417381407011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 137 ) := '009000417381407011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(138).setValue("009000430861252011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 138 ) := '009000430861252011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(139).setValue("009000436623202011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 139 ) := '009000436623202011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(140).setValue("009003496288090011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 140 ) := '009003496288090011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(141).setValue("009001502267225011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 141 ) := '009001502267225011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(142).setValue("009000513427686011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 142 ) := '009000513427686011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(143).setValue("009001513427686011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 143 ) := '009001513427686011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(144).setValue("000597523340768151168");                                                                                     //Natural: ASSIGN #CT-DATA ( 144 ) := '000597523340768151168'
        pnd_Client_Table_Pnd_Ct_Data.getValue(145).setValue("003850523340768009871");                                                                                     //Natural: ASSIGN #CT-DATA ( 145 ) := '003850523340768009871'
        pnd_Client_Table_Pnd_Ct_Data.getValue(146).setValue("010053523340768007270");                                                                                     //Natural: ASSIGN #CT-DATA ( 146 ) := '010053523340768007270'
        pnd_Client_Table_Pnd_Ct_Data.getValue(147).setValue("009002528302612011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 147 ) := '009002528302612011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(148).setValue("008120546537696011351");                                                                                     //Natural: ASSIGN #CT-DATA ( 148 ) := '008120546537696011351'
        pnd_Client_Table_Pnd_Ct_Data.getValue(149).setValue("009003546537696011402");                                                                                     //Natural: ASSIGN #CT-DATA ( 149 ) := '009003546537696011402'
        pnd_Client_Table_Pnd_Ct_Data.getValue(150).setValue("003763549724277010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 150 ) := '003763549724277010229'
        pnd_Client_Table_Pnd_Ct_Data.getValue(151).setValue("003763550587373010229");                                                                                     //Natural: ASSIGN #CT-DATA ( 151 ) := '003763550587373010229'
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133");
        Global.format(3, "PS=58 LS=133");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(60),"DTF File Report - Accepted Records",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year_A,
            NEWLINE,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(60),"DTF File Report - Rejected Records",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year_A,
            NEWLINE,NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(60),"DTF File Report - Summary Report",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year_A,
            NEWLINE,NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(60),"CUNY Contracts",new TabSetting(120),"REPORT: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year_A,NEWLINE,NEWLINE,
            NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(60),"SUNY Contracts",new TabSetting(120),"REPORT: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year_A,NEWLINE,NEWLINE,
            NEWLINE);

        getReports().setDisplayColumns(2, "/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Typ(),"Orig DA/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr(),"Orig Cont/Type",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ(),"/Plan",
        		ldaTwrl9715.getForm_R_Tirf_Plan(),"Plan/Name",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
        		pnd_Employee_Desc,"Plan/Income",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt(),"/Percent",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Per(), new ReportEditMask ("ZZ9.9999"));
        getReports().setDisplayColumns(5, "/TIN",
        		ldaTwrl9715.getForm_R_Tirf_Tin(),"/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Typ(),"Orig DA/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr(),"Orig Cont/Type",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ(),"/Plan",
        		ldaTwrl9715.getForm_R_Tirf_Plan(),"Plan/Name",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
        		pnd_Employee_Desc,"Plan/Income",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt(),"/Percent",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Per(), new ReportEditMask ("ZZ9.9999"));
        getReports().setDisplayColumns(6, "/TIN",
        		ldaTwrl9715.getForm_R_Tirf_Tin(),"/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Typ(),"Orig DA/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr(),"Orig Cont/Type",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ(),"/Plan",
        		ldaTwrl9715.getForm_R_Tirf_Plan(),"Plan/Name",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
        		pnd_Employee_Desc,"Plan/Income",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt(),"/Percent",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Per(), new ReportEditMask ("ZZ9.9999"));
        getReports().setDisplayColumns(3, "/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Nbr(),"Contract/Type",
        		ldaTwrl9715.getForm_R_Tirf_Contract_Typ(),"Orig DA/Contract",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Nbr(),"Orig Cont/Type",
        		ldaTwrl9715.getForm_R_Tirf_Orig_Contract_Typ(),"/Plan",
        		ldaTwrl9715.getForm_R_Tirf_Plan(),"Plan/Name",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1(),NEWLINE," ",
        		ldaTwrl3620.getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2(),"Employer/Type",
        		pnd_Employee_Desc,"Plan/Income",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Gross_Amt(),"/Percent",
        		ldaTwrl9715.getForm_R_Tirf_Ltr_Per(), new ReportEditMask ("ZZ9.9999"));
    }
}
