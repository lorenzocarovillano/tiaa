/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:36:10 PM
**        * FROM NATURAL PROGRAM : Twrp3536
************************************************************
**        * FILE NAME            : Twrp3536.java
**        * CLASS NAME           : Twrp3536
**        * INSTANCE NAME        : Twrp3536
************************************************************
************************************************************************
** PROGRAM : TWRP3536
** SYSTEM  : TAXWARS
** DATE    : 12/16/2002
** AUTHOR  : MARINA NACHBER
** FUNCTION: WRITES RECORD SEQUENCE NUMBER TO IRS CORRECTION RECORDS
** HISTORY.....:
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3536 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Irs_Record;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_1;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_2;
    private DbsField pnd_Irs_Record_Pnd_Rec_Seq_No;
    private DbsField pnd_Irs_Record_Pnd_Irs_Filler_3;
    private DbsField pnd_Rec_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Irs_Record = localVariables.newGroupInRecord("pnd_Irs_Record", "#IRS-RECORD");
        pnd_Irs_Record_Pnd_Irs_Filler_1 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_1", "#IRS-FILLER-1", FieldType.STRING, 250);
        pnd_Irs_Record_Pnd_Irs_Filler_2 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_2", "#IRS-FILLER-2", FieldType.STRING, 249);
        pnd_Irs_Record_Pnd_Rec_Seq_No = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Rec_Seq_No", "#REC-SEQ-NO", FieldType.NUMERIC, 8);
        pnd_Irs_Record_Pnd_Irs_Filler_3 = pnd_Irs_Record.newFieldInGroup("pnd_Irs_Record_Pnd_Irs_Filler_3", "#IRS-FILLER-3", FieldType.STRING, 243);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3536() throws Exception
    {
        super("Twrp3536");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #IRS-RECORD
        while (condition(getWorkFiles().read(1, pnd_Irs_Record)))
        {
            pnd_Rec_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #REC-COUNT
            //*  WRITE (0) '#rec-count ' #REC-COUNT
            pnd_Irs_Record_Pnd_Rec_Seq_No.setValue(pnd_Rec_Count);                                                                                                        //Natural: ASSIGN #REC-SEQ-NO := #REC-COUNT
            getWorkFiles().write(2, false, pnd_Irs_Record);                                                                                                               //Natural: WRITE WORK FILE 2 #IRS-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
    }
}
