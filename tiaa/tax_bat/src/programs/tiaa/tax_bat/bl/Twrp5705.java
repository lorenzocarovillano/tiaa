/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:41:40 PM
**        * FROM NATURAL PROGRAM : Twrp5705
************************************************************
**        * FILE NAME            : Twrp5705.java
**        * CLASS NAME           : Twrp5705
**        * INSTANCE NAME        : Twrp5705
************************************************************
************************************************************************
* PROGRAM : TWRP5705
* SYSTEM  : TAXWARS
* FUNCTION: BASED ON YEAR FOUND IN PARM CARD, PROCESSES W2 ORIGINAL FOR
*           TAX YEAR.  READS PAYMENT EXTRACT FILE CREATED BY PTW1071D.
*           AND MONTHLY REPORTS. WRITES W2 ORIGINAL FILE.
* HISTORY : 04/14/10  J.ROTHOLZ - MODIFIED OLD VERSION OF TWRP5705.
*           05/28/10  J.ROTHOLZ - TIMESTAMP ADDED TO TBL4 UPDATE.
*           01/03/11  J.ROTHOLZ - FIX TO REMOVE ZIP FROM ADDRESS LINES
*           02/16/11  J.ROTHOLZ - FIX TO CORRECT PARTICIPANT RESIDENCY
*           10/19/11  M.BERLIN  - CHANGED REFERENCES FROM FIELD
*                                 TWRPYMNT-COMPANY-CDE TO FIELD
*                                 TWRPYMNT-COMPANY-CDE-FORM     /* MB
* 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
* 06/10/15: FENDAYA COR AND NAS SUNSET. FE201506
* 10/05/18: ARIVU - EIN CHANGES. TAG - EINCHG
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5705 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaTwrl0900 ldaTwrl0900;
    private PdaMdma101 pdaMdma101;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwrafrm2 pdaTwrafrm2;
    private PdaTwrarsdc pdaTwrarsdc;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Break_Key;

    private DbsGroup pnd_Break_Key__R_Field_1;
    private DbsField pnd_Break_Key_Pnd_Twrpymnt_Tax_Year;
    private DbsField pnd_Break_Key_Pnd_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Break_Key_Pnd_Twrpymnt_Residency_Code;
    private DbsField pnd_Break_Key_Pnd_Twrpymnt_Company_Cde_Form;
    private DbsField pnd_Debug;
    private DbsField pnd_Init_Tax_Yr;

    private DbsGroup pnd_Init_Tax_Yr__R_Field_2;
    private DbsField pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N;
    private DbsField pnd_Employer_State_Id;

    private DbsGroup pnd_Employer_State_Id__R_Field_3;
    private DbsField pnd_Employer_State_Id_Pnd_State_Code;
    private DbsField pnd_Employer_State_Id_Pnd_State_Id;

    private DbsGroup pnd_Orig_Rec;
    private DbsField pnd_Orig_Rec_Pnd_Company_Code;
    private DbsField pnd_Orig_Rec_Pnd_Sep_00;
    private DbsField pnd_Orig_Rec_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Orig_Rec_Pnd_Sep_01;
    private DbsField pnd_Orig_Rec_Pnd_Name;

    private DbsGroup pnd_Orig_Rec__R_Field_4;
    private DbsField pnd_Orig_Rec_Pnd_Name_Pref;
    private DbsField pnd_Orig_Rec_Pnd_Name_Remains;
    private DbsField pnd_Orig_Rec_Pnd_Sep_02;
    private DbsField pnd_Orig_Rec_Pnd_Address_1st_Line;
    private DbsField pnd_Orig_Rec_Pnd_Sep_03;
    private DbsField pnd_Orig_Rec_Pnd_Address_2nd_Line;
    private DbsField pnd_Orig_Rec_Pnd_Sep_04;
    private DbsField pnd_Orig_Rec_Pnd_Address_3rd_Line;
    private DbsField pnd_Orig_Rec_Pnd_Sep_05;
    private DbsField pnd_Orig_Rec_Pnd_City;
    private DbsField pnd_Orig_Rec_Pnd_Sep_06;
    private DbsField pnd_Orig_Rec_Pnd_State;
    private DbsField pnd_Orig_Rec_Pnd_Sep_07;
    private DbsField pnd_Orig_Rec_Pnd_Zip_Code;
    private DbsField pnd_Orig_Rec_Pnd_Sep_08;
    private DbsField pnd_Orig_Rec_Pnd_Gross_Amount_Box_1;
    private DbsField pnd_Orig_Rec_Pnd_Sep_10;
    private DbsField pnd_Orig_Rec_Pnd_Federal_Tax_Box_2;
    private DbsField pnd_Orig_Rec_Pnd_Sep_11;
    private DbsField pnd_Orig_Rec_Pnd_Gross_Amount_Box_11;
    private DbsField pnd_Orig_Rec_Pnd_Sep_12;
    private DbsField pnd_Orig_Rec_Pnd_State_Id_Box_15;
    private DbsField pnd_Orig_Rec_Pnd_Sep_13;
    private DbsField pnd_Orig_Rec_Pnd_Gross_Amount_Box_16;
    private DbsField pnd_Orig_Rec_Pnd_Sep_14;
    private DbsField pnd_Orig_Rec_Pnd_State_Tax_Box_17;
    private DbsField pnd_Orig_Rec_Pnd_Sep_15;
    private DbsField pnd_Orig_Rec_Pnd_Filler;
    private DbsField pnd_Orig_Rec_Pnd_Sort_Field;

    private DbsGroup pnd_Hdr_Rec;
    private DbsField pnd_Hdr_Rec_Pnd_Company_Code;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_00;
    private DbsField pnd_Hdr_Rec_Pnd_Tax_Id_Nbr;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_01;
    private DbsField pnd_Hdr_Rec_Pnd_Name;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_02;
    private DbsField pnd_Hdr_Rec_Pnd_Address_1st_Line;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_03;
    private DbsField pnd_Hdr_Rec_Pnd_Address_2nd_Line;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_04;
    private DbsField pnd_Hdr_Rec_Pnd_Address_3rd_Line;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_05;
    private DbsField pnd_Hdr_Rec_Pnd_City;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_06;
    private DbsField pnd_Hdr_Rec_Pnd_State;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_07;
    private DbsField pnd_Hdr_Rec_Pnd_Zip_Code;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_08;
    private DbsField pnd_Hdr_Rec_Pnd_Gross_Amount_Box_1;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_10;
    private DbsField pnd_Hdr_Rec_Pnd_Federal_Tax_Box_2;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_11;
    private DbsField pnd_Hdr_Rec_Pnd_Gross_Amount_Box_11;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_12;
    private DbsField pnd_Hdr_Rec_Pnd_State_Id_Box_15;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_13;
    private DbsField pnd_Hdr_Rec_Pnd_Gross_Amount_Box_16;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_14;
    private DbsField pnd_Hdr_Rec_Pnd_State_Tax_Box_17;
    private DbsField pnd_Hdr_Rec_Pnd_Sep_15;
    private DbsField pnd_Hdr_Rec_Pnd_Filler;
    private DbsField pnd_Hdr_Rec_Pnd_Sort_Field;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Fed_Tax;
    private DbsField pnd_Sta_Tax;
    private DbsField pnd_H_Company;
    private DbsField pnd_H_Tax_Id;
    private DbsField pnd_H_Residency;
    private DbsField pnd_H_Citizen_Cde;
    private DbsField pnd_H_Tax_Citizenship;
    private DbsField pnd_H_Residency_Type;
    private DbsField pnd_H_Contract;
    private DbsField pnd_H_Gross_Amt;
    private DbsField pnd_H_Fed_Tax;
    private DbsField pnd_H_Sta_Tax;
    private DbsField pnd_Co;
    private DbsField pnd_Co_Ndx;
    private DbsField pnd_Co_Line;
    private DbsField pnd_Co_Lit;

    private DbsGroup pnd_Tot_Array;
    private DbsField pnd_Tot_Array_Pnd_Co_Name;
    private DbsField pnd_Tot_Array_Pnd_Tot_Pymnts;
    private DbsField pnd_Tot_Array_Pnd_Tot_Forms;
    private DbsField pnd_Tot_Array_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Tot_Array_Pnd_Tot_Fed_Tax;
    private DbsField pnd_Tot_Array_Pnd_Tot_State_Tax;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Temp_State_Id;
    private DbsField pnd_Op;
    private DbsField pnd_Ia;
    private DbsField pnd_Ml;
    private DbsField pnd_Nl;
    private DbsField pnd_Tot_Payments;
    private DbsField pnd_Source_Op;
    private DbsField pnd_Source_Ia;
    private DbsField pnd_Source_Nl;
    private DbsField pnd_Source_Ml;
    private DbsField pnd_Op_Contracts;
    private DbsField pnd_Ia_Contracts;
    private DbsField pnd_Ml_Contracts;
    private DbsField pnd_Nl_Contracts;
    private DbsField pnd_Opia_Cntrcts;
    private DbsField pnd_Orig_Written;
    private DbsField pnd_Ws_Tax_Year;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_5;
    private DbsField pnd_Key_Pnd_Tax_Year;

    private DbsGroup pnd_Key__R_Field_6;
    private DbsField pnd_Key_Pnd_Tax_Year_N;
    private DbsField pnd_Key_Pnd_Active_Ind;
    private DbsField pnd_Key_Pnd_Form_Type;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Datn;
    private DbsField pnd_Cntl_Ndx;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    private DbsRecord internalLoopRecord;
    private DbsField rD1Twrpymnt_Company_Cde_FormOld;
    private DbsField rD1Twrpymnt_Tax_Id_NbrOld;
    private DbsField rD1Twrpymnt_Residency_CodeOld;
    private DbsField rD1Twrpymnt_Citizen_CdeOld;
    private DbsField rD1Twrpymnt_Tax_CitizenshipOld;
    private DbsField rD1Twrpymnt_Residency_TypeOld;
    private DbsField rD1Twrpymnt_Contract_NbrOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwrafrm2 = new PdaTwrafrm2(localVariables);
        pdaTwrarsdc = new PdaTwrarsdc(localVariables);

        // Local Variables
        pnd_Break_Key = localVariables.newFieldInRecord("pnd_Break_Key", "#BREAK-KEY", FieldType.STRING, 27);

        pnd_Break_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Break_Key__R_Field_1", "REDEFINE", pnd_Break_Key);
        pnd_Break_Key_Pnd_Twrpymnt_Tax_Year = pnd_Break_Key__R_Field_1.newFieldInGroup("pnd_Break_Key_Pnd_Twrpymnt_Tax_Year", "#TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Break_Key_Pnd_Twrpymnt_Tax_Id_Nbr = pnd_Break_Key__R_Field_1.newFieldInGroup("pnd_Break_Key_Pnd_Twrpymnt_Tax_Id_Nbr", "#TWRPYMNT-TAX-ID-NBR", 
            FieldType.STRING, 10);
        pnd_Break_Key_Pnd_Twrpymnt_Residency_Code = pnd_Break_Key__R_Field_1.newFieldInGroup("pnd_Break_Key_Pnd_Twrpymnt_Residency_Code", "#TWRPYMNT-RESIDENCY-CODE", 
            FieldType.STRING, 2);
        pnd_Break_Key_Pnd_Twrpymnt_Company_Cde_Form = pnd_Break_Key__R_Field_1.newFieldInGroup("pnd_Break_Key_Pnd_Twrpymnt_Company_Cde_Form", "#TWRPYMNT-COMPANY-CDE-FORM", 
            FieldType.STRING, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Init_Tax_Yr = localVariables.newFieldInRecord("pnd_Init_Tax_Yr", "#INIT-TAX-YR", FieldType.STRING, 4);

        pnd_Init_Tax_Yr__R_Field_2 = localVariables.newGroupInRecord("pnd_Init_Tax_Yr__R_Field_2", "REDEFINE", pnd_Init_Tax_Yr);
        pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N = pnd_Init_Tax_Yr__R_Field_2.newFieldInGroup("pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N", "#INIT-TAX-YR-N", FieldType.NUMERIC, 
            4);
        pnd_Employer_State_Id = localVariables.newFieldInRecord("pnd_Employer_State_Id", "#EMPLOYER-STATE-ID", FieldType.STRING, 16);

        pnd_Employer_State_Id__R_Field_3 = localVariables.newGroupInRecord("pnd_Employer_State_Id__R_Field_3", "REDEFINE", pnd_Employer_State_Id);
        pnd_Employer_State_Id_Pnd_State_Code = pnd_Employer_State_Id__R_Field_3.newFieldInGroup("pnd_Employer_State_Id_Pnd_State_Code", "#STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Employer_State_Id_Pnd_State_Id = pnd_Employer_State_Id__R_Field_3.newFieldInGroup("pnd_Employer_State_Id_Pnd_State_Id", "#STATE-ID", FieldType.STRING, 
            14);

        pnd_Orig_Rec = localVariables.newGroupInRecord("pnd_Orig_Rec", "#ORIG-REC");
        pnd_Orig_Rec_Pnd_Company_Code = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Sep_00 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_00", "#SEP-00", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Tax_Id_Nbr = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 11);
        pnd_Orig_Rec_Pnd_Sep_01 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_01", "#SEP-01", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Name = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Name", "#NAME", FieldType.STRING, 35);

        pnd_Orig_Rec__R_Field_4 = pnd_Orig_Rec.newGroupInGroup("pnd_Orig_Rec__R_Field_4", "REDEFINE", pnd_Orig_Rec_Pnd_Name);
        pnd_Orig_Rec_Pnd_Name_Pref = pnd_Orig_Rec__R_Field_4.newFieldInGroup("pnd_Orig_Rec_Pnd_Name_Pref", "#NAME-PREF", FieldType.STRING, 3);
        pnd_Orig_Rec_Pnd_Name_Remains = pnd_Orig_Rec__R_Field_4.newFieldInGroup("pnd_Orig_Rec_Pnd_Name_Remains", "#NAME-REMAINS", FieldType.STRING, 32);
        pnd_Orig_Rec_Pnd_Sep_02 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_02", "#SEP-02", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Address_1st_Line = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Address_1st_Line", "#ADDRESS-1ST-LINE", FieldType.STRING, 35);
        pnd_Orig_Rec_Pnd_Sep_03 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_03", "#SEP-03", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Address_2nd_Line = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Address_2nd_Line", "#ADDRESS-2ND-LINE", FieldType.STRING, 35);
        pnd_Orig_Rec_Pnd_Sep_04 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_04", "#SEP-04", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Address_3rd_Line = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Address_3rd_Line", "#ADDRESS-3RD-LINE", FieldType.STRING, 35);
        pnd_Orig_Rec_Pnd_Sep_05 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_05", "#SEP-05", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_City = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_City", "#CITY", FieldType.STRING, 20);
        pnd_Orig_Rec_Pnd_Sep_06 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_06", "#SEP-06", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_State = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_State", "#STATE", FieldType.STRING, 4);
        pnd_Orig_Rec_Pnd_Sep_07 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_07", "#SEP-07", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Zip_Code = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Zip_Code", "#ZIP-CODE", FieldType.STRING, 10);
        pnd_Orig_Rec_Pnd_Sep_08 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_08", "#SEP-08", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_1 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Gross_Amount_Box_1", "#GROSS-AMOUNT-BOX-1", FieldType.STRING, 
            15);
        pnd_Orig_Rec_Pnd_Sep_10 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_10", "#SEP-10", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Federal_Tax_Box_2 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Federal_Tax_Box_2", "#FEDERAL-TAX-BOX-2", FieldType.STRING, 
            13);
        pnd_Orig_Rec_Pnd_Sep_11 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_11", "#SEP-11", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_11 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Gross_Amount_Box_11", "#GROSS-AMOUNT-BOX-11", FieldType.STRING, 
            15);
        pnd_Orig_Rec_Pnd_Sep_12 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_12", "#SEP-12", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_State_Id_Box_15 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_State_Id_Box_15", "#STATE-ID-BOX-15", FieldType.STRING, 2);
        pnd_Orig_Rec_Pnd_Sep_13 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_13", "#SEP-13", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_16 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Gross_Amount_Box_16", "#GROSS-AMOUNT-BOX-16", FieldType.STRING, 
            15);
        pnd_Orig_Rec_Pnd_Sep_14 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_14", "#SEP-14", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_State_Tax_Box_17 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_State_Tax_Box_17", "#STATE-TAX-BOX-17", FieldType.STRING, 13);
        pnd_Orig_Rec_Pnd_Sep_15 = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sep_15", "#SEP-15", FieldType.STRING, 1);
        pnd_Orig_Rec_Pnd_Filler = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Filler", "#FILLER", FieldType.STRING, 26);
        pnd_Orig_Rec_Pnd_Sort_Field = pnd_Orig_Rec.newFieldInGroup("pnd_Orig_Rec_Pnd_Sort_Field", "#SORT-FIELD", FieldType.STRING, 1);

        pnd_Hdr_Rec = localVariables.newGroupInRecord("pnd_Hdr_Rec", "#HDR-REC");
        pnd_Hdr_Rec_Pnd_Company_Code = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 4);
        pnd_Hdr_Rec_Pnd_Sep_00 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_00", "#SEP-00", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Tax_Id_Nbr = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Tax_Id_Nbr", "#TAX-ID-NBR", FieldType.STRING, 3);
        pnd_Hdr_Rec_Pnd_Sep_01 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_01", "#SEP-01", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Name = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Name", "#NAME", FieldType.STRING, 4);
        pnd_Hdr_Rec_Pnd_Sep_02 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_02", "#SEP-02", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Address_1st_Line = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Address_1st_Line", "#ADDRESS-1ST-LINE", FieldType.STRING, 9);
        pnd_Hdr_Rec_Pnd_Sep_03 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_03", "#SEP-03", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Address_2nd_Line = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Address_2nd_Line", "#ADDRESS-2ND-LINE", FieldType.STRING, 9);
        pnd_Hdr_Rec_Pnd_Sep_04 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_04", "#SEP-04", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Address_3rd_Line = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Address_3rd_Line", "#ADDRESS-3RD-LINE", FieldType.STRING, 9);
        pnd_Hdr_Rec_Pnd_Sep_05 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_05", "#SEP-05", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_City = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_City", "#CITY", FieldType.STRING, 4);
        pnd_Hdr_Rec_Pnd_Sep_06 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_06", "#SEP-06", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_State = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Hdr_Rec_Pnd_Sep_07 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_07", "#SEP-07", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Zip_Code = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Zip_Code", "#ZIP-CODE", FieldType.STRING, 8);
        pnd_Hdr_Rec_Pnd_Sep_08 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_08", "#SEP-08", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_1 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Gross_Amount_Box_1", "#GROSS-AMOUNT-BOX-1", FieldType.STRING, 
            17);
        pnd_Hdr_Rec_Pnd_Sep_10 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_10", "#SEP-10", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Federal_Tax_Box_2 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Federal_Tax_Box_2", "#FEDERAL-TAX-BOX-2", FieldType.STRING, 15);
        pnd_Hdr_Rec_Pnd_Sep_11 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_11", "#SEP-11", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_11 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Gross_Amount_Box_11", "#GROSS-AMOUNT-BOX-11", FieldType.STRING, 
            18);
        pnd_Hdr_Rec_Pnd_Sep_12 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_12", "#SEP-12", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_State_Id_Box_15 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_State_Id_Box_15", "#STATE-ID-BOX-15", FieldType.STRING, 17);
        pnd_Hdr_Rec_Pnd_Sep_13 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_13", "#SEP-13", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_16 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Gross_Amount_Box_16", "#GROSS-AMOUNT-BOX-16", FieldType.STRING, 
            18);
        pnd_Hdr_Rec_Pnd_Sep_14 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_14", "#SEP-14", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_State_Tax_Box_17 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_State_Tax_Box_17", "#STATE-TAX-BOX-17", FieldType.STRING, 18);
        pnd_Hdr_Rec_Pnd_Sep_15 = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sep_15", "#SEP-15", FieldType.STRING, 1);
        pnd_Hdr_Rec_Pnd_Filler = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Filler", "#FILLER", FieldType.STRING, 130);
        pnd_Hdr_Rec_Pnd_Sort_Field = pnd_Hdr_Rec.newFieldInGroup("pnd_Hdr_Rec_Pnd_Sort_Field", "#SORT-FIELD", FieldType.STRING, 1);
        pnd_Gross_Amt = localVariables.newFieldInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Fed_Tax = localVariables.newFieldInRecord("pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Sta_Tax = localVariables.newFieldInRecord("pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_H_Company = localVariables.newFieldInRecord("pnd_H_Company", "#H-COMPANY", FieldType.STRING, 1);
        pnd_H_Tax_Id = localVariables.newFieldInRecord("pnd_H_Tax_Id", "#H-TAX-ID", FieldType.STRING, 10);
        pnd_H_Residency = localVariables.newFieldInRecord("pnd_H_Residency", "#H-RESIDENCY", FieldType.STRING, 2);
        pnd_H_Citizen_Cde = localVariables.newFieldInRecord("pnd_H_Citizen_Cde", "#H-CITIZEN-CDE", FieldType.STRING, 2);
        pnd_H_Tax_Citizenship = localVariables.newFieldInRecord("pnd_H_Tax_Citizenship", "#H-TAX-CITIZENSHIP", FieldType.STRING, 1);
        pnd_H_Residency_Type = localVariables.newFieldInRecord("pnd_H_Residency_Type", "#H-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_H_Contract = localVariables.newFieldInRecord("pnd_H_Contract", "#H-CONTRACT", FieldType.STRING, 8);
        pnd_H_Gross_Amt = localVariables.newFieldInRecord("pnd_H_Gross_Amt", "#H-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_H_Fed_Tax = localVariables.newFieldInRecord("pnd_H_Fed_Tax", "#H-FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_H_Sta_Tax = localVariables.newFieldInRecord("pnd_H_Sta_Tax", "#H-STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Co = localVariables.newFieldInRecord("pnd_Co", "#CO", FieldType.STRING, 1);
        pnd_Co_Ndx = localVariables.newFieldInRecord("pnd_Co_Ndx", "#CO-NDX", FieldType.NUMERIC, 2);
        pnd_Co_Line = localVariables.newFieldInRecord("pnd_Co_Line", "#CO-LINE", FieldType.STRING, 88);
        pnd_Co_Lit = localVariables.newFieldInRecord("pnd_Co_Lit", "#CO-LIT", FieldType.STRING, 9);

        pnd_Tot_Array = localVariables.newGroupArrayInRecord("pnd_Tot_Array", "#TOT-ARRAY", new DbsArrayController(1, 3));
        pnd_Tot_Array_Pnd_Co_Name = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Co_Name", "#CO-NAME", FieldType.STRING, 60);
        pnd_Tot_Array_Pnd_Tot_Pymnts = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Tot_Pymnts", "#TOT-PYMNTS", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Array_Pnd_Tot_Forms = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Tot_Forms", "#TOT-FORMS", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Array_Pnd_Tot_Gross_Amt = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tot_Array_Pnd_Tot_Fed_Tax = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Tot_Fed_Tax", "#TOT-FED-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Array_Pnd_Tot_State_Tax = pnd_Tot_Array.newFieldInGroup("pnd_Tot_Array_Pnd_Tot_State_Tax", "#TOT-STATE-TAX", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Temp_State_Id = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 15);
        pnd_Op = localVariables.newFieldInRecord("pnd_Op", "#OP", FieldType.BOOLEAN, 1);
        pnd_Ia = localVariables.newFieldInRecord("pnd_Ia", "#IA", FieldType.BOOLEAN, 1);
        pnd_Ml = localVariables.newFieldInRecord("pnd_Ml", "#ML", FieldType.BOOLEAN, 1);
        pnd_Nl = localVariables.newFieldInRecord("pnd_Nl", "#NL", FieldType.BOOLEAN, 1);
        pnd_Tot_Payments = localVariables.newFieldInRecord("pnd_Tot_Payments", "#TOT-PAYMENTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Op = localVariables.newFieldInRecord("pnd_Source_Op", "#SOURCE-OP", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Ia = localVariables.newFieldInRecord("pnd_Source_Ia", "#SOURCE-IA", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Nl = localVariables.newFieldInRecord("pnd_Source_Nl", "#SOURCE-NL", FieldType.PACKED_DECIMAL, 8);
        pnd_Source_Ml = localVariables.newFieldInRecord("pnd_Source_Ml", "#SOURCE-ML", FieldType.PACKED_DECIMAL, 8);
        pnd_Op_Contracts = localVariables.newFieldInRecord("pnd_Op_Contracts", "#OP-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Ia_Contracts = localVariables.newFieldInRecord("pnd_Ia_Contracts", "#IA-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Ml_Contracts = localVariables.newFieldInRecord("pnd_Ml_Contracts", "#ML-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Nl_Contracts = localVariables.newFieldInRecord("pnd_Nl_Contracts", "#NL-CONTRACTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Opia_Cntrcts = localVariables.newFieldInRecord("pnd_Opia_Cntrcts", "#OPIA-CNTRCTS", FieldType.PACKED_DECIMAL, 8);
        pnd_Orig_Written = localVariables.newFieldInRecord("pnd_Orig_Written", "#ORIG-WRITTEN", FieldType.PACKED_DECIMAL, 8);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 33);

        pnd_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Key__R_Field_5", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Tax_Year = pnd_Key__R_Field_5.newFieldInGroup("pnd_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);

        pnd_Key__R_Field_6 = pnd_Key__R_Field_5.newGroupInGroup("pnd_Key__R_Field_6", "REDEFINE", pnd_Key_Pnd_Tax_Year);
        pnd_Key_Pnd_Tax_Year_N = pnd_Key__R_Field_6.newFieldInGroup("pnd_Key_Pnd_Tax_Year_N", "#TAX-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Key_Pnd_Active_Ind = pnd_Key__R_Field_5.newFieldInGroup("pnd_Key_Pnd_Active_Ind", "#ACTIVE-IND", FieldType.STRING, 1);
        pnd_Key_Pnd_Form_Type = pnd_Key__R_Field_5.newFieldInGroup("pnd_Key_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Test_Ind = localVariables.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 4);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.NUMERIC, 4);
        pnd_Cntl_Ndx = localVariables.newFieldInRecord("pnd_Cntl_Ndx", "#CNTL-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rD1Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", FieldType.STRING, 
            1);
        rD1Twrpymnt_Tax_Id_NbrOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Tax_Id_Nbr_OLD", "Twrpymnt_Tax_Id_Nbr_OLD", FieldType.STRING, 10);
        rD1Twrpymnt_Residency_CodeOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Residency_Code_OLD", "Twrpymnt_Residency_Code_OLD", FieldType.STRING, 
            2);
        rD1Twrpymnt_Citizen_CdeOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Citizen_Cde_OLD", "Twrpymnt_Citizen_Cde_OLD", FieldType.STRING, 
            2);
        rD1Twrpymnt_Tax_CitizenshipOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Tax_Citizenship_OLD", "Twrpymnt_Tax_Citizenship_OLD", FieldType.STRING, 
            1);
        rD1Twrpymnt_Residency_TypeOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Residency_Type_OLD", "Twrpymnt_Residency_Type_OLD", FieldType.STRING, 
            1);
        rD1Twrpymnt_Contract_NbrOld = internalLoopRecord.newFieldInRecord("RD1_Twrpymnt_Contract_Nbr_OLD", "Twrpymnt_Contract_Nbr_OLD", FieldType.STRING, 
            8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
        pnd_Orig_Rec_Pnd_Sep_00.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_01.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_02.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_03.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_04.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_05.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_06.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_07.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_08.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_10.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_11.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_12.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_13.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_14.setInitialValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_15.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Company_Code.setInitialValue("Comp");
        pnd_Hdr_Rec_Pnd_Sep_00.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Tax_Id_Nbr.setInitialValue("TIN");
        pnd_Hdr_Rec_Pnd_Sep_01.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Name.setInitialValue("Name");
        pnd_Hdr_Rec_Pnd_Sep_02.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Address_1st_Line.setInitialValue("Address 1");
        pnd_Hdr_Rec_Pnd_Sep_03.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Address_2nd_Line.setInitialValue("Address 2");
        pnd_Hdr_Rec_Pnd_Sep_04.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Address_3rd_Line.setInitialValue("Address 3");
        pnd_Hdr_Rec_Pnd_Sep_05.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_City.setInitialValue("City");
        pnd_Hdr_Rec_Pnd_Sep_06.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_State.setInitialValue("St");
        pnd_Hdr_Rec_Pnd_Sep_07.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Zip_Code.setInitialValue("Zip Code");
        pnd_Hdr_Rec_Pnd_Sep_08.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_1.setInitialValue("Gross Amt / Box 1");
        pnd_Hdr_Rec_Pnd_Sep_10.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Federal_Tax_Box_2.setInitialValue("Fed Tax / Box 2");
        pnd_Hdr_Rec_Pnd_Sep_11.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_11.setInitialValue("Gross Amt / Box 11");
        pnd_Hdr_Rec_Pnd_Sep_12.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_State_Id_Box_15.setInitialValue("State ID / Box 15");
        pnd_Hdr_Rec_Pnd_Sep_13.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_Gross_Amount_Box_16.setInitialValue("Gross Amt / Box 16");
        pnd_Hdr_Rec_Pnd_Sep_14.setInitialValue("H'05'");
        pnd_Hdr_Rec_Pnd_State_Tax_Box_17.setInitialValue("State Tax / Box 17");
        pnd_Hdr_Rec_Pnd_Sep_15.setInitialValue("H'05'");
        pnd_Co_Lit.setInitialValue("Company: ");
        pnd_Tot_Array_Pnd_Co_Name.getValue(1).setInitialValue("TIAA - TEACHERS INSURANCE AND ANNUITY ASSOC.");
        pnd_Tot_Array_Pnd_Co_Name.getValue(2).setInitialValue("TRST - TIAA-CREF AS AGENT FOR JPMORGAN CHASE BK RET PLNS TR");
        pnd_Tot_Array_Pnd_Co_Name.getValue(3).setInitialValue("GRAND TOTALS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5705() throws Exception
    {
        super("Twrp5705");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP5705", onError);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*  FE201506                                                                                                                                                     //Natural: FORMAT ( 01 ) LS = 132 PS = 60
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #SYS-DATE := *DATX
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATX ( EM = MM/DD/YYYY ) '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'W-2 IRS Original Reporting Control Report' 120T 'Report: RPT1' // 58T 'Tax Year:' #WS-TAX-YEAR ( EM = 9999 ) //
                                                                                                                                                                          //Natural: PERFORM TAX-YEAR-LOOKUP
        sub_Tax_Year_Lookup();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORM-CONTROL-LOOKUP
        sub_Form_Control_Lookup();
        if (condition(Global.isEscape())) {return;}
        //* *******************************
        //*   BEGIN MAIN PROCESSING LOOP
        //* *******************************
        boolean endOfDataRd1 = true;                                                                                                                                      //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstRd1 = true;
        RD1:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData618();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventRd1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRd1 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            pnd_Break_Key_Pnd_Twrpymnt_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                              //Natural: ASSIGN #TWRPYMNT-TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            pnd_Break_Key_Pnd_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());                                                          //Natural: ASSIGN #TWRPYMNT-TAX-ID-NBR := #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR
            pnd_Break_Key_Pnd_Twrpymnt_Residency_Code.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());                                                  //Natural: ASSIGN #TWRPYMNT-RESIDENCY-CODE := #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE
            pnd_Break_Key_Pnd_Twrpymnt_Company_Cde_Form.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                              //Natural: ASSIGN #TWRPYMNT-COMPANY-CDE-FORM := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #BREAK-KEY
            pnd_Op.setValue(false);                                                                                                                                       //Natural: MOVE FALSE TO #OP #IA #ML #NL
            pnd_Ia.setValue(false);
            pnd_Ml.setValue(false);
            pnd_Nl.setValue(false);
            //*   ALL PAYMENTS
            pnd_Co.reset();                                                                                                                                               //Natural: RESET #CO
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M") && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S")  //Natural: IF TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND ( TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'M' OR = 'U' )
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U")))))
                {
                                                                                                                                                                          //Natural: PERFORM TAKE-VARIOUS-COUNTS
                    sub_Take_Various_Counts();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ADD TWRPYMNT-INT-AMT ( #I ) TO TWRPYMNT-GROSS-AMT ( #I )
                    pnd_Gross_Amt.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                        //Natural: ADD TWRPYMNT-GROSS-AMT ( #I ) TO #GROSS-AMT
                    pnd_H_Gross_Amt.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                      //Natural: ADD TWRPYMNT-GROSS-AMT ( #I ) TO #H-GROSS-AMT
                    pnd_Fed_Tax.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                                      //Natural: ADD TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #FED-TAX
                    pnd_H_Fed_Tax.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                                    //Natural: ADD TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #H-FED-TAX
                    pnd_Sta_Tax.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                                        //Natural: ADD TWRPYMNT-STATE-WTHLD ( #I ) TO #STA-TAX
                    pnd_H_Sta_Tax.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                                      //Natural: ADD TWRPYMNT-STATE-WTHLD ( #I ) TO #H-STA-TAX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  MB
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().equals("T")))                                                                         //Natural: IF TWRPYMNT-COMPANY-CDE-FORM = 'T'
            {
                pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(1).nadd(pnd_Gross_Amt);                                                                                          //Natural: ADD #GROSS-AMT TO #TOT-GROSS-AMT ( 1 )
                pnd_Tot_Array_Pnd_Tot_Fed_Tax.getValue(1).nadd(pnd_Fed_Tax);                                                                                              //Natural: ADD #FED-TAX TO #TOT-FED-TAX ( 1 )
                pnd_Tot_Array_Pnd_Tot_State_Tax.getValue(1).nadd(pnd_Sta_Tax);                                                                                            //Natural: ADD #STA-TAX TO #TOT-STATE-TAX ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(2).nadd(pnd_Gross_Amt);                                                                                          //Natural: ADD #GROSS-AMT TO #TOT-GROSS-AMT ( 2 )
                pnd_Tot_Array_Pnd_Tot_Fed_Tax.getValue(2).nadd(pnd_Fed_Tax);                                                                                              //Natural: ADD #FED-TAX TO #TOT-FED-TAX ( 2 )
                pnd_Tot_Array_Pnd_Tot_State_Tax.getValue(2).nadd(pnd_Sta_Tax);                                                                                            //Natural: ADD #STA-TAX TO #TOT-STATE-TAX ( 2 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(3).nadd(pnd_Gross_Amt);                                                                                              //Natural: ADD #GROSS-AMT TO #TOT-GROSS-AMT ( 3 )
            pnd_Tot_Array_Pnd_Tot_Fed_Tax.getValue(3).nadd(pnd_Fed_Tax);                                                                                                  //Natural: ADD #FED-TAX TO #TOT-FED-TAX ( 3 )
            pnd_Tot_Array_Pnd_Tot_State_Tax.getValue(3).nadd(pnd_Sta_Tax);                                                                                                //Natural: ADD #STA-TAX TO #TOT-STATE-TAX ( 3 )
            pnd_Orig_Rec.reset();                                                                                                                                         //Natural: RESET #ORIG-REC #GROSS-AMT #FED-TAX #STA-TAX
            pnd_Gross_Amt.reset();
            pnd_Fed_Tax.reset();
            pnd_Sta_Tax.reset();
            rD1Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                          //Natural: END-WORK
            rD1Twrpymnt_Tax_Id_NbrOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr());
            rD1Twrpymnt_Residency_CodeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code());
            rD1Twrpymnt_Citizen_CdeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Citizen_Cde());
            rD1Twrpymnt_Tax_CitizenshipOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Citizenship());
            rD1Twrpymnt_Residency_TypeOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type());
            rD1Twrpymnt_Contract_NbrOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());
        }
        RD1_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventRd1(endOfDataRd1);
        }
        if (Global.isEscape()) return;
        //* ***************
        //*  WRITE HEADER
        //* ***************
        pnd_Hdr_Rec_Pnd_Sort_Field.setValue("H'00'");                                                                                                                     //Natural: ASSIGN #HDR-REC.#SORT-FIELD := H'00'
        getWorkFiles().write(2, false, pnd_Hdr_Rec);                                                                                                                      //Natural: WRITE WORK FILE 2 #HDR-REC
                                                                                                                                                                          //Natural: PERFORM SET-DELIMITERS
        sub_Set_Delimiters();
        if (condition(Global.isEscape())) {return;}
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_1.setValueEdited(pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(3),new ReportEditMask("-99999999999.99"));                            //Natural: MOVE EDITED #TOT-GROSS-AMT ( 3 ) ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-1
        pnd_Orig_Rec_Pnd_Federal_Tax_Box_2.setValueEdited(pnd_Tot_Array_Pnd_Tot_Fed_Tax.getValue(3),new ReportEditMask("-999999999.99"));                                 //Natural: MOVE EDITED #TOT-FED-TAX ( 3 ) ( EM = -999999999.99 ) TO #ORIG-REC.#FEDERAL-TAX-BOX-2
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_11.setValueEdited(pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(3),new ReportEditMask("-99999999999.99"));                           //Natural: MOVE EDITED #TOT-GROSS-AMT ( 3 ) ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-11
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_16.setValueEdited(pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(3),new ReportEditMask("-99999999999.99"));                           //Natural: MOVE EDITED #TOT-GROSS-AMT ( 3 ) ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-16
        pnd_Orig_Rec_Pnd_State_Tax_Box_17.setValueEdited(pnd_Tot_Array_Pnd_Tot_State_Tax.getValue(3),new ReportEditMask("-999999999.99"));                                //Natural: MOVE EDITED #TOT-STATE-TAX ( 3 ) ( EM = -999999999.99 ) TO #ORIG-REC.#STATE-TAX-BOX-17
        //* ****************
        //*  WRITE TRAILER
        //* ****************
        pnd_Orig_Rec_Pnd_Name.setValue("TOTALS");                                                                                                                         //Natural: ASSIGN #ORIG-REC.#NAME := 'TOTALS'
        pnd_Orig_Rec_Pnd_Sort_Field.setValue("H'FF'");                                                                                                                    //Natural: ASSIGN #ORIG-REC.#SORT-FIELD := H'FF'
        getWorkFiles().write(2, false, pnd_Orig_Rec);                                                                                                                     //Natural: WRITE WORK FILE 2 #ORIG-REC
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TABLE
        sub_Update_Control_Table();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-DETAIL
        sub_Write_Report_Detail();
        if (condition(Global.isEscape())) {return;}
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDRESS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* ********************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W2-ORIGINAL-REC
        //*  IF #MDMA101.#O-BASE-ADDRESS-ZIP-CODE = ' '
        //*   MOVE #MDMA101.#O-BASE-ADDRESS-ZIP-CODE TO #ORIG-REC.#ZIP-CODE
        //*  ELSE
        //*   COMPRESS #TWRAPART.ADDR-ZIP '-' #TWRAPART.ADDR-ZIP-4
        //*     INTO #ORIG-REC.#ZIP-CODE LEAVING NO
        //*  END-IF
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENCY
        //* *************************************
        //*  TWRARSDC.#I-1078-IND         := TWRPYMNT-FRM1078-IND
        //*  TWRARSDC.#I-RSDNCY-CODE      := #H-RESIDENCY
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-DETAIL
        //* *************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAKE-VARIOUS-COUNTS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORM-CONTROL-LOOKUP
        //* **************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-TABLE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TAX-YEAR-LOOKUP
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-DELIMITERS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
        //* **
        //* **                                                                                                                                                            //Natural: ON ERROR
        //* **
        //* ***************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  *************************************
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Name_Address() throws Exception                                                                                                                  //Natural: GET-NAME-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  #TWRAPART.#FUNCTION    := 'R'   /* FE201506
        //*  #TWRAPART.#DISPLAY-IND := TRUE
        //*  #TWRAPART.#TAX-YEAR    := *DATN / 10000
        //*  #TWRAPART.#TIN         := #H-TAX-ID
        //*  PIN-EXPANSION
        //*  PIN-EXPANSION
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(pnd_H_Tax_Id);                                                                                                  //Natural: ASSIGN #MDMA101.#I-SSN-A9 := #H-TAX-ID
        //*  PIN-EXPANSION - START
        //*  FE201506
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' USING #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MR") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MR.") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MRS")  //Natural: IF #MDMA101.#O-PREFIX = 'MR' OR = 'MR.' OR = 'MRS' OR = 'MRS.' OR = 'MS' OR = 'MS.'
                || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MRS.") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MS") || pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix().equals("MS.")))
            {
                pnd_Orig_Rec_Pnd_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(),              //Natural: COMPRESS #MDMA101.#O-FIRST-NAME #MDMA101.#O-MIDDLE-NAME #MDMA101.#O-LAST-NAME #MDMA101.#O-SUFFIX INTO #ORIG-REC.#NAME
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Orig_Rec_Pnd_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Prefix(), pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(),                   //Natural: COMPRESS #MDMA101.#O-PREFIX #MDMA101.#O-FIRST-NAME #MDMA101.#O-MIDDLE-NAME #MDMA101.#O-LAST-NAME #MDMA101.#O-SUFFIX INTO #ORIG-REC.#NAME
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Suffix()));
            }                                                                                                                                                             //Natural: END-IF
            pnd_Orig_Rec_Pnd_Address_1st_Line.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                            //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-1 TO #ORIG-REC.#ADDRESS-1ST-LINE
            pnd_Orig_Rec_Pnd_Address_2nd_Line.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                            //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-2 TO #ORIG-REC.#ADDRESS-2ND-LINE
            pnd_Orig_Rec_Pnd_Address_3rd_Line.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                            //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-LINE-3 TO #ORIG-REC.#ADDRESS-3RD-LINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Orig_Rec_Pnd_Name.setValue("*** PARTICIPANT NAME NOT FOUND ***");                                                                                         //Natural: MOVE '*** PARTICIPANT NAME NOT FOUND ***' TO #ORIG-REC.#NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  PIN-EXPANSION - END
        if (condition(pnd_Orig_Rec_Pnd_Name_Pref.equals("DR ") || pnd_Orig_Rec_Pnd_Name_Pref.equals("DR.")))                                                              //Natural: IF #ORIG-REC.#NAME-PREF = 'DR ' OR = 'DR.'
        {
            pnd_Orig_Rec_Pnd_Name.setValue(pnd_Orig_Rec_Pnd_Name_Remains, MoveOption.LeftJustified);                                                                      //Natural: MOVE LEFT #ORIG-REC.#NAME-REMAINS TO #ORIG-REC.#NAME
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().setValue(pnd_Key_Pnd_Tax_Year_N);                                                                                          //Natural: ASSIGN TWRATBL2.#TAX-YEAR := #KEY.#TAX-YEAR-N
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        if (condition(DbsUtil.maskMatches(pnd_H_Residency,"NN")))                                                                                                         //Natural: IF #H-RESIDENCY = MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_H_Residency));                                      //Natural: COMPRESS '0' #H-RESIDENCY INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_H_Residency);                                                                                            //Natural: ASSIGN TWRATBL2.#STATE-CDE := #H-RESIDENCY
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  MB - REMOVED IRRELEVANT COMPANIES
        short decideConditionsMet830 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #H-COMPANY;//Natural: VALUE 'T'
        if (condition((pnd_H_Company.equals("T"))))
        {
            decideConditionsMet830++;
            pnd_Ws_Pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Tiaa());                                                                       //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TIAA TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_H_Company.equals("X"))))
        {
            decideConditionsMet830++;
            pnd_Ws_Pnd_Temp_State_Id.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Tax_Id_Trust());                                                                      //Natural: MOVE TWRATBL2.TIRCNTL-STATE-TAX-ID-TRUST TO #TEMP-STATE-ID
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet830 > 0))
        {
            pnd_Employer_State_Id_Pnd_State_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                            //Natural: MOVE TIRCNTL-STATE-ALPHA-CODE TO #STATE-CODE
            pnd_Employer_State_Id_Pnd_State_Id.setValue(pnd_Ws_Pnd_Temp_State_Id);                                                                                        //Natural: MOVE #TEMP-STATE-ID TO #STATE-ID
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_W2_Original_Rec() throws Exception                                                                                                             //Natural: WRITE-W2-ORIGINAL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(pnd_H_Gross_Amt.equals(getZero())))                                                                                                                 //Natural: IF #H-GROSS-AMT = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EINCHG - START
        getReports().write(0, "#H-COMPANY FROM OLD PYMNT FILE VALUE-->",pnd_H_Company);                                                                                   //Natural: WRITE '#H-COMPANY FROM OLD PYMNT FILE VALUE-->' #H-COMPANY
        if (Global.isEscape()) return;
        if (condition(pnd_Break_Key_Pnd_Twrpymnt_Tax_Year.greaterOrEqual(2018) && pnd_H_Company.equals("T")))                                                             //Natural: IF #TWRPYMNT-TAX-YEAR GE 2018 AND #H-COMPANY = 'T'
        {
            pnd_Orig_Rec_Pnd_Company_Code.setValue("A");                                                                                                                  //Natural: MOVE 'A' TO #ORIG-REC.#COMPANY-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Orig_Rec_Pnd_Company_Code.setValue(pnd_H_Company);                                                                                                        //Natural: MOVE #H-COMPANY TO #ORIG-REC.#COMPANY-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EINCHG - END
        //*  MOVE #H-COMPANY TO #ORIG-REC.#COMPANY-CODE
        //*  FE2015
        pnd_Orig_Rec_Pnd_Tax_Id_Nbr.setValueEdited(pnd_H_Tax_Id,new ReportEditMask("XXXXXXXXX"));                                                                         //Natural: MOVE EDITED #H-TAX-ID ( EM = XXXXXXXXX ) TO #ORIG-REC.#TAX-ID-NBR
        pnd_Orig_Rec_Pnd_Address_1st_Line.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                //Natural: ASSIGN #ORIG-REC.#ADDRESS-1ST-LINE := #MDMA101.#O-BASE-ADDRESS-LINE-1
        //*  #ORIG-REC.#CONTRACT-NBR        := #H-CONTRACT
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_1.setValueEdited(pnd_H_Gross_Amt,new ReportEditMask("-99999999999.99"));                                                        //Natural: MOVE EDITED #H-GROSS-AMT ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-1
        pnd_Orig_Rec_Pnd_Federal_Tax_Box_2.setValueEdited(pnd_H_Fed_Tax,new ReportEditMask("-999999999.99"));                                                             //Natural: MOVE EDITED #H-FED-TAX ( EM = -999999999.99 ) TO #ORIG-REC.#FEDERAL-TAX-BOX-2
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_11.setValueEdited(pnd_H_Gross_Amt,new ReportEditMask("-99999999999.99"));                                                       //Natural: MOVE EDITED #H-GROSS-AMT ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-11
        pnd_Orig_Rec_Pnd_State_Id_Box_15.setValueEdited(pnd_Employer_State_Id,new ReportEditMask("XX"));                                                                  //Natural: MOVE EDITED #EMPLOYER-STATE-ID ( EM = XX ) TO #ORIG-REC.#STATE-ID-BOX-15
        pnd_Orig_Rec_Pnd_Gross_Amount_Box_16.setValueEdited(pnd_H_Gross_Amt,new ReportEditMask("-99999999999.99"));                                                       //Natural: MOVE EDITED #H-GROSS-AMT ( EM = -99999999999.99 ) TO #ORIG-REC.#GROSS-AMOUNT-BOX-16
        pnd_Orig_Rec_Pnd_State_Tax_Box_17.setValueEdited(pnd_H_Sta_Tax,new ReportEditMask("-999999999.99"));                                                              //Natural: MOVE EDITED #H-STA-TAX ( EM = -999999999.99 ) TO #ORIG-REC.#STATE-TAX-BOX-17
        //*  MOVE #STATE-CODE
        //*  MOVE EDITED #EMPLOYER-STATE-ID   (EM=XX)
        //*    TO #ORIG-REC.#STATE
        //*  FE201506 START
        if (condition(DbsUtil.is(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().getText(),"N2")))                                                         //Natural: IF #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE IS ( N2 )
        {
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCY
            sub_Get_Residency();
            if (condition(Global.isEscape())) {return;}
            pnd_Orig_Rec_Pnd_State.setValue(pdaTwrarsdc.getTwrarsdc_Pnd_O_Rsdncy_Code());                                                                                 //Natural: ASSIGN #ORIG-REC.#STATE := TWRARSDC.#O-RSDNCY-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code(),"AA")))                                                      //Natural: IF #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE = MASK ( AA )
            {
                pnd_Orig_Rec_Pnd_State.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code());                                                          //Natural: ASSIGN #ORIG-REC.#STATE := #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FE201506
        pnd_Orig_Rec_Pnd_City.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_City());                                                                              //Natural: MOVE #MDMA101.#O-BASE-ADDRESS-CITY TO #ORIG-REC.#CITY
        pnd_Orig_Rec_Pnd_Zip_Code.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Zip_Code());                                                                      //Natural: ASSIGN #ORIG-REC.#ZIP-CODE := #MDMA101.#O-BASE-ADDRESS-ZIP-CODE
        DbsUtil.examine(new ExamineSource(pnd_Orig_Rec_Pnd_Address_2nd_Line,true), new ExamineSearch(pnd_Orig_Rec_Pnd_Zip_Code), new ExamineReplace(" "));                //Natural: EXAMINE FULL #ORIG-REC.#ADDRESS-2ND-LINE #ORIG-REC.#ZIP-CODE REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Orig_Rec_Pnd_Address_3rd_Line,true), new ExamineSearch(pnd_Orig_Rec_Pnd_Zip_Code), new ExamineReplace(" "));                //Natural: EXAMINE FULL #ORIG-REC.#ADDRESS-3RD-LINE #ORIG-REC.#ZIP-CODE REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM SET-DELIMITERS
        sub_Set_Delimiters();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(2, false, pnd_Orig_Rec);                                                                                                                     //Natural: WRITE WORK FILE 2 #ORIG-REC
        pnd_Tot_Array_Pnd_Tot_Forms.getValue(3).nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOT-FORMS ( 3 )
        if (condition(pnd_H_Company.equals("T")))                                                                                                                         //Natural: IF #H-COMPANY = 'T'
        {
            pnd_Tot_Array_Pnd_Tot_Forms.getValue(1).nadd(1);                                                                                                              //Natural: ADD 1 TO #TOT-FORMS ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Array_Pnd_Tot_Forms.getValue(2).nadd(1);                                                                                                              //Natural: ADD 1 TO #TOT-FORMS ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Residency() throws Exception                                                                                                                     //Natural: GET-RESIDENCY
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwrarsdc.getTwrarsdc_Pnd_I_Tax_Year().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                             //Natural: ASSIGN TWRARSDC.#I-TAX-YEAR := TWRPYMNT-TAX-YEAR
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Ctznshp().setValue(pnd_H_Citizen_Cde);                                                                                              //Natural: ASSIGN TWRARSDC.#I-CTZNSHP := #H-CITIZEN-CDE
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Tax_Ctz().setValue(pnd_H_Tax_Citizenship);                                                                                          //Natural: ASSIGN TWRARSDC.#I-TAX-CTZ := #H-TAX-CITIZENSHIP
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Type().setValue(pnd_H_Residency_Type);                                                                                       //Natural: ASSIGN TWRARSDC.#I-RSDNCY-TYPE := #H-RESIDENCY-TYPE
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Code().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code());                                             //Natural: ASSIGN TWRARSDC.#I-RSDNCY-CODE := #MDMA101.#O-BASE-ADDRESS-GEOGRAPHIC-CODE
        //*  TWRARSDC.#I-LOC-CODE         := DERPYMNT-LOCALITY-CDE
        DbsUtil.callnat(Twrnrsdc.class , getCurrentProcessState(), pdaTwrarsdc.getTwrarsdc_Pnd_Input_Parms(), pdaTwrarsdc.getTwrarsdc_Pnd_Output_Data(),                  //Natural: CALLNAT 'TWRNRSDC' USING TWRARSDC.#INPUT-PARMS TWRARSDC.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Report_Detail() throws Exception                                                                                                               //Natural: WRITE-REPORT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #CO-NDX 1 3
        for (pnd_Co_Ndx.setValue(1); condition(pnd_Co_Ndx.lessOrEqual(3)); pnd_Co_Ndx.nadd(1))
        {
            pnd_Co_Line.setValue(DbsUtil.compress(pnd_Co_Lit, pnd_Tot_Array_Pnd_Co_Name.getValue(pnd_Co_Ndx)));                                                           //Natural: COMPRESS #CO-LIT #CO-NAME ( #CO-NDX ) INTO #CO-LINE
            getReports().write(1, ReportOption.NOTITLE,pnd_Co_Line,NEWLINE,"TOTAL W2 PAYMENTS               :",new TabSetting(44),pnd_Tot_Array_Pnd_Tot_Pymnts.getValue(pnd_Co_Ndx),  //Natural: WRITE ( 01 ) #CO-LINE / 'TOTAL W2 PAYMENTS               :' 44T #TOT-PYMNTS ( #CO-NDX ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TOTAL FORMS WRITTEN TO FILE     :' 44T #TOT-FORMS ( #CO-NDX ) ( EM = ZZ,ZZZ,ZZ9 ) / 'TOTAL GROSS AMOUNT              :' 35T #TOT-GROSS-AMT ( #CO-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL FEDERAL TAX WITHHELD      :' 38T #TOT-FED-TAX ( #CO-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 'TOTAL STATE TAX WITHHELD        :' 38T #TOT-STATE-TAX ( #CO-NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) //
                new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL FORMS WRITTEN TO FILE     :",new TabSetting(44),pnd_Tot_Array_Pnd_Tot_Forms.getValue(pnd_Co_Ndx), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL GROSS AMOUNT              :",new TabSetting(35),pnd_Tot_Array_Pnd_Tot_Gross_Amt.getValue(pnd_Co_Ndx), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL FEDERAL TAX WITHHELD      :",new TabSetting(38),pnd_Tot_Array_Pnd_Tot_Fed_Tax.getValue(pnd_Co_Ndx), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,"TOTAL STATE TAX WITHHELD        :",new TabSetting(38),pnd_Tot_Array_Pnd_Tot_State_Tax.getValue(pnd_Co_Ndx), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Take_Various_Counts() throws Exception                                                                                                               //Natural: TAKE-VARIOUS-COUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Tot_Array_Pnd_Tot_Pymnts.getValue(3).nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOT-PYMNTS ( 3 )
        //*  MB
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().equals("T")))                                                                             //Natural: IF TWRPYMNT-COMPANY-CDE-FORM = 'T'
        {
            pnd_Tot_Array_Pnd_Tot_Pymnts.getValue(1).nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-PYMNTS ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tot_Array_Pnd_Tot_Pymnts.getValue(2).nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-PYMNTS ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet925 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( TWRPYMNT-SETTLE-TYPE ( #I ) = 'E' OR = 'M' OR = 'T' )
        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M") 
            || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")))
        {
            decideConditionsMet925++;
            pnd_Source_Op.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-OP
            pnd_Op.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #OP
        }                                                                                                                                                                 //Natural: WHEN TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'OP'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("OP")))
        {
            decideConditionsMet925++;
            pnd_Source_Op.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-OP
            pnd_Op.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #OP
        }                                                                                                                                                                 //Natural: WHEN TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'AP'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("AP")))
        {
            decideConditionsMet925++;
            pnd_Source_Ia.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-IA
            pnd_Ia.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #IA
        }                                                                                                                                                                 //Natural: WHEN TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'ML'
        else if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("ML")))
        {
            decideConditionsMet925++;
            pnd_Source_Ml.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-ML
            pnd_Ml.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #ML
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Source_Nl.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SOURCE-NL
            pnd_Nl.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO #NL
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet943 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OP AND #IA AND #GROSS-AMT NE 0
        if (condition(pnd_Op.getBoolean() && pnd_Ia.getBoolean() && pnd_Gross_Amt.notEquals(getZero())))
        {
            decideConditionsMet943++;
            pnd_Opia_Cntrcts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #OPIA-CNTRCTS
        }                                                                                                                                                                 //Natural: WHEN #OP AND #GROSS-AMT NE 0
        else if (condition(pnd_Op.getBoolean() && pnd_Gross_Amt.notEquals(getZero())))
        {
            decideConditionsMet943++;
            pnd_Op_Contracts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #OP-CONTRACTS
        }                                                                                                                                                                 //Natural: WHEN #IA AND #GROSS-AMT NE 0
        else if (condition(pnd_Ia.getBoolean() && pnd_Gross_Amt.notEquals(getZero())))
        {
            decideConditionsMet943++;
            pnd_Ia_Contracts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #IA-CONTRACTS
        }                                                                                                                                                                 //Natural: WHEN #NL AND #GROSS-AMT NE 0
        else if (condition(pnd_Nl.getBoolean() && pnd_Gross_Amt.notEquals(getZero())))
        {
            decideConditionsMet943++;
            pnd_Nl_Contracts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NL-CONTRACTS
        }                                                                                                                                                                 //Natural: WHEN #ML AND #GROSS-AMT NE 0
        else if (condition(pnd_Ml.getBoolean() && pnd_Gross_Amt.notEquals(getZero())))
        {
            decideConditionsMet943++;
            pnd_Ml_Contracts.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ML-CONTRACTS
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  W-2
    private void sub_Form_Control_Lookup() throws Exception                                                                                                               //Natural: FORM-CONTROL-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Key_Pnd_Tax_Year_N);                                                                                      //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #KEY.#TAX-YEAR-N
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(8);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 8
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Twrnfrm2.class , getCurrentProcessState(), pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRM2' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean()))                                                                                   //Natural: IF #TWRATBL4.#TIRCNTL-IRS-ORIG
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(15),"ORIGINAL REPORTING PROCESS FOR",pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Form_Name().getValue(8 + 1),new  //Natural: WRITE ( 1 ) '***' 15T 'ORIGINAL REPORTING PROCESS FOR' #TWRAFRMN.#FORM-NAME ( 8 ) 77T '***' / '***' 15T 'ALREADY EXECUTED ON' #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) ( EM = MM/DD/YYYY ) 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(15),"ALREADY EXECUTED ON",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1), new 
                ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(! (pnd_Debug.getBoolean())))                                                                                                                    //Natural: IF NOT #DEBUG
            {
                DbsUtil.terminate(102);  if (true) return;                                                                                                                //Natural: TERMINATE 102
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, NEWLINE,">>> TERMINATE (102) Bypassed for Debugging Purposes",NEWLINE);                                                             //Natural: WRITE / '>>> TERMINATE (102) Bypassed for Debugging Purposes' /
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Form............................: ",pdaTwrafrm2.getPnd_Twrafrmn_Pnd_Form_Name().getValue(8 + 1),NEWLINE,new  //Natural: WRITE ( 1 ) / 1T 'Form............................: ' #TWRAFRMN.#FORM-NAME ( 8 ) / 1T 'Original Reporting..............: ' #TWRATBL4.#TIRCNTL-IRS-ORIG ( EM = N/Y ) /
            TabSetting(1),"Original Reporting..............: ",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig(), new ReportEditMask ("N/Y"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Control_Table() throws Exception                                                                                                              //Natural: UPDATE-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Cntl_Ndx.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #CNTL-NDX
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(pnd_Sys_Date);                                                                                 //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := #SYS-DATE
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Cref_Gross_Amt().getValue(1).setValue(Global.getTIMN());                                                                  //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-CREF-GROSS-AMT ( 1 ) := *TIMN
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().setValue(true);                                                                                                //Natural: ASSIGN #TWRATBL4.#TIRCNTL-IRS-ORIG := TRUE
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Tax_Year_Lookup() throws Exception                                                                                                                   //Natural: TAX-YEAR-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Key.reset();                                                                                                                                                  //Natural: RESET #KEY
        getWorkFiles().read(3, pnd_Key_Pnd_Tax_Year, pnd_Test_Ind);                                                                                                       //Natural: READ WORK FILE 3 ONCE #KEY.#TAX-YEAR #TEST-IND
        pnd_Datn.compute(new ComputeParameters(false, pnd_Datn), Global.getDATN().divide(10000));                                                                         //Natural: ASSIGN #DATN := *DATN / 10000
        //*  NEXT YEAR
        //*  CURRENT YR
        //*  CURRENT YR - 1
        //*  CURRENT YR - 2
        //*  CURRENT YR - 3
        short decideConditionsMet1003 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #KEY.#TAX-YEAR;//Natural: VALUE '+1'
        if (condition((pnd_Key_Pnd_Tax_Year.equals("+1"))))
        {
            decideConditionsMet1003++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.add(1));                                                                      //Natural: ASSIGN #WS-TAX-YEAR := #DATN + 1
        }                                                                                                                                                                 //Natural: VALUE '0'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("0"))))
        {
            decideConditionsMet1003++;
            pnd_Ws_Tax_Year.setValue(pnd_Datn);                                                                                                                           //Natural: ASSIGN #WS-TAX-YEAR := #DATN
        }                                                                                                                                                                 //Natural: VALUE ' ', '-1'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals(" ") || pnd_Key_Pnd_Tax_Year.equals("-1"))))
        {
            decideConditionsMet1003++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(1));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 1
        }                                                                                                                                                                 //Natural: VALUE '-2'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("-2"))))
        {
            decideConditionsMet1003++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(2));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 2
        }                                                                                                                                                                 //Natural: VALUE '-3'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("-3"))))
        {
            decideConditionsMet1003++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(3));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 3
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1003 > 0))
        {
            pnd_Key_Pnd_Tax_Year_N.setValue(pnd_Ws_Tax_Year);                                                                                                             //Natural: ASSIGN #KEY.#TAX-YEAR-N := #WS-TAX-YEAR
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pnd_Key_Pnd_Tax_Year,"NNNN")))                                                                                              //Natural: IF #KEY.#TAX-YEAR = MASK ( NNNN )
            {
                pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Key_Pnd_Tax_Year.val());                                                       //Natural: ASSIGN #WS-TAX-YEAR := VAL ( #KEY.#TAX-YEAR )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "Invalid Tax Year Input",pnd_Key_Pnd_Tax_Year,"...  Program Terminated.");                                                          //Natural: WRITE 'Invalid Tax Year Input' #KEY.#TAX-YEAR '...  Program Terminated.'
                if (Global.isEscape()) return;
                DbsUtil.terminate(16);  if (true) return;                                                                                                                 //Natural: TERMINATE 16
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition((pnd_Test_Ind.greater(" ")) && (pnd_Test_Ind.notEquals("TEST"))))                                                                                   //Natural: IF ( #TEST-IND GT ' ' ) AND ( #TEST-IND NE 'TEST' )
        {
            getReports().write(0, "INVALID TEST INDICATOR:",pnd_Test_Ind,"PROGRAM TERMINATED.");                                                                          //Natural: WRITE 'INVALID TEST INDICATOR:' #TEST-IND 'PROGRAM TERMINATED.'
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Delimiters() throws Exception                                                                                                                    //Natural: SET-DELIMITERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  #ORIG-REC.#SEP-09
        pnd_Orig_Rec_Pnd_Sep_01.setValue("H'05'");                                                                                                                        //Natural: MOVE H'05' TO #ORIG-REC.#SEP-01 #ORIG-REC.#SEP-02 #ORIG-REC.#SEP-03 #ORIG-REC.#SEP-04 #ORIG-REC.#SEP-05 #ORIG-REC.#SEP-06 #ORIG-REC.#SEP-07 #ORIG-REC.#SEP-08 #ORIG-REC.#SEP-10 #ORIG-REC.#SEP-11 #ORIG-REC.#SEP-12 #ORIG-REC.#SEP-13 #ORIG-REC.#SEP-14 #ORIG-REC.#SEP-15 #ORIG-REC.#SEP-00
        pnd_Orig_Rec_Pnd_Sep_02.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_03.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_04.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_05.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_06.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_07.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_08.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_10.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_11.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_12.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_13.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_14.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_15.setValue("H'05'");
        pnd_Orig_Rec_Pnd_Sep_00.setValue("H'05'");
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                                                 //Natural: WRITE '=' #MDMA101.#O-BASE-ADDRESS-LINE-1
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                                                 //Natural: WRITE '=' #MDMA101.#O-BASE-ADDRESS-LINE-2
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                                                 //Natural: WRITE '=' #MDMA101.#O-BASE-ADDRESS-LINE-3
        if (Global.isEscape()) return;
        //*  WRITE '=' #TWRAPART.ADDR-LNE-4
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
        sub_Error_Routine();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventRd1() throws Exception {atBreakEventRd1(false);}
    private void atBreakEventRd1(boolean endOfData) throws Exception
    {
        boolean pnd_Break_KeyIsBreak = pnd_Break_Key.isBreak(endOfData);
        if (condition(pnd_Break_KeyIsBreak))
        {
            //*  MB
            if (condition(pnd_H_Gross_Amt.notEquals(getZero())))                                                                                                          //Natural: IF #H-GROSS-AMT NE 0
            {
                //*        HOLD BREAK INFO
                pnd_H_Company.setValue(rD1Twrpymnt_Company_Cde_FormOld);                                                                                                  //Natural: ASSIGN #H-COMPANY := OLD ( TWRPYMNT-COMPANY-CDE-FORM )
                pnd_H_Tax_Id.setValue(rD1Twrpymnt_Tax_Id_NbrOld);                                                                                                         //Natural: ASSIGN #H-TAX-ID := OLD ( TWRPYMNT-TAX-ID-NBR )
                pnd_H_Residency.setValue(rD1Twrpymnt_Residency_CodeOld);                                                                                                  //Natural: ASSIGN #H-RESIDENCY := OLD ( TWRPYMNT-RESIDENCY-CODE )
                pnd_H_Citizen_Cde.setValue(rD1Twrpymnt_Citizen_CdeOld);                                                                                                   //Natural: ASSIGN #H-CITIZEN-CDE := OLD ( TWRPYMNT-CITIZEN-CDE )
                pnd_H_Tax_Citizenship.setValue(rD1Twrpymnt_Tax_CitizenshipOld);                                                                                           //Natural: ASSIGN #H-TAX-CITIZENSHIP := OLD ( TWRPYMNT-TAX-CITIZENSHIP )
                pnd_H_Residency_Type.setValue(rD1Twrpymnt_Residency_TypeOld);                                                                                             //Natural: ASSIGN #H-RESIDENCY-TYPE := OLD ( TWRPYMNT-RESIDENCY-TYPE )
                pnd_H_Contract.setValue(rD1Twrpymnt_Contract_NbrOld);                                                                                                     //Natural: ASSIGN #H-CONTRACT := OLD ( TWRPYMNT-CONTRACT-NBR )
                //*        SETUP AND WRITE RECORD
                                                                                                                                                                          //Natural: PERFORM GET-NAME-ADDRESS
                sub_Get_Name_Address();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                sub_Get_State_Info();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-W2-ORIGINAL-REC
                sub_Write_W2_Original_Rec();
                if (condition(Global.isEscape())) {return;}
                //*        RESET ACCUMULATORS
                pnd_H_Gross_Amt.reset();                                                                                                                                  //Natural: RESET #H-GROSS-AMT #H-FED-TAX #H-STA-TAX
                pnd_H_Fed_Tax.reset();
                pnd_H_Sta_Tax.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(46),"W-2 IRS Original Reporting Control Report",new 
            TabSetting(120),"Report: RPT1",NEWLINE,NEWLINE,new TabSetting(58),"Tax Year:",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData618() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Init_Tax_Yr_Pnd_Init_Tax_Yr_N.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                //Natural: ASSIGN #INIT-TAX-YR-N := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
        }                                                                                                                                                                 //Natural: END-START
    }
}
