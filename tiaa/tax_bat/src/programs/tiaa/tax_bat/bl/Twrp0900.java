/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:39 PM
**        * FROM NATURAL PROGRAM : Twrp0900
************************************************************
**        * FILE NAME            : Twrp0900.java
**        * CLASS NAME           : Twrp0900
**        * INSTANCE NAME        : Twrp0900
************************************************************
********************************************************************
* PROGRAM : TWRP0900
* FUNCTION: CREATES A FLAT FILE (FLAT FILE2) FOR A GIVEN TAX YEAR
*              AND CREATE A FLAT FILE1 TO SAVE TAX YEAR
* INPUT   : FILE 94 (TWRPYMNT-PAYMENT-FILE)
* OUTPUT  : FLAT FILE1 , FLAT FILE2 AND CONTROL TOTAL
* AUTHOR  : EDITH  9/23
* HISTORY : 10/14/99 M. SUPONITSKY. VARIABLE LENGTH FILE.
*                                  READ PAYMENT FILE BY SUPERDESCRIPTOR
*         : STOWED DUE TO UPDATE ON  TWRL0900 (OCC.OF PE FIELDS = 40)
*         : READ CONTROL FILE (FLAT-FILE) FOR THE LATEST TAX-YEAR
*             TO USED AS KEY IN EXTRACTING RECORDS FROM PAYMENT FILE
*             (EDITH 6/19/99)
* 10/25/99: M. SUPONITSKY - ADDED CANADIAN EXTRACT FOR THE REPORTS.
* 6/15/99 : UPDATED DUE TO ADDITION OF IVC-PROTECT FIELD TO TWRL0900/90A
*  (EDITH)    W/C WILL BE USED IN THE COMPUTATION OF TAXABLE AMT.
*
* 10/8/99 : INCLUSION OF NEW COMPANY , TIAA-LIFE
*  (EDITH)
* 04/02/02: ADDED NEW COMPANY - TCII
* 12/09/02: ADDITION OF NRA-REFUND-AMT TO TWRL0900.
* 10/14/03: STOWED DUE TO UPDATE ON TWRL0900-ADDED CONTRACT TYPE& IRC-TO
* 09/16/04: TOPS RELEASE 3 CHANGES   R. MA
*           ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*           CODE 'OP', 'NL' AND 'PL'.
* 07/14/08: ADDED TRADE DATE AND CHECK REASON TO THE EXTRACT TO
*           BE USED IN INTERNAL 1035 EXCHANGE RECONCILIATION PROCESS.
* 05/20/09: ADDITION OF ORIGIN AND AREA CODES TO TWRL0900. (F.TWAHIR)
*           M001
* 10/17/11: SUPPORTING NEW COMPANY CODE "F"; CHANGE CAN BE
*           REFERENCED AS DY1
* 01/2012   RSALGADO  RE-COMPILED WITH NEW TWRL0900
*           - POPULATE WITH NEW SUNY/CUNY PLAN LEVEL INFORMATION
*           - POPULATE WITH IRR-AMT
* 11/2014   O SOTTO - ADDED NEW FIELDS FOR FATCA.  CHANGES MARKED
*           11/2014.
********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0900 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9415 ldaTwrl9415;
    private LdaTwrl9420 ldaTwrl9420;
    private LdaTwrl0900 ldaTwrl0900;
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Can_Start;
    private DbsField pnd_Ws_Pnd_Can_End;
    private DbsField pnd_Ws_Pnd_Super_Start;
    private DbsField pnd_Ws_Pnd_Super_End;
    private DbsField pnd_Ws_Pnd_Param_Tax_Year_A;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Param_Tax_Year;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_In;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Rec_Ritten;
    private DbsField pnd_Ws_Pnd_Rec_Read;
    private DbsField pnd_Ws_Pnd_Cref_Cnt;
    private DbsField pnd_Ws_Pnd_Life_Cnt;
    private DbsField pnd_Ws_Pnd_Tiaa_Cnt;
    private DbsField pnd_Ws_Pnd_Tcii_Cnt;
    private DbsField pnd_Ws_Pnd_Fsbc_Cnt;
    private DbsField pnd_Ws_Pnd_Trst_Cnt;
    private DbsField pnd_Ws_Pnd_Othr_Cnt;
    private DbsField pnd_Ws_Pnd_Cnt;
    private DbsField pnd_Ws_Pnd_Canadian_Found;
    private DbsField pnd_Trade_Date_A;

    private DbsGroup pnd_Trade_Date_A__R_Field_2;
    private DbsField pnd_Trade_Date_A_Pnd_Trade_Date_D;

    private DbsRecord internalLoopRecord;
    private DbsField read01Twrpymnt_Tax_YearOld;
    private DbsField read01Twrpymnt_Tax_Id_NbrOld;
    private DbsField read01Twrpymnt_Contract_NbrOld;
    private DbsField read01Twrpymnt_Payee_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());
        ldaTwrl9420 = new LdaTwrl9420();
        registerRecord(ldaTwrl9420);
        registerRecord(ldaTwrl9420.getVw_payr());
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Can_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Can_Start", "#CAN-START", FieldType.STRING, 25);
        pnd_Ws_Pnd_Can_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Can_End", "#CAN-END", FieldType.STRING, 25);
        pnd_Ws_Pnd_Super_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Super_Start", "#SUPER-START", FieldType.STRING, 5);
        pnd_Ws_Pnd_Super_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Super_End", "#SUPER-END", FieldType.STRING, 5);
        pnd_Ws_Pnd_Param_Tax_Year_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Param_Tax_Year_A", "#PARAM-TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Param_Tax_Year_A);
        pnd_Ws_Pnd_Param_Tax_Year = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Param_Tax_Year", "#PARAM-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_In = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_In", "#IN", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Rec_Ritten = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Ritten", "#REC-RITTEN", FieldType.NUMERIC, 9);
        pnd_Ws_Pnd_Rec_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Ws_Pnd_Cref_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Life_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Life_Cnt", "#LIFE-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Tiaa_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Tcii_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Tcii_Cnt", "#TCII-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Fsbc_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Fsbc_Cnt", "#FSBC-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Trst_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Trst_Cnt", "#TRST-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Othr_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Canadian_Found = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Canadian_Found", "#CANADIAN-FOUND", FieldType.BOOLEAN, 1);
        pnd_Trade_Date_A = localVariables.newFieldInRecord("pnd_Trade_Date_A", "#TRADE-DATE-A", FieldType.STRING, 4);

        pnd_Trade_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Trade_Date_A__R_Field_2", "REDEFINE", pnd_Trade_Date_A);
        pnd_Trade_Date_A_Pnd_Trade_Date_D = pnd_Trade_Date_A__R_Field_2.newFieldInGroup("pnd_Trade_Date_A_Pnd_Trade_Date_D", "#TRADE-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        read01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("Read01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 4);
        read01Twrpymnt_Tax_Id_NbrOld = internalLoopRecord.newFieldInRecord("Read01_Twrpymnt_Tax_Id_Nbr_OLD", "Twrpymnt_Tax_Id_Nbr_OLD", FieldType.STRING, 
            10);
        read01Twrpymnt_Contract_NbrOld = internalLoopRecord.newFieldInRecord("Read01_Twrpymnt_Contract_Nbr_OLD", "Twrpymnt_Contract_Nbr_OLD", FieldType.STRING, 
            8);
        read01Twrpymnt_Payee_CdeOld = internalLoopRecord.newFieldInRecord("Read01_Twrpymnt_Payee_Cde_OLD", "Twrpymnt_Payee_Cde_OLD", FieldType.STRING, 
            2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl9415.initializeValues();
        ldaTwrl9420.initializeValues();
        ldaTwrl0900.initializeValues();
        ldaTwrl0901.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0900() throws Exception
    {
        super("Twrp0900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Ws_Pnd_Super_Start.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                         //Natural: ASSIGN #WS.#SUPER-START := #WS.#SUPER-END := #PARAM-TAX-YEAR-A := #TWRP0600-TAX-YEAR-CCYY
        pnd_Ws_Pnd_Super_End.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());
        pnd_Ws_Pnd_Param_Tax_Year_A.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Super_Start,5,1);                                                                                          //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#SUPER-START,5,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Super_End,5,1);                                                                                           //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#SUPER-END,5,1 )
        ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year().setValue(pnd_Ws_Pnd_Param_Tax_Year);                                                                             //Natural: ASSIGN #FF1-TAX-YEAR := #PARAM-TAX-YEAR
        getWorkFiles().write(2, false, ldaTwrl0901.getPnd_Flat_File1());                                                                                                  //Natural: WRITE WORK FILE 02 #FLAT-FILE1
        ldaTwrl9420.getVw_payr().startDatabaseRead                                                                                                                        //Natural: READ PAYR BY TWRPYMNT-FORM-SD = #WS.#SUPER-START THRU #WS.#SUPER-END
        (
        "READ01",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Ws_Pnd_Super_Start, "And", WcType.BY) ,
        new Wc("TWRPYMNT_FORM_SD", "<=", pnd_Ws_Pnd_Super_End, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        boolean endOfDataRead01 = true;
        boolean firstRead01 = true;
        READ01:
        while (condition(ldaTwrl9420.getVw_payr().readNextRow("READ01")))
        {
            if (condition(ldaTwrl9420.getVw_payr().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataRead01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Ws_Pnd_Rec_Read.nadd(1);                                                                                                                                  //Natural: AT BREAK OF TWRPYMNT-FORM-SD /24/;//Natural: ADD 1 TO #REC-READ
            pnd_Ws_Pnd_Cnt.setValue(1);                                                                                                                                   //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pymnt_Group().setValuesByName(ldaTwrl9420.getVw_payr());                                                                        //Natural: MOVE BY NAME PAYR TO #XTAXYR-F94.PYMNT-GROUP
                                                                                                                                                                          //Natural: PERFORM DETERMINE-FORM-COMPANY
            sub_Determine_Form_Company();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments().setValue(ldaTwrl9420.getPayr_Count_Casttwrpymnt_Payments());                                          //Natural: ASSIGN #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS := PAYR.C*TWRPYMNT-PAYMENTS
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Pymnt_Occur().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).setValuesByName(ldaTwrl9420.getPayr_Twrpymnt_Payments().getValue(1, //Natural: MOVE BY NAME PAYR.TWRPYMNT-PAYMENTS ( 1:#C-TWRPYMNT-PAYMENTS ) TO #XTAXYR-F94.#PYMNT-OCCUR ( 1:#C-TWRPYMNT-PAYMENTS )
                ":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).setValue(ldaTwrl9420.getPayr_Twrpymnt_Fatca_Ind());                                            //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) := PAYR.TWRPYMNT-FATCA-IND
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Giin().getValue(1).setValue(ldaTwrl9420.getPayr_Twrpymnt_Giin());                                                      //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-GIIN ( 1 ) := PAYR.TWRPYMNT-GIIN
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Tax_Rate().getValue("*").setValue(ldaTwrl9420.getPayr_Twrpymnt_Chptr4_Tax_Rate().getValue("*"));                //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-CHPTR4-TAX-RATE ( * ) := PAYR.TWRPYMNT-CHPTR4-TAX-RATE ( * )
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Exempt_Cde().getValue("*").setValue(ldaTwrl9420.getPayr_Twrpymnt_Chptr4_Exempt_Cde().getValue("*"));            //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-CHPTR4-EXEMPT-CDE ( * ) := PAYR.TWRPYMNT-CHPTR4-EXEMPT-CDE ( * )
            FOR01:                                                                                                                                                        //Natural: FOR #IN = 1 TO #C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_In.setValue(1); condition(pnd_Ws_Pnd_In.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_In.nadd(1))
            {
                if (condition(!ldaTwrl9420.getPayr_Twrpymnt_Additions_2().getValue(pnd_Ws_Pnd_In).getSubstring(1,5).equals(" ")))                                         //Natural: IF SUBSTR ( PAYR.TWRPYMNT-ADDITIONS-2 ( #IN ) ,1,5 ) NE ' '
                {
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_In)); //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #IN ) := PAYR.TWRPYMNT-NRA-REFUND-AMT ( #IN )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Nra_Refund_Amt().getValue(pnd_Ws_Pnd_In).reset();                                                              //Natural: RESET #XTAXYR-F94.TWRPYMNT-NRA-REFUND-AMT ( #IN )
                }                                                                                                                                                         //Natural: END-IF
                //*  SUNY/CUNY
                ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Omni_Plan().getValue(pnd_Ws_Pnd_In));         //Natural: ASSIGN #XTAXYR-F94.#OMNI-PLAN ( #IN ) := PAYR.TWRPYMNT-OMNI-PLAN ( #IN )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Subplan().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Subplan().getValue(pnd_Ws_Pnd_In));        //Natural: ASSIGN #XTAXYR-F94.#OMNI-SUBPLAN ( #IN ) := PAYR.TWRPYMNT-SUBPLAN ( #IN )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Check_Reason().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Check_Reason().getValue(pnd_Ws_Pnd_In));   //Natural: ASSIGN #XTAXYR-F94.#CHECK-REASON ( #IN ) := PAYR.TWRPYMNT-CHECK-REASON ( #IN )
                //*  RCC
                if (condition(!ldaTwrl9420.getPayr_Twrpymnt_Additions_4().getValue(pnd_Ws_Pnd_In).getSubstring(1,6).equals(" ")))                                         //Natural: IF SUBSTR ( PAYR.TWRPYMNT-ADDITIONS-4 ( #IN ) ,1,6 ) NE ' '
                {
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Irr_Amt().getValue(pnd_Ws_Pnd_In));    //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-IRR-AMT ( #IN ) := PAYR.TWRPYMNT-IRR-AMT ( #IN )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(pnd_Ws_Pnd_In).setValue(0);                                                                 //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-IRR-AMT ( #IN ) := 0
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Contract().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Orig_Contract_Nbr().getValue(pnd_Ws_Pnd_In)); //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGNTNG-CONTRACT ( #IN ) := PAYR.TWRPYMNT-ORIG-CONTRACT-NBR ( #IN )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Subplan().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Orig_Subplan().getValue(pnd_Ws_Pnd_In)); //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGNTNG-SUBPLAN ( #IN ) := PAYR.TWRPYMNT-ORIG-SUBPLAN ( #IN )
                //*  M001
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Error_Reason().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Error_Reason().getValue(pnd_Ws_Pnd_In)); //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ERROR-REASON ( #IN ) := PAYR.TWRPYMNT-ERROR-REASON ( #IN )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Origin_Area().getValue(pnd_Ws_Pnd_In).setValue(ldaTwrl9420.getPayr_Twrpymnt_Orign_Area().getValue(pnd_Ws_Pnd_In)); //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORIGIN-AREA ( #IN ) := PAYR.TWRPYMNT-ORIGN-AREA ( #IN )
                if (condition(ldaTwrl9420.getPayr_Twrpymnt_Additions_Tops().getValue(pnd_Ws_Pnd_In).getSubstring(11,4).equals(" ")))                                      //Natural: IF SUBSTRING ( PAYR.TWRPYMNT-ADDITIONS-TOPS ( #IN ) ,11,4 ) = ' '
                {
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Trade_Date().getValue(pnd_Ws_Pnd_In).reset();                                                                       //Natural: RESET #XTAXYR-F94.#TRADE-DATE ( #IN )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Trade_Date_A.setValue(ldaTwrl9420.getPayr_Twrpymnt_Trade_Date().getValue(pnd_Ws_Pnd_In));                                                         //Natural: ASSIGN #TRADE-DATE-A := PAYR.TWRPYMNT-TRADE-DATE ( #IN )
                    ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Trade_Date().getValue(pnd_Ws_Pnd_In).setValue(pnd_Trade_Date_A_Pnd_Trade_Date_D);                                   //Natural: ASSIGN #XTAXYR-F94.#TRADE-DATE ( #IN ) := #TRADE-DATE-D
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9420.getPayr_Twrpymnt_Can_Wthld_Amt().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).notEquals(new              //Natural: IF PAYR.TWRPYMNT-CAN-WTHLD-AMT ( 1:#C-TWRPYMNT-PAYMENTS ) NE 0.00
                DbsDecimal("0.00"))))
            {
                pnd_Ws_Pnd_Canadian_Found.setValue(true);                                                                                                                 //Natural: ASSIGN #WS.#CANADIAN-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, true, ldaTwrl0900.getPnd_Xtaxyr_F94_Pymnt_Group(), ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Pymnt_Occur().getValue(1,":",                    //Natural: WRITE WORK FILE 01 VARIABLE #XTAXYR-F94.PYMNT-GROUP #XTAXYR-F94.#PYMNT-OCCUR ( 1:#C-TWRPYMNT-PAYMENTS )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94().reset();                                                                                                                      //Natural: RESET #XTAXYR-F94
            pnd_Ws_Pnd_Rec_Ritten.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-RITTEN
            pnd_Ws_Pnd_Cnt.setValue(2);                                                                                                                                   //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            read01Twrpymnt_Tax_YearOld.setValue(ldaTwrl9420.getPayr_Twrpymnt_Tax_Year());                                                                                 //Natural: AT END OF DATA;//Natural: END-READ
            read01Twrpymnt_Tax_Id_NbrOld.setValue(ldaTwrl9420.getPayr_Twrpymnt_Tax_Id_Nbr());
            read01Twrpymnt_Contract_NbrOld.setValue(ldaTwrl9420.getPayr_Twrpymnt_Contract_Nbr());
            read01Twrpymnt_Payee_CdeOld.setValue(ldaTwrl9420.getPayr_Twrpymnt_Payee_Cde());
        }
        if (condition(ldaTwrl9420.getVw_payr().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead01(endOfDataRead01);
        }
        if (condition(ldaTwrl9420.getVw_payr().getAtEndOfData()))
        {
            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"TOTAL RECORDS READ         : ",pnd_Ws_Pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS          : ",    //Natural: WRITE ( 1 ) NOTITLE NOHDR 'TOTAL RECORDS READ         : ' #REC-READ / '  1. CREF RECORDS          : ' #CREF-CNT ( 1 ) / '  2. LIFE RECORDS          : ' #LIFE-CNT ( 1 ) / '  3. TIAA RECORDS          : ' #TIAA-CNT ( 1 ) / '  4. TCII RECORDS          : ' #TCII-CNT ( 1 ) / '  5. FSB  RECORDS          : ' #FSBC-CNT ( 1 ) / '  6. TRUST RECORDS         : ' #TRST-CNT ( 1 ) / '  7. OTHER CO. RECORDS     : ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS WRITTEN      : ' #REC-RITTEN / '  1. CREF RECORDS          : ' #CREF-CNT ( 2 ) / '  2. LIFE RECORDS          : ' #LIFE-CNT ( 2 ) / '  3. TIAA RECORDS          : ' #TIAA-CNT ( 2 ) / '  4. TCII RECORDS          : ' #TCII-CNT ( 2 ) / '  5. FSB  RECORDS          : ' #FSBC-CNT ( 2 ) / '  6. TRUST RECORDS         : ' #TRST-CNT ( 2 ) / '  7. OTHER CO. RECORDS     : ' #OTHR-CNT ( 2 )
                pnd_Ws_Pnd_Cref_Cnt.getValue(1),NEWLINE,"  2. LIFE RECORDS          : ",pnd_Ws_Pnd_Life_Cnt.getValue(1),NEWLINE,"  3. TIAA RECORDS          : ",
                pnd_Ws_Pnd_Tiaa_Cnt.getValue(1),NEWLINE,"  4. TCII RECORDS          : ",pnd_Ws_Pnd_Tcii_Cnt.getValue(1),NEWLINE,"  5. FSB  RECORDS          : ",
                pnd_Ws_Pnd_Fsbc_Cnt.getValue(1),NEWLINE,"  6. TRUST RECORDS         : ",pnd_Ws_Pnd_Trst_Cnt.getValue(1),NEWLINE,"  7. OTHER CO. RECORDS     : ",
                pnd_Ws_Pnd_Othr_Cnt.getValue(1),NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS WRITTEN      : ",pnd_Ws_Pnd_Rec_Ritten,NEWLINE,"  1. CREF RECORDS          : ",
                pnd_Ws_Pnd_Cref_Cnt.getValue(2),NEWLINE,"  2. LIFE RECORDS          : ",pnd_Ws_Pnd_Life_Cnt.getValue(2),NEWLINE,"  3. TIAA RECORDS          : ",
                pnd_Ws_Pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  4. TCII RECORDS          : ",pnd_Ws_Pnd_Tcii_Cnt.getValue(2),NEWLINE,"  5. FSB  RECORDS          : ",
                pnd_Ws_Pnd_Fsbc_Cnt.getValue(2),NEWLINE,"  6. TRUST RECORDS         : ",pnd_Ws_Pnd_Trst_Cnt.getValue(2),NEWLINE,"  7. OTHER CO. RECORDS     : ",
                pnd_Ws_Pnd_Othr_Cnt.getValue(2));
            if (condition(Global.isEscape())) return;
            //*   DY1 FIX BEGINS >>
            //*      '  5. TRUST RECORDS         : ' #TRST-CNT(1)    /
            //*      '  6. OTHER CO. RECORDS     : ' #OTHR-CNT(1)   ///
            //*   DY1 FIX ENDS   <<
            //*   DY1 FIX BEGINS >>
            //*      '  5. TRUST RECORDS         : ' #TRST-CNT(2)    /
            //*      '  6. OTHER CO. RECORDS     : ' #OTHR-CNT(2)
            //*   DY1 FIX ENDS   <<
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ****************************
        //* *********************************
        //* *********************************
        //*  SUNY/CUNY
        //* ***************************************
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*   DY1
        short decideConditionsMet616 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF PAYR.TWRPYMNT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("C"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Cref_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("L"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Life_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #LIFE-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("T"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Tiaa_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("S"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Tcii_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #TCII-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("F"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Fsbc_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #FSBC-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl9420.getPayr_Twrpymnt_Company_Cde().equals("X"))))
        {
            decideConditionsMet616++;
            pnd_Ws_Pnd_Trst_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #TRST-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Ws_Pnd_Othr_Cnt.getValue(pnd_Ws_Pnd_Cnt).nadd(1);                                                                                                         //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Canadian() throws Exception                                                                                                                  //Natural: PROCESS-CANADIAN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Can_End.setValue(pnd_Ws_Pnd_Can_Start);                                                                                                                //Natural: ASSIGN #WS.#CAN-END := #WS.#CAN-START
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Can_Start,25,1);                                                                                           //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#CAN-START,25,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Can_End,25,1);                                                                                            //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#CAN-END,25,1 )
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY BY TWRPYMNT-FORM-SD = #WS.#CAN-START THRU #WS.#CAN-END
        (
        "READ02",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Ws_Pnd_Can_Start, "And", WcType.BY) ,
        new Wc("TWRPYMNT_FORM_SD", "<=", pnd_Ws_Pnd_Can_End, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        READ02:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("READ02")))
        {
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pymnt_Group().setValuesByName(ldaTwrl9415.getVw_pay());                                                                         //Natural: MOVE BY NAME PAY TO #XTAXYR-F94.PYMNT-GROUP
                                                                                                                                                                          //Natural: PERFORM DETERMINE-FORM-COMPANY
            sub_Determine_Form_Company();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments().setValue(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments());                                           //Natural: ASSIGN #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS := PAY.C*TWRPYMNT-PAYMENTS
            ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Pymnt_Occur().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).setValuesByName(ldaTwrl9415.getPay_Twrpymnt_Payments().getValue(1, //Natural: MOVE BY NAME PAY.TWRPYMNT-PAYMENTS ( 1:#C-TWRPYMNT-PAYMENTS ) TO #XTAXYR-F94.#PYMNT-OCCUR ( 1:#C-TWRPYMNT-PAYMENTS )
                ":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fatca_Ind().getValue(1).setValue(ldaTwrl9415.getPay_Twrpymnt_Fatca_Ind());                                             //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-FATCA-IND ( 1 ) := PAY.TWRPYMNT-FATCA-IND
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Giin().getValue(1).setValue(ldaTwrl9415.getPay_Twrpymnt_Giin());                                                       //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-GIIN ( 1 ) := PAY.TWRPYMNT-GIIN
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Tax_Rate().getValue("*").setValue(ldaTwrl9415.getPay_Twrpymnt_Chptr4_Tax_Rate().getValue("*"));                 //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-CHPTR4-TAX-RATE ( * ) := PAY.TWRPYMNT-CHPTR4-TAX-RATE ( * )
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Chptr4_Exempt_Cde().getValue("*").setValue(ldaTwrl9415.getPay_Twrpymnt_Chptr4_Exempt_Cde().getValue("*"));             //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-CHPTR4-EXEMPT-CDE ( * ) := PAY.TWRPYMNT-CHPTR4-EXEMPT-CDE ( * )
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Irr_Amt().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).setValue(ldaTwrl9415.getPay_Twrpymnt_Irr_Amt().getValue(1, //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-IRR-AMT ( 1:#C-TWRPYMNT-PAYMENTS ) := PAY.TWRPYMNT-IRR-AMT ( 1:#C-TWRPYMNT-PAYMENTS )
                ":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Contract().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Contract_Nbr().getValue(1, //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGNTNG-CONTRACT ( 1:#C-TWRPYMNT-PAYMENTS ) := PAY.TWRPYMNT-ORIG-CONTRACT-NBR ( 1:#C-TWRPYMNT-PAYMENTS )
                ":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgntng_Subplan().getValue(1,":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()).setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Subplan().getValue(1, //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-ORGNTNG-SUBPLAN ( 1:#C-TWRPYMNT-PAYMENTS ) := PAY.TWRPYMNT-ORIG-SUBPLAN ( 1:#C-TWRPYMNT-PAYMENTS )
                ":",ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            getWorkFiles().write(4, true, ldaTwrl0900.getPnd_Xtaxyr_F94_Pymnt_Group(), ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Pymnt_Occur().getValue(1,":",                    //Natural: WRITE WORK FILE 04 VARIABLE #XTAXYR-F94.PYMNT-GROUP #XTAXYR-F94.#PYMNT-OCCUR ( 1:#C-TWRPYMNT-PAYMENTS )
                ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()));
            ldaTwrl0900.getPnd_Xtaxyr_F94().reset();                                                                                                                      //Natural: RESET #XTAXYR-F94
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Form_Company() throws Exception                                                                                                            //Natural: DETERMINE-FORM-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        short decideConditionsMet656 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-TAX-YEAR;//Natural: VALUE 2004:9999
        if (condition(((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year().greaterOrEqual(2004) && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year().lessOrEqual(9999)))))
        {
            decideConditionsMet656++;
            if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("X")))                                                                              //Natural: IF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE = 'X'
            {
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().setValue("X");                                                                                  //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM := 'X'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().setValue("T");                                                                                  //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM := 'T'
                //*  TAX YEAR <= 2002
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2003
        else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year().equals(2003))))
        {
            decideConditionsMet656++;
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().setValue("S");                                                                                      //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM := 'S'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde());                                     //Natural: ASSIGN #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM := #XTAXYR-F94.TWRPYMNT-COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 34T 'TAX WITHHOLDING AND REPORTING SYSTEM' 119T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'EXTRACTED PAYMENT RECORDS' / 'RUNTIME : ' *TIMX 44T 'TAX YEAR ' #PARAM-TAX-YEAR / 44T 'CONTROL TOTALS'
                        TabSetting(119),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(39),"EXTRACTED PAYMENT RECORDS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(44),"TAX YEAR ",pnd_Ws_Pnd_Param_Tax_Year,NEWLINE,new 
                        TabSetting(44),"CONTROL TOTALS");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRead01() throws Exception {atBreakEventRead01(false);}
    private void atBreakEventRead01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl9420_getPayr_Twrpymnt_Form_Sd24IsBreak = ldaTwrl9420.getPayr_Twrpymnt_Form_Sd().isBreak(endOfData);
        if (condition(ldaTwrl9420_getPayr_Twrpymnt_Form_Sd24IsBreak))
        {
            if (condition(pnd_Ws_Pnd_Canadian_Found.getBoolean()))                                                                                                        //Natural: IF #WS.#CANADIAN-FOUND
            {
                pnd_Ws_Pnd_Can_Start.setValueEdited(read01Twrpymnt_Tax_YearOld,new ReportEditMask("9999"));                                                               //Natural: MOVE EDITED OLD ( PAYR.TWRPYMNT-TAX-YEAR ) ( EM = 9999 ) TO #WS.#CAN-START
                setValueToSubstring(read01Twrpymnt_Tax_Id_NbrOld,pnd_Ws_Pnd_Can_Start,5,10);                                                                              //Natural: MOVE OLD ( PAYR.TWRPYMNT-TAX-ID-NBR ) TO SUBSTR ( #CAN-START,5,10 )
                setValueToSubstring(read01Twrpymnt_Contract_NbrOld,pnd_Ws_Pnd_Can_Start,15,8);                                                                            //Natural: MOVE OLD ( PAYR.TWRPYMNT-CONTRACT-NBR ) TO SUBSTR ( #CAN-START,15,8 )
                setValueToSubstring(read01Twrpymnt_Payee_CdeOld,pnd_Ws_Pnd_Can_Start,23,2);                                                                               //Natural: MOVE OLD ( PAYR.TWRPYMNT-PAYEE-CDE ) TO SUBSTR ( #CAN-START,23,2 )
                                                                                                                                                                          //Natural: PERFORM PROCESS-CANADIAN
                sub_Process_Canadian();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Pnd_Canadian_Found.reset();                                                                                                                        //Natural: RESET #WS.#CANADIAN-FOUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
