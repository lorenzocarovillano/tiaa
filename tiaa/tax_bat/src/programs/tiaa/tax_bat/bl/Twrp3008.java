/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:19 PM
**        * FROM NATURAL PROGRAM : Twrp3008
************************************************************
**        * FILE NAME            : Twrp3008.java
**        * CLASS NAME           : Twrp3008
**        * INSTANCE NAME        : Twrp3008
************************************************************
************************************************************************
*
* PROGRAM  : TWRP3640
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : NEW YORK QUARTERLY TAPE FILE, & PRINT DUMP REPORT.
* CREATED  : 02 / 01 / 2001.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS ORIGINAL NEW YORK STATE REPORTING TAPE FILE,
*            AND PRODUCES A PRINTED DUMP OF ALL THE RECORDS ON FILE.
*            PROGRAM PRODUCES THE ORIGINAL NEW YORK REPORTING TAPE.
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3008 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3640 ldaTwrl3640;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_1;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_2;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;

    private DbsGroup pnd_Input_Parm__R_Field_3;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Columns;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Filler01;
    private DbsField pnd_Filler02;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3640 = new LdaTwrl3640();
        registerRecord(ldaTwrl3640);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_1", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_2 = pnd_Input_Parm__R_Field_1.newGroupInGroup("pnd_Input_Parm__R_Field_2", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_Input_Parm__R_Field_3 = pnd_Input_Parm__R_Field_2.newGroupInGroup("pnd_Input_Parm__R_Field_3", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Cc", "#PYMNT-DATE-TO-CC", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yy", "#PYMNT-DATE-TO-YY", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_2.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_1.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Columns = localVariables.newFieldInRecord("pnd_Columns", "#COLUMNS", FieldType.STRING, 100);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 8);
        pnd_Filler01 = localVariables.newFieldInRecord("pnd_Filler01", "#FILLER01", FieldType.STRING, 225);
        pnd_Filler02 = localVariables.newFieldInRecord("pnd_Filler02", "#FILLER02", FieldType.STRING, 250);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 4);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 4);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 4);
        l = localVariables.newFieldInRecord("l", "L", FieldType.PACKED_DECIMAL, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3640.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3008() throws Exception
    {
        super("Twrp3008");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3008|Main");
        OnErrorManager.pushEvent("TWRP3008", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133;//Natural: FORMAT ( 03 ) PS = 60 LS = 133;//Natural: FORMAT ( 04 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                           //Natural: IF #YEAR-END-ADJ-IND = 'Y'
                {
                    getReports().print(0, "END-OF-YEAR 5TH QTR ADJUSTMENTS ONLY -","NO TAPE WILL BE GENERATED");                                                          //Natural: PRINT ( 00 ) 'END-OF-YEAR 5TH QTR ADJUSTMENTS ONLY -' 'NO TAPE WILL BE GENERATED'
                    getReports().print(1, "END-OF-YEAR 5TH QTR ADJUSTMENTS ONLY -","NO TAPE WILL BE GENERATED");                                                          //Natural: PRINT ( 01 ) 'END-OF-YEAR 5TH QTR ADJUSTMENTS ONLY -' 'NO TAPE WILL BE GENERATED'
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                    sub_End_Of_Program_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Columns.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "----+----1----+----2----+----3----+----4----+----5", "----+----6----+----7----+----8----+----9----+----0")); //Natural: COMPRESS '----+----1----+----2----+----3----+----4----+----5' '----+----6----+----7----+----8----+----9----+----0' INTO #COLUMNS LEAVING NO SPACE
                RD1:                                                                                                                                                      //Natural: READ WORK FILE 01 #OUT1-NY-REC1
                while (condition(getWorkFiles().read(1, ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1())))
                {
                    pnd_Read_Ctr.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #READ-CTR
                    //*                                                                                                                                                   //Natural: DECIDE ON FIRST VALUE OF #OUT1A-NY-ID
                    short decideConditionsMet213 = 0;                                                                                                                     //Natural: VALUES '1A'
                    if (condition((ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().equals("1A"))))
                    {
                        decideConditionsMet213++;
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank().moveAll(".");                                                                              //Natural: MOVE ALL '.' TO #OUT1A-NY-BLANK
                    }                                                                                                                                                     //Natural: VALUES '1E'
                    else if (condition((ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().equals("1E"))))
                    {
                        decideConditionsMet213++;
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1E-NY-BLANK1
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1E-NY-BLANK2
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank().moveAll(".");                                                                              //Natural: MOVE ALL '.' TO #OUT1E-NY-BLANK
                    }                                                                                                                                                     //Natural: VALUES '1W'
                    else if (condition((ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().equals("1W"))))
                    {
                        decideConditionsMet213++;
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1W-NY-BLANK1
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1W-NY-BLANK2
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1W-NY-BLANK3
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1W-NY-BLANK4
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1W-NY-BLANK5
                    }                                                                                                                                                     //Natural: VALUES '1T'
                    else if (condition((ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().equals("1T"))))
                    {
                        decideConditionsMet213++;
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank().moveAll(".");                                                                              //Natural: MOVE ALL '.' TO #OUT1T-NY-BLANK
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1T-NY-BLANK1
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1T-NY-BLANK2
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3().moveAll(".");                                                                             //Natural: MOVE ALL '.' TO #OUT1T-NY-BLANK3
                    }                                                                                                                                                     //Natural: VALUES '1F'
                    else if (condition((ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().equals("1F"))))
                    {
                        decideConditionsMet213++;
                        ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank().moveAll(".");                                                                              //Natural: MOVE ALL '.' TO #OUT1F-NY-BLANK
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_D100().setValue(ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_I100());                                                  //Natural: ASSIGN #D100 := #I100
                    ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_D28().setValue(ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_I28());                                                    //Natural: ASSIGN #D28 := #I28
                    j.setValue(1);                                                                                                                                        //Natural: ASSIGN J := 1
                    k.setValue(100);                                                                                                                                      //Natural: ASSIGN K := 100
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(7),pnd_Columns);                                                         //Natural: WRITE ( 01 ) NOTITLE NOHDR 07T #COLUMNS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR01:                                                                                                                                                //Natural: FOR I = 1 TO 1
                    for (i.setValue(1); condition(i.lessOrEqual(1)); i.nadd(1))
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_D100(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D100 109T K ( EM = ZZZ9 )
                            TabSetting(109),k, new ReportEditMask ("ZZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        j.nadd(100);                                                                                                                                      //Natural: ASSIGN J := J + 100
                        k.nadd(100);                                                                                                                                      //Natural: ASSIGN K := K + 100
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    j.setValue(101);                                                                                                                                      //Natural: ASSIGN J := 101
                    k.setValue(128);                                                                                                                                      //Natural: ASSIGN K := 128
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),j, new ReportEditMask ("ZZZ9"),new TabSetting(7),ldaTwrl3640.getPnd_Out1_Ny_Record_Pnd_D28(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T J ( EM = ZZZ9 ) 07T #D28 109T K ( EM = ZZZ9 )
                        TabSetting(109),k, new ReportEditMask ("ZZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-WORK
                RD1_Exit:
                if (Global.isEscape()) return;
                //* *------
                if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                            //Natural: IF #READ-CTR = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(6),"NY QUARTERLY    TAPE FILE (WORK FILE 01) IS EMPTY",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'NY QUARTERLY    TAPE FILE (WORK FILE 01) IS EMPTY' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                        TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
                sub_End_Of_Program_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *------------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                //* *-------                                                                                                                                              //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"NUMBER OF NY QUARTERLY    RECORDS FOUND.......",pnd_Read_Ctr,                    //Natural: WRITE ( 00 ) NOTITLE NOHDR 01T 'NUMBER OF NY QUARTERLY    RECORDS FOUND.......' #READ-CTR
            new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),"NUMBER OF NY QUARTERLY    RECORDS FOUND.......",pnd_Read_Ctr,                    //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T 'NUMBER OF NY QUARTERLY    RECORDS FOUND.......' #READ-CTR
            new ReportEditMask ("ZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(43),"       Tax Withholding & Reporting System       ",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 43T '       Tax Withholding & Reporting System       ' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(43),"NEW YORK QUARTERLY TAPE FILE DUMP",new   //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 43T 'NEW YORK QUARTERLY TAPE FILE DUMP' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");
        Global.format(3, "PS=60 LS=133");
        Global.format(4, "PS=60 LS=133");
    }
}
