/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:20 PM
**        * FROM NATURAL PROGRAM : Twrp1739
************************************************************
**        * FILE NAME            : Twrp1739.java
**        * CLASS NAME           : Twrp1739
**        * INSTANCE NAME        : Twrp1739
************************************************************
************************************************************************
*
*  PROGRAM :  TWRP1739
*  SYSTEM  :  TAX REPORTING SYSTEM - TAXWARS
*  FUNCTION:  REFORMAT OMNI TRANSACTION FILES FOR THESE SOURCE CODES:
*
*             1.  SOURCE NZ - ADAS
*             2.  SOURCE DC - IA DEATH CLAIMS
*             3.  SOURCE EW - GENERIC WARRANTS
*
*  UPDATED :
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1739 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_P_Year;

    private DbsGroup pnd_P_Year__R_Field_1;
    private DbsField pnd_P_Year_Pnd_P_Year_N;
    private DbsField pnd_Curr_Year;

    private DbsGroup pnd_Curr_Year__R_Field_2;
    private DbsField pnd_Curr_Year_Pnd_Curr_Year_A;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Last_Bus_Date;
    private DbsField pnd_Last_Bus_Day;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_P_Year = localVariables.newFieldInRecord("pnd_P_Year", "#P-YEAR", FieldType.STRING, 4);

        pnd_P_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_P_Year__R_Field_1", "REDEFINE", pnd_P_Year);
        pnd_P_Year_Pnd_P_Year_N = pnd_P_Year__R_Field_1.newFieldInGroup("pnd_P_Year_Pnd_P_Year_N", "#P-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Curr_Year = localVariables.newFieldInRecord("pnd_Curr_Year", "#CURR-YEAR", FieldType.NUMERIC, 4);

        pnd_Curr_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_Curr_Year__R_Field_2", "REDEFINE", pnd_Curr_Year);
        pnd_Curr_Year_Pnd_Curr_Year_A = pnd_Curr_Year__R_Field_2.newFieldInGroup("pnd_Curr_Year_Pnd_Curr_Year_A", "#CURR-YEAR-A", FieldType.STRING, 4);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Bus_Date = localVariables.newFieldInRecord("pnd_Last_Bus_Date", "#LAST-BUS-DATE", FieldType.DATE);
        pnd_Last_Bus_Day = localVariables.newFieldInRecord("pnd_Last_Bus_Day", "#LAST-BUS-DAY", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0700.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1739() throws Exception
    {
        super("Twrp1739");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 250 PS = 60
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
                                                                                                                                                                          //Natural: PERFORM S001-DISPLAY-RECORD
            sub_S001_Display_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet209 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF TWRT-SOURCE;//Natural: VALUE 'NZ', 'DC'
            if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("NZ") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("DC"))))
            {
                decideConditionsMet209++;
                                                                                                                                                                          //Natural: PERFORM S020-REFORMAT-TWRP0650-2-7
                sub_S020_Reformat_Twrp0650_2_7();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM S030-REFORMAT-TWRP0660-3-9
                sub_S030_Reformat_Twrp0660_3_9();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM S030-REFORMAT-TWRP0660-3-10
                sub_S030_Reformat_Twrp0660_3_10();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM S040-REFORMAT-TWRP0700-4-1
                sub_S040_Reformat_Twrp0700_4_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'EW'
            else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW"))))
            {
                decideConditionsMet209++;
                                                                                                                                                                          //Natural: PERFORM S040-REFORMAT-TWRP0700-4-1
                sub_S040_Reformat_Twrp0700_4_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM S040-REFORMAT-TWRP0700-4-2
                sub_S040_Reformat_Twrp0700_4_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM S050-WRITE-WORK-FILE
            sub_S050_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* * --------------------------------------------------------------------
        //*  'Cntrct/Type'          TWRT-CONTRACT-TYPE
        //* * --------------------------------------------------------------------
        //* * --------------------------------------------------------------------
        //* * --------------------------------------------------------------------
        //* * --------------------------------------------------------------------
        //*        WRITE (1) 25T '=' TWRT-IVC-AMT
        //*        WRITE (1) 25T '=' TWRT-IVC-AMT
        //* * --------------------------------------------------------------------
        //* * --------------------------------------------------------------------
        //* * --------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }
    private void sub_S001_Display_Record() throws Exception                                                                                                               //Natural: S001-DISPLAY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getReports().display(1, "PIN",                                                                                                                                    //Natural: DISPLAY ( 1 ) 'PIN' TWRT-PIN-ID 'Tax-ID/Number' TWRT-TAX-ID 'ID/Tp' TWRT-TAX-ID-TYPE 'Contract/Number' TWRT-CNTRCT-NBR 'Pay/ee' TWRT-PAYEE-CDE 'Plan/Type' TWRT-PLAN-TYPE 'Pay/Date' TWRT-PAYMT-DTE ( SG = OFF ) 'Pay/Type' TWRT-PAYMT-TYPE 'Sett/Type' TWRT-SETTL-TYPE 'Dist/Cd-1' TWRT-DISTRIB-CDE 'Dist/Cd-2' TWRT-DISTRIBUTION-CODE 'Roll/Ind' TWRT-ROLLOVER-IND 'LOB' TWRT-LOB-CDE 'Tax/Type' TWRT-TAX-TYPE 'Gross/Amount' TWRT-GROSS-AMT 'Interest/Amount' TWRT-INTEREST-AMT 'IVC/Cde' TWRT-IVC-CDE 'IVC/AMOUNT' TWRT-IVC-AMT 'Federal/Withholding' TWRT-FED-WHHLD-AMT 'Canadian/Withholding' TWRT-CAN-WHHLD-AMT 'NRA/Withholding' TWRT-NRA-WHHLD-AMT 'State/Withholding' TWRT-STATE-WHHLD-AMT 'Local/Withholding' TWRT-LOCAL-WHHLD-AMT 'Res/Cde' TWRT-STATE-RSDNCY 'Loc/Cde' TWRT-LOCALITY-CDE 'Ctz/Cde' TWRT-CITIZENSHIP 'Plan' TWRT-PLAN-NUM 'SubPlan' TWRT-SUBPLAN 'Rev/Ind' TWRT-OP-REV-IND 'Adj/Ind' TWRT-OP-ADJ-IND 'Mnt/Ind' TWRT-OP-MAINT-IND 'Elc/Cde' TWRT-TAX-ELCT-CDE 'Elc/Cde' TWRT-ELECTION-CODE ( SG = OFF ) 'Rec/Cnt' #READ-CNT ( SG = OFF )
        		ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(),"Tax-ID/Number",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(),"ID/Tp",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),"Contract/Number",
        		ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),"Pay/ee",
        		ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),"Plan/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type(),"Pay/Date",
        		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(), new SignPosition (false),"Pay/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type(),"Sett/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type(),"Dist/Cd-1",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde(),"Dist/Cd-2",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code(),"Roll/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind(),"LOB",
        		ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde(),"Tax/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type(),"Gross/Amount",
        		ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),"Interest/Amount",
        		ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),"IVC/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde(),"IVC/AMOUNT",
        		ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),"Federal/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),"Canadian/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt(),"NRA/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),"State/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),"Local/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),"Res/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),"Loc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde(),"Ctz/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),"Plan",
        		ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),"SubPlan",
        		ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),"Rev/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind(),"Adj/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind(),"Mnt/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind(),"Elc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde(),"Elc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Election_Code(), new SignPosition (false),"Rec/Cnt",
        		pnd_Read_Cnt, new SignPosition (false));
        if (Global.isEscape()) return;
        //*  DISPLAY (2)
        //*  'REC/CNT'              #READ-CNT              (SG=OFF)
        //*  S001-DISPLAY-RECORD
    }
    private void sub_S020_Reformat_Twrp0650_2_7() throws Exception                                                                                                        //Natural: S020-REFORMAT-TWRP0650-2-7
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().notEquals(1) && ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().equals("X")))                          //Natural: IF TWRT-CITIZENSHIP NE 01 AND TWRT-TAX-ELCT-CDE = 'X'
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                                                                             //Natural: IF TWRT-PYMNT-TAX-FORM-1078 = 'Y'
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().notEquals("Y")))                                                                             //Natural: IF TWRT-TAX-ELCT-CDE NE 'Y'
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 2.7a");                                                                         //Natural: WRITE ( 1 ) 25T 'Change 2.7a'
                    if (Global.isEscape()) return;
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde());                                    //Natural: WRITE ( 1 ) 25T '=' TWRT-TAX-ELCT-CDE
                    if (Global.isEscape()) return;
                    ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().setValue("Y");                                                                                         //Natural: ASSIGN TWRT-TAX-ELCT-CDE := 'Y'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde());                                    //Natural: WRITE ( 1 ) 25T '=' TWRT-TAX-ELCT-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y") || ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals(" ")))        //Natural: IF TWRT-PYMNT-TAX-FORM-1001 = 'Y' OR = ' '
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().notEquals("N")))                                                                             //Natural: IF TWRT-TAX-ELCT-CDE NE 'N'
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 2.7b");                                                                         //Natural: WRITE ( 1 ) 25T 'Change 2.7b'
                    if (Global.isEscape()) return;
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde());                                    //Natural: WRITE ( 1 ) 25T '=' TWRT-TAX-ELCT-CDE
                    if (Global.isEscape()) return;
                    ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde().setValue("N");                                                                                         //Natural: ASSIGN TWRT-TAX-ELCT-CDE := 'N'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde());                                    //Natural: WRITE ( 1 ) 25T '=' TWRT-TAX-ELCT-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  S020-REFORMAT-TWRP0650-2-7
    }
    private void sub_S030_Reformat_Twrp0660_3_9() throws Exception                                                                                                        //Natural: S030-REFORMAT-TWRP0660-3-9
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals("2") && ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().equals(getZero())))                              //Natural: IF TWRT-IVC-CDE = '2' AND TWRT-IVC-AMT = 0
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("Z")))                                                                                      //Natural: IF TWRT-IVC-CDE NE 'Z'
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 3.9a");                                                                             //Natural: WRITE ( 1 ) 25T 'Change 3.9a'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("Z");                                                                                                  //Natural: ASSIGN TWRT-IVC-CDE := 'Z'
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  S030-REFORMAT-TWRP0660-3-9
    }
    private void sub_S030_Reformat_Twrp0660_3_10() throws Exception                                                                                                       //Natural: S030-REFORMAT-TWRP0660-3-10
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        //*   NEW YORK CITY
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("3E")))                                                                                       //Natural: IF TWRT-STATE-RSDNCY = '3E'
        {
            //*   NEW YORK STATE
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().notEquals("35")))                                                                                //Natural: IF TWRT-STATE-RSDNCY NE '35'
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 3.10a");                                                                            //Natural: WRITE ( 1 ) 25T 'Change 3.10a'
                if (Global.isEscape()) return;
                //*   NEW YORK STATE
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy());                                        //Natural: WRITE ( 1 ) 25T '=' TWRT-STATE-RSDNCY
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().setValue("35");                                                                                            //Natural: ASSIGN TWRT-STATE-RSDNCY := '35'
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy());                                        //Natural: WRITE ( 1 ) 25T '=' TWRT-STATE-RSDNCY
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  S030-REFORMAT-TWRP0660-3-10
    }
    private void sub_S040_Reformat_Twrp0700_4_1() throws Exception                                                                                                        //Natural: S040-REFORMAT-TWRP0700-4-1
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().equals(getZero())))                                                                                       //Natural: IF TWRT-IVC-AMT = 0
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals("Z")))                                                                                         //Natural: IF TWRT-IVC-CDE = 'Z'
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1a");                                                                             //Natural: WRITE ( 1 ) 25T 'Change 4.1a'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("K");                                                                                                  //Natural: ASSIGN TWRT-IVC-CDE := 'K'
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals("K") || ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals("U"))))                        //Natural: IF NOT ( TWRT-IVC-CDE = 'K' OR = 'U' )
                {
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals(" ")))                                                                                 //Natural: IF TWRT-IVC-CDE = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1b");                                                                     //Natural: WRITE ( 1 ) 25T 'Change 4.1b'
                        if (Global.isEscape()) return;
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                     //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                        if (Global.isEscape()) return;
                        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("U");                                                                                          //Natural: ASSIGN TWRT-IVC-CDE := 'U'
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                     //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().greater(getZero())))                                                                                      //Natural: IF TWRT-IVC-AMT GT 0
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals(" ")))                                                                                         //Natural: IF TWRT-IVC-CDE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1c");                                                                         //Natural: WRITE ( 1 ) 25T 'Change 4.1c'
                    if (Global.isEscape()) return;
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                         //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                    if (Global.isEscape()) return;
                    ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("K");                                                                                              //Natural: ASSIGN TWRT-IVC-CDE := 'K'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                         //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROTH IRA W/D (WITHIN 1ST 5 YRS)
        //*  ROTH CONV W/D - DELETED
        //*  DEATH LUMP SUM & ROTH DEATH W/D
        //*  ROTH SURVIVOR
        if (condition(((((ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N20") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N69")  //Natural: IF ( ( SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'N20' THRU 'N69' OR SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'T20' THRU 'T69' ) AND ( TWRT-PAY-SET-TYPES = 'NR' OR = 'NV' OR = 'DC' ) ) OR ( TWRT-PAY-SET-TYPES = 'AD' )
            <= 0) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T20") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T69") 
            <= 0)) && ((ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NR") || ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NV")) || 
            ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("DC"))) || ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("AD"))))
        {
            //*  OR
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("U")))                                                                                      //Natural: IF TWRT-IVC-CDE NE 'U'
            {
                //*        TWRT-IVC-AMT  NE  0.00
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1d");                                                                             //Natural: WRITE ( 1 ) 25T 'Change 4.1d'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("U");                                                                                                  //Natural: ASSIGN TWRT-IVC-CDE := 'U'
                //*        TWRT-IVC-AMT  :=  0.00
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                //*        WRITE (1) 25T '=' TWRT-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N70") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N99")  //Natural: IF ( SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'N70' THRU 'N99' OR SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'T70' THRU 'T99' ) AND TWRT-PAY-SET-TYPES = 'NX'
            <= 0) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T70") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T99") 
            <= 0)) && ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NX"))))
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                      //Natural: IF TWRT-IVC-CDE NE 'K'
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1e");                                                                             //Natural: WRITE ( 1 ) 25T 'Change 4.1e'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("K");                                                                                                  //Natural: ASSIGN TWRT-IVC-CDE := 'K'
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().less(getZero())))                                                                                         //Natural: IF TWRT-IVC-AMT LT 0
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().equals(" ")))                                                                                         //Natural: IF TWRT-IVC-CDE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("U")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'U'
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1f");                                                                         //Natural: WRITE ( 1 ) 25T 'Change 4.1f'
                    if (Global.isEscape()) return;
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                         //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                    if (Global.isEscape()) return;
                    ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("U");                                                                                              //Natural: ASSIGN TWRT-IVC-CDE := 'U'
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                         //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((((((((ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N70") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("N99")  //Natural: IF ( SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'N70' THRU 'N99' OR SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'T70' THRU 'T99' OR SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'K90' THRU 'K99' OR SUBSTR ( TWRT-CNTRCT-NBR,1,3 ) = 'J90' THRU 'J99' OR SUBSTR ( TWRT-CNTRCT-NBR,1,4 ) = 'K83 ' THRU 'K849' OR SUBSTR ( TWRT-CNTRCT-NBR,1,4 ) = 'J83 ' THRU 'J849' ) AND ( TWRT-PAY-SET-TYPES = 'NC' OR = 'NO' )
            <= 0) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T70") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("T99") 
            <= 0)) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("K90") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("K99") 
            <= 0)) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("J90") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,3).compareTo("J99") 
            <= 0)) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,4).compareTo("K83 ") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,4).compareTo("K849") 
            <= 0)) || (ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,4).compareTo("J83 ") >= 0 && ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().getSubstring(1,4).compareTo("J849") 
            <= 0)) && (ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NC") || ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NO")))))
        {
            //*  OR
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("U")))                                                                                      //Natural: IF TWRT-IVC-CDE NE 'U'
            {
                //*        TWRT-IVC-AMT  NE  0.00
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.1g");                                                                             //Natural: WRITE ( 1 ) 25T 'Change 4.1g'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("U");                                                                                                  //Natural: ASSIGN TWRT-IVC-CDE := 'U'
                //*        TWRT-IVC-AMT  :=  0.00
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde());                                             //Natural: WRITE ( 1 ) 25T '=' TWRT-IVC-CDE
                if (Global.isEscape()) return;
                //*        WRITE (1) 25T '=' TWRT-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  S040-REFORMAT-TWRP0700-4-1
    }
    private void sub_S040_Reformat_Twrp0700_4_2() throws Exception                                                                                                        //Natural: S040-REFORMAT-TWRP0700-4-2
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Election_Code().notEquals(getZero()) || ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate().notEquals(new            //Natural: IF TWRT-ELECTION-CODE NE 0 OR TWRT-FED-NRA-TAX-RATE NE 0.00
            DbsDecimal("0.00"))))
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"Change 4.2a");                                                                                 //Natural: WRITE ( 1 ) 25T 'Change 4.2a'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Election_Code());                                           //Natural: WRITE ( 1 ) 25T '=' TWRT-ELECTION-CODE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate());                                        //Natural: WRITE ( 1 ) 25T '=' TWRT-FED-NRA-TAX-RATE
            if (Global.isEscape()) return;
            ldaTwrl0700.getTwrt_Record_Twrt_Election_Code().setValue(0);                                                                                                  //Natural: ASSIGN TWRT-ELECTION-CODE := 0
            ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate().setValue(0);                                                                                               //Natural: ASSIGN TWRT-FED-NRA-TAX-RATE := 0.00
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Election_Code());                                           //Natural: WRITE ( 1 ) 25T '=' TWRT-ELECTION-CODE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(25),"=",ldaTwrl0700.getTwrt_Record_Twrt_Fed_Nra_Tax_Rate());                                        //Natural: WRITE ( 1 ) 25T '=' TWRT-FED-NRA-TAX-RATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  S040-REFORMAT-TWRP0700-4-2
    }
    private void sub_S050_Write_Work_File() throws Exception                                                                                                              //Natural: S050-WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getWorkFiles().write(2, false, ldaTwrl0700.getTwrt_Record());                                                                                                     //Natural: WRITE WORK FILE 2 TWRT-RECORD
        //*  S050-WRITE-WORK-FILE
    }
    private void sub_S900_Write_Error() throws Exception                                                                                                                  //Natural: S900-WRITE-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* * --------------------------------------------------------------------
        getReports().write(0, "=",pnd_Read_Cnt,NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde(),NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Source(),            //Natural: WRITE '=' #READ-CNT / '=' TWRT-COMPANY-CDE / '=' TWRT-SOURCE / '=' TWRT-CNTRCT-NBR / '=' TWRT-PAYEE-CDE / '=' TWRT-TAX-ID / '=' TWRT-PIN-ID / '=' TWRT-PAYMT-DTE
            NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(),
            NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(),NEWLINE,"=",ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte());
        if (Global.isEscape()) return;
        //*  S900-WRITE-ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program: TWRP1739-01",new                    //Natural: WRITE ( 1 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program: TWRP1739-01' 18X 'Tax Reporting And Withholding System' 30X 'Date: '*DATU / 38X 'Reformat Tax Transactions (EDITS)' 30X 'Page:   ' *PAGE-NUMBER ( 1 ) /
                        ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(38),"Reformat Tax Transactions (EDITS)",new 
                        ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(1),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=250 PS=60");

        getReports().setDisplayColumns(1, "PIN",
        		ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(),"Tax-ID/Number",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(),"ID/Tp",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),"Contract/Number",
        		ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),"Pay/ee",
        		ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),"Plan/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type(),"Pay/Date",
        		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(), new SignPosition (false),"Pay/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type(),"Sett/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type(),"Dist/Cd-1",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde(),"Dist/Cd-2",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code(),"Roll/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind(),"LOB",
        		ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde(),"Tax/Type",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type(),"Gross/Amount",
        		ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),"Interest/Amount",
        		ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),"IVC/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde(),"IVC/AMOUNT",
        		ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),"Federal/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),"Canadian/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt(),"NRA/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),"State/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt(),"Local/Withholding",
        		ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt(),"Res/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),"Loc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde(),"Ctz/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),"Plan",
        		ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),"SubPlan",
        		ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),"Rev/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind(),"Adj/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind(),"Mnt/Ind",
        		ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind(),"Elc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Tax_Elct_Cde(),"Elc/Cde",
        		ldaTwrl0700.getTwrt_Record_Twrt_Election_Code(), new SignPosition (false),"Rec/Cnt",
        		pnd_Read_Cnt, new SignPosition (false));
    }
}
