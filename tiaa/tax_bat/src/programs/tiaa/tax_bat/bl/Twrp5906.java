/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:42:15 PM
**        * FROM NATURAL PROGRAM : Twrp5906
************************************************************
**        * FILE NAME            : Twrp5906.java
**        * CLASS NAME           : Twrp5906
**        * INSTANCE NAME        : Twrp5906
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: 1042-S IRS ORIGINAL REPORTING
**SAG SYSTEM: TAX
************************************************************************
* PROGRAM  : TWRP5906
* SYSTEM   : TAX
* TITLE    : 1042-S IRS ORIGINAL REPORTING
* GENERATED: NOV 11,99 AT 10:37 AM
* FUNCTION : THIS PROGRAM CREATES 1042-S IRS TAPES FOR ORIGINAL
*          | REPORTING
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 07/19/00  F.ORTIZ - FIXED SUBSCRIPT USED IN TOTALING TAXES WITHHELD.
*                     CHANGED (1:#C-1042S) TO (#C-1042S)
* 11/21/00  F.ORTIZ - REPLACED TAX RATE FIELD WITH NEW FORM FILE
*                     TIRF-1042-S-TAX-RATE.
* 11/21/00  F.ORTIZ - SET FOREIGN STUDENT WITHHOLDING ALLOWANCE AND
*                     NET INCOME AMOUNT TO BLANK. REMOVE NET INCOME AMT
*                     FIELDS FROM THE REJECT AND ACCEPT REPORTS.
* 11/21/00  F.ORTIZ - ADD FILE RECORD COUNT TO CONTROL REPORT.
* 11/21/00  F.ORTIZ - ADD TEST INDICATOR PARAMETER FOR TESTING WITH IRS.
* 01/18/01  F.ORTIZ - REMOVED UNNECESSARY LOGIC AFTER READ CONTROL FILE.
* 11/09/01  R.MA    - IRS HAS NEW 1042-S LAYOUT MANDATED FOR YEAR 2001.
*                     CHANGE BOTH PDA AND PROGRAM ACCORDINGLY.
* 12/04/01  F.ORTIZ - FIXED STATE LOOKUP TO BYPASS NON-NUMERIC CODES.
* 12/27/01  F.ORTIZ - ADD COUNTRY NAME LOOKUP FOR IRS COUNTRY CODE.
* 01/16/02  F.ORTIZ - REPLACED WITHHOLDING AGENT's phone number with
*                     CONTACT NUMBER FOUND ON T RECORD.
* 01/16/02  F.ORTIZ - REPLACED ALPHA BOX NUMBERS WHICH APPLIED TO LAST
*                     YEARS REPORT TO NUMERIC VALUES TO CORRESPOND TO
*                     2001 TAX FORM. ACCEPT AND REJECT REPORTS WERE
*                     MODIFIED.
* 02/04/02  F.ORTIZ - REPLACE IRS UPDATE FIELDS WITH MOORE UPDATE FIELDS
*                     AND CONTROL RECORD UPDATE. (MUST REMOVE BEFORE
*                     IRS REPORTING)
*  REMOVED
* 02/13/02  F.ORTIZ - MODIFIED FIELD #Q-SPECIAL-DATA-ENTRIES TO INCLUDE
*                     DO NOT MAIL FLAG FOR FOREIGN ADDRESS IN FIRST BYTE
*                     AND TEST INDICATOR IN SECOND BYTE. THIS IS FOR
*                     MOORE REPORTING.  (MUST REMOVE BEFORE IRS RPTING)
*  REMOVED
* 02/21/02  F.ORTIZ - ROUND THE GROSS AND FEDERAL TAX WITHHELD AMOUNT
*                     TO WHOLE DOLLAR AMOUNTS ON REPORTS AND Q RECORD.
* 02/22/02  F.ORTIZ - REARRANGED PARTICIPANT NAME FOR MOORE REPORTING
* 04/26/02  F.ORTIZ - CALL TO PROVINCE CODE CONVERSION WAS MISSING
*                     IN WRITE-Q-RECORD ROUTINE.  FIXED PROBLEM.
* 08/13/02  J.ROTHOLZ RECOMPILED DUE TO INCREASE IN #COMP-NAME
*                     LENGTH IN TWRLCOMP & TWRACOMP
* 10/30/02  F.ORTIZ - UPDATED COMPANY ROUTINE TO USE NEW TWRNCOM2 TO
*                     RETRIEVE SERVICES COMPANY NAME.
* 11/20/02  F.ORTIZ - POPULATE STREET ADDRESS 2 AND BOX 8 (REFUND AMT)
*                     ON IRS FILE.
* 02/04/05  R.MA    - UPDATE IRS 1042-S LAYOUT (TWRL5906) FOR 2004.
*                     ALSO UPDATE ROUTINE TO ACCESS TWRNCOM2, IN ORDER
*                     TO RETRIEVE THE APPROPRIATE COMPANY INFO.
*                     CONTROL REPORTS ARE UPDATED TO SHOW TWO COMPANY
*                     IN HEADINGS.
* 03/24/05  A.YOUNG - REVISED #Q-RECORD SEQUENCE NUMBER (#Q-REC-SEQ-NUM)
*                     TO BE INCREMENTED BY 1 FOR EACH RECORD.
*                   - REVISED TO ACCEPT AND PROCESS TIRF-TAX-YEAR,
*                     #TWRACOM2.#FED-ID, #TWRACOM2.#COMP-TCC, AND
*                     TIRF-1042-S-INCOME-CODE.  RECEIVED THE FOLLOWING
*                     ERROR WHILE TRYING TO COMPILE:
*                     NAT0300 OPERANDS ARE NOT DATA TRANSFER COMPATIBLE.
* 08/22/05  R.MA    - ADD LOGIC TO BLANK OUT INVALID CHARACTERS IN NAME
*                     AND ADDRESS FIELDS OF Q-RECORDS.
* 11/30/05  R.MA    - IRS REQUIREMENTS FOR INVALID CHARACTERS IN NAME,
*                     ADDRESS LINES HAD BEEN CHANGED AGAIN FOR 2005.
*                     UPDATE THE LOGIC IN THIS PROGRAM ACCORDINGLY.
*                     ALSO ENSURE THE TWRACOM2 HAD BEEN UPDATED FOR THE
*                     NEW CHARLOTTE ADDRESS, AND THE VALID TAX RATE WAS
*                     APPLIED.
* 11/15/06  R.MA    - UPDATE IRS 1042-S LAYOUT (TWRL5906) FOR 2006.
*                     ALSO UPDATE ROUTINE TO ACCESS TWRNCOM2, IN ORDER
*                     TO RETRIEVE THE APPROPRIATE INFO FOR NEW FIELDS:
*                     WITHHOLDING AGENT's Contact Name and Department
*                     TITLE IN THE 'W' RECORD.
*                     THE NEW FIELD IN THE 'Q' RECORD - US TAX WITHHELD
*                     INDICATOR, WOULD NEED TO BE POPULATED BASED UPON
*                     THE CALCULATION RESULT OF THREE EXISTING FIELDS.
*                     SEVERAL OTHER EXISTING FIELDS HAD ALSO BEEN
*                     UPDATED PER USER REQUEST.
* 01/30/08  A. YOUNG- REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
* 10/01/08  A. YOUNG- REVISED 2008 1042-S LAYOUT TWRL5906, PER IRS.
*                   - INCREASED TO 820 BYTES.
*                   - ADDED BYTE DISPLACEMENTS TO TWRL5906.
*                   - INCLUDED TWRN0001 TO AUTOMATE #T-TEST-IND
*                     POPULATION IN DEVELOPMENT ENVIRONMENTS.
*                   - ZERO-FILLED 'Q' RECORD POSITIONS 371-382
*                     (#Q-WTHHLDNG-BY-OTHER-AGENTS), PER USER REQUEST.
* 05/05/09  A. YOUNG- REVISED TO REPLACE INVALID CHARACTERS ')' AND '�'
*                     WITH 'A' AND 'a', INSTEAD OF BLANK.
*                   - REVISED TO INCLUDE #W-WTHHLDNG-AGENT-NAME-3,
*                     #W-WTHHLDNG-AGENT-STREET-2, #W-WTHHLDNG-AGENT-CITY
*                     AND #Q-CITY IN INVALID CHARACTER PROCESSING.
* 11/01/10  A. YOUNG- REVISED 2010 1042-S DATA, PER IRS.
*                     #Q-TIN-TYPE "4"= TIN REQUIRED BUT NOT PROVIDED.
* 02/08/11  A. YOUNG- REVISED TO EXCLUDE INVALID CHARACTER "\" BASED ON
*                     IRS REJECTION OF TEST FILE.
* 02/23/16  D. DUTTA- FIN PROJECT CHANGES. POPULATING THE FIN# INTO
*                     #Q-RCPNT-FORGN-TAX-ID-NBR. TAG - DUTTAD
* 11/14/16  S. SINHA- UPDATE IRS 1042-S LAYOUT (TWRL5906) FOR 2016.
*                   - CHANGED SOME FIELD NAMES TO TWRL5906.
*                   - ADDED SOME NEW FIELDS    TO TWRL5906.
*                   - ADDED BYTE DISPLACEMENTS TO TWRL5906.
*                   - #W-CHAPTER-3-STATUS AND #Q-CHAPTER-IND LOGIC
*                     CHANGED.                   TAG - SINHASN
* 12/08/16 RAHUL DAS- POPULATE ALL ZEROS IN
*                     #Q-TAX-ASSUMED-WTHHLDNG-AGENT TAG - DASRAH
*
* 4/24/2017  WEBBJ RESTOW ONLY FOR PIN EXPANSION.
*
* 12/26/2017 RUPOLEENA - ADDED NEW FIELDS LOB-CODE,UNIQUE FORM
*                        IDENTIFIER & AMENDMENT NUMBER.
*                        TAG - MUKHERR
*
* 10/05/2018 KAUSHIK   - EIN CHANGE - TAG: EINCHG
* 01/28/2018 VIKRAM    - DATE OF BIRTH FORMAT CHANGE 1042-S ORIGINAL
*                        REPORTING 2018 - TAG - VIKRAM
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5906 extends BLNatBase
{
    // Data Areas
    private PdaTwrafrmn pdaTwrafrmn;
    private PdaTwra0001 pdaTwra0001;
    private LdaTwrl5001 ldaTwrl5001;
    private LdaTwrl5906 ldaTwrl5906;
    private LdaTwrl9705 ldaTwrl9705;
    private PdaTwracom2 pdaTwracom2;
    private PdaTwratbl2 pdaTwratbl2;
    private PdaTwratbl3 pdaTwratbl3;
    private PdaTwratbl4 pdaTwratbl4;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_1042s_Control_Totals;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Description;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld;
    private DbsField pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt;

    private DbsGroup pnd_1042s_Wh_Control_Totals;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Description;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld;
    private DbsField pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt;
    private DbsField pnd_Rounded_Gross;
    private DbsField pnd_Rounded_Tax_Wthld;
    private DbsField pnd_Rounded_Refund_Amt;
    private DbsField pnd_C_1042_S;
    private DbsField pnd_Ndx;
    private DbsField pnd_1042_Ndx;
    private DbsField pnd_Err_Ndx;
    private DbsField pnd_Sys_Err_Ndx;
    private DbsField pnd_Cntl_Ndx;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Empty_Form_Cnt;
    private DbsField pnd_Irs_Reject_Cnt;
    private DbsField pnd_Accept_Cnt;
    private DbsField pnd_Total_Accept_Cnt;
    private DbsField pnd_Total_Irs_Reject_Cnt;
    private DbsField pnd_Total_Wh_Irs_Reject_Cnt;
    private DbsField pnd_Total_Wh_Accept_Cnt;
    private DbsField pnd_Wh_Accept_Cnt;
    private DbsField pnd_Wh_Irs_Reject_Cnt;
    private DbsField pnd_Form_Cnt;
    private DbsField pnd_Form_Cnt_Tiaa;
    private DbsField pnd_Form_Cnt_Trst;
    private DbsField pnd_Q_Record_Cnt;
    private DbsField pnd_Q_Record_Cnt_Tiaa;
    private DbsField pnd_Q_Record_Cnt_Trst;
    private DbsField pnd_Q_Empty_Record_Cnt;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Total_Q_Record_Cnt;
    private DbsField pnd_File_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_1042s_Isn;
    private DbsField pnd_Company_Code;
    private DbsField pnd_Province_Name;
    private DbsField pnd_Province_Code;
    private DbsField pnd_Gross_Income;
    private DbsField pnd_Federal_Tax;
    private DbsField pnd_Corr_Tax_Rt;
    private DbsField pnd_Corr_Tax_Rat;
    private DbsField pnd_Corr_Tax_Rate;
    private DbsField pnd_Refund_Amt;
    private DbsField pnd_1042s_Tax_Rate;
    private DbsField pnd_Total_Gross_Income;
    private DbsField pnd_Total_Federal_Tax;
    private DbsField pnd_Wthhldng_Agent_Cnt;
    private DbsField pnd_Eof;
    private DbsField pnd_Display_Key;

    private DbsGroup pnd_Display_Key__R_Field_1;
    private DbsField pnd_Display_Key_Pnd_Dkey_Tin;
    private DbsField pnd_Display_Key_Pnd_Dkey_Contract;
    private DbsField pnd_Sys_Date;
    private DbsField pnd_Sys_Time;
    private DbsField pnd_Error_Complete_Flag;
    private DbsField pnd_1042s_Complete_Flag;
    private DbsField pnd_1042s_Ndx;
    private DbsField pnd_Name;
    private DbsField pnd_Dash;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Reject_Geo;
    private DbsField pnd_Irs_Country_Name;
    private DbsField pnd_Tiaa_Company_Name;
    private DbsField pnd_Tiaa_Company_Tin;
    private DbsField pnd_Tcc_Convert;

    private DbsGroup pnd_Tcc_Convert__R_Field_2;
    private DbsField pnd_Tcc_Convert_Pnd_Tcc_N5;
    private DbsField pnd_I;
    private DbsField pnd_Cur_Tax_Yr;
    private DbsField pnd_Chk_Fields;

    private DbsGroup pnd_Chk_Fields__R_Field_3;
    private DbsField pnd_Chk_Fields_Pnd_Name_1;
    private DbsField pnd_Chk_Fields_Pnd_Name_2;
    private DbsField pnd_Chk_Fields_Pnd_Name_3;
    private DbsField pnd_Chk_Fields_Pnd_Addr_1;
    private DbsField pnd_Chk_Fields_Pnd_Addr_2;
    private DbsField pnd_Chk_Fields_Pnd_City_3;

    private DbsGroup pnd_Chk_Fields__R_Field_4;
    private DbsField pnd_Chk_Fields_Pnd_Chk_Char;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Valid_Cnt;
    private DbsField pnd_Counters_Pnd_Invld_Cnt;
    private DbsField pnd_Counters_Pnd_Valid_Cha_Cnt;
    private DbsField pnd_Counters_Pnd_Invld_Cha_Cnt;
    private DbsField pnd_Counters_Pnd_Invld_Cha_Tot;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        pdaTwra0001 = new PdaTwra0001(localVariables);
        ldaTwrl5001 = new LdaTwrl5001();
        registerRecord(ldaTwrl5001);
        ldaTwrl5906 = new LdaTwrl5906();
        registerRecord(ldaTwrl5906);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());
        pdaTwracom2 = new PdaTwracom2(localVariables);
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        pdaTwratbl3 = new PdaTwratbl3(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);

        // Local Variables

        pnd_1042s_Control_Totals = localVariables.newGroupArrayInRecord("pnd_1042s_Control_Totals", "#1042S-CONTROL-TOTALS", new DbsArrayController(1, 
            6));
        pnd_1042s_Control_Totals_Pnd_1042s_Description = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Description", "#1042S-DESCRIPTION", 
            FieldType.STRING, 15);
        pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income", 
            "#1042S-GROSS-INCOME", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt", 
            "#1042S-INTEREST-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt", "#1042S-PENSION-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld", 
            "#1042S-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld", 
            "#1042S-C4-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt = pnd_1042s_Control_Totals.newFieldInGroup("pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt", "#1042S-REFUND-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);

        pnd_1042s_Wh_Control_Totals = localVariables.newGroupArrayInRecord("pnd_1042s_Wh_Control_Totals", "#1042S-WH-CONTROL-TOTALS", new DbsArrayController(1, 
            6));
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt", 
            "#1042S-WH-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Description = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Description", 
            "#1042S-WH-DESCRIPTION", FieldType.STRING, 15);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income", 
            "#1042S-WH-GROSS-INCOME", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt", 
            "#1042S-WH-INTEREST-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt", 
            "#1042S-WH-PENSION-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld", 
            "#1042S-WH-NRA-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld", 
            "#1042S-WH-C4-TAX-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt = pnd_1042s_Wh_Control_Totals.newFieldInGroup("pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt", 
            "#1042S-WH-REFUND-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Rounded_Gross = localVariables.newFieldInRecord("pnd_Rounded_Gross", "#ROUNDED-GROSS", FieldType.PACKED_DECIMAL, 12);
        pnd_Rounded_Tax_Wthld = localVariables.newFieldInRecord("pnd_Rounded_Tax_Wthld", "#ROUNDED-TAX-WTHLD", FieldType.PACKED_DECIMAL, 12);
        pnd_Rounded_Refund_Amt = localVariables.newFieldInRecord("pnd_Rounded_Refund_Amt", "#ROUNDED-REFUND-AMT", FieldType.PACKED_DECIMAL, 12);
        pnd_C_1042_S = localVariables.newFieldInRecord("pnd_C_1042_S", "#C-1042-S", FieldType.PACKED_DECIMAL, 3);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_1042_Ndx = localVariables.newFieldInRecord("pnd_1042_Ndx", "#1042-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Err_Ndx = localVariables.newFieldInRecord("pnd_Err_Ndx", "#ERR-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Sys_Err_Ndx = localVariables.newFieldInRecord("pnd_Sys_Err_Ndx", "#SYS-ERR-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntl_Ndx = localVariables.newFieldInRecord("pnd_Cntl_Ndx", "#CNTL-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Form_Cnt = localVariables.newFieldInRecord("pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irs_Reject_Cnt = localVariables.newFieldInRecord("pnd_Irs_Reject_Cnt", "#IRS-REJECT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Cnt = localVariables.newFieldInRecord("pnd_Accept_Cnt", "#ACCEPT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Accept_Cnt = localVariables.newFieldInRecord("pnd_Total_Accept_Cnt", "#TOTAL-ACCEPT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Irs_Reject_Cnt = localVariables.newFieldInRecord("pnd_Total_Irs_Reject_Cnt", "#TOTAL-IRS-REJECT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Wh_Irs_Reject_Cnt = localVariables.newFieldInRecord("pnd_Total_Wh_Irs_Reject_Cnt", "#TOTAL-WH-IRS-REJECT-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Total_Wh_Accept_Cnt = localVariables.newFieldInRecord("pnd_Total_Wh_Accept_Cnt", "#TOTAL-WH-ACCEPT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Wh_Accept_Cnt = localVariables.newFieldInRecord("pnd_Wh_Accept_Cnt", "#WH-ACCEPT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Wh_Irs_Reject_Cnt = localVariables.newFieldInRecord("pnd_Wh_Irs_Reject_Cnt", "#WH-IRS-REJECT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt = localVariables.newFieldInRecord("pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Tiaa = localVariables.newFieldInRecord("pnd_Form_Cnt_Tiaa", "#FORM-CNT-TIAA", FieldType.PACKED_DECIMAL, 7);
        pnd_Form_Cnt_Trst = localVariables.newFieldInRecord("pnd_Form_Cnt_Trst", "#FORM-CNT-TRST", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Record_Cnt = localVariables.newFieldInRecord("pnd_Q_Record_Cnt", "#Q-RECORD-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Q_Record_Cnt_Tiaa = localVariables.newFieldInRecord("pnd_Q_Record_Cnt_Tiaa", "#Q-RECORD-CNT-TIAA", FieldType.PACKED_DECIMAL, 8);
        pnd_Q_Record_Cnt_Trst = localVariables.newFieldInRecord("pnd_Q_Record_Cnt_Trst", "#Q-RECORD-CNT-TRST", FieldType.PACKED_DECIMAL, 8);
        pnd_Q_Empty_Record_Cnt = localVariables.newFieldInRecord("pnd_Q_Empty_Record_Cnt", "#Q-EMPTY-RECORD-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Total_Q_Record_Cnt = localVariables.newFieldInRecord("pnd_Total_Q_Record_Cnt", "#TOTAL-Q-RECORD-CNT", FieldType.PACKED_DECIMAL, 8);
        pnd_File_Cnt = localVariables.newFieldInRecord("pnd_File_Cnt", "#FILE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_1042s_Isn = localVariables.newFieldInRecord("pnd_1042s_Isn", "#1042S-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Company_Code = localVariables.newFieldInRecord("pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_Province_Name = localVariables.newFieldInRecord("pnd_Province_Name", "#PROVINCE-NAME", FieldType.STRING, 20);
        pnd_Province_Code = localVariables.newFieldInRecord("pnd_Province_Code", "#PROVINCE-CODE", FieldType.STRING, 2);
        pnd_Gross_Income = localVariables.newFieldInRecord("pnd_Gross_Income", "#GROSS-INCOME", FieldType.NUMERIC, 12);
        pnd_Federal_Tax = localVariables.newFieldInRecord("pnd_Federal_Tax", "#FEDERAL-TAX", FieldType.NUMERIC, 12);
        pnd_Corr_Tax_Rt = localVariables.newFieldInRecord("pnd_Corr_Tax_Rt", "#CORR-TAX-RT", FieldType.PACKED_DECIMAL, 3);
        pnd_Corr_Tax_Rat = localVariables.newFieldInRecord("pnd_Corr_Tax_Rat", "#CORR-TAX-RAT", FieldType.PACKED_DECIMAL, 5, 2);
        pnd_Corr_Tax_Rate = localVariables.newFieldInRecord("pnd_Corr_Tax_Rate", "#CORR-TAX-RATE", FieldType.PACKED_DECIMAL, 5, 2);
        pnd_Refund_Amt = localVariables.newFieldInRecord("pnd_Refund_Amt", "#REFUND-AMT", FieldType.NUMERIC, 12);
        pnd_1042s_Tax_Rate = localVariables.newFieldInRecord("pnd_1042s_Tax_Rate", "#1042S-TAX-RATE", FieldType.NUMERIC, 4);
        pnd_Total_Gross_Income = localVariables.newFieldInRecord("pnd_Total_Gross_Income", "#TOTAL-GROSS-INCOME", FieldType.NUMERIC, 15);
        pnd_Total_Federal_Tax = localVariables.newFieldInRecord("pnd_Total_Federal_Tax", "#TOTAL-FEDERAL-TAX", FieldType.NUMERIC, 15);
        pnd_Wthhldng_Agent_Cnt = localVariables.newFieldInRecord("pnd_Wthhldng_Agent_Cnt", "#WTHHLDNG-AGENT-CNT", FieldType.NUMERIC, 3);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Display_Key = localVariables.newFieldInRecord("pnd_Display_Key", "#DISPLAY-KEY", FieldType.STRING, 18);

        pnd_Display_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Display_Key__R_Field_1", "REDEFINE", pnd_Display_Key);
        pnd_Display_Key_Pnd_Dkey_Tin = pnd_Display_Key__R_Field_1.newFieldInGroup("pnd_Display_Key_Pnd_Dkey_Tin", "#DKEY-TIN", FieldType.STRING, 10);
        pnd_Display_Key_Pnd_Dkey_Contract = pnd_Display_Key__R_Field_1.newFieldInGroup("pnd_Display_Key_Pnd_Dkey_Contract", "#DKEY-CONTRACT", FieldType.STRING, 
            8);
        pnd_Sys_Date = localVariables.newFieldInRecord("pnd_Sys_Date", "#SYS-DATE", FieldType.DATE);
        pnd_Sys_Time = localVariables.newFieldInRecord("pnd_Sys_Time", "#SYS-TIME", FieldType.TIME);
        pnd_Error_Complete_Flag = localVariables.newFieldInRecord("pnd_Error_Complete_Flag", "#ERROR-COMPLETE-FLAG", FieldType.BOOLEAN, 1);
        pnd_1042s_Complete_Flag = localVariables.newFieldInRecord("pnd_1042s_Complete_Flag", "#1042S-COMPLETE-FLAG", FieldType.BOOLEAN, 1);
        pnd_1042s_Ndx = localVariables.newFieldInRecord("pnd_1042s_Ndx", "#1042S-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 40);
        pnd_Dash = localVariables.newFieldInRecord("pnd_Dash", "#DASH", FieldType.STRING, 13);
        pnd_Test_Ind = localVariables.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 4);
        pnd_Reject_Geo = localVariables.newFieldInRecord("pnd_Reject_Geo", "#REJECT-GEO", FieldType.STRING, 3);
        pnd_Irs_Country_Name = localVariables.newFieldInRecord("pnd_Irs_Country_Name", "#IRS-COUNTRY-NAME", FieldType.STRING, 35);
        pnd_Tiaa_Company_Name = localVariables.newFieldInRecord("pnd_Tiaa_Company_Name", "#TIAA-COMPANY-NAME", FieldType.STRING, 40);
        pnd_Tiaa_Company_Tin = localVariables.newFieldInRecord("pnd_Tiaa_Company_Tin", "#TIAA-COMPANY-TIN", FieldType.STRING, 9);
        pnd_Tcc_Convert = localVariables.newFieldInRecord("pnd_Tcc_Convert", "#TCC-CONVERT", FieldType.STRING, 10);

        pnd_Tcc_Convert__R_Field_2 = localVariables.newGroupInRecord("pnd_Tcc_Convert__R_Field_2", "REDEFINE", pnd_Tcc_Convert);
        pnd_Tcc_Convert_Pnd_Tcc_N5 = pnd_Tcc_Convert__R_Field_2.newFieldInGroup("pnd_Tcc_Convert_Pnd_Tcc_N5", "#TCC-N5", FieldType.STRING, 5);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Cur_Tax_Yr = localVariables.newFieldInRecord("pnd_Cur_Tax_Yr", "#CUR-TAX-YR", FieldType.STRING, 4);
        pnd_Chk_Fields = localVariables.newFieldInRecord("pnd_Chk_Fields", "#CHK-FIELDS", FieldType.STRING, 240);

        pnd_Chk_Fields__R_Field_3 = localVariables.newGroupInRecord("pnd_Chk_Fields__R_Field_3", "REDEFINE", pnd_Chk_Fields);
        pnd_Chk_Fields_Pnd_Name_1 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_Name_1", "#NAME-1", FieldType.STRING, 40);
        pnd_Chk_Fields_Pnd_Name_2 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_Name_2", "#NAME-2", FieldType.STRING, 40);
        pnd_Chk_Fields_Pnd_Name_3 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_Name_3", "#NAME-3", FieldType.STRING, 40);
        pnd_Chk_Fields_Pnd_Addr_1 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_Addr_1", "#ADDR-1", FieldType.STRING, 40);
        pnd_Chk_Fields_Pnd_Addr_2 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_Addr_2", "#ADDR-2", FieldType.STRING, 40);
        pnd_Chk_Fields_Pnd_City_3 = pnd_Chk_Fields__R_Field_3.newFieldInGroup("pnd_Chk_Fields_Pnd_City_3", "#CITY-3", FieldType.STRING, 40);

        pnd_Chk_Fields__R_Field_4 = localVariables.newGroupInRecord("pnd_Chk_Fields__R_Field_4", "REDEFINE", pnd_Chk_Fields);
        pnd_Chk_Fields_Pnd_Chk_Char = pnd_Chk_Fields__R_Field_4.newFieldArrayInGroup("pnd_Chk_Fields_Pnd_Chk_Char", "#CHK-CHAR", FieldType.STRING, 1, 
            new DbsArrayController(1, 240));

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Valid_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Valid_Cnt", "#VALID-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Invld_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Invld_Cnt", "#INVLD-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Valid_Cha_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Valid_Cha_Cnt", "#VALID-CHA-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Invld_Cha_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Invld_Cha_Cnt", "#INVLD-CHA-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Counters_Pnd_Invld_Cha_Tot = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Invld_Cha_Tot", "#INVLD-CHA-TOT", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl5001.initializeValues();
        ldaTwrl5906.initializeValues();
        ldaTwrl9705.initializeValues();

        localVariables.reset();
        pnd_Eof.setInitialValue(false);
        pnd_Dash.setInitialValue("-------------");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5906() throws Exception
    {
        super("Twrp5906");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP5906", onError);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  01/30/08 - AY                                                                                                                                                //Natural: FORMAT ( 1 ) LS = 132 PS = 55;//Natural: FORMAT ( 2 ) LS = 132 PS = 55;//Natural: FORMAT ( 3 ) LS = 132 PS = 55;//Natural: FORMAT ( 4 ) LS = 132 PS = 55
        //*  01/30/08 - AY
        //*                                                                                                                                                               //Natural: FORMAT ( 5 ) LS = 132 PS = 55
        pnd_Sys_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #SYS-DATE := *DATX
        pnd_Sys_Time.setValue(Global.getTIMX());                                                                                                                          //Natural: ASSIGN #SYS-TIME := *TIMX
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 44T '1042-S IRS Original Reporting Control Report' 120T 'Report: RPT1' // 10T 'Tax Year: ' TIRF-TAX-YEAR ( EM = XXXX ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T '1042-S IRS Original Reporting' 120T 'Report: RPT2' // 59T 'Tax Year' TIRF-TAX-YEAR ( EM = XXXX ) // 'Form Type:' #TWRAFRMN.#FORM-NAME ( 3 ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T '1042-S IRS Original Reporting' 120T 'Report: RPT3' / 59T 'Reject Report'// 'Company: ' #TWRACOM2.#COMP-SHORT-NAME / 'Tax Year:' TIRF-TAX-YEAR ( EM = XXXX ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 4 ) TITLE LEFT *DATX ( EM = MM/DD/YYYY ) '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T '1042-S IRS Original Reporting' 120T 'Report: RPT4' / 59T 'Accept Report'// 'Company: ' #TWRACOM2.#COMP-SHORT-NAME / 'Tax Year:' TIRF-TAX-YEAR ( EM = XXXX ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 5 ) TITLE LEFT #SYS-DATE ( EM = MM/DD/YYYY ) '-' #SYS-TIME ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 5 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 39T '1042-S IRS Original Withholding Reconciliation Report' 120T 'Report: RPT5' // 59T 'Tax Year' TIRF-TAX-YEAR ( EM = XXXX ) // 'Form Type:' #TWRAFRMN.#FORM-NAME ( 3 ) //
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(1).setValue("REJECTED FORMS");                                                                            //Natural: ASSIGN #1042S-DESCRIPTION ( 1 ) := 'REJECTED FORMS'
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(2).setValue("ACCEPTED FORMS");                                                                            //Natural: ASSIGN #1042S-DESCRIPTION ( 2 ) := 'ACCEPTED FORMS'
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(3).setValue("TOTALS");                                                                                    //Natural: ASSIGN #1042S-DESCRIPTION ( 3 ) := 'TOTALS'
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(4).setValue("GRAND TOTALS");                                                                              //Natural: ASSIGN #1042S-DESCRIPTION ( 4 ) := 'GRAND TOTALS'
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(5).setValue("TIAA TOTALS");                                                                               //Natural: ASSIGN #1042S-DESCRIPTION ( 5 ) := 'TIAA TOTALS'
        pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(6).setValue("TRUST TOTALS");                                                                              //Natural: ASSIGN #1042S-DESCRIPTION ( 6 ) := 'TRUST TOTALS'
        boolean endOfDataForm = true;                                                                                                                                     //Natural: READ WORK FILE 1 FORM-U #1042S-ISN #TEST-IND
        boolean firstForm = true;
        FORM:
        while (condition(getWorkFiles().read(1, ldaTwrl9705.getVw_form_U(), pnd_1042s_Isn, pnd_Test_Ind)))
        {
            CheckAtStartofData1140();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventForm();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataForm = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            if (condition(ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean()))                                                                                          //Natural: IF TIRF-EMPTY-FORM
            {
                pnd_Empty_Form_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #EMPTY-FORM-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Display_Key_Pnd_Dkey_Tin.setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                                      //Natural: ASSIGN #DKEY-TIN := TIRF-TIN
            pnd_Display_Key_Pnd_Dkey_Contract.setValue(ldaTwrl9705.getForm_U_Tirf_Contract_Nbr());                                                                        //Natural: ASSIGN #DKEY-CONTRACT := TIRF-CONTRACT-NBR
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #DISPLAY-KEY
            if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                       //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
            {
                pnd_Irs_Reject_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #IRS-REJECT-CNT
                pnd_Total_Irs_Reject_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-IRS-REJECT-CNT
                pnd_Ndx.setValue(1);                                                                                                                                      //Natural: ASSIGN #NDX := 1
                WH_REJECT_LOOP:                                                                                                                                           //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
                for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_C_1042_S.nadd(1))
                {
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(1,":",pnd_C_1042_S).greater(getZero())))                                            //Natural: IF TIRF-NRA-TAX-WTHLD ( 1:#C-1042-S ) GT 0
                    {
                        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx).nadd(1);                                                                      //Natural: ADD 1 TO #1042S-WH-FORM-CNT ( #NDX )
                        pnd_Total_Wh_Irs_Reject_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #TOTAL-WH-IRS-REJECT-CNT
                        if (true) break WH_REJECT_LOOP;                                                                                                                   //Natural: ESCAPE BOTTOM ( WH-REJECT-LOOP. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM INCREMENT-TOTALS
                sub_Increment_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM INCREMENT-WITHHOLDING-TOTALS
                sub_Increment_Withholding_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Name.setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme())); //Natural: COMPRESS TIRF-PART-FIRST-NME TIRF-PART-MDDLE-NME TIRF-PART-LAST-NME INTO #NAME
                FOR01:                                                                                                                                                    //Natural: FOR #1042S-NDX = 1 TO C*TIRF-1042-S-LINE-GRP
                for (pnd_1042s_Ndx.setValue(1); condition(pnd_1042s_Ndx.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_1042s_Ndx.nadd(1))
                {
                    if (condition((getReports().getAstLineCount(3).add(7).add(ldaTwrl9705.getForm_U_Count_Casttirf_Sys_Err())).greater(59)))                              //Natural: IF ( *LINE-COUNT ( 3 ) + 7 + C*TIRF-SYS-ERR ) GT 59
                    {
                        getReports().newPage(new ReportSpecification(3));                                                                                                 //Natural: NEWPAGE ( 3 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Reject_Geo.reset();                                                                                                                               //Natural: RESET #REJECT-GEO
                    if (condition(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde().greater(" ")))                                                                                 //Natural: IF TIRF-APO-GEO-CDE GT ' '
                    {
                        pnd_Reject_Geo.setValue(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde());                                                                                //Natural: ASSIGN #REJECT-GEO := TIRF-APO-GEO-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl9705.getForm_U_Tirf_Geo_Cde().greater(" ")))                                                                                 //Natural: IF TIRF-GEO-CDE GT ' '
                        {
                            pnd_Reject_Geo.setValue(ldaTwrl9705.getForm_U_Tirf_Geo_Cde());                                                                                //Natural: ASSIGN #REJECT-GEO := TIRF-GEO-CDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Rounded_Gross.compute(new ComputeParameters(true, pnd_Rounded_Gross), ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_1042s_Ndx));         //Natural: COMPUTE ROUNDED #ROUNDED-GROSS = TIRF-GROSS-INCOME ( #1042S-NDX )
                    pnd_Rounded_Tax_Wthld.compute(new ComputeParameters(true, pnd_Rounded_Tax_Wthld), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_1042s_Ndx)); //Natural: COMPUTE ROUNDED #ROUNDED-TAX-WTHLD = TIRF-NRA-TAX-WTHLD ( #1042S-NDX )
                    pnd_Rounded_Refund_Amt.compute(new ComputeParameters(true, pnd_Rounded_Refund_Amt), ldaTwrl9705.getForm_U_Tirf_1042_S_Refund_Amt().getValue(pnd_1042s_Ndx)); //Natural: COMPUTE ROUNDED #ROUNDED-REFUND-AMT = TIRF-1042-S-REFUND-AMT ( #1042S-NDX )
                    getReports().display(3, new ReportEmptyLineSuppression(true),"////TIN",                                                                               //Natural: DISPLAY ( 3 ) ( ES = ON ) '////TIN' TIRF-TIN ( IS = ON ) '////Contract' TIRF-CONTRACT-NBR ( IS = ON ) '////Payee' TIRF-PAYEE-CDE ( IS = ON ) 'Name' #NAME / TIRF-STREET-ADDR / TIRF-STREET-ADDR-CONT-2 / TIRF-CITY / 'Geo' #REJECT-GEO / TIRF-PROVINCE-CODE / TIRF-ZIP ( EM = XXXXXXXXX ) / TIRF-COUNTRY-NAME '(1)' 4X '/' TIRF-1042-S-INCOME-CODE ( #1042S-NDX ) ( AD = R LC = ���������� ) / '(2)' '/' #ROUNDED-GROSS ( AD = R EM = -ZZZ,ZZZ,ZZZ,ZZ9 ) / '(5)' '//Amounts' TIRF-1042-S-TAX-RATE ( #1042S-NDX ) ( AD = R LC = ��������� ) / '(6)' 5X '/' TIRF-EXEMPT-CODE ( #1042S-NDX ) ( LC = ���������� ) / '(7)' '/' #ROUNDED-TAX-WTHLD ( AD = R EM = -ZZZ,ZZZ,ZZZ,ZZ9 ) / '(8)' '/' #ROUNDED-REFUND-AMT ( AD = R EM = -ZZZ,ZZZ,ZZZ,ZZ9 ) / '(15)' 3X '/' TIRF-IRS-COUNTRY-CODE ( AD = R LC = ���������� ) / '(16)' 3X '/' TIRF-GEO-CDE ( AD = R LC = ���������� )
                    		ldaTwrl9705.getForm_U_Tirf_Tin(), new IdenticalSuppress(true),"////Contract",
                    		ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(), new IdenticalSuppress(true),"////Payee",
                    		ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new IdenticalSuppress(true),"Name",
                    		pnd_Name,NEWLINE,ldaTwrl9705.getForm_U_Tirf_Street_Addr(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_Street_Addr_Cont_2(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_City(),
                        NEWLINE,"Geo",
                    		pnd_Reject_Geo,NEWLINE,ldaTwrl9705.getForm_U_Tirf_Province_Code(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_Zip(), new ReportEditMask ("XXXXXXXXX"),
                        NEWLINE,ldaTwrl9705.getForm_U_Tirf_Country_Name(),"(1)",
                    		new ColumnSpacing(4),"/",
                    		ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_1042s_Ndx), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),
                        NEWLINE,"(2)",
                    		"/",
                    		pnd_Rounded_Gross, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(5)",
                    		"//Amounts",
                    		ldaTwrl9705.getForm_U_Tirf_1042_S_Tax_Rate().getValue(pnd_1042s_Ndx), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"),
                        NEWLINE,"(6)",
                    		new ColumnSpacing(5),"/",
                    		ldaTwrl9705.getForm_U_Tirf_Exempt_Code().getValue(pnd_1042s_Ndx), new FieldAttributes("LC=����������"),NEWLINE,"(7)",
                    		"/",
                    		pnd_Rounded_Tax_Wthld, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(8)",
                    		"/",
                    		pnd_Rounded_Refund_Amt, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(15)",
                    		new ColumnSpacing(3),"/",
                    		ldaTwrl9705.getForm_U_Tirf_Irs_Country_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),NEWLINE,"(16)",
                        
                    		new ColumnSpacing(3),"/",
                    		ldaTwrl9705.getForm_U_Tirf_Geo_Cde(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_1042s_Ndx.less(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())))                                                            //Natural: IF #1042S-NDX LT C*TIRF-1042-S-LINE-GRP
                    {
                        getReports().display(3, new ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code()),pnd_Dash);                                           //Natural: DISPLAY ( 3 ) T*TIRF-1042-S-INCOME-CODE #DASH
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR02:                                                                                                                                                    //Natural: FOR #SYS-ERR-NDX = 1 TO C*TIRF-SYS-ERR
                for (pnd_Sys_Err_Ndx.setValue(1); condition(pnd_Sys_Err_Ndx.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_Sys_Err())); pnd_Sys_Err_Ndx.nadd(1))
                {
                    pnd_Err_Ndx.setValue(ldaTwrl9705.getForm_U_Tirf_Sys_Err().getValue(pnd_Sys_Err_Ndx));                                                                 //Natural: ASSIGN #ERR-NDX := TIRF-SYS-ERR ( #SYS-ERR-NDX )
                    getReports().write(3, new ColumnSpacing(10),new ReportTAsterisk(pnd_Name),pnd_Err_Ndx, new ReportZeroPrint (false),"-",ldaTwrl5001.getPnd_Twrl5001_Pnd_Err_Desc().getValue(pnd_Err_Ndx.getInt() + 1)); //Natural: WRITE ( 3 ) 10X T*#NAME #ERR-NDX ( ZP = OFF ) '-' #TWRL5001.#ERR-DESC ( #ERR-NDX )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().skip(3, 1);                                                                                                                                  //Natural: SKIP ( 3 ) 1
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM-FILE
                sub_Update_Form_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accept_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #ACCEPT-CNT
            pnd_Total_Accept_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-ACCEPT-CNT
            pnd_Ndx.setValue(2);                                                                                                                                          //Natural: ASSIGN #NDX := 2
            WH_ACCEPT_LOOP:                                                                                                                                               //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
            for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_C_1042_S.nadd(1))
            {
                if (condition(ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(1,":",pnd_C_1042_S).greater(getZero())))                                                //Natural: IF TIRF-NRA-TAX-WTHLD ( 1:#C-1042-S ) GT 0
                {
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx).nadd(1);                                                                          //Natural: ADD 1 TO #1042S-WH-FORM-CNT ( #NDX )
                    pnd_Total_Wh_Accept_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-WH-ACCEPT-CNT
                    if (true) break WH_ACCEPT_LOOP;                                                                                                                       //Natural: ESCAPE BOTTOM ( WH-ACCEPT-LOOP. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Company_Code.notEquals(ldaTwrl9705.getForm_U_Tirf_Company_Cde())))                                                                          //Natural: IF #COMPANY-CODE NE TIRF-COMPANY-CDE
            {
                pnd_Company_Code.setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                                                      //Natural: ASSIGN #COMPANY-CODE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
                sub_Get_Company_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM POPULATE-WITHHOLDING-AGENT-INFO
                sub_Populate_Withholding_Agent_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  WITHHOLDING AGENT RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-W-RECORD
                sub_Write_W_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INCREMENT-TOTALS
            sub_Increment_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INCREMENT-WITHHOLDING-TOTALS
            sub_Increment_Withholding_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  RECIPIENT RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-Q-RECORD
            sub_Write_Q_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-FORM-FILE
            sub_Update_Form_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Write_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WRITE-CNT
            //*                                                                                                                                                           //Natural: AT BREAK TIRF-COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-WORK
        FORM_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventForm(endOfDataForm);
        }
        if (Global.isEscape()) return;
        //*  MOVED HERE FROM AT BREAK OF COMPANY  /*
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().skip(5, 1);                                                                                                                                          //Natural: SKIP ( 5 ) 1
        pnd_Ndx.setValue(5);                                                                                                                                              //Natural: ASSIGN #NDX := 5
        pnd_Form_Cnt.setValue(pnd_Form_Cnt_Tiaa);                                                                                                                         //Natural: ASSIGN #FORM-CNT := #FORM-CNT-TIAA
        pnd_Record_Cnt.setValue(pnd_Q_Record_Cnt_Tiaa);                                                                                                                   //Natural: ASSIGN #RECORD-CNT := #Q-RECORD-CNT-TIAA
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().skip(5, 1);                                                                                                                                          //Natural: SKIP ( 5 ) 1
        pnd_Ndx.setValue(6);                                                                                                                                              //Natural: ASSIGN #NDX := 6
        pnd_Form_Cnt.setValue(pnd_Form_Cnt_Trst);                                                                                                                         //Natural: ASSIGN #FORM-CNT := #FORM-CNT-TRST
        pnd_Record_Cnt.setValue(pnd_Q_Record_Cnt_Trst);                                                                                                                   //Natural: ASSIGN #RECORD-CNT := #Q-RECORD-CNT-TRST
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Ndx.setValue(4);                                                                                                                                              //Natural: ASSIGN #NDX := 4
        pnd_Form_Cnt.compute(new ComputeParameters(false, pnd_Form_Cnt), pnd_Total_Irs_Reject_Cnt.add(pnd_Total_Accept_Cnt));                                             //Natural: ASSIGN #FORM-CNT := #TOTAL-IRS-REJECT-CNT + #TOTAL-ACCEPT-CNT
        pnd_Record_Cnt.setValue(pnd_Total_Q_Record_Cnt);                                                                                                                  //Natural: ASSIGN #RECORD-CNT := #TOTAL-Q-RECORD-CNT
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx).compute(new ComputeParameters(false, pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx)),  //Natural: ASSIGN #1042S-WH-FORM-CNT ( #NDX ) := #TOTAL-WH-IRS-REJECT-CNT + #TOTAL-WH-ACCEPT-CNT
            pnd_Total_Wh_Irs_Reject_Cnt.add(pnd_Total_Wh_Accept_Cnt));
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().skip(5, 1);                                                                                                                                          //Natural: SKIP ( 5 ) 1
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  END OF TRANSMISSION RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-F-RECORD
        sub_Write_F_Record();
        if (condition(Global.isEscape())) {return;}
        //*        08/22/05  R.MA
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),"Total records read:   ",pnd_Read_Cnt,NEWLINE,new TabSetting(10),"Records bypassed:",NEWLINE,new    //Natural: WRITE ( 1 ) 10T 'Total records read:   ' #READ-CNT / 10T 'Records bypassed:' / 10T '   Empty Forms:       ' #EMPTY-FORM-CNT / 10T '   Errors:            ' #TOTAL-IRS-REJECT-CNT / 10T 'Recs had invalid char:' #INVLD-CNT / 10T 'Total records written:' #WRITE-CNT / 10T 'Total File Count:     ' #FILE-CNT
            TabSetting(10),"   Empty Forms:       ",pnd_Empty_Form_Cnt,NEWLINE,new TabSetting(10),"   Errors:            ",pnd_Total_Irs_Reject_Cnt,NEWLINE,new 
            TabSetting(10),"Recs had invalid char:",pnd_Counters_Pnd_Invld_Cnt,NEWLINE,new TabSetting(10),"Total records written:",pnd_Write_Cnt,NEWLINE,new 
            TabSetting(10),"Total File Count:     ",pnd_File_Cnt);
        if (Global.isEscape()) return;
        if (condition(pnd_Read_Cnt.greater(getZero())))                                                                                                                   //Natural: IF #READ-CNT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL-TABLE
            sub_Update_Control_Table();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().skip(2, 4);                                                                                                                                          //Natural: SKIP ( 2 ) 4 LINES
        getReports().write(2, "Zero Q Records are Empty Accepted Q records bypassed because of rounding to zero.");                                                       //Natural: WRITE ( 2 ) 'Zero Q Records are Empty Accepted Q records bypassed because of rounding to zero.'
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-TOTALS
        //*      VALUE '14'
        //*       #Q-CHAPTER-4-WTHHLDNG-IND := 1
        //*       #Q-CHAPTER-3-WTHHLDNG-IND := 1
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-WITHHOLDING-TOTALS
        //*      VALUE '14'
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ALL-TOTALS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-T-RECORD
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-W-RECORD
        //*                                              ENDS HERE     11/30/05 RM
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-Q-RECORD
        //* ** ('01'=INDIVIDUAL, '05' = TRUST, '10' = ESTATE, '20' = INCOMPLETE)
        //* **
        //* *************************************************** MUKHERR BEGINS
        //*  #Q-TIN-TYPE CONVERTED TO #Q-TIN-RESERVED FOR 2017
        //* ***************************************************
        //*  DECIDE ON FIRST VALUE TIRF-TAX-ID-TYPE
        //*   VALUE '1','3'
        //*     #Q-TIN-TYPE                  := 1
        //*   VALUE '2'
        //*      #Q-TIN-TYPE                  := 2
        //*   VALUE '5'
        //*    RESET #Q-TIN-TYPE
        //*     #Q-TIN-TYPE                  := 4
        //*   NONE
        //*     WRITE 'INVALID TIN TYPE FOR IRS' TIRF-TAX-ID-TYPE
        //*       'TIN'     TIRF-TIN
        //*       'CNTRCT'  TIRF-CONTRACT-NBR
        //*       '-'      TIRF-PAYEE-CDE
        //*    RESET #Q-TIN-TYPE
        //*     #Q-TIN-TYPE                  := 4
        //*  END-DECIDE
        //* *#Q-RCPNT-CNTRY-RESIDENCE          := #IRS-COUNTRY-NAME
        //* *
        //*   #Q-US-TAX-WTHHLD-IND CHANGED TO RESERVE FIELD 2017
        //*  IF SUBSTR(TIRF-FUTURE-USE,20,1) EQ 'Y'
        //*  * DECIDE FOR FIRST CONDITION
        //*  *   WHEN #CORR-TAX-RT       = TIRF-CHPTR4-TAX-RATE (#1042-NDX)
        //*  *     #Q-US-TAX-WTHHLD-IND := 0
        //*  *   WHEN #CORR-TAX-RT       < TIRF-CHPTR4-TAX-RATE (#1042-NDX)
        //*  *     #Q-US-TAX-WTHHLD-IND := 2
        //*  *   WHEN #CORR-TAX-RT       > TIRF-CHPTR4-TAX-RATE (#1042-NDX)
        //*        #Q-US-TAX-WTHHLD-IND := 1
        //*      WHEN NONE
        //*        IGNORE
        //*    END-DECIDE
        //*  ELSE
        //*    DECIDE FOR FIRST CONDITION
        //*      WHEN #CORR-TAX-RT       = TIRF-1042-S-TAX-RATE(#1042-NDX)
        //*        #Q-US-TAX-WTHHLD-IND := 0
        //*      WHEN #CORR-TAX-RT       < TIRF-1042-S-TAX-RATE(#1042-NDX)
        //*        #Q-US-TAX-WTHHLD-IND := 2
        //*      WHEN #CORR-TAX-RT       > TIRF-1042-S-TAX-RATE(#1042-NDX)
        //*        #Q-US-TAX-WTHHLD-IND := 1
        //*      WHEN NONE
        //*        IGNORE
        //*    END-DECIDE
        //*  END-IF
        //*                                                            ENDS HERE
        //*  #Q-LOB-CODE                   := ' '
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-C-RECORD
        //* *******************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-F-RECORD
        //* *#F-MEDIA-CNT                 := '001'
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-WITHHOLDING-AGENT-INFO
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PROVINCE-CODE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IRS-COUNTRY-NAME
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-CODE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-INVALID-CHAR
        //*  02/08/11 - MASK (C) INCLUDES ALPHANUMERIC CHARACTERS (UPPER / LOWER
        //*             CASE) AND BLANK.
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-REPORT
        //*     '///Zip code' #POSTAL-CODE
        //*     'M/a/i/l'  #DO-NOT-MAIL
        //*  '/T/i/n'   #Q-TIN-TYPE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-TABLE
        //* ***********************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL-TABLE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-FORM-FILE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  ENVIRONMENT             /* 10/01/08 - AY
        //*    MUKHER
        //* *----------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ENVIRONMENT-IDENTIFICATION
        //* *----------------------------------------**
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Increment_Totals() throws Exception                                                                                                                  //Natural: INCREMENT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp().notEquals(getZero())))                                                                       //Natural: IF C*TIRF-1042-S-LINE-GRP NE 0
        {
            FOR03:                                                                                                                                                        //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
            for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_C_1042_S.nadd(1))
            {
                pnd_Rounded_Gross.compute(new ComputeParameters(true, pnd_Rounded_Gross), ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_C_1042_S));              //Natural: COMPUTE ROUNDED #ROUNDED-GROSS := TIRF-GROSS-INCOME ( #C-1042-S )
                pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                                //Natural: ADD #ROUNDED-GROSS TO #1042S-GROSS-INCOME ( #NDX )
                pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(3).nadd(pnd_Rounded_Gross);                                                                      //Natural: ADD #ROUNDED-GROSS TO #1042S-GROSS-INCOME ( 3 )
                pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(4).nadd(pnd_Rounded_Gross);                                                                      //Natural: ADD #ROUNDED-GROSS TO #1042S-GROSS-INCOME ( 4 )
                //*  INTEREST
                short decideConditionsMet1489 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                if (condition((ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                {
                    decideConditionsMet1489++;
                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                            //Natural: ADD #ROUNDED-GROSS TO #1042S-INTEREST-AMT ( #NDX )
                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(3).nadd(pnd_Rounded_Gross);                                                                  //Natural: ADD #ROUNDED-GROSS TO #1042S-INTEREST-AMT ( 3 )
                    //*  PENSION AND ANNUITIES             /* SINHASN
                    pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(4).nadd(pnd_Rounded_Gross);                                                                  //Natural: ADD #ROUNDED-GROSS TO #1042S-INTEREST-AMT ( 4 )
                }                                                                                                                                                         //Natural: VALUE '15'
                else if (condition((ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("15"))))
                {
                    decideConditionsMet1489++;
                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                             //Natural: ADD #ROUNDED-GROSS TO #1042S-PENSION-AMT ( #NDX )
                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(3).nadd(pnd_Rounded_Gross);                                                                   //Natural: ADD #ROUNDED-GROSS TO #1042S-PENSION-AMT ( 3 )
                    pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(4).nadd(pnd_Rounded_Gross);                                                                   //Natural: ADD #ROUNDED-GROSS TO #1042S-PENSION-AMT ( 4 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(ldaTwrl9705.getForm_U_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                    //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
                {
                    pnd_Rounded_Tax_Wthld.compute(new ComputeParameters(true, pnd_Rounded_Tax_Wthld), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-TAX-WTHLD := TIRF-NRA-TAX-WTHLD ( #C-1042-S )
                    pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(pnd_Ndx).nadd(pnd_Rounded_Tax_Wthld);                                                        //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-C4-TAX-WTHLD ( #NDX )
                    pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(3).nadd(pnd_Rounded_Tax_Wthld);                                                              //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-C4-TAX-WTHLD ( 3 )
                    //*  SINHASN
                    pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(4).nadd(pnd_Rounded_Tax_Wthld);                                                              //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-C4-TAX-WTHLD ( 4 )
                    ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_Ind().setValue(4);                                                                                          //Natural: ASSIGN #Q-CHAPTER-IND := 4
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rounded_Tax_Wthld.compute(new ComputeParameters(true, pnd_Rounded_Tax_Wthld), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-TAX-WTHLD := TIRF-NRA-TAX-WTHLD ( #C-1042-S )
                    pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(pnd_Ndx).nadd(pnd_Rounded_Tax_Wthld);                                                       //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-NRA-TAX-WTHLD ( #NDX )
                    pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(3).nadd(pnd_Rounded_Tax_Wthld);                                                             //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-NRA-TAX-WTHLD ( 3 )
                    //*  SINHASN
                    pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(4).nadd(pnd_Rounded_Tax_Wthld);                                                             //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-NRA-TAX-WTHLD ( 4 )
                    ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_Ind().setValue(3);                                                                                          //Natural: ASSIGN #Q-CHAPTER-IND := 3
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rounded_Refund_Amt.compute(new ComputeParameters(true, pnd_Rounded_Refund_Amt), ldaTwrl9705.getForm_U_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-REFUND-AMT := TIRF-1042-S-REFUND-AMT ( #C-1042-S )
                pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Refund_Amt);                                                             //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-REFUND-AMT ( #NDX )
                pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(3).nadd(pnd_Rounded_Refund_Amt);                                                                   //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-REFUND-AMT ( 3 )
                pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(4).nadd(pnd_Rounded_Refund_Amt);                                                                   //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-REFUND-AMT ( 4 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("X")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'X'
            {
                pnd_Form_Cnt_Trst.nadd(1);                                                                                                                                //Natural: ADD 1 TO #FORM-CNT-TRST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Form_Cnt_Tiaa.nadd(1);                                                                                                                                //Natural: ADD 1 TO #FORM-CNT-TIAA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Increment_Withholding_Totals() throws Exception                                                                                                      //Natural: INCREMENT-WITHHOLDING-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        if (condition(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp().notEquals(getZero())))                                                                       //Natural: IF C*TIRF-1042-S-LINE-GRP NE 0
        {
            FOR04:                                                                                                                                                        //Natural: FOR #C-1042-S = 1 TO C*TIRF-1042-S-LINE-GRP
            for (pnd_C_1042_S.setValue(1); condition(pnd_C_1042_S.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_C_1042_S.nadd(1))
            {
                //*  ONLY PROCESS CASES - FO
                if (condition(ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S).lessOrEqual(getZero())))                                                  //Natural: IF TIRF-NRA-TAX-WTHLD ( #C-1042-S ) LE 0
                {
                    //*  THAT HAVE TAXES WTHHLD
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rounded_Gross.compute(new ComputeParameters(true, pnd_Rounded_Gross), ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_C_1042_S));              //Natural: COMPUTE ROUNDED #ROUNDED-GROSS := TIRF-GROSS-INCOME ( #C-1042-S )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                          //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-GROSS-INCOME ( #NDX )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(3).nadd(pnd_Rounded_Gross);                                                                //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-GROSS-INCOME ( 3 )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(4).nadd(pnd_Rounded_Gross);                                                                //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-GROSS-INCOME ( 4 )
                //*  INTEREST
                short decideConditionsMet1543 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE TIRF-1042-S-INCOME-CODE ( #C-1042-S );//Natural: VALUE '01'
                if (condition((ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("01"))))
                {
                    decideConditionsMet1543++;
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                      //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-INTEREST-AMT ( #NDX )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(3).nadd(pnd_Rounded_Gross);                                                            //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-INTEREST-AMT ( 3 )
                    //*  PENSION AND ANNUITIES             /* SINHASN
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(4).nadd(pnd_Rounded_Gross);                                                            //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-INTEREST-AMT ( 4 )
                }                                                                                                                                                         //Natural: VALUE '15'
                else if (condition((ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_C_1042_S).equals("15"))))
                {
                    decideConditionsMet1543++;
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Gross);                                                       //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-PENSION-AMT ( #NDX )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(3).nadd(pnd_Rounded_Gross);                                                             //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-PENSION-AMT ( 3 )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(4).nadd(pnd_Rounded_Gross);                                                             //Natural: ADD #ROUNDED-GROSS TO #1042S-WH-PENSION-AMT ( 4 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(ldaTwrl9705.getForm_U_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                    //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
                {
                    pnd_Rounded_Tax_Wthld.compute(new ComputeParameters(true, pnd_Rounded_Tax_Wthld), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-TAX-WTHLD = TIRF-NRA-TAX-WTHLD ( #C-1042-S )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(pnd_Ndx).nadd(pnd_Rounded_Tax_Wthld);                                                  //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-C4-TAX-WTHLD ( #NDX )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(3).nadd(pnd_Rounded_Tax_Wthld);                                                        //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-C4-TAX-WTHLD ( 3 )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(4).nadd(pnd_Rounded_Tax_Wthld);                                                        //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-C4-TAX-WTHLD ( 4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rounded_Tax_Wthld.compute(new ComputeParameters(true, pnd_Rounded_Tax_Wthld), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-TAX-WTHLD = TIRF-NRA-TAX-WTHLD ( #C-1042-S )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(pnd_Ndx).nadd(pnd_Rounded_Tax_Wthld);                                                 //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-NRA-TAX-WTHLD ( #NDX )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(3).nadd(pnd_Rounded_Tax_Wthld);                                                       //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-NRA-TAX-WTHLD ( 3 )
                    pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(4).nadd(pnd_Rounded_Tax_Wthld);                                                       //Natural: ADD #ROUNDED-TAX-WTHLD TO #1042S-WH-NRA-TAX-WTHLD ( 4 )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rounded_Refund_Amt.compute(new ComputeParameters(true, pnd_Rounded_Refund_Amt), ldaTwrl9705.getForm_U_Tirf_1042_S_Refund_Amt().getValue(pnd_C_1042_S)); //Natural: COMPUTE ROUNDED #ROUNDED-REFUND-AMT := TIRF-1042-S-REFUND-AMT ( #C-1042-S )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt.getValue(pnd_Ndx).nadd(pnd_Rounded_Refund_Amt);                                                       //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-WH-REFUND-AMT ( #NDX )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt.getValue(3).nadd(pnd_Rounded_Refund_Amt);                                                             //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-WH-REFUND-AMT ( 3 )
                pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt.getValue(4).nadd(pnd_Rounded_Refund_Amt);                                                             //Natural: ADD #ROUNDED-REFUND-AMT TO #1042S-WH-REFUND-AMT ( 4 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_All_Totals() throws Exception                                                                                                                  //Natural: WRITE-ALL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        getReports().write(2, NEWLINE,pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),NEWLINE);                                                                         //Natural: WRITE ( 2 ) / #TWRACOM2.#COMP-SHORT-NAME /
        if (Global.isEscape()) return;
        //*   ACCEPT
        getReports().write(5, NEWLINE,pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),NEWLINE);                                                                         //Natural: WRITE ( 5 ) / #TWRACOM2.#COMP-SHORT-NAME /
        if (Global.isEscape()) return;
        pnd_Ndx.setValue(2);                                                                                                                                              //Natural: ASSIGN #NDX := 2
        pnd_Form_Cnt.setValue(pnd_Accept_Cnt);                                                                                                                            //Natural: ASSIGN #FORM-CNT := #ACCEPT-CNT
        pnd_Record_Cnt.setValue(pnd_Q_Record_Cnt);                                                                                                                        //Natural: ASSIGN #RECORD-CNT := #Q-RECORD-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        //*   REJECT
        getReports().write(2, "Zero Q Records",new ColumnSpacing(12),pnd_Q_Empty_Record_Cnt);                                                                             //Natural: WRITE ( 2 ) 'Zero Q Records' 12X #Q-EMPTY-RECORD-CNT
        if (Global.isEscape()) return;
        pnd_Ndx.setValue(1);                                                                                                                                              //Natural: ASSIGN #NDX := 1
        pnd_Form_Cnt.setValue(pnd_Irs_Reject_Cnt);                                                                                                                        //Natural: ASSIGN #FORM-CNT := #IRS-REJECT-CNT
        pnd_Record_Cnt.setValue(0);                                                                                                                                       //Natural: ASSIGN #RECORD-CNT := 0
        //*   TOTAL
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Ndx.setValue(3);                                                                                                                                              //Natural: ASSIGN #NDX := 3
        pnd_Form_Cnt.compute(new ComputeParameters(false, pnd_Form_Cnt), pnd_Irs_Reject_Cnt.add(pnd_Accept_Cnt));                                                         //Natural: ASSIGN #FORM-CNT := #IRS-REJECT-CNT + #ACCEPT-CNT
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx).compute(new ComputeParameters(false, pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx)),  //Natural: ASSIGN #1042S-WH-FORM-CNT ( #NDX ) := #1042S-WH-FORM-CNT ( 1 ) + #1042S-WH-FORM-CNT ( 2 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(1).add(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(2)));
        pnd_Record_Cnt.setValue(pnd_Q_Record_Cnt);                                                                                                                        //Natural: ASSIGN #RECORD-CNT := #Q-RECORD-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
        sub_Write_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Company_Code.equals("X")))                                                                                                                      //Natural: IF #COMPANY-CODE = 'X'
        {
            pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(6).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(3));                                //Natural: ADD #1042S-GROSS-INCOME ( 3 ) TO #1042S-GROSS-INCOME ( 6 )
            pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(6).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(3));                                //Natural: ADD #1042S-INTEREST-AMT ( 3 ) TO #1042S-INTEREST-AMT ( 6 )
            pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(6).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(3));                                  //Natural: ADD #1042S-PENSION-AMT ( 3 ) TO #1042S-PENSION-AMT ( 6 )
            pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(6).nadd(pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(3));                                //Natural: ADD #1042S-C4-TAX-WTHLD ( 3 ) TO #1042S-C4-TAX-WTHLD ( 6 )
            pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(6).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(3));                              //Natural: ADD #1042S-NRA-TAX-WTHLD ( 3 ) TO #1042S-NRA-TAX-WTHLD ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(6).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(3));                    //Natural: ADD #1042S-WH-GROSS-INCOME ( 3 ) TO #1042S-WH-GROSS-INCOME ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(6).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(3));                    //Natural: ADD #1042S-WH-INTEREST-AMT ( 3 ) TO #1042S-WH-INTEREST-AMT ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(6).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(3));                      //Natural: ADD #1042S-WH-PENSION-AMT ( 3 ) TO #1042S-WH-PENSION-AMT ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(6).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(3));                    //Natural: ADD #1042S-WH-C4-TAX-WTHLD ( 3 ) TO #1042S-WH-C4-TAX-WTHLD ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(6).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(3));                  //Natural: ADD #1042S-WH-NRA-TAX-WTHLD ( 3 ) TO #1042S-WH-NRA-TAX-WTHLD ( 6 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(6).nadd(1);                                                                                        //Natural: ADD 1 TO #1042S-WH-FORM-CNT ( 6 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(5).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(3));                                //Natural: ADD #1042S-GROSS-INCOME ( 3 ) TO #1042S-GROSS-INCOME ( 5 )
            pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(5).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(3));                                //Natural: ADD #1042S-INTEREST-AMT ( 3 ) TO #1042S-INTEREST-AMT ( 5 )
            pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(5).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(3));                                  //Natural: ADD #1042S-PENSION-AMT ( 3 ) TO #1042S-PENSION-AMT ( 5 )
            pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(5).nadd(pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(3));                                //Natural: ADD #1042S-C4-TAX-WTHLD ( 3 ) TO #1042S-C4-TAX-WTHLD ( 5 )
            pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(5).nadd(pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(3));                              //Natural: ADD #1042S-NRA-TAX-WTHLD ( 3 ) TO #1042S-NRA-TAX-WTHLD ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(5).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(3));                    //Natural: ADD #1042S-WH-GROSS-INCOME ( 3 ) TO #1042S-WH-GROSS-INCOME ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(5).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(3));                    //Natural: ADD #1042S-WH-INTEREST-AMT ( 3 ) TO #1042S-WH-INTEREST-AMT ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(5).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(3));                      //Natural: ADD #1042S-WH-PENSION-AMT ( 3 ) TO #1042S-WH-PENSION-AMT ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(5).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(3));                    //Natural: ADD #1042S-WH-C4-TAX-WTHLD ( 3 ) TO #1042S-WH-C4-TAX-WTHLD ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(5).nadd(pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(3));                  //Natural: ADD #1042S-WH-NRA-TAX-WTHLD ( 3 ) TO #1042S-WH-NRA-TAX-WTHLD ( 5 )
            pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(5).nadd(1);                                                                                        //Natural: ADD 1 TO #1042S-WH-FORM-CNT ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  FO 11/02
        //*  FO 11/02
        pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(1,":",3).reset();                                                                                        //Natural: RESET #1042S-GROSS-INCOME ( 1:3 ) #1042S-INTEREST-AMT ( 1:3 ) #1042S-PENSION-AMT ( 1:3 ) #1042S-NRA-TAX-WTHLD ( 1:3 ) #1042S-C4-TAX-WTHLD ( 1:3 ) #1042S-REFUND-AMT ( 1:3 ) #1042S-WH-FORM-CNT ( 1:3 ) #1042S-WH-GROSS-INCOME ( 1:3 ) #1042S-WH-INTEREST-AMT ( 1:3 ) #1042S-WH-PENSION-AMT ( 1:3 ) #1042S-WH-NRA-TAX-WTHLD ( 1:3 ) #1042S-WH-C4-TAX-WTHLD ( 1:3 ) #1042S-WH-REFUND-AMT ( 1:3 ) #ACCEPT-CNT #IRS-REJECT-CNT
        pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(1,":",3).reset();
        pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(1,":",3).reset();
        pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(1,":",3).reset();
        pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(1,":",3).reset();
        pnd_1042s_Control_Totals_Pnd_1042s_Refund_Amt.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(1,":",3).reset();
        pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt.getValue(1,":",3).reset();
        pnd_Accept_Cnt.reset();
        pnd_Irs_Reject_Cnt.reset();
        //*   SAVE TOTALS TO CONTROL FILE
        //*  THE FOLLOWING TOTALS ARE NOT IN USE 02/05  RM
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().display(2, " ",                                                                                                                                      //Natural: DISPLAY ( 2 ) ' ' #1042S-DESCRIPTION ( #NDX ) 'Form/Count' #FORM-CNT ( EM = Z,ZZZ,ZZ9 ) 'Q Rec/Count' #RECORD-CNT ( EM = Z,ZZZ,ZZ9 ) '/Gross Income' #1042S-GROSS-INCOME ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Pension/Annuities' #1042S-PENSION-AMT ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest' #1042S-INTEREST-AMT ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'NRA Tax/Withheld' #1042S-NRA-TAX-WTHLD ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'FATCA Tax/Withheld' #1042S-C4-TAX-WTHLD ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 )
        		pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(pnd_Ndx),"Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"Q Rec/Count",
        		pnd_Record_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"FATCA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  '/Amt Repaid'                              /* REMOVED FOR FATCA - RCC
        //*  #1042S-REFUND-AMT(#NDX) (EM=-Z,ZZZ,ZZ9.99) /* ALWAYS ZERO ANYWAY
        //*   WITHHOLDING RECONCILIATION REPORT
        //*  ADJUST FOR
        getReports().display(5, " ",                                                                                                                                      //Natural: DISPLAY ( 5 ) ' ' #1042S-DESCRIPTION ( #NDX ) 'Form/Count' #1042S-WH-FORM-CNT ( #NDX ) ( EM = Z,ZZZ,ZZ9 ) '/Gross Income' #1042S-WH-GROSS-INCOME ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'Pension/Annuities' #1042S-WH-PENSION-AMT ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Interest' #1042S-WH-INTEREST-AMT ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'NRA Tax/Withheld' #1042S-WH-NRA-TAX-WTHLD ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 'FATCA Tax/Withheld' #1042S-WH-C4-TAX-WTHLD ( #NDX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) '/Amt Repaid' #1042S-WH-REFUND-AMT ( #NDX ) ( EM = -ZZ,ZZ9.99 )
        		pnd_1042s_Control_Totals_Pnd_1042s_Description.getValue(pnd_Ndx),"Form/Count",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt.getValue(pnd_Ndx), new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"FATCA Tax/Withheld",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amt Repaid",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt.getValue(pnd_Ndx), new ReportEditMask ("-ZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  #1042S-WH-REFUND-AMT(#NDX)    (EM=-ZZ,ZZZ,ZZZ,ZZ9.99) /* FATCA
    }
    //*  TRANSMITTER RECORD
    private void sub_Write_T_Record() throws Exception                                                                                                                    //Natural: WRITE-T-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  #COMPANY-CODE := 'S'        /* SERVICES /* BACK TO BE TIAA FOR 2004
        //*  PERFORM GET-COMPANY-INFO              /*           02/04/2005  RM
        ldaTwrl5906.getPnd_T_Record().reset();                                                                                                                            //Natural: RESET #T-RECORD
        //*  03-24-2005
        pnd_Rec_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-CNT
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Record_Type().setValue("T");                                                                                                    //Natural: ASSIGN #T-RECORD-TYPE := 'T'
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Tax_Year().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                                     //Natural: ASSIGN #T-TAX-YEAR := TIRF-TAX-YEAR
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                           //Natural: ASSIGN #T-TRANSMITTER-TIN := #TWRACOM2.#FED-ID
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                                    //Natural: ASSIGN #T-TRANSMITTER-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                          //Natural: ASSIGN #T-TRANSMITTER-ADDRESS := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                            //Natural: ASSIGN #T-TRANSMITTER-CITY := #TWRACOM2.#CITY
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                          //Natural: ASSIGN #T-TRANSMITTER-STATE := #TWRACOM2.#STATE
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                            //Natural: ASSIGN #T-TRANSMITTER-ZIP := #TWRACOM2.#ZIP-9
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                                        //Natural: ASSIGN #T-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Contact_Phone_Nbr().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Phone());                                                          //Natural: ASSIGN #T-CONTACT-PHONE-NBR := #TWRACOM2.#PHONE
        pnd_Tcc_Convert.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Tcc());                                                                                             //Natural: ASSIGN #TCC-CONVERT := #TWRACOM2.#COMP-TCC
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Transmitter_Control_Code().setValue(pnd_Tcc_Convert_Pnd_Tcc_N5);                                                                //Natural: ASSIGN #T-TRANSMITTER-CONTROL-CODE := #TCC-N5
        //*  10/01/08
        if (condition(pdaTwra0001.getTwra0001_Pnd_Prod_Test().equals("TEST")))                                                                                            //Natural: IF TWRA0001.#PROD-TEST = 'TEST'
        {
            if (condition(pnd_Test_Ind.equals(" ")))                                                                                                                      //Natural: IF #TEST-IND = ' '
            {
                ldaTwrl5906.getPnd_T_Record_Pnd_T_Test_Ind().setValue("TEST");                                                                                            //Natural: ASSIGN #T-TEST-IND := 'TEST'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl5906.getPnd_T_Record_Pnd_T_Test_Ind().setValue(pnd_Test_Ind);                                                                                      //Natural: ASSIGN #T-TEST-IND := #TEST-IND
            }                                                                                                                                                             //Natural: END-IF
            //*  10/01/08
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl5906.getPnd_T_Record_Pnd_T_Rec_Seq_Num().setValue(pnd_Rec_Cnt);                                                                                            //Natural: ASSIGN #T-REC-SEQ-NUM := #REC-CNT
        getWorkFiles().write(2, false, ldaTwrl5906.getPnd_T_Record());                                                                                                    //Natural: WRITE WORK FILE 2 #T-RECORD
        pnd_File_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #FILE-CNT
    }
    private void sub_Write_W_Record() throws Exception                                                                                                                    //Natural: WRITE-W-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  VARIFIED      11/15/06  RM
        pnd_Rec_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-CNT
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Record_Type().setValue("W");                                                                                                    //Natural: ASSIGN #W-RECORD-TYPE := 'W'
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Return_Type_Ind().setValue(0);                                                                                                  //Natural: ASSIGN #W-RETURN-TYPE-IND := 0
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Pro_Rata_Basis().setValue(0);                                                                                                   //Natural: ASSIGN #W-PRO-RATA-BASIS := 0
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Ein_Ind().setValue(0);                                                                                           //Natural: ASSIGN #W-WTHHLDNG-AGENT-EIN-IND := 0
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Tax_Year().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Year());                                                                     //Natural: ASSIGN #W-TAX-YEAR := TIRF-TAX-YEAR
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Final_Return_Ind().setValue(0);                                                                                                 //Natural: ASSIGN #W-FINAL-RETURN-IND := 0
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Rec_Seq_Num().setValue(pnd_Rec_Cnt);                                                                                            //Natural: ASSIGN #W-REC-SEQ-NUM := #REC-CNT
        //*    UPDATED LOGIC TO CHECK INVALID CHARACTERS STARTS HERE   11/30/05 RM
        //* *RESET #NAME-1 #NAME-2 #NAME-3                            /* 02/08/11
        //* *      #ADDR-1 #ADDR-2 #CITY-3               /* 05/05/09  /* 02/08/11
        //*  02/08/11
        //*  05/05/09
        //*  05/05/09
        //*  05/05/09
        pnd_Chk_Fields.reset();                                                                                                                                           //Natural: RESET #CHK-FIELDS
        pnd_Chk_Fields_Pnd_Name_1.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_1());                                                                    //Natural: ASSIGN #NAME-1 := #W-WTHHLDNG-AGENT-NAME-1
        pnd_Chk_Fields_Pnd_Name_2.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_2());                                                                    //Natural: ASSIGN #NAME-2 := #W-WTHHLDNG-AGENT-NAME-2
        pnd_Chk_Fields_Pnd_Name_3.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_3());                                                                    //Natural: ASSIGN #NAME-3 := #W-WTHHLDNG-AGENT-NAME-3
        pnd_Chk_Fields_Pnd_Addr_1.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Street_1());                                                                  //Natural: ASSIGN #ADDR-1 := #W-WTHHLDNG-AGENT-STREET-1
        pnd_Chk_Fields_Pnd_Addr_2.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Street_2());                                                                  //Natural: ASSIGN #ADDR-2 := #W-WTHHLDNG-AGENT-STREET-2
        pnd_Chk_Fields_Pnd_City_3.setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_City());                                                                      //Natural: ASSIGN #CITY-3 := #W-WTHHLDNG-AGENT-CITY
        //*  05/05/09
        //*  05/05/09
        //*  05/05/09
                                                                                                                                                                          //Natural: PERFORM CHECK-INVALID-CHAR
        sub_Check_Invalid_Char();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_1().setValue(pnd_Chk_Fields_Pnd_Name_1);                                                                    //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-1 := #NAME-1
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_2().setValue(pnd_Chk_Fields_Pnd_Name_2);                                                                    //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-2 := #NAME-2
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_3().setValue(pnd_Chk_Fields_Pnd_Name_3);                                                                    //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-3 := #NAME-3
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Street_1().setValue(pnd_Chk_Fields_Pnd_Addr_1);                                                                  //Natural: ASSIGN #W-WTHHLDNG-AGENT-STREET-1 := #ADDR-1
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Street_2().setValue(pnd_Chk_Fields_Pnd_Addr_2);                                                                  //Natural: ASSIGN #W-WTHHLDNG-AGENT-STREET-2 := #ADDR-2
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_City().setValue(pnd_Chk_Fields_Pnd_City_3);                                                                      //Natural: ASSIGN #W-WTHHLDNG-AGENT-CITY := #CITY-3
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Foreign_Tin_X().setValue(" ");                                                                                   //Natural: ASSIGN #W-WTHHLDNG-AGENT-FOREIGN-TIN-X := ' '
        if (condition(ldaTwrl9705.getForm_U_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                            //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
        {
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Giin_X().setValue(" ");                                                                                      //Natural: ASSIGN #W-WTHHLDNG-AGENT-GIIN-X := ' '
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Ind().setValue(4);                                                                                                 //Natural: ASSIGN #W-WTHHLDNG-IND := 4
            //*  #W-CHAPTER-3-STATUS-X         := '  '                /* MUKHERR
            //*  #W-CHAPTER-4-STATUS-X         := '01'                /* MUKHERR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Ind().setValue(3);                                                                                                 //Natural: ASSIGN #W-WTHHLDNG-IND := 3
            //*  #W-CHAPTER-3-STATUS-X         := '01'                /* MUKHERR
            //*  #W-CHAPTER-4-STATUS-X         := '  '                /* MUKHERR
            //*  END - FATCA
        }                                                                                                                                                                 //Natural: END-IF
        //*  SINHASN
        //*  SINHASN
        //*  SINHASN
        if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                                      //Natural: IF TIRF-TAX-YEAR GE '2016'
        {
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Chapter_3_Status_X().setValue("01");                                                                                        //Natural: ASSIGN #W-CHAPTER-3-STATUS-X := '01'
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Chapter_4_Status_X().setValue("01");                                                                                        //Natural: ASSIGN #W-CHAPTER-4-STATUS-X := '01'
            //*  SINHASN
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(2, false, ldaTwrl5906.getPnd_W_Record());                                                                                                    //Natural: WRITE WORK FILE 2 #W-RECORD
        pnd_File_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #FILE-CNT
    }
    //*  RECIPIENT RECORD
    private void sub_Write_Q_Record() throws Exception                                                                                                                    //Natural: WRITE-Q-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  VARIFIED  11/15/06      RM
        ldaTwrl5906.getPnd_Q_Record().reset();                                                                                                                            //Natural: RESET #Q-RECORD
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Record_Type().setValue("Q");                                                                                                    //Natural: ASSIGN #Q-RECORD-TYPE := 'Q'
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Return_Type_Ind().setValue(0);                                                                                                  //Natural: ASSIGN #Q-RETURN-TYPE-IND := 0
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Pro_Rata_Basis().setValue(0);                                                                                                   //Natural: ASSIGN #Q-PRO-RATA-BASIS := 0
        //*  REPLACED #Q-RECIPIENT-CODE
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Account_Nbr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),               //Natural: COMPRESS TIRF-CONTRACT-NBR '-' TIRF-PAYEE-CDE INTO #Q-ACCOUNT-NBR LEAVING NO SPACE
            "-", ldaTwrl9705.getForm_U_Tirf_Payee_Cde()));
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Reserved_2().setValue(" ");                                                                                                     //Natural: ASSIGN #Q-RESERVED-2 := ' '
        //* ***
        //*  REARRANGED THE PARTICIPANT NAME ORDER    /* FO 02/02
        //* ***
        //* FO11/02
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_1().setValue(DbsUtil.compress(ldaTwrl9705.getForm_U_Tirf_Part_First_Nme(), ldaTwrl9705.getForm_U_Tirf_Part_Mddle_Nme(),    //Natural: COMPRESS TIRF-PART-FIRST-NME TIRF-PART-MDDLE-NME TIRF-PART-LAST-NME INTO #Q-NAME-1
            ldaTwrl9705.getForm_U_Tirf_Part_Last_Nme()));
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_1().setValue(ldaTwrl9705.getForm_U_Tirf_Street_Addr());                                                                  //Natural: ASSIGN #Q-STREET-1 := TIRF-STREET-ADDR
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_2().setValue(ldaTwrl9705.getForm_U_Tirf_Street_Addr_Cont_1());                                                           //Natural: ASSIGN #Q-STREET-2 := TIRF-STREET-ADDR-CONT-1
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_City().setValue(ldaTwrl9705.getForm_U_Tirf_City());                                                                             //Natural: ASSIGN #Q-CITY := TIRF-CITY
        if (condition(ldaTwrl9705.getForm_U_Tirf_Geo_Cde().equals("00")))                                                                                                 //Natural: IF TIRF-GEO-CDE = '00'
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_State().setValue(ldaTwrl9705.getForm_U_Tirf_Apo_Geo_Cde());                                                                 //Natural: ASSIGN #Q-STATE := TIRF-APO-GEO-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE-CODE
            sub_Get_State_Code();
            if (condition(Global.isEscape())) {return;}
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                                  //Natural: ASSIGN #Q-STATE := #STATE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-PROVINCE-CODE
        sub_Get_Province_Code();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.examine(new ExamineSource(pnd_Province_Code), new ExamineTranslate(TranslateOption.Upper));                                                               //Natural: EXAMINE #PROVINCE-CODE AND TRANSLATE INTO UPPER CASE
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Province_Code().setValue(pnd_Province_Code);                                                                                    //Natural: ASSIGN #Q-PROVINCE-CODE := #PROVINCE-CODE
        //*  GU
        if (condition(DbsUtil.maskMatches(ldaTwrl9705.getForm_U_Tirf_Geo_Cde(),"NN") || ldaTwrl9705.getForm_U_Tirf_Geo_Cde().equals("6")))                                //Natural: IF TIRF-GEO-CDE = MASK ( NN ) OR TIRF-GEO-CDE = '6'
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code().setValue(" ");                                                                                               //Natural: ASSIGN #Q-COUNTRY-CODE := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Geo_Cde());                                                              //Natural: ASSIGN #Q-COUNTRY-CODE := TIRF-GEO-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Zip().setValue(ldaTwrl9705.getForm_U_Tirf_Zip());                                                                               //Natural: ASSIGN #Q-ZIP := TIRF-ZIP
        //*  ADDED    11/15/06 RM
        DbsUtil.examine(new ExamineSource(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Zip()), new ExamineSearch("-"), new ExamineDelete());                                         //Natural: EXAMINE #Q-ZIP FOR '-' DELETE
        //*  ADDED    11/15/06 RM
        //*  MUKHERR
        DbsUtil.examine(new ExamineSource(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Zip()), new ExamineSearch(" "), new ExamineDelete());                                         //Natural: EXAMINE #Q-ZIP FOR ' ' DELETE
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tin_Reserved().setValue(" ");                                                                                                   //Natural: ASSIGN #Q-TIN-RESERVED := ' '
        //*  IF #Q-TIN-TYPE = 0 OR = 4       /* MUKHERR                 /* 11/01/10
        //*  MUKHERR
        if (condition((ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().notEquals("1")) && (ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().notEquals("2")) && (ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type().notEquals("3")))) //Natural: IF ( TIRF-TAX-ID-TYPE NE '1' ) AND ( TIRF-TAX-ID-TYPE NE '2' ) AND ( TIRF-TAX-ID-TYPE NE '3' )
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tin().reset();                                                                                                              //Natural: RESET #Q-TIN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tin().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                           //Natural: ASSIGN #Q-TIN := TIRF-TIN
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-IRS-COUNTRY-NAME
        sub_Get_Irs_Country_Name();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Rcpnt_Cntry_Residence_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Country_Code());                                           //Natural: ASSIGN #Q-RCPNT-CNTRY-RESIDENCE-CODE := TIRF-IRS-COUNTRY-CODE
        ldaTwrl5906.getPnd_Q_Record_Pnd_Q_State_Tax_Wthhld().setValue("000000000000");                                                                                    //Natural: ASSIGN #Q-STATE-TAX-WTHHLD := '000000000000'
        if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("C")))                                                                                              //Natural: IF TIRF-COMPANY-CDE EQ 'C'
        {
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Payer_Name().setValue(pnd_Tiaa_Company_Name);                                                                               //Natural: ASSIGN #Q-PAYER-NAME := #TIAA-COMPANY-NAME
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Payer_Tin().setValue(pnd_Tiaa_Company_Tin);                                                                                 //Natural: ASSIGN #Q-PAYER-TIN := #TIAA-COMPANY-TIN
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #1042-NDX = 1 TO C*TIRF-1042-S-LINE-GRP
        for (pnd_1042_Ndx.setValue(1); condition(pnd_1042_Ndx.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_1042_Ndx.nadd(1))
        {
            //*  FATCA &&&
            if (condition(ldaTwrl9705.getForm_U_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                        //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
            {
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Income_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Chptr4_Income_Cde().getValue(pnd_1042_Ndx));                          //Natural: ASSIGN #Q-INCOME-CODE := TIRF-CHPTR4-INCOME-CDE ( #1042-NDX )
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Exempt_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl9705.getForm_U_Tirf_Chptr4_Exempt_Cde().getValue(pnd_1042_Ndx))); //Natural: COMPRESS TIRF-CHPTR4-EXEMPT-CDE ( #1042-NDX ) INTO #Q-CHAPTER-4-EXEMPT-CDE LEAVING NO
                pnd_1042s_Tax_Rate.compute(new ComputeParameters(false, pnd_1042s_Tax_Rate), ldaTwrl9705.getForm_U_Tirf_Chptr4_Tax_Rate().getValue(pnd_1042_Ndx).multiply(100)); //Natural: ASSIGN #1042S-TAX-RATE := TIRF-CHPTR4-TAX-RATE ( #1042-NDX ) * 100
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Tax_Rate().setValueEdited(pnd_1042s_Tax_Rate,new ReportEditMask("9999"));                                     //Natural: MOVE EDITED #1042S-TAX-RATE ( EM = 9999 ) TO #Q-CHAPTER-4-TAX-RATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Tax_Rate().setValue("0000");                                                                                  //Natural: ASSIGN #Q-CHAPTER-4-TAX-RATE := '0000'
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Exempt_Cde().setValue("15");                                                                                  //Natural: ASSIGN #Q-CHAPTER-4-EXEMPT-CDE := '15'
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Income_Code().setValue(ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_1042_Ndx));                         //Natural: ASSIGN #Q-INCOME-CODE := TIRF-1042-S-INCOME-CODE ( #1042-NDX )
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Exempt_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, 0, ldaTwrl9705.getForm_U_Tirf_Exempt_Code().getValue(pnd_1042_Ndx))); //Natural: COMPRESS 0 TIRF-EXEMPT-CODE ( #1042-NDX ) INTO #Q-CHAPTER-3-EXEMPT-CDE LEAVING NO
                pnd_1042s_Tax_Rate.compute(new ComputeParameters(false, pnd_1042s_Tax_Rate), ldaTwrl9705.getForm_U_Tirf_1042_S_Tax_Rate().getValue(pnd_1042_Ndx).multiply(100)); //Natural: ASSIGN #1042S-TAX-RATE := TIRF-1042-S-TAX-RATE ( #1042-NDX ) * 100
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Tax_Rate().setValueEdited(pnd_1042s_Tax_Rate,new ReportEditMask("9999"));                                     //Natural: MOVE EDITED #1042S-TAX-RATE ( EM = 9999 ) TO #Q-CHAPTER-3-TAX-RATE
                //*       #Q-EXEMPTION-CODE LEAVING NO                       /*
            }                                                                                                                                                             //Natural: END-IF
            pnd_Federal_Tax.compute(new ComputeParameters(true, pnd_Federal_Tax), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_1042_Ndx));                     //Natural: COMPUTE ROUNDED #FEDERAL-TAX = TIRF-NRA-TAX-WTHLD ( #1042-NDX )
            //*  10/01/08
            //*  10/01/08
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Us_Tax_Wthhld().setValueEdited(pnd_Federal_Tax,new ReportEditMask("999999999999"));                                         //Natural: MOVE EDITED #FEDERAL-TAX ( EM = 999999999999 ) TO #Q-US-TAX-WTHHLD
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Total_Wthhldng_Credit().setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Us_Tax_Wthhld());                                        //Natural: ASSIGN #Q-TOTAL-WTHHLDNG-CREDIT := #Q-US-TAX-WTHHLD
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Wthhldng_By_Other_Agents_N().setValue(0);                                                                                   //Natural: ASSIGN #Q-WTHHLDNG-BY-OTHER-AGENTS-N := 0
            pnd_Gross_Income.compute(new ComputeParameters(true, pnd_Gross_Income), ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_1042_Ndx));                    //Natural: COMPUTE ROUNDED #GROSS-INCOME = TIRF-GROSS-INCOME ( #1042-NDX )
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Gross_Income().setValueEdited(pnd_Gross_Income,new ReportEditMask("999999999999"));                                         //Natural: MOVE EDITED #GROSS-INCOME ( EM = 999999999999 ) TO #Q-GROSS-INCOME
            //* *  THE FOLLOWING LINES ADDED TO POPULATE THE INDICATOR  11/15/06  RM
            //*                                                           STARTS HERE
            if (condition(ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_1042_Ndx).equals(getZero())))                                                            //Natural: IF TIRF-GROSS-INCOME ( #1042-NDX ) = 0
            {
                pnd_Corr_Tax_Rate.setValue(0);                                                                                                                            //Natural: ASSIGN #CORR-TAX-RATE := 0
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Corr_Tax_Rate.compute(new ComputeParameters(true, pnd_Corr_Tax_Rate), ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_1042_Ndx).divide(ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_1042_Ndx))); //Natural: COMPUTE ROUNDED #CORR-TAX-RATE = TIRF-NRA-TAX-WTHLD ( #1042-NDX ) / TIRF-GROSS-INCOME ( #1042-NDX )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Corr_Tax_Rat.compute(new ComputeParameters(true, pnd_Corr_Tax_Rat), pnd_Corr_Tax_Rate.multiply(100));                                                     //Natural: COMPUTE ROUNDED #CORR-TAX-RAT := #CORR-TAX-RATE * 100
            //*  MUKHERR
            pnd_Corr_Tax_Rt.compute(new ComputeParameters(true, pnd_Corr_Tax_Rt), pnd_Corr_Tax_Rat);                                                                      //Natural: COMPUTE ROUNDED #CORR-TAX-RT := #CORR-TAX-RAT
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Us_Tax_Reserved().setValue(" ");                                                                                            //Natural: ASSIGN #Q-US-TAX-RESERVED := ' '
            pnd_Refund_Amt.compute(new ComputeParameters(true, pnd_Refund_Amt), ldaTwrl9705.getForm_U_Tirf_1042_S_Refund_Amt().getValue(pnd_1042_Ndx));                   //Natural: COMPUTE ROUNDED #REFUND-AMT = TIRF-1042-S-REFUND-AMT ( #1042-NDX )
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Amt_Repaid().setValueEdited(pnd_Refund_Amt,new ReportEditMask("999999999999"));                                             //Natural: MOVE EDITED #REFUND-AMT ( EM = 999999999999 ) TO #Q-AMT-REPAID
            if (condition(pnd_Gross_Income.equals(getZero()) && pnd_Federal_Tax.equals(getZero()) && pnd_Refund_Amt.equals(getZero())))                                   //Natural: IF #GROSS-INCOME = 0 AND #FEDERAL-TAX = 0 AND #REFUND-AMT = 0
            {
                pnd_Q_Empty_Record_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #Q-EMPTY-RECORD-CNT
                //*  FOR LOOP
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Gross_Income.nadd(pnd_Gross_Income);                                                                                                                //Natural: ADD #GROSS-INCOME TO #TOTAL-GROSS-INCOME
            pnd_Total_Federal_Tax.nadd(pnd_Federal_Tax);                                                                                                                  //Natural: ADD #FEDERAL-TAX TO #TOTAL-FEDERAL-TAX
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ASSIGN #REC-CNT := #REC-CNT + 1
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Rec_Seq_Num().setValue(pnd_Rec_Cnt);                                                                                        //Natural: ASSIGN #Q-REC-SEQ-NUM := #REC-CNT
            //*    UPDATED LOGIC TO CHECK INVALID CHARACTERS STARTS HERE   11/30/05 RM
            //* *RESET #NAME-1 #NAME-2 #NAME-3                            /* 02/08/11
            //* *      #ADDR-1 #ADDR-2 #CITY-3               /* 05/05/09  /* 02/08/11
            //*  02/08/11
            //*  05/05/09
            pnd_Chk_Fields.reset();                                                                                                                                       //Natural: RESET #CHK-FIELDS
            pnd_Chk_Fields_Pnd_Name_1.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_1());                                                                               //Natural: ASSIGN #NAME-1 := #Q-NAME-1
            pnd_Chk_Fields_Pnd_Name_2.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_2());                                                                               //Natural: ASSIGN #NAME-2 := #Q-NAME-2
            pnd_Chk_Fields_Pnd_Name_3.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_3());                                                                               //Natural: ASSIGN #NAME-3 := #Q-NAME-3
            pnd_Chk_Fields_Pnd_Addr_1.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_1());                                                                             //Natural: ASSIGN #ADDR-1 := #Q-STREET-1
            pnd_Chk_Fields_Pnd_Addr_2.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_2());                                                                             //Natural: ASSIGN #ADDR-2 := #Q-STREET-2
            pnd_Chk_Fields_Pnd_City_3.setValue(ldaTwrl5906.getPnd_Q_Record_Pnd_Q_City());                                                                                 //Natural: ASSIGN #CITY-3 := #Q-CITY
            //*  05/05/09
                                                                                                                                                                          //Natural: PERFORM CHECK-INVALID-CHAR
            sub_Check_Invalid_Char();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_1().setValue(pnd_Chk_Fields_Pnd_Name_1);                                                                               //Natural: ASSIGN #Q-NAME-1 := #NAME-1
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_2().setValue(pnd_Chk_Fields_Pnd_Name_2);                                                                               //Natural: ASSIGN #Q-NAME-2 := #NAME-2
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_3().setValue(pnd_Chk_Fields_Pnd_Name_3);                                                                               //Natural: ASSIGN #Q-NAME-3 := #NAME-3
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_1().setValue(pnd_Chk_Fields_Pnd_Addr_1);                                                                             //Natural: ASSIGN #Q-STREET-1 := #ADDR-1
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_2().setValue(pnd_Chk_Fields_Pnd_Addr_2);                                                                             //Natural: ASSIGN #Q-STREET-2 := #ADDR-2
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_City().setValue(pnd_Chk_Fields_Pnd_City_3);                                                                                 //Natural: ASSIGN #Q-CITY := #CITY-3
            //*                                              ENDS HERE     11/30/05 RM
            //*                                                      /* &&& - FATCA
            if (condition(ldaTwrl9705.getForm_U_Tirf_Future_Use().getSubstring(20,1).equals("Y")))                                                                        //Natural: IF SUBSTR ( TIRF-FUTURE-USE,20,1 ) = 'Y'
            {
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Status().setValue(" ");                                                                                       //Natural: ASSIGN #Q-CHAPTER-3-STATUS := ' '
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Status_X().setValue(ldaTwrl9705.getForm_U_Tirf_Chptr4_Status_Cde().getValue(1));                              //Natural: ASSIGN #Q-CHAPTER-4-STATUS-X := TIRF-CHPTR4-STATUS-CDE ( 1 )
                //*    #Q-CHAPTER-4-WTHHLDNG-IND     := 1                   /* SINHASN
                //*    #Q-CHAPTER-3-WTHHLDNG-IND     := ' '                 /* SINHASN
                //*  SINHASN
                //*  SINHASN
                if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                              //Natural: IF TIRF-TAX-YEAR GE '2016'
                {
                    ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_Ind().setValue(4);                                                                                          //Natural: ASSIGN #Q-CHAPTER-IND := 4
                    //*  SINHASN
                }                                                                                                                                                         //Natural: END-IF
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Giin().setValue(ldaTwrl9705.getForm_U_Tirf_Giin());                                                                     //Natural: ASSIGN #Q-GIIN := TIRF-GIIN
                //* *#Q-CSINHASN4-STATUS-X         := TIRF-CHPTR4-STATUS-CDE (1)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Status().setValue(19);                                                                                        //Natural: ASSIGN #Q-CHAPTER-3-STATUS := 19
                ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Status_X().setValue(" ");                                                                                     //Natural: ASSIGN #Q-CHAPTER-4-STATUS-X := ' '
                //*  #Q-CHAPTER-3-WTHHLDNG-IND     := 1                    /* SINHASN
                //*  #Q-CHAPTER-4-WTHHLDNG-IND     := ' '                  /* SINHASN
                //*  SINHASN
                //*  SINHASN
                //*  SINHASN
                if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().greaterOrEqual("2016")))                                                                              //Natural: IF TIRF-TAX-YEAR GE '2016'
                {
                    ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_Ind().setValue(3);                                                                                          //Natural: ASSIGN #Q-CHAPTER-IND := 3
                    ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Status().setValue(16);                                                                                    //Natural: ASSIGN #Q-CHAPTER-3-STATUS := 16
                    //*  SINHASN
                }                                                                                                                                                         //Natural: END-IF
                //*  END - FATCA
                //*  SINHASN
                //*  MUKHERR
                //* MUKHERR
                //*  MUKHERR
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Reserved_6().setValue(" ");                                                                                                 //Natural: ASSIGN #Q-RESERVED-6 := ' '
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Lob_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Lob());                                                                      //Natural: ASSIGN #Q-LOB-CODE := TIRF-LOB
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Org_Cnt_Num().setValue(ldaTwrl9705.getForm_U_Tirf_1042s_Org_Cntl_Num());                                                    //Natural: ASSIGN #Q-ORG-CNT-NUM := TIRF-1042S-ORG-CNTL-NUM
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Amnd_Num().setValue(ldaTwrl9705.getForm_U_Tirf_1042s_Amd_Num());                                                            //Natural: ASSIGN #Q-AMND-NUM := TIRF-1042S-AMD-NUM
            //* *MOVE EDITED TIRF-DOB (EM=DDMMYYYY) TO #Q-DATE-BIRTH-X
            //*  VIKRAM
            //*  DUTTAD
            //*  DASRAH
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Date_Birth_X().setValueEdited(ldaTwrl9705.getForm_U_Tirf_Dob(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED TIRF-DOB ( EM = YYYYMMDD ) TO #Q-DATE-BIRTH-X
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Pri_Wthhldng_Agent_Name_L1().setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_1());                           //Natural: ASSIGN #Q-PRI-WTHHLDNG-AGENT-NAME-L1 := #W-WTHHLDNG-AGENT-NAME-1
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Pri_Wthhldng_Agent_Ein_X().setValue(ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Ein());                                //Natural: ASSIGN #Q-PRI-WTHHLDNG-AGENT-EIN-X := #W-WTHHLDNG-AGENT-EIN
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Rcpnt_Forgn_Tax_Id_Nbr().setValue(ldaTwrl9705.getForm_U_Tirf_Fin());                                                        //Natural: ASSIGN #Q-RCPNT-FORGN-TAX-ID-NBR := TIRF-FIN
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tax_Assumed_Wthhldng_Agent().setValue("000000000000");                                                                      //Natural: ASSIGN #Q-TAX-ASSUMED-WTHHLDNG-AGENT := '000000000000'
            getWorkFiles().write(2, false, ldaTwrl5906.getPnd_Q_Record());                                                                                                //Natural: WRITE WORK FILE 2 #Q-RECORD
            pnd_Q_Record_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #Q-RECORD-CNT
            pnd_Total_Q_Record_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-Q-RECORD-CNT
            pnd_File_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #FILE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-REPORT
            sub_Write_Detail_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("X")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'X'
            {
                pnd_Q_Record_Cnt_Trst.nadd(1);                                                                                                                            //Natural: ADD 1 TO #Q-RECORD-CNT-TRST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Q_Record_Cnt_Tiaa.nadd(1);                                                                                                                            //Natural: ADD 1 TO #Q-RECORD-CNT-TIAA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_C_Record() throws Exception                                                                                                                    //Natural: WRITE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaTwrl5906.getPnd_C_Record_Pnd_C_Record_Type().setValue("C");                                                                                                    //Natural: ASSIGN #C-RECORD-TYPE := 'C'
        pnd_Rec_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-CNT
        ldaTwrl5906.getPnd_C_Record_Pnd_C_Tot_Q_Record_Cnt().setValue(pnd_Q_Record_Cnt);                                                                                  //Natural: MOVE #Q-RECORD-CNT TO #C-TOT-Q-RECORD-CNT
        ldaTwrl5906.getPnd_C_Record_Pnd_C_Tot_Gross_Amt_Paid().setValueEdited(pnd_Total_Gross_Income,new ReportEditMask("999999999999999"));                              //Natural: MOVE EDITED #TOTAL-GROSS-INCOME ( EM = 9 ( 15 ) ) TO #C-TOT-GROSS-AMT-PAID
        ldaTwrl5906.getPnd_C_Record_Pnd_C_Tot_Us_Tax_Wthhld().setValueEdited(pnd_Total_Federal_Tax,new ReportEditMask("999999999999999"));                                //Natural: MOVE EDITED #TOTAL-FEDERAL-TAX ( EM = 9 ( 15 ) ) TO #C-TOT-US-TAX-WTHHLD
        ldaTwrl5906.getPnd_C_Record_Pnd_C_Rec_Seq_Num().setValue(pnd_Rec_Cnt);                                                                                            //Natural: ASSIGN #C-REC-SEQ-NUM := #REC-CNT
        getWorkFiles().write(2, false, ldaTwrl5906.getPnd_C_Record());                                                                                                    //Natural: WRITE WORK FILE 2 #C-RECORD
        pnd_File_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #FILE-CNT
    }
    //*  END OF TRANSMISSION RECORD
    private void sub_Write_F_Record() throws Exception                                                                                                                    //Natural: WRITE-F-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        ldaTwrl5906.getPnd_F_Record().reset();                                                                                                                            //Natural: RESET #F-RECORD
        pnd_Rec_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-CNT
        ldaTwrl5906.getPnd_F_Record_Pnd_F_Record_Type().setValue("F");                                                                                                    //Natural: ASSIGN #F-RECORD-TYPE := 'F'
        ldaTwrl5906.getPnd_F_Record_Pnd_F_Wthhldng_Agent_Cnt().setValueEdited(pnd_Wthhldng_Agent_Cnt,new ReportEditMask("999"));                                          //Natural: MOVE EDITED #WTHHLDNG-AGENT-CNT ( EM = 999 ) TO #F-WTHHLDNG-AGENT-CNT
        ldaTwrl5906.getPnd_F_Record_Pnd_F_Rec_Seq_Num().setValue(pnd_Rec_Cnt);                                                                                            //Natural: ASSIGN #F-REC-SEQ-NUM := #REC-CNT
        getWorkFiles().write(2, false, ldaTwrl5906.getPnd_F_Record());                                                                                                    //Natural: WRITE WORK FILE 2 #F-RECORD
        pnd_File_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #FILE-CNT
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  1042-S
        pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data().reset();                                                                                                            //Natural: RESET #TWRACOM2.#OUTPUT-DATA
        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Company_Code);                                                                                           //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #COMPANY-CODE
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN #TWRACOM2.#TAX-YEAR := VAL ( TIRF-TAX-YEAR )
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(3);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 03
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2());                                                                        //Natural: CALLNAT 'TWRNCOM2' #TWRACOM2
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Code().getBoolean())))                                                                                       //Natural: IF NOT #TWRACOM2.#RET-CODE
        {
            getReports().write(0, pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Msg(),"TERMINATING PROGRAM",Global.getPROGRAM());                                                   //Natural: WRITE #TWRACOM2.#RET-MSG 'TERMINATING PROGRAM' *PROGRAM
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Withholding_Agent_Info() throws Exception                                                                                                   //Natural: POPULATE-WITHHOLDING-AGENT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        ldaTwrl5906.getPnd_W_Record().reset();                                                                                                                            //Natural: RESET #W-RECORD
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Ein().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                        //Natural: ASSIGN #W-WTHHLDNG-AGENT-EIN := #TWRACOM2.#FED-ID
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name());                                                  //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-1 := #TWRACOM2.#COMP-NAME
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Street_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                      //Natural: ASSIGN #W-WTHHLDNG-AGENT-STREET-1 := #TWRACOM2.#ADDRESS ( 1 )
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                         //Natural: ASSIGN #W-WTHHLDNG-AGENT-CITY := #TWRACOM2.#CITY
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                       //Natural: ASSIGN #W-WTHHLDNG-AGENT-STATE := #TWRACOM2.#STATE
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                         //Natural: ASSIGN #W-WTHHLDNG-AGENT-ZIP := #TWRACOM2.#ZIP-9
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Contact_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Name());                                         //Natural: ASSIGN #W-WTHHLDNG-AGENT-CONTACT-NAME := #TWRACOM2.#CONTACT-NAME
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Dept_Title().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Contact_Title());                                          //Natural: ASSIGN #W-WTHHLDNG-AGENT-DEPT-TITLE := #TWRACOM2.#CONTACT-TITLE
        ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Phone_Nbr().setValue(ldaTwrl5906.getPnd_T_Record_Pnd_T_Contact_Phone_Nbr());                                     //Natural: ASSIGN #W-WTHHLDNG-AGENT-PHONE-NBR := #T-CONTACT-PHONE-NBR
        //*  SERVICES OR TRUST   02/05 RM
        if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().equals("X") || pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().equals("S")))                                //Natural: IF #TWRACOM2.#COMP-CODE = 'X' OR = 'S'
        {
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_1().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Agent_A());                                           //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-1 := #TWRACOM2.#COMP-AGENT-A
            ldaTwrl5906.getPnd_W_Record_Pnd_W_Wthhldng_Agent_Name_2().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Agent_B());                                           //Natural: ASSIGN #W-WTHHLDNG-AGENT-NAME-2 := #TWRACOM2.#COMP-AGENT-B
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wthhldng_Agent_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #WTHHLDNG-AGENT-CNT
    }
    private void sub_Get_Province_Code() throws Exception                                                                                                                 //Natural: GET-PROVINCE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pdaTwratbl2.getTwratbl2().reset();                                                                                                                                //Natural: RESET TWRATBL2 #PROVINCE-CODE
        pnd_Province_Code.reset();
        if (condition(ldaTwrl9705.getForm_U_Tirf_Province_Code().equals(" ")))                                                                                            //Natural: IF TIRF-PROVINCE-CODE = ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  NUMERIC LOOKUP
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Province_Code());                                                                     //Natural: ASSIGN TWRATBL2.#STATE-CDE := TIRF-PROVINCE-CODE
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2());                                                                            //Natural: CALLNAT 'TWRNTBL2' TWRATBL2
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean())))                                                                                         //Natural: IF NOT TWRATBL2.#RETURN-CDE
        {
            getReports().write(0, "Province code",ldaTwrl9705.getForm_U_Tirf_Province_Code(),"not found on state table");                                                 //Natural: WRITE 'Province code' TIRF-PROVINCE-CODE 'not found on state table'
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Province_Code.setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                                                   //Natural: ASSIGN #PROVINCE-CODE := TIRCNTL-STATE-ALPHA-CODE
    }
    private void sub_Get_Irs_Country_Name() throws Exception                                                                                                              //Natural: GET-IRS-COUNTRY-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  ALPHA CODE LOOKUP
        pdaTwratbl3.getTwratbl3().reset();                                                                                                                                //Natural: RESET TWRATBL3 #IRS-COUNTRY-NAME
        pnd_Irs_Country_Name.reset();
        pdaTwratbl3.getTwratbl3_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL3.#FUNCTION := '1'
        pdaTwratbl3.getTwratbl3_Pnd_Country_Cde().setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Country_Code());                                                                //Natural: ASSIGN TWRATBL3.#COUNTRY-CDE := TIRF-IRS-COUNTRY-CODE
        pdaTwratbl3.getTwratbl3_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl3.getTwratbl3_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN TWRATBL3.#TAX-YEAR := VAL ( TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrntbl3.class , getCurrentProcessState(), pdaTwratbl3.getTwratbl3());                                                                            //Natural: CALLNAT 'TWRNTBL3' TWRATBL3
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwratbl3.getTwratbl3_Pnd_Return_Cde().getBoolean())))                                                                                         //Natural: IF NOT TWRATBL3.#RETURN-CDE
        {
            getReports().write(0, "Country code",ldaTwrl9705.getForm_U_Tirf_Irs_Country_Code(),"not found on country tbl");                                               //Natural: WRITE 'Country code' TIRF-IRS-COUNTRY-CODE 'not found on country tbl'
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Irs_Country_Name.setValue(pdaTwratbl3.getTwratbl3_Tircntl_Cntry_Full_Name());                                                                                 //Natural: ASSIGN #IRS-COUNTRY-NAME := TIRCNTL-CNTRY-FULL-NAME
    }
    private void sub_Get_State_Code() throws Exception                                                                                                                    //Natural: GET-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaTwratbl2.getTwratbl2().reset();                                                                                                                                //Natural: RESET TWRATBL2 #STATE
        pdaTwracom2.getPnd_Twracom2_Pnd_State().reset();
        //*  FO 12/01 GEO IS NOW POPULATED FOR
        if (condition(! (DbsUtil.is(ldaTwrl9705.getForm_U_Tirf_Geo_Cde().getText(),"N2"))))                                                                               //Natural: IF NOT TIRF-GEO-CDE IS ( N2 )
        {
            //*           FOR COUNTRIES.  SO ONLY
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*           PERFORM STATE LOOKUP FOR
            //*  NUMERIC LOOKUP
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                             //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
        pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaTwrl9705.getForm_U_Tirf_Geo_Cde()));                     //Natural: COMPRESS '0' TIRF-GEO-CDE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( TIRF-TAX-YEAR )
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2());                                                                            //Natural: CALLNAT 'TWRNTBL2' TWRATBL2
        if (condition(Global.isEscape())) return;
        if (condition(! (pdaTwratbl2.getTwratbl2_Pnd_Return_Cde().getBoolean())))                                                                                         //Natural: IF NOT TWRATBL2.#RETURN-CDE
        {
            getReports().write(0, "Geo code",ldaTwrl9705.getForm_U_Tirf_Geo_Cde(),"not found on state table");                                                            //Natural: WRITE 'Geo code' TIRF-GEO-CDE 'not found on state table'
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwracom2.getPnd_Twracom2_Pnd_State().setValue(pdaTwratbl2.getTwratbl2_Tircntl_State_Alpha_Code());                                                             //Natural: ASSIGN #STATE := TIRCNTL-STATE-ALPHA-CODE
    }
    //*      08/22/05     RM
    private void sub_Check_Invalid_Char() throws Exception                                                                                                                //Natural: CHECK-INVALID-CHAR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************** /* UPDATED TO CHECK BOTH 'Q' & 'W'
        //*                                                        11/30/05     RM
        //*                                     /* VALIDATED FOR 2006  11/15/06 RM
        //*  05/05/09
        pnd_Counters_Pnd_Valid_Cha_Cnt.reset();                                                                                                                           //Natural: RESET #VALID-CHA-CNT #INVLD-CHA-CNT
        pnd_Counters_Pnd_Invld_Cha_Cnt.reset();
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 240
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(240)); pnd_I.nadd(1))
        {
            //*  02/08/11
            //*  02/08/11
            if (condition(pnd_Chk_Fields_Pnd_Chk_Char.getValue(1).equals("%") || DbsUtil.maskMatches(pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I),"C")                     //Natural: IF #CHK-CHAR ( 1 ) = '%' OR ( #CHK-CHAR ( #I ) = MASK ( C ) ) OR ( #CHK-CHAR ( #I ) = '&' OR = '-' OR = ',' OR = "'" OR = '/' OR = '#' OR = '.' )
                || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("&") || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("-") || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals(",") 
                || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("'") || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("/") || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("#") 
                || pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals(".")))
            {
                pnd_Counters_Pnd_Valid_Cha_Cnt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #VALID-CHA-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counters_Pnd_Invld_Cha_Cnt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #INVLD-CHA-CNT
                //*  <<< COMMENT THE FOLLOWING DISPLAY IN PRODUCTION!!! >>>
                //*    DISPLAY '=' #CHK-CHAR (#I) '=' #I '=' #INVLD-CHA-CNT '=' #NAME-1
                //*  05/05/09
                if (condition(pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("(")))                                                                                   //Natural: IF #CHK-CHAR ( #I ) = '('
                {
                    pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).setValue("A");                                                                                            //Natural: ASSIGN #CHK-CHAR ( #I ) := 'A'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).equals("�")))                                                                               //Natural: IF #CHK-CHAR ( #I ) = '�'
                    {
                        pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).setValue("a");                                                                                        //Natural: ASSIGN #CHK-CHAR ( #I ) := 'a'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Chk_Fields_Pnd_Chk_Char.getValue(pnd_I).setValue(" ");                                                                                        //Natural: ASSIGN #CHK-CHAR ( #I ) := ' '
                    }                                                                                                                                                     //Natural: END-IF
                    //*  05/05/09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Counters_Pnd_Invld_Cha_Cnt.greater(getZero())))                                                                                                 //Natural: IF #INVLD-CHA-CNT GT 0
        {
            pnd_Counters_Pnd_Invld_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #INVLD-CNT
            pnd_Counters_Pnd_Invld_Cha_Tot.nadd(pnd_Counters_Pnd_Invld_Cha_Cnt);                                                                                          //Natural: ADD #INVLD-CHA-CNT TO #INVLD-CHA-TOT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Counters_Pnd_Valid_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #VALID-CNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Detail_Report() throws Exception                                                                                                               //Natural: WRITE-DETAIL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //*  FO 11/02
        getReports().display(4, new ReportEmptyLineSuppression(true),"//Tax-ID/Number",                                                                                   //Natural: DISPLAY ( 4 ) ( ES = ON ) '//Tax-ID/Number' #Q-TIN '//Account/Number' #Q-ACCOUNT-NBR VERT AS '///Name-Address Lines' #Q-NAME-1 #Q-STREET-1 #Q-STREET-2 #Q-CITY #Q-STATE #Q-ZIP #Q-PROVINCE-CODE #Q-COUNTRY-CODE HORIZ '/R/E/T' #Q-RETURN-TYPE-IND 'C/o/m/p' TIRF-COMPANY-CDE '(1)' 5X '/' #Q-INCOME-CODE ( AD = R LC = ���������� ) / '(2)' '/' #Q-GROSS-INCOME-N ( AD = R LC = � EM = -ZZZ,ZZZ,ZZZ,ZZ9 ) / '(3a)' 2X 'Amounts' #Q-CHAPTER-3-EXEMPT-CDE ( LC = ���������� ) / '(3b)' 1X '/' #Q-CHAPTER-3-TAX-RATE-N ( AD = R LC = ���������� EM = Z9.99 ) / '(4a)' 5X '/' #Q-CHAPTER-4-EXEMPT-CDE ( LC = ���������� ) / '(4b)' '/' #Q-CHAPTER-4-TAX-RATE-N ( AD = R LC = ���������� EM = Z9.99 ) / '(7)' '/' #Q-US-TAX-WTHHLD-N ( AD = R LC = � EM = -ZZZ,ZZZ,ZZZ,ZZ9 ) / '(13h)' 12X '/' #Q-CHAPTER-3-STATUS / '(13i)' '/' #Q-CHAPTER-4-STATUS-X / '(15)' 5X '/' #Q-RCPNT-CNTRY-RESIDENCE-CODE ( AD = R LC = ��������� ) / '(16)' 5X '/' #Q-COUNTRY-CODE ( AD = R LC = ��������� )
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tin(),"//Account/Number",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Account_Nbr(),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "///Name-Address Lines",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_1(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_1(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_2(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_City(),
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_State(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Zip(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Province_Code(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code(),
            ReportMatrixOutputType.HORIZONTAL,"/R/E/T",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Return_Type_Ind(),"C/o/m/p",
        		ldaTwrl9705.getForm_U_Tirf_Company_Cde(),"(1)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Income_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),NEWLINE,"(2)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Gross_Income_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=�"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),
            NEWLINE,"(3a)",
        		new ColumnSpacing(2),"Amounts",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Exempt_Cde(), new FieldAttributes("LC=����������"),NEWLINE,"(3b)",
        		new ColumnSpacing(1),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Tax_Rate_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"), new ReportEditMask 
            ("Z9.99"),NEWLINE,"(4a)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Exempt_Cde(), new FieldAttributes("LC=����������"),NEWLINE,"(4b)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Tax_Rate_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"), new ReportEditMask 
            ("Z9.99"),NEWLINE,"(7)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Us_Tax_Wthhld_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=�"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),
            NEWLINE,"(13h)",
        		new ColumnSpacing(12),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Status(),NEWLINE,"(13i)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Status_X(),NEWLINE,"(15)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Rcpnt_Cntry_Residence_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"),NEWLINE,"(16)",
            
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"));
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 4 ) 1
    }
    //*  01/30/08 - AY
    //*  1042-S
    private void sub_Read_Control_Table() throws Exception                                                                                                                //Natural: READ-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := VAL ( TIRF-TAX-YEAR )
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val());
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(3);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 3
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  01/30/08 - AY
        //*  01/30/08 - AY
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Form...............: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(3 + 1),NEWLINE,new  //Natural: WRITE ( 1 ) / 10T 'Form...............: ' #TWRAFRMN.#FORM-NAME ( 3 ) / 10T 'IRS Reporting......: ' #TWRATBL4.#TIRCNTL-IRS-ORIG ( EM = N/Y ) //
            TabSetting(10),"IRS Reporting......: ",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig(), new ReportEditMask ("N/Y"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Cntl_Ndx.setValue(1);                                                                                                                                         //Natural: MOVE 1 TO #CNTL-NDX
        pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(1).setValue(pnd_Sys_Date);                                                                                 //Natural: ASSIGN #TWRATBL4.TIRCNTL-RPT-DTE ( 1 ) := #SYS-DATE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().setValue(true);                                                                                                //Natural: ASSIGN #TWRATBL4.#TIRCNTL-IRS-ORIG := TRUE
    }
    private void sub_Update_Control_Table() throws Exception                                                                                                              //Natural: UPDATE-CONTROL-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Update_Form_File() throws Exception                                                                                                                  //Natural: UPDATE-FORM-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*   UPDATE FORM FILE
        UPDATE_FORM:                                                                                                                                                      //Natural: GET FORM-U #1042S-ISN
        ldaTwrl9705.getVw_form_U().readByID(pnd_1042s_Isn.getLong(), "UPDATE_FORM");
        //*  HELD NOT REPORTED
        if (condition(ldaTwrl9705.getForm_U_Tirf_Irs_Reject_Ind().equals("Y")))                                                                                           //Natural: IF TIRF-IRS-REJECT-IND = 'Y'
        {
            ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Ind().setValue("H");                                                                                                       //Natural: ASSIGN TIRF-IRS-RPT-IND := 'H'
            //*  ORIGINAL REPORTING
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Ind().setValue("O");                                                                                                       //Natural: ASSIGN TIRF-IRS-RPT-IND := 'O'
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl9705.getForm_U_Tirf_Irs_Rpt_Date().setValue(pnd_Sys_Date);                                                                                                 //Natural: ASSIGN TIRF-IRS-RPT-DATE := #SYS-DATE
        ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Sys_Time);                                                                                                        //Natural: ASSIGN TIRF-LU-TS := #SYS-TIME
        ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN TIRF-LU-USER := *INIT-USER
        ldaTwrl9705.getVw_form_U().updateDBRow("UPDATE_FORM");                                                                                                            //Natural: UPDATE ( UPDATE-FORM. )
        pnd_Et_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #ET-CNT
        if (condition(pnd_Et_Cnt.greaterOrEqual(50)))                                                                                                                     //Natural: IF #ET-CNT GE 50
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Cnt.reset();                                                                                                                                           //Natural: RESET #ET-CNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Environment_Identification() throws Exception                                                                                                        //Natural: ENVIRONMENT-IDENTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------**
        //*  DETERMINE IF 'PROD' OR 'TEST' ENVIRONMENT.
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"- Calling Sub-Program 'TWRN0001'...");                                                                                 //Natural: WRITE ( 00 ) *PROGRAM '- Calling Sub-Program "TWRN0001"...'
        if (Global.isEscape()) return;
        pdaTwra0001.getTwra0001().reset();                                                                                                                                //Natural: RESET TWRA0001
        DbsUtil.callnat(Twrn0001.class , getCurrentProcessState(), pdaTwra0001.getTwra0001());                                                                            //Natural: CALLNAT 'TWRN0001' TWRA0001
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Prod_Test());                                                                                               //Natural: WRITE ( 00 ) '=' TWRA0001.#PROD-TEST
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return());                                                                                         //Natural: WRITE ( 00 ) '=' TWRA0001.#TWRN0001-RETURN
        if (Global.isEscape()) return;
        //* *----------------------------------------**
        //*  ENVIRONMENT-IDENTIFICATION
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "#1042S-ISN:",pnd_1042s_Isn,"#TEST-IND:",pnd_Test_Ind);                                                                                     //Natural: WRITE '#1042S-ISN:'#1042S-ISN "#TEST-IND:" #TEST-IND
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventForm() throws Exception {atBreakEventForm(false);}
    private void atBreakEventForm(boolean endOfData) throws Exception
    {
        boolean pnd_Display_KeyIsBreak = pnd_Display_Key.isBreak(endOfData);
        boolean ldaTwrl9705_getForm_U_Tirf_Company_CdeIsBreak = ldaTwrl9705.getForm_U_Tirf_Company_Cde().isBreak(endOfData);
        if (condition(pnd_Display_KeyIsBreak || ldaTwrl9705_getForm_U_Tirf_Company_CdeIsBreak))
        {
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 3 )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl9705_getForm_U_Tirf_Company_CdeIsBreak))
        {
            //*  RECONCILIATION RECORD (NEW, 2001)
                                                                                                                                                                          //Natural: PERFORM WRITE-C-RECORD
            sub_Write_C_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-ALL-TOTALS
            sub_Write_All_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  RCC - MOVED AFTER END- WORK /*
            //*    IF OLD(TIRF-COMPANY-CDE) = TIRF-COMPANY-CDE  /* END OF FILE
            //*      /* PRINT GRAND TOTALS
            //*      #NDX := 4
            //*      #FORM-CNT   := #TOTAL-IRS-REJECT-CNT + #TOTAL-ACCEPT-CNT
            //*      #RECORD-CNT := #TOTAL-Q-RECORD-CNT
            //*        #1042S-WH-FORM-CNT(#NDX)
            //*        := #TOTAL-WH-IRS-REJECT-CNT + #TOTAL-WH-ACCEPT-CNT
            //*      SKIP (2) 1
            //*      SKIP (5) 1
            //*      PERFORM WRITE-TOTALS
            //*      ESCAPE BOTTOM (FORM.) IMMEDIATE
            //*    END-IF
            //*  REJECT REPORT
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
            //*  ACCEPT REPORT
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
            pnd_Q_Record_Cnt.reset();                                                                                                                                     //Natural: RESET #Q-RECORD-CNT #Q-EMPTY-RECORD-CNT #TOTAL-GROSS-INCOME #TOTAL-FEDERAL-TAX
            pnd_Q_Empty_Record_Cnt.reset();
            pnd_Total_Gross_Income.reset();
            pnd_Total_Federal_Tax.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");
        Global.format(4, "LS=132 PS=55");
        Global.format(5, "LS=132 PS=55");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"1042-S IRS Original Reporting Control Report",new 
            TabSetting(120),"Report: RPT1",NEWLINE,NEWLINE,new TabSetting(10),"Tax Year: ",ldaTwrl9705.getForm_U_Tirf_Tax_Year(), new ReportEditMask ("XXXX"),
            NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"1042-S IRS Original Reporting",new TabSetting(120),"Report: RPT2",NEWLINE,NEWLINE,new 
            TabSetting(59),"Tax Year",ldaTwrl9705.getForm_U_Tirf_Tax_Year(), new ReportEditMask ("XXXX"),NEWLINE,NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),
            NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"1042-S IRS Original Reporting",new TabSetting(120),"Report: RPT3",NEWLINE,new 
            TabSetting(59),"Reject Report",NEWLINE,NEWLINE,"Company: ",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),NEWLINE,"Tax Year:",ldaTwrl9705.getForm_U_Tirf_Tax_Year(), 
            new ReportEditMask ("XXXX"),NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"1042-S IRS Original Reporting",new TabSetting(120),"Report: RPT4",NEWLINE,new 
            TabSetting(59),"Accept Report",NEWLINE,NEWLINE,"Company: ",pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Short_Name(),NEWLINE,"Tax Year:",ldaTwrl9705.getForm_U_Tirf_Tax_Year(), 
            new ReportEditMask ("XXXX"),NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,pnd_Sys_Date, new ReportEditMask ("MM/DD/YYYY"),"-",pnd_Sys_Time, new 
            ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(39),"1042-S IRS Original Withholding Reconciliation Report",new 
            TabSetting(120),"Report: RPT5",NEWLINE,NEWLINE,new TabSetting(59),"Tax Year",ldaTwrl9705.getForm_U_Tirf_Tax_Year(), new ReportEditMask ("XXXX"),
            NEWLINE,NEWLINE,"Form Type:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(3, new ReportEmptyLineSuppression(true),"////TIN",
        		ldaTwrl9705.getForm_U_Tirf_Tin(), new IdenticalSuppress(true),"////Contract",
        		ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(), new IdenticalSuppress(true),"////Payee",
        		ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new IdenticalSuppress(true),"Name",
        		pnd_Name,NEWLINE,ldaTwrl9705.getForm_U_Tirf_Street_Addr(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_Street_Addr_Cont_2(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_City(),
            NEWLINE,"Geo",
        		pnd_Reject_Geo,NEWLINE,ldaTwrl9705.getForm_U_Tirf_Province_Code(),NEWLINE,ldaTwrl9705.getForm_U_Tirf_Zip(), new ReportEditMask ("XXXXXXXXX"),
            NEWLINE,ldaTwrl9705.getForm_U_Tirf_Country_Name(),"(1)",
        		new ColumnSpacing(4),"/",
        		ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),NEWLINE,"(2)",
        		"/",
        		pnd_Rounded_Gross, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(5)",
        		"//Amounts",
        		ldaTwrl9705.getForm_U_Tirf_1042_S_Tax_Rate(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"),NEWLINE,"(6)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl9705.getForm_U_Tirf_Exempt_Code(), new FieldAttributes("LC=����������"),NEWLINE,"(7)",
        		"/",
        		pnd_Rounded_Tax_Wthld, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(8)",
        		"/",
        		pnd_Rounded_Refund_Amt, new FieldAttributes ("AD=R"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,"(15)",
        		new ColumnSpacing(3),"/",
        		ldaTwrl9705.getForm_U_Tirf_Irs_Country_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),NEWLINE,"(16)",
        		new ColumnSpacing(3),"/",
        		ldaTwrl9705.getForm_U_Tirf_Geo_Cde(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"));
        getReports().setDisplayColumns(2, " ",
        		pnd_1042s_Control_Totals_Pnd_1042s_Description,"Form/Count",
        		pnd_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"Q Rec/Count",
        		pnd_Record_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Control_Totals_Pnd_1042s_Gross_Income, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Control_Totals_Pnd_1042s_Pension_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Control_Totals_Pnd_1042s_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_Nra_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"FATCA Tax/Withheld",
        		pnd_1042s_Control_Totals_Pnd_1042s_C4_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(5, " ",
        		pnd_1042s_Control_Totals_Pnd_1042s_Description,"Form/Count",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Form_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"/Gross Income",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Gross_Income, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"Pension/Annuities",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Pension_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Interest",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Interest_Amt, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"NRA Tax/Withheld",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Nra_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"FATCA Tax/Withheld",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_C4_Tax_Wthld, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),"/Amt Repaid",
        		pnd_1042s_Wh_Control_Totals_Pnd_1042s_Wh_Refund_Amt, new ReportEditMask ("-ZZ,ZZ9.99"));
        getReports().setDisplayColumns(4, new ReportEmptyLineSuppression(true),"//Tax-ID/Number",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Tin(),"//Account/Number",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Account_Nbr(),ReportMatrixOutputType.VERTICAL,ReportMatrixVertical.AS, "///Name-Address Lines",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Name_1(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_1(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Street_2(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_City(),
            ldaTwrl5906.getPnd_Q_Record_Pnd_Q_State(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Zip(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Province_Code(),ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code(),
            ReportMatrixOutputType.HORIZONTAL,"/R/E/T",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Return_Type_Ind(),"C/o/m/p",
        		ldaTwrl9705.getForm_U_Tirf_Company_Cde(),"(1)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Income_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"),NEWLINE,"(2)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Gross_Income_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=�"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),
            NEWLINE,"(3a)",
        		new ColumnSpacing(2),"Amounts",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Exempt_Cde(), new FieldAttributes("LC=����������"),NEWLINE,"(3b)",
        		new ColumnSpacing(1),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Tax_Rate_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"), new ReportEditMask 
            ("Z9.99"),NEWLINE,"(4a)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Exempt_Cde(), new FieldAttributes("LC=����������"),NEWLINE,"(4b)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Tax_Rate_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=����������"), new ReportEditMask 
            ("Z9.99"),NEWLINE,"(7)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Us_Tax_Wthhld_N(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=�"), new ReportEditMask ("-ZZZ,ZZZ,ZZZ,ZZ9"),
            NEWLINE,"(13h)",
        		new ColumnSpacing(12),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_3_Status(),NEWLINE,"(13i)",
        		"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Chapter_4_Status_X(),NEWLINE,"(15)",
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Rcpnt_Cntry_Residence_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"),NEWLINE,"(16)",
            
        		new ColumnSpacing(5),"/",
        		ldaTwrl5906.getPnd_Q_Record_Pnd_Q_Country_Code(), new FieldAttributes ("AD=R"), new FieldAttributes("LC=���������"));
    }
    private void CheckAtStartofData1140() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  10/01/08
                                                                                                                                                                          //Natural: PERFORM ENVIRONMENT-IDENTIFICATION
            sub_Environment_Identification();
            if (condition(Global.isEscape())) {return;}
            //*  10/01/08
            if (condition(pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return().notEquals("00")))                                                                                 //Natural: IF #TWRN0001-RETURN NE '00'
            {
                getReports().write(0, "Invalid Return Code",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return(),"from TWRN0001Environment Routine!");                           //Natural: WRITE 'Invalid Return Code' #TWRN0001-RETURN 'from TWRN0001Environment Routine!'
                if (condition(Global.isEscape())) return;
                DbsUtil.terminate(16);  if (true) return;                                                                                                                 //Natural: TERMINATE 16
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-TABLE
            sub_Read_Control_Table();
            if (condition(Global.isEscape())) {return;}
            //*  GET TIAA COMPANY INFO
            //*  FOR CREF PAYER FIELDS
            //*  ADD 'X'     02/04/05  RM
            if (condition(ldaTwrl9705.getForm_U_Tirf_Company_Cde().equals("X")))                                                                                          //Natural: IF TIRF-COMPANY-CDE = 'X'
            {
                pnd_Company_Code.setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                                                      //Natural: ASSIGN #COMPANY-CODE := TIRF-COMPANY-CDE
                //* EINCHG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* EINCHG
                //* EINCHG
                if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().greaterOrEqual("2018")))                                                                              //Natural: IF TIRF-TAX-YEAR >= '2018'
                {
                    pnd_Company_Code.setValue("A");                                                                                                                       //Natural: ASSIGN #COMPANY-CODE := 'A'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Company_Code.setValue("T");                                                                                                                       //Natural: ASSIGN #COMPANY-CODE := 'T'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
            sub_Get_Company_Info();
            if (condition(Global.isEscape())) {return;}
            pnd_Tiaa_Company_Name.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Name());                                                                                  //Natural: ASSIGN #TIAA-COMPANY-NAME := #TWRACOM2.#COMP-NAME
            pnd_Tiaa_Company_Tin.setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                                                      //Natural: ASSIGN #TIAA-COMPANY-TIN := #TWRACOM2.#FED-ID
            //*  TRANSMITTER RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-T-RECORD
            sub_Write_T_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_Company_Code.setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                                                          //Natural: ASSIGN #COMPANY-CODE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
            sub_Get_Company_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM POPULATE-WITHHOLDING-AGENT-INFO
            sub_Populate_Withholding_Agent_Info();
            if (condition(Global.isEscape())) {return;}
            //*  WITHHOLDING AGENT RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-W-RECORD
            sub_Write_W_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
