/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:03 PM
**        * FROM NATURAL PROGRAM : Twrp3001
************************************************************
**        * FILE NAME            : Twrp3001.java
**        * CLASS NAME           : Twrp3001
**        * INSTANCE NAME        : Twrp3001
************************************************************
* PROGRAM : TWRP3001
* FUNCTION: CREATES A FLAT FILE THAT CONTAINS RECORDS OF ALL RESIDENTS
*            OF NEW YORK WITH STATE WITHHOLD AMOUNT AND WITHIN
*            SPECIFIED INTERFACE DATE AND PAYMENT DATE.
* INPUT   : FILE 94 (TWRPYMNT-PAYMENT-FILE)
* UPDATE  : TO SELECT ALL RECORDS TO BE PROCESSED BASED ON
*            INTERFACE DATE AND PAYMENT DATE SINCE THE DATABASE WILL
*            NOW CONTAIN FILES FOR 3 TAX YEARS (3/15/99 - EDITH)
*           EX : FOR TAX YEAR 1998
*     QTR    INTERFACE DATE     PAYMENT DATE     PARAM CARD
*     ---  -----------------  ---------------    ---------
*      1   19971201-RUNDATE   19980101-19980331  19980331 N
*      2   19971201-RUNDATE   19980101-19980630  19980630 N
*      3   19971201-RUNDATE   19980101-19980930  19980930 N
*      4   19971201-RUNDATE   19980101-19981231  19981231 N
*      5   19971201-RUNDATE   19980101-19981231  19981231 Y
*         :  EDITH 4/13/99
*                PYMNT-DATE-FROM = 1ST  DAY OF THE TAX YEAR
*                PYMNT-DATE-TO   = LAST DAY OF EACH QTR RUN
*                INTF-DATE-FROM  = LAST MONTH OF PRIOR TAX YEAR
*                INTF-DATE-TO    = RUNDATE
* THIS PROGRAM CLONED FROM TWRP3040
********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3001 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3041 ldaTwrl3041;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_payr;
    private DbsField payr_Twrpymnt_Tax_Year;
    private DbsField payr_Twrpymnt_Tax_Id_Type;
    private DbsField payr_Twrpymnt_Tax_Id_Nbr;
    private DbsField payr_Twrpymnt_Company_Cde;
    private DbsField payr_Twrpymnt_Tax_Citizenship;
    private DbsField payr_Twrpymnt_Contract_Nbr;
    private DbsField payr_Twrpymnt_Payee_Cde;
    private DbsField payr_Twrpymnt_Distribution_Cde;
    private DbsField payr_Twrpymnt_Residency_Type;
    private DbsField payr_Twrpymnt_Residency_Code;
    private DbsField payr_Twrpymnt_Ivc_Indicator;
    private DbsField payr_Twrpymnt_Paymt_Category;
    private DbsField payr_Twrpymnt_Locality_Cde;
    private DbsField payr_Count_Casttwrpymnt_Payments;

    private DbsGroup payr_Twrpymnt_Payments;
    private DbsField payr_Twrpymnt_Pymnt_Dte;
    private DbsField payr_Twrpymnt_Pymnt_Status;
    private DbsField payr_Twrpymnt_Pymnt_Intfce_Dte;
    private DbsField payr_Twrpymnt_Updte_Srce_Cde;
    private DbsField payr_Twrpymnt_Orgn_Srce_Cde;
    private DbsField payr_Twrpymnt_Payment_Type;
    private DbsField payr_Twrpymnt_Settle_Type;
    private DbsField payr_Twrpymnt_Gross_Amt;
    private DbsField payr_Twrpymnt_Ivc_Amt;
    private DbsField payr_Twrpymnt_Fed_Wthld_Amt;
    private DbsField payr_Twrpymnt_State_Wthld;
    private DbsField payr_Twrpymnt_Local_Wthld;
    private DbsField payr_Twrpymnt_Contract_Seq_No;
    private DbsField payr_Twrpymnt_Form_Sd;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Pay_Super_Start;
    private DbsField pnd_Ws_Pnd_Pay_Super_End;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Ritten;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Life_Cnt;
    private DbsField pnd_Serv_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Fsb_Cnt;
    private DbsField pnd_Trst_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Intf_Date_To;

    private DbsGroup pnd_Intf_Date_To__R_Field_1;
    private DbsField pnd_Intf_Date_To_Pnd_Intf_Date_To_N;
    private DbsField pnd_Intf_Date_From;

    private DbsGroup pnd_Intf_Date_From__R_Field_2;
    private DbsField pnd_Intf_Date_From_Pnd_Intf_Date_From_Yy;
    private DbsField pnd_Intf_Date_From_Pnd_Intf_Date_From_Mm;
    private DbsField pnd_Intf_Date_From_Pnd_Intf_Date_From_Dd;
    private DbsField pnd_Pymnt_Date_From;

    private DbsGroup pnd_Pymnt_Date_From__R_Field_3;
    private DbsField pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Yy;
    private DbsField pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Mm;
    private DbsField pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Dd;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_4;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To;

    private DbsGroup pnd_Input_Parm__R_Field_5;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy;

    private DbsGroup pnd_Input_Parm__R_Field_6;
    private DbsField pnd_Input_Parm_Pnd_Parm_Tax_Year;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm;
    private DbsField pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd;
    private DbsField pnd_Input_Parm_Pnd_Filler2;
    private DbsField pnd_Input_Parm_Pnd_Year_End_Adj_Ind;
    private DbsField pnd_Run_Type;
    private DbsField pnd_C_Int_Count;

    private DbsRecord internalLoopRecord;
    private DbsField rD_PAYTwrpymnt_Tax_YearOld;
    private DbsField rD_PAYTwrpymnt_Tax_Id_TypeOld;
    private DbsField rD_PAYTwrpymnt_Tax_Id_NbrOld;
    private DbsField rD_PAYTwrpymnt_Company_CdeOld;
    private DbsField rD_PAYTwrpymnt_Tax_CitizenshipOld;
    private DbsField rD_PAYTwrpymnt_Contract_NbrOld;
    private DbsField rD_PAYTwrpymnt_Payee_CdeOld;
    private DbsField rD_PAYTwrpymnt_Distribution_CdeOld;
    private DbsField rD_PAYTwrpymnt_Residency_TypeOld;
    private DbsField rD_PAYTwrpymnt_Residency_CodeOld;
    private DbsField rD_PAYTwrpymnt_Ivc_IndicatorOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3041 = new LdaTwrl3041();
        registerRecord(ldaTwrl3041);

        // Local Variables
        localVariables = new DbsRecord();

        vw_payr = new DataAccessProgramView(new NameInfo("vw_payr", "PAYR"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT", DdmPeriodicGroups.getInstance().getGroups("TWRPYMNT_PAYMENT_FILE"));
        payr_Twrpymnt_Tax_Year = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_YEAR");
        payr_Twrpymnt_Tax_Id_Type = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Tax_Id_Type", "TWRPYMNT-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_ID_TYPE");
        payr_Twrpymnt_Tax_Id_Type.setDdmHeader("TAX IDEN-/TIFICATION/NUMBER");
        payr_Twrpymnt_Tax_Id_Nbr = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_ID_NBR");
        payr_Twrpymnt_Company_Cde = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPYMNT_COMPANY_CDE");
        payr_Twrpymnt_Tax_Citizenship = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Tax_Citizenship", "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_TAX_CITIZENSHIP");
        payr_Twrpymnt_Contract_Nbr = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPYMNT_CONTRACT_NBR");
        payr_Twrpymnt_Payee_Cde = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPYMNT_PAYEE_CDE");
        payr_Twrpymnt_Distribution_Cde = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_DISTRIBUTION_CDE");
        payr_Twrpymnt_Residency_Type = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_TYPE");
        payr_Twrpymnt_Residency_Code = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_CODE");
        payr_Twrpymnt_Ivc_Indicator = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Ivc_Indicator", "TWRPYMNT-IVC-INDICATOR", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPYMNT_IVC_INDICATOR");
        payr_Twrpymnt_Paymt_Category = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_PAYMT_CATEGORY");
        payr_Twrpymnt_Locality_Cde = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Locality_Cde", "TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRPYMNT_LOCALITY_CDE");
        payr_Count_Casttwrpymnt_Payments = vw_payr.getRecord().newFieldInGroup("payr_Count_Casttwrpymnt_Payments", "C*TWRPYMNT-PAYMENTS", RepeatingFieldStrategy.CAsteriskVariable, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");

        payr_Twrpymnt_Payments = vw_payr.getRecord().newGroupInGroup("payr_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Pymnt_Dte = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Pymnt_Dte", "TWRPYMNT-PYMNT-DTE", FieldType.STRING, 8, new 
            DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Pymnt_Status = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Pymnt_Status", "TWRPYMNT-PYMNT-STATUS", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_STATUS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Pymnt_Intfce_Dte = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Pymnt_Intfce_Dte", "TWRPYMNT-PYMNT-INTFCE-DTE", FieldType.STRING, 
            8, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_INTFCE_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Updte_Srce_Cde = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Updte_Srce_Cde", "TWRPYMNT-UPDTE-SRCE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_UPDTE_SRCE_CDE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Orgn_Srce_Cde = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Orgn_Srce_Cde", "TWRPYMNT-ORGN-SRCE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ORGN_SRCE_CDE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Payment_Type = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Payment_Type", "TWRPYMNT-PAYMENT-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PAYMENT_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Settle_Type = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Settle_Type", "TWRPYMNT-SETTLE-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_SETTLE_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Gross_Amt = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Gross_Amt", "TWRPYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GROSS_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Ivc_Amt = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Ivc_Amt", "TWRPYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Fed_Wthld_Amt = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Fed_Wthld_Amt", "TWRPYMNT-FED-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_FED_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_State_Wthld = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_State_Wthld", "TWRPYMNT-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_STATE_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Local_Wthld = payr_Twrpymnt_Payments.newFieldArrayInGroup("payr_Twrpymnt_Local_Wthld", "TWRPYMNT-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 24) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_LOCAL_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        payr_Twrpymnt_Contract_Seq_No = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Contract_Seq_No", "TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_SEQ_NO");
        payr_Twrpymnt_Form_Sd = vw_payr.getRecord().newFieldInGroup("payr_Twrpymnt_Form_Sd", "TWRPYMNT-FORM-SD", FieldType.STRING, 37, RepeatingFieldStrategy.None, 
            "TWRPYMNT_FORM_SD");
        payr_Twrpymnt_Form_Sd.setSuperDescriptor(true);
        registerRecord(vw_payr);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Pay_Super_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pay_Super_Start", "#PAY-SUPER-START", FieldType.STRING, 5);
        pnd_Ws_Pnd_Pay_Super_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pay_Super_End", "#PAY-SUPER-END", FieldType.STRING, 5);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Ritten = localVariables.newFieldInRecord("pnd_Rec_Ritten", "#REC-RITTEN", FieldType.PACKED_DECIMAL, 9);
        pnd_Cref_Cnt = localVariables.newFieldInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Life_Cnt = localVariables.newFieldInRecord("pnd_Life_Cnt", "#LIFE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Serv_Cnt = localVariables.newFieldInRecord("pnd_Serv_Cnt", "#SERV-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tiaa_Cnt = localVariables.newFieldInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Fsb_Cnt = localVariables.newFieldInRecord("pnd_Fsb_Cnt", "#FSB-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Trst_Cnt = localVariables.newFieldInRecord("pnd_Trst_Cnt", "#TRST-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Othr_Cnt = localVariables.newFieldInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Intf_Date_To = localVariables.newFieldInRecord("pnd_Intf_Date_To", "#INTF-DATE-TO", FieldType.STRING, 8);

        pnd_Intf_Date_To__R_Field_1 = localVariables.newGroupInRecord("pnd_Intf_Date_To__R_Field_1", "REDEFINE", pnd_Intf_Date_To);
        pnd_Intf_Date_To_Pnd_Intf_Date_To_N = pnd_Intf_Date_To__R_Field_1.newFieldInGroup("pnd_Intf_Date_To_Pnd_Intf_Date_To_N", "#INTF-DATE-TO-N", FieldType.NUMERIC, 
            8);
        pnd_Intf_Date_From = localVariables.newFieldInRecord("pnd_Intf_Date_From", "#INTF-DATE-FROM", FieldType.STRING, 8);

        pnd_Intf_Date_From__R_Field_2 = localVariables.newGroupInRecord("pnd_Intf_Date_From__R_Field_2", "REDEFINE", pnd_Intf_Date_From);
        pnd_Intf_Date_From_Pnd_Intf_Date_From_Yy = pnd_Intf_Date_From__R_Field_2.newFieldInGroup("pnd_Intf_Date_From_Pnd_Intf_Date_From_Yy", "#INTF-DATE-FROM-YY", 
            FieldType.NUMERIC, 4);
        pnd_Intf_Date_From_Pnd_Intf_Date_From_Mm = pnd_Intf_Date_From__R_Field_2.newFieldInGroup("pnd_Intf_Date_From_Pnd_Intf_Date_From_Mm", "#INTF-DATE-FROM-MM", 
            FieldType.NUMERIC, 2);
        pnd_Intf_Date_From_Pnd_Intf_Date_From_Dd = pnd_Intf_Date_From__R_Field_2.newFieldInGroup("pnd_Intf_Date_From_Pnd_Intf_Date_From_Dd", "#INTF-DATE-FROM-DD", 
            FieldType.NUMERIC, 2);
        pnd_Pymnt_Date_From = localVariables.newFieldInRecord("pnd_Pymnt_Date_From", "#PYMNT-DATE-FROM", FieldType.STRING, 8);

        pnd_Pymnt_Date_From__R_Field_3 = localVariables.newGroupInRecord("pnd_Pymnt_Date_From__R_Field_3", "REDEFINE", pnd_Pymnt_Date_From);
        pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Yy = pnd_Pymnt_Date_From__R_Field_3.newFieldInGroup("pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Yy", "#PYMNT-DATE-FROM-YY", 
            FieldType.NUMERIC, 4);
        pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Mm = pnd_Pymnt_Date_From__R_Field_3.newFieldInGroup("pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Mm", "#PYMNT-DATE-FROM-MM", 
            FieldType.NUMERIC, 2);
        pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Dd = pnd_Pymnt_Date_From__R_Field_3.newFieldInGroup("pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Dd", "#PYMNT-DATE-FROM-DD", 
            FieldType.NUMERIC, 2);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Input_Parm__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_4", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Pymnt_Date_To = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To", "#PYMNT-DATE-TO", FieldType.STRING, 
            8);

        pnd_Input_Parm__R_Field_5 = pnd_Input_Parm__R_Field_4.newGroupInGroup("pnd_Input_Parm__R_Field_5", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy", "#PYMNT-DATE-TO-YYYY", 
            FieldType.NUMERIC, 4);

        pnd_Input_Parm__R_Field_6 = pnd_Input_Parm__R_Field_5.newGroupInGroup("pnd_Input_Parm__R_Field_6", "REDEFINE", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);
        pnd_Input_Parm_Pnd_Parm_Tax_Year = pnd_Input_Parm__R_Field_6.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Tax_Year", "#PARM-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm", "#PYMNT-DATE-TO-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd = pnd_Input_Parm__R_Field_5.newFieldInGroup("pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd", "#PYMNT-DATE-TO-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_Parm_Pnd_Filler2 = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Year_End_Adj_Ind = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Year_End_Adj_Ind", "#YEAR-END-ADJ-IND", FieldType.STRING, 
            1);
        pnd_Run_Type = localVariables.newFieldInRecord("pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 24);
        pnd_C_Int_Count = localVariables.newFieldInRecord("pnd_C_Int_Count", "#C-INT-COUNT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rD_PAYTwrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 4);
        rD_PAYTwrpymnt_Tax_Id_TypeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Tax_Id_Type_OLD", "Twrpymnt_Tax_Id_Type_OLD", FieldType.STRING, 
            1);
        rD_PAYTwrpymnt_Tax_Id_NbrOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Tax_Id_Nbr_OLD", "Twrpymnt_Tax_Id_Nbr_OLD", FieldType.STRING, 
            10);
        rD_PAYTwrpymnt_Company_CdeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Company_Cde_OLD", "Twrpymnt_Company_Cde_OLD", FieldType.STRING, 
            1);
        rD_PAYTwrpymnt_Tax_CitizenshipOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Tax_Citizenship_OLD", "Twrpymnt_Tax_Citizenship_OLD", 
            FieldType.STRING, 1);
        rD_PAYTwrpymnt_Contract_NbrOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Contract_Nbr_OLD", "Twrpymnt_Contract_Nbr_OLD", FieldType.STRING, 
            8);
        rD_PAYTwrpymnt_Payee_CdeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Payee_Cde_OLD", "Twrpymnt_Payee_Cde_OLD", FieldType.STRING, 
            2);
        rD_PAYTwrpymnt_Distribution_CdeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Distribution_Cde_OLD", "Twrpymnt_Distribution_Cde_OLD", 
            FieldType.STRING, 2);
        rD_PAYTwrpymnt_Residency_TypeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Residency_Type_OLD", "Twrpymnt_Residency_Type_OLD", FieldType.STRING, 
            1);
        rD_PAYTwrpymnt_Residency_CodeOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Residency_Code_OLD", "Twrpymnt_Residency_Code_OLD", FieldType.STRING, 
            2);
        rD_PAYTwrpymnt_Ivc_IndicatorOld = internalLoopRecord.newFieldInRecord("RD_PAY_Twrpymnt_Ivc_Indicator_OLD", "Twrpymnt_Ivc_Indicator_OLD", FieldType.STRING, 
            1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payr.reset();
        internalLoopRecord.reset();

        ldaTwrl3041.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3001() throws Exception
    {
        super("Twrp3001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp3001|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*    (VALIDATE PARAMETER CARD)
                if (condition((pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y") && pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12) && pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd.equals(31))  //Natural: IF ( #YEAR-END-ADJ-IND = 'Y' AND #PYMNT-DATE-TO-MM = 12 AND #PYMNT-DATE-TO-DD = 31 ) OR ( #YEAR-END-ADJ-IND = 'N' AND ( ( #PYMNT-DATE-TO-MM = 03 AND #PYMNT-DATE-TO-DD = 31 ) OR ( #PYMNT-DATE-TO-MM = 06 AND #PYMNT-DATE-TO-DD = 30 ) OR ( #PYMNT-DATE-TO-MM = 09 AND #PYMNT-DATE-TO-DD = 30 ) OR ( #PYMNT-DATE-TO-MM = 12 AND #PYMNT-DATE-TO-DD = 31 ) ) )
                    || (pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("N") && ((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3) && pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd.equals(31)) 
                    || (pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6) && pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd.equals(30)) || (pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9) 
                    && pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd.equals(30)) || (pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12) && pnd_Input_Parm_Pnd_Pymnt_Date_To_Dd.equals(31))))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!",NEWLINE,"ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES",     //Natural: WRITE '!!!!! ERROR FOUND, PROGRAM IS TERMINATED !!!!!' / 'ERROR: #INPUT-PARM CONTAINS INVALID COMBINATION OF INPUT VALUES' / 'PLEASE CONTACT SYSTEM SUPPORT' /// '********** VALID COMBINATION OF INPUT VALUES ARE: **********' / ' #PYMNT-DTE-TO (YYYYMMDD) #YEAR-END-ADJ (Y/N)' / '                YYYY0331          N    - 1ST QTR' / '                YYYY0630          N    - 2ND QTR' / '                YYYY0930          N    - 3RD QTR' / '                YYYY1231          N    - 4TH QTR' / '                YYYY1231          Y    - Y-E ADJ'
                        NEWLINE,"PLEASE CONTACT SYSTEM SUPPORT",NEWLINE,NEWLINE,NEWLINE,"********** VALID COMBINATION OF INPUT VALUES ARE: **********",NEWLINE,
                        " #PYMNT-DTE-TO (YYYYMMDD) #YEAR-END-ADJ (Y/N)",NEWLINE,"                YYYY0331          N    - 1ST QTR",NEWLINE,"                YYYY0630          N    - 2ND QTR",
                        NEWLINE,"                YYYY0930          N    - 3RD QTR",NEWLINE,"                YYYY1231          N    - 4TH QTR",NEWLINE,"                YYYY1231          Y    - Y-E ADJ");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DEFINE-RUN-TYPE
                sub_Define_Run_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*   (DEFINE INTERFACE DATE FROM PAYMENT-DATE-TO)
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFINE-RUN-TYPE
                pnd_Intf_Date_From_Pnd_Intf_Date_From_Yy.compute(new ComputeParameters(false, pnd_Intf_Date_From_Pnd_Intf_Date_From_Yy), pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy.subtract(1)); //Natural: ASSIGN #INTF-DATE-FROM-YY := #PYMNT-DATE-TO-YYYY - 1
                pnd_Intf_Date_From_Pnd_Intf_Date_From_Mm.setValue(12);                                                                                                    //Natural: ASSIGN #INTF-DATE-FROM-MM := 12
                pnd_Intf_Date_From_Pnd_Intf_Date_From_Dd.setValue(1);                                                                                                     //Natural: ASSIGN #INTF-DATE-FROM-DD := 01
                //*   (DEFINE PAYMENT-DATE-FROM FROM PAYMENT-DATE-TO)
                pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Yy.setValue(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy);                                                               //Natural: ASSIGN #PYMNT-DATE-FROM-YY := #PYMNT-DATE-TO-YYYY
                pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Mm.setValue(1);                                                                                                   //Natural: ASSIGN #PYMNT-DATE-FROM-MM := 01
                pnd_Pymnt_Date_From_Pnd_Pymnt_Date_From_Dd.setValue(1);                                                                                                   //Natural: ASSIGN #PYMNT-DATE-FROM-DD := 01
                pnd_Intf_Date_To_Pnd_Intf_Date_To_N.setValue(Global.getDATN());                                                                                           //Natural: ASSIGN #INTF-DATE-TO-N := *DATN
                pnd_Ws_Pnd_Pay_Super_Start.setValue(pnd_Input_Parm_Pnd_Parm_Tax_Year);                                                                                    //Natural: ASSIGN #WS.#PAY-SUPER-START := #WS.#PAY-SUPER-END := #PARM-TAX-YEAR
                pnd_Ws_Pnd_Pay_Super_End.setValue(pnd_Input_Parm_Pnd_Parm_Tax_Year);
                setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Pay_Super_Start,5,1);                                                                              //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#PAY-SUPER-START,5,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Pay_Super_End,5,1);                                                                               //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#PAY-SUPER-END ,5,1 )
                vw_payr.startDatabaseRead                                                                                                                                 //Natural: READ PAYR BY TWRPYMNT-FORM-SD = #WS.#PAY-SUPER-START THRU #WS.#PAY-SUPER-END
                (
                "RD_PAY",
                new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Ws_Pnd_Pay_Super_Start, "And", WcType.BY) ,
                new Wc("TWRPYMNT_FORM_SD", "<=", pnd_Ws_Pnd_Pay_Super_End, WcType.BY) },
                new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
                );
                boolean endOfDataRdPay = true;
                boolean firstRdPay = true;
                RD_PAY:
                while (condition(vw_payr.readNextRow("RD_PAY")))
                {
                    if (condition(vw_payr.getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd_Pay();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRdPay = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //BEFORE BREAK PROCESSING                                                                                                                             //Natural: BEFORE BREAK PROCESSING
                    pnd_Rec_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-READ
                    if (condition(!(payr_Twrpymnt_Residency_Code.equals("35"))))                                                                                          //Natural: ACCEPT IF PAYR.TWRPYMNT-RESIDENCY-CODE = '35'
                    {
                        continue;
                    }
                    //END-BEFORE                                                                                                                                          //Natural: END-BEFORE
                    //*                                                                                                                                                   //Natural: AT BREAK OF TWRPYMNT-FORM-SD
                    pnd_C_Int_Count.setValue(payr_Count_Casttwrpymnt_Payments);                                                                                           //Natural: ASSIGN #C-INT-COUNT := C*TWRPYMNT-PAYMENTS
                    if (condition(pnd_C_Int_Count.greater(24)))                                                                                                           //Natural: IF #C-INT-COUNT > 24
                    {
                        pnd_C_Int_Count.setValue(24);                                                                                                                     //Natural: ASSIGN #C-INT-COUNT := 24
                        getReports().display(0, "TAX-ID-NBR",                                                                                                             //Natural: DISPLAY ( 0 ) 'TAX-ID-NBR' PAYR.TWRPYMNT-TAX-ID-NBR 'CONTRACT-NBR' PAYR.TWRPYMNT-CONTRACT-NBR 'PAYEE-CDE' PAYR.TWRPYMNT-PAYEE-CDE 'C*' PAYR.C*TWRPYMNT-PAYMENTS
                        		payr_Twrpymnt_Tax_Id_Nbr,"CONTRACT-NBR",
                        		payr_Twrpymnt_Contract_Nbr,"PAYEE-CDE",
                        		payr_Twrpymnt_Payee_Cde,"C*",
                        		payr_Count_Casttwrpymnt_Payments);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD_PAY"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD_PAY"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    FOR01:                                                                                                                                                //Natural: FOR #J = 1 TO #C-INT-COUNT
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_C_Int_Count)); pnd_J.nadd(1))
                    {
                        if (condition((((payr_Twrpymnt_State_Wthld.getValue(pnd_J).notEquals(getZero()) || payr_Twrpymnt_Local_Wthld.getValue(pnd_J).notEquals(getZero()))  //Natural: IF ( PAYR.TWRPYMNT-STATE-WTHLD ( #J ) NE 0 OR PAYR.TWRPYMNT-LOCAL-WTHLD ( #J ) NE 0 ) AND PAYR.TWRPYMNT-PYMNT-INTFCE-DTE ( #J ) = #INTF-DATE-FROM THRU #INTF-DATE-TO AND PAYR.TWRPYMNT-PYMNT-DTE ( #J ) = #PYMNT-DATE-FROM THRU #PYMNT-DATE-TO
                            && (payr_Twrpymnt_Pymnt_Intfce_Dte.getValue(pnd_J).greaterOrEqual(pnd_Intf_Date_From) && payr_Twrpymnt_Pymnt_Intfce_Dte.getValue(pnd_J).lessOrEqual(pnd_Intf_Date_To))) 
                            && (payr_Twrpymnt_Pymnt_Dte.getValue(pnd_J).greaterOrEqual(pnd_Pymnt_Date_From) && payr_Twrpymnt_Pymnt_Dte.getValue(pnd_J).lessOrEqual(pnd_Input_Parm_Pnd_Pymnt_Date_To)))))
                        {
                            pnd_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #K
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte().getValue(pnd_K).setValue(payr_Twrpymnt_Pymnt_Dte.getValue(pnd_J));                      //Natural: ASSIGN #TWRPYMNT-PYMNT-DTE ( #K ) := TWRPYMNT-PYMNT-DTE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte().getValue(pnd_K).setValue(payr_Twrpymnt_Pymnt_Dte.getValue(pnd_J));                      //Natural: ASSIGN #TWRPYMNT-PYMNT-DTE ( #K ) := TWRPYMNT-PYMNT-DTE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte().getValue(pnd_K).setValue(payr_Twrpymnt_Pymnt_Intfce_Dte.getValue(pnd_J));        //Natural: ASSIGN #TWRPYMNT-PYMNT-INTFCE-DTE ( #K ) := TWRPYMNT-PYMNT-INTFCE-DTE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde().getValue(pnd_K).setValue(payr_Twrpymnt_Updte_Srce_Cde.getValue(pnd_J));            //Natural: ASSIGN #TWRPYMNT-UPDTE-SRCE-CDE ( #K ) := TWRPYMNT-UPDTE-SRCE-CDE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_K).setValue(payr_Twrpymnt_Orgn_Srce_Cde.getValue(pnd_J));              //Natural: ASSIGN #TWRPYMNT-ORGN-SRCE-CDE ( #K ) := TWRPYMNT-ORGN-SRCE-CDE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type().getValue(pnd_K).setValue(payr_Twrpymnt_Payment_Type.getValue(pnd_J));                //Natural: ASSIGN #TWRPYMNT-PAYMENT-TYPE ( #K ) := TWRPYMNT-PAYMENT-TYPE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type().getValue(pnd_K).setValue(payr_Twrpymnt_Settle_Type.getValue(pnd_J));                  //Natural: ASSIGN #TWRPYMNT-SETTLE-TYPE ( #K ) := TWRPYMNT-SETTLE-TYPE ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt().getValue(pnd_K).setValue(payr_Twrpymnt_Gross_Amt.getValue(pnd_J));                      //Natural: ASSIGN #TWRPYMNT-GROSS-AMT ( #K ) := TWRPYMNT-GROSS-AMT ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt().getValue(pnd_K).setValue(payr_Twrpymnt_Ivc_Amt.getValue(pnd_J));                          //Natural: ASSIGN #TWRPYMNT-IVC-AMT ( #K ) := TWRPYMNT-IVC-AMT ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_K).setValue(payr_Twrpymnt_Fed_Wthld_Amt.getValue(pnd_J));              //Natural: ASSIGN #TWRPYMNT-FED-WTHLD-AMT ( #K ) := TWRPYMNT-FED-WTHLD-AMT ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(pnd_K).setValue(payr_Twrpymnt_State_Wthld.getValue(pnd_J));                  //Natural: ASSIGN #TWRPYMNT-STATE-WTHLD ( #K ) := TWRPYMNT-STATE-WTHLD ( #J )
                            ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld().getValue(pnd_K).setValue(payr_Twrpymnt_Local_Wthld.getValue(pnd_J));                  //Natural: ASSIGN #TWRPYMNT-LOCAL-WTHLD ( #K ) := TWRPYMNT-LOCAL-WTHLD ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_PAY"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_PAY"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    rD_PAYTwrpymnt_Tax_YearOld.setValue(payr_Twrpymnt_Tax_Year);                                                                                          //Natural: AT END OF DATA;//Natural: END-READ
                    rD_PAYTwrpymnt_Tax_Id_TypeOld.setValue(payr_Twrpymnt_Tax_Id_Type);
                    rD_PAYTwrpymnt_Tax_Id_NbrOld.setValue(payr_Twrpymnt_Tax_Id_Nbr);
                    rD_PAYTwrpymnt_Company_CdeOld.setValue(payr_Twrpymnt_Company_Cde);
                    rD_PAYTwrpymnt_Tax_CitizenshipOld.setValue(payr_Twrpymnt_Tax_Citizenship);
                    rD_PAYTwrpymnt_Contract_NbrOld.setValue(payr_Twrpymnt_Contract_Nbr);
                    rD_PAYTwrpymnt_Payee_CdeOld.setValue(payr_Twrpymnt_Payee_Cde);
                    rD_PAYTwrpymnt_Distribution_CdeOld.setValue(payr_Twrpymnt_Distribution_Cde);
                    rD_PAYTwrpymnt_Residency_TypeOld.setValue(payr_Twrpymnt_Residency_Type);
                    rD_PAYTwrpymnt_Residency_CodeOld.setValue(payr_Twrpymnt_Residency_Code);
                    rD_PAYTwrpymnt_Ivc_IndicatorOld.setValue(payr_Twrpymnt_Ivc_Indicator);
                }
                if (condition(vw_payr.getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd_Pay(endOfDataRdPay);
                }
                if (condition(vw_payr.getAtEndOfData()))
                {
                    //*  JB1011
                    pnd_Tiaa_Cnt.compute(new ComputeParameters(false, pnd_Tiaa_Cnt), pnd_Tiaa_Cnt.add(pnd_Cref_Cnt).add(pnd_Life_Cnt).add(pnd_Serv_Cnt).add(pnd_Fsb_Cnt)); //Natural: COMPUTE #TIAA-CNT = #TIAA-CNT + #CREF-CNT + #LIFE-CNT + #SERV-CNT + #FSB-CNT
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(34),"NEW YORK QUARTERLY TAX REPORTING",NEWLINE,"RUNDATE : ",Global.getDATX(),  //Natural: WRITE ( 01 ) NOTITLE NOHDR / *PROGRAM 34T 'NEW YORK QUARTERLY TAX REPORTING' / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 39T 'EXTRACTED PAYMENT RECORDS' / 'RUNTIME : ' *TIMX 44T 'CONTROL TOTALS' // 'TOTAL RECORDS READ         : ' #REC-READ /// 'TOTAL RECORDS WRITTEN      : ' #REC-RITTEN / '  ** TIAA  RECORDS         : ' #TIAA-CNT / '  ** TRUST RECORDS         : ' #TRST-CNT / '  ** OTHER RECORDS         : ' #OTHR-CNT /// '******* PARAMETER USED     : ' #INPUT-PARM // '(INTF-DATE & PYMNT-DATE USED ON THIS QTR RUN)' // 'A) INTERFACE-DATE-FROM     : ' #INTF-DATE-FROM ( EM = XXXX/XX/XX ) / 'B) INTERFACE-DATE-TO       : ' #INTF-DATE-TO ( EM = XXXX/XX/XX ) / 'C) PAYMENT-DATE-FROM       : ' #PYMNT-DATE-FROM ( EM = XXXX/XX/XX ) / 'D) PAYMENT-DATE-TO         : ' #PYMNT-DATE-TO ( EM = XXXX/XX/XX )
                        new ReportEditMask ("MM/DD/YYYY"),new TabSetting(39),"EXTRACTED PAYMENT RECORDS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(44),"CONTROL TOTALS",NEWLINE,NEWLINE,"TOTAL RECORDS READ         : ",pnd_Rec_Read, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS WRITTEN      : ",pnd_Rec_Ritten, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"  ** TIAA  RECORDS         : ",pnd_Tiaa_Cnt, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"  ** TRUST RECORDS         : ",pnd_Trst_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"  ** OTHER RECORDS         : ",pnd_Othr_Cnt, 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,"******* PARAMETER USED     : ",pnd_Input_Parm,NEWLINE,NEWLINE,"(INTF-DATE & PYMNT-DATE USED ON THIS QTR RUN)",NEWLINE,NEWLINE,"A) INTERFACE-DATE-FROM     : ",pnd_Intf_Date_From, 
                        new ReportEditMask ("XXXX/XX/XX"),NEWLINE,"B) INTERFACE-DATE-TO       : ",pnd_Intf_Date_To, new ReportEditMask ("XXXX/XX/XX"),NEWLINE,"C) PAYMENT-DATE-FROM       : ",pnd_Pymnt_Date_From, 
                        new ReportEditMask ("XXXX/XX/XX"),NEWLINE,"D) PAYMENT-DATE-TO         : ",pnd_Input_Parm_Pnd_Pymnt_Date_To, new ReportEditMask ("XXXX/XX/XX"));
                    if (condition(Global.isEscape())) return;
                    //*    /         '  1. CREF  RECORDS         : ' #CREF-CNT
                    //*    /         '  2. LIFE  RECORDS         : ' #LIFE-CNT
                    //*    /         '  ** TCII  RECORDS         : ' #SERV-CNT
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPANY-CNT
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TEST
                //* *------------
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Define_Run_Type() throws Exception                                                                                                                   //Natural: DEFINE-RUN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------------
        //*         (DEFINE RUN-TYPE TO BE PRINTED IN THE HEADER)
        if (condition(pnd_Input_Parm_Pnd_Year_End_Adj_Ind.equals("Y")))                                                                                                   //Natural: IF #YEAR-END-ADJ-IND = 'Y'
        {
            pnd_Run_Type.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy, " YEAD END ADJUSTMENT"));                                                       //Natural: COMPRESS #PYMNT-DATE-TO-YYYY ' YEAD END ADJUSTMENT' TO #RUN-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet278 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PYMNT-DATE-TO-MM;//Natural: VALUE 03
            if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(3))))
            {
                decideConditionsMet278++;
                pnd_Run_Type.setValue(DbsUtil.compress("1ST QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '1ST QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(6))))
            {
                decideConditionsMet278++;
                pnd_Run_Type.setValue(DbsUtil.compress("2ND QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '2ND QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(9))))
            {
                decideConditionsMet278++;
                pnd_Run_Type.setValue(DbsUtil.compress("3RD QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '3RD QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Input_Parm_Pnd_Pymnt_Date_To_Mm.equals(12))))
            {
                decideConditionsMet278++;
                pnd_Run_Type.setValue(DbsUtil.compress("4TH QUARTER OF ", pnd_Input_Parm_Pnd_Pymnt_Date_To_Yyyy));                                                        //Natural: COMPRESS '4TH QUARTER OF ' #PYMNT-DATE-TO-YYYY TO #RUN-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        //*  JB1011
        short decideConditionsMet296 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF PAYR.TWRPYMNT-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((payr_Twrpymnt_Company_Cde.equals("C"))))
        {
            decideConditionsMet296++;
            pnd_Cref_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CREF-CNT
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((payr_Twrpymnt_Company_Cde.equals("L"))))
        {
            decideConditionsMet296++;
            pnd_Life_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #LIFE-CNT
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((payr_Twrpymnt_Company_Cde.equals("S"))))
        {
            decideConditionsMet296++;
            pnd_Serv_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #SERV-CNT
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((payr_Twrpymnt_Company_Cde.equals("T"))))
        {
            decideConditionsMet296++;
            pnd_Tiaa_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TIAA-CNT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((payr_Twrpymnt_Company_Cde.equals("X"))))
        {
            decideConditionsMet296++;
            pnd_Trst_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TRST-CNT
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((payr_Twrpymnt_Company_Cde.equals("F"))))
        {
            decideConditionsMet296++;
            pnd_Fsb_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #FSB-CNT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #OTHR-CNT
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Test() throws Exception                                                                                                                        //Natural: PRINT-TEST
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------
        getReports().display(1, "TAXYR",                                                                                                                                  //Natural: DISPLAY ( 01 ) 'TAXYR' #EXT-CALIF.TWRPYMNT-TAX-YEAR 'TX-TY' #EXT-CALIF.TWRPYMNT-TAX-ID-TYPE 'TIN' #EXT-CALIF.TWRPYMNT-TAX-ID-NBR 'CO' #EXT-CALIF.TWRPYMNT-COMPANY-CDE 'CIT' #EXT-CALIF.TWRPYMNT-TAX-CITIZENSHIP 'CNTRCT#' #EXT-CALIF.TWRPYMNT-CONTRACT-NBR 'PAYEE' #EXT-CALIF.TWRPYMNT-PAYEE-CDE 'RES' #EXT-CALIF.TWRPYMNT-RESIDENCY-CODE 'C*' #EXT-CALIF.#C-TWRPYMNT-PAYMENTS 'PAYDATE' #EXT-CALIF.#TWRPYMNT-PYMNT-DTE ( 1:#K ) 'INTFDATE' #EXT-CALIF.#TWRPYMNT-PYMNT-INTFCE-DTE ( 1:#K ) 'UPSC' #EXT-CALIF.#TWRPYMNT-UPDTE-SRCE-CDE ( 1:#K ) 'ORGSC' #EXT-CALIF.#TWRPYMNT-ORGN-SRCE-CDE ( 1:#K ) 'PT' #EXT-CALIF.#TWRPYMNT-PAYMENT-TYPE ( 1:#K ) 'ST' #EXT-CALIF.#TWRPYMNT-SETTLE-TYPE ( 1:#K ) 'GROSS' #EXT-CALIF.#TWRPYMNT-GROSS-AMT ( 1:#K ) 'STATE-AMT' #EXT-CALIF.#TWRPYMNT-STATE-WTHLD ( 1:#K ) 'LOCAL-AMT' #EXT-CALIF.#TWRPYMNT-LOCAL-WTHLD ( 1:#K )
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Year(),"TX-TY",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type(),"TIN",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr(),"CO",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde(),"CIT",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Citizenship(),"CNTRCT#",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Contract_Nbr(),"PAYEE",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Payee_Cde(),"RES",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Code(),"C*",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_C_Twrpymnt_Payments(),"PAYDATE",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte().getValue(1,":",pnd_K),"INTFDATE",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte().getValue(1,":",pnd_K),"UPSC",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde().getValue(1,":",pnd_K),"ORGSC",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde().getValue(1,":",pnd_K),"PT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type().getValue(1,":",pnd_K),"ST",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type().getValue(1,":",pnd_K),"GROSS",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt().getValue(1,":",pnd_K),"STATE-AMT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld().getValue(1,":",pnd_K),"LOCAL-AMT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld().getValue(1,":",pnd_K));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRd_Pay() throws Exception {atBreakEventRd_Pay(false);}
    private void atBreakEventRd_Pay(boolean endOfData) throws Exception
    {
        boolean payr_Twrpymnt_Form_SdIsBreak = payr_Twrpymnt_Form_Sd.isBreak(endOfData);
        if (condition(payr_Twrpymnt_Form_SdIsBreak))
        {
            if (condition(pnd_K.greater(getZero())))                                                                                                                      //Natural: IF #K > 0
            {
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Year().setValue(rD_PAYTwrpymnt_Tax_YearOld);                                                                    //Natural: MOVE OLD ( PAYR.TWRPYMNT-TAX-YEAR ) TO #EXT-CALIF.TWRPYMNT-TAX-YEAR
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type().setValue(rD_PAYTwrpymnt_Tax_Id_TypeOld);                                                              //Natural: MOVE OLD ( PAYR.TWRPYMNT-TAX-ID-TYPE ) TO #EXT-CALIF.TWRPYMNT-TAX-ID-TYPE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr().setValue(rD_PAYTwrpymnt_Tax_Id_NbrOld);                                                                //Natural: MOVE OLD ( PAYR.TWRPYMNT-TAX-ID-NBR ) TO #EXT-CALIF.TWRPYMNT-TAX-ID-NBR
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde().setValue(rD_PAYTwrpymnt_Company_CdeOld);                                                              //Natural: MOVE OLD ( PAYR.TWRPYMNT-COMPANY-CDE ) TO #EXT-CALIF.TWRPYMNT-COMPANY-CDE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Citizenship().setValue(rD_PAYTwrpymnt_Tax_CitizenshipOld);                                                      //Natural: MOVE OLD ( PAYR.TWRPYMNT-TAX-CITIZENSHIP ) TO #EXT-CALIF.TWRPYMNT-TAX-CITIZENSHIP
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Contract_Nbr().setValue(rD_PAYTwrpymnt_Contract_NbrOld);                                                            //Natural: MOVE OLD ( PAYR.TWRPYMNT-CONTRACT-NBR ) TO #EXT-CALIF.TWRPYMNT-CONTRACT-NBR
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Payee_Cde().setValue(rD_PAYTwrpymnt_Payee_CdeOld);                                                                  //Natural: MOVE OLD ( PAYR.TWRPYMNT-PAYEE-CDE ) TO #EXT-CALIF.TWRPYMNT-PAYEE-CDE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Distribution_Cde().setValue(rD_PAYTwrpymnt_Distribution_CdeOld);                                                    //Natural: MOVE OLD ( PAYR.TWRPYMNT-DISTRIBUTION-CDE ) TO #EXT-CALIF.TWRPYMNT-DISTRIBUTION-CDE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Type().setValue(rD_PAYTwrpymnt_Residency_TypeOld);                                                        //Natural: MOVE OLD ( PAYR.TWRPYMNT-RESIDENCY-TYPE ) TO #EXT-CALIF.TWRPYMNT-RESIDENCY-TYPE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Code().setValue(rD_PAYTwrpymnt_Residency_CodeOld);                                                        //Natural: MOVE OLD ( PAYR.TWRPYMNT-RESIDENCY-CODE ) TO #EXT-CALIF.TWRPYMNT-RESIDENCY-CODE
                ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Ivc_Indicator().setValue(rD_PAYTwrpymnt_Ivc_IndicatorOld);                                                          //Natural: MOVE OLD ( PAYR.TWRPYMNT-IVC-INDICATOR ) TO #EXT-CALIF.TWRPYMNT-IVC-INDICATOR
                ldaTwrl3041.getPnd_Ext_Calif_Pnd_C_Twrpymnt_Payments().setValue(pnd_K);                                                                                   //Natural: ASSIGN #C-TWRPYMNT-PAYMENTS := #K
                getWorkFiles().write(1, false, ldaTwrl3041.getPnd_Ext_Calif());                                                                                           //Natural: WRITE WORK FILE 01 #EXT-CALIF
                //*      PERFORM  PRINT-TEST                        /* TEST ONLY
                pnd_Rec_Ritten.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-RITTEN
                //*  NO MORE DIFFERENT COMPANY COUNT, ALL IN TCII NOW    03/01/04  RM
                //*  NO MORE DIFFERENT COMPANY COUNT, ALL IN TIAA NOW    03/31/04  RM
                //*  REVERT BACK TO DO COMPANY COUNT, THEN WILL GROUP UNDER
                //*  EITHER TIAA OR TRST AT THE END.                     11/04/04  RM
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
                sub_Company_Cnt();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*          ADD 1 TO #SERV-CNT
                //*          ADD 1 TO  #TIAA-CNT
            }                                                                                                                                                             //Natural: END-IF
            pnd_K.reset();                                                                                                                                                //Natural: RESET #K #EXT-CALIF
            ldaTwrl3041.getPnd_Ext_Calif().reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, "TAX-ID-NBR",
        		payr_Twrpymnt_Tax_Id_Nbr,"CONTRACT-NBR",
        		payr_Twrpymnt_Contract_Nbr,"PAYEE-CDE",
        		payr_Twrpymnt_Payee_Cde,"C*",
        		payr_Count_Casttwrpymnt_Payments);
        getReports().setDisplayColumns(1, "TAXYR",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Year(),"TX-TY",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type(),"TIN",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr(),"CO",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Company_Cde(),"CIT",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Tax_Citizenship(),"CNTRCT#",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Contract_Nbr(),"PAYEE",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Payee_Cde(),"RES",
        		ldaTwrl3041.getPnd_Ext_Calif_Twrpymnt_Residency_Code(),"C*",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_C_Twrpymnt_Payments(),"PAYDATE",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte(),"INTFDATE",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte(),"UPSC",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde(),"ORGSC",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde(),"PT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type(),"ST",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type(),"GROSS",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt(),"STATE-AMT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld(),"LOCAL-AMT",
        		ldaTwrl3041.getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld());
    }
}
