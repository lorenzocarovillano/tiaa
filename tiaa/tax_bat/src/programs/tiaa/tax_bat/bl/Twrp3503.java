/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:18 PM
**        * FROM NATURAL PROGRAM : Twrp3503
************************************************************
**        * FILE NAME            : Twrp3503.java
**        * CLASS NAME           : Twrp3503
**        * INSTANCE NAME        : Twrp3503
************************************************************
************************************************************************
** PROGRAM : TWRP3503
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: DRIVER PROGRAM FOR 1099 IRS CORRECTION REPORTING
** HISTORY.....:
**
**    10/19/2006 - JR - ADDED CALL TO TWRP3550 FOR 2006 CHANGES
**    01/30/2008 - AY - REVISED TO ACCOMMODATE FORM 480.7C, WHICH
**                      REPLACES FORMS 480.6A AND 480.6B AS OF TAX YEAR
**                      2007.  REPLACED TWRL5000 WITH TWRAFRMN /
**                      TWRNFRMN.
**
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3503 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwrafrmn pdaTwrafrmn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Prev_Rpt_Date;
    private DbsField pnd_Ws_Pnd_System_Year;
    private DbsField pnd_Ws_Pnd_Datx;
    private DbsField pnd_Ws_Pnd_Timx;
    private DbsField pnd_Ws_Pnd_Tima;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_Prev_Rpt_Date = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Prev_Rpt_Date", "#PREV-RPT-DATE", FieldType.DATE, new DbsArrayController(1, 
            2));
        pnd_Ws_Pnd_System_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_System_Year", "#SYSTEM-YEAR", FieldType.STRING, 4);
        pnd_Ws_Pnd_Datx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Ws_Pnd_Timx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timx", "#TIMX", FieldType.TIME);
        pnd_Ws_Pnd_Tima = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tima", "#TIMA", FieldType.STRING, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3503() throws Exception
    {
        super("Twrp3503");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet131 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WS.#TAX-YEAR;//Natural: VALUE 1999
        if (condition((pnd_Ws_Pnd_Tax_Year.equals(1999))))
        {
            decideConditionsMet131++;
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3531' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3531"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 2000, 2001
        else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2000) || pnd_Ws_Pnd_Tax_Year.equals(2001))))
        {
            decideConditionsMet131++;
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3537' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3537"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 2002
        else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2002))))
        {
            decideConditionsMet131++;
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3535' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3535"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 2003
        else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2003))))
        {
            decideConditionsMet131++;
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3538' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3538"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 2004,2005
        else if (condition((pnd_Ws_Pnd_Tax_Year.equals(2004) || pnd_Ws_Pnd_Tax_Year.equals(2005))))
        {
            decideConditionsMet131++;
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3549' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3549"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            Global.getSTACK().pushData(StackOption.TOP, pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year(), pnd_Ws_Pnd_System_Year, pnd_Ws_Pnd_Datx, pnd_Ws_Pnd_Tima,             //Natural: FETCH 'TWRP3550' #TWRATBL4.#TAX-YEAR #WS.#SYSTEM-YEAR #WS.#DATX #WS.#TIMA #WS.#PREV-RPT-DATE ( * )
                pnd_Ws_Pnd_Prev_Rpt_Date.getValue("*"));
            Global.setFetchProgram(DbsUtil.getBlType("TWRP3550"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTROL
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP3503|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Form.equals("1099")))                                                                                                            //Natural: IF #WS.#FORM = '1099'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown input parmameter:",pnd_Ws_Pnd_Form,new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Unknown input parmameter:' #WS.#FORM 77T '***' / '***' 25T 'Valid value is:.......:"1099" - 1099-R & 1099-INT' 77T '***'
                        TabSetting(25),"Valid value is:.......:'1099' - 1099-R & 1099-INT",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Ws_Pnd_Terminate.setValue(101);                                                                                                                   //Natural: ASSIGN #WS.#TERMINATE := 101
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                    sub_Terminate_Processing();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"IRS Correction Reporting parameters:",NEWLINE,new TabSetting(23),"Tax Year...........:",    //Natural: WRITE ( 1 ) 7T 'IRS Correction Reporting parameters:' / 23T 'Tax Year...........:' #WS.#TAX-YEAR
                    pnd_Ws_Pnd_Tax_Year);
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Datx.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #WS.#DATX := *DATX
                pnd_Ws_Pnd_System_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_System_Year), Global.getDATN().divide(10000).subtract(1));                         //Natural: ASSIGN #WS.#SYSTEM-YEAR := *DATN / 10000 - 1
                pnd_Ws_Pnd_Timx.setValue(Global.getTIMX());                                                                                                               //Natural: ASSIGN #WS.#TIMX := *TIMX
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                //*  01/30/2008 - AY
                DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
                    pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                FOR01:                                                                                                                                                    //Natural: FOR #I = 1 TO 2
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
                {
                    pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                    //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                    //*  01/30/2008
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL
                    sub_Read_Control();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(23),"Form...............: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1)); //Natural: WRITE ( 1 ) / 23T 'Form...............: ' #TWRAFRMN.#FORM-NAME ( #I )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean())))                                                                   //Natural: IF NOT #TWRATBL4.#TIRCNTL-IRS-ORIG
                    {
                        //*  01/30/2008 - AY
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"IRS Original Reporting for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new  //Natural: WRITE ( 1 ) '***' 25T 'IRS Original Reporting for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***' / '***' 25T 'had not been run' 77T '***'
                            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"had not been run",new TabSetting(77),"***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(102);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 102
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().equals(12)))                                                                         //Natural: IF C-TIRCNTL-RPT-TBL-PE = 12
                    {
                        //*  01/30/2008
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"11 cycles of IRS Correction Reporting had",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T '11 cycles of IRS Correction Reporting had' 77T '***' / '***' 25T 'been run for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***'
                            TabSetting(25),"been run for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new TabSetting(77),
                            "***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(103);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 103
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition((pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()).add(25)).greater(pnd_Ws_Pnd_Datx))) //Natural: IF ( TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) + 25 ) > #WS.#DATX
                    {
                        //*  01/30/2008
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Last cycle of IRS Correction Reporting had",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Last cycle of IRS Correction Reporting had' 77T '***' / '***' 25T 'been run for' #TWRAFRMN.#FORM-NAME ( #I ) 77T '***' / '***' 25T 'within previous 25 days on' TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE ) ( EM = MM/DD/YYYY ) 77T '***'
                            TabSetting(25),"been run for",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),new TabSetting(77),"***",NEWLINE,"***",new 
                            TabSetting(25),"within previous 25 days on",pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe()), 
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Pnd_Terminate.setValue(104);                                                                                                               //Natural: ASSIGN #WS.#TERMINATE := 104
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
                        sub_Terminate_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Prev_Rpt_Date.getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe())); //Natural: ASSIGN #WS.#PREV-RPT-DATE ( #I ) := TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE )
                    pdaTwratbl4.getPnd_Twratbl4_Tircntl_Rpt_Dte().getValue(pdaTwratbl4.getPnd_Twratbl4_C_Tircntl_Rpt_Tbl_Pe().getDec().add(1)).setValue(pnd_Ws_Pnd_Datx); //Natural: ASSIGN TIRCNTL-RPT-DTE ( C-TIRCNTL-RPT-TBL-PE + 1 ) := #WS.#DATX
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTROL
                    sub_Update_Control();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Ws_Pnd_Tima.setValueEdited(pnd_Ws_Pnd_Timx,new ReportEditMask("MMDDYYYYHHIISST"));                                                                    //Natural: MOVE EDITED #WS.#TIMX ( EM = MMDDYYYYHHIISST ) TO #WS.#TIMA
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(13),"System Date used for this run:",pnd_Ws_Pnd_Datx, new ReportEditMask                //Natural: WRITE ( 1 ) / 13T 'System Date used for this run:' #WS.#DATX ( EM = MM/DD/YYYY ) / 13T 'System Time used for this run:' #WS.#TIMX ( EM = MM/DD/YYYY�HH:II:SS.T )
                    ("MM/DD/YYYY"),NEWLINE,new TabSetting(13),"System Time used for this run:",pnd_Ws_Pnd_Timx, new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"));
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Control() throws Exception                                                                                                                      //Natural: READ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Control() throws Exception                                                                                                                    //Natural: UPDATE-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrntb4u.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4U' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                       //Natural: TERMINATE #WS.#TERMINATE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
    }
}
