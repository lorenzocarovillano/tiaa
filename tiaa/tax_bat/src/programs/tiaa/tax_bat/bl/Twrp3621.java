/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:38:26 PM
**        * FROM NATURAL PROGRAM : Twrp3621
************************************************************
**        * FILE NAME            : Twrp3621.java
**        * CLASS NAME           : Twrp3621
**        * INSTANCE NAME        : Twrp3621
************************************************************
***********************************************************************
*
*         :  CLONED TWRP3620 - FOR NY STATE ONLY
*
* PROGRAM  : TWRP3625
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING, AND REPORTING SYSTEM.
* TITLE    : PRODUCES MAG. MEDIA FILES FOR MAG. MEDIA STATE REPORTING.
* CREATED  : 01 / 14 / 2014.
*   BY     : ROXAN CARREON
* FUNCTION : PROGRAM PRODUCES STATE MAGNETIC MEDIA FILES FOR
*            NEW YORK STATE ONLY
*
*  HISTORY:
*         :  CLONED TWRP3620 FOR NY STATE ONLY
*
* 11/28/17 RM - RESTOW MODULE - TWRL3502 MODIFIED 2017 LAYOUT CHANGE
*
* 10/05/18 RAHUL : EIN CHANGE - TAG: EINCHG
***********************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3621 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl2 pdaTwratbl2;
    private LdaTwrl3502 ldaTwrl3502;
    private LdaTwrl3513 ldaTwrl3513;
    private PdaTwracom2 pdaTwracom2;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Work_File_Pnd_Tirf_Sys_Err;
    private DbsField pnd_Work_File_Pnd_Tirf_Empty_Form;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Distr;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Tax_Wthld;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Code;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Distr;
    private DbsField pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_Work_File_Pnd_Tirf_Isn;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Tirf_Isn_A;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Rpt_Ind;
    private DbsField pnd_Work_File_Pnd_Tirf_State_Code;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year_Cc;
    private DbsField pnd_Work_File_Pnd_Tirf_Tax_Year_Yy;
    private DbsField pnd_Work_File_Pnd_Tirf_Name;
    private DbsField pnd_Work_File_Pnd_Tirf_Tin;
    private DbsField pnd_Work_File_Pnd_Tirf_Account;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln1;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln2;
    private DbsField pnd_Work_File_Pnd_Tirf_Addr_Ln3;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Tot_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1w;
    private DbsField pnd_Work_Area_Pnd_Grand_1e;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_State_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Today;
    private DbsField pnd_Work_Area_Pnd_Trailer;
    private DbsField pnd_Work_Area_Pnd_I;
    private DbsField pnd_Work_Area_Pnd_Old_Company_Cde;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Distr;
    private DbsField pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld;
    private DbsField pnd_Work_Area_Pnd_Fil1;
    private DbsField pnd_Work_Area_Pnd_Fil2;
    private DbsField pnd_Work_Area_Pnd_Fil3;
    private DbsField pnd_Work_Area_Pnd_Fil4;
    private DbsField pnd_Work_Area_Pnd_Daten;

    private DbsGroup pnd_Work_Area__R_Field_3;
    private DbsField pnd_Work_Area_Pnd_Daten_Cc;
    private DbsField pnd_Work_Area_Pnd_Daten_Yy;
    private DbsField pnd_Work_Area_Pnd_Daten_Mm;
    private DbsField pnd_Work_Area_Pnd_Daten_Dd;
    private DbsField pnd_Rec_Seq_Number;
    private DbsField pnd_Prev_A_Rec_Company;
    private DbsField pnd_Prev_C_Rec_Company;
    private DbsField pnd_Temp_State_Id;

    private DbsGroup pnd_Temp_State_Id__R_Field_4;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill;
    private DbsField pnd_Temp_State_Id_Pnd_Temp_State_Id_7;
    private DbsField pnd_Temp_State_Id_20;

    private DbsGroup pnd_Work_Out_Ny_Record;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1;

    private DbsGroup pnd_Work_Out_Ny_Record__R_Field_5;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7;
    private DbsField pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8;

    private DbsGroup pnd_W_Work_File;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Company_Cde;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Sys_Err;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Empty_Form;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Distr;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Code;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Isn;

    private DbsGroup pnd_W_Work_File__R_Field_6;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Isn_A;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_State_Code;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year;

    private DbsGroup pnd_W_Work_File__R_Field_7;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Name;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Tin;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Account;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2;
    private DbsField pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3;
    private DbsField pnd_Out_Ny_Record;

    private DbsGroup pnd_Out_Ny_Record__R_Field_8;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5;
    private DbsField pnd_W_Accum_Taxable_Amt;
    private DbsField pnd_W_Accum_Tax_Wthld;
    private DbsField pnd_Save_Outb_Tin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl2 = new PdaTwratbl2(localVariables);
        ldaTwrl3502 = new LdaTwrl3502();
        registerRecord(ldaTwrl3502);
        ldaTwrl3513 = new LdaTwrl3513();
        registerRecord(ldaTwrl3513);
        pdaTwracom2 = new PdaTwracom2(localVariables);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Tirf_Company_Cde = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Tirf_Sys_Err = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Sys_Err", "#TIRF-SYS-ERR", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Tirf_Empty_Form = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Empty_Form", "#TIRF-EMPTY-FORM", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Tirf_State_Distr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Distr", "#TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_File_Pnd_Tirf_State_Tax_Wthld = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Tax_Wthld", "#TIRF-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_Pnd_Tirf_Loc_Code = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Code", "#TIRF-LOC-CODE", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Tirf_Loc_Distr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Distr", "#TIRF-LOC-DISTR", FieldType.NUMERIC, 11, 
            2);
        pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld", "#TIRF-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            9, 2);
        pnd_Work_File_Pnd_Tirf_Isn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Isn", "#TIRF-ISN", FieldType.NUMERIC, 8);

        pnd_Work_File__R_Field_1 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File_Pnd_Tirf_Isn);
        pnd_Work_File_Pnd_Tirf_Isn_A = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Isn_A", "#TIRF-ISN-A", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Tirf_State_Rpt_Ind = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Rpt_Ind", "#TIRF-STATE-RPT-IND", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Tirf_State_Code = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_State_Code", "#TIRF-STATE-CODE", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Tirf_Tax_Year = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);

        pnd_Work_File__R_Field_2 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File_Pnd_Tirf_Tax_Year);
        pnd_Work_File_Pnd_Tirf_Tax_Year_Cc = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year_Cc", "#TIRF-TAX-YEAR-CC", FieldType.STRING, 
            2);
        pnd_Work_File_Pnd_Tirf_Tax_Year_Yy = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tax_Year_Yy", "#TIRF-TAX-YEAR-YY", FieldType.STRING, 
            2);
        pnd_Work_File_Pnd_Tirf_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Name", "#TIRF-NAME", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Tin = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 9);
        pnd_Work_File_Pnd_Tirf_Account = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Account", "#TIRF-ACCOUNT", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Tirf_Addr_Ln1 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln1", "#TIRF-ADDR-LN1", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Addr_Ln2 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln2", "#TIRF-ADDR-LN2", FieldType.STRING, 40);
        pnd_Work_File_Pnd_Tirf_Addr_Ln3 = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Tirf_Addr_Ln3", "#TIRF-ADDR-LN3", FieldType.STRING, 40);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Tot_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_1w", "#TOT-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1w = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1w", "#GRAND-1W", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Grand_1e = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Grand_1e", "#GRAND-1E", FieldType.NUMERIC, 8);
        pnd_Work_Area_Pnd_Tot_State_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Distr", "#TOT-STATE-DISTR", FieldType.NUMERIC, 
            18, 2);
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_State_Tax_Wthld", "#TOT-STATE-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Today = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Work_Area_Pnd_Trailer = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Trailer", "#TRAILER", FieldType.STRING, 20);
        pnd_Work_Area_Pnd_I = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_I", "#I", FieldType.NUMERIC, 5);
        pnd_Work_Area_Pnd_Old_Company_Cde = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Old_Company_Cde", "#OLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Tot_Loc_Distr = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Distr", "#TOT-LOC-DISTR", FieldType.NUMERIC, 18, 2);
        pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Loc_Tax_Wthld", "#TOT-LOC-TAX-WTHLD", FieldType.NUMERIC, 
            13, 2);
        pnd_Work_Area_Pnd_Fil1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil1", "#FIL1", FieldType.STRING, 122);
        pnd_Work_Area_Pnd_Fil2 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil2", "#FIL2", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil3 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil3", "#FIL3", FieldType.STRING, 250);
        pnd_Work_Area_Pnd_Fil4 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fil4", "#FIL4", FieldType.STRING, 225);
        pnd_Work_Area_Pnd_Daten = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Daten", "#DATEN", FieldType.NUMERIC, 8);

        pnd_Work_Area__R_Field_3 = pnd_Work_Area.newGroupInGroup("pnd_Work_Area__R_Field_3", "REDEFINE", pnd_Work_Area_Pnd_Daten);
        pnd_Work_Area_Pnd_Daten_Cc = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Cc", "#DATEN-CC", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Yy = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Yy", "#DATEN-YY", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Mm = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Mm", "#DATEN-MM", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Daten_Dd = pnd_Work_Area__R_Field_3.newFieldInGroup("pnd_Work_Area_Pnd_Daten_Dd", "#DATEN-DD", FieldType.STRING, 2);
        pnd_Rec_Seq_Number = localVariables.newFieldInRecord("pnd_Rec_Seq_Number", "#REC-SEQ-NUMBER", FieldType.NUMERIC, 8);
        pnd_Prev_A_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_A_Rec_Company", "#PREV-A-REC-COMPANY", FieldType.STRING, 1);
        pnd_Prev_C_Rec_Company = localVariables.newFieldInRecord("pnd_Prev_C_Rec_Company", "#PREV-C-REC-COMPANY", FieldType.STRING, 1);
        pnd_Temp_State_Id = localVariables.newFieldInRecord("pnd_Temp_State_Id", "#TEMP-STATE-ID", FieldType.STRING, 14);

        pnd_Temp_State_Id__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_State_Id__R_Field_4", "REDEFINE", pnd_Temp_State_Id);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill = pnd_Temp_State_Id__R_Field_4.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_Fill", "#TEMP-STATE-ID-FILL", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_Pnd_Temp_State_Id_7 = pnd_Temp_State_Id__R_Field_4.newFieldInGroup("pnd_Temp_State_Id_Pnd_Temp_State_Id_7", "#TEMP-STATE-ID-7", 
            FieldType.STRING, 7);
        pnd_Temp_State_Id_20 = localVariables.newFieldInRecord("pnd_Temp_State_Id_20", "#TEMP-STATE-ID-20", FieldType.STRING, 20);

        pnd_Work_Out_Ny_Record = localVariables.newGroupInRecord("pnd_Work_Out_Ny_Record", "#WORK-OUT-NY-RECORD");
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1", "#W-OUT-IRS-REC1", 
            FieldType.STRING, 100);

        pnd_Work_Out_Ny_Record__R_Field_5 = pnd_Work_Out_Ny_Record.newGroupInGroup("pnd_Work_Out_Ny_Record__R_Field_5", "REDEFINE", pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Id", "#W-OUTW-NY-ID", 
            FieldType.STRING, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tin", "#W-OUTW-NY-TIN", 
            FieldType.STRING, 9);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Name", "#W-OUTW-NY-NAME", 
            FieldType.STRING, 30);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1 = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank1", 
            "#W-OUTW-NY-BLANK1", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Wages_Ind", 
            "#W-OUTW-NY-WAGES-IND", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2 = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank2", 
            "#W-OUTW-NY-BLANK2", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Gross", "#W-OUTW-NY-GROSS", 
            FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3 = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank3", 
            "#W-OUTW-NY-BLANK3", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt", 
            "#W-OUTW-NY-TAXABLE-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4 = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank4", 
            "#W-OUTW-NY-BLANK4", FieldType.STRING, 1);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld", 
            "#W-OUTW-NY-TAX-WTHLD", FieldType.NUMERIC, 14, 2);
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5 = pnd_Work_Out_Ny_Record__R_Field_5.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Blank5", 
            "#W-OUTW-NY-BLANK5", FieldType.STRING, 12);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2", "#W-OUT-IRS-REC2", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3", "#W-OUT-IRS-REC3", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4", "#W-OUT-IRS-REC4", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5", "#W-OUT-IRS-REC5", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6", "#W-OUT-IRS-REC6", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7", "#W-OUT-IRS-REC7", 
            FieldType.STRING, 100);
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8 = pnd_Work_Out_Ny_Record.newFieldInGroup("pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8", "#W-OUT-IRS-REC8", 
            FieldType.STRING, 50);

        pnd_W_Work_File = localVariables.newGroupInRecord("pnd_W_Work_File", "#W-WORK-FILE");
        pnd_W_Work_File_Pnd_W_Tirf_Company_Cde = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Company_Cde", "#W-TIRF-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_Sys_Err = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Sys_Err", "#W-TIRF-SYS-ERR", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_Empty_Form = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Empty_Form", "#W-TIRF-EMPTY-FORM", FieldType.STRING, 
            1);
        pnd_W_Work_File_Pnd_W_Tirf_State_Distr = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Distr", "#W-TIRF-STATE-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld", "#W-TIRF-STATE-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Code = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Code", "#W-TIRF-LOC-CODE", FieldType.STRING, 
            5);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr", "#W-TIRF-LOC-DISTR", FieldType.NUMERIC, 
            11, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld", "#W-TIRF-LOC-TAX-WTHLD", 
            FieldType.NUMERIC, 9, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Isn = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Isn", "#W-TIRF-ISN", FieldType.NUMERIC, 8);

        pnd_W_Work_File__R_Field_6 = pnd_W_Work_File.newGroupInGroup("pnd_W_Work_File__R_Field_6", "REDEFINE", pnd_W_Work_File_Pnd_W_Tirf_Isn);
        pnd_W_Work_File_Pnd_W_Tirf_Isn_A = pnd_W_Work_File__R_Field_6.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Isn_A", "#W-TIRF-ISN-A", FieldType.STRING, 
            8);
        pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind", "#W-TIRF-STATE-RPT-IND", 
            FieldType.STRING, 1);
        pnd_W_Work_File_Pnd_W_Tirf_State_Code = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_State_Code", "#W-TIRF-STATE-CODE", FieldType.STRING, 
            2);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year", "#W-TIRF-TAX-YEAR", FieldType.STRING, 
            4);

        pnd_W_Work_File__R_Field_7 = pnd_W_Work_File.newGroupInGroup("pnd_W_Work_File__R_Field_7", "REDEFINE", pnd_W_Work_File_Pnd_W_Tirf_Tax_Year);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc = pnd_W_Work_File__R_Field_7.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Cc", "#W-TIRF-TAX-YEAR-CC", 
            FieldType.STRING, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy = pnd_W_Work_File__R_Field_7.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tax_Year_Yy", "#W-TIRF-TAX-YEAR-YY", 
            FieldType.STRING, 2);
        pnd_W_Work_File_Pnd_W_Tirf_Name = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Name", "#W-TIRF-NAME", FieldType.STRING, 40);
        pnd_W_Work_File_Pnd_W_Tirf_Tin = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Tin", "#W-TIRF-TIN", FieldType.STRING, 9);
        pnd_W_Work_File_Pnd_W_Tirf_Account = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Account", "#W-TIRF-ACCOUNT", FieldType.STRING, 
            10);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1", "#W-TIRF-ADDR-LN1", FieldType.STRING, 
            40);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2", "#W-TIRF-ADDR-LN2", FieldType.STRING, 
            40);
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3 = pnd_W_Work_File.newFieldInGroup("pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3", "#W-TIRF-ADDR-LN3", FieldType.STRING, 
            40);
        pnd_Out_Ny_Record = localVariables.newFieldInRecord("pnd_Out_Ny_Record", "#OUT-NY-RECORD", FieldType.STRING, 100);

        pnd_Out_Ny_Record__R_Field_8 = localVariables.newGroupInRecord("pnd_Out_Ny_Record__R_Field_8", "REDEFINE", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Id = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Id", "#OUTW-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin", "#OUTW-NY-TIN", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Name = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Name", "#OUTW-NY-NAME", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1 = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1", "#OUTW-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind", "#OUTW-NY-WAGES-IND", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2 = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2", "#OUTW-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross", "#OUTW-NY-GROSS", FieldType.NUMERIC, 
            14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3 = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3", "#OUTW-NY-BLANK3", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt", "#OUTW-NY-TAXABLE-AMT", 
            FieldType.NUMERIC, 14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4 = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4", "#OUTW-NY-BLANK4", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld", "#OUTW-NY-TAX-WTHLD", 
            FieldType.NUMERIC, 14, 2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5 = pnd_Out_Ny_Record__R_Field_8.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5", "#OUTW-NY-BLANK5", 
            FieldType.STRING, 12);
        pnd_W_Accum_Taxable_Amt = localVariables.newFieldInRecord("pnd_W_Accum_Taxable_Amt", "#W-ACCUM-TAXABLE-AMT", FieldType.NUMERIC, 14, 2);
        pnd_W_Accum_Tax_Wthld = localVariables.newFieldInRecord("pnd_W_Accum_Tax_Wthld", "#W-ACCUM-TAX-WTHLD", FieldType.NUMERIC, 14, 2);
        pnd_Save_Outb_Tin = localVariables.newFieldInRecord("pnd_Save_Outb_Tin", "#SAVE-OUTB-TIN", FieldType.STRING, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3502.initializeValues();
        ldaTwrl3513.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3621() throws Exception
    {
        super("Twrp3621");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Work_Area_Pnd_Today.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #TODAY := *DATX
        pnd_Work_Area_Pnd_Daten.setValue(Global.getDATN());                                                                                                               //Natural: ASSIGN #DATEN := *DATN
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #OUT-IRS-REC1 #OUT-IRS-REC2 #OUT-IRS-REC3 #OUT-IRS-REC4 #OUT-IRS-REC5 #OUT-IRS-REC6 #OUT-IRS-REC7 #OUT-IRS-REC8 #WORK-FILE
        while (condition(getWorkFiles().read(1, ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5(), 
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7(), ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8(), 
            pnd_Work_File)))
        {
            CheckAtStartofData910();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(pnd_Prev_C_Rec_Company.notEquals(pnd_Work_File_Pnd_Tirf_Company_Cde)))                                                                          //Natural: IF #PREV-C-REC-COMPANY NE #TIRF-COMPANY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
                sub_Create_C_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_C_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                      //Natural: ASSIGN #PREV-C-REC-COMPANY := #TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_A_Rec_Company.notEquals(pnd_Work_File_Pnd_Tirf_Company_Cde)))                                                                          //Natural: IF #PREV-A-REC-COMPANY NE #TIRF-COMPANY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
                sub_Create_A_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_A_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                      //Natural: ASSIGN #PREV-A-REC-COMPANY := #TIRF-COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Seq_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-SEQ-NUMBER
            ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Outb_Seq_Num().setValue(pnd_Rec_Seq_Number);                                                                            //Natural: ASSIGN #OUTB-SEQ-NUM := #REC-SEQ-NUMBER
            pnd_Out_Ny_Record.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1());                                                                             //Natural: ASSIGN #OUT-NY-RECORD := #OUT-IRS-REC1
            if (condition(pnd_Save_Outb_Tin.equals(pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin)))                                                                                   //Natural: IF #SAVE-OUTB-TIN = #OUTW-NY-TIN
            {
                pnd_W_Accum_Taxable_Amt.nadd(pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt);                                                                                  //Natural: ASSIGN #W-ACCUM-TAXABLE-AMT := #W-ACCUM-TAXABLE-AMT + #OUTW-NY-TAXABLE-AMT
                pnd_W_Accum_Tax_Wthld.nadd(pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld);                                                                                      //Natural: ASSIGN #W-ACCUM-TAX-WTHLD := #W-ACCUM-TAX-WTHLD + #OUTW-NY-TAX-WTHLD
                pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "3", pnd_Work_File_Pnd_Tirf_Isn_A));                               //Natural: COMPRESS #OLD-COMPANY-CDE '3' #TIRF-ISN-A INTO #TRAILER
                                                                                                                                                                          //Natural: PERFORM REFORMAT-REC
                sub_Reformat_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
                sub_Write_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM REFORMAT-REC
                sub_Reformat_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Out_Ny_Record.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1());                                                                         //Natural: ASSIGN #OUT-NY-RECORD := #OUT-IRS-REC1
                pnd_W_Accum_Taxable_Amt.setValue(pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt);                                                                              //Natural: ASSIGN #W-ACCUM-TAXABLE-AMT := #OUTW-NY-TAXABLE-AMT
                pnd_W_Accum_Tax_Wthld.setValue(pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld);                                                                                  //Natural: ASSIGN #W-ACCUM-TAX-WTHLD := #OUTW-NY-TAX-WTHLD
                pnd_Save_Outb_Tin.setValue(pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin);                                                                                            //Natural: ASSIGN #SAVE-OUTB-TIN := #OUTW-NY-TIN
            }                                                                                                                                                             //Natural: END-IF
            //*  $$$$
            if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                //Natural: IF #TIRF-STATE-CODE = 'NY'
            {
                pnd_Work_Area_Pnd_Grand_1w.nadd(1);                                                                                                                       //Natural: ADD 1 TO #GRAND-1W
                pnd_Work_Area_Pnd_Tot_1w.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOT-1W
                pnd_Work_Area_Pnd_Tot_State_Distr.nadd(pnd_Work_File_Pnd_Tirf_State_Distr);                                                                               //Natural: ADD #TIRF-STATE-DISTR TO #TOT-STATE-DISTR
                pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.nadd(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                                       //Natural: ADD #TIRF-STATE-TAX-WTHLD TO #TOT-STATE-TAX-WTHLD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  $$$$  ROX
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
        sub_Write_Record();
        if (condition(Global.isEscape())) {return;}
        //*  $$$$
        if (condition(pnd_Rec_Seq_Number.notEquals(getZero())))                                                                                                           //Natural: IF #REC-SEQ-NUMBER NE 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-C-RECORD
            sub_Create_C_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TRAILER-REC
            sub_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-A-RECORD
        //* ********************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-A-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-C-RECORD
        //* *****************************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-RECORD
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-HEADER-REC
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRAILER-REC
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NY-TRAILER-REC
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* ********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY-INFO
        //* *********************************
        //* *------------
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-REC
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Create_A_Record() throws Exception                                                                                                                   //Natural: CREATE-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Old_Company_Cde.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                                   //Natural: ASSIGN #OLD-COMPANY-CDE := #TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM NY-A-RECORD
        sub_Ny_A_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Ny_A_Record() throws Exception                                                                                                                       //Natural: NY-A-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Work_File_Pnd_Tirf_Tax_Year_Yy)); //Natural: COMPRESS '12' #TIRF-TAX-YEAR-YY INTO #OUT1E-NY-REPORT-QUARTER LEAVING NO SPACE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id().setValue("1E");                                                                                               //Natural: ASSIGN #OUT1E-NY-ID := '1E'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                          //Natural: ASSIGN #OUT1E-NY-TIN := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Payer_A());                                             //Natural: ASSIGN #OUT1E-NY-TRANS-NAME := #TWRACOM2.#COMP-PAYER-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1E-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1E-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1E-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1E-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1E-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1E-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type().setValue("O");                                                                                          //Natural: ASSIGN #OUT1E-NY-RET-TYPE := 'O'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind().setValue(" ");                                                                                           //Natural: ASSIGN #OUT1E-NY-EMP-IND := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "2"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '2' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Create_C_Record() throws Exception                                                                                                                   //Natural: CREATE-C-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        pnd_Work_Area_Pnd_Grand_1e.nadd(1);                                                                                                                               //Natural: ADD 1 TO #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id().setValue("1T");                                                                                               //Natural: ASSIGN #OUT1T-NY-ID := '1T'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Tot_1w);                                                                     //Natural: ASSIGN #OUT1T-NY-TOTAL-1W := #TOT-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1T-NY-BLANK := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross().setValue(pnd_Work_Area_Pnd_Tot_State_Distr);                                                         //Natural: ASSIGN #OUT1T-NY-TOTAL-GROSS := #TOT-STATE-DISTR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross().setValue(0);                                                                                         //Natural: ASSIGN #OUT1T-NY-TOTAL-GROSS := 0
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK1 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt().setValue(pnd_Work_Area_Pnd_Tot_State_Distr);                                                   //Natural: ASSIGN #OUT1T-NY-TOTAL-TAXABLE-AMT := #TOT-STATE-DISTR
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK2 := ' '
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld().setValue(pnd_Work_Area_Pnd_Tot_State_Tax_Wthld);                                                       //Natural: ASSIGN #OUT1T-NY-TAX-WTHLD := #TOT-STATE-TAX-WTHLD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3().setValue(" ");                                                                                            //Natural: ASSIGN #OUT1T-NY-BLANK3 := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress(pnd_Work_Area_Pnd_Old_Company_Cde, "4"));                                                                     //Natural: COMPRESS #OLD-COMPANY-CDE '4' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
        pnd_Work_Area_Pnd_Tot_1w.reset();                                                                                                                                 //Natural: RESET #TOT-1W #TOT-STATE-DISTR #TOT-STATE-TAX-WTHLD
        pnd_Work_Area_Pnd_Tot_State_Distr.reset();
        pnd_Work_Area_Pnd_Tot_State_Tax_Wthld.reset();
    }
    private void sub_Header_Record() throws Exception                                                                                                                     //Natural: HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  EINCHG
        //*  EINCHG
        if (condition(pnd_Work_File_Pnd_Tirf_Tax_Year.greaterOrEqual("2018")))                                                                                            //Natural: IF #TIRF-TAX-YEAR GE '2018'
        {
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue("A");                                                                                                              //Natural: ASSIGN #OLD-COMPANY-CDE := 'A'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue("T");                                                                                                              //Natural: ASSIGN #OLD-COMPANY-CDE := 'T'
            //*  EINCHG
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY-INFO
        sub_Get_Company_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-HEADER-REC
            sub_Ny_Header_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ny_Header_Rec() throws Exception                                                                                                                     //Natural: NY-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        //*   #FED-ID
        //*   #COMP-NAME
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id().setValue("1A");                                                                                               //Natural: ASSIGN #OUT1A-NY-ID := '1A'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm().setValue(pnd_Work_Area_Pnd_Daten_Mm);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-MM := #DATEN-MM
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd().setValue(pnd_Work_Area_Pnd_Daten_Dd);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-DD := #DATEN-DD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy().setValue(pnd_Work_Area_Pnd_Daten_Yy);                                                             //Natural: ASSIGN #OUT1A-NY-CREATE-DATE-YY := #DATEN-YY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Fed_Id());                                                       //Natural: ASSIGN #OUT1A-NY-ID-NUM := #TWRACOM2.#FED-ID
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Trans_A());                                             //Natural: ASSIGN #OUT1A-NY-TRANS-NAME := #TWRACOM2.#COMP-TRANS-A
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Address().getValue(1));                                         //Natural: ASSIGN #OUT1A-NY-ADDRESS := #ADDRESS ( 1 )
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_City().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_City());                                                           //Natural: ASSIGN #OUT1A-NY-CITY := #CITY
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_State().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_State());                                                         //Natural: ASSIGN #OUT1A-NY-STATE := #STATE
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip().setValue(pdaTwracom2.getPnd_Twracom2_Pnd_Zip_9());                                                           //Natural: ASSIGN #OUT1A-NY-ZIP := #ZIP-9
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1A-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("A", "1"));                                                                                                   //Natural: COMPRESS 'A' '1' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Trailer_Rec() throws Exception                                                                                                                       //Natural: TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Work_File_Pnd_Tirf_State_Code.equals("NY")))                                                                                                    //Natural: IF #TIRF-STATE-CODE = 'NY'
        {
                                                                                                                                                                          //Natural: PERFORM NY-TRAILER-REC
            sub_Ny_Trailer_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ny_Trailer_Rec() throws Exception                                                                                                                    //Natural: NY-TRAILER-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Rec_Seq_Number.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #REC-SEQ-NUMBER
        ldaTwrl3513.getPnd_Out1_Ny_Record().reset();                                                                                                                      //Natural: RESET #OUT1-NY-RECORD
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id().setValue("1F");                                                                                               //Natural: ASSIGN #OUT1F-NY-ID := '1F'
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e().setValue(pnd_Work_Area_Pnd_Grand_1e);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1E := #GRAND-1E
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w().setValue(pnd_Work_Area_Pnd_Grand_1w);                                                                   //Natural: ASSIGN #OUT1F-NY-TOTAL-1W := #GRAND-1W
        ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank().setValue(" ");                                                                                             //Natural: ASSIGN #OUT1F-NY-BLANK := ' '
        pnd_Work_Area_Pnd_Trailer.setValue(DbsUtil.compress("Z", "9"));                                                                                                   //Natural: COMPRESS 'Z' '9' INTO #TRAILER
        getWorkFiles().write(2, false, ldaTwrl3513.getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1(), pnd_Work_Area_Pnd_Fil1, pnd_Work_Area_Pnd_Fil2, pnd_Work_Area_Pnd_Fil3,      //Natural: WRITE WORK FILE 02 #OUT1-NY-REC1 #FIL1 #FIL2 #FIL3 #TRAILER
            pnd_Work_Area_Pnd_Trailer);
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwratbl2.getTwratbl2_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwratbl2.getTwratbl2_Pnd_Tax_Year()), pnd_Work_File_Pnd_Tirf_Tax_Year.val());      //Natural: ASSIGN TWRATBL2.#TAX-YEAR := VAL ( #TIRF-TAX-YEAR )
        pdaTwratbl2.getTwratbl2_Pnd_Abend_Ind().setValue(false);                                                                                                          //Natural: ASSIGN TWRATBL2.#ABEND-IND := FALSE
        pdaTwratbl2.getTwratbl2_Pnd_Display_Ind().setValue(false);                                                                                                        //Natural: ASSIGN TWRATBL2.#DISPLAY-IND := FALSE
        if (condition(DbsUtil.maskMatches(pnd_Work_File_Pnd_Tirf_State_Code,"NN")))                                                                                       //Natural: IF #TIRF-STATE-CODE = MASK ( NN )
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("1");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '1'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pnd_Work_File_Pnd_Tirf_State_Code));                    //Natural: COMPRESS '0' #TIRF-STATE-CODE INTO TWRATBL2.#STATE-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwratbl2.getTwratbl2_Pnd_Function().setValue("2");                                                                                                         //Natural: ASSIGN TWRATBL2.#FUNCTION := '2'
            pdaTwratbl2.getTwratbl2_Pnd_State_Cde().setValue(pnd_Work_File_Pnd_Tirf_State_Code);                                                                          //Natural: ASSIGN TWRATBL2.#STATE-CDE := #TIRF-STATE-CODE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Twrntbl2.class , getCurrentProcessState(), pdaTwratbl2.getTwratbl2_Input_Parms(), new AttributeParameter("O"), pdaTwratbl2.getTwratbl2_Output_Data(),  //Natural: CALLNAT 'TWRNTBL2' USING TWRATBL2.INPUT-PARMS ( AD = O ) TWRATBL2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Company_Info() throws Exception                                                                                                                  //Natural: GET-COMPANY-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwracom2.getPnd_Twracom2_Pnd_Comp_Code().setValue(pnd_Work_Area_Pnd_Old_Company_Cde);                                                                          //Natural: ASSIGN #TWRACOM2.#COMP-CODE := #OLD-COMPANY-CDE
        pdaTwracom2.getPnd_Twracom2_Pnd_Form_Type().setValue(1);                                                                                                          //Natural: ASSIGN #TWRACOM2.#FORM-TYPE := 01
        pdaTwracom2.getPnd_Twracom2_Pnd_Tax_Year().setValueEdited(new ReportEditMask("9999"),pnd_Work_File_Pnd_Tirf_Tax_Year);                                            //Natural: MOVE EDITED #TIRF-TAX-YEAR TO #TWRACOM2.#TAX-YEAR ( EM = 9999 )
        pdaTwracom2.getPnd_Twracom2_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOM2.#ABEND-IND := #TWRACOM2.#DISPLAY-IND := FALSE
        pdaTwracom2.getPnd_Twracom2_Pnd_Display_Ind().setValue(false);
        DbsUtil.callnat(Twrncom2.class , getCurrentProcessState(), pdaTwracom2.getPnd_Twracom2_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracom2.getPnd_Twracom2_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOM2' USING #TWRACOM2.#INPUT-PARMS ( AD = O ) #TWRACOM2.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Code().equals(false)))                                                                                          //Natural: IF #RET-CODE = FALSE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"State Original Return Code Company Inf.'TWRNCOM2'",new TabSetting(77),"***",NEWLINE,"***",new                  //Natural: WRITE ( 00 ) '***' 06T 'State Original Return Code Company Inf."TWRNCOM2"' 77T '***' / '***' 06T 'Return Message...:' #TWRACOM2.#RET-MSG 77T '***' / '***' 06T 'Program..........:' *PROGRAM 77T '***'
                TabSetting(6),"Return Message...:",pdaTwracom2.getPnd_Twracom2_Pnd_Ret_Msg(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Program..........:",Global.getPROGRAM(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Taxable_Amt.setValue(pnd_W_Accum_Taxable_Amt);                                                                               //Natural: ASSIGN #W-OUTW-NY-TAXABLE-AMT := #W-ACCUM-TAXABLE-AMT
        pnd_Work_Out_Ny_Record_Pnd_W_Outw_Ny_Tax_Wthld.setValue(pnd_W_Accum_Tax_Wthld);                                                                                   //Natural: ASSIGN #W-OUTW-NY-TAX-WTHLD := #W-ACCUM-TAX-WTHLD
        getWorkFiles().write(2, false, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3,   //Natural: WRITE WORK FILE 02 #W-OUT-IRS-REC1 #W-OUT-IRS-REC2 #W-OUT-IRS-REC3 #W-OUT-IRS-REC4 #W-OUT-IRS-REC5 #W-OUT-IRS-REC6 #W-OUT-IRS-REC7 #W-OUT-IRS-REC8 #TRAILER
            pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6, pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7, 
            pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8, pnd_Work_Area_Pnd_Trailer);
    }
    //*   $$$$  ROX
    private void sub_Reformat_Rec() throws Exception                                                                                                                      //Natural: REFORMAT-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec1.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1());                                                         //Natural: ASSIGN #W-OUT-IRS-REC1 := #OUT-IRS-REC1
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec2.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2());                                                         //Natural: ASSIGN #W-OUT-IRS-REC2 := #OUT-IRS-REC2
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec3.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3());                                                         //Natural: ASSIGN #W-OUT-IRS-REC3 := #OUT-IRS-REC3
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec4.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4());                                                         //Natural: ASSIGN #W-OUT-IRS-REC4 := #OUT-IRS-REC4
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec5.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5());                                                         //Natural: ASSIGN #W-OUT-IRS-REC5 := #OUT-IRS-REC5
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec6.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6());                                                         //Natural: ASSIGN #W-OUT-IRS-REC6 := #OUT-IRS-REC6
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec7.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7());                                                         //Natural: ASSIGN #W-OUT-IRS-REC7 := #OUT-IRS-REC7
        pnd_Work_Out_Ny_Record_Pnd_W_Out_Irs_Rec8.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8());                                                         //Natural: ASSIGN #W-OUT-IRS-REC8 := #OUT-IRS-REC8
        pnd_W_Work_File_Pnd_W_Tirf_Company_Cde.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                              //Natural: ASSIGN #W-TIRF-COMPANY-CDE := #TIRF-COMPANY-CDE
        pnd_W_Work_File_Pnd_W_Tirf_Sys_Err.setValue(pnd_Work_File_Pnd_Tirf_Sys_Err);                                                                                      //Natural: ASSIGN #W-TIRF-SYS-ERR := #TIRF-SYS-ERR
        pnd_W_Work_File_Pnd_W_Tirf_Empty_Form.setValue(pnd_Work_File_Pnd_Tirf_Empty_Form);                                                                                //Natural: ASSIGN #W-TIRF-EMPTY-FORM := #TIRF-EMPTY-FORM
        pnd_W_Work_File_Pnd_W_Tirf_State_Distr.setValue(pnd_Work_File_Pnd_Tirf_State_Distr);                                                                              //Natural: ASSIGN #W-TIRF-STATE-DISTR := #TIRF-STATE-DISTR
        pnd_W_Work_File_Pnd_W_Tirf_State_Tax_Wthld.setValue(pnd_Work_File_Pnd_Tirf_State_Tax_Wthld);                                                                      //Natural: ASSIGN #W-TIRF-STATE-TAX-WTHLD := #TIRF-STATE-TAX-WTHLD
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Code.setValue(pnd_Work_File_Pnd_Tirf_Loc_Code);                                                                                    //Natural: ASSIGN #W-TIRF-LOC-CODE := #TIRF-LOC-CODE
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Distr.setValue(pnd_Work_File_Pnd_Tirf_Loc_Distr);                                                                                  //Natural: ASSIGN #W-TIRF-LOC-DISTR := #TIRF-LOC-DISTR
        pnd_W_Work_File_Pnd_W_Tirf_Loc_Tax_Wthld.setValue(pnd_Work_File_Pnd_Tirf_Loc_Tax_Wthld);                                                                          //Natural: ASSIGN #W-TIRF-LOC-TAX-WTHLD := #TIRF-LOC-TAX-WTHLD
        pnd_W_Work_File_Pnd_W_Tirf_Isn.setValue(pnd_Work_File_Pnd_Tirf_Isn);                                                                                              //Natural: ASSIGN #W-TIRF-ISN := #TIRF-ISN
        pnd_W_Work_File_Pnd_W_Tirf_State_Rpt_Ind.setValue(pnd_Work_File_Pnd_Tirf_State_Rpt_Ind);                                                                          //Natural: ASSIGN #W-TIRF-STATE-RPT-IND := #TIRF-STATE-RPT-IND
        pnd_W_Work_File_Pnd_W_Tirf_State_Code.setValue(pnd_Work_File_Pnd_Tirf_State_Code);                                                                                //Natural: ASSIGN #W-TIRF-STATE-CODE := #TIRF-STATE-CODE
        pnd_W_Work_File_Pnd_W_Tirf_Tax_Year.setValue(pnd_Work_File_Pnd_Tirf_Tax_Year);                                                                                    //Natural: ASSIGN #W-TIRF-TAX-YEAR := #TIRF-TAX-YEAR
        pnd_W_Work_File_Pnd_W_Tirf_Name.setValue(pnd_Work_File_Pnd_Tirf_Name);                                                                                            //Natural: ASSIGN #W-TIRF-NAME := #TIRF-NAME
        pnd_W_Work_File_Pnd_W_Tirf_Tin.setValue(pnd_Work_File_Pnd_Tirf_Tin);                                                                                              //Natural: ASSIGN #W-TIRF-TIN := #TIRF-TIN
        pnd_W_Work_File_Pnd_W_Tirf_Account.setValue(pnd_Work_File_Pnd_Tirf_Account);                                                                                      //Natural: ASSIGN #W-TIRF-ACCOUNT := #TIRF-ACCOUNT
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln1.setValue(pnd_Work_File_Pnd_Tirf_Addr_Ln1);                                                                                    //Natural: ASSIGN #W-TIRF-ADDR-LN1 := #TIRF-ADDR-LN1
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln2.setValue(pnd_Work_File_Pnd_Tirf_Addr_Ln2);                                                                                    //Natural: ASSIGN #W-TIRF-ADDR-LN2 := #TIRF-ADDR-LN2
        pnd_W_Work_File_Pnd_W_Tirf_Addr_Ln3.setValue(pnd_Work_File_Pnd_Tirf_Addr_Ln3);                                                                                    //Natural: ASSIGN #W-TIRF-ADDR-LN3 := #TIRF-ADDR-LN3
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
    private void CheckAtStartofData910() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
            sub_Get_State_Info();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM HEADER-RECORD
            sub_Header_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Old_Company_Cde.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);                                                                               //Natural: ASSIGN #OLD-COMPANY-CDE := #PREV-A-REC-COMPANY := #PREV-C-REC-COMPANY := #TIRF-COMPANY-CDE
            pnd_Prev_A_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);
            pnd_Prev_C_Rec_Company.setValue(pnd_Work_File_Pnd_Tirf_Company_Cde);
                                                                                                                                                                          //Natural: PERFORM CREATE-A-RECORD
            sub_Create_A_Record();
            if (condition(Global.isEscape())) {return;}
            pnd_Out_Ny_Record.setValue(ldaTwrl3502.getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1());                                                                             //Natural: ASSIGN #OUT-NY-RECORD := #OUT-IRS-REC1
            pnd_Save_Outb_Tin.setValue(pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin);                                                                                                //Natural: ASSIGN #SAVE-OUTB-TIN := #OUTW-NY-TIN
            //*  $$$$
        }                                                                                                                                                                 //Natural: END-START
    }
}
