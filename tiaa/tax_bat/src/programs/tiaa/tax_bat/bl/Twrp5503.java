/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:54 PM
**        * FROM NATURAL PROGRAM : Twrp5503
************************************************************
**        * FILE NAME            : Twrp5503.java
**        * CLASS NAME           : Twrp5503
**        * INSTANCE NAME        : Twrp5503
************************************************************
************************************************************************
** PROGRAM : TWRP5503
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: RECONCILIATION REPORT FOR 480.7C
**           (RESIDENCY CODE = '42' OR 'RQ')
** HISTORY.....:
**              USED FOR TAX YEARS >= 2008.
**
** 07/15/2009 - W2 FORM AUTOMATION PROJECT             R. MA
**              PUT IN COMMENTS AND TAG WITH '*W2' WHERE CHANGES ARE
**              NECESSARY.
**              IN THIS PROGRAM, THE W2 DATA HAD BEEN IDENTIFIED AND
**              BLOCKED FOR PUERTO RICO REPORTING.
** 2008 ENHANCEMENTS:
** 12/31/2008 PUERTO RICO FORM 480.7C
** 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
**              'Roll Roth 403 to Roth IRA - Death'
** 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5503 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl0900 ldaTwrl0900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Company_Line;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_S2;
    private DbsField pnd_Ws_Pnd_Taxable_Amt;
    private DbsField pnd_Ws_Pnd_Rollover;
    private DbsField pnd_Ws_Pnd_Tax_Free_Exchange;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Gross_Cv;
    private DbsField pnd_Ws_Pnd_Int_Cv;
    private DbsField pnd_Ws_Pnd_Distrib_Cv;
    private DbsField pnd_Ws_Pnd_Taxable_Cv;
    private DbsField pnd_Ws_Pnd_Ivc_Cv;
    private DbsField pnd_Ws_Pnd_R_Gross_Cv;
    private DbsField pnd_Ws_Pnd_R_Ivc_Cv;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Cntl_Text1;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Gross_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Int_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Distrib_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt_Cv;
    private DbsField pnd_Cntl_Pnd_Roll_Gross_Cv;
    private DbsField pnd_Cntl_Pnd_Roll_Ivc_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Int_Amt;
    private DbsField pnd_Cntl_Pnd_Distrib_Amt;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt;
    private DbsField pnd_Cntl_Pnd_Ivc_Amt;
    private DbsField pnd_Cntl_Pnd_Roll_Gross;
    private DbsField pnd_Cntl_Pnd_Roll_Ivc;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Twrpymnt_Company_Cde_FormOld;
    private DbsField readWork01Twrpymnt_Tax_YearOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Company_Line = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Company_Line", "#COMPANY-LINE", FieldType.STRING, 72);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S2", "#S2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Taxable_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Rollover = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rollover", "#ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tax_Free_Exchange = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Free_Exchange", "#TAX-FREE-EXCHANGE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Cv", "#GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Int_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Int_Cv", "#INT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Distrib_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Distrib_Cv", "#DISTRIB-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Taxable_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Taxable_Cv", "#TAXABLE-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Cv", "#IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_R_Gross_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_R_Gross_Cv", "#R-GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_R_Ivc_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_R_Ivc_Cv", "#R-IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 5));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 19);
        pnd_Cntl_Pnd_Cntl_Text1 = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text1", "#CNTL-TEXT1", FieldType.STRING, 19);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Gross_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt_Cv", "#GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Int_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt_Cv", "#INT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Distrib_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Distrib_Amt_Cv", "#DISTRIB-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Taxable_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt_Cv", "#TAXABLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Ivc_Amt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt_Cv", "#IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Roll_Gross_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Roll_Gross_Cv", "#ROLL-GROSS-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Roll_Ivc_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Roll_Ivc_Cv", "#ROLL-IVC-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupArrayInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS", new DbsArrayController(1, 2));
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Int_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Distrib_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Distrib_Amt", "#DISTRIB-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Taxable_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Ivc_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Roll_Gross = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Roll_Gross", "#ROLL-GROSS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Roll_Ivc = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Roll_Ivc", "#ROLL-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Twrpymnt_Company_Cde_FormOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Company_Cde_Form_OLD", "Twrpymnt_Company_Cde_Form_OLD", 
            FieldType.STRING, 1);
        readWork01Twrpymnt_Tax_YearOld = internalLoopRecord.newFieldInRecord("ReadWork01_Twrpymnt_Tax_Year_OLD", "Twrpymnt_Tax_Year_OLD", FieldType.NUMERIC, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaTwrl0600.initializeValues();
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(5);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("- Tax Free Exchange");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("- Qual. Distrib.");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("480.7C");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(4).setInitialValue("  Designated Roth");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=N)#CNTL.#TRAN-CNT-CV(4)	(AD=N)#CNTL.#TRAN-CNT-CV(5)	(AD=N");
        pnd_Cntl_Pnd_Gross_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#GROSS-AMT-CV(2)	(AD=I)#CNTL.#GROSS-AMT-CV(3)	(AD=N)#CNTL.#GROSS-AMT-CV(4)	(AD=N)#CNTL.#GROSS-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Int_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#INT-AMT-CV(2)	(AD=I)#CNTL.#INT-AMT-CV(3)	(AD=N)#CNTL.#INT-AMT-CV(4)	(AD=N)#CNTL.#INT-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Distrib_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#DISTRIB-AMT-CV(2)	(AD=I)#CNTL.#DISTRIB-AMT-CV(3)	(AD=N)#CNTL.#DISTRIB-AMT-CV(4)	(AD=N)#CNTL.#DISTRIB-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Taxable_Amt_Cv.setInitialAttributeValue("AD=N)#CNTL.#TAXABLE-AMT-CV(2)	(AD=I)#CNTL.#TAXABLE-AMT-CV(3)	(AD=I)#CNTL.#TAXABLE-AMT-CV(4)	(AD=I)#CNTL.#TAXABLE-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Ivc_Amt_Cv.setInitialAttributeValue("AD=I)#CNTL.#IVC-AMT-CV(2)	(AD=I)#CNTL.#IVC-AMT-CV(3)	(AD=N)#CNTL.#IVC-AMT-CV(4)	(AD=N)#CNTL.#IVC-AMT-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Roll_Gross_Cv.setInitialAttributeValue("AD=I)#CNTL.#ROLL-GROSS-CV(2)	(AD=I)#CNTL.#ROLL-GROSS-CV(3)	(AD=N)#CNTL.#ROLL-GROSS-CV(4)	(AD=N)#CNTL.#ROLL-GROSS-CV(5)	(AD=I");
        pnd_Cntl_Pnd_Roll_Ivc_Cv.setInitialAttributeValue("AD=I)#CNTL.#ROLL-IVC-CV(2)	(AD=I)#CNTL.#ROLL-IVC-CV(3)	(AD=N)#CNTL.#ROLL-IVC-CV(4)	(AD=N)#CNTL.#ROLL-IVC-CV(5)	(AD=I");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5503() throws Exception
    {
        super("Twrp5503");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'Summary of Puerto Rico Payment Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / #WS.#COMPANY-LINE //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-CONTROL-RECORD
        sub_Process_Control_Record();
        if (condition(Global.isEscape())) {return;}
        short decideConditionsMet218 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TWRP0600-TAX-YEAR-CCYY;//Natural: VALUE '2011' : '9999'
        if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2011' : '9999"))))
        {
            decideConditionsMet218++;
            ignore();
        }                                                                                                                                                                 //Natural: VALUE '2008' : '2010'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2008' : '2010"))))
        {
            decideConditionsMet218++;
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5553"));                                                                                                        //Natural: FETCH 'TWRP5553'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE '2007'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2007"))))
        {
            decideConditionsMet218++;
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5543"));                                                                                                        //Natural: FETCH 'TWRP5543'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE '2004' : '2006'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2004' : '2006"))))
        {
            decideConditionsMet218++;
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5533"));                                                                                                        //Natural: FETCH 'TWRP5533'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE '2003'
        else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals("2003"))))
        {
            decideConditionsMet218++;
            //*  <= 2002
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5523"));                                                                                                        //Natural: FETCH 'TWRP5523'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            Global.setFetchProgram(DbsUtil.getBlType("TWRP5513"));                                                                                                        //Natural: FETCH 'TWRP5513'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData235();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  BEFORE BREAK PROCESSING
            //*    ACCEPT IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE
            //*      = 'J' OR = 'Q' OR = 'T'
            //*    ACCEPT IF #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR       = '121620002'
            //*      AND     #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR     = 'J8300004'
            //*      AND     #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR     = 'K9110962'
            //*      AND     #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '1'
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                //*  *W2                                EXCLUDE W2 DATA    07/15/2009  RM
                if (condition(((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("7") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M"))  //Natural: IF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE = '7' AND #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'S' OR = 'T' OR = 'E' OR = 'U' OR = 'M'
                    && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")))))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Rollover.reset();                                                                                                                              //Natural: RESET #WS.#ROLLOVER #WS.#TAX-FREE-EXCHANGE
                pnd_Ws_Pnd_Tax_Free_Exchange.reset();
                //*  ROLLOVER
                //*  TAX FREE EXCHANGE
                short decideConditionsMet280 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #XTAXYR-F94.TWRPYMNT-DISTRIBUTION-CDE;//Natural: VALUE 'G', 'H', 'Z', '[', '='
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("G") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("H") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("Z") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("[") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("="))))
                {
                    decideConditionsMet280++;
                    pnd_Ws_Pnd_Rollover.setValue(true);                                                                                                                   //Natural: ASSIGN #WS.#ROLLOVER := TRUE
                }                                                                                                                                                         //Natural: VALUE '6'
                else if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Distribution_Cde().equals("6"))))
                {
                    decideConditionsMet280++;
                    pnd_Ws_Pnd_Tax_Free_Exchange.setValue(true);                                                                                                          //Natural: ASSIGN #WS.#TAX-FREE-EXCHANGE := TRUE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*                                                    ALL PAYMENTS
                pnd_Cntl_Pnd_Tran_Cnt.getValue(1,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 1,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 1,1 )
                short decideConditionsMet292 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ROLLOVER
                if (condition(pnd_Ws_Pnd_Rollover.getBoolean()))
                {
                    decideConditionsMet292++;
                    pnd_Cntl_Pnd_Roll_Gross.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#ROLL-GROSS ( 1,1 )
                    pnd_Cntl_Pnd_Roll_Ivc.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#ROLL-IVC ( 1,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Gross_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 1,1 )
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(1,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 1,1 )
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(! (ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C")))) //Natural: IF NOT #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*                                                    ACTIVE PAYMENTS
                pnd_Cntl_Pnd_Tran_Cnt.getValue(2,1).nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( 2,1 )
                pnd_Cntl_Pnd_Int_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I));                                         //Natural: ADD #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) TO #CNTL.#INT-AMT ( 2,1 )
                short decideConditionsMet306 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ROLLOVER
                if (condition(pnd_Ws_Pnd_Rollover.getBoolean()))
                {
                    decideConditionsMet306++;
                    pnd_Cntl_Pnd_Roll_Gross.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#ROLL-GROSS ( 2,1 )
                    pnd_Cntl_Pnd_Roll_Ivc.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                    //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#ROLL-IVC ( 2,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Cntl_Pnd_Gross_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( 2,1 )
                    pnd_Cntl_Pnd_Ivc_Amt.getValue(2,1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                                     //Natural: ADD #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I ) TO #CNTL.#IVC-AMT ( 2,1 )
                    pnd_Ws_Pnd_Taxable_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Taxable_Amt), ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Int_Amt().getValue(pnd_Ws_Pnd_I)).subtract(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Ivc_Amt().getValue(pnd_Ws_Pnd_I))); //Natural: ASSIGN #WS.#TAXABLE-AMT := #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) + #XTAXYR-F94.TWRPYMNT-INT-AMT ( #I ) - #XTAXYR-F94.TWRPYMNT-IVC-AMT ( #I )
                    pnd_Cntl_Pnd_Taxable_Amt.getValue(2,1).nadd(pnd_Ws_Pnd_Taxable_Amt);                                                                                  //Natural: ADD #WS.#TAXABLE-AMT TO #CNTL.#TAXABLE-AMT ( 2,1 )
                    if (condition(pnd_Ws_Pnd_Tax_Free_Exchange.getBoolean()))                                                                                             //Natural: IF #WS.#TAX-FREE-EXCHANGE
                    {
                        pnd_Cntl_Pnd_Taxable_Amt.getValue(3,1).nadd(pnd_Ws_Pnd_Taxable_Amt);                                                                              //Natural: ADD #WS.#TAXABLE-AMT TO #CNTL.#TAXABLE-AMT ( 3,1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Roth_Qual_Ind().getValue(pnd_Ws_Pnd_I).equals("Y")))                                         //Natural: IF TWRPYMNT-ROTH-QUAL-IND ( #I ) = 'Y'
                        {
                            pnd_Cntl_Pnd_Taxable_Amt.getValue(4,1).nadd(pnd_Ws_Pnd_Taxable_Amt);                                                                          //Natural: ADD #WS.#TAXABLE-AMT TO #CNTL.#TAXABLE-AMT ( 4,1 )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Twrpymnt_Company_Cde_FormOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form());                                                   //Natural: END-WORK
            readWork01Twrpymnt_Tax_YearOld.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Company_Line.setValue("Grand Totals");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Grand Totals'
        pnd_Ws_Pnd_S2.setValue(2);                                                                                                                                        //Natural: ASSIGN #WS.#S2 := 2
                                                                                                                                                                          //Natural: PERFORM WRITE-PUERTO-RICO-REPORT
        sub_Write_Puerto_Rico_Report();
        if (condition(Global.isEscape())) {return;}
        //* **********************
        //*  S U B R O U T I N E S
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PUERTO-RICO-REPORT
        //* *****************************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMPANY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-CONTROL-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_Puerto_Rico_Report() throws Exception                                                                                                          //Natural: WRITE-PUERTO-RICO-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Cntl_Pnd_Tran_Cnt.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue(2,pnd_Ws_Pnd_S2));                                                            //Natural: ADD #CNTL.#TRAN-CNT ( 2,#S2 ) TO #CNTL.#TRAN-CNT ( 5,#S2 )
        pnd_Cntl_Pnd_Gross_Amt.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                          //Natural: ADD #CNTL.#GROSS-AMT ( 2,#S2 ) TO #CNTL.#GROSS-AMT ( 5,#S2 )
        pnd_Cntl_Pnd_Int_Amt.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                              //Natural: ADD #CNTL.#INT-AMT ( 2,#S2 ) TO #CNTL.#INT-AMT ( 5,#S2 )
        pnd_Cntl_Pnd_Distrib_Amt.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Distrib_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                      //Natural: ADD #CNTL.#DISTRIB-AMT ( 2,#S2 ) TO #CNTL.#DISTRIB-AMT ( 5,#S2 )
        pnd_Cntl_Pnd_Taxable_Amt.getValue(5,pnd_Ws_Pnd_S2).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Taxable_Amt.getValue(5,pnd_Ws_Pnd_S2)), pnd_Cntl_Pnd_Taxable_Amt.getValue(2, //Natural: ASSIGN #CNTL.#TAXABLE-AMT ( 5,#S2 ) := #CNTL.#TAXABLE-AMT ( 2,#S2 ) - #CNTL.#TAXABLE-AMT ( 3,#S2 ) - #CNTL.#TAXABLE-AMT ( 4,#S2 )
            pnd_Ws_Pnd_S2).subtract(pnd_Cntl_Pnd_Taxable_Amt.getValue(3,pnd_Ws_Pnd_S2)).subtract(pnd_Cntl_Pnd_Taxable_Amt.getValue(4,pnd_Ws_Pnd_S2)));
        pnd_Cntl_Pnd_Ivc_Amt.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Ivc_Amt.getValue(2,pnd_Ws_Pnd_S2));                                                              //Natural: ADD #CNTL.#IVC-AMT ( 2,#S2 ) TO #CNTL.#IVC-AMT ( 5,#S2 )
        pnd_Cntl_Pnd_Roll_Gross.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Roll_Gross.getValue(2,pnd_Ws_Pnd_S2));                                                        //Natural: ADD #CNTL.#ROLL-GROSS ( 2,#S2 ) TO #CNTL.#ROLL-GROSS ( 5,#S2 )
        pnd_Cntl_Pnd_Roll_Ivc.getValue(5,pnd_Ws_Pnd_S2).nadd(pnd_Cntl_Pnd_Roll_Ivc.getValue(2,pnd_Ws_Pnd_S2));                                                            //Natural: ADD #CNTL.#ROLL-IVC ( 2,#S2 ) TO #CNTL.#ROLL-IVC ( 5,#S2 )
        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Gross_Cv.setValue(pnd_Cntl_Pnd_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                              //Natural: ASSIGN #WS.#GROSS-CV := #CNTL.#GROSS-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Int_Cv.setValue(pnd_Cntl_Pnd_Int_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#INT-CV := #CNTL.#INT-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Distrib_Cv.setValue(pnd_Cntl_Pnd_Distrib_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#DISTRIB-CV := #CNTL.#DISTRIB-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Taxable_Cv.setValue(pnd_Cntl_Pnd_Taxable_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                          //Natural: ASSIGN #WS.#TAXABLE-CV := #CNTL.#TAXABLE-AMT-CV ( #S1 )
            pnd_Ws_Pnd_Ivc_Cv.setValue(pnd_Cntl_Pnd_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                  //Natural: ASSIGN #WS.#IVC-CV := #CNTL.#IVC-AMT-CV ( #S1 )
            pnd_Ws_Pnd_R_Gross_Cv.setValue(pnd_Cntl_Pnd_Roll_Gross_Cv.getValue(pnd_Ws_Pnd_S1));                                                                           //Natural: ASSIGN #WS.#R-GROSS-CV := #CNTL.#ROLL-GROSS-CV ( #S1 )
            pnd_Ws_Pnd_R_Ivc_Cv.setValue(pnd_Cntl_Pnd_Roll_Ivc_Cv.getValue(pnd_Ws_Pnd_S1));                                                                               //Natural: ASSIGN #WS.#R-IVC-CV := #CNTL.#ROLL-IVC-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) / '/' #CNTL-TEXT1 ( #S1 ) '/Trans Count' #CNTL.#TRAN-CNT ( #S1,#S2 ) ( CV = #TRAN-CV ) '/Gross Amount' #CNTL.#GROSS-AMT ( #S1,#S2 ) ( CV = #GROSS-CV ) 3X '/Interest' #CNTL.#INT-AMT ( #S1,#S2 ) ( CV = #INT-CV ) 'Distribution Amount' #CNTL.#DISTRIB-AMT ( #S1,#S2 ) ( CV = #DISTRIB-CV ) / 'Rollover Amount' #CNTL.#ROLL-GROSS ( #S1,#S2 ) ( CV = #R-GROSS-CV ) 'Taxable Amount' #CNTL.#TAXABLE-AMT ( #S1,#S2 ) ( CV = #TAXABLE-CV ) 3X 'IVC Amount' #CNTL.#IVC-AMT ( #S1,#S2 ) ( CV = #IVC-CV ) / 'Rollover IVC' #CNTL.#ROLL-IVC ( #S1,#S2 ) ( CV = #R-IVC-CV )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),NEWLINE,"/",
            		pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1),"/Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Tran_Cv,"/Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"/Interest",
            		pnd_Cntl_Pnd_Int_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Int_Cv,"Distribution Amount",
            		pnd_Cntl_Pnd_Distrib_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Distrib_Cv,NEWLINE,"Rollover Amount",
            		pnd_Cntl_Pnd_Roll_Gross.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_R_Gross_Cv,"Taxable Amount",
            		pnd_Cntl_Pnd_Taxable_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Taxable_Cv,new ColumnSpacing(3),"IVC Amount",
            		pnd_Cntl_Pnd_Ivc_Amt.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_Ivc_Cv,NEWLINE,"Rollover IVC",
            		pnd_Cntl_Pnd_Roll_Ivc.getValue(pnd_Ws_Pnd_S1,pnd_Ws_Pnd_S2), pnd_Ws_Pnd_R_Ivc_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Company() throws Exception                                                                                                                   //Natural: PROCESS-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Control_Record() throws Exception                                                                                                            //Natural: PROCESS-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(3, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 03 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Control Record is Empty",new TabSetting(77),"***");                                      //Natural: WRITE ( 1 ) '***' 25T 'Control Record is Empty' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_Form().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde_FormIsBreak))
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(readWork01Twrpymnt_Company_Cde_FormOld);                                                                 //Natural: ASSIGN #TWRACOMP.#COMP-CODE := OLD ( #XTAXYR-F94.TWRPYMNT-COMPANY-CDE-FORM )
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().setValue(readWork01Twrpymnt_Tax_YearOld);                                                                          //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := OLD ( #XTAXYR-F94.TWRPYMNT-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMPANY
            sub_Process_Company();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Company_Line.setValue("Company:");                                                                                                                 //Natural: ASSIGN #WS.#COMPANY-LINE := 'Company:'
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),pnd_Ws_Pnd_Company_Line,11,4);                                                          //Natural: MOVE #TWRACOMP.#COMP-SHORT-NAME TO SUBSTR ( #WS.#COMPANY-LINE,11,4 )
            setValueToSubstring("-",pnd_Ws_Pnd_Company_Line,16,1);                                                                                                        //Natural: MOVE '-' TO SUBSTR ( #WS.#COMPANY-LINE,16,1 )
            setValueToSubstring(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Name(),pnd_Ws_Pnd_Company_Line,18,55);                                                               //Natural: MOVE #TWRACOMP.#COMP-NAME TO SUBSTR ( #WS.#COMPANY-LINE,18,55 )
            pnd_Cntl_Pnd_Tran_Cnt.getValue("*",2).nadd(pnd_Cntl_Pnd_Tran_Cnt.getValue("*",1));                                                                            //Natural: ADD #CNTL.#TRAN-CNT ( *,1 ) TO #CNTL.#TRAN-CNT ( *,2 )
            pnd_Cntl_Pnd_Gross_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Gross_Amt.getValue("*",1));                                                                          //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) TO #CNTL.#GROSS-AMT ( *,2 )
            pnd_Cntl_Pnd_Int_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Int_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#INT-AMT ( *,1 ) TO #CNTL.#INT-AMT ( *,2 )
            pnd_Cntl_Pnd_Distrib_Amt.getValue("*",1).compute(new ComputeParameters(false, pnd_Cntl_Pnd_Distrib_Amt.getValue("*",1)), pnd_Cntl_Pnd_Gross_Amt.getValue("*", //Natural: ADD #CNTL.#GROSS-AMT ( *,1 ) #CNTL.#INT-AMT ( *,1 ) GIVING #CNTL.#DISTRIB-AMT ( *,1 )
                1).add(pnd_Cntl_Pnd_Int_Amt.getValue("*",1)));
            pnd_Cntl_Pnd_Distrib_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Distrib_Amt.getValue("*",1));                                                                      //Natural: ADD #CNTL.#DISTRIB-AMT ( *,1 ) TO #CNTL.#DISTRIB-AMT ( *,2 )
            pnd_Cntl_Pnd_Taxable_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Taxable_Amt.getValue("*",1));                                                                      //Natural: ADD #CNTL.#TAXABLE-AMT ( *,1 ) TO #CNTL.#TAXABLE-AMT ( *,2 )
            pnd_Cntl_Pnd_Ivc_Amt.getValue("*",2).nadd(pnd_Cntl_Pnd_Ivc_Amt.getValue("*",1));                                                                              //Natural: ADD #CNTL.#IVC-AMT ( *,1 ) TO #CNTL.#IVC-AMT ( *,2 )
            pnd_Cntl_Pnd_Roll_Gross.getValue("*",2).nadd(pnd_Cntl_Pnd_Roll_Gross.getValue("*",1));                                                                        //Natural: ADD #CNTL.#ROLL-GROSS ( *,1 ) TO #CNTL.#ROLL-GROSS ( *,2 )
            pnd_Cntl_Pnd_Roll_Ivc.getValue("*",2).nadd(pnd_Cntl_Pnd_Roll_Ivc.getValue("*",1));                                                                            //Natural: ADD #CNTL.#ROLL-IVC ( *,1 ) TO #CNTL.#ROLL-IVC ( *,2 )
            pnd_Ws_Pnd_S2.setValue(1);                                                                                                                                    //Natural: ASSIGN #WS.#S2 := 1
                                                                                                                                                                          //Natural: PERFORM WRITE-PUERTO-RICO-REPORT
            sub_Write_Puerto_Rico_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cntl_Pnd_Totals.getValue("*",1).reset();                                                                                                                  //Natural: RESET #CNTL.#TOTALS ( *,1 )
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(45),"Summary of Puerto Rico Payment Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,pnd_Ws_Pnd_Company_Line,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,NEWLINE,"/",
        		pnd_Cntl_Pnd_Cntl_Text1,"/Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"/Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt, pnd_Ws_Pnd_Gross_Cv,new ColumnSpacing(3),"/Interest",
        		pnd_Cntl_Pnd_Int_Amt, pnd_Ws_Pnd_Int_Cv,"Distribution Amount",
        		pnd_Cntl_Pnd_Distrib_Amt, pnd_Ws_Pnd_Distrib_Cv,NEWLINE,"Rollover Amount",
        		pnd_Cntl_Pnd_Roll_Gross, pnd_Ws_Pnd_R_Gross_Cv,"Taxable Amount",
        		pnd_Cntl_Pnd_Taxable_Amt, pnd_Ws_Pnd_Taxable_Cv,new ColumnSpacing(3),"IVC Amount",
        		pnd_Cntl_Pnd_Ivc_Amt, pnd_Ws_Pnd_Ivc_Cv,NEWLINE,"Rollover IVC",
        		pnd_Cntl_Pnd_Roll_Ivc, pnd_Ws_Pnd_R_Ivc_Cv);
    }
    private void CheckAtStartofData235() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
            //*  BK
        }                                                                                                                                                                 //Natural: END-START
    }
}
