/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:26 PM
**        * FROM NATURAL PROGRAM : Twrp5001
************************************************************
**        * FILE NAME            : Twrp5001.java
**        * CLASS NAME           : Twrp5001
**        * INSTANCE NAME        : Twrp5001
************************************************************
************************************************************************
** PROGRAM : TWRP5001
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: BATCH FORM ROLLUP FOR PAYMENT LOADS AFTER MASS MAILING.
**
**           CHECK FOR CODE MARKED WITH !!!!
**           THIS IS THE CODE TO ENABLE END OF YEAR
**           FORM TEST FOR THE CURRENT YEAR!
**
** HISTORY.....:
**    12/30/2014 FE FATCA CHANGES. RESTOW TO PICKUP FIELDS IN TWRA5006
**    03/18/13 - R CARREON ADDED SOURCE CODE 'EW' GENERIC WRRNTS /* RC01
**    08/13/2012 MS SUNY CHANGES.
**    06/04/12 - R CARREON - ADDED SOURCE CODE VL - RC0612
**    01/13/12 - R CARREON - REPLACED TWRL810A BY TWRL0702. SUNY/CUNY
**    09/28/11 - M BERLIN - ADDED MCCAMISH SOURCE CODE 'AM' MODELLED
**                          FROM 'NV'                    /* 09/28/11
**    05/07/08 - ROSE MA - ROTH 401K/403B PROJECT
**                         RECOMPILED DUE TO UPDATED TWRL0700.
**    10/08/2003 MS 480.6A FOR ALL COMPANIES STARTING WITH 2003.
**    09/05/2002 MS MODIFIED TO ROLLUP TCII AND CREF FOR PUERTO RICO.
**    11/02/2001 MS MODIFIED FOR 2001 REPORTING.
* 08/2016  F.ENDAYA    COR/NAS SUNSET. FE201608
**
* 4/7/2017 - WEBBJ - PIN EXPANSION. RESTOW ONLY
* 10/19/2017 DASDH RESTOW FOR 5498 CHANGES
**
** 11/24/2020 - PALDE - RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5001 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaTwrapart pdaTwrapart;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwra5000 pdaTwra5000;
    private PdaTwra5006 pdaTwra5006;
    private PdaTwra5008 pdaTwra5008;
    private LdaTwrl5000 ldaTwrl5000;
    private LdaTwrl5009 ldaTwrl5009;
    private LdaTwrl9415 ldaTwrl9415;
    private LdaTwrl9501 ldaTwrl9501;
    private LdaTwrl0702 ldaTwrl0702;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Isn;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Lib;
    private DbsField pnd_Ws_Pnd_Part_Read;
    private DbsField pnd_Ws_Pnd_Part_Processed;
    private DbsField pnd_Ws_Pnd_Pymnt_Read;
    private DbsField pnd_Ws_Pnd_Pymnt_Processed;
    private DbsField pnd_Ws_Pnd_Start_Tax_Year;
    private DbsField pnd_Ws_Pnd_Prior_Tax_Year;
    private DbsField pnd_Ws_Pnd_Ok_To_Rollup;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Terminate_Msg;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTwrapart = new PdaTwrapart(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwra5000 = new PdaTwra5000(localVariables);
        pdaTwra5006 = new PdaTwra5006(localVariables);
        pdaTwra5008 = new PdaTwra5008(localVariables);
        ldaTwrl5000 = new LdaTwrl5000();
        registerRecord(ldaTwrl5000);
        ldaTwrl5009 = new LdaTwrl5009();
        registerRecord(ldaTwrl5009);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());
        ldaTwrl9501 = new LdaTwrl9501();
        registerRecord(ldaTwrl9501);
        registerRecord(ldaTwrl9501.getVw_twrparti_Part());
        ldaTwrl0702 = new LdaTwrl0702();
        registerRecord(ldaTwrl0702);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Isn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Lib = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lib", "#LIB", FieldType.STRING, 4);
        pnd_Ws_Pnd_Part_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Part_Read", "#PART-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Part_Processed = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Part_Processed", "#PART-PROCESSED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Read", "#PYMNT-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt_Processed = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Processed", "#PYMNT-PROCESSED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Start_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Start_Tax_Year", "#START-TAX-YEAR", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Prior_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prior_Tax_Year", "#PRIOR-TAX-YEAR", FieldType.PACKED_DECIMAL, 4);
        pnd_Ws_Pnd_Ok_To_Rollup = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ok_To_Rollup", "#OK-TO-ROLLUP", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            10));
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Terminate_Msg = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 38);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl5000.initializeValues();
        ldaTwrl5009.initializeValues();
        ldaTwrl9415.initializeValues();
        ldaTwrl9501.initializeValues();
        ldaTwrl0702.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5001() throws Exception
    {
        super("Twrp5001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  $$$$
        //*  #TWRA5000.#DEBUG-IND := TRUE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
            //*  FE201608
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
            sub_Open_Mq();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                          //Natural: IF #TWRA5000.#DEBUG-IND
        {
            getReports().write(0, "Starting Program:",Global.getPROGRAM(),"- Payment Batch load after mass mailing");                                                     //Natural: WRITE ( 0 ) 'Starting Program:' *PROGRAM '- Payment Batch load after mass mailing'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM HOUSEKEEPING
        sub_Housekeeping();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").equals(true))))                                                                     //Natural: IF NOT #TWRA5000.#OK-TO-ROLLUP ( * )
        {
            getReports().skip(1, 4);                                                                                                                                      //Natural: SKIP ( 1 ) 4 LINES
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Before Mass Mailing. Forms will not be generated!");                                            //Natural: WRITE ( 1 ) 7T 'Before Mass Mailing. Forms will not be generated!'
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-PARTICIPANT-FILE
        sub_Process_Participant_File();
        if (condition(Global.isEscape())) {return;}
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3 LINES
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Participant records read.....:",pnd_Ws_Pnd_Part_Read, new ReportEditMask                    //Natural: WRITE ( 1 ) / 7T 'Participant records read.....:' #WS.#PART-READ / 7T 'Participant records processed:' #WS.#PART-PROCESSED
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Participant records processed:",pnd_Ws_Pnd_Part_Processed, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  5498
        pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(4).reset();                                                                                               //Natural: RESET #TWRA5000.#OK-TO-ROLLUP ( 4 )
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").getBoolean()))                                                                         //Natural: IF #TWRA5000.#OK-TO-ROLLUP ( * )
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-PAYMENT-FILE
            sub_Process_Payment_File();
            if (condition(Global.isEscape())) {return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Payment     records read.....:",pnd_Ws_Pnd_Pymnt_Read, new ReportEditMask               //Natural: WRITE ( 1 ) / 7T 'Payment     records read.....:' #WS.#PYMNT-READ / 7T 'Payment     records processed:' #WS.#PYMNT-PROCESSED
                ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"Payment     records processed:",pnd_Ws_Pnd_Pymnt_Processed, new ReportEditMask ("-Z,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().skip(1, 4);                                                                                                                                      //Natural: SKIP ( 1 ) 4 LINES
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"Before Mass Mailing. 'Payment' Forms will not be generated!");                                  //Natural: WRITE ( 1 ) 7T 'Before Mass Mailing. "Payment" Forms will not be generated!'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTROL REPUPT
        DbsUtil.callnat(Twrn5006.class , getCurrentProcessState(), pdaTwra5000.getPnd_Twra5000(), pdaTwra5006.getPnd_Twra5006());                                         //Natural: CALLNAT 'TWRN5006' USING #TWRA5000 #TWRA5006
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Terminate.notEquals(getZero())))                                                                                                         //Natural: IF #WS.#TERMINATE NE 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(pnd_Ws_Pnd_Terminate);  if (true) return;                                                                                                   //Natural: TERMINATE #WS.#TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************
        //*  S U B R O U T I N E S
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PARTICIPANT-FILE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PAYMENT-FILE
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-FORMS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HOUSEKEEPING
        //* *****************************
        //*  !!!!
        //*  #TWRA5000.#TAX-YEAR                := *DATN / 10000
        //*  !!!!
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TRAN
        //* *****************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-PROCESSING
        //*  ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  *****************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  CLOSE MQ FE201608 END
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0012'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Participant_File() throws Exception                                                                                                          //Natural: PROCESS-PARTICIPANT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                          //Natural: IF #TWRA5000.#DEBUG-IND
        {
            getReports().write(0, Global.getPROGRAM(),"Participant form generation");                                                                                     //Natural: WRITE ( 0 ) *PROGRAM 'Participant form generation'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra5000.getPnd_Twra5000_Pnd_Process_Ind().setValue("9");                                                                                                      //Natural: ASSIGN #TWRA5000.#PROCESS-IND := '9'
        //*   WORK FILE 2 TWRT-RECORD #WS.#ISN
        //*  RCC
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(2, ldaTwrl0702.getTwrt_Record())))
        {
            CheckAtStartofData825();

            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                      //Natural: AT START OF DATA;//Natural: IF #TWRA5000.#DEBUG-IND
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRAN
                sub_Display_Tran();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  RCC
            pnd_Ws_Pnd_Part_Read.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS.#PART-READ
            pnd_Ws_Pnd_Isn.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Isn());                                                                                               //Natural: ASSIGN #WS.#ISN := TWRT-ISN
            if (condition(pnd_Ws_Pnd_Isn.equals(getZero())))                                                                                                              //Natural: IF #WS.#ISN = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            G_PART:                                                                                                                                                       //Natural: GET TWRPARTI-PART #WS.#ISN
            ldaTwrl9501.getVw_twrparti_Part().readByID(pnd_Ws_Pnd_Isn.getLong(), "G_PART");
            if (condition(ldaTwrl9501.getTwrparti_Part_Twrparti_Status().notEquals(" ")))                                                                                 //Natural: IF TWRPARTI-PART.TWRPARTI-STATUS NE ' '
            {
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                  //Natural: IF #TWRA5000.#DEBUG-IND
                {
                    getReports().write(0, Global.getPROGRAM(),"Not an active participant record.");                                                                       //Natural: WRITE ( 0 ) *PROGRAM 'Not an active participant record.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9501.getTwrparti_Part_Twrparti_Part_Eff_End_Dte().notEquals("99999999")))                                                                //Natural: IF TWRPARTI-PART.TWRPARTI-PART-EFF-END-DTE NE '99999999'
            {
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                  //Natural: IF #TWRA5000.#DEBUG-IND
                {
                    getReports().write(0, Global.getPROGRAM(),"End Date is not 99999999.");                                                                               //Natural: WRITE ( 0 ) *PROGRAM 'End Date is not 99999999.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Start_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Start_Tax_Year), ldaTwrl9501.getTwrparti_Part_Twrparti_Part_Eff_Start_Dte().val().divide(10000)); //Natural: ASSIGN #WS.#START-TAX-YEAR := VAL ( TWRPARTI-PART.TWRPARTI-PART-EFF-START-DTE ) / 10000
            if (condition(pnd_Ws_Pnd_Start_Tax_Year.greater(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year())))                                                                 //Natural: IF #WS.#START-TAX-YEAR > #TWRA5000.#TAX-YEAR
            {
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                  //Natural: IF #TWRA5000.#DEBUG-IND
                {
                    getReports().write(0, Global.getPROGRAM(),"Current Year changes.");                                                                                   //Natural: WRITE ( 0 ) *PROGRAM 'Current Year changes.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pdaTwrapart.getPnd_Twrapart_Pnd_Tin().setValue(ldaTwrl9501.getTwrparti_Part_Twrparti_Tax_Id());                                                               //Natural: ASSIGN #TWRAPART.#TIN := TWRPARTI-PART.TWRPARTI-TAX-ID
            pdaTwrapart.getPnd_Twrapart_Pnd_Contract_Nbr().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr());                                                        //Natural: ASSIGN #TWRAPART.#CONTRACT-NBR := TWRT-RECORD.TWRT-CNTRCT-NBR
            pdaTwrapart.getPnd_Twrapart_Pnd_Payee_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde());                                                            //Natural: ASSIGN #TWRAPART.#PAYEE-CDE := TWRT-RECORD.TWRT-PAYEE-CDE
            //* ***********************************************************************
            //* * COPYCODE   : TWRC5000
            //* * SYSTEM     : TAXWARS
            //* * AUTHOR     : MICHAEL SUPONITSKY
            //* * FUNCTION   : NAME AND ADDRESS LOOKUP DURING FORM ROLLUP.
            //* * HISTORY.....:
            //* *    MM/DD/YYYY - MICHAEL SUPONITSKY - XXXXXXXXXXXXXXXXXXXXX
            //* *
            //* ***********************************************************************
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                      //Natural: IF #TWRA5000.#DEBUG-IND
            {
                getReports().write(0, Global.getPROGRAM(),"Name and Address lookup for TIN:",pdaTwrapart.getPnd_Twrapart_Pnd_Tin());                                      //Natural: WRITE ( 0 ) *PROGRAM 'Name and Address lookup for TIN:' #TWRAPART.#TIN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaTwrapart.getPnd_Twrapart_Pnd_Function().setValue("R");                                                                                                     //Natural: ASSIGN #TWRAPART.#FUNCTION := 'R'
            pdaTwrapart.getPnd_Twrapart_Pnd_Tax_Year().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                              //Natural: ASSIGN #TWRAPART.#TAX-YEAR := #TWRA5000.#TAX-YEAR
            DbsUtil.callnat(Twrnpart.class , getCurrentProcessState(), pdaTwrapart.getPnd_Twrapart());                                                                    //Natural: CALLNAT 'TWRNPART' USING #TWRAPART
            if (condition(Global.isEscape())) return;
            if (condition(! (pdaTwrapart.getPnd_Twrapart_Pnd_Twra5000_Ret_Code().getBoolean())))                                                                          //Natural: IF NOT #TWRAPART.#TWRA5000-RET-CODE
            {
                pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Code().setValue(pdaTwrapart.getPnd_Twrapart_Pnd_Twra5000_Ret_Code().getBoolean());                                    //Natural: ASSIGN #TWRA5000.#RET-CODE := #TWRAPART.#TWRA5000-RET-CODE
                pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Msg().setValue(pdaTwrapart.getPnd_Twrapart_Pnd_Twra5000_Ret_Msg());                                                   //Natural: ASSIGN #TWRA5000.#RET-MSG := #TWRAPART.#TWRA5000-RET-MSG
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2011) && pdaTwrapart.getPnd_Twrapart_Pnd_On_Cor().getBoolean() &&                     //Natural: IF #TWRA5000.#TAX-YEAR GE 2011 AND #TWRAPART.#ON-COR AND #TWRAPART.#ON-CNAD
                pdaTwrapart.getPnd_Twrapart_Pnd_On_Cnad().getBoolean()))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwrapart.getPnd_Twrapart_Pin().reset();                                                                                                                //Natural: RESET #TWRAPART.PIN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                      //Natural: IF #TWRA5000.#DEBUG-IND
            {
                getReports().write(0, Global.getPROGRAM(),"After Name and Address lookup","Part info found:",pdaTwrapart.getPnd_Twrapart_Pnd_Ret_Code(),                  //Natural: WRITE ( 0 ) *PROGRAM 'After Name and Address lookup' 'Part info found:' #TWRAPART.#RET-CODE ( EM = N/Y ) 'Return Msg:' #TWRAPART.#RET-MSG / 'On COR:' #TWRAPART.#ON-COR ( EM = N/Y ) 'On CNAD:' #TWRAPART.#ON-CNAD ( EM = N/Y ) 'On Part:' #TWRAPART.#ON-PART ( EM = N/Y )
                    new ReportEditMask ("N/Y"),"Return Msg:",pdaTwrapart.getPnd_Twrapart_Pnd_Ret_Msg(),NEWLINE,"On COR:",pdaTwrapart.getPnd_Twrapart_Pnd_On_Cor(), 
                    new ReportEditMask ("N/Y"),"On CNAD:",pdaTwrapart.getPnd_Twrapart_Pnd_On_Cnad(), new ReportEditMask ("N/Y"),"On Part:",pdaTwrapart.getPnd_Twrapart_Pnd_On_Part(), 
                    new ReportEditMask ("N/Y"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwrapart.getPnd_Twrapart_Pnd_On_Cnad().getBoolean()))                                                                                        //Natural: IF #TWRAPART.#ON-CNAD
            {
                if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                  //Natural: IF #TWRA5000.#DEBUG-IND
                {
                    getReports().write(0, Global.getPROGRAM(),"Participant is on Corporate Name and Address.");                                                           //Natural: WRITE ( 0 ) *PROGRAM 'Participant is on Corporate Name and Address.'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Part_Processed.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WS.#PART-PROCESSED
            pdaTwra5008.getPnd_Twra5008_Pnd_Ok_To_Regen().getValue(0 + 1).setValue(true);                                                                                 //Natural: ASSIGN #TWRA5008.#OK-TO-REGEN ( 0 ) := TRUE
            pdaTwra5008.getPnd_Twra5008_Pnd_Ok_To_Regen().getValue(1 + 1,":",10 + 1).setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").getBoolean()); //Natural: ASSIGN #TWRA5008.#OK-TO-REGEN ( 1:10 ) := #TWRA5000.#OK-TO-ROLLUP ( * )
            pdaTwra5008.getPnd_Twra5008_Pnd_Tax_Year().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                              //Natural: ASSIGN #TWRA5008.#TAX-YEAR := #TWRA5000.#TAX-YEAR
            pdaTwra5008.getPnd_Twra5008_Pnd_Tin().setValue(ldaTwrl9501.getTwrparti_Part_Twrparti_Tax_Id());                                                               //Natural: ASSIGN #TWRA5008.#TIN := TWRPARTI-PART.TWRPARTI-TAX-ID
                                                                                                                                                                          //Natural: PERFORM GEN-FORMS
            sub_Gen_Forms();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Part_Read.equals(getZero())))                                                                                                            //Natural: IF #WS.#PART-READ = 0
        {
            pnd_Ws_Pnd_Terminate_Msg.setValue("Participant transaction file is empty!");                                                                                  //Natural: ASSIGN #WS.#TERMINATE-MSG := 'Participant transaction file is empty!'
            //*  #WS.#TERMINATE      := 101
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
            //*  TERMINATE #WS.#TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Payment_File() throws Exception                                                                                                              //Natural: PROCESS-PAYMENT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                          //Natural: IF #TWRA5000.#DEBUG-IND
        {
            getReports().write(0, Global.getPROGRAM(),"Payment form generation");                                                                                         //Natural: WRITE ( 0 ) *PROGRAM 'Payment form generation'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra5000.getPnd_Twra5000_Pnd_Process_Ind().setValue("7");                                                                                                      //Natural: ASSIGN #TWRA5000.#PROCESS-IND := '7'
        //*  D WORK FILE 3 TWRT-RECORD #WS.#ISN
        //*  RCC
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 RECORD TWRT-RECORD
        while (condition(getWorkFiles().read(3, ldaTwrl0702.getTwrt_Record())))
        {
            CheckAtStartofData925();

            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean()))                                                                                      //Natural: AT START OF DATA;//Natural: IF #TWRA5000.#DEBUG-IND
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRAN
                sub_Display_Tran();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  RCC
            pnd_Ws_Pnd_Pymnt_Read.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WS.#PYMNT-READ
            pnd_Ws_Pnd_Isn.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Isn());                                                                                               //Natural: ASSIGN #WS.#ISN := TWRT-ISN
            if (condition(pnd_Ws_Pnd_Isn.equals(getZero())))                                                                                                              //Natural: IF #WS.#ISN = 0
            {
                pnd_Ws_Pnd_Terminate_Msg.setValue("Payment ISN is missing");                                                                                              //Natural: ASSIGN #WS.#TERMINATE-MSG := 'Payment ISN is missing'
                pnd_Ws_Pnd_Terminate.setValue(104);                                                                                                                       //Natural: ASSIGN #WS.#TERMINATE := 104
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRAN
                sub_Display_Tran();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").reset();                                                                                         //Natural: RESET #TWRA5000.#OK-TO-ROLLUP ( * )
            pnd_Ws_Pnd_Pymnt_Processed.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WS.#PYMNT-PROCESSED
            G_PYMNT:                                                                                                                                                      //Natural: GET PAY #WS.#ISN
            ldaTwrl9415.getVw_pay().readByID(pnd_Ws_Pnd_Isn.getLong(), "G_PYMNT");
            //*  1099-R, 1099-INT
            //*  1042-S
            //*  PUERTO RICO
            //*  480.6A, 480.6B
            //*  1099-R
            //*  SUNY LETTER
            short decideConditionsMet956 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN PAY.TWRPYMNT-TAX-CITIZENSHIP = 'U' AND #WS.#OK-TO-ROLLUP ( 1:2 )
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship().equals("U") && pnd_Ws_Pnd_Ok_To_Rollup.getValue(1,":",2).equals(true)))
            {
                decideConditionsMet956++;
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(1,":",2).setValue(true);                                                                          //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 1:2 ) := TRUE
            }                                                                                                                                                             //Natural: WHEN PAY.TWRPYMNT-TAX-CITIZENSHIP = 'F' AND #WS.#OK-TO-ROLLUP ( 3 )
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship().equals("F") && pnd_Ws_Pnd_Ok_To_Rollup.getValue(3).equals(true)))
            {
                decideConditionsMet956++;
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(3).setValue(true);                                                                                //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 3 ) := TRUE
            }                                                                                                                                                             //Natural: WHEN PAY.TWRPYMNT-RESIDENCY-TYPE = '3' AND #WS.#OK-TO-ROLLUP ( 5:6 )
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Residency_Type().equals("3") && pnd_Ws_Pnd_Ok_To_Rollup.getValue(5,":",6).equals(true)))
            {
                decideConditionsMet956++;
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(5,":",6).setValue(true);                                                                          //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 5:6 ) := TRUE
            }                                                                                                                                                             //Natural: WHEN #TWRA5000.#TAX-YEAR GE 2012 AND #TWRA5000.#OK-TO-ROLLUP ( 1 )
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2012) && pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(1).equals(true)))
            {
                decideConditionsMet956++;
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(10).setValue(true);                                                                               //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 10 ) := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet956 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  NR4
            if (condition(pnd_Ws_Pnd_Ok_To_Rollup.getValue(7).getBoolean()))                                                                                              //Natural: IF #WS.#OK-TO-ROLLUP ( 7 )
            {
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(7).setValue(true);                                                                                //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( 7 ) := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                       //Natural: ASSIGN #TWRA5000.#PYMNT-START-KEY := #TWRA5000.#TAX-YEAR
            setValueToSubstring(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr(),pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),5,10);                                         //Natural: MOVE PAY.TWRPYMNT-TAX-ID-NBR TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,5,10 )
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2012)))                                                                               //Natural: IF #TWRA5000.#TAX-YEAR GE 2012
            {
                pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key());                                              //Natural: ASSIGN #TWRA5000.#PYMNT-END-KEY := #TWRA5000.#PYMNT-START-KEY
                setValueToSubstring(pnd_Ws_Const_Low_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),15,1);                                                      //Natural: MOVE LOW-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,15,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key(),15,1);                                                       //Natural: MOVE HIGH-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-END-KEY ,15,1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr(),pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),15,8);                                   //Natural: MOVE PAY.TWRPYMNT-CONTRACT-NBR TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,15,8 )
                setValueToSubstring(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde(),pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),23,2);                                      //Natural: MOVE PAY.TWRPYMNT-PAYEE-CDE TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,23,2 )
                pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key());                                              //Natural: ASSIGN #TWRA5000.#PYMNT-END-KEY := #TWRA5000.#PYMNT-START-KEY
                setValueToSubstring(pnd_Ws_Const_Low_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_Start_Key(),25,1);                                                      //Natural: MOVE LOW-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-START-KEY,25,1 )
                setValueToSubstring(pnd_Ws_Const_High_Values,pdaTwra5000.getPnd_Twra5000_Pnd_Pymnt_End_Key(),25,1);                                                       //Natural: MOVE HIGH-VALUES TO SUBSTR ( #TWRA5000.#PYMNT-END-KEY ,25,1 )
                pdaTwra5000.getPnd_Twra5000_Twrpymnt_Distribution_Cde().setValue(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde());                                         //Natural: ASSIGN #TWRA5000.TWRPYMNT-DISTRIBUTION-CDE := PAY.TWRPYMNT-DISTRIBUTION-CDE
                pdaTwra5000.getPnd_Twra5000_Twrpymnt_Paymt_Category().setValue(ldaTwrl9415.getPay_Twrpymnt_Paymt_Category());                                             //Natural: ASSIGN #TWRA5000.TWRPYMNT-PAYMT-CATEGORY := PAY.TWRPYMNT-PAYMT-CATEGORY
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GEN-FORMS
            sub_Gen_Forms();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Pymnt_Read.equals(getZero())))                                                                                                           //Natural: IF #WS.#PYMNT-READ = 0
        {
            pnd_Ws_Pnd_Terminate_Msg.setValue("Payment transaction file is empty!");                                                                                      //Natural: ASSIGN #WS.#TERMINATE-MSG := 'Payment transaction file is empty!'
            //*  #WS.#TERMINATE      := 102
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
            //*  TERMINATE #WS.#TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Forms() throws Exception                                                                                                                         //Natural: GEN-FORMS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        DbsUtil.callnat(Twrn5000.class , getCurrentProcessState(), pdaTwrapart.getPnd_Twrapart(), pdaTwra5000.getPnd_Twra5000(), ldaTwrl5009.getPnd_Twrl5009(),           //Natural: CALLNAT 'TWRN5000' USING #TWRAPART #TWRA5000 #TWRL5009 #TWRA5006 #TWRA5008
            pdaTwra5006.getPnd_Twra5006(), pdaTwra5008.getPnd_Twra5008());
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Code().getBoolean()))                                                                                           //Natural: IF #TWRA5000.#RET-CODE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Terminate_Msg.setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Ret_Msg());                                                                                 //Natural: ASSIGN #WS.#TERMINATE-MSG := #TWRA5000.#RET-MSG
            pnd_Ws_Pnd_Terminate.setValue(103);                                                                                                                           //Natural: ASSIGN #WS.#TERMINATE := 103
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Housekeeping() throws Exception                                                                                                                      //Natural: HOUSEKEEPING
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year()), Global.getDATN().divide(10000).subtract(1)); //Natural: ASSIGN #TWRA5000.#TAX-YEAR := *DATN / 10000 - 1
        pnd_Ws_Pnd_Prior_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Prior_Tax_Year), pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().subtract(1));               //Natural: ASSIGN #PRIOR-TAX-YEAR := #TWRA5000.#TAX-YEAR - 1
        //*  D WORK FILE 3 ONCE TWRT-RECORD #WS.#ISN
        //*  RCC
        getWorkFiles().read(3, ldaTwrl0702.getTwrt_Record());                                                                                                             //Natural: READ WORK FILE 3 ONCE RECORD TWRT-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            ignore();
            //* SAIK
            ldaTwrl0702.getTwrt_Record_Twrt_Isn().reset();                                                                                                                //Natural: RESET TWRT-ISN
            //*  RCC
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Ws_Pnd_Isn.setValue(ldaTwrl0702.getTwrt_Record_Twrt_Isn());                                                                                                   //Natural: ASSIGN #WS.#ISN := TWRT-ISN
        //*  09/28/11
        //*  RC01
        if (condition((((((ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("OP") || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("NV")) || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("AM"))  //Natural: IF TWRT-SOURCE = 'OP' OR = 'NV' OR = 'AM' OR = 'VL' OR = 'EW' AND TWRT-RECORD.TWRT-TAX-YEAR = #PRIOR-TAX-YEAR
            || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("VL")) || ldaTwrl0702.getTwrt_Record_Twrt_Source().equals("EW")) && ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year().equals(pnd_Ws_Pnd_Prior_Tax_Year))))
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Prior_Tax_Year);                                                                               //Natural: ASSIGN #TWRA5000.#TAX-YEAR := #PRIOR-TAX-YEAR
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().close(3);                                                                                                                                          //Natural: CLOSE WORK FILE 3
        getReports().write(1, ReportOption.NOTITLE,"Forms rollup - batch load after mass mailing (payment file):",NEWLINE,new TabSetting(7),"Tax Year:",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(),new  //Natural: WRITE ( 1 ) 'Forms rollup - batch load after mass mailing (payment file):' / 7T 'Tax Year:' #TWRA5000.#TAX-YEAR 5X 'S  T  A  T  U  S'
            ColumnSpacing(5),"S  T  A  T  U  S");
        if (Global.isEscape()) return;
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year());                                                                  //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRA5000.#TAX-YEAR
        if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().greaterOrEqual(2012)))                                                                                   //Natural: IF #TWRA5000.#TAX-YEAR GE 2012
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit().setValue(10);                                                                                                    //Natural: ASSIGN #TWRA5000.#FORM-LIMIT := 10
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit().setValue(7);                                                                                                     //Natural: ASSIGN #TWRA5000.#FORM-LIMIT := 7
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #TWRA5000.#FORM-LIMIT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaTwra5000.getPnd_Twra5000_Pnd_Form_Limit())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_I.equals(8) || pnd_Ws_Pnd_I.equals(9)))                                                                                              //Natural: IF #I = 8 OR = 9
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                            //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
            DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
            if (condition(! (pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_First_Time_Forms().getBoolean())))                                                                   //Natural: IF NOT #TWRATBL4.#TIRCNTL-FIRST-TIME-FORMS
            {
                pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                     //Natural: ASSIGN #TWRA5000.#OK-TO-ROLLUP ( #I ) := TRUE
                pdaTwra5000.getPnd_Twra5000_Pnd_First_Time_Rollup().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_First_Time_Forms().getBoolean()); //Natural: ASSIGN #TWRA5000.#FIRST-TIME-ROLLUP ( #I ) := #TIRCNTL-FIRST-TIME-FORMS
                pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Mass_Mailing().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Mass_Mailing().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-MASS-MAILING ( #I ) := #TIRCNTL-MASS-MAILING
                pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_2nd_Mass_Mailing().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_2nd_Mass_Mailing().getBoolean()); //Natural: ASSIGN #TWRA5000.#CNTL-2ND-MASS-MAILING ( #I ) := #TIRCNTL-2ND-MASS-MAILING
                pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Irs_Orig().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig().getBoolean());         //Natural: ASSIGN #TWRA5000.#CNTL-IRS-ORIG ( #I ) := #TIRCNTL-IRS-ORIG
                pdaTwra5000.getPnd_Twra5000_Pnd_Cntl_Irs_Corr().getValue(pnd_Ws_Pnd_I).setValue(pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Corr().getBoolean());         //Natural: ASSIGN #TWRA5000.#CNTL-IRS-CORR ( #I ) := #TIRCNTL-IRS-CORR
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Form....: ",ldaTwrl5000.getPnd_Twrl5000_Pnd_Form_Name().getValue(2 + 1,pnd_Ws_Pnd_I),pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue(pnd_Ws_Pnd_I),  //Natural: WRITE ( 1 ) / 7T 'Form....: ' #TWRL5000.#FORM-NAME ( 2,#I ) #TWRA5000.#OK-TO-ROLLUP ( #I ) ( EM = BEFORE�MASS�MAILING/AFTER�MASS�MAILING )
                new ReportEditMask ("BEFORE MASS MAILING/AFTER MASS MAILING"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Lib.setValue(Global.getLIBRARY_ID());                                                                                                                  //Natural: ASSIGN #WS.#LIB := *LIBRARY-ID
        if (condition(pnd_Ws_Pnd_Lib.equals("PROJ") || pnd_Ws_Pnd_Lib.equals("ACPT")))                                                                                    //Natural: IF #WS.#LIB = 'PROJ' OR = 'ACPT'
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(50);                                                                                                      //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 50
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Et_Limit().setValue(100);                                                                                                     //Natural: ASSIGN #TWRA5000.#ET-LIMIT := 100
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Ok_To_Rollup.getValue("*").setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").getBoolean());                                        //Natural: ASSIGN #WS.#OK-TO-ROLLUP ( * ) := #TWRA5006.#FORMS-TO-REPORT ( 1:10 ) := #TWRA5000.#OK-TO-ROLLUP ( * )
        pdaTwra5006.getPnd_Twra5006_Pnd_Forms_To_Report().getValue(1 + 1,":",10 + 1).setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Ok_To_Rollup().getValue("*").getBoolean());
        pdaTwra5000.getPnd_Twra5000_Pnd_Create_Ts().setValue(Global.getTIMX());                                                                                           //Natural: ASSIGN #TWRA5000.#CREATE-TS := *TIMX
        pdaTwra5000.getPnd_Twra5000_Pnd_Srce_Date().setValue(Global.getDATX());                                                                                           //Natural: ASSIGN #TWRA5000.#SRCE-DATE := *DATX
        pdaTwrapart.getPnd_Twrapart_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRAPART.#ABEND-IND := #TWRAPART.#DISPLAY-IND := FALSE
        pdaTwrapart.getPnd_Twrapart_Pnd_Display_Ind().setValue(false);
        pdaTwrapart.getPnd_Twrapart_Pnd_Debug_Ind().setValue(pdaTwra5000.getPnd_Twra5000_Pnd_Debug_Ind().getBoolean());                                                   //Natural: ASSIGN #TWRAPART.#DEBUG-IND := #TWRA5000.#DEBUG-IND
    }
    private void sub_Display_Tran() throws Exception                                                                                                                      //Natural: DISPLAY-TRAN
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "Srce",                                                                                                                                   //Natural: DISPLAY ( 1 ) 'Srce' TWRT-RECORD.TWRT-SOURCE ( LC = � ) 'YEAR' TWRT-RECORD.TWRT-TAX-YEAR ( EM = 9999 ) 'ISN' #WS.#ISN ( EM = ZZZZZZZ9 HC = R ) 'Tin' TWRT-RECORD.TWRT-TAX-ID ( EM = XXX-XX-XXXX ) 'Contract' TWRT-RECORD.TWRT-CNTRCT-NBR 'Pyee' TWRT-RECORD.TWRT-PAYEE-CDE ( LC = � ) 'Pymnt Dte' TWRT-RECORD.TWRT-PAYMT-DTE-ALPHA ( EM = XXXX/XX/XX ) '/' #WS.#TERMINATE-MSG ( AL = 23 )
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(), new FieldAttributes("LC=�"),"YEAR",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(), new ReportEditMask ("9999"),"ISN",
        		pnd_Ws_Pnd_Isn, new ReportEditMask ("ZZZZZZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Tin",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"Pyee",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(), new FieldAttributes("LC=�"),"Pymnt Dte",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"/",
        		pnd_Ws_Pnd_Terminate_Msg, new AlphanumericLength (23));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Processing() throws Exception                                                                                                                  //Natural: ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),pnd_Ws_Pnd_Terminate_Msg,new TabSetting(77),"***");                                           //Natural: WRITE ( 1 ) '***' 25T #WS.#TERMINATE-MSG 77T '***'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    }
    //*  FE201608 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Srce",
        		ldaTwrl0702.getTwrt_Record_Twrt_Source(), new FieldAttributes("LC=�"),"YEAR",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(), new ReportEditMask ("9999"),"ISN",
        		pnd_Ws_Pnd_Isn, new ReportEditMask ("ZZZZZZZ9"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Tin",
        		ldaTwrl0702.getTwrt_Record_Twrt_Tax_Id(), new ReportEditMask ("XXX-XX-XXXX"),"Contract",
        		ldaTwrl0702.getTwrt_Record_Twrt_Cntrct_Nbr(),"Pyee",
        		ldaTwrl0702.getTwrt_Record_Twrt_Payee_Cde(), new FieldAttributes("LC=�"),"Pymnt Dte",
        		ldaTwrl0702.getTwrt_Record_Twrt_Paymt_Dte_Alpha(), new ReportEditMask ("XXXX/XX/XX"),"/",
        		pnd_Ws_Pnd_Terminate_Msg, new AlphanumericLength (23));
    }
    private void CheckAtStartofData825() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pdaTwra5000.getPnd_Twra5000_Pnd_Srce_Cde().setValue(ldaTwrl0702.getTwrt_Record_Twrt_Source());                                                                //Natural: ASSIGN #TWRA5000.#SRCE-CDE := TWRT-RECORD.TWRT-SOURCE
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2 LINES
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Participant transaction file:",NEWLINE,new TabSetting(7),"Tax Year....:",ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),NEWLINE,new  //Natural: WRITE ( 1 ) / 7T 'Participant transaction file:' / 7T 'Tax Year....:' TWRT-RECORD.TWRT-TAX-YEAR / 7T 'Source......:' 4X TWRT-RECORD.TWRT-SOURCE / 7T 'Process Date:' #TWRA5000.#SRCE-DATE ( EM = MM/DD/YYYY )
                TabSetting(7),"Source......:",new ColumnSpacing(4),ldaTwrl0702.getTwrt_Record_Twrt_Source(),NEWLINE,new TabSetting(7),"Process Date:",pdaTwra5000.getPnd_Twra5000_Pnd_Srce_Date(), 
                new ReportEditMask ("MM/DD/YYYY"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year())))                                              //Natural: IF #TWRA5000.#TAX-YEAR NE TWRT-RECORD.TWRT-TAX-YEAR
            {
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 1 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Participant transaction file is not for",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(),  //Natural: WRITE ( 1 ) / 7T 'Participant transaction file is not for' #TWRA5000.#TAX-YEAR ( SG = OFF ) / 7T 'Forms will not be generated!'
                    new SignPosition (false),NEWLINE,new TabSetting(7),"Forms will not be generated!");
                if (condition(Global.isEscape())) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData925() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2 LINES
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Payment transaction file:",NEWLINE,new TabSetting(7),"Tax Year....:",ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year(),NEWLINE,new  //Natural: WRITE ( 1 ) / 7T 'Payment transaction file:' / 7T 'Tax Year....:' TWRT-RECORD.TWRT-TAX-YEAR / 7T 'Source......:' 4X TWRT-RECORD.TWRT-SOURCE / 7T 'Process Date:' #TWRA5000.#SRCE-DATE ( EM = MM/DD/YYYY )
                TabSetting(7),"Source......:",new ColumnSpacing(4),ldaTwrl0702.getTwrt_Record_Twrt_Source(),NEWLINE,new TabSetting(7),"Process Date:",pdaTwra5000.getPnd_Twra5000_Pnd_Srce_Date(), 
                new ReportEditMask ("MM/DD/YYYY"));
            if (condition(Global.isEscape())) return;
            if (condition(pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year().notEquals(ldaTwrl0702.getTwrt_Record_Twrt_Tax_Year())))                                              //Natural: IF #TWRA5000.#TAX-YEAR NE TWRT-RECORD.TWRT-TAX-YEAR
            {
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 1 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(7),"Payment transaction file is not for",pdaTwra5000.getPnd_Twra5000_Pnd_Tax_Year(),    //Natural: WRITE ( 1 ) / 7T 'Payment transaction file is not for' #TWRA5000.#TAX-YEAR ( SG = OFF ) / 7T 'Forms will not be generated!'
                    new SignPosition (false),NEWLINE,new TabSetting(7),"Forms will not be generated!");
                if (condition(Global.isEscape())) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
