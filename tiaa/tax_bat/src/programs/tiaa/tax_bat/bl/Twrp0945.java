/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:32:04 PM
**        * FROM NATURAL PROGRAM : Twrp0945
************************************************************
**        * FILE NAME            : Twrp0945.java
**        * CLASS NAME           : Twrp0945
**        * INSTANCE NAME        : Twrp0945
************************************************************
* PROGRAM  : TWRP0945
* FUNCTION : PRINTS CANADIAN STATE TRANSACTIONS
* INPUT    : FLAT-FILE1 & 3
* WRITTEN BY : EDITH
* HISTORY
* --------------------------------------------------
*          : TO PROCESS 'APTM' & 'IATM'  3/3/99
*          : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*              IVC-IND = K BUT NOT ROLL-OVER  (EDITH 6/17/99)
*          : NEW DEFINITION OF ROLLOVER BASED ON DISTRIBUTION CODE
*               AND INCLUSION OF NEW CO., TIAA-LIFE (EDITH 10/8/99)
* 12/13/99 : DISTRIBUTION CODE '6' IS NOT ROLLOVER ANYMORE
* 12/14/99 : DISTRIBUTION CODE 'Y' IS NOT ROLLOVER ANYMORE
* 12/15/99 : UPDATED DUE TO DELETION OF IA MAINTENANCE (TMAL,TMIL)
* 04/02/02 : ADDED NEW COMPANY - TCII (J.ROTHOLZ)
* 05/30/02 : ALLOW IVC AMOUNTS IN ROLLOVERS (JH)
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 06/30/03 RM INVESTMENT SOLUTIONS - AUTOMATE SSSS PROJECT
*             NEW SOURCE CODE 'SI' FOR INVESTMENT SOLUTIONS SHOULD BE
*             ADDED FOR THIS PROJECT, BUT NOT IN THIS PROGRAM. THERE IS
*             NO CHANGE IN HERE.
* 10/06/03 RM - 2003 1099 & 5498 SDR
*               DISTRIBUTION CODE 'X' REMOVED.
* 09/20/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODES 'OP', 'NL', 'PL'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           SOURCE CODE 'NV' HAD BEEN ADDED TO ALL CONTROL REPORTS.
*           THIS PROGRAM IS ONE OF THE CONTROL REPORTS BUT DON't need
*           ANY CODE CHANGE THIS TIME.
* 07/19/11  RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*              'Roll Roth 403 to Roth IRA - Death'
*
* 10/18/11  MB - ADDED NEW COMPANY F TO SUBROUTINE CO-DESC /* 10/18/11
* 11/20/14  O SOTTO FATCA CHANGES MARKED 11/2014.
* 07/24/14  PRB69664-ADDED CO-DESC PARAGRAPH TO RESOLVE THE ARRAY
*           INDEX ISSUE IN P1040TWD *ROXAN 06/02*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0945 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_Fed_Amt;
    private DbsField pnd_Counters_Pnd_Nra_Amt;
    private DbsField pnd_Counters_Pnd_Can_Amt;
    private DbsField pnd_Counters_Pnd_Pr_Amt;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Cmpy_Totals;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Cmpny;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Int_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Fed_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Nra_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Can_Amt;
    private DbsField pnd_Cmpy_Totals_Pnd_Tot_Pr_Amt;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Dashes;

    private DbsGroup pnd_Print_Fields;
    private DbsField pnd_Print_Fields_Pnd_Prtfld1;
    private DbsField pnd_Print_Fields_Pnd_Prtfld2;
    private DbsField pnd_Print_Fields_Pnd_Prtfld3;
    private DbsField pnd_Print_Fields_Pnd_Prtfld4;
    private DbsField pnd_Print_Fields_Pnd_Prtfld5;
    private DbsField pnd_Print_Fields_Pnd_Prtfld6;
    private DbsField pnd_Print_Fields_Pnd_Prtfld7;
    private DbsField pnd_Print_Fields_Pnd_Prtfld8;

    private DbsGroup pnd_Print0_Fields;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld9;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld10;

    private DbsGroup pnd_Work_Area;
    private DbsField pnd_Work_Area_Pnd_Rec_Read;
    private DbsField pnd_Work_Area_Pnd_Rec_Process;
    private DbsField pnd_Work_Area_Pnd_Print_Com;
    private DbsField pnd_Work_Area_Pnd_Source;
    private DbsField pnd_Work_Area_Pnd_Sv_Source;
    private DbsField pnd_Work_Area_Pnd_New_Source;
    private DbsField pnd_Work_Area_Pnd_Occ;
    private DbsField pnd_Work_Area_Pnd_J;
    private DbsField pnd_Work_Area_Pnd_K;
    private DbsField pnd_Work_Area_Pnd_First_Rec;
    private DbsField pnd_Work_Area_Pnd_A;
    private DbsField pnd_Work_Area_Pnd_Page;
    private DbsField pnd_Work_Area_Pnd_Sub_Skip;
    private DbsField pnd_Work_Area_Pnd_Sv_Srce12;
    private DbsField pnd_Work_Area_Pnd_Source12;
    private DbsField pnd_Work_Area_Pnd_Source34;

    private DbsGroup pnd_Work_Area_Pnd_Savers;
    private DbsField pnd_Work_Area_Pnd_Sv_Company;
    private DbsField pnd_Work_Area_Pnd_Sv_Srce_Code;
    private DbsField pnd_Work_Area_Pnd_Tot_Prt;
    private DbsField pnd_Work_Area_Pnd_Tax_Amt;
    private DbsField pnd_Work_Area_Pnd_Fatca;
    private DbsField pnd_Work_Area_Pnd_Hdg1;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;
    private DbsField pnd_Var_File1_Pnd_Start_Date;
    private DbsField pnd_Var_File1_Pnd_End_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 4));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Fed_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Amt", "#FED-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Nra_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Amt", "#NRA-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Can_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Amt", "#CAN-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Counters_Pnd_Pr_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Amt", "#PR-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Cmpy_Totals = localVariables.newGroupArrayInRecord("pnd_Cmpy_Totals", "#CMPY-TOTALS", new DbsArrayController(1, 7));
        pnd_Cmpy_Totals_Pnd_Tot_Cmpny = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Cmpny", "#TOT-CMPNY", FieldType.STRING, 4);
        pnd_Cmpy_Totals_Pnd_Tot_Trans_Cnt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", FieldType.NUMERIC, 
            7);
        pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Int_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Int_Amt", "#TOT-INT-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Fed_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Fed_Amt", "#TOT-FED-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Nra_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Nra_Amt", "#TOT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Can_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Can_Amt", "#TOT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Cmpy_Totals_Pnd_Tot_Pr_Amt = pnd_Cmpy_Totals.newFieldInGroup("pnd_Cmpy_Totals_Pnd_Tot_Pr_Amt", "#TOT-PR-AMT", FieldType.PACKED_DECIMAL, 12, 
            2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Dashes = localVariables.newFieldInRecord("pnd_Dashes", "#DASHES", FieldType.STRING, 131);

        pnd_Print_Fields = localVariables.newGroupInRecord("pnd_Print_Fields", "#PRINT-FIELDS");
        pnd_Print_Fields_Pnd_Prtfld1 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld1", "#PRTFLD1", FieldType.STRING, 17);
        pnd_Print_Fields_Pnd_Prtfld2 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld2", "#PRTFLD2", FieldType.STRING, 8);
        pnd_Print_Fields_Pnd_Prtfld3 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld3", "#PRTFLD3", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld4 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld4", "#PRTFLD4", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld5 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld5", "#PRTFLD5", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld6 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld6", "#PRTFLD6", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld7 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld7", "#PRTFLD7", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld8 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld8", "#PRTFLD8", FieldType.STRING, 15);

        pnd_Print0_Fields = localVariables.newGroupInRecord("pnd_Print0_Fields", "#PRINT0-FIELDS");
        pnd_Print0_Fields_Pnd_Prtfld9 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld9", "#PRTFLD9", FieldType.STRING, 15);
        pnd_Print0_Fields_Pnd_Prtfld10 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld10", "#PRTFLD10", FieldType.STRING, 15);

        pnd_Work_Area = localVariables.newGroupInRecord("pnd_Work_Area", "#WORK-AREA");
        pnd_Work_Area_Pnd_Rec_Read = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Work_Area_Pnd_Rec_Process = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 7);
        pnd_Work_Area_Pnd_Print_Com = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Sv_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_New_Source = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Work_Area_Pnd_Occ = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Occ", "#OCC", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_J = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_K = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_Work_Area_Pnd_First_Rec = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_A = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_A", "#A", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Page = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Page", "#PAGE", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Sub_Skip = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sub_Skip", "#SUB-SKIP", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Sv_Srce12 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Srce12", "#SV-SRCE12", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Source12 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Work_Area_Pnd_Source34 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Source34", "#SOURCE34", FieldType.STRING, 2);

        pnd_Work_Area_Pnd_Savers = pnd_Work_Area.newGroupInGroup("pnd_Work_Area_Pnd_Savers", "#SAVERS");
        pnd_Work_Area_Pnd_Sv_Company = pnd_Work_Area_Pnd_Savers.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Work_Area_Pnd_Sv_Srce_Code = pnd_Work_Area_Pnd_Savers.newFieldInGroup("pnd_Work_Area_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 
            4);
        pnd_Work_Area_Pnd_Tot_Prt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Tax_Amt = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Work_Area_Pnd_Fatca = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Fatca", "#FATCA", FieldType.BOOLEAN, 1);
        pnd_Work_Area_Pnd_Hdg1 = pnd_Work_Area.newFieldInGroup("pnd_Work_Area_Pnd_Hdg1", "#HDG1", FieldType.STRING, 9);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Start_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_End_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_Cmpy_Cnt.setInitialValue(7);
        pnd_Work_Area_Pnd_First_Rec.setInitialValue(true);
        pnd_Work_Area_Pnd_A.setInitialValue(true);
        pnd_Work_Area_Pnd_Page.setInitialValue(false);
        pnd_Work_Area_Pnd_Sub_Skip.setInitialValue(false);
        pnd_Work_Area_Pnd_Tot_Prt.setInitialValue(false);
        pnd_Work_Area_Pnd_Fatca.setInitialValue(false);
        pnd_Work_Area_Pnd_Hdg1.setInitialValue("NON-FATCA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0945() throws Exception
    {
        super("Twrp0945");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  --------------------------------------------
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Dashes.moveAll("=");                                                                                                                                          //Natural: MOVE ALL '=' TO #DASHES
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Var_File1_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Var_File1_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *READ WORK FILE 2  RECORD #FLAT-FILE3 /* 11/2014
        //*  11/2014
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            //*  11/2014
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().greater(getZero()) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().notEquals("Y")))) //Natural: ACCEPT IF #FF3-CAN-WTHLD-AMT > 0 AND #FF3-TWRPYMNT-FATCA-IND NE 'Y'
            {
                continue;
            }
            pnd_Work_Area_Pnd_Rec_Process.nadd(1);                                                                                                                        //Natural: ADD 1 TO #REC-PROCESS
            if (condition(pnd_Work_Area_Pnd_First_Rec.getBoolean()))                                                                                                      //Natural: IF #FIRST-REC
            {
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Work_Area_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                             //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_Work_Area_Pnd_First_Rec.setValue(false);                                                                                                              //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Work_Area_Pnd_Sv_Srce_Code)))                                                //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                    sub_Source_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  11/2014 START
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                sub_Co_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                           //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
                {
                    DbsUtil.examine(new ExamineSource(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new                   //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
                        ExamineGivingIndex(pnd_Tot_Ndx));
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                    sub_Accum_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR01:                                                                                                                                                //Natural: FOR #TOT-NDX 1 #CMPY-CNT
                    for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
                    {
                        if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                   //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                        {
                            pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                    //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                            sub_Accum_Total();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  11/2014 END
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- PROCESS-RECORD
            //*                  -------------
            pnd_Work_Area_Pnd_Tax_Amt.reset();                                                                                                                            //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet219 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet219++;
                //*                                           BG
                //*        OR = '6')     H4
                pnd_Work_Area_Pnd_Tax_Amt.setValue(0);                                                                                                                    //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet219++;
                pnd_Work_Area_Pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Area_Pnd_Tax_Amt.setValue(0);                                                                                                                    //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR02:                                                                                                                                                        //Natural: FOR #K 1 3
            for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(3)); pnd_Work_Area_Pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Work_Area_Pnd_K).nadd(1);                                                                                         //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                         //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                             //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(pnd_Work_Area_Pnd_Tax_Amt);                                                               //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                             //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                       //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-AMT ( #K )
                pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                       //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-AMT ( #K )
                pnd_Counters_Pnd_Can_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                       //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-AMT ( #K )
                if (condition((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ"))  //Natural: IF ( #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42' ) AND #FF3-STATE-WTHLD > 0
                    || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().greater(getZero()))))
                {
                    pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                      //Natural: ADD #FF3-STATE-WTHLD TO #PR-AMT ( #K )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Area_Pnd_Rec_Process.equals(getZero())))                                                                                                   //Natural: IF #REC-PROCESS = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  11/2014 START
            //*  ROXAN 06/02
                                                                                                                                                                          //Natural: PERFORM CO-DESC
            sub_Co_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                               //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
            {
                DbsUtil.examine(new ExamineSource(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                sub_Accum_Total();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR03:                                                                                                                                                    //Natural: FOR #TOT-NDX 1 #CMPY-CNT
                for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
                {
                    if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                       //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                    {
                        pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                        //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                        sub_Accum_Total();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  11/2014 END
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM()," -- CONTROL TOTAL",NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS READ      : ", //Natural: WRITE ( 1 ) *INIT-USER '-' *PROGRAM ' -- CONTROL TOTAL' // 'TOTAL NUMBER OF RECORDS READ      : ' #REC-READ / 'TOTAL NUMBER OF RECORDS PROCESSED : ' #REC-PROCESS
            pnd_Work_Area_Pnd_Rec_Read,NEWLINE,"TOTAL NUMBER OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Rec_Process);
        if (Global.isEscape()) return;
        //*  11/2014 START
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        pnd_Counters.getValue("*").resetInitial();                                                                                                                        //Natural: RESET INITIAL #COUNTERS ( * ) #PRINT-FIELDS #PRINT0-FIELDS #WORK-AREA
        pnd_Print_Fields.resetInitial();
        pnd_Print0_Fields.resetInitial();
        pnd_Work_Area.resetInitial();
        pnd_Work_Area_Pnd_Fatca.setValue(true);                                                                                                                           //Natural: ASSIGN #FATCA := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 2 #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Work_Area_Pnd_Rec_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #REC-READ
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt().greater(getZero()) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y")))) //Natural: ACCEPT IF #FF3-CAN-WTHLD-AMT > 0 AND #FF3-TWRPYMNT-FATCA-IND EQ 'Y'
            {
                continue;
            }
            pnd_Work_Area_Pnd_Rec_Process.nadd(1);                                                                                                                        //Natural: ADD 1 TO #REC-PROCESS
            if (condition(pnd_Work_Area_Pnd_First_Rec.getBoolean()))                                                                                                      //Natural: IF #FIRST-REC
            {
                pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                              //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Work_Area_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                             //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_Work_Area_Pnd_First_Rec.setValue(false);                                                                                                              //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Work_Area_Pnd_Sv_Company)))                                                     //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Work_Area_Pnd_Sv_Srce_Code)))                                                //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                    sub_Source_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                sub_Co_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                           //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
                {
                    DbsUtil.examine(new ExamineSource(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new                   //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
                        ExamineGivingIndex(pnd_Tot_Ndx));
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                    sub_Accum_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR04:                                                                                                                                                //Natural: FOR #TOT-NDX 1 #CMPY-CNT
                    for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
                    {
                        if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                   //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                        {
                            pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                    //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                            sub_Accum_Total();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------- PROCESS-RECORD
            //*                  -------------
            pnd_Work_Area_Pnd_Tax_Amt.reset();                                                                                                                            //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet324 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet324++;
                //*                                           BG
                //*        OR = '6')     H4
                pnd_Work_Area_Pnd_Tax_Amt.setValue(0);                                                                                                                    //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet324++;
                pnd_Work_Area_Pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Work_Area_Pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Work_Area_Pnd_Tax_Amt.setValue(0);                                                                                                                    //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR05:                                                                                                                                                        //Natural: FOR #K 1 3
            for (pnd_Work_Area_Pnd_K.setValue(1); condition(pnd_Work_Area_Pnd_K.lessOrEqual(3)); pnd_Work_Area_Pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Work_Area_Pnd_K).nadd(1);                                                                                         //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                         //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                             //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(pnd_Work_Area_Pnd_Tax_Amt);                                                               //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                             //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                       //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-AMT ( #K )
                pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                       //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-AMT ( #K )
                pnd_Counters_Pnd_Can_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                       //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-AMT ( #K )
                if (condition((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ"))  //Natural: IF ( #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42' ) AND #FF3-STATE-WTHLD > 0
                    || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")) && ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld().greater(getZero()))))
                {
                    pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Work_Area_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                      //Natural: ADD #FF3-STATE-WTHLD TO #PR-AMT ( #K )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Work_Area_Pnd_Rec_Process.equals(getZero())))                                                                                                   //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ROXAN 06/02
                                                                                                                                                                          //Natural: PERFORM CO-DESC
            sub_Co_Desc();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Work_Area_Pnd_Print_Com)))                                                               //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
            {
                DbsUtil.examine(new ExamineSource(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Work_Area_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                sub_Accum_Total();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR06:                                                                                                                                                    //Natural: FOR #TOT-NDX 1 #CMPY-CNT
                for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
                {
                    if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                       //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                    {
                        pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Work_Area_Pnd_Print_Com);                                                        //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                        sub_Accum_Total();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-COMPANY-TOTALS
        sub_Print_Company_Totals();
        if (condition(Global.isEscape())) {return;}
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM()," -- CONTROL TOTAL",NEWLINE,NEWLINE,"TOTAL NUMBER OF RECORDS READ      : ", //Natural: WRITE ( 1 ) *INIT-USER '-' *PROGRAM ' -- CONTROL TOTAL' // 'TOTAL NUMBER OF RECORDS READ      : ' #REC-READ / 'TOTAL NUMBER OF RECORDS PROCESSED : ' #REC-PROCESS
            pnd_Work_Area_Pnd_Rec_Read,NEWLINE,"TOTAL NUMBER OF RECORDS PROCESSED : ",pnd_Work_Area_Pnd_Rec_Process);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  --------------------------------------------
        //*  --------------------------------------------
        //*  --------------------------------------------
        //*  NONE      #PRINT-COM := '    '
        //*  --------------------------------------------
        //*                  --------
        //*  --------------------------------------------
        //*                  ----------
        //*  --------------------------------------------
        //*  --------------------------------------------
        //*  --------------------------------------------
        //*  --------------------------------------------
        //*                  ----------
        //* ***********************************************************************
        //* ***********************************************************************
        //*  --------------------------------------------
        //*                    -------------
        //*  --------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }
    private void sub_Accum_Total() throws Exception                                                                                                                       //Natural: ACCUM-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cmpy_Totals_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Trans_Cnt.getValue(3));                                                             //Natural: ADD #TRANS-CNT ( 3 ) TO #TOT-TRANS-CNT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Gross_Amt.getValue(3));                                                             //Natural: ADD #GROSS-AMT ( 3 ) TO #TOT-GROSS-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Ivc_Amt.getValue(3));                                                                 //Natural: ADD #IVC-AMT ( 3 ) TO #TOT-IVC-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Taxable_Amt.getValue(3));                                                         //Natural: ADD #TAXABLE-AMT ( 3 ) TO #TOT-TAXABLE-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Int_Amt.getValue(3));                                                                 //Natural: ADD #INT-AMT ( 3 ) TO #TOT-INT-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Fed_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Fed_Amt.getValue(3));                                                                 //Natural: ADD #FED-AMT ( 3 ) TO #TOT-FED-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Nra_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Nra_Amt.getValue(3));                                                                 //Natural: ADD #NRA-AMT ( 3 ) TO #TOT-NRA-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Pr_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Pr_Amt.getValue(3));                                                                   //Natural: ADD #PR-AMT ( 3 ) TO #TOT-PR-AMT ( #TOT-NDX )
        pnd_Cmpy_Totals_Pnd_Tot_Can_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Can_Amt.getValue(3));                                                                 //Natural: ADD #CAN-AMT ( 3 ) TO #TOT-CAN-AMT ( #TOT-NDX )
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
        sub_Source_To_Prt();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Work_Area_Pnd_Sv_Srce_Code.getSubstring(1,2))))                    //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                     //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Work_Area_Pnd_Tot_Prt.getBoolean()))                                                                                                            //Natural: IF #TOT-PRT
        {
            ignore();
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-CTR2-CTR4
            sub_Move_Ctr2_Ctr4();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Occ.setValue(4);                                                                                                                            //Natural: ASSIGN #OCC := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT GRANDTOTAL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Work_Area_Pnd_Occ.setValue(3);                                                                                                                                //Natural: ASSIGN #OCC := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_Area_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                      //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
        pnd_Counters.getValue("*").reset();                                                                                                                               //Natural: RESET #COUNTERS ( * )
        pnd_Work_Area_Pnd_A.setValue(true);                                                                                                                               //Natural: ASSIGN #A := TRUE
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*  9/20/04  RM
        //*  10/18/11
        //*  11/2014
        short decideConditionsMet474 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("CREF");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("LIFE");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TIAA");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TCII");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("TRST");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Work_Area_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet474++;
            pnd_Work_Area_Pnd_Print_Com.setValue("FSB");                                                                                                                  //Natural: ASSIGN #PRINT-COM := 'FSB'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Print_Com.setValue("OTHR");                                                                                                                 //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Ctr2_Ctr4() throws Exception                                                                                                                    //Natural: MOVE-CTR2-CTR4
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue(4).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue(2));                                                                          //Natural: ASSIGN #TRANS-CNT ( 4 ) := #TRANS-CNT ( 2 )
        pnd_Counters_Pnd_Gross_Amt.getValue(4).setValue(pnd_Counters_Pnd_Gross_Amt.getValue(2));                                                                          //Natural: ASSIGN #GROSS-AMT ( 4 ) := #GROSS-AMT ( 2 )
        pnd_Counters_Pnd_Ivc_Amt.getValue(4).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue(2));                                                                              //Natural: ASSIGN #IVC-AMT ( 4 ) := #IVC-AMT ( 2 )
        pnd_Counters_Pnd_Taxable_Amt.getValue(4).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue(2));                                                                      //Natural: ASSIGN #TAXABLE-AMT ( 4 ) := #TAXABLE-AMT ( 2 )
        pnd_Counters_Pnd_Int_Amt.getValue(4).setValue(pnd_Counters_Pnd_Int_Amt.getValue(2));                                                                              //Natural: ASSIGN #INT-AMT ( 4 ) := #INT-AMT ( 2 )
        pnd_Counters_Pnd_Fed_Amt.getValue(4).setValue(pnd_Counters_Pnd_Fed_Amt.getValue(2));                                                                              //Natural: ASSIGN #FED-AMT ( 4 ) := #FED-AMT ( 2 )
        pnd_Counters_Pnd_Nra_Amt.getValue(4).setValue(pnd_Counters_Pnd_Nra_Amt.getValue(2));                                                                              //Natural: ASSIGN #NRA-AMT ( 4 ) := #NRA-AMT ( 2 )
        pnd_Counters_Pnd_Can_Amt.getValue(4).setValue(pnd_Counters_Pnd_Can_Amt.getValue(2));                                                                              //Natural: ASSIGN #CAN-AMT ( 4 ) := #CAN-AMT ( 2 )
        pnd_Counters_Pnd_Pr_Amt.getValue(4).setValue(pnd_Counters_Pnd_Pr_Amt.getValue(2));                                                                                //Natural: ASSIGN #PR-AMT ( 4 ) := #PR-AMT ( 2 )
        pnd_Work_Area_Pnd_Occ.setValue(2);                                                                                                                                //Natural: ASSIGN #OCC := 2
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_Work_Area_Pnd_J.setValue(1); condition(pnd_Work_Area_Pnd_J.lessOrEqual(pnd_Work_Area_Pnd_Occ)); pnd_Work_Area_Pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Work_Area_Pnd_J).reset();                                                                                             //Natural: RESET #TRANS-CNT ( #J ) #GROSS-AMT ( #J ) #IVC-AMT ( #J ) #TAXABLE-AMT ( #J ) #INT-AMT ( #J ) #FED-AMT ( #J ) #NRA-AMT ( #J ) #CAN-AMT ( #J ) #PR-AMT ( #J )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Can_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
            pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Work_Area_Pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        //*  FOR DETAIL PER SOURCE-CODE
        short decideConditionsMet511 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 1
        if (condition((pnd_Work_Area_Pnd_Occ.equals(1))))
        {
            decideConditionsMet511++;
            pnd_Print_Fields_Pnd_Prtfld1.setValue(pnd_Work_Area_Pnd_New_Source);                                                                                          //Natural: MOVE #NEW-SOURCE TO #PRTFLD1
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
            //*  FOR TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Tot_Prt.setValue(false);                                                                                                                    //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Work_Area_Pnd_Occ.equals(4))))
        {
            decideConditionsMet511++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            //*  9/20/04  RM
            if (condition(pnd_Work_Area_Pnd_Sv_Srce12.equals("YY")))                                                                                                      //Natural: IF #SV-SRCE12 = 'YY'
            {
                pnd_Work_Area_Pnd_Sv_Srce12.setValue("NL");                                                                                                               //Natural: ASSIGN #SV-SRCE12 := 'NL'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Area_Pnd_Sv_Srce12.equals("ZZ")))                                                                                                      //Natural: IF #SV-SRCE12 = 'ZZ'
            {
                pnd_Work_Area_Pnd_Sv_Srce12.setValue("ML");                                                                                                               //Natural: ASSIGN #SV-SRCE12 := 'ML'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      (", pnd_Work_Area_Pnd_Sv_Srce12, ")"));                     //Natural: COMPRESS 'TOTAL      (' #SV-SRCE12 ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Work_Area_Pnd_Sub_Skip.setValue(true);                                                                                                                    //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Counters.getValue(4).reset();                                                                                                                             //Natural: RESET #COUNTERS ( 4 )
            pnd_Work_Area_Pnd_Tot_Prt.setValue(true);                                                                                                                     //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Work_Area_Pnd_Occ.equals(3))))
        {
            decideConditionsMet511++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL (", pnd_Work_Area_Pnd_Print_Com, ")"));                     //Natural: COMPRESS 'GRANDTOTAL (' #PRINT-COM ')' TO #PRTFLD1 LEAVING NO SPACE
            pnd_Work_Area_Pnd_Page.setValue(true);                                                                                                                        //Natural: ASSIGN #PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Rtn() throws Exception                                                                                                                         //Natural: PRINT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        pnd_Print_Fields_Pnd_Prtfld2.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZ,ZZ9"));                           //Natural: MOVE EDITED #TRANS-CNT ( #OCC ) ( EM = ZZZZ,ZZ9 ) TO #PRTFLD2
        pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                 //Natural: MOVE EDITED #GROSS-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD3
        pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                      //Natural: MOVE EDITED #IVC-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD4
        pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));               //Natural: MOVE EDITED #TAXABLE-AMT ( #OCC ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD5
        pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                      //Natural: MOVE EDITED #INT-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD6
        pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Fed_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                      //Natural: MOVE EDITED #FED-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD7
        pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Nra_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                      //Natural: MOVE EDITED #NRA-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD8
    }
    private void sub_Print_Rtn0() throws Exception                                                                                                                        //Natural: PRINT-RTN0
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print0_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Can_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #CAN-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD9
        pnd_Print0_Fields_Pnd_Prtfld10.setValueEdited(pnd_Counters_Pnd_Pr_Amt.getValue(pnd_Work_Area_Pnd_Occ),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #PR-AMT ( #OCC ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD10
    }
    private void sub_Print_Rtn1() throws Exception                                                                                                                        //Natural: PRINT-RTN1
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 98T #PRINT0-FIELDS
            TabSetting(98),pnd_Print0_Fields_Pnd_Prtfld9,pnd_Print0_Fields_Pnd_Prtfld10);
        if (Global.isEscape()) return;
        pnd_Print_Fields.reset();                                                                                                                                         //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
        pnd_Print0_Fields.reset();
        if (condition(pnd_Work_Area_Pnd_Sub_Skip.getBoolean()))                                                                                                           //Natural: IF #SUB-SKIP
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Work_Area_Pnd_Sub_Skip.setValue(false);                                                                                                                   //Natural: ASSIGN #SUB-SKIP := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Area_Pnd_Page.getBoolean()))                                                                                                               //Natural: IF #PAGE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Work_Area_Pnd_Page.setValue(false);                                                                                                                       //Natural: ASSIGN #PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Company_Totals() throws Exception                                                                                                              //Natural: PRINT-COMPANY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,pnd_Dashes);                                                                                                           //Natural: WRITE ( 1 ) #DASHES
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #TOT-NDX 1 #CMPY-CNT
        for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
        {
            if (condition(pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                               //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld1.setValue(DbsUtil.compress("Grand Total", pnd_Cmpy_Totals_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx)));                                  //Natural: COMPRESS 'Grand Total' #TOT-CMPNY ( #TOT-NDX ) INTO #PRTFLD1
            pnd_Print_Fields_Pnd_Prtfld2.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZ,ZZ9"));                          //Natural: MOVE EDITED #TOT-TRANS-CNT ( #TOT-NDX ) ( EM = ZZZZ,ZZ9 ) TO #PRTFLD2
            pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));                //Natural: MOVE EDITED #TOT-GROSS-AMT ( #TOT-NDX ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD3
            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #TOT-IVC-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD4
            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZ,ZZZ,ZZ9.99-"));              //Natural: MOVE EDITED #TOT-TAXABLE-AMT ( #TOT-NDX ) ( EM = ZZZZZZ,ZZZ,ZZ9.99- ) TO #PRTFLD5
            pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #TOT-INT-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD6
            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Fed_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #TOT-FED-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD7
            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Nra_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                     //Natural: MOVE EDITED #TOT-NRA-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD8
            pnd_Print0_Fields_Pnd_Prtfld9.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Can_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                    //Natural: MOVE EDITED #TOT-CAN-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD9
            pnd_Print0_Fields_Pnd_Prtfld10.setValueEdited(pnd_Cmpy_Totals_Pnd_Tot_Pr_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ,ZZ9.99-"));                    //Natural: MOVE EDITED #TOT-PR-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ,ZZ9.99- ) TO #PRTFLD10
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 98T #PRINT0-FIELDS
                TabSetting(98),pnd_Print0_Fields_Pnd_Prtfld9,pnd_Print0_Fields_Pnd_Prtfld10);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Work_Area_Pnd_Source12.setValue(pnd_Work_Area_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                            //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Work_Area_Pnd_Source34.setValue(pnd_Work_Area_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                            //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        //*  ADD 'PL' 'NL' 9/20/04  RM
        short decideConditionsMet598 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Work_Area_Pnd_Source34.equals("  "))))
        {
            decideConditionsMet598++;
            pnd_Work_Area_Pnd_Source.setValue(pnd_Work_Area_Pnd_Source12);                                                                                                //Natural: ASSIGN #SOURCE := #SOURCE12
        }                                                                                                                                                                 //Natural: VALUE 'OL', 'TM', 'ML', 'AL', 'IL', 'PL', 'NL'
        else if (condition((pnd_Work_Area_Pnd_Source34.equals("OL") || pnd_Work_Area_Pnd_Source34.equals("TM") || pnd_Work_Area_Pnd_Source34.equals("ML") 
            || pnd_Work_Area_Pnd_Source34.equals("AL") || pnd_Work_Area_Pnd_Source34.equals("IL") || pnd_Work_Area_Pnd_Source34.equals("PL") || pnd_Work_Area_Pnd_Source34.equals("NL"))))
        {
            decideConditionsMet598++;
            pnd_Work_Area_Pnd_Source.setValue(pnd_Work_Area_Pnd_Source34);                                                                                                //Natural: ASSIGN #SOURCE := #SOURCE34
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Source.setValue(" ");                                                                                                                       //Natural: ASSIGN #SOURCE := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet607 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Work_Area_Pnd_Source.equals("DS"))))
        {
            decideConditionsMet607++;
            pnd_Work_Area_Pnd_New_Source.setValue("DSS");                                                                                                                 //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Work_Area_Pnd_Source.equals("GS"))))
        {
            decideConditionsMet607++;
            pnd_Work_Area_Pnd_New_Source.setValue("GSRA");                                                                                                                //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Work_Area_Pnd_Source.equals("MS"))))
        {
            decideConditionsMet607++;
            pnd_Work_Area_Pnd_New_Source.setValue("MSS");                                                                                                                 //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Work_Area_Pnd_Source.equals("SS"))))
        {
            decideConditionsMet607++;
            pnd_Work_Area_Pnd_New_Source.setValue("SSSS");                                                                                                                //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Work_Area_Pnd_New_Source.setValue(pnd_Work_Area_Pnd_Source);                                                                                              //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Work_Area_Pnd_A.getBoolean()))                                                                                                                  //Natural: IF #A
        {
            pnd_Work_Area_Pnd_Sv_Srce12.setValue(pnd_Work_Area_Pnd_Source12);                                                                                             //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
            pnd_Work_Area_Pnd_A.setValue(false);                                                                                                                          //Natural: ASSIGN #A := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Area_Pnd_Source12.equals(pnd_Work_Area_Pnd_Sv_Srce12)))                                                                                    //Natural: IF #SOURCE12 = #SV-SRCE12
        {
            pnd_Work_Area_Pnd_Occ.setValue(1);                                                                                                                            //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  PRINT TOTAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area_Pnd_Occ.setValue(4);                                                                                                                            //Natural: ASSIGN #OCC := 4
            //*  PRINT DETAIL OF SV-SRCE-CDE
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Occ.setValue(1);                                                                                                                            //Natural: ASSIGN #OCC := 1
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
            sub_Print_Fields();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Area_Pnd_Sv_Srce12.setValue(pnd_Work_Area_Pnd_Source12);                                                                                             //Natural: ASSIGN #SV-SRCE12 := #SOURCE12
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    //*  11/2014 START
                    if (condition(pnd_Work_Area_Pnd_Fatca.getBoolean()))                                                                                                  //Natural: IF #FATCA
                    {
                        pnd_Work_Area_Pnd_Hdg1.setValue("  FATCA  ");                                                                                                     //Natural: ASSIGN #HDG1 := '  FATCA  '
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  11/2014
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 116T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 49T 'CANADIAN STATE TRANSACTIONS' / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 92T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY : ' #PRINT-COM /// 'SYSTEM' 14X 'TRANS' 10X 'GROSS' 11X 'TAX FREE' 10X 'TAXABLE' 9X 'INTEREST' 8X 'FEDERAL' 7X #HDG1 / 'SRCE' 16X 'COUNT' 10X 'AMOUNT' 13X 'IVC' 13X 'AMOUNT' 10X 'AMOUNT' 8X 'WITHHELD' 8X 'WITHHELD' / 98T '--------------' 2X '--------------' / 101T 'CANADIAN' 11X 'PR' / 101T 'WITHHELD' 8X 'WITHHELD' //
                        TabSetting(116),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(49),"CANADIAN STATE TRANSACTIONS",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new 
                        TabSetting(92),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Work_Area_Pnd_Print_Com,NEWLINE,NEWLINE,NEWLINE,"SYSTEM",new 
                        ColumnSpacing(14),"TRANS",new ColumnSpacing(10),"GROSS",new ColumnSpacing(11),"TAX FREE",new ColumnSpacing(10),"TAXABLE",new ColumnSpacing(9),"INTEREST",new 
                        ColumnSpacing(8),"FEDERAL",new ColumnSpacing(7),pnd_Work_Area_Pnd_Hdg1,NEWLINE,"SRCE",new ColumnSpacing(16),"COUNT",new ColumnSpacing(10),"AMOUNT",new 
                        ColumnSpacing(13),"IVC",new ColumnSpacing(13),"AMOUNT",new ColumnSpacing(10),"AMOUNT",new ColumnSpacing(8),"WITHHELD",new ColumnSpacing(8),"WITHHELD",NEWLINE,new 
                        TabSetting(98),"--------------",new ColumnSpacing(2),"--------------",NEWLINE,new TabSetting(101),"CANADIAN",new ColumnSpacing(11),"PR",NEWLINE,new 
                        TabSetting(101),"WITHHELD",new ColumnSpacing(8),"WITHHELD",NEWLINE,NEWLINE);
                    //*    9X   'INTEREST'  8X 'FEDERAL' 11X 'NRA' /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
