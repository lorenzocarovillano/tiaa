/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:34:37 PM
**        * FROM NATURAL PROGRAM : Twrp3105
************************************************************
**        * FILE NAME            : Twrp3105.java
**        * CLASS NAME           : Twrp3105
**        * INSTANCE NAME        : Twrp3105
************************************************************
********************************************************************
* PROGRAM  : TWRP3105  (COPY VERSION OF TWRP0905)
* FUNCTION : PRINTS REPORTABLE TRANSACTION BY SOURCE CODE REPORT ONLY.
*            DO NOT PRINT MATCH REPORT
* AUTHOR   : EDITH  4/7/99
* HISTORY
* -------------------------------------------------------------
* 7/13/99  : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*  (EDITH)     IVC-IND = K BUT NOT ROLL-OVER
*          : UPDATED TO INCLUDE NEW COMPANY CODE 'L' - TIAA LIFE
* 12/10/99 : NEW DEFINITION OF ROLL-OVER BASED ON DISTRIBUTION CODE
*            NEW ROUTINE FOR THE COMPUTATION OF TAXABLE AMT.
*            INCLUDED PROCESSING OF 'IP' - IPRO
*            INCLUDED PROCESSING OF NEW SOUCE CODES
*                'TMAL' (APAL) - CREATED DUE TO DELETED 'APTM'
*                'TMIL' (IAIL) - CREATED DUE TO DELETED 'IATM'
* 12/13/99 : DISTIBUTION CODE '6' IS NOT A ROLLOVER ANYMORE
* 12/14/99 : DISTIBUTION CODE 'Y' IS NOT A ROLLOVER ANYMORE
* 05/31/02  JH - ALLOW IVC AMOUNTS FOR ROLLOVERS
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 07/10/03 RM INVESTMENT SOLUTIONS - AUTOMATE SSSS PROJECT
*             ADD NEW SOURCE CODE 'SI' FOR INVESTMENT SOLUTIONS
*             UPDATE ALL RELATED LOGIC TO INCLUDE 'SI'.
* 12/01/04 RM - TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODE 'OP'.
*             EXPAND COUNTER TABLE FROM 18 TO 21, AND ALLIGN THE PRINT
*             FIELDS.
*             UPDATE ALL RELATED LOGIC TO INCLUDE 'OP'.
* 06/06/06  RM - NAVISYS CHANGES
*           ADD NEW SOURCE CODE 'NV'.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'NV'.
* 10/31/06  RM - VUL CHANGES
*           ADD NEW SOURCE CODE 'VL'.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'VL'.
* 02/22/08  RM - INTERMEDIATE RESULT TOO LARGE PROBLEM
*           ENLARGE COUNTERS FROM 9.2 TO 11.2, AND INCREASE EM AND PRINT
*           FIELDS.
* 11/30/11 RSALGADO - REPLACE PRINT ROUTINES TO CORRECT ALIGNMENT
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3105 extends BLNatBase
{
    // Data Areas
    private LdaTwrl3001 ldaTwrl3001;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_Fed_Wthld;
    private DbsField pnd_Counters_Pnd_Nra_Wthld;
    private DbsField pnd_Counters_Pnd_State_Wthld;
    private DbsField pnd_Counters_Pnd_Local_Wthld;
    private DbsField pnd_Counters_Pnd_Pr_Wthld;
    private DbsField pnd_Counters_Pnd_Can_Wthld;
    private DbsField pnd_Prt_Srce;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Type_Trans;
    private DbsField pnd_Trans_Desc;
    private DbsField pnd_Source;
    private DbsField pnd_Source12;
    private DbsField pnd_Sv_Srce12;
    private DbsField pnd_Sub_Src;
    private DbsField pnd_Source34;
    private DbsField pnd_New_Source;
    private DbsField pnd_Prt_Source;
    private DbsField pnd_P;
    private DbsField pnd_Occ;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_First;
    private DbsField pnd_First_Rec;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;

    private DbsGroup pnd_Var_File1__R_Field_1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year_A;

    private DbsGroup pnd_Input_Ff1;
    private DbsField pnd_Input_Ff1_Pnd_Intf_Date_Fr;
    private DbsField pnd_Input_Ff1_Pnd_Intf_Date_To;
    private DbsField pnd_Input_Ff1_Pnd_Pymt_Date_Fr;
    private DbsField pnd_Input_Ff1_Pnd_Pymt_Date_To;
    private DbsField pnd_Page;
    private DbsField pnd_Sub_Skip;
    private DbsField pnd_Eof;
    private DbsField pnd_Tot_Prt;
    private DbsField pnd_Error_Source;
    private DbsField pnd_Total_For_Ap_Ia;
    private DbsField pnd_Prt_Tot_Ol;
    private DbsField pnd_Tax_Amt;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Company;
    private DbsField pnd_Savers_Pnd_Sv_Tax_Ctz;
    private DbsField pnd_Savers_Pnd_Sv_Srce_Code;
    private DbsField pnd_Savers_Pnd_Sv_Payset;
    private DbsField pnd_Temp_Pr_Wthld;
    private DbsField pnd_Temp_State_Wthld;
    private DbsField pnd_C;
    private DbsField pnd_Name;
    private DbsField pnd_Print_Tc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl3001 = new LdaTwrl3001();
        registerRecord(ldaTwrl3001);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 21));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Fed_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Wthld", "#FED-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Nra_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Wthld", "#NRA-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_State_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_State_Wthld", "#STATE-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Local_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Local_Wthld", "#LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Pr_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Wthld", "#PR-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Can_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Wthld", "#CAN-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Srce = localVariables.newFieldInRecord("pnd_Prt_Srce", "#PRT-SRCE", FieldType.STRING, 4);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Type_Trans = localVariables.newFieldInRecord("pnd_Type_Trans", "#TYPE-TRANS", FieldType.STRING, 2);
        pnd_Trans_Desc = localVariables.newFieldInRecord("pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Source = localVariables.newFieldInRecord("pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_Source12 = localVariables.newFieldInRecord("pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Sv_Srce12 = localVariables.newFieldInRecord("pnd_Sv_Srce12", "#SV-SRCE12", FieldType.STRING, 2);
        pnd_Sub_Src = localVariables.newFieldInRecord("pnd_Sub_Src", "#SUB-SRC", FieldType.STRING, 2);
        pnd_Source34 = localVariables.newFieldInRecord("pnd_Source34", "#SOURCE34", FieldType.STRING, 2);
        pnd_New_Source = localVariables.newFieldInRecord("pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Prt_Source = localVariables.newFieldInRecord("pnd_Prt_Source", "#PRT-SOURCE", FieldType.STRING, 4);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pnd_Occ = localVariables.newFieldInRecord("pnd_Occ", "#OCC", FieldType.NUMERIC, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_First_Rec = localVariables.newFieldInRecord("pnd_First_Rec", "#FIRST-REC", FieldType.BOOLEAN, 1);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Var_File1__R_Field_1 = pnd_Var_File1.newGroupInGroup("pnd_Var_File1__R_Field_1", "REDEFINE", pnd_Var_File1_Pnd_Tax_Year);
        pnd_Var_File1_Pnd_Tax_Year_A = pnd_Var_File1__R_Field_1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);

        pnd_Input_Ff1 = localVariables.newGroupInRecord("pnd_Input_Ff1", "#INPUT-FF1");
        pnd_Input_Ff1_Pnd_Intf_Date_Fr = pnd_Input_Ff1.newFieldInGroup("pnd_Input_Ff1_Pnd_Intf_Date_Fr", "#INTF-DATE-FR", FieldType.STRING, 8);
        pnd_Input_Ff1_Pnd_Intf_Date_To = pnd_Input_Ff1.newFieldInGroup("pnd_Input_Ff1_Pnd_Intf_Date_To", "#INTF-DATE-TO", FieldType.STRING, 8);
        pnd_Input_Ff1_Pnd_Pymt_Date_Fr = pnd_Input_Ff1.newFieldInGroup("pnd_Input_Ff1_Pnd_Pymt_Date_Fr", "#PYMT-DATE-FR", FieldType.STRING, 8);
        pnd_Input_Ff1_Pnd_Pymt_Date_To = pnd_Input_Ff1.newFieldInGroup("pnd_Input_Ff1_Pnd_Pymt_Date_To", "#PYMT-DATE-TO", FieldType.STRING, 8);
        pnd_Page = localVariables.newFieldInRecord("pnd_Page", "#PAGE", FieldType.BOOLEAN, 1);
        pnd_Sub_Skip = localVariables.newFieldInRecord("pnd_Sub_Skip", "#SUB-SKIP", FieldType.BOOLEAN, 1);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Tot_Prt = localVariables.newFieldInRecord("pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Error_Source = localVariables.newFieldInRecord("pnd_Error_Source", "#ERROR-SOURCE", FieldType.BOOLEAN, 1);
        pnd_Total_For_Ap_Ia = localVariables.newFieldInRecord("pnd_Total_For_Ap_Ia", "#TOTAL-FOR-AP-IA", FieldType.BOOLEAN, 1);
        pnd_Prt_Tot_Ol = localVariables.newFieldInRecord("pnd_Prt_Tot_Ol", "#PRT-TOT-OL", FieldType.NUMERIC, 1);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Company = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Tax_Ctz = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Tax_Ctz", "#SV-TAX-CTZ", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Srce_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 4);
        pnd_Savers_Pnd_Sv_Payset = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Payset", "#SV-PAYSET", FieldType.STRING, 2);
        pnd_Temp_Pr_Wthld = localVariables.newFieldInRecord("pnd_Temp_Pr_Wthld", "#TEMP-PR-WTHLD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_State_Wthld = localVariables.newFieldInRecord("pnd_Temp_State_Wthld", "#TEMP-STATE-WTHLD", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 19);
        pnd_Print_Tc = localVariables.newFieldInRecord("pnd_Print_Tc", "#PRINT-TC", FieldType.STRING, 21);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl3001.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_First_Rec.setInitialValue(true);
        pnd_Page.setInitialValue(false);
        pnd_Sub_Skip.setInitialValue(false);
        pnd_Eof.setInitialValue(false);
        pnd_Tot_Prt.setInitialValue(false);
        pnd_Error_Source.setInitialValue(false);
        pnd_Total_For_Ap_Ia.setInitialValue(false);
        pnd_Prt_Tot_Ol.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp3105() throws Exception
    {
        super("Twrp3105");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133;//Natural: FORMAT ( 02 ) PS = 60 LS = 133
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #INFO-FF1
        while (condition(getWorkFiles().read(1, ldaTwrl3001.getPnd_Info_Ff1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr());                                                                            //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YR
            pnd_Input_Ff1_Pnd_Intf_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr());                                                                  //Natural: ASSIGN #INTF-DATE-FR := #FF1-INTF-DATE-FR
            pnd_Input_Ff1_Pnd_Intf_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_To());                                                                  //Natural: ASSIGN #INTF-DATE-TO := #FF1-INTF-DATE-TO
            pnd_Input_Ff1_Pnd_Pymt_Date_Fr.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr());                                                                  //Natural: ASSIGN #PYMT-DATE-FR := #FF1-PYMT-DATE-FR
            pnd_Input_Ff1_Pnd_Pymt_Date_To.setValue(ldaTwrl3001.getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To());                                                                  //Natural: ASSIGN #PYMT-DATE-TO := #FF1-PYMT-DATE-TO
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 02 RECORD #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            if (condition(pnd_First_Rec.equals(true)))                                                                                                                    //Natural: IF #FIRST-REC = TRUE
            {
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
                pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                              //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                pnd_Savers_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                   //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
                pnd_First_Rec.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST-REC := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals(pnd_Savers_Pnd_Sv_Tax_Ctz)))                                                 //Natural: IF #FF3-TAX-CITIZENSHIP = #SV-TAX-CTZ
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                               //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                    {
                        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().equals(pnd_Savers_Pnd_Sv_Payset)))                                              //Natural: IF #FF3-PAYSET-TYPE = #SV-PAYSET
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  BREAK OF PAYSET
                                                                                                                                                                          //Natural: PERFORM PAYSET-BRK
                            sub_Payset_Brk();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                            sub_Reset_Ctrs();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  BREAK OF SOURCE-CDE
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                        sub_Source_Brk();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                        sub_Reset_Ctrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BREAK OF TAX-CTZ
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                    sub_Company_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                    sub_Account_Grandtotal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                          //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  BREAK OF COMPANY
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                sub_Account_Grandtotal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                              //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  *** PROCESS RECORDS ***
            pnd_Tax_Amt.reset();                                                                                                                                          //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  TAXFREE EXCHANGE
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            short decideConditionsMet225 = 0;                                                                                                                             //Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && ((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")))))
            {
                decideConditionsMet225++;
                //*                                           BG
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT = TRUE OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().equals(true) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet225++;
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Tax_Amt.setValue(0);                                                                                                                                  //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Temp_Pr_Wthld.reset();                                                                                                                                    //Natural: RESET #TEMP-PR-WTHLD #TEMP-STATE-WTHLD
            pnd_Temp_State_Wthld.reset();
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ")      //Natural: IF #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))
            {
                pnd_Temp_Pr_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                              //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-PR-WTHLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Temp_State_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                                           //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-STATE-WTHLD
                //*  TOPS R3 +1 COMPANY        12/01/04   RM
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #K 1 5
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(5)); pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_K).nadd(1);                                                                                                       //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                                       //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                                           //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_K).nadd(pnd_Tax_Amt);                                                                                           //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                                           //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                                   //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-WTHLD ( #K )
                pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                                   //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-WTHLD ( #K )
                pnd_Counters_Pnd_Local_Wthld.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                                   //Natural: ADD #FF3-LOCAL-WTHLD TO #LOCAL-WTHLD ( #K )
                pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_K).nadd(pnd_Temp_Pr_Wthld);                                                                                        //Natural: ADD #TEMP-PR-WTHLD TO #PR-WTHLD ( #K )
                pnd_Counters_Pnd_State_Wthld.getValue(pnd_K).nadd(pnd_Temp_State_Wthld);                                                                                  //Natural: ADD #TEMP-STATE-WTHLD TO #STATE-WTHLD ( #K )
                pnd_Counters_Pnd_Can_Wthld.getValue(pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                                   //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-WTHLD ( #K )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (condition(pnd_Rec_Read.equals(getZero())))                                                                                                                    //Natural: IF #REC-READ = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Eof.setValue(true);                                                                                                                                       //Natural: ASSIGN #EOF := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
            sub_Account_Grandtotal();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  TOPS R3 EXPAND COUNTER    12/01/04   RM
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",NEWLINE,"RUNDATE    : ",Global.getDATX(),  //Natural: WRITE ( 1 ) / *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME    : ' *TIMX 52T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO / 49T '-- SUMMARY REPORT - -' /// 11X 'COMPANY' 8X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 5X 'FEDERAL TAX' 6X 'NRA TAX' / 9X 'TAX CTZNSHP' 6X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 6X 'WITHHOLDING' 4X 'WITHHOLDING' / 74T '-----------' 4X '-----------' 4X '-----------' 4X '-----------' / 75T 'STATE TAX' 6X 'LOCAL TAX' 5X 'PUERTO RICO' 5X 'CANADIAN' / 74T 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' //
            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",new TabSetting(95),"INTERFACE DATE",pnd_Input_Ff1_Pnd_Intf_Date_Fr,"THRU",pnd_Input_Ff1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME    : ",Global.getTIMX(),new 
            TabSetting(52),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Input_Ff1_Pnd_Pymt_Date_Fr,"THRU",pnd_Input_Ff1_Pnd_Pymt_Date_To,NEWLINE,new 
            TabSetting(49),"-- SUMMARY REPORT - -",NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(11),"COMPANY",new ColumnSpacing(8),"TRANS",new ColumnSpacing(11),"GROSS",new 
            ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new ColumnSpacing(7),"INTEREST",new ColumnSpacing(5),"FEDERAL TAX",new ColumnSpacing(6),"NRA TAX",NEWLINE,new 
            ColumnSpacing(9),"TAX CTZNSHP",new ColumnSpacing(6),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new 
            ColumnSpacing(9),"AMOUNT",new ColumnSpacing(6),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,new TabSetting(74),"-----------",new 
            ColumnSpacing(4),"-----------",new ColumnSpacing(4),"-----------",new ColumnSpacing(4),"-----------",NEWLINE,new TabSetting(75),"STATE TAX",new 
            ColumnSpacing(6),"LOCAL TAX",new ColumnSpacing(5),"PUERTO RICO",new ColumnSpacing(5),"CANADIAN",NEWLINE,new TabSetting(74),"WITHHOLDING",new 
            ColumnSpacing(4),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #C 6 21
        for (pnd_C.setValue(6); condition(pnd_C.lessOrEqual(21)); pnd_C.nadd(1))
        {
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF #C
            short decideConditionsMet280 = 0;                                                                                                                             //Natural: VALUE 6
            if (condition((pnd_C.equals(6))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("CREF-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_C.equals(7))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("CREF-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_C.equals(8))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL CREF -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL CREF -->: '
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_C.equals(9))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("LIFE-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_C.equals(10))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("LIFE-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_C.equals(11))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL LIFE -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL LIFE -->: '
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_C.equals(12))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TIAA-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_C.equals(13))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TIAA-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_C.equals(14))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL TIAA -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TIAA -->: '
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_C.equals(15))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TCII-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_C.equals(16))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TCII-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((pnd_C.equals(17))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL TCII -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TCII -->: '
            }                                                                                                                                                             //Natural: VALUE 18
            else if (condition((pnd_C.equals(18))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TRST-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 19
            else if (condition((pnd_C.equals(19))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("TRST-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_C.equals(20))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL TRST -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TRST -->: '
            }                                                                                                                                                             //Natural: VALUE 21
            else if (condition((pnd_C.equals(21))))
            {
                decideConditionsMet280++;
                pnd_Name.setValue("-- TOTAL OTHERS->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL OTHERS->: '
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_P.setValue(pnd_C);                                                                                                                                        //Natural: ASSIGN #P := #C
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 01 ) 1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"***** NO. OF RECORDS READ      : ",pnd_Rec_Read,NEWLINE);                                     //Natural: WRITE ( 01 ) /// '***** NO. OF RECORDS READ      : ' #REC-READ /
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------------------------
        //* *------------
        //* *------------
        //* *------------
        //* *-----------------------------
        //* *------------
        //* *------------
        //* *---------------------------
        //* *------------
        //* *------------------------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *------------
        //* *---------------------------------
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 02 )
    }
    private void sub_Payset_Brk() throws Exception                                                                                                                        //Natural: PAYSET-BRK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Type_Trans.setValue(pnd_Savers_Pnd_Sv_Payset);                                                                                                                //Natural: ASSIGN #TYPE-TRANS := #SV-PAYSET
        DbsUtil.callnat(Twrn0901.class , getCurrentProcessState(), pnd_Var_File1_Pnd_Tax_Year_A, pnd_Type_Trans, pnd_Trans_Desc);                                         //Natural: CALLNAT 'TWRN0901' #TAX-YEAR-A #TYPE-TRANS #TRANS-DESC
        if (condition(Global.isEscape())) return;
        pnd_Occ.setValue(1);                                                                                                                                              //Natural: ASSIGN #OCC := 1
        if (condition(pnd_First.getBoolean()))                                                                                                                            //Natural: IF #FIRST
        {
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
            sub_Source_To_Prt();
            if (condition(Global.isEscape())) {return;}
            pnd_First.setValue(false);                                                                                                                                    //Natural: ASSIGN #FIRST := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name.setValue(pnd_Trans_Desc);                                                                                                                                //Natural: MOVE #TRANS-DESC TO #NAME
        pnd_Trans_Desc.reset();                                                                                                                                           //Natural: RESET #TRANS-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                           //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------
                                                                                                                                                                          //Natural: PERFORM PAYSET-BRK
        sub_Payset_Brk();
        if (condition(Global.isEscape())) {return;}
        pnd_Occ.setValue(2);                                                                                                                                              //Natural: ASSIGN #OCC := 2
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                       //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
            //*                     (SAVE CNT/AMT OF OLD SOURCE-CODE FOR PRINTING)
            //*  TOTAL IS PRINTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                      //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Occ.setValue(5);                                                                                                                                      //Natural: ASSIGN #OCC := 5
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
                //*  TO RESET TOTAL CTR FOR NEW SRCE-CDE
                pnd_Counters.getValue(5).reset();                                                                                                                         //Natural: RESET #COUNTERS ( 5 )
                pnd_Occ.setValue(3);                                                                                                                                      //Natural: ASSIGN #OCC := 3
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        //*  TO PRINT TOTAL OF LAST BATCH SC
        if (condition(pnd_Eof.getBoolean()))                                                                                                                              //Natural: IF #EOF
        {
            //*  TOTAL IS PRINTED
            if (condition(pnd_Tot_Prt.getBoolean()))                                                                                                                      //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Occ.setValue(5);                                                                                                                                      //Natural: ASSIGN #OCC := 5
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Occ.setValue(4);                                                                                                                                              //Natural: ASSIGN #OCC := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
    }
    private void sub_Move_Value_5() throws Exception                                                                                                                      //Natural: MOVE-VALUE-5
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue(5).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue(3));                                                                          //Natural: ASSIGN #TRANS-CNT ( 5 ) := #TRANS-CNT ( 3 )
        pnd_Counters_Pnd_Gross_Amt.getValue(5).setValue(pnd_Counters_Pnd_Gross_Amt.getValue(3));                                                                          //Natural: ASSIGN #GROSS-AMT ( 5 ) := #GROSS-AMT ( 3 )
        pnd_Counters_Pnd_Ivc_Amt.getValue(5).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue(3));                                                                              //Natural: ASSIGN #IVC-AMT ( 5 ) := #IVC-AMT ( 3 )
        pnd_Counters_Pnd_Taxable_Amt.getValue(5).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue(3));                                                                      //Natural: ASSIGN #TAXABLE-AMT ( 5 ) := #TAXABLE-AMT ( 3 )
        pnd_Counters_Pnd_Int_Amt.getValue(5).setValue(pnd_Counters_Pnd_Int_Amt.getValue(3));                                                                              //Natural: ASSIGN #INT-AMT ( 5 ) := #INT-AMT ( 3 )
        pnd_Counters_Pnd_Fed_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Fed_Wthld.getValue(3));                                                                          //Natural: ASSIGN #FED-WTHLD ( 5 ) := #FED-WTHLD ( 3 )
        pnd_Counters_Pnd_Nra_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Nra_Wthld.getValue(3));                                                                          //Natural: ASSIGN #NRA-WTHLD ( 5 ) := #NRA-WTHLD ( 3 )
        pnd_Counters_Pnd_State_Wthld.getValue(5).setValue(pnd_Counters_Pnd_State_Wthld.getValue(3));                                                                      //Natural: ASSIGN #STATE-WTHLD ( 5 ) := #STATE-WTHLD ( 3 )
        pnd_Counters_Pnd_Local_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Local_Wthld.getValue(3));                                                                      //Natural: ASSIGN #LOCAL-WTHLD ( 5 ) := #LOCAL-WTHLD ( 3 )
        pnd_Counters_Pnd_Pr_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Pr_Wthld.getValue(3));                                                                            //Natural: ASSIGN #PR-WTHLD ( 5 ) := #PR-WTHLD ( 3 )
        pnd_Counters_Pnd_Can_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Can_Wthld.getValue(3));                                                                          //Natural: ASSIGN #CAN-WTHLD ( 5 ) := #CAN-WTHLD ( 3 )
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------
        //*  12/01/2004 RM
        short decideConditionsMet484 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet484++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet484++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet484++;
            pnd_Print_Com.setValue("LIFE");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet484++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet484++;
            pnd_Print_Com.setValue("TRST");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet500 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-TAX-CTZ;//Natural: VALUE 'F'
        if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F"))))
        {
            decideConditionsMet500++;
            pnd_Print_Tc.setValue("F - FOREIGN");                                                                                                                         //Natural: ASSIGN #PRINT-TC := 'F - FOREIGN'
        }                                                                                                                                                                 //Natural: VALUE 'U'
        else if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U"))))
        {
            decideConditionsMet500++;
            pnd_Print_Tc.setValue("U - US CTZN/RES.ALIEN");                                                                                                               //Natural: ASSIGN #PRINT-TC := 'U - US CTZN/RES.ALIEN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Occ)); pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_J).reset();                                                                                                           //Natural: RESET #TRANS-CNT ( #J ) #GROSS-AMT ( #J ) #IVC-AMT ( #J ) #TAXABLE-AMT ( #J ) #INT-AMT ( #J ) #FED-WTHLD ( #J ) #NRA-WTHLD ( #J ) #STATE-WTHLD ( #J ) #LOCAL-WTHLD ( #J ) #PR-WTHLD ( #J ) #CAN-WTHLD ( #J )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_J).reset();
            pnd_Counters_Pnd_State_Wthld.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Local_Wthld.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_J).reset();
            pnd_Counters_Pnd_Can_Wthld.getValue(pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Source12.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                                             //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Source34.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                                             //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        //*  ADD 'SI'       RM 07/10/03
        //*  ADD 'OP'       RM 12/01/04
        short decideConditionsMet525 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Source34.equals("  "))))
        {
            decideConditionsMet525++;
            //*  ADD 'NV'       RM 06/06/06
            //*  ADD 'VL'       RM 10/31/06
            //*  ADD 'AM'       MS 01/07/12
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("CP") || pnd_Source12.equals("DC") || pnd_Source12.equals("DS") || pnd_Source12.equals("ED")   //Natural: IF #SOURCE12 = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IP' OR = 'IS' OR = 'MS' OR = 'NV' OR = 'NZ' OR = 'OP' OR = 'RE' OR = 'SI' OR = 'SS' OR = 'VL' OR = 'AM'
                || pnd_Source12.equals("EW") || pnd_Source12.equals("GS") || pnd_Source12.equals("IA") || pnd_Source12.equals("IP") || pnd_Source12.equals("IS") 
                || pnd_Source12.equals("MS") || pnd_Source12.equals("NV") || pnd_Source12.equals("NZ") || pnd_Source12.equals("OP") || pnd_Source12.equals("RE") 
                || pnd_Source12.equals("SI") || pnd_Source12.equals("SS") || pnd_Source12.equals("VL") || pnd_Source12.equals("AM")))
            {
                pnd_Source.setValue(pnd_Source12);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE12
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source12);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE12
                //*  ADD 'PL'       RM 12/01/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'OL','PL'
        else if (condition((pnd_Source34.equals("OL") || pnd_Source34.equals("PL"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("CP") || pnd_Source12.equals("DC") || pnd_Source12.equals("DS") || pnd_Source12.equals("ED")   //Natural: IF #SOURCE12 = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IP' OR = 'IS' OR = 'MS' OR = 'NV' OR = 'NZ' OR = 'OP' OR = 'RE' OR = 'SI' OR = 'SS' OR = 'VL' OR = 'YY' OR = 'ZZ' OR = 'AM'
                || pnd_Source12.equals("EW") || pnd_Source12.equals("GS") || pnd_Source12.equals("IA") || pnd_Source12.equals("IP") || pnd_Source12.equals("IS") 
                || pnd_Source12.equals("MS") || pnd_Source12.equals("NV") || pnd_Source12.equals("NZ") || pnd_Source12.equals("OP") || pnd_Source12.equals("RE") 
                || pnd_Source12.equals("SI") || pnd_Source12.equals("SS") || pnd_Source12.equals("VL") || pnd_Source12.equals("YY") || pnd_Source12.equals("ZZ") 
                || pnd_Source12.equals("AM")))
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  3-2-99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Source34.equals("TM"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("IA")))                                                                                        //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'ML'
        else if (condition((pnd_Source34.equals("ML"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("ZZ")))                                                                                                                     //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  ADD 'NL'  RM 12/01/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'NL'
        else if (condition((pnd_Source34.equals("NL"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("YY")))                                                                                                                     //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
                //*   11/10/99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Source34.equals("AL"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("AP")))                                                                                                                     //Natural: IF #SOURCE12 = 'AP'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Source34.equals("IL"))))
        {
            decideConditionsMet525++;
            if (condition(pnd_Source12.equals("IA")))                                                                                                                     //Natural: IF #SOURCE12 = 'IA'
            {
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Source.setValue(true);                                                                                                                          //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Source.setValue(pnd_Source34);                                                                                                                        //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet594 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Source.equals("DS"))))
        {
            decideConditionsMet594++;
            pnd_New_Source.setValue("DSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Source.equals("GS"))))
        {
            decideConditionsMet594++;
            pnd_New_Source.setValue("GSRA");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Source.equals("MS"))))
        {
            decideConditionsMet594++;
            pnd_New_Source.setValue("MSS");                                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Source.equals("SS"))))
        {
            decideConditionsMet594++;
            pnd_New_Source.setValue("SSSS");                                                                                                                              //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_New_Source.setValue(pnd_Source);                                                                                                                          //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Prt_Source.setValue(pnd_New_Source);                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := #NEW-SOURCE
        short decideConditionsMet608 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'OL'
        if (condition((pnd_Source.equals("OL"))))
        {
            decideConditionsMet608++;
            if (condition(pnd_Source12.equals("ZZ")))                                                                                                                     //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Prt_Source.setValue("MLOL");                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := 'MLOL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                     //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
                //*  ADD 'PL'  RM 12/01/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'PL'
        else if (condition((pnd_Source.equals("PL"))))
        {
            decideConditionsMet608++;
            if (condition(pnd_Source12.equals("YY")))                                                                                                                     //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Prt_Source.setValue("NLPL");                                                                                                                          //Natural: ASSIGN #PRT-SOURCE := 'NLPL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                     //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Source.equals("TM"))))
        {
            decideConditionsMet608++;
            pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                         //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Source.equals("AL"))))
        {
            decideConditionsMet608++;
            pnd_Prt_Source.setValue("TMAL");                                                                                                                              //Natural: ASSIGN #PRT-SOURCE := 'TMAL'
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Source.equals("IL"))))
        {
            decideConditionsMet608++;
            pnd_Prt_Source.setValue("TMIL");                                                                                                                              //Natural: ASSIGN #PRT-SOURCE := 'TMIL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Prt_Srce.setValue(pnd_New_Source);                                                                                                                            //Natural: MOVE #NEW-SOURCE TO #PRT-SRCE
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        short decideConditionsMet638 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 1
        if (condition((pnd_Occ.equals(1))))
        {
            decideConditionsMet638++;
            pnd_P.setValue(pnd_Occ);                                                                                                                                      //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Tot_Prt.setValue(false);                                                                                                                                  //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Occ.equals(2))))
        {
            decideConditionsMet638++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL   ( ", pnd_Prt_Source, ")"));                                                     //Natural: COMPRESS 'SUBTOTAL   ( ' #PRT-SOURCE ')' TO #NAME LEAVING NO SPACE
            pnd_Sub_Skip.setValue(true);                                                                                                                                  //Natural: ASSIGN #SUB-SKIP := TRUE
            pnd_P.setValue(pnd_Occ);                                                                                                                                      //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            //*  ADD 'PL'  RM 12/01/04
            //*  TO PRINT TOTAL AFTER 'OL' SUBTOTAL
            if (condition(pnd_Source.equals("OL") || pnd_Source.equals("PL")))                                                                                            //Natural: IF #SOURCE = 'OL' OR = 'PL'
            {
                pnd_Prt_Tot_Ol.setValue(1);                                                                                                                               //Natural: ASSIGN #PRT-TOT-OL := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tot_Prt.setValue(false);                                                                                                                              //Natural: ASSIGN #TOT-PRT := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prt_Tot_Ol.equals(1)))                                                                                                                      //Natural: IF #PRT-TOT-OL = 1
            {
                //*                               (3-2-99)
                //*  FOR 'APOL'  & 'APTM'
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,                    //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
                    2))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                               //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                    pnd_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Sub_Src, ")"));                                                //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #NAME LEAVING NO SPACE
                    pnd_Occ.setValue(3);                                                                                                                                  //Natural: ASSIGN #OCC := 3
                    pnd_Sub_Skip.setValue(true);                                                                                                                          //Natural: ASSIGN #SUB-SKIP := TRUE
                    pnd_P.setValue(pnd_Occ);                                                                                                                              //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
                    sub_Print_Rtn1();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Prt_Tot_Ol.setValue(0);                                                                                                                           //Natural: ASSIGN #PRT-TOT-OL := 0
                    pnd_Tot_Prt.setValue(true);                                                                                                                           //Natural: ASSIGN #TOT-PRT := TRUE
                    //*                                           3-2-99
                    //*  SINCE TOTAL FOR AP/IA
                    //*  IS PRINTED ALREADY
                    if (condition(pnd_Source12.equals("AP") || pnd_Source12.equals("IA")))                                                                                //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
                    {
                        pnd_Total_For_Ap_Ia.setValue(true);                                                                                                               //Natural: ASSIGN #TOTAL-FOR-AP-IA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                       3-2-99 : TO PRINT TOTAL FOR 'AP' OR 'AI'
            //*                                IF THERE IS NO 'APOL' OR 'IAOL read
            if (condition(pnd_Total_For_Ap_Ia.equals(true)))                                                                                                              //Natural: IF #TOTAL-FOR-AP-IA = TRUE
            {
                pnd_Total_For_Ap_Ia.setValue(false);                                                                                                                      //Natural: ASSIGN #TOTAL-FOR-AP-IA := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Source.equals("TM")))                                                                                                                   //Natural: IF #SOURCE = 'TM'
                {
                    pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                               //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                    pnd_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Sub_Src, ")"));                                                //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #NAME LEAVING NO SPACE
                    pnd_Occ.setValue(3);                                                                                                                                  //Natural: ASSIGN #OCC := 3
                    pnd_Sub_Skip.setValue(true);                                                                                                                          //Natural: ASSIGN #SUB-SKIP := TRUE
                    pnd_P.setValue(pnd_Occ);                                                                                                                              //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
                    sub_Print_Rtn1();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Prt_Tot_Ol.setValue(0);                                                                                                                           //Natural: ASSIGN #PRT-TOT-OL := 0
                    pnd_Tot_Prt.setValue(true);                                                                                                                           //Natural: ASSIGN #TOT-PRT := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Occ.equals(4))))
        {
            decideConditionsMet638++;
            pnd_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL ( ", pnd_Print_Com, "-", pnd_Savers_Pnd_Sv_Tax_Ctz, ")"));                      //Natural: COMPRESS 'GRANDTOTAL ( ' #PRINT-COM '-' #SV-TAX-CTZ ')' TO #NAME LEAVING NO SPACE
            pnd_Page.setValue(true);                                                                                                                                      //Natural: ASSIGN #PAGE := TRUE
            pnd_P.setValue(pnd_Occ);                                                                                                                                      //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Occ.equals(5))))
        {
            decideConditionsMet638++;
            short decideConditionsMet705 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #PRT-SOURCE;//Natural: VALUE 'TMAL'
            if (condition((pnd_Prt_Source.equals("TMAL"))))
            {
                decideConditionsMet705++;
                pnd_Sub_Src.setValue("AP");                                                                                                                               //Natural: ASSIGN #SUB-SRC := 'AP'
            }                                                                                                                                                             //Natural: VALUE 'TMIL'
            else if (condition((pnd_Prt_Source.equals("TMIL"))))
            {
                decideConditionsMet705++;
                pnd_Sub_Src.setValue("IA");                                                                                                                               //Natural: ASSIGN #SUB-SRC := 'IA'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Sub_Src.setValue(pnd_Prt_Source.getSubstring(1,2));                                                                                                   //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL ( ", pnd_Sub_Src, ")"));                                                             //Natural: COMPRESS 'TOTAL ( ' #SUB-SRC ')' TO #NAME LEAVING NO SPACE
            pnd_P.setValue(pnd_Occ);                                                                                                                                      //Natural: ASSIGN #P := #OCC
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Tot_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Account_Grandtotal() throws Exception                                                                                                                //Natural: ACCOUNT-GRANDTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        short decideConditionsMet726 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SV-COMPANY = 'C' AND #SV-TAX-CTZ = 'F'
        if (condition(pnd_Savers_Pnd_Sv_Company.equals("C") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(6);                                                                                                                                            //Natural: ASSIGN #C := 6
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'C' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("C") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(7);                                                                                                                                            //Natural: ASSIGN #C := 7
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'L' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("L") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(9);                                                                                                                                            //Natural: ASSIGN #C := 9
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'L' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("L") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(10);                                                                                                                                           //Natural: ASSIGN #C := 10
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'T' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("T") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(12);                                                                                                                                           //Natural: ASSIGN #C := 12
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'T' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("T") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(13);                                                                                                                                           //Natural: ASSIGN #C := 13
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'S' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("S") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(15);                                                                                                                                           //Natural: ASSIGN #C := 15
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'S' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("S") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(16);                                                                                                                                           //Natural: ASSIGN #C := 16
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("X") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(18);                                                                                                                                           //Natural: ASSIGN #C := 18
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("X") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet726++;
            pnd_C.setValue(19);                                                                                                                                           //Natural: ASSIGN #C := 19
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_C.setValue(21);                                                                                                                                           //Natural: ASSIGN #C := 21
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM MOVE-VAL
        sub_Move_Val();
        if (condition(Global.isEscape())) {return;}
        //*  REALIGN EXPANDED COUNTER  12/01/04  RM
        //*  TOTAL OF CREF
        //*  TOTAL OF LIFE
        //*  TOTAL OF TIAA
        //*  TOTAL OF TCII
        //*  TOTAL OF TRST
        short decideConditionsMet759 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #C;//Natural: VALUE 6,7
        if (condition((pnd_C.equals(6) || pnd_C.equals(7))))
        {
            decideConditionsMet759++;
            pnd_C.setValue(8);                                                                                                                                            //Natural: ASSIGN #C := 8
        }                                                                                                                                                                 //Natural: VALUE 9,10
        else if (condition((pnd_C.equals(9) || pnd_C.equals(10))))
        {
            decideConditionsMet759++;
            pnd_C.setValue(11);                                                                                                                                           //Natural: ASSIGN #C := 11
        }                                                                                                                                                                 //Natural: VALUE 12,13
        else if (condition((pnd_C.equals(12) || pnd_C.equals(13))))
        {
            decideConditionsMet759++;
            pnd_C.setValue(14);                                                                                                                                           //Natural: ASSIGN #C := 14
        }                                                                                                                                                                 //Natural: VALUE 15,16
        else if (condition((pnd_C.equals(15) || pnd_C.equals(16))))
        {
            decideConditionsMet759++;
            pnd_C.setValue(17);                                                                                                                                           //Natural: ASSIGN #C := 17
        }                                                                                                                                                                 //Natural: VALUE 18,19
        else if (condition((pnd_C.equals(18) || pnd_C.equals(19))))
        {
            decideConditionsMet759++;
            pnd_C.setValue(20);                                                                                                                                           //Natural: ASSIGN #C := 20
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM MOVE-VAL
        sub_Move_Val();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Move_Val() throws Exception                                                                                                                          //Natural: MOVE-VAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------
        pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_C).nadd(pnd_Counters_Pnd_Trans_Cnt.getValue(4));                                                                          //Natural: ADD #TRANS-CNT ( 4 ) TO #TRANS-CNT ( #C )
        pnd_Counters_Pnd_Gross_Amt.getValue(pnd_C).nadd(pnd_Counters_Pnd_Gross_Amt.getValue(4));                                                                          //Natural: ADD #GROSS-AMT ( 4 ) TO #GROSS-AMT ( #C )
        pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_C).nadd(pnd_Counters_Pnd_Ivc_Amt.getValue(4));                                                                              //Natural: ADD #IVC-AMT ( 4 ) TO #IVC-AMT ( #C )
        pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_C).nadd(pnd_Counters_Pnd_Taxable_Amt.getValue(4));                                                                      //Natural: ADD #TAXABLE-AMT ( 4 ) TO #TAXABLE-AMT ( #C )
        pnd_Counters_Pnd_Int_Amt.getValue(pnd_C).nadd(pnd_Counters_Pnd_Int_Amt.getValue(4));                                                                              //Natural: ADD #INT-AMT ( 4 ) TO #INT-AMT ( #C )
        pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_Fed_Wthld.getValue(4));                                                                          //Natural: ADD #FED-WTHLD ( 4 ) TO #FED-WTHLD ( #C )
        pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_Pr_Wthld.getValue(4));                                                                            //Natural: ADD #PR-WTHLD ( 4 ) TO #PR-WTHLD ( #C )
        pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_Nra_Wthld.getValue(4));                                                                          //Natural: ADD #NRA-WTHLD ( 4 ) TO #NRA-WTHLD ( #C )
        pnd_Counters_Pnd_State_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_State_Wthld.getValue(4));                                                                      //Natural: ADD #STATE-WTHLD ( 4 ) TO #STATE-WTHLD ( #C )
        pnd_Counters_Pnd_Local_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_Local_Wthld.getValue(4));                                                                      //Natural: ADD #LOCAL-WTHLD ( 4 ) TO #LOCAL-WTHLD ( #C )
        pnd_Counters_Pnd_Can_Wthld.getValue(pnd_C).nadd(pnd_Counters_Pnd_Can_Wthld.getValue(4));                                                                          //Natural: ADD #CAN-WTHLD ( 4 ) TO #CAN-WTHLD ( #C )
    }
    private void sub_Print_Rtn1() throws Exception                                                                                                                        //Natural: PRINT-RTN1
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------- .
        if (condition(pnd_Error_Source.getBoolean()))                                                                                                                     //Natural: IF #ERROR-SOURCE
        {
            getReports().display(2, "///SYSTEM/SOURCE",                                                                                                                   //Natural: DISPLAY ( 2 ) '///SYSTEM/SOURCE' #PRT-SRCE '///TYPE OF/TRANSACTION' #NAME '///TRANS/COUNT' #TRANS-CNT ( #P ) ( EM = ZZZZ,ZZ9 ) '///GROSS/AMOUT' #GROSS-AMT ( #P ) ( EM = ZZZZZ,ZZZ,ZZ9.99 ) '///TAX FREE/IVC' #IVC-AMT ( #P ) ( EM = ZZZZZ,ZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING' #TAXABLE-AMT ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING' #INT-AMT ( #P ) ( EM = ZZZZ,ZZ9.99- ) 'FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING' #FED-WTHLD ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) 'NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING' #NRA-WTHLD ( #P ) ( EM = ZZZZ,ZZ9.99- )
            		pnd_Prt_Srce,"///TYPE OF/TRANSACTION",
            		pnd_Name,"///TRANS/COUNT",
            		pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9"),"///GROSS/AMOUT",
            		pnd_Counters_Pnd_Gross_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),"///TAX FREE/IVC",
            		pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING",
            		pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING",
                
            		pnd_Counters_Pnd_Int_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9.99-"),"FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING",
                
            		pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING",
                
            		pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            pnd_Error_Source.setValue(false);                                                                                                                             //Natural: ASSIGN #ERROR-SOURCE := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(1, "///SYSTEM/SOURCE",                                                                                                                   //Natural: DISPLAY ( 1 ) '///SYSTEM/SOURCE' #PRT-SRCE '///TYPE OF/TRANSACTION' #NAME '///TRANS/COUNT' #TRANS-CNT ( #P ) ( EM = ZZZZ,ZZ9 ) '///GROSS/AMOUNT' #GROSS-AMT ( #P ) ( EM = ZZZZZ,ZZZ,ZZ9.99- ) '///TAX FREE/IVC' #IVC-AMT ( #P ) ( EM = ZZZZZ,ZZZ,ZZ9.99- ) 'TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING' #TAXABLE-AMT ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) 'INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING' #INT-AMT ( #P ) ( EM = ZZZZ,ZZ9.99- ) 'FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING' #FED-WTHLD ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) 'NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING' #NRA-WTHLD ( #P ) ( EM = ZZZZ,ZZ9.99- )
            		pnd_Prt_Srce,"///TYPE OF/TRANSACTION",
            		pnd_Name,"///TRANS/COUNT",
            		pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9"),"///GROSS/AMOUNT",
            		pnd_Counters_Pnd_Gross_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"///TAX FREE/IVC",
            		pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING",
            		pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING",
                
            		pnd_Counters_Pnd_Int_Amt.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9.99-"),"FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING",
                
            		pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING",
                
            		pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_P), new ReportEditMask ("ZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,new ReportTAsterisk(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_P)),pnd_Counters_Pnd_State_Wthld.getValue(pnd_P),    //Natural: WRITE ( 1 ) T*#TAXABLE-AMT ( #P ) #STATE-WTHLD ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) T*#INT-AMT ( #P ) #LOCAL-WTHLD ( #P ) ( EM = ZZZZ,ZZ9.99- ) T*#FED-WTHLD ( #P ) #PR-WTHLD ( #P ) ( EM = ZZZZ,ZZZ,ZZ9.99- ) T*#NRA-WTHLD ( #P ) #CAN-WTHLD ( #P ) ( EM = ZZZZ,ZZ9.99- )
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Int_Amt.getValue(pnd_P)),pnd_Counters_Pnd_Local_Wthld.getValue(pnd_P), 
                new ReportEditMask ("ZZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_P)),pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_P), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_P)),pnd_Counters_Pnd_Can_Wthld.getValue(pnd_P), 
                new ReportEditMask ("ZZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SOURCE-CDE
        pnd_Prt_Srce.reset();                                                                                                                                             //Natural: RESET #PRT-SRCE
        if (condition(pnd_Sub_Skip.getBoolean()))                                                                                                                         //Natural: IF #SUB-SKIP
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Sub_Skip.setValue(false);                                                                                                                                 //Natural: ASSIGN #SUB-SKIP := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRANDTOTAL IS PRINTED
        if (condition(pnd_Page.getBoolean()))                                                                                                                             //Natural: IF #PAGE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Page.setValue(false);                                                                                                                                     //Natural: ASSIGN #PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Source_Prt() throws Exception                                                                                                                  //Natural: ERROR-SOURCE-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Prt_Srce.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                                               //Natural: ASSIGN #PRT-SRCE := #SV-SRCE-CODE
        pnd_Name.setValue(DbsUtil.compress(pnd_Savers_Pnd_Sv_Company, "-", pnd_Trans_Desc));                                                                              //Natural: COMPRESS #SV-COMPANY '-' #TRANS-DESC INTO #NAME
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
        sub_Print_Rtn1();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(42),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 42T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) 33T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME    : ' *TIMX 52T 'TAX YEAR ' #TAX-YEAR 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE    : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(33),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",new TabSetting(95),"INTERFACE DATE",pnd_Input_Ff1_Pnd_Intf_Date_Fr,"THRU",pnd_Input_Ff1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME    : ",Global.getTIMX(),new 
                        TabSetting(52),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(95)," PAYMENT  DATE",pnd_Input_Ff1_Pnd_Pymt_Date_Fr,"THRU",
                        pnd_Input_Ff1_Pnd_Pymt_Date_To,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Print_Tc,
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(42),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR / *INIT-USER '-' *PROGRAM 42T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 33T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' 95T 'INTERFACE DATE' #INTF-DATE-FR 'THRU' #INTF-DATE-TO / 'RUNTIME : ' *TIMX 39T 'EXCEPTION REPORT - ERRONEOUS SOURCE CODES' 95T ' PAYMENT  DATE' #PYMT-DATE-FR 'THRU' #PYMT-DATE-TO / 39T 'TAX YEAR ' #TAX-YEAR ///
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(33),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",new TabSetting(95),"INTERFACE DATE",pnd_Input_Ff1_Pnd_Intf_Date_Fr,"THRU",pnd_Input_Ff1_Pnd_Intf_Date_To,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                        TabSetting(39),"EXCEPTION REPORT - ERRONEOUS SOURCE CODES",new TabSetting(95)," PAYMENT  DATE",pnd_Input_Ff1_Pnd_Pymt_Date_Fr,"THRU",pnd_Input_Ff1_Pnd_Pymt_Date_To,NEWLINE,new 
                        TabSetting(39),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(2, "///SYSTEM/SOURCE",
        		pnd_Prt_Srce,"///TYPE OF/TRANSACTION",
        		pnd_Name,"///TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"///GROSS/AMOUT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99"),"///TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-"),"FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING",
        		pnd_Counters_Pnd_Fed_Wthld, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING",
        		pnd_Counters_Pnd_Nra_Wthld, new ReportEditMask ("ZZZZ,ZZ9.99-"));
        getReports().setDisplayColumns(1, "///SYSTEM/SOURCE",
        		pnd_Prt_Srce,"///TYPE OF/TRANSACTION",
        		pnd_Name,"///TRANS/COUNT",
        		pnd_Counters_Pnd_Trans_Cnt, new ReportEditMask ("ZZZZ,ZZ9"),"///GROSS/AMOUNT",
        		pnd_Counters_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"///TAX FREE/IVC",
        		pnd_Counters_Pnd_Ivc_Amt, new ReportEditMask ("ZZZZZ,ZZZ,ZZ9.99-"),"TAXABLE/AMOUNT/-----------/STATE TAX/WITHHOLDING",
        		pnd_Counters_Pnd_Taxable_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"INTEREST/AMOUNT/-----------/LOCAL TAX/WITHHOLDING",
        		pnd_Counters_Pnd_Int_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-"),"FEDERAL TAX/WITHHOLDING/-----------/PUERTO RICO/WITHHOLDING",
        		pnd_Counters_Pnd_Fed_Wthld, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99-"),"NRA TAX/WITHHOLDING/-----------/CANADIAN/WITHHOLDING",
        		pnd_Counters_Pnd_Nra_Wthld, new ReportEditMask ("ZZZZ,ZZ9.99-"));
    }
}
