/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:58 PM
**        * FROM NATURAL PROGRAM : Twrp5505
************************************************************
**        * FILE NAME            : Twrp5505.java
**        * CLASS NAME           : Twrp5505
**        * INSTANCE NAME        : Twrp5505
************************************************************
************************************************************************
** PROGRAM : TWRP5505
** SYSTEM  : TAXWARS
** AUTHOR  : J.ROTHOLZ
** FUNCTION: PAYMENT SUMMARY FOR SUNY/CUNY
**
** HISTORY :
** ---------------------------------------------------------------------
** 02/18/15: OS - RECOMPILED FOR UPDATED TWRL0900     ------------------
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5505 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0900 ldaTwrl0900;
    private PdaIaaatxia pdaIaaatxia;
    private PdaTwrapstp pdaTwrapstp;
    private PdaTwraplr2 pdaTwraplr2;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Pnd_Cntl_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Prev_Contract;
    private DbsField pnd_Ws_Pnd_Prev_Payee;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Pln_Cnt;
    private DbsField pnd_Ws_Pnd_S;
    private DbsField pnd_Ws_Pnd_S1;
    private DbsField pnd_Ws_Pnd_Tran_Cv;
    private DbsField pnd_Ws_Pnd_Part_Cv;
    private DbsField pnd_Ws_Pnd_Ia_Break;

    private DbsGroup pnd_Cntl;
    private DbsField pnd_Cntl_Pnd_Cntl_Text;
    private DbsField pnd_Cntl_Pnd_Cntl_Text1;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt_Cv;
    private DbsField pnd_Cntl_Pnd_Part_Cnt_Cv;

    private DbsGroup pnd_Cntl_Pnd_Totals;
    private DbsField pnd_Cntl_Pnd_Tran_Cnt;
    private DbsField pnd_Cntl_Pnd_Part_Cnt;
    private DbsField pnd_Cntl_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Pnd_Taxable_Amt;
    private DbsField pnd_Cntl_Pnd_Fed_Tax;
    private DbsField pnd_Cntl_Pnd_Sta_Tax;
    private DbsField pnd_Cntl_Pnd_Loc_Tax;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Suny_Found;

    private DbsGroup pnd_Case_Fields_Pnd_Totals_C;
    private DbsField pnd_Case_Fields_Pnd_Tran_Cnt_C;
    private DbsField pnd_Case_Fields_Pnd_Part_Cnt_C;
    private DbsField pnd_Case_Fields_Pnd_Gross_Amt_C;
    private DbsField pnd_Case_Fields_Pnd_Taxable_Amt_C;
    private DbsField pnd_Case_Fields_Pnd_Fed_Tax_C;
    private DbsField pnd_Case_Fields_Pnd_Sta_Tax_C;
    private DbsField pnd_Case_Fields_Pnd_Loc_Tax_C;

    private DbsGroup pnd_Mp;
    private DbsField pnd_Mp_Pnd_Plan;
    private DbsField pnd_Mp_Pnd_Plan_Type_Ind;
    private DbsField pnd_Mp_Pnd_Plan_Pct;
    private DbsField pnd_Mp_Pnd_Mtr;

    private DbsGroup ws_Var;
    private DbsField ws_Var_Ws_Gross_Amt;
    private DbsField ws_Var_Ws_Ind_Gross_Amt_Bkp;
    private DbsField ws_Var_Ws_Tin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0900 = new LdaTwrl0900();
        registerRecord(ldaTwrl0900);
        localVariables = new DbsRecord();
        pdaIaaatxia = new PdaIaaatxia(localVariables);
        pdaTwrapstp = new PdaTwrapstp(localVariables);
        pdaTwraplr2 = new PdaTwraplr2(localVariables);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Pnd_Cntl_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Pnd_Cntl_Max", "#CNTL-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Prev_Contract = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Ws_Pnd_Prev_Payee = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Payee", "#PREV-PAYEE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Pln_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pln_Cnt", "#PLN-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S", "#S", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_S1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S1", "#S1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Tran_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tran_Cv", "#TRAN-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Part_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Part_Cv", "#PART-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Ia_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ia_Break", "#IA-BREAK", FieldType.BOOLEAN, 1);

        pnd_Cntl = localVariables.newGroupArrayInRecord("pnd_Cntl", "#CNTL", new DbsArrayController(1, 7));
        pnd_Cntl_Pnd_Cntl_Text = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Cntl_Text1 = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Cntl_Text1", "#CNTL-TEXT1", FieldType.STRING, 26);
        pnd_Cntl_Pnd_Tran_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt_Cv", "#TRAN-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Cntl_Pnd_Part_Cnt_Cv = pnd_Cntl.newFieldInGroup("pnd_Cntl_Pnd_Part_Cnt_Cv", "#PART-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);

        pnd_Cntl_Pnd_Totals = pnd_Cntl.newGroupInGroup("pnd_Cntl_Pnd_Totals", "#TOTALS");
        pnd_Cntl_Pnd_Tran_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Part_Cnt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Part_Cnt", "#PART-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Cntl_Pnd_Gross_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Taxable_Amt = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Fed_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Cntl_Pnd_Sta_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Pnd_Loc_Tax = pnd_Cntl_Pnd_Totals.newFieldInGroup("pnd_Cntl_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Suny_Found = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Suny_Found", "#SUNY-FOUND", FieldType.BOOLEAN, 1);

        pnd_Case_Fields_Pnd_Totals_C = pnd_Case_Fields.newGroupInGroup("pnd_Case_Fields_Pnd_Totals_C", "#TOTALS-C");
        pnd_Case_Fields_Pnd_Tran_Cnt_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Tran_Cnt_C", "#TRAN-CNT-C", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Case_Fields_Pnd_Part_Cnt_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Part_Cnt_C", "#PART-CNT-C", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Case_Fields_Pnd_Gross_Amt_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Gross_Amt_C", "#GROSS-AMT-C", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Case_Fields_Pnd_Taxable_Amt_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Taxable_Amt_C", "#TAXABLE-AMT-C", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Case_Fields_Pnd_Fed_Tax_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Fed_Tax_C", "#FED-TAX-C", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Case_Fields_Pnd_Sta_Tax_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Sta_Tax_C", "#STA-TAX-C", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Case_Fields_Pnd_Loc_Tax_C = pnd_Case_Fields_Pnd_Totals_C.newFieldInGroup("pnd_Case_Fields_Pnd_Loc_Tax_C", "#LOC-TAX-C", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Mp = localVariables.newGroupArrayInRecord("pnd_Mp", "#MP", new DbsArrayController(1, 20));
        pnd_Mp_Pnd_Plan = pnd_Mp.newFieldInGroup("pnd_Mp_Pnd_Plan", "#PLAN", FieldType.STRING, 6);
        pnd_Mp_Pnd_Plan_Type_Ind = pnd_Mp.newFieldInGroup("pnd_Mp_Pnd_Plan_Type_Ind", "#PLAN-TYPE-IND", FieldType.STRING, 1);
        pnd_Mp_Pnd_Plan_Pct = pnd_Mp.newFieldInGroup("pnd_Mp_Pnd_Plan_Pct", "#PLAN-PCT", FieldType.PACKED_DECIMAL, 7, 4);
        pnd_Mp_Pnd_Mtr = pnd_Mp.newFieldInGroup("pnd_Mp_Pnd_Mtr", "#MTR", FieldType.PACKED_DECIMAL, 3);

        ws_Var = localVariables.newGroupInRecord("ws_Var", "WS-VAR");
        ws_Var_Ws_Gross_Amt = ws_Var.newFieldInGroup("ws_Var_Ws_Gross_Amt", "WS-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        ws_Var_Ws_Ind_Gross_Amt_Bkp = ws_Var.newFieldInGroup("ws_Var_Ws_Ind_Gross_Amt_Bkp", "WS-IND-GROSS-AMT-BKP", FieldType.PACKED_DECIMAL, 13, 2);
        ws_Var_Ws_Tin = ws_Var.newFieldInGroup("ws_Var_Ws_Tin", "WS-TIN", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0900.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Pnd_Cntl_Max.setInitialValue(7);
        pnd_Ws_Pnd_Ia_Break.setInitialValue(true);
        pnd_Cntl_Pnd_Cntl_Text.getValue(1).setInitialValue("* All Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(2).setInitialValue("Active Payments");
        pnd_Cntl_Pnd_Cntl_Text.getValue(3).setInitialValue("- Non NY and");
        pnd_Cntl_Pnd_Cntl_Text.getValue(4).setInitialValue("- NY Bypassed");
        pnd_Cntl_Pnd_Cntl_Text.getValue(5).setInitialValue("= SUNY-CUNY Communications");
        pnd_Cntl_Pnd_Cntl_Text.getValue(6).setInitialValue("- SUNY-CUNY");
        pnd_Cntl_Pnd_Cntl_Text.getValue(7).setInitialValue("- IA Multiplan Non SUNY");
        pnd_Cntl_Pnd_Cntl_Text1.getValue(7).setInitialValue("  & Missing Invalid Plans.");
        pnd_Cntl_Pnd_Tran_Cnt_Cv.setInitialAttributeValue("AD=I)#CNTL.#TRAN-CNT-CV(2)	(AD=I)#CNTL.#TRAN-CNT-CV(3)	(AD=I)#CNTL.#TRAN-CNT-CV(4)	(AD=I)#CNTL.#TRAN-CNT-CV(5)	(AD=I)#CNTL.#TRAN-CNT-CV(6)	(AD=N)#CNTL.#TRAN-CNT-CV(7)	(AD=N");
        pnd_Cntl_Pnd_Part_Cnt_Cv.setInitialAttributeValue("AD=N)#CNTL.#PART-CNT-CV(2)	(AD=I)#CNTL.#PART-CNT-CV(3)	(AD=N)#CNTL.#PART-CNT-CV(4)	(AD=N)#CNTL.#PART-CNT-CV(5)	(AD=I)#CNTL.#PART-CNT-CV(6)	(AD=N)#CNTL.#PART-CNT-CV(7)	(AD=N");
        pnd_Mp_Pnd_Plan.getValue(1).setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5505() throws Exception
    {
        super("Twrp5505");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Summary of SUNY - CUNY Transactions' 120T 'Report: RPT1' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaTwrapstp.getPnd_Twrapstp_Pnd_Function().setValue(1);                                                                                                           //Natural: ASSIGN #TWRAPSTP.#FUNCTION := 1
        pdaTwraplr2.getTwraplr2_Pnd_Function().setValue(2);                                                                                                               //Natural: ASSIGN TWRAPLR2.#FUNCTION := 2
        pdaTwraplr2.getTwraplr2_Pnd_Act_Ind().setValue("A");                                                                                                              //Natural: ASSIGN TWRAPLR2.#ACT-IND := 'A'
        pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().setValue("H'FF'");                                                                                                         //Natural: ASSIGN TWRAPLR2.#PLAN-NUM := H'FF'
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #XTAXYR-F94
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0900.getPnd_Xtaxyr_F94())))
        {
            CheckAtStartofData333();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(pnd_Ws_Pnd_Prev_Contract.equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr()) && pnd_Ws_Pnd_Prev_Payee.equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde()))) //Natural: AT START OF DATA;//Natural: AT BREAK OF #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR;//Natural: IF #WS.#PREV-CONTRACT = #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR AND #WS.#PREV-PAYEE = #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Prev_Contract.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                                 //Natural: ASSIGN #WS.#PREV-CONTRACT := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
                pnd_Ws_Pnd_Prev_Payee.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                                       //Natural: ASSIGN #WS.#PREV-PAYEE := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
                pnd_Ws_Pnd_Ia_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #WS.#IA-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("M") && ((((ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("E")  //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'M' AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'E' OR = 'M' OR = 'S' OR = 'T' OR = 'U'
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("M")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("S")) 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("T")) || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("U")))))
                {
                    //*  W-2
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_S1.setValue(1);                                                                                                                                //Natural: ASSIGN #WS.#S1 := 1
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                sub_Add_To_Array();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals(" ") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Pymnt_Status().getValue(pnd_Ws_Pnd_I).equals("C"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-PYMNT-STATUS ( #I ) = ' ' OR = 'C'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_S1.setValue(2);                                                                                                                                //Natural: ASSIGN #WS.#S1 := 2
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                sub_Add_To_Array();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NON NY PAYMENTS
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Type().equals("1") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Residency_Code().equals("35"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-RESIDENCY-TYPE = '1' AND #XTAXYR-F94.TWRPYMNT-RESIDENCY-CODE = '35'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_S1.setValue(3);                                                                                                                            //Natural: ASSIGN #WS.#S1 := 3
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                    sub_Add_To_Array();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  NY BYPASSED
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde().greater("02") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Company_Cde().equals("L")        //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYEE-CDE > '02' OR #XTAXYR-F94.TWRPYMNT-COMPANY-CDE = 'L' OR #XTAXYR-F94.TWRPYMNT-ORGN-SRCE-CDE ( #I ) = 'CP' OR = 'NV' OR = 'VL' OR = 'AM' OR #XTAXYR-F94.TWRPYMNT-UPDTE-SRCE-CDE ( #I ) = 'CP' OR = 'NV' OR = 'VL' OR = 'AM'
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("CP") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("NV") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("VL") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Orgn_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("AM") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("CP") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("NV") 
                    || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("VL") || ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Updte_Srce_Cde().getValue(pnd_Ws_Pnd_I).equals("AM")))
                {
                    pnd_Ws_Pnd_S1.setValue(4);                                                                                                                            //Natural: ASSIGN #WS.#S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                    sub_Add_To_Array();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I).equals("E") && ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I).equals("C"))) //Natural: IF #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) = 'E' AND #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I ) = 'C'
                {
                                                                                                                                                                          //Natural: PERFORM ACCESS-IA
                    sub_Access_Ia();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaIaaatxia.getIaaatxia_Pnd_Successful().getBoolean() && pdaIaaatxia.getIaaatxia_Pnd_Settle_Option().notEquals("21")))                  //Natural: IF IAAATXIA.#SUCCESSFUL AND IAAATXIA.#SETTLE-OPTION NE '21'
                    {
                        pnd_Ws_Pnd_S1.setValue(4);                                                                                                                        //Natural: ASSIGN #WS.#S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                        sub_Add_To_Array();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUNY-P-S
                sub_Suny_P_S();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pdaTwrapstp.getPnd_Twrapstp_Pnd_Ltr_Include().getBoolean())))                                                                            //Natural: IF NOT #TWRAPSTP.#LTR-INCLUDE
                {
                    pnd_Ws_Pnd_S1.setValue(4);                                                                                                                            //Natural: ASSIGN #WS.#S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
                    sub_Add_To_Array();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_Ws_Pnd_I).equals("MLTPLN")))                                                     //Natural: IF #XTAXYR-F94.#OMNI-PLAN ( #I ) = 'MLTPLN'
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-MULTI-PLAN-PAYMENT
                    sub_Process_Multi_Plan_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-PLAN-PAYMENT
                    sub_Process_Plan_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-ARRAY
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-ARRAY-C
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TO-LINE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *****************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCESS-IA
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MULTI-PLAN-PAYMENT
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PLAN-PAYMENT
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLAN
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUNY-P-S
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Add_To_Array() throws Exception                                                                                                                      //Natural: ADD-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1).nadd(1);                                                                                                            //Natural: ADD 1 TO #CNTL.#TRAN-CNT ( #S1 )
        pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #CNTL.#GROSS-AMT ( #S1 )
        pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                 //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #CNTL.#FED-TAX ( #S1 )
        pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #CNTL.#STA-TAX ( #S1 )
        pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1).nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                   //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #CNTL.#LOC-TAX ( #S1 )
        if (condition((pnd_Ws_Pnd_S1.equals(4)) && (pnd_Ws_Pnd_I.equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_C_Twrpymnt_Payments()))))                                       //Natural: IF ( #S1 = 4 ) AND ( #I = #XTAXYR-F94.#C-TWRPYMNT-PAYMENTS )
        {
            ws_Var_Ws_Gross_Amt.compute(new ComputeParameters(false, ws_Var_Ws_Gross_Amt), pnd_Cntl_Pnd_Gross_Amt.getValue(4).subtract(ws_Var_Ws_Gross_Amt));             //Natural: ASSIGN WS-GROSS-AMT := #CNTL.#GROSS-AMT ( 4 ) - WS-GROSS-AMT
            getReports().write(0, "SUNY:TIN:",ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr(),"IND GROSS AMT:",ws_Var_Ws_Gross_Amt,"TOTAL-GROSS:",pnd_Cntl_Pnd_Gross_Amt.getValue(4),  //Natural: WRITE 'SUNY:TIN:' #XTAXYR-F94.TWRPYMNT-TAX-ID-NBR 'IND GROSS AMT:' WS-GROSS-AMT 'TOTAL-GROSS:' #CNTL.#GROSS-AMT ( 4 )
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            ws_Var_Ws_Gross_Amt.setValue(pnd_Cntl_Pnd_Gross_Amt.getValue(4));                                                                                             //Natural: ASSIGN WS-GROSS-AMT := #CNTL.#GROSS-AMT ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Add_To_Array_C() throws Exception                                                                                                                    //Natural: ADD-TO-ARRAY-C
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Case_Fields_Pnd_Tran_Cnt_C.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TRAN-CNT-C
        pnd_Case_Fields_Pnd_Gross_Amt_C.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I));                                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) TO #GROSS-AMT-C
        pnd_Case_Fields_Pnd_Fed_Tax_C.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I));                                                //Natural: ADD #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) TO #FED-TAX-C
        pnd_Case_Fields_Pnd_Sta_Tax_C.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I));                                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) TO #STA-TAX-C
        pnd_Case_Fields_Pnd_Loc_Tax_C.nadd(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I));                                                  //Natural: ADD #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) TO #LOC-TAX-C
    }
    private void sub_Add_To_Line() throws Exception                                                                                                                       //Natural: ADD-TO-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1).nadd(pnd_Case_Fields_Pnd_Tran_Cnt_C);                                                                               //Natural: ADD #TRAN-CNT-C TO #CNTL.#TRAN-CNT ( #S1 )
        pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1).nadd(pnd_Case_Fields_Pnd_Gross_Amt_C);                                                                             //Natural: ADD #GROSS-AMT-C TO #CNTL.#GROSS-AMT ( #S1 )
        pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1).nadd(pnd_Case_Fields_Pnd_Fed_Tax_C);                                                                                 //Natural: ADD #FED-TAX-C TO #CNTL.#FED-TAX ( #S1 )
        pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1).nadd(pnd_Case_Fields_Pnd_Sta_Tax_C);                                                                                 //Natural: ADD #STA-TAX-C TO #CNTL.#STA-TAX ( #S1 )
        pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1).nadd(pnd_Case_Fields_Pnd_Loc_Tax_C);                                                                                 //Natural: ADD #LOC-TAX-C TO #CNTL.#LOC-TAX ( #S1 )
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #WS.#S1 = 1 TO #CNTL-MAX
        for (pnd_Ws_Pnd_S1.setValue(1); condition(pnd_Ws_Pnd_S1.lessOrEqual(pnd_Ws_Const_Pnd_Cntl_Max)); pnd_Ws_Pnd_S1.nadd(1))
        {
            pnd_Ws_Pnd_Tran_Cv.setValue(pnd_Cntl_Pnd_Tran_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#TRAN-CV := #CNTL.#TRAN-CNT-CV ( #S1 )
            pnd_Ws_Pnd_Part_Cv.setValue(pnd_Cntl_Pnd_Part_Cnt_Cv.getValue(pnd_Ws_Pnd_S1));                                                                                //Natural: ASSIGN #WS.#PART-CV := #CNTL.#PART-CNT-CV ( #S1 )
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",                         //Natural: DISPLAY ( 1 ) ( HC = R ES = ON ) '/' #CNTL-TEXT ( #S1 ) / '/' #CNTL-TEXT1 ( #S1 ) '/Trans Count' #CNTL.#TRAN-CNT ( #S1 ) ( CV = #TRAN-CV ) 'Nbr of/Part.' #CNTL.#PART-CNT ( #S1 ) ( CV = #PART-CV ) '/Gross Amount' #CNTL.#GROSS-AMT ( #S1 ) '/Federal Tax' #CNTL.#FED-TAX ( #S1 ) '/State Tax' #CNTL.#STA-TAX ( #S1 ) '/Local Tax' #CNTL.#LOC-TAX ( #S1 )
            		pnd_Cntl_Pnd_Cntl_Text.getValue(pnd_Ws_Pnd_S1),NEWLINE,"/",
            		pnd_Cntl_Pnd_Cntl_Text1.getValue(pnd_Ws_Pnd_S1),"/Trans Count",
            		pnd_Cntl_Pnd_Tran_Cnt.getValue(pnd_Ws_Pnd_S1), pnd_Ws_Pnd_Tran_Cv,"Nbr of/Part.",
            		pnd_Cntl_Pnd_Part_Cnt.getValue(pnd_Ws_Pnd_S1), pnd_Ws_Pnd_Part_Cv,"/Gross Amount",
            		pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1),"/Federal Tax",
            		pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1),"/State Tax",
            		pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1),"/Local Tax",
            		pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Access_Ia() throws Exception                                                                                                                         //Natural: ACCESS-IA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        if (condition(pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Ppcn_Nbr().notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr()) || pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Payee_Cde_A().notEquals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde()))) //Natural: IF IAAATXIA.#CNTRCT-PPCN-NBR NE #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR OR IAAATXIA.#CNTRCT-PAYEE-CDE-A NE #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
        {
            pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Ppcn_Nbr().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Contract_Nbr());                                                //Natural: ASSIGN IAAATXIA.#CNTRCT-PPCN-NBR := #XTAXYR-F94.TWRPYMNT-CONTRACT-NBR
            pdaIaaatxia.getIaaatxia_Pnd_Cntrct_Payee_Cde_A().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payee_Cde());                                                //Natural: ASSIGN IAAATXIA.#CNTRCT-PAYEE-CDE-A := #XTAXYR-F94.TWRPYMNT-PAYEE-CDE
            DbsUtil.callnat(Iaantxi1.class , getCurrentProcessState(), pdaIaaatxia.getIaaatxia());                                                                        //Natural: CALLNAT 'IAANTXI1' USING IAAATXIA
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Multi_Plan_Payment() throws Exception                                                                                                        //Natural: PROCESS-MULTI-PLAN-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        if (condition(pnd_Ws_Pnd_Ia_Break.getBoolean()))                                                                                                                  //Natural: IF #IA-BREAK
        {
            pnd_Ws_Pnd_Ia_Break.reset();                                                                                                                                  //Natural: RESET #IA-BREAK #MP ( * )
            pnd_Mp.getValue("*").reset();
                                                                                                                                                                          //Natural: PERFORM ACCESS-IA
            sub_Access_Ia();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaIaaatxia.getIaaatxia_Pnd_Plan_Count().equals(getZero())))                                                                                    //Natural: IF IAAATXIA.#PLAN-COUNT = 0
            {
                pnd_Ws_Pnd_Pln_Cnt.setValue(1);                                                                                                                           //Natural: ASSIGN #WS.#PLN-CNT := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Pln_Cnt.setValue(pdaIaaatxia.getIaaatxia_Pnd_Plan_Count());                                                                                    //Natural: ASSIGN #WS.#PLN-CNT := IAAATXIA.#PLAN-COUNT
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #WS.#S = 1 TO #WS.#PLN-CNT
            for (pnd_Ws_Pnd_S.setValue(1); condition(pnd_Ws_Pnd_S.lessOrEqual(pnd_Ws_Pnd_Pln_Cnt)); pnd_Ws_Pnd_S.nadd(1))
            {
                //*  CLIENT ID
                //*  NOT SUNY
                if (condition(pdaIaaatxia.getIaaatxia_Pnd_Sub_Plan_Nmbr().getValue(pnd_Ws_Pnd_S).equals(" ")))                                                            //Natural: IF IAAATXIA.#SUB-PLAN-NMBR ( #S ) = ' '
                {
                    pnd_Mp_Pnd_Plan.getValue(pnd_Ws_Pnd_S).setValue(pdaIaaatxia.getIaaatxia_Pnd_Plan_Nmbr().getValue(pnd_Ws_Pnd_S));                                      //Natural: ASSIGN #MP.#PLAN ( #S ) := IAAATXIA.#PLAN-NMBR ( #S )
                    pnd_Mp_Pnd_Plan_Type_Ind.getValue(pnd_Ws_Pnd_S).setValue(" ");                                                                                        //Natural: ASSIGN #MP.#PLAN-TYPE-IND ( #S ) := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Mp_Pnd_Plan.getValue(pnd_Ws_Pnd_S).setValue(pdaIaaatxia.getIaaatxia_Pnd_Plan_Nmbr().getValue(pnd_Ws_Pnd_S));                                      //Natural: ASSIGN #MP.#PLAN ( #S ) := TWRAPLR2.#PLAN-NUM := IAAATXIA.#PLAN-NMBR ( #S )
                    pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().setValue(pdaIaaatxia.getIaaatxia_Pnd_Plan_Nmbr().getValue(pnd_Ws_Pnd_S));
                                                                                                                                                                          //Natural: PERFORM GET-PLAN
                    sub_Get_Plan();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Mp_Pnd_Plan_Type_Ind.getValue(pnd_Ws_Pnd_S).setValue(pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind());                                                  //Natural: ASSIGN #MP.#PLAN-TYPE-IND ( #S ) := TWRAPLR2.#PL-TYPE-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Mp_Pnd_Plan_Type_Ind.getValue(1,":",pnd_Ws_Pnd_Pln_Cnt).equals("C") || pnd_Mp_Pnd_Plan_Type_Ind.getValue(1,":",pnd_Ws_Pnd_Pln_Cnt).equals("S")  //Natural: IF #MP.#PLAN-TYPE-IND ( 1:#PLN-CNT ) = 'C' OR = 'S' OR = 'X'
            || pnd_Mp_Pnd_Plan_Type_Ind.getValue(1,":",pnd_Ws_Pnd_Pln_Cnt).equals("X")))
        {
            pnd_Case_Fields_Pnd_Suny_Found.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#SUNY-FOUND := TRUE
            pnd_Mp_Pnd_Plan_Pct.getValue(1,":",pnd_Ws_Pnd_Pln_Cnt).setValue(pdaIaaatxia.getIaaatxia_Pnd_Plan_Pct().getValue(1,":",pnd_Ws_Pnd_Pln_Cnt));                   //Natural: ASSIGN #MP.#PLAN-PCT ( 1:#PLN-CNT ) := IAAATXIA.#PLAN-PCT ( 1:#PLN-CNT )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_S1.setValue(4);                                                                                                                                    //Natural: ASSIGN #WS.#S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
            sub_Add_To_Array();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_S1.setValue(5);                                                                                                                                        //Natural: ASSIGN #WS.#S1 := 5
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
        sub_Add_To_Array();
        if (condition(Global.isEscape())) {return;}
        FOR04:                                                                                                                                                            //Natural: FOR #WS.#S = 1 TO #WS.#PLN-CNT
        for (pnd_Ws_Pnd_S.setValue(1); condition(pnd_Ws_Pnd_S.lessOrEqual(pnd_Ws_Pnd_Pln_Cnt)); pnd_Ws_Pnd_S.nadd(1))
        {
            if (condition(pnd_Mp_Pnd_Plan_Type_Ind.getValue(pnd_Ws_Pnd_S).equals("C") || pnd_Mp_Pnd_Plan_Type_Ind.getValue(pnd_Ws_Pnd_S).equals("S") ||                   //Natural: IF #MP.#PLAN-TYPE-IND ( #S ) = 'C' OR = 'S' OR = 'X'
                pnd_Mp_Pnd_Plan_Type_Ind.getValue(pnd_Ws_Pnd_S).equals("X")))
            {
                pnd_Ws_Pnd_S1.setValue(6);                                                                                                                                //Natural: ASSIGN #WS.#S1 := 6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_S1.setValue(7);                                                                                                                                //Natural: ASSIGN #WS.#S1 := 7
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1).compute(new ComputeParameters(true, pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1)), pnd_Cntl_Pnd_Gross_Amt.getValue(pnd_Ws_Pnd_S1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Gross_Amt().getValue(pnd_Ws_Pnd_I)).multiply(pnd_Mp_Pnd_Plan_Pct.getValue(pnd_Ws_Pnd_S)).divide(100)); //Natural: COMPUTE ROUNDED #CNTL.#GROSS-AMT ( #S1 ) := #CNTL.#GROSS-AMT ( #S1 ) + #XTAXYR-F94.TWRPYMNT-GROSS-AMT ( #I ) * #MP.#PLAN-PCT ( #S ) / 100
            pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1).compute(new ComputeParameters(true, pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1)), pnd_Cntl_Pnd_Fed_Tax.getValue(pnd_Ws_Pnd_S1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Fed_Wthld_Amt().getValue(pnd_Ws_Pnd_I)).multiply(pnd_Mp_Pnd_Plan_Pct.getValue(pnd_Ws_Pnd_S)).divide(100)); //Natural: COMPUTE ROUNDED #CNTL.#FED-TAX ( #S1 ) := #CNTL.#FED-TAX ( #S1 ) + #XTAXYR-F94.TWRPYMNT-FED-WTHLD-AMT ( #I ) * #MP.#PLAN-PCT ( #S ) / 100
            pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1).compute(new ComputeParameters(true, pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1)), pnd_Cntl_Pnd_Sta_Tax.getValue(pnd_Ws_Pnd_S1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_State_Wthld().getValue(pnd_Ws_Pnd_I)).multiply(pnd_Mp_Pnd_Plan_Pct.getValue(pnd_Ws_Pnd_S)).divide(100)); //Natural: COMPUTE ROUNDED #CNTL.#STA-TAX ( #S1 ) := #CNTL.#STA-TAX ( #S1 ) + #XTAXYR-F94.TWRPYMNT-STATE-WTHLD ( #I ) * #MP.#PLAN-PCT ( #S ) / 100
            pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1).compute(new ComputeParameters(true, pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1)), pnd_Cntl_Pnd_Loc_Tax.getValue(pnd_Ws_Pnd_S1).add(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Local_Wthld().getValue(pnd_Ws_Pnd_I)).multiply(pnd_Mp_Pnd_Plan_Pct.getValue(pnd_Ws_Pnd_S)).divide(100)); //Natural: COMPUTE ROUNDED #CNTL.#LOC-TAX ( #S1 ) := #CNTL.#LOC-TAX ( #S1 ) + #XTAXYR-F94.TWRPYMNT-LOCAL-WTHLD ( #I ) * #MP.#PLAN-PCT ( #S ) / 100
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Plan_Payment() throws Exception                                                                                                              //Natural: PROCESS-PLAN-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_Ws_Pnd_I))))                               //Natural: IF TWRAPLR2.#PLAN-NUM = #XTAXYR-F94.#OMNI-PLAN ( #I )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwraplr2.getTwraplr2_Pnd_Plan_Num().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Pnd_Omni_Plan().getValue(pnd_Ws_Pnd_I));                                        //Natural: ASSIGN TWRAPLR2.#PLAN-NUM := #XTAXYR-F94.#OMNI-PLAN ( #I )
                                                                                                                                                                          //Natural: PERFORM GET-PLAN
            sub_Get_Plan();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet562 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF TWRAPLR2.#PL-TYPE-IND;//Natural: VALUE 'C', 'S', 'X'
        if (condition((pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind().equals("C") || pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind().equals("S") || pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind().equals("X"))))
        {
            decideConditionsMet562++;
            pnd_Case_Fields_Pnd_Suny_Found.setValue(true);                                                                                                                //Natural: ASSIGN #CASE-FIELDS.#SUNY-FOUND := TRUE
            pnd_Ws_Pnd_S1.setValue(5);                                                                                                                                    //Natural: ASSIGN #WS.#S1 := 5
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
            sub_Add_To_Array();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_S1.setValue(6);                                                                                                                                    //Natural: ASSIGN #WS.#S1 := 6
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
            sub_Add_To_Array();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'I', 'M'
        else if (condition((pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind().equals("I") || pdaTwraplr2.getTwraplr2_Pnd_Pl_Type_Ind().equals("M"))))
        {
            decideConditionsMet562++;
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY-C
            sub_Add_To_Array_C();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_S1.setValue(4);                                                                                                                                    //Natural: ASSIGN #WS.#S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-ARRAY
            sub_Add_To_Array();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Plan() throws Exception                                                                                                                          //Natural: GET-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        DbsUtil.callnat(Twrnplr2.class , getCurrentProcessState(), pdaTwraplr2.getTwraplr2_Input_Parms(), new AttributeParameter("O"), pdaTwraplr2.getTwraplr2_Output_Data(),  //Natural: CALLNAT 'TWRNPLR2' USING TWRAPLR2.INPUT-PARMS ( AD = O ) TWRAPLR2.OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Suny_P_S() throws Exception                                                                                                                          //Natural: SUNY-P-S
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        if (condition(pdaTwrapstp.getPnd_Twrapstp_Pnd_Payment_Type().equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I))                 //Natural: IF #TWRAPSTP.#PAYMENT-TYPE = #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I ) AND #TWRAPSTP.#SETTLE-TYPE = #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I )
            && pdaTwrapstp.getPnd_Twrapstp_Pnd_Settle_Type().equals(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I))))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTwrapstp.getPnd_Twrapstp_Pnd_Payment_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Payment_Type().getValue(pnd_Ws_Pnd_I));                        //Natural: ASSIGN #TWRAPSTP.#PAYMENT-TYPE := #XTAXYR-F94.TWRPYMNT-PAYMENT-TYPE ( #I )
            pdaTwrapstp.getPnd_Twrapstp_Pnd_Settle_Type().setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Settle_Type().getValue(pnd_Ws_Pnd_I));                          //Natural: ASSIGN #TWRAPSTP.#SETTLE-TYPE := #XTAXYR-F94.TWRPYMNT-SETTLE-TYPE ( #I )
            DbsUtil.callnat(Twrnpstp.class , getCurrentProcessState(), pdaTwrapstp.getPnd_Twrapstp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrapstp.getPnd_Twrapstp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNPSTP' USING #TWRAPSTP.#INPUT-PARMS ( AD = O ) #TWRAPSTP.#OUTPUT-DATA ( AD = M )
                new AttributeParameter("M"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_NbrIsBreak = ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_Nbr().isBreak(endOfData);
        if (condition(ldaTwrl0900_getPnd_Xtaxyr_F94_Twrpymnt_Tax_Id_NbrIsBreak))
        {
            pnd_Cntl_Pnd_Part_Cnt.getValue(1).nadd(1);                                                                                                                    //Natural: ADD 1 TO #CNTL.#PART-CNT ( 1 )
            pnd_Cntl_Pnd_Part_Cnt.getValue(2).nadd(1);                                                                                                                    //Natural: ADD 1 TO #CNTL.#PART-CNT ( 2 )
            if (condition(pnd_Case_Fields_Pnd_Suny_Found.getBoolean()))                                                                                                   //Natural: IF #CASE-FIELDS.#SUNY-FOUND
            {
                pnd_Cntl_Pnd_Part_Cnt.getValue(5).nadd(1);                                                                                                                //Natural: ADD 1 TO #CNTL.#PART-CNT ( 5 )
                pnd_Ws_Pnd_S1.setValue(5);                                                                                                                                //Natural: ASSIGN #S1 := 5
                                                                                                                                                                          //Natural: PERFORM ADD-TO-LINE
                sub_Add_To_Line();
                if (condition(Global.isEscape())) {return;}
                //* * WS-GROSS-AMT := #CNTL.#GROSS-AMT(5) - WS-GROSS-AMT
                //* * WRITE 'SNJY:TIN:' WS-TIN
                //* *     '#GROSS-AMT-C:' #CASE-FIELDS.#GROSS-AMT-C
                //* *     '#FED-TAX-C:'   #CASE-FIELDS.#FED-TAX-C
                //* *     '#STA-TAX-C:'   #CASE-FIELDS.#STA-TAX-C
                //* *     '#LOC-TAX-C:'   #CASE-FIELDS.#LOC-TAX-C
                //* *     'IND GROSS AMT:' WS-GROSS-AMT
                //* *     'TOTAL-GROSS:' #CNTL.#GROSS-AMT(5)
                //* * 'GROSS(7):' #CNTL.#GROSS-AMT(7)
                //* * WS-GROSS-AMT := #CNTL.#GROSS-AMT(5)
                //* * WS-TIN    := ' '
                pnd_Ws_Pnd_S1.setValue(7);                                                                                                                                //Natural: ASSIGN #S1 := 7
                                                                                                                                                                          //Natural: PERFORM ADD-TO-LINE
                sub_Add_To_Line();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_S1.setValue(4);                                                                                                                                //Natural: ASSIGN #S1 := 4
                                                                                                                                                                          //Natural: PERFORM ADD-TO-LINE
                sub_Add_To_Line();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Case_Fields.reset();                                                                                                                                      //Natural: RESET #CASE-FIELDS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Summary of SUNY - CUNY Transactions",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, 
            new ReportEditMask ("9999"), new SignPosition (false),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ReportEmptyLineSuppression(true),"/",
            
        		pnd_Cntl_Pnd_Cntl_Text,NEWLINE,"/",
        		pnd_Cntl_Pnd_Cntl_Text1,"/Trans Count",
        		pnd_Cntl_Pnd_Tran_Cnt, pnd_Ws_Pnd_Tran_Cv,"Nbr of/Part.",
        		pnd_Cntl_Pnd_Part_Cnt, pnd_Ws_Pnd_Part_Cv,"/Gross Amount",
        		pnd_Cntl_Pnd_Gross_Amt,"/Federal Tax",
        		pnd_Cntl_Pnd_Fed_Tax,"/State Tax",
        		pnd_Cntl_Pnd_Sta_Tax,"/Local Tax",
        		pnd_Cntl_Pnd_Loc_Tax);
    }
    private void CheckAtStartofData333() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Tax_Year.setValue(ldaTwrl0900.getPnd_Xtaxyr_F94_Twrpymnt_Tax_Year());                                                                              //Natural: ASSIGN #WS.#TAX-YEAR := #XTAXYR-F94.TWRPYMNT-TAX-YEAR
        }                                                                                                                                                                 //Natural: END-START
    }
}
