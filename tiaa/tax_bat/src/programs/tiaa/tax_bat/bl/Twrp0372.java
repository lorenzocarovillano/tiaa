/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:29:54 PM
**        * FROM NATURAL PROGRAM : Twrp0372
************************************************************
**        * FILE NAME            : Twrp0372.java
**        * CLASS NAME           : Twrp0372
**        * INSTANCE NAME        : Twrp0372
************************************************************
************************************************************************
** PROGRAM NAME:  TWRP0372
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  B.KARCHEVSKY
** PURPOSE     :  CITE FILE DUMP BY ERRORS
**             :  (CITED TRANSACTIONS DISPLAY)
************************************************************************
** MODIFICATION LOG
** ----------------
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0372 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9900 ldaTwrl9900;
    private LdaTwrl9807 ldaTwrl9807;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Twrcited_Gross_Amt;
    private DbsField pnd_Err_Stats;
    private DbsField pnd_Error;
    private DbsField pnd_Er_Su;

    private DbsGroup pnd_Er_Su__R_Field_1;
    private DbsField pnd_Er_Su_Pnd_Er_Tbl;
    private DbsField pnd_Er_Su_Pnd_Er_Yr;
    private DbsField pnd_Er_No;
    private DbsField pnd_Er_No_Old;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9900 = new LdaTwrl9900();
        registerRecord(ldaTwrl9900);
        registerRecord(ldaTwrl9900.getVw_citation_View());
        ldaTwrl9807 = new LdaTwrl9807();
        registerRecord(ldaTwrl9807);
        registerRecord(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Twrcited_Gross_Amt = localVariables.newFieldArrayInRecord("pnd_Twrcited_Gross_Amt", "#TWRCITED-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 100));
        pnd_Err_Stats = localVariables.newFieldArrayInRecord("pnd_Err_Stats", "#ERR-STATS", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 100));
        pnd_Error = localVariables.newFieldArrayInRecord("pnd_Error", "#ERROR", FieldType.STRING, 37, new DbsArrayController(1, 100));
        pnd_Er_Su = localVariables.newFieldInRecord("pnd_Er_Su", "#ER-SU", FieldType.NUMERIC, 8);

        pnd_Er_Su__R_Field_1 = localVariables.newGroupInRecord("pnd_Er_Su__R_Field_1", "REDEFINE", pnd_Er_Su);
        pnd_Er_Su_Pnd_Er_Tbl = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Tbl", "#ER-TBL", FieldType.NUMERIC, 1);
        pnd_Er_Su_Pnd_Er_Yr = pnd_Er_Su__R_Field_1.newFieldInGroup("pnd_Er_Su_Pnd_Er_Yr", "#ER-YR", FieldType.NUMERIC, 4);
        pnd_Er_No = localVariables.newFieldInRecord("pnd_Er_No", "#ER-NO", FieldType.NUMERIC, 3);
        pnd_Er_No_Old = localVariables.newFieldInRecord("pnd_Er_No_Old", "#ER-NO-OLD", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9900.initializeValues();
        ldaTwrl9807.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0372() throws Exception
    {
        super("Twrp0372");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *                                                                                                                                                             //Natural: FORMAT ( 00 ) LS = 132 PS = 60;//Natural: FORMAT ( 01 ) LS = 132 PS = 60
        //* *
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #ER-NO CITATION-VIEW
        while (condition(getWorkFiles().read(1, pnd_Er_No, ldaTwrl9900.getVw_citation_View())))
        {
            CheckAtStartofData83();

            pnd_I.nadd(1);                                                                                                                                                //Natural: AT START OF DATA;//Natural: ADD 1 TO #I
            if (condition(pnd_Er_No_Old.notEquals(pnd_Er_No)))                                                                                                            //Natural: IF #ER-NO-OLD NE #ER-NO
            {
                getReports().write(1, "FOR ERROR",pnd_Er_No_Old,"NUMBER OF RECORDS =",pnd_Err_Stats.getValue(pnd_Er_No_Old.getInt() + 1), new ReportEditMask              //Natural: WRITE ( 01 ) 'FOR ERROR' #ER-NO-OLD 'NUMBER OF RECORDS =' #ERR-STATS ( #ER-NO-OLD ) 'GROSS AMOUNT =' #TWRCITED-GROSS-AMT ( #ER-NO-OLD )
                    ("-ZZZ,ZZZ,ZZ9"),"GROSS AMOUNT =",pnd_Twrcited_Gross_Amt.getValue(pnd_Er_No_Old.getInt() + 1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Er_No_Old.setValue(pnd_Er_No);                                                                                                                        //Natural: ASSIGN #ER-NO-OLD := #ER-NO
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Twrcited_Gross_Amt.getValue(pnd_Er_No.getInt() + 1).setValue(ldaTwrl9900.getCitation_View_Twrcited_Gross_Amt());                                          //Natural: ASSIGN #TWRCITED-GROSS-AMT ( #ER-NO ) := TWRCITED-GROSS-AMT
            pnd_Err_Stats.getValue(pnd_Er_No.getInt() + 1).nadd(1);                                                                                                       //Natural: ADD 1 TO #ERR-STATS ( #ER-NO )
            getReports().write(1, NEWLINE,ldaTwrl9900.getCitation_View_Twrcited_Contract_Nbr(),ldaTwrl9900.getCitation_View_Twrcited_Payee_Cde(),new ColumnSpacing(1),ldaTwrl9900.getCitation_View_Twrcited_Pymnt_Dte(),new  //Natural: WRITE ( 01 ) / TWRCITED-CONTRACT-NBR TWRCITED-PAYEE-CDE 1X TWRCITED-PYMNT-DTE 1X TWRCITED-TAX-ID-NBR TWRCITED-PROCESSED-IND 2X TWRCITED-TAX-ID-TYPE TWRCITED-GROSS-AMT TWRCITED-INT-AMT TWRCITED-IVC-AMT TWRCITED-FED-WTHLD-AMT TWRCITED-NRA-WTHLD-AMT TWRCITED-STATE-WTHLD TWRCITED-LOCAL-WTHLD 3X TWRCITED-RESIDENCY-CODE 2X TWRCITED-TAX-CITIZENSHIP
                ColumnSpacing(1),ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Nbr(),ldaTwrl9900.getCitation_View_Twrcited_Processed_Ind(),new ColumnSpacing(2),ldaTwrl9900.getCitation_View_Twrcited_Tax_Id_Type(),ldaTwrl9900.getCitation_View_Twrcited_Gross_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Int_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Ivc_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Fed_Wthld_Amt(),ldaTwrl9900.getCitation_View_Twrcited_Nra_Wthld_Amt(),ldaTwrl9900.getCitation_View_Twrcited_State_Wthld(),ldaTwrl9900.getCitation_View_Twrcited_Local_Wthld(),new 
                ColumnSpacing(3),ldaTwrl9900.getCitation_View_Twrcited_Residency_Code(),new ColumnSpacing(2),ldaTwrl9900.getCitation_View_Twrcited_Tax_Citizenship());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, "FOR ERROR",pnd_Er_No_Old,"NUMBER OF RECORDS =",pnd_Err_Stats.getValue(pnd_Er_No_Old.getInt() + 1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS AMOUNT =",pnd_Twrcited_Gross_Amt.getValue(pnd_Er_No_Old.getInt() + 1),  //Natural: WRITE ( 01 ) 'FOR ERROR' #ER-NO-OLD 'NUMBER OF RECORDS =' #ERR-STATS ( #ER-NO-OLD ) 'GROSS AMOUNT =' #TWRCITED-GROSS-AMT ( #ER-NO-OLD )
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //* *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"FOR YEAR :",pnd_Er_Su_Pnd_Er_Yr,NEWLINE,"TOTAL RECORDS OF THE INPUT FILE    :",    //Natural: WRITE ( 01 ) /// '*' ( 131 ) / 'FOR YEAR :' #ER-YR / 'TOTAL RECORDS OF THE INPUT FILE    :' #I
            pnd_I);
        if (Global.isEscape()) return;
        //* *PRINT ERROR STATISTICS
        FOR01:                                                                                                                                                            //Natural: FOR #ER-NO 0 98
        for (pnd_Er_No.setValue(0); condition(pnd_Er_No.lessOrEqual(98)); pnd_Er_No.nadd(1))
        {
            if (condition(pnd_Err_Stats.getValue(pnd_Er_No.getInt() + 1).greater(getZero())))                                                                             //Natural: IF #ERR-STATS ( #ER-NO ) > 0
            {
                getReports().write(1, pnd_Er_No," - ",pnd_Error.getValue(pnd_Er_No.getInt() + 1),":",pnd_Err_Stats.getValue(pnd_Er_No.getInt() + 1), new                  //Natural: WRITE ( 01 ) #ER-NO ' - ' #ERROR ( #ER-NO ) ':' #ERR-STATS ( #ER-NO ) ':' #TWRCITED-GROSS-AMT ( #ER-NO )
                    ReportEditMask ("-ZZZ,ZZZ,ZZ9"),":",pnd_Twrcited_Gross_Amt.getValue(pnd_Er_No.getInt() + 1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT 'Job Name:' *INIT-USER / 'PROGRAM: TWRP0372-01' 18X 'Tax Reporting And Withholding System' 30X 'PAGE:   ' *PAGE-NUMBER ( 01 ) / 33X 'Cited Transactions Loaded Into the Cite File' 27X 'Date: '*DATU / 5X 'ERROR: ' #ER-NO #ERROR ( #ER-NO ) // 'Contract Payee Pay   Taxpayer Proc Tax ID    Gross     Interest' '     IVC        Fed Tax        NRA       State       Local  Res Ctz' / 'Number   Code  Date  Ind.ID No Ind Type     Amount       Amount  ' '   Amount     Amount       Amount      Amount      Amount Cde Cde' / '                                                                '
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #LOAD-ERR-TBL
    }
    private void sub_Pnd_Load_Err_Tbl() throws Exception                                                                                                                  //Natural: #LOAD-ERR-TBL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Er_Su_Pnd_Er_Yr.setValue(ldaTwrl9900.getCitation_View_Twrcited_Tax_Year());                                                                                   //Natural: ASSIGN #ER-YR := TWRCITED-TAX-YEAR
        pnd_Er_Su_Pnd_Er_Tbl.setValue(7);                                                                                                                                 //Natural: MOVE 7 TO #ER-TBL
        ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().startDatabaseRead                                                                                             //Natural: READ TIRCNTL-ERROR-MSG-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-SEQ = #ER-SU
        (
        "READ02",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", ">=", pnd_Er_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_SEQ", "ASC") }
        );
        READ02:
        while (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().readNextRow("READ02")))
        {
            if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tbl_Nbr().notEquals(pnd_Er_Su_Pnd_Er_Tbl) || ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Tax_Year().notEquals(pnd_Er_Su_Pnd_Er_Yr))) //Natural: IF TIRCNTL-TBL-NBR NE #ER-TBL OR TIRCNTL-TAX-YEAR NE #ER-YR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Seq_Nbr().greater(98)))                                                                  //Natural: IF TIRCNTL-SEQ-NBR > 98
            {
                getReports().write(0, "TABLE FOR ERRORS OVERFLOW.",Global.getPROGRAM()," REQUIRE UPDATE",NEWLINE,"CONTACT DEVELOPERS");                                   //Natural: WRITE 'TABLE FOR ERRORS OVERFLOW.' *PROGRAM ' REQUIRE UPDATE' / 'CONTACT DEVELOPERS'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(90);  if (true) return;                                                                                                                 //Natural: TERMINATE 90
            }                                                                                                                                                             //Natural: END-IF
            pnd_Error.getValue(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Seq_Nbr().getInt() + 1).setValue(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr()); //Natural: MOVE TIRCNTL-ERROR-DESCR TO #ERROR ( TIRCNTL-SEQ-NBR )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Error.getValue(0 + 1).setValue("UNKNOWN ERROR NUMBER");                                                                                                       //Natural: ASSIGN #ERROR ( 0 ) := 'UNKNOWN ERROR NUMBER'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"Job Name:",Global.getINIT_USER(),NEWLINE,"PROGRAM: TWRP0372-01",new 
            ColumnSpacing(18),"Tax Reporting And Withholding System",new ColumnSpacing(30),"PAGE:   ",getReports().getPageNumberDbs(1),NEWLINE,new ColumnSpacing(33),"Cited Transactions Loaded Into the Cite File",new 
            ColumnSpacing(27),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(5),"ERROR: ",pnd_Er_No,pnd_Error,NEWLINE,NEWLINE,"Contract Payee Pay   Taxpayer Proc Tax ID    Gross     Interest",
            "     IVC        Fed Tax        NRA       State       Local  Res Ctz",NEWLINE,"Number   Code  Date  Ind.ID No Ind Type     Amount       Amount  ",
            "   Amount     Amount       Amount      Amount      Amount Cde Cde",NEWLINE,"                                                                ");
    }
    private void CheckAtStartofData83() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM #LOAD-ERR-TBL
            sub_Pnd_Load_Err_Tbl();
            if (condition(Global.isEscape())) {return;}
            pnd_Er_No_Old.setValue(pnd_Er_No);                                                                                                                            //Natural: ASSIGN #ER-NO-OLD := #ER-NO
        }                                                                                                                                                                 //Natural: END-START
    }
}
