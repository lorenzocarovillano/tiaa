/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:35:26 PM
**        * FROM NATURAL PROGRAM : Twrp3512
************************************************************
**        * FILE NAME            : Twrp3512.java
**        * CLASS NAME           : Twrp3512
**        * INSTANCE NAME        : Twrp3512
************************************************************
************************************************************************
** PROGRAM   : TWRP3512
** SYSTEM    : TAXWARS
** AUTHOR    : DIANE YHUN
** FUNCTION  : DISPLAY ACTIVE SUNY-CUNY PLANS.
**           : THIS IS A MONTHLY JOB.
** HISTORY   : 07-16-2012 SUNY-CUNY PHASE 2
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp3512 extends BLNatBase
{
    // Data Areas
    private LdaTwrlplrd ldaTwrlplrd;
    private PdaTwrarsdc pdaTwrarsdc;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Plan_Start;
    private DbsField pnd_Ws_Pnd_Plan_End;
    private DbsField pnd_State_Code;

    private DbsGroup pnd_State_Code__R_Field_1;
    private DbsField pnd_State_Code_Pnd_State_Code_1;
    private DbsField pnd_State_Code_Pnd_State_Code_2;
    private DbsField pnd_Plan_Cnt_Per_State;
    private DbsField pnd_Record_Count;
    private DbsField pnd_State_Count;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Temp_Space;
    private DbsField pnd_Ws_Date;
    private DbsField pnd_File_End;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrlplrd = new LdaTwrlplrd();
        registerRecord(ldaTwrlplrd);
        registerRecord(ldaTwrlplrd.getVw_twrlpln());
        localVariables = new DbsRecord();
        pdaTwrarsdc = new PdaTwrarsdc(localVariables);

        // Local Variables

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Plan_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Plan_Start", "#PLAN-START", FieldType.STRING, 7);
        pnd_Ws_Pnd_Plan_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Plan_End", "#PLAN-END", FieldType.STRING, 7);
        pnd_State_Code = localVariables.newFieldInRecord("pnd_State_Code", "#STATE-CODE", FieldType.STRING, 3);

        pnd_State_Code__R_Field_1 = localVariables.newGroupInRecord("pnd_State_Code__R_Field_1", "REDEFINE", pnd_State_Code);
        pnd_State_Code_Pnd_State_Code_1 = pnd_State_Code__R_Field_1.newFieldInGroup("pnd_State_Code_Pnd_State_Code_1", "#STATE-CODE-1", FieldType.STRING, 
            1);
        pnd_State_Code_Pnd_State_Code_2 = pnd_State_Code__R_Field_1.newFieldInGroup("pnd_State_Code_Pnd_State_Code_2", "#STATE-CODE-2", FieldType.STRING, 
            2);
        pnd_Plan_Cnt_Per_State = localVariables.newFieldInRecord("pnd_Plan_Cnt_Per_State", "#PLAN-CNT-PER-STATE", FieldType.NUMERIC, 6);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);
        pnd_State_Count = localVariables.newFieldInRecord("pnd_State_Count", "#STATE-COUNT", FieldType.NUMERIC, 6);
        pnd_Page_Number = localVariables.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 3);
        pnd_Temp_Space = localVariables.newFieldInRecord("pnd_Temp_Space", "#TEMP-SPACE", FieldType.STRING, 40);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.STRING, 8);
        pnd_File_End = localVariables.newFieldInRecord("pnd_File_End", "#FILE-END", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrlplrd.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Pnd_Plan_Start.setInitialValue("ATX011");
        pnd_Ws_Pnd_Plan_End.setInitialValue("ATX011");
        pnd_Plan_Cnt_Per_State.setInitialValue(0);
        pnd_Record_Count.setInitialValue(0);
        pnd_State_Count.setInitialValue(0);
        pnd_Page_Number.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Twrp3512() throws Exception
    {
        super("Twrp3512");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP3512", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Ws_Date.setValue(Global.getDATU());                                                                                                                           //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: MOVE *DATU TO #WS-DATE
                                                                                                                                                                          //Natural: PERFORM INTIALIZE-SECTION
        sub_Intialize_Section();
        if (condition(Global.isEscape())) {return;}
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Plan_Start,7,1);                                                                                           //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#PLAN-START,7,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Plan_End,7,1);                                                                                            //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#PLAN-END,7,1 )
        ldaTwrlplrd.getVw_twrlpln().startDatabaseRead                                                                                                                     //Natural: READ TWRLPLN BY RT-SUPER1 = #WS.#PLAN-START THRU #WS.#PLAN-END
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Ws_Pnd_Plan_Start, "And", WcType.BY) ,
        new Wc("RT_SUPER1", "<=", pnd_Ws_Pnd_Plan_End, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        READ01:
        while (condition(ldaTwrlplrd.getVw_twrlpln().readNextRow("READ01")))
        {
            if (condition(ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Ind().notEquals(true)))                                                                                       //Natural: IF RT-PL-COMM-IND NOT = TRUE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res(), ldaTwrlplrd.getTwrlpln_Rt_Plan(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Name(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Type(),  //Natural: END-ALL
                ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Ind(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Excl_Ind(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Hybrid_Ind(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Srce(), 
                ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Name());
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res(), ldaTwrlplrd.getTwrlpln_Rt_Plan());                                                                         //Natural: SORT BY RT-PL-RES RT-PLAN USING RT-PL-NAME RT-PL-TYPE RT-PL-COMM-IND RT-PL-EXCL-IND RT-PL-HYBRID-IND RT-PL-SRCE RT-PL-COMM-NAME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res(), ldaTwrlplrd.getTwrlpln_Rt_Plan(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Name(), 
            ldaTwrlplrd.getTwrlpln_Rt_Pl_Type(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Ind(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Excl_Ind(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Hybrid_Ind(), 
            ldaTwrlplrd.getTwrlpln_Rt_Pl_Srce(), ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Name())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
            pnd_Plan_Cnt_Per_State.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PLAN-CNT-PER-STATE
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            if (condition(pnd_Record_Count.equals(1)))                                                                                                                    //Natural: IF #RECORD-COUNT = 1
            {
                pnd_State_Code.setValue(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res());                                                                                              //Natural: ASSIGN #STATE-CODE := RT-PL-RES
                pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Code().setValue(pnd_State_Code_Pnd_State_Code_2);                                                                    //Natural: ASSIGN #I-RSDNCY-CODE := #STATE-CODE-2
                DbsUtil.callnat(Twrnrsdc.class , getCurrentProcessState(), pdaTwrarsdc.getTwrarsdc());                                                                    //Natural: CALLNAT 'TWRNRSDC' USING TWRARSDC
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROLS-HEADER
                sub_Write_Controls_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF RT-PL-RES
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            pnd_File_End.setValue("Y");                                                                                                                                   //Natural: ASSIGN #FILE-END := 'Y'
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROLS
            sub_Write_Controls();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INTIALIZE-SECTION
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADINGS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BODY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTAL-PLAN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROLS-HEADER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROLS
        //* ***********************************************************************
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Intialize_Section() throws Exception                                                                                                                 //Natural: INTIALIZE-SECTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_File_End.setValue("N");                                                                                                                                       //Natural: ASSIGN #FILE-END := 'N'
        pdaTwrarsdc.getTwrarsdc_Pnd_Input_Parms().reset();                                                                                                                //Natural: RESET #INPUT-PARMS
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Tax_Year().compute(new ComputeParameters(false, pdaTwrarsdc.getTwrarsdc_Pnd_I_Tax_Year()), Global.getDATN().divide(10000));         //Natural: ASSIGN #I-TAX-YEAR := *DATN / 10000
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Ctznshp().setValue("01");                                                                                                           //Natural: ASSIGN #I-CTZNSHP := '01'
        pdaTwrarsdc.getTwrarsdc_Pnd_I_1078_Ind().setValue("Y");                                                                                                           //Natural: ASSIGN #I-1078-IND := 'Y'
        pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Type().setValue("1");                                                                                                        //Natural: ASSIGN #I-RSDNCY-TYPE := '1'
    }
    private void sub_Write_Headings() throws Exception                                                                                                                    //Natural: WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        pnd_Page_Number.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name: PTW1211M",new ColumnSpacing(33),"Tax Reporting And Withholding","System",new             //Natural: WRITE ( 1 ) NOTITLE NOHDR 'Job Name: PTW1211M' 33X 'Tax Reporting And Withholding' 'System' 30X 'Page:' #PAGE-NUMBER
            ColumnSpacing(30),"Page:",pnd_Page_Number);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"Program:",Global.getPROGRAM(),new ColumnSpacing(28),"Plans Selected for Participant","Letter Reporting",new           //Natural: WRITE ( 1 ) 'Program:' *PROGRAM 28X 'Plans Selected for Participant' 'Letter Reporting' 25X 'Date:' #WS-DATE
            ColumnSpacing(25),"Date:",pnd_Ws_Date);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"        ");                                                                                                           //Natural: WRITE ( 1 ) '        '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),"State:",ldaTwrlplrd.getTwrlpln_Rt_Pl_Res(),new ColumnSpacing(1),"-",new ColumnSpacing(1),        //Natural: WRITE ( 1 ) 1X 'State:' RT-PL-RES 1X '-' 1X #O-RSDNCY-DESC
            pdaTwrarsdc.getTwrarsdc_Pnd_O_Rsdncy_Desc());
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"        ");                                                                                                           //Natural: WRITE ( 1 ) '        '
        if (Global.isEscape()) return;
    }
    private void sub_Write_Body() throws Exception                                                                                                                        //Natural: WRITE-BODY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(1, "Plan/Nbr.",                                                                                                                              //Natural: DISPLAY ( 1 ) 'Plan/Nbr.' RT-PLAN 'Plan Name/Letter Plan Name' RT-PL-NAME 'Comm/Ind' RT-PL-COMM-IND 'Emp. Type/Ind' RT-PL-TYPE 'Exc/Ind' RT-PL-EXCL-IND ( EM = N/Y ) 'Hybrid/Ind' RT-PL-HYBRID-IND ( EM = N/Y ) '/Source' RT-PL-SRCE
        		ldaTwrlplrd.getTwrlpln_Rt_Plan(),"Plan Name/Letter Plan Name",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Name(),"Comm/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Ind().getBoolean(),"Emp. Type/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Type(),"Exc/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Excl_Ind().getBoolean(), new ReportEditMask ("N/Y"),"Hybrid/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Hybrid_Ind().getBoolean(), new ReportEditMask ("N/Y"),"/Source",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Srce());
        if (Global.isEscape()) return;
        getReports().display(1, new ColumnSpacing(7),ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Name());                                                                           //Natural: DISPLAY ( 1 ) 7X RT-PL-COMM-NAME
        if (Global.isEscape()) return;
    }
    private void sub_Write_Total_Plan() throws Exception                                                                                                                  //Natural: WRITE-TOTAL-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Temp_Space.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pdaTwrarsdc.getTwrarsdc_Pnd_O_Rsdncy_Desc(), ") = "));                               //Natural: COMPRESS '(' #O-RSDNCY-DESC ') = ' INTO #TEMP-SPACE LEAVING NO SPACE
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(8),"Total Number of Plans",pnd_Temp_Space,new ColumnSpacing(1),pnd_Plan_Cnt_Per_State,               //Natural: WRITE ( 1 ) 8X 'Total Number of Plans' #TEMP-SPACE 1X #PLAN-CNT-PER-STATE ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Write_Controls_Header() throws Exception                                                                                                             //Natural: WRITE-CONTROLS-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, new ColumnSpacing(2),"CNT",new ColumnSpacing(11),"STATES",new ColumnSpacing(21),"TOTAL PLANS");                                             //Natural: WRITE 2X 'CNT' 11X 'STATES' 21X 'TOTAL PLANS'
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(1),"------",new ColumnSpacing(1),"-----------------------------",new ColumnSpacing(3),"---------------");                 //Natural: WRITE 1X '------' 1X '-----------------------------' 3X '---------------'
        if (Global.isEscape()) return;
    }
    private void sub_Write_Controls() throws Exception                                                                                                                    //Natural: WRITE-CONTROLS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_File_End.equals("Y")))                                                                                                                          //Natural: IF #FILE-END = 'Y'
        {
            getReports().write(0, "------------",new ColumnSpacing(27),"----------------");                                                                               //Natural: WRITE '------------' 27X '----------------'
            if (Global.isEscape()) return;
            getReports().write(0, "GRAND TOTAL:",new ColumnSpacing(32),pnd_Record_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                             //Natural: WRITE 'GRAND TOTAL:' 32X #RECORD-COUNT ( EM = ZZZ,ZZZ,ZZ9 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_State_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #STATE-COUNT
            getReports().write(0, pnd_State_Count,ldaTwrlplrd.getTwrlpln_Rt_Pl_Res(),pdaTwrarsdc.getTwrarsdc_Pnd_O_Rsdncy_Desc(),pnd_Plan_Cnt_Per_State,                  //Natural: WRITE #STATE-COUNT RT-PL-RES #O-RSDNCY-DESC #PLAN-CNT-PER-STATE ( EM = ZZZ,ZZZ,ZZ9 )
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADINGS
                    sub_Write_Headings();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "ON ERROR FOR PLAN",ldaTwrlplrd.getTwrlpln_Rt_Plan(),"; STATE= ",pdaTwrarsdc.getTwrarsdc_Pnd_O_Rsdncy_Desc());                              //Natural: WRITE 'ON ERROR FOR PLAN' RT-PLAN '; STATE= ' #O-RSDNCY-DESC
        getReports().write(0, "ERR: ",Global.getERROR_NR());                                                                                                              //Natural: WRITE '=' *ERROR-NR
        getReports().write(0, "ELIN: ",Global.getERROR_LINE());                                                                                                           //Natural: WRITE '=' *ERROR-LINE
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 99
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaTwrlplrd_getTwrlpln_Rt_Pl_ResIsBreak = ldaTwrlplrd.getTwrlpln_Rt_Pl_Res().isBreak(endOfData);
        if (condition(ldaTwrlplrd_getTwrlpln_Rt_Pl_ResIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTAL-PLAN
            sub_Write_Total_Plan();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROLS
            sub_Write_Controls();
            if (condition(Global.isEscape())) {return;}
            pnd_Plan_Cnt_Per_State.reset();                                                                                                                               //Natural: RESET #PLAN-CNT-PER-STATE #I-RSDNCY-CODE #OUTPUT-DATA
            pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Code().reset();
            pdaTwrarsdc.getTwrarsdc_Pnd_Output_Data().reset();
            pnd_State_Code.setValue(ldaTwrlplrd.getTwrlpln_Rt_Pl_Res());                                                                                                  //Natural: ASSIGN #STATE-CODE := RT-PL-RES
            pdaTwrarsdc.getTwrarsdc_Pnd_I_Rsdncy_Code().setValue(pnd_State_Code_Pnd_State_Code_2);                                                                        //Natural: ASSIGN #I-RSDNCY-CODE := #STATE-CODE-2
            DbsUtil.callnat(Twrnrsdc.class , getCurrentProcessState(), pdaTwrarsdc.getTwrarsdc());                                                                        //Natural: CALLNAT 'TWRNRSDC' USING TWRARSDC
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "Plan/Nbr.",
        		ldaTwrlplrd.getTwrlpln_Rt_Plan(),"Plan Name/Letter Plan Name",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Name(),"Comm/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Comm_Ind().getBoolean(),"Emp. Type/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Type(),"Exc/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Excl_Ind().getBoolean(), new ReportEditMask ("N/Y"),"Hybrid/Ind",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Hybrid_Ind().getBoolean(), new ReportEditMask ("N/Y"),"/Source",
        		ldaTwrlplrd.getTwrlpln_Rt_Pl_Srce());
    }
}
