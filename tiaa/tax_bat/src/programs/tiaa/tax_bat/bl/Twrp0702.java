/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:57 PM
**        * FROM NATURAL PROGRAM : Twrp0702
************************************************************
**        * FILE NAME            : Twrp0702.java
**        * CLASS NAME           : Twrp0702
**        * INSTANCE NAME        : Twrp0702
************************************************************
*********************************************************
** PROGRAM     :  TWRP0702
** SYSTEM      :  TAX REPORTING SYSTEM
** AUTHOR      :  MICHAEL SUPONITSKY
** PURPOSE     :  TRANSACTION FILE EDIT\VALIDATION
**             :  PROCESSING.
** INPUT       :  1.TRANSACTION FILE EXTRACT FROM FEEDERS
** (FILES)  -  :    (DETAIL) - FILE# 1
**             :  2.TRANSACTION FILE EXTRACT FROM FEEDERS
**             :    (SUMMARY)- FILE# 5
**-------------------------------------------------------
** OUTPUT      :  3. ACCEPTED RECS FILE(DETAIL)  FILE# 2
** (FILES)   - :  4. ACCEPTED RECS FILE(SUMMARY) FILE# 6
**             :  5. CITED RECS FILE   (DETAIL)  FILE# 4
**             :  6. CITED RECS FILE   (SUMMARY) FILE# 7
**             :  7. REJECTED RECS FILE(DETAIL)  FILE# 3    10/22/07  RM
**             : ---------------------------------------
** (REPORTS) - :  1. ACCEPTED TAX TRANSACTIONS REPORT
**             :  2. CITED    TAX TRANSACTIONS REPORT
**             :  3. REJECTED TAX TRANSACTIONS REPORT
**             :  4. EDIT CONTROL              REPORT
**             :
** DATE        :  09/13/2004
************************************************
** MODIFICATION LOG
************************************************
** DATE      PURPOSE
**----------------------------------------------
* 12/06/2017 SAURAV VIKRAM - DISTRIBUTION CODE CHANGED FROM 'L2'  OR
*                             'L7' TO 'L'.                    /* VIKRAM1
* 10/17/16 - DUTTAD   CODE CHANGES FOR ADJUSTMENT RECORDS.    /* DUTTAD1
* 10/17/16 - DUTTAD   CODE CHANGES FOR ACCEPTING THE MCCAMISH REVERSALS.
*  09/23/16 SAURAV VIKRAM - ADDED TWO PASSING VALUE FOR       - VIKRAM
*                          #YEAR AND #PLUS-HALF TO RECTIFY
*                          DISTRIBUTION CODE AS 'B' TO 'B1'
*                          HAVING ERROR FOR AGE 59.5. REF.-PRB73067
*  06/10/15 - FENDAYA COR AND NAS SUNSET. FE201506
*  05/22/13 - RC CHECK 3RD POSITION OF SUBPLAN IF RMD-IND = Y  /* RC07
*                VALUE M - MDO, ALL OTHER - RMD
*  03/15/13 - RC ADDED SOURCE CODE 'EW' GENERIC WARRANTS     /* RC06
*  03/08/13 - VR ADDED SOURCE CODE 'NZ' ADAS                 /* VR02
*                ADDED SOURCE CODE 'DC' IA DEATH CLAIMS      /* VR02
*  01/16/13 - VR ADDED REASON CODE 'SM' FOR RMD              /* VR01
*  05/31/12 - RC ADDED SOURCE 'VL' TO CORRECT CONTRACT-TYPE  /* RC05
*  03/13/12 - MB ADDED SOURCE 'AM' TO CORRECT CONTRACT-TYPE  /* MB
*  02/10/12 - RC SUNY-CUNY  ADJUSTMENT / REVERSAL
*  01/12/12 - RC SUNY-CUNY. USE TWRL9415 INSTEAD OF TWRL703A
*                           ADDITIONAL FIELDS NOT DEFINED IN TWRL703A
*                           REPORTS CHANGE DTO REFLECT SUNY/CUNY FIELDS
*  11/18/11 - RC NON-ERISA TDA. NEW COMPANY 'F'
*  09/28/11 - MB ADDED MCCAMISH SOURCR CODE "AM" MODELLED
*                FROM "VL"                               /* 09/28/11
*  09/28/11 - RS CONVERT DIST CODE 'B' TO 'B7' AND,       SCAN RS1011
*                ADD TEST FOR DIST CODES:
*                H4 = 'Roll Roth 403 to Roth IRA - Death'
*                B7 = 'Distribution from Roth Plans (over 59 1/2)'
** 04/06/11 FA - PROLINE PAYMENT, BYPASS TWRNDICV FOR SUB-PLAN 'VT'
**               UN-COMMENT ALL REFERENCE TO LOCAL&CANADIAN WITHHOLDING
** 09/20/10 AY - INC1139740 : DISTRIB. TABLE ENTRIES APPROACHING LIMIT.
**                            REVISED TO INCREASE DIST-MAX VALUE TO 202.
** 03/11/10 JR - FIX REVERSALS FOR MULTI-COUNTRY LOCALITIES
** 02/02/10 MS - INVALID LOCALITY
** 10/02/09 MS - REMOVE RESIDENCY 52 OVERRIDE
** 08/25/09 FA - OMNI_NRA  PROJECT
** 07/21/09 FA - W-2 TAXWAR PROJECT
** 05/12/08 RM - ROTH 401K/403B PROJECT
**               RECOMPILED DUE TO UPDATED TWRL0700.
** 10/22/07 RM - CONVERT DISTRIBUTION CODE A7 TO 7 AND ACCEPT THE
**               PAYMENT INSTEAD OF REJECTING.
**               CREATE NEGATIVE AMOUNT FIELDS IN WS TO DISPLAY MINUS
**               SIGN IN REVERSAL REPORT (TWRP0702-5).
** 07/27/06 JR - FIX FOR IPRO PAYMENTS WITH EMPTY SUB-PLAN
**               AND ROLLOVER TO IRA CASES WITH EMPTY SUB-PLAN
** 03/10/06 JR - OMNIPAY DAILY FEED - ADJUSTMENTS & REVERSALS
** 12/06/05 BK - RESTOWED FOR NEW TWRNCOMP PARMS
** 09/02/05 MS - TOPS REL 6.5 AND NAVISYS CHANGES
** 11/01/05 MS - PAYMENT DATE RANGE CHECK
** 11/16/05 MS - INVALID COMPANY CHECK AND LOCALITY PROBLEM
** 04/14/17 J BREMER  PIN EXPANSION SCAN 08/2017
* 04/11/18   'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
** 04/12/18 VIKRAM  DATA SIZE OF #ACC-STATE INCREASED TO (P8.2)
**                  FROM (P7.2) TAG - VIKRAM3
** 02/13/19 ARIVU   RESET ALPHANUMERIC PIN TO 0
** 04/04/19 PALDE   RESET LOCALITY CODE TAG /*PALDE
* 07/11/19    TWRT-LOB-CDE = VT IS NOT PROCESSED -CPS SUNSET - SAURAV
** 8/26/20    COMP-MAX INCREASED TO 6 TO ADD CREF COMPANY TAG AHMEDMA
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0702 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaTwratin pdaTwratin;
    private PdaTwracomp pdaTwracomp;
    private PdaTwradicv pdaTwradicv;
    private PdaTwradist pdaTwradist;
    private LdaTwrl0600 ldaTwrl0600;
    private LdaTwrl9802 ldaTwrl9802;
    private LdaTwrl9804 ldaTwrl9804;
    private LdaTwrl9806 ldaTwrl9806;
    private LdaTwrl9807 ldaTwrl9807;
    private LdaTwrl0700 ldaTwrl0700;
    private LdaTwrl0710 ldaTwrl0710;
    private LdaTwrl9415 ldaTwrl9415;
    private LdaTwrl820d ldaTwrl820d;
    private PdaMdma101 pdaMdma101;
    private PdaTwra0710 pdaTwra0710;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Debug;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;
    private DbsField pnd_Ws_Const_Err_Max;
    private DbsField pnd_Ws_Const_Comp_Max;
    private DbsField pnd_Ws_Const_Source_Max;
    private DbsField pnd_Ws_Const_Dist_Max;
    private DbsField pnd_Ws_Const_St_Max;
    private DbsField pnd_Ws_Const_Cn_Max;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_L;
    private DbsField pnd_Ws_Pnd_Er;
    private DbsField pnd_Ws_Pnd_Comp_Idx;
    private DbsField pnd_Ws_Pnd_Source_Idx;
    private DbsField pnd_Ws_Pnd_Dist_Idx;
    private DbsField pnd_Ws_Pnd_St_Idx;
    private DbsField pnd_Ws_Pnd_Cn_Idx;
    private DbsField pnd_Ws_Pnd_E;
    private DbsField pnd_Ws_Pnd_Errors;
    private DbsField pnd_Ws_Pnd_Comp_Name;
    private DbsField pnd_Ws_Pnd_Comp_Name_Disp;
    private DbsField pnd_Ws_Pnd_Puri;
    private DbsField pnd_Ws_Pnd_State_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Interest_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt;
    private DbsField pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt;
    private DbsField pnd_Ws_Pnd_Pnd_Puri;
    private DbsField pnd_Ws_Pnd_Twrt_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Pnd_State_Amt;
    private DbsField pnd_Ws_Pnd_Er_Desc;
    private DbsField pnd_Ws_Pnd_Disp_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Locality;
    private DbsField pnd_Ws_Pnd_Work_Date;
    private DbsField pnd_Ws_Pnd_P_Date;
    private DbsField pnd_Ws_Pnd_P_Year;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_P_Year_N;
    private DbsField pnd_Ws_Pnd_Comp_Break;
    private DbsField pnd_Ws_Pnd_Source_Break;
    private DbsField pnd_Ws_Pnd_Res_Valid;
    private DbsField pnd_Ws_Pnd_Roll;
    private DbsField pnd_Ws_Pnd_Last_Bus_Date;
    private DbsField pnd_Ws_Pnd_Last_Bus_Day;
    private DbsField pnd_Ws_Pnd_Last_Bus_Compare;
    private DbsField pnd_Ws_Pnd_T_Source_Cnt;
    private DbsField pnd_Ws_Pnd_T_Source_Code;
    private DbsField pnd_Ws_Pnd_T_Source_Name;
    private DbsField pnd_Ws_Pnd_T_Dist_Cnt;
    private DbsField pnd_Ws_Pnd_T_Payment_Type;
    private DbsField pnd_Ws_Pnd_T_Settle_Type;
    private DbsField pnd_Ws_Pnd_T_Dist_Code_1;
    private DbsField pnd_Ws_Pnd_T_Dist_Code_2;
    private DbsField pnd_Ws_Pnd_T_Refer_Num;
    private DbsField pnd_Ws_Pnd_T_State_Cnt;
    private DbsField pnd_Ws_Pnd_T_State_Code;
    private DbsField pnd_Ws_Pnd_T_Country_Cnt;
    private DbsField pnd_Ws_Pnd_T_Country_Code2;
    private DbsField pnd_Ws_Pnd_T_Country_Code3;
    private DbsField pnd_Ws_Pnd_Er_Su;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_Er_Yr;
    private DbsField pnd_Ws_Pnd_Er_Nbr;
    private DbsField pnd_Ws_Pnd_So_Su;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws__Filler2;
    private DbsField pnd_Ws_Pnd_So_Yr;
    private DbsField pnd_Ws_Pnd_So_Cde;
    private DbsField pnd_Ws_Pnd_Dist_Su;

    private DbsGroup pnd_Ws__R_Field_4;
    private DbsField pnd_Ws_Pnd_Di_T_Num;
    private DbsField pnd_Ws_Pnd_Di_T_Yr;
    private DbsField pnd_Ws_Pnd_St_Su;

    private DbsGroup pnd_Ws__R_Field_5;
    private DbsField pnd_Ws__Filler3;
    private DbsField pnd_Ws_Pnd_St_Yr;
    private DbsField pnd_Ws_Pnd_Cn_Su;

    private DbsGroup pnd_Ws__R_Field_6;
    private DbsField pnd_Ws__Filler4;
    private DbsField pnd_Ws_Pnd_Cn_Yr;

    private DbsGroup pnd_Ws_Pnd_Rej_Grp;
    private DbsField pnd_Ws_Pnd_Rej_Cnt;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Rej_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Rej_Gross;
    private DbsField pnd_Ws_Pnd_Rej_Ivc;
    private DbsField pnd_Ws_Pnd_Rej_Int;
    private DbsField pnd_Ws_Pnd_Rej_Fed;
    private DbsField pnd_Ws_Pnd_Rej_Can;
    private DbsField pnd_Ws_Pnd_Rej_Nra;
    private DbsField pnd_Ws_Pnd_Rej_Pr;
    private DbsField pnd_Ws_Pnd_Rej_State;
    private DbsField pnd_Ws_Pnd_Rej_Local;

    private DbsGroup pnd_Ws_Pnd_Cit_Grp;
    private DbsField pnd_Ws_Pnd_Cit_Cnt;
    private DbsField pnd_Ws_Pnd_Cit_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Cit_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Cit_Gross;
    private DbsField pnd_Ws_Pnd_Cit_Ivc;
    private DbsField pnd_Ws_Pnd_Cit_Int;
    private DbsField pnd_Ws_Pnd_Cit_Can;
    private DbsField pnd_Ws_Pnd_Cit_Nra;
    private DbsField pnd_Ws_Pnd_Cit_Pr;
    private DbsField pnd_Ws_Pnd_Cit_Fed;
    private DbsField pnd_Ws_Pnd_Cit_State;
    private DbsField pnd_Ws_Pnd_Cit_Local;

    private DbsGroup pnd_Ws_Pnd_Acc_Grp;
    private DbsField pnd_Ws_Pnd_Acc_Cnt;
    private DbsField pnd_Ws_Pnd_Acc_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Acc_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Acc_Gross;
    private DbsField pnd_Ws_Pnd_Acc_Ivc;
    private DbsField pnd_Ws_Pnd_Acc_Int;
    private DbsField pnd_Ws_Pnd_Acc_Fed;
    private DbsField pnd_Ws_Pnd_Acc_Can;
    private DbsField pnd_Ws_Pnd_Acc_Nra;
    private DbsField pnd_Ws_Pnd_Acc_Pr;
    private DbsField pnd_Ws_Pnd_Acc_State;
    private DbsField pnd_Ws_Pnd_Acc_Local;

    private DbsGroup pnd_Ws_Pnd_Comb_Grp;
    private DbsField pnd_Ws_Pnd_Comb_Gross;
    private DbsField pnd_Ws_Pnd_Comb_Ivc;
    private DbsField pnd_Ws_Pnd_Comb_Int;
    private DbsField pnd_Ws_Pnd_Comb_Fed;
    private DbsField pnd_Ws_Pnd_Comb_Can;
    private DbsField pnd_Ws_Pnd_Comb_Nra;
    private DbsField pnd_Ws_Pnd_Comb_Pr;
    private DbsField pnd_Ws_Pnd_Comb_State;
    private DbsField pnd_Ws_Pnd_Comb_Local;

    private DbsGroup pnd_Ws_Pnd_Comb_Grp_M;
    private DbsField pnd_Ws_Pnd_Comb_Gross_M;
    private DbsField pnd_Ws_Pnd_Comb_Ivc_M;
    private DbsField pnd_Ws_Pnd_Comb_Int_M;
    private DbsField pnd_Ws_Pnd_Comb_Fed_M;
    private DbsField pnd_Ws_Pnd_Comb_Nra_M;
    private DbsField pnd_Ws_Pnd_Comb_Pr_M;
    private DbsField pnd_Ws_Pnd_Comb_State_M;

    private DbsGroup pnd_Ws_Pnd_Dif_Grp;
    private DbsField pnd_Ws_Pnd_Dif_Cnt;
    private DbsField pnd_Ws_Pnd_Dif_Gross;
    private DbsField pnd_Ws_Pnd_Dif_Ivc;
    private DbsField pnd_Ws_Pnd_Dif_Int;
    private DbsField pnd_Ws_Pnd_Dif_Fed;
    private DbsField pnd_Ws_Pnd_Dif_Nra;
    private DbsField pnd_Ws_Pnd_Dif_Can;
    private DbsField pnd_Ws_Pnd_Dif_Pr;
    private DbsField pnd_Ws_Pnd_Dif_State;
    private DbsField pnd_Ws_Pnd_Dif_Local;

    private DbsGroup pnd_Ws_Pnd_Ac_Grp;
    private DbsField pnd_Ws_Pnd_Ac_Cnt;
    private DbsField pnd_Ws_Pnd_Ac_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Ac_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Ac_Gross;
    private DbsField pnd_Ws_Pnd_Ac_Ivc;
    private DbsField pnd_Ws_Pnd_Ac_Int;
    private DbsField pnd_Ws_Pnd_Ac_Nra;
    private DbsField pnd_Ws_Pnd_Ac_Fed;
    private DbsField pnd_Ws_Pnd_Ac_Can;
    private DbsField pnd_Ws_Pnd_Ac_State;
    private DbsField pnd_Ws_Pnd_Ac_Local;
    private DbsField pnd_Ws_Pnd_Ac_Pr;

    private DbsGroup pnd_Ws_Pnd_Inp_Grp;
    private DbsField pnd_Ws_Pnd_Inp_Cnt;
    private DbsField pnd_Ws_Pnd_Inp_Gross;
    private DbsField pnd_Ws_Pnd_Inp_Ivc;
    private DbsField pnd_Ws_Pnd_Inp_Int;
    private DbsField pnd_Ws_Pnd_Inp_Fed;
    private DbsField pnd_Ws_Pnd_Inp_Can;
    private DbsField pnd_Ws_Pnd_Inp_Nra;
    private DbsField pnd_Ws_Pnd_Inp_State;
    private DbsField pnd_Ws_Pnd_Inp_Local;

    private DbsGroup pnd_Ws_Pnd_Rev_Grp;
    private DbsField pnd_Ws_Pnd_Rev_Cnt;
    private DbsField pnd_Ws_Pnd_Rev_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Rev_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Rev_Gross;
    private DbsField pnd_Ws_Pnd_Rev_Ivc;
    private DbsField pnd_Ws_Pnd_Rev_Int;
    private DbsField pnd_Ws_Pnd_Rev_Fed;
    private DbsField pnd_Ws_Pnd_Rev_Can;
    private DbsField pnd_Ws_Pnd_Rev_Nra;
    private DbsField pnd_Ws_Pnd_Rev_Pr;
    private DbsField pnd_Ws_Pnd_Rev_State;
    private DbsField pnd_Ws_Pnd_Rev_Local;

    private DbsGroup pnd_Ws_Pnd_Rev_Grp_M;
    private DbsField pnd_Ws_Pnd_Rev_Gross_Unkn_M;
    private DbsField pnd_Ws_Pnd_Rev_Gross_Roll_M;
    private DbsField pnd_Ws_Pnd_Rev_Gross_M;
    private DbsField pnd_Ws_Pnd_Rev_Ivc_M;
    private DbsField pnd_Ws_Pnd_Rev_Int_M;
    private DbsField pnd_Ws_Pnd_Rev_Fed_M;
    private DbsField pnd_Ws_Pnd_Rev_Nra_M;
    private DbsField pnd_Ws_Pnd_Rev_Pr_M;
    private DbsField pnd_Ws_Pnd_Rev_State_M;
    private DbsField pnd_Ws_Pnd_Rev_Local_M;

    private DbsGroup pnd_Ws_Pnd_Adj_Grp;
    private DbsField pnd_Ws_Pnd_Adj_Cnt;
    private DbsField pnd_Ws_Pnd_Adj_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Adj_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Adj_Gross;
    private DbsField pnd_Ws_Pnd_Adj_Ivc;
    private DbsField pnd_Ws_Pnd_Adj_Int;
    private DbsField pnd_Ws_Pnd_Adj_Fed;
    private DbsField pnd_Ws_Pnd_Adj_Can;
    private DbsField pnd_Ws_Pnd_Adj_Nra;
    private DbsField pnd_Ws_Pnd_Adj_Pr;
    private DbsField pnd_Ws_Pnd_Adj_State;
    private DbsField pnd_Ws_Pnd_Adj_Local;

    private DbsGroup pnd_Ws_Pnd_Neg_Grp;
    private DbsField pnd_Ws_Pnd_Neg_Cnt;
    private DbsField pnd_Ws_Pnd_Neg_Gross_Unkn;
    private DbsField pnd_Ws_Pnd_Neg_Gross_Roll;
    private DbsField pnd_Ws_Pnd_Neg_Gross;
    private DbsField pnd_Ws_Pnd_Neg_Ivc;
    private DbsField pnd_Ws_Pnd_Neg_Int;
    private DbsField pnd_Ws_Pnd_Neg_Fed;
    private DbsField pnd_Ws_Pnd_Neg_Can;
    private DbsField pnd_Ws_Pnd_Neg_Nra;
    private DbsField pnd_Ws_Pnd_Neg_Pr;
    private DbsField pnd_Ws_Pnd_Neg_State;
    private DbsField pnd_Ws_Pnd_Neg_Local;

    private DbsGroup pnd_Omni_Summary_Amounts;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_Gross;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_Int;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_Fed;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_Nra;
    private DbsField pnd_Omni_Summary_Amounts_Pnd_Omni_State;
    private DbsField pnd_Twrpymnt_Form_Sd;

    private DbsGroup pnd_Twrpymnt_Form_Sd__R_Field_7;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code;
    private DbsField pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code;
    private DbsField pnd_Twrpymnt_Ssn_Con_Pay_Sd;

    private DbsGroup pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8;
    private DbsField pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Year;
    private DbsField pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Payee_Cde;
    private DbsField pnd_Super_Dist_Code;

    private DbsGroup pnd_Super_Dist_Code__R_Field_9;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Tax_Year;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Settl_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Pay_Type;
    private DbsField pnd_Super_Dist_Code_Pnd_S_Dist_Code;
    private DbsField pnd_Search_Cat_Dist;

    private DbsGroup pnd_Search_Cat_Dist__R_Field_10;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Cat;
    private DbsField pnd_Search_Cat_Dist_Pnd_Search_Dist;
    private DbsField pnd_Cat_Dist_Table;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_11;

    private DbsGroup pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries;
    private DbsField pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method;

    private DbsGroup pnd_Cat_Dist_Table__R_Field_12;
    private DbsField pnd_Cat_Dist_Table_Pnd_Pay_Category;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method;
    private DbsField pnd_Cat_Dist_Table_Pnd_Dist_Method_Code;
    private DbsField pnd_Twrpymnt_Tax_Citizenship;
    private DbsField pnd_Twrpymnt_Paymt_Category;
    private DbsField pnd_Twrpymnt_Dist_Method;
    private DbsField pnd_Twrpymnt_Residency_Type;
    private DbsField pnd_Twrpymnt_Pymnt_Refer_Nbr;
    private DbsField pnd_Twrpymnt_Locality_Cde;
    private DbsField pnd_Twrpymnt_Contract_Seq_No;
    private DbsField pnd_Tiaa_Ignore;
    private DbsField pnd_Rev_Adj;
    private DbsField pnd_Rev_Match;
    private DbsField pnd_Adj_Net_Neg;
    private DbsField pnd_Bypass_Payment;
    private DbsField pnd_Payment_Found;
    private DbsField pnd_Payment_24;
    private DbsField pnd_Us_Resident;
    private DbsField pnd_Cycle_Processed;
    private DbsField pnd_Over_59_Half;
    private DbsField pnd_Cnt;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField pnd_Valid_Distr_Cd;
    private DbsField pnd_Pay_Stl_Ms;
    private DbsField pnd_Pay_Stl_Me;
    private DbsField pnd_Distr_7;
    private DbsField pnd_Vt_Ps_Err;

    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DbsGroup pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date;
    private DbsField pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField pnd_Iaa_Contract;
    private DbsField pnd_Oc_N;

    private DbsGroup pnd_Oc_N__R_Field_14;
    private DbsField pnd_Oc_N_Pnd_Oc_A;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwradicv = new PdaTwradicv(localVariables);
        pdaTwradist = new PdaTwradist(localVariables);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        ldaTwrl9802 = new LdaTwrl9802();
        registerRecord(ldaTwrl9802);
        registerRecord(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi());
        ldaTwrl9804 = new LdaTwrl9804();
        registerRecord(ldaTwrl9804);
        registerRecord(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View());
        ldaTwrl9806 = new LdaTwrl9806();
        registerRecord(ldaTwrl9806);
        registerRecord(ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View());
        ldaTwrl9807 = new LdaTwrl9807();
        registerRecord(ldaTwrl9807);
        registerRecord(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View());
        ldaTwrl0700 = new LdaTwrl0700();
        registerRecord(ldaTwrl0700);
        ldaTwrl0710 = new LdaTwrl0710();
        registerRecord(ldaTwrl0710);
        ldaTwrl9415 = new LdaTwrl9415();
        registerRecord(ldaTwrl9415);
        registerRecord(ldaTwrl9415.getVw_pay());
        ldaTwrl820d = new LdaTwrl820d();
        registerRecord(ldaTwrl820d);
        registerRecord(ldaTwrl820d.getVw_dist_Tbl());
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaTwra0710 = new PdaTwra0710(localVariables);

        // Local Variables
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_Err_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Err_Max", "ERR-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Comp_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Comp_Max", "COMP-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Source_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Source_Max", "SOURCE-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Dist_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Dist_Max", "DIST-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_St_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_St_Max", "ST-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Const_Cn_Max = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Cn_Max", "CN-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Rec_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_L = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Er = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Er", "#ER", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Source_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Source_Idx", "#SOURCE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Dist_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dist_Idx", "#DIST-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_St_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Idx", "#ST-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Cn_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cn_Idx", "#CN-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_E = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_E", "#E", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Errors = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Errors", "#ERRORS", FieldType.NUMERIC, 2, new DbsArrayController(1, 60));
        pnd_Ws_Pnd_Comp_Name = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Comp_Name", "#COMP-NAME", FieldType.STRING, 4, new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Comp_Name_Disp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Name_Disp", "#COMP-NAME-DISP", FieldType.STRING, 4);
        pnd_Ws_Pnd_Puri = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Puri", "#PURI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_State_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Amt", "#STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Gross_Amt", "#TWRT-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Twrt_Interest_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Interest_Amt", "#TWRT-INTEREST-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt", "#TWRT-FED-WHHLD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt", "#TWRT-NRA-WHHLD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pnd_Puri = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pnd_Puri", "##PURI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Twrt_Ivc_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Twrt_Ivc_Amt", "#TWRT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pnd_State_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pnd_State_Amt", "##STATE-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Er_Desc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Er_Desc", "#ER-DESC", FieldType.STRING, 40, new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Disp_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Amt", "#DISP-AMT", FieldType.STRING, 15);
        pnd_Ws_Pnd_Disp_Locality = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Locality", "#DISP-LOCALITY", FieldType.STRING, 3);
        pnd_Ws_Pnd_Work_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);
        pnd_Ws_Pnd_P_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_P_Date", "#P-DATE", FieldType.DATE);
        pnd_Ws_Pnd_P_Year = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_P_Year", "#P-YEAR", FieldType.STRING, 4);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_P_Year);
        pnd_Ws_Pnd_P_Year_N = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_P_Year_N", "#P-YEAR-N", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Comp_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Break", "#COMP-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Source_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Source_Break", "#SOURCE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Res_Valid = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Res_Valid", "#RES-VALID", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Roll = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Roll", "#ROLL", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Last_Bus_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Bus_Date", "#LAST-BUS-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Last_Bus_Day = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Bus_Day", "#LAST-BUS-DAY", FieldType.STRING, 1);
        pnd_Ws_Pnd_Last_Bus_Compare = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Bus_Compare", "#LAST-BUS-COMPARE", FieldType.STRING, 4);
        pnd_Ws_Pnd_T_Source_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_T_Source_Cnt", "#T-SOURCE-CNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Ws_Pnd_T_Source_Code = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Source_Code", "#T-SOURCE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            50));
        pnd_Ws_Pnd_T_Source_Name = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Source_Name", "#T-SOURCE-NAME", FieldType.STRING, 23, new DbsArrayController(1, 
            50));
        pnd_Ws_Pnd_T_Dist_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_T_Dist_Cnt", "#T-DIST-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_T_Payment_Type = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Payment_Type", "#T-PAYMENT-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            202));
        pnd_Ws_Pnd_T_Settle_Type = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Settle_Type", "#T-SETTLE-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            202));
        pnd_Ws_Pnd_T_Dist_Code_1 = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Dist_Code_1", "#T-DIST-CODE-1", FieldType.STRING, 1, new DbsArrayController(1, 
            202));
        pnd_Ws_Pnd_T_Dist_Code_2 = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Dist_Code_2", "#T-DIST-CODE-2", FieldType.STRING, 2, new DbsArrayController(1, 
            202));
        pnd_Ws_Pnd_T_Refer_Num = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Refer_Num", "#T-REFER-NUM", FieldType.STRING, 2, new DbsArrayController(1, 
            202));
        pnd_Ws_Pnd_T_State_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_T_State_Cnt", "#T-STATE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_T_State_Code = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_State_Code", "#T-STATE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 
            75));
        pnd_Ws_Pnd_T_Country_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_T_Country_Cnt", "#T-COUNTRY-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_T_Country_Code2 = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Country_Code2", "#T-COUNTRY-CODE2", FieldType.STRING, 2, new DbsArrayController(1, 
            350));
        pnd_Ws_Pnd_T_Country_Code3 = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_T_Country_Code3", "#T-COUNTRY-CODE3", FieldType.STRING, 3, new DbsArrayController(1, 
            350));
        pnd_Ws_Pnd_Er_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Er_Su", "#ER-SU", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Er_Su);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ws_Pnd_Er_Yr = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Er_Yr", "#ER-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Er_Nbr = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Er_Nbr", "#ER-NBR", FieldType.NUMERIC, 3);
        pnd_Ws_Pnd_So_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_So_Su", "#SO-SU", FieldType.STRING, 7);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_So_Su);
        pnd_Ws__Filler2 = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Ws_Pnd_So_Yr = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_So_Yr", "#SO-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_So_Cde = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_So_Cde", "#SO-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Dist_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dist_Su", "#DIST-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_4 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_4", "REDEFINE", pnd_Ws_Pnd_Dist_Su);
        pnd_Ws_Pnd_Di_T_Num = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws_Pnd_Di_T_Num", "#DI-T-NUM", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Di_T_Yr = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws_Pnd_Di_T_Yr", "#DI-T-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_St_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St_Su", "#ST-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_5 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_5", "REDEFINE", pnd_Ws_Pnd_St_Su);
        pnd_Ws__Filler3 = pnd_Ws__R_Field_5.newFieldInGroup("pnd_Ws__Filler3", "_FILLER3", FieldType.STRING, 1);
        pnd_Ws_Pnd_St_Yr = pnd_Ws__R_Field_5.newFieldInGroup("pnd_Ws_Pnd_St_Yr", "#ST-YR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Cn_Su = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cn_Su", "#CN-SU", FieldType.STRING, 5);

        pnd_Ws__R_Field_6 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_6", "REDEFINE", pnd_Ws_Pnd_Cn_Su);
        pnd_Ws__Filler4 = pnd_Ws__R_Field_6.newFieldInGroup("pnd_Ws__Filler4", "_FILLER4", FieldType.STRING, 1);
        pnd_Ws_Pnd_Cn_Yr = pnd_Ws__R_Field_6.newFieldInGroup("pnd_Ws_Pnd_Cn_Yr", "#CN-YR", FieldType.NUMERIC, 4);

        pnd_Ws_Pnd_Rej_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Rej_Grp", "#REJ-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Rej_Cnt = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Cnt", "#REJ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rej_Gross_Unkn = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Unkn", "#REJ-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Gross_Roll = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross_Roll", "#REJ-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Gross = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Gross", "#REJ-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rej_Ivc = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Ivc", "#REJ-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rej_Int = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Int", "#REJ-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Fed = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Fed", "#REJ-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rej_Can = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Can", "#REJ-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Nra = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Nra", "#REJ-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Pr = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Pr", "#REJ-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_State = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_State", "#REJ-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rej_Local = pnd_Ws_Pnd_Rej_Grp.newFieldInGroup("pnd_Ws_Pnd_Rej_Local", "#REJ-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Cit_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Cit_Grp", "#CIT-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Cit_Cnt = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Cnt", "#CIT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cit_Gross_Unkn = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Gross_Unkn", "#CIT-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Cit_Gross_Roll = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Gross_Roll", "#CIT-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Cit_Gross = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Gross", "#CIT-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Cit_Ivc = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Ivc", "#CIT-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Cit_Int = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Int", "#CIT-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cit_Can = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Can", "#CIT-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cit_Nra = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Nra", "#CIT-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cit_Pr = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Pr", "#CIT-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cit_Fed = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Fed", "#CIT-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Cit_State = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_State", "#CIT-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cit_Local = pnd_Ws_Pnd_Cit_Grp.newFieldInGroup("pnd_Ws_Pnd_Cit_Local", "#CIT-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Acc_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Acc_Grp", "#ACC-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Acc_Cnt = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Cnt", "#ACC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Acc_Gross_Unkn = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Gross_Unkn", "#ACC-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Acc_Gross_Roll = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Gross_Roll", "#ACC-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Acc_Gross = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Gross", "#ACC-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Acc_Ivc = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Ivc", "#ACC-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Acc_Int = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Int", "#ACC-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Acc_Fed = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Fed", "#ACC-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Acc_Can = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Can", "#ACC-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Acc_Nra = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Nra", "#ACC-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Acc_Pr = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Pr", "#ACC-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Acc_State = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_State", "#ACC-STATE", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Acc_Local = pnd_Ws_Pnd_Acc_Grp.newFieldInGroup("pnd_Ws_Pnd_Acc_Local", "#ACC-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Comb_Grp = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Comb_Grp", "#COMB-GRP");
        pnd_Ws_Pnd_Comb_Gross = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Gross", "#COMB-GROSS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Ivc = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Ivc", "#COMB-IVC", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Int = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Int", "#COMB-INT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Fed = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Fed", "#COMB-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Comb_Can = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Can", "#COMB-CAN", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Nra = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Nra", "#COMB-NRA", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Pr = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Pr", "#COMB-PR", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_State = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_State", "#COMB-STATE", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Local = pnd_Ws_Pnd_Comb_Grp.newFieldInGroup("pnd_Ws_Pnd_Comb_Local", "#COMB-LOCAL", FieldType.PACKED_DECIMAL, 10, 2);

        pnd_Ws_Pnd_Comb_Grp_M = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Comb_Grp_M", "#COMB-GRP-M");
        pnd_Ws_Pnd_Comb_Gross_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Gross_M", "#COMB-GROSS-M", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Ivc_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Ivc_M", "#COMB-IVC-M", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Pnd_Comb_Int_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Int_M", "#COMB-INT-M", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Fed_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Fed_M", "#COMB-FED-M", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Comb_Nra_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Nra_M", "#COMB-NRA-M", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_Pr_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_Pr_M", "#COMB-PR-M", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Comb_State_M = pnd_Ws_Pnd_Comb_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Comb_State_M", "#COMB-STATE-M", FieldType.PACKED_DECIMAL, 10, 2);

        pnd_Ws_Pnd_Dif_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Dif_Grp", "#DIF-GRP", new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Dif_Cnt = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Cnt", "#DIF-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dif_Gross = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Gross", "#DIF-GROSS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Ivc = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Ivc", "#DIF-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Int = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Int", "#DIF-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Fed = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Fed", "#DIF-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Nra = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Nra", "#DIF-NRA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Can = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Can", "#DIF-CAN", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Pr = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Pr", "#DIF-PR", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_State = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_State", "#DIF-STATE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dif_Local = pnd_Ws_Pnd_Dif_Grp.newFieldInGroup("pnd_Ws_Pnd_Dif_Local", "#DIF-LOCAL", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Ws_Pnd_Ac_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Ac_Grp", "#AC-GRP", new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Ac_Cnt = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Cnt", "#AC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ac_Gross_Unkn = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Gross_Unkn", "#AC-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ac_Gross_Roll = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Gross_Roll", "#AC-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ac_Gross = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Gross", "#AC-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ac_Ivc = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Ivc", "#AC-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Ac_Int = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Int", "#AC-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ac_Nra = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Nra", "#AC-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ac_Fed = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Fed", "#AC-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Ac_Can = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Can", "#AC-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ac_State = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_State", "#AC-STATE", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Ac_Local = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Local", "#AC-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ac_Pr = pnd_Ws_Pnd_Ac_Grp.newFieldInGroup("pnd_Ws_Pnd_Ac_Pr", "#AC-PR", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Inp_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Inp_Grp", "#INP-GRP", new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Inp_Cnt = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Cnt", "#INP-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Inp_Gross = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Gross", "#INP-GROSS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Ivc = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Ivc", "#INP-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Int = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Int", "#INP-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Fed = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Fed", "#INP-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Can = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Can", "#INP-CAN", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Nra = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Nra", "#INP-NRA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_State = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_State", "#INP-STATE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Inp_Local = pnd_Ws_Pnd_Inp_Grp.newFieldInGroup("pnd_Ws_Pnd_Inp_Local", "#INP-LOCAL", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Ws_Pnd_Rev_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Rev_Grp", "#REV-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Rev_Cnt = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Cnt", "#REV-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rev_Gross_Unkn = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross_Unkn", "#REV-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rev_Gross_Roll = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross_Roll", "#REV-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rev_Gross = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross", "#REV-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rev_Ivc = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Ivc", "#REV-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rev_Int = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Int", "#REV-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Fed = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Fed", "#REV-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rev_Can = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Can", "#REV-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Nra = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Nra", "#REV-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Pr = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Pr", "#REV-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_State = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_State", "#REV-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Local = pnd_Ws_Pnd_Rev_Grp.newFieldInGroup("pnd_Ws_Pnd_Rev_Local", "#REV-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Rev_Grp_M = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Rev_Grp_M", "#REV-GRP-M", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Rev_Gross_Unkn_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross_Unkn_M", "#REV-GROSS-UNKN-M", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Rev_Gross_Roll_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross_Roll_M", "#REV-GROSS-ROLL-M", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Rev_Gross_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Gross_M", "#REV-GROSS-M", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Rev_Ivc_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Ivc_M", "#REV-IVC-M", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rev_Int_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Int_M", "#REV-INT-M", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Fed_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Fed_M", "#REV-FED-M", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Rev_Nra_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Nra_M", "#REV-NRA-M", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Pr_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Pr_M", "#REV-PR-M", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_State_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_State_M", "#REV-STATE-M", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Rev_Local_M = pnd_Ws_Pnd_Rev_Grp_M.newFieldInGroup("pnd_Ws_Pnd_Rev_Local_M", "#REV-LOCAL-M", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Adj_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Adj_Grp", "#ADJ-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Adj_Cnt = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Cnt", "#ADJ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Adj_Gross_Unkn = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Gross_Unkn", "#ADJ-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Adj_Gross_Roll = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Gross_Roll", "#ADJ-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Adj_Gross = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Gross", "#ADJ-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Adj_Ivc = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Ivc", "#ADJ-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Adj_Int = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Int", "#ADJ-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_Fed = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Fed", "#ADJ-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Adj_Can = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Can", "#ADJ-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_Nra = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Nra", "#ADJ-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_Pr = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Pr", "#ADJ-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_State = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_State", "#ADJ-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Adj_Local = pnd_Ws_Pnd_Adj_Grp.newFieldInGroup("pnd_Ws_Pnd_Adj_Local", "#ADJ-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Neg_Grp = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Neg_Grp", "#NEG-GRP", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Neg_Cnt = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Cnt", "#NEG-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Neg_Gross_Unkn = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Gross_Unkn", "#NEG-GROSS-UNKN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Neg_Gross_Roll = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Gross_Roll", "#NEG-GROSS-ROLL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Neg_Gross = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Gross", "#NEG-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Neg_Ivc = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Ivc", "#NEG-IVC", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Neg_Int = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Int", "#NEG-INT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Neg_Fed = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Fed", "#NEG-FED", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Ws_Pnd_Neg_Can = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Can", "#NEG-CAN", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Neg_Nra = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Nra", "#NEG-NRA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Neg_Pr = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Pr", "#NEG-PR", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Neg_State = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_State", "#NEG-STATE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Neg_Local = pnd_Ws_Pnd_Neg_Grp.newFieldInGroup("pnd_Ws_Pnd_Neg_Local", "#NEG-LOCAL", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Omni_Summary_Amounts = localVariables.newGroupInRecord("pnd_Omni_Summary_Amounts", "#OMNI-SUMMARY-AMOUNTS");
        pnd_Omni_Summary_Amounts_Pnd_Omni_Gross = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_Gross", "#OMNI-GROSS", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc", "#OMNI-IVC", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Omni_Summary_Amounts_Pnd_Omni_Int = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_Int", "#OMNI-INT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Omni_Summary_Amounts_Pnd_Omni_Fed = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_Fed", "#OMNI-FED", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Omni_Summary_Amounts_Pnd_Omni_Nra = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_Nra", "#OMNI-NRA", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Omni_Summary_Amounts_Pnd_Omni_State = pnd_Omni_Summary_Amounts.newFieldInGroup("pnd_Omni_Summary_Amounts_Pnd_Omni_State", "#OMNI-STATE", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Twrpymnt_Form_Sd = localVariables.newFieldInRecord("pnd_Twrpymnt_Form_Sd", "#TWRPYMNT-FORM-SD", FieldType.STRING, 35);

        pnd_Twrpymnt_Form_Sd__R_Field_7 = localVariables.newGroupInRecord("pnd_Twrpymnt_Form_Sd__R_Field_7", "REDEFINE", pnd_Twrpymnt_Form_Sd);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year", "#TWR-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr", "#TWR-TAX-ID-NBR", 
            FieldType.STRING, 10);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr", "#TWR-CONTRACT-NBR", 
            FieldType.STRING, 8);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde", "#TWR-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde", "#TWR-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship", 
            "#TWR-TAX-CITIZENSHIP", FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category", "#TWR-PAYMT-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code", "#TWR-DIST-CODE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type", "#TWR-RESIDENCY-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code", "#TWR-RESIDENCY-CODE", 
            FieldType.STRING, 2);
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code = pnd_Twrpymnt_Form_Sd__R_Field_7.newFieldInGroup("pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code", "#TWR-LOCALITY-CODE", 
            FieldType.STRING, 3);
        pnd_Twrpymnt_Ssn_Con_Pay_Sd = localVariables.newFieldInRecord("pnd_Twrpymnt_Ssn_Con_Pay_Sd", "#TWRPYMNT-SSN-CON-PAY-SD", FieldType.STRING, 27);

        pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8 = localVariables.newGroupInRecord("pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8", "REDEFINE", pnd_Twrpymnt_Ssn_Con_Pay_Sd);
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Year = pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Year", 
            "#TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Id_Nbr = pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Id_Nbr", 
            "#TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10);
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Contract_Nbr = pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Contract_Nbr", 
            "#TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Payee_Cde = pnd_Twrpymnt_Ssn_Con_Pay_Sd__R_Field_8.newFieldInGroup("pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Payee_Cde", 
            "#TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Super_Dist_Code = localVariables.newFieldInRecord("pnd_Super_Dist_Code", "#SUPER-DIST-CODE", FieldType.STRING, 8);

        pnd_Super_Dist_Code__R_Field_9 = localVariables.newGroupInRecord("pnd_Super_Dist_Code__R_Field_9", "REDEFINE", pnd_Super_Dist_Code);
        pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr = pnd_Super_Dist_Code__R_Field_9.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr", "#S-TBL-NBR", FieldType.NUMERIC, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Tax_Year = pnd_Super_Dist_Code__R_Field_9.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Tax_Year", "#S-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Super_Dist_Code_Pnd_S_Settl_Type = pnd_Super_Dist_Code__R_Field_9.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Settl_Type", "#S-SETTL-TYPE", 
            FieldType.STRING, 1);
        pnd_Super_Dist_Code_Pnd_S_Pay_Type = pnd_Super_Dist_Code__R_Field_9.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Pay_Type", "#S-PAY-TYPE", FieldType.STRING, 
            1);
        pnd_Super_Dist_Code_Pnd_S_Dist_Code = pnd_Super_Dist_Code__R_Field_9.newFieldInGroup("pnd_Super_Dist_Code_Pnd_S_Dist_Code", "#S-DIST-CODE", FieldType.STRING, 
            1);
        pnd_Search_Cat_Dist = localVariables.newFieldInRecord("pnd_Search_Cat_Dist", "#SEARCH-CAT-DIST", FieldType.STRING, 2);

        pnd_Search_Cat_Dist__R_Field_10 = localVariables.newGroupInRecord("pnd_Search_Cat_Dist__R_Field_10", "REDEFINE", pnd_Search_Cat_Dist);
        pnd_Search_Cat_Dist_Pnd_Search_Cat = pnd_Search_Cat_Dist__R_Field_10.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Cat", "#SEARCH-CAT", FieldType.STRING, 
            1);
        pnd_Search_Cat_Dist_Pnd_Search_Dist = pnd_Search_Cat_Dist__R_Field_10.newFieldInGroup("pnd_Search_Cat_Dist_Pnd_Search_Dist", "#SEARCH-DIST", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table = localVariables.newFieldInRecord("pnd_Cat_Dist_Table", "#CAT-DIST-TABLE", FieldType.STRING, 18);

        pnd_Cat_Dist_Table__R_Field_11 = localVariables.newGroupInRecord("pnd_Cat_Dist_Table__R_Field_11", "REDEFINE", pnd_Cat_Dist_Table);

        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries = pnd_Cat_Dist_Table__R_Field_11.newGroupArrayInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries", "#CAT-DIST-ENTRIES", 
            new DbsArrayController(1, 6));
        pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method", "#CAT-DIST-METHOD", 
            FieldType.STRING, 2);

        pnd_Cat_Dist_Table__R_Field_12 = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newGroupInGroup("pnd_Cat_Dist_Table__R_Field_12", "REDEFINE", pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method);
        pnd_Cat_Dist_Table_Pnd_Pay_Category = pnd_Cat_Dist_Table__R_Field_12.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Pay_Category", "#PAY-CATEGORY", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method = pnd_Cat_Dist_Table__R_Field_12.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method", "#DIST-METHOD", FieldType.STRING, 
            1);
        pnd_Cat_Dist_Table_Pnd_Dist_Method_Code = pnd_Cat_Dist_Table_Pnd_Cat_Dist_Entries.newFieldInGroup("pnd_Cat_Dist_Table_Pnd_Dist_Method_Code", "#DIST-METHOD-CODE", 
            FieldType.STRING, 1);
        pnd_Twrpymnt_Tax_Citizenship = localVariables.newFieldInRecord("pnd_Twrpymnt_Tax_Citizenship", "#TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Twrpymnt_Paymt_Category = localVariables.newFieldInRecord("pnd_Twrpymnt_Paymt_Category", "#TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 1);
        pnd_Twrpymnt_Dist_Method = localVariables.newFieldInRecord("pnd_Twrpymnt_Dist_Method", "#TWRPYMNT-DIST-METHOD", FieldType.STRING, 1);
        pnd_Twrpymnt_Residency_Type = localVariables.newFieldInRecord("pnd_Twrpymnt_Residency_Type", "#TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 1);
        pnd_Twrpymnt_Pymnt_Refer_Nbr = localVariables.newFieldInRecord("pnd_Twrpymnt_Pymnt_Refer_Nbr", "#TWRPYMNT-PYMNT-REFER-NBR", FieldType.STRING, 
            2);
        pnd_Twrpymnt_Locality_Cde = localVariables.newFieldInRecord("pnd_Twrpymnt_Locality_Cde", "#TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 3);
        pnd_Twrpymnt_Contract_Seq_No = localVariables.newFieldInRecord("pnd_Twrpymnt_Contract_Seq_No", "#TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2);
        pnd_Tiaa_Ignore = localVariables.newFieldInRecord("pnd_Tiaa_Ignore", "#TIAA-IGNORE", FieldType.BOOLEAN, 1);
        pnd_Rev_Adj = localVariables.newFieldInRecord("pnd_Rev_Adj", "#REV-ADJ", FieldType.BOOLEAN, 1);
        pnd_Rev_Match = localVariables.newFieldInRecord("pnd_Rev_Match", "#REV-MATCH", FieldType.BOOLEAN, 1);
        pnd_Adj_Net_Neg = localVariables.newFieldInRecord("pnd_Adj_Net_Neg", "#ADJ-NET-NEG", FieldType.BOOLEAN, 1);
        pnd_Bypass_Payment = localVariables.newFieldInRecord("pnd_Bypass_Payment", "#BYPASS-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Payment_Found = localVariables.newFieldInRecord("pnd_Payment_Found", "#PAYMENT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Payment_24 = localVariables.newFieldInRecord("pnd_Payment_24", "#PAYMENT-24", FieldType.BOOLEAN, 1);
        pnd_Us_Resident = localVariables.newFieldInRecord("pnd_Us_Resident", "#US-RESIDENT", FieldType.BOOLEAN, 1);
        pnd_Cycle_Processed = localVariables.newFieldInRecord("pnd_Cycle_Processed", "#CYCLE-PROCESSED", FieldType.BOOLEAN, 1);
        pnd_Over_59_Half = localVariables.newFieldInRecord("pnd_Over_59_Half", "#OVER-59-HALF", FieldType.BOOLEAN, 1);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        j = localVariables.newFieldInRecord("j", "J", FieldType.PACKED_DECIMAL, 3);
        k = localVariables.newFieldInRecord("k", "K", FieldType.PACKED_DECIMAL, 3);
        pnd_Valid_Distr_Cd = localVariables.newFieldArrayInRecord("pnd_Valid_Distr_Cd", "#VALID-DISTR-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            6));
        pnd_Pay_Stl_Ms = localVariables.newFieldArrayInRecord("pnd_Pay_Stl_Ms", "#PAY-STL-MS", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_Pay_Stl_Me = localVariables.newFieldArrayInRecord("pnd_Pay_Stl_Me", "#PAY-STL-ME", FieldType.STRING, 1, new DbsArrayController(1, 3));
        pnd_Distr_7 = localVariables.newFieldInRecord("pnd_Distr_7", "#DISTR-7", FieldType.STRING, 1);
        pnd_Vt_Ps_Err = localVariables.newFieldInRecord("pnd_Vt_Ps_Err", "#VT-PS-ERR", FieldType.BOOLEAN, 1);

        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "TIRCNTL_5_Y_SC_CO_TO_FRM_SP");
        cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setSuperDescriptor(true);
        registerRecord(vw_cntl);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp = localVariables.newFieldInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "#TIRCNTL-5-Y-SC-CO-TO-FRM-SP", FieldType.STRING, 
            24);

        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13 = localVariables.newGroupInRecord("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13", "REDEFINE", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5 = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5", 
            "#CNTL-TBL-5", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year", 
            "#CNTL-TAX-YEAR", FieldType.STRING, 4);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source", 
            "#CNTL-SOURCE", FieldType.STRING, 2);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company", 
            "#CNTL-COMPANY", FieldType.STRING, 1);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date", 
            "#CNTL-TO-DATE", FieldType.STRING, 8);
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date = pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp__R_Field_13.newFieldInGroup("pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date", 
            "#CNTL-INTERFACE-DATE", FieldType.STRING, 8);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        registerRecord(vw_iaa_Cntrct);

        pnd_Iaa_Contract = localVariables.newFieldInRecord("pnd_Iaa_Contract", "#IAA-CONTRACT", FieldType.STRING, 10);
        pnd_Oc_N = localVariables.newFieldInRecord("pnd_Oc_N", "#OC-N", FieldType.NUMERIC, 2);

        pnd_Oc_N__R_Field_14 = localVariables.newGroupInRecord("pnd_Oc_N__R_Field_14", "REDEFINE", pnd_Oc_N);
        pnd_Oc_N_Pnd_Oc_A = pnd_Oc_N__R_Field_14.newFieldInGroup("pnd_Oc_N_Pnd_Oc_A", "#OC-A", FieldType.STRING, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntl.reset();
        vw_iaa_Cntrct.reset();

        ldaTwrl0600.initializeValues();
        ldaTwrl9802.initializeValues();
        ldaTwrl9804.initializeValues();
        ldaTwrl9806.initializeValues();
        ldaTwrl9807.initializeValues();
        ldaTwrl0700.initializeValues();
        ldaTwrl0710.initializeValues();
        ldaTwrl9415.initializeValues();
        ldaTwrl820d.initializeValues();

        localVariables.reset();
        pnd_Debug.setInitialValue(true);
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
        pnd_Ws_Const_Err_Max.setInitialValue(60);
        pnd_Ws_Const_Comp_Max.setInitialValue(6);
        pnd_Ws_Const_Source_Max.setInitialValue(50);
        pnd_Ws_Const_Dist_Max.setInitialValue(202);
        pnd_Ws_Const_St_Max.setInitialValue(75);
        pnd_Ws_Const_Cn_Max.setInitialValue(350);
        pnd_Ws_Pnd_Comp_Name.getValue(0 + 1).setInitialValue("????");
        pnd_Ws_Pnd_Er_Su.setInitialValue(70000000);
        pnd_Ws_Pnd_So_Su.setInitialValue("5");
        pnd_Ws_Pnd_Dist_Su.setInitialValue("1");
        pnd_Ws_Pnd_St_Su.setInitialValue("2");
        pnd_Ws_Pnd_Cn_Su.setInitialValue("3");
        pnd_Cat_Dist_Table.setInitialValue("PCAPRBLCDLRERCDRRE");
        pnd_Tiaa_Ignore.setInitialValue(false);
        pnd_Rev_Adj.setInitialValue(false);
        pnd_Rev_Match.setInitialValue(false);
        pnd_Adj_Net_Neg.setInitialValue(false);
        pnd_Bypass_Payment.setInitialValue(false);
        pnd_Payment_Found.setInitialValue(false);
        pnd_Payment_24.setInitialValue(false);
        pnd_Us_Resident.setInitialValue(false);
        pnd_Cycle_Processed.setInitialValue(false);
        pnd_Over_59_Half.setInitialValue(false);
        pnd_Valid_Distr_Cd.getValue(1).setInitialValue("1");
        pnd_Valid_Distr_Cd.getValue(2).setInitialValue("2");
        pnd_Valid_Distr_Cd.getValue(3).setInitialValue("7");
        pnd_Valid_Distr_Cd.getValue(4).setInitialValue("8");
        pnd_Valid_Distr_Cd.getValue(5).setInitialValue("P");
        pnd_Valid_Distr_Cd.getValue(6).setInitialValue("D");
        pnd_Pay_Stl_Ms.getValue(1).setInitialValue("1");
        pnd_Pay_Stl_Ms.getValue(2).setInitialValue("2");
        pnd_Pay_Stl_Ms.getValue(3).setInitialValue("7");
        pnd_Pay_Stl_Me.getValue(1).setInitialValue("8");
        pnd_Pay_Stl_Me.getValue(2).setInitialValue("P");
        pnd_Pay_Stl_Me.getValue(3).setInitialValue("D");
        pnd_Distr_7.setInitialValue("7");
        pnd_Vt_Ps_Err.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0702() throws Exception
    {
        super("Twrp0702");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        //* **
        //*  ACCEPTED RECORDS REPORT                                                                                                                                      //Natural: FORMAT ( 00 ) LS = 133 PS = 60
        //*  REJECTED RECORDS REPORT                                                                                                                                      //Natural: FORMAT ( 02 ) LS = 133 PS = 61
        //*  CITED RECORDS REPORT                                                                                                                                         //Natural: FORMAT ( 03 ) LS = 133 PS = 61
        //*  SUMMARY TOTALS REPORT                                                                                                                                        //Natural: FORMAT ( 04 ) LS = 133 PS = 61
        //*  REVERSAL RECORDS REPORT                                                                                                                                      //Natural: FORMAT ( 05 ) LS = 133 PS = 61
        //*  ADJUSTMENT RECORDS REPORT                                                                                                                                    //Natural: FORMAT ( 06 ) LS = 133 PS = 61
        //*                                                                                                                                                               //Natural: FORMAT ( 07 ) LS = 133 PS = 61
        //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOMP.#ABEND-IND := #TWRACOMP.#DISPLAY-IND := #TWRADIST.#ABEND-IND := #TWRADIST.#DISPLAY-IND := #TWRADICV.#ABEND-IND := #TWRADICV.#DISPLAY-IND := FALSE
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().setValue(false);
        pdaTwradist.getPnd_Twradist_Pnd_Abend_Ind().setValue(false);
        pdaTwradist.getPnd_Twradist_Pnd_Display_Ind().setValue(false);
        pdaTwradicv.getPnd_Twradicv_Pnd_Abend_Ind().setValue(false);
        pdaTwradicv.getPnd_Twradicv_Pnd_Display_Ind().setValue(false);
        pdaTwratin.getTwratin_Pnd_I_Tin().setValue(pnd_Ws_Const_Low_Values);                                                                                              //Natural: ASSIGN TWRATIN.#I-TIN := #TWRADIST.#IN-DIST := LOW-VALUES
        pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(pnd_Ws_Const_Low_Values);
        //*  TRANSACTION FILE
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD TWRT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaTwrl0700.getTwrt_Record())))
        {
            CheckAtStartofData1303();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  BEFORE BREAK PROCESSING
            //*    ACCEPT IF TWRT-CNTRCT-NBR = 'N9066019'
            //*    ACCEPT IF TWRT-TAX-ID     = '121620001'
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-COMPANY-CDE
            //*                                                                                                                                                           //Natural: AT BREAK OF TWRT-SOURCE
            //*  IF #DEBUG
            //*    WRITE '******************************'
            //*    WRITE '*** DISPLAY DATA VALUES    ***'
            //*    WRITE '******************************'
            //*    WRITE '=' TWRT-DISTRIBUTION-CODE
            //*    WRITE '=' TWRT-PLAN-TYPE
            //*    WRITE '=' TWRT-REASON-CODE
            //*    WRITE '=' TWRT-OP-REV-IND
            //*    WRITE '=' TWRT-OP-ADJ-IND
            //*    WRITE '=' TWRT-OP-MAINT-IND /
            //*  END-IF
            //*  PIN EXP 08/2017
            ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(), MoveOption.RightJustified);                                       //Natural: MOVE RIGHT TWRT-PIN-ID TO TWRT-PIN-ID
            //*  PIN EXP 08/2017
            DbsUtil.examine(new ExamineSource(ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(),true), new ExamineSearch(" "), new ExamineReplace("0"));                           //Natural: EXAMINE FULL TWRT-PIN-ID FOR ' ' REPLACE WITH '0'
            //*  BYPAS ALPHANUMERIC
            if (condition(! (DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id(),"999999999999"))))                                                              //Natural: IF TWRT-PIN-ID NE MASK ( 999999999999 )
            {
                //*  BYPAS ALPHANUMERIC
                ldaTwrl0700.getTwrt_Record_Twrt_Pin_Id().setValue("000000000000");                                                                                        //Natural: MOVE '000000000000' TO TWRT-PIN-ID
                //*  BYPAS ALPHANUMERIC
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Comp_Break.getBoolean()))                                                                                                            //Natural: IF #WS.#COMP-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
                sub_Get_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Comp_Break.reset();                                                                                                                            //Natural: RESET #WS.#COMP-BREAK
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Source_Break.getBoolean()))                                                                                                          //Natural: IF #WS.#SOURCE-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM READ-SUMMARY-RECORD
                sub_Read_Summary_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Source_Break.reset();                                                                                                                          //Natural: RESET #WS.#SOURCE-BREAK
            }                                                                                                                                                             //Natural: END-IF
            //*  INPUT RECORD COUNTER
            pnd_Ws_Pnd_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-CNT
            //* **********************************************************************
            //*  7/27/06 CORRECTION FOR "TEST01" PLAN ENTERED IN OMNIPAY FOR IPRO (JR)
            //* **********************************************************************
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num().equals("TEST01") && ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().equals(" ")))                          //Natural: IF TWRT-PLAN-NUM = 'TEST01' AND TWRT-LOB-CDE = ' '
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().setValue("GR1");                                                                                                //Natural: ASSIGN TWRT-LOB-CDE := 'GR1'
            }                                                                                                                                                             //Natural: END-IF
            //* **********************************************************************
            //*  7/27/06 CORRECTION FOR ROLLOVER TO IRA WITH EMPTY SUB-PLAN      (JR)
            //* **********************************************************************
            //*  RC06
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")))                                                                                         //Natural: IF TWRT-SOURCE = 'EW'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().equals(" ") && ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("G")                    //Natural: IF TWRT-LOB-CDE = ' ' AND TWRT-DISTRIBUTION-CODE = 'G' AND TWRT-PLAN-TYPE NE 'E'
                    && ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().notEquals("E")))
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().setValue("RA6");                                                                                            //Natural: ASSIGN TWRT-LOB-CDE := 'RA6'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* **********************************************************************
            //*  8/09/11 CORRECTION FOR "LOCALITY CODE' FOR RESIDENCY 'NY' F.ALLIE
            //* **********************************************************************
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("35") && ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt().notEquals(getZero())))         //Natural: IF TWRT-STATE-RSDNCY = '35' AND TWRT-LOCAL-WHHLD-AMT NE 0
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().setValue("3E");                                                                                            //Natural: ASSIGN TWRT-LOCALITY-CDE := '3E'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num().equals("TEST01") && ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().equals(" ")))                          //Natural: IF TWRT-PLAN-NUM = 'TEST01' AND TWRT-LOB-CDE = ' '
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().setValue("GR1");                                                                                                //Natural: ASSIGN TWRT-LOB-CDE := 'GR1'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().notEquals(pdaTwratin.getTwratin_Pnd_I_Tin())))                                                         //Natural: IF TWRT-TAX-ID NE TWRATIN.#I-TIN
            {
                pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id());                                                                     //Natural: ASSIGN TWRATIN.#I-TIN := TWRT-TAX-ID
                pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type());                                                           //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := TWRT-TAX-ID-TYPE
                DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
                    new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0700.getTwrt_Record_Twrt_Election_Code().reset();                                                                                                      //Natural: RESET TWRT-ELECTION-CODE #WS.#ER #WS.#RES-VALID #ERRORS ( * )
            pnd_Ws_Pnd_Er.reset();
            pnd_Ws_Pnd_Res_Valid.reset();
            pnd_Ws_Pnd_Errors.getValue("*").reset();
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") ||                   //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
                ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ")))
            {
                pnd_Ws_Pnd_Puri.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                              //Natural: ASSIGN #PURI := TWRT-STATE-WHHLD-AMT
                pnd_Ws_Pnd_State_Amt.reset();                                                                                                                             //Natural: RESET #STATE-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Puri.reset();                                                                                                                                  //Natural: RESET #PURI
                pnd_Ws_Pnd_State_Amt.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt());                                                                         //Natural: ASSIGN #STATE-AMT := TWRT-STATE-WHHLD-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                      //Natural: IF TWRT-OP-REV-IND = 'X'
            {
                pnd_Ws_Pnd_Rev_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                    //Natural: ADD 1 TO #REV-CNT ( #COMP-IDX )
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    pnd_Ws_Pnd_Rev_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #REV-GROSS-UNKN ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                              //Natural: IF #ROLL
                {
                    pnd_Ws_Pnd_Rev_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #REV-GROSS-ROLL ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Rev_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                        //Natural: ADD TWRT-GROSS-AMT TO #REV-GROSS ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                       //Natural: ADD TWRT-INTEREST-AMT TO #REV-INT ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                      //Natural: ADD TWRT-FED-WHHLD-AMT TO #REV-FED ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                      //Natural: ADD TWRT-NRA-WHHLD-AMT TO #REV-NRA ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                       //Natural: ADD #PURI TO #REV-PR ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                            //Natural: ADD TWRT-IVC-AMT TO #REV-IVC ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                               //Natural: ADD #STATE-AMT TO #REV-STATE ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                  //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #REV-LOCAL ( #COMP-IDX )
                pnd_Ws_Pnd_Rev_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #REV-CAN ( #COMP-IDX )
                pnd_Ws_Pnd_P_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte_Alpha());                                       //Natural: MOVE EDITED TWRT-PAYMT-DTE-ALPHA TO #P-DATE ( EM = YYYYMMDD )
                //*                                                        10/22/07     RM
                pnd_Ws_Pnd_Twrt_Gross_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Twrt_Gross_Amt), ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().multiply(-1));     //Natural: ASSIGN #TWRT-GROSS-AMT := TWRT-GROSS-AMT * -1
                pnd_Ws_Pnd_Twrt_Interest_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Twrt_Interest_Amt), ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().multiply(-1)); //Natural: ASSIGN #TWRT-INTEREST-AMT := TWRT-INTEREST-AMT * -1
                pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt), ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().multiply(-1)); //Natural: ASSIGN #TWRT-FED-WHHLD-AMT := TWRT-FED-WHHLD-AMT * -1
                pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Twrt_Nra_Whhld_Amt), ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().multiply(-1)); //Natural: ASSIGN #TWRT-NRA-WHHLD-AMT := TWRT-NRA-WHHLD-AMT * -1
                pnd_Ws_Pnd_Pnd_Puri.compute(new ComputeParameters(false, pnd_Ws_Pnd_Pnd_Puri), pnd_Ws_Pnd_Puri.multiply(-1));                                             //Natural: ASSIGN ##PURI := #PURI * -1
                pnd_Ws_Pnd_Twrt_Ivc_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Twrt_Ivc_Amt), ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().multiply(-1));           //Natural: ASSIGN #TWRT-IVC-AMT := TWRT-IVC-AMT * -1
                pnd_Ws_Pnd_Pnd_State_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Pnd_State_Amt), pnd_Ws_Pnd_State_Amt.multiply(-1));                              //Natural: ASSIGN ##STATE-AMT := #STATE-AMT * -1
                //*  WRITE REVERSAL DETAIL LINE
                getReports().write(6, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new           //Natural: WRITE ( 06 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #P-DATE TWRATIN.#O-TIN TWRT-TAX-ID-TYPE #TWRT-GROSS-AMT #TWRT-INTEREST-AMT 2X #TWRT-IVC-AMT 2X #TWRT-FED-WHHLD-AMT 1X ##STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 3X TWRT-PLAN-NUM TWRT-ORIG-CONTRACT-NBR
                    ColumnSpacing(1),pnd_Ws_Pnd_P_Date, new ReportEditMask ("MM/DD/YYYY"),pdaTwratin.getTwratin_Pnd_O_Tin(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),pnd_Ws_Pnd_Twrt_Gross_Amt,pnd_Ws_Pnd_Twrt_Interest_Amt,new 
                    ColumnSpacing(2),pnd_Ws_Pnd_Twrt_Ivc_Amt,new ColumnSpacing(2),pnd_Ws_Pnd_Twrt_Fed_Whhld_Amt,new ColumnSpacing(1),pnd_Ws_Pnd_Pnd_State_Amt,new 
                    ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),
                    ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      TWRT-LOCAL-WHHLD-AMT
                getReports().write(6, ReportOption.NOTITLE,new ColumnSpacing(77),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new ColumnSpacing(13),   //Natural: WRITE ( 06 ) 77X TWRT-NRA-WHHLD-AMT #PURI 13X TWRT-SUBPLAN TWRT-ORIG-SUBPLAN
                    ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" ")))                          //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' '
            {
                pnd_Ws_Pnd_Adj_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                    //Natural: ADD 1 TO #ADJ-CNT ( #COMP-IDX )
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    pnd_Ws_Pnd_Adj_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #ADJ-GROSS-UNKN ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                              //Natural: IF #ROLL
                {
                    pnd_Ws_Pnd_Adj_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #ADJ-GROSS-ROLL ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Adj_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                        //Natural: ADD TWRT-GROSS-AMT TO #ADJ-GROSS ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                       //Natural: ADD TWRT-INTEREST-AMT TO #ADJ-INT ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                      //Natural: ADD TWRT-FED-WHHLD-AMT TO #ADJ-FED ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                      //Natural: ADD TWRT-NRA-WHHLD-AMT TO #ADJ-NRA ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                       //Natural: ADD #PURI TO #ADJ-PR ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                            //Natural: ADD TWRT-IVC-AMT TO #ADJ-IVC ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                               //Natural: ADD #STATE-AMT TO #ADJ-STATE ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                  //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #ADJ-LOCAL ( #COMP-IDX )
                pnd_Ws_Pnd_Adj_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #ADJ-CAN ( #COMP-IDX )
                pnd_Ws_Pnd_P_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte_Alpha());                                       //Natural: MOVE EDITED TWRT-PAYMT-DTE-ALPHA TO #P-DATE ( EM = YYYYMMDD )
                //*  WRITE ADJUSTMENT DETAIL LINE
                getReports().write(7, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new           //Natural: WRITE ( 07 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #P-DATE TWRATIN.#O-TIN TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT 2X TWRT-IVC-AMT 2X TWRT-FED-WHHLD-AMT 1X #STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 3X TWRT-PLAN-NUM TWRT-ORIG-CONTRACT-NBR
                    ColumnSpacing(1),pnd_Ws_Pnd_P_Date, new ReportEditMask ("MM/DD/YYYY"),pdaTwratin.getTwratin_Pnd_O_Tin(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),new 
                    ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),new ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),new 
                    ColumnSpacing(1),pnd_Ws_Pnd_State_Amt,new ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new 
                    ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      TWRT-LOCAL-WHHLD-AMT
                getReports().write(7, ReportOption.NOTITLE,new ColumnSpacing(77),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new ColumnSpacing(13),   //Natural: WRITE ( 07 ) 77X TWRT-NRA-WHHLD-AMT #PURI 13X TWRT-SUBPLAN TWRT-ORIG-SUBPLAN
                    ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* ************************************************
            //*  COMPRESS ADDRESS ARRAY TO ELIMINATE BLANK LINES
            //* ************************************************
            pnd_Ws_Pnd_L.reset();                                                                                                                                         //Natural: RESET #L
            FOR02:                                                                                                                                                        //Natural: FOR #K 1 7
            for (pnd_Ws_Pnd_K.setValue(1); condition(pnd_Ws_Pnd_K.lessOrEqual(7)); pnd_Ws_Pnd_K.nadd(1))
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(pnd_Ws_Pnd_K).greater(" ")))                                                             //Natural: IF TWRT-NA-LINE ( #K ) > ' '
                {
                    pnd_Ws_Pnd_L.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #L
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(pnd_Ws_Pnd_K.getDec().add(1)).greater(" ")))                                         //Natural: IF TWRT-NA-LINE ( #K + 1 ) > ' '
                    {
                        pnd_Ws_Pnd_L.nadd(1);                                                                                                                             //Natural: ADD 1 TO #L
                        ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(pnd_Ws_Pnd_L).setValue(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(pnd_Ws_Pnd_K.getDec().add(1))); //Natural: MOVE TWRT-NA-LINE ( #K + 1 ) TO TWRT-NA-LINE ( #L )
                        ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(pnd_Ws_Pnd_K.getDec().add(1)).reset();                                                         //Natural: RESET TWRT-NA-LINE ( #K + 1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* **
            //*   VALIDATE SOME FIELDS OF THE FILE.
            //*   CHECK FATAL ERRORS FIRST (REJECTED) FIRST AND
            //*   THEN - CONDITIONAL(CITED) ONES.
            //* **
            pnd_Rev_Adj.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #REV-ADJ
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") || ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X")))                          //Natural: IF TWRT-OP-REV-IND = 'X' OR TWRT-OP-ADJ-IND = 'X'
            {
                pnd_Rev_Adj.setValue(true);                                                                                                                               //Natural: ASSIGN #REV-ADJ := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(),"YYYYMMDD")))                                                                   //Natural: IF TWRT-PAYMT-DTE = MASK ( YYYYMMDD )
            {
                pnd_Ws_Pnd_Work_Date.setValueEdited(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte(),new ReportEditMask("99999999"));                                          //Natural: MOVE EDITED TWRT-PAYMT-DTE ( EM = 99999999 ) TO #WORK-DATE
                pnd_Ws_Pnd_P_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Work_Date);                                                                    //Natural: MOVE EDITED #WORK-DATE TO #P-DATE ( EM = YYYYMMDD )
                if (condition(Global.getDATX().subtract(pnd_Ws_Pnd_P_Date).greaterOrEqual(-100) && pnd_Ws_Pnd_P_Date.lessOrEqual(800)))                                   //Natural: IF *DATX - #P-DATE = -100 THRU 800
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_E.setValue(8);                                                                                                                             //Natural: ASSIGN #E := 08
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Work_Date.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte());                                                                               //Natural: ASSIGN #WORK-DATE := TWRT-PAYMT-DTE
                pnd_Ws_Pnd_P_Date.reset();                                                                                                                                //Natural: RESET #P-DATE
                pnd_Ws_Pnd_E.setValue(8);                                                                                                                                 //Natural: ASSIGN #E := 08
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().notEquals(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code())))                                          //Natural: IF TWRT-SOURCE NE #INP-SUMMARY.#SOURCE-CODE
            {
                pnd_Ws_Pnd_E.setValue(1);                                                                                                                                 //Natural: ASSIGN #E := 01
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(106);  if (true) return;                                                                                                                //Natural: TERMINATE 106
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Comp_Idx.equals(getZero())))                                                                                                         //Natural: IF #COMP-IDX = 0
            {
                pnd_Ws_Pnd_E.setValue(2);                                                                                                                                 //Natural: ASSIGN #E := 02
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr().equals(" ")))                                                                                      //Natural: IF TWRT-CNTRCT-NBR = ' '
            {
                pnd_Ws_Pnd_E.setValue(3);                                                                                                                                 //Natural: ASSIGN #E := 03
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year().greaterOrEqual(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Tx_Year_N().subtract(1)) &&                     //Natural: IF NOT TWRT-TAX-YEAR = #INP-SUMMARY.#TX-YEAR-N - 1 THRU #INP-SUMMARY.#TX-YEAR-N
                ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year().lessOrEqual(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Tx_Year_N()))))
            {
                pnd_Ws_Pnd_E.setValue(29);                                                                                                                                //Natural: ASSIGN #E := 29
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  1099-INT, 1099-R , W-2
            short decideConditionsMet1924 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF TWRT-TAX-TYPE;//Natural: VALUE 'I', 'R' , 'W'
            if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("I") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("R") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("W"))))
            {
                decideConditionsMet1924++;
                //*      IF TWRT-SOURCE NE 'AM' /* VIKRAM2 COMMENT START
                //*        TWRT-PYMNT-TAX-FORM-1078  := 'Y'   /* W-9  -  NEEDED FOR ANNUAL
                //*      END-IF                               /* VL -  VIKRAM2 COMMENT END
                //*                                          /* RESTORE BELOW FOR ID3
                //*                                          /* REPLACE ABOVE 3 LINES
                //*  RC05 - VIKRAM2 START
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("AM") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL")))                            //Natural: IF TWRT-SOURCE EQ 'AM' OR = 'VL'
                {
                    ignore();
                    //*  W-9 - VIKRAM2 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().setValue("Y");                                                                                  //Natural: ASSIGN TWRT-PYMNT-TAX-FORM-1078 := 'Y'
                    //*  1042-S
                    //*  NO W-9
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("S"))))
            {
                decideConditionsMet1924++;
                ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().setValue("N");                                                                                      //Natural: ASSIGN TWRT-PYMNT-TAX-FORM-1078 := 'N'
                //*  STATE TAX INVALID
                if (condition(pnd_Ws_Pnd_State_Amt.notEquals(new DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))                                                    //Natural: IF #STATE-AMT NE 0.00 AND NOT #REV-ADJ
                {
                    pnd_Ws_Pnd_E.setValue(18);                                                                                                                            //Natural: ASSIGN #E := 18
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_E.setValue(78);                                                                                                                                //Natural: ASSIGN #E := 78
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RS1011
            //*  UNTIL 2011
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B ") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B1")            //Natural: IF TWRT-DISTRIBUTION-CODE = 'B ' OR = 'B1' OR = 'B2' OR = 'B4' OR = 'B8' OR = 'BL' OR = 'BG' OR = 'H' OR = 'B7' OR = 'H4'
                || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B2") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B4") 
                || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B8") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("BL") 
                || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("BG") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("H") || 
                ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B7") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("H4")))
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("ROTHP");                                                                                        //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'ROTHP'
                ldaTwrl0700.getTwrt_Record_Twrt_Roth_Qual_Ind().setValue("N");                                                                                            //Natural: ASSIGN TWRT-ROTH-QUAL-IND := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  W-2
                short decideConditionsMet1962 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF TWRT-PLAN-TYPE;//Natural: VALUE 'I'
                if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("I"))))
                {
                    decideConditionsMet1962++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("IRA");                                                                                      //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'IRA'
                }                                                                                                                                                         //Natural: VALUE 'E'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("E"))))
                {
                    decideConditionsMet1962++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("ROTH");                                                                                     //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'ROTH'
                }                                                                                                                                                         //Natural: VALUE 'S'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("S"))))
                {
                    decideConditionsMet1962++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("SEP");                                                                                      //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'SEP'
                    //* *    VALUE 'B', 'G', 'N', 'P'
                    //* *      TWRT-CONTRACT-TYPE   := 'DA-IA'
                }                                                                                                                                                         //Natural: VALUE 'B', 'G', 'N', 'P', '4'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("B") || ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("G") 
                    || ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("N") || ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("P") || ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type().equals("4"))))
                {
                    decideConditionsMet1962++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("DA-IA");                                                                                    //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'DA-IA'
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet1962 > 0))
                {
                    //*  ADD VL RC05
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("NV") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("AM") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL"))) //Natural: IF TWRT-SOURCE = 'NV' OR = 'AM' OR = 'VL'
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("PA");                                                                                   //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'PA'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    //*  09/28/11
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("AM")))                        //Natural: IF TWRT-SOURCE = 'VL' OR = 'AM'
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().setValue("PA");                                                                                   //Natural: ASSIGN TWRT-CONTRACT-TYPE := 'PA'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_E.setValue(79);                                                                                                                        //Natural: ASSIGN #E := 79
                                                                                                                                                                          //Natural: PERFORM W
                        sub_W();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type());                                                   //Natural: ASSIGN TWRT-DISTRIBUTION-TYPE := TWRT-PAYMT-TYPE
            ldaTwrl0700.getTwrt_Record_Twrt_Check_Reason().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());                                                        //Natural: ASSIGN TWRT-CHECK-REASON := TWRT-SETTL-TYPE
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind().equals("Y")))                                                                                    //Natural: IF TWRT-ROLLOVER-IND = 'Y'
            {
                pnd_Ws_Pnd_Roll.setValue(true);                                                                                                                           //Natural: ASSIGN #WS.#ROLL := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Roll.reset();                                                                                                                                  //Natural: RESET #WS.#ROLL
            }                                                                                                                                                             //Natural: END-IF
            //*  PROLINE F.ALLIE 04-06-2011
            pnd_Vt_Ps_Err.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #VT-PS-ERR
            //*        "
            //*        "
            //*        "
            //*        "
            //*        "
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().equals("VT")))                                                                                        //Natural: IF TWRT-LOB-CDE = 'VT'
            {
                pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().setValue(true);                                                                                                //Natural: ASSIGN #TWRADICV.#RET-CODE := TRUE
                pdaTwradicv.getPnd_Twradicv_Pnd_P_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type());                                                          //Natural: ASSIGN #TWRADICV.#P-TYPE := TWRT-PAYMT-TYPE
                pdaTwradicv.getPnd_Twradicv_Pnd_S_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());                                                          //Natural: ASSIGN #TWRADICV.#S-TYPE := TWRT-SETTL-TYPE
                pnd_Vt_Ps_Err.setValue(true);                                                                                                                             //Natural: ASSIGN #VT-PS-ERR := TRUE
                //*   WRITE '=== VT ===>  PPCN' TWRT-CNTRCT-NBR
                //*        "
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  RC06
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")))                                                                                     //Natural: IF TWRT-SOURCE = 'EW'
                {
                    pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().reset();                                                                                                   //Natural: RESET #TWRADICV.#RET-CODE #TIAA-IGNORE
                    pnd_Tiaa_Ignore.reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tiaa_Ignore.reset();                                                                                                                              //Natural: RESET #TIAA-IGNORE TWRT-PAYMT-TYPE TWRT-SETTL-TYPE #P-S-TYPES #TWRADICV.#RET-CODE #TWRADICV.#P-TYPE #TWRADICV.#S-TYPE
                    ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().reset();
                    ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().reset();
                    pdaTwradicv.getPnd_Twradicv_Pnd_P_S_Types().reset();
                    pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().reset();
                    pdaTwradicv.getPnd_Twradicv_Pnd_P_Type().reset();
                    pdaTwradicv.getPnd_Twradicv_Pnd_S_Type().reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-DISTRIBUTION-CODE
            sub_Convert_Distribution_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-PAY-SET-TYPES
            sub_Check_Pay_Set_Types();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DUTTAD
                                                                                                                                                                          //Natural: PERFORM DISTRIBUTION-D-CONVERSION
            sub_Distribution_D_Conversion();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PROLINE F.ALLIE 04-06-2011
            if (condition(! (pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().getBoolean()) || pnd_Vt_Ps_Err.getBoolean()))                                                     //Natural: IF NOT #TWRADICV.#RET-CODE OR #VT-PS-ERR
            {
                pnd_Ws_Pnd_E.setValue(9);                                                                                                                                 //Natural: ASSIGN #E := 09
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                                  //Natural: IF #WS.#ROLL
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pay_Set_Types().equals("NX")))                                                                              //Natural: IF TWRT-PAY-SET-TYPES = 'NX'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().notEquals(new DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))                     //Natural: IF TWRT-FED-WHHLD-AMT NE 0.00 AND NOT #REV-ADJ
                    {
                        pnd_Ws_Pnd_E.setValue(17);                                                                                                                        //Natural: ASSIGN #E := 17
                                                                                                                                                                          //Natural: PERFORM W
                        sub_W();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt().notEquals(new DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))                   //Natural: IF TWRT-STATE-WHHLD-AMT NE 0.00 AND NOT #REV-ADJ
                    {
                        pnd_Ws_Pnd_E.setValue(18);                                                                                                                        //Natural: ASSIGN #E := 18
                                                                                                                                                                          //Natural: PERFORM W
                        sub_W();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().equals(" ") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().equals("000000000") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().equals("0"))) //Natural: IF TWRT-TAX-ID = ' ' OR = '000000000' OR = '0'
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),           //Natural: COMPRESS TWRT-CNTRCT-NBR TWRT-PAYEE-CDE INTO TWRT-TAX-ID LEAVING NO SPACE
                    ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde()));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().getSubstring(1,9),"999999999") && ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().getSubstring(10,1).equals(" "))  //Natural: IF SUBSTR ( TWRT-TAX-ID,1,9 ) = MASK ( 999999999 ) AND SUBSTR ( TWRT-TAX-ID,10,1 ) = ' ' OR SUBSTR ( TWRT-TAX-ID,1,2 ) NE '99'
                || !ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id().getSubstring(1,2).equals("99"))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().setValue("5");                                                                                              //Natural: ASSIGN TWRT-TAX-ID-TYPE := '5'
            }                                                                                                                                                             //Natural: END-IF
            //*   CHECK STATE TABLE IF THE CODE IS NUMERIC
            //*   CHECK COUNTRY TABLE IF THE CODE IS ALPHA
            //* **
            pnd_Ws_Pnd_Disp_Locality.reset();                                                                                                                             //Natural: RESET #DISP-LOCALITY
            short decideConditionsMet2061 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRT-STATE-RSDNCY = '35' AND TWRT-LOCALITY-CDE = '3E'
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("35") && ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))
            {
                decideConditionsMet2061++;
                pnd_Ws_Pnd_Res_Valid.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#RES-VALID := TRUE
            }                                                                                                                                                             //Natural: WHEN TWRT-STATE-RSDNCY = MASK ( 99 )
            else if (condition(DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),"99")))
            {
                decideConditionsMet2061++;
                                                                                                                                                                          //Natural: PERFORM CHECK-STATE
                sub_Check_State();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Pnd_Res_Valid.getBoolean()))                                                                                                         //Natural: IF #WS.#RES-VALID
                {
                    pnd_Ws_Pnd_Disp_Locality.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde());                                                                    //Natural: ASSIGN #DISP-LOCALITY := TWRT-LOCALITY-CDE
                    ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().reset();                                                                                               //Natural: RESET TWRT-LOCALITY-CDE
                    //* PALDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*      TWRT-ANNT-LOCALITY-CODE := TWRT-LOCALITY-CDE
                ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().setValue(" ");                                                                                       //Natural: ASSIGN TWRT-ANNT-LOCALITY-CODE := ' '
                ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().reset();                                                                                                   //Natural: RESET TWRT-LOCALITY-CDE
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("PR")))                                                                               //Natural: IF TWRT-STATE-RSDNCY = 'PR'
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().setValue("RQ");                                                                                        //Natural: ASSIGN TWRT-STATE-RSDNCY := 'RQ'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-COUNTRY
                sub_Check_Country();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(! (pnd_Ws_Pnd_Res_Valid.getBoolean())))                                                                                                         //Natural: IF NOT #WS.#RES-VALID
            {
                pnd_Ws_Pnd_E.setValue(13);                                                                                                                                //Natural: ASSIGN #E := 13
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                                   //Natural: IF TWRT-GROSS-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(16);                                                                                                                                //Natural: ASSIGN #E := 16
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().greater(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt()) && ! (pnd_Rev_Adj.getBoolean())))                //Natural: IF TWRT-IVC-AMT > TWRT-GROSS-AMT AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(16);                                                                                                                                //Natural: ASSIGN #E := 16
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().equals(new DbsDecimal("0.00")) && ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().equals(new         //Natural: IF TWRT-INTEREST-AMT = 0.00 AND TWRT-GROSS-AMT = 0.00 AND NOT #REV-ADJ
                DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))
            {
                pnd_Ws_Pnd_E.setValue(43);                                                                                                                                //Natural: ASSIGN #E := 43
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().less(new DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))                                  //Natural: IF TWRT-FED-WHHLD-AMT < 0.00 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(17);                                                                                                                                //Natural: ASSIGN #E := 17
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().less(new DbsDecimal("0.00")) && ! (pnd_Rev_Adj.getBoolean())))                                  //Natural: IF TWRT-NRA-WHHLD-AMT < 0.00 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(18);                                                                                                                                //Natural: ASSIGN #E := 18
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                                //Natural: IF TWRT-INTEREST-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(19);                                                                                                                                //Natural: ASSIGN #E := 19
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                                     //Natural: IF TWRT-IVC-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(80);                                                                                                                                //Natural: ASSIGN #E := 80
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                             //Natural: IF TWRT-LOCAL-WHHLD-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(25);                                                                                                                                //Natural: ASSIGN #E := 25
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                               //Natural: IF TWRT-CAN-WHHLD-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(32);                                                                                                                                //Natural: ASSIGN #E := 32
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                               //Natural: IF TWRT-NRA-WHHLD-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(33);                                                                                                                                //Natural: ASSIGN #E := 33
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Total_Distr_Amt().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                             //Natural: IF TWRT-TOTAL-DISTR-AMT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(40);                                                                                                                                //Natural: ASSIGN #E := 40
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Distr_Pct().less(getZero()) && ! (pnd_Rev_Adj.getBoolean())))                                                   //Natural: IF TWRT-DISTR-PCT < 0 AND NOT #REV-ADJ
            {
                pnd_Ws_Pnd_E.setValue(41);                                                                                                                                //Natural: ASSIGN #E := 41
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*    ACCEPT OMNI INTEREST ONLY PAYMENT WITH TAX WITHHOLDING FOR NRA
            //*  OMNI_NRA
            //*   09/28/11
            //*   RC06
            //*   ADD 'VL' RC05
            //*     "
            //*     "
            //*     "
            if (condition(((((ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("S") && ((((ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("OP") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("NV"))  //Natural: IF TWRT-TAX-TYPE = 'S' AND TWRT-SOURCE = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL' AND TWRT-INTEREST-AMT NE 0.00 AND TWRT-GROSS-AMT = 0.00 AND TWRT-NRA-WHHLD-AMT NE 0.00
                || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("AM")) || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")) || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL"))) 
                && ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().notEquals(new DbsDecimal("0.00"))) && ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().equals(new 
                DbsDecimal("0.00"))) && ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().notEquals(new DbsDecimal("0.00")))))
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().add(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()).greater(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt())  //Natural: IF TWRT-FED-WHHLD-AMT + TWRT-STATE-WHHLD-AMT + TWRT-LOCAL-WHHLD-AMT + TWRT-CAN-WHHLD-AMT + TWRT-NRA-WHHLD-AMT > TWRT-INTEREST-AMT AND NOT #REV-ADJ
                    && ! (pnd_Rev_Adj.getBoolean())))
                {
                    pnd_Ws_Pnd_E.setValue(50);                                                                                                                            //Natural: ASSIGN #E := 50
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  OMNI_NRA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().add(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt()).add(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()).greater(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt())  //Natural: IF TWRT-FED-WHHLD-AMT + TWRT-STATE-WHHLD-AMT + TWRT-LOCAL-WHHLD-AMT + TWRT-CAN-WHHLD-AMT + TWRT-NRA-WHHLD-AMT > TWRT-GROSS-AMT AND NOT #REV-ADJ
                    && ! (pnd_Rev_Adj.getBoolean())))
                {
                    pnd_Ws_Pnd_E.setValue(50);                                                                                                                            //Natural: ASSIGN #E := 50
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  REJECT OMNI NRA COMBINED PENSION AND INTEREST PAYMENTS WITH TAX WTHLD
            //*  OMNI_NRA
            //*   09/28/11
            //*   RC06
            //*  ADD 'VL' RC05
            //*     "
            //*     "
            //*     "
            //*     "
            if (condition(((((ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("S") && ((((ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("OP") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("NV"))  //Natural: IF TWRT-TAX-TYPE = 'S' AND TWRT-SOURCE = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL' AND TWRT-INTEREST-AMT NE 0.00 AND TWRT-GROSS-AMT NE 0.00 AND TWRT-NRA-WHHLD-AMT NE 0.00
                || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("AM")) || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")) || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL"))) 
                && ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().notEquals(new DbsDecimal("0.00"))) && ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().notEquals(new 
                DbsDecimal("0.00"))) && ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().notEquals(new DbsDecimal("0.00")))))
            {
                pnd_Ws_Pnd_E.setValue(86);                                                                                                                                //Natural: ASSIGN #E := 86
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  OMNI_NRA
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(1).equals(" ")))                                                                             //Natural: IF TWRT-NA-LINE ( 1 ) = ' '
            {
                pnd_Ws_Pnd_E.setValue(22);                                                                                                                                //Natural: ASSIGN #E := 22
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1) || ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(2) || ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(3)))) //Natural: IF NOT ( TWRT-CITIZENSHIP = 1 OR = 2 OR = 3 )
            {
                pnd_Ws_Pnd_E.setValue(14);                                                                                                                                //Natural: ASSIGN #E := 14
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y") && ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))        //Natural: IF TWRT-PYMNT-TAX-FORM-1078 = 'Y' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
            {
                pnd_Ws_Pnd_E.setValue(34);                                                                                                                                //Natural: ASSIGN #E := 34
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") ||                   //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
                ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ")))
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1)))                                                                                   //Natural: IF TWRT-CITIZENSHIP = 1
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().setValue("42");                                                                                        //Natural: ASSIGN TWRT-STATE-RSDNCY := '42'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().setValue("RQ");                                                                                        //Natural: ASSIGN TWRT-STATE-RSDNCY := 'RQ'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") || ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X")))                          //Natural: IF TWRT-OP-REV-IND = 'X' OR TWRT-OP-ADJ-IND = 'X'
            {
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PAYMENT
                sub_Lookup_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*   REVERSAL NOT MATCHED
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") && ! (pnd_Rev_Match.getBoolean())))                                                    //Natural: IF TWRT-OP-REV-IND = 'X' AND NOT #REV-MATCH
            {
                pnd_Ws_Pnd_E.setValue(81);                                                                                                                                //Natural: ASSIGN #E := 81
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ADJUSTMENT REC - RCC
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" ")))                          //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM GET-ADJUSTMENT-PAYMENT
                sub_Get_Adjustment_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*   ADJUSTMENT W/MAINTENANCE
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" ") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind().equals("X"))) //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' ' AND TWRT-OP-MAINT-IND = 'X'
            {
                pnd_Ws_Pnd_E.setValue(83);                                                                                                                                //Natural: ASSIGN #E := 83
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   NON-FINANCIAL MAINTENANCE
            //*   (ADJUSTMENT IMPLIED)
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals(" ") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" "))) //Natural: IF TWRT-OP-MAINT-IND = 'X' AND TWRT-OP-ADJ-IND = ' ' AND TWRT-OP-REV-IND = ' '
            {
                pnd_Ws_Pnd_E.setValue(83);                                                                                                                                //Natural: ASSIGN #E := 83
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*   REVERSAL W/MAINTENANCE
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Maint_Ind().equals("X")))                        //Natural: IF TWRT-OP-REV-IND = 'X' AND TWRT-OP-MAINT-IND = 'X'
            {
                pnd_Ws_Pnd_E.setValue(85);                                                                                                                                //Natural: ASSIGN #E := 85
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* ***************************
            //*   CITED ERROR PROCESSING  *
            //* ***************************
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" ")))                          //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' '
            {
                if (condition(pnd_Adj_Net_Neg.getBoolean()))                                                                                                              //Natural: IF #ADJ-NET-NEG
                {
                    pnd_Ws_Pnd_E.setValue(82);                                                                                                                            //Natural: ASSIGN #E := 82
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*    IF TWRT-NRA-EXEMPTION-CODE = '2 ' OR = ' 2' OR = '4 ' OR = ' 4'
                //*        OR = ' '
                //*      IGNORE
                //*    ELSE
                //*      #E := 84  PERFORM W
                //*    END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  INT. ONLY
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().equals(new DbsDecimal("0.00")) && ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().notEquals(new      //Natural: IF TWRT-GROSS-AMT = 0.00 AND TWRT-INTEREST-AMT NE 0.00
                DbsDecimal("0.00"))))
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().reset();                                                                                                        //Natural: RESET TWRT-IVC-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().notEquals(new DbsDecimal("0.00"))))                                                         //Natural: IF TWRT-GROSS-AMT NE 0.00
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().setValue("U");                                                                                          //Natural: ASSIGN TWRT-IVC-CDE := 'U'
                        pnd_Ws_Pnd_E.setValue(20);                                                                                                                        //Natural: ASSIGN #E := 20
                                                                                                                                                                          //Natural: PERFORM W
                        sub_W();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().reset();                                                                                                //Natural: RESET TWRT-IVC-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Na_Line().getValue(7).greater(" ")))                                                                            //Natural: IF TWRT-NA-LINE ( 7 ) > ' '
            {
                pnd_Ws_Pnd_E.setValue(23);                                                                                                                                //Natural: ASSIGN #E := 23
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde().equals("00") || ! (DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),                 //Natural: IF TWRT-PAYEE-CDE = '00' OR TWRT-PAYEE-CDE NE MASK ( 99 )
                "99"))))
            {
                pnd_Ws_Pnd_E.setValue(4);                                                                                                                                 //Natural: ASSIGN #E := 04
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde().setValue("01");                                                                                               //Natural: ASSIGN TWRT-PAYEE-CDE := '01'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().equals("5") && ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1)))                          //Natural: IF TWRT-TAX-ID-TYPE = '5' AND TWRT-CITIZENSHIP = 1
            {
                pnd_Ws_Pnd_E.setValue(6);                                                                                                                                 //Natural: ASSIGN #E := 06
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().greaterOrEqual("1") && ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().lessOrEqual("5"))))       //Natural: IF NOT TWRT-TAX-ID-TYPE = '1' THRU '5'
            {
                pnd_Ws_Pnd_E.setValue(5);                                                                                                                                 //Natural: ASSIGN #E := 05
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type().setValue(1);                                                                                                //Natural: ASSIGN TWRT-TAX-ID-TYPE := 01
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet2298 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRT-DOB = 0
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Dob().equals(getZero())))
            {
                decideConditionsMet2298++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN TWRT-DOB = MASK ( YYYYMMDD ) AND TWRT-DOB < *DATN
            else if (condition(DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Dob(),"YYYYMMDD") && ldaTwrl0700.getTwrt_Record_Twrt_Dob().less(Global.getDATN())))
            {
                decideConditionsMet2298++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ws_Pnd_E.setValue(15);                                                                                                                                //Natural: ASSIGN #E := 15
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaTwrl0700.getTwrt_Record_Twrt_Dob().reset();                                                                                                            //Natural: RESET TWRT-DOB
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RC06
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")))                                                                                         //Natural: IF TWRT-SOURCE = 'EW'
            {
                if (condition(DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Dod(),"YYYYMMDD") || ldaTwrl0700.getTwrt_Record_Twrt_Dod().equals(getZero())))          //Natural: IF TWRT-DOD = MASK ( YYYYMMDD ) OR = 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_E.setValue(30);                                                                                                                            //Natural: ASSIGN #E := 30
                                                                                                                                                                          //Natural: PERFORM W
                    sub_W();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  1099-INT, 1099-R
            //*  W-8BEN
            if (condition(((ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("I") || ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("R")) && ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().equals("Y")))) //Natural: IF TWRT-TAX-TYPE = 'I' OR = 'R' AND TWRT-PYMNT-TAX-FORM-1001 = 'Y'
            {
                ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001().setValue("N");                                                                                      //Natural: ASSIGN TWRT-PYMNT-TAX-FORM-1001 := 'N'
                pnd_Ws_Pnd_E.setValue(42);                                                                                                                                //Natural: ASSIGN #E := 42
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaTwrl0700.getTwrt_Record_Twrt_Payment_Form1001().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1001());                                           //Natural: ASSIGN TWRT-PAYMENT-FORM1001 := TWRT-PYMNT-TAX-FORM-1001
            if (condition(pnd_Ws_Pnd_Disp_Locality.notEquals(" ")))                                                                                                       //Natural: IF #DISP-LOCALITY NE ' '
            {
                pnd_Ws_Pnd_E.setValue(11);                                                                                                                                //Natural: ASSIGN #E := 11
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind().equals("Y") || ldaTwrl0700.getTwrt_Record_Twrt_Rollover_Ind().equals("N"))))                  //Natural: IF NOT TWRT-ROLLOVER-IND = 'Y' OR = 'N'
            {
                pnd_Ws_Pnd_E.setValue(12);                                                                                                                                //Natural: ASSIGN #E := 12
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().greater(getZero()) && ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().greater(getZero())))      //Natural: IF TWRT-FED-WHHLD-AMT > 0 AND TWRT-NRA-WHHLD-AMT > 0
            {
                pnd_Ws_Pnd_E.setValue(44);                                                                                                                                //Natural: ASSIGN #E := 44
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt().less(getZero())))                                                                              //Natural: IF TWRT-TOT-IVC-9B-AMT < 0
            {
                pnd_Ws_Pnd_Disp_Amt.setValueEdited(ldaTwrl0700.getTwrt_Record_Twrt_Tot_Ivc_9b_Amt(),new ReportEditMask("-ZZZ,ZZZ,ZZ9.99"));                               //Natural: MOVE EDITED TWRT-TOT-IVC-9B-AMT ( EM = -ZZZ,ZZZ,ZZ9.99 ) TO #DISP-AMT
                pnd_Ws_Pnd_E.setValue(26);                                                                                                                                //Natural: ASSIGN #E := 26
                                                                                                                                                                          //Natural: PERFORM W
                sub_W();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  CITED
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Flag().equals("C")))                                                                                            //Natural: IF TWRT-FLAG = 'C'
            {
                pnd_Ws_Pnd_Cit_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                    //Natural: ADD 1 TO #CIT-CNT ( #COMP-IDX )
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    pnd_Ws_Pnd_Cit_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #CIT-GROSS-UNKN ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                              //Natural: IF #ROLL
                {
                    pnd_Ws_Pnd_Cit_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #CIT-GROSS-ROLL ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                        //Natural: ADD TWRT-GROSS-AMT TO #CIT-GROSS ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                       //Natural: ADD TWRT-INTEREST-AMT TO #CIT-INT ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                      //Natural: ADD TWRT-FED-WHHLD-AMT TO #CIT-FED ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                      //Natural: ADD TWRT-NRA-WHHLD-AMT TO #CIT-NRA ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                       //Natural: ADD #PURI TO #CIT-PR ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                            //Natural: ADD TWRT-IVC-AMT TO #CIT-IVC ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                               //Natural: ADD #STATE-AMT TO #CIT-STATE ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                  //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #CIT-LOCAL ( #COMP-IDX )
                pnd_Ws_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #CIT-CAN ( #COMP-IDX )
                //*  CITED
                getWorkFiles().write(4, false, ldaTwrl0700.getTwrt_Record(), pnd_Ws_Pnd_Errors.getValue(1,":",60));                                                       //Natural: WRITE WORK FILE 04 TWRT-RECORD #ERRORS ( 1:60 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Acc_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                    //Natural: ADD 1 TO #ACC-CNT ( #COMP-IDX )
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    pnd_Ws_Pnd_Acc_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #ACC-GROSS-UNKN ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                              //Natural: IF #ROLL
                {
                    pnd_Ws_Pnd_Acc_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #ACC-GROSS-ROLL ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Acc_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                        //Natural: ADD TWRT-GROSS-AMT TO #ACC-GROSS ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                       //Natural: ADD TWRT-INTEREST-AMT TO #ACC-INT ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                      //Natural: ADD TWRT-FED-WHHLD-AMT TO #ACC-FED ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                      //Natural: ADD TWRT-NRA-WHHLD-AMT TO #ACC-NRA ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                       //Natural: ADD #PURI TO #ACC-PR ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                            //Natural: ADD TWRT-IVC-AMT TO #ACC-IVC ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                               //Natural: ADD #STATE-AMT TO #ACC-STATE ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                  //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #ACC-LOCAL ( #COMP-IDX )
                pnd_Ws_Pnd_Acc_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #ACC-CAN ( #COMP-IDX )
                //* *   CREATE A RECORD ON THE ACCEPTED RECORDS EXTRACT
                //*  ACCEPTED
                getWorkFiles().write(2, false, ldaTwrl0700.getTwrt_Record(), pnd_Ws_Pnd_Errors.getValue(1,":",60));                                                       //Natural: WRITE WORK FILE 02 TWRT-RECORD #ERRORS ( 1:60 )
                //* **  CREATE LINE ON THE ACCEPTED RECS REPORT !!!
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new           //Natural: WRITE ( 02 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #P-DATE TWRATIN.#O-TIN TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT 2X TWRT-IVC-AMT 2X TWRT-FED-WHHLD-AMT 1X #STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 3X TWRT-PLAN-NUM TWRT-ORIG-CONTRACT-NBR
                    ColumnSpacing(1),pnd_Ws_Pnd_P_Date, new ReportEditMask ("MM/DD/YYYY"),pdaTwratin.getTwratin_Pnd_O_Tin(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),new 
                    ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),new ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),new 
                    ColumnSpacing(1),pnd_Ws_Pnd_State_Amt,new ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new 
                    ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      TWRT-LOCAL-WHHLD-AMT
                getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(77),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new ColumnSpacing(13),   //Natural: WRITE ( 02 ) 77X TWRT-NRA-WHHLD-AMT #PURI 13X TWRT-SUBPLAN TWRT-ORIG-SUBPLAN
                    ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF EXTRACT PROCESSING
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Rec_Cnt.equals(getZero())))                                                                                                              //Natural: IF #REC-CNT = 0
        {
            //*  PERFORM  ERROR-DISPLAY-START
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Tax Transaction File (Work File 01) is Empty",new TabSetting(77),"***");                 //Natural: WRITE ( 0 ) '***' 25T 'Tax Transaction File (Work File 01) is Empty' 77T '***'
            if (Global.isEscape()) return;
            //*  PERFORM  ERROR-DISPLAY-END
            //*  TERMINATE 111
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Rev_Cnt.getValue("*").greater(getZero())) && pnd_Ws_Pnd_Rec_Cnt.greater(getZero())))                                                  //Natural: IF NOT #REV-CNT ( * ) > 0 AND #REC-CNT > 0
        {
            getReports().write(6, ReportOption.NOTITLE,"***",new TabSetting(25),"No Reversals Found on Tax Transaction File  ",new TabSetting(77),"***");                 //Natural: WRITE ( 06 ) '***' 25T 'No Reversals Found on Tax Transaction File  ' 77T '***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Adj_Cnt.getValue("*").greater(getZero())) && pnd_Ws_Pnd_Rec_Cnt.greater(getZero())))                                                  //Natural: IF NOT #ADJ-CNT ( * ) > 0 AND #REC-CNT > 0
        {
            getReports().write(7, ReportOption.NOTITLE,"***",new TabSetting(25),"No Adjustments Found on Tax Transaction File",new TabSetting(77),"***");                 //Natural: WRITE ( 07 ) '***' 25T 'No Adjustments Found on Tax Transaction File' 77T '***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ReportOption.NOTITLE,"Total Input    Records:",pnd_Ws_Pnd_Rec_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"));                                      //Natural: WRITE 'Total Input    Records:' #REC-CNT
        if (Global.isEscape()) return;
        //*  -----------------------------------------           /* << DUTTAD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISTRIBUTION-D-CONVERSION
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PAYMENT
        //*  =========================
        //*  CHANGE SIGN FOR REVERSALS
        //*  =========================
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-DISTRIBUTION-CODE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACTUAL-DIST-ACCESS
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PAY-SET-TYPES
        //* *************************************
        //* **  CHECK THE PAYMENT & SETTLEMENT TYPES (DISTR. CODE TABLE)
        //* * MOVE SUBSTR(TWRT-SUBPLAN,1,3) TO #TWRADICV.#LOB
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-STATE
        //* *****************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-COUNTRY
        //* *******************************
        //* *******************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-AGE-ROUTINE
        //*  #MDMA100.#I-SSN-A9         := TWRT-TAX-ID
        //*  CALLNAT 'MDMN100A' USING #MDMA100
        //*  IF #MDMA100.#O-RETURN-CODE = '0000'
        //* * CALCULATE AGE
        //* *******************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: W
        //* *******************
        //*  WRITE THE CITED RECORD OUT AND GENERATE THE REPORT LINE
        //*      TWRT-LOCAL-WHHLD-AMT
        //*    FOR #IDX = 1        TO  5
        //*        TWRT-LOCAL-WHHLD-AMT
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-TOTALS
        //* ***************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DIST-CODE-TABLE
        //* **************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-COUNTRY-TABLE
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-TABLE
        //* **********************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-SOURCE-TABLE
        //* ***********************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-COMPANY
        //* ******************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-SUMMARY-RECORD
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-RESIDENCY-TYPE
        //* ****************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-CITIZENSHIP-CODE
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-DIST-CODE-TABLE
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-RUN-DATE
        //* ********************************
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADJUSTMENT-PAYMENT
        //* *------------
        //* *******************
        //* *******************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  ****************************************
        //* *****************
        //*  REPORT HEADERS *
        //* *****************
        //* ******************
        //*  ACCEPTED RECORDS
        //* ******************
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 02 )
        //*  REJECTED RECORDS
        //* ******************
        //* ***************                                                                                                                                               //Natural: AT TOP OF PAGE ( 03 )
        //*  CITED RECORDS
        //* ***************
        //* ************************                                                                                                                                      //Natural: AT TOP OF PAGE ( 04 )
        //*  SUMMARY CONTROL REPORT
        //* ************************
        //* ******************                                                                                                                                            //Natural: AT TOP OF PAGE ( 05 )
        //*  REVERSAL RECORDS
        //* ******************
        //* ********************                                                                                                                                          //Natural: AT TOP OF PAGE ( 06 )
        //*  ADJUSTMENT RECORDS
        //* ********************
        //* **                                                                                                                                                            //Natural: AT TOP OF PAGE ( 07 )
        //*  FE201506 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    private void sub_Distribution_D_Conversion() throws Exception                                                                                                         //Natural: DISTRIBUTION-D-CONVERSION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------
        //*   DIST D CHANGES APPLIES TO US CITIZEN ONLY
        //*  SAURAV
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1) || ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y") ||                     //Natural: IF TWRT-CITIZENSHIP = 1 OR TWRT-PYMNT-TAX-FORM-1078 = 'Y' OR TWRT-LOB-CDE NE 'VT'
            ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().notEquals("VT")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Iaa_Contract.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr());                                                                                          //Natural: ASSIGN #IAA-CONTRACT := TWRT-CNTRCT-NBR
        pnd_Oc_N.reset();                                                                                                                                                 //Natural: RESET #OC-N
        vw_iaa_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #IAA-CONTRACT
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Iaa_Contract, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_iaa_Cntrct.readNextRow("FIND01", true)))
        {
            vw_iaa_Cntrct.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Cntrct.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  OPTION CODE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Oc_N.setValue(iaa_Cntrct_Cntrct_Orgn_Cde);                                                                                                                //Natural: ASSIGN #OC-N := CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("I") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("T")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("C")  //Natural: IF ( TWRT-PAYMT-TYPE = 'I' AND TWRT-SETTL-TYPE = 'T' ) OR ( TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'X' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'T' )
            && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("X")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("T"))))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Oc_N_Pnd_Oc_A.equals("03") || pnd_Oc_N_Pnd_Oc_A.equals("17") || pnd_Oc_N_Pnd_Oc_A.equals("18") || pnd_Oc_N_Pnd_Oc_A.equals("35")                //Natural: IF #OC-A = '03' OR = '17' OR = '18' OR = '35' OR = '36' OR = '37' OR = '38' OR = '40' OR = '42' OR = '43' OR = '44' OR = '45' OR = '46' OR TWRT-CONTRACT-TYPE = 'PA' OR = 'SR1' OR TWRT-SOURCE = 'VL' OR = 'NV'
            || pnd_Oc_N_Pnd_Oc_A.equals("36") || pnd_Oc_N_Pnd_Oc_A.equals("37") || pnd_Oc_N_Pnd_Oc_A.equals("38") || pnd_Oc_N_Pnd_Oc_A.equals("40") || pnd_Oc_N_Pnd_Oc_A.equals("42") 
            || pnd_Oc_N_Pnd_Oc_A.equals("43") || pnd_Oc_N_Pnd_Oc_A.equals("44") || pnd_Oc_N_Pnd_Oc_A.equals("45") || pnd_Oc_N_Pnd_Oc_A.equals("46") || ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().equals("PA") 
            || ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type().equals("SR1") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("VL") || ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("NV")))
        {
            if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("A") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("E")  //Natural: IF ( TWRT-PAYMT-TYPE = 'A' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'E' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'D' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'C' ) OR ( TWRT-PAYMT-TYPE = 'C' AND TWRT-SETTL-TYPE = 'A' ) OR ( TWRT-PAYMT-TYPE = 'N' AND TWRT-SETTL-TYPE = 'K' ) OR ( TWRT-PAYMT-TYPE = 'B' AND TWRT-SETTL-TYPE = 'C' )
                && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("D") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("C")) 
                || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("C")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("C") 
                && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("A")) || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("N") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("K")) 
                || (ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals("B") && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals("C"))))
            {
                //* *          DECIDE ON FIRST VALUE OF TWRT-DISTRIBUTION-CODE-1
                short decideConditionsMet2603 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF TWRT-DISTRIB-CDE;//Natural: VALUE '1'
                if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals("1"))))
                {
                    decideConditionsMet2603++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue("<");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := '<'
                }                                                                                                                                                         //Natural: VALUE '2'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals("2"))))
                {
                    decideConditionsMet2603++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue(">");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := '>'
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals("3"))))
                {
                    decideConditionsMet2603++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue("X");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'X'
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals("4"))))
                {
                    decideConditionsMet2603++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue("W");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'W'
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals("7"))))
                {
                    decideConditionsMet2603++;
                    ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue("V");                                                                                          //Natural: ASSIGN TWRT-DISTRIB-CDE := 'V'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().display(0, "PPCN",                                                                                                                           //Natural: DISPLAY 'PPCN' #IAA-CONTRACT 'SRC' TWRT-SOURCE 'CNTR TYP' TWRT-CONTRACT-TYPE 'ORG' #OC-A 'CTZN' TWRT-CITIZENSHIP 'DIST' TWRT-DISTRIBUTION-CODE 'DIST1' TWRT-DISTRIB-CDE '1078' TWRT-PYMNT-TAX-FORM-1078 'PAYTYP' TWRT-PAYMT-TYPE 'SETTLE' TWRT-SETTL-TYPE
                		pnd_Iaa_Contract,"SRC",
                		ldaTwrl0700.getTwrt_Record_Twrt_Source(),"CNTR TYP",
                		ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type(),"ORG",
                		pnd_Oc_N_Pnd_Oc_A,"CTZN",
                		ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),"DIST",
                		ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code(),"DIST1",
                		ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde(),"1078",
                		ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"PAYTYP",
                		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type(),"SETTLE",
                		ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> DUTTAD
    }
    private void sub_Lookup_Payment() throws Exception                                                                                                                    //Natural: LOOKUP-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Rev_Match.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #REV-MATCH
        pnd_Adj_Net_Neg.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #ADJ-NET-NEG
        pnd_Twrpymnt_Contract_Seq_No.setValue(0);                                                                                                                         //Natural: ASSIGN #TWRPYMNT-CONTRACT-SEQ-NO := 0
        pnd_Payment_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #PAYMENT-FOUND := FALSE
        pnd_Payment_24.setValue(false);                                                                                                                                   //Natural: ASSIGN #PAYMENT-24 := FALSE
                                                                                                                                                                          //Natural: PERFORM LOOKUP-RESIDENCY-TYPE
        sub_Lookup_Residency_Type();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOOKUP-CITIZENSHIP-CODE
        sub_Lookup_Citizenship_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOOKUP-DIST-CODE-TABLE
        sub_Lookup_Dist_Code_Table();
        if (condition(Global.isEscape())) {return;}
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                       //Natural: ASSIGN #TWR-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id());                                                                       //Natural: ASSIGN #TWR-TAX-ID-NBR := TWRT-TAX-ID
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr());                                                                 //Natural: ASSIGN #TWR-CONTRACT-NBR := TWRT-CNTRCT-NBR
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde());                                                                     //Natural: ASSIGN #TWR-PAYEE-CDE := TWRT-PAYEE-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde());                                                                 //Natural: ASSIGN #TWR-COMPANY-CDE := TWRT-COMPANY-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship.setValue(pnd_Twrpymnt_Tax_Citizenship);                                                                              //Natural: ASSIGN #TWR-TAX-CITIZENSHIP := #TWRPYMNT-TAX-CITIZENSHIP
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category.setValue(pnd_Twrpymnt_Paymt_Category);                                                                                //Natural: ASSIGN #TWR-PAYMT-CATEGORY := #TWRPYMNT-PAYMT-CATEGORY
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde());                                                                   //Natural: ASSIGN #TWR-DIST-CODE := TWRT-DISTRIB-CDE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type.setValue(pnd_Twrpymnt_Residency_Type);                                                                                //Natural: ASSIGN #TWR-RESIDENCY-TYPE := #TWRPYMNT-RESIDENCY-TYPE
        pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code.setValue(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy());                                                             //Natural: ASSIGN #TWR-RESIDENCY-CODE := TWRT-STATE-RSDNCY
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde().equals("3E")))                                                                                       //Natural: IF TWRT-LOCALITY-CDE = '3E'
        {
            pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.reset();                                                                                                           //Natural: RESET #TWR-LOCALITY-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   03/2010 JR
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().notEquals(" ") && ! (DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code(), //Natural: IF TWRT-ANNT-LOCALITY-CODE NE ' ' AND TWRT-ANNT-LOCALITY-CODE NE MASK ( ..' ' )
                "..' '"))))
            {
                pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code());                                                //Natural: ASSIGN #TWR-LOCALITY-CODE := TWRT-ANNT-LOCALITY-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde());                                                      //Natural: ASSIGN #TWR-LOCALITY-CODE := TWRT-LOCALITY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Omni_Summary_Amounts_Pnd_Omni_Gross.reset();                                                                                                                  //Natural: RESET #OMNI-GROSS #OMNI-INT #OMNI-IVC #OMNI-NRA #OMNI-FED #OMNI-STATE
        pnd_Omni_Summary_Amounts_Pnd_Omni_Int.reset();
        pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc.reset();
        pnd_Omni_Summary_Amounts_Pnd_Omni_Nra.reset();
        pnd_Omni_Summary_Amounts_Pnd_Omni_Fed.reset();
        pnd_Omni_Summary_Amounts_Pnd_Omni_State.reset();
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY WITH TWRPYMNT-FORM-SD = #TWRPYMNT-FORM-SD
        (
        "RL1",
        new Wc[] { new Wc("TWRPYMNT_FORM_SD", ">=", pnd_Twrpymnt_Form_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_FORM_SD", "ASC") }
        );
        RL1:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("RL1")))
        {
            if (condition(pnd_Payment_24.equals(false)))                                                                                                                  //Natural: IF #PAYMENT-24 = FALSE
            {
                if (condition(ldaTwrl9415.getPay_Twrpymnt_Locality_Cde().equals("3E")))                                                                                   //Natural: IF PAY.TWRPYMNT-LOCALITY-CDE = '3E'
                {
                    pnd_Twrpymnt_Locality_Cde.setValue(" ");                                                                                                              //Natural: ASSIGN #TWRPYMNT-LOCALITY-CDE := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Twrpymnt_Locality_Cde.setValue(ldaTwrl9415.getPay_Twrpymnt_Locality_Cde());                                                                       //Natural: ASSIGN #TWRPYMNT-LOCALITY-CDE := PAY.TWRPYMNT-LOCALITY-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Year.equals(ldaTwrl9415.getPay_Twrpymnt_Tax_Year()) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Id_Nbr.equals(ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr())  //Natural: IF #TWR-TAX-YEAR = PAY.TWRPYMNT-TAX-YEAR AND #TWR-TAX-ID-NBR = PAY.TWRPYMNT-TAX-ID-NBR AND #TWR-CONTRACT-NBR = PAY.TWRPYMNT-CONTRACT-NBR AND #TWR-PAYEE-CDE = PAY.TWRPYMNT-PAYEE-CDE AND #TWR-COMPANY-CDE = PAY.TWRPYMNT-COMPANY-CDE AND #TWR-TAX-CITIZENSHIP = PAY.TWRPYMNT-TAX-CITIZENSHIP AND #TWR-PAYMT-CATEGORY = PAY.TWRPYMNT-PAYMT-CATEGORY AND #TWR-DIST-CODE = PAY.TWRPYMNT-DISTRIBUTION-CDE AND #TWR-RESIDENCY-TYPE = PAY.TWRPYMNT-RESIDENCY-TYPE AND #TWR-RESIDENCY-CODE = PAY.TWRPYMNT-RESIDENCY-CODE AND #TWR-LOCALITY-CODE = #TWRPYMNT-LOCALITY-CDE
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Contract_Nbr.equals(ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr()) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Payee_Cde.equals(ldaTwrl9415.getPay_Twrpymnt_Payee_Cde()) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Company_Cde.equals(ldaTwrl9415.getPay_Twrpymnt_Company_Cde()) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Tax_Citizenship.equals(ldaTwrl9415.getPay_Twrpymnt_Tax_Citizenship()) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Paymt_Category.equals(ldaTwrl9415.getPay_Twrpymnt_Paymt_Category()) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Dist_Code.equals(ldaTwrl9415.getPay_Twrpymnt_Distribution_Cde()) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Type.equals(ldaTwrl9415.getPay_Twrpymnt_Residency_Type()) && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Residency_Code.equals(ldaTwrl9415.getPay_Twrpymnt_Residency_Code()) 
                && pnd_Twrpymnt_Form_Sd_Pnd_Twr_Locality_Code.equals(pnd_Twrpymnt_Locality_Cde)))
            {
                pnd_Payment_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #PAYMENT-FOUND := TRUE
                pnd_Twrpymnt_Contract_Seq_No.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TWRPYMNT-CONTRACT-SEQ-NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            j.setValue(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments());                                                                                                 //Natural: ASSIGN J := C*PAY.TWRPYMNT-PAYMENTS
            if (condition(j.greaterOrEqual(24)))                                                                                                                          //Natural: IF J >= 24
            {
                j.setValue(24);                                                                                                                                           //Natural: ASSIGN J := 24
                pnd_Payment_24.setValue(true);                                                                                                                            //Natural: ASSIGN #PAYMENT-24 := TRUE
                pnd_Payment_Found.setValue(false);                                                                                                                        //Natural: ASSIGN #PAYMENT-FOUND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR K = 1 TO J
            for (k.setValue(1); condition(k.lessOrEqual(j)); k.nadd(1))
            {
                //*  DUTTAD1
                //*  >> DUTTAD
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X")))                                                                                  //Natural: IF TWRT-OP-REV-IND = 'X'
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt()), NMath.abs              //Natural: ASSIGN TWRT-GROSS-AMT := ABS ( TWRT-GROSS-AMT )
                        (ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt()));
                    ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt()), NMath.abs                  //Natural: ASSIGN TWRT-IVC-AMT := ABS ( TWRT-IVC-AMT )
                        (ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt()));
                    ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt()),                  //Natural: ASSIGN TWRT-INTEREST-AMT := ABS ( TWRT-INTEREST-AMT )
                        NMath.abs (ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt()));
                    ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt()),                //Natural: ASSIGN TWRT-FED-WHHLD-AMT := ABS ( TWRT-FED-WHHLD-AMT )
                        NMath.abs (ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt()));
                    ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()),                //Natural: ASSIGN TWRT-NRA-WHHLD-AMT := ABS ( TWRT-NRA-WHHLD-AMT )
                        NMath.abs (ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()));
                    ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt().compute(new ComputeParameters(false, ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()),            //Natural: ASSIGN TWRT-STATE-WHHLD-AMT := ABS ( TWRT-STATE-WHHLD-AMT )
                        NMath.abs (ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()));
                    //*  DUTTAD1
                }                                                                                                                                                         //Natural: END-IF
                //*  ========================
                //*  CHECK FOR REVERSAL MATCH
                //*  ========================
                //*  09/28/11
                //*  RC06
                //*  RC05
                if (condition((((((((((ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals("X") && (((((ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("OP")  //Natural: IF TWRT-OP-REV-IND = 'X' AND ( PAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL' OR ( #DEBUG AND PAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'NL' OR = ' ' ) ) AND PAY.TWRPYMNT-PYMNT-STATUS ( K ) = ' ' OR = 'C' AND PAY.TWRPYMNT-PYMNT-DTE ( K ) = TWRT-PAYMT-DTE-ALPHA AND PAY.TWRPYMNT-GROSS-AMT ( K ) = TWRT-GROSS-AMT AND PAY.TWRPYMNT-IVC-AMT ( K ) = TWRT-IVC-AMT AND PAY.TWRPYMNT-INT-AMT ( K ) = TWRT-INTEREST-AMT AND PAY.TWRPYMNT-FED-WTHLD-AMT ( K ) = TWRT-FED-WHHLD-AMT AND PAY.TWRPYMNT-NRA-WTHLD-AMT ( K ) = TWRT-NRA-WHHLD-AMT AND PAY.TWRPYMNT-STATE-WTHLD ( K ) = TWRT-STATE-WHHLD-AMT
                    || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("NV")) || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("AM")) 
                    || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("EW")) || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("VL")) 
                    || (pnd_Debug.getBoolean() && (ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("NL") || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals(" "))))) 
                    && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals(" ") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals("C"))) 
                    && ldaTwrl9415.getPay_Twrpymnt_Pymnt_Dte().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte_Alpha())) && ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt())) 
                    && ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt())) && ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt())) 
                    && ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt())) && ldaTwrl9415.getPay_Twrpymnt_Nra_Wthld_Amt().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt())) 
                    && ldaTwrl9415.getPay_Twrpymnt_State_Wthld().getValue(k).equals(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()))))
                {
                    if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan().equals(" ") && ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr().equals(" ")))         //Natural: IF TWRT-ORIG-SUBPLAN = ' ' AND TWRT-ORIG-CONTRACT-NBR = ' '
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan().setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Subplan().getValue(pnd_Ws_Pnd_K));                       //Natural: MOVE PAY.TWRPYMNT-ORIG-SUBPLAN ( #K ) TO TWRT-ORIG-SUBPLAN
                        ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr().setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Contract_Nbr().getValue(k));                        //Natural: MOVE PAY.TWRPYMNT-ORIG-CONTRACT-NBR ( K ) TO TWRT-ORIG-CONTRACT-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Rev_Match.setValue(true);                                                                                                                         //Natural: ASSIGN #REV-MATCH := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  ============================
                //*  ACCUM FOR NET NEGATIVE CHECK
                //*  =============================
                //*  09/28/11
                //*  RC06
                //*  RC05
                if (condition((((ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" "))                    //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' ' AND PAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'OP' OR = 'NV' OR = 'AM' OR = 'EW' OR = 'VL' AND PAY.TWRPYMNT-PYMNT-STATUS ( K ) = ' ' OR = 'C'
                    && ((((ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("OP") || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("NV")) 
                    || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("AM")) || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("EW")) 
                    || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("VL"))) && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals(" ") 
                    || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals("C")))))
                {
                    pnd_Omni_Summary_Amounts_Pnd_Omni_Gross.nadd(ldaTwrl9415.getPay_Twrpymnt_Gross_Amt().getValue(k));                                                    //Natural: ASSIGN #OMNI-GROSS := #OMNI-GROSS + PAY.TWRPYMNT-GROSS-AMT ( K )
                    pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc.nadd(ldaTwrl9415.getPay_Twrpymnt_Ivc_Amt().getValue(k));                                                        //Natural: ASSIGN #OMNI-IVC := #OMNI-IVC + PAY.TWRPYMNT-IVC-AMT ( K )
                    pnd_Omni_Summary_Amounts_Pnd_Omni_Int.nadd(ldaTwrl9415.getPay_Twrpymnt_Int_Amt().getValue(k));                                                        //Natural: ASSIGN #OMNI-INT := #OMNI-INT + PAY.TWRPYMNT-INT-AMT ( K )
                    pnd_Omni_Summary_Amounts_Pnd_Omni_Fed.nadd(ldaTwrl9415.getPay_Twrpymnt_Fed_Wthld_Amt().getValue(k));                                                  //Natural: ASSIGN #OMNI-FED := #OMNI-FED + PAY.TWRPYMNT-FED-WTHLD-AMT ( K )
                    pnd_Omni_Summary_Amounts_Pnd_Omni_Nra.nadd(ldaTwrl9415.getPay_Twrpymnt_Nra_Wthld_Amt().getValue(k));                                                  //Natural: ASSIGN #OMNI-NRA := #OMNI-NRA + PAY.TWRPYMNT-NRA-WTHLD-AMT ( K )
                    pnd_Omni_Summary_Amounts_Pnd_Omni_State.nadd(ldaTwrl9415.getPay_Twrpymnt_State_Wthld().getValue(k));                                                  //Natural: ASSIGN #OMNI-STATE := #OMNI-STATE + PAY.TWRPYMNT-STATE-WTHLD ( K )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RL1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RL1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(j.less(24)))                                                                                                                                    //Natural: IF J < 24
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Op_Adj_Ind().equals("X") && ldaTwrl0700.getTwrt_Record_Twrt_Op_Rev_Ind().equals(" ") && (pnd_Omni_Summary_Amounts_Pnd_Omni_Gross.add(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt()).less(getZero())  //Natural: IF TWRT-OP-ADJ-IND = 'X' AND TWRT-OP-REV-IND = ' ' AND ( #OMNI-GROSS + TWRT-GROSS-AMT < 0 OR #OMNI-IVC + TWRT-IVC-AMT < 0 OR #OMNI-INT + TWRT-INTEREST-AMT < 0 OR #OMNI-FED + TWRT-FED-WHHLD-AMT < 0 OR #OMNI-NRA + TWRT-NRA-WHHLD-AMT < 0 OR #OMNI-STATE + TWRT-STATE-WHHLD-AMT < 0 )
            || pnd_Omni_Summary_Amounts_Pnd_Omni_Ivc.add(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt()).less(getZero()) || pnd_Omni_Summary_Amounts_Pnd_Omni_Int.add(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt()).less(getZero()) 
            || pnd_Omni_Summary_Amounts_Pnd_Omni_Fed.add(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt()).less(getZero()) || pnd_Omni_Summary_Amounts_Pnd_Omni_Nra.add(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt()).less(getZero()) 
            || pnd_Omni_Summary_Amounts_Pnd_Omni_State.add(ldaTwrl0700.getTwrt_Record_Twrt_State_Whhld_Amt()).less(getZero()))))
        {
            pnd_Adj_Net_Neg.setValue(true);                                                                                                                               //Natural: ASSIGN #ADJ-NET-NEG := TRUE
            pnd_Ws_Pnd_Neg_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                        //Natural: ADD 1 TO #NEG-CNT ( #COMP-IDX )
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                      //Natural: IF TWRT-IVC-CDE NE 'K'
            {
                pnd_Ws_Pnd_Neg_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                   //Natural: ADD TWRT-GROSS-AMT TO #NEG-GROSS-UNKN ( #COMP-IDX )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                                  //Natural: IF #ROLL
            {
                pnd_Ws_Pnd_Neg_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                   //Natural: ADD TWRT-GROSS-AMT TO #NEG-GROSS-ROLL ( #COMP-IDX )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Neg_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                            //Natural: ADD TWRT-GROSS-AMT TO #NEG-GROSS ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                           //Natural: ADD TWRT-INTEREST-AMT TO #NEG-INT ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                          //Natural: ADD TWRT-FED-WHHLD-AMT TO #NEG-FED ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                          //Natural: ADD TWRT-NRA-WHHLD-AMT TO #NEG-NRA ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                           //Natural: ADD #PURI TO #NEG-PR ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                                //Natural: ADD TWRT-IVC-AMT TO #NEG-IVC ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                                   //Natural: ADD #STATE-AMT TO #NEG-STATE ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                      //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #NEG-LOCAL ( #COMP-IDX )
            pnd_Ws_Pnd_Neg_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                          //Natural: ADD TWRT-CAN-WHHLD-AMT TO #NEG-CAN ( #COMP-IDX )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Convert_Distribution_Code() throws Exception                                                                                                         //Natural: CONVERT-DISTRIBUTION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        //*  10/22/07   RM
        //*  RCC 04/26
        //*  RS1011 START
        short decideConditionsMet2753 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TWRT-DISTRIBUTION-CODE;//Natural: VALUE 'A7'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("A7"))))
        {
            decideConditionsMet2753++;
            ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("7");                                                                                            //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := '7'
        }                                                                                                                                                                 //Natural: VALUE 'A6', 'AA6'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("A6") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("AA6"))))
        {
            decideConditionsMet2753++;
            ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("6");                                                                                            //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := '6'
        }                                                                                                                                                                 //Natural: VALUE 'A4', 'AA4'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("A4") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("AA4"))))
        {
            decideConditionsMet2753++;
            ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("4");                                                                                            //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := '4'
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("B"))))
        {
            decideConditionsMet2753++;
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year().greaterOrEqual(2011)))                                                                               //Natural: IF TWRT-TAX-YEAR GE 2011
            {
                                                                                                                                                                          //Natural: PERFORM GET-AGE-ROUTINE
                sub_Get_Age_Routine();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Over_59_Half.getBoolean()))                                                                                                             //Natural: IF #OVER-59-HALF
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("B7");                                                                                   //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := 'B7'
                    //*  VIKRAM START
                    //*  VIKRAM END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("B1");                                                                                   //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := 'B1'
                }                                                                                                                                                         //Natural: END-IF
                //*  RS1011 END
                //*  VIKRAM1 START
                //*  VIKRAM1 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'L2', 'L7'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("L2") || ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals("L7"))))
        {
            decideConditionsMet2753++;
            ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().setValue("L");                                                                                            //Natural: ASSIGN TWRT-DISTRIBUTION-CODE := 'L'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code())))                                          //Natural: IF #TWRADIST.#IN-DIST NE TWRT-DISTRIBUTION-CODE
        {
            pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code());                                                      //Natural: ASSIGN #TWRADIST.#IN-DIST := TWRT-DISTRIBUTION-CODE
            pdaTwradist.getPnd_Twradist_Pnd_Tax_Year().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                              //Natural: ASSIGN #TWRADIST.#TAX-YEAR := TWRT-TAX-YEAR
            pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("2");                                                                                                     //Natural: ASSIGN #TWRADIST.#FUNCTION := '2'
                                                                                                                                                                          //Natural: PERFORM ACTUAL-DIST-ACCESS
            sub_Actual_Dist_Access();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  INT. ONLY
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt().equals(new DbsDecimal("0.00")) && ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt().notEquals(new          //Natural: IF TWRT-GROSS-AMT = 0.00 AND TWRT-INTEREST-AMT NE 0.00
            DbsDecimal("0.00"))))
        {
            ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().reset();                                                                                                        //Natural: RESET TWRT-DISTRIB-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                                           //Natural: ASSIGN TWRT-DISTRIB-CDE := #TWRADIST.#OUT-DIST
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Actual_Dist_Access() throws Exception                                                                                                                //Natural: ACTUAL-DIST-ACCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        DbsUtil.callnat(Twrndist.class , getCurrentProcessState(), pdaTwradist.getPnd_Twradist_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradist.getPnd_Twradist_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDIST' USING #TWRADIST.#INPUT-PARMS ( AD = O ) #TWRADIST.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Check_Pay_Set_Types() throws Exception                                                                                                               //Natural: CHECK-PAY-SET-TYPES
    {
        if (BLNatReinput.isReinput()) return;

        pdaTwradicv.getPnd_Twradicv_Pnd_Tax_Year().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                  //Natural: ASSIGN #TWRADICV.#TAX-YEAR := TWRT-TAX-YEAR
        pdaTwradicv.getPnd_Twradicv_Pnd_Source().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Source());                                                                      //Natural: ASSIGN #TWRADICV.#SOURCE := TWRT-SOURCE
        //*  #TWRADICV.#REASON-CODE     := TWRT-REASON-CODE                /* VR01
        //*  VR01
        short decideConditionsMet2803 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN TWRT-SOURCE = 'OP' AND TWRT-RMD-IND = 'Y'
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("OP") && ldaTwrl0700.getTwrt_Record_Twrt_Rmd_Ind().equals("Y")))
        {
            decideConditionsMet2803++;
            //*  RC07
            //*  RC07
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Subplan().getSubstring(3,1).equals("M")))                                                                       //Natural: IF SUBSTR ( TWRT-SUBPLAN,3,1 ) = 'M'
            {
                pdaTwradicv.getPnd_Twradicv_Pnd_Reason_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Reason_Code());                                                    //Natural: ASSIGN #TWRADICV.#REASON-CODE := TWRT-REASON-CODE
                //*  RC07
                //*  SYSTEMATIC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTwradicv.getPnd_Twradicv_Pnd_Reason_Code().setValue("SM");                                                                                             //Natural: ASSIGN #TWRADICV.#REASON-CODE := 'SM'
                //*  RC07
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaTwradicv.getPnd_Twradicv_Pnd_Reason_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Reason_Code());                                                        //Natural: ASSIGN #TWRADICV.#REASON-CODE := TWRT-REASON-CODE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaTwradicv.getPnd_Twradicv_Pnd_Dist_Code().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code());                                                        //Natural: ASSIGN #TWRADICV.#DIST-CODE := TWRT-DISTRIBUTION-CODE
        pdaTwradicv.getPnd_Twradicv_Pnd_Plan_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type());                                                                //Natural: ASSIGN #TWRADICV.#PLAN-TYPE := TWRT-PLAN-TYPE
        pdaTwradicv.getPnd_Twradicv_Pnd_Lob().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde());                                                                        //Natural: ASSIGN #TWRADICV.#LOB := TWRT-LOB-CDE
        pdaTwradicv.getPnd_Twradicv_Pnd_Gross_Amt().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                                                //Natural: ASSIGN #TWRADICV.#GROSS-AMT := TWRT-GROSS-AMT
        pdaTwradicv.getPnd_Twradicv_Pnd_Int_Amt().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                                               //Natural: ASSIGN #TWRADICV.#INT-AMT := TWRT-INTEREST-AMT
        //*  IF #DEBUG
        //*    WRITE (00) /
        //*      '=' #TWRADICV.#TAX-YEAR  /
        //*      '=' #TWRADICV.#SOURCE /
        //*      '=' #TWRADICV.#REASON-CODE /
        //*      '=' #TWRADICV.#DIST-CODE /
        //*      '=' #TWRADICV.#LOB /
        //*      '=' #TWRADICV.#GROSS-AMT /
        //*      '=' #TWRADICV.#INT-AMT /
        //*  END-IF
        //*   W-2 NEW SUBPROGRAM (TWRN0702) TO DETERMINE PRODUCT & SETTLEMENT TYPE
        //*  PROLINE  F.ALLIE  04-06-2011
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Lob_Cde().notEquals("VT")))                                                                                         //Natural: IF TWRT-LOB-CDE NE 'VT'
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type().equals("W") && ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code().equals(pnd_Valid_Distr_Cd.getValue("*")))) //Natural: IF TWRT-TAX-TYPE = 'W' AND TWRT-DISTRIBUTION-CODE = #VALID-DISTR-CD ( * )
            {
                pnd_Tiaa_Ignore.reset();                                                                                                                                  //Natural: RESET #TIAA-IGNORE TWRT-DISTRIB-CDE
                ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().reset();
                DbsUtil.callnat(Twrn0702.class , getCurrentProcessState(), ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type(), ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),       //Natural: CALLNAT 'TWRN0702' USING TWRT-PLAN-TYPE TWRT-PLAN-NUM TWRT-DISTRIBUTION-CODE TWRT-DISTRIB-CDE TWRT-REASON-CODE #P-S-TYPES #TWRADICV.#RET-CODE #TIAA-IGNORE
                    ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code(), ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde(), ldaTwrl0700.getTwrt_Record_Twrt_Reason_Code(), 
                    pdaTwradicv.getPnd_Twradicv_Pnd_P_S_Types(), pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code(), pnd_Tiaa_Ignore);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  RC06
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Source().equals("EW")))                                                                                     //Natural: IF TWRT-SOURCE = 'EW'
                {
                    pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().setValue(true);                                                                                            //Natural: ASSIGN #TWRADICV.#RET-CODE := TRUE
                    pdaTwradicv.getPnd_Twradicv_Pnd_P_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type());                                                      //Natural: ASSIGN #TWRADICV.#P-TYPE := TWRT-PAYMT-TYPE
                    pdaTwradicv.getPnd_Twradicv_Pnd_S_Type().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());                                                      //Natural: ASSIGN #TWRADICV.#S-TYPE := TWRT-SETTL-TYPE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.callnat(Twrndicv.class , getCurrentProcessState(), pdaTwradicv.getPnd_Twradicv_Pnd_Input_Parms(), new AttributeParameter("O"),                //Natural: CALLNAT 'TWRNDICV' USING #TWRADICV.#INPUT-PARMS ( AD = O ) #TWRADICV.#OUTPUT-DATA ( AD = M )
                        pdaTwradicv.getPnd_Twradicv_Pnd_Output_Data(), new AttributeParameter("M"));
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #DEBUG
        //*    WRITE (00) '=' #TWRADICV.#RET-CODE (EM=N/Y)
        //*  END-IF
        //*  VALID P/S TYPES
        if (condition(pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().getBoolean()))                                                                                           //Natural: IF #TWRADICV.#RET-CODE
        {
            ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().setValue(pdaTwradicv.getPnd_Twradicv_Pnd_P_Type());                                                              //Natural: ASSIGN TWRT-PAYMT-TYPE := #TWRADICV.#P-TYPE
            ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().setValue(pdaTwradicv.getPnd_Twradicv_Pnd_S_Type());                                                              //Natural: ASSIGN TWRT-SETTL-TYPE := #TWRADICV.#S-TYPE
            FOR04:                                                                                                                                                        //Natural: FOR #DIST-IDX = 1 TO #T-DIST-CNT
            for (pnd_Ws_Pnd_Dist_Idx.setValue(1); condition(pnd_Ws_Pnd_Dist_Idx.lessOrEqual(pnd_Ws_Pnd_T_Dist_Cnt)); pnd_Ws_Pnd_Dist_Idx.nadd(1))
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type().equals(pnd_Ws_Pnd_T_Payment_Type.getValue(pnd_Ws_Pnd_Dist_Idx)) && ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type().equals(pnd_Ws_Pnd_T_Settle_Type.getValue(pnd_Ws_Pnd_Dist_Idx))  //Natural: IF TWRT-PAYMT-TYPE = #T-PAYMENT-TYPE ( #DIST-IDX ) AND TWRT-SETTL-TYPE = #T-SETTLE-TYPE ( #DIST-IDX ) AND TWRT-DISTRIB-CDE = #T-DIST-CODE-1 ( #DIST-IDX )
                    && ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde().equals(pnd_Ws_Pnd_T_Dist_Code_1.getValue(pnd_Ws_Pnd_Dist_Idx))))
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Type_Refer().setValue(pnd_Ws_Pnd_T_Refer_Num.getValue(pnd_Ws_Pnd_Dist_Idx));                                          //Natural: ASSIGN TWRT-TYPE-REFER := #T-REFER-NUM ( #DIST-IDX )
                    pnd_Vt_Ps_Err.setValue(false);                                                                                                                        //Natural: ASSIGN #VT-PS-ERR := FALSE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwradicv.getPnd_Twradicv_Pnd_Ret_Code().reset();                                                                                                               //Natural: RESET #TWRADICV.#RET-CODE
    }
    //*  VALIDATE STATE CODE
    private void sub_Check_State() throws Exception                                                                                                                       //Natural: CHECK-STATE
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #ST-IDX = 1 TO #T-STATE-CNT
        for (pnd_Ws_Pnd_St_Idx.setValue(1); condition(pnd_Ws_Pnd_St_Idx.lessOrEqual(pnd_Ws_Pnd_T_State_Cnt)); pnd_Ws_Pnd_St_Idx.nadd(1))
        {
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals(pnd_Ws_Pnd_T_State_Code.getValue(pnd_Ws_Pnd_St_Idx))))                                    //Natural: IF TWRT-STATE-RSDNCY = #T-STATE-CODE ( #ST-IDX )
            {
                pnd_Ws_Pnd_Res_Valid.setValue(true);                                                                                                                      //Natural: ASSIGN #WS.#RES-VALID := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  CHECK THE COUNTRY CODE
    private void sub_Check_Country() throws Exception                                                                                                                     //Natural: CHECK-COUNTRY
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #CN-IDX = 1 TO #T-COUNTRY-CNT
        for (pnd_Ws_Pnd_Cn_Idx.setValue(1); condition(pnd_Ws_Pnd_Cn_Idx.lessOrEqual(pnd_Ws_Pnd_T_Country_Cnt)); pnd_Ws_Pnd_Cn_Idx.nadd(1))
        {
            //*  WRITE '=' TWRT-STATE-RSDNCY '=' #T-COUNTRY-CODE2(#CN-IDX)
            //*  /  '='  TWRT-ANNT-LOCALITY-CODE '=' #T-COUNTRY-CODE3(#CN-IDX)
            if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals(pnd_Ws_Pnd_T_Country_Code2.getValue(pnd_Ws_Pnd_Cn_Idx))))                                 //Natural: IF TWRT-STATE-RSDNCY = #T-COUNTRY-CODE2 ( #CN-IDX )
            {
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().equals(" ") || ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code().equals(pnd_Ws_Pnd_T_Country_Code3.getValue(pnd_Ws_Pnd_Cn_Idx)))) //Natural: IF TWRT-ANNT-LOCALITY-CODE = ' ' OR TWRT-ANNT-LOCALITY-CODE = #T-COUNTRY-CODE3 ( #CN-IDX )
                {
                    pnd_Ws_Pnd_Res_Valid.setValue(true);                                                                                                                  //Natural: ASSIGN #WS.#RES-VALID := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE '------------------------------------------------------------'
    }
    //*  RS1011 START
    private void sub_Get_Age_Routine() throws Exception                                                                                                                   //Natural: GET-AGE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Over_59_Half.setValue(false);                                                                                                                                 //Natural: ASSIGN #OVER-59-HALF := FALSE
        //* * GET DOB
        //*  #TWRAPART.#TNAD-ADDR     := FALSE  /* FE201506 START COMMENT OUT
        //*  #TWRAPART.#ABEND-IND     := FALSE
        //*  #TWRAPART.#DISPLAY-IND   := FALSE
        //*  #TWRAPART.#DEBUG-IND     := FALSE
        //*  #TWRAPART.#FUNCTION      := 'R'
        //*  #TWRAPART.#TAX-YEAR      := TWRT-TAX-YEAR
        //*  #TWRAPART.#TIN           := TWRT-TAX-ID /* FE201506 END COMMENT OUT
        //*  RESET #MDMA100                            /* FE201506 START
        //*  PIN EXP 08/2017
        //*   PIN EXP 08/2017
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn_A9().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id());                                                                      //Natural: ASSIGN #MDMA101.#I-SSN-A9 := TWRT-TAX-ID
        //*   PIN EXP 08/2017
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' USING #MDMA101
        if (condition(Global.isEscape())) return;
        //*  IF #MDMA100.#O-RETURN-CODE = '0000'
        //*   PIN EXP 08/2017
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE = '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                                                                //Natural: WRITE '=' #MDMA101.#O-RETURN-CODE
            if (Global.isEscape()) return;
            //*      ?????
            getReports().write(0, ReportOption.NOTITLE,ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id(),"SSN NOT ON MDM   ");                                                     //Natural: WRITE TWRT-TAX-ID 'SSN NOT ON MDM   '
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  VIKRAM
            //*  VIKRAM
        }                                                                                                                                                                 //Natural: END-IF
        pdaTwra0710.getPnd_Twra0710_Pnd_Lookup_Type().setValue(1);                                                                                                        //Natural: ASSIGN #TWRA0710.#LOOKUP-TYPE := 1
        pdaTwra0710.getPnd_Twra0710_Pnd_Plus_Half().setValue(true);                                                                                                       //Natural: ASSIGN #TWRA0710.#PLUS-HALF := TRUE
        pdaTwra0710.getPnd_Twra0710_Pnd_Years().setValue(59);                                                                                                             //Natural: ASSIGN #TWRA0710.#YEARS := 59
        //*  MOVE #MDMA100.#O-DATE-OF-BIRTH TO #TWRA0710.#DDOB
        //*   PIN EXP 08/2017
        pdaTwra0710.getPnd_Twra0710_Pnd_Ddob().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                                 //Natural: MOVE #MDMA101.#O-DATE-OF-BIRTH TO #TWRA0710.#DDOB
        pdaTwra0710.getPnd_Twra0710_Pnd_Ddop().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Dte());                                                                     //Natural: ASSIGN #TWRA0710.#DDOP := TWRT-PAYMT-DTE
        DbsUtil.callnat(Twrn0710.class , getCurrentProcessState(), pdaTwra0710.getPnd_Twra0710());                                                                        //Natural: CALLNAT 'TWRN0710' USING #TWRA0710
        if (condition(Global.isEscape())) return;
        if (condition(pdaTwra0710.getPnd_Twra0710_Pnd_Flag().equals("Y")))                                                                                                //Natural: IF #FLAG = 'Y'
        {
            pnd_Over_59_Half.setValue(true);                                                                                                                              //Natural: ASSIGN #OVER-59-HALF := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-AGE-ROUTINE           /* RS1011 END
    }
    //*  ERROR ROUTINE !!!!
    private void sub_W() throws Exception                                                                                                                                 //Natural: W
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Er_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                            //Natural: ASSIGN #ER-YR := TWRT-TAX-YEAR
        pnd_Ws_Pnd_Er_Nbr.setValue(pnd_Ws_Pnd_E);                                                                                                                         //Natural: ASSIGN #ER-NBR := #E
        pnd_Ws_Pnd_Er_Desc.getValue("*").reset();                                                                                                                         //Natural: RESET #ER-DESC ( * )
        ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().startDatabaseFind                                                                                             //Natural: FIND ( 1 ) TIRCNTL-ERROR-MSG-TBL-VIEW-VIEW WITH TIRCNTL-NBR-YEAR-SEQ = #ER-SU
        (
        "FIND02",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_SEQ", "=", pnd_Ws_Pnd_Er_Su, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().readNextRow("FIND02", true)))
        {
            ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().setIfNotFoundControlFlag(false);
            if (condition(ldaTwrl9807.getVw_tircntl_Error_Msg_Tbl_View_View().getAstCOUNTER().equals(0)))                                                                 //Natural: IF NO RECORDS FOUND
            {
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress("Specified Error Was Not Found:", pnd_Ws_Pnd_E));          //Natural: COMPRESS 'Specified Error Was Not Found:' #E INTO TIRCNTL-ERROR-DESCR
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Type().setValue("R");                                                                        //Natural: ASSIGN TIRCNTL-ERROR-TYPE := 'R'
            }                                                                                                                                                             //Natural: END-NOREC
            short decideConditionsMet2953 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #E;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_E.equals(1))))
            {
                decideConditionsMet2953++;
                pnd_Ws_Pnd_Er_Desc.getValue(1).setValue(DbsUtil.compress("Detail  Source Code:", ldaTwrl0700.getTwrt_Record_Twrt_Source()));                              //Natural: COMPRESS 'Detail  Source Code:' TWRT-SOURCE INTO #ER-DESC ( 1 )
                pnd_Ws_Pnd_Er_Desc.getValue(2).setValue(DbsUtil.compress("Summary Source Code:", ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code()));                      //Natural: COMPRESS 'Summary Source Code:' #INP-SUMMARY.#SOURCE-CODE INTO #ER-DESC ( 2 )
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_E.equals(2))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-COMPANY-CDE INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde()));
                pnd_Ws_Pnd_Er_Desc.getValue(1).setValue(DbsUtil.compress("Tax Year:", ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A()));                                     //Natural: COMPRESS 'Tax Year:' TWRT-TAX-YEAR-A INTO #ER-DESC ( 1 )
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ws_Pnd_E.equals(5))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-TAX-ID-TYPE INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type()));
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Ws_Pnd_E.equals(7))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-SOURCE INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Source()));
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_Ws_Pnd_E.equals(8))))
            {
                decideConditionsMet2953++;
                //*  CHG 6 TO7, ADD 6 (VR01)
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(CompressOption.Numeric, ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS NUMERIC TIRCNTL-ERROR-DESCR #WORK-DATE INTO TIRCNTL-ERROR-DESCR
                    pnd_Ws_Pnd_Work_Date));
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_Ws_Pnd_E.equals(9))))
            {
                decideConditionsMet2953++;
                pnd_Ws_Pnd_Er_Desc.getValue(1).setValue(DbsUtil.compress("Payment Type:", ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type()));                                 //Natural: COMPRESS 'Payment Type:' TWRT-PAYMT-TYPE INTO #ER-DESC ( 1 )
                pnd_Ws_Pnd_Er_Desc.getValue(2).setValue(DbsUtil.compress("Settl.  Type:", ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type()));                                 //Natural: COMPRESS 'Settl.  Type:' TWRT-SETTL-TYPE INTO #ER-DESC ( 2 )
                pnd_Ws_Pnd_Er_Desc.getValue(3).setValue(DbsUtil.compress("Distrib Code:", ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code()));                          //Natural: COMPRESS 'Distrib Code:' TWRT-DISTRIBUTION-CODE INTO #ER-DESC ( 3 )
                pnd_Ws_Pnd_Er_Desc.getValue(4).setValue(DbsUtil.compress("Sub Plan    :", ldaTwrl0700.getTwrt_Record_Twrt_Subplan()));                                    //Natural: COMPRESS 'Sub Plan    :' TWRT-SUBPLAN INTO #ER-DESC ( 4 )
                pnd_Ws_Pnd_Er_Desc.getValue(5).setValue(DbsUtil.compress("Reason Code :", ldaTwrl0700.getTwrt_Record_Twrt_Reason_Code()));                                //Natural: COMPRESS 'Reason Code :' TWRT-REASON-CODE INTO #ER-DESC ( 5 )
                pnd_Ws_Pnd_Er_Desc.getValue(6).setValue(DbsUtil.compress("RMD Ind     :", ldaTwrl0700.getTwrt_Record_Twrt_Rmd_Ind()));                                    //Natural: COMPRESS 'RMD Ind     :' TWRT-RMD-IND INTO #ER-DESC ( 6 )
                //*  W-2
                if (condition(pnd_Tiaa_Ignore.getBoolean()))                                                                                                              //Natural: IF #TIAA-IGNORE
                {
                    pnd_Ws_Pnd_Er_Desc.getValue(7).setValue(DbsUtil.compress("*** W2 Error, DO NOT Research  ***"));                                                      //Natural: COMPRESS '*** W2 Error, DO NOT Research  ***' INTO #ER-DESC ( 7 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Ws_Pnd_E.equals(11))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR #DISP-LOCALITY INTO TIRCNTL-ERROR-DESCR
                    pnd_Ws_Pnd_Disp_Locality));
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Ws_Pnd_E.equals(13))))
            {
                decideConditionsMet2953++;
                pnd_Ws_Pnd_Er_Desc.getValue(1).setValue(DbsUtil.compress("Residency Code:", ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy()));                             //Natural: COMPRESS 'Residency Code:' TWRT-STATE-RSDNCY INTO #ER-DESC ( 1 )
                pnd_Ws_Pnd_Er_Desc.getValue(2).setValue(DbsUtil.compress("Locality  Code:", ldaTwrl0700.getTwrt_Record_Twrt_Locality_Cde()));                             //Natural: COMPRESS 'Locality  Code:' TWRT-LOCALITY-CDE INTO #ER-DESC ( 2 )
                pnd_Ws_Pnd_Er_Desc.getValue(3).setValue(DbsUtil.compress("3 char Country:", ldaTwrl0700.getTwrt_Record_Twrt_Annt_Locality_Code()));                       //Natural: COMPRESS '3 char Country:' TWRT-ANNT-LOCALITY-CODE INTO #ER-DESC ( 3 )
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_Ws_Pnd_E.equals(15))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-DOB INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Dob()));
            }                                                                                                                                                             //Natural: VALUE 26
            else if (condition((pnd_Ws_Pnd_E.equals(26))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR #DISP-AMT INTO TIRCNTL-ERROR-DESCR
                    pnd_Ws_Pnd_Disp_Amt));
            }                                                                                                                                                             //Natural: VALUE 29
            else if (condition((pnd_Ws_Pnd_E.equals(29))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-TAX-YEAR-A INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A()));
            }                                                                                                                                                             //Natural: VALUE 78
            else if (condition((pnd_Ws_Pnd_E.equals(78))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-TAX-TYPE INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Tax_Type()));
            }                                                                                                                                                             //Natural: VALUE 79
            else if (condition((pnd_Ws_Pnd_E.equals(79))))
            {
                decideConditionsMet2953++;
                ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr().setValue(DbsUtil.compress(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),  //Natural: COMPRESS TIRCNTL-ERROR-DESCR TWRT-PLAN-TYPE INTO TIRCNTL-ERROR-DESCR
                    ldaTwrl0700.getTwrt_Record_Twrt_Plan_Type()));
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Type().equals("R")))                                                               //Natural: IF TIRCNTL-ERROR-TYPE = 'R'
            {
                pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(1);                                                                                    //Natural: ADD 1 TO #REJ-CNT ( #COMP-IDX )
                ldaTwrl0700.getTwrt_Record_Twrt_Flag().setValue("R");                                                                                                     //Natural: ASSIGN TWRT-FLAG := 'R'
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Cde().notEquals("K")))                                                                                  //Natural: IF TWRT-IVC-CDE NE 'K'
                {
                    pnd_Ws_Pnd_Rej_Gross_Unkn.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #REJ-GROSS-UNKN ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Roll.getBoolean()))                                                                                                              //Natural: IF #ROLL
                {
                    pnd_Ws_Pnd_Rej_Gross_Roll.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                               //Natural: ADD TWRT-GROSS-AMT TO #REJ-GROSS-ROLL ( #COMP-IDX )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt());                                        //Natural: ADD TWRT-GROSS-AMT TO #REJ-GROSS ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt());                                       //Natural: ADD TWRT-INTEREST-AMT TO #REJ-INT ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt());                                      //Natural: ADD TWRT-FED-WHHLD-AMT TO #REJ-FED ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt());                                      //Natural: ADD TWRT-NRA-WHHLD-AMT TO #REJ-NRA ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_Puri);                                                                       //Natural: ADD #PURI TO #REJ-PR ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt());                                            //Natural: ADD TWRT-IVC-AMT TO #REJ-IVC ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(pnd_Ws_Pnd_State_Amt);                                                               //Natural: ADD #STATE-AMT TO #REJ-STATE ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Local.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Local_Whhld_Amt());                                  //Natural: ADD TWRT-LOCAL-WHHLD-AMT TO #REJ-LOCAL ( #COMP-IDX )
                pnd_Ws_Pnd_Rej_Can.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).nadd(ldaTwrl0700.getTwrt_Record_Twrt_Can_Whhld_Amt());                                      //Natural: ADD TWRT-CAN-WHHLD-AMT TO #REJ-CAN ( #COMP-IDX )
                ldaTwrl0700.getTwrt_Record_Twrt_Rej_Err_Nbr().setValueEdited(pnd_Ws_Pnd_E,new ReportEditMask("99"));                                                      //Natural: MOVE EDITED #E ( EM = 99 ) TO TWRT-REJ-ERR-NBR
                //*  REJECTED RECORD
                getWorkFiles().write(3, false, ldaTwrl0700.getTwrt_Record());                                                                                             //Natural: WRITE WORK FILE 03 TWRT-RECORD
                if (condition(getReports().getAstLineCount(3).greater(58)))                                                                                               //Natural: IF *LINE-COUNT ( 03 ) > 58
                {
                    getReports().newPage(new ReportSpecification(3));                                                                                                     //Natural: NEWPAGE ( 03 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new           //Natural: WRITE ( 03 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #P-DATE TWRATIN.#O-TIN TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT 2X TWRT-IVC-AMT 2X TWRT-FED-WHHLD-AMT 1X #STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 3X TWRT-PLAN-NUM TWRT-ORIG-CONTRACT-NBR
                    ColumnSpacing(1),pnd_Ws_Pnd_P_Date, new ReportEditMask ("MM/DD/YYYY"),pdaTwratin.getTwratin_Pnd_O_Tin(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),new 
                    ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),new ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),new 
                    ColumnSpacing(1),pnd_Ws_Pnd_State_Amt,new ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new 
                    ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   WRITE (03) 101X #PURI 13X TWRT-LOB-CDE               /* RCC
                getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(77),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new ColumnSpacing(13),   //Natural: WRITE ( 03 ) 77X TWRT-NRA-WHHLD-AMT #PURI 13X TWRT-SUBPLAN TWRT-ORIG-SUBPLAN
                    ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  W-2  F.ALLIE   /* CHG 6 TO 7 (VR01)
                getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(33),"Error: ",ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),          //Natural: WRITE ( 03 ) 33X 'Error: ' TIRCNTL-ERROR-DESCR ' - Rejected'
                    " - Rejected");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR07:                                                                                                                                                    //Natural: FOR #IDX = 1 TO 7
                for (pnd_Ws_Pnd_Idx.setValue(1); condition(pnd_Ws_Pnd_Idx.lessOrEqual(7)); pnd_Ws_Pnd_Idx.nadd(1))
                {
                    if (condition(pnd_Ws_Pnd_Er_Desc.getValue(pnd_Ws_Pnd_Idx).notEquals(" ")))                                                                            //Natural: IF #ER-DESC ( #IDX ) NE ' '
                    {
                        getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(33),pnd_Ws_Pnd_Er_Desc.getValue(pnd_Ws_Pnd_Idx));                                    //Natural: WRITE ( 03 ) 33X #ER-DESC ( #IDX )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CITED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Er.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #ER
                pnd_Ws_Pnd_Errors.getValue(pnd_Ws_Pnd_Er).setValue(pnd_Ws_Pnd_E);                                                                                         //Natural: ASSIGN #ERRORS ( #ER ) := #E
                if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Flag().notEquals("R")))                                                                                     //Natural: IF TWRT-FLAG NE 'R'
                {
                    ldaTwrl0700.getTwrt_Record_Twrt_Flag().setValue("C");                                                                                                 //Natural: ASSIGN TWRT-FLAG := 'C'
                    if (condition(getReports().getAstLineCount(4).greater(58)))                                                                                           //Natural: IF *LINE-COUNT ( 04 ) > 58
                    {
                        getReports().newPage(new ReportSpecification(4));                                                                                                 //Natural: NEWPAGE ( 04 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(4, ReportOption.NOTITLE,NEWLINE,ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr(),ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde(),new       //Natural: WRITE ( 04 ) / TWRT-CNTRCT-NBR TWRT-PAYEE-CDE 1X #P-DATE TWRATIN.#O-TIN TWRT-TAX-ID-TYPE TWRT-GROSS-AMT TWRT-INTEREST-AMT 2X TWRT-IVC-AMT 2X TWRT-FED-WHHLD-AMT 1X #STATE-AMT 4X TWRT-STATE-RSDNCY TWRT-CITIZENSHIP 3X TWRT-PLAN-NUM TWRT-ORIG-CONTRACT-NBR
                        ColumnSpacing(1),pnd_Ws_Pnd_P_Date, new ReportEditMask ("MM/DD/YYYY"),pdaTwratin.getTwratin_Pnd_O_Tin(),ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id_Type(),ldaTwrl0700.getTwrt_Record_Twrt_Gross_Amt(),ldaTwrl0700.getTwrt_Record_Twrt_Interest_Amt(),new 
                        ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Ivc_Amt(),new ColumnSpacing(2),ldaTwrl0700.getTwrt_Record_Twrt_Fed_Whhld_Amt(),new 
                        ColumnSpacing(1),pnd_Ws_Pnd_State_Amt,new ColumnSpacing(4),ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),new 
                        ColumnSpacing(3),ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(4, ReportOption.NOTITLE,new ColumnSpacing(77),ldaTwrl0700.getTwrt_Record_Twrt_Nra_Whhld_Amt(),pnd_Ws_Pnd_Puri,new                  //Natural: WRITE ( 04 ) 77X TWRT-NRA-WHHLD-AMT #PURI 13X TWRT-SUBPLAN TWRT-ORIG-SUBPLAN
                        ColumnSpacing(13),ldaTwrl0700.getTwrt_Record_Twrt_Subplan(),ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(4, ReportOption.NOTITLE,new ColumnSpacing(33),"Error:",ldaTwrl9807.getTircntl_Error_Msg_Tbl_View_View_Tircntl_Error_Descr(),           //Natural: WRITE ( 04 ) 33X 'Error:' TIRCNTL-ERROR-DESCR '- Cited'
                    "- Cited");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR08:                                                                                                                                                    //Natural: FOR #IDX = 1 TO 4
                for (pnd_Ws_Pnd_Idx.setValue(1); condition(pnd_Ws_Pnd_Idx.lessOrEqual(4)); pnd_Ws_Pnd_Idx.nadd(1))
                {
                    if (condition(pnd_Ws_Pnd_Er_Desc.getValue(pnd_Ws_Pnd_Idx).notEquals(" ")))                                                                            //Natural: IF #ER-DESC ( #IDX ) NE ' '
                    {
                        getReports().write(4, ReportOption.NOTITLE,new ColumnSpacing(33),pnd_Ws_Pnd_Er_Desc.getValue(pnd_Ws_Pnd_Idx));                                    //Natural: WRITE ( 04 ) 33X #ER-DESC ( #IDX )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    //*  LIFE
    //*  TCII
    //*  TIAA
    //*  TRUST
    //*  COMPANY F          /* RCC
    private void sub_Set_Totals() throws Exception                                                                                                                        //Natural: SET-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Inp_Cnt.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt());                                                                          //Natural: ASSIGN #INP-CNT ( 1 ) := #L-REC-CNT
        pnd_Ws_Pnd_Inp_Gross.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A());                                                                        //Natural: ASSIGN #INP-GROSS ( 1 ) := #L-GROSS-A
        pnd_Ws_Pnd_Inp_Ivc.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A());                                                                            //Natural: ASSIGN #INP-IVC ( 1 ) := #L-IVC-A
        pnd_Ws_Pnd_Inp_Int.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A());                                                                            //Natural: ASSIGN #INP-INT ( 1 ) := #L-INT-A
        pnd_Ws_Pnd_Inp_Fed.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A());                                                                            //Natural: ASSIGN #INP-FED ( 1 ) := #L-FED-A
        pnd_Ws_Pnd_Inp_Nra.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A());                                                                            //Natural: ASSIGN #INP-NRA ( 1 ) := #L-NRA-A
        pnd_Ws_Pnd_Inp_Can.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A());                                                                       //Natural: ASSIGN #INP-CAN ( 1 ) := #L-CANADIAN-A
        pnd_Ws_Pnd_Inp_State.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A());                                                                        //Natural: ASSIGN #INP-STATE ( 1 ) := #L-STATE-A
        pnd_Ws_Pnd_Inp_Local.getValue(1).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A());                                                                        //Natural: ASSIGN #INP-LOCAL ( 1 ) := #L-LOCAL-A
        pnd_Ws_Pnd_Inp_Cnt.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt());                                                                          //Natural: ASSIGN #INP-CNT ( 2 ) := #S-REC-CNT
        pnd_Ws_Pnd_Inp_Gross.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A());                                                                        //Natural: ASSIGN #INP-GROSS ( 2 ) := #S-GROSS-A
        pnd_Ws_Pnd_Inp_Ivc.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A());                                                                            //Natural: ASSIGN #INP-IVC ( 2 ) := #S-IVC-A
        pnd_Ws_Pnd_Inp_Int.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A());                                                                            //Natural: ASSIGN #INP-INT ( 2 ) := #S-INT-A
        pnd_Ws_Pnd_Inp_Fed.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A());                                                                            //Natural: ASSIGN #INP-FED ( 2 ) := #S-FED-A
        pnd_Ws_Pnd_Inp_Nra.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A());                                                                            //Natural: ASSIGN #INP-NRA ( 2 ) := #S-NRA-A
        pnd_Ws_Pnd_Inp_Can.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A());                                                                       //Natural: ASSIGN #INP-CAN ( 2 ) := #S-CANADIAN-A
        pnd_Ws_Pnd_Inp_State.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A());                                                                        //Natural: ASSIGN #INP-STATE ( 2 ) := #S-STATE-A
        pnd_Ws_Pnd_Inp_Local.getValue(2).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A());                                                                        //Natural: ASSIGN #INP-LOCAL ( 2 ) := #S-LOCAL-A
        pnd_Ws_Pnd_Inp_Cnt.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt());                                                                          //Natural: ASSIGN #INP-CNT ( 3 ) := #T-REC-CNT
        pnd_Ws_Pnd_Inp_Gross.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A());                                                                        //Natural: ASSIGN #INP-GROSS ( 3 ) := #T-GROSS-A
        pnd_Ws_Pnd_Inp_Ivc.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A());                                                                            //Natural: ASSIGN #INP-IVC ( 3 ) := #T-IVC-A
        pnd_Ws_Pnd_Inp_Int.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A());                                                                            //Natural: ASSIGN #INP-INT ( 3 ) := #T-INT-A
        pnd_Ws_Pnd_Inp_Fed.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A());                                                                            //Natural: ASSIGN #INP-FED ( 3 ) := #T-FED-A
        pnd_Ws_Pnd_Inp_Nra.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A());                                                                            //Natural: ASSIGN #INP-NRA ( 3 ) := #T-NRA-A
        pnd_Ws_Pnd_Inp_Can.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A());                                                                       //Natural: ASSIGN #INP-CAN ( 3 ) := #T-CANADIAN-A
        pnd_Ws_Pnd_Inp_State.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A());                                                                        //Natural: ASSIGN #INP-STATE ( 3 ) := #T-STATE-A
        pnd_Ws_Pnd_Inp_Local.getValue(3).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A());                                                                        //Natural: ASSIGN #INP-LOCAL ( 3 ) := #T-LOCAL-A
        pnd_Ws_Pnd_Inp_Cnt.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt());                                                                          //Natural: ASSIGN #INP-CNT ( 4 ) := #C-REC-CNT
        pnd_Ws_Pnd_Inp_Gross.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A());                                                                        //Natural: ASSIGN #INP-GROSS ( 4 ) := #C-GROSS-A
        pnd_Ws_Pnd_Inp_Ivc.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A());                                                                            //Natural: ASSIGN #INP-IVC ( 4 ) := #C-IVC-A
        pnd_Ws_Pnd_Inp_Int.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A());                                                                            //Natural: ASSIGN #INP-INT ( 4 ) := #C-INT-A
        pnd_Ws_Pnd_Inp_Fed.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A());                                                                            //Natural: ASSIGN #INP-FED ( 4 ) := #C-FED-A
        pnd_Ws_Pnd_Inp_Nra.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A());                                                                            //Natural: ASSIGN #INP-NRA ( 4 ) := #C-NRA-A
        pnd_Ws_Pnd_Inp_Can.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A());                                                                       //Natural: ASSIGN #INP-CAN ( 4 ) := #C-CANADIAN-A
        pnd_Ws_Pnd_Inp_State.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A());                                                                        //Natural: ASSIGN #INP-STATE ( 4 ) := #C-STATE-A
        pnd_Ws_Pnd_Inp_Local.getValue(4).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A());                                                                        //Natural: ASSIGN #INP-LOCAL ( 4 ) := #C-LOCAL-A
        pnd_Ws_Pnd_Inp_Cnt.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt());                                                                          //Natural: ASSIGN #INP-CNT ( 5 ) := #F-REC-CNT
        pnd_Ws_Pnd_Inp_Gross.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A());                                                                        //Natural: ASSIGN #INP-GROSS ( 5 ) := #F-GROSS-A
        pnd_Ws_Pnd_Inp_Ivc.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A());                                                                            //Natural: ASSIGN #INP-IVC ( 5 ) := #F-IVC-A
        pnd_Ws_Pnd_Inp_Int.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A());                                                                            //Natural: ASSIGN #INP-INT ( 5 ) := #F-INT-A
        pnd_Ws_Pnd_Inp_Fed.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A());                                                                            //Natural: ASSIGN #INP-FED ( 5 ) := #F-FED-A
        pnd_Ws_Pnd_Inp_Nra.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A());                                                                            //Natural: ASSIGN #INP-NRA ( 5 ) := #F-NRA-A
        pnd_Ws_Pnd_Inp_Can.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A());                                                                       //Natural: ASSIGN #INP-CAN ( 5 ) := #F-CANADIAN-A
        pnd_Ws_Pnd_Inp_State.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A());                                                                        //Natural: ASSIGN #INP-STATE ( 5 ) := #F-STATE-A
        pnd_Ws_Pnd_Inp_Local.getValue(5).setValue(ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A());                                                                        //Natural: ASSIGN #INP-LOCAL ( 5 ) := #F-LOCAL-A
    }
    //*  TABLE 1
    private void sub_Load_Dist_Code_Table() throws Exception                                                                                                              //Natural: LOAD-DIST-CODE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Di_T_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                          //Natural: ASSIGN #DI-T-YR := TWRT-TAX-YEAR
        ldaTwrl820d.getVw_dist_Tbl().startDatabaseRead                                                                                                                    //Natural: READ DIST-TBL BY TIRCNTL-NBR-YEAR-DISTR-CODE = #DIST-SU
        (
        "READ02",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Ws_Pnd_Dist_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") }
        );
        READ02:
        while (condition(ldaTwrl820d.getVw_dist_Tbl().readNextRow("READ02")))
        {
            if (condition(ldaTwrl820d.getDist_Tbl_Tircntl_Tbl_Nbr().equals(1) && ldaTwrl820d.getDist_Tbl_Tircntl_Tax_Year().equals(pnd_Ws_Pnd_Di_T_Yr)))                  //Natural: IF TIRCNTL-TBL-NBR = 1 AND TIRCNTL-TAX-YEAR = #DI-T-YR
            {
                pnd_Ws_Pnd_T_Dist_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-DIST-CNT
                pnd_Ws_Pnd_T_Payment_Type.getValue(pnd_Ws_Pnd_T_Dist_Cnt).setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Type());                                         //Natural: ASSIGN #T-PAYMENT-TYPE ( #T-DIST-CNT ) := TIRCNTL-PAYMT-TYPE
                pnd_Ws_Pnd_T_Settle_Type.getValue(pnd_Ws_Pnd_T_Dist_Cnt).setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Settl_Type());                                          //Natural: ASSIGN #T-SETTLE-TYPE ( #T-DIST-CNT ) := TIRCNTL-SETTL-TYPE
                pnd_Ws_Pnd_T_Dist_Code_1.getValue(pnd_Ws_Pnd_T_Dist_Cnt).setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Dist_Code());                                           //Natural: ASSIGN #T-DIST-CODE-1 ( #T-DIST-CNT ) := TIRCNTL-DIST-CODE
                pnd_Ws_Pnd_T_Refer_Num.getValue(pnd_Ws_Pnd_T_Dist_Cnt).setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Type_Refer());                                            //Natural: ASSIGN #T-REFER-NUM ( #T-DIST-CNT ) := TIRCNTL-TYPE-REFER
                if (condition(pdaTwradist.getPnd_Twradist_Pnd_In_Dist().notEquals(ldaTwrl820d.getDist_Tbl_Tircntl_Dist_Code())))                                          //Natural: IF #TWRADIST.#IN-DIST NE TIRCNTL-DIST-CODE
                {
                    pdaTwradist.getPnd_Twradist_Pnd_In_Dist().setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Dist_Code());                                                      //Natural: ASSIGN #TWRADIST.#IN-DIST := TIRCNTL-DIST-CODE
                    pdaTwradist.getPnd_Twradist_Pnd_Tax_Year().setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                      //Natural: ASSIGN #TWRADIST.#TAX-YEAR := TWRT-TAX-YEAR
                    pdaTwradist.getPnd_Twradist_Pnd_Function().setValue("1");                                                                                             //Natural: ASSIGN #TWRADIST.#FUNCTION := '1'
                                                                                                                                                                          //Natural: PERFORM ACTUAL-DIST-ACCESS
                    sub_Actual_Dist_Access();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_T_Dist_Code_2.getValue(pnd_Ws_Pnd_T_Dist_Cnt).setValue(pdaTwradist.getPnd_Twradist_Pnd_Out_Dist());                                            //Natural: ASSIGN #T-DIST-CODE-2 ( #T-DIST-CNT ) := #TWRADIST.#OUT-DIST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_T_Dist_Cnt.equals(getZero())))                                                                                                           //Natural: IF #T-DIST-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Distribution Table For Tax Year:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"Is Missing",new  //Natural: WRITE ( 0 ) '***' 25T 'Distribution Table For Tax Year:' TWRT-TAX-YEAR 'Is Missing' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(108);  if (true) return;                                                                                                                    //Natural: TERMINATE 108
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TABLE 3
    private void sub_Load_Country_Table() throws Exception                                                                                                                //Natural: LOAD-COUNTRY-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Cn_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                            //Natural: ASSIGN #CN-YR := TWRT-TAX-YEAR
        ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-COUNTRY-CODE-TBL-VIEW-VI BY TIRCNTL-NBR-YEAR-ALPHA-CDE = #CN-SU
        (
        "READ03",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_ALPHA_CDE", ">=", pnd_Ws_Pnd_Cn_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_ALPHA_CDE", "ASC") }
        );
        READ03:
        while (condition(ldaTwrl9802.getVw_tircntl_Country_Code_Tbl_View_Vi().readNextRow("READ03")))
        {
            if (condition(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tbl_Nbr().equals(3) && ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Tax_Year().equals(pnd_Ws_Pnd_St_Yr))) //Natural: IF TIRCNTL-TBL-NBR = 3 AND TIRCNTL-TAX-YEAR = #ST-YR
            {
                pnd_Ws_Pnd_T_Country_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #T-COUNTRY-CNT
                pnd_Ws_Pnd_T_Country_Code2.getValue(pnd_Ws_Pnd_T_Country_Cnt).setValue(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Alpha_Code());       //Natural: ASSIGN #T-COUNTRY-CODE2 ( #T-COUNTRY-CNT ) := TIRCNTL-CNTRY-ALPHA-CODE
                pnd_Ws_Pnd_T_Country_Code3.getValue(pnd_Ws_Pnd_T_Country_Cnt).setValue(ldaTwrl9802.getTircntl_Country_Code_Tbl_View_Vi_Tircntl_Cntry_Unique_Cde());       //Natural: ASSIGN #T-COUNTRY-CODE3 ( #T-COUNTRY-CNT ) := TIRCNTL-CNTRY-UNIQUE-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_T_Country_Cnt.equals(getZero())))                                                                                                        //Natural: IF #T-COUNTRY-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Country Code Table For Tax Year:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"Is Missing",new  //Natural: WRITE ( 0 ) '***' 25T 'Country Code Table For Tax Year:' TWRT-TAX-YEAR 'Is Missing' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(110);  if (true) return;                                                                                                                    //Natural: TERMINATE 110
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TABLE 2
    private void sub_Load_State_Table() throws Exception                                                                                                                  //Natural: LOAD-STATE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_St_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                            //Natural: ASSIGN #ST-YR := TWRT-TAX-YEAR
        ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-STATE-CODE-TBL-VIEW-VIEW BY TIRCNTL-NBR-YEAR-OLD-STATE-CDE = #ST-SU
        (
        "READ04",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", ">=", pnd_Ws_Pnd_St_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_OLD_STATE_CDE", "ASC") }
        );
        READ04:
        while (condition(ldaTwrl9806.getVw_tircntl_State_Code_Tbl_View_View().readNextRow("READ04")))
        {
            if (condition(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Tbl_Nbr().equals(2) && ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_Tax_Year().equals(pnd_Ws_Pnd_St_Yr))) //Natural: IF TIRCNTL-TBL-NBR = 2 AND TIRCNTL-TAX-YEAR = #ST-YR
            {
                pnd_Ws_Pnd_T_State_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #T-STATE-CNT
                pnd_Ws_Pnd_T_State_Code.getValue(pnd_Ws_Pnd_T_State_Cnt).setValue(ldaTwrl9806.getTircntl_State_Code_Tbl_View_View_Tircntl_State_Old_Code().getSubstring(2, //Natural: MOVE SUBSTR ( TIRCNTL-STATE-OLD-CODE,2,2 ) TO #T-STATE-CODE ( #T-STATE-CNT )
                    2));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_T_State_Cnt.equals(getZero())))                                                                                                          //Natural: IF #T-STATE-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"State Code Table For Tax Year:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"Is Missing",new  //Natural: WRITE ( 0 ) '***' 25T 'State Code Table For Tax Year:' TWRT-TAX-YEAR 'Is Missing' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(109);  if (true) return;                                                                                                                    //Natural: TERMINATE 109
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TABLE 5 - FEEDER SYSTEM TABLE
    private void sub_Load_Source_Table() throws Exception                                                                                                                 //Natural: LOAD-SOURCE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_So_Yr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                                            //Natural: ASSIGN #SO-YR := TWRT-TAX-YEAR
        ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().startDatabaseRead                                                                                            //Natural: READ TIRCNTL-FEEDER-SYS-TBL-VIEW-VIEW BY TIRCNTL-5-Y-SC-CO-TO-FRM-SP = #SO-SU
        (
        "READ05",
        new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Ws_Pnd_So_Su, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") }
        );
        READ05:
        while (condition(ldaTwrl9804.getVw_tircntl_Feeder_Sys_Tbl_View_View().readNextRow("READ05")))
        {
            if (condition(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tbl_Nbr().equals(5) && ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Tax_Year().equals(pnd_Ws_Pnd_So_Yr))) //Natural: IF TIRCNTL-TBL-NBR = 5 AND TIRCNTL-TAX-YEAR = #SO-YR
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Company_Cde().equals(" "))))                                                          //Natural: ACCEPT IF TIRCNTL-COMPANY-CDE = ' '
            {
                continue;
            }
            pnd_Ws_Pnd_T_Source_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #T-SOURCE-CNT
            pnd_Ws_Pnd_T_Source_Code.getValue(pnd_Ws_Pnd_T_Source_Cnt).setValue(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code());               //Natural: ASSIGN #T-SOURCE-CODE ( #T-SOURCE-CNT ) := TIRCNTL-RPT-SOURCE-CODE
            pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_T_Source_Cnt).setValue(ldaTwrl9804.getTircntl_Feeder_Sys_Tbl_View_View_Tircntl_Rpt_Source_Code_Desc());          //Natural: ASSIGN #T-SOURCE-NAME ( #T-SOURCE-CNT ) := TIRCNTL-RPT-SOURCE-CODE-DESC
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_T_Source_Cnt.equals(getZero())))                                                                                                         //Natural: IF #T-SOURCE-CNT = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Source Code Table For Tax Year:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year(),"Is Missing",new  //Natural: WRITE ( 0 ) '***' 25T 'Source Code Table For Tax Year:' TWRT-TAX-YEAR 'Is Missing' 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(107);  if (true) return;                                                                                                                    //Natural: TERMINATE 107
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Company() throws Exception                                                                                                                      //Natural: LOAD-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        FOR09:                                                                                                                                                            //Natural: FOR #COMP-IDX = 1 TO COMP-MAX
        for (pnd_Ws_Pnd_Comp_Idx.setValue(1); condition(pnd_Ws_Pnd_Comp_Idx.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_Comp_Idx.nadd(1))
        {
            //*  RCC
            //*  AHMEDMA
            //*  AHMEDMA
            short decideConditionsMet3226 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #COMP-IDX;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Comp_Idx.equals(1))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("L");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'L'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(2))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("S");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'S'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(3))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("T");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'T'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(4))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("X");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'X'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(5))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("F");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'F'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Ws_Pnd_Comp_Idx.equals(6))))
            {
                decideConditionsMet3226++;
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue("C");                                                                                                //Natural: ASSIGN #TWRACOMP.#COMP-CODE := 'C'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet3226 > 0))
            {
                DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"),                    //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
                    pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(), new AttributeParameter("M"));
                if (condition(Global.isEscape())) return;
                if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Ret_Code().getBoolean()))                                                                                   //Natural: IF #TWRACOMP.#RET-CODE
                {
                    pnd_Ws_Pnd_Comp_Name.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).setValue(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name());                          //Natural: ASSIGN #WS.#COMP-NAME ( #COMP-IDX ) := #TWRACOMP.#COMP-SHORT-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                          //Natural: RESET #COMP-IDX
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Company() throws Exception                                                                                                                       //Natural: GET-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  RCC
        //*  AHMEDMA
        //*  AHMEDMA
        short decideConditionsMet3257 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF TWRT-COMPANY-CDE;//Natural: VALUE 'L'
        if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("L"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(1);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("S"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(2);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("T"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(3);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 3
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("X"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(4);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 4
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("F"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(5);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 5
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().equals("C"))))
        {
            decideConditionsMet3257++;
            pnd_Ws_Pnd_Comp_Idx.setValue(6);                                                                                                                              //Natural: ASSIGN #COMP-IDX := 6
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Comp_Idx.reset();                                                                                                                                  //Natural: RESET #COMP-IDX
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Comp_Name_Disp.setValue(pnd_Ws_Pnd_Comp_Name.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));                                                              //Natural: ASSIGN #COMP-NAME-DISP := #WS.#COMP-NAME ( #COMP-IDX )
    }
    private void sub_Get_Control_Record() throws Exception                                                                                                                //Natural: GET-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getWorkFiles().read(8, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 08 ONCE RECORD #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing Control Record",new TabSetting(77),"***");                                       //Natural: WRITE ( 0 ) '***' 25T 'Missing Control Record' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(101);  if (true) return;                                                                                                                    //Natural: TERMINATE 101
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  09/28/11
        //*  RC06
        if (condition(! (ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NV") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("OP")  //Natural: IF NOT #TWRP0600-ORIGIN-CODE = 'NV' OR = 'OP' OR = 'VL' OR = 'AM' OR = 'EW'
            || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AM") 
            || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("EW"))))
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Control Record - Invalid Source Code:",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new  //Natural: WRITE ( 0 ) '***' 25T 'Control Record - Invalid Source Code:' #TWRP0600-ORIGIN-CODE 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(102);  if (true) return;                                                                                                                    //Natural: TERMINATE 102
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_P_Year.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                                                              //Natural: ASSIGN #P-YEAR := #TWRP0600-TAX-YEAR-CCYY
        pnd_Ws_Pnd_P_Year_N.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #P-YEAR-N
        pnd_Ws_Pnd_Last_Bus_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());             //Natural: MOVE EDITED #TWRP0600-INTERFACE-CCYYMMDD TO #LAST-BUS-DATE ( EM = YYYYMMDD )
        pnd_Ws_Pnd_Last_Bus_Day.setValueEdited(pnd_Ws_Pnd_Last_Bus_Date,new ReportEditMask("O"));                                                                         //Natural: MOVE EDITED #LAST-BUS-DATE ( EM = O ) TO #LAST-BUS-DAY
        //*  FRI
        if (condition(((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals("1229") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals("1230"))  //Natural: IF SUBSTR ( #TWRP0600-INTERFACE-CCYYMMDD,5,4 ) = '1229' OR = '1230' AND #LAST-BUS-DAY = '6'
            && pnd_Ws_Pnd_Last_Bus_Day.equals("6"))))
        {
            pnd_Ws_Pnd_Last_Bus_Compare.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4));                         //Natural: MOVE SUBSTR ( #TWRP0600-INTERFACE-CCYYMMDD,5,4 ) TO #LAST-BUS-COMPARE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Last_Bus_Compare.setValue("1231");                                                                                                                 //Natural: ASSIGN #LAST-BUS-COMPARE := '1231'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A().equals(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy()) ||                     //Natural: IF TWRT-TAX-YEAR-A = #TWRP0600-TAX-YEAR-CCYY OR SUBSTR ( #TWRP0600-INTERFACE-CCYYMMDD,5,4 ) = #LAST-BUS-COMPARE AND TWRT-TAX-YEAR-A = #P-YEAR
            ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().getSubstring(5,4).equals(pnd_Ws_Pnd_Last_Bus_Compare) && ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A().equals(pnd_Ws_Pnd_P_Year)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Invalid Tax Year",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Control Record:",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),new  //Natural: WRITE ( 0 ) '***' 25T 'Invalid Tax Year' 77T '***' / '***' 25T 'Control Record:' #TWRP0600-TAX-YEAR-CCYY 77T '***' / '***' 25T 'Input   Record:' TWRT-TAX-YEAR-A 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Input   Record:",ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A(),new TabSetting(77),
                "***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(103);  if (true) return;                                                                                                                    //Natural: TERMINATE 103
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_P_Year.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year_A());                                                                                         //Natural: ASSIGN #P-YEAR := TWRT-TAX-YEAR-A
    }
    private void sub_Read_Summary_Record() throws Exception                                                                                                               //Natural: READ-SUMMARY-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getWorkFiles().read(5, ldaTwrl0710.getPnd_Inp_Summary());                                                                                                         //Natural: READ WORK FILE 05 ONCE RECORD #INP-SUMMARY
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Missing Summary Record",new TabSetting(77),"***");                                       //Natural: WRITE ( 0 ) '***' 25T 'Missing Summary Record' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(104);  if (true) return;                                                                                                                    //Natural: TERMINATE 104
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code().notEquals(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code())))                 //Natural: IF #INP-SUMMARY.#SOURCE-CODE NE #TWRP0600-ORIGIN-CODE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Control Record - Invalid Source Code:",ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code(),new  //Natural: WRITE ( 0 ) '***' 25T 'Control Record - Invalid Source Code:' #INP-SUMMARY.#SOURCE-CODE 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(105);  if (true) return;                                                                                                                    //Natural: TERMINATE 105
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_T_Source_Code.getValue(1,":",pnd_Ws_Pnd_T_Source_Cnt),true), new ExamineSearch(ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code(),  //Natural: EXAMINE FULL #T-SOURCE-CODE ( 1:#T-SOURCE-CNT ) FOR FULL #INP-SUMMARY.#SOURCE-CODE GIVING INDEX IN #WS.#SOURCE-IDX
            true), new ExamineGivingIndex(pnd_Ws_Pnd_Source_Idx));
        if (condition(pnd_Ws_Pnd_Source_Idx.equals(getZero())))                                                                                                           //Natural: IF #WS.#SOURCE-IDX = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Invalid Source Code",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Summary Record:",ldaTwrl0710.getPnd_Inp_Summary_Pnd_Source_Code(),new  //Natural: WRITE ( 0 ) '***' 25T 'Invalid Source Code' 77T '***' / '***' 25T 'Summary Record:' #INP-SUMMARY.#SOURCE-CODE 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(112);  if (true) return;                                                                                                                    //Natural: TERMINATE 112
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SET-TOTALS
        sub_Set_Totals();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Lookup_Residency_Type() throws Exception                                                                                                             //Natural: LOOKUP-RESIDENCY-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrpymnt_Residency_Type.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := ' '
        pnd_Us_Resident.setValue(false);                                                                                                                                  //Natural: ASSIGN #US-RESIDENT := FALSE
        //*  CANADA
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("CA")))                                                                                       //Natural: IF TWRT-STATE-RSDNCY = 'CA'
        {
            pnd_Twrpymnt_Residency_Type.setValue("4");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '4'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMONWEALTH OF PUERTO RICO
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("42") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("PR") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("RQ"))) //Natural: IF TWRT-STATE-RSDNCY = '42' OR = 'PR' OR = 'RQ'
        {
            pnd_Twrpymnt_Residency_Type.setValue("3");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '3'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PROVINCE OF CANADA
        if (condition(((ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("74") && ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("88"))          //Natural: IF TWRT-STATE-RSDNCY = '74' THRU '88' OR TWRT-STATE-RSDNCY = '96'
            || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("96"))))
        {
            pnd_Twrpymnt_Residency_Type.setValue("5");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '5'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  STATE
        if (condition((((ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("00") && ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("41"))         //Natural: IF TWRT-STATE-RSDNCY = '00' THRU '41' OR TWRT-STATE-RSDNCY = '43' THRU '57' OR TWRT-STATE-RSDNCY = '97' OR = 'US'
            || (ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().greaterOrEqual("43") && ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().lessOrEqual("57"))) 
            || (ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("97") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("US")))))
        {
            pnd_Twrpymnt_Residency_Type.setValue("1");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '1'
            pnd_Us_Resident.setValue(true);                                                                                                                               //Natural: ASSIGN #US-RESIDENT := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  COUNTRY
        if (condition(DbsUtil.maskMatches(ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy(),"AA") || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("98")             //Natural: IF TWRT-STATE-RSDNCY = MASK ( AA ) OR = '98' OR = '99'
            || ldaTwrl0700.getTwrt_Record_Twrt_State_Rsdncy().equals("99")))
        {
            pnd_Twrpymnt_Residency_Type.setValue("2");                                                                                                                    //Natural: ASSIGN #TWRPYMNT-RESIDENCY-TYPE := '2'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Citizenship_Code() throws Exception                                                                                                           //Natural: LOOKUP-CITIZENSHIP-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Citizenship().equals(1) || ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078().equals("Y")))                      //Natural: IF TWRT-CITIZENSHIP = 01 OR TWRT-PYMNT-TAX-FORM-1078 = 'Y'
        {
            pnd_Twrpymnt_Tax_Citizenship.setValue("U");                                                                                                                   //Natural: ASSIGN #TWRPYMNT-TAX-CITIZENSHIP := 'U'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrpymnt_Tax_Citizenship.setValue("F");                                                                                                                   //Natural: ASSIGN #TWRPYMNT-TAX-CITIZENSHIP := 'F'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Lookup_Dist_Code_Table() throws Exception                                                                                                            //Natural: LOOKUP-DIST-CODE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        if (condition(ldaTwrl0700.getTwrt_Record_Twrt_Type_Refer().equals(" ")))                                                                                          //Natural: IF TWRT-TYPE-REFER = ' '
        {
            pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue("  ");                                                                                                                  //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := '  '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Type_Refer());                                                                          //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := TWRT-TYPE-REFER
            //*   1/P, 2/R, OR 3/L
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Twrpymnt_Paymt_Category.setValue(" ");                                                                                                                        //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := ' '
        pnd_Twrpymnt_Dist_Method.setValue(" ");                                                                                                                           //Natural: ASSIGN #TWRPYMNT-DIST-METHOD := ' '
        pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr.setValue(1);                                                                                                                    //Natural: ASSIGN #S-TBL-NBR := 1
        pnd_Super_Dist_Code_Pnd_S_Tax_Year.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                                          //Natural: ASSIGN #S-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Super_Dist_Code_Pnd_S_Settl_Type.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());                                                                      //Natural: ASSIGN #S-SETTL-TYPE := TWRT-SETTL-TYPE
        pnd_Super_Dist_Code_Pnd_S_Pay_Type.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type());                                                                        //Natural: ASSIGN #S-PAY-TYPE := TWRT-PAYMT-TYPE
        pnd_Super_Dist_Code_Pnd_S_Dist_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #S-DIST-CODE := ' '
        ldaTwrl820d.getVw_dist_Tbl().startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) DIST-TBL WITH TIRCNTL-NBR-YEAR-DISTR-CODE = #SUPER-DIST-CODE
        (
        "READ06",
        new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Super_Dist_Code, WcType.BY) },
        new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") },
        1
        );
        READ06:
        while (condition(ldaTwrl820d.getVw_dist_Tbl().readNextRow("READ06")))
        {
            if (condition(pnd_Super_Dist_Code_Pnd_S_Tbl_Nbr.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Tbl_Nbr()) && pnd_Super_Dist_Code_Pnd_S_Tax_Year.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Tax_Year())  //Natural: IF #S-TBL-NBR = DIST-TBL.TIRCNTL-TBL-NBR AND #S-TAX-YEAR = DIST-TBL.TIRCNTL-TAX-YEAR AND #S-SETTL-TYPE = DIST-TBL.TIRCNTL-SETTL-TYPE AND #S-PAY-TYPE = DIST-TBL.TIRCNTL-PAYMT-TYPE
                && pnd_Super_Dist_Code_Pnd_S_Settl_Type.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Settl_Type()) && pnd_Super_Dist_Code_Pnd_S_Pay_Type.equals(ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Type())))
            {
                if (condition(pnd_Twrpymnt_Pymnt_Refer_Nbr.equals("  ")))                                                                                                 //Natural: IF #TWRPYMNT-PYMNT-REFER-NBR = '  '
                {
                    pnd_Twrpymnt_Pymnt_Refer_Nbr.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Type_Refer());                                                                  //Natural: ASSIGN #TWRPYMNT-PYMNT-REFER-NBR := DIST-TBL.TIRCNTL-TYPE-REFER
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet3402 = 0;                                                                                                                        //Natural: DECIDE ON FIRST DIST-TBL.TIRCNTL-PAYMT-CATEGORY;//Natural: VALUE 'P'
                if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("P"))))
                {
                    decideConditionsMet3402++;
                    pnd_Twrpymnt_Paymt_Category.setValue("1");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '1'
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("R"))))
                {
                    decideConditionsMet3402++;
                    pnd_Twrpymnt_Paymt_Category.setValue("2");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '2'
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category().equals("L"))))
                {
                    decideConditionsMet3402++;
                    pnd_Twrpymnt_Paymt_Category.setValue("3");                                                                                                            //Natural: ASSIGN #TWRPYMNT-PAYMT-CATEGORY := '3'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Search_Cat_Dist_Pnd_Search_Cat.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Paymt_Category());                                                            //Natural: ASSIGN #SEARCH-CAT := DIST-TBL.TIRCNTL-PAYMT-CATEGORY
                pnd_Search_Cat_Dist_Pnd_Search_Dist.setValue(ldaTwrl820d.getDist_Tbl_Tircntl_Dist_Method());                                                              //Natural: ASSIGN #SEARCH-DIST := DIST-TBL.TIRCNTL-DIST-METHOD
                FOR10:                                                                                                                                                    //Natural: FOR I = 1 TO 6
                for (i.setValue(1); condition(i.lessOrEqual(6)); i.nadd(1))
                {
                    if (condition(pnd_Search_Cat_Dist.equals(pnd_Cat_Dist_Table_Pnd_Cat_Dist_Method.getValue(i))))                                                        //Natural: IF #SEARCH-CAT-DIST = #CAT-DIST-METHOD ( I )
                    {
                        pnd_Twrpymnt_Dist_Method.setValue(pnd_Cat_Dist_Table_Pnd_Dist_Method_Code.getValue(i));                                                           //Natural: ASSIGN #TWRPYMNT-DIST-METHOD := #DIST-METHOD-CODE ( I )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Edit_Run_Date() throws Exception                                                                                                                     //Natural: EDIT-RUN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cycle_Processed.setValue(false);                                                                                                                              //Natural: ASSIGN #CYCLE-PROCESSED := FALSE
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tbl_5.setValue("5");                                                                                                     //Natural: ASSIGN #CNTL-TBL-5 := '5'
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Tax_Year.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy());                              //Natural: ASSIGN #CNTL-TAX-YEAR := #TWRP0600-TAX-YEAR-CCYY
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Source.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code());                                  //Natural: ASSIGN #CNTL-SOURCE := #TWRP0600-ORIGIN-CODE
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Company.setValue(" ");                                                                                                   //Natural: ASSIGN #CNTL-COMPANY := ' '
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_To_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                 //Natural: ASSIGN #CNTL-TO-DATE := #TWRP0600-TO-CCYYMMDD
        pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp_Pnd_Cntl_Interface_Date.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd());                   //Natural: ASSIGN #CNTL-INTERFACE-DATE := #TWRP0600-INTERFACE-CCYYMMDD
        //*  IF #DEBUG
        //*    WRITE (0) //
        //*      '=' #CNTL-TBL-5 /
        //*      '=' #CNTL-TAX-YEAR /
        //*      '=' #CNTL-SOURCE /
        //*      '=' #CNTL-COMPANY /
        //*      '=' #CNTL-TO-DATE /
        //*      '=' #CNTL-INTERFACE-DATE //
        //*      '=' TIRCNTL-5-Y-SC-CO-TO-FRM-SP
        //*  END-IF
        vw_cntl.createHistogram                                                                                                                                           //Natural: HISTOGRAM ( 1 ) CNTL TIRCNTL-5-Y-SC-CO-TO-FRM-SP #TIRCNTL-5-Y-SC-CO-TO-FRM-SP
        (
        "HIST01",
        "TIRCNTL_5_Y_SC_CO_TO_FRM_SP",
        new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp, WcType.WITH) },
        1
        );
        HIST01:
        while (condition(vw_cntl.readNextRow("HIST01")))
        {
            if (condition(cntl_Tircntl_5_Y_Sc_Co_To_Frm_Sp.equals(pnd_Tircntl_5_Y_Sc_Co_To_Frm_Sp)))                                                                      //Natural: IF TIRCNTL-5-Y-SC-CO-TO-FRM-SP = #TIRCNTL-5-Y-SC-CO-TO-FRM-SP
            {
                pnd_Cycle_Processed.setValue(true);                                                                                                                       //Natural: ASSIGN #CYCLE-PROCESSED := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        if (condition(pnd_Cycle_Processed.equals(true)))                                                                                                                  //Natural: IF #CYCLE-PROCESSED = TRUE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(6),"Batch Cycle Already Executed For :",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new  //Natural: WRITE ( 00 ) '***' 06T 'Batch Cycle Already Executed For :' #TWRP0600-ORIGIN-CODE 77T '***' / '***' 06T 'Processing Date (YYYYMMDD):' #TWRP0600-TO-CCYYMMDD ( EM = XXXX/XX/XX ) 77T '***' / '***' 06T 'Interface  Date (YYYYMMDD):' #TWRP0600-INTERFACE-CCYYMMDD ( EM = XXXX/XX/XX ) 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Processing Date (YYYYMMDD):",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(), 
                new ReportEditMask ("XXXX/XX/XX"),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Interface  Date (YYYYMMDD):",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd(), 
                new ReportEditMask ("XXXX/XX/XX"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Adjustment_Payment() throws Exception                                                                                                            //Natural: GET-ADJUSTMENT-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Year.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year());                                                           //Natural: ASSIGN #TWRPYMNT-TAX-YEAR := TWRT-TAX-YEAR
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Tax_Id_Nbr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id());                                                           //Natural: ASSIGN #TWRPYMNT-TAX-ID-NBR := TWRT-TAX-ID
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Contract_Nbr.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr());                                                     //Natural: ASSIGN #TWRPYMNT-CONTRACT-NBR := TWRT-CNTRCT-NBR
        pnd_Twrpymnt_Ssn_Con_Pay_Sd_Pnd_Twrpymnt_Payee_Cde.setValue(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde());                                                         //Natural: ASSIGN #TWRPYMNT-PAYEE-CDE := TWRT-PAYEE-CDE
        ldaTwrl9415.getVw_pay().startDatabaseRead                                                                                                                         //Natural: READ PAY WITH TWRPYMNT-SSN-CON-PAY-SD = #TWRPYMNT-SSN-CON-PAY-SD
        (
        "READ07",
        new Wc[] { new Wc("TWRPYMNT_SSN_CON_PAY_SD", ">=", pnd_Twrpymnt_Ssn_Con_Pay_Sd, WcType.BY) },
        new Oc[] { new Oc("TWRPYMNT_SSN_CON_PAY_SD", "ASC") }
        );
        READ07:
        while (condition(ldaTwrl9415.getVw_pay().readNextRow("READ07")))
        {
            if (condition(ldaTwrl9415.getPay_Twrpymnt_Tax_Year().equals(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Year()) && ldaTwrl9415.getPay_Twrpymnt_Tax_Id_Nbr().equals(ldaTwrl0700.getTwrt_Record_Twrt_Tax_Id())  //Natural: IF TWRPYMNT-TAX-YEAR EQ TWRT-TAX-YEAR AND TWRPYMNT-TAX-ID-NBR EQ TWRT-TAX-ID AND TWRPYMNT-CONTRACT-NBR EQ TWRT-CNTRCT-NBR AND TWRPYMNT-PAYEE-CDE EQ TWRT-PAYEE-CDE
                && ldaTwrl9415.getPay_Twrpymnt_Contract_Nbr().equals(ldaTwrl0700.getTwrt_Record_Twrt_Cntrct_Nbr()) && ldaTwrl9415.getPay_Twrpymnt_Payee_Cde().equals(ldaTwrl0700.getTwrt_Record_Twrt_Payee_Cde())))
            {
                FOR11:                                                                                                                                                    //Natural: FOR K = 1 TO C*PAY.TWRPYMNT-PAYMENTS
                for (k.setValue(1); condition(k.lessOrEqual(ldaTwrl9415.getPay_Count_Casttwrpymnt_Payments())); k.nadd(1))
                {
                    //*  RC06
                    if (condition(((((ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("OP") || ldaTwrl9415.getPay_Twrpymnt_Orgn_Srce_Cde().getValue(k).equals("EW"))  //Natural: IF PAY.TWRPYMNT-ORGN-SRCE-CDE ( K ) = 'OP' OR = 'EW' AND PAY.TWRPYMNT-PYMNT-STATUS ( K ) = ' ' OR = 'C' AND TWRT-PLAN-NUM = PAY.TWRPYMNT-OMNI-PLAN ( K ) AND TWRT-SUBPLAN = PAY.TWRPYMNT-SUBPLAN ( K )
                        && (ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals(" ") || ldaTwrl9415.getPay_Twrpymnt_Pymnt_Status().getValue(k).equals("C"))) 
                        && ldaTwrl0700.getTwrt_Record_Twrt_Plan_Num().equals(ldaTwrl9415.getPay_Twrpymnt_Omni_Plan().getValue(k))) && ldaTwrl0700.getTwrt_Record_Twrt_Subplan().equals(ldaTwrl9415.getPay_Twrpymnt_Subplan().getValue(k)))))
                    {
                        ldaTwrl0700.getTwrt_Record_Twrt_Orig_Subplan().setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Subplan().getValue(k));                                  //Natural: MOVE PAY.TWRPYMNT-ORIG-SUBPLAN ( K ) TO TWRT-ORIG-SUBPLAN
                        ldaTwrl0700.getTwrt_Record_Twrt_Orig_Contract_Nbr().setValue(ldaTwrl9415.getPay_Twrpymnt_Orig_Contract_Nbr().getValue(k));                        //Natural: MOVE PAY.TWRPYMNT-ORIG-CONTRACT-NBR ( K ) TO TWRT-ORIG-CONTRACT-NBR
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 0 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 0 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 0 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    //*  FE201506 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR12:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,"=",pnd_Rc);                                                                                                           //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,              //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE,"****************************",NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 01",new     //Natural: WRITE ( 02 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 01' 16X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 02 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X 'Accepted Tax Transactions (EDITS)' 31X 'Date: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(2),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8),"Accepted Tax Transactions (EDITS)",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/",
                        NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 02",new     //Natural: WRITE ( 03 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 02' 16X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 03 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X 'Rejected Tax Transactions (Edits)' 31X 'Date: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(3),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8),"Rejected Tax Transactions (Edits)",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/",
                        NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 03",new     //Natural: WRITE ( 04 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 03' 16X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 04 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X ' Cited Tax Transactions (Edits)  ' 31X 'DATE: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(4),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8)," Cited Tax Transactions (Edits)  ",new ColumnSpacing(31),"DATE: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/",
                        NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 04",new     //Natural: WRITE ( 05 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 04' 16X 'Tax Reporting And Withholding System' 26X 'Page:   ' *PAGE-NUMBER ( 05 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X ' Summary Totals Primary (Edits)' 29X 'DATE: '*DATU / 54X #WS.#COMP-NAME-DISP
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(26),"Page:   ",getReports().getPageNumberDbs(5),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8)," Summary Totals Primary (Edits)",new ColumnSpacing(29),"DATE: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp);
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"Program:",Global.getPROGRAM(),"- 05",new     //Natural: WRITE ( 06 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'Program:' *PROGRAM '- 05' 16X 'Tax Reporting And Withholding System' 30X 'Page:   ' *PAGE-NUMBER ( 06 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X 'Reversal Tax Transactions (Edits)' 31X 'Date: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"Page:   ",getReports().getPageNumberDbs(6),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8),"Reversal Tax Transactions (Edits)",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/",
                        NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE,ReportOption.NOHDR,"Job Name:",Global.getINIT_USER(),NEWLINE,"PROGRAM:",Global.getPROGRAM(),"- 06",new     //Natural: WRITE ( 07 ) NOTITLE NOHDR 'Job Name:' *INIT-USER / 'PROGRAM:' *PROGRAM '- 06' 16X 'Tax Reporting And Withholding System' 30X 'PAGE:   ' *PAGE-NUMBER ( 07 ) / 'System -' #T-SOURCE-NAME ( #SOURCE-IDX ) 8X 'Adjustment Tax Transactions (Edits)' 31X 'Date: '*DATU / 54X #WS.#COMP-NAME-DISP / / 'Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest' '      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng' / 'Number   Code  Date    Ind.ID No  Type     Amount       Amount' '      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/' / '                                                              ' '                            PUERTO-RICO                  Subplan'
                        ColumnSpacing(16),"Tax Reporting And Withholding System",new ColumnSpacing(30),"PAGE:   ",getReports().getPageNumberDbs(7),NEWLINE,"System -",pnd_Ws_Pnd_T_Source_Name.getValue(pnd_Ws_Pnd_Source_Idx),new 
                        ColumnSpacing(8),"Adjustment Tax Transactions (Edits)",new ColumnSpacing(31),"Date: ",Global.getDATU(),NEWLINE,new ColumnSpacing(54),
                        pnd_Ws_Pnd_Comp_Name_Disp,NEWLINE,NEWLINE,"Contract Payee Pay     Taxpayer   Tax ID    Gross     Interest","      IVC         FED TAX      STATE     Res Ctz Plan    Orgntng",
                        NEWLINE,"Number   Code  Date    Ind.ID No  Type     Amount       Amount","      Amount      NRA Amt      Amount    Cde Cde SubPlan Cntrct/",
                        NEWLINE,"                                                              ","                            PUERTO-RICO                  Subplan");
                    //*    / 51X #WS.#COMP-NAME-DISP 2X #P-YEAR
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Company_Cde().isBreak(endOfData);
        boolean ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak = ldaTwrl0700.getTwrt_Record_Twrt_Source().isBreak(endOfData);
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_Company_CdeIsBreak || ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            //*  ----------------------------
            //*  COMPANY BREAK PROCESSING
            //*  PRINT ACCEPTED, REJECTED, CITED RECS
            //*  REPORTS FOR THE PREVIOUS COMPANY
            //*  ----------------------------
            if (condition(pnd_Ws_Pnd_Acc_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #ACC-CNT ( #COMP-IDX ) > 0
            {
                //*  ACCEPTED
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Company:",new ColumnSpacing(19),pnd_Ws_Pnd_Acc_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 02 ) / '*' ( 131 ) / 'Totals By Company:' 19X #ACC-GROSS ( #COMP-IDX ) #ACC-INT ( #COMP-IDX ) #ACC-IVC ( #COMP-IDX ) #ACC-FED ( #COMP-IDX ) #ACC-STATE ( #COMP-IDX )
                    pnd_Ws_Pnd_Acc_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Acc_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Acc_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Acc_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                //*        #ACC-LOCAL (#COMP-IDX)
                if (condition(pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() //Natural: IF #ACC-PR ( #COMP-IDX ) > 0 OR #ACC-NRA ( #COMP-IDX ) > 0
                    + 1).greater(getZero())))
                {
                    getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 02 ) 77X #ACC-NRA ( #COMP-IDX ) #ACC-PR ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 02 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #REJ-CNT ( #COMP-IDX ) > 0
            {
                //*  REJECTED
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Company:",new ColumnSpacing(19),pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 03 ) / '*' ( 131 ) / 'Totals By Company:' 19X #REJ-GROSS ( #COMP-IDX ) #REJ-INT ( #COMP-IDX ) #REJ-IVC ( #COMP-IDX ) #REJ-FED ( #COMP-IDX ) #REJ-STATE ( #COMP-IDX )
                    pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                //*        #REJ-NRA   (#COMP-IDX)
                //*        #REJ-LOCAL (#COMP-IDX)
                if (condition(pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() //Natural: IF #REJ-PR ( #COMP-IDX ) > 0 OR #REJ-NRA ( #COMP-IDX ) > 0
                    + 1).greater(getZero())))
                {
                    getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 03 ) 77X #REJ-NRA ( #COMP-IDX ) #REJ-PR ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 03 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Cit_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #CIT-CNT ( #COMP-IDX ) > 0
            {
                //*  CITED
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Company:",new ColumnSpacing(19),pnd_Ws_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 04 ) / '*' ( 131 ) / 'Totals By Company:' 19X #CIT-GROSS ( #COMP-IDX ) #CIT-INT ( #COMP-IDX ) #CIT-IVC ( #COMP-IDX ) #CIT-FED ( #COMP-IDX ) #CIT-STATE ( #COMP-IDX )
                    pnd_Ws_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Cit_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                //*        #CIT-NRA   (#COMP-IDX)
                //*        #CIT-LOCAL (#COMP-IDX)
                if (condition(pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() //Natural: IF #CIT-PR ( #COMP-IDX ) > 0 OR #CIT-NRA ( #COMP-IDX ) > 0
                    + 1).greater(getZero())))
                {
                    getReports().write(4, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 04 ) 77X #CIT-NRA ( #COMP-IDX ) #CIT-PR ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 04 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            //*      10/22/07    RM
            if (condition(pnd_Ws_Pnd_Rev_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #REV-CNT ( #COMP-IDX ) > 0
            {
                pnd_Ws_Pnd_Rev_Gross_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Gross_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-GROSS-M ( #COMP-IDX ) := #REV-GROSS ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                pnd_Ws_Pnd_Rev_Int_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Int_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-INT-M ( #COMP-IDX ) := #REV-INT ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                pnd_Ws_Pnd_Rev_Ivc_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Ivc_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-IVC-M ( #COMP-IDX ) := #REV-IVC ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                pnd_Ws_Pnd_Rev_Fed_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Fed_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-FED-M ( #COMP-IDX ) := #REV-FED ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                pnd_Ws_Pnd_Rev_Nra_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Nra_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-NRA-M ( #COMP-IDX ) := #REV-NRA ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                pnd_Ws_Pnd_Rev_State_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_State_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-STATE-M ( #COMP-IDX ) := #REV-STATE ( #COMP-IDX ) * -1
                    + 1)), pnd_Ws_Pnd_Rev_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                //*  REVERSALS
                getReports().write(6, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Company:",new ColumnSpacing(19),pnd_Ws_Pnd_Rev_Gross_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 06 ) / '*' ( 131 ) / 'Totals By Company:' 19X #REV-GROSS-M ( #COMP-IDX ) #REV-INT-M ( #COMP-IDX ) #REV-IVC-M ( #COMP-IDX ) #REV-FED-M ( #COMP-IDX ) #REV-STATE-M ( #COMP-IDX )
                    pnd_Ws_Pnd_Rev_Int_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rev_Ivc_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rev_Fed_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Rev_State_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                //*        #REV-NRA-M   (#COMP-IDX)
                //*        #REV-LOCAL-M (#COMP-IDX)
                if (condition(pnd_Ws_Pnd_Rev_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Ws_Pnd_Rev_Nra_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: IF #REV-PR ( #COMP-IDX ) > 0 OR #REV-NRA-M ( #COMP-IDX ) > 0
                    + 1).greater(getZero())))
                {
                    pnd_Ws_Pnd_Rev_Pr_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).compute(new ComputeParameters(false, pnd_Ws_Pnd_Rev_Pr_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt()  //Natural: ASSIGN #REV-PR-M ( #COMP-IDX ) := #REV-PR ( #COMP-IDX ) * -1
                        + 1)), pnd_Ws_Pnd_Rev_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).multiply(-1));
                    //*   #REV-CAN (#COMP-IDX)
                    getReports().write(6, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Rev_Nra_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Rev_Pr_M.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 06 ) 77X #REV-NRA-M ( #COMP-IDX ) #REV-PR-M ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(6));                                                                                                         //Natural: NEWPAGE ( 06 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Adj_Cnt.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero())))                                                              //Natural: IF #ADJ-CNT ( #COMP-IDX ) > 0
            {
                //*  ADJUSTMENTS
                getReports().write(7, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Company:",new ColumnSpacing(19),pnd_Ws_Pnd_Adj_Gross.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1), //Natural: WRITE ( 07 ) / '*' ( 131 ) / 'Totals By Company:' 19X #ADJ-GROSS ( #COMP-IDX ) #ADJ-INT ( #COMP-IDX ) #ADJ-IVC ( #COMP-IDX ) #ADJ-FED ( #COMP-IDX ) #ADJ-STATE ( #COMP-IDX )
                    pnd_Ws_Pnd_Adj_Int.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Adj_Ivc.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Adj_Fed.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),
                    pnd_Ws_Pnd_Adj_State.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1));
                if (condition(Global.isEscape())) return;
                //*        #ADJ-NRA   (#COMP-IDX)
                //*        #ADJ-LOCAL (#COMP-IDX)
                if (condition(pnd_Ws_Pnd_Adj_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1).greater(getZero()) || pnd_Ws_Pnd_Adj_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() //Natural: IF #ADJ-PR ( #COMP-IDX ) > 0 OR #ADJ-NRA ( #COMP-IDX ) > 0
                    + 1).greater(getZero())))
                {
                    //*   #ADJ-CAN (#COMP-IDX)
                    getReports().write(7, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Adj_Nra.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1),pnd_Ws_Pnd_Adj_Pr.getValue(pnd_Ws_Pnd_Comp_Idx.getInt() + 1)); //Natural: WRITE ( 07 ) 77X #ADJ-NRA ( #COMP-IDX ) #ADJ-PR ( #COMP-IDX )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(7));                                                                                                         //Natural: NEWPAGE ( 07 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Comp_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #WS.#COMP-BREAK := TRUE
            //*  END OF COMPANY BREAK PROCESSING
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaTwrl0700_getTwrt_Record_Twrt_SourceIsBreak))
        {
            //* *-----------------------
            //*  SOURCE CODE BREAK PROCESSING
            //*  PRINT ACCEPTED, REJECTED, CITED, REVERSAL AND ADJUSTMENT RECS
            //*  REPORTS FOR THE PREVIOUS SOURCE CODE
            //* *
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #ACC-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #ACC-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #ACC-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #ACC-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #ACC-NRA ( * )
            pnd_Ws_Pnd_Comb_Can.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Can), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Can.getValue("*")));                 //Natural: COMPUTE #COMB-CAN = 0.00 + #ACC-CAN ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #ACC-PR ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #ACC-STATE ( * )
            pnd_Ws_Pnd_Comb_Local.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Local), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Acc_Local.getValue("*")));           //Natural: COMPUTE #COMB-LOCAL = 0.00 + #ACC-LOCAL ( * )
            if (condition(pnd_Ws_Pnd_Acc_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #ACC-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                //*  ACCEPTED
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Source Code:",new ColumnSpacing(15),                //Natural: WRITE ( 02 ) // '*' ( 131 ) / 'Totals By Source Code:' 15X #COMB-GROSS #COMB-INT #COMB-IVC #COMB-FED #COMB-STATE
                    pnd_Ws_Pnd_Comb_Gross,pnd_Ws_Pnd_Comb_Int,pnd_Ws_Pnd_Comb_Ivc,pnd_Ws_Pnd_Comb_Fed,pnd_Ws_Pnd_Comb_State);
                if (condition(Global.isEscape())) return;
                //*        #COMB-NRA
                //*        #COMB-LOCAL
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero()) || pnd_Ws_Pnd_Comb_Nra.greater(getZero())))                                                           //Natural: IF #COMB-PR > 0 OR #COMB-NRA > 0
                {
                    getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Comb_Nra,pnd_Ws_Pnd_Comb_Pr);                                             //Natural: WRITE ( 02 ) 77X #COMB-NRA #COMB-PR
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 02 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #REJ-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #REJ-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #REJ-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #REJ-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #REJ-NRA ( * )
            pnd_Ws_Pnd_Comb_Can.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Can), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Can.getValue("*")));                 //Natural: COMPUTE #COMB-CAN = 0.00 + #REJ-CAN ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #REJ-PR ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #REJ-STATE ( * )
            pnd_Ws_Pnd_Comb_Local.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Local), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rej_Local.getValue("*")));           //Natural: COMPUTE #COMB-LOCAL = 0.00 + #REJ-LOCAL ( * )
            if (condition(pnd_Ws_Pnd_Rej_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #REJ-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                //*  REJECTED
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Source Code:",new ColumnSpacing(15),                //Natural: WRITE ( 03 ) // '*' ( 131 ) / 'Totals By Source Code:' 15X #COMB-GROSS #COMB-INT #COMB-IVC #COMB-FED #COMB-STATE
                    pnd_Ws_Pnd_Comb_Gross,pnd_Ws_Pnd_Comb_Int,pnd_Ws_Pnd_Comb_Ivc,pnd_Ws_Pnd_Comb_Fed,pnd_Ws_Pnd_Comb_State);
                if (condition(Global.isEscape())) return;
                //*        #COMB-NRA
                //*        #COMB-LOCAL
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero()) || pnd_Ws_Pnd_Comb_Nra.greater(getZero())))                                                           //Natural: IF #COMB-PR > 0 OR #COMB-NRA > 0
                {
                    //*  #COMB-CAN
                    getReports().write(3, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Comb_Nra,pnd_Ws_Pnd_Comb_Pr);                                             //Natural: WRITE ( 03 ) 77X #COMB-NRA #COMB-PR
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 03 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  06-20-00 FRANK
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #CIT-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #CIT-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #CIT-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #CIT-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #CIT-NRA ( * )
            pnd_Ws_Pnd_Comb_Can.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Can), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Can.getValue("*")));                 //Natural: COMPUTE #COMB-CAN = 0.00 + #CIT-CAN ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #CIT-PR ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #CIT-STATE ( * )
            pnd_Ws_Pnd_Comb_Local.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Local), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Cit_Local.getValue("*")));           //Natural: COMPUTE #COMB-LOCAL = 0.00 + #CIT-LOCAL ( * )
            if (condition(pnd_Ws_Pnd_Cit_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #CIT-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                //*  CITED /**OR REJECTED
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Source Code:",new ColumnSpacing(15),                //Natural: WRITE ( 04 ) // '*' ( 131 ) / 'Totals By Source Code:' 15X #COMB-GROSS #COMB-INT #COMB-IVC #COMB-FED #COMB-STATE
                    pnd_Ws_Pnd_Comb_Gross,pnd_Ws_Pnd_Comb_Int,pnd_Ws_Pnd_Comb_Ivc,pnd_Ws_Pnd_Comb_Fed,pnd_Ws_Pnd_Comb_State);
                if (condition(Global.isEscape())) return;
                //*        #COMB-NRA
                //*        #COMB-LOCAL
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero()) || pnd_Ws_Pnd_Comb_Nra.greater(getZero())))                                                           //Natural: IF #COMB-PR > 0 OR #COMB-NRA > 0
                {
                    //*  #COMB-CAN
                    getReports().write(4, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Comb_Nra,pnd_Ws_Pnd_Comb_Pr);                                             //Natural: WRITE ( 04 ) 77X #COMB-NRA #COMB-PR
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(4));                                                                                                         //Natural: NEWPAGE ( 04 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #REV-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #REV-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #REV-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #REV-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #REV-NRA ( * )
            pnd_Ws_Pnd_Comb_Can.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Can), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Can.getValue("*")));                 //Natural: COMPUTE #COMB-CAN = 0.00 + #REV-CAN ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #REV-PR ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #REV-STATE ( * )
            pnd_Ws_Pnd_Comb_Local.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Local), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Rev_Local.getValue("*")));           //Natural: COMPUTE #COMB-LOCAL = 0.00 + #REV-LOCAL ( * )
            if (condition(pnd_Ws_Pnd_Rev_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #REV-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                //*                                                        10/22/07     RM
                pnd_Ws_Pnd_Comb_Gross_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross_M), pnd_Ws_Pnd_Comb_Gross.multiply(-1));                               //Natural: ASSIGN #COMB-GROSS-M := #COMB-GROSS * -1
                pnd_Ws_Pnd_Comb_Int_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int_M), pnd_Ws_Pnd_Comb_Int.multiply(-1));                                     //Natural: ASSIGN #COMB-INT-M := #COMB-INT * -1
                pnd_Ws_Pnd_Comb_Ivc_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc_M), pnd_Ws_Pnd_Comb_Ivc.multiply(-1));                                     //Natural: ASSIGN #COMB-IVC-M := #COMB-IVC * -1
                pnd_Ws_Pnd_Comb_Fed_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed_M), pnd_Ws_Pnd_Comb_Fed.multiply(-1));                                     //Natural: ASSIGN #COMB-FED-M := #COMB-FED * -1
                pnd_Ws_Pnd_Comb_Nra_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra_M), pnd_Ws_Pnd_Comb_Nra.multiply(-1));                                     //Natural: ASSIGN #COMB-NRA-M := #COMB-NRA * -1
                pnd_Ws_Pnd_Comb_Pr_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr_M), pnd_Ws_Pnd_Comb_Pr.multiply(-1));                                        //Natural: ASSIGN #COMB-PR-M := #COMB-PR * -1
                pnd_Ws_Pnd_Comb_State_M.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State_M), pnd_Ws_Pnd_Comb_State.multiply(-1));                               //Natural: ASSIGN #COMB-STATE-M := #COMB-STATE * -1
                //*  REVERSALS
                //*  #COMB-LOCAL
                getReports().write(6, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Source Code:",new ColumnSpacing(15),                //Natural: WRITE ( 06 ) // '*' ( 131 ) / 'Totals By Source Code:' 15X #COMB-GROSS-M #COMB-INT-M #COMB-IVC-M #COMB-FED-M #COMB-STATE-M
                    pnd_Ws_Pnd_Comb_Gross_M,pnd_Ws_Pnd_Comb_Int_M,pnd_Ws_Pnd_Comb_Ivc_M,pnd_Ws_Pnd_Comb_Fed_M,pnd_Ws_Pnd_Comb_State_M);
                if (condition(Global.isEscape())) return;
                //*        #COMB-NRA-M
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero()) || pnd_Ws_Pnd_Comb_Nra_M.greater(getZero())))                                                         //Natural: IF #COMB-PR > 0 OR #COMB-NRA-M > 0
                {
                    //*  #COMB-CAN
                    getReports().write(6, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Comb_Nra_M,pnd_Ws_Pnd_Comb_Pr_M);                                         //Natural: WRITE ( 06 ) 77X #COMB-NRA-M #COMB-PR-M
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(6));                                                                                                         //Natural: NEWPAGE ( 06 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Comb_Gross.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Gross), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Gross.getValue("*")));           //Natural: COMPUTE #COMB-GROSS = 0.00 + #ADJ-GROSS ( * )
            pnd_Ws_Pnd_Comb_Int.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Int), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Int.getValue("*")));                 //Natural: COMPUTE #COMB-INT = 0.00 + #ADJ-INT ( * )
            pnd_Ws_Pnd_Comb_Ivc.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Ivc), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Ivc.getValue("*")));                 //Natural: COMPUTE #COMB-IVC = 0.00 + #ADJ-IVC ( * )
            pnd_Ws_Pnd_Comb_Fed.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Fed), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Fed.getValue("*")));                 //Natural: COMPUTE #COMB-FED = 0.00 + #ADJ-FED ( * )
            pnd_Ws_Pnd_Comb_Nra.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Nra), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Nra.getValue("*")));                 //Natural: COMPUTE #COMB-NRA = 0.00 + #ADJ-NRA ( * )
            pnd_Ws_Pnd_Comb_Can.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Can), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Can.getValue("*")));                 //Natural: COMPUTE #COMB-CAN = 0.00 + #ADJ-CAN ( * )
            pnd_Ws_Pnd_Comb_Pr.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Pr), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Pr.getValue("*")));                    //Natural: COMPUTE #COMB-PR = 0.00 + #ADJ-PR ( * )
            pnd_Ws_Pnd_Comb_State.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_State), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_State.getValue("*")));           //Natural: COMPUTE #COMB-STATE = 0.00 + #ADJ-STATE ( * )
            pnd_Ws_Pnd_Comb_Local.compute(new ComputeParameters(false, pnd_Ws_Pnd_Comb_Local), new DbsDecimal("0.00").add(pnd_Ws_Pnd_Adj_Local.getValue("*")));           //Natural: COMPUTE #COMB-LOCAL = 0.00 + #ADJ-LOCAL ( * )
            if (condition(pnd_Ws_Pnd_Adj_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #ADJ-CNT ( * ) > 0
            {
                pnd_Ws_Pnd_Comp_Name_Disp.reset();                                                                                                                        //Natural: RESET #WS.#COMP-NAME-DISP
                //*  ADJUSTMENTS
                //*  #COMB-LOCAL
                getReports().write(7, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(131),NEWLINE,"Totals By Source Code:",new ColumnSpacing(15),                //Natural: WRITE ( 07 ) // '*' ( 131 ) / 'Totals By Source Code:' 15X #COMB-GROSS #COMB-INT #COMB-IVC #COMB-FED #COMB-STATE
                    pnd_Ws_Pnd_Comb_Gross,pnd_Ws_Pnd_Comb_Int,pnd_Ws_Pnd_Comb_Ivc,pnd_Ws_Pnd_Comb_Fed,pnd_Ws_Pnd_Comb_State);
                if (condition(Global.isEscape())) return;
                //*        #COMB-NRA
                if (condition(pnd_Ws_Pnd_Comb_Pr.greater(getZero()) || pnd_Ws_Pnd_Comb_Nra.greater(getZero())))                                                           //Natural: IF #COMB-PR > 0 OR #COMB-NRA > 0
                {
                    getReports().write(7, ReportOption.NOTITLE,new ColumnSpacing(77),pnd_Ws_Pnd_Comb_Nra,pnd_Ws_Pnd_Comb_Pr);                                             //Natural: WRITE ( 07 ) 77X #COMB-NRA #COMB-PR
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(7));                                                                                                         //Natural: NEWPAGE ( 07 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            //* **
            //*  COMPARE TOTALS OF THE FILE WITH TOTALS OF THE TRAILER
            //*  AND PRINT CONTROL TOTALS REPORT
            //* **
            FOR01:                                                                                                                                                        //Natural: FOR #J = 1 TO COMP-MAX
            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(pnd_Ws_Const_Comp_Max)); pnd_Ws_Pnd_J.nadd(1))
            {
                pnd_Ws_Pnd_Comp_Name_Disp.setValue(pnd_Ws_Pnd_Comp_Name.getValue(pnd_Ws_Pnd_J.getInt() + 1));                                                             //Natural: ASSIGN #COMP-NAME-DISP := #WS.#COMP-NAME ( #J )
                pnd_Ws_Pnd_Dif_Cnt.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Cnt.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Cnt.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Cnt.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-CNT ( #J ) = #INP-CNT ( #J ) - #ACC-CNT ( #J ) - #REJ-CNT ( #J ) - #CIT-CNT ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Dif_Gross.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Gross.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Gross.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Gross.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-GROSS ( #J ) = #INP-GROSS ( #J ) - #ACC-GROSS ( #J ) - #REJ-GROSS ( #J ) - #CIT-GROSS ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_J.getInt() 
                    + 1)));
                pnd_Ws_Pnd_Dif_Int.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Int.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Int.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Int.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-INT ( #J ) = #INP-INT ( #J ) - #ACC-INT ( #J ) - #REJ-INT ( #J ) - #CIT-INT ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Dif_Fed.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Fed.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Fed.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Fed.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-FED ( #J ) = #INP-FED ( #J ) - #ACC-FED ( #J ) - #REJ-FED ( #J ) - #CIT-FED ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Dif_Nra.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Nra.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Nra.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-NRA ( #J ) = #INP-NRA ( #J ) - #ACC-NRA ( #J ) - #REJ-NRA ( #J ) - #CIT-NRA ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Dif_Ivc.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Ivc.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Ivc.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Ivc.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-IVC ( #J ) = #INP-IVC ( #J ) - #ACC-IVC ( #J ) - #REJ-IVC ( #J ) - #CIT-IVC ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Dif_State.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_State.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_State.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_State.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-STATE ( #J ) = #INP-STATE ( #J ) - #ACC-STATE ( #J ) - #REJ-STATE ( #J ) - #CIT-STATE ( #J ) - #ACC-PR ( #J ) - #REJ-PR ( #J ) - #CIT-PR ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_State.getValue(pnd_Ws_Pnd_J.getInt() 
                    + 1)).subtract(pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_J.getInt() 
                    + 1)));
                pnd_Ws_Pnd_Dif_Local.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Local.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Local.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Local.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-LOCAL ( #J ) = #INP-LOCAL ( #J ) - #ACC-LOCAL ( #J ) - #REJ-LOCAL ( #J ) - #CIT-LOCAL ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_J.getInt() 
                    + 1)));
                pnd_Ws_Pnd_Dif_Can.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Dif_Can.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Inp_Can.getValue(pnd_Ws_Pnd_J).subtract(pnd_Ws_Pnd_Acc_Can.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #DIF-CAN ( #J ) = #INP-CAN ( #J ) - #ACC-CAN ( #J ) - #REJ-CAN ( #J ) - #CIT-CAN ( #J )
                    + 1)).subtract(pnd_Ws_Pnd_Rej_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1)).subtract(pnd_Ws_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_J.getInt() + 
                    1)));
                pnd_Ws_Pnd_Ac_Cnt.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Cnt.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Cnt.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-CNT ( #J ) = #CIT-CNT ( #J ) + #ACC-CNT ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Gross_Unkn.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Gross_Unkn.getValue(pnd_Ws_Pnd_J)),                    //Natural: COMPUTE #AC-GROSS-UNKN ( #J ) = #CIT-GROSS-UNKN ( #J ) + #ACC-GROSS-UNKN ( #J )
                    pnd_Ws_Pnd_Cit_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1).add(pnd_Ws_Pnd_Acc_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Gross_Roll.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Gross_Roll.getValue(pnd_Ws_Pnd_J)),                    //Natural: COMPUTE #AC-GROSS-ROLL ( #J ) = #CIT-GROSS-ROLL ( #J ) + #ACC-GROSS-ROLL ( #J )
                    pnd_Ws_Pnd_Cit_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1).add(pnd_Ws_Pnd_Acc_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Gross.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Gross.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-GROSS ( #J ) = #CIT-GROSS ( #J ) + #ACC-GROSS ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Int.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Int.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-INT ( #J ) = #CIT-INT ( #J ) + #ACC-INT ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Ivc.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Ivc.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-IVC ( #J ) = #CIT-IVC ( #J ) + #ACC-IVC ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Nra.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Nra.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-NRA ( #J ) = #CIT-NRA ( #J ) + #ACC-NRA ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Can.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Can.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-CAN ( #J ) = #CIT-CAN ( #J ) + #ACC-CAN ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Local.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Local.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-LOCAL ( #J ) = #CIT-LOCAL ( #J ) + #ACC-LOCAL ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Fed.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Fed.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-FED ( #J ) = #CIT-FED ( #J ) + #ACC-FED ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_State.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_State.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_State.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-STATE ( #J ) = #CIT-STATE ( #J ) + #ACC-STATE ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_State.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                pnd_Ws_Pnd_Ac_Pr.getValue(pnd_Ws_Pnd_J).compute(new ComputeParameters(false, pnd_Ws_Pnd_Ac_Pr.getValue(pnd_Ws_Pnd_J)), pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_J.getInt()  //Natural: COMPUTE #AC-PR ( #J ) = #CIT-PR ( #J ) + #ACC-PR ( #J )
                    + 1).add(pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1)));
                if (condition(pnd_Ws_Pnd_Inp_Cnt.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) || pnd_Ws_Pnd_Dif_Cnt.getValue(pnd_Ws_Pnd_J).notEquals(getZero())))          //Natural: IF #INP-CNT ( #J ) NE 0 OR #DIF-CNT ( #J ) NE 0
                {
                    getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,"NUMBER OF INPUT RECORDS   :    ",pnd_Ws_Pnd_Inp_Cnt.getValue(pnd_Ws_Pnd_J),NEWLINE,"GROSS            AMOUNT   :",pnd_Ws_Pnd_Inp_Gross.getValue(pnd_Ws_Pnd_J),NEWLINE,"IVC              AMOUNT   :",pnd_Ws_Pnd_Inp_Ivc.getValue(pnd_Ws_Pnd_J),NEWLINE,"INTEREST         AMOUNT   :",pnd_Ws_Pnd_Inp_Int.getValue(pnd_Ws_Pnd_J),NEWLINE,"FEDERAL      W/H AMOUNT   :",pnd_Ws_Pnd_Inp_Fed.getValue(pnd_Ws_Pnd_J),NEWLINE,"NRA          W/H AMOUNT   :",pnd_Ws_Pnd_Inp_Nra.getValue(pnd_Ws_Pnd_J),NEWLINE,"STATE + PR   W/H AMOUNT   :",pnd_Ws_Pnd_Inp_State.getValue(pnd_Ws_Pnd_J),NEWLINE,"LOCAL        W/H AMOUNT   :",pnd_Ws_Pnd_Inp_Local.getValue(pnd_Ws_Pnd_J),NEWLINE,"CANADIAN     W/H AMOUNT   :",pnd_Ws_Pnd_Inp_Can.getValue(pnd_Ws_Pnd_J),NEWLINE,NEWLINE,"NUMBER OF REJECTED RECORDS:    ",pnd_Ws_Pnd_Rej_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new  //Natural: WRITE ( 05 ) // 'NUMBER OF INPUT RECORDS   :    ' #INP-CNT ( #J ) / 'GROSS            AMOUNT   :' #INP-GROSS ( #J ) / 'IVC              AMOUNT   :' #INP-IVC ( #J ) / 'INTEREST         AMOUNT   :' #INP-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :' #INP-FED ( #J ) / 'NRA          W/H AMOUNT   :' #INP-NRA ( #J ) / 'STATE + PR   W/H AMOUNT   :' #INP-STATE ( #J ) / 'LOCAL        W/H AMOUNT   :' #INP-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :' #INP-CAN ( #J ) // 'NUMBER OF REJECTED RECORDS:    ' #REJ-CNT ( #J ) 3X '=========>' / 'GROSS            AMOUNT   :  ' #REJ-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #REJ-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :   ' #REJ-IVC ( #J ) 10X 'UNKNOWN IVC          :' #REJ-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #REJ-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #REJ-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #REJ-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #REJ-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #REJ-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #REJ-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #REJ-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #REJ-PR ( #J ) // 'NUMBER OF ACCEPTED RECORDS:    ' #ACC-CNT ( #J ) 3X '=========>' / 'GROSS            AMOUNT   :  ' #ACC-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #ACC-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :  ' #ACC-IVC ( #J ) 10X 'UNKNOWN IVC          :' #ACC-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #ACC-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #ACC-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #ACC-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #ACC-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #ACC-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #ACC-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #ACC-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #ACC-PR ( #J ) // 'NUMBER OF CITED    RECORDS:    ' #CIT-CNT ( #J ) 3X '=========>' / 'GROSS            AMOUNT   :  ' #CIT-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #CIT-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :   ' #CIT-IVC ( #J ) 10X 'UNKNOWN IVC          :' #CIT-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #CIT-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #CIT-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #CIT-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #CIT-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #CIT-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #CIT-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #CIT-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #CIT-PR ( #J ) // 'NUMBER OF COMBINED RECORDS:    ' #AC-CNT ( #J ) 3X '=========>' / '(Accepted and Cited)' / 'GROSS            AMOUNT   :  ' #AC-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #AC-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :  ' #AC-IVC ( #J ) 10X 'UNKNOWN IVC          :' #AC-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #AC-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #AC-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #AC-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #AC-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #AC-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #AC-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #AC-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #AC-PR ( #J )
                        ColumnSpacing(3),"=========>",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Rej_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Rej_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :   ",pnd_Ws_Pnd_Rej_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Rej_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",pnd_Ws_Pnd_Rej_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Rej_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Rej_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,NEWLINE,"NUMBER OF ACCEPTED RECORDS:    ",pnd_Ws_Pnd_Acc_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(3),"=========>",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Acc_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Acc_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :  ",pnd_Ws_Pnd_Acc_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Acc_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",pnd_Ws_Pnd_Acc_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Acc_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Acc_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,NEWLINE,"NUMBER OF CITED    RECORDS:    ",pnd_Ws_Pnd_Cit_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(3),"=========>",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Cit_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Cit_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :   ",pnd_Ws_Pnd_Cit_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Cit_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",pnd_Ws_Pnd_Cit_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Cit_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Cit_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,NEWLINE,"NUMBER OF COMBINED RECORDS:    ",pnd_Ws_Pnd_Ac_Cnt.getValue(pnd_Ws_Pnd_J),new 
                        ColumnSpacing(3),"=========>",NEWLINE,"(Accepted and Cited)",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Ac_Gross.getValue(pnd_Ws_Pnd_J),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Ac_Gross_Roll.getValue(pnd_Ws_Pnd_J),NEWLINE,"IVC              AMOUNT   :  ",pnd_Ws_Pnd_Ac_Ivc.getValue(pnd_Ws_Pnd_J),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Ac_Gross_Unkn.getValue(pnd_Ws_Pnd_J),NEWLINE,"INTEREST         AMOUNT   :    ",
                        pnd_Ws_Pnd_Ac_Int.getValue(pnd_Ws_Pnd_J),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Ac_Fed.getValue(pnd_Ws_Pnd_J),NEWLINE,
                        "NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_Nra.getValue(pnd_Ws_Pnd_J),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_State.getValue(pnd_Ws_Pnd_J),
                        NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_Pr.getValue(pnd_Ws_Pnd_J),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_Local.getValue(pnd_Ws_Pnd_J),
                        NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_Can.getValue(pnd_Ws_Pnd_J),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Ac_Pr.getValue(pnd_Ws_Pnd_J));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().newPage(new ReportSpecification(5));                                                                                                     //Natural: NEWPAGE ( 05 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,"NUMBER OF REVERSAL RECORDS:    ",pnd_Ws_Pnd_Rev_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new  //Natural: WRITE ( 05 ) // 'NUMBER OF REVERSAL RECORDS:    ' #REV-CNT ( #J ) 3X '=========>' / '(Full Payment Reversal)' / 'GROSS            AMOUNT   :  ' #REV-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #REV-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :   ' #REV-IVC ( #J ) 10X 'UNKNOWN IVC          :' #REV-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #REV-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #REV-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #REV-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #REV-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #REV-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #REV-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #REV-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #REV-PR ( #J ) // 'NUMBER OF ADJUSTED RECORDS:    ' #ADJ-CNT ( #J ) 3X '=========>' / '(Contract Level Adjustment)' / 'GROSS            AMOUNT   :  ' #ADJ-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #ADJ-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :   ' #ADJ-IVC ( #J ) 10X 'UNKNOWN IVC          :' #ADJ-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #ADJ-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #ADJ-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #ADJ-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #ADJ-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #ADJ-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #ADJ-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #ADJ-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #ADJ-PR ( #J ) // 'NUMBER OF NEGATIVE ADJUST.:    ' #NEG-CNT ( #J ) 3X '=========>' / '(Net Negative Adjustments)' / 'GROSS            AMOUNT   :  ' #NEG-GROSS ( #J ) 10X 'ROLLOVER GROSS AMOUNT:' #NEG-GROSS-ROLL ( #J ) / 'IVC              AMOUNT   :   ' #NEG-IVC ( #J ) 10X 'UNKNOWN IVC          :' #NEG-GROSS-UNKN ( #J ) / 'INTEREST         AMOUNT   :    ' #NEG-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :   ' #NEG-FED ( #J ) / 'NRA          W/H AMOUNT   :    ' #NEG-NRA ( #J ) / 'STATE        W/H AMOUNT   :    ' #NEG-STATE ( #J ) / 'PUERTO RICO  W/H AMOUNT   :    ' #NEG-PR ( #J ) / 'LOCAL        W/H AMOUNT   :    ' #NEG-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :    ' #NEG-CAN ( #J ) / 'PUERTO-RICAN W/H AMOUNT   :    ' #NEG-PR ( #J )
                        ColumnSpacing(3),"=========>",NEWLINE,"(Full Payment Reversal)",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Rev_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Rev_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :   ",pnd_Ws_Pnd_Rev_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Rev_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",pnd_Ws_Pnd_Rev_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Rev_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Rev_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,NEWLINE,"NUMBER OF ADJUSTED RECORDS:    ",pnd_Ws_Pnd_Adj_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(3),"=========>",NEWLINE,"(Contract Level Adjustment)",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Adj_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Adj_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :   ",pnd_Ws_Pnd_Adj_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Adj_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",pnd_Ws_Pnd_Adj_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Adj_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Adj_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,NEWLINE,"NUMBER OF NEGATIVE ADJUST.:    ",pnd_Ws_Pnd_Neg_Cnt.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(3),"=========>",NEWLINE,"(Net Negative Adjustments)",NEWLINE,"GROSS            AMOUNT   :  ",pnd_Ws_Pnd_Neg_Gross.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"ROLLOVER GROSS AMOUNT:",pnd_Ws_Pnd_Neg_Gross_Roll.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"IVC              AMOUNT   :   ",pnd_Ws_Pnd_Neg_Ivc.getValue(pnd_Ws_Pnd_J.getInt() + 1),new 
                        ColumnSpacing(10),"UNKNOWN IVC          :",pnd_Ws_Pnd_Neg_Gross_Unkn.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"INTEREST         AMOUNT   :    ",
                        pnd_Ws_Pnd_Neg_Int.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"FEDERAL      W/H AMOUNT   :   ",pnd_Ws_Pnd_Neg_Fed.getValue(pnd_Ws_Pnd_J.getInt() + 1),
                        NEWLINE,"NRA          W/H AMOUNT   :    ",pnd_Ws_Pnd_Neg_Nra.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"STATE        W/H AMOUNT   :    ",
                        pnd_Ws_Pnd_Neg_State.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO RICO  W/H AMOUNT   :    ",pnd_Ws_Pnd_Neg_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1),
                        NEWLINE,"LOCAL        W/H AMOUNT   :    ",pnd_Ws_Pnd_Neg_Local.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"CANADIAN     W/H AMOUNT   :    ",
                        pnd_Ws_Pnd_Neg_Can.getValue(pnd_Ws_Pnd_J.getInt() + 1),NEWLINE,"PUERTO-RICAN W/H AMOUNT   :    ",pnd_Ws_Pnd_Neg_Pr.getValue(pnd_Ws_Pnd_J.getInt() + 1));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Pnd_Dif_Cnt.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) || pnd_Ws_Pnd_Dif_Gross.getValue(pnd_Ws_Pnd_J).notEquals(getZero())      //Natural: IF #DIF-CNT ( #J ) NE 0 OR #DIF-GROSS ( #J ) NE 0 OR #DIF-IVC ( #J ) NE 0 OR #DIF-INT ( #J ) NE 0 OR #DIF-FED ( #J ) NE 0 OR #DIF-NRA ( #J ) NE 0 OR #DIF-STATE ( #J ) NE 0 OR #DIF-LOCAL ( #J ) NE 0 OR #DIF-CAN ( #J ) NE 0
                        || pnd_Ws_Pnd_Dif_Ivc.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) || pnd_Ws_Pnd_Dif_Int.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) 
                        || pnd_Ws_Pnd_Dif_Fed.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) || pnd_Ws_Pnd_Dif_Nra.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) 
                        || pnd_Ws_Pnd_Dif_State.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) || pnd_Ws_Pnd_Dif_Local.getValue(pnd_Ws_Pnd_J).notEquals(getZero()) 
                        || pnd_Ws_Pnd_Dif_Can.getValue(pnd_Ws_Pnd_J).notEquals(getZero())))
                    {
                        getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(10),"T H E   R E P O R T   F O R ",pnd_Ws_Pnd_Comp_Name_Disp,        //Natural: WRITE ( 05 ) / / 10X 'T H E   R E P O R T   F O R ' #WS.#COMP-NAME-DISP ' I S   N O T   B A L A N C E D  !!!'
                            " I S   N O T   B A L A N C E D  !!!");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(5));                                                                                                 //Natural: NEWPAGE ( 05 )
                        if (condition(Global.isEscape())){return;}
                        getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE," W A R N I N G !!!",NEWLINE,NEWLINE,"************ OUT OF BALANCE CONDITION ************", //Natural: WRITE ( 05 ) // ' W A R N I N G !!!' // '************ OUT OF BALANCE CONDITION ************' / 'NUMBER OF DIFFERENCES     :    ' #DIF-CNT ( #J ) / 'GROSS            AMOUNT   :' #DIF-GROSS ( #J ) / 'IVC              AMOUNT   :' #DIF-IVC ( #J ) / 'INTEREST         AMOUNT   :' #DIF-INT ( #J ) / 'FEDERAL      W/H AMOUNT   :' #DIF-FED ( #J ) / 'NRA          W/H AMOUNT   :' #DIF-NRA ( #J ) / 'STATE + PR   W/H AMOUNT   :' #DIF-STATE ( #J ) / 'LOCAL        W/H AMOUNT   :' #DIF-LOCAL ( #J ) / 'CANADIAN     W/H AMOUNT   :' #DIF-CAN ( #J )
                            NEWLINE,"NUMBER OF DIFFERENCES     :    ",pnd_Ws_Pnd_Dif_Cnt.getValue(pnd_Ws_Pnd_J),NEWLINE,"GROSS            AMOUNT   :",pnd_Ws_Pnd_Dif_Gross.getValue(pnd_Ws_Pnd_J),
                            NEWLINE,"IVC              AMOUNT   :",pnd_Ws_Pnd_Dif_Ivc.getValue(pnd_Ws_Pnd_J),NEWLINE,"INTEREST         AMOUNT   :",pnd_Ws_Pnd_Dif_Int.getValue(pnd_Ws_Pnd_J),
                            NEWLINE,"FEDERAL      W/H AMOUNT   :",pnd_Ws_Pnd_Dif_Fed.getValue(pnd_Ws_Pnd_J),NEWLINE,"NRA          W/H AMOUNT   :",pnd_Ws_Pnd_Dif_Nra.getValue(pnd_Ws_Pnd_J),
                            NEWLINE,"STATE + PR   W/H AMOUNT   :",pnd_Ws_Pnd_Dif_State.getValue(pnd_Ws_Pnd_J),NEWLINE,"LOCAL        W/H AMOUNT   :",pnd_Ws_Pnd_Dif_Local.getValue(pnd_Ws_Pnd_J),
                            NEWLINE,"CANADIAN     W/H AMOUNT   :",pnd_Ws_Pnd_Dif_Can.getValue(pnd_Ws_Pnd_J));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(5));                                                                                                 //Natural: NEWPAGE ( 05 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(10),"T H E   R E P O R T   F O R ",pnd_Ws_Pnd_Comp_Name_Disp,        //Natural: WRITE ( 05 ) / / 10X 'T H E   R E P O R T   F O R ' #WS.#COMP-NAME-DISP ' I S   B A L A N C E D  !!!'
                            " I S   B A L A N C E D  !!!");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().newPage(new ReportSpecification(5));                                                                                                 //Natural: NEWPAGE ( 05 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  RCC
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            //* **
            //*   CREATE SUMMARY RECORDS WITH TOTALS FOR ACCEPTED & CITED RECS !!!!
            //*    PREPARE THE RECORD #INP-SUMMARY TO WRITE OUT
            //* **
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_Z().setValue("Z");                                                                                                         //Natural: ASSIGN #Z := 'Z'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Comp_Code().setValue("L");                                                                                               //Natural: ASSIGN #L-COMP-CODE := 'L'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().setValue(pnd_Ws_Pnd_Acc_Cnt.getValue(1 + 1));                                                                  //Natural: ASSIGN #L-REC-CNT := #ACC-CNT ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().setValue(pnd_Ws_Pnd_Acc_Gross.getValue(1 + 1));                                                                //Natural: ASSIGN #L-GROSS-A := #ACC-GROSS ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().setValue(pnd_Ws_Pnd_Acc_Ivc.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-IVC-A := #ACC-IVC ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().setValue(pnd_Ws_Pnd_Acc_Int.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-INT-A := #ACC-INT ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().setValue(pnd_Ws_Pnd_Acc_Fed.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-FED-A := #ACC-FED ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().setValue(pnd_Ws_Pnd_Acc_Nra.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-NRA-A := #ACC-NRA ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().setValue(pnd_Ws_Pnd_Acc_State.getValue(1 + 1));                                                                //Natural: ASSIGN #L-STATE-A := #ACC-STATE ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().setValue(pnd_Ws_Pnd_Acc_Local.getValue(1 + 1));                                                                //Natural: ASSIGN #L-LOCAL-A := #ACC-LOCAL ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().setValue(pnd_Ws_Pnd_Acc_Can.getValue(1 + 1));                                                               //Natural: ASSIGN #L-CANADIAN-A := #ACC-CAN ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Comp_Code().setValue("S");                                                                                               //Natural: ASSIGN #S-COMP-CODE := 'S'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt().setValue(pnd_Ws_Pnd_Acc_Cnt.getValue(2 + 1));                                                                  //Natural: ASSIGN #S-REC-CNT := #ACC-CNT ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A().setValue(pnd_Ws_Pnd_Acc_Gross.getValue(2 + 1));                                                                //Natural: ASSIGN #S-GROSS-A := #ACC-GROSS ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A().setValue(pnd_Ws_Pnd_Acc_Ivc.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-IVC-A := #ACC-IVC ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A().setValue(pnd_Ws_Pnd_Acc_Int.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-INT-A := #ACC-INT ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A().setValue(pnd_Ws_Pnd_Acc_Fed.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-FED-A := #ACC-FED ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A().setValue(pnd_Ws_Pnd_Acc_Nra.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-NRA-A := #ACC-NRA ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A().setValue(pnd_Ws_Pnd_Acc_State.getValue(2 + 1));                                                                //Natural: ASSIGN #S-STATE-A := #ACC-STATE ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A().setValue(pnd_Ws_Pnd_Acc_Local.getValue(2 + 1));                                                                //Natural: ASSIGN #S-LOCAL-A := #ACC-LOCAL ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A().setValue(pnd_Ws_Pnd_Acc_Can.getValue(2 + 1));                                                               //Natural: ASSIGN #S-CANADIAN-A := #ACC-CAN ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Comp_Code().setValue("T");                                                                                               //Natural: ASSIGN #T-COMP-CODE := 'T'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().setValue(pnd_Ws_Pnd_Acc_Cnt.getValue(3 + 1));                                                                  //Natural: ASSIGN #T-REC-CNT := #ACC-CNT ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().setValue(pnd_Ws_Pnd_Acc_Gross.getValue(3 + 1));                                                                //Natural: ASSIGN #T-GROSS-A := #ACC-GROSS ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().setValue(pnd_Ws_Pnd_Acc_Ivc.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-IVC-A := #ACC-IVC ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().setValue(pnd_Ws_Pnd_Acc_Int.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-INT-A := #ACC-INT ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().setValue(pnd_Ws_Pnd_Acc_Fed.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-FED-A := #ACC-FED ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().setValue(pnd_Ws_Pnd_Acc_Nra.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-NRA-A := #ACC-NRA ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().setValue(pnd_Ws_Pnd_Acc_State.getValue(3 + 1));                                                                //Natural: ASSIGN #T-STATE-A := #ACC-STATE ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().setValue(pnd_Ws_Pnd_Acc_Local.getValue(3 + 1));                                                                //Natural: ASSIGN #T-LOCAL-A := #ACC-LOCAL ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().setValue(pnd_Ws_Pnd_Acc_Can.getValue(3 + 1));                                                               //Natural: ASSIGN #T-CANADIAN-A := #ACC-CAN ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Comp_Code().setValue("X");                                                                                               //Natural: ASSIGN #C-COMP-CODE := 'X'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt().setValue(pnd_Ws_Pnd_Acc_Cnt.getValue(4 + 1));                                                                  //Natural: ASSIGN #C-REC-CNT := #ACC-CNT ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A().setValue(pnd_Ws_Pnd_Acc_Gross.getValue(4 + 1));                                                                //Natural: ASSIGN #C-GROSS-A := #ACC-GROSS ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A().setValue(pnd_Ws_Pnd_Acc_Ivc.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-IVC-A := #ACC-IVC ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A().setValue(pnd_Ws_Pnd_Acc_Int.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-INT-A := #ACC-INT ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A().setValue(pnd_Ws_Pnd_Acc_Fed.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-FED-A := #ACC-FED ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A().setValue(pnd_Ws_Pnd_Acc_Nra.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-NRA-A := #ACC-NRA ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A().setValue(pnd_Ws_Pnd_Acc_State.getValue(4 + 1));                                                                //Natural: ASSIGN #C-STATE-A := #ACC-STATE ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A().setValue(pnd_Ws_Pnd_Acc_Local.getValue(4 + 1));                                                                //Natural: ASSIGN #C-LOCAL-A := #ACC-LOCAL ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A().setValue(pnd_Ws_Pnd_Acc_Can.getValue(4 + 1));                                                               //Natural: ASSIGN #C-CANADIAN-A := #ACC-CAN ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Comp_Code().setValue("F");                                                                                               //Natural: ASSIGN #F-COMP-CODE := 'F'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt().setValue(pnd_Ws_Pnd_Acc_Cnt.getValue(5 + 1));                                                                  //Natural: ASSIGN #F-REC-CNT := #ACC-CNT ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A().setValue(pnd_Ws_Pnd_Acc_Gross.getValue(5 + 1));                                                                //Natural: ASSIGN #F-GROSS-A := #ACC-GROSS ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A().setValue(pnd_Ws_Pnd_Acc_Ivc.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-IVC-A := #ACC-IVC ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A().setValue(pnd_Ws_Pnd_Acc_Int.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-INT-A := #ACC-INT ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A().setValue(pnd_Ws_Pnd_Acc_Fed.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-FED-A := #ACC-FED ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A().setValue(pnd_Ws_Pnd_Acc_Nra.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-NRA-A := #ACC-NRA ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A().setValue(pnd_Ws_Pnd_Acc_State.getValue(5 + 1));                                                                //Natural: ASSIGN #F-STATE-A := #ACC-STATE ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A().setValue(pnd_Ws_Pnd_Acc_Local.getValue(5 + 1));                                                                //Natural: ASSIGN #F-LOCAL-A := #ACC-LOCAL ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A().setValue(pnd_Ws_Pnd_Acc_Can.getValue(5 + 1));                                                               //Natural: ASSIGN #F-CANADIAN-A := #ACC-CAN ( 5 )
            if (condition(pnd_Ws_Pnd_Acc_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #ACC-CNT ( * ) > 0
            {
                getWorkFiles().write(6, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                         //Natural: WRITE WORK FILE 06 #INP-SUMMARY
                //*  RCC
            }                                                                                                                                                             //Natural: END-IF
            //* **
            //*  PREPARE THE RECORD #INP-SUMMARY TO WRITE OUT
            //* **
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_Z().setValue("Z");                                                                                                         //Natural: ASSIGN #Z := 'Z'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Comp_Code().setValue("L");                                                                                               //Natural: ASSIGN #L-COMP-CODE := 'L'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Rec_Cnt().setValue(pnd_Ws_Pnd_Cit_Cnt.getValue(1 + 1));                                                                  //Natural: ASSIGN #L-REC-CNT := #CIT-CNT ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Gross_A().setValue(pnd_Ws_Pnd_Cit_Gross.getValue(1 + 1));                                                                //Natural: ASSIGN #L-GROSS-A := #CIT-GROSS ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Ivc_A().setValue(pnd_Ws_Pnd_Cit_Ivc.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-IVC-A := #CIT-IVC ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Int_A().setValue(pnd_Ws_Pnd_Cit_Int.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-INT-A := #CIT-INT ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Fed_A().setValue(pnd_Ws_Pnd_Cit_Fed.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-FED-A := #CIT-FED ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Nra_A().setValue(pnd_Ws_Pnd_Cit_Nra.getValue(1 + 1));                                                                    //Natural: ASSIGN #L-NRA-A := #CIT-NRA ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_State_A().setValue(pnd_Ws_Pnd_Cit_State.getValue(1 + 1));                                                                //Natural: ASSIGN #L-STATE-A := #CIT-STATE ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Local_A().setValue(pnd_Ws_Pnd_Cit_Local.getValue(1 + 1));                                                                //Natural: ASSIGN #L-LOCAL-A := #CIT-LOCAL ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_L_Canadian_A().setValue(pnd_Ws_Pnd_Cit_Can.getValue(1 + 1));                                                               //Natural: ASSIGN #L-CANADIAN-A := #CIT-CAN ( 1 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Comp_Code().setValue("S");                                                                                               //Natural: ASSIGN #S-COMP-CODE := 'S'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Rec_Cnt().setValue(pnd_Ws_Pnd_Cit_Cnt.getValue(2 + 1));                                                                  //Natural: ASSIGN #S-REC-CNT := #CIT-CNT ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Gross_A().setValue(pnd_Ws_Pnd_Cit_Gross.getValue(2 + 1));                                                                //Natural: ASSIGN #S-GROSS-A := #CIT-GROSS ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Ivc_A().setValue(pnd_Ws_Pnd_Cit_Ivc.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-IVC-A := #CIT-IVC ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Int_A().setValue(pnd_Ws_Pnd_Cit_Int.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-INT-A := #CIT-INT ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Fed_A().setValue(pnd_Ws_Pnd_Cit_Fed.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-FED-A := #CIT-FED ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Nra_A().setValue(pnd_Ws_Pnd_Cit_Nra.getValue(2 + 1));                                                                    //Natural: ASSIGN #S-NRA-A := #CIT-NRA ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_State_A().setValue(pnd_Ws_Pnd_Cit_State.getValue(2 + 1));                                                                //Natural: ASSIGN #S-STATE-A := #CIT-STATE ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Local_A().setValue(pnd_Ws_Pnd_Cit_Local.getValue(2 + 1));                                                                //Natural: ASSIGN #S-LOCAL-A := #CIT-LOCAL ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_S_Canadian_A().setValue(pnd_Ws_Pnd_Cit_Can.getValue(2 + 1));                                                               //Natural: ASSIGN #S-CANADIAN-A := #CIT-CAN ( 2 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Comp_Code().setValue("T");                                                                                               //Natural: ASSIGN #T-COMP-CODE := 'T'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Rec_Cnt().setValue(pnd_Ws_Pnd_Cit_Cnt.getValue(3 + 1));                                                                  //Natural: ASSIGN #T-REC-CNT := #CIT-CNT ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Gross_A().setValue(pnd_Ws_Pnd_Cit_Gross.getValue(3 + 1));                                                                //Natural: ASSIGN #T-GROSS-A := #CIT-GROSS ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Ivc_A().setValue(pnd_Ws_Pnd_Cit_Ivc.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-IVC-A := #CIT-IVC ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Int_A().setValue(pnd_Ws_Pnd_Cit_Int.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-INT-A := #CIT-INT ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Fed_A().setValue(pnd_Ws_Pnd_Cit_Fed.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-FED-A := #CIT-FED ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Nra_A().setValue(pnd_Ws_Pnd_Cit_Nra.getValue(3 + 1));                                                                    //Natural: ASSIGN #T-NRA-A := #CIT-NRA ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_State_A().setValue(pnd_Ws_Pnd_Cit_State.getValue(3 + 1));                                                                //Natural: ASSIGN #T-STATE-A := #CIT-STATE ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Local_A().setValue(pnd_Ws_Pnd_Cit_Local.getValue(3 + 1));                                                                //Natural: ASSIGN #T-LOCAL-A := #CIT-LOCAL ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_T_Canadian_A().setValue(pnd_Ws_Pnd_Cit_Can.getValue(3 + 1));                                                               //Natural: ASSIGN #T-CANADIAN-A := #CIT-CAN ( 3 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Comp_Code().setValue("X");                                                                                               //Natural: ASSIGN #C-COMP-CODE := 'X'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Rec_Cnt().setValue(pnd_Ws_Pnd_Cit_Cnt.getValue(4 + 1));                                                                  //Natural: ASSIGN #C-REC-CNT := #CIT-CNT ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Gross_A().setValue(pnd_Ws_Pnd_Cit_Gross.getValue(4 + 1));                                                                //Natural: ASSIGN #C-GROSS-A := #CIT-GROSS ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Ivc_A().setValue(pnd_Ws_Pnd_Cit_Ivc.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-IVC-A := #CIT-IVC ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Int_A().setValue(pnd_Ws_Pnd_Cit_Int.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-INT-A := #CIT-INT ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Fed_A().setValue(pnd_Ws_Pnd_Cit_Fed.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-FED-A := #CIT-FED ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Nra_A().setValue(pnd_Ws_Pnd_Cit_Nra.getValue(4 + 1));                                                                    //Natural: ASSIGN #C-NRA-A := #CIT-NRA ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_State_A().setValue(pnd_Ws_Pnd_Cit_State.getValue(4 + 1));                                                                //Natural: ASSIGN #C-STATE-A := #CIT-STATE ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Local_A().setValue(pnd_Ws_Pnd_Cit_Local.getValue(4 + 1));                                                                //Natural: ASSIGN #C-LOCAL-A := #CIT-LOCAL ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_C_Canadian_A().setValue(pnd_Ws_Pnd_Cit_Can.getValue(4 + 1));                                                               //Natural: ASSIGN #C-CANADIAN-A := #CIT-CAN ( 4 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Comp_Code().setValue("F");                                                                                               //Natural: ASSIGN #F-COMP-CODE := 'F'
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Rec_Cnt().setValue(pnd_Ws_Pnd_Cit_Cnt.getValue(5 + 1));                                                                  //Natural: ASSIGN #F-REC-CNT := #CIT-CNT ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Gross_A().setValue(pnd_Ws_Pnd_Cit_Gross.getValue(5 + 1));                                                                //Natural: ASSIGN #F-GROSS-A := #CIT-GROSS ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Ivc_A().setValue(pnd_Ws_Pnd_Cit_Ivc.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-IVC-A := #CIT-IVC ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Int_A().setValue(pnd_Ws_Pnd_Cit_Int.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-INT-A := #CIT-INT ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Fed_A().setValue(pnd_Ws_Pnd_Cit_Fed.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-FED-A := #CIT-FED ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Nra_A().setValue(pnd_Ws_Pnd_Cit_Nra.getValue(5 + 1));                                                                    //Natural: ASSIGN #F-NRA-A := #CIT-NRA ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_State_A().setValue(pnd_Ws_Pnd_Cit_State.getValue(5 + 1));                                                                //Natural: ASSIGN #F-STATE-A := #CIT-STATE ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Local_A().setValue(pnd_Ws_Pnd_Cit_Local.getValue(5 + 1));                                                                //Natural: ASSIGN #F-LOCAL-A := #CIT-LOCAL ( 5 )
            ldaTwrl0710.getPnd_Inp_Summary_Pnd_F_Canadian_A().setValue(pnd_Ws_Pnd_Cit_Can.getValue(5 + 1));                                                               //Natural: ASSIGN #F-CANADIAN-A := #CIT-CAN ( 5 )
            if (condition(pnd_Ws_Pnd_Cit_Cnt.getValue("*").greater(getZero())))                                                                                           //Natural: IF #CIT-CNT ( * ) > 0
            {
                getWorkFiles().write(7, false, ldaTwrl0710.getPnd_Inp_Summary());                                                                                         //Natural: WRITE WORK FILE 07 #INP-SUMMARY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Acc_Grp.getValue("*").reset();                                                                                                                     //Natural: RESET #ACC-GRP ( * ) #REJ-GRP ( * ) #CIT-GRP ( * )
            pnd_Ws_Pnd_Rej_Grp.getValue("*").reset();
            pnd_Ws_Pnd_Cit_Grp.getValue("*").reset();
            pnd_Ws_Pnd_Source_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #SOURCE-BREAK := TRUE
            //*  END OF SOURCE CODE BREAK PROCESSING
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=61");
        Global.format(3, "LS=133 PS=61");
        Global.format(4, "LS=133 PS=61");
        Global.format(5, "LS=133 PS=61");
        Global.format(6, "LS=133 PS=61");
        Global.format(7, "LS=133 PS=61");

        getReports().setDisplayColumns(0, "PPCN",
        		pnd_Iaa_Contract,"SRC",
        		ldaTwrl0700.getTwrt_Record_Twrt_Source(),"CNTR TYP",
        		ldaTwrl0700.getTwrt_Record_Twrt_Contract_Type(),"ORG",
        		pnd_Oc_N_Pnd_Oc_A,"CTZN",
        		ldaTwrl0700.getTwrt_Record_Twrt_Citizenship(),"DIST",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distribution_Code(),"DIST1",
        		ldaTwrl0700.getTwrt_Record_Twrt_Distrib_Cde(),"1078",
        		ldaTwrl0700.getTwrt_Record_Twrt_Pymnt_Tax_Form_1078(),"PAYTYP",
        		ldaTwrl0700.getTwrt_Record_Twrt_Paymt_Type(),"SETTLE",
        		ldaTwrl0700.getTwrt_Record_Twrt_Settl_Type());
    }
    private void CheckAtStartofData1303() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-RECORD
            sub_Get_Control_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EDIT-RUN-DATE
            sub_Edit_Run_Date();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-COMPANY
            sub_Load_Company();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-SOURCE-TABLE
            sub_Load_Source_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-DIST-CODE-TABLE
            sub_Load_Dist_Code_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-TABLE
            sub_Load_State_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-COUNTRY-TABLE
            sub_Load_Country_Table();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Comp_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #WS.#COMP-BREAK := #WS.#SOURCE-BREAK := TRUE
            pnd_Ws_Pnd_Source_Break.setValue(true);
        }                                                                                                                                                                 //Natural: END-START
    }
}
