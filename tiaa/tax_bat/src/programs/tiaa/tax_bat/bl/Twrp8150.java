/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:42 PM
**        * FROM NATURAL PROGRAM : Twrp8150
************************************************************
**        * FILE NAME            : Twrp8150.java
**        * CLASS NAME           : Twrp8150
**        * INSTANCE NAME        : Twrp8150
************************************************************
************************************************************************
* PROGRAM     :  TWRP8150
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE A TABLE FOR NEW TAX YEAR COUNTRY CODES, USING
*                AN OLD TAX YEAR COUNTRY CODES TABLE.
*                TABLE NUMBER 3
* DATE        :  09/15/2000
*
************************************************************************
*  CHANGE  LOG
* ----------------------------------------------------------------------
* 02/11/05 MS  FIXED PRODUCTION PROBLEM PROPAGATING THE TABLE.
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8150 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rc;
    private DbsField rc_Tircntl_Tbl_Nbr;
    private DbsField rc_Tircntl_Tax_Year;
    private DbsField rc_Tircntl_Seq_Nbr;

    private DbsGroup rc_Tircntl_Country_Code_Tbl;
    private DbsField rc_Tircntl_Cntry_Alpha_Code;
    private DbsField rc_Tircntl_Cntry_Abbr;
    private DbsField rc_Tircntl_Cntry_Full_Name;
    private DbsField rc_Tircntl_Cntry_Treaty_Ind;
    private DbsField rc_Tircntl_Cntry_Tax_Period;
    private DbsField rc_Tircntl_Cntry_Tax_Non_Period;
    private DbsField rc_Tircntl_Cntry_Tax_Dpi;
    private DbsField rc_Tircntl_Cntry_Tax_Rtb;
    private DbsField rc_Tircntl_Cntry_Active_Cde;
    private DbsField rc_Tircntl_Cntry_Compl_Dte;
    private DbsField rc_Tircntl_Cntry_Start_Dte;
    private DbsField rc_Tircntl_Cntry_Updte_Dte;
    private DbsField rc_Tircntl_Cntry_Comment;
    private DbsField rc_Tircntl_Cntry_Updte_User;
    private DbsField rc_Tircntl_Cntry_Unique_Cde;
    private DbsField rc_Tircntl_Cntry_Pa_Pp;
    private DbsField rc_Tircntl_Cntry_Pa_Np;
    private DbsField rc_Tircntl_Cntry_Ira_Pp;
    private DbsField rc_Tircntl_Cntry_Ira_Np;
    private DbsField rc_Tircntl_Cntry_Early_Distrib_Ind;
    private DbsField rc_Tircntl_Cntry_Dpi_Np_Sp_Ind;
    private DbsField rc_Tircntl_Cntry_Dpi_Np_Sp_Rate;
    private DbsField rc_Tircntl_Cntry_Form_Name;

    private DataAccessProgramView vw_uc_View;
    private DbsField uc_View_Tircntl_Tbl_Nbr;
    private DbsField uc_View_Tircntl_Tax_Year;
    private DbsField uc_View_Tircntl_Seq_Nbr;

    private DbsGroup uc_View_Tircntl_Country_Code_Tbl;
    private DbsField uc_View_Tircntl_Cntry_Alpha_Code;
    private DbsField uc_View_Tircntl_Cntry_Abbr;
    private DbsField uc_View_Tircntl_Cntry_Full_Name;
    private DbsField uc_View_Tircntl_Cntry_Treaty_Ind;
    private DbsField uc_View_Tircntl_Cntry_Tax_Period;
    private DbsField uc_View_Tircntl_Cntry_Tax_Non_Period;
    private DbsField uc_View_Tircntl_Cntry_Tax_Dpi;
    private DbsField uc_View_Tircntl_Cntry_Tax_Rtb;
    private DbsField uc_View_Tircntl_Cntry_Active_Cde;
    private DbsField uc_View_Tircntl_Cntry_Compl_Dte;
    private DbsField uc_View_Tircntl_Cntry_Start_Dte;
    private DbsField uc_View_Tircntl_Cntry_Updte_Dte;
    private DbsField uc_View_Tircntl_Cntry_Comment;
    private DbsField uc_View_Tircntl_Cntry_Updte_User;
    private DbsField uc_View_Tircntl_Cntry_Unique_Cde;
    private DbsField uc_View_Tircntl_Cntry_Pa_Pp;
    private DbsField uc_View_Tircntl_Cntry_Pa_Np;
    private DbsField uc_View_Tircntl_Cntry_Ira_Pp;
    private DbsField uc_View_Tircntl_Cntry_Ira_Np;
    private DbsField uc_View_Tircntl_Cntry_Early_Distrib_Ind;
    private DbsField uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind;
    private DbsField uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate;
    private DbsField uc_View_Tircntl_Cntry_Form_Name;

    private DataAccessProgramView vw_sc;
    private DbsField sc_Tircntl_Tbl_Nbr;
    private DbsField sc_Tircntl_Tax_Year;
    private DbsField sc_Tircntl_Seq_Nbr;

    private DbsGroup sc_Tircntl_Country_Code_Tbl;
    private DbsField sc_Tircntl_Cntry_Alpha_Code;
    private DbsField sc_Tircntl_Cntry_Abbr;
    private DbsField sc_Tircntl_Cntry_Full_Name;
    private DbsField sc_Tircntl_Cntry_Treaty_Ind;
    private DbsField sc_Tircntl_Cntry_Tax_Period;
    private DbsField sc_Tircntl_Cntry_Tax_Non_Period;
    private DbsField sc_Tircntl_Cntry_Tax_Dpi;
    private DbsField sc_Tircntl_Cntry_Tax_Rtb;
    private DbsField sc_Tircntl_Cntry_Active_Cde;
    private DbsField sc_Tircntl_Cntry_Compl_Dte;
    private DbsField sc_Tircntl_Cntry_Start_Dte;
    private DbsField sc_Tircntl_Cntry_Updte_Dte;
    private DbsField sc_Tircntl_Cntry_Comment;
    private DbsField sc_Tircntl_Cntry_Updte_User;
    private DbsField sc_Tircntl_Cntry_Unique_Cde;
    private DbsField sc_Tircntl_Cntry_Pa_Pp;
    private DbsField sc_Tircntl_Cntry_Pa_Np;
    private DbsField sc_Tircntl_Cntry_Ira_Pp;
    private DbsField sc_Tircntl_Cntry_Ira_Np;
    private DbsField sc_Tircntl_Cntry_Early_Distrib_Ind;
    private DbsField sc_Tircntl_Cntry_Dpi_Np_Sp_Ind;
    private DbsField sc_Tircntl_Cntry_Dpi_Np_Sp_Rate;
    private DbsField sc_Tircntl_Cntry_Form_Name;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Unique_Cde;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Unique_Cde;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rc = new DataAccessProgramView(new NameInfo("vw_rc", "RC"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        rc_Tircntl_Tbl_Nbr = vw_rc.getRecord().newFieldInGroup("rc_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rc_Tircntl_Tax_Year = vw_rc.getRecord().newFieldInGroup("rc_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rc_Tircntl_Seq_Nbr = vw_rc.getRecord().newFieldInGroup("rc_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rc_Tircntl_Country_Code_Tbl = vw_rc.getRecord().newGroupInGroup("RC_TIRCNTL_COUNTRY_CODE_TBL", "TIRCNTL-COUNTRY-CODE-TBL");
        rc_Tircntl_Cntry_Alpha_Code = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Alpha_Code", "TIRCNTL-CNTRY-ALPHA-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ALPHA_CODE");
        rc_Tircntl_Cntry_Abbr = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Abbr", "TIRCNTL-CNTRY-ABBR", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_CNTRY_ABBR");
        rc_Tircntl_Cntry_Full_Name = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Full_Name", "TIRCNTL-CNTRY-FULL-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FULL_NAME");
        rc_Tircntl_Cntry_Treaty_Ind = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Treaty_Ind", "TIRCNTL-CNTRY-TREATY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TREATY_IND");
        rc_Tircntl_Cntry_Tax_Period = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Tax_Period", "TIRCNTL-CNTRY-TAX-PERIOD", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_PERIOD");
        rc_Tircntl_Cntry_Tax_Non_Period = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Tax_Non_Period", "TIRCNTL-CNTRY-TAX-NON-PERIOD", 
            FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_NON_PERIOD");
        rc_Tircntl_Cntry_Tax_Dpi = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Tax_Dpi", "TIRCNTL-CNTRY-TAX-DPI", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_DPI");
        rc_Tircntl_Cntry_Tax_Rtb = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Tax_Rtb", "TIRCNTL-CNTRY-TAX-RTB", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_RTB");
        rc_Tircntl_Cntry_Active_Cde = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Active_Cde", "TIRCNTL-CNTRY-ACTIVE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ACTIVE_CDE");
        rc_Tircntl_Cntry_Compl_Dte = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Compl_Dte", "TIRCNTL-CNTRY-COMPL-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMPL_DTE");
        rc_Tircntl_Cntry_Start_Dte = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Start_Dte", "TIRCNTL-CNTRY-START-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_START_DTE");
        rc_Tircntl_Cntry_Updte_Dte = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Updte_Dte", "TIRCNTL-CNTRY-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_DTE");
        rc_Tircntl_Cntry_Comment = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Comment", "TIRCNTL-CNTRY-COMMENT", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMMENT");
        rc_Tircntl_Cntry_Updte_User = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Updte_User", "TIRCNTL-CNTRY-UPDTE-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_USER");
        rc_Tircntl_Cntry_Unique_Cde = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Unique_Cde", "TIRCNTL-CNTRY-UNIQUE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UNIQUE_CDE");
        rc_Tircntl_Cntry_Unique_Cde.setDdmHeader("UNQ/CNTR/CODE");
        rc_Tircntl_Cntry_Pa_Pp = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Pa_Pp", "TIRCNTL-CNTRY-PA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_PP");
        rc_Tircntl_Cntry_Pa_Pp.setDdmHeader("PA/PP/RATE");
        rc_Tircntl_Cntry_Pa_Np = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Pa_Np", "TIRCNTL-CNTRY-PA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_NP");
        rc_Tircntl_Cntry_Pa_Np.setDdmHeader("PA/NP/RATE");
        rc_Tircntl_Cntry_Ira_Pp = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Ira_Pp", "TIRCNTL-CNTRY-IRA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_PP");
        rc_Tircntl_Cntry_Ira_Pp.setDdmHeader("IRA/PP/RATE");
        rc_Tircntl_Cntry_Ira_Np = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Ira_Np", "TIRCNTL-CNTRY-IRA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_NP");
        rc_Tircntl_Cntry_Ira_Np.setDdmHeader("IRA/NP/RATE");
        rc_Tircntl_Cntry_Early_Distrib_Ind = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Early_Distrib_Ind", "TIRCNTL-CNTRY-EARLY-DISTRIB-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_EARLY_DISTRIB_IND");
        rc_Tircntl_Cntry_Early_Distrib_Ind.setDdmHeader("EARLY/DISTR/IND");
        rc_Tircntl_Cntry_Dpi_Np_Sp_Ind = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Dpi_Np_Sp_Ind", "TIRCNTL-CNTRY-DPI-NP-SP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_IND");
        rc_Tircntl_Cntry_Dpi_Np_Sp_Ind.setDdmHeader("DPI NP/SPECIAL/IND");
        rc_Tircntl_Cntry_Dpi_Np_Sp_Rate = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Dpi_Np_Sp_Rate", "TIRCNTL-CNTRY-DPI-NP-SP-RATE", 
            FieldType.PACKED_DECIMAL, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_RATE");
        rc_Tircntl_Cntry_Dpi_Np_Sp_Rate.setDdmHeader("DPI NP/SPECIAL/RATE");
        rc_Tircntl_Cntry_Form_Name = rc_Tircntl_Country_Code_Tbl.newFieldInGroup("rc_Tircntl_Cntry_Form_Name", "TIRCNTL-CNTRY-FORM-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FORM_NAME");
        rc_Tircntl_Cntry_Form_Name.setDdmHeader("COUNTRY/FORM/NAME");
        registerRecord(vw_rc);

        vw_uc_View = new DataAccessProgramView(new NameInfo("vw_uc_View", "UC-VIEW"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        uc_View_Tircntl_Tbl_Nbr = vw_uc_View.getRecord().newFieldInGroup("uc_View_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        uc_View_Tircntl_Tax_Year = vw_uc_View.getRecord().newFieldInGroup("uc_View_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        uc_View_Tircntl_Seq_Nbr = vw_uc_View.getRecord().newFieldInGroup("uc_View_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        uc_View_Tircntl_Country_Code_Tbl = vw_uc_View.getRecord().newGroupInGroup("UC_VIEW_TIRCNTL_COUNTRY_CODE_TBL", "TIRCNTL-COUNTRY-CODE-TBL");
        uc_View_Tircntl_Cntry_Alpha_Code = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Alpha_Code", "TIRCNTL-CNTRY-ALPHA-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ALPHA_CODE");
        uc_View_Tircntl_Cntry_Abbr = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Abbr", "TIRCNTL-CNTRY-ABBR", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ABBR");
        uc_View_Tircntl_Cntry_Full_Name = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Full_Name", "TIRCNTL-CNTRY-FULL-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FULL_NAME");
        uc_View_Tircntl_Cntry_Treaty_Ind = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Treaty_Ind", "TIRCNTL-CNTRY-TREATY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TREATY_IND");
        uc_View_Tircntl_Cntry_Tax_Period = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Tax_Period", "TIRCNTL-CNTRY-TAX-PERIOD", 
            FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_PERIOD");
        uc_View_Tircntl_Cntry_Tax_Non_Period = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Tax_Non_Period", "TIRCNTL-CNTRY-TAX-NON-PERIOD", 
            FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_NON_PERIOD");
        uc_View_Tircntl_Cntry_Tax_Dpi = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Tax_Dpi", "TIRCNTL-CNTRY-TAX-DPI", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_DPI");
        uc_View_Tircntl_Cntry_Tax_Rtb = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Tax_Rtb", "TIRCNTL-CNTRY-TAX-RTB", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_RTB");
        uc_View_Tircntl_Cntry_Active_Cde = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Active_Cde", "TIRCNTL-CNTRY-ACTIVE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ACTIVE_CDE");
        uc_View_Tircntl_Cntry_Compl_Dte = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Compl_Dte", "TIRCNTL-CNTRY-COMPL-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMPL_DTE");
        uc_View_Tircntl_Cntry_Start_Dte = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Start_Dte", "TIRCNTL-CNTRY-START-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_START_DTE");
        uc_View_Tircntl_Cntry_Updte_Dte = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Updte_Dte", "TIRCNTL-CNTRY-UPDTE-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_DTE");
        uc_View_Tircntl_Cntry_Comment = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Comment", "TIRCNTL-CNTRY-COMMENT", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMMENT");
        uc_View_Tircntl_Cntry_Updte_User = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Updte_User", "TIRCNTL-CNTRY-UPDTE-USER", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_USER");
        uc_View_Tircntl_Cntry_Unique_Cde = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Unique_Cde", "TIRCNTL-CNTRY-UNIQUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UNIQUE_CDE");
        uc_View_Tircntl_Cntry_Unique_Cde.setDdmHeader("UNQ/CNTR/CODE");
        uc_View_Tircntl_Cntry_Pa_Pp = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Pa_Pp", "TIRCNTL-CNTRY-PA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_PP");
        uc_View_Tircntl_Cntry_Pa_Pp.setDdmHeader("PA/PP/RATE");
        uc_View_Tircntl_Cntry_Pa_Np = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Pa_Np", "TIRCNTL-CNTRY-PA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_NP");
        uc_View_Tircntl_Cntry_Pa_Np.setDdmHeader("PA/NP/RATE");
        uc_View_Tircntl_Cntry_Ira_Pp = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Ira_Pp", "TIRCNTL-CNTRY-IRA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_PP");
        uc_View_Tircntl_Cntry_Ira_Pp.setDdmHeader("IRA/PP/RATE");
        uc_View_Tircntl_Cntry_Ira_Np = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Ira_Np", "TIRCNTL-CNTRY-IRA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_NP");
        uc_View_Tircntl_Cntry_Ira_Np.setDdmHeader("IRA/NP/RATE");
        uc_View_Tircntl_Cntry_Early_Distrib_Ind = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Early_Distrib_Ind", "TIRCNTL-CNTRY-EARLY-DISTRIB-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_EARLY_DISTRIB_IND");
        uc_View_Tircntl_Cntry_Early_Distrib_Ind.setDdmHeader("EARLY/DISTR/IND");
        uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind", "TIRCNTL-CNTRY-DPI-NP-SP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_IND");
        uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind.setDdmHeader("DPI NP/SPECIAL/IND");
        uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate", "TIRCNTL-CNTRY-DPI-NP-SP-RATE", 
            FieldType.PACKED_DECIMAL, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_RATE");
        uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate.setDdmHeader("DPI NP/SPECIAL/RATE");
        uc_View_Tircntl_Cntry_Form_Name = uc_View_Tircntl_Country_Code_Tbl.newFieldInGroup("uc_View_Tircntl_Cntry_Form_Name", "TIRCNTL-CNTRY-FORM-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FORM_NAME");
        uc_View_Tircntl_Cntry_Form_Name.setDdmHeader("COUNTRY/FORM/NAME");
        registerRecord(vw_uc_View);

        vw_sc = new DataAccessProgramView(new NameInfo("vw_sc", "SC"), "TIRCNTL_COUNTRY_CODE_TBL_VIEW", "TIR_CONTROL");
        sc_Tircntl_Tbl_Nbr = vw_sc.getRecord().newFieldInGroup("sc_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        sc_Tircntl_Tax_Year = vw_sc.getRecord().newFieldInGroup("sc_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        sc_Tircntl_Seq_Nbr = vw_sc.getRecord().newFieldInGroup("sc_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        sc_Tircntl_Country_Code_Tbl = vw_sc.getRecord().newGroupInGroup("SC_TIRCNTL_COUNTRY_CODE_TBL", "TIRCNTL-COUNTRY-CODE-TBL");
        sc_Tircntl_Cntry_Alpha_Code = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Alpha_Code", "TIRCNTL-CNTRY-ALPHA-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ALPHA_CODE");
        sc_Tircntl_Cntry_Abbr = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Abbr", "TIRCNTL-CNTRY-ABBR", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_CNTRY_ABBR");
        sc_Tircntl_Cntry_Full_Name = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Full_Name", "TIRCNTL-CNTRY-FULL-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FULL_NAME");
        sc_Tircntl_Cntry_Treaty_Ind = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Treaty_Ind", "TIRCNTL-CNTRY-TREATY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TREATY_IND");
        sc_Tircntl_Cntry_Tax_Period = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Tax_Period", "TIRCNTL-CNTRY-TAX-PERIOD", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_PERIOD");
        sc_Tircntl_Cntry_Tax_Non_Period = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Tax_Non_Period", "TIRCNTL-CNTRY-TAX-NON-PERIOD", 
            FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_NON_PERIOD");
        sc_Tircntl_Cntry_Tax_Dpi = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Tax_Dpi", "TIRCNTL-CNTRY-TAX-DPI", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_DPI");
        sc_Tircntl_Cntry_Tax_Rtb = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Tax_Rtb", "TIRCNTL-CNTRY-TAX-RTB", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_TAX_RTB");
        sc_Tircntl_Cntry_Active_Cde = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Active_Cde", "TIRCNTL-CNTRY-ACTIVE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_ACTIVE_CDE");
        sc_Tircntl_Cntry_Compl_Dte = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Compl_Dte", "TIRCNTL-CNTRY-COMPL-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMPL_DTE");
        sc_Tircntl_Cntry_Start_Dte = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Start_Dte", "TIRCNTL-CNTRY-START-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_START_DTE");
        sc_Tircntl_Cntry_Updte_Dte = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Updte_Dte", "TIRCNTL-CNTRY-UPDTE-DTE", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_DTE");
        sc_Tircntl_Cntry_Comment = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Comment", "TIRCNTL-CNTRY-COMMENT", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_COMMENT");
        sc_Tircntl_Cntry_Updte_User = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Updte_User", "TIRCNTL-CNTRY-UPDTE-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UPDTE_USER");
        sc_Tircntl_Cntry_Unique_Cde = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Unique_Cde", "TIRCNTL-CNTRY-UNIQUE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_UNIQUE_CDE");
        sc_Tircntl_Cntry_Unique_Cde.setDdmHeader("UNQ/CNTR/CODE");
        sc_Tircntl_Cntry_Pa_Pp = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Pa_Pp", "TIRCNTL-CNTRY-PA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_PP");
        sc_Tircntl_Cntry_Pa_Pp.setDdmHeader("PA/PP/RATE");
        sc_Tircntl_Cntry_Pa_Np = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Pa_Np", "TIRCNTL-CNTRY-PA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_PA_NP");
        sc_Tircntl_Cntry_Pa_Np.setDdmHeader("PA/NP/RATE");
        sc_Tircntl_Cntry_Ira_Pp = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Ira_Pp", "TIRCNTL-CNTRY-IRA-PP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_PP");
        sc_Tircntl_Cntry_Ira_Pp.setDdmHeader("IRA/PP/RATE");
        sc_Tircntl_Cntry_Ira_Np = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Ira_Np", "TIRCNTL-CNTRY-IRA-NP", FieldType.PACKED_DECIMAL, 
            5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_IRA_NP");
        sc_Tircntl_Cntry_Ira_Np.setDdmHeader("IRA/NP/RATE");
        sc_Tircntl_Cntry_Early_Distrib_Ind = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Early_Distrib_Ind", "TIRCNTL-CNTRY-EARLY-DISTRIB-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_EARLY_DISTRIB_IND");
        sc_Tircntl_Cntry_Early_Distrib_Ind.setDdmHeader("EARLY/DISTR/IND");
        sc_Tircntl_Cntry_Dpi_Np_Sp_Ind = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Dpi_Np_Sp_Ind", "TIRCNTL-CNTRY-DPI-NP-SP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_IND");
        sc_Tircntl_Cntry_Dpi_Np_Sp_Ind.setDdmHeader("DPI NP/SPECIAL/IND");
        sc_Tircntl_Cntry_Dpi_Np_Sp_Rate = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Dpi_Np_Sp_Rate", "TIRCNTL-CNTRY-DPI-NP-SP-RATE", 
            FieldType.PACKED_DECIMAL, 5, 3, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_DPI_NP_SP_RATE");
        sc_Tircntl_Cntry_Dpi_Np_Sp_Rate.setDdmHeader("DPI NP/SPECIAL/RATE");
        sc_Tircntl_Cntry_Form_Name = sc_Tircntl_Country_Code_Tbl.newFieldInGroup("sc_Tircntl_Cntry_Form_Name", "TIRCNTL-CNTRY-FORM-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TIRCNTL_CNTRY_FORM_NAME");
        sc_Tircntl_Cntry_Form_Name.setDdmHeader("COUNTRY/FORM/NAME");
        registerRecord(vw_sc);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.NUMERIC, 8);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Unique_Cde = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Unique_Cde", "#S1-UNIQUE-CDE", FieldType.STRING, 3);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 8);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Unique_Cde = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Unique_Cde", "#S2-UNIQUE-CDE", FieldType.STRING, 3);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rc.reset();
        vw_uc_View.reset();
        vw_sc.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8150() throws Exception
    {
        super("Twrp8150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8150|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(3);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 3
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Unique_Cde.setValue(" ");                                                                                                               //Natural: ASSIGN #S1-UNIQUE-CDE := ' '
                vw_rc.startDatabaseRead                                                                                                                                   //Natural: READ RC WITH TIRCNTL-NBR-YEAR-UNQ-CDE = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_UNQ_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_UNQ_CDE", "ASC") }
                );
                READ01:
                while (condition(vw_rc.readNextRow("READ01")))
                {
                    if (condition(rc_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rc_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                        //Natural: IF RC.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RC.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(3);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 3
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Unique_Cde.setValue(rc_Tircntl_Cntry_Unique_Cde);                                                                                   //Natural: ASSIGN #S2-UNIQUE-CDE := RC.TIRCNTL-CNTRY-UNIQUE-CDE
                    vw_uc_View.startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) UC-VIEW WITH TIRCNTL-NBR-YEAR-UNQ-CDE = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_UNQ_CDE", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_UNQ_CDE", "ASC") },
                    1
                    );
                    RL2:
                    while (condition(vw_uc_View.readNextRow("RL2")))
                    {
                        if (condition(uc_View_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && uc_View_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year)            //Natural: IF UC-VIEW.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UC-VIEW.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UC-VIEW.TIRCNTL-CNTRY-UNIQUE-CDE = #S2-UNIQUE-CDE
                            && uc_View_Tircntl_Cntry_Unique_Cde.equals(pnd_Super2_Pnd_S2_Unique_Cde)))
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(uc_View_Tircntl_Cntry_Alpha_Code.equals(rc_Tircntl_Cntry_Alpha_Code) && uc_View_Tircntl_Cntry_Abbr.equals(rc_Tircntl_Cntry_Abbr)    //Natural: IF UC-VIEW.TIRCNTL-CNTRY-ALPHA-CODE = RC.TIRCNTL-CNTRY-ALPHA-CODE AND UC-VIEW.TIRCNTL-CNTRY-ABBR = RC.TIRCNTL-CNTRY-ABBR AND UC-VIEW.TIRCNTL-CNTRY-FULL-NAME = RC.TIRCNTL-CNTRY-FULL-NAME AND UC-VIEW.TIRCNTL-CNTRY-TREATY-IND = RC.TIRCNTL-CNTRY-TREATY-IND AND UC-VIEW.TIRCNTL-CNTRY-TAX-PERIOD = RC.TIRCNTL-CNTRY-TAX-PERIOD AND UC-VIEW.TIRCNTL-CNTRY-TAX-DPI = RC.TIRCNTL-CNTRY-TAX-DPI AND UC-VIEW.TIRCNTL-CNTRY-TAX-RTB = RC.TIRCNTL-CNTRY-TAX-RTB AND UC-VIEW.TIRCNTL-CNTRY-ACTIVE-CDE = RC.TIRCNTL-CNTRY-ACTIVE-CDE AND UC-VIEW.TIRCNTL-CNTRY-COMPL-DTE = RC.TIRCNTL-CNTRY-COMPL-DTE AND UC-VIEW.TIRCNTL-CNTRY-START-DTE = RC.TIRCNTL-CNTRY-START-DTE AND UC-VIEW.TIRCNTL-CNTRY-UPDTE-DTE = RC.TIRCNTL-CNTRY-UPDTE-DTE AND UC-VIEW.TIRCNTL-CNTRY-COMMENT = RC.TIRCNTL-CNTRY-COMMENT AND UC-VIEW.TIRCNTL-CNTRY-UPDTE-USER = RC.TIRCNTL-CNTRY-UPDTE-USER AND UC-VIEW.TIRCNTL-CNTRY-UNIQUE-CDE = RC.TIRCNTL-CNTRY-UNIQUE-CDE AND UC-VIEW.TIRCNTL-CNTRY-PA-PP = RC.TIRCNTL-CNTRY-PA-PP AND UC-VIEW.TIRCNTL-CNTRY-PA-NP = RC.TIRCNTL-CNTRY-PA-NP AND UC-VIEW.TIRCNTL-CNTRY-IRA-PP = RC.TIRCNTL-CNTRY-IRA-PP AND UC-VIEW.TIRCNTL-CNTRY-IRA-NP = RC.TIRCNTL-CNTRY-IRA-NP AND UC-VIEW.TIRCNTL-CNTRY-FORM-NAME = RC.TIRCNTL-CNTRY-FORM-NAME AND UC-VIEW.TIRCNTL-CNTRY-DPI-NP-SP-IND = RC.TIRCNTL-CNTRY-DPI-NP-SP-IND AND UC-VIEW.TIRCNTL-CNTRY-DPI-NP-SP-RATE = RC.TIRCNTL-CNTRY-DPI-NP-SP-RATE AND UC-VIEW.TIRCNTL-CNTRY-TAX-NON-PERIOD = RC.TIRCNTL-CNTRY-TAX-NON-PERIOD AND UC-VIEW.TIRCNTL-CNTRY-EARLY-DISTRIB-IND = RC.TIRCNTL-CNTRY-EARLY-DISTRIB-IND
                            && uc_View_Tircntl_Cntry_Full_Name.equals(rc_Tircntl_Cntry_Full_Name) && uc_View_Tircntl_Cntry_Treaty_Ind.equals(rc_Tircntl_Cntry_Treaty_Ind) 
                            && uc_View_Tircntl_Cntry_Tax_Period.equals(rc_Tircntl_Cntry_Tax_Period) && uc_View_Tircntl_Cntry_Tax_Dpi.equals(rc_Tircntl_Cntry_Tax_Dpi) 
                            && uc_View_Tircntl_Cntry_Tax_Rtb.equals(rc_Tircntl_Cntry_Tax_Rtb) && uc_View_Tircntl_Cntry_Active_Cde.equals(rc_Tircntl_Cntry_Active_Cde) 
                            && uc_View_Tircntl_Cntry_Compl_Dte.equals(rc_Tircntl_Cntry_Compl_Dte) && uc_View_Tircntl_Cntry_Start_Dte.equals(rc_Tircntl_Cntry_Start_Dte) 
                            && uc_View_Tircntl_Cntry_Updte_Dte.equals(rc_Tircntl_Cntry_Updte_Dte) && uc_View_Tircntl_Cntry_Comment.equals(rc_Tircntl_Cntry_Comment) 
                            && uc_View_Tircntl_Cntry_Updte_User.equals(rc_Tircntl_Cntry_Updte_User) && uc_View_Tircntl_Cntry_Unique_Cde.equals(rc_Tircntl_Cntry_Unique_Cde) 
                            && uc_View_Tircntl_Cntry_Pa_Pp.equals(rc_Tircntl_Cntry_Pa_Pp) && uc_View_Tircntl_Cntry_Pa_Np.equals(rc_Tircntl_Cntry_Pa_Np) 
                            && uc_View_Tircntl_Cntry_Ira_Pp.equals(rc_Tircntl_Cntry_Ira_Pp) && uc_View_Tircntl_Cntry_Ira_Np.equals(rc_Tircntl_Cntry_Ira_Np) 
                            && uc_View_Tircntl_Cntry_Form_Name.equals(rc_Tircntl_Cntry_Form_Name) && uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind.equals(rc_Tircntl_Cntry_Dpi_Np_Sp_Ind) 
                            && uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate.equals(rc_Tircntl_Cntry_Dpi_Np_Sp_Rate) && uc_View_Tircntl_Cntry_Tax_Non_Period.equals(rc_Tircntl_Cntry_Tax_Non_Period) 
                            && uc_View_Tircntl_Cntry_Early_Distrib_Ind.equals(rc_Tircntl_Cntry_Early_Distrib_Ind)))
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET UC-VIEW *ISN ( RL2. )
                            vw_uc_View.readByID(vw_uc_View.getAstISN("RL2"), "G1");
                            uc_View_Tircntl_Cntry_Alpha_Code.setValue(rc_Tircntl_Cntry_Alpha_Code);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-ALPHA-CODE := RC.TIRCNTL-CNTRY-ALPHA-CODE
                            uc_View_Tircntl_Cntry_Abbr.setValue(rc_Tircntl_Cntry_Abbr);                                                                                   //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-ABBR := RC.TIRCNTL-CNTRY-ABBR
                            uc_View_Tircntl_Cntry_Full_Name.setValue(rc_Tircntl_Cntry_Full_Name);                                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-FULL-NAME := RC.TIRCNTL-CNTRY-FULL-NAME
                            uc_View_Tircntl_Cntry_Treaty_Ind.setValue(rc_Tircntl_Cntry_Treaty_Ind);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-TREATY-IND := RC.TIRCNTL-CNTRY-TREATY-IND
                            uc_View_Tircntl_Cntry_Tax_Period.setValue(rc_Tircntl_Cntry_Tax_Period);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-TAX-PERIOD := RC.TIRCNTL-CNTRY-TAX-PERIOD
                            uc_View_Tircntl_Cntry_Tax_Dpi.setValue(rc_Tircntl_Cntry_Tax_Dpi);                                                                             //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-TAX-DPI := RC.TIRCNTL-CNTRY-TAX-DPI
                            uc_View_Tircntl_Cntry_Tax_Rtb.setValue(rc_Tircntl_Cntry_Tax_Rtb);                                                                             //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-TAX-RTB := RC.TIRCNTL-CNTRY-TAX-RTB
                            uc_View_Tircntl_Cntry_Active_Cde.setValue(rc_Tircntl_Cntry_Active_Cde);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-ACTIVE-CDE := RC.TIRCNTL-CNTRY-ACTIVE-CDE
                            uc_View_Tircntl_Cntry_Compl_Dte.setValue(rc_Tircntl_Cntry_Compl_Dte);                                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-COMPL-DTE := RC.TIRCNTL-CNTRY-COMPL-DTE
                            uc_View_Tircntl_Cntry_Start_Dte.setValue(rc_Tircntl_Cntry_Start_Dte);                                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-START-DTE := RC.TIRCNTL-CNTRY-START-DTE
                            uc_View_Tircntl_Cntry_Updte_Dte.setValue(rc_Tircntl_Cntry_Updte_Dte);                                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-UPDTE-DTE := RC.TIRCNTL-CNTRY-UPDTE-DTE
                            uc_View_Tircntl_Cntry_Comment.setValue(rc_Tircntl_Cntry_Comment);                                                                             //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-COMMENT := RC.TIRCNTL-CNTRY-COMMENT
                            uc_View_Tircntl_Cntry_Updte_User.setValue(rc_Tircntl_Cntry_Updte_User);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-UPDTE-USER := RC.TIRCNTL-CNTRY-UPDTE-USER
                            uc_View_Tircntl_Cntry_Unique_Cde.setValue(rc_Tircntl_Cntry_Unique_Cde);                                                                       //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-UNIQUE-CDE := RC.TIRCNTL-CNTRY-UNIQUE-CDE
                            uc_View_Tircntl_Cntry_Pa_Pp.setValue(rc_Tircntl_Cntry_Pa_Pp);                                                                                 //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-PA-PP := RC.TIRCNTL-CNTRY-PA-PP
                            uc_View_Tircntl_Cntry_Pa_Np.setValue(rc_Tircntl_Cntry_Pa_Np);                                                                                 //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-PA-NP := RC.TIRCNTL-CNTRY-PA-NP
                            uc_View_Tircntl_Cntry_Ira_Pp.setValue(rc_Tircntl_Cntry_Ira_Pp);                                                                               //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-IRA-PP := RC.TIRCNTL-CNTRY-IRA-PP
                            uc_View_Tircntl_Cntry_Ira_Np.setValue(rc_Tircntl_Cntry_Ira_Np);                                                                               //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-IRA-NP := RC.TIRCNTL-CNTRY-IRA-NP
                            uc_View_Tircntl_Cntry_Form_Name.setValue(rc_Tircntl_Cntry_Form_Name);                                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-FORM-NAME := RC.TIRCNTL-CNTRY-FORM-NAME
                            uc_View_Tircntl_Cntry_Dpi_Np_Sp_Ind.setValue(rc_Tircntl_Cntry_Dpi_Np_Sp_Ind);                                                                 //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-DPI-NP-SP-IND := RC.TIRCNTL-CNTRY-DPI-NP-SP-IND
                            uc_View_Tircntl_Cntry_Dpi_Np_Sp_Rate.setValue(rc_Tircntl_Cntry_Dpi_Np_Sp_Rate);                                                               //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-DPI-NP-SP-RATE := RC.TIRCNTL-CNTRY-DPI-NP-SP-RATE
                            uc_View_Tircntl_Cntry_Tax_Non_Period.setValue(rc_Tircntl_Cntry_Tax_Non_Period);                                                               //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-TAX-NON-PERIOD := RC.TIRCNTL-CNTRY-TAX-NON-PERIOD
                            uc_View_Tircntl_Cntry_Early_Distrib_Ind.setValue(rc_Tircntl_Cntry_Early_Distrib_Ind);                                                         //Natural: ASSIGN UC-VIEW.TIRCNTL-CNTRY-EARLY-DISTRIB-IND := RC.TIRCNTL-CNTRY-EARLY-DISTRIB-IND
                            vw_uc_View.updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                            pnd_Updt_Ctr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        sc_Tircntl_Tbl_Nbr.setValue(rc_Tircntl_Tbl_Nbr);                                                                                                  //Natural: ASSIGN SC.TIRCNTL-TBL-NBR := RC.TIRCNTL-TBL-NBR
                        sc_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                                //Natural: ASSIGN SC.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        sc_Tircntl_Seq_Nbr.setValue(rc_Tircntl_Seq_Nbr);                                                                                                  //Natural: ASSIGN SC.TIRCNTL-SEQ-NBR := RC.TIRCNTL-SEQ-NBR
                        sc_Tircntl_Cntry_Alpha_Code.setValue(rc_Tircntl_Cntry_Alpha_Code);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-ALPHA-CODE := RC.TIRCNTL-CNTRY-ALPHA-CODE
                        sc_Tircntl_Cntry_Abbr.setValue(rc_Tircntl_Cntry_Abbr);                                                                                            //Natural: ASSIGN SC.TIRCNTL-CNTRY-ABBR := RC.TIRCNTL-CNTRY-ABBR
                        sc_Tircntl_Cntry_Full_Name.setValue(rc_Tircntl_Cntry_Full_Name);                                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-FULL-NAME := RC.TIRCNTL-CNTRY-FULL-NAME
                        sc_Tircntl_Cntry_Treaty_Ind.setValue(rc_Tircntl_Cntry_Treaty_Ind);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-TREATY-IND := RC.TIRCNTL-CNTRY-TREATY-IND
                        sc_Tircntl_Cntry_Tax_Period.setValue(rc_Tircntl_Cntry_Tax_Period);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-TAX-PERIOD := RC.TIRCNTL-CNTRY-TAX-PERIOD
                        sc_Tircntl_Cntry_Tax_Dpi.setValue(rc_Tircntl_Cntry_Tax_Dpi);                                                                                      //Natural: ASSIGN SC.TIRCNTL-CNTRY-TAX-DPI := RC.TIRCNTL-CNTRY-TAX-DPI
                        sc_Tircntl_Cntry_Tax_Rtb.setValue(rc_Tircntl_Cntry_Tax_Rtb);                                                                                      //Natural: ASSIGN SC.TIRCNTL-CNTRY-TAX-RTB := RC.TIRCNTL-CNTRY-TAX-RTB
                        sc_Tircntl_Cntry_Active_Cde.setValue(rc_Tircntl_Cntry_Active_Cde);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-ACTIVE-CDE := RC.TIRCNTL-CNTRY-ACTIVE-CDE
                        sc_Tircntl_Cntry_Compl_Dte.setValue(rc_Tircntl_Cntry_Compl_Dte);                                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-COMPL-DTE := RC.TIRCNTL-CNTRY-COMPL-DTE
                        sc_Tircntl_Cntry_Start_Dte.setValue(rc_Tircntl_Cntry_Start_Dte);                                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-START-DTE := RC.TIRCNTL-CNTRY-START-DTE
                        sc_Tircntl_Cntry_Updte_Dte.setValue(rc_Tircntl_Cntry_Updte_Dte);                                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-UPDTE-DTE := RC.TIRCNTL-CNTRY-UPDTE-DTE
                        sc_Tircntl_Cntry_Comment.setValue(rc_Tircntl_Cntry_Comment);                                                                                      //Natural: ASSIGN SC.TIRCNTL-CNTRY-COMMENT := RC.TIRCNTL-CNTRY-COMMENT
                        sc_Tircntl_Cntry_Updte_User.setValue(rc_Tircntl_Cntry_Updte_User);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-UPDTE-USER := RC.TIRCNTL-CNTRY-UPDTE-USER
                        sc_Tircntl_Cntry_Unique_Cde.setValue(rc_Tircntl_Cntry_Unique_Cde);                                                                                //Natural: ASSIGN SC.TIRCNTL-CNTRY-UNIQUE-CDE := RC.TIRCNTL-CNTRY-UNIQUE-CDE
                        sc_Tircntl_Cntry_Pa_Pp.setValue(rc_Tircntl_Cntry_Pa_Pp);                                                                                          //Natural: ASSIGN SC.TIRCNTL-CNTRY-PA-PP := RC.TIRCNTL-CNTRY-PA-PP
                        sc_Tircntl_Cntry_Pa_Np.setValue(rc_Tircntl_Cntry_Pa_Np);                                                                                          //Natural: ASSIGN SC.TIRCNTL-CNTRY-PA-NP := RC.TIRCNTL-CNTRY-PA-NP
                        sc_Tircntl_Cntry_Ira_Pp.setValue(rc_Tircntl_Cntry_Ira_Pp);                                                                                        //Natural: ASSIGN SC.TIRCNTL-CNTRY-IRA-PP := RC.TIRCNTL-CNTRY-IRA-PP
                        sc_Tircntl_Cntry_Ira_Np.setValue(rc_Tircntl_Cntry_Ira_Np);                                                                                        //Natural: ASSIGN SC.TIRCNTL-CNTRY-IRA-NP := RC.TIRCNTL-CNTRY-IRA-NP
                        sc_Tircntl_Cntry_Form_Name.setValue(rc_Tircntl_Cntry_Form_Name);                                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-FORM-NAME := RC.TIRCNTL-CNTRY-FORM-NAME
                        sc_Tircntl_Cntry_Dpi_Np_Sp_Ind.setValue(rc_Tircntl_Cntry_Dpi_Np_Sp_Ind);                                                                          //Natural: ASSIGN SC.TIRCNTL-CNTRY-DPI-NP-SP-IND := RC.TIRCNTL-CNTRY-DPI-NP-SP-IND
                        sc_Tircntl_Cntry_Dpi_Np_Sp_Rate.setValue(rc_Tircntl_Cntry_Dpi_Np_Sp_Rate);                                                                        //Natural: ASSIGN SC.TIRCNTL-CNTRY-DPI-NP-SP-RATE := RC.TIRCNTL-CNTRY-DPI-NP-SP-RATE
                        sc_Tircntl_Cntry_Tax_Non_Period.setValue(rc_Tircntl_Cntry_Tax_Non_Period);                                                                        //Natural: ASSIGN SC.TIRCNTL-CNTRY-TAX-NON-PERIOD := RC.TIRCNTL-CNTRY-TAX-NON-PERIOD
                        sc_Tircntl_Cntry_Early_Distrib_Ind.setValue(rc_Tircntl_Cntry_Early_Distrib_Ind);                                                                  //Natural: ASSIGN SC.TIRCNTL-CNTRY-EARLY-DISTRIB-IND := RC.TIRCNTL-CNTRY-EARLY-DISTRIB-IND
                        vw_sc.insertDBRow();                                                                                                                              //Natural: STORE SC
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(3);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 3
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Unique_Cde.setValue(" ");                                                                                                               //Natural: ASSIGN #S1-UNIQUE-CDE := ' '
                vw_rc.startDatabaseRead                                                                                                                                   //Natural: READ RC WITH TIRCNTL-NBR-YEAR-UNQ-CDE = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_UNQ_CDE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_UNQ_CDE", "ASC") }
                );
                RL3:
                while (condition(vw_rc.readNextRow("RL3")))
                {
                    if (condition(rc_Tircntl_Tbl_Nbr.equals(3)))                                                                                                          //Natural: IF RC.TIRCNTL-TBL-NBR = 3
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rc_Tircntl_Tbl_Nbr,new TabSetting(5),rc_Tircntl_Tax_Year,new          //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T RC.TIRCNTL-TBL-NBR 05T RC.TIRCNTL-TAX-YEAR 11T RC.TIRCNTL-SEQ-NBR 18T RC.TIRCNTL-CNTRY-ALPHA-CODE 25T RC.TIRCNTL-CNTRY-ABBR 31T RC.TIRCNTL-CNTRY-FULL-NAME 70T RC.TIRCNTL-CNTRY-TREATY-IND 75T RC.TIRCNTL-CNTRY-TAX-PERIOD 84T RC.TIRCNTL-CNTRY-TAX-NON-PERIOD 93T RC.TIRCNTL-CNTRY-TAX-DPI 102T RC.TIRCNTL-CNTRY-TAX-RTB 112T RC.TIRCNTL-CNTRY-ACTIVE-CDE 116T RC.TIRCNTL-CNTRY-START-DTE
                        TabSetting(11),rc_Tircntl_Seq_Nbr,new TabSetting(18),rc_Tircntl_Cntry_Alpha_Code,new TabSetting(25),rc_Tircntl_Cntry_Abbr,new TabSetting(31),rc_Tircntl_Cntry_Full_Name,new 
                        TabSetting(70),rc_Tircntl_Cntry_Treaty_Ind,new TabSetting(75),rc_Tircntl_Cntry_Tax_Period,new TabSetting(84),rc_Tircntl_Cntry_Tax_Non_Period,new 
                        TabSetting(93),rc_Tircntl_Cntry_Tax_Dpi,new TabSetting(102),rc_Tircntl_Cntry_Tax_Rtb,new TabSetting(112),rc_Tircntl_Cntry_Active_Cde,new 
                        TabSetting(116),rc_Tircntl_Cntry_Start_Dte);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*        *ISN (RL3.)
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(17),"Unique:",rc_Tircntl_Cntry_Unique_Cde,new ColumnSpacing(1),"'PA PP':",rc_Tircntl_Cntry_Pa_Pp,  //Natural: WRITE ( 01 ) NOTITLE NOHDR 17T 'Unique:' RC.TIRCNTL-CNTRY-UNIQUE-CDE 1X '"PA PP":' RC.TIRCNTL-CNTRY-PA-PP ( EM = Z9.999 ) 1X '"PA NP":' RC.TIRCNTL-CNTRY-PA-NP ( EM = Z9.999 ) 1X '"IRA PP":' RC.TIRCNTL-CNTRY-IRA-PP ( EM = Z9.999 ) 1X '"IRA NP":' RC.TIRCNTL-CNTRY-IRA-NP ( EM = Z9.999 ) 1X 'DPI:' RC.TIRCNTL-CNTRY-DPI-NP-SP-IND 1X 'DPI Rate:' RC.TIRCNTL-CNTRY-DPI-NP-SP-RATE ( EM = Z9.999 ) 1X 'Early Dist:' RC.TIRCNTL-CNTRY-EARLY-DISTRIB-IND
                        new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'PA NP':",rc_Tircntl_Cntry_Pa_Np, new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'IRA PP':",rc_Tircntl_Cntry_Ira_Pp, 
                        new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"'IRA NP':",rc_Tircntl_Cntry_Ira_Np, new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"DPI:",rc_Tircntl_Cntry_Dpi_Np_Sp_Ind,new 
                        ColumnSpacing(1),"DPI Rate:",rc_Tircntl_Cntry_Dpi_Np_Sp_Rate, new ReportEditMask ("Z9.999"),new ColumnSpacing(1),"Early Dist:",rc_Tircntl_Cntry_Early_Distrib_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(rc_Tircntl_Cntry_Form_Name.notEquals(" ")))                                                                                             //Natural: IF RC.TIRCNTL-CNTRY-FORM-NAME NE ' '
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(17),"Form:",rc_Tircntl_Cntry_Form_Name);                             //Natural: WRITE ( 01 ) NOTITLE NOHDR 17T 'Form:' RC.TIRCNTL-CNTRY-FORM-NAME
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"Country Code Table Report",new           //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 53T 'Country Code Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"CNTRY",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'CNTRY' 24T 'CNTRY' 31T '                                   ' 68T 'TRETY' 75T ' Tax   ' 84T 'No Tax ' 93T '  Tax  ' 102T '  Tax  ' 111T 'ACTV' 117T ' Start  '
                        TabSetting(24),"CNTRY",new TabSetting(31),"                                   ",new TabSetting(68),"TRETY",new TabSetting(75)," Tax   ",new 
                        TabSetting(84),"No Tax ",new TabSetting(93),"  Tax  ",new TabSetting(102),"  Tax  ",new TabSetting(111),"ACTV",new TabSetting(117),
                        " Start  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Alpha",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Alpha' 24T 'ABBRV' 31T 'Country Full Name                  ' 68T ' IND ' 75T 'Period ' 84T 'Period ' 93T '  DPI  ' 102T '  RTB  ' 111T 'IND ' 117T '  Date  '
                        TabSetting(24),"ABBRV",new TabSetting(31),"Country Full Name                  ",new TabSetting(68)," IND ",new TabSetting(75),"Period ",new 
                        TabSetting(84),"Period ",new TabSetting(93),"  DPI  ",new TabSetting(102),"  RTB  ",new TabSetting(111),"IND ",new TabSetting(117),
                        "  Date  ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '=====' 31T '===================================' 68T '=====' 75T '=======' 84T '=======' 93T '=======' 102T '=======' 111T '====' 117T '========'
                        TabSetting(24),"=====",new TabSetting(31),"===================================",new TabSetting(68),"=====",new TabSetting(75),"=======",new 
                        TabSetting(84),"=======",new TabSetting(93),"=======",new TabSetting(102),"=======",new TabSetting(111),"====",new TabSetting(117),
                        "========");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
