/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:33:29 PM
**        * FROM NATURAL PROGRAM : Twrp1930
************************************************************
**        * FILE NAME            : Twrp1930.java
**        * CLASS NAME           : Twrp1930
**        * INSTANCE NAME        : Twrp1930
************************************************************
***********************************************************************
* PROGRAM : TWRP1930
* FUNCTION: PRINTS ALL CREATED RECORDS BY COMPANY, SYSTEM SOURCE &
*                              STATUS FOR THE YEAR (YTD)
* INPUTS  : FILE 96 (TWR-IRA) - CONTRIBUTION FILE
*           CONTROL RECORD - FOR THE PARAMETER DATE
* OUTPUT  : IRA5498 CONTRIBUTION FILE REPORT
*           CONTROL TOTAL
* AUTHOR  : EDITH 7/21/99
* NOTE    : INACTIVATED RECORDS FROM CONTRIBUTION FILE HAVE (+) SIGN
*             AMOUNTS AND THE USER WANTS TO DISPLAY THESE AMOUNTS WITH
*             A (-) SIGN.
*         : THE PARAMETER DATE WHICH WILL INDICATE THE DATE RANGE
*               WILL BE COMING FROM A CONTROL RECORD.  THE FF: SHOULD
*               BE THE CONTENT OF THE DATE RANGE.
*                      CREATE-DATE-FROM = 1ST LOAD DATE
*                      CREATE-DATE-TO   = RUNDATE
* UPDATES : ORIGINAL LOAD COUNT AND AMOUNTS OF SOURCE CODES LOADED IN
*            BATCH (P1 & P2) AND ADDED ONLINE (ML) SHOULD NOT CHANGE
*            WHEN REPORTED.(AS PER SALLY) EDITH 8/26/99
*
*         : TRANS.COUNT OF INACTIVATED RECORDS TO BE DISPLAYED WITH
*              NEGATIVE (-) SIGN . EDITH 8/28/99
*         : TO ENCLOSE INACTIVATED RECORD'S CNT & AMT IN ( ).
*           PRINT THE TOTAL NUMBER OF RECORDS WITH ZERO FINANCIAL AMTS
*             PER SOURCE CODE & PRINT DETAILS ON A SEPARATE REPORT.
*                 EDITH 9/1/99 (AS PER JOE B.)
*         : TO ACCOUNT FOR ORIGINAL RECORD BEFORE IT WAS INACTIVATED
*            NOTE : I TOTALLY CHANGED THE LOGIC OF THE PROGRAM
*              EDITH 9/9/99
*         : 01/08/2000 EDITH
*           TO READ FLAT-FILE FOR THE TAX YEAR AND PROCESSING DATE TO BE
*                  USED BY THE PROGRAM
*         : 08/25/2005 J.ROTHOLZ - ADDED CODE FOR SEP IRA
*         : 09/07/2005 B.KARCHEV - UPDATE REPORT LAYOUT
*         : 02/03/2006 B.KARCHEV - FIX BUG IN TITLE
*         : 10/19/2017 DASDH - 5498 BOX CHANGES
*         : 11/12/2020 SAIK  - 5498 TAX COMPLIANCE CHANGES
***********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp1930 extends BLNatBase
{
    // Data Areas
    private LdaTwrl190a ldaTwrl190a;
    private LdaTwrl0600 ldaTwrl0600;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Prt_Var;
    private DbsField pnd_Prt_Var_Pnd_Prt1;
    private DbsField pnd_Prt_Var_Pnd_Prt2;
    private DbsField pnd_Prt_Var_Pnd_Prt3;

    private DbsGroup pnd_Var;
    private DbsField pnd_Var_Pnd_Count;
    private DbsField pnd_Var_Pnd_Classic_Amt;
    private DbsField pnd_Var_Pnd_Roth_Amt;
    private DbsField pnd_Var_Pnd_Sep_Amt;
    private DbsField pnd_Var_Pnd_Rollovr_Amt;
    private DbsField pnd_Var_Pnd_Rechar_Amt;
    private DbsField pnd_Var_Pnd_Conv_Amt;
    private DbsField pnd_Var_Pnd_Fmv_Amt;
    private DbsField pnd_Var_Pnd_Postpn_Amt;
    private DbsField pnd_Var_Pnd_Repayments_Amt;

    private DbsGroup pnd_Orig_Var;
    private DbsField pnd_Orig_Var_Pnd_Orig_Count;
    private DbsField pnd_Orig_Var_Pnd_Orig_Classic_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Roth_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Sep_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Rollovr_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Rechar_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Conv_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Fmv_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Postpn_Amt;
    private DbsField pnd_Orig_Var_Pnd_Orig_Repayments_Amt;
    private DbsField pnd_Temp_Amt;
    private DbsField pnd_Ttl_Cnt;
    private DbsField pnd_Ttl_Amt;
    private DbsField pnd_Print_Cnt;
    private DbsField pnd_Amt;
    private DbsField pnd_Print_Amt;
    private DbsField pnd_Print_Run;
    private DbsField pnd_Print_Com;
    private DbsField pnd_Param_Freq;
    private DbsField pnd_Tax_Year;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_New_Record;
    private DbsField pnd_New_Company;
    private DbsField pnd_End_Orig_Rec;

    private DbsGroup pnd_Saver;
    private DbsField pnd_Saver_Pnd_Sv_Company;
    private DbsField pnd_Saver_Pnd_Sv_Source;

    private DbsGroup pnd_Saver__R_Field_1;
    private DbsField pnd_Saver_Pnd_Sv_Source_Org;
    private DbsField pnd_Saver_Pnd_Sv_Status;
    private DbsField pnd_Cref_Cnt;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Serv_Cnt;
    private DbsField pnd_Othr_Cnt;
    private DbsField pnd_Print_Source;

    private DbsGroup pnd_Print_Source__R_Field_2;
    private DbsField pnd_Print_Source_Pnd_Print_Source_Org;
    private DbsField pnd_Print_Source_Pnd_Print_Source_Upd;
    private DbsField pnd_Zero_Amt;
    private DbsField pnd_Total_Zero_Amt;
    private DbsField pnd_Prt0;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl190a = new LdaTwrl190a();
        registerRecord(ldaTwrl190a);
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Prt_Var = localVariables.newGroupInRecord("pnd_Prt_Var", "#PRT-VAR");
        pnd_Prt_Var_Pnd_Prt1 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt1", "#PRT1", FieldType.STRING, 4);
        pnd_Prt_Var_Pnd_Prt2 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt2", "#PRT2", FieldType.STRING, 14);
        pnd_Prt_Var_Pnd_Prt3 = pnd_Prt_Var.newFieldInGroup("pnd_Prt_Var_Pnd_Prt3", "#PRT3", FieldType.STRING, 21);

        pnd_Var = localVariables.newGroupArrayInRecord("pnd_Var", "#VAR", new DbsArrayController(1, 4));
        pnd_Var_Pnd_Count = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Count", "#COUNT", FieldType.NUMERIC, 8);
        pnd_Var_Pnd_Classic_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Classic_Amt", "#CLASSIC-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Roth_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Roth_Amt", "#ROTH-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Sep_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Sep_Amt", "#SEP-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Rollovr_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Rollovr_Amt", "#ROLLOVR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Rechar_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Rechar_Amt", "#RECHAR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Conv_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Conv_Amt", "#CONV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Fmv_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Fmv_Amt", "#FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Postpn_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Postpn_Amt", "#POSTPN-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Var_Pnd_Repayments_Amt = pnd_Var.newFieldInGroup("pnd_Var_Pnd_Repayments_Amt", "#REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Orig_Var = localVariables.newGroupInRecord("pnd_Orig_Var", "#ORIG-VAR");
        pnd_Orig_Var_Pnd_Orig_Count = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Count", "#ORIG-COUNT", FieldType.NUMERIC, 8);
        pnd_Orig_Var_Pnd_Orig_Classic_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Classic_Amt", "#ORIG-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Orig_Var_Pnd_Orig_Roth_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Roth_Amt", "#ORIG-ROTH-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Orig_Var_Pnd_Orig_Sep_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Sep_Amt", "#ORIG-SEP-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Orig_Var_Pnd_Orig_Rollovr_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Rollovr_Amt", "#ORIG-ROLLOVR-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Orig_Var_Pnd_Orig_Rechar_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Rechar_Amt", "#ORIG-RECHAR-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Orig_Var_Pnd_Orig_Conv_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Conv_Amt", "#ORIG-CONV-AMT", FieldType.PACKED_DECIMAL, 13, 
            2);
        pnd_Orig_Var_Pnd_Orig_Fmv_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Fmv_Amt", "#ORIG-FMV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Orig_Var_Pnd_Orig_Postpn_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Postpn_Amt", "#ORIG-POSTPN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Orig_Var_Pnd_Orig_Repayments_Amt = pnd_Orig_Var.newFieldInGroup("pnd_Orig_Var_Pnd_Orig_Repayments_Amt", "#ORIG-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Temp_Amt = localVariables.newFieldArrayInRecord("pnd_Temp_Amt", "#TEMP-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 9));
        pnd_Ttl_Cnt = localVariables.newFieldInRecord("pnd_Ttl_Cnt", "#TTL-CNT", FieldType.STRING, 11);
        pnd_Ttl_Amt = localVariables.newFieldArrayInRecord("pnd_Ttl_Amt", "#TTL-AMT", FieldType.STRING, 19, new DbsArrayController(1, 9));
        pnd_Print_Cnt = localVariables.newFieldInRecord("pnd_Print_Cnt", "#PRINT-CNT", FieldType.STRING, 11);
        pnd_Amt = localVariables.newFieldArrayInRecord("pnd_Amt", "#AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 9));
        pnd_Print_Amt = localVariables.newFieldArrayInRecord("pnd_Print_Amt", "#PRINT-AMT", FieldType.STRING, 19, new DbsArrayController(1, 9));
        pnd_Print_Run = localVariables.newFieldInRecord("pnd_Print_Run", "#PRINT-RUN", FieldType.STRING, 11);
        pnd_Print_Com = localVariables.newFieldInRecord("pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Param_Freq = localVariables.newFieldInRecord("pnd_Param_Freq", "#PARAM-FREQ", FieldType.STRING, 1);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 9);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_New_Record = localVariables.newFieldInRecord("pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_New_Company = localVariables.newFieldInRecord("pnd_New_Company", "#NEW-COMPANY", FieldType.BOOLEAN, 1);
        pnd_End_Orig_Rec = localVariables.newFieldInRecord("pnd_End_Orig_Rec", "#END-ORIG-REC", FieldType.BOOLEAN, 1);

        pnd_Saver = localVariables.newGroupInRecord("pnd_Saver", "#SAVER");
        pnd_Saver_Pnd_Sv_Company = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Saver_Pnd_Sv_Source = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Source", "#SV-SOURCE", FieldType.STRING, 6);

        pnd_Saver__R_Field_1 = pnd_Saver.newGroupInGroup("pnd_Saver__R_Field_1", "REDEFINE", pnd_Saver_Pnd_Sv_Source);
        pnd_Saver_Pnd_Sv_Source_Org = pnd_Saver__R_Field_1.newFieldInGroup("pnd_Saver_Pnd_Sv_Source_Org", "#SV-SOURCE-ORG", FieldType.STRING, 3);
        pnd_Saver_Pnd_Sv_Status = pnd_Saver.newFieldInGroup("pnd_Saver_Pnd_Sv_Status", "#SV-STATUS", FieldType.STRING, 1);
        pnd_Cref_Cnt = localVariables.newFieldArrayInRecord("pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Tiaa_Cnt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Serv_Cnt = localVariables.newFieldArrayInRecord("pnd_Serv_Cnt", "#SERV-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Othr_Cnt = localVariables.newFieldArrayInRecord("pnd_Othr_Cnt", "#OTHR-CNT", FieldType.NUMERIC, 9, new DbsArrayController(1, 2));
        pnd_Print_Source = localVariables.newFieldInRecord("pnd_Print_Source", "#PRINT-SOURCE", FieldType.STRING, 6);

        pnd_Print_Source__R_Field_2 = localVariables.newGroupInRecord("pnd_Print_Source__R_Field_2", "REDEFINE", pnd_Print_Source);
        pnd_Print_Source_Pnd_Print_Source_Org = pnd_Print_Source__R_Field_2.newFieldInGroup("pnd_Print_Source_Pnd_Print_Source_Org", "#PRINT-SOURCE-ORG", 
            FieldType.STRING, 3);
        pnd_Print_Source_Pnd_Print_Source_Upd = pnd_Print_Source__R_Field_2.newFieldInGroup("pnd_Print_Source_Pnd_Print_Source_Upd", "#PRINT-SOURCE-UPD", 
            FieldType.STRING, 3);
        pnd_Zero_Amt = localVariables.newFieldInRecord("pnd_Zero_Amt", "#ZERO-AMT", FieldType.NUMERIC, 8);
        pnd_Total_Zero_Amt = localVariables.newFieldInRecord("pnd_Total_Zero_Amt", "#TOTAL-ZERO-AMT", FieldType.PACKED_DECIMAL, 9);
        pnd_Prt0 = localVariables.newFieldInRecord("pnd_Prt0", "#PRT0", FieldType.STRING, 21);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl190a.initializeValues();
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_New_Record.setInitialValue(true);
        pnd_New_Company.setInitialValue(false);
        pnd_End_Orig_Rec.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp1930() throws Exception
    {
        super("Twrp1930");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                             //Natural: READ WORK FILE 01 ONCE #TWRP0600-CONTROL-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, " ************************************ ",NEWLINE," ***                              *** ",NEWLINE," ***    CONTROL RECORD IS EMPTY   *** ", //Natural: WRITE ' ************************************ ' / ' ***                              *** ' / ' ***    CONTROL RECORD IS EMPTY   *** ' / ' ***    PLEASE INFORM SYSTEMS !!  *** ' / ' ***                              *** ' / ' ************************************ '
                NEWLINE," ***    PLEASE INFORM SYSTEMS !!  *** ",NEWLINE," ***                              *** ",NEWLINE," ************************************ ");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #F96-FF1
        while (condition(getWorkFiles().read(2, ldaTwrl190a.getPnd_F96_Ff1())))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            pnd_Cnt.setValue(1);                                                                                                                                          //Natural: ASSIGN #CNT := 1
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rec_Process.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-PROCESS
            pnd_Cnt.setValue(2);                                                                                                                                          //Natural: ASSIGN #CNT := 2
                                                                                                                                                                          //Natural: PERFORM COMPANY-CNT
            sub_Company_Cnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_New_Record.getBoolean()))                                                                                                                   //Natural: IF #NEW-RECORD
            {
                pnd_New_Record.setValue(false);                                                                                                                           //Natural: ASSIGN #NEW-RECORD := FALSE
                                                                                                                                                                          //Natural: PERFORM SAVE-FIELDS
                sub_Save_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals(pnd_Saver_Pnd_Sv_Company)))                                                                //Natural: IF #F96-FF1.TWRC-COMPANY-CDE = #SV-COMPANY
            {
                if (condition(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd().equals(pnd_Saver_Pnd_Sv_Source)))                                                          //Natural: IF #F96-FF1.#TWRC-SOURCE-UPD = #SV-SOURCE
                {
                                                                                                                                                                          //Natural: PERFORM ORIGINAL-COUNT
                    sub_Original_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  AT BREAK OF SOURCE CODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL
                    sub_Print_Subtotal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Saver_Pnd_Sv_Source.setValue(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd());                                                                   //Natural: ASSIGN #SV-SOURCE := #F96-FF1.#TWRC-SOURCE-UPD
                    pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                           //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
                                                                                                                                                                          //Natural: PERFORM ORIGINAL-COUNT
                    sub_Original_Count();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  AT BREAK OF COMPANY
                //*  INDEX VALUE 3 CONTAINS TOTALS BY COMPANY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_New_Company.setValue(true);                                                                                                                           //Natural: ASSIGN #NEW-COMPANY := TRUE
                pnd_J.setValue(3);                                                                                                                                        //Natural: ASSIGN #J := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-GRANDTOTAL
                sub_Print_Grandtotal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SAVE-FIELDS
                sub_Save_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ORIGINAL-COUNT
                sub_Original_Count();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*           -- CHECK IF ACTIVE RECORD HAS ZERO FINANCIAL AMOUNT --
            //*  DASDH
            //*  SAIK
            if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(" ") && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt().equals(getZero()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt().equals(getZero())  //Natural: IF #F96-FF1.TWRC-STATUS = ' ' AND #F96-FF1.TWRC-CLASSIC-AMT = 0 AND #F96-FF1.TWRC-ROTH-AMT = 0 AND #F96-FF1.TWRC-SEP-AMT = 0 AND #F96-FF1.TWRC-ROLLOVER-AMT = 0 AND #F96-FF1.TWRC-RECHAR-AMT = 0 AND #F96-FF1.TWRC-ROTH-CONVERSION-AMT = 0 AND #F96-FF1.TWRC-FAIR-MKT-VAL-AMT = 0 AND #F96-FF1.TWRC-POSTPN-AMT = 0 AND #F96-FF1.TWRC-REPAYMENTS-AMT = 0
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt().equals(getZero()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt().equals(getZero()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt().equals(getZero()) 
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt().equals(getZero()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt().equals(getZero()) 
                && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt().equals(getZero()) && ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt().equals(getZero())))
            {
                pnd_Zero_Amt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #ZERO-AMT
                pnd_Total_Zero_Amt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-ZERO-AMT
                getReports().display(2, "ORG/SRCD",                                                                                                                       //Natural: DISPLAY ( 2 ) 'ORG/SRCD' #F96-FF1.#TWRC-ORIG-SC 'UPD/SRCD' #F96-FF1.#TWRC-UPDT-SC 'STATUS' #F96-FF1.TWRC-STATUS 'TIN' #F96-FF1.TWRC-TAX-ID 'CONTRACT/NUMBER' #F96-FF1.TWRC-CONTRACT 'PAYEE/CODE' #F96-FF1.TWRC-PAYEE
                		ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc(),"UPD/SRCD",
                		ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Updt_Sc(),"STATUS",
                		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status(),"TIN",
                		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id(),"CONTRACT/NUMBER",
                		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Contract(),"PAYEE/CODE",
                		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Payee());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SUBTRACT CNT FROM SUBTTL
                pnd_Var_Pnd_Count.getValue(2).nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #COUNT ( 2 )
                //*  SUBTRACT CNT FROM GRNDTTL
                pnd_Var_Pnd_Count.getValue(3).nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #COUNT ( 3 )
                //*  SUBTRACT CNT FROM GRNDTTL - ALL
                pnd_Var_Pnd_Count.getValue(4).nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #COUNT ( 4 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Rec_Process.equals(getZero())))                                                                                                                 //Natural: IF #REC-PROCESS = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_J.setValue(3);                                                                                                                                            //Natural: ASSIGN #J := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-GRANDTOTAL
            sub_Print_Grandtotal();
            if (condition(Global.isEscape())) {return;}
            pnd_J.setValue(4);                                                                                                                                            //Natural: ASSIGN #J := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-GRANDTOTAL
            sub_Print_Grandtotal();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER()," - ",Global.getPROGRAM(),NEWLINE,"RUNDATE    : ",Global.getDATX(),           //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER ' - ' *PROGRAM / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) 2X '(YTD-RUN)' / 'RUNTIME    : ' *TIMX / 'TAX YEAR   : ' #TWRP0600-TAX-YEAR-CCYY / 'DATE RANGE : ' #TWRP0600-FROM-CCYYMMDD ' THRU ' #TWRP0600-TO-CCYYMMDD / 'TOTAL RECORDS READ         : ' #REC-READ / '  1. CREF RECORDS          : ' #CREF-CNT ( 1 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 1 ) / '  3. TCII RECORDS          : ' #SERV-CNT ( 1 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 1 ) /// 'TOTAL RECORDS PROCESS      : ' #REC-PROCESS / '  1. CREF RECORDS          : ' #CREF-CNT ( 2 ) / '  2. TIAA RECORDS          : ' #TIAA-CNT ( 2 ) / '  3. TCII RECORDS          : ' #SERV-CNT ( 2 ) / '  4. NON-CREF/TIAA RECORDS : ' #OTHR-CNT ( 2 ) ///
            new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(2),"(YTD-RUN)",NEWLINE,"RUNTIME    : ",Global.getTIMX(),NEWLINE,"TAX YEAR   : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),
            NEWLINE,"DATE RANGE : ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd()," THRU ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd(),
            NEWLINE,"TOTAL RECORDS READ         : ",pnd_Rec_Read,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(1),NEWLINE,"  2. TIAA RECORDS          : ",
            pnd_Tiaa_Cnt.getValue(1),NEWLINE,"  3. TCII RECORDS          : ",pnd_Serv_Cnt.getValue(1),NEWLINE,"  4. NON-CREF/TIAA RECORDS : ",pnd_Othr_Cnt.getValue(1),
            NEWLINE,NEWLINE,NEWLINE,"TOTAL RECORDS PROCESS      : ",pnd_Rec_Process,NEWLINE,"  1. CREF RECORDS          : ",pnd_Cref_Cnt.getValue(2),NEWLINE,
            "  2. TIAA RECORDS          : ",pnd_Tiaa_Cnt.getValue(2),NEWLINE,"  3. TCII RECORDS          : ",pnd_Serv_Cnt.getValue(2),NEWLINE,"  4. NON-CREF/TIAA RECORDS : ",
            pnd_Othr_Cnt.getValue(2),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*                  --------------
        //*                  ----------------
        //*   ADD RECORD CNT & AMT TO SUBTOTAL & GRANDTOTAL
        //*                  -----------
        //*   ACCOUNT CNT & AMT TO SUBTOTAL & GRANDTOTAL
        //*                  -----------
        //*                  -----------
        //*                  ------------------
        //*                  ------------------
        //*                  --------------
        //*                  ----------------
        //*       //  #PRT3
        //*                  ----------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
    }
    private void sub_Original_Count() throws Exception                                                                                                                    //Natural: ORIGINAL-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------------
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-ORIG-REC
        sub_Account_Orig_Rec();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals(pnd_Saver_Pnd_Sv_Status)))                                                                          //Natural: IF #F96-FF1.TWRC-STATUS = #SV-STATUS
        {
            if (condition(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status().equals("I")))                                                                                          //Natural: IF #F96-FF1.TWRC-STATUS = 'I'
            {
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
                sub_Account_Rec();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-REC
            sub_Account_Rec();
            if (condition(Global.isEscape())) {return;}
            pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                                   //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Account_Orig_Rec() throws Exception                                                                                                                  //Natural: ACCOUNT-ORIG-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------------
        pnd_Orig_Var_Pnd_Orig_Count.nadd(1);                                                                                                                              //Natural: ADD 1 TO #ORIG-COUNT
        pnd_Orig_Var_Pnd_Orig_Classic_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                                            //Natural: ADD #F96-FF1.TWRC-CLASSIC-AMT TO #ORIG-CLASSIC-AMT
        pnd_Orig_Var_Pnd_Orig_Roth_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                                                  //Natural: ADD #F96-FF1.TWRC-ROTH-AMT TO #ORIG-ROTH-AMT
        pnd_Orig_Var_Pnd_Orig_Sep_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                                    //Natural: ADD #F96-FF1.TWRC-SEP-AMT TO #ORIG-SEP-AMT
        pnd_Orig_Var_Pnd_Orig_Rollovr_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                                           //Natural: ADD #F96-FF1.TWRC-ROLLOVER-AMT TO #ORIG-ROLLOVR-AMT
        pnd_Orig_Var_Pnd_Orig_Rechar_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                                              //Natural: ADD #F96-FF1.TWRC-RECHAR-AMT TO #ORIG-RECHAR-AMT
        pnd_Orig_Var_Pnd_Orig_Conv_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                                       //Natural: ADD #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #ORIG-CONV-AMT
        pnd_Orig_Var_Pnd_Orig_Fmv_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                                           //Natural: ADD #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #ORIG-FMV-AMT
        //*  DASDH
        pnd_Orig_Var_Pnd_Orig_Postpn_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                                              //Natural: ADD #F96-FF1.TWRC-POSTPN-AMT TO #ORIG-POSTPN-AMT
        //*  SAIK
        pnd_Orig_Var_Pnd_Orig_Repayments_Amt.nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                                      //Natural: ADD #F96-FF1.TWRC-REPAYMENTS-AMT TO #ORIG-REPAYMENTS-AMT
        FOR01:                                                                                                                                                            //Natural: FOR #I 2 4
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            pnd_Var_Pnd_Count.getValue(pnd_I).nadd(1);                                                                                                                    //Natural: ADD 1 TO #COUNT ( #I )
            pnd_Var_Pnd_Classic_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                                  //Natural: ADD #F96-FF1.TWRC-CLASSIC-AMT TO #CLASSIC-AMT ( #I )
            pnd_Var_Pnd_Roth_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                                        //Natural: ADD #F96-FF1.TWRC-ROTH-AMT TO #ROTH-AMT ( #I )
            pnd_Var_Pnd_Sep_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                          //Natural: ADD #F96-FF1.TWRC-SEP-AMT TO #SEP-AMT ( #I )
            pnd_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                                 //Natural: ADD #F96-FF1.TWRC-ROLLOVER-AMT TO #ROLLOVR-AMT ( #I )
            pnd_Var_Pnd_Rechar_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                                    //Natural: ADD #F96-FF1.TWRC-RECHAR-AMT TO #RECHAR-AMT ( #I )
            pnd_Var_Pnd_Conv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                             //Natural: ADD #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #CONV-AMT ( #I )
            pnd_Var_Pnd_Fmv_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                                 //Natural: ADD #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #FMV-AMT ( #I )
            //*  DASDH
            pnd_Var_Pnd_Postpn_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                                    //Natural: ADD #F96-FF1.TWRC-POSTPN-AMT TO #POSTPN-AMT ( #I )
            //*  SAIK
            pnd_Var_Pnd_Repayments_Amt.getValue(pnd_I).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                            //Natural: ADD #F96-FF1.TWRC-REPAYMENTS-AMT TO #REPAYMENTS-AMT ( #I )
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Account_Rec() throws Exception                                                                                                                       //Natural: ACCOUNT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
        pnd_Var_Pnd_Count.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT ( 1 )
        pnd_Var_Pnd_Classic_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                                          //Natural: ADD #F96-FF1.TWRC-CLASSIC-AMT TO #CLASSIC-AMT ( 1 )
        pnd_Var_Pnd_Roth_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                                                //Natural: ADD #F96-FF1.TWRC-ROTH-AMT TO #ROTH-AMT ( 1 )
        pnd_Var_Pnd_Sep_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                                  //Natural: ADD #F96-FF1.TWRC-SEP-AMT TO #SEP-AMT ( 1 )
        pnd_Var_Pnd_Rollovr_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                                         //Natural: ADD #F96-FF1.TWRC-ROLLOVER-AMT TO #ROLLOVR-AMT ( 1 )
        pnd_Var_Pnd_Rechar_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                                            //Natural: ADD #F96-FF1.TWRC-RECHAR-AMT TO #RECHAR-AMT ( 1 )
        pnd_Var_Pnd_Conv_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                                     //Natural: ADD #F96-FF1.TWRC-ROTH-CONVERSION-AMT TO #CONV-AMT ( 1 )
        pnd_Var_Pnd_Fmv_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                                         //Natural: ADD #F96-FF1.TWRC-FAIR-MKT-VAL-AMT TO #FMV-AMT ( 1 )
        //*  DASDH
        pnd_Var_Pnd_Postpn_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                                            //Natural: ADD #F96-FF1.TWRC-POSTPN-AMT TO #POSTPN-AMT ( 1 )
        //*  SAIK
        pnd_Var_Pnd_Repayments_Amt.getValue(1).nadd(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                                    //Natural: ADD #F96-FF1.TWRC-REPAYMENTS-AMT TO #REPAYMENTS-AMT ( 1 )
        FOR02:                                                                                                                                                            //Natural: FOR #I 2 4
        for (pnd_I.setValue(2); condition(pnd_I.lessOrEqual(4)); pnd_I.nadd(1))
        {
            pnd_Var_Pnd_Count.getValue(pnd_I).nsubtract(1);                                                                                                               //Natural: SUBTRACT 1 FROM #COUNT ( #I )
            pnd_Var_Pnd_Classic_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Classic_Amt());                                                             //Natural: SUBTRACT #F96-FF1.TWRC-CLASSIC-AMT FROM #CLASSIC-AMT ( #I )
            pnd_Var_Pnd_Roth_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Amt());                                                                   //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-AMT FROM #ROTH-AMT ( #I )
            pnd_Var_Pnd_Sep_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Sep_Amt());                                                                     //Natural: SUBTRACT #F96-FF1.TWRC-SEP-AMT FROM #SEP-AMT ( #I )
            pnd_Var_Pnd_Rollovr_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rollover_Amt());                                                            //Natural: SUBTRACT #F96-FF1.TWRC-ROLLOVER-AMT FROM #ROLLOVR-AMT ( #I )
            pnd_Var_Pnd_Rechar_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Rechar_Amt());                                                               //Natural: SUBTRACT #F96-FF1.TWRC-RECHAR-AMT FROM #RECHAR-AMT ( #I )
            pnd_Var_Pnd_Conv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt());                                                        //Natural: SUBTRACT #F96-FF1.TWRC-ROTH-CONVERSION-AMT FROM #CONV-AMT ( #I )
            pnd_Var_Pnd_Fmv_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt());                                                            //Natural: SUBTRACT #F96-FF1.TWRC-FAIR-MKT-VAL-AMT FROM #FMV-AMT ( #I )
            //*  DASDH
            pnd_Var_Pnd_Postpn_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Postpn_Amt());                                                               //Natural: SUBTRACT #F96-FF1.TWRC-POSTPN-AMT FROM #POSTPN-AMT ( #I )
            //*  DASDH
            //*  SAIK
            pnd_Var_Pnd_Repayments_Amt.getValue(pnd_I).nsubtract(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Repayments_Amt());                                                       //Natural: SUBTRACT #F96-FF1.TWRC-REPAYMENTS-AMT FROM #REPAYMENTS-AMT ( #I )
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Save_Fields() throws Exception                                                                                                                       //Natural: SAVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Saver_Pnd_Sv_Company.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde());                                                                                 //Natural: ASSIGN #SV-COMPANY := #F96-FF1.TWRC-COMPANY-CDE
        pnd_Saver_Pnd_Sv_Source.setValue(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Source_Upd());                                                                               //Natural: ASSIGN #SV-SOURCE := #F96-FF1.#TWRC-SOURCE-UPD
        pnd_Saver_Pnd_Sv_Status.setValue(ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status());                                                                                       //Natural: ASSIGN #SV-STATUS := #F96-FF1.TWRC-STATUS
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Company_Cnt() throws Exception                                                                                                                       //Natural: COMPANY-CNT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        short decideConditionsMet365 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #F96-FF1.TWRC-COMPANY-CDE;//Natural: VALUE 'C'
        if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("C"))))
        {
            decideConditionsMet365++;
            pnd_Cref_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #CREF-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("T"))))
        {
            decideConditionsMet365++;
            pnd_Tiaa_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #TIAA-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaTwrl190a.getPnd_F96_Ff1_Twrc_Company_Cde().equals("S"))))
        {
            decideConditionsMet365++;
            pnd_Serv_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #SERV-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Othr_Cnt.getValue(pnd_Cnt).nadd(1);                                                                                                                       //Natural: ADD 1 TO #OTHR-CNT ( #CNT )
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Detail_Total() throws Exception                                                                                                                //Natural: PRINT-DETAIL-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print_Source.setValue(pnd_Saver_Pnd_Sv_Source);                                                                                                               //Natural: ASSIGN #PRINT-SOURCE := #SV-SOURCE
        if (condition(pnd_Print_Source_Pnd_Print_Source_Org.equals("ZZ ")))                                                                                               //Natural: IF #PRINT-SOURCE-ORG = 'ZZ '
        {
            pnd_Print_Source_Pnd_Print_Source_Org.setValue("ML ");                                                                                                        //Natural: ASSIGN #PRINT-SOURCE-ORG := 'ML '
        }                                                                                                                                                                 //Natural: END-IF
        //*            --- PRINT ORIGINAL ACTIVE RECORD OR  ACTIVE RECORD
        //*                                                 BEFORE INACTIVATED.
        if (condition(pnd_Orig_Var_Pnd_Orig_Count.notEquals(getZero())))                                                                                                  //Natural: IF #ORIG-COUNT NE 0
        {
            if (condition(pnd_Print_Source_Pnd_Print_Source_Upd.equals("   ")))                                                                                           //Natural: IF #PRINT-SOURCE-UPD = '   '
            {
                pnd_Prt_Var_Pnd_Prt3.setValue(pnd_Print_Source);                                                                                                          //Natural: ASSIGN #PRT3 := #PRINT-SOURCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_Source_Pnd_Print_Source_Org, "/", pnd_Print_Source_Pnd_Print_Source_Upd)); //Natural: COMPRESS #PRINT-SOURCE-ORG '/' #PRINT-SOURCE-UPD TO #PRT3 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            //*  BK
            //*  DASDH
            //*  SAIK
            getReports().write(1, ReportOption.NOTITLE,pnd_Prt_Var_Pnd_Prt3,new TabSetting(24),pnd_Orig_Var_Pnd_Orig_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new          //Natural: WRITE ( 1 ) #PRT3 24T #ORIG-COUNT ( EM = Z,ZZZ,ZZ9 ) 3X #ORIG-CLASSIC-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ORIG-ROTH-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ORIG-SEP-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ORIG-ROLLOVR-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) /36T #ORIG-POSTPN-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 56T #ORIG-RECHAR-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ORIG-CONV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) 3X #ORIG-FMV-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) /36T #ORIG-REPAYMENTS-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) /
                ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Classic_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Roth_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Sep_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Rollovr_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(36),pnd_Orig_Var_Pnd_Orig_Postpn_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(56),pnd_Orig_Var_Pnd_Orig_Rechar_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Conv_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),pnd_Orig_Var_Pnd_Orig_Fmv_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(36),pnd_Orig_Var_Pnd_Orig_Repayments_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZ9.99"),
                NEWLINE);
            if (Global.isEscape()) return;
            pnd_Orig_Var.reset();                                                                                                                                         //Natural: RESET #ORIG-VAR #PRT3
            pnd_Prt_Var_Pnd_Prt3.reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Pnd_Count.getValue(1).notEquals(getZero())))                                                                                                //Natural: IF #COUNT ( 1 ) NE 0
        {
            if (condition(pnd_Print_Source_Pnd_Print_Source_Upd.equals("   ")))                                                                                           //Natural: IF #PRINT-SOURCE-UPD = '   '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prt_Var_Pnd_Prt1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "/", pnd_Print_Source_Pnd_Print_Source_Upd));                               //Natural: COMPRESS '/' #PRINT-SOURCE-UPD TO #PRT1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Saver_Pnd_Sv_Status.equals("I")))                                                                                                           //Natural: IF #SV-STATUS = 'I'
            {
                pnd_Prt_Var_Pnd_Prt2.setValue(" - INACTIVATED");                                                                                                          //Natural: ASSIGN #PRT2 := ' - INACTIVATED'
            }                                                                                                                                                             //Natural: END-IF
            //*  DASDH
            //*  SAIK
            //*  SAIK
            pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_Source_Pnd_Print_Source_Org, pnd_Prt_Var_Pnd_Prt1,                    //Natural: COMPRESS #PRINT-SOURCE-ORG #PRT1 #PRT2 TO #PRT3 LEAVING NO SPACE
                pnd_Prt_Var_Pnd_Prt2));
            pnd_Amt.getValue(1).setValue(pnd_Var_Pnd_Classic_Amt.getValue(1));                                                                                            //Natural: ASSIGN #AMT ( 1 ) := #CLASSIC-AMT ( 1 )
            pnd_Amt.getValue(2).setValue(pnd_Var_Pnd_Roth_Amt.getValue(1));                                                                                               //Natural: ASSIGN #AMT ( 2 ) := #ROTH-AMT ( 1 )
            pnd_Amt.getValue(3).setValue(pnd_Var_Pnd_Sep_Amt.getValue(1));                                                                                                //Natural: ASSIGN #AMT ( 3 ) := #SEP-AMT ( 1 )
            pnd_Amt.getValue(4).setValue(pnd_Var_Pnd_Rollovr_Amt.getValue(1));                                                                                            //Natural: ASSIGN #AMT ( 4 ) := #ROLLOVR-AMT ( 1 )
            pnd_Amt.getValue(5).setValue(pnd_Var_Pnd_Rechar_Amt.getValue(1));                                                                                             //Natural: ASSIGN #AMT ( 5 ) := #RECHAR-AMT ( 1 )
            pnd_Amt.getValue(6).setValue(pnd_Var_Pnd_Conv_Amt.getValue(1));                                                                                               //Natural: ASSIGN #AMT ( 6 ) := #CONV-AMT ( 1 )
            pnd_Amt.getValue(7).setValue(pnd_Var_Pnd_Fmv_Amt.getValue(1));                                                                                                //Natural: ASSIGN #AMT ( 7 ) := #FMV-AMT ( 1 )
            pnd_Amt.getValue(8).setValue(pnd_Var_Pnd_Postpn_Amt.getValue(1));                                                                                             //Natural: ASSIGN #AMT ( 8 ) := #POSTPN-AMT ( 1 )
            pnd_Amt.getValue(9).setValue(pnd_Var_Pnd_Repayments_Amt.getValue(1));                                                                                         //Natural: ASSIGN #AMT ( 9 ) := #REPAYMENTS-AMT ( 1 )
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 9
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
            {
                pnd_Print_Amt.getValue(pnd_I).setValueEdited(pnd_Amt.getValue(pnd_I),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                                     //Natural: MOVE EDITED #AMT ( #I ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #PRINT-AMT ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Print_Cnt.setValueEdited(pnd_Var_Pnd_Count.getValue(1),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                           //Natural: MOVE EDITED #COUNT ( 1 ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #PRINT-CNT
            //*  BK
            //*  DASDH
            //*  SAIK
            getReports().write(1, ReportOption.NOTITLE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Print_Cnt,new ColumnSpacing(1),pnd_Print_Amt.getValue(1),new         //Natural: WRITE ( 1 ) #PRT3 1X #PRINT-CNT 1X #PRINT-AMT ( 1 ) 1X #PRINT-AMT ( 2 ) 1X #PRINT-AMT ( 3 ) 1X #PRINT-AMT ( 4 ) / 35T #PRINT-AMT ( 8 ) 55T #PRINT-AMT ( 5 ) 1X #PRINT-AMT ( 6 ) 1X #PRINT-AMT ( 7 ) / 35T #PRINT-AMT ( 9 ) /
                ColumnSpacing(1),pnd_Print_Amt.getValue(2),new ColumnSpacing(1),pnd_Print_Amt.getValue(3),new ColumnSpacing(1),pnd_Print_Amt.getValue(4),NEWLINE,new 
                TabSetting(35),pnd_Print_Amt.getValue(8),new TabSetting(55),pnd_Print_Amt.getValue(5),new ColumnSpacing(1),pnd_Print_Amt.getValue(6),new 
                ColumnSpacing(1),pnd_Print_Amt.getValue(7),NEWLINE,new TabSetting(35),pnd_Print_Amt.getValue(9),NEWLINE);
            if (Global.isEscape()) return;
            pnd_Var.getValue(1).reset();                                                                                                                                  //Natural: RESET #VAR ( 1 ) #PRINT-AMT ( * ) #AMT ( * ) #PRT-VAR #PRINT-CNT
            pnd_Print_Amt.getValue("*").reset();
            pnd_Amt.getValue("*").reset();
            pnd_Prt_Var.reset();
            pnd_Print_Cnt.reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Subtotal() throws Exception                                                                                                                    //Natural: PRINT-SUBTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------------
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-TOTAL
        sub_Print_Detail_Total();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_New_Company.getBoolean()))                                                                                                                      //Natural: IF #NEW-COMPANY
        {
            pnd_New_Company.setValue(false);                                                                                                                              //Natural: ASSIGN #NEW-COMPANY := FALSE
                                                                                                                                                                          //Natural: PERFORM SUBTOTAL-PRINT
            sub_Subtotal_Print();
            if (condition(Global.isEscape())) {return;}
            //*        -- WILL ONLY PRINT SUBTOTAL AT BREAK OF ORIGINAL SOURCE CODE --
            //*  SAME COMPANY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Saver_Pnd_Sv_Source_Org.equals(ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc())))                                                             //Natural: IF #SV-SOURCE-ORG = #F96-FF1.#TWRC-ORIG-SC
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM SUBTOTAL-PRINT
                sub_Subtotal_Print();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Subtotal_Print() throws Exception                                                                                                                    //Natural: SUBTOTAL-PRINT
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------------
        //*     -- PRINT REC.CNT OF ACTIVE RECORDS W/ 0 FINANCIAL AMTS.--
        pnd_Prt0.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_Source_Pnd_Print_Source_Org, " WITH ZERO AMT"));                                      //Natural: COMPRESS #PRINT-SOURCE-ORG ' WITH ZERO AMT' TO #PRT0 LEAVING NO SPACE
        pnd_Print_Cnt.setValueEdited(pnd_Zero_Amt,new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                                                //Natural: MOVE EDITED #ZERO-AMT ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #PRINT-CNT
        getReports().write(1, ReportOption.NOTITLE,pnd_Prt0,new ColumnSpacing(1),pnd_Print_Cnt);                                                                          //Natural: WRITE ( 1 ) #PRT0 1X #PRINT-CNT
        if (Global.isEscape()) return;
        pnd_Zero_Amt.reset();                                                                                                                                             //Natural: RESET #ZERO-AMT #PRINT-CNT
        pnd_Print_Cnt.reset();
        //*     -- PRINT SUBTOTAL --
        pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL ( ", pnd_Print_Source_Pnd_Print_Source_Org, ")"));                        //Natural: COMPRESS 'SUBTOTAL ( ' #PRINT-SOURCE-ORG ')' TO #PRT3 LEAVING NO SPACE
        //*  ENCLOSE NEGATIVE AMT & CNT W/ ( )
        if (condition(pnd_Var_Pnd_Count.getValue(2).less(getZero())))                                                                                                     //Natural: IF #COUNT ( 2 ) < 0
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Var_Pnd_Count.getValue(2),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                             //Natural: MOVE EDITED #COUNT ( 2 ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #TTL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Var_Pnd_Count.getValue(2),new ReportEditMask("'  'Z,ZZZ,ZZ9"));                                                                //Natural: MOVE EDITED #COUNT ( 2 ) ( EM = '  'Z,ZZZ,ZZ9 ) TO #TTL-CNT
            //*  DASDH
            //*  SAIK
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Amt.getValue(1).setValue(pnd_Var_Pnd_Classic_Amt.getValue(2));                                                                                           //Natural: ASSIGN #TEMP-AMT ( 1 ) := #CLASSIC-AMT ( 2 )
        pnd_Temp_Amt.getValue(2).setValue(pnd_Var_Pnd_Roth_Amt.getValue(2));                                                                                              //Natural: ASSIGN #TEMP-AMT ( 2 ) := #ROTH-AMT ( 2 )
        pnd_Temp_Amt.getValue(3).setValue(pnd_Var_Pnd_Sep_Amt.getValue(2));                                                                                               //Natural: ASSIGN #TEMP-AMT ( 3 ) := #SEP-AMT ( 2 )
        pnd_Temp_Amt.getValue(4).setValue(pnd_Var_Pnd_Rollovr_Amt.getValue(2));                                                                                           //Natural: ASSIGN #TEMP-AMT ( 4 ) := #ROLLOVR-AMT ( 2 )
        pnd_Temp_Amt.getValue(5).setValue(pnd_Var_Pnd_Rechar_Amt.getValue(2));                                                                                            //Natural: ASSIGN #TEMP-AMT ( 5 ) := #RECHAR-AMT ( 2 )
        pnd_Temp_Amt.getValue(6).setValue(pnd_Var_Pnd_Conv_Amt.getValue(2));                                                                                              //Natural: ASSIGN #TEMP-AMT ( 6 ) := #CONV-AMT ( 2 )
        pnd_Temp_Amt.getValue(7).setValue(pnd_Var_Pnd_Fmv_Amt.getValue(2));                                                                                               //Natural: ASSIGN #TEMP-AMT ( 7 ) := #FMV-AMT ( 2 )
        pnd_Temp_Amt.getValue(8).setValue(pnd_Var_Pnd_Postpn_Amt.getValue(2));                                                                                            //Natural: ASSIGN #TEMP-AMT ( 8 ) := #POSTPN-AMT ( 2 )
        pnd_Temp_Amt.getValue(9).setValue(pnd_Var_Pnd_Repayments_Amt.getValue(2));                                                                                        //Natural: ASSIGN #TEMP-AMT ( 9 ) := #REPAYMENTS-AMT ( 2 )
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 9
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            if (condition(pnd_Temp_Amt.getValue(pnd_I).less(getZero())))                                                                                                  //Natural: IF #TEMP-AMT ( #I ) < 0
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                                  //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("'  'ZZ,ZZZ,ZZZ,ZZ9.99"));                                     //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = '  'ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  BK
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(24),"-",new RepeatItem(9),new ColumnSpacing(3),"-",new RepeatItem(17),new ColumnSpacing(3),"-",new      //Natural: WRITE ( 1 ) 24T '-' ( 9 ) 3X '-' ( 17 ) 3X '-' ( 17 ) 3X '-' ( 17 ) 3X '-' ( 17 )
            RepeatItem(17),new ColumnSpacing(3),"-",new RepeatItem(17),new ColumnSpacing(3),"-",new RepeatItem(17));
        if (Global.isEscape()) return;
        //*  BK
        //*  DASDH
        //*  SAIK
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Ttl_Cnt,new ColumnSpacing(1),pnd_Ttl_Amt.getValue(1),new         //Natural: WRITE ( 1 ) / #PRT3 1X #TTL-CNT 1X #TTL-AMT ( 1 ) 1X #TTL-AMT ( 2 ) 1X #TTL-AMT ( 3 ) 1X #TTL-AMT ( 4 ) /35T #TTL-AMT ( 8 ) 55T #TTL-AMT ( 5 ) 1X #TTL-AMT ( 6 ) 1X #TTL-AMT ( 7 ) /35T #TTL-AMT ( 9 ) /
            ColumnSpacing(1),pnd_Ttl_Amt.getValue(2),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(3),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(4),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(8),new TabSetting(55),pnd_Ttl_Amt.getValue(5),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(6),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(7),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(9),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Ttl_Amt.getValue("*").reset();                                                                                                                                //Natural: RESET #TTL-AMT ( * ) #TTL-CNT #PRT3 #VAR ( 2 )
        pnd_Ttl_Cnt.reset();
        pnd_Prt_Var_Pnd_Prt3.reset();
        pnd_Var.getValue(2).reset();
    }
    private void sub_Print_Grandtotal() throws Exception                                                                                                                  //Natural: PRINT-GRANDTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------------
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTAL
        sub_Print_Subtotal();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_J.equals(3)))                                                                                                                                   //Natural: IF #J = 3
        {
            pnd_Prt_Var_Pnd_Prt3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL ( ", pnd_Print_Com, ")"));                                          //Natural: COMPRESS 'GRANDTOTAL ( ' #PRINT-COM ')' TO #PRT3 LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prt_Var_Pnd_Prt3.setValue("GRANDTOTAL (ALL)");                                                                                                            //Natural: MOVE 'GRANDTOTAL (ALL)' TO #PRT3
            //*  DASDH
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Temp_Amt.getValue(1).setValue(pnd_Var_Pnd_Classic_Amt.getValue(pnd_J));                                                                                       //Natural: ASSIGN #TEMP-AMT ( 1 ) := #CLASSIC-AMT ( #J )
        pnd_Temp_Amt.getValue(2).setValue(pnd_Var_Pnd_Roth_Amt.getValue(pnd_J));                                                                                          //Natural: ASSIGN #TEMP-AMT ( 2 ) := #ROTH-AMT ( #J )
        pnd_Temp_Amt.getValue(3).setValue(pnd_Var_Pnd_Sep_Amt.getValue(pnd_J));                                                                                           //Natural: ASSIGN #TEMP-AMT ( 3 ) := #SEP-AMT ( #J )
        pnd_Temp_Amt.getValue(4).setValue(pnd_Var_Pnd_Rollovr_Amt.getValue(pnd_J));                                                                                       //Natural: ASSIGN #TEMP-AMT ( 4 ) := #ROLLOVR-AMT ( #J )
        pnd_Temp_Amt.getValue(5).setValue(pnd_Var_Pnd_Rechar_Amt.getValue(pnd_J));                                                                                        //Natural: ASSIGN #TEMP-AMT ( 5 ) := #RECHAR-AMT ( #J )
        pnd_Temp_Amt.getValue(6).setValue(pnd_Var_Pnd_Conv_Amt.getValue(pnd_J));                                                                                          //Natural: ASSIGN #TEMP-AMT ( 6 ) := #CONV-AMT ( #J )
        pnd_Temp_Amt.getValue(7).setValue(pnd_Var_Pnd_Fmv_Amt.getValue(pnd_J));                                                                                           //Natural: ASSIGN #TEMP-AMT ( 7 ) := #FMV-AMT ( #J )
        pnd_Temp_Amt.getValue(8).setValue(pnd_Var_Pnd_Postpn_Amt.getValue(pnd_J));                                                                                        //Natural: ASSIGN #TEMP-AMT ( 8 ) := #POSTPN-AMT ( #J )
        pnd_Temp_Amt.getValue(9).setValue(pnd_Var_Pnd_Repayments_Amt.getValue(pnd_J));                                                                                    //Natural: ASSIGN #TEMP-AMT ( 9 ) := #REPAYMENTS-AMT ( #J )
        //*  REPRESENT NEGATIVE AMT & CNT W/ ( )
        if (condition(pnd_Var_Pnd_Count.getValue(pnd_J).less(getZero())))                                                                                                 //Natural: IF #COUNT ( #J ) < 0
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Var_Pnd_Count.getValue(pnd_J),new ReportEditMask("' ('Z,ZZZ,ZZ9')'"));                                                         //Natural: MOVE EDITED #COUNT ( #J ) ( EM = ' ('Z,ZZZ,ZZ9')' ) TO #TTL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ttl_Cnt.setValueEdited(pnd_Var_Pnd_Count.getValue(pnd_J),new ReportEditMask("'  'Z,ZZZ,ZZ9"));                                                            //Natural: MOVE EDITED #COUNT ( #J ) ( EM = '  'Z,ZZZ,ZZ9 ) TO #TTL-CNT
            //*  SAIK
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 9
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            if (condition(pnd_Temp_Amt.getValue(pnd_I).less(getZero())))                                                                                                  //Natural: IF #TEMP-AMT ( #I ) < 0
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("' ('ZZ,ZZZ,ZZZ,ZZ9.99')'"));                                  //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = ' ('ZZ,ZZZ,ZZZ,ZZ9.99')' ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ttl_Amt.getValue(pnd_I).setValueEdited(pnd_Temp_Amt.getValue(pnd_I),new ReportEditMask("'  'ZZ,ZZZ,ZZZ,ZZ9.99"));                                     //Natural: MOVE EDITED #TEMP-AMT ( #I ) ( EM = '  'ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #TTL-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  BK
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(24),"=",new RepeatItem(9),new ColumnSpacing(3),"=",new RepeatItem(17),new ColumnSpacing(3),"=",new      //Natural: WRITE ( 1 ) 24T '=' ( 9 ) 3X '=' ( 17 ) 3X '=' ( 17 ) 3X '=' ( 17 ) 3X '=' ( 17 )
            RepeatItem(17),new ColumnSpacing(3),"=",new RepeatItem(17),new ColumnSpacing(3),"=",new RepeatItem(17));
        if (Global.isEscape()) return;
        //*  BK
        //*  DASDH
        //*  SAIK
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Prt_Var_Pnd_Prt3,new ColumnSpacing(1),pnd_Ttl_Cnt,new ColumnSpacing(1),pnd_Ttl_Amt.getValue(1),new         //Natural: WRITE ( 1 ) / #PRT3 1X #TTL-CNT 1X #TTL-AMT ( 1 ) 1X #TTL-AMT ( 2 ) 1X #TTL-AMT ( 3 ) 1X #TTL-AMT ( 4 ) /35T #TTL-AMT ( 8 ) 55T #TTL-AMT ( 5 ) 1X #TTL-AMT ( 6 ) 1X #TTL-AMT ( 7 ) /35T #TTL-AMT ( 9 )
            ColumnSpacing(1),pnd_Ttl_Amt.getValue(2),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(3),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(4),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(8),new TabSetting(55),pnd_Ttl_Amt.getValue(5),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(6),new ColumnSpacing(1),pnd_Ttl_Amt.getValue(7),NEWLINE,new 
            TabSetting(35),pnd_Ttl_Amt.getValue(9));
        if (Global.isEscape()) return;
        pnd_Ttl_Amt.getValue("*").reset();                                                                                                                                //Natural: RESET #TTL-AMT ( * ) #TTL-CNT #PRT3 #VAR ( 3 )
        pnd_Ttl_Cnt.reset();
        pnd_Prt_Var_Pnd_Prt3.reset();
        pnd_Var.getValue(3).reset();
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE," TOTAL - - > ",pnd_Total_Zero_Amt);                                                //Natural: WRITE ( 2 ) NOTITLE NOHDR // ' TOTAL - - > ' #TOTAL-ZERO-AMT
        if (Global.isEscape()) return;
        pnd_Total_Zero_Amt.reset();                                                                                                                                       //Natural: RESET #TOTAL-ZERO-AMT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        short decideConditionsMet555 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Saver_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet555++;
            pnd_Print_Com.setValue("CREF");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet555++;
            pnd_Print_Com.setValue("TCII");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Saver_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet555++;
            pnd_Print_Com.setValue("TIAA");                                                                                                                               //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  DASDH
                    //*  SAIK
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 114T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 49T 'IRA5498 CONTRIBUTION FILE' 114T '(YTD-RUN)' / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TWRP0600-TAX-YEAR-CCYY /// 'COMPANY : ' #PRINT-COM // 27T 'TRANS' 4X 'IRA CONTRIBUTION' 3X 'IRA CONTRIBUTION' 4X 'IRA CONTRIBUTION' / 3X 'SYSTEM SOURCE' 28T 'COUNT' 5X 'CLASSIC AMOUNT' 6X 'ROTH  AMOUNT' 7X 'SEP  AMOUNT' 8X 'R/O CONTRIBUTION' / 38T 'LATE R/O AMT' 55T '------------------' 2X '------------------' 3X '------------------' / 38T 'REPAYMENTS' 58T 'RECHAR-AMOUNT' 4X 'CONVERSION AMOUNT' 4X 'FAIR-MKT-VAL AMT.' //
                        TabSetting(114),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(49),"IRA5498 CONTRIBUTION FILE",new TabSetting(114),"(YTD-RUN)",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(54),"TAX YEAR ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),NEWLINE,NEWLINE,NEWLINE,"COMPANY : ",pnd_Print_Com,NEWLINE,NEWLINE,new 
                        TabSetting(27),"TRANS",new ColumnSpacing(4),"IRA CONTRIBUTION",new ColumnSpacing(3),"IRA CONTRIBUTION",new ColumnSpacing(4),"IRA CONTRIBUTION",NEWLINE,new 
                        ColumnSpacing(3),"SYSTEM SOURCE",new TabSetting(28),"COUNT",new ColumnSpacing(5),"CLASSIC AMOUNT",new ColumnSpacing(6),"ROTH  AMOUNT",new 
                        ColumnSpacing(7),"SEP  AMOUNT",new ColumnSpacing(8),"R/O CONTRIBUTION",NEWLINE,new TabSetting(38),"LATE R/O AMT",new TabSetting(55),"------------------",new 
                        ColumnSpacing(2),"------------------",new ColumnSpacing(3),"------------------",NEWLINE,new TabSetting(38),"REPAYMENTS",new TabSetting(58),"RECHAR-AMOUNT",new 
                        ColumnSpacing(4),"CONVERSION AMOUNT",new ColumnSpacing(4),"FAIR-MKT-VAL AMT.",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 114T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 49T 'IRA5498 CONTRIBUTION FILE' 114T '(YTD-RUN)' / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TWRP0600-TAX-YEAR-CCYY / 40T 'LIST OF RECORDS WITH ZERO FINANCIAL AMOUNTS' // 'COMPANY : ' #PRINT-COM //
                        TabSetting(114),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(49),"IRA5498 CONTRIBUTION FILE",new TabSetting(114),"(YTD-RUN)",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(54),"TAX YEAR ",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy(),NEWLINE,new 
                        TabSetting(40),"LIST OF RECORDS WITH ZERO FINANCIAL AMOUNTS",NEWLINE,NEWLINE,"COMPANY : ",pnd_Print_Com,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().setDisplayColumns(2, "ORG/SRCD",
        		ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc(),"UPD/SRCD",
        		ldaTwrl190a.getPnd_F96_Ff1_Pnd_Twrc_Updt_Sc(),"STATUS",
        		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Status(),"TIN",
        		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Tax_Id(),"CONTRACT/NUMBER",
        		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Contract(),"PAYEE/CODE",
        		ldaTwrl190a.getPnd_F96_Ff1_Twrc_Payee());
    }
}
