/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:33 PM
**        * FROM NATURAL PROGRAM : Txwp5070
************************************************************
**        * FILE NAME            : Txwp5070.java
**        * CLASS NAME           : Txwp5070
**        * INSTANCE NAME        : Txwp5070
************************************************************
************************************************************************
* PROGRAM   : TXWP5070
* FUNCTION..: REPORT COUNTS FOR ALL NEWLY CREATED FEDERAL, STATE AND
*             LOCAL ELECTIONS FOR A GIVEN MONTH AND DISPLAY KEY DATA
* PROGRAMMER: J.ROTHOLZ (06/17/2002)
* HISTORY...:
*
* 12/19/2002  R.MA  FIX YTD CONTROL TOTAL OUT OF SYNC PROBLEM.
*                   ADD ELC-RES TO BREAK KEY TO DEFINE CASE.
*                   ADD NEW FIELD ELC-RES TO REPORT AND REARRANGE THE
*                   CONTROL TOTAL FIELDS.
*
* 09/01/2004  R.MA  CHANGE LOGIC IN PARM CARD MANIPULATION, SINCE THIS
*                   JOB IS SCHEDULED TO BE RUN ON THE LAST BUSINESS DAY
*                   INSTEAD OF THE FIRST DAY OF THE FOLLOWING MONTH.
*
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp5070 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_election_V;
    private DbsField election_V_Tin;
    private DbsField election_V_Tin_Type;
    private DbsField election_V_Contract_Nbr;
    private DbsField election_V_Payee_Cde;
    private DbsField election_V_Elc_Cde;
    private DbsField election_V_Elc_Type;
    private DbsField election_V_Elc_Res;
    private DbsField election_V_Active_Ind;
    private DbsField election_V_Create_Ts;
    private DbsField election_V_Lu_Ts;
    private DbsGroup election_V_CommentMuGroup;
    private DbsField election_V_Comment;
    private DbsField election_V_Super_2;
    private DbsField pnd_Date1_A;

    private DbsGroup pnd_Date1_A__R_Field_1;
    private DbsField pnd_Date1_A_Pnd_Date1_A_Yyyy;
    private DbsField pnd_Date1_A_Pnd_Date1_A_Mm;
    private DbsField pnd_Date1_A_Pnd_Date1_A_Dd;
    private DbsField pnd_Parm;

    private DbsGroup pnd_Parm__R_Field_2;
    private DbsField pnd_Parm_Pnd_Year;
    private DbsField pnd_Parm_Pnd_Month;
    private DbsField pnd_Parm_Pnd_Day1;
    private DbsField pnd_Month_Long;
    private DbsField pnd_Dummy_Ts;
    private DbsField pnd_Dummy_Date_Time;

    private DbsGroup pnd_Dummy_Date_Time__R_Field_3;
    private DbsField pnd_Dummy_Date_Time_Pnd_Ddt_Yyyy;
    private DbsField pnd_Dummy_Date_Time_Pnd_Ddt_Mm;
    private DbsField pnd_Dummy_Date_Time_Pnd_Ddt_Dd;
    private DbsField pnd_Dummy_Date_Time_Pnd_Ddt_Time;
    private DbsField pnd_Mo_Case_Ind;
    private DbsField pnd_Yr_Case_Ind;
    private DbsField pnd_Mo_Tran_Cnt;
    private DbsField pnd_Yr_Tran_Cnt;

    private DbsGroup pnd_Mo_Array;
    private DbsField pnd_Mo_Array_Pnd_Mo_Case;
    private DbsField pnd_Mo_Array_Pnd_Mo_Tran;

    private DbsGroup pnd_Yr_Array;
    private DbsField pnd_Yr_Array_Pnd_Yr_Case;
    private DbsField pnd_Yr_Array_Pnd_Yr_Tran;
    private DbsField pnd_Elc_Type_Decide;
    private DbsField pnd_Ndx;
    private DbsField pnd_Max_Read;
    private DbsField pnd_Date_D;

    private DbsRecord internalLoopRecord;
    private DbsField rD1Elc_TypeOld;
    private DbsField rD1TinOld;
    private DbsField rD1Tin_TypeOld;
    private DbsField rD1Contract_NbrOld;
    private DbsField rD1Payee_CdeOld;
    private DbsField rD1Elc_CdeOld;
    private DbsField rD1Elc_ResOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_election_V = new DataAccessProgramView(new NameInfo("vw_election_V", "ELECTION-V"), "TWR_ELECTION", "TWR_ELECTION");
        election_V_Tin = vw_election_V.getRecord().newFieldInGroup("election_V_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        election_V_Tin.setDdmHeader("TAX/ID/NUMBER");
        election_V_Tin_Type = vw_election_V.getRecord().newFieldInGroup("election_V_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        election_V_Tin_Type.setDdmHeader("TIN/TYPE");
        election_V_Contract_Nbr = vw_election_V.getRecord().newFieldInGroup("election_V_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        election_V_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        election_V_Payee_Cde = vw_election_V.getRecord().newFieldInGroup("election_V_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PAYEE_CDE");
        election_V_Payee_Cde.setDdmHeader("PAYEE/CODE");
        election_V_Elc_Cde = vw_election_V.getRecord().newFieldInGroup("election_V_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ELC_CDE");
        election_V_Elc_Cde.setDdmHeader("ELC/CDE");
        election_V_Elc_Type = vw_election_V.getRecord().newFieldInGroup("election_V_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_TYPE");
        election_V_Elc_Type.setDdmHeader("ELC/TYP");
        election_V_Elc_Res = vw_election_V.getRecord().newFieldInGroup("election_V_Elc_Res", "ELC-RES", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ELC_RES");
        election_V_Elc_Res.setDdmHeader("ELC/RES");
        election_V_Active_Ind = vw_election_V.getRecord().newFieldInGroup("election_V_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        election_V_Active_Ind.setDdmHeader("ACT/IND");
        election_V_Create_Ts = vw_election_V.getRecord().newFieldInGroup("election_V_Create_Ts", "CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CREATE_TS");
        election_V_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        election_V_Lu_Ts = vw_election_V.getRecord().newFieldInGroup("election_V_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        election_V_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        election_V_CommentMuGroup = vw_election_V.getRecord().newGroupInGroup("ELECTION_V_COMMENTMuGroup", "COMMENTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "TWR_ELECTION_COMMENT");
        election_V_Comment = election_V_CommentMuGroup.newFieldArrayInGroup("election_V_Comment", "COMMENT", FieldType.STRING, 50, new DbsArrayController(1, 
            1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "COMMENT");
        election_V_Comment.setDdmHeader("COMMENTS");
        election_V_Super_2 = vw_election_V.getRecord().newFieldInGroup("election_V_Super_2", "SUPER-2", FieldType.BINARY, 37, RepeatingFieldStrategy.None, 
            "SUPER_2");
        election_V_Super_2.setSuperDescriptor(true);
        registerRecord(vw_election_V);

        pnd_Date1_A = localVariables.newFieldInRecord("pnd_Date1_A", "#DATE1-A", FieldType.STRING, 8);

        pnd_Date1_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Date1_A__R_Field_1", "REDEFINE", pnd_Date1_A);
        pnd_Date1_A_Pnd_Date1_A_Yyyy = pnd_Date1_A__R_Field_1.newFieldInGroup("pnd_Date1_A_Pnd_Date1_A_Yyyy", "#DATE1-A-YYYY", FieldType.STRING, 4);
        pnd_Date1_A_Pnd_Date1_A_Mm = pnd_Date1_A__R_Field_1.newFieldInGroup("pnd_Date1_A_Pnd_Date1_A_Mm", "#DATE1-A-MM", FieldType.STRING, 2);
        pnd_Date1_A_Pnd_Date1_A_Dd = pnd_Date1_A__R_Field_1.newFieldInGroup("pnd_Date1_A_Pnd_Date1_A_Dd", "#DATE1-A-DD", FieldType.STRING, 2);
        pnd_Parm = localVariables.newFieldInRecord("pnd_Parm", "#PARM", FieldType.STRING, 8);

        pnd_Parm__R_Field_2 = localVariables.newGroupInRecord("pnd_Parm__R_Field_2", "REDEFINE", pnd_Parm);
        pnd_Parm_Pnd_Year = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Parm_Pnd_Month = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Month", "#MONTH", FieldType.STRING, 2);
        pnd_Parm_Pnd_Day1 = pnd_Parm__R_Field_2.newFieldInGroup("pnd_Parm_Pnd_Day1", "#DAY1", FieldType.STRING, 2);
        pnd_Month_Long = localVariables.newFieldInRecord("pnd_Month_Long", "#MONTH-LONG", FieldType.STRING, 9);
        pnd_Dummy_Ts = localVariables.newFieldInRecord("pnd_Dummy_Ts", "#DUMMY-TS", FieldType.TIME);
        pnd_Dummy_Date_Time = localVariables.newFieldInRecord("pnd_Dummy_Date_Time", "#DUMMY-DATE-TIME", FieldType.STRING, 15);

        pnd_Dummy_Date_Time__R_Field_3 = localVariables.newGroupInRecord("pnd_Dummy_Date_Time__R_Field_3", "REDEFINE", pnd_Dummy_Date_Time);
        pnd_Dummy_Date_Time_Pnd_Ddt_Yyyy = pnd_Dummy_Date_Time__R_Field_3.newFieldInGroup("pnd_Dummy_Date_Time_Pnd_Ddt_Yyyy", "#DDT-YYYY", FieldType.STRING, 
            4);
        pnd_Dummy_Date_Time_Pnd_Ddt_Mm = pnd_Dummy_Date_Time__R_Field_3.newFieldInGroup("pnd_Dummy_Date_Time_Pnd_Ddt_Mm", "#DDT-MM", FieldType.STRING, 
            2);
        pnd_Dummy_Date_Time_Pnd_Ddt_Dd = pnd_Dummy_Date_Time__R_Field_3.newFieldInGroup("pnd_Dummy_Date_Time_Pnd_Ddt_Dd", "#DDT-DD", FieldType.STRING, 
            2);
        pnd_Dummy_Date_Time_Pnd_Ddt_Time = pnd_Dummy_Date_Time__R_Field_3.newFieldInGroup("pnd_Dummy_Date_Time_Pnd_Ddt_Time", "#DDT-TIME", FieldType.STRING, 
            7);
        pnd_Mo_Case_Ind = localVariables.newFieldInRecord("pnd_Mo_Case_Ind", "#MO-CASE-IND", FieldType.BOOLEAN, 1);
        pnd_Yr_Case_Ind = localVariables.newFieldInRecord("pnd_Yr_Case_Ind", "#YR-CASE-IND", FieldType.BOOLEAN, 1);
        pnd_Mo_Tran_Cnt = localVariables.newFieldInRecord("pnd_Mo_Tran_Cnt", "#MO-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Yr_Tran_Cnt = localVariables.newFieldInRecord("pnd_Yr_Tran_Cnt", "#YR-TRAN-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Mo_Array = localVariables.newGroupArrayInRecord("pnd_Mo_Array", "#MO-ARRAY", new DbsArrayController(1, 3));
        pnd_Mo_Array_Pnd_Mo_Case = pnd_Mo_Array.newFieldInGroup("pnd_Mo_Array_Pnd_Mo_Case", "#MO-CASE", FieldType.PACKED_DECIMAL, 7);
        pnd_Mo_Array_Pnd_Mo_Tran = pnd_Mo_Array.newFieldInGroup("pnd_Mo_Array_Pnd_Mo_Tran", "#MO-TRAN", FieldType.PACKED_DECIMAL, 7);

        pnd_Yr_Array = localVariables.newGroupArrayInRecord("pnd_Yr_Array", "#YR-ARRAY", new DbsArrayController(1, 3));
        pnd_Yr_Array_Pnd_Yr_Case = pnd_Yr_Array.newFieldInGroup("pnd_Yr_Array_Pnd_Yr_Case", "#YR-CASE", FieldType.PACKED_DECIMAL, 7);
        pnd_Yr_Array_Pnd_Yr_Tran = pnd_Yr_Array.newFieldInGroup("pnd_Yr_Array_Pnd_Yr_Tran", "#YR-TRAN", FieldType.PACKED_DECIMAL, 7);
        pnd_Elc_Type_Decide = localVariables.newFieldInRecord("pnd_Elc_Type_Decide", "#ELC-TYPE-DECIDE", FieldType.STRING, 1);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 1);
        pnd_Max_Read = localVariables.newFieldInRecord("pnd_Max_Read", "#MAX-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rD1Elc_TypeOld = internalLoopRecord.newFieldInRecord("RD1_Elc_Type_OLD", "Elc_Type_OLD", FieldType.STRING, 1);
        rD1TinOld = internalLoopRecord.newFieldInRecord("RD1_tin_OLD", "tin_OLD", FieldType.STRING, 10);
        rD1Tin_TypeOld = internalLoopRecord.newFieldInRecord("RD1_Tin_Type_OLD", "Tin_Type_OLD", FieldType.STRING, 1);
        rD1Contract_NbrOld = internalLoopRecord.newFieldInRecord("RD1_Contract_Nbr_OLD", "Contract_Nbr_OLD", FieldType.STRING, 10);
        rD1Payee_CdeOld = internalLoopRecord.newFieldInRecord("RD1_Payee_Cde_OLD", "Payee_Cde_OLD", FieldType.STRING, 2);
        rD1Elc_CdeOld = internalLoopRecord.newFieldInRecord("RD1_Elc_Cde_OLD", "Elc_Cde_OLD", FieldType.NUMERIC, 3);
        rD1Elc_ResOld = internalLoopRecord.newFieldInRecord("RD1_Elc_Res_OLD", "Elc_Res_OLD", FieldType.STRING, 3);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_election_V.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Max_Read.setInitialValue(9000000);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp5070() throws Exception
    {
        super("Txwp5070");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Txwp5070|Main");
        setupReports();
        while(true)
        {
            try
            {
                pnd_Dummy_Date_Time.setValue("2000xx010000001");                                                                                                          //Natural: ASSIGN #DUMMY-DATE-TIME := '2000xx010000001'
                if (Global.isEscape()) return;                                                                                                                            //Natural: FORMAT ( 1 ) PS = 58 LS = 80;//Natural: WRITE ( 1 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 30T 'TIAA CREF Tax System' 67T 'Page  :' *PAGE-NUMBER ( 1 ) ( EM = Z,ZZ9 ) / *PROGRAM 17T 'Monthly Election Data Entry Control Report' 67T 'Report:  RPT0' / 34T 'Month: ' #MONTH-LONG //
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* *************************
                //*  M A I N    P R O G R A M
                //* *************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Parm_Pnd_Year,pnd_Parm_Pnd_Month);                                                                 //Natural: INPUT #YEAR #MONTH
                if (condition(pnd_Parm_Pnd_Year.equals(" ") && pnd_Parm_Pnd_Month.equals(" ")))                                                                           //Natural: IF #YEAR = ' ' AND #MONTH = ' '
                {
                    pnd_Parm.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM"));                                                                               //Natural: MOVE EDITED *DATX ( EM = YYYYMM ) TO #PARM
                    //* * MOVE '200408' TO #PARM
                    //*  MOVE '01' TO #DAY1
                    //*  MOVE EDITED #PARM TO #DATE-D (EM=YYYYMMDD)
                    //*  #DATE-D := #DATE-D - 1
                    //*  MOVE EDITED #DATE-D (EM=YYYYMM) TO #PARM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Parm_Pnd_Year.equals(" ") || pnd_Parm_Pnd_Month.equals(" ")))                                                                           //Natural: IF #YEAR = ' ' OR #MONTH = ' '
                {
                    getReports().write(1, NEWLINE,NEWLINE,"****************************************************","ERROR:  INPUT MONTH AND YEAR REQUIRED 'YYYY' & 'MM' ",  //Natural: WRITE ( 1 ) // '****************************************************' 'ERROR:  INPUT MONTH AND YEAR REQUIRED "YYYY" & "MM" ' '****************************************************'
                        "****************************************************");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(24);  if (true) return;                                                                                                             //Natural: TERMINATE 24
                }                                                                                                                                                         //Natural: END-IF
                pnd_Dummy_Date_Time_Pnd_Ddt_Mm.setValue(pnd_Parm_Pnd_Month);                                                                                              //Natural: ASSIGN #DDT-MM := #MONTH
                pnd_Dummy_Ts.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),pnd_Dummy_Date_Time);                                                                   //Natural: MOVE EDITED #DUMMY-DATE-TIME TO #DUMMY-TS ( EM = YYYYMMDDHHIISST )
                pnd_Month_Long.setValueEdited(pnd_Dummy_Ts,new ReportEditMask("LLLLLLLLL"));                                                                              //Natural: MOVE EDITED #DUMMY-TS ( EM = LLLLLLLLL ) TO #MONTH-LONG
                vw_election_V.startDatabaseRead                                                                                                                           //Natural: READ ( #MAX-READ ) ELECTION-V BY SUPER-2
                (
                "RD1",
                new Oc[] { new Oc("SUPER_2", "ASC") },
                pnd_Max_Read.getInt()
                );
                boolean endOfDataRd1 = true;
                boolean firstRd1 = true;
                RD1:
                while (condition(vw_election_V.readNextRow("RD1")))
                {
                    if (condition(vw_election_V.getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd1();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRd1 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT BREAK OF SUPER-2 /30/
                    if (condition(election_V_Comment.getValue(1).getSubstring(1,14).equals("Converted From")))                                                            //Natural: REJECT IF SUBSTR ( ELECTION-V.COMMENT ( 1 ) ,1,14 ) = 'Converted From'
                    {
                        continue;
                    }
                    //*         OR ELECTION-V.COMMENT(1)
                    //*            = 'The election was created during payment generation'
                    pnd_Date1_A.setValueEdited(election_V_Create_Ts,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED ELECTION-V.CREATE-TS ( EM = YYYYMMDD ) TO #DATE1-A
                    //*  CREATED IN SELECTED YEAR
                    //*  CUT OFF AT SELECTED MONTH      RM
                    if (condition(pnd_Date1_A_Pnd_Date1_A_Yyyy.equals(pnd_Parm_Pnd_Year) && pnd_Date1_A_Pnd_Date1_A_Mm.lessOrEqual(pnd_Parm_Pnd_Month)))                  //Natural: IF #DATE1-A-YYYY = #YEAR AND #DATE1-A-MM LE #MONTH
                    {
                        pnd_Yr_Case_Ind.setValue(true);                                                                                                                   //Natural: ASSIGN #YR-CASE-IND := TRUE
                        pnd_Yr_Tran_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #YR-TRAN-CNT
                        //*  CREATED IN SELECTED MONTH/YEAR
                        if (condition(pnd_Date1_A_Pnd_Date1_A_Mm.equals(pnd_Parm_Pnd_Month)))                                                                             //Natural: IF #DATE1-A-MM = #MONTH
                        {
                            pnd_Mo_Case_Ind.setValue(true);                                                                                                               //Natural: ASSIGN #MO-CASE-IND := TRUE
                            pnd_Mo_Tran_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #MO-TRAN-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    rD1Elc_TypeOld.setValue(election_V_Elc_Type);                                                                                                         //Natural: END-READ
                    rD1TinOld.setValue(election_V_Tin);
                    rD1Tin_TypeOld.setValue(election_V_Tin_Type);
                    rD1Contract_NbrOld.setValue(election_V_Contract_Nbr);
                    rD1Payee_CdeOld.setValue(election_V_Payee_Cde);
                    rD1Elc_CdeOld.setValue(election_V_Elc_Cde);
                    rD1Elc_ResOld.setValue(election_V_Elc_Res);
                }
                if (condition(vw_election_V.getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd1(endOfDataRd1);
                }
                if (Global.isEscape()) return;
                getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"-------------------- MONTHLY TOTALS --------------------",NEWLINE,"Federal Trans:",pnd_Mo_Array_Pnd_Mo_Tran.getValue(1),new  //Natural: WRITE ( 1 ) /// '-------------------- MONTHLY TOTALS --------------------' / 'Federal Trans:' #MO-TRAN ( 1 ) 10X 'Federal Cases:' #MO-CASE ( 1 ) / 'State   Trans:' #MO-TRAN ( 2 ) 10X 'State   Cases:' #MO-CASE ( 2 ) / 'Local   Trans:' #MO-TRAN ( 3 ) 10X 'Local   Cases:' #MO-CASE ( 3 ) /// '---------------------- YTD TOTALS ----------------------' / 'Federal Trans:' #YR-TRAN ( 1 ) 10X 'Federal Cases:' #YR-CASE ( 1 ) / 'State   Trans:' #YR-TRAN ( 2 ) 10X 'State   Cases:' #YR-CASE ( 2 ) / 'Local   Trans:' #YR-TRAN ( 3 ) 10X 'Local   Cases:' #YR-CASE ( 3 ) ///
                    ColumnSpacing(10),"Federal Cases:",pnd_Mo_Array_Pnd_Mo_Case.getValue(1),NEWLINE,"State   Trans:",pnd_Mo_Array_Pnd_Mo_Tran.getValue(2),new 
                    ColumnSpacing(10),"State   Cases:",pnd_Mo_Array_Pnd_Mo_Case.getValue(2),NEWLINE,"Local   Trans:",pnd_Mo_Array_Pnd_Mo_Tran.getValue(3),new 
                    ColumnSpacing(10),"Local   Cases:",pnd_Mo_Array_Pnd_Mo_Case.getValue(3),NEWLINE,NEWLINE,NEWLINE,"---------------------- YTD TOTALS ----------------------",NEWLINE,"Federal Trans:",pnd_Yr_Array_Pnd_Yr_Tran.getValue(1),new 
                    ColumnSpacing(10),"Federal Cases:",pnd_Yr_Array_Pnd_Yr_Case.getValue(1),NEWLINE,"State   Trans:",pnd_Yr_Array_Pnd_Yr_Tran.getValue(2),new 
                    ColumnSpacing(10),"State   Cases:",pnd_Yr_Array_Pnd_Yr_Case.getValue(2),NEWLINE,"Local   Trans:",pnd_Yr_Array_Pnd_Yr_Tran.getValue(3),new 
                    ColumnSpacing(10),"Local   Cases:",pnd_Yr_Array_Pnd_Yr_Case.getValue(3),NEWLINE,NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void atBreakEventRd1() throws Exception {atBreakEventRd1(false);}
    private void atBreakEventRd1(boolean endOfData) throws Exception
    {
        boolean election_V_Super_230IsBreak = election_V_Super_2.isBreak(endOfData);
        if (condition(election_V_Super_230IsBreak))
        {
            if (condition(pnd_Yr_Case_Ind.getBoolean()))                                                                                                                  //Natural: IF #YR-CASE-IND
            {
                pnd_Elc_Type_Decide.setValue(rD1Elc_TypeOld);                                                                                                             //Natural: ASSIGN #ELC-TYPE-DECIDE := OLD ( ELECTION-V.ELC-TYPE )
                short decideConditionsMet118 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #ELC-TYPE-DECIDE;//Natural: VALUE 'F'
                if (condition((pnd_Elc_Type_Decide.equals("F"))))
                {
                    decideConditionsMet118++;
                    pnd_Ndx.setValue(1);                                                                                                                                  //Natural: ASSIGN #NDX := 1
                }                                                                                                                                                         //Natural: VALUE 'S'
                else if (condition((pnd_Elc_Type_Decide.equals("S"))))
                {
                    decideConditionsMet118++;
                    pnd_Ndx.setValue(2);                                                                                                                                  //Natural: ASSIGN #NDX := 2
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Elc_Type_Decide.equals("T"))))
                {
                    decideConditionsMet118++;
                    pnd_Ndx.setValue(3);                                                                                                                                  //Natural: ASSIGN #NDX := 3
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Yr_Array_Pnd_Yr_Case.getValue(pnd_Ndx).nadd(1);                                                                                                       //Natural: ADD 1 TO #YR-CASE ( #NDX )
                pnd_Yr_Array_Pnd_Yr_Tran.getValue(pnd_Ndx).nadd(pnd_Yr_Tran_Cnt);                                                                                         //Natural: ADD #YR-TRAN-CNT TO #YR-TRAN ( #NDX )
                pnd_Yr_Case_Ind.resetInitial();                                                                                                                           //Natural: RESET INITIAL #YR-CASE-IND #YR-TRAN-CNT
                pnd_Yr_Tran_Cnt.resetInitial();
                if (condition(pnd_Mo_Case_Ind.getBoolean()))                                                                                                              //Natural: IF #MO-CASE-IND
                {
                    pnd_Mo_Array_Pnd_Mo_Case.getValue(pnd_Ndx).nadd(1);                                                                                                   //Natural: ADD 1 TO #MO-CASE ( #NDX )
                    pnd_Mo_Array_Pnd_Mo_Tran.getValue(pnd_Ndx).nadd(pnd_Mo_Tran_Cnt);                                                                                     //Natural: ADD #MO-TRAN-CNT TO #MO-TRAN ( #NDX )
                    pnd_Mo_Case_Ind.resetInitial();                                                                                                                       //Natural: RESET INITIAL #MO-CASE-IND #MO-TRAN-CNT
                    pnd_Mo_Tran_Cnt.resetInitial();
                    getReports().display(1, "TIN",                                                                                                                        //Natural: DISPLAY ( 1 ) 'TIN' OLD ( ELECTION-V.TIN ) 'TIN/TYPE' OLD ( ELECTION-V.TIN-TYPE ) 'CONTRACT/NUMBER' OLD ( ELECTION-V.CONTRACT-NBR ) 'PAYEE/CODE' OLD ( ELECTION-V.PAYEE-CDE ) 'ELECTION/CODE' OLD ( ELECTION-V.ELC-CDE ) 'ELECTION/TYPE' OLD ( ELECTION-V.ELC-TYPE ) 'ELECTION/RES ' OLD ( ELECTION-V.ELC-RES )
                    		rD1TinOld,"TIN/TYPE",
                    		rD1Tin_TypeOld,"CONTRACT/NUMBER",
                    		rD1Contract_NbrOld,"PAYEE/CODE",
                    		rD1Payee_CdeOld,"ELECTION/CODE",
                    		rD1Elc_CdeOld,"ELECTION/TYPE",
                    		rD1Elc_TypeOld,"ELECTION/RES ",
                    		rD1Elc_ResOld);
                    if (condition(Global.isEscape())) return;
                    //*          'CREATE/MOD/DATE'  OLD(ELECTION-V.CREATE-TS)(EM=MM'/'DD'/'YY)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=80");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(30),"TIAA CREF Tax System",new 
            TabSetting(67),"Page  :",getReports().getPageNumberDbs(1), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(17),"Monthly Election Data Entry Control Report",new 
            TabSetting(67),"Report:  RPT0",NEWLINE,new TabSetting(34),"Month: ",pnd_Month_Long,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "TIN",
        		rD1TinOld,"TIN/TYPE",
        		rD1Tin_TypeOld,"CONTRACT/NUMBER",
        		rD1Contract_NbrOld,"PAYEE/CODE",
        		rD1Payee_CdeOld,"ELECTION/CODE",
        		rD1Elc_CdeOld,"ELECTION/TYPE",
        		rD1Elc_TypeOld,"ELECTION/RES ",
        		rD1Elc_ResOld);
    }
}
