/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:46:04 PM
**        * FROM NATURAL PROGRAM : Txwp2905
************************************************************
**        * FILE NAME            : Txwp2905.java
**        * CLASS NAME           : Txwp2905
**        * INSTANCE NAME        : Txwp2905
************************************************************
************************************************************************
* SUBPROGRAM: TXWP2905
* FUNCTION..: UPDATE REFERENCE TABLE (112) CONTROL DATE RECORD
*             ASSIGN IA-CUTOFF DATE VALUE TO CUTOFF DATE 2
* PROGRAMMER: J.ROTHOLZ (06/13/2002)
* HISTORY...:
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Txwp2905 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ref;
    private DbsField ref_Rt_Table_Id;
    private DbsField ref_Rt_A_I_Ind;
    private DbsField ref_Rt_Desc5;

    private DbsGroup ref__R_Field_1;
    private DbsField ref_Pnd_Rt_Desc5_Inverse_Effective_Ts;
    private DbsField ref_Pnd_Rt_Desc5_Effective_Ts;
    private DbsField ref_Pnd_Rt_Desc5_Create_User_Id;
    private DbsField ref_Pnd_Rt_Desc5_Create_Ts;
    private DbsField ref_Pnd_Rt_Desc5_Lu_User_Id;
    private DbsField ref_Pnd_Rt_Desc5_Lu_Ts;
    private DbsField ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2;

    private DbsGroup ref__R_Field_2;
    private DbsField ref_Pnd_Ia_Cutoff_Date_2_Alpha;
    private DbsField pnd_Low_Val;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_3;
    private DbsField pnd_Key_Pnd_Key_Table_Id;
    private DbsField pnd_Key_Pnd_Key_A_I_Ind;
    private DbsField pnd_Show_Field;

    private DbsGroup pnd_Show_Field__R_Field_4;
    private DbsField pnd_Show_Field_Pnd_Sf_P13;
    private DbsField pnd_Show_Field_Pnd_Sf_Ts1;
    private DbsField pnd_Show_Field_Pnd_Sf_User1;
    private DbsField pnd_Show_Field_Pnd_Sf_Ts2;
    private DbsField pnd_Show_Field_Pnd_Sf_User2;
    private DbsField pnd_Show_Field_Pnd_Sf_Ts3;
    private DbsField pnd_Show_Field_Pnd_Sf_Ts4;

    private DbsGroup pnd_Show_Field__R_Field_5;
    private DbsField pnd_Show_Field_Pnd_Sf_Ts4_Alpha;

    private DbsGroup pnd_Show_Field__R_Field_6;
    private DbsField pnd_Show_Field_Pnd_Show_Field_Part1;
    private DbsField pnd_Show_Field_Pnd_Show_Field_Part2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_ref = new DataAccessProgramView(new NameInfo("vw_ref", "REF"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ref_Rt_Table_Id = vw_ref.getRecord().newFieldInGroup("ref_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ref_Rt_A_I_Ind = vw_ref.getRecord().newFieldInGroup("ref_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Rt_Desc5 = vw_ref.getRecord().newFieldInGroup("ref_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");

        ref__R_Field_1 = vw_ref.getRecord().newGroupInGroup("ref__R_Field_1", "REDEFINE", ref_Rt_Desc5);
        ref_Pnd_Rt_Desc5_Inverse_Effective_Ts = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Inverse_Effective_Ts", "#RT-DESC5-INVERSE-EFFECTIVE-TS", 
            FieldType.PACKED_DECIMAL, 13);
        ref_Pnd_Rt_Desc5_Effective_Ts = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Effective_Ts", "#RT-DESC5-EFFECTIVE-TS", FieldType.TIME);
        ref_Pnd_Rt_Desc5_Create_User_Id = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Create_User_Id", "#RT-DESC5-CREATE-USER-ID", FieldType.STRING, 
            8);
        ref_Pnd_Rt_Desc5_Create_Ts = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Create_Ts", "#RT-DESC5-CREATE-TS", FieldType.TIME);
        ref_Pnd_Rt_Desc5_Lu_User_Id = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Lu_User_Id", "#RT-DESC5-LU-USER-ID", FieldType.STRING, 8);
        ref_Pnd_Rt_Desc5_Lu_Ts = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Lu_Ts", "#RT-DESC5-LU-TS", FieldType.TIME);
        ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2 = ref__R_Field_1.newFieldInGroup("ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2", "#RT-DESC5-IA-CUTOFF-DATE-2", FieldType.TIME);

        ref__R_Field_2 = ref__R_Field_1.newGroupInGroup("ref__R_Field_2", "REDEFINE", ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2);
        ref_Pnd_Ia_Cutoff_Date_2_Alpha = ref__R_Field_2.newFieldInGroup("ref_Pnd_Ia_Cutoff_Date_2_Alpha", "#IA-CUTOFF-DATE-2-ALPHA", FieldType.STRING, 
            7);
        registerRecord(vw_ref);

        pnd_Low_Val = localVariables.newFieldInRecord("pnd_Low_Val", "#LOW-VAL", FieldType.STRING, 1);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 7);

        pnd_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Key__R_Field_3", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Key_Table_Id = pnd_Key__R_Field_3.newFieldInGroup("pnd_Key_Pnd_Key_Table_Id", "#KEY-TABLE-ID", FieldType.STRING, 5);
        pnd_Key_Pnd_Key_A_I_Ind = pnd_Key__R_Field_3.newFieldInGroup("pnd_Key_Pnd_Key_A_I_Ind", "#KEY-A-I-IND", FieldType.STRING, 1);
        pnd_Show_Field = localVariables.newFieldInRecord("pnd_Show_Field", "#SHOW-FIELD", FieldType.STRING, 51);

        pnd_Show_Field__R_Field_4 = localVariables.newGroupInRecord("pnd_Show_Field__R_Field_4", "REDEFINE", pnd_Show_Field);
        pnd_Show_Field_Pnd_Sf_P13 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_P13", "#SF-P13", FieldType.PACKED_DECIMAL, 13);
        pnd_Show_Field_Pnd_Sf_Ts1 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_Ts1", "#SF-TS1", FieldType.TIME);
        pnd_Show_Field_Pnd_Sf_User1 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_User1", "#SF-USER1", FieldType.STRING, 8);
        pnd_Show_Field_Pnd_Sf_Ts2 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_Ts2", "#SF-TS2", FieldType.TIME);
        pnd_Show_Field_Pnd_Sf_User2 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_User2", "#SF-USER2", FieldType.STRING, 8);
        pnd_Show_Field_Pnd_Sf_Ts3 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_Ts3", "#SF-TS3", FieldType.TIME);
        pnd_Show_Field_Pnd_Sf_Ts4 = pnd_Show_Field__R_Field_4.newFieldInGroup("pnd_Show_Field_Pnd_Sf_Ts4", "#SF-TS4", FieldType.TIME);

        pnd_Show_Field__R_Field_5 = pnd_Show_Field__R_Field_4.newGroupInGroup("pnd_Show_Field__R_Field_5", "REDEFINE", pnd_Show_Field_Pnd_Sf_Ts4);
        pnd_Show_Field_Pnd_Sf_Ts4_Alpha = pnd_Show_Field__R_Field_5.newFieldInGroup("pnd_Show_Field_Pnd_Sf_Ts4_Alpha", "#SF-TS4-ALPHA", FieldType.STRING, 
            7);

        pnd_Show_Field__R_Field_6 = localVariables.newGroupInRecord("pnd_Show_Field__R_Field_6", "REDEFINE", pnd_Show_Field);
        pnd_Show_Field_Pnd_Show_Field_Part1 = pnd_Show_Field__R_Field_6.newFieldInGroup("pnd_Show_Field_Pnd_Show_Field_Part1", "#SHOW-FIELD-PART1", FieldType.STRING, 
            25);
        pnd_Show_Field_Pnd_Show_Field_Part2 = pnd_Show_Field__R_Field_6.newFieldInGroup("pnd_Show_Field_Pnd_Show_Field_Part2", "#SHOW-FIELD-PART2", FieldType.STRING, 
            26);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ref.reset();

        localVariables.reset();
        pnd_Low_Val.setInitialValue("H'00'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Txwp2905() throws Exception
    {
        super("Txwp2905");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 23 LS = 80;//Natural: WRITE ( 0 ) TITLE LEFT *TIMX ( EM = MM/DD/YYYY�HH:IIAP ) 30T 'TIAA CREF Tax System' 67T 'Page  :' *PAGE-NUMBER ( 0 ) ( EM = Z,ZZ9 ) / *PROGRAM 23T 'IA CUTOFF TABLE UPDATE FOR AP LOAD' 67T 'Report:  RPT0' / 34T 'Before/After' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *************************
        //*  M A I N    P R O G R A M
        //* *************************
        pnd_Key_Pnd_Key_Table_Id.setValue("TX005");                                                                                                                       //Natural: ASSIGN #KEY-TABLE-ID := 'TX005'
        pnd_Key_Pnd_Key_A_I_Ind.setValue("A");                                                                                                                            //Natural: ASSIGN #KEY-A-I-IND := 'A'
        setValueToSubstring(pnd_Low_Val,pnd_Key,7,1);                                                                                                                     //Natural: MOVE #LOW-VAL TO SUBSTR ( #KEY,7,1 )
        vw_ref.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) REF BY RT-SUPER5 FROM #KEY
        (
        "RD1",
        new Wc[] { new Wc("RT_SUPER5", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER5", "ASC") },
        1
        );
        RD1:
        while (condition(vw_ref.readNextRow("RD1")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (DbsUtil.is(ref_Pnd_Ia_Cutoff_Date_2_Alpha.getText(),"T"))))                                                                                      //Natural: IF NOT ( #IA-CUTOFF-DATE-2-ALPHA IS ( T ) )
        {
            ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2.setValueEdited(new ReportEditMask("YYYYMMDDHHIISST"),"200001010000001");                                                    //Natural: MOVE EDITED '200001010000001' TO #RT-DESC5-IA-CUTOFF-DATE-2 ( EM = YYYYMMDDHHIISST )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ref_Rt_Table_Id.equals(pnd_Key_Pnd_Key_Table_Id) && ref_Rt_A_I_Ind.equals(pnd_Key_Pnd_Key_A_I_Ind)))                                                //Natural: IF REF.RT-TABLE-ID = #KEY-TABLE-ID AND REF.RT-A-I-IND = #KEY-A-I-IND
        {
            if (condition(ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2.equals(ref_Pnd_Rt_Desc5_Effective_Ts)))                                                                       //Natural: IF #RT-DESC5-IA-CUTOFF-DATE-2 = #RT-DESC5-EFFECTIVE-TS
            {
                getReports().write(0, NEWLINE,"*************************************************",NEWLINE,"WARNING:  Cutoff Dates are Equal (No Update)     ",            //Natural: WRITE / '*************************************************' / 'WARNING:  Cutoff Dates are Equal (No Update)     ' / '*************************************************' /
                    NEWLINE,"*************************************************",NEWLINE);
                if (Global.isEscape()) return;
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            GT1:                                                                                                                                                          //Natural: GET REF *ISN ( RD1. )
            vw_ref.readByID(vw_ref.getAstISN("RD1"), "GT1");
            pnd_Show_Field.setValue(ref_Rt_Desc5.getSubstring(1,51));                                                                                                     //Natural: MOVE SUBSTR ( RT-DESC5,1,51 ) TO #SHOW-FIELD
            if (condition(! (DbsUtil.is(pnd_Show_Field_Pnd_Sf_Ts4_Alpha.getText(),"T"))))                                                                                 //Natural: IF NOT ( #SF-TS4-ALPHA IS ( T ) )
            {
                pnd_Show_Field_Pnd_Sf_Ts4.reset();                                                                                                                        //Natural: RESET #SF-TS4
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(0, "IA Cutoff/Date",                                                                                                                     //Natural: DISPLAY 'IA Cutoff/Date' #SF-TS1 ( EM = MM'/'DD'/'YY ) 'Create/User' #SF-USER1 'Create/Date' #SF-TS2 ( EM = MM'/'DD'/'YY ) 'Last Update/User' #SF-USER2 'LAST UPDATE/DATE TIME' #SF-TS3 'IA CUTOFF/DATE 2' #SF-TS4 ( EM = MM'/'DD'/'YY )
            		pnd_Show_Field_Pnd_Sf_Ts1, new ReportEditMask ("MM'/'DD'/'YY"),"Create/User",
            		pnd_Show_Field_Pnd_Sf_User1,"Create/Date",
            		pnd_Show_Field_Pnd_Sf_Ts2, new ReportEditMask ("MM'/'DD'/'YY"),"Last Update/User",
            		pnd_Show_Field_Pnd_Sf_User2,"LAST UPDATE/DATE TIME",
            		pnd_Show_Field_Pnd_Sf_Ts3,"IA CUTOFF/DATE 2",
            		pnd_Show_Field_Pnd_Sf_Ts4, new ReportEditMask ("MM'/'DD'/'YY"));
            if (Global.isEscape()) return;
            ref_Pnd_Rt_Desc5_Lu_User_Id.setValue(Global.getINIT_USER());                                                                                                  //Natural: ASSIGN #RT-DESC5-LU-USER-ID := *INIT-USER
            ref_Pnd_Rt_Desc5_Lu_Ts.setValue(Global.getTIMX());                                                                                                            //Natural: ASSIGN #RT-DESC5-LU-TS := *TIMX
            ref_Pnd_Rt_Desc5_Ia_Cutoff_Date_2.setValue(ref_Pnd_Rt_Desc5_Effective_Ts);                                                                                    //Natural: ASSIGN #RT-DESC5-IA-CUTOFF-DATE-2 := #RT-DESC5-EFFECTIVE-TS
            pnd_Show_Field.setValue(ref_Rt_Desc5.getSubstring(1,51));                                                                                                     //Natural: MOVE SUBSTR ( RT-DESC5,1,51 ) TO #SHOW-FIELD
            getReports().write(0, new TabSetting(1),pnd_Show_Field_Pnd_Sf_Ts1, new ReportEditMask ("MM'/'DD'/'YY"),new TabSetting(11),pnd_Show_Field_Pnd_Sf_User1,new     //Natural: WRITE 01T #SF-TS1 ( EM = MM'/'DD'/'YY ) 11T #SF-USER1 20T #SF-TS2 ( EM = MM'/'DD'/'YY ) 29T #SF-USER2 41T #SF-TS3 53T #SF-TS4 ( EM = MM'/'DD'/'YY )
                TabSetting(20),pnd_Show_Field_Pnd_Sf_Ts2, new ReportEditMask ("MM'/'DD'/'YY"),new TabSetting(29),pnd_Show_Field_Pnd_Sf_User2,new TabSetting(41),pnd_Show_Field_Pnd_Sf_Ts3,new 
                TabSetting(53),pnd_Show_Field_Pnd_Sf_Ts4, new ReportEditMask ("MM'/'DD'/'YY"));
            if (Global.isEscape()) return;
            vw_ref.updateDBRow("GT1");                                                                                                                                    //Natural: UPDATE ( GT1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, NEWLINE,"*************************************************",NEWLINE,"JOB FAILED: No Active IA Cutoff Date Record Found",                //Natural: WRITE / '*************************************************' / 'JOB FAILED: No Active IA Cutoff Date Record Found' / '*************************************************' /
                NEWLINE,"*************************************************",NEWLINE);
            if (Global.isEscape()) return;
            //*  TERMINATE 24
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=80");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:IIAP"),new TabSetting(30),"TIAA CREF Tax System",new 
            TabSetting(67),"Page  :",getReports().getPageNumberDbs(0), new ReportEditMask ("Z,ZZ9"),NEWLINE,Global.getPROGRAM(),new TabSetting(23),"IA CUTOFF TABLE UPDATE FOR AP LOAD",new 
            TabSetting(67),"Report:  RPT0",NEWLINE,new TabSetting(34),"Before/After",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, "IA Cutoff/Date",
        		pnd_Show_Field_Pnd_Sf_Ts1, new ReportEditMask ("MM'/'DD'/'YY"),"Create/User",
        		pnd_Show_Field_Pnd_Sf_User1,"Create/Date",
        		pnd_Show_Field_Pnd_Sf_Ts2, new ReportEditMask ("MM'/'DD'/'YY"),"Last Update/User",
        		pnd_Show_Field_Pnd_Sf_User2,"LAST UPDATE/DATE TIME",
        		pnd_Show_Field_Pnd_Sf_Ts3,"IA CUTOFF/DATE 2",
        		pnd_Show_Field_Pnd_Sf_Ts4, new ReportEditMask ("MM'/'DD'/'YY"));
    }
}
