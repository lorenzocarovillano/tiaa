/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:42:13 PM
**        * FROM NATURAL PROGRAM : Twrp5905
************************************************************
**        * FILE NAME            : Twrp5905.java
**        * CLASS NAME           : Twrp5905
**        * INSTANCE NAME        : Twrp5905
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: 1042-S IRS REPORTING EXTRACT
**SAG SYSTEM: TAX
************************************************************************
* PROGRAM  : TWRP5905
* SYSTEM   : TAX
* TITLE    : 1042-S IRS REPORTING EXTRACT
* GENERATED: NOV 23,99 AT 10:37 AM
* FUNCTION : THIS PROGRAM EXTRACT ALL ACTIVE 1042-S FORMS FROM FORM
*          | FILE DATABASE.
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* NOV 21 00  F.ORTIZ  ADD TEST INDICATOR FOR CREATING AN IRS 1042-S
*                     TEST FILE.
* DEC 20 01  F.ORTIZ  MODIFIED PGM FOR NEW IRS LAYOUT.
* NOV 15 06  R.MA     RECOMPILED FOR UPDATED LDA(TWRL9705).
* 01/30/08 : A. YOUNG - REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
* 03/18/08 : R. MA    UPDATE TO ACCEPT A DIFFERENT WRKF1 INPUT -
*                     P1070TW1 (TAX YEAR IS NOW -1).
*                     THEN IT IS DECIDED BY CALCULATION IN THIS PROGRAM.
* 01/22/09 : A. YOUNG - REVISED TO ACCEPT AND PROCESS NUMERIC TAX YEAR.
*          :          - REVISED TO ENSURE ' ' INPUT DATA WILL DEFAULT
*          :            TO PREVIOUS TAX YEAR.
* 12/2014  : RCC      - RESTOW. CHANGE IN TWRL9705
* 02/2016  : DEBASISH - RE-STOWED AS LDA TWRL9705 WAS MODIFIED FOR FIN
*                       UPGRADE.
* 12/2016  : SNEHA    - RE-STOWED AS LDA TWRL9705 WAS MODIFIED
**
* 4/21/2017 - WEBBJ - PIN EXPANSION. STOW ONLY
**
* 12/26/2017 - SAI.K - RESTOW FOR TWRL9705 1042-S 2017 CHANGES
**    10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5905 extends BLNatBase
{
    // Data Areas
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwrafrmn pdaTwrafrmn;
    private LdaTwrl9705 ldaTwrl9705;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Pnd_Tax_Year;
    private DbsField pnd_Key_Pnd_Active_Ind;
    private DbsField pnd_Key_Pnd_Form_Type;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Empty_Form_Cnt;
    private DbsField pnd_Irs_Reject_Cnt;
    private DbsField pnd_Test_Ind;
    private DbsField pnd_Datn;
    private DbsField pnd_Ws_Tax_Year;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 33);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Tax_Year = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Key_Pnd_Active_Ind = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Active_Ind", "#ACTIVE-IND", FieldType.STRING, 1);
        pnd_Key_Pnd_Form_Type = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Empty_Form_Cnt = localVariables.newFieldInRecord("pnd_Empty_Form_Cnt", "#EMPTY-FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irs_Reject_Cnt = localVariables.newFieldInRecord("pnd_Irs_Reject_Cnt", "#IRS-REJECT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Test_Ind = localVariables.newFieldInRecord("pnd_Test_Ind", "#TEST-IND", FieldType.STRING, 4);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.NUMERIC, 4);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9705.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5905() throws Exception
    {
        super("Twrp5905");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*    03/18/08  RM
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATX ( EM = MM/DD/YYYY ) '-' *TIMX ( EM = HH:IIAP ) 48T 'Tax Withholding and Reporting System' 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 44T '1042-S IRS Original Reporting Control Report' 120T 'Report: RPT1' // 10T 'Tax Year: ' #WS-TAX-YEAR ( EM = 9999 ) //
        pnd_Key.reset();                                                                                                                                                  //Natural: RESET #KEY
        getWorkFiles().read(1, pnd_Key_Pnd_Tax_Year, pnd_Test_Ind);                                                                                                       //Natural: READ WORK FILE 1 ONCE #KEY.#TAX-YEAR #TEST-IND
        //*                                                START CHG   03/18/08  RM
        //*  IF #KEY.#TAX-YEAR NE MASK (NNNN) AND #KEY.#TAX-YEAR NE ' '
        //*   WRITE 'INVALID TAX YEAR FORMAT.' #KEY.#TAX-YEAR 'PROGRAM TERMINATED.'
        //*   TERMINATE 16
        //*  END-IF
        pnd_Datn.compute(new ComputeParameters(false, pnd_Datn), Global.getDATN().divide(10000));                                                                         //Natural: ASSIGN #DATN := *DATN / 10000
        //*  NEXT YEAR
        //*  CURRENT YR       /* 01/22/09
        //*  CURRENT YR - 1   /* 01/22/09
        //*  CURRENT YR - 2
        //*  CURRENT YR - 3
        short decideConditionsMet401 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #KEY.#TAX-YEAR;//Natural: VALUE '+1'
        if (condition((pnd_Key_Pnd_Tax_Year.equals("+1"))))
        {
            decideConditionsMet401++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.add(1));                                                                      //Natural: ASSIGN #WS-TAX-YEAR := #DATN + 1
            //*  VALUE ' ', '0'
        }                                                                                                                                                                 //Natural: VALUE '0'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("0"))))
        {
            decideConditionsMet401++;
            pnd_Ws_Tax_Year.setValue(pnd_Datn);                                                                                                                           //Natural: ASSIGN #WS-TAX-YEAR := #DATN
            //*  VALUE '-1'
        }                                                                                                                                                                 //Natural: VALUE ' ', '-1'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals(" ") || pnd_Key_Pnd_Tax_Year.equals("-1"))))
        {
            decideConditionsMet401++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(1));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 1
        }                                                                                                                                                                 //Natural: VALUE '-2'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("-2"))))
        {
            decideConditionsMet401++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(2));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 2
        }                                                                                                                                                                 //Natural: VALUE '-3'
        else if (condition((pnd_Key_Pnd_Tax_Year.equals("-3"))))
        {
            decideConditionsMet401++;
            pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Datn.subtract(3));                                                                 //Natural: ASSIGN #WS-TAX-YEAR := #DATN - 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*    IGNORE                                                 /* 01/22/09
            //*  01/22/09
            if (condition(DbsUtil.maskMatches(pnd_Key_Pnd_Tax_Year,"NNNN")))                                                                                              //Natural: IF #KEY.#TAX-YEAR = MASK ( NNNN )
            {
                pnd_Ws_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Tax_Year), pnd_Key_Pnd_Tax_Year.val());                                                       //Natural: ASSIGN #WS-TAX-YEAR := VAL ( #KEY.#TAX-YEAR )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "Invalid Tax Year Input",pnd_Key_Pnd_Tax_Year,"...  Program Terminated.");                                                          //Natural: WRITE 'Invalid Tax Year Input' #KEY.#TAX-YEAR '...  Program Terminated.'
                if (Global.isEscape()) return;
                DbsUtil.terminate(16);  if (true) return;                                                                                                                 //Natural: TERMINATE 16
                //*  01/22/09
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  IF #KEY.#TAX-YEAR = ' '
        //*    #KEY.#TAX-YEAR := *DATN / 10000 - 1
        //*  END-IF
        //*                                               END CHG     03/18/08  RM
        if (condition((pnd_Test_Ind.greater(" ")) && (pnd_Test_Ind.notEquals("TEST"))))                                                                                   //Natural: IF ( #TEST-IND GT ' ' ) AND ( #TEST-IND NE 'TEST' )
        {
            getReports().write(0, "INVALID TEST INDICATOR:",pnd_Test_Ind,"PROGRAM TERMINATED.");                                                                          //Natural: WRITE 'INVALID TEST INDICATOR:' #TEST-IND 'PROGRAM TERMINATED.'
            if (Global.isEscape()) return;
            DbsUtil.terminate(16);  if (true) return;                                                                                                                     //Natural: TERMINATE 16
            //*    03/18/08  RM
            //*  1042-S
        }                                                                                                                                                                 //Natural: END-IF
        //*   CHECK CONTROL FILE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);                                                                                             //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS-TAX-YEAR
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Tax_Year);
        //*                              := VAL(#KEY.#TAX-YEAR)
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                                       //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                                     //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(3);                                                                                                           //Natural: ASSIGN #TWRATBL4.#FORM-IND := 3
        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        //*  01/30/08 - AY
        //*  01/30/08 - AY
        //*    03/18/08    RM
        //*  ACTIVE FORMS
        //*  1042-S
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(10),"Form...............: ",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(3 + 1),NEWLINE,new  //Natural: WRITE ( 1 ) / 10T 'Form...............: ' #TWRAFRMN.#FORM-NAME ( 3 ) / 10T 'Original Reporting.: ' #TWRATBL4.#TIRCNTL-IRS-ORIG ( EM = N/Y )
            TabSetting(10),"Original Reporting.: ",pdaTwratbl4.getPnd_Twratbl4_Pnd_Tircntl_Irs_Orig(), new ReportEditMask ("N/Y"));
        if (Global.isEscape()) return;
        //*  IF #TWRATBL4.#TIRCNTL-IRS-ORIG
        //*    PERFORM  ERROR-DISPLAY-START
        //*    WRITE(1)
        //*      '***' 15T 'ORIGINAL REPORTING PROCESS FOR'
        //*       #TWRL5000.#FORM-NAME(3) 67T '***' /
        //*      '***' 15T 'ALREADY EXECUTED ON'
        //*      #TWRATBL4.TIRCNTL-RPT-DTE(1) (EM=MM/DD/YYYY) 67T '***'
        //*    PERFORM  ERROR-DISPLAY-END
        //*    TERMINATE 102
        //*  END-IF
        //*   GET FORM DATA
        pnd_Key_Pnd_Tax_Year.setValue(pnd_Ws_Tax_Year);                                                                                                                   //Natural: ASSIGN #KEY.#TAX-YEAR := #WS-TAX-YEAR
        pnd_Key_Pnd_Active_Ind.setValue("A");                                                                                                                             //Natural: ASSIGN #KEY.#ACTIVE-IND := 'A'
        pnd_Key_Pnd_Form_Type.setValue(3);                                                                                                                                //Natural: ASSIGN #KEY.#FORM-TYPE := 3
        ldaTwrl9705.getVw_form_U().startDatabaseRead                                                                                                                      //Natural: READ FORM-U BY TIRF-SUPERDE-3 = #KEY
        (
        "READ_FORM",
        new Wc[] { new Wc("TIRF_SUPERDE_3", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_3", "ASC") }
        );
        READ_FORM:
        while (condition(ldaTwrl9705.getVw_form_U().readNextRow("READ_FORM")))
        {
            if (condition(ldaTwrl9705.getForm_U_Tirf_Tax_Year().notEquals(pnd_Key_Pnd_Tax_Year) || ldaTwrl9705.getForm_U_Tirf_Form_Type().notEquals(pnd_Key_Pnd_Form_Type))) //Natural: IF TIRF-TAX-YEAR NE #KEY.#TAX-YEAR OR TIRF-FORM-TYPE NE #KEY.#FORM-TYPE
            {
                if (true) break READ_FORM;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ-FORM. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            getWorkFiles().write(2, false, ldaTwrl9705.getVw_form_U(), ldaTwrl9705.getVw_form_U().getAstISN("READ_FORM"), pnd_Test_Ind);                                  //Natural: WRITE WORK FILE 2 FORM-U *ISN ( READ-FORM. ) #TEST-IND
            pnd_Write_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WRITE-CNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),"Total records read:   ",pnd_Read_Cnt,NEWLINE,new TabSetting(10),"Total records written:",          //Natural: WRITE ( 1 ) 10T 'Total records read:   ' #READ-CNT / 10T 'Total records written:' #WRITE-CNT
            pnd_Write_Cnt);
        if (Global.isEscape()) return;
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(48),"Tax Withholding and Reporting System",new TabSetting(120),"Page:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(44),"1042-S IRS Original Reporting Control Report",new 
            TabSetting(120),"Report: RPT1",NEWLINE,NEWLINE,new TabSetting(10),"Tax Year: ",pnd_Ws_Tax_Year, new ReportEditMask ("9999"),NEWLINE,NEWLINE);
    }
}
