/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:31:47 PM
**        * FROM NATURAL PROGRAM : Twrp0905
************************************************************
**        * FILE NAME            : Twrp0905.java
**        * CLASS NAME           : Twrp0905
**        * INSTANCE NAME        : Twrp0905
************************************************************
* PROGRAM  : TWRP0905
* FUNCTION : PRINTS YTD AND DAILY REPORTABLE TRANSACTIONS BY SOURCE
*               CODE WITHIN COMPANY
* AUTHOR   : EDITH
* UPDATE
* -----------------------------------------------------------------
* 12/17/98 : NO TAXABLE AMOUNT IF KNOWN ROLL-OVER PAYMENT
* 04/02/99 : TO INCLUDE 'CP' IN MATCH REPORT
*          : TO INCLUDE PROCESSING OF IA MAINTENANCE RECORDS
* 06/15/99 : TO COMPUTE TAXABLE AMOUNT IF IVC-PROTECT IS ON OR
*              IVC-IND = K BUT NOT ROLL-OVER  (EDITH)
*          : NEW DEFINITION OF ROLL-OVER BASED ON DISTRIBUTION CODE
* 10/08/99 : INCLUSION OF NEW COMPANY, TIAA-LIFE  (EDITH)
* 11/05/99 : NEW ROUTINE FOR THE COMPUTATION OF TAX-AMT (10/19/99 EDITH)
*          : TO INCLUDE PRINTING OF THE FF. SOURCE CODES IN MATCH REPORT
*              ED = EXCESS DEFERRAL       GS = GSRA LOAN DEFERRAL
*              IS = INSURANCE SETTLEMENT  RE = REAC
* 11/12/99 : TO INCLUDE PROCESSING OF 'IP' - IPRO (11/8/99 EDITH)
*              MATCHING OF 'IP' FROM CONTROL FILE DON'T NEED 'TO-DATE'
* 11/10/99 : INCLUDED PROCESSING OF NEW SOURCES :
* 11/18/99     'TMAL'(APAL) - CREATED DUE TO DELETED 'APTM'
*              'TMIL'(IAIL) - CREATED DUE TO DELETED 'IATM'
* 11/30/99 : WITH BREAK OF TAX CITIZENSHIP AND WITH SUMMARY REPORT
* 12/14/99 : DISTRIBUTION CODES '6' AND 'Y' NO LONGER ROLLOVERS
* 04/02/02 : ADDED NEW COMPANY - TCII (J.ROTHOLZ)
* 05/30/02 : JH  ALLOW IVC AMOUNTS WITH ROLLOVERS
* 06/24/02 : JH  FURTHER TCII ADDITIONS
* 10/08/02 JH DISTR CODE 6 (TAXFREE EXCHANGE) IS TREATED LIKE ROLLOVER
* 06/12/03 RM INVESTMENT SOLUTIONS - AUTOMATE SSSS PROJECT
*             ADD NEW SOURCE CODE 'SI' FOR INVESTMENT SOLUTIONS
*             EXPAND SOURCE CODE TABLE FROM 14 TO 15
*             UPDATE ALL RELATED LOGIC TO INCLUDE 'SI'.
* 10/13/03 RM - 2003 1099 & 5498 SDR
*               DISTRIBUTION CODE 'X' REMOVED.
* 09/17/04 RM TOPS RELEASE 3 CHANGES
*             ADD NEW COMPANY CODE 'X' FOR TRUST COMPANY AND NEW SOURCE
*             CODE 'OP'.
*             EXPAND SOURCE CODE TABLE FROM 15 TO 16, AND COUNTER TABLE
*             FROM 18 TO 21, THEN ALLIGN THE PRINT FIELDS.
*             UPDATE ALL RELATED LOGIC TO INCLUDE 'OP'.
* 06/06/06: NAVISYS CHANGES   R. MA
*           ADD NEW SOURCE CODE 'NV'.
*           EXPAND SOURCE CODE TABLE FROM 16 TO 17.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'NV'.
* 10/30/06: VUL CHANGES   R. MA
*           ADD NEW SOURCE CODE 'VL'.
*           EXPAND SOURCE CODE TABLE FROM 17 TO 18.
*           UPDATE ALL RELATED LOGIC TO INCLUDE 'VL'.
* 07/19/11 - RS ADDED TEST FOR DIST CODE '=' (H4)    SCAN RS0711
*                'Roll Roth 403 to Roth IRA - Death'
* 09/29/11: M BERLIN
*           ADDED NEW MCCAMISH SOURCE CODE 'AM' MODELLED FROM 'VL'.
*           EXPANDED SOURCE CODE TABLE FROM 18 TO 19. /* 09/29/11
* 10/17/11: SUPPORTING NEW COMPANY CODE "F"; CHANGE CAN BE
*           REFERENCED AS DY1                              (D.YHUN)
* 11/21/14: O SOTTO FATCA CHANGES MARKED 11/2014.
* 02/27/15: F. ENDAYA FATCA CHANGES FOR 1040, 1050 AND 1071.  FE150301
*           GRAND TOTAL FIX FOR COLUMNS FOR INTEREST AND WITHHOLDING.
* --------------------------------------------------------------------

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0905 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0901 ldaTwrl0901;
    private LdaTwrl0902 ldaTwrl0902;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_tcf_Feeder_View;
    private DbsField tcf_Feeder_View_Tircntl_Tbl_Nbr;
    private DbsField tcf_Feeder_View_Tircntl_Tax_Year;
    private DbsField tcf_Feeder_View_Tircntl_Rpt_Source_Code;
    private DbsField tcf_Feeder_View_Tircntl_Company_Cde;
    private DbsField tcf_Feeder_View_Tircntl_Input_Gross_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Ivt_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Int_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Fed_Wthldg_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_State_Wthldg_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Nra_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Can_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Input_Local_Wthldg_Amt;
    private DbsField tcf_Feeder_View_Tircntl_Frm_Intrfce_Dte;
    private DbsField tcf_Feeder_View_Tircntl_To_Intrfce_Dte;
    private DbsField tcf_Feeder_View_Tircntl_5_Y_Sc_Co_To_Frm_Sp;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Trans_Cnt;
    private DbsField pnd_Counters_Pnd_Gross_Amt;
    private DbsField pnd_Counters_Pnd_Ivc_Amt;
    private DbsField pnd_Counters_Pnd_Taxable_Amt;
    private DbsField pnd_Counters_Pnd_Int_Amt;
    private DbsField pnd_Counters_Pnd_Fed_Wthld;
    private DbsField pnd_Counters_Pnd_Nra_Wthld;
    private DbsField pnd_Counters_Pnd_State_Wthld;
    private DbsField pnd_Counters_Pnd_Local_Wthld;
    private DbsField pnd_Counters_Pnd_Pr_Wthld;
    private DbsField pnd_Counters_Pnd_Can_Wthld;
    private DbsField pnd_Cmpy_Cnt;

    private DbsGroup pnd_Tot_Counters;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Cmpny;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Ivc_Amt;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Taxable_Amt;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Int_Amt;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Fed_Wthld;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Nra_Wthld;
    private DbsField pnd_Tot_Counters_Pnd_Tot_State_Wthld;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Local_Wthld;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Pr_Wthld;
    private DbsField pnd_Tot_Counters_Pnd_Tot_Can_Wthld;
    private DbsField pnd_Tot_Ndx;

    private DbsGroup pnd_Print_Fields;
    private DbsField pnd_Print_Fields_Pnd_Prtfld1;
    private DbsField pnd_Print_Fields_Pnd_Prtfld2;
    private DbsField pnd_Print_Fields_Pnd_Prtfld3;
    private DbsField pnd_Print_Fields_Pnd_Prtfld4;
    private DbsField pnd_Print_Fields_Pnd_Prtfld5;
    private DbsField pnd_Print_Fields_Pnd_Prtfld6;
    private DbsField pnd_Print_Fields_Pnd_Prtfld7;
    private DbsField pnd_Print_Fields_Pnd_Prtfld8;
    private DbsField pnd_Print_Fields_Pnd_Prtfld9;

    private DbsGroup pnd_Print0_Fields;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld01;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld02;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld03;
    private DbsField pnd_Print0_Fields_Pnd_Prtfld04;

    private DbsGroup pnd_Save_Areas;
    private DbsField pnd_Save_Areas_Pnd_Rec_Read;
    private DbsField pnd_Save_Areas_Pnd_Print_Com;
    private DbsField pnd_Save_Areas_Pnd_Print_Tc;
    private DbsField pnd_Save_Areas_Pnd_Print_Run;
    private DbsField pnd_Save_Areas_Pnd_Type_Trans;
    private DbsField pnd_Save_Areas_Pnd_Trans_Desc;
    private DbsField pnd_Save_Areas_Pnd_Source;
    private DbsField pnd_Save_Areas_Pnd_Sub_Src;
    private DbsField pnd_Save_Areas_Pnd_Source12;
    private DbsField pnd_Save_Areas_Pnd_Source34;
    private DbsField pnd_Save_Areas_Pnd_New_Source;
    private DbsField pnd_Save_Areas_Pnd_Prt_Source;
    private DbsField pnd_Save_Areas_Pnd_Occ;
    private DbsField pnd_Save_Areas_Pnd_I;
    private DbsField pnd_Save_Areas_Pnd_J;
    private DbsField pnd_Save_Areas_Pnd_K;
    private DbsField pnd_Save_Areas_Pnd_First;
    private DbsField pnd_Save_Areas_Pnd_Page;
    private DbsField pnd_Save_Areas_Pnd_Sub_Skip;
    private DbsField pnd_Save_Areas_Pnd_Eof;
    private DbsField pnd_Save_Areas_Pnd_Tot_Prt;
    private DbsField pnd_Save_Areas_Pnd_Error_Source;
    private DbsField pnd_Save_Areas_Pnd_Total_For_Ap_Ia;
    private DbsField pnd_Save_Areas_Pnd_Prt_Tot_Ol;
    private DbsField pnd_Save_Areas_Pnd_Tax_Amt;
    private DbsField pnd_Save_Areas_Pnd_Temp_Pr_Wthld;
    private DbsField pnd_Save_Areas_Pnd_Temp_State_Wthld;
    private DbsField pnd_Save_Areas_Pnd_Company;
    private DbsField pnd_Save_Areas_Pnd_Comp;
    private DbsField pnd_Save_Areas_Pnd_Scde;
    private DbsField pnd_Save_Areas_Pnd_X;
    private DbsField pnd_Y;

    private DbsGroup pnd_Var_File1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year;

    private DbsGroup pnd_Var_File1__R_Field_1;
    private DbsField pnd_Var_File1_Pnd_Tax_Year_A;
    private DbsField pnd_Var_File1_Pnd_Start_Date;
    private DbsField pnd_Var_File1_Pnd_End_Date;
    private DbsField pnd_Var_File1_Pnd_Freq;
    private DbsField pnd_Var_File1_Pnd_Total_Rec;

    private DbsGroup pnd_Ctr_Check_Amounts;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Co;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Sc;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Gross;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Ivc;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Int;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Fed;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_State;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Nra;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Can;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Local;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Error;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Err_Message;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Chk_Message;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Prt_Message;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_E;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_F;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_D;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_S;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_C;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Srce_Found;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Amt_Check;
    private DbsField pnd_Ctr_Check_Amounts_Pnd_Blank;

    private DbsGroup pnd_Prt_Ff1_Cntrl;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Co;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Sc;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Gross;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Ivc;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Int;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Fed;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_State;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Nra;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Can;
    private DbsField pnd_Prt_Ff1_Cntrl_Pnd_Prt_Local;

    private DbsGroup pnd_Savers;
    private DbsField pnd_Savers_Pnd_Sv_Company;
    private DbsField pnd_Savers_Pnd_Sv_Tax_Ctz;
    private DbsField pnd_Savers_Pnd_Sv_Srce_Code;
    private DbsField pnd_Savers_Pnd_Sv_Payset;
    private DbsField pnd_Key_Date;

    private DbsGroup pnd_Key_Date__R_Field_2;
    private DbsField pnd_Key_Date_Pnd_Key_Table_Nbr;
    private DbsField pnd_Key_Date_Pnd_Key_Tax_Year;
    private DbsField pnd_Key_Date_Pnd_Key_Sc;
    private DbsField pnd_Key_Date_Pnd_Key_Comp;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_To_Date;

    private DbsGroup pnd_Key_Date__R_Field_3;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_To_Date_Dd;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_From_Date;

    private DbsGroup pnd_Key_Date__R_Field_4;
    private DbsField pnd_Key_Date_Pnd_Key_Intf_From_Date_N;
    private DbsField pnd_Key_Date_Ip;

    private DbsGroup pnd_Key_Date_Ip__R_Field_5;
    private DbsField pnd_Key_Date_Ip_Pnd_Key_Table_Nbr_Ip;
    private DbsField pnd_Key_Date_Ip_Pnd_Key_Tax_Year_Ip;
    private DbsField pnd_Key_Date_Ip_Pnd_Key_Sc_Ip;
    private DbsField pnd_Key_Date_Ip_Pnd_Key_Comp_Ip;

    private DbsGroup pnd_Sum_Var;
    private DbsField pnd_Sum_Var_Pnd_Sum_Grs;
    private DbsField pnd_Sum_Var_Pnd_Sum_Ivc;
    private DbsField pnd_Sum_Var_Pnd_Sum_Int;
    private DbsField pnd_Sum_Var_Pnd_Sum_Fed;
    private DbsField pnd_Sum_Var_Pnd_Sum_Ste;
    private DbsField pnd_Sum_Var_Pnd_Sum_Nra;
    private DbsField pnd_Sum_Var_Pnd_Sum_Can;
    private DbsField pnd_Sum_Var_Pnd_Sum_Loc;
    private DbsField pnd_Param_Intf_From_Date;

    private DbsGroup pnd_Param_Intf_From_Date__R_Field_6;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm;
    private DbsField pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd;
    private DbsField pnd_Param_Intf_To_Date;
    private DbsField pnd_Srce_Tab;
    private DbsField pnd_Name;

    private DbsGroup pnd_Ttl_Var;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Src;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Grs;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Ivc;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Int;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Fed;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Ste;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Nra;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Lcl;
    private DbsField pnd_Ttl_Var_Pnd_Ttl_Can;
    private DbsField pnd_Fatca;
    private DbsField pnd_Nra_Rpt;
    private DbsField pnd_Hdg_Lit;
    private DbsField pnd_Hdg_Lit2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0901 = new LdaTwrl0901();
        registerRecord(ldaTwrl0901);
        ldaTwrl0902 = new LdaTwrl0902();
        registerRecord(ldaTwrl0902);

        // Local Variables
        localVariables = new DbsRecord();

        vw_tcf_Feeder_View = new DataAccessProgramView(new NameInfo("vw_tcf_Feeder_View", "TCF-FEEDER-VIEW"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        tcf_Feeder_View_Tircntl_Tbl_Nbr = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_TBL_NBR");
        tcf_Feeder_View_Tircntl_Tax_Year = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "TIRCNTL_TAX_YEAR");
        tcf_Feeder_View_Tircntl_Rpt_Source_Code = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        tcf_Feeder_View_Tircntl_Company_Cde = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_CDE");
        tcf_Feeder_View_Tircntl_Input_Gross_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Gross_Amt", "TIRCNTL-INPUT-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_GROSS_AMT");
        tcf_Feeder_View_Tircntl_Input_Ivt_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Ivt_Amt", "TIRCNTL-INPUT-IVT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_IVT_AMT");
        tcf_Feeder_View_Tircntl_Input_Int_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Int_Amt", "TIRCNTL-INPUT-INT-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_INT_AMT");
        tcf_Feeder_View_Tircntl_Input_Fed_Wthldg_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Fed_Wthldg_Amt", 
            "TIRCNTL-INPUT-FED-WTHLDG-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_FED_WTHLDG_AMT");
        tcf_Feeder_View_Tircntl_Input_State_Wthldg_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_State_Wthldg_Amt", 
            "TIRCNTL-INPUT-STATE-WTHLDG-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_STATE_WTHLDG_AMT");
        tcf_Feeder_View_Tircntl_Input_Nra_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Nra_Amt", "TIRCNTL-INPUT-NRA-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_NRA_AMT");
        tcf_Feeder_View_Tircntl_Input_Can_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Can_Amt", "TIRCNTL-INPUT-CAN-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_CAN_AMT");
        tcf_Feeder_View_Tircntl_Input_Local_Wthldg_Amt = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Input_Local_Wthldg_Amt", 
            "TIRCNTL-INPUT-LOCAL-WTHLDG-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_LOCAL_WTHLDG_AMT");
        tcf_Feeder_View_Tircntl_Frm_Intrfce_Dte = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        tcf_Feeder_View_Tircntl_To_Intrfce_Dte = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        tcf_Feeder_View_Tircntl_5_Y_Sc_Co_To_Frm_Sp = vw_tcf_Feeder_View.getRecord().newFieldInGroup("tcf_Feeder_View_Tircntl_5_Y_Sc_Co_To_Frm_Sp", "TIRCNTL-5-Y-SC-CO-TO-FRM-SP", 
            FieldType.STRING, 24, RepeatingFieldStrategy.None, "TIRCNTL_5_Y_SC_CO_TO_FRM_SP");
        tcf_Feeder_View_Tircntl_5_Y_Sc_Co_To_Frm_Sp.setSuperDescriptor(true);
        registerRecord(vw_tcf_Feeder_View);

        pnd_Counters = localVariables.newGroupArrayInRecord("pnd_Counters", "#COUNTERS", new DbsArrayController(1, 25));
        pnd_Counters_Pnd_Trans_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 9);
        pnd_Counters_Pnd_Gross_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Ivc_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Taxable_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Counters_Pnd_Int_Amt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Fed_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Fed_Wthld", "#FED-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Nra_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Nra_Wthld", "#NRA-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_State_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_State_Wthld", "#STATE-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Local_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Local_Wthld", "#LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counters_Pnd_Pr_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Pr_Wthld", "#PR-WTHLD", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Counters_Pnd_Can_Wthld = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Can_Wthld", "#CAN-WTHLD", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cmpy_Cnt = localVariables.newFieldInRecord("pnd_Cmpy_Cnt", "#CMPY-CNT", FieldType.PACKED_DECIMAL, 3);

        pnd_Tot_Counters = localVariables.newGroupArrayInRecord("pnd_Tot_Counters", "#TOT-COUNTERS", new DbsArrayController(1, 7));
        pnd_Tot_Counters_Pnd_Tot_Cmpny = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Cmpny", "#TOT-CMPNY", FieldType.STRING, 4);
        pnd_Tot_Counters_Pnd_Tot_Trans_Cnt = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", FieldType.NUMERIC, 
            9);
        pnd_Tot_Counters_Pnd_Tot_Gross_Amt = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Tot_Counters_Pnd_Tot_Ivc_Amt = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Ivc_Amt", "#TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Counters_Pnd_Tot_Taxable_Amt = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Taxable_Amt", "#TOT-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Tot_Counters_Pnd_Tot_Int_Amt = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Int_Amt", "#TOT-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Counters_Pnd_Tot_Fed_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Fed_Wthld", "#TOT-FED-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tot_Counters_Pnd_Tot_Nra_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Nra_Wthld", "#TOT-NRA-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Counters_Pnd_Tot_State_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_State_Wthld", "#TOT-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Counters_Pnd_Tot_Local_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Local_Wthld", "#TOT-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Counters_Pnd_Tot_Pr_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Pr_Wthld", "#TOT-PR-WTHLD", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tot_Counters_Pnd_Tot_Can_Wthld = pnd_Tot_Counters.newFieldInGroup("pnd_Tot_Counters_Pnd_Tot_Can_Wthld", "#TOT-CAN-WTHLD", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Print_Fields = localVariables.newGroupInRecord("pnd_Print_Fields", "#PRINT-FIELDS");
        pnd_Print_Fields_Pnd_Prtfld1 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld1", "#PRTFLD1", FieldType.STRING, 4);
        pnd_Print_Fields_Pnd_Prtfld2 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld2", "#PRTFLD2", FieldType.STRING, 19);
        pnd_Print_Fields_Pnd_Prtfld3 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld3", "#PRTFLD3", FieldType.STRING, 10);
        pnd_Print_Fields_Pnd_Prtfld4 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld4", "#PRTFLD4", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld5 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld5", "#PRTFLD5", FieldType.STRING, 13);
        pnd_Print_Fields_Pnd_Prtfld6 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld6", "#PRTFLD6", FieldType.STRING, 18);
        pnd_Print_Fields_Pnd_Prtfld7 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld7", "#PRTFLD7", FieldType.STRING, 12);
        pnd_Print_Fields_Pnd_Prtfld8 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld8", "#PRTFLD8", FieldType.STRING, 15);
        pnd_Print_Fields_Pnd_Prtfld9 = pnd_Print_Fields.newFieldInGroup("pnd_Print_Fields_Pnd_Prtfld9", "#PRTFLD9", FieldType.STRING, 14);

        pnd_Print0_Fields = localVariables.newGroupInRecord("pnd_Print0_Fields", "#PRINT0-FIELDS");
        pnd_Print0_Fields_Pnd_Prtfld01 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld01", "#PRTFLD01", FieldType.STRING, 15);
        pnd_Print0_Fields_Pnd_Prtfld02 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld02", "#PRTFLD02", FieldType.STRING, 12);
        pnd_Print0_Fields_Pnd_Prtfld03 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld03", "#PRTFLD03", FieldType.STRING, 15);
        pnd_Print0_Fields_Pnd_Prtfld04 = pnd_Print0_Fields.newFieldInGroup("pnd_Print0_Fields_Pnd_Prtfld04", "#PRTFLD04", FieldType.STRING, 14);

        pnd_Save_Areas = localVariables.newGroupInRecord("pnd_Save_Areas", "#SAVE-AREAS");
        pnd_Save_Areas_Pnd_Rec_Read = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 9);
        pnd_Save_Areas_Pnd_Print_Com = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Print_Com", "#PRINT-COM", FieldType.STRING, 4);
        pnd_Save_Areas_Pnd_Print_Tc = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Print_Tc", "#PRINT-TC", FieldType.STRING, 21);
        pnd_Save_Areas_Pnd_Print_Run = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Print_Run", "#PRINT-RUN", FieldType.STRING, 11);
        pnd_Save_Areas_Pnd_Type_Trans = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Type_Trans", "#TYPE-TRANS", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_Trans_Desc = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Trans_Desc", "#TRANS-DESC", FieldType.STRING, 19);
        pnd_Save_Areas_Pnd_Source = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Source", "#SOURCE", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_Sub_Src = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Sub_Src", "#SUB-SRC", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_Source12 = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Source12", "#SOURCE12", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_Source34 = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Source34", "#SOURCE34", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_New_Source = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_New_Source", "#NEW-SOURCE", FieldType.STRING, 4);
        pnd_Save_Areas_Pnd_Prt_Source = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Prt_Source", "#PRT-SOURCE", FieldType.STRING, 4);
        pnd_Save_Areas_Pnd_Occ = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Occ", "#OCC", FieldType.NUMERIC, 1);
        pnd_Save_Areas_Pnd_I = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Save_Areas_Pnd_J = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_J", "#J", FieldType.NUMERIC, 1);
        pnd_Save_Areas_Pnd_K = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_K", "#K", FieldType.NUMERIC, 1);
        pnd_Save_Areas_Pnd_First = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Page = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Page", "#PAGE", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Sub_Skip = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Sub_Skip", "#SUB-SKIP", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Eof = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Tot_Prt = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Tot_Prt", "#TOT-PRT", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Error_Source = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Error_Source", "#ERROR-SOURCE", FieldType.BOOLEAN, 1);
        pnd_Save_Areas_Pnd_Total_For_Ap_Ia = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Total_For_Ap_Ia", "#TOTAL-FOR-AP-IA", FieldType.BOOLEAN, 
            1);
        pnd_Save_Areas_Pnd_Prt_Tot_Ol = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Prt_Tot_Ol", "#PRT-TOT-OL", FieldType.NUMERIC, 1);
        pnd_Save_Areas_Pnd_Tax_Amt = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Save_Areas_Pnd_Temp_Pr_Wthld = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Temp_Pr_Wthld", "#TEMP-PR-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Save_Areas_Pnd_Temp_State_Wthld = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Temp_State_Wthld", "#TEMP-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Save_Areas_Pnd_Company = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Company", "#COMPANY", FieldType.STRING, 1);
        pnd_Save_Areas_Pnd_Comp = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Comp", "#COMP", FieldType.STRING, 1);
        pnd_Save_Areas_Pnd_Scde = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_Scde", "#SCDE", FieldType.STRING, 2);
        pnd_Save_Areas_Pnd_X = pnd_Save_Areas.newFieldInGroup("pnd_Save_Areas_Pnd_X", "#X", FieldType.NUMERIC, 1);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);

        pnd_Var_File1 = localVariables.newGroupInRecord("pnd_Var_File1", "#VAR-FILE1");
        pnd_Var_File1_Pnd_Tax_Year = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Var_File1__R_Field_1 = pnd_Var_File1.newGroupInGroup("pnd_Var_File1__R_Field_1", "REDEFINE", pnd_Var_File1_Pnd_Tax_Year);
        pnd_Var_File1_Pnd_Tax_Year_A = pnd_Var_File1__R_Field_1.newFieldInGroup("pnd_Var_File1_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Var_File1_Pnd_Start_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Start_Date", "#START-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_End_Date = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_End_Date", "#END-DATE", FieldType.STRING, 8);
        pnd_Var_File1_Pnd_Freq = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Freq", "#FREQ", FieldType.STRING, 1);
        pnd_Var_File1_Pnd_Total_Rec = pnd_Var_File1.newFieldInGroup("pnd_Var_File1_Pnd_Total_Rec", "#TOTAL-REC", FieldType.PACKED_DECIMAL, 9);

        pnd_Ctr_Check_Amounts = localVariables.newGroupInRecord("pnd_Ctr_Check_Amounts", "#CTR-CHECK-AMOUNTS");
        pnd_Ctr_Check_Amounts_Pnd_Co = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Co", "#CO", FieldType.STRING, 1, new DbsArrayController(1, 
            6));
        pnd_Ctr_Check_Amounts_Pnd_Sc = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Sc", "#SC", FieldType.STRING, 2, new DbsArrayController(1, 
            6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Gross = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Gross", "#GROSS", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Ivc = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Ivc", "#IVC", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Int = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Int", "#INT", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Fed = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Fed", "#FED", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_State = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_State", "#STATE", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Nra = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Nra", "#NRA", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Can = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Can", "#CAN", FieldType.PACKED_DECIMAL, 13, 
            2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Local = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Local", "#LOCAL", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1, 6, 1, 19));
        pnd_Ctr_Check_Amounts_Pnd_Error = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Error", "#ERROR", FieldType.NUMERIC, 1, 
            new DbsArrayController(1, 8));
        pnd_Ctr_Check_Amounts_Pnd_Err_Message = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Err_Message", "#ERR-MESSAGE", FieldType.STRING, 
            26);
        pnd_Ctr_Check_Amounts_Pnd_Chk_Message = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Chk_Message", "#CHK-MESSAGE", FieldType.STRING, 
            30);
        pnd_Ctr_Check_Amounts_Pnd_Prt_Message = pnd_Ctr_Check_Amounts.newFieldArrayInGroup("pnd_Ctr_Check_Amounts_Pnd_Prt_Message", "#PRT-MESSAGE", FieldType.STRING, 
            30, new DbsArrayController(1, 6));
        pnd_Ctr_Check_Amounts_Pnd_E = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_E", "#E", FieldType.NUMERIC, 1);
        pnd_Ctr_Check_Amounts_Pnd_F = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_F", "#F", FieldType.NUMERIC, 2);
        pnd_Ctr_Check_Amounts_Pnd_D = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_D", "#D", FieldType.NUMERIC, 2);
        pnd_Ctr_Check_Amounts_Pnd_S = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_S", "#S", FieldType.NUMERIC, 1);
        pnd_Ctr_Check_Amounts_Pnd_C = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1 = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1", "#PRT-FLD1", FieldType.STRING, 
            2);
        pnd_Ctr_Check_Amounts_Pnd_Srce_Found = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Srce_Found", "#SRCE-FOUND", FieldType.BOOLEAN, 
            1);
        pnd_Ctr_Check_Amounts_Pnd_Amt_Check = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Amt_Check", "#AMT-CHECK", FieldType.BOOLEAN, 
            1);
        pnd_Ctr_Check_Amounts_Pnd_Blank = pnd_Ctr_Check_Amounts.newFieldInGroup("pnd_Ctr_Check_Amounts_Pnd_Blank", "#BLANK", FieldType.BOOLEAN, 1);

        pnd_Prt_Ff1_Cntrl = localVariables.newGroupInRecord("pnd_Prt_Ff1_Cntrl", "#PRT-FF1-CNTRL");
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Co = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Co", "#PRT-CO", FieldType.STRING, 1);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Sc = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Sc", "#PRT-SC", FieldType.STRING, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Gross = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Gross", "#PRT-GROSS", FieldType.PACKED_DECIMAL, 
            14, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Ivc = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Ivc", "#PRT-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Int = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Int", "#PRT-INT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Fed = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Fed", "#PRT-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_State = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_State", "#PRT-STATE", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Nra = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Nra", "#PRT-NRA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Can = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Can", "#PRT-CAN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Prt_Ff1_Cntrl_Pnd_Prt_Local = pnd_Prt_Ff1_Cntrl.newFieldInGroup("pnd_Prt_Ff1_Cntrl_Pnd_Prt_Local", "#PRT-LOCAL", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Savers = localVariables.newGroupInRecord("pnd_Savers", "#SAVERS");
        pnd_Savers_Pnd_Sv_Company = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Company", "#SV-COMPANY", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Tax_Ctz = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Tax_Ctz", "#SV-TAX-CTZ", FieldType.STRING, 1);
        pnd_Savers_Pnd_Sv_Srce_Code = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Srce_Code", "#SV-SRCE-CODE", FieldType.STRING, 4);
        pnd_Savers_Pnd_Sv_Payset = pnd_Savers.newFieldInGroup("pnd_Savers_Pnd_Sv_Payset", "#SV-PAYSET", FieldType.STRING, 2);
        pnd_Key_Date = localVariables.newFieldInRecord("pnd_Key_Date", "#KEY-DATE", FieldType.STRING, 24);

        pnd_Key_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Key_Date__R_Field_2", "REDEFINE", pnd_Key_Date);
        pnd_Key_Date_Pnd_Key_Table_Nbr = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Table_Nbr", "#KEY-TABLE-NBR", FieldType.NUMERIC, 
            1);
        pnd_Key_Date_Pnd_Key_Tax_Year = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Tax_Year", "#KEY-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Key_Date_Pnd_Key_Sc = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Sc", "#KEY-SC", FieldType.STRING, 2);
        pnd_Key_Date_Pnd_Key_Comp = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Comp", "#KEY-COMP", FieldType.STRING, 1);
        pnd_Key_Date_Pnd_Key_Intf_To_Date = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_To_Date", "#KEY-INTF-TO-DATE", FieldType.STRING, 
            8);

        pnd_Key_Date__R_Field_3 = pnd_Key_Date__R_Field_2.newGroupInGroup("pnd_Key_Date__R_Field_3", "REDEFINE", pnd_Key_Date_Pnd_Key_Intf_To_Date);
        pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy = pnd_Key_Date__R_Field_3.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy", "#KEY-INTF-TO-DATE-YY", 
            FieldType.NUMERIC, 4);
        pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm = pnd_Key_Date__R_Field_3.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm", "#KEY-INTF-TO-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Key_Date_Pnd_Key_Intf_To_Date_Dd = pnd_Key_Date__R_Field_3.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_To_Date_Dd", "#KEY-INTF-TO-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Key_Date_Pnd_Key_Intf_From_Date = pnd_Key_Date__R_Field_2.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_From_Date", "#KEY-INTF-FROM-DATE", FieldType.STRING, 
            8);

        pnd_Key_Date__R_Field_4 = pnd_Key_Date__R_Field_2.newGroupInGroup("pnd_Key_Date__R_Field_4", "REDEFINE", pnd_Key_Date_Pnd_Key_Intf_From_Date);
        pnd_Key_Date_Pnd_Key_Intf_From_Date_N = pnd_Key_Date__R_Field_4.newFieldInGroup("pnd_Key_Date_Pnd_Key_Intf_From_Date_N", "#KEY-INTF-FROM-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Key_Date_Ip = localVariables.newFieldInRecord("pnd_Key_Date_Ip", "#KEY-DATE-IP", FieldType.STRING, 8);

        pnd_Key_Date_Ip__R_Field_5 = localVariables.newGroupInRecord("pnd_Key_Date_Ip__R_Field_5", "REDEFINE", pnd_Key_Date_Ip);
        pnd_Key_Date_Ip_Pnd_Key_Table_Nbr_Ip = pnd_Key_Date_Ip__R_Field_5.newFieldInGroup("pnd_Key_Date_Ip_Pnd_Key_Table_Nbr_Ip", "#KEY-TABLE-NBR-IP", 
            FieldType.NUMERIC, 1);
        pnd_Key_Date_Ip_Pnd_Key_Tax_Year_Ip = pnd_Key_Date_Ip__R_Field_5.newFieldInGroup("pnd_Key_Date_Ip_Pnd_Key_Tax_Year_Ip", "#KEY-TAX-YEAR-IP", FieldType.NUMERIC, 
            4);
        pnd_Key_Date_Ip_Pnd_Key_Sc_Ip = pnd_Key_Date_Ip__R_Field_5.newFieldInGroup("pnd_Key_Date_Ip_Pnd_Key_Sc_Ip", "#KEY-SC-IP", FieldType.STRING, 2);
        pnd_Key_Date_Ip_Pnd_Key_Comp_Ip = pnd_Key_Date_Ip__R_Field_5.newFieldInGroup("pnd_Key_Date_Ip_Pnd_Key_Comp_Ip", "#KEY-COMP-IP", FieldType.STRING, 
            1);

        pnd_Sum_Var = localVariables.newGroupInRecord("pnd_Sum_Var", "#SUM-VAR");
        pnd_Sum_Var_Pnd_Sum_Grs = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Grs", "#SUM-GRS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Ivc = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Ivc", "#SUM-IVC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Int = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Int", "#SUM-INT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Fed = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Fed", "#SUM-FED", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Ste = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Ste", "#SUM-STE", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Nra = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Nra", "#SUM-NRA", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Can = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Can", "#SUM-CAN", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Sum_Var_Pnd_Sum_Loc = pnd_Sum_Var.newFieldInGroup("pnd_Sum_Var_Pnd_Sum_Loc", "#SUM-LOC", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Param_Intf_From_Date = localVariables.newFieldInRecord("pnd_Param_Intf_From_Date", "#PARAM-INTF-FROM-DATE", FieldType.STRING, 8);

        pnd_Param_Intf_From_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Param_Intf_From_Date__R_Field_6", "REDEFINE", pnd_Param_Intf_From_Date);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy = pnd_Param_Intf_From_Date__R_Field_6.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy", 
            "#PARAM-INTF-FROM-DATE-YY", FieldType.NUMERIC, 4);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm = pnd_Param_Intf_From_Date__R_Field_6.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm", 
            "#PARAM-INTF-FROM-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd = pnd_Param_Intf_From_Date__R_Field_6.newFieldInGroup("pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Dd", 
            "#PARAM-INTF-FROM-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Param_Intf_To_Date = localVariables.newFieldInRecord("pnd_Param_Intf_To_Date", "#PARAM-INTF-TO-DATE", FieldType.STRING, 8);
        pnd_Srce_Tab = localVariables.newFieldArrayInRecord("pnd_Srce_Tab", "#SRCE-TAB", FieldType.STRING, 2, new DbsArrayController(1, 19));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 19);

        pnd_Ttl_Var = localVariables.newGroupArrayInRecord("pnd_Ttl_Var", "#TTL-VAR", new DbsArrayController(1, 19));
        pnd_Ttl_Var_Pnd_Ttl_Src = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Src", "#TTL-SRC", FieldType.STRING, 2);
        pnd_Ttl_Var_Pnd_Ttl_Grs = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Grs", "#TTL-GRS", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_Ttl_Var_Pnd_Ttl_Ivc = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Ivc", "#TTL-IVC", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Int = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Int", "#TTL-INT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Fed = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Fed", "#TTL-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Ste = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Ste", "#TTL-STE", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Nra = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Nra", "#TTL-NRA", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Lcl = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Lcl", "#TTL-LCL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ttl_Var_Pnd_Ttl_Can = pnd_Ttl_Var.newFieldInGroup("pnd_Ttl_Var_Pnd_Ttl_Can", "#TTL-CAN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Fatca = localVariables.newFieldInRecord("pnd_Fatca", "#FATCA", FieldType.BOOLEAN, 1);
        pnd_Nra_Rpt = localVariables.newFieldInRecord("pnd_Nra_Rpt", "#NRA-RPT", FieldType.BOOLEAN, 1);
        pnd_Hdg_Lit = localVariables.newFieldInRecord("pnd_Hdg_Lit", "#HDG-LIT", FieldType.STRING, 13);
        pnd_Hdg_Lit2 = localVariables.newFieldInRecord("pnd_Hdg_Lit2", "#HDG-LIT2", FieldType.STRING, 16);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_tcf_Feeder_View.reset();

        ldaTwrl0901.initializeValues();
        ldaTwrl0902.initializeValues();

        localVariables.reset();
        pnd_Cmpy_Cnt.setInitialValue(7);
        pnd_Save_Areas_Pnd_First.setInitialValue(true);
        pnd_Save_Areas_Pnd_Page.setInitialValue(false);
        pnd_Save_Areas_Pnd_Sub_Skip.setInitialValue(false);
        pnd_Save_Areas_Pnd_Eof.setInitialValue(false);
        pnd_Save_Areas_Pnd_Tot_Prt.setInitialValue(false);
        pnd_Save_Areas_Pnd_Error_Source.setInitialValue(false);
        pnd_Save_Areas_Pnd_Total_For_Ap_Ia.setInitialValue(false);
        pnd_Save_Areas_Pnd_Prt_Tot_Ol.setInitialValue(0);
        pnd_Save_Areas_Pnd_X.setInitialValue(0);
        pnd_Y.setInitialValue(0);
        pnd_Ctr_Check_Amounts_Pnd_Srce_Found.setInitialValue(false);
        pnd_Ctr_Check_Amounts_Pnd_Amt_Check.setInitialValue(false);
        pnd_Ctr_Check_Amounts_Pnd_Blank.setInitialValue(false);
        pnd_Srce_Tab.getValue(1).setInitialValue("AM");
        pnd_Srce_Tab.getValue(2).setInitialValue("AP");
        pnd_Srce_Tab.getValue(3).setInitialValue("CP");
        pnd_Srce_Tab.getValue(4).setInitialValue("DC");
        pnd_Srce_Tab.getValue(5).setInitialValue("DS");
        pnd_Srce_Tab.getValue(6).setInitialValue("ED");
        pnd_Srce_Tab.getValue(7).setInitialValue("EW");
        pnd_Srce_Tab.getValue(8).setInitialValue("GS");
        pnd_Srce_Tab.getValue(9).setInitialValue("IA");
        pnd_Srce_Tab.getValue(10).setInitialValue("IP");
        pnd_Srce_Tab.getValue(11).setInitialValue("IS");
        pnd_Srce_Tab.getValue(12).setInitialValue("MS");
        pnd_Srce_Tab.getValue(13).setInitialValue("NV");
        pnd_Srce_Tab.getValue(14).setInitialValue("NZ");
        pnd_Srce_Tab.getValue(15).setInitialValue("OP");
        pnd_Srce_Tab.getValue(16).setInitialValue("RE");
        pnd_Srce_Tab.getValue(17).setInitialValue("SI");
        pnd_Srce_Tab.getValue(18).setInitialValue("SS");
        pnd_Srce_Tab.getValue(19).setInitialValue("VL");
        pnd_Fatca.setInitialValue(false);
        pnd_Nra_Rpt.setInitialValue(false);
        pnd_Hdg_Lit.setInitialValue("NON-FATCA TAX");
        pnd_Hdg_Lit2.setInitialValue("NON-FATCA WTHHLD");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0905() throws Exception
    {
        super("Twrp0905");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  -----------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132;//Natural: FORMAT ( 3 ) PS = 60 LS = 132
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  = #TWRP0600-TO-CCYYMMDD
        //*  = #TWRP0600-INTERFACE-***
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #FLAT-FILE1
        while (condition(getWorkFiles().read(1, ldaTwrl0901.getPnd_Flat_File1())))
        {
            pnd_Var_File1_Pnd_Tax_Year.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tax_Year());                                                                        //Natural: ASSIGN #TAX-YEAR := #FF1-TAX-YEAR
            pnd_Var_File1_Pnd_Start_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Start_Date());                                                                    //Natural: ASSIGN #START-DATE := #FF1-START-DATE
            pnd_Var_File1_Pnd_End_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                        //Natural: ASSIGN #END-DATE := #FF1-END-DATE
            pnd_Var_File1_Pnd_Freq.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Frequency());                                                                           //Natural: ASSIGN #FREQ := #FF1-FREQUENCY
            pnd_Var_File1_Pnd_Total_Rec.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Tot_Rec_Cnt());                                                                    //Natural: ASSIGN #TOTAL-REC := #FF1-TOT-REC-CNT
            pnd_Param_Intf_To_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_Intf_To_Date());                                                                        //Natural: ASSIGN #PARAM-INTF-TO-DATE := #FF1-INTF-TO-DATE
            pnd_Param_Intf_From_Date.setValue(ldaTwrl0901.getPnd_Flat_File1_Pnd_Ff1_End_Date());                                                                          //Natural: ASSIGN #PARAM-INTF-FROM-DATE := #FF1-END-DATE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Var_File1_Pnd_Freq.equals("D")))                                                                                                                //Natural: IF #FREQ = 'D'
        {
                                                                                                                                                                          //Natural: PERFORM INFO-FROM-F98T5
            sub_Info_From_F98t5();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            CheckAtStartofData405();

            //*  11/2014
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().notEquals("Y"))))                                                                  //Natural: ACCEPT IF #FF3-TWRPYMNT-FATCA-IND NE 'Y'
            {
                continue;
            }
            pnd_Save_Areas_Pnd_Rec_Read.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-READ
            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals(pnd_Savers_Pnd_Sv_Tax_Ctz)))                                                 //Natural: IF #FF3-TAX-CITIZENSHIP = #SV-TAX-CTZ
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                               //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                    {
                        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().equals(pnd_Savers_Pnd_Sv_Payset)))                                              //Natural: IF #FF3-PAYSET-TYPE = #SV-PAYSET
                        {
                            ignore();
                            //*  BREAK OF PAYTYP-SETTYP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM PAYSET-BRK
                            sub_Payset_Brk();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                            sub_Reset_Ctrs();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  BREAK OF SRCE-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                        sub_Source_Brk();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                        sub_Reset_Ctrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BREAK OF TAX-CTZ
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                    sub_Company_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                    sub_Account_Grandtotal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                          //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  BREAK OF COMPANY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PRINT MATCH REPORT
                                                                                                                                                                          //Natural: PERFORM CHECK-AMOUNTS
                sub_Check_Amounts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                sub_Account_Grandtotal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ttl_Var.getValue("*").reset();                                                                                                                        //Natural: RESET #TTL-VAR ( * )
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                              //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*                      *** PROCESS RECORD ***
            pnd_Save_Areas_Pnd_Tax_Amt.reset();                                                                                                                           //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet451 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet451++;
                //*                                        BG
                //*        OR= '6')      H4
                pnd_Save_Areas_Pnd_Tax_Amt.setValue(0);                                                                                                                   //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet451++;
                pnd_Save_Areas_Pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Save_Areas_Pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Tax_Amt.setValue(0);                                                                                                                   //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Save_Areas_Pnd_Temp_Pr_Wthld.reset();                                                                                                                     //Natural: RESET #TEMP-PR-WTHLD #TEMP-STATE-WTHLD
            pnd_Save_Areas_Pnd_Temp_State_Wthld.reset();
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ")      //Natural: IF #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))
            {
                pnd_Save_Areas_Pnd_Temp_Pr_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                               //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-PR-WTHLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Temp_State_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                            //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-STATE-WTHLD
                //*  +1 COMPANY   RM 9/16/04
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #K 1 5
            for (pnd_Save_Areas_Pnd_K.setValue(1); condition(pnd_Save_Areas_Pnd_K.lessOrEqual(5)); pnd_Save_Areas_Pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Save_Areas_Pnd_K).nadd(1);                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                        //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                            //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Tax_Amt);                                                             //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                            //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                    //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-WTHLD ( #K )
                pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                    //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-WTHLD ( #K )
                pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                    //Natural: ADD #FF3-LOCAL-WTHLD TO #LOCAL-WTHLD ( #K )
                pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Temp_Pr_Wthld);                                                          //Natural: ADD #TEMP-PR-WTHLD TO #PR-WTHLD ( #K )
                pnd_Counters_Pnd_State_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Temp_State_Wthld);                                                    //Natural: ADD #TEMP-STATE-WTHLD TO #STATE-WTHLD ( #K )
                pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                    //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-WTHLD ( #K )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Save_Areas_Pnd_Rec_Read.equals(getZero())))                                                                                                     //Natural: IF #REC-READ = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Save_Areas_Pnd_Eof.setValue(true);                                                                                                                        //Natural: ASSIGN #EOF := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
            sub_Account_Grandtotal();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Var_File1_Pnd_Freq.equals("D")))                                                                                                            //Natural: IF #FREQ = 'D'
            {
                //*  TO PRINT MATCH REPORT
                                                                                                                                                                          //Natural: PERFORM CHECK-AMOUNTS
                sub_Check_Amounts();
                if (condition(Global.isEscape())) {return;}
                pnd_Ttl_Var.getValue("*").reset();                                                                                                                        //Natural: RESET #TTL-VAR ( * )
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape())) {return;}
                //*  PASSED THE CHECK-AMOUNTS SUBROUTINE
                if (condition(pnd_Ctr_Check_Amounts_Pnd_Amt_Check.getBoolean()))                                                                                          //Natural: IF #AMT-CHECK
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-FLATFILE1
                    sub_Check_Flatfile1();
                    if (condition(Global.isEscape())) {return;}
                    //*  JH 06/24/2002
                    getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"STATUS OF CONTROL FILE : ",NEWLINE,NEWLINE,NEWLINE,NEWLINE,"  AREA #1  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(1), //Natural: WRITE /// 'STATUS OF CONTROL FILE : ' // // '  AREA #1  : ' #PRT-MESSAGE ( 1 ) / '  AREA #2  : ' #PRT-MESSAGE ( 2 ) / '  AREA #3  : ' #PRT-MESSAGE ( 3 ) / '  AREA #4  : ' #PRT-MESSAGE ( 4 ) /// #CO ( * ) / #SC ( *,* )
                        NEWLINE,"  AREA #2  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(2),NEWLINE,"  AREA #3  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(3),
                        NEWLINE,"  AREA #4  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(4),NEWLINE,NEWLINE,NEWLINE,pnd_Ctr_Check_Amounts_Pnd_Co.getValue("*"),
                        NEWLINE,pnd_Ctr_Check_Amounts_Pnd_Sc.getValue("*","*"));
                    if (Global.isEscape()) return;
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"ERROR CODES LEGEND:",NEWLINE,NEWLINE,"1 - UNBALANCE GROSS AMOUNT",                //Natural: WRITE ( 2 ) /// 'ERROR CODES LEGEND:' // '1 - UNBALANCE GROSS AMOUNT' / '2 - UNBALANCE IVC AMOUNT' / '3 - UNBALANCE INT AMOUNT' / '4 - UNBALANCE FEDERAL AMOUNT' / '5 - UNBALANCE STATE AMOUNT' / '6 - UNBALANCE NRA AMOUNT' / '7 - UNBALANCE LOCAL AMOUNT' / '8 - UNBALANCE CANADIAN AMOUNT'
                        NEWLINE,"2 - UNBALANCE IVC AMOUNT",NEWLINE,"3 - UNBALANCE INT AMOUNT",NEWLINE,"4 - UNBALANCE FEDERAL AMOUNT",NEWLINE,"5 - UNBALANCE STATE AMOUNT",
                        NEWLINE,"6 - UNBALANCE NRA AMOUNT",NEWLINE,"7 - UNBALANCE LOCAL AMOUNT",NEWLINE,"8 - UNBALANCE CANADIAN AMOUNT");
                    if (Global.isEscape()) return;
                    //*  ADD 'SI'   RM 6/12/03
                    //*  ADD 'OP'   RM 9/16/04
                    //*  ADD 'NV'   RM 06/06/06
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(25),"-- NO VALID SOURCE CODES ","(AM,AP,CP,DC,DS,ED,EW,GS,IA,IP,IS,MS,NV,NZ,OP,RE,SI,SS,VL)", //Natural: WRITE ( 2 ) /// 25T '-- NO VALID SOURCE CODES ' '(AM,AP,CP,DC,DS,ED,EW,GS,IA,IP,IS,MS,NV,NZ,OP,RE,SI,SS,VL)' 'to be matched --'
                        "to be matched --");
                    if (Global.isEscape()) return;
                    //*  ADD 'VL'   RM 10/30/06
                }                                                                                                                                                         //Natural: END-IF
                //*  YTD                       /* ADD 'AM'   /* 09/29/11
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** TOTAL NUMBER OF RECORDS : ",pnd_Var_File1_Pnd_Total_Rec);                         //Natural: WRITE ( 1 ) /// '*** TOTAL NUMBER OF RECORDS : ' #TOTAL-REC
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*   DY1
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR #C = 6 TO 22
        FOR02:                                                                                                                                                            //Natural: FOR #C = 6 TO 25
        for (pnd_Ctr_Check_Amounts_Pnd_C.setValue(6); condition(pnd_Ctr_Check_Amounts_Pnd_C.lessOrEqual(25)); pnd_Ctr_Check_Amounts_Pnd_C.nadd(1))
        {
            short decideConditionsMet521 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #C;//Natural: VALUE 6
            if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(6))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("CREF-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(7))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("CREF-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(8))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL CREF -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL CREF -->: '
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(9))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("LIFE-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(10))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("LIFE-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(11))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL LIFE -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL LIFE -->: '
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(12))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TIAA-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(13))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TIAA-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(14))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL TIAA -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TIAA -->: '
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(15))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TCII-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(16))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TCII-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(17))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL TCII -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TCII -->: '
                //*   DY1 FIX BEGINS  >>
                //*     VALUE 18  #NAME := 'TRST-FOREIGN CTZ : '
                //*     VALUE 19  #NAME := 'TRST-U.S. CTZ    : '
                //*     VALUE 20  #NAME := '-- TOTAL TRST -->: '
                //*     VALUE 21  #NAME := '-- TOTAL OTHERS->: '
                //*     VALUE 22  #NAME := '-- GRAND TOTALS->: '
            }                                                                                                                                                             //Natural: VALUE 18
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(18))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("FSB-FOREIGN CTZ  : ");                                                                                                                 //Natural: ASSIGN #NAME := 'FSB-FOREIGN CTZ  : '
            }                                                                                                                                                             //Natural: VALUE 19
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(19))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("FSB-U.S. CTZ     : ");                                                                                                                 //Natural: ASSIGN #NAME := 'FSB-U.S. CTZ     : '
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(20))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL FSB  -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL FSB  -->: '
            }                                                                                                                                                             //Natural: VALUE 21
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(21))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TRST-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 22
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(22))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("TRST-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 23
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(23))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL TRST -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TRST -->: '
            }                                                                                                                                                             //Natural: VALUE 24
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(24))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- TOTAL OTHERS->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL OTHERS->: '
            }                                                                                                                                                             //Natural: VALUE 25
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(25))))
            {
                decideConditionsMet521++;
                pnd_Name.setValue("-- GRAND TOTALS->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- GRAND TOTALS->: '
                //*   DY1 FIX ENDS    <<
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF #C = 6  OR = 22                         /* DY1
            //*  DY1
            if (condition(pnd_Ctr_Check_Amounts_Pnd_C.equals(6) || pnd_Ctr_Check_Amounts_Pnd_C.equals(25)))                                                               //Natural: IF #C = 6 OR = 25
            {
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",NEWLINE,"RUNDATE    : ",Global.getDATX(),  //Natural: WRITE ( 1 ) / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' / 'RUNTIME    : ' *TIMX 51T '-- SUMMARY REPORT - -' / 'TAX YEAR   : ' #TAX-YEAR 41T 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// 11X 'COMPANY' 8X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 6X 'FEDERAL TAX' 2X #HDG-LIT / 9X 'TAX CTZNSHP' 6X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 7X 'WITHHOLDING' 4X 'WITHHOLDING' / 74T '-----------' 4X '-----------' 5X '-----------' 4X '-----------' / 75T 'STATE TAX' 6X 'LOCAL TAX' 6X 'PUERTO RICO' 5X 'CANADIAN' / 74T 'WITHHOLDING' 4X 'WITHHOLDING' 5X 'WITHHOLDING' 4X 'WITHHOLDING' //
                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",NEWLINE,"RUNTIME    : ",Global.getTIMX(),new 
                    TabSetting(51),"-- SUMMARY REPORT - -",NEWLINE,"TAX YEAR   : ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(41),"DATE RANGE : ",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,new 
                    ColumnSpacing(11),"COMPANY",new ColumnSpacing(8),"TRANS",new ColumnSpacing(11),"GROSS",new ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new 
                    ColumnSpacing(7),"INTEREST",new ColumnSpacing(6),"FEDERAL TAX",new ColumnSpacing(2),pnd_Hdg_Lit,NEWLINE,new ColumnSpacing(9),"TAX CTZNSHP",new 
                    ColumnSpacing(6),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new 
                    ColumnSpacing(7),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,new TabSetting(74),"-----------",new ColumnSpacing(4),"-----------",new 
                    ColumnSpacing(5),"-----------",new ColumnSpacing(4),"-----------",NEWLINE,new TabSetting(75),"STATE TAX",new ColumnSpacing(6),"LOCAL TAX",new 
                    ColumnSpacing(6),"PUERTO RICO",new ColumnSpacing(5),"CANADIAN",NEWLINE,new TabSetting(74),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",new 
                    ColumnSpacing(5),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      12X 'TAXABLE' 7X 'INTEREST'  6X 'FEDERAL TAX'  6X 'NRA TAX'
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #C = 22                               /*  DY1
            //*   DY1
            if (condition(pnd_Ctr_Check_Amounts_Pnd_C.equals(25)))                                                                                                        //Natural: IF #C = 25
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Trans_Cnt.getValue(25)), pnd_Counters_Pnd_Trans_Cnt.getValue(25).add(pnd_Counters_Pnd_Trans_Cnt.getValue(8)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(11)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(14)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(17)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(20)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(23)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(24))); //Natural: ADD #TRANS-CNT ( 8 ) #TRANS-CNT ( 11 ) #TRANS-CNT ( 14 ) #TRANS-CNT ( 17 ) #TRANS-CNT ( 20 ) #TRANS-CNT ( 23 ) #TRANS-CNT ( 24 ) TO #TRANS-CNT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #TRANS-CNT  (17) #TRANS-CNT  (20) #TRANS-CNT  (21)
                //*      #TRANS-CNT  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Gross_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Gross_Amt.getValue(25)), pnd_Counters_Pnd_Gross_Amt.getValue(25).add(pnd_Counters_Pnd_Gross_Amt.getValue(8)).add(pnd_Counters_Pnd_Gross_Amt.getValue(11)).add(pnd_Counters_Pnd_Gross_Amt.getValue(14)).add(pnd_Counters_Pnd_Gross_Amt.getValue(17)).add(pnd_Counters_Pnd_Gross_Amt.getValue(20)).add(pnd_Counters_Pnd_Gross_Amt.getValue(23)).add(pnd_Counters_Pnd_Gross_Amt.getValue(24))); //Natural: ADD #GROSS-AMT ( 8 ) #GROSS-AMT ( 11 ) #GROSS-AMT ( 14 ) #GROSS-AMT ( 17 ) #GROSS-AMT ( 20 ) #GROSS-AMT ( 23 ) #GROSS-AMT ( 24 ) TO #GROSS-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #GROSS-AMT  (17) #GROSS-AMT  (20) #GROSS-AMT  (21)
                //*      #GROSS-AMT  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Ivc_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Ivc_Amt.getValue(25)), pnd_Counters_Pnd_Ivc_Amt.getValue(25).add(pnd_Counters_Pnd_Ivc_Amt.getValue(8)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(11)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(14)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(17)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(20)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(23)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(24))); //Natural: ADD #IVC-AMT ( 8 ) #IVC-AMT ( 11 ) #IVC-AMT ( 14 ) #IVC-AMT ( 17 ) #IVC-AMT ( 20 ) #IVC-AMT ( 23 ) #IVC-AMT ( 24 ) TO #IVC-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #IVC-AMT    (17) #IVC-AMT    (20) #IVC-AMT    (21)
                //*      #IVC-AMT    (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Taxable_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Taxable_Amt.getValue(25)), pnd_Counters_Pnd_Taxable_Amt.getValue(25).add(pnd_Counters_Pnd_Taxable_Amt.getValue(8)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(11)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(14)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(17)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(20)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(23)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(24))); //Natural: ADD #TAXABLE-AMT ( 8 ) #TAXABLE-AMT ( 11 ) #TAXABLE-AMT ( 14 ) #TAXABLE-AMT ( 17 ) #TAXABLE-AMT ( 20 ) #TAXABLE-AMT ( 23 ) #TAXABLE-AMT ( 24 ) TO #TAXABLE-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #TAXABLE-AMT(17) #TAXABLE-AMT(20) #TAXABLE-AMT(21)
                //*      #TAXABLE-AMT(22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Int_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Int_Amt.getValue(25)), pnd_Counters_Pnd_Int_Amt.getValue(25).add(pnd_Counters_Pnd_Int_Amt.getValue(8)).add(pnd_Counters_Pnd_Int_Amt.getValue(11)).add(pnd_Counters_Pnd_Int_Amt.getValue(14)).add(pnd_Counters_Pnd_Int_Amt.getValue(17)).add(pnd_Counters_Pnd_Int_Amt.getValue(20)).add(pnd_Counters_Pnd_Int_Amt.getValue(23)).add(pnd_Counters_Pnd_Int_Amt.getValue(24))); //Natural: ADD #INT-AMT ( 8 ) #INT-AMT ( 11 ) #INT-AMT ( 14 ) #INT-AMT ( 17 ) #INT-AMT ( 20 ) #INT-AMT ( 23 ) #INT-AMT ( 24 ) TO #INT-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #INT-AMT    (17) #INT-AMT    (20) #INT-AMT    (21)
                //*      #INT-AMT    (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Fed_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Fed_Wthld.getValue(25)), pnd_Counters_Pnd_Fed_Wthld.getValue(25).add(pnd_Counters_Pnd_Fed_Wthld.getValue(8)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(11)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(14)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(17)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(20)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(23)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(24))); //Natural: ADD #FED-WTHLD ( 8 ) #FED-WTHLD ( 11 ) #FED-WTHLD ( 14 ) #FED-WTHLD ( 17 ) #FED-WTHLD ( 20 ) #FED-WTHLD ( 23 ) #FED-WTHLD ( 24 ) TO #FED-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #FED-WTHLD  (17) #FED-WTHLD  (20) #FED-WTHLD  (21)
                //*      #FED-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Nra_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Nra_Wthld.getValue(25)), pnd_Counters_Pnd_Nra_Wthld.getValue(25).add(pnd_Counters_Pnd_Nra_Wthld.getValue(8)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(11)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(14)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(17)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(20)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(23)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(24))); //Natural: ADD #NRA-WTHLD ( 8 ) #NRA-WTHLD ( 11 ) #NRA-WTHLD ( 14 ) #NRA-WTHLD ( 17 ) #NRA-WTHLD ( 20 ) #NRA-WTHLD ( 23 ) #NRA-WTHLD ( 24 ) TO #NRA-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #NRA-WTHLD  (17) #NRA-WTHLD  (20) #NRA-WTHLD  (21)
                //*      #NRA-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_State_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_State_Wthld.getValue(25)), pnd_Counters_Pnd_State_Wthld.getValue(25).add(pnd_Counters_Pnd_State_Wthld.getValue(8)).add(pnd_Counters_Pnd_State_Wthld.getValue(11)).add(pnd_Counters_Pnd_State_Wthld.getValue(14)).add(pnd_Counters_Pnd_State_Wthld.getValue(17)).add(pnd_Counters_Pnd_State_Wthld.getValue(20)).add(pnd_Counters_Pnd_State_Wthld.getValue(23)).add(pnd_Counters_Pnd_State_Wthld.getValue(24))); //Natural: ADD #STATE-WTHLD ( 8 ) #STATE-WTHLD ( 11 ) #STATE-WTHLD ( 14 ) #STATE-WTHLD ( 17 ) #STATE-WTHLD ( 20 ) #STATE-WTHLD ( 23 ) #STATE-WTHLD ( 24 ) TO #STATE-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #STATE-WTHLD(17) #STATE-WTHLD(20) #STATE-WTHLD(21)
                //*      #STATE-WTHLD(22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Local_Wthld.getValue(22).compute(new ComputeParameters(false, pnd_Counters_Pnd_Local_Wthld.getValue(22)), pnd_Counters_Pnd_Local_Wthld.getValue(22).add(pnd_Counters_Pnd_Local_Wthld.getValue(8)).add(pnd_Counters_Pnd_Local_Wthld.getValue(11)).add(pnd_Counters_Pnd_Local_Wthld.getValue(14)).add(pnd_Counters_Pnd_Local_Wthld.getValue(17)).add(pnd_Counters_Pnd_Local_Wthld.getValue(20)).add(pnd_Counters_Pnd_Local_Wthld.getValue(21))); //Natural: ADD #LOCAL-WTHLD ( 8 ) #LOCAL-WTHLD ( 11 ) #LOCAL-WTHLD ( 14 ) #LOCAL-WTHLD ( 17 ) #LOCAL-WTHLD ( 20 ) #LOCAL-WTHLD ( 21 ) TO #LOCAL-WTHLD ( 22 )
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Pr_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Pr_Wthld.getValue(25)), pnd_Counters_Pnd_Pr_Wthld.getValue(25).add(pnd_Counters_Pnd_Pr_Wthld.getValue(8)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(11)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(14)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(17)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(20)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(23)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(24))); //Natural: ADD #PR-WTHLD ( 8 ) #PR-WTHLD ( 11 ) #PR-WTHLD ( 14 ) #PR-WTHLD ( 17 ) #PR-WTHLD ( 20 ) #PR-WTHLD ( 23 ) #PR-WTHLD ( 24 ) TO #PR-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #PR-WTHLD   (17) #PR-WTHLD   (20) #PR-WTHLD   (21)
                //*      #PR-WTHLD   (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Can_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Can_Wthld.getValue(25)), pnd_Counters_Pnd_Can_Wthld.getValue(25).add(pnd_Counters_Pnd_Can_Wthld.getValue(8)).add(pnd_Counters_Pnd_Can_Wthld.getValue(11)).add(pnd_Counters_Pnd_Can_Wthld.getValue(14)).add(pnd_Counters_Pnd_Can_Wthld.getValue(17)).add(pnd_Counters_Pnd_Can_Wthld.getValue(20)).add(pnd_Counters_Pnd_Can_Wthld.getValue(23)).add(pnd_Counters_Pnd_Can_Wthld.getValue(24))); //Natural: ADD #CAN-WTHLD ( 8 ) #CAN-WTHLD ( 11 ) #CAN-WTHLD ( 14 ) #CAN-WTHLD ( 17 ) #CAN-WTHLD ( 20 ) #CAN-WTHLD ( 23 ) #CAN-WTHLD ( 24 ) TO #CAN-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #CAN-WTHLD  (17) #CAN-WTHLD  (20) #CAN-WTHLD  (21)
                //*      #CAN-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld2.setValue(pnd_Name);                                                                                                              //Natural: MOVE #NAME TO #PRTFLD2
            pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9"));                //Natural: MOVE EDITED #TRANS-CNT ( #C ) ( EM = ZZZZZZZZ9 ) TO #PRTFLD3
            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZZZ9.99-"));         //Natural: MOVE EDITED #GROSS-AMT ( #C ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));              //Natural: MOVE EDITED #IVC-AMT ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
            pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZZZ9.99-"));       //Natural: MOVE EDITED #TAXABLE-AMT ( #C ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD6
            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZ9.99-"));               //Natural: MOVE EDITED #INT-AMT ( #C ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZ9.99-"));           //Natural: MOVE EDITED #FED-WTHLD ( #C ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD8
            pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));            //Natural: MOVE EDITED #NRA-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
            pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Counters_Pnd_State_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));        //Natural: MOVE EDITED #STATE-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
            pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZ9.99-"));         //Natural: MOVE EDITED #LOCAL-WTHLD ( #C ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
            pnd_Print0_Fields_Pnd_Prtfld03.setValueEdited(pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZ9.99-"));          //Natural: MOVE EDITED #PR-WTHLD ( #C ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD03
            pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));          //Natural: MOVE EDITED #CAN-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,pnd_Print_Fields_Pnd_Prtfld9,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 73T #PRINT0-FIELDS
                TabSetting(73),pnd_Print0_Fields_Pnd_Prtfld01,pnd_Print0_Fields_Pnd_Prtfld02,pnd_Print0_Fields_Pnd_Prtfld03,pnd_Print0_Fields_Pnd_Prtfld04);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Print_Fields.reset();                                                                                                                                     //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
            pnd_Print0_Fields.reset();
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  11/2014 END
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"***** NO. OF RECORDS READ      : ",pnd_Save_Areas_Pnd_Rec_Read,NEWLINE);                      //Natural: WRITE ( 1 ) /// '***** NO. OF RECORDS READ      : ' #REC-READ /
        if (Global.isEscape()) return;
        //*  11/2014 START
        pnd_Savers.resetInitial();                                                                                                                                        //Natural: RESET INITIAL #SAVERS #COUNTERS ( * ) #PRINT-FIELDS #PRINT0-FIELDS #SAVE-AREAS #CTR-CHECK-AMOUNTS #PRT-FF1-CNTRL #KEY-DATE #KEY-DATE-IP #SUM-VAR #PARAM-INTF-FROM-DATE #PARAM-INTF-TO-DATE #NAME #TTL-VAR ( * )
        pnd_Counters.getValue("*").resetInitial();
        pnd_Print_Fields.resetInitial();
        pnd_Print0_Fields.resetInitial();
        pnd_Save_Areas.resetInitial();
        pnd_Ctr_Check_Amounts.resetInitial();
        pnd_Prt_Ff1_Cntrl.resetInitial();
        pnd_Key_Date.resetInitial();
        pnd_Key_Date_Ip.resetInitial();
        pnd_Sum_Var.resetInitial();
        pnd_Param_Intf_From_Date.resetInitial();
        pnd_Param_Intf_To_Date.resetInitial();
        pnd_Name.resetInitial();
        pnd_Ttl_Var.getValue("*").resetInitial();
        pnd_Fatca.setValue(true);                                                                                                                                         //Natural: ASSIGN #FATCA := TRUE
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Var_File1_Pnd_Freq.equals("D")))                                                                                                                //Natural: IF #FREQ = 'D'
        {
                                                                                                                                                                          //Natural: PERFORM INFO-FROM-F98T5
            sub_Info_From_F98t5();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD #FLAT-FILE3
        while (condition(getWorkFiles().read(2, ldaTwrl0902.getPnd_Flat_File3())))
        {
            CheckAtStartofData670();

            //*  11/2014
            if (condition(!(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Twrpymnt_Fatca_Ind().equals("Y"))))                                                                     //Natural: ACCEPT IF #FF3-TWRPYMNT-FATCA-IND EQ 'Y'
            {
                continue;
            }
            pnd_Save_Areas_Pnd_Rec_Read.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-READ
            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code().equals(pnd_Savers_Pnd_Sv_Company)))                                                        //Natural: IF #FF3-COMPANY-CODE = #SV-COMPANY
            {
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals(pnd_Savers_Pnd_Sv_Tax_Ctz)))                                                 //Natural: IF #FF3-TAX-CITIZENSHIP = #SV-TAX-CTZ
                {
                    if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().equals(pnd_Savers_Pnd_Sv_Srce_Code)))                                               //Natural: IF #FF3-SOURCE-CODE = #SV-SRCE-CODE
                    {
                        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type().equals(pnd_Savers_Pnd_Sv_Payset)))                                              //Natural: IF #FF3-PAYSET-TYPE = #SV-PAYSET
                        {
                            ignore();
                            //*  BREAK OF PAYTYP-SETTYP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM PAYSET-BRK
                            sub_Payset_Brk();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                            sub_Reset_Ctrs();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  BREAK OF SRCE-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
                        sub_Source_Brk();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                        sub_Reset_Ctrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BREAK OF TAX-CTZ
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                    sub_Company_Brk();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                    sub_Account_Grandtotal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                          //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  BREAK OF COMPANY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
                sub_Company_Brk();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PRINT MATCH REPORT
                                                                                                                                                                          //Natural: PERFORM CHECK-AMOUNTS
                sub_Check_Amounts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
                sub_Account_Grandtotal();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ttl_Var.getValue("*").reset();                                                                                                                        //Natural: RESET #TTL-VAR ( * )
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                              //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
                pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                 //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*                      *** PROCESS RECORD ***
            pnd_Save_Areas_Pnd_Tax_Amt.reset();                                                                                                                           //Natural: RESET #TAX-AMT
            //*  RECHAR  5/30/2002
            //*  ROLLOVER
            //*  RS0711
            short decideConditionsMet716 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FF3-TAX-CITIZENSHIP = 'U' AND ( #FF3-DISTRIBUTION-CDE = 'N' OR = 'R' OR = 'G' OR = 'H' OR = 'Z' OR = '[' OR = '6' OR = '=' )
            if (condition((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship().equals("U") && (((((((ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("N") 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("R")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("G")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("H")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("Z")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("[")) || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("6")) 
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Distribution_Cde().equals("=")))))
            {
                decideConditionsMet716++;
                //*                                        BG
                //*        OR= '6')      H4
                pnd_Save_Areas_Pnd_Tax_Amt.setValue(0);                                                                                                                   //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: WHEN #FF3-IVC-PROTECT OR #FF3-IVC-IND = 'K'
            else if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Protect().getBoolean() || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Ind().equals("K")))
            {
                decideConditionsMet716++;
                pnd_Save_Areas_Pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Save_Areas_Pnd_Tax_Amt), ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt().subtract(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt())); //Natural: ASSIGN #TAX-AMT := #FF3-GROSS-AMT - #FF3-IVC-AMT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Tax_Amt.setValue(0);                                                                                                                   //Natural: ASSIGN #TAX-AMT := 0
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Save_Areas_Pnd_Temp_Pr_Wthld.reset();                                                                                                                     //Natural: RESET #TEMP-PR-WTHLD #TEMP-STATE-WTHLD
            pnd_Save_Areas_Pnd_Temp_State_Wthld.reset();
            if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("PR") || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("RQ")      //Natural: IF #FF3-RESIDENCY-CODE = 'PR' OR = 'RQ' OR = '42'
                || ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Residency_Code().equals("42")))
            {
                pnd_Save_Areas_Pnd_Temp_Pr_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                               //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-PR-WTHLD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Temp_State_Wthld.nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_State_Wthld());                                                            //Natural: ADD #FF3-STATE-WTHLD TO #TEMP-STATE-WTHLD
                //*  +1 COMPANY   RM 9/16/04
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #K 1 5
            for (pnd_Save_Areas_Pnd_K.setValue(1); condition(pnd_Save_Areas_Pnd_K.lessOrEqual(5)); pnd_Save_Areas_Pnd_K.nadd(1))
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Save_Areas_Pnd_K).nadd(1);                                                                                        //Natural: ADD 1 TO #TRANS-CNT ( #K )
                pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Gross_Amt());                                        //Natural: ADD #FF3-GROSS-AMT TO #GROSS-AMT ( #K )
                pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Ivc_Amt());                                            //Natural: ADD #FF3-IVC-AMT TO #IVC-AMT ( #K )
                pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Tax_Amt);                                                             //Natural: ADD #TAX-AMT TO #TAXABLE-AMT ( #K )
                pnd_Counters_Pnd_Int_Amt.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Int_Amt());                                            //Natural: ADD #FF3-INT-AMT TO #INT-AMT ( #K )
                pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Fed_Wthld_Amt());                                    //Natural: ADD #FF3-FED-WTHLD-AMT TO #FED-WTHLD ( #K )
                pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Nra_Wthld_Amt());                                    //Natural: ADD #FF3-NRA-WTHLD-AMT TO #NRA-WTHLD ( #K )
                pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Local_Wthld());                                    //Natural: ADD #FF3-LOCAL-WTHLD TO #LOCAL-WTHLD ( #K )
                pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Temp_Pr_Wthld);                                                          //Natural: ADD #TEMP-PR-WTHLD TO #PR-WTHLD ( #K )
                pnd_Counters_Pnd_State_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(pnd_Save_Areas_Pnd_Temp_State_Wthld);                                                    //Natural: ADD #TEMP-STATE-WTHLD TO #STATE-WTHLD ( #K )
                pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Save_Areas_Pnd_K).nadd(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Can_Wthld_Amt());                                    //Natural: ADD #FF3-CAN-WTHLD-AMT TO #CAN-WTHLD ( #K )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Save_Areas_Pnd_Rec_Read.equals(getZero())))                                                                                                     //Natural: IF #REC-READ = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40)," **** NO RECORDS PROCESSED **** ");                            //Natural: WRITE ( 1 ) //// 40T ' **** NO RECORDS PROCESSED **** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Save_Areas_Pnd_Eof.setValue(true);                                                                                                                        //Natural: ASSIGN #EOF := TRUE
                                                                                                                                                                          //Natural: PERFORM COMPANY-BRK
            sub_Company_Brk();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-GRANDTOTAL
            sub_Account_Grandtotal();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Var_File1_Pnd_Freq.equals("D")))                                                                                                            //Natural: IF #FREQ = 'D'
            {
                //*  TO PRINT MATCH REPORT
                                                                                                                                                                          //Natural: PERFORM CHECK-AMOUNTS
                sub_Check_Amounts();
                if (condition(Global.isEscape())) {return;}
                pnd_Ttl_Var.getValue("*").reset();                                                                                                                        //Natural: RESET #TTL-VAR ( * )
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                sub_Reset_Ctrs();
                if (condition(Global.isEscape())) {return;}
                //*  PASSED THE CHECK-AMOUNTS SUBROUTINE
                if (condition(pnd_Ctr_Check_Amounts_Pnd_Amt_Check.getBoolean()))                                                                                          //Natural: IF #AMT-CHECK
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-FLATFILE1
                    sub_Check_Flatfile1();
                    if (condition(Global.isEscape())) {return;}
                    //*  JH 06/24/2002
                    getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"STATUS OF CONTROL FILE : ",NEWLINE,NEWLINE,NEWLINE,NEWLINE,"  AREA #1  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(1), //Natural: WRITE /// 'STATUS OF CONTROL FILE : ' // // '  AREA #1  : ' #PRT-MESSAGE ( 1 ) / '  AREA #2  : ' #PRT-MESSAGE ( 2 ) / '  AREA #3  : ' #PRT-MESSAGE ( 3 ) / '  AREA #4  : ' #PRT-MESSAGE ( 4 ) /// #CO ( * ) / #SC ( *,* )
                        NEWLINE,"  AREA #2  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(2),NEWLINE,"  AREA #3  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(3),
                        NEWLINE,"  AREA #4  : ",pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(4),NEWLINE,NEWLINE,NEWLINE,pnd_Ctr_Check_Amounts_Pnd_Co.getValue("*"),
                        NEWLINE,pnd_Ctr_Check_Amounts_Pnd_Sc.getValue("*","*"));
                    if (Global.isEscape()) return;
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"ERROR CODES LEGEND:",NEWLINE,NEWLINE,"1 - UNBALANCE GROSS AMOUNT",                //Natural: WRITE ( 2 ) /// 'ERROR CODES LEGEND:' // '1 - UNBALANCE GROSS AMOUNT' / '2 - UNBALANCE IVC AMOUNT' / '3 - UNBALANCE INT AMOUNT' / '4 - UNBALANCE FEDERAL AMOUNT' / '5 - UNBALANCE STATE AMOUNT' / '6 - UNBALANCE NRA AMOUNT' / '7 - UNBALANCE LOCAL AMOUNT' / '8 - UNBALANCE CANADIAN AMOUNT'
                        NEWLINE,"2 - UNBALANCE IVC AMOUNT",NEWLINE,"3 - UNBALANCE INT AMOUNT",NEWLINE,"4 - UNBALANCE FEDERAL AMOUNT",NEWLINE,"5 - UNBALANCE STATE AMOUNT",
                        NEWLINE,"6 - UNBALANCE NRA AMOUNT",NEWLINE,"7 - UNBALANCE LOCAL AMOUNT",NEWLINE,"8 - UNBALANCE CANADIAN AMOUNT");
                    if (Global.isEscape()) return;
                    //*  ADD 'SI'   RM 6/12/03
                    //*  ADD 'OP'   RM 9/16/04
                    //*  ADD 'NV'   RM 06/06/06
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(25),"-- NO VALID SOURCE CODES ","(AM,AP,CP,DC,DS,ED,EW,GS,IA,IP,IS,MS,NV,NZ,OP,RE,SI,SS,VL)", //Natural: WRITE ( 2 ) /// 25T '-- NO VALID SOURCE CODES ' '(AM,AP,CP,DC,DS,ED,EW,GS,IA,IP,IS,MS,NV,NZ,OP,RE,SI,SS,VL)' 'to be matched --'
                        "to be matched --");
                    if (Global.isEscape()) return;
                    //*  ADD 'VL'   RM 10/30/06
                }                                                                                                                                                         //Natural: END-IF
                //*  YTD                       /* ADD 'AM'   /* 09/29/11
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"*** TOTAL NUMBER OF RECORDS : ",pnd_Var_File1_Pnd_Total_Rec);                         //Natural: WRITE ( 1 ) /// '*** TOTAL NUMBER OF RECORDS : ' #TOTAL-REC
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*   DY1
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR #C = 6 TO 22
        FOR04:                                                                                                                                                            //Natural: FOR #C = 6 TO 25
        for (pnd_Ctr_Check_Amounts_Pnd_C.setValue(6); condition(pnd_Ctr_Check_Amounts_Pnd_C.lessOrEqual(25)); pnd_Ctr_Check_Amounts_Pnd_C.nadd(1))
        {
            short decideConditionsMet786 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #C;//Natural: VALUE 6
            if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(6))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("CREF-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(7))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("CREF-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'CREF-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(8))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL CREF -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL CREF -->: '
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(9))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("LIFE-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(10))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("LIFE-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'LIFE-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(11))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL LIFE -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL LIFE -->: '
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(12))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TIAA-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(13))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TIAA-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TIAA-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(14))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL TIAA -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TIAA -->: '
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(15))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TCII-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(16))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TCII-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TCII-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(17))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL TCII -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TCII -->: '
                //*   DY1 FIX BEGINS  >>
                //*     VALUE 18  #NAME := 'TRST-FOREIGN CTZ : '
                //*     VALUE 19  #NAME := 'TRST-U.S. CTZ    : '
                //*     VALUE 20  #NAME := '-- TOTAL TRST -->: '
                //*     VALUE 21  #NAME := '-- TOTAL OTHERS->: '
                //*     VALUE 22  #NAME := '-- GRAND TOTALS->: '
            }                                                                                                                                                             //Natural: VALUE 18
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(18))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("FSB-FOREIGN CTZ  : ");                                                                                                                 //Natural: ASSIGN #NAME := 'FSB-FOREIGN CTZ  : '
            }                                                                                                                                                             //Natural: VALUE 19
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(19))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("FSB-U.S. CTZ     : ");                                                                                                                 //Natural: ASSIGN #NAME := 'FSB-U.S. CTZ     : '
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(20))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL FSB  -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL FSB  -->: '
            }                                                                                                                                                             //Natural: VALUE 21
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(21))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TRST-FOREIGN CTZ : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-FOREIGN CTZ : '
            }                                                                                                                                                             //Natural: VALUE 22
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(22))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("TRST-U.S. CTZ    : ");                                                                                                                 //Natural: ASSIGN #NAME := 'TRST-U.S. CTZ    : '
            }                                                                                                                                                             //Natural: VALUE 23
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(23))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL TRST -->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL TRST -->: '
            }                                                                                                                                                             //Natural: VALUE 24
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(24))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- TOTAL OTHERS->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- TOTAL OTHERS->: '
            }                                                                                                                                                             //Natural: VALUE 25
            else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(25))))
            {
                decideConditionsMet786++;
                pnd_Name.setValue("-- GRAND TOTALS->: ");                                                                                                                 //Natural: ASSIGN #NAME := '-- GRAND TOTALS->: '
                //*   DY1 FIX ENDS    <<
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF #C = 6  OR = 22                         /* DY1
            //*  DY1
            if (condition(pnd_Ctr_Check_Amounts_Pnd_C.equals(6) || pnd_Ctr_Check_Amounts_Pnd_C.equals(25)))                                                               //Natural: IF #C = 6 OR = 25
            {
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",NEWLINE,"RUNDATE    : ",Global.getDATX(),  //Natural: WRITE ( 1 ) / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' / 'RUNDATE    : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' / 'RUNTIME    : ' *TIMX 51T '-- SUMMARY REPORT - -' / 'TAX YEAR   : ' #TAX-YEAR 41T 'DATE RANGE : ' #START-DATE 'THRU' #END-DATE /// 11X 'COMPANY' 8X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 6X 'FEDERAL TAX' 2X #HDG-LIT / 9X 'TAX CTZNSHP' 6X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 7X 'WITHHOLDING' 4X 'WITHHOLDING' / 74T '-----------' 4X '-----------' 5X '-----------' 4X '-----------' / 75T 'STATE TAX' 6X 'LOCAL TAX' 6X 'PUERTO RICO' 5X 'CANADIAN' / 74T 'WITHHOLDING' 4X 'WITHHOLDING' 5X 'WITHHOLDING' 4X 'WITHHOLDING' //
                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",NEWLINE,"RUNTIME    : ",Global.getTIMX(),new 
                    TabSetting(51),"-- SUMMARY REPORT - -",NEWLINE,"TAX YEAR   : ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(41),"DATE RANGE : ",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,new 
                    ColumnSpacing(11),"COMPANY",new ColumnSpacing(8),"TRANS",new ColumnSpacing(11),"GROSS",new ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new 
                    ColumnSpacing(7),"INTEREST",new ColumnSpacing(6),"FEDERAL TAX",new ColumnSpacing(2),pnd_Hdg_Lit,NEWLINE,new ColumnSpacing(9),"TAX CTZNSHP",new 
                    ColumnSpacing(6),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new 
                    ColumnSpacing(7),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,new TabSetting(74),"-----------",new ColumnSpacing(4),"-----------",new 
                    ColumnSpacing(5),"-----------",new ColumnSpacing(4),"-----------",NEWLINE,new TabSetting(75),"STATE TAX",new ColumnSpacing(6),"LOCAL TAX",new 
                    ColumnSpacing(6),"PUERTO RICO",new ColumnSpacing(5),"CANADIAN",NEWLINE,new TabSetting(74),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",new 
                    ColumnSpacing(5),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      12X 'TAXABLE' 7X 'INTEREST'  6X 'FEDERAL TAX'  6X 'NRA TAX'
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #C = 22                               /*  DY1
            //*   DY1
            if (condition(pnd_Ctr_Check_Amounts_Pnd_C.equals(25)))                                                                                                        //Natural: IF #C = 25
            {
                pnd_Counters_Pnd_Trans_Cnt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Trans_Cnt.getValue(25)), pnd_Counters_Pnd_Trans_Cnt.getValue(25).add(pnd_Counters_Pnd_Trans_Cnt.getValue(8)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(11)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(14)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(17)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(20)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(23)).add(pnd_Counters_Pnd_Trans_Cnt.getValue(24))); //Natural: ADD #TRANS-CNT ( 8 ) #TRANS-CNT ( 11 ) #TRANS-CNT ( 14 ) #TRANS-CNT ( 17 ) #TRANS-CNT ( 20 ) #TRANS-CNT ( 23 ) #TRANS-CNT ( 24 ) TO #TRANS-CNT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #TRANS-CNT  (17) #TRANS-CNT  (20) #TRANS-CNT  (21)
                //*      #TRANS-CNT  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Gross_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Gross_Amt.getValue(25)), pnd_Counters_Pnd_Gross_Amt.getValue(25).add(pnd_Counters_Pnd_Gross_Amt.getValue(8)).add(pnd_Counters_Pnd_Gross_Amt.getValue(11)).add(pnd_Counters_Pnd_Gross_Amt.getValue(14)).add(pnd_Counters_Pnd_Gross_Amt.getValue(17)).add(pnd_Counters_Pnd_Gross_Amt.getValue(20)).add(pnd_Counters_Pnd_Gross_Amt.getValue(23)).add(pnd_Counters_Pnd_Gross_Amt.getValue(24))); //Natural: ADD #GROSS-AMT ( 8 ) #GROSS-AMT ( 11 ) #GROSS-AMT ( 14 ) #GROSS-AMT ( 17 ) #GROSS-AMT ( 20 ) #GROSS-AMT ( 23 ) #GROSS-AMT ( 24 ) TO #GROSS-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #GROSS-AMT  (17) #GROSS-AMT  (20) #GROSS-AMT  (21)
                //*      #GROSS-AMT  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Ivc_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Ivc_Amt.getValue(25)), pnd_Counters_Pnd_Ivc_Amt.getValue(25).add(pnd_Counters_Pnd_Ivc_Amt.getValue(8)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(11)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(14)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(17)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(20)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(23)).add(pnd_Counters_Pnd_Ivc_Amt.getValue(24))); //Natural: ADD #IVC-AMT ( 8 ) #IVC-AMT ( 11 ) #IVC-AMT ( 14 ) #IVC-AMT ( 17 ) #IVC-AMT ( 20 ) #IVC-AMT ( 23 ) #IVC-AMT ( 24 ) TO #IVC-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #IVC-AMT    (17) #IVC-AMT    (20) #IVC-AMT    (21)
                //*      #IVC-AMT    (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Taxable_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Taxable_Amt.getValue(25)), pnd_Counters_Pnd_Taxable_Amt.getValue(25).add(pnd_Counters_Pnd_Taxable_Amt.getValue(8)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(11)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(14)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(17)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(20)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(23)).add(pnd_Counters_Pnd_Taxable_Amt.getValue(24))); //Natural: ADD #TAXABLE-AMT ( 8 ) #TAXABLE-AMT ( 11 ) #TAXABLE-AMT ( 14 ) #TAXABLE-AMT ( 17 ) #TAXABLE-AMT ( 20 ) #TAXABLE-AMT ( 23 ) #TAXABLE-AMT ( 24 ) TO #TAXABLE-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #TAXABLE-AMT(17) #TAXABLE-AMT(20) #TAXABLE-AMT(21)
                //*      #TAXABLE-AMT(22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Int_Amt.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Int_Amt.getValue(25)), pnd_Counters_Pnd_Int_Amt.getValue(25).add(pnd_Counters_Pnd_Int_Amt.getValue(8)).add(pnd_Counters_Pnd_Int_Amt.getValue(11)).add(pnd_Counters_Pnd_Int_Amt.getValue(14)).add(pnd_Counters_Pnd_Int_Amt.getValue(17)).add(pnd_Counters_Pnd_Int_Amt.getValue(20)).add(pnd_Counters_Pnd_Int_Amt.getValue(23)).add(pnd_Counters_Pnd_Int_Amt.getValue(24))); //Natural: ADD #INT-AMT ( 8 ) #INT-AMT ( 11 ) #INT-AMT ( 14 ) #INT-AMT ( 17 ) #INT-AMT ( 20 ) #INT-AMT ( 23 ) #INT-AMT ( 24 ) TO #INT-AMT ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #INT-AMT    (17) #INT-AMT    (20) #INT-AMT    (21)
                //*      #INT-AMT    (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Fed_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Fed_Wthld.getValue(25)), pnd_Counters_Pnd_Fed_Wthld.getValue(25).add(pnd_Counters_Pnd_Fed_Wthld.getValue(8)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(11)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(14)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(17)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(20)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(23)).add(pnd_Counters_Pnd_Fed_Wthld.getValue(24))); //Natural: ADD #FED-WTHLD ( 8 ) #FED-WTHLD ( 11 ) #FED-WTHLD ( 14 ) #FED-WTHLD ( 17 ) #FED-WTHLD ( 20 ) #FED-WTHLD ( 23 ) #FED-WTHLD ( 24 ) TO #FED-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #FED-WTHLD  (17) #FED-WTHLD  (20) #FED-WTHLD  (21)
                //*      #FED-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Nra_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Nra_Wthld.getValue(25)), pnd_Counters_Pnd_Nra_Wthld.getValue(25).add(pnd_Counters_Pnd_Nra_Wthld.getValue(8)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(11)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(14)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(17)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(20)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(23)).add(pnd_Counters_Pnd_Nra_Wthld.getValue(24))); //Natural: ADD #NRA-WTHLD ( 8 ) #NRA-WTHLD ( 11 ) #NRA-WTHLD ( 14 ) #NRA-WTHLD ( 17 ) #NRA-WTHLD ( 20 ) #NRA-WTHLD ( 23 ) #NRA-WTHLD ( 24 ) TO #NRA-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #NRA-WTHLD  (17) #NRA-WTHLD  (20) #NRA-WTHLD  (21)
                //*      #NRA-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_State_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_State_Wthld.getValue(25)), pnd_Counters_Pnd_State_Wthld.getValue(25).add(pnd_Counters_Pnd_State_Wthld.getValue(8)).add(pnd_Counters_Pnd_State_Wthld.getValue(11)).add(pnd_Counters_Pnd_State_Wthld.getValue(14)).add(pnd_Counters_Pnd_State_Wthld.getValue(17)).add(pnd_Counters_Pnd_State_Wthld.getValue(20)).add(pnd_Counters_Pnd_State_Wthld.getValue(23)).add(pnd_Counters_Pnd_State_Wthld.getValue(24))); //Natural: ADD #STATE-WTHLD ( 8 ) #STATE-WTHLD ( 11 ) #STATE-WTHLD ( 14 ) #STATE-WTHLD ( 17 ) #STATE-WTHLD ( 20 ) #STATE-WTHLD ( 23 ) #STATE-WTHLD ( 24 ) TO #STATE-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #STATE-WTHLD(17) #STATE-WTHLD(20) #STATE-WTHLD(21)
                //*      #STATE-WTHLD(22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Local_Wthld.getValue(22).compute(new ComputeParameters(false, pnd_Counters_Pnd_Local_Wthld.getValue(22)), pnd_Counters_Pnd_Local_Wthld.getValue(22).add(pnd_Counters_Pnd_Local_Wthld.getValue(8)).add(pnd_Counters_Pnd_Local_Wthld.getValue(11)).add(pnd_Counters_Pnd_Local_Wthld.getValue(14)).add(pnd_Counters_Pnd_Local_Wthld.getValue(17)).add(pnd_Counters_Pnd_Local_Wthld.getValue(20)).add(pnd_Counters_Pnd_Local_Wthld.getValue(21))); //Natural: ADD #LOCAL-WTHLD ( 8 ) #LOCAL-WTHLD ( 11 ) #LOCAL-WTHLD ( 14 ) #LOCAL-WTHLD ( 17 ) #LOCAL-WTHLD ( 20 ) #LOCAL-WTHLD ( 21 ) TO #LOCAL-WTHLD ( 22 )
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Pr_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Pr_Wthld.getValue(25)), pnd_Counters_Pnd_Pr_Wthld.getValue(25).add(pnd_Counters_Pnd_Pr_Wthld.getValue(8)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(11)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(14)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(17)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(20)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(23)).add(pnd_Counters_Pnd_Pr_Wthld.getValue(24))); //Natural: ADD #PR-WTHLD ( 8 ) #PR-WTHLD ( 11 ) #PR-WTHLD ( 14 ) #PR-WTHLD ( 17 ) #PR-WTHLD ( 20 ) #PR-WTHLD ( 23 ) #PR-WTHLD ( 24 ) TO #PR-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #PR-WTHLD   (17) #PR-WTHLD   (20) #PR-WTHLD   (21)
                //*      #PR-WTHLD   (22)
                //*  DY1 FIX ENDS   <<
                pnd_Counters_Pnd_Can_Wthld.getValue(25).compute(new ComputeParameters(false, pnd_Counters_Pnd_Can_Wthld.getValue(25)), pnd_Counters_Pnd_Can_Wthld.getValue(25).add(pnd_Counters_Pnd_Can_Wthld.getValue(8)).add(pnd_Counters_Pnd_Can_Wthld.getValue(11)).add(pnd_Counters_Pnd_Can_Wthld.getValue(14)).add(pnd_Counters_Pnd_Can_Wthld.getValue(17)).add(pnd_Counters_Pnd_Can_Wthld.getValue(20)).add(pnd_Counters_Pnd_Can_Wthld.getValue(23)).add(pnd_Counters_Pnd_Can_Wthld.getValue(24))); //Natural: ADD #CAN-WTHLD ( 8 ) #CAN-WTHLD ( 11 ) #CAN-WTHLD ( 14 ) #CAN-WTHLD ( 17 ) #CAN-WTHLD ( 20 ) #CAN-WTHLD ( 23 ) #CAN-WTHLD ( 24 ) TO #CAN-WTHLD ( 25 )
                //*  DY1 FIX BEGINS >>
                //*      #CAN-WTHLD  (17) #CAN-WTHLD  (20) #CAN-WTHLD  (21)
                //*      #CAN-WTHLD  (22)
                //*  DY1 FIX ENDS   <<
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld2.setValue(pnd_Name);                                                                                                              //Natural: MOVE #NAME TO #PRTFLD2
            pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9"));                //Natural: MOVE EDITED #TRANS-CNT ( #C ) ( EM = ZZZZZZZZ9 ) TO #PRTFLD3
            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZZZ9.99-"));         //Natural: MOVE EDITED #GROSS-AMT ( #C ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));              //Natural: MOVE EDITED #IVC-AMT ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
            pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZZZ9.99-"));       //Natural: MOVE EDITED #TAXABLE-AMT ( #C ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD6
            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZ9.99-"));               //Natural: MOVE EDITED #INT-AMT ( #C ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZ9.99-"));           //Natural: MOVE EDITED #FED-WTHLD ( #C ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD8
            pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));            //Natural: MOVE EDITED #NRA-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
            pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Counters_Pnd_State_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));        //Natural: MOVE EDITED #STATE-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
            pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZ9.99-"));         //Natural: MOVE EDITED #LOCAL-WTHLD ( #C ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
            pnd_Print0_Fields_Pnd_Prtfld03.setValueEdited(pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZZ9.99-"));          //Natural: MOVE EDITED #PR-WTHLD ( #C ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD03
            pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C),new ReportEditMask("ZZZZZZZZ9.99-"));          //Natural: MOVE EDITED #CAN-WTHLD ( #C ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,pnd_Print_Fields_Pnd_Prtfld9,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 73T #PRINT0-FIELDS
                TabSetting(73),pnd_Print0_Fields_Pnd_Prtfld01,pnd_Print0_Fields_Pnd_Prtfld02,pnd_Print0_Fields_Pnd_Prtfld03,pnd_Print0_Fields_Pnd_Prtfld04);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Print_Fields.reset();                                                                                                                                     //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
            pnd_Print0_Fields.reset();
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"***** NO. OF RECORDS READ      : ",pnd_Save_Areas_Pnd_Rec_Read,NEWLINE);                      //Natural: WRITE ( 1 ) /// '***** NO. OF RECORDS READ      : ' #REC-READ /
        if (Global.isEscape()) return;
        pnd_Fatca.setValue(false);                                                                                                                                        //Natural: ASSIGN #FATCA := FALSE
        pnd_Nra_Rpt.setValue(true);                                                                                                                                       //Natural: ASSIGN #NRA-RPT := TRUE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #TOT-NDX 1 #CMPY-CNT
        for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
        {
            if (condition(pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                              //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress("Grand Total", pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx)));                                 //Natural: COMPRESS 'Grand Total' #TOT-CMPNY ( #TOT-NDX ) TO #PRTFLD2
            pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZ9"));                        //Natural: MOVE EDITED #TOT-TRANS-CNT ( #TOT-NDX ) ( EM = ZZZZZZZZ9 ) TO #PRTFLD3
            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZZZZ9.99-"));                 //Natural: MOVE EDITED #TOT-GROSS-AMT ( #TOT-NDX ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZ9.99-"));                      //Natural: MOVE EDITED #TOT-IVC-AMT ( #TOT-NDX ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
            pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZZZZ9.99-"));               //Natural: MOVE EDITED #TOT-TAXABLE-AMT ( #TOT-NDX ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD6
            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ9.99-"));                       //Natural: MOVE EDITED #TOT-INT-AMT ( #TOT-NDX ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Fed_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZZ9.99-"));                   //Natural: MOVE EDITED #TOT-FED-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD8
            pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Nra_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZ9.99-"));                    //Natural: MOVE EDITED #TOT-NRA-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
            pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Tot_Counters_Pnd_Tot_State_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZ9.99-"));                //Natural: MOVE EDITED #TOT-STATE-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
            pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Local_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZ9.99-"));                 //Natural: MOVE EDITED #TOT-LOCAL-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
            pnd_Print0_Fields_Pnd_Prtfld03.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Pr_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZZ9.99-"));                  //Natural: MOVE EDITED #TOT-PR-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZZZ9.99- ) TO #PRTFLD03
            pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Tot_Counters_Pnd_Tot_Can_Wthld.getValue(pnd_Tot_Ndx),new ReportEditMask("ZZZZZZZZ9.99-"));                  //Natural: MOVE EDITED #TOT-CAN-WTHLD ( #TOT-NDX ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,pnd_Print_Fields_Pnd_Prtfld9,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 73T #PRINT0-FIELDS
                TabSetting(73),pnd_Print0_Fields_Pnd_Prtfld01,pnd_Print0_Fields_Pnd_Prtfld02,pnd_Print0_Fields_Pnd_Prtfld03,pnd_Print0_Fields_Pnd_Prtfld04);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  -----------------------------------------------------------------
        //*                  ----------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //* ***********************************************************************
        //*  -----------------------------------------------------------------
        //*                  ------------
        //*  -----------------------------------------------------------------
        //*  NONE IGNORE
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*                  ----------
        //*  -----------------------------------------------------------------
        //*                  -------------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*                  ------------
        //*  -----------------------------------------------------------------
        //*   DY1 FIX BEGINS >>
        //*     WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'F'  #C := 18
        //*     WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'U'  #C := 19
        //*     WHEN NONE                                     #C := 21
        //*     VALUE 18,19   #C := 20
        //*  NONE          IGNORE
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*                  -------------
        //*  F1. FOR #E 1  5
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*  -----------------------------------------------------------------
        //*                  ---------------
        //*  FOR #E 1 5
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  -----------------------------------------------------------------                                                                                            //Natural: AT TOP OF PAGE ( 3 )
        //*  GET FINANCIAL AMOUNTS OF DEFINED SOURCE CODES FROM FEEDER SYSTEM TABLE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INFO-FROM-F98T5
        //*                  ---------------
        //*  FOR #X 1 5
        //*       VALUE 5  #COMP := 'X'
        //*  -----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REC
    }
    private void sub_Payset_Brk() throws Exception                                                                                                                        //Natural: PAYSET-BRK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Areas_Pnd_Type_Trans.setValue(pnd_Savers_Pnd_Sv_Payset);                                                                                                 //Natural: ASSIGN #TYPE-TRANS := #SV-PAYSET
        DbsUtil.callnat(Twrn0901.class , getCurrentProcessState(), pnd_Var_File1_Pnd_Tax_Year_A, pnd_Save_Areas_Pnd_Type_Trans, pnd_Save_Areas_Pnd_Trans_Desc);           //Natural: CALLNAT 'TWRN0901' #TAX-YEAR-A #TYPE-TRANS #TRANS-DESC
        if (condition(Global.isEscape())) return;
        pnd_Save_Areas_Pnd_Occ.setValue(1);                                                                                                                               //Natural: ASSIGN #OCC := 1
        if (condition(pnd_Save_Areas_Pnd_First.getBoolean()))                                                                                                             //Natural: IF #FIRST
        {
                                                                                                                                                                          //Natural: PERFORM SOURCE-TO-PRT
            sub_Source_To_Prt();
            if (condition(Global.isEscape())) {return;}
            pnd_Save_Areas_Pnd_First.setValue(false);                                                                                                                     //Natural: ASSIGN #FIRST := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Print_Fields_Pnd_Prtfld2.setValue(pnd_Save_Areas_Pnd_Trans_Desc);                                                                                             //Natural: MOVE #TRANS-DESC TO #PRTFLD2
        pnd_Save_Areas_Pnd_Trans_Desc.reset();                                                                                                                            //Natural: RESET #TRANS-DESC
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                           //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
    }
    private void sub_Source_Brk() throws Exception                                                                                                                        //Natural: SOURCE-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
                                                                                                                                                                          //Natural: PERFORM PAYSET-BRK
        sub_Payset_Brk();
        if (condition(Global.isEscape())) {return;}
        pnd_Save_Areas_Pnd_Occ.setValue(2);                                                                                                                               //Natural: ASSIGN #OCC := 2
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Save_Areas_Pnd_First.setValue(true);                                                                                                                          //Natural: ASSIGN #FIRST := TRUE
        if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2))))                       //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
        {
            ignore();
            //*                     (SAVE CNT/AMT OF OLD SOURCE-CODE FOR PRINTING)
            //*  TOTAL IS PRINTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Save_Areas_Pnd_Tot_Prt.getBoolean()))                                                                                                       //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Occ.setValue(5);                                                                                                                       //Natural: ASSIGN #OCC := 5
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
                //*  TO RESET TOTAL CTR FOR NEW SRCE-CDE
                pnd_Counters.getValue(5).reset();                                                                                                                         //Natural: RESET #COUNTERS ( 5 )
                pnd_Save_Areas_Pnd_Occ.setValue(3);                                                                                                                       //Natural: ASSIGN #OCC := 3
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Company_Brk() throws Exception                                                                                                                       //Natural: COMPANY-BRK
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
                                                                                                                                                                          //Natural: PERFORM SOURCE-BRK
        sub_Source_Brk();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CO-DESC
        sub_Co_Desc();
        if (condition(Global.isEscape())) {return;}
        //*  11/2014 START
        if (condition(pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue("*").equals(pnd_Save_Areas_Pnd_Print_Com)))                                                                 //Natural: IF #TOT-CMPNY ( * ) = #PRINT-COM
        {
            DbsUtil.examine(new ExamineSource(pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue("*")), new ExamineSearch(pnd_Save_Areas_Pnd_Print_Com), new ExamineGivingIndex(pnd_Tot_Ndx)); //Natural: EXAMINE #TOT-CMPNY ( * ) FOR #PRINT-COM GIVING INDEX #TOT-NDX
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
            sub_Accum_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR06:                                                                                                                                                        //Natural: FOR #TOT-NDX 1 #CMPY-CNT
            for (pnd_Tot_Ndx.setValue(1); condition(pnd_Tot_Ndx.lessOrEqual(pnd_Cmpy_Cnt)); pnd_Tot_Ndx.nadd(1))
            {
                if (condition(pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).equals(" ")))                                                                          //Natural: IF #TOT-CMPNY ( #TOT-NDX ) = ' '
                {
                    pnd_Tot_Counters_Pnd_Tot_Cmpny.getValue(pnd_Tot_Ndx).setValue(pnd_Save_Areas_Pnd_Print_Com);                                                          //Natural: ASSIGN #TOT-CMPNY ( #TOT-NDX ) := #PRINT-COM
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTAL
                    sub_Accum_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  11/2014 END
        //*  TO PRINT TOTAL OF LAST BATCH SC
        if (condition(pnd_Save_Areas_Pnd_Eof.getBoolean()))                                                                                                               //Natural: IF #EOF
        {
            //*  TOTAL IS PRINTED
            if (condition(pnd_Save_Areas_Pnd_Tot_Prt.getBoolean()))                                                                                                       //Natural: IF #TOT-PRT
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-VALUE-5
                sub_Move_Value_5();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Occ.setValue(5);                                                                                                                       //Natural: ASSIGN #OCC := 5
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
                sub_Print_Fields();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Areas_Pnd_Occ.setValue(4);                                                                                                                               //Natural: ASSIGN #OCC := 4
                                                                                                                                                                          //Natural: PERFORM PRINT-FIELDS
        sub_Print_Fields();
        if (condition(Global.isEscape())) {return;}
        pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                        //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
    }
    private void sub_Accum_Total() throws Exception                                                                                                                       //Natural: ACCUM-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tot_Counters_Pnd_Tot_Trans_Cnt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Trans_Cnt.getValue(4));                                                            //Natural: ADD #TRANS-CNT ( 4 ) TO #TOT-TRANS-CNT ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Gross_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Gross_Amt.getValue(4));                                                            //Natural: ADD #GROSS-AMT ( 4 ) TO #TOT-GROSS-AMT ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Ivc_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Ivc_Amt.getValue(4));                                                                //Natural: ADD #IVC-AMT ( 4 ) TO #TOT-IVC-AMT ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Taxable_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Taxable_Amt.getValue(4));                                                        //Natural: ADD #TAXABLE-AMT ( 4 ) TO #TOT-TAXABLE-AMT ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Int_Amt.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Int_Amt.getValue(4));                                                                //Natural: ADD #INT-AMT ( 4 ) TO #TOT-INT-AMT ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Fed_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Fed_Wthld.getValue(4));                                                            //Natural: ADD #FED-WTHLD ( 4 ) TO #TOT-FED-WTHLD ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Nra_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Nra_Wthld.getValue(4));                                                            //Natural: ADD #NRA-WTHLD ( 4 ) TO #TOT-NRA-WTHLD ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_State_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_State_Wthld.getValue(4));                                                        //Natural: ADD #STATE-WTHLD ( 4 ) TO #TOT-STATE-WTHLD ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Local_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Local_Wthld.getValue(4));                                                        //Natural: ADD #LOCAL-WTHLD ( 4 ) TO #TOT-LOCAL-WTHLD ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Pr_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Pr_Wthld.getValue(4));                                                              //Natural: ADD #PR-WTHLD ( 4 ) TO #TOT-PR-WTHLD ( #TOT-NDX )
        pnd_Tot_Counters_Pnd_Tot_Can_Wthld.getValue(pnd_Tot_Ndx).nadd(pnd_Counters_Pnd_Can_Wthld.getValue(4));                                                            //Natural: ADD #CAN-WTHLD ( 4 ) TO #TOT-CAN-WTHLD ( #TOT-NDX )
    }
    private void sub_Move_Value_5() throws Exception                                                                                                                      //Natural: MOVE-VALUE-5
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Trans_Cnt.getValue(5).setValue(pnd_Counters_Pnd_Trans_Cnt.getValue(3));                                                                          //Natural: ASSIGN #TRANS-CNT ( 5 ) := #TRANS-CNT ( 3 )
        pnd_Counters_Pnd_Gross_Amt.getValue(5).setValue(pnd_Counters_Pnd_Gross_Amt.getValue(3));                                                                          //Natural: ASSIGN #GROSS-AMT ( 5 ) := #GROSS-AMT ( 3 )
        pnd_Counters_Pnd_Ivc_Amt.getValue(5).setValue(pnd_Counters_Pnd_Ivc_Amt.getValue(3));                                                                              //Natural: ASSIGN #IVC-AMT ( 5 ) := #IVC-AMT ( 3 )
        pnd_Counters_Pnd_Taxable_Amt.getValue(5).setValue(pnd_Counters_Pnd_Taxable_Amt.getValue(3));                                                                      //Natural: ASSIGN #TAXABLE-AMT ( 5 ) := #TAXABLE-AMT ( 3 )
        pnd_Counters_Pnd_Int_Amt.getValue(5).setValue(pnd_Counters_Pnd_Int_Amt.getValue(3));                                                                              //Natural: ASSIGN #INT-AMT ( 5 ) := #INT-AMT ( 3 )
        pnd_Counters_Pnd_Fed_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Fed_Wthld.getValue(3));                                                                          //Natural: ASSIGN #FED-WTHLD ( 5 ) := #FED-WTHLD ( 3 )
        pnd_Counters_Pnd_Nra_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Nra_Wthld.getValue(3));                                                                          //Natural: ASSIGN #NRA-WTHLD ( 5 ) := #NRA-WTHLD ( 3 )
        pnd_Counters_Pnd_State_Wthld.getValue(5).setValue(pnd_Counters_Pnd_State_Wthld.getValue(3));                                                                      //Natural: ASSIGN #STATE-WTHLD ( 5 ) := #STATE-WTHLD ( 3 )
        pnd_Counters_Pnd_Local_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Local_Wthld.getValue(3));                                                                      //Natural: ASSIGN #LOCAL-WTHLD ( 5 ) := #LOCAL-WTHLD ( 3 )
        pnd_Counters_Pnd_Pr_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Pr_Wthld.getValue(3));                                                                            //Natural: ASSIGN #PR-WTHLD ( 5 ) := #PR-WTHLD ( 3 )
        pnd_Counters_Pnd_Can_Wthld.getValue(5).setValue(pnd_Counters_Pnd_Can_Wthld.getValue(3));                                                                          //Natural: ASSIGN #CAN-WTHLD ( 5 ) := #CAN-WTHLD ( 3 )
    }
    private void sub_Co_Desc() throws Exception                                                                                                                           //Natural: CO-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -------
        //*  5/23/2002
        //*  DY1
        //*  9/17/2004
        //*  11/2014 END
        short decideConditionsMet1157 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #SV-COMPANY;//Natural: VALUE 'C'
        if (condition((pnd_Savers_Pnd_Sv_Company.equals("C"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("CREF");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'CREF'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("L"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("LIFE");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'LIFE'
        }                                                                                                                                                                 //Natural: VALUE 'T'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("T"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("TIAA");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'TIAA'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("S"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("TCII");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'TCII'
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("F"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("FSB ");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'FSB '
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Savers_Pnd_Sv_Company.equals("X"))))
        {
            decideConditionsMet1157++;
            pnd_Save_Areas_Pnd_Print_Com.setValue("TRST");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'TRST'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Save_Areas_Pnd_Print_Com.setValue("OTHR");                                                                                                                //Natural: ASSIGN #PRINT-COM := 'OTHR'
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1173 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #SV-TAX-CTZ;//Natural: VALUE 'F'
        if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F"))))
        {
            decideConditionsMet1173++;
            pnd_Save_Areas_Pnd_Print_Tc.setValue("F - FOREIGN");                                                                                                          //Natural: ASSIGN #PRINT-TC := 'F - FOREIGN'
        }                                                                                                                                                                 //Natural: VALUE 'U'
        else if (condition((pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U"))))
        {
            decideConditionsMet1173++;
            pnd_Save_Areas_Pnd_Print_Tc.setValue("U - US CTZN/RES.ALIEN");                                                                                                //Natural: ASSIGN #PRINT-TC := 'U - US CTZN/RES.ALIEN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Run_Desc() throws Exception                                                                                                                          //Natural: RUN-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------
        short decideConditionsMet1184 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #FREQ;//Natural: VALUE 'D'
        if (condition((pnd_Var_File1_Pnd_Freq.equals("D"))))
        {
            decideConditionsMet1184++;
            pnd_Save_Areas_Pnd_Print_Run.setValue("(Daily run)");                                                                                                         //Natural: ASSIGN #PRINT-RUN := '(Daily run)'
        }                                                                                                                                                                 //Natural: VALUE 'Y'
        else if (condition((pnd_Var_File1_Pnd_Freq.equals("Y"))))
        {
            decideConditionsMet1184++;
            pnd_Save_Areas_Pnd_Print_Run.setValue("(YTD run)");                                                                                                           //Natural: ASSIGN #PRINT-RUN := '(YTD run)'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Reset_Ctrs() throws Exception                                                                                                                        //Natural: RESET-CTRS
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #J 1 #OCC
        for (pnd_Save_Areas_Pnd_J.setValue(1); condition(pnd_Save_Areas_Pnd_J.lessOrEqual(pnd_Save_Areas_Pnd_Occ)); pnd_Save_Areas_Pnd_J.nadd(1))
        {
            pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Save_Areas_Pnd_J).reset();                                                                                            //Natural: RESET #TRANS-CNT ( #J ) #GROSS-AMT ( #J ) #IVC-AMT ( #J ) #TAXABLE-AMT ( #J ) #INT-AMT ( #J ) #FED-WTHLD ( #J ) #NRA-WTHLD ( #J ) #STATE-WTHLD ( #J ) #LOCAL-WTHLD ( #J ) #PR-WTHLD ( #J ) #CAN-WTHLD ( #J )
            pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Int_Amt.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_State_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
            pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Save_Areas_Pnd_J).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Source_To_Prt() throws Exception                                                                                                                     //Natural: SOURCE-TO-PRT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Areas_Pnd_Source12.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,2));                                                                              //Natural: ASSIGN #SOURCE12 := SUBSTRING ( #SV-SRCE-CODE,1,2 )
        pnd_Save_Areas_Pnd_Source34.setValue(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(3,2));                                                                              //Natural: ASSIGN #SOURCE34 := SUBSTRING ( #SV-SRCE-CODE,3,2 )
        //*  ADD 'SI'       RM 06/12/03
        //*  ADD 'VL'       RM 10/30/06
        short decideConditionsMet1204 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #SOURCE34;//Natural: VALUE '  '
        if (condition((pnd_Save_Areas_Pnd_Source34.equals("  "))))
        {
            decideConditionsMet1204++;
            //*  09/29/11
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("AM") || pnd_Save_Areas_Pnd_Source12.equals("AP") || pnd_Save_Areas_Pnd_Source12.equals("CP")                //Natural: IF #SOURCE12 = 'AM' OR = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IP' OR = 'IS' OR = 'MS' OR = 'NV' OR = 'NZ' OR = 'OP' OR = 'RE' OR = 'SI' OR = 'SS' OR = 'VL'
                || pnd_Save_Areas_Pnd_Source12.equals("DC") || pnd_Save_Areas_Pnd_Source12.equals("DS") || pnd_Save_Areas_Pnd_Source12.equals("ED") || pnd_Save_Areas_Pnd_Source12.equals("EW") 
                || pnd_Save_Areas_Pnd_Source12.equals("GS") || pnd_Save_Areas_Pnd_Source12.equals("IA") || pnd_Save_Areas_Pnd_Source12.equals("IP") || pnd_Save_Areas_Pnd_Source12.equals("IS") 
                || pnd_Save_Areas_Pnd_Source12.equals("MS") || pnd_Save_Areas_Pnd_Source12.equals("NV") || pnd_Save_Areas_Pnd_Source12.equals("NZ") || pnd_Save_Areas_Pnd_Source12.equals("OP") 
                || pnd_Save_Areas_Pnd_Source12.equals("RE") || pnd_Save_Areas_Pnd_Source12.equals("SI") || pnd_Save_Areas_Pnd_Source12.equals("SS") || pnd_Save_Areas_Pnd_Source12.equals("VL")))
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source12);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE12
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source12);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE12
                //*  ADD 'PL'       RM 09/17/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'OL','PL'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("OL") || pnd_Save_Areas_Pnd_Source34.equals("PL"))))
        {
            decideConditionsMet1204++;
            //*  09/29/11
            //*  ADD 'SI'       RM 06/12/03
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("AM") || pnd_Save_Areas_Pnd_Source12.equals("AP") || pnd_Save_Areas_Pnd_Source12.equals("CP")                //Natural: IF #SOURCE12 = 'AM' OR = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'NV' OR = 'IP' OR = 'IS' OR = 'MS' OR = 'NZ' OR = 'OP' OR = 'RE' OR = 'SI' OR = 'SS' OR = 'VL' OR = 'YY' OR = 'ZZ'
                || pnd_Save_Areas_Pnd_Source12.equals("DC") || pnd_Save_Areas_Pnd_Source12.equals("DS") || pnd_Save_Areas_Pnd_Source12.equals("ED") || pnd_Save_Areas_Pnd_Source12.equals("EW") 
                || pnd_Save_Areas_Pnd_Source12.equals("GS") || pnd_Save_Areas_Pnd_Source12.equals("IA") || pnd_Save_Areas_Pnd_Source12.equals("NV") || pnd_Save_Areas_Pnd_Source12.equals("IP") 
                || pnd_Save_Areas_Pnd_Source12.equals("IS") || pnd_Save_Areas_Pnd_Source12.equals("MS") || pnd_Save_Areas_Pnd_Source12.equals("NZ") || pnd_Save_Areas_Pnd_Source12.equals("OP") 
                || pnd_Save_Areas_Pnd_Source12.equals("RE") || pnd_Save_Areas_Pnd_Source12.equals("SI") || pnd_Save_Areas_Pnd_Source12.equals("SS") || pnd_Save_Areas_Pnd_Source12.equals("VL") 
                || pnd_Save_Areas_Pnd_Source12.equals("YY") || pnd_Save_Areas_Pnd_Source12.equals("ZZ")))
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  ADD 'OP' 'YY'  RM 09/17/04
                //*  ADD 'NV'       RM 06/06/06
                //*  ADD 'VL'       RM 10/30/06
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  3-2-99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("TM"))))
        {
            decideConditionsMet1204++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("AP") || pnd_Save_Areas_Pnd_Source12.equals("IA")))                                                          //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'ML'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("ML"))))
        {
            decideConditionsMet1204++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("ZZ")))                                                                                                      //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
                //*  ADD 'NL'  RM 9/17/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'NL'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("NL"))))
        {
            decideConditionsMet1204++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("YY")))                                                                                                      //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
                //*   11/10/99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("AL"))))
        {
            decideConditionsMet1204++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("AP")))                                                                                                      //Natural: IF #SOURCE12 = 'AP'
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Save_Areas_Pnd_Source34.equals("IL"))))
        {
            decideConditionsMet1204++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("IA")))                                                                                                      //Natural: IF #SOURCE12 = 'IA'
            {
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Error_Source.setValue(true);                                                                                                           //Natural: ASSIGN #ERROR-SOURCE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-SOURCE-PRT
                sub_Error_Source_Prt();
                if (condition(Global.isEscape())) {return;}
                pnd_Save_Areas_Pnd_Source.setValue(pnd_Save_Areas_Pnd_Source34);                                                                                          //Natural: ASSIGN #SOURCE := #SOURCE34
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1274 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'DS'
        if (condition((pnd_Save_Areas_Pnd_Source.equals("DS"))))
        {
            decideConditionsMet1274++;
            pnd_Save_Areas_Pnd_New_Source.setValue("DSS");                                                                                                                //Natural: ASSIGN #NEW-SOURCE := 'DSS'
        }                                                                                                                                                                 //Natural: VALUE 'GS'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("GS"))))
        {
            decideConditionsMet1274++;
            pnd_Save_Areas_Pnd_New_Source.setValue("GSRA");                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'GSRA'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("MS"))))
        {
            decideConditionsMet1274++;
            pnd_Save_Areas_Pnd_New_Source.setValue("MSS");                                                                                                                //Natural: ASSIGN #NEW-SOURCE := 'MSS'
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("SS"))))
        {
            decideConditionsMet1274++;
            pnd_Save_Areas_Pnd_New_Source.setValue("SSSS");                                                                                                               //Natural: ASSIGN #NEW-SOURCE := 'SSSS'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Save_Areas_Pnd_New_Source.setValue(pnd_Save_Areas_Pnd_Source);                                                                                            //Natural: ASSIGN #NEW-SOURCE := #SOURCE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Save_Areas_Pnd_Prt_Source.setValue(pnd_Save_Areas_Pnd_New_Source);                                                                                            //Natural: ASSIGN #PRT-SOURCE := #NEW-SOURCE
        short decideConditionsMet1288 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #SOURCE;//Natural: VALUE 'OL'
        if (condition((pnd_Save_Areas_Pnd_Source.equals("OL"))))
        {
            decideConditionsMet1288++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("ZZ")))                                                                                                      //Natural: IF #SOURCE12 = 'ZZ'
            {
                pnd_Save_Areas_Pnd_Prt_Source.setValue("MLOL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE := 'MLOL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                      //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
                //*  ADD 'PL'  RM 9/17/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'PL'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("PL"))))
        {
            decideConditionsMet1288++;
            if (condition(pnd_Save_Areas_Pnd_Source12.equals("YY")))                                                                                                      //Natural: IF #SOURCE12 = 'YY'
            {
                pnd_Save_Areas_Pnd_Prt_Source.setValue("NLPL");                                                                                                           //Natural: ASSIGN #PRT-SOURCE := 'NLPL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                      //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TM'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("TM"))))
        {
            decideConditionsMet1288++;
            pnd_Save_Areas_Pnd_Prt_Source.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                          //Natural: ASSIGN #PRT-SOURCE := #SV-SRCE-CODE
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("AL"))))
        {
            decideConditionsMet1288++;
            pnd_Save_Areas_Pnd_Prt_Source.setValue("TMAL");                                                                                                               //Natural: ASSIGN #PRT-SOURCE := 'TMAL'
        }                                                                                                                                                                 //Natural: VALUE 'IL'
        else if (condition((pnd_Save_Areas_Pnd_Source.equals("IL"))))
        {
            decideConditionsMet1288++;
            pnd_Save_Areas_Pnd_Prt_Source.setValue("TMIL");                                                                                                               //Natural: ASSIGN #PRT-SOURCE := 'TMIL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Print_Fields_Pnd_Prtfld1.setValue(pnd_Save_Areas_Pnd_New_Source);                                                                                             //Natural: MOVE #NEW-SOURCE TO #PRTFLD1
    }
    private void sub_Print_Fields() throws Exception                                                                                                                      //Natural: PRINT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------
        short decideConditionsMet1316 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #OCC;//Natural: VALUE 1
        if (condition((pnd_Save_Areas_Pnd_Occ.equals(1))))
        {
            decideConditionsMet1316++;
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            pnd_Save_Areas_Pnd_Tot_Prt.setValue(false);                                                                                                                   //Natural: ASSIGN #TOT-PRT := FALSE
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Save_Areas_Pnd_Occ.equals(2))))
        {
            decideConditionsMet1316++;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUBTOTAL   ( ", pnd_Save_Areas_Pnd_Prt_Source, ")"));                  //Natural: COMPRESS 'SUBTOTAL   ( ' #PRT-SOURCE ')' TO #PRTFLD2 LEAVING NO SPACE
            pnd_Save_Areas_Pnd_Sub_Skip.setValue(true);                                                                                                                   //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            //*  ADD 'PL'  RM 9/17/04
            //*  TO PRINT TOTAL AFTER 'OL' SUBTOTAL
            if (condition(pnd_Save_Areas_Pnd_Source.equals("OL") || pnd_Save_Areas_Pnd_Source.equals("PL")))                                                              //Natural: IF #SOURCE = 'OL' OR = 'PL'
            {
                pnd_Save_Areas_Pnd_Prt_Tot_Ol.setValue(1);                                                                                                                //Natural: ASSIGN #PRT-TOT-OL := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Var_File1_Pnd_Freq.equals("D")))                                                                                                        //Natural: IF #FREQ = 'D'
                {
                    if (condition(pnd_Save_Areas_Pnd_Source.equals("AM") || pnd_Save_Areas_Pnd_Source.equals("AP") || pnd_Save_Areas_Pnd_Source.equals("CP")              //Natural: IF #SOURCE = 'AM' OR = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'ED' OR = 'EW' OR = 'GS' OR = 'IA' OR = 'IP' OR = 'IS' OR = 'MS' OR = 'NZ' OR = 'NV' OR = 'OP' OR = 'RE' OR = 'SI' OR = 'SS' OR = 'VL'
                        || pnd_Save_Areas_Pnd_Source.equals("DC") || pnd_Save_Areas_Pnd_Source.equals("DS") || pnd_Save_Areas_Pnd_Source.equals("ED") || 
                        pnd_Save_Areas_Pnd_Source.equals("EW") || pnd_Save_Areas_Pnd_Source.equals("GS") || pnd_Save_Areas_Pnd_Source.equals("IA") || pnd_Save_Areas_Pnd_Source.equals("IP") 
                        || pnd_Save_Areas_Pnd_Source.equals("IS") || pnd_Save_Areas_Pnd_Source.equals("MS") || pnd_Save_Areas_Pnd_Source.equals("NZ") || 
                        pnd_Save_Areas_Pnd_Source.equals("NV") || pnd_Save_Areas_Pnd_Source.equals("OP") || pnd_Save_Areas_Pnd_Source.equals("RE") || pnd_Save_Areas_Pnd_Source.equals("SI") 
                        || pnd_Save_Areas_Pnd_Source.equals("SS") || pnd_Save_Areas_Pnd_Source.equals("VL")))
                    {
                        //*  ADD 'SI'  RM 06/12/03
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTALS
                        sub_Accum_Totals();
                        if (condition(Global.isEscape())) {return;}
                        //*  ADD 'OP'  RM 09/16/04
                        //*  ADD 'NV'  RM 06/06/06
                        //*  ADD 'VL'  RM 10/30/06
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ADD 'AM'  /* 09/29/11
                }                                                                                                                                                         //Natural: END-IF
                pnd_Save_Areas_Pnd_Tot_Prt.setValue(false);                                                                                                               //Natural: ASSIGN #TOT-PRT := FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*  RESET AFTER VALUES ARE PRINTED TO
            //*  REPORTS (1) AND (2)
            pnd_Print_Fields.reset();                                                                                                                                     //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
            pnd_Print0_Fields.reset();
            //*   (3-2-99)
            if (condition(pnd_Save_Areas_Pnd_Prt_Tot_Ol.equals(1)))                                                                                                       //Natural: IF #PRT-TOT-OL = 1
            {
                //*  FOR 'APOL'  & 'APTM'
                if (condition(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code().getSubstring(1,2).equals(pnd_Savers_Pnd_Sv_Srce_Code.getSubstring(1,                    //Natural: IF SUBSTRING ( #FF3-SOURCE-CODE,1,2 ) = SUBSTRING ( #SV-SRCE-CODE,1,2 )
                    2))))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Save_Areas_Pnd_Sub_Src.setValue(pnd_Save_Areas_Pnd_Prt_Source.getSubstring(1,2));                                                                 //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                    pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Save_Areas_Pnd_Sub_Src,                    //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRTFLD2 LEAVING NO SPACE
                        ")"));
                    pnd_Save_Areas_Pnd_Occ.setValue(3);                                                                                                                   //Natural: ASSIGN #OCC := 3
                    pnd_Save_Areas_Pnd_Sub_Skip.setValue(true);                                                                                                           //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
                    sub_Print_Rtn();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
                    sub_Print_Rtn0();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
                    sub_Print_Rtn1();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Save_Areas_Pnd_Prt_Tot_Ol.setValue(0);                                                                                                            //Natural: ASSIGN #PRT-TOT-OL := 0
                    pnd_Save_Areas_Pnd_Tot_Prt.setValue(true);                                                                                                            //Natural: ASSIGN #TOT-PRT := TRUE
                    //*                                           3-2-99
                    //*  SINCE TOTAL FOR AP/IA
                    //*   IS PRINTED ALREADY
                    if (condition(pnd_Save_Areas_Pnd_Source12.equals("AP") || pnd_Save_Areas_Pnd_Source12.equals("IA")))                                                  //Natural: IF #SOURCE12 = 'AP' OR = 'IA'
                    {
                        pnd_Save_Areas_Pnd_Total_For_Ap_Ia.setValue(true);                                                                                                //Natural: ASSIGN #TOTAL-FOR-AP-IA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                            3-2-99 : TO PRINT TOTAL FOR 'AP' OR 'AI'
            //*                                   IF THERE IS NO 'APOL' OR 'IAOL read
            if (condition(pnd_Save_Areas_Pnd_Total_For_Ap_Ia.equals(true)))                                                                                               //Natural: IF #TOTAL-FOR-AP-IA = TRUE
            {
                pnd_Save_Areas_Pnd_Total_For_Ap_Ia.setValue(false);                                                                                                       //Natural: ASSIGN #TOTAL-FOR-AP-IA := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Save_Areas_Pnd_Source.equals("TM")))                                                                                                    //Natural: IF #SOURCE = 'TM'
                {
                    pnd_Save_Areas_Pnd_Sub_Src.setValue(pnd_Save_Areas_Pnd_Prt_Source.getSubstring(1,2));                                                                 //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
                    pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Save_Areas_Pnd_Sub_Src,                    //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRTFLD2 LEAVING NO SPACE
                        ")"));
                    pnd_Save_Areas_Pnd_Occ.setValue(3);                                                                                                                   //Natural: ASSIGN #OCC := 3
                    pnd_Save_Areas_Pnd_Sub_Skip.setValue(true);                                                                                                           //Natural: ASSIGN #SUB-SKIP := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
                    sub_Print_Rtn();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
                    sub_Print_Rtn0();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
                    sub_Print_Rtn1();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CTRS
                    sub_Reset_Ctrs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Save_Areas_Pnd_Prt_Tot_Ol.setValue(0);                                                                                                            //Natural: ASSIGN #PRT-TOT-OL := 0
                    pnd_Save_Areas_Pnd_Tot_Prt.setValue(true);                                                                                                            //Natural: ASSIGN #TOT-PRT := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Save_Areas_Pnd_Occ.equals(4))))
        {
            decideConditionsMet1316++;
            pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "GRANDTOTAL ( ", pnd_Save_Areas_Pnd_Print_Com, "-",                     //Natural: COMPRESS 'GRANDTOTAL ( ' #PRINT-COM '-' #SV-TAX-CTZ ')' TO #PRTFLD2 LEAVING NO SPACE
                pnd_Savers_Pnd_Sv_Tax_Ctz, ")"));
            pnd_Save_Areas_Pnd_Page.setValue(true);                                                                                                                       //Natural: ASSIGN #PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Save_Areas_Pnd_Occ.equals(5))))
        {
            decideConditionsMet1316++;
            //*  11/11/99
            short decideConditionsMet1401 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #PRT-SOURCE;//Natural: VALUE 'TMAL'
            if (condition((pnd_Save_Areas_Pnd_Prt_Source.equals("TMAL"))))
            {
                decideConditionsMet1401++;
                pnd_Save_Areas_Pnd_Sub_Src.setValue("AP");                                                                                                                //Natural: ASSIGN #SUB-SRC := 'AP'
            }                                                                                                                                                             //Natural: VALUE 'TMIL'
            else if (condition((pnd_Save_Areas_Pnd_Prt_Source.equals("TMIL"))))
            {
                decideConditionsMet1401++;
                pnd_Save_Areas_Pnd_Sub_Src.setValue("IA");                                                                                                                //Natural: ASSIGN #SUB-SRC := 'IA'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Save_Areas_Pnd_Sub_Src.setValue(pnd_Save_Areas_Pnd_Prt_Source.getSubstring(1,2));                                                                     //Natural: ASSIGN #SUB-SRC := SUBSTRING ( #PRT-SOURCE,1,2 )
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TOTAL      ( ", pnd_Save_Areas_Pnd_Sub_Src, ")"));                     //Natural: COMPRESS 'TOTAL      ( ' #SUB-SRC ')' TO #PRTFLD2 LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
            sub_Print_Rtn();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN0
            sub_Print_Rtn0();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
            sub_Print_Rtn1();
            if (condition(Global.isEscape())) {return;}
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Save_Areas_Pnd_Tot_Prt.setValue(true);                                                                                                                    //Natural: ASSIGN #TOT-PRT := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  + 1 FOR 'SI'  RM 06/12/03
    private void sub_Accum_Totals() throws Exception                                                                                                                      //Natural: ACCUM-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L6490:                                                                                                                                                    //Natural: FOR #I 1 19
        for (pnd_Save_Areas_Pnd_I.setValue(1); condition(pnd_Save_Areas_Pnd_I.lessOrEqual(19)); pnd_Save_Areas_Pnd_I.nadd(1))
        {
            //*  + 1 FOR 'OP'  RM 09/16/04
            //*  + 1 FOR 'NV'  RM 06/06/06
            //*  + 1 FOR 'VL'  RM 10/30/06
            //*  + 1 FOR 'AM'  /* 09/29/11
            if (condition(pnd_Save_Areas_Pnd_Source.equals(pnd_Srce_Tab.getValue(pnd_Save_Areas_Pnd_I))))                                                                 //Natural: IF #SOURCE = #SRCE-TAB ( #I )
            {
                pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Save_Areas_Pnd_I).setValue(pnd_Save_Areas_Pnd_Source);                                                               //Natural: MOVE #SOURCE TO #TTL-SRC ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Grs.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Save_Areas_Pnd_Occ));                                 //Natural: ADD #GROSS-AMT ( #OCC ) TO #TTL-GRS ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Ivc.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Save_Areas_Pnd_Occ));                                   //Natural: ADD #IVC-AMT ( #OCC ) TO #TTL-IVC ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Int.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Save_Areas_Pnd_Occ));                                   //Natural: ADD #INT-AMT ( #OCC ) TO #TTL-INT ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Fed.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Save_Areas_Pnd_Occ));                                 //Natural: ADD #FED-WTHLD ( #OCC ) TO #TTL-FED ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Ste.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_State_Wthld.getValue(pnd_Save_Areas_Pnd_Occ));                               //Natural: ADD #STATE-WTHLD ( #OCC ) TO #TTL-STE ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Nra.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Save_Areas_Pnd_Occ));                                 //Natural: ADD #NRA-WTHLD ( #OCC ) TO #TTL-NRA ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Lcl.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Save_Areas_Pnd_Occ));                               //Natural: ADD #LOCAL-WTHLD ( #OCC ) TO #TTL-LCL ( #I )
                pnd_Ttl_Var_Pnd_Ttl_Can.getValue(pnd_Save_Areas_Pnd_I).nadd(pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Save_Areas_Pnd_Occ));                                 //Natural: ADD #CAN-WTHLD ( #OCC ) TO #TTL-CAN ( #I )
                pnd_Ctr_Check_Amounts_Pnd_Amt_Check.setValue(true);                                                                                                       //Natural: ASSIGN #AMT-CHECK := TRUE
                if (true) break PND_PND_L6490;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L6490. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Account_Grandtotal() throws Exception                                                                                                                //Natural: ACCOUNT-GRANDTOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ------------------
        short decideConditionsMet1444 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SV-COMPANY = 'C' AND #SV-TAX-CTZ = 'F'
        if (condition(pnd_Savers_Pnd_Sv_Company.equals("C") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(6);                                                                                                                      //Natural: ASSIGN #C := 6
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'C' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("C") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(7);                                                                                                                      //Natural: ASSIGN #C := 7
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'L' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("L") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(9);                                                                                                                      //Natural: ASSIGN #C := 9
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'L' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("L") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(10);                                                                                                                     //Natural: ASSIGN #C := 10
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'T' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("T") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(12);                                                                                                                     //Natural: ASSIGN #C := 12
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'T' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("T") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(13);                                                                                                                     //Natural: ASSIGN #C := 13
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'S' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("S") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(15);                                                                                                                     //Natural: ASSIGN #C := 15
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'S' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("S") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(16);                                                                                                                     //Natural: ASSIGN #C := 16
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'F' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("F") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(18);                                                                                                                     //Natural: ASSIGN #C := 18
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'F' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("F") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(19);                                                                                                                     //Natural: ASSIGN #C := 19
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'F'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("X") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("F")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(21);                                                                                                                     //Natural: ASSIGN #C := 21
        }                                                                                                                                                                 //Natural: WHEN #SV-COMPANY = 'X' AND #SV-TAX-CTZ = 'U'
        else if (condition(pnd_Savers_Pnd_Sv_Company.equals("X") && pnd_Savers_Pnd_Sv_Tax_Ctz.equals("U")))
        {
            decideConditionsMet1444++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(22);                                                                                                                     //Natural: ASSIGN #C := 22
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(24);                                                                                                                     //Natural: ASSIGN #C := 24
            //*   DY1 FIX ENDS   <<
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM MOVE-VAL
        sub_Move_Val();
        if (condition(Global.isEscape())) {return;}
        //*  TOTAL OF CREF
        //*  TOTAL OF LIFE
        //*  TOTAL OF TIAA
        //*  TOTAL OF TCII
        //*  TOTAL OF FSB           /* DY1
        //*  TOTAL OF TRST          /* DY1
        //*  OTHR WILL BE DOUBLED       /* 11/2014
        short decideConditionsMet1481 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #C;//Natural: VALUE 6,7
        if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(6) || pnd_Ctr_Check_Amounts_Pnd_C.equals(7))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(8);                                                                                                                      //Natural: ASSIGN #C := 8
        }                                                                                                                                                                 //Natural: VALUE 9,10
        else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(9) || pnd_Ctr_Check_Amounts_Pnd_C.equals(10))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(11);                                                                                                                     //Natural: ASSIGN #C := 11
        }                                                                                                                                                                 //Natural: VALUE 12,13
        else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(12) || pnd_Ctr_Check_Amounts_Pnd_C.equals(13))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(14);                                                                                                                     //Natural: ASSIGN #C := 14
        }                                                                                                                                                                 //Natural: VALUE 15,16
        else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(15) || pnd_Ctr_Check_Amounts_Pnd_C.equals(16))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(17);                                                                                                                     //Natural: ASSIGN #C := 17
        }                                                                                                                                                                 //Natural: VALUE 18,19
        else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(18) || pnd_Ctr_Check_Amounts_Pnd_C.equals(19))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(20);                                                                                                                     //Natural: ASSIGN #C := 20
        }                                                                                                                                                                 //Natural: VALUE 21,22
        else if (condition((pnd_Ctr_Check_Amounts_Pnd_C.equals(21) || pnd_Ctr_Check_Amounts_Pnd_C.equals(22))))
        {
            decideConditionsMet1481++;
            pnd_Ctr_Check_Amounts_Pnd_C.setValue(23);                                                                                                                     //Natural: ASSIGN #C := 23
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM MOVE-VAL
        sub_Move_Val();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Move_Val() throws Exception                                                                                                                          //Natural: MOVE-VAL
    {
        if (BLNatReinput.isReinput()) return;

        //*                  --------
        pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Trans_Cnt.getValue(4));                                                    //Natural: ADD #TRANS-CNT ( 4 ) TO #TRANS-CNT ( #C )
        pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Gross_Amt.getValue(4));                                                    //Natural: ADD #GROSS-AMT ( 4 ) TO #GROSS-AMT ( #C )
        pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Ivc_Amt.getValue(4));                                                        //Natural: ADD #IVC-AMT ( 4 ) TO #IVC-AMT ( #C )
        pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Taxable_Amt.getValue(4));                                                //Natural: ADD #TAXABLE-AMT ( 4 ) TO #TAXABLE-AMT ( #C )
        pnd_Counters_Pnd_Int_Amt.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Int_Amt.getValue(4));                                                        //Natural: ADD #INT-AMT ( 4 ) TO #INT-AMT ( #C )
        pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Fed_Wthld.getValue(4));                                                    //Natural: ADD #FED-WTHLD ( 4 ) TO #FED-WTHLD ( #C )
        pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Pr_Wthld.getValue(4));                                                      //Natural: ADD #PR-WTHLD ( 4 ) TO #PR-WTHLD ( #C )
        pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Nra_Wthld.getValue(4));                                                    //Natural: ADD #NRA-WTHLD ( 4 ) TO #NRA-WTHLD ( #C )
        pnd_Counters_Pnd_State_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_State_Wthld.getValue(4));                                                //Natural: ADD #STATE-WTHLD ( 4 ) TO #STATE-WTHLD ( #C )
        pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Local_Wthld.getValue(4));                                                //Natural: ADD #LOCAL-WTHLD ( 4 ) TO #LOCAL-WTHLD ( #C )
        pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Ctr_Check_Amounts_Pnd_C).nadd(pnd_Counters_Pnd_Can_Wthld.getValue(4));                                                    //Natural: ADD #CAN-WTHLD ( 4 ) TO #CAN-WTHLD ( #C )
    }
    private void sub_Print_Rtn() throws Exception                                                                                                                         //Natural: PRINT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        pnd_Print_Fields_Pnd_Prtfld3.setValueEdited(pnd_Counters_Pnd_Trans_Cnt.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9"));                         //Natural: MOVE EDITED #TRANS-CNT ( #OCC ) ( EM = ZZZZZZZZ9 ) TO #PRTFLD3
        pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Counters_Pnd_Gross_Amt.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZZZZ9.99-"));                  //Natural: MOVE EDITED #GROSS-AMT ( #OCC ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
        pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Counters_Pnd_Ivc_Amt.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                       //Natural: MOVE EDITED #IVC-AMT ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
        pnd_Print_Fields_Pnd_Prtfld6.setValueEdited(pnd_Counters_Pnd_Taxable_Amt.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZZZZ9.99-"));                //Natural: MOVE EDITED #TAXABLE-AMT ( #OCC ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD6
        pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Counters_Pnd_Int_Amt.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZ9.99-"));                        //Natural: MOVE EDITED #INT-AMT ( #OCC ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
        pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Counters_Pnd_Fed_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                     //Natural: MOVE EDITED #FED-WTHLD ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD8
        pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Counters_Pnd_Nra_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                     //Natural: MOVE EDITED #NRA-WTHLD ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
    }
    private void sub_Print_Rtn0() throws Exception                                                                                                                        //Natural: PRINT-RTN0
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
        pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Counters_Pnd_State_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                 //Natural: MOVE EDITED #STATE-WTHLD ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
        pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Counters_Pnd_Local_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZ9.99-"));                  //Natural: MOVE EDITED #LOCAL-WTHLD ( #OCC ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
        pnd_Print0_Fields_Pnd_Prtfld03.setValueEdited(pnd_Counters_Pnd_Pr_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                    //Natural: MOVE EDITED #PR-WTHLD ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD03
        pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Counters_Pnd_Can_Wthld.getValue(pnd_Save_Areas_Pnd_Occ),new ReportEditMask("ZZZZZZZZ9.99-"));                   //Natural: MOVE EDITED #CAN-WTHLD ( #OCC ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
    }
    private void sub_Print_Rtn1() throws Exception                                                                                                                        //Natural: PRINT-RTN1
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ----------
        if (condition(pnd_Save_Areas_Pnd_Error_Source.getBoolean()))                                                                                                      //Natural: IF #ERROR-SOURCE
        {
            getReports().write(3, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4, //Natural: WRITE ( 3 ) #PRINT-FIELDS
                pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,pnd_Print_Fields_Pnd_Prtfld9);
            if (Global.isEscape()) return;
            pnd_Save_Areas_Pnd_Error_Source.setValue(false);                                                                                                              //Natural: ASSIGN #ERROR-SOURCE := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Fields_Pnd_Prtfld1,pnd_Print_Fields_Pnd_Prtfld2,pnd_Print_Fields_Pnd_Prtfld3,pnd_Print_Fields_Pnd_Prtfld4,pnd_Print_Fields_Pnd_Prtfld5,pnd_Print_Fields_Pnd_Prtfld6,pnd_Print_Fields_Pnd_Prtfld7,pnd_Print_Fields_Pnd_Prtfld8,pnd_Print_Fields_Pnd_Prtfld9,NEWLINE,new  //Natural: WRITE ( 1 ) #PRINT-FIELDS / 73T #PRINT0-FIELDS
                TabSetting(73),pnd_Print0_Fields_Pnd_Prtfld01,pnd_Print0_Fields_Pnd_Prtfld02,pnd_Print0_Fields_Pnd_Prtfld03,pnd_Print0_Fields_Pnd_Prtfld04);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALUES OF SUB-TOTAL NOT BE RESET , STILL
        if (condition(pnd_Save_Areas_Pnd_Occ.equals(2)))                                                                                                                  //Natural: IF #OCC = 2
        {
            ignore();
            //*  TO BE PRINTED IN MATCH REPORT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Print_Fields.reset();                                                                                                                                     //Natural: RESET #PRINT-FIELDS #PRINT0-FIELDS
            pnd_Print0_Fields.reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Areas_Pnd_Sub_Skip.getBoolean()))                                                                                                          //Natural: IF #SUB-SKIP
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            pnd_Save_Areas_Pnd_Sub_Skip.setValue(false);                                                                                                                  //Natural: ASSIGN #SUB-SKIP := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GRANDTOTAL IS PRINTED
        if (condition(pnd_Save_Areas_Pnd_Page.getBoolean()))                                                                                                              //Natural: IF #PAGE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Save_Areas_Pnd_Page.setValue(false);                                                                                                                      //Natural: ASSIGN #PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*   DY1
    private void sub_Check_Amounts() throws Exception                                                                                                                     //Natural: CHECK-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        F1:                                                                                                                                                               //Natural: FOR #E 1 6
        for (pnd_Ctr_Check_Amounts_Pnd_E.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_E.lessOrEqual(6)); pnd_Ctr_Check_Amounts_Pnd_E.nadd(1))
        {
            //*  + 1 FOR 'SI'  RM 06/12/03
            if (condition(pnd_Savers_Pnd_Sv_Company.equals(pnd_Ctr_Check_Amounts_Pnd_Co.getValue(pnd_Ctr_Check_Amounts_Pnd_E))))                                          //Natural: IF #SV-COMPANY = #CO ( #E )
            {
                F2:                                                                                                                                                       //Natural: FOR #D 1 19
                for (pnd_Ctr_Check_Amounts_Pnd_D.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_D.lessOrEqual(19)); pnd_Ctr_Check_Amounts_Pnd_D.nadd(1))
                {
                    //*  + 1 FOR 'OP'  RM 09/17/04
                    //*  + 1 FOR 'NV'  RM 06/06/06
                    //*  + 1 FOR 'VL'  RM 10/30/06
                    //*  + 1 FOR 'AM'  /* 09/29/11
                    if (condition(pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(" ")))                                                             //Natural: IF #TTL-SRC ( #D ) = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Sc.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-SRC ( #D ) = #SC ( #E,#D )
                            pnd_Ctr_Check_Amounts_Pnd_D))))
                        {
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Grs.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Gross.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-GRS ( #D ) = #GROSS ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(1).setValue(1);                                                                                  //Natural: ASSIGN #ERROR ( 1 ) := 1
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Ivc.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Ivc.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-IVC ( #D ) = #IVC ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(2).setValue(2);                                                                                  //Natural: ASSIGN #ERROR ( 2 ) := 2
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Int.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Int.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-INT ( #D ) = #INT ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(3).setValue(3);                                                                                  //Natural: ASSIGN #ERROR ( 3 ) := 3
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Fed.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Fed.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-FED ( #D ) = #FED ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(4).setValue(4);                                                                                  //Natural: ASSIGN #ERROR ( 4 ) := 4
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Ste.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_State.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-STE ( #D ) = #STATE ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(5).setValue(5);                                                                                  //Natural: ASSIGN #ERROR ( 5 ) := 5
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Nra.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Nra.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-NRA ( #D ) = #NRA ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(6).setValue(6);                                                                                  //Natural: ASSIGN #ERROR ( 6 ) := 6
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Lcl.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Local.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-LCL ( #D ) = #LOCAL ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(7).setValue(7);                                                                                  //Natural: ASSIGN #ERROR ( 7 ) := 7
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Ttl_Var_Pnd_Ttl_Can.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(pnd_Ctr_Check_Amounts_Pnd_Can.getValue(pnd_Ctr_Check_Amounts_Pnd_E, //Natural: IF #TTL-CAN ( #D ) = #CAN ( #E,#D )
                                pnd_Ctr_Check_Amounts_Pnd_D))))
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctr_Check_Amounts_Pnd_Error.getValue(8).setValue(8);                                                                                  //Natural: ASSIGN #ERROR ( 8 ) := 8
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Co.setValue(pnd_Ctr_Check_Amounts_Pnd_Co.getValue(pnd_Ctr_Check_Amounts_Pnd_E));                                    //Natural: ASSIGN #PRT-CO := #CO ( #E )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Sc.setValue(pnd_Ctr_Check_Amounts_Pnd_Sc.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));        //Natural: ASSIGN #PRT-SC := #SC ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Gross.setValue(pnd_Ctr_Check_Amounts_Pnd_Gross.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));  //Natural: ASSIGN #PRT-GROSS := #GROSS ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Ivc.setValue(pnd_Ctr_Check_Amounts_Pnd_Ivc.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));      //Natural: ASSIGN #PRT-IVC := #IVC ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Int.setValue(pnd_Ctr_Check_Amounts_Pnd_Int.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));      //Natural: ASSIGN #PRT-INT := #INT ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Fed.setValue(pnd_Ctr_Check_Amounts_Pnd_Fed.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));      //Natural: ASSIGN #PRT-FED := #FED ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_State.setValue(pnd_Ctr_Check_Amounts_Pnd_State.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));  //Natural: ASSIGN #PRT-STATE := #STATE ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Nra.setValue(pnd_Ctr_Check_Amounts_Pnd_Nra.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));      //Natural: ASSIGN #PRT-NRA := #NRA ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Local.setValue(pnd_Ctr_Check_Amounts_Pnd_Local.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));  //Natural: ASSIGN #PRT-LOCAL := #LOCAL ( #E,#D )
                            pnd_Prt_Ff1_Cntrl_Pnd_Prt_Can.setValue(pnd_Ctr_Check_Amounts_Pnd_Can.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_D));      //Natural: ASSIGN #PRT-CAN := #CAN ( #E,#D )
                            PND_PND_L7310:                                                                                                                                //Natural: FOR #S 1 8
                            for (pnd_Ctr_Check_Amounts_Pnd_S.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_S.lessOrEqual(8)); pnd_Ctr_Check_Amounts_Pnd_S.nadd(1))
                            {
                                if (condition(pnd_Ctr_Check_Amounts_Pnd_Error.getValue(pnd_Ctr_Check_Amounts_Pnd_S).equals(getZero())))                                   //Natural: IF #ERROR ( #S ) = 0
                                {
                                    pnd_Ctr_Check_Amounts_Pnd_Err_Message.setValue("BALANCE AMOUNTS");                                                                    //Natural: ASSIGN #ERR-MESSAGE := 'BALANCE AMOUNTS'
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Ctr_Check_Amounts_Pnd_Err_Message.setValue("UNBALANCE AMOUNT(S)");                                                                //Natural: ASSIGN #ERR-MESSAGE := 'UNBALANCE AMOUNT(S)'
                                    if (true) break PND_PND_L7310;                                                                                                        //Natural: ESCAPE BOTTOM ( ##L7310. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F2"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Ctr_Check_Amounts_Pnd_Srce_Found.setValue(true);                                                                                          //Natural: ASSIGN #SRCE-FOUND := TRUE
                            pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1.setValue(pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Ctr_Check_Amounts_Pnd_D));                                   //Natural: ASSIGN #PRT-FLD1 := #TTL-SRC ( #D )
                            pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Grs.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-GRS ( #D ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
                            pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Ivc.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-IVC ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
                            pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Int.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-INT ( #D ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
                            pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Fed.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-FED ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD8
                            pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Nra.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-NRA ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
                            pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Ste.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-STE ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
                            pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Lcl.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-LCL ( #D ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
                            pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Can.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-")); //Natural: MOVE EDITED #TTL-CAN ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
                                                                                                                                                                          //Natural: PERFORM PRINT-MATCH
                            sub_Print_Match();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("F2"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Ctr_Check_Amounts_Pnd_Err_Message.reset();                                                                                                //Natural: RESET #ERR-MESSAGE
                            //*  (SAME #SC)
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (#TTL-SRC=' ')
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (F2.)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  (SAME #CO)
            }                                                                                                                                                             //Natural: END-IF
            //*  (F1.)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Ctr_Check_Amounts_Pnd_Srce_Found.getBoolean()))                                                                                                 //Natural: IF #SRCE-FOUND
        {
            pnd_Ctr_Check_Amounts_Pnd_Srce_Found.setValue(false);                                                                                                         //Natural: ASSIGN #SRCE-FOUND := FALSE
            //*  + 1 FOR 'SI'  RM 06/12/03
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ctr_Check_Amounts_Pnd_Err_Message.setValue("SOURCE CODE NOT ON FILE");                                                                                    //Natural: ASSIGN #ERR-MESSAGE := 'SOURCE CODE NOT ON FILE'
            FOR08:                                                                                                                                                        //Natural: FOR #D 1 19
            for (pnd_Ctr_Check_Amounts_Pnd_D.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_D.lessOrEqual(19)); pnd_Ctr_Check_Amounts_Pnd_D.nadd(1))
            {
                //*  + 1 FOR 'OP'  RM 09/17/04
                //*  + 1 FOR 'NV'  RM 06/06/06
                //*  + 1 FOR 'AM'  /* 09/29/11
                //*  + 1 FOR 'VL'  RM 10/30/06
                if (condition(pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Ctr_Check_Amounts_Pnd_D).equals(" ")))                                                                 //Natural: IF #TTL-SRC ( #D ) = ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1.setValue(pnd_Ttl_Var_Pnd_Ttl_Src.getValue(pnd_Ctr_Check_Amounts_Pnd_D));                                           //Natural: ASSIGN #PRT-FLD1 := #TTL-SRC ( #D )
                    pnd_Print_Fields_Pnd_Prtfld4.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Grs.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZZZZ9.99-"));    //Natural: MOVE EDITED #TTL-GRS ( #D ) ( EM = ZZZZZZZZZZZ9.99- ) TO #PRTFLD4
                    pnd_Print_Fields_Pnd_Prtfld5.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Ivc.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-"));       //Natural: MOVE EDITED #TTL-IVC ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD5
                    pnd_Print_Fields_Pnd_Prtfld7.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Int.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZ9.99-"));        //Natural: MOVE EDITED #TTL-INT ( #D ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD7
                    pnd_Print_Fields_Pnd_Prtfld8.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Fed.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-"));       //Natural: MOVE EDITED #TTL-FED ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD8
                    pnd_Print_Fields_Pnd_Prtfld9.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Nra.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-"));       //Natural: MOVE EDITED #TTL-NRA ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD9
                    pnd_Print0_Fields_Pnd_Prtfld01.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Ste.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-"));     //Natural: MOVE EDITED #TTL-STE ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD01
                    pnd_Print0_Fields_Pnd_Prtfld02.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Lcl.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZ9.99-"));      //Natural: MOVE EDITED #TTL-LCL ( #D ) ( EM = ZZZZZZZ9.99- ) TO #PRTFLD02
                    pnd_Print0_Fields_Pnd_Prtfld04.setValueEdited(pnd_Ttl_Var_Pnd_Ttl_Can.getValue(pnd_Ctr_Check_Amounts_Pnd_D),new ReportEditMask("ZZZZZZZZ9.99-"));     //Natural: MOVE EDITED #TTL-CAN ( #D ) ( EM = ZZZZZZZZ9.99- ) TO #PRTFLD04
                                                                                                                                                                          //Natural: PERFORM PRINT-MATCH
                    sub_Print_Match();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Ctr_Check_Amounts_Pnd_Err_Message.reset();                                                                                                                //Natural: RESET #ERR-MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Match() throws Exception                                                                                                                       //Natural: PRINT-MATCH
    {
        if (BLNatReinput.isReinput()) return;

        //*                  -----------
        //*  FE150301 60T58
        getReports().write(2, ReportOption.NOTITLE,"PAYMENT FILE -->",new ColumnSpacing(1),pnd_Savers_Pnd_Sv_Company,new ColumnSpacing(1),pnd_Ctr_Check_Amounts_Pnd_Prt_Fld1,new  //Natural: WRITE ( 2 ) 'PAYMENT FILE -->' 1X #SV-COMPANY 1X #PRT-FLD1 4X #PRTFLD4 1X #PRTFLD5 3X #PRTFLD7 2X #PRTFLD8 2X #PRTFLD01 / 60T #PRTFLD9 2X #PRTFLD02 4X #PRTFLD04 / 'CONTROL FILE -->' 1X #PRT-CO 1X #PRT-SC 2X #PRT-GROSS 2X #PRT-IVC 1X #PRT-INT 1X #PRT-FED 3X #PRT-STATE / 59T #PRT-NRA 1X #PRT-LOCAL 3X #PRT-CAN 1X #ERROR ( * ) / 105T #ERR-MESSAGE //
            ColumnSpacing(4),pnd_Print_Fields_Pnd_Prtfld4,new ColumnSpacing(1),pnd_Print_Fields_Pnd_Prtfld5,new ColumnSpacing(3),pnd_Print_Fields_Pnd_Prtfld7,new 
            ColumnSpacing(2),pnd_Print_Fields_Pnd_Prtfld8,new ColumnSpacing(2),pnd_Print0_Fields_Pnd_Prtfld01,NEWLINE,new TabSetting(60),pnd_Print_Fields_Pnd_Prtfld9,new 
            ColumnSpacing(2),pnd_Print0_Fields_Pnd_Prtfld02,new ColumnSpacing(4),pnd_Print0_Fields_Pnd_Prtfld04,NEWLINE,"CONTROL FILE -->",new ColumnSpacing(1),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Co,new 
            ColumnSpacing(1),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Sc,new ColumnSpacing(2),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Gross, new ReportEditMask ("ZZZZZZ,ZZZ,ZZ9.99-"),new 
            ColumnSpacing(2),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Ivc, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Int, new 
            ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Fed, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(3),pnd_Prt_Ff1_Cntrl_Pnd_Prt_State, 
            new ReportEditMask ("ZZZZZZ,ZZ9.99-"),NEWLINE,new TabSetting(59),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Nra, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Local, 
            new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(3),pnd_Prt_Ff1_Cntrl_Pnd_Prt_Can, new ReportEditMask ("ZZZZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ctr_Check_Amounts_Pnd_Error.getValue("*"),NEWLINE,new 
            TabSetting(105),pnd_Ctr_Check_Amounts_Pnd_Err_Message,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Ctr_Check_Amounts_Pnd_Error.getValue("*").reset();                                                                                                            //Natural: RESET #ERROR ( * ) #PRT-FF1-CNTRL
        pnd_Prt_Ff1_Cntrl.reset();
    }
    private void sub_Error_Source_Prt() throws Exception                                                                                                                  //Natural: ERROR-SOURCE-PRT
    {
        if (BLNatReinput.isReinput()) return;

        //*                   ----------------
        pnd_Print_Fields_Pnd_Prtfld1.setValue(pnd_Savers_Pnd_Sv_Srce_Code);                                                                                               //Natural: MOVE #SV-SRCE-CODE TO #PRTFLD1
        pnd_Print_Fields_Pnd_Prtfld2.setValue(DbsUtil.compress(pnd_Savers_Pnd_Sv_Company, "-", pnd_Save_Areas_Pnd_Trans_Desc));                                           //Natural: COMPRESS #SV-COMPANY '-' #TRANS-DESC TO #PRTFLD2
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN
        sub_Print_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-RTN1
        sub_Print_Rtn1();
        if (condition(Global.isEscape())) {return;}
    }
    //*  DY1
    private void sub_Check_Flatfile1() throws Exception                                                                                                                   //Natural: CHECK-FLATFILE1
    {
        if (BLNatReinput.isReinput()) return;

        FOR09:                                                                                                                                                            //Natural: FOR #E 1 6
        for (pnd_Ctr_Check_Amounts_Pnd_E.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_E.lessOrEqual(6)); pnd_Ctr_Check_Amounts_Pnd_E.nadd(1))
        {
            //*  + 1 FOR 'SI'  RM 06/12/03
            if (condition(pnd_Ctr_Check_Amounts_Pnd_Co.getValue(pnd_Ctr_Check_Amounts_Pnd_E).equals(" ")))                                                                //Natural: IF #CO ( #E ) = ' '
            {
                PND_PND_L7695:                                                                                                                                            //Natural: FOR #F 1 19
                for (pnd_Ctr_Check_Amounts_Pnd_F.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_F.lessOrEqual(19)); pnd_Ctr_Check_Amounts_Pnd_F.nadd(1))
                {
                    //*  + 1 FOR 'OP'  RM 09/17/04
                    //*  + 1 FOR 'VL'  RM 10/30/06
                    if (condition(pnd_Ctr_Check_Amounts_Pnd_Sc.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_F).equals(" ")))                            //Natural: IF #SC ( #E,#F ) = ' '
                    {
                        pnd_Ctr_Check_Amounts_Pnd_Blank.setValue(true);                                                                                                   //Natural: ASSIGN #BLANK := TRUE
                        //*  + 1 FOR 'AM'  /* 09/29/11
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ctr_Check_Amounts_Pnd_Blank.setValue(false);                                                                                                  //Natural: ASSIGN #BLANK := FALSE
                        if (true) break PND_PND_L7695;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L7695. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ctr_Check_Amounts_Pnd_Blank.getBoolean()))                                                                                              //Natural: IF #BLANK
                {
                    pnd_Ctr_Check_Amounts_Pnd_Chk_Message.setValue("NO CONTENT");                                                                                         //Natural: ASSIGN #CHK-MESSAGE := 'NO CONTENT'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Check_Amounts_Pnd_Chk_Message.setValue("NO COMP.CODE BUT W/ SRCE CODE");                                                                      //Natural: ASSIGN #CHK-MESSAGE := 'NO COMP.CODE BUT W/ SRCE CODE'
                }                                                                                                                                                         //Natural: END-IF
                //*  (WITH COMPANY)
                //*  + 1 FOR 'SI'  RM 06/12/03
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                PND_PND_L7760:                                                                                                                                            //Natural: FOR #F 1 19
                for (pnd_Ctr_Check_Amounts_Pnd_F.setValue(1); condition(pnd_Ctr_Check_Amounts_Pnd_F.lessOrEqual(19)); pnd_Ctr_Check_Amounts_Pnd_F.nadd(1))
                {
                    //*  + 1 FOR 'OP'  RM 09/17/04
                    //*  + 1 FOR 'VL'  RM 10/30/06
                    if (condition(pnd_Ctr_Check_Amounts_Pnd_Sc.getValue(pnd_Ctr_Check_Amounts_Pnd_E,pnd_Ctr_Check_Amounts_Pnd_F).equals(" ")))                            //Natural: IF #SC ( #E,#F ) = ' '
                    {
                        pnd_Ctr_Check_Amounts_Pnd_Blank.setValue(true);                                                                                                   //Natural: ASSIGN #BLANK := TRUE
                        //*  + 1 FOR 'AM'  /* 09/29/11
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ctr_Check_Amounts_Pnd_Blank.setValue(false);                                                                                                  //Natural: ASSIGN #BLANK := FALSE
                        if (true) break PND_PND_L7760;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L7760. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ctr_Check_Amounts_Pnd_Blank.getBoolean()))                                                                                              //Natural: IF #BLANK
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Check_Amounts_Pnd_Chk_Message.setValue("WITH VALID CONTENT(S)");                                                                              //Natural: ASSIGN #CHK-MESSAGE := 'WITH VALID CONTENT(S)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctr_Check_Amounts_Pnd_Prt_Message.getValue(pnd_Ctr_Check_Amounts_Pnd_E).setValue(pnd_Ctr_Check_Amounts_Pnd_Chk_Message);                                  //Natural: ASSIGN #PRT-MESSAGE ( #E ) := #CHK-MESSAGE
            pnd_Ctr_Check_Amounts_Pnd_Chk_Message.reset();                                                                                                                //Natural: RESET #CHK-MESSAGE #BLANK
            pnd_Ctr_Check_Amounts_Pnd_Blank.reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*   DY1
    private void sub_Info_From_F98t5() throws Exception                                                                                                                   //Natural: INFO-FROM-F98T5
    {
        if (BLNatReinput.isReinput()) return;

        FOR10:                                                                                                                                                            //Natural: FOR #X 1 6
        for (pnd_Save_Areas_Pnd_X.setValue(1); condition(pnd_Save_Areas_Pnd_X.lessOrEqual(6)); pnd_Save_Areas_Pnd_X.nadd(1))
        {
            //*   DY1
            //*   DY1
            short decideConditionsMet1750 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #X;//Natural: VALUE 1
            if (condition((pnd_Save_Areas_Pnd_X.equals(1))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("C");                                                                                                                    //Natural: ASSIGN #COMP := 'C'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Save_Areas_Pnd_X.equals(2))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("T");                                                                                                                    //Natural: ASSIGN #COMP := 'T'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Save_Areas_Pnd_X.equals(3))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("L");                                                                                                                    //Natural: ASSIGN #COMP := 'L'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Save_Areas_Pnd_X.equals(4))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("S");                                                                                                                    //Natural: ASSIGN #COMP := 'S'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Save_Areas_Pnd_X.equals(5))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("F");                                                                                                                    //Natural: ASSIGN #COMP := 'F'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Save_Areas_Pnd_X.equals(6))))
            {
                decideConditionsMet1750++;
                pnd_Save_Areas_Pnd_Comp.setValue("X");                                                                                                                    //Natural: ASSIGN #COMP := 'X'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  + 1 FOR 'SI'  RM 06/12/03
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR11:                                                                                                                                                        //Natural: FOR #Y 1 19
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(19)); pnd_Y.nadd(1))
            {
                //*  + 1 FOR 'OP'  RM 09/17/04
                //*  + 1 FOR 'NV'  RM 06/06/06
                //*  + 1 FOR 'VL'  RM 10/30/06
                //*  + 1 FOR 'AM'  /* 09/29/11
                //*  09/29/11
                short decideConditionsMet1773 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #Y;//Natural: VALUE 1
                if (condition((pnd_Y.equals(1))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("AM");                                                                                                               //Natural: ASSIGN #SCDE := 'AM'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Y.equals(2))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("AP");                                                                                                               //Natural: ASSIGN #SCDE := 'AP'
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Y.equals(3))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("CP");                                                                                                               //Natural: ASSIGN #SCDE := 'CP'
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_Y.equals(4))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("DC");                                                                                                               //Natural: ASSIGN #SCDE := 'DC'
                }                                                                                                                                                         //Natural: VALUE 5
                else if (condition((pnd_Y.equals(5))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("DS");                                                                                                               //Natural: ASSIGN #SCDE := 'DS'
                }                                                                                                                                                         //Natural: VALUE 6
                else if (condition((pnd_Y.equals(6))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("ED");                                                                                                               //Natural: ASSIGN #SCDE := 'ED'
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((pnd_Y.equals(7))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("EW");                                                                                                               //Natural: ASSIGN #SCDE := 'EW'
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((pnd_Y.equals(8))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("GS");                                                                                                               //Natural: ASSIGN #SCDE := 'GS'
                }                                                                                                                                                         //Natural: VALUE 9
                else if (condition((pnd_Y.equals(9))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("IA");                                                                                                               //Natural: ASSIGN #SCDE := 'IA'
                }                                                                                                                                                         //Natural: VALUE 10
                else if (condition((pnd_Y.equals(10))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("IP");                                                                                                               //Natural: ASSIGN #SCDE := 'IP'
                }                                                                                                                                                         //Natural: VALUE 11
                else if (condition((pnd_Y.equals(11))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("IS");                                                                                                               //Natural: ASSIGN #SCDE := 'IS'
                }                                                                                                                                                         //Natural: VALUE 12
                else if (condition((pnd_Y.equals(12))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("MS");                                                                                                               //Natural: ASSIGN #SCDE := 'MS'
                }                                                                                                                                                         //Natural: VALUE 13
                else if (condition((pnd_Y.equals(13))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("NV");                                                                                                               //Natural: ASSIGN #SCDE := 'NV'
                }                                                                                                                                                         //Natural: VALUE 14
                else if (condition((pnd_Y.equals(14))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("NZ");                                                                                                               //Natural: ASSIGN #SCDE := 'NZ'
                }                                                                                                                                                         //Natural: VALUE 15
                else if (condition((pnd_Y.equals(15))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("OP");                                                                                                               //Natural: ASSIGN #SCDE := 'OP'
                }                                                                                                                                                         //Natural: VALUE 16
                else if (condition((pnd_Y.equals(16))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("RE");                                                                                                               //Natural: ASSIGN #SCDE := 'RE'
                }                                                                                                                                                         //Natural: VALUE 17
                else if (condition((pnd_Y.equals(17))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("SI");                                                                                                               //Natural: ASSIGN #SCDE := 'SI'
                }                                                                                                                                                         //Natural: VALUE 18
                else if (condition((pnd_Y.equals(18))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("SS");                                                                                                               //Natural: ASSIGN #SCDE := 'SS'
                }                                                                                                                                                         //Natural: VALUE 19
                else if (condition((pnd_Y.equals(19))))
                {
                    decideConditionsMet1773++;
                    pnd_Save_Areas_Pnd_Scde.setValue("VL");                                                                                                               //Natural: ASSIGN #SCDE := 'VL'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Sum_Var.reset();                                                                                                                                      //Natural: RESET #SUM-VAR
                //*                            /* FOR 'IP'  - 11/11/99
                if (condition(pnd_Save_Areas_Pnd_X.equals(2) && pnd_Y.equals(10)))                                                                                        //Natural: IF #X = 2 AND #Y = 10
                {
                    pnd_Key_Date_Pnd_Key_Table_Nbr.setValue(5);                                                                                                           //Natural: ASSIGN #KEY-TABLE-NBR := #KEY-TABLE-NBR-IP := 5
                    pnd_Key_Date_Ip_Pnd_Key_Table_Nbr_Ip.setValue(5);
                    pnd_Key_Date_Pnd_Key_Tax_Year.setValue(pnd_Var_File1_Pnd_Tax_Year);                                                                                   //Natural: ASSIGN #KEY-TAX-YEAR := #KEY-TAX-YEAR-IP := #TAX-YEAR
                    pnd_Key_Date_Ip_Pnd_Key_Tax_Year_Ip.setValue(pnd_Var_File1_Pnd_Tax_Year);
                    pnd_Key_Date_Pnd_Key_Sc.setValue(pnd_Save_Areas_Pnd_Scde);                                                                                            //Natural: ASSIGN #KEY-SC := #KEY-SC-IP := #SCDE
                    pnd_Key_Date_Ip_Pnd_Key_Sc_Ip.setValue(pnd_Save_Areas_Pnd_Scde);
                    pnd_Key_Date_Pnd_Key_Comp.setValue(pnd_Save_Areas_Pnd_Comp);                                                                                          //Natural: ASSIGN #KEY-COMP := #KEY-COMP-IP := #COMP
                    pnd_Key_Date_Ip_Pnd_Key_Comp_Ip.setValue(pnd_Save_Areas_Pnd_Comp);
                    pnd_Key_Date_Pnd_Key_Intf_From_Date.setValue(pnd_Param_Intf_From_Date);                                                                               //Natural: ASSIGN #KEY-INTF-FROM-DATE := #PARAM-INTF-FROM-DATE
                    vw_tcf_Feeder_View.startDatabaseRead                                                                                                                  //Natural: READ TCF-FEEDER-VIEW WITH TIRCNTL-5-Y-SC-CO-TO-FRM-SP STARTING FROM #KEY-DATE-IP
                    (
                    "READ04",
                    new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Key_Date_Ip, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") }
                    );
                    READ04:
                    while (condition(vw_tcf_Feeder_View.readNextRow("READ04")))
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-REC
                        sub_Check_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  OTHER THAN 'IP'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Key_Date_Pnd_Key_Table_Nbr.setValue(5);                                                                                                           //Natural: ASSIGN #KEY-TABLE-NBR := 5
                    pnd_Key_Date_Pnd_Key_Tax_Year.setValue(pnd_Var_File1_Pnd_Tax_Year);                                                                                   //Natural: ASSIGN #KEY-TAX-YEAR := #TAX-YEAR
                    pnd_Key_Date_Pnd_Key_Sc.setValue(pnd_Save_Areas_Pnd_Scde);                                                                                            //Natural: ASSIGN #KEY-SC := #SCDE
                    pnd_Key_Date_Pnd_Key_Comp.setValue(pnd_Save_Areas_Pnd_Comp);                                                                                          //Natural: ASSIGN #KEY-COMP := #COMP
                    //*                    ---> DEFINE #KEY-INTF-TO-DATE BASED ON SOURCE CDE--
                    short decideConditionsMet1833 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #SCDE;//Natural: VALUES 'AP','IA'
                    if (condition((pnd_Save_Areas_Pnd_Scde.equals("AP") || pnd_Save_Areas_Pnd_Scde.equals("IA"))))
                    {
                        decideConditionsMet1833++;
                        if (condition(pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm.less(12)))                                                                     //Natural: IF #PARAM-INTF-FROM-DATE-MM < 12
                        {
                            pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy.setValue(pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy);                                          //Natural: ASSIGN #KEY-INTF-TO-DATE-YY := #PARAM-INTF-FROM-DATE-YY
                            pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm.compute(new ComputeParameters(false, pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm), pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm.add(1)); //Natural: ASSIGN #KEY-INTF-TO-DATE-MM := #PARAM-INTF-FROM-DATE-MM + 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy.compute(new ComputeParameters(false, pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy), pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy.add(1)); //Natural: ASSIGN #KEY-INTF-TO-DATE-YY := #PARAM-INTF-FROM-DATE-YY + 1
                            pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm.setValue(1);                                                                                             //Natural: ASSIGN #KEY-INTF-TO-DATE-MM := 01
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Key_Date_Pnd_Key_Intf_To_Date_Dd.setValue(1);                                                                                                 //Natural: ASSIGN #KEY-INTF-TO-DATE-DD := 01
                    }                                                                                                                                                     //Natural: VALUES 'CP'
                    else if (condition((pnd_Save_Areas_Pnd_Scde.equals("CP"))))
                    {
                        decideConditionsMet1833++;
                        pnd_Key_Date_Pnd_Key_Intf_To_Date_Yy.setValue(pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Yy);                                              //Natural: ASSIGN #KEY-INTF-TO-DATE-YY := #PARAM-INTF-FROM-DATE-YY
                        pnd_Key_Date_Pnd_Key_Intf_To_Date_Mm.setValue(pnd_Param_Intf_From_Date_Pnd_Param_Intf_From_Date_Mm);                                              //Natural: ASSIGN #KEY-INTF-TO-DATE-MM := #PARAM-INTF-FROM-DATE-MM
                        pnd_Key_Date_Pnd_Key_Intf_To_Date_Dd.setValue(1);                                                                                                 //Natural: ASSIGN #KEY-INTF-TO-DATE-DD := 01
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Key_Date_Pnd_Key_Intf_To_Date.setValue(pnd_Param_Intf_To_Date);                                                                               //Natural: ASSIGN #KEY-INTF-TO-DATE := #PARAM-INTF-TO-DATE
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Key_Date_Pnd_Key_Intf_From_Date.setValue(pnd_Param_Intf_From_Date);                                                                               //Natural: ASSIGN #KEY-INTF-FROM-DATE := #PARAM-INTF-FROM-DATE
                    vw_tcf_Feeder_View.startDatabaseRead                                                                                                                  //Natural: READ TCF-FEEDER-VIEW WITH TIRCNTL-5-Y-SC-CO-TO-FRM-SP STARTING FROM #KEY-DATE
                    (
                    "READ05",
                    new Wc[] { new Wc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", ">=", pnd_Key_Date, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_5_Y_SC_CO_TO_FRM_SP", "ASC") }
                    );
                    READ05:
                    while (condition(vw_tcf_Feeder_View.readNextRow("READ05")))
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-REC
                        sub_Check_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Check_Amounts_Pnd_Gross.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Grs);                                                   //Natural: ASSIGN #GROSS ( #X,#Y ) := #SUM-GRS
                pnd_Ctr_Check_Amounts_Pnd_Ivc.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Ivc);                                                     //Natural: ASSIGN #IVC ( #X,#Y ) := #SUM-IVC
                pnd_Ctr_Check_Amounts_Pnd_Int.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Int);                                                     //Natural: ASSIGN #INT ( #X,#Y ) := #SUM-INT
                pnd_Ctr_Check_Amounts_Pnd_Fed.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Fed);                                                     //Natural: ASSIGN #FED ( #X,#Y ) := #SUM-FED
                pnd_Ctr_Check_Amounts_Pnd_State.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Ste);                                                   //Natural: ASSIGN #STATE ( #X,#Y ) := #SUM-STE
                pnd_Ctr_Check_Amounts_Pnd_Nra.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Nra);                                                     //Natural: ASSIGN #NRA ( #X,#Y ) := #SUM-NRA
                pnd_Ctr_Check_Amounts_Pnd_Can.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Can);                                                     //Natural: ASSIGN #CAN ( #X,#Y ) := #SUM-CAN
                pnd_Ctr_Check_Amounts_Pnd_Local.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(pnd_Sum_Var_Pnd_Sum_Loc);                                                   //Natural: ASSIGN #LOCAL ( #X,#Y ) := #SUM-LOC
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ctr_Check_Amounts_Pnd_Co.getValue(pnd_Save_Areas_Pnd_X).setValue(pnd_Save_Areas_Pnd_Company);                                                             //Natural: ASSIGN #CO ( #X ) := #COMPANY
            pnd_Save_Areas_Pnd_Company.reset();                                                                                                                           //Natural: RESET #COMPANY
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Rec() throws Exception                                                                                                                         //Natural: CHECK-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*                  ---------
        if (condition(tcf_Feeder_View_Tircntl_Tbl_Nbr.equals(pnd_Key_Date_Pnd_Key_Table_Nbr) && tcf_Feeder_View_Tircntl_Tax_Year.equals(pnd_Key_Date_Pnd_Key_Tax_Year)    //Natural: IF TCF-FEEDER-VIEW.TIRCNTL-TBL-NBR = #KEY-TABLE-NBR AND TCF-FEEDER-VIEW.TIRCNTL-TAX-YEAR = #KEY-TAX-YEAR AND TCF-FEEDER-VIEW.TIRCNTL-RPT-SOURCE-CODE = #KEY-SC AND TCF-FEEDER-VIEW.TIRCNTL-COMPANY-CDE = #KEY-COMP AND TCF-FEEDER-VIEW.TIRCNTL-FRM-INTRFCE-DTE = #KEY-INTF-FROM-DATE-N
            && tcf_Feeder_View_Tircntl_Rpt_Source_Code.equals(pnd_Key_Date_Pnd_Key_Sc) && tcf_Feeder_View_Tircntl_Company_Cde.equals(pnd_Key_Date_Pnd_Key_Comp) 
            && tcf_Feeder_View_Tircntl_Frm_Intrfce_Dte.equals(pnd_Key_Date_Pnd_Key_Intf_From_Date_N)))
        {
            pnd_Save_Areas_Pnd_Company.setValue(tcf_Feeder_View_Tircntl_Company_Cde);                                                                                     //Natural: ASSIGN #COMPANY := TIRCNTL-COMPANY-CDE
            pnd_Ctr_Check_Amounts_Pnd_Sc.getValue(pnd_Save_Areas_Pnd_X,pnd_Y).setValue(tcf_Feeder_View_Tircntl_Rpt_Source_Code);                                          //Natural: ASSIGN #SC ( #X,#Y ) := TIRCNTL-RPT-SOURCE-CODE
            pnd_Sum_Var_Pnd_Sum_Grs.nadd(tcf_Feeder_View_Tircntl_Input_Gross_Amt);                                                                                        //Natural: ADD TIRCNTL-INPUT-GROSS-AMT TO #SUM-GRS
            pnd_Sum_Var_Pnd_Sum_Ivc.nadd(tcf_Feeder_View_Tircntl_Input_Ivt_Amt);                                                                                          //Natural: ADD TIRCNTL-INPUT-IVT-AMT TO #SUM-IVC
            pnd_Sum_Var_Pnd_Sum_Int.nadd(tcf_Feeder_View_Tircntl_Input_Int_Amt);                                                                                          //Natural: ADD TIRCNTL-INPUT-INT-AMT TO #SUM-INT
            pnd_Sum_Var_Pnd_Sum_Fed.nadd(tcf_Feeder_View_Tircntl_Input_Fed_Wthldg_Amt);                                                                                   //Natural: ADD TIRCNTL-INPUT-FED-WTHLDG-AMT TO #SUM-FED
            pnd_Sum_Var_Pnd_Sum_Ste.nadd(tcf_Feeder_View_Tircntl_Input_State_Wthldg_Amt);                                                                                 //Natural: ADD TIRCNTL-INPUT-STATE-WTHLDG-AMT TO #SUM-STE
            pnd_Sum_Var_Pnd_Sum_Nra.nadd(tcf_Feeder_View_Tircntl_Input_Nra_Amt);                                                                                          //Natural: ADD TIRCNTL-INPUT-NRA-AMT TO #SUM-NRA
            pnd_Sum_Var_Pnd_Sum_Can.nadd(tcf_Feeder_View_Tircntl_Input_Can_Amt);                                                                                          //Natural: ADD TIRCNTL-INPUT-CAN-AMT TO #SUM-CAN
            pnd_Sum_Var_Pnd_Sum_Loc.nadd(tcf_Feeder_View_Tircntl_Input_Local_Wthldg_Amt);                                                                                 //Natural: ADD TIRCNTL-INPUT-LOCAL-WTHLDG-AMT TO #SUM-LOC
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  11/2014 START
                    if (condition(pnd_Fatca.getBoolean()))                                                                                                                //Natural: IF #FATCA
                    {
                        pnd_Hdg_Lit.setValue("  FATCA TAX  ");                                                                                                            //Natural: ASSIGN #HDG-LIT := '  FATCA TAX  '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Nra_Rpt.getBoolean()))                                                                                                          //Natural: IF #NRA-RPT
                        {
                            pnd_Hdg_Lit.setValue("   NRA TAX   ");                                                                                                        //Natural: ASSIGN #HDG-LIT := '   NRA TAX   '
                        }                                                                                                                                                 //Natural: END-IF
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CO-DESC
                    sub_Co_Desc();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RUN-DESC
                    sub_Run_Desc();
                    if (condition(Global.isEscape())) {return;}
                    //*  11/2014
                    if (condition(! (pnd_Nra_Rpt.getBoolean())))                                                                                                          //Natural: IF NOT #NRA-RPT
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' 121T #PRINT-RUN / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// / 'COMPANY    : ' #PRINT-COM / 'TAX-CTZSHP : ' #PRINT-TC /// 'SYSTEM' 5X 'TYPE OF' 8X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 5X 'FEDERAL TAX' 2X #HDG-LIT / 'SRCE' 5X 'TRANSACTION' 6X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 6X 'WITHHOLDING' 4X 'WITHHOLDING' / 74T '-----------' 4X '-----------' 4X '-----------' 4X '-----------' / 75T 'STATE TAX' 6X 'LOCAL TAX' 5X 'PUERTO RICO' 5X 'CANADIAN' / 74T 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' //
                            TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                            TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",new TabSetting(121),pnd_Save_Areas_Pnd_Print_Run,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                            TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(97),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"COMPANY    : ",pnd_Save_Areas_Pnd_Print_Com,NEWLINE,"TAX-CTZSHP : ",pnd_Save_Areas_Pnd_Print_Tc,NEWLINE,NEWLINE,NEWLINE,"SYSTEM",new 
                            ColumnSpacing(5),"TYPE OF",new ColumnSpacing(8),"TRANS",new ColumnSpacing(11),"GROSS",new ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new 
                            ColumnSpacing(7),"INTEREST",new ColumnSpacing(5),"FEDERAL TAX",new ColumnSpacing(2),pnd_Hdg_Lit,NEWLINE,"SRCE",new ColumnSpacing(5),"TRANSACTION",new 
                            ColumnSpacing(6),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new 
                            ColumnSpacing(6),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,new TabSetting(74),"-----------",new ColumnSpacing(4),"-----------",new 
                            ColumnSpacing(4),"-----------",new ColumnSpacing(4),"-----------",NEWLINE,new TabSetting(75),"STATE TAX",new ColumnSpacing(6),"LOCAL TAX",new 
                            ColumnSpacing(5),"PUERTO RICO",new ColumnSpacing(5),"CANADIAN",NEWLINE,new TabSetting(74),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",new 
                            ColumnSpacing(4),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                        //*    12X 'TAXABLE' 7X 'INTEREST'  5X 'FEDERAL TAX'  6X 'NRA TAX'
                        //*  11/2014 START
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' 121T #PRINT-RUN / 'RUNTIME : ' *TIMX 54T 'TAX YEAR ' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// 'SYSTEM' 5X 'TYPE OF' 8X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 5X 'FEDERAL TAX' 2X #HDG-LIT / 'SRCE' 5X 'TRANSACTION' 6X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 6X 'WITHHOLDING' 4X 'WITHHOLDING' / 74T '-----------' 4X '-----------' 4X '-----------' 4X '-----------' / 75T 'STATE TAX' 6X 'LOCAL TAX' 5X 'PUERTO RICO' 5X 'CANADIAN' / 74T 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' 4X 'WITHHOLDING' //
                            TabSetting(121),"PAGE",getReports().getPageNumberDbs(1),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                            TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",new TabSetting(121),pnd_Save_Areas_Pnd_Print_Run,NEWLINE,"RUNTIME : ",Global.getTIMX(),new 
                            TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(97),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"SYSTEM",new 
                            ColumnSpacing(5),"TYPE OF",new ColumnSpacing(8),"TRANS",new ColumnSpacing(11),"GROSS",new ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new 
                            ColumnSpacing(7),"INTEREST",new ColumnSpacing(5),"FEDERAL TAX",new ColumnSpacing(2),pnd_Hdg_Lit,NEWLINE,"SRCE",new ColumnSpacing(5),"TRANSACTION",new 
                            ColumnSpacing(6),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new 
                            ColumnSpacing(6),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,new TabSetting(74),"-----------",new ColumnSpacing(4),"-----------",new 
                            ColumnSpacing(4),"-----------",new ColumnSpacing(4),"-----------",NEWLINE,new TabSetting(75),"STATE TAX",new ColumnSpacing(6),"LOCAL TAX",new 
                            ColumnSpacing(5),"PUERTO RICO",new ColumnSpacing(5),"CANADIAN",NEWLINE,new TabSetting(74),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",new 
                            ColumnSpacing(4),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING",NEWLINE,NEWLINE);
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  11/2014 START
                    if (condition(pnd_Fatca.getBoolean()))                                                                                                                //Natural: IF #FATCA
                    {
                        pnd_Hdg_Lit2.setValue("  FATCA WTHHLD  ");                                                                                                        //Natural: ASSIGN #HDG-LIT2 := '  FATCA WTHHLD  '
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 2 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' / 'RUNTIME : ' *TIMX 51T 'M A T C H   R E P O R T' / 54T 'TAX YEAR ' #TAX-YEAR 97T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// 18T 'CO SRCE' 9X 'GROSS' 12X 'IVC' 9X 'INTEREST' 8X 'FEDERAL' 5X 'STATE WTHLD' 3X '<ERROR CODES / REMARK >' / 21T 'CODE' 9X 'AMOUNT' 9X 'AMOUNT' 9X 'AMOUNT' 9X 'AMOUNT' 9X 'AMOUNT' / 60T '--------------' 1X '--------------' 1X '--------------' / 60T #HDG-LIT2 4X 'LOCAL' 9X 'CANADIAN' / 64T 'AMOUNT' 9X 'AMOUNT' 9X 'AMOUNT' //
                        TabSetting(121),"PAGE",getReports().getPageNumberDbs(2),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(51),"M A T C H   R E P O R T",NEWLINE,new 
                        TabSetting(54),"TAX YEAR ",pnd_Var_File1_Pnd_Tax_Year,new TabSetting(97),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,new 
                        TabSetting(18),"CO SRCE",new ColumnSpacing(9),"GROSS",new ColumnSpacing(12),"IVC",new ColumnSpacing(9),"INTEREST",new ColumnSpacing(8),"FEDERAL",new 
                        ColumnSpacing(5),"STATE WTHLD",new ColumnSpacing(3),"<ERROR CODES / REMARK >",NEWLINE,new TabSetting(21),"CODE",new ColumnSpacing(9),"AMOUNT",new 
                        ColumnSpacing(9),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new ColumnSpacing(9),"AMOUNT",NEWLINE,new 
                        TabSetting(60),"--------------",new ColumnSpacing(1),"--------------",new ColumnSpacing(1),"--------------",NEWLINE,new TabSetting(60),pnd_Hdg_Lit2,new 
                        ColumnSpacing(4),"LOCAL",new ColumnSpacing(9),"CANADIAN",NEWLINE,new TabSetting(64),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new ColumnSpacing(9),
                        "AMOUNT",NEWLINE,NEWLINE);
                    //*    / 62T 'NRA WTHLD' 8X 'LOCAL'   9X 'CANADIAN'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  11/2014 START
                    if (condition(pnd_Fatca.getBoolean()))                                                                                                                //Natural: IF #FATCA
                    {
                        pnd_Hdg_Lit.setValue("  FATCA TAX  ");                                                                                                            //Natural: ASSIGN #HDG-LIT := '  FATCA TAX  '
                        //*  11/2014 END
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,Global.getPROGRAM(),new TabSetting(44),"TAX WITHHOLDING AND REPORTING SYSTEM",new  //Natural: WRITE ( 3 ) NOTITLE NOHDR / *PROGRAM 44T 'TAX WITHHOLDING AND REPORTING SYSTEM' 116T 'PAGE' *PAGE-NUMBER ( 3 ) / 'RUNDATE : ' *DATX ( EM = MM/DD/YYYY ) 35T 'REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY' / 'RUNTIME : ' *TIMX 41T 'EXCEPTION REPORT - ERRONEOUS SOURCE CODES' 92T 'DATE RANGE :' #START-DATE 'THRU' #END-DATE /// 'SYSTEM' 2X 'CO-TYPE OF' 10X 'TRANS' 11X 'GROSS' 9X 'TAX FREE' 12X 'TAXABLE' 7X 'INTEREST' 5X 'FEDERAL TAX' 2X #HDG-LIT / 'SRCE' 5X 'TRANSACTION' 8X 'COUNT' 11X 'AMOUNT' 11X 'IVC' 14X 'AMOUNT' 9X 'AMOUNT' 6X 'WITHHOLDING' 4X 'WITHHOLDING ' //
                        TabSetting(116),"PAGE",getReports().getPageNumberDbs(3),NEWLINE,"RUNDATE : ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new 
                        TabSetting(35),"REPORTABLE TRANSACTIONS BY SYSTEM SOURCE WITHIN COMPANY",NEWLINE,"RUNTIME : ",Global.getTIMX(),new TabSetting(41),"EXCEPTION REPORT - ERRONEOUS SOURCE CODES",new 
                        TabSetting(92),"DATE RANGE :",pnd_Var_File1_Pnd_Start_Date,"THRU",pnd_Var_File1_Pnd_End_Date,NEWLINE,NEWLINE,NEWLINE,"SYSTEM",new 
                        ColumnSpacing(2),"CO-TYPE OF",new ColumnSpacing(10),"TRANS",new ColumnSpacing(11),"GROSS",new ColumnSpacing(9),"TAX FREE",new ColumnSpacing(12),"TAXABLE",new 
                        ColumnSpacing(7),"INTEREST",new ColumnSpacing(5),"FEDERAL TAX",new ColumnSpacing(2),pnd_Hdg_Lit,NEWLINE,"SRCE",new ColumnSpacing(5),"TRANSACTION",new 
                        ColumnSpacing(8),"COUNT",new ColumnSpacing(11),"AMOUNT",new ColumnSpacing(11),"IVC",new ColumnSpacing(14),"AMOUNT",new ColumnSpacing(9),"AMOUNT",new 
                        ColumnSpacing(6),"WITHHOLDING",new ColumnSpacing(4),"WITHHOLDING ",NEWLINE,NEWLINE);
                    //*    'TAXABLE' 7X 'INTEREST' 5X  'FEDERAL TAX' 6X 'NRA TAX' /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");
    }
    private void CheckAtStartofData405() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                     //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                                  //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
            pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                    //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
            pnd_Savers_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                       //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData670() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Savers_Pnd_Sv_Company.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Company_Code());                                                                     //Natural: ASSIGN #SV-COMPANY := #FF3-COMPANY-CODE
            pnd_Savers_Pnd_Sv_Tax_Ctz.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Tax_Citizenship());                                                                  //Natural: ASSIGN #SV-TAX-CTZ := #FF3-TAX-CITIZENSHIP
            pnd_Savers_Pnd_Sv_Srce_Code.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Source_Code());                                                                    //Natural: ASSIGN #SV-SRCE-CODE := #FF3-SOURCE-CODE
            pnd_Savers_Pnd_Sv_Payset.setValue(ldaTwrl0902.getPnd_Flat_File3_Pnd_Ff3_Payset_Type());                                                                       //Natural: ASSIGN #SV-PAYSET := #FF3-PAYSET-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
}
