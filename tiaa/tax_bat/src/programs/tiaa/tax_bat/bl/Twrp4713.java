/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:16 PM
**        * FROM NATURAL PROGRAM : Twrp4713
************************************************************
**        * FILE NAME            : Twrp4713.java
**        * CLASS NAME           : Twrp4713
**        * INSTANCE NAME        : Twrp4713
************************************************************
*********************************************************************
* TWRP4713 :                                                        *
* 5498 RECON REPORT GENERATION FOR TAX FILE 5498 P2 REPORTING       *
*                                                                   *
* HISTORY                                                           *
* 06/17/2019  -  SAIK   - INITIAL VERSION                           *
* 10/13/2020 - RE-STOW COMPONENT FOR 5498           /* SECURE-ACT   *
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
*********************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp4713 extends BLNatBase
{
    // Data Areas
    private LdaTwrl2001 ldaTwrl2001;
    private LdaTwrl9710 ldaTwrl9710;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Key;

    private DbsGroup pnd_Read_Key__R_Field_1;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tax_Year;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Active_Ind;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Tin;
    private DbsField pnd_Read_Key_Pnd_Read_Key_Form_Type;

    private DbsGroup pnd_Db_Read_Fields;
    private DbsField pnd_Db_Read_Fields_Pnd_Cnt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Sep_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Account_Fmv_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db;
    private DbsField pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db;
    private DbsField pnd_Ws_Tax_Year;

    private DbsGroup pnd_Ws_Tax_Year__R_Field_2;
    private DbsField pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A;
    private DbsField pnd_Detl_Temp_Db;
    private DbsField pnd_Total_Unq_Tin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl2001 = new LdaTwrl2001();
        registerRecord(ldaTwrl2001);
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Key = localVariables.newFieldInRecord("pnd_Read_Key", "#READ-KEY", FieldType.STRING, 32);

        pnd_Read_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Key__R_Field_1", "REDEFINE", pnd_Read_Key);
        pnd_Read_Key_Pnd_Read_Key_Tax_Year = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tax_Year", "#READ-KEY-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Read_Key_Pnd_Read_Key_Active_Ind = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Active_Ind", "#READ-KEY-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Read_Key_Pnd_Read_Key_Tin = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Tin", "#READ-KEY-TIN", FieldType.STRING, 10);
        pnd_Read_Key_Pnd_Read_Key_Form_Type = pnd_Read_Key__R_Field_1.newFieldInGroup("pnd_Read_Key_Pnd_Read_Key_Form_Type", "#READ-KEY-FORM-TYPE", FieldType.NUMERIC, 
            2);

        pnd_Db_Read_Fields = localVariables.newGroupInRecord("pnd_Db_Read_Fields", "#DB-READ-FIELDS");
        pnd_Db_Read_Fields_Pnd_Cnt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Cnt_Db", "#CNT-DB", FieldType.NUMERIC, 8);
        pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db", "#TRAD-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Sep_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Sep_Amt_Db", "#SEP-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db", "#TRAD-IRA-ROLLOVER-DB", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db", "#IRA-RECHAR-AMT-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Account_Fmv_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Account_Fmv_Db", "#ACCOUNT-FMV-DB", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db", "#ROTH-IRA-CONTRIB-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db", "#ROTH-IRA-CONVERSION-DB", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db = pnd_Db_Read_Fields.newFieldInGroup("pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db", "#POSTPN-AMT-DB", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Tax_Year = localVariables.newFieldInRecord("pnd_Ws_Tax_Year", "#WS-TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Ws_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Tax_Year__R_Field_2", "REDEFINE", pnd_Ws_Tax_Year);
        pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A = pnd_Ws_Tax_Year__R_Field_2.newFieldInGroup("pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A", "#WS-TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Detl_Temp_Db = localVariables.newFieldInRecord("pnd_Detl_Temp_Db", "#DETL-TEMP-DB", FieldType.NUMERIC, 10);
        pnd_Total_Unq_Tin = localVariables.newFieldInRecord("pnd_Total_Unq_Tin", "#TOTAL-UNQ-TIN", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl2001.initializeValues();
        ldaTwrl9710.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp4713() throws Exception
    {
        super("Twrp4713");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 100 LS = 200
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #RPT-TWRL2001
        while (condition(getWorkFiles().read(1, ldaTwrl2001.getPnd_Rpt_Twrl2001())))
        {
            pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year());                                                               //Natural: MOVE #RPT-TAX-YEAR TO #WS-TAX-YEAR-A
            pnd_Read_Key_Pnd_Read_Key_Tax_Year.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tax_Year());                                                              //Natural: ASSIGN #READ-KEY-TAX-YEAR := #RPT-TAX-YEAR
            pnd_Read_Key_Pnd_Read_Key_Active_Ind.setValue("A");                                                                                                           //Natural: ASSIGN #READ-KEY-ACTIVE-IND := 'A'
            pnd_Read_Key_Pnd_Read_Key_Tin.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Tin());                                                                        //Natural: ASSIGN #READ-KEY-TIN := #RPT-TIN
            pnd_Read_Key_Pnd_Read_Key_Form_Type.setValue(ldaTwrl2001.getPnd_Rpt_Twrl2001_Pnd_Rpt_Form_Type());                                                            //Natural: ASSIGN #READ-KEY-FORM-TYPE := #RPT-FORM-TYPE
            ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                    //Natural: READ FORM BY TIRF-SUPERDE-2 STARTING FROM #READ-KEY
            (
            "RD1",
            new Wc[] { new Wc("TIRF_SUPERDE_2", ">=", pnd_Read_Key, WcType.BY) },
            new Oc[] { new Oc("TIRF_SUPERDE_2", "ASC") }
            );
            RD1:
            while (condition(ldaTwrl9710.getVw_form().readNextRow("RD1")))
            {
                if (condition(ldaTwrl9710.getForm_Tirf_Tax_Year().notEquals(pnd_Read_Key_Pnd_Read_Key_Tax_Year) || ldaTwrl9710.getForm_Tirf_Active_Ind().notEquals(pnd_Read_Key_Pnd_Read_Key_Active_Ind)  //Natural: IF TIRF-TAX-YEAR NE #READ-KEY-TAX-YEAR OR TIRF-ACTIVE-IND NE #READ-KEY-ACTIVE-IND OR TIRF-TIN NE #READ-KEY-TIN OR TIRF-FORM-TYPE NE #READ-KEY-FORM-TYPE
                    || ldaTwrl9710.getForm_Tirf_Tin().notEquals(pnd_Read_Key_Pnd_Read_Key_Tin) || ldaTwrl9710.getForm_Tirf_Form_Type().notEquals(pnd_Read_Key_Pnd_Read_Key_Form_Type)))
                {
                    if (true) break RD1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD1. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DB-READ-PROCESS
                sub_Db_Read_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DB-READ-PROCESS
        //* ***********************************************************************
        //*                           REPORT GENERATION                           *
        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"TAX WITHHOLDING & REPORTING SYSTEM",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'TAX WITHHOLDING & REPORTING SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(47),"TAX FILE RECONCILIATION (5498) REPORT",new           //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 47T 'TAX FILE RECONCILIATION (5498) REPORT' 120T 'REPORT: RPT1' /
            TabSetting(120),"REPORT: RPT1",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(49)," TAX YEAR:",pnd_Ws_Tax_Year_Pnd_Ws_Tax_Year_A);                                                    //Natural: WRITE ( 01 ) NOTITLE 49T' TAX YEAR:' #WS-TAX-YEAR-A
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(18),"REC COUNT",new TabSetting(29),"IRA CONTRIBUTION",new TabSetting(53),"  SEP AMOUNT",new             //Natural: WRITE ( 01 ) NOTITLE 18T 'REC COUNT' 29T 'IRA CONTRIBUTION' 53T '  SEP AMOUNT' 70T '      IRA ROLLOVER ' 91T 'RECHARACTERIZATION' 112T '        F M V' / 32T '  TRAD/ROTH   ' 53T 'LATE R/O AMT ' 70T '    IRA CONVERSION  ' / '-' ( 132 )
            TabSetting(70),"      IRA ROLLOVER ",new TabSetting(91),"RECHARACTERIZATION",new TabSetting(112),"        F M V",NEWLINE,new TabSetting(32),"  TRAD/ROTH   ",new 
            TabSetting(53),"LATE R/O AMT ",new TabSetting(70),"    IRA CONVERSION  ",NEWLINE,"-",new RepeatItem(132));
        if (Global.isEscape()) return;
        //*  / 11T  'UNIQUE TIN COUNT'
        getReports().write(1, ReportOption.NOTITLE,"DATABASE READ ",new TabSetting(19),pnd_Db_Read_Fields_Pnd_Cnt_Db, new ReportEditMask ("Z,ZZZ,ZZ9"),new                //Natural: WRITE ( 01 ) 'DATABASE READ ' 19T #CNT-DB ( EM = Z,ZZZ,ZZ9 ) 29T #TRAD-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #SEP-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 70T #TRAD-IRA-ROLLOVER-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 91T #IRA-RECHAR-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 112T #ACCOUNT-FMV-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / 29T #ROTH-IRA-CONTRIB-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 50T #POSTPN-AMT-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) 73T #ROTH-IRA-CONVERSION-DB ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 ) / / '-' ( 132 ) /
            TabSetting(29),pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Db_Read_Fields_Pnd_Sep_Amt_Db, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(91),pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(112),pnd_Db_Read_Fields_Pnd_Account_Fmv_Db, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(29),pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(50),pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db, new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(73),pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"-",new RepeatItem(132),NEWLINE);
        if (Global.isEscape()) return;
        //*  19T #TOTAL-UNQ-TIN       (EM=Z,ZZZ,ZZ9)
    }
    private void sub_Db_Read_Process() throws Exception                                                                                                                   //Natural: DB-READ-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(ldaTwrl9710.getForm_Tirf_Form_Type().equals(4)))                                                                                                    //Natural: IF TIRF-FORM-TYPE = 04
        {
            pnd_Db_Read_Fields_Pnd_Cnt_Db.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CNT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Contrib_Db.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib());                                                                 //Natural: ADD TIRF-TRAD-IRA-CONTRIB TO #TRAD-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Sep_Amt_Db.nadd(ldaTwrl9710.getForm_Tirf_Sep_Amt());                                                                                   //Natural: ADD TIRF-SEP-AMT TO #SEP-AMT-DB
            pnd_Db_Read_Fields_Pnd_Trad_Ira_Rollover_Db.nadd(ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover());                                                               //Natural: ADD TIRF-TRAD-IRA-ROLLOVER TO #TRAD-IRA-ROLLOVER-DB
            pnd_Db_Read_Fields_Pnd_Ira_Rechar_Amt_Db.nadd(ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt());                                                                     //Natural: ADD TIRF-IRA-RECHAR-AMT TO #IRA-RECHAR-AMT-DB
            pnd_Db_Read_Fields_Pnd_Account_Fmv_Db.nadd(ldaTwrl9710.getForm_Tirf_Account_Fmv());                                                                           //Natural: ADD TIRF-ACCOUNT-FMV TO #ACCOUNT-FMV-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Contrib_Db.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                                                 //Natural: ADD TIRF-ROTH-IRA-CONTRIB TO #ROTH-IRA-CONTRIB-DB
            pnd_Db_Read_Fields_Pnd_Roth_Ira_Conversion_Db.nadd(ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion());                                                           //Natural: ADD TIRF-ROTH-IRA-CONVERSION TO #ROTH-IRA-CONVERSION-DB
            pnd_Db_Read_Fields_Pnd_Postpn_Amt_Db.nadd(ldaTwrl9710.getForm_Tirf_Postpn_Amt());                                                                             //Natural: ADD TIRF-POSTPN-AMT TO #POSTPN-AMT-DB
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=100 LS=200");
    }
}
