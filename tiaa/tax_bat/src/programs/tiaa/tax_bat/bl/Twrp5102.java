/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:40:37 PM
**        * FROM NATURAL PROGRAM : Twrp5102
************************************************************
**        * FILE NAME            : Twrp5102.java
**        * CLASS NAME           : Twrp5102
**        * INSTANCE NAME        : Twrp5102
************************************************************
************************************************************************
** PROGRAM : TWRP5102
** SYSTEM  : TAXWARS
** AUTHOR  : MICHAEL SUPONITSKY
** FUNCTION: LINK TIN PROCESS.
** HISTORY.....:
**
** 11/07/01 J.ROTHOLZ SPLIT ORIGINAL PROGRAM TWRP5100 INTO TWO FETCHED
**                    MODULES: TWRP5101 TO BE RUN FOR YEARS PRIOR TO
**                    2001, AND TWRP5102 TO BE RUN FOR 2001 AND
**                    BEYOND. TWRP5100 WAS STRIPPED DOWN TO FUNCTION
**                    AS A DRIVER PASSING PARMS VIA FETCH.
** 08/13/02 J.ROTHOLZ RECOMPILED DUE TO INCREASE IN #COMP-NAME LENGTH
**                    IN TWRLCOMP & TWRACOMP
** 09/23/02 R. MA     CHANGE TO ADD COMPANY CODE TO TIRF-SUPERDE-9, IN
**                    ORDER TO INCORPERATE THE NEWLY ADDED COMPANY TO
**                    LINK TIN PROCESS. ALL RELATED LOGIC IN THE PROGRAM
**                    WAS UPDATED.
**                    TWRL9710, TWRL9715 AND TWRP5101 ARE ALSO UPDATED.
** 06/23/05 MS.       IRS ACCOUNT NUMBER. IRS SEQUENCE NUMBER ON NON
**                    EMPTY RECORD IS POPULATED WITH THE NUMBER FROM
**                    EMPTY RECORD.
** 12/06/05 - BK. TAX YEAR ADDED TO TWRNCOMP PARMS
** 01/30/08 - AY  REPLACED TWRL5000 WITH TWRAFRMN / TWRNFRMN.
** 12/04/2020 - RE-STOW COMPONENT FOR 5498 IRS REPORTING  2020
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp5102 extends BLNatBase
{
    // Data Areas
    private PdaTwracomp pdaTwracomp;
    private PdaTwratbl4 pdaTwratbl4;
    private PdaTwradcat pdaTwradcat;
    private PdaTwrairaa pdaTwrairaa;
    private PdaTwratin pdaTwratin;
    private PdaTwrafrmn pdaTwrafrmn;
    private PdaTwra5017 pdaTwra5017;
    private LdaTwrl9705 ldaTwrl9705;
    private LdaTwrl9710 ldaTwrl9710;
    private LdaTwrl9715 ldaTwrl9715;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_hist_Form;
    private DbsField hist_Form_Tirf_Superde_9;

    private DbsGroup pnd_Ws_Const;
    private DbsField pnd_Ws_Const_Low_Values;
    private DbsField pnd_Ws_Const_High_Values;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Parms;
    private DbsField pnd_Ws_Pnd_Form;
    private DbsField pnd_Ws_Pnd_Tax_Year;
    private DbsField pnd_Ws_Pnd_Cases_In_Error;
    private DbsField pnd_Ws_Pnd_Cases_Linked;
    private DbsField pnd_Ws_Pnd_Ok_To_Link;
    private DbsField pnd_Ws_Pnd_Reported_To_Irs;
    private DbsField pnd_Ws_Pnd_Form_Type;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Irs_Seq;
    private DbsField pnd_Ws_Pnd_Pt;
    private DbsField pnd_Ws_Pnd_St;
    private DbsField pnd_Ws_Pnd_Create_Ts;
    private DbsField pnd_Ws_Pnd_Hld_Company_Cde;
    private DbsField pnd_Ws_Pnd_Work_Tin;
    private DbsField pnd_Ws_Pnd_Hist_S_Start;
    private DbsField pnd_Ws_Pnd_Hist_S_End;
    private DbsField pnd_Ws_Pnd_S4_Start;
    private DbsField pnd_Ws_Pnd_S4_End;
    private DbsField pnd_Ws_Pnd_S9_Start;
    private DbsField pnd_Ws_Pnd_S9_End;

    private DbsGroup pnd_Case_Fields;
    private DbsField pnd_Case_Fields_Pnd_Case_Updated;
    private DbsField pnd_Case_Fields_Pnd_Empty_Form_Rpt;
    private DbsField pnd_Case_Fields_Pnd_Non_Empty_Form_Non_Rpt;
    private DbsField pnd_Case_Fields_Pnd_Empty_Form;
    private DbsField pnd_Case_Fields_Pnd_Non_Empty_Form;
    private DbsField pnd_Case_Fields_Pnd_Empty_Form_Tin;
    private DbsField pnd_Case_Fields_Pnd_Non_Empty_Form_Tin;
    private DbsField pnd_Case_Fields_Pnd_Empty_Form_Isn;
    private DbsField pnd_Case_Fields_Pnd_Non_Empty_Form_Isn;

    private DbsRecord internalLoopRecord;
    private DbsField h_FORMTirf_Superde_9Old;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTwracomp = new PdaTwracomp(localVariables);
        pdaTwratbl4 = new PdaTwratbl4(localVariables);
        pdaTwradcat = new PdaTwradcat(localVariables);
        pdaTwrairaa = new PdaTwrairaa(localVariables);
        pdaTwratin = new PdaTwratin(localVariables);
        pdaTwrafrmn = new PdaTwrafrmn(localVariables);
        pdaTwra5017 = new PdaTwra5017(localVariables);
        ldaTwrl9705 = new LdaTwrl9705();
        registerRecord(ldaTwrl9705);
        registerRecord(ldaTwrl9705.getVw_form_U());
        ldaTwrl9710 = new LdaTwrl9710();
        registerRecord(ldaTwrl9710);
        registerRecord(ldaTwrl9710.getVw_form());
        ldaTwrl9715 = new LdaTwrl9715();
        registerRecord(ldaTwrl9715);
        registerRecord(ldaTwrl9715.getVw_form_R());

        // Local Variables

        vw_hist_Form = new DataAccessProgramView(new NameInfo("vw_hist_Form", "HIST-FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        hist_Form_Tirf_Superde_9 = vw_hist_Form.getRecord().newFieldInGroup("hist_Form_Tirf_Superde_9", "TIRF-SUPERDE-9", FieldType.STRING, 33, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_9");
        hist_Form_Tirf_Superde_9.setSuperDescriptor(true);
        registerRecord(vw_hist_Form);

        pnd_Ws_Const = localVariables.newGroupInRecord("pnd_Ws_Const", "#WS-CONST");
        pnd_Ws_Const_Low_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_Low_Values", "LOW-VALUES", FieldType.STRING, 1);
        pnd_Ws_Const_High_Values = pnd_Ws_Const.newFieldInGroup("pnd_Ws_Const_High_Values", "HIGH-VALUES", FieldType.STRING, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Parms = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Ws_Pnd_Form = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Form", "#FORM", FieldType.STRING, 6);
        pnd_Ws_Pnd_Tax_Year = pnd_Ws_Pnd_Input_Parms.newFieldInGroup("pnd_Ws_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Cases_In_Error = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Cases_In_Error", "#CASES-IN-ERROR", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            4));
        pnd_Ws_Pnd_Cases_Linked = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Cases_Linked", "#CASES-LINKED", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            4));
        pnd_Ws_Pnd_Ok_To_Link = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ok_To_Link", "#OK-TO-LINK", FieldType.BOOLEAN, 1, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Reported_To_Irs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Reported_To_Irs", "#REPORTED-TO-IRS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Form_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Form_Type", "#FORM-TYPE", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Irs_Seq = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Irs_Seq", "#IRS-SEQ", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Pt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pt", "#PT", FieldType.STRING, 1);
        pnd_Ws_Pnd_St = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_St", "#ST", FieldType.STRING, 1);
        pnd_Ws_Pnd_Create_Ts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Create_Ts", "#CREATE-TS", FieldType.TIME);
        pnd_Ws_Pnd_Hld_Company_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hld_Company_Cde", "#HLD-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Work_Tin = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Work_Tin", "#WORK-TIN", FieldType.STRING, 11);
        pnd_Ws_Pnd_Hist_S_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hist_S_Start", "#HIST-S-START", FieldType.STRING, 8);
        pnd_Ws_Pnd_Hist_S_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Hist_S_End", "#HIST-S-END", FieldType.STRING, 8);
        pnd_Ws_Pnd_S4_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_Start", "#S4-START", FieldType.STRING, 32);
        pnd_Ws_Pnd_S4_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S4_End", "#S4-END", FieldType.STRING, 32);
        pnd_Ws_Pnd_S9_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S9_Start", "#S9-START", FieldType.STRING, 24);
        pnd_Ws_Pnd_S9_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_S9_End", "#S9-END", FieldType.STRING, 24);

        pnd_Case_Fields = localVariables.newGroupInRecord("pnd_Case_Fields", "#CASE-FIELDS");
        pnd_Case_Fields_Pnd_Case_Updated = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Case_Updated", "#CASE-UPDATED", FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Empty_Form_Rpt = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Empty_Form_Rpt", "#EMPTY-FORM-RPT", FieldType.BOOLEAN, 
            1);
        pnd_Case_Fields_Pnd_Non_Empty_Form_Non_Rpt = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Non_Empty_Form_Non_Rpt", "#NON-EMPTY-FORM-NON-RPT", 
            FieldType.BOOLEAN, 1);
        pnd_Case_Fields_Pnd_Empty_Form = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Empty_Form", "#EMPTY-FORM", FieldType.PACKED_DECIMAL, 7);
        pnd_Case_Fields_Pnd_Non_Empty_Form = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Non_Empty_Form", "#NON-EMPTY-FORM", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Case_Fields_Pnd_Empty_Form_Tin = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Empty_Form_Tin", "#EMPTY-FORM-TIN", FieldType.STRING, 
            10);
        pnd_Case_Fields_Pnd_Non_Empty_Form_Tin = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Non_Empty_Form_Tin", "#NON-EMPTY-FORM-TIN", FieldType.STRING, 
            10);
        pnd_Case_Fields_Pnd_Empty_Form_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Empty_Form_Isn", "#EMPTY-FORM-ISN", FieldType.PACKED_DECIMAL, 
            11);
        pnd_Case_Fields_Pnd_Non_Empty_Form_Isn = pnd_Case_Fields.newFieldInGroup("pnd_Case_Fields_Pnd_Non_Empty_Form_Isn", "#NON-EMPTY-FORM-ISN", FieldType.PACKED_DECIMAL, 
            11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        h_FORMTirf_Superde_9Old = internalLoopRecord.newFieldInRecord("H_FORM_Tirf_Superde_9_OLD", "Tirf_Superde_9_OLD", FieldType.STRING, 33);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_hist_Form.reset();
        internalLoopRecord.reset();

        ldaTwrl9705.initializeValues();
        ldaTwrl9710.initializeValues();
        ldaTwrl9715.initializeValues();

        localVariables.reset();
        pnd_Ws_Const_Low_Values.setInitialValue("H'00'");
        pnd_Ws_Const_High_Values.setInitialValue("H'FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp5102() throws Exception
    {
        super("Twrp5102");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 23 LS = 80 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 04 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 05 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 06 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 07 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 08 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 09 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 16 ) PS = 58 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
            //*  01/30/08 - AY
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 37T 'TaxWaRS' 68T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'Control Report' 68T 'Report: RPT1' / //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'Link TIN Error report' 120T 'Report: RPT2' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'Link TIN Error report' 120T 'Report: RPT3' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'Link TIN Error report' 120T 'Report: RPT4' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'Link TIN Error report' 120T 'Report: RPT5' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'Link TIN report' 120T 'Report: RPT6' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'Link TIN report' 120T 'Report: RPT7' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 08 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'Link TIN report' 120T 'Report: RPT8' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 09 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 63T 'TaxWaRS' 120T 'Page:' *PAGE-NUMBER ( 09 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'Link TIN report' 120T 'Report: RPT9' / 60T 'Tax Year:' #WS.#TAX-YEAR ( EM = 9999 SG = OFF ) / 63T #TWRAFRMN.#FORM-NAME ( #FORM-TYPE ) //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARMS
        sub_Process_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        //*  01/30/08 - AY
        pnd_Ws_Pnd_I.reset();                                                                                                                                             //Natural: RESET #I
        pdaTwradcat.getPnd_Twradcat_Pnd_Abend_Ind().setValue(pdaTwradcat.getPnd_Twradcat_Pnd_Display_Ind().getBoolean());                                                 //Natural: ASSIGN #TWRADCAT.#ABEND-IND := #TWRADCAT.#DISPLAY-IND
        pdaTwracomp.getPnd_Twracomp_Pnd_Abend_Ind().setValue(false);                                                                                                      //Natural: ASSIGN #TWRACOMP.#ABEND-IND := #TWRACOMP.#DISPLAY-IND := #TWRAIRAA.#ABEND-IND := #TWRAIRAA.#DISPLAY-IND := FALSE
        pdaTwracomp.getPnd_Twracomp_Pnd_Display_Ind().setValue(false);
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Abend_Ind().setValue(false);
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Display_Ind().setValue(false);
        pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().setValue(pnd_Ws_Const_High_Values);                                                                                   //Natural: ASSIGN #TWRADCAT.#DCAT-CODE := TWRATIN.#I-TIN-TYPE := TWRATIN.#I-TIN := #TWRACOMP.#COMP-CODE := #TWRA5017.#COUNTRY-CODE := #TWRAIRAA.#IRA-ACCT-TYPE := HIGH-VALUES
        pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(pnd_Ws_Const_High_Values);
        pdaTwratin.getTwratin_Pnd_I_Tin().setValue(pnd_Ws_Const_High_Values);
        pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(pnd_Ws_Const_High_Values);
        pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().setValue(pnd_Ws_Const_High_Values);
        pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().setValue(pnd_Ws_Const_High_Values);
        pdaTwra5017.getPnd_Twra5017_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                         //Natural: ASSIGN #TWRA5017.#TAX-YEAR := #TWRAFRMN.#TAX-YEAR := #WS.#TAX-YEAR
        pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);
        //*  01/30/08 - AY
        DbsUtil.callnat(Twrnfrmn.class , getCurrentProcessState(), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNFRMN' USING #TWRAFRMN.#INPUT-PARMS ( AD = O ) #TWRAFRMN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
        vw_hist_Form.createHistogram                                                                                                                                      //Natural: HISTOGRAM HIST-FORM FOR TIRF-SUPERDE-9 STARTING FROM #HIST-S-START THRU #HIST-S-END
        (
        "H_FORM",
        "TIRF_SUPERDE_9",
        new Wc[] { new Wc("TIRF_SUPERDE_9", ">=", pnd_Ws_Pnd_Hist_S_Start, "And", WcType.WITH) ,
        new Wc("TIRF_SUPERDE_9", "<=", pnd_Ws_Pnd_Hist_S_End, WcType.WITH) }
        );
        boolean endOfDataHForm = true;
        boolean firstHForm = true;
        H_FORM:
        while (condition(vw_hist_Form.readNextRow("H_FORM")))
        {
            if (condition(vw_hist_Form.getAstCOUNTER().greater(0)))
            {
                atBreakEventH_Form();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataHForm = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  AT BREAK OF TIRF-SUPERDE-9 /22/
            pnd_Ws_Pnd_I.nadd(1);                                                                                                                                         //Natural: AT BREAK OF TIRF-SUPERDE-9 /23/;//Natural: AT BREAK OF TIRF-SUPERDE-9 /7/;//Natural: ADD 1 TO #WS.#I
            h_FORMTirf_Superde_9Old.setValue(hist_Form_Tirf_Superde_9);                                                                                                   //Natural: END-HISTOGRAM
        }
        if (condition(vw_hist_Form.getAstCOUNTER().greater(0)))
        {
            atBreakEventH_Form(endOfDataHForm);
        }
        if (Global.isEscape()) return;
        getReports().skip(1, 4);                                                                                                                                          //Natural: SKIP ( 1 ) 4 LINES
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(50),"Cases",new ColumnSpacing(5)," Cases",NEWLINE,new TabSetting(50),"Linked",new ColumnSpacing(4),"in Error",NEWLINE,new  //Natural: WRITE ( 1 ) 50T 'Cases' 5X ' Cases' / 50T 'Linked' 4X 'in Error' / 50T '------' 4X '--------'
            TabSetting(50),"------",new ColumnSpacing(4),"--------");
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 4
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(4)); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  01/30/08 - AY
            if (condition(pnd_Ws_Pnd_Ok_To_Link.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                                     //Natural: IF #WS.#OK-TO-LINK ( #I )
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(14),"Form...............:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1),pnd_Ws_Pnd_Cases_Linked.getValue(pnd_Ws_Pnd_I),  //Natural: WRITE ( 1 ) 14T 'Form...............:' #TWRAFRMN.#FORM-NAME ( #I ) #WS.#CASES-LINKED ( #I ) #WS.#CASES-IN-ERROR ( #I )
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),pnd_Ws_Pnd_Cases_In_Error.getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* **********************
        //*  S U B R O U T I N E S
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CASE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-REPORTED-TO-IRS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LINK-TIN-CASE
        //* ******************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LINK-TIN-REPORT
        //*      'Rechar/Ind'         FORM-U.TIRF-5498-RECHAR-IND    (LC=��)
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COULD-NOT-LINK
        //*        'Rechar/Ind'         TIRF-5498-RECHAR-IND    (RD2-FORM.)(LC=��)
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-DCAT
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-TIN
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-COMPANY
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-COUNTRY
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-IRA-ACCT-TYPE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARMS
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Read_Case() throws Exception                                                                                                                         //Natural: READ-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  MOVE LOW-VALUES                    TO SUBSTR(#WS.#S9-START,23,1)
        //*  MOVE HIGH-VALUES                   TO SUBSTR(#WS.#S9-END,23,1)
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S9_Start,24,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S9-START,24,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S9_End,24,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S9-END,24,1 )
        pnd_Case_Fields.reset();                                                                                                                                          //Natural: RESET #CASE-FIELDS
        //*  ACTIVE NON REPORTED
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY FORM.TIRF-SUPERDE-9 = #WS.#S9-START THRU #WS.#S9-END WHERE FORM.TIRF-IRS-RPT-IND = ' ' OR = 'H'
        (
        "RD_FORM",
        new Wc[] { new Wc("( TIRF_IRS_RPT_IND = ' ' OR TIRF_IRS_RPT_IND = 'H' )  AND 1 ", "=" , "1", WcType.OTHER) ,
        new Wc("TIRF_SUPERDE_9", ">=", pnd_Ws_Pnd_S9_Start, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_9", "<=", pnd_Ws_Pnd_S9_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_9", "ASC") }
        );
        RD_FORM:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD_FORM")))
        {
            //*  RESET LINK TINS
            //*  FROM PREV RUNS
            if (condition(ldaTwrl9710.getForm_Tirf_Link_Tin().notEquals(" ")))                                                                                            //Natural: IF FORM.TIRF-LINK-TIN NE ' '
            {
                G3:                                                                                                                                                       //Natural: GET FORM-U *ISN ( RD-FORM. )
                ldaTwrl9705.getVw_form_U().readByID(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"), "G3");
                ldaTwrl9705.getForm_U_Tirf_Link_Tin().reset();                                                                                                            //Natural: RESET FORM-U.TIRF-LINK-TIN
                ldaTwrl9705.getVw_form_U().updateDBRow("G3");                                                                                                             //Natural: UPDATE ( G3. )
                pnd_Case_Fields_Pnd_Case_Updated.setValue(true);                                                                                                          //Natural: ASSIGN #CASE-FIELDS.#CASE-UPDATED := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Hld_Company_Cde.setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                                                  //Natural: ASSIGN #HLD-COMPANY-CDE := TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM CHECK-REPORTED-TO-IRS
            sub_Check_Reported_To_Irs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD_FORM"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD_FORM"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean() && ! (pnd_Ws_Pnd_Reported_To_Irs.getBoolean())))                                             //Natural: REJECT IF FORM.TIRF-EMPTY-FORM AND NOT #WS.#REPORTED-TO-IRS
            {
                continue;
            }
            if (condition(ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean()))                                                                                            //Natural: IF FORM.TIRF-EMPTY-FORM
            {
                pnd_Case_Fields_Pnd_Empty_Form.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CASE-FIELDS.#EMPTY-FORM
                pnd_Case_Fields_Pnd_Empty_Form_Rpt.setValue(true);                                                                                                        //Natural: ASSIGN #CASE-FIELDS.#EMPTY-FORM-RPT := TRUE
                pnd_Case_Fields_Pnd_Empty_Form_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                              //Natural: ASSIGN #CASE-FIELDS.#EMPTY-FORM-TIN := FORM.TIRF-TIN
                pnd_Case_Fields_Pnd_Empty_Form_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                               //Natural: ASSIGN #CASE-FIELDS.#EMPTY-FORM-ISN := *ISN ( RD-FORM. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Case_Fields_Pnd_Non_Empty_Form.nadd(1);                                                                                                               //Natural: ADD 1 TO #CASE-FIELDS.#NON-EMPTY-FORM
                if (condition(pnd_Ws_Pnd_Reported_To_Irs.getBoolean()))                                                                                                   //Natural: IF #WS.#REPORTED-TO-IRS
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Case_Fields_Pnd_Non_Empty_Form_Non_Rpt.setValue(true);                                                                                            //Natural: ASSIGN #CASE-FIELDS.#NON-EMPTY-FORM-NON-RPT := TRUE
                    pnd_Case_Fields_Pnd_Non_Empty_Form_Tin.setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                      //Natural: ASSIGN #CASE-FIELDS.#NON-EMPTY-FORM-TIN := FORM.TIRF-TIN
                    pnd_Case_Fields_Pnd_Non_Empty_Form_Isn.setValue(ldaTwrl9710.getVw_form().getAstISN("RD_FORM"));                                                       //Natural: ASSIGN #CASE-FIELDS.#NON-EMPTY-FORM-ISN := *ISN ( RD-FORM. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().notEquals(getZero())))                                                                                     //Natural: IF *COUNTER ( RD-FORM. ) NE 0
        {
            short decideConditionsMet1099 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CASE-FIELDS.#EMPTY-FORM = 1 AND #CASE-FIELDS.#NON-EMPTY-FORM = 1 AND #CASE-FIELDS.#EMPTY-FORM-RPT AND #CASE-FIELDS.#NON-EMPTY-FORM-NON-RPT
            if (condition(pnd_Case_Fields_Pnd_Empty_Form.equals(1) && pnd_Case_Fields_Pnd_Non_Empty_Form.equals(1) && pnd_Case_Fields_Pnd_Empty_Form_Rpt.getBoolean() 
                && pnd_Case_Fields_Pnd_Non_Empty_Form_Non_Rpt.getBoolean()))
            {
                decideConditionsMet1099++;
                                                                                                                                                                          //Natural: PERFORM LINK-TIN-CASE
                sub_Link_Tin_Case();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CASE-FIELDS.#EMPTY-FORM = 0 AND #CASE-FIELDS.#NON-EMPTY-FORM = 0 OR #CASE-FIELDS.#EMPTY-FORM = 1 AND #CASE-FIELDS.#NON-EMPTY-FORM = 0 OR #CASE-FIELDS.#EMPTY-FORM = 0 AND #CASE-FIELDS.#NON-EMPTY-FORM = 1
            else if (condition((((pnd_Case_Fields_Pnd_Empty_Form.equals(getZero()) && pnd_Case_Fields_Pnd_Non_Empty_Form.equals(getZero())) || (pnd_Case_Fields_Pnd_Empty_Form.equals(1) 
                && pnd_Case_Fields_Pnd_Non_Empty_Form.equals(getZero()))) || (pnd_Case_Fields_Pnd_Empty_Form.equals(getZero()) && pnd_Case_Fields_Pnd_Non_Empty_Form.equals(1)))))
            {
                decideConditionsMet1099++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM COULD-NOT-LINK
                sub_Could_Not_Link();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Case_Fields_Pnd_Case_Updated.getBoolean()))                                                                                                     //Natural: IF #CASE-FIELDS.#CASE-UPDATED
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Reported_To_Irs() throws Exception                                                                                                             //Natural: CHECK-REPORTED-TO-IRS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Ws_Pnd_Reported_To_Irs.reset();                                                                                                                               //Natural: RESET #WS.#REPORTED-TO-IRS
        pnd_Ws_Pnd_S4_End.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());                                                                                                 //Natural: ASSIGN #WS.#S4-END := #WS.#S4-START := FORM.TIRF-SUPERDE-4
        pnd_Ws_Pnd_S4_Start.setValue(ldaTwrl9710.getForm_Tirf_Superde_4());
        setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_S4_Start,32,1);                                                                                            //Natural: MOVE LOW-VALUES TO SUBSTR ( #WS.#S4-START,32,1 )
        setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_S4_End,32,1);                                                                                             //Natural: MOVE HIGH-VALUES TO SUBSTR ( #WS.#S4-END,32,1 )
        ldaTwrl9715.getVw_form_R().startDatabaseRead                                                                                                                      //Natural: READ ( 1 ) FORM-R BY FORM-R.TIRF-SUPERDE-4 = #WS.#S4-START THRU #WS.#S4-END WHERE FORM-R.TIRF-IRS-RPT-IND NE 'H' AND FORM-R.TIRF-COMPANY-CDE = #HLD-COMPANY-CDE
        (
        "RD1_FORM",
        new Wc[] { new Wc("TIRF_IRS_RPT_IND", "!=", "H", "And", WcType.WHERE) ,
        new Wc("TIRF_COMPANY_CDE", "=", pnd_Ws_Pnd_Hld_Company_Cde, WcType.WHERE) ,
        new Wc("TIRF_SUPERDE_4", ">=", pnd_Ws_Pnd_S4_Start.getBinary(), "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_4", "<=", pnd_Ws_Pnd_S4_End.getBinary(), WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_4", "ASC") },
        1
        );
        RD1_FORM:
        while (condition(ldaTwrl9715.getVw_form_R().readNextRow("RD1_FORM")))
        {
            pnd_Ws_Pnd_Reported_To_Irs.setValue(true);                                                                                                                    //Natural: ASSIGN #WS.#REPORTED-TO-IRS := TRUE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Link_Tin_Case() throws Exception                                                                                                                     //Natural: LINK-TIN-CASE
    {
        if (BLNatReinput.isReinput()) return;

        G1:                                                                                                                                                               //Natural: GET FORM-U #CASE-FIELDS.#EMPTY-FORM-ISN
        ldaTwrl9705.getVw_form_U().readByID(pnd_Case_Fields_Pnd_Empty_Form_Isn.getLong(), "G1");
        pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9705.getForm_U_Tirf_Form_Type());                                                                                            //Natural: ASSIGN #WS.#FORM-TYPE := FORM-U.TIRF-FORM-TYPE
        ldaTwrl9705.getForm_U_Tirf_Link_Tin().setValue(pnd_Case_Fields_Pnd_Non_Empty_Form_Tin);                                                                           //Natural: ASSIGN FORM-U.TIRF-LINK-TIN := #CASE-FIELDS.#NON-EMPTY-FORM-TIN
        ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
        ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Ws_Pnd_Create_Ts);                                                                                                //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#CREATE-TS
        pnd_Ws_Pnd_Irs_Seq.setValue(ldaTwrl9705.getForm_U_Tirf_Irs_Form_Seq());                                                                                           //Natural: ASSIGN #WS.#IRS-SEQ := FORM-U.TIRF-IRS-FORM-SEQ
        ldaTwrl9705.getVw_form_U().updateDBRow("G1");                                                                                                                     //Natural: UPDATE ( G1. )
                                                                                                                                                                          //Natural: PERFORM LINK-TIN-REPORT
        sub_Link_Tin_Report();
        if (condition(Global.isEscape())) {return;}
        G2:                                                                                                                                                               //Natural: GET FORM-U #CASE-FIELDS.#NON-EMPTY-FORM-ISN
        ldaTwrl9705.getVw_form_U().readByID(pnd_Case_Fields_Pnd_Non_Empty_Form_Isn.getLong(), "G2");
        pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9705.getForm_U_Tirf_Form_Type());                                                                                            //Natural: ASSIGN #WS.#FORM-TYPE := FORM-U.TIRF-FORM-TYPE
        ldaTwrl9705.getForm_U_Tirf_Link_Tin().setValue(pnd_Case_Fields_Pnd_Empty_Form_Tin);                                                                               //Natural: ASSIGN FORM-U.TIRF-LINK-TIN := #CASE-FIELDS.#EMPTY-FORM-TIN
        ldaTwrl9705.getForm_U_Tirf_Irs_Form_Seq().setValue(pnd_Ws_Pnd_Irs_Seq);                                                                                           //Natural: ASSIGN FORM-U.TIRF-IRS-FORM-SEQ := #WS.#IRS-SEQ
        ldaTwrl9705.getForm_U_Tirf_Lu_User().setValue(Global.getINIT_USER());                                                                                             //Natural: ASSIGN FORM-U.TIRF-LU-USER := *INIT-USER
        ldaTwrl9705.getForm_U_Tirf_Lu_Ts().setValue(pnd_Ws_Pnd_Create_Ts);                                                                                                //Natural: ASSIGN FORM-U.TIRF-LU-TS := #WS.#CREATE-TS
        ldaTwrl9705.getVw_form_U().updateDBRow("G2");                                                                                                                     //Natural: UPDATE ( G2. )
                                                                                                                                                                          //Natural: PERFORM LINK-TIN-REPORT
        sub_Link_Tin_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Case_Fields_Pnd_Case_Updated.setValue(true);                                                                                                                  //Natural: ASSIGN #CASE-FIELDS.#CASE-UPDATED := TRUE
        short decideConditionsMet1144 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM-TYPE;//Natural: VALUE 1
        if (condition((pnd_Ws_Pnd_Form_Type.equals(1))))
        {
            decideConditionsMet1144++;
            getReports().skip(6, 1);                                                                                                                                      //Natural: SKIP ( 6 ) 1 LINES
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(2))))
        {
            decideConditionsMet1144++;
            getReports().skip(9, 1);                                                                                                                                      //Natural: SKIP ( 9 ) 1 LINES
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(3))))
        {
            decideConditionsMet1144++;
            getReports().skip(7, 1);                                                                                                                                      //Natural: SKIP ( 7 ) 1 LINES
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(4))))
        {
            decideConditionsMet1144++;
            getReports().skip(8, 1);                                                                                                                                      //Natural: SKIP ( 8 ) 1 LINES
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1144 > 0))
        {
            pnd_Ws_Pnd_Cases_Linked.getValue(pnd_Ws_Pnd_Form_Type).nadd(1);                                                                                               //Natural: ADD 1 TO #WS.#CASES-LINKED ( #FORM-TYPE )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Link_Tin_Report() throws Exception                                                                                                                   //Natural: LINK-TIN-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9705.getForm_U_Tirf_Tin()))) //Natural: IF TWRATIN.#I-TIN-TYPE NE FORM-U.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM-U.TIRF-TIN
        {
            pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9705.getForm_U_Tirf_Tax_Id_Type());                                                                    //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM-U.TIRF-TAX-ID-TYPE
            pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9705.getForm_U_Tirf_Tin());                                                                                 //Natural: ASSIGN TWRATIN.#I-TIN := FORM-U.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM DECODE-TIN
            sub_Decode_Tin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BK
        if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().notEquals(ldaTwrl9705.getForm_U_Tirf_Company_Cde())))                                                   //Natural: IF #TWRACOMP.#COMP-CODE NE FORM-U.TIRF-COMPANY-CDE
        {
            pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Company_Cde());                                                               //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM-U.TIRF-COMPANY-CDE
            pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year().compute(new ComputeParameters(false, pdaTwracomp.getPnd_Twracomp_Pnd_Tax_Year()), ldaTwrl9705.getForm_U_Tirf_Tax_Year().val()); //Natural: ASSIGN #TWRACOMP.#TAX-YEAR := VAL ( FORM-U.TIRF-TAX-YEAR )
                                                                                                                                                                          //Natural: PERFORM DECODE-COMPANY
            sub_Decode_Company();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1172 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM-TYPE;//Natural: VALUE 1
        if (condition((pnd_Ws_Pnd_Form_Type.equals(1))))
        {
            decideConditionsMet1172++;
            if (condition(pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().notEquals(ldaTwrl9705.getForm_U_Tirf_Distr_Category())))                                            //Natural: IF #TWRADCAT.#DCAT-CODE NE FORM-U.TIRF-DISTR-CATEGORY
            {
                pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Distr_Category());                                                        //Natural: ASSIGN #TWRADCAT.#DCAT-CODE := FORM-U.TIRF-DISTR-CATEGORY
                                                                                                                                                                          //Natural: PERFORM DECODE-DCAT
                sub_Decode_Dcat();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Pt.setValue(ldaTwrl9705.getForm_U_Tirf_Key().getSubstring(4,1));                                                                                   //Natural: MOVE SUBSTR ( FORM-U.TIRF-KEY,4,1 ) TO #WS.#PT
            pnd_Ws_Pnd_St.setValue(ldaTwrl9705.getForm_U_Tirf_Key().getSubstring(5,1));                                                                                   //Natural: MOVE SUBSTR ( FORM-U.TIRF-KEY,5,1 ) TO #WS.#ST
            getReports().display(6, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"Dist/Code",            //Natural: DISPLAY ( 6 ) FORM-U.TIRF-CONTRACT-NBR FORM-U.TIRF-PAYEE-CDE ( LC = � ) 'Dist/Code' FORM-U.TIRF-DISTRIBUTION-CDE ( LC = � ) 'Dist/Cat' #TWRADCAT.#DCAT-SHORT 'P/T' #WS.#PT 'S/T' #WS.#ST '/Tax Id' #O-TIN '/Link TIN' FORM-U.TIRF-LINK-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM-U.TIRF-EMPTY-FORM ( EM = /YES ) 'Gross/Amount' FORM-U.TIRF-GROSS-AMT ( HC = R ) 'Taxable/Amount' FORM-U.TIRF-TAXABLE-AMT ( HC = R ) 'IVC/Amount' FORM-U.TIRF-IVC-AMT ( HC = R ) 'Fed/Tax' FORM-U.TIRF-FED-TAX-WTHLD ( HC = R ) '# of/States' FORM-U.C*TIRF-1099-R-STATE-GRP ( HC = R )
                
            		ldaTwrl9705.getForm_U_Tirf_Distribution_Cde(), new FieldAttributes("LC=�"),"Dist/Cat",
            		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(),"P/T",
            		pnd_Ws_Pnd_Pt,"S/T",
            		pnd_Ws_Pnd_St,"/Tax Id",
            		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
            		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
            		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
            		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Gross/Amount",
            		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Taxable/Amount",
            		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"IVC/Amount",
            		ldaTwrl9705.getForm_U_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fed/Tax",
            		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"# of/States",
            		ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(2))))
        {
            decideConditionsMet1172++;
            getReports().display(9, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",              //Natural: DISPLAY ( 9 ) FORM-U.TIRF-CONTRACT-NBR FORM-U.TIRF-PAYEE-CDE ( LC = � ) '/Tax Id' #O-TIN '/Link TIN' FORM-U.TIRF-LINK-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM-U.TIRF-EMPTY-FORM ( EM = /YES ) 'Interest/Amount' FORM-U.TIRF-INT-AMT ( HC = R ) 'Backup/Withholding' FORM-U.TIRF-FED-TAX-WTHLD ( HC = R )
                
            		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
            		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
            		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
            		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Interest/Amount",
            		ldaTwrl9705.getForm_U_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
            		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(3))))
        {
            decideConditionsMet1172++;
            if (condition(pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().notEquals(ldaTwrl9705.getForm_U_Tirf_Country_Code())))                                           //Natural: IF #TWRA5017.#COUNTRY-CODE NE FORM-U.TIRF-COUNTRY-CODE
            {
                pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().setValue(ldaTwrl9705.getForm_U_Tirf_Country_Code());                                                       //Natural: ASSIGN #TWRA5017.#COUNTRY-CODE := FORM-U.TIRF-COUNTRY-CODE
                                                                                                                                                                          //Natural: PERFORM DECODE-COUNTRY
                sub_Decode_Country();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(7, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Country",             //Natural: DISPLAY ( 7 ) FORM-U.TIRF-CONTRACT-NBR FORM-U.TIRF-PAYEE-CDE ( LC = � ) '/Country' #TWRA5017.#COUNTRY-DESC ( AL = 28 ) '/Tax Id' #O-TIN '/Link TIN' FORM-U.TIRF-LINK-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM-U.TIRF-EMPTY-FORM ( EM = /YES ) 'Line/Count' FORM-U.C*TIRF-1042-S-LINE-GRP 'Income/Code' FORM-U.TIRF-1042-S-INCOME-CODE ( 1 ) ( LC = �� ) 'Tax/Rate' FORM-U.TIRF-TAX-RATE ( 1 ) 'Exempt/Code' FORM-U.TIRF-EXEMPT-CODE ( 1 ) ( LC = �� ) 'Gross/Income' FORM-U.TIRF-GROSS-INCOME ( 1 ) 'NRA/Tax' FORM-U.TIRF-NRA-TAX-WTHLD ( 1 )
                
            		pdaTwra5017.getPnd_Twra5017_Pnd_Country_Desc(), new AlphanumericLength (28),"/Tax Id",
            		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
            		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
            		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
            		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Line/Count",
            		ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp(),"Income/Code",
            		ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(1), new FieldAttributes("LC=��"),"Tax/Rate",
            		ldaTwrl9705.getForm_U_Tirf_Tax_Rate().getValue(1),"Exempt/Code",
            		ldaTwrl9705.getForm_U_Tirf_Exempt_Code().getValue(1), new FieldAttributes("LC=��"),"Gross/Income",
            		ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(1),"NRA/Tax",
            		ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(1));
            if (Global.isEscape()) return;
            FOR02:                                                                                                                                                        //Natural: FOR #J = 2 TO FORM-U.C*TIRF-1042-S-LINE-GRP
            for (pnd_Ws_Pnd_J.setValue(2); condition(pnd_Ws_Pnd_J.lessOrEqual(ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp())); pnd_Ws_Pnd_J.nadd(1))
            {
                getReports().write(7, new ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(1)),new ColumnSpacing(3),ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code().getValue(pnd_Ws_Pnd_J),new  //Natural: WRITE ( 7 ) T*FORM-U.TIRF-1042-S-INCOME-CODE ( 1 ) 3X FORM-U.TIRF-1042-S-INCOME-CODE ( #J ) T*FORM-U.TIRF-TAX-RATE ( 1 ) FORM-U.TIRF-TAX-RATE ( #J ) T*FORM-U.TIRF-EXEMPT-CODE ( 1 ) 3X FORM-U.TIRF-EXEMPT-CODE ( #J ) T*FORM-U.TIRF-GROSS-INCOME ( 1 ) FORM-U.TIRF-GROSS-INCOME ( #J ) T*FORM-U.TIRF-NRA-TAX-WTHLD ( 1 ) FORM-U.TIRF-NRA-TAX-WTHLD ( #J )
                    ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_Tax_Rate().getValue(1)),ldaTwrl9705.getForm_U_Tirf_Tax_Rate().getValue(pnd_Ws_Pnd_J),new 
                    ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_Exempt_Code().getValue(1)),new ColumnSpacing(3),ldaTwrl9705.getForm_U_Tirf_Exempt_Code().getValue(pnd_Ws_Pnd_J),new 
                    ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(1)),ldaTwrl9705.getForm_U_Tirf_Gross_Income().getValue(pnd_Ws_Pnd_J),new 
                    ReportTAsterisk(ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(1)),ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Pnd_J));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Ws_Pnd_Form_Type.equals(4))))
        {
            decideConditionsMet1172++;
            if (condition(pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().notEquals(ldaTwrl9705.getForm_U_Tirf_Ira_Acct_Type())))                                         //Natural: IF #TWRAIRAA.#IRA-ACCT-TYPE NE FORM-U.TIRF-IRA-ACCT-TYPE
            {
                pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().setValue(ldaTwrl9705.getForm_U_Tirf_Ira_Acct_Type());                                                     //Natural: ASSIGN #TWRAIRAA.#IRA-ACCT-TYPE := FORM-U.TIRF-IRA-ACCT-TYPE
                                                                                                                                                                          //Natural: PERFORM DECODE-IRA-ACCT-TYPE
                sub_Decode_Ira_Acct_Type();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  ROTH IRA
            if (condition(ldaTwrl9705.getForm_U_Tirf_Ira_Acct_Type().equals("02")))                                                                                       //Natural: IF FORM-U.TIRF-IRA-ACCT-TYPE = '02'
            {
                ldaTwrl9705.getForm_U_Tirf_Trad_Ira_Contrib().setValue(ldaTwrl9705.getForm_U_Tirf_Roth_Ira_Contrib());                                                    //Natural: ASSIGN FORM-U.TIRF-TRAD-IRA-CONTRIB := FORM-U.TIRF-ROTH-IRA-CONTRIB
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(8, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),"Pyee/Code",                                                                                //Natural: DISPLAY ( 8 ) FORM-U.TIRF-CONTRACT-NBR 'Pyee/Code' FORM-U.TIRF-PAYEE-CDE ( LC = � ) '/Tax Id' #O-TIN '/Link TIN' FORM-U.TIRF-LINK-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM-U.TIRF-EMPTY-FORM ( EM = /YES ) 'IRA/Acct' #TWRAIRAA.#IRAA-SHORT '/Contributions' FORM-U.TIRF-TRAD-IRA-CONTRIB ( HC = R ) 'Rollover' FORM-U.TIRF-TRAD-IRA-ROLLOVER ( HC = R ) 'Rechar.' FORM-U.TIRF-IRA-RECHAR-AMT ( HC = R ) 'Roth/Conversion' FORM-U.TIRF-ROTH-IRA-CONVERSION ( HC = R ) '/FMV' FORM-U.TIRF-ACCOUNT-FMV ( HC = R )
            		ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",
            		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
            		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
            		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
            		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"IRA/Acct",
            		pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Short(),"/Contributions",
            		ldaTwrl9705.getForm_U_Tirf_Trad_Ira_Contrib(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rollover",
            		ldaTwrl9705.getForm_U_Tirf_Trad_Ira_Rollover(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rechar.",
            		ldaTwrl9705.getForm_U_Tirf_Ira_Rechar_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Roth/Conversion",
            		ldaTwrl9705.getForm_U_Tirf_Roth_Ira_Conversion(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/FMV",
            		ldaTwrl9705.getForm_U_Tirf_Account_Fmv(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Could_Not_Link() throws Exception                                                                                                                    //Natural: COULD-NOT-LINK
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  ACTIVE NON REPORTED
        ldaTwrl9710.getVw_form().startDatabaseRead                                                                                                                        //Natural: READ FORM BY FORM.TIRF-SUPERDE-9 = #WS.#S9-START THRU #WS.#S9-END WHERE FORM.TIRF-IRS-RPT-IND = ' ' OR = 'H'
        (
        "RD2_FORM",
        new Wc[] { new Wc("( TIRF_IRS_RPT_IND = ' ' OR TIRF_IRS_RPT_IND = 'H' )  AND 1 ", "=" , "1", WcType.OTHER) ,
        new Wc("TIRF_SUPERDE_9", ">=", pnd_Ws_Pnd_S9_Start, "And", WcType.BY) ,
        new Wc("TIRF_SUPERDE_9", "<=", pnd_Ws_Pnd_S9_End, WcType.BY) },
        new Oc[] { new Oc("TIRF_SUPERDE_9", "ASC") }
        );
        RD2_FORM:
        while (condition(ldaTwrl9710.getVw_form().readNextRow("RD2_FORM")))
        {
            CheckAtStartofData1211();

            if (condition(pdaTwratin.getTwratin_Pnd_I_Tin_Type().notEquals(ldaTwrl9710.getForm_Tirf_Tax_Id_Type()) || pdaTwratin.getTwratin_Pnd_I_Tin().notEquals(ldaTwrl9710.getForm_Tirf_Tin()))) //Natural: AT START OF DATA;//Natural: IF TWRATIN.#I-TIN-TYPE NE FORM.TIRF-TAX-ID-TYPE OR TWRATIN.#I-TIN NE FORM.TIRF-TIN
            {
                pdaTwratin.getTwratin_Pnd_I_Tin_Type().setValue(ldaTwrl9710.getForm_Tirf_Tax_Id_Type());                                                                  //Natural: ASSIGN TWRATIN.#I-TIN-TYPE := FORM.TIRF-TAX-ID-TYPE
                pdaTwratin.getTwratin_Pnd_I_Tin().setValue(ldaTwrl9710.getForm_Tirf_Tin());                                                                               //Natural: ASSIGN TWRATIN.#I-TIN := FORM.TIRF-TIN
                                                                                                                                                                          //Natural: PERFORM DECODE-TIN
                sub_Decode_Tin();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().notEquals(ldaTwrl9710.getForm_Tirf_Company_Cde())))                                                 //Natural: IF #TWRACOMP.#COMP-CODE NE FORM.TIRF-COMPANY-CDE
            {
                pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Code().setValue(ldaTwrl9710.getForm_Tirf_Company_Cde());                                                             //Natural: ASSIGN #TWRACOMP.#COMP-CODE := FORM.TIRF-COMPANY-CDE
                                                                                                                                                                          //Natural: PERFORM DECODE-COMPANY
                sub_Decode_Company();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1223 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM-TYPE;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Form_Type.equals(1))))
            {
                decideConditionsMet1223++;
                if (condition(pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().notEquals(ldaTwrl9710.getForm_Tirf_Distr_Category())))                                          //Natural: IF #TWRADCAT.#DCAT-CODE NE FORM.TIRF-DISTR-CATEGORY
                {
                    pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Code().setValue(ldaTwrl9710.getForm_Tirf_Distr_Category());                                                      //Natural: ASSIGN #TWRADCAT.#DCAT-CODE := FORM.TIRF-DISTR-CATEGORY
                                                                                                                                                                          //Natural: PERFORM DECODE-DCAT
                    sub_Decode_Dcat();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Pt.setValue(ldaTwrl9710.getForm_Tirf_Key().getSubstring(4,1));                                                                                 //Natural: MOVE SUBSTR ( FORM.TIRF-KEY ,4,1 ) TO #WS.#PT
                pnd_Ws_Pnd_St.setValue(ldaTwrl9710.getForm_Tirf_Key().getSubstring(5,1));                                                                                 //Natural: MOVE SUBSTR ( FORM.TIRF-KEY ,5,1 ) TO #WS.#ST
                getReports().display(2, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"Dist/Code",            //Natural: DISPLAY ( 2 ) FORM.TIRF-CONTRACT-NBR FORM.TIRF-PAYEE-CDE ( LC = � ) 'Dist/Code' FORM.TIRF-DISTRIBUTION-CDE ( LC = � ) 'Dist/Cat' #TWRADCAT.#DCAT-SHORT 'P/T' #WS.#PT 'S/T' #WS.#ST '/Tax Id' #O-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM.TIRF-EMPTY-FORM ( EM = /YES ) 'Gross/Amount' FORM.TIRF-GROSS-AMT ( HC = R ) 'Taxable/Amount' FORM.TIRF-TAXABLE-AMT ( HC = R ) 'IVC/Amount' FORM.TIRF-IVC-AMT ( HC = R ) 'Fed/Tax' FORM.TIRF-FED-TAX-WTHLD ( HC = R ) '# of/States' FORM.C*TIRF-1099-R-STATE-GRP ( HC = R )
                    
                		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new FieldAttributes("LC=�"),"Dist/Cat",
                		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(),"P/T",
                		pnd_Ws_Pnd_Pt,"S/T",
                		pnd_Ws_Pnd_St,"/Tax Id",
                		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
                		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
                		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Gross/Amount",
                		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Taxable/Amount",
                		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"IVC/Amount",
                		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fed/Tax",
                		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"# of/States",
                		ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(2))))
            {
                decideConditionsMet1223++;
                getReports().display(5, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",              //Natural: DISPLAY ( 5 ) FORM.TIRF-CONTRACT-NBR FORM.TIRF-PAYEE-CDE ( LC = � ) '/Tax Id' #O-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM.TIRF-EMPTY-FORM ( EM = /YES ) 'Interest/Amount' FORM.TIRF-INT-AMT ( HC = R ) 'Backup/Withholding' FORM.TIRF-FED-TAX-WTHLD ( HC = R )
                    
                		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
                		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
                		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Interest/Amount",
                		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
                		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(3))))
            {
                decideConditionsMet1223++;
                if (condition(pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().notEquals(ldaTwrl9710.getForm_Tirf_Country_Code())))                                         //Natural: IF #TWRA5017.#COUNTRY-CODE NE FORM.TIRF-COUNTRY-CODE
                {
                    pdaTwra5017.getPnd_Twra5017_Pnd_Country_Code().setValue(ldaTwrl9710.getForm_Tirf_Country_Code());                                                     //Natural: ASSIGN #TWRA5017.#COUNTRY-CODE := FORM.TIRF-COUNTRY-CODE
                                                                                                                                                                          //Natural: PERFORM DECODE-COUNTRY
                    sub_Decode_Country();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(3, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Country",             //Natural: DISPLAY ( 3 ) FORM.TIRF-CONTRACT-NBR FORM.TIRF-PAYEE-CDE ( LC = � ) '/Country' #TWRA5017.#COUNTRY-DESC '/Tax Id' #O-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM.TIRF-EMPTY-FORM ( EM = /YES ) 'Line/Count' C*TIRF-1042-S-LINE-GRP 'Income/Code' FORM.TIRF-1042-S-INCOME-CODE ( 1 ) ( LC = �� ) 'Tax/Rate' FORM.TIRF-TAX-RATE ( 1 ) 'Exempt/Code' FORM.TIRF-EXEMPT-CODE ( 1 ) ( LC = �� ) 'Gross/Income' FORM.TIRF-GROSS-INCOME ( 1 ) 'NRA/Tax' FORM.TIRF-NRA-TAX-WTHLD ( 1 )
                    
                		pdaTwra5017.getPnd_Twra5017_Pnd_Country_Desc(),"/Tax Id",
                		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
                		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
                		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Line/Count",
                		ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp(),"Income/Code",
                		ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(1), new FieldAttributes("LC=��"),"Tax/Rate",
                		ldaTwrl9710.getForm_Tirf_Tax_Rate().getValue(1),"Exempt/Code",
                		ldaTwrl9710.getForm_Tirf_Exempt_Code().getValue(1), new FieldAttributes("LC=��"),"Gross/Income",
                		ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(1),"NRA/Tax",
                		ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(1));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #J = 2 TO FORM.C*TIRF-1042-S-LINE-GRP
                for (pnd_Ws_Pnd_J.setValue(2); condition(pnd_Ws_Pnd_J.lessOrEqual(ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp())); pnd_Ws_Pnd_J.nadd(1))
                {
                    getReports().write(3, new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_1042_S_Income_Code()),(1),new ColumnSpacing(3),ldaTwrl9710.getForm_Tirf_1042_S_Income_Code().getValue(pnd_Ws_Pnd_J),new  //Natural: WRITE ( 3 ) FORM.T*TIRF-1042-S-INCOME-CODE ( 1 ) 3X FORM.TIRF-1042-S-INCOME-CODE ( #J ) FORM.T*TIRF-TAX-RATE ( 1 ) FORM.TIRF-TAX-RATE ( #J ) FORM.T*TIRF-EXEMPT-CODE ( 1 ) 3X FORM.TIRF-EXEMPT-CODE ( #J ) FORM.T*TIRF-GROSS-INCOME ( 1 ) FORM.TIRF-GROSS-INCOME ( #J ) FORM.T*TIRF-NRA-TAX-WTHLD ( 1 ) FORM.TIRF-NRA-TAX-WTHLD ( #J )
                        ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Tax_Rate()),(1),ldaTwrl9710.getForm_Tirf_Tax_Rate().getValue(pnd_Ws_Pnd_J),new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Exempt_Code()),(1),new 
                        ColumnSpacing(3),ldaTwrl9710.getForm_Tirf_Exempt_Code().getValue(pnd_Ws_Pnd_J),new ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Gross_Income()),(1),ldaTwrl9710.getForm_Tirf_Gross_Income().getValue(pnd_Ws_Pnd_J),new 
                        ReportTAsterisk(ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld()),(1),ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld().getValue(pnd_Ws_Pnd_J));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(4))))
            {
                decideConditionsMet1223++;
                if (condition(pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().notEquals(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type())))                                       //Natural: IF #TWRAIRAA.#IRA-ACCT-TYPE NE FORM.TIRF-IRA-ACCT-TYPE
                {
                    pdaTwrairaa.getPnd_Twrairaa_Pnd_Ira_Acct_Type().setValue(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type());                                                   //Natural: ASSIGN #TWRAIRAA.#IRA-ACCT-TYPE := FORM.TIRF-IRA-ACCT-TYPE
                                                                                                                                                                          //Natural: PERFORM DECODE-IRA-ACCT-TYPE
                    sub_Decode_Ira_Acct_Type();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  ROTH IRA
                if (condition(ldaTwrl9710.getForm_Tirf_Ira_Acct_Type().equals("02")))                                                                                     //Natural: IF FORM.TIRF-IRA-ACCT-TYPE = '02'
                {
                    ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib().setValue(ldaTwrl9710.getForm_Tirf_Roth_Ira_Contrib());                                                    //Natural: ASSIGN FORM.TIRF-TRAD-IRA-CONTRIB := FORM.TIRF-ROTH-IRA-CONTRIB
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(4, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",              //Natural: DISPLAY ( 4 ) FORM.TIRF-CONTRACT-NBR FORM.TIRF-PAYEE-CDE ( LC = � ) '/Tax Id' #O-TIN '/Comp' #TWRACOMP.#COMP-SHORT-NAME 'Zero/Form' FORM.TIRF-EMPTY-FORM ( EM = /YES ) 'IRA/Acct' #TWRAIRAA.#IRAA-SHORT '/Contributions' FORM.TIRF-TRAD-IRA-CONTRIB ( HC = R ) 'Rollover' FORM.TIRF-TRAD-IRA-ROLLOVER ( HC = R ) 'Rechar' FORM.TIRF-IRA-RECHAR-AMT ( HC = R ) 'Roth/Conversion' FORM.TIRF-ROTH-IRA-CONVERSION ( HC = R ) '/FMV' FORM.TIRF-ACCOUNT-FMV ( HC = R )
                    
                		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
                		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
                		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"IRA/Acct",
                		pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Short(),"/Contributions",
                		ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rollover",
                		ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rechar",
                		ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Roth/Conversion",
                		ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/FMV",
                		ldaTwrl9710.getForm_Tirf_Account_Fmv(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2_FORM"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaTwrl9710.getVw_form().getAstCOUNTER().notEquals(getZero())))                                                                                     //Natural: IF *COUNTER ( RD2-FORM. ) NE 0
        {
            short decideConditionsMet1258 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM-TYPE;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_Form_Type.equals(1))))
            {
                decideConditionsMet1258++;
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 2 ) 1 LINES
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(2))))
            {
                decideConditionsMet1258++;
                getReports().skip(5, 1);                                                                                                                                  //Natural: SKIP ( 5 ) 1 LINES
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(3))))
            {
                decideConditionsMet1258++;
                getReports().skip(3, 1);                                                                                                                                  //Natural: SKIP ( 3 ) 1 LINES
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ws_Pnd_Form_Type.equals(4))))
            {
                decideConditionsMet1258++;
                getReports().skip(4, 1);                                                                                                                                  //Natural: SKIP ( 4 ) 1 LINES
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1258 > 0))
            {
                pnd_Ws_Pnd_Cases_In_Error.getValue(pnd_Ws_Pnd_Form_Type).nadd(1);                                                                                         //Natural: ADD 1 TO #WS.#CASES-IN-ERROR ( #FORM-TYPE )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Decode_Dcat() throws Exception                                                                                                                       //Natural: DECODE-DCAT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.callnat(Twrndcat.class , getCurrentProcessState(), pdaTwradcat.getPnd_Twradcat_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwradcat.getPnd_Twradcat_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNDCAT' USING #TWRADCAT.#INPUT-PARMS ( AD = O ) #TWRADCAT.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Decode_Tin() throws Exception                                                                                                                        //Natural: DECODE-TIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        DbsUtil.callnat(Twrntin.class , getCurrentProcessState(), pdaTwratin.getTwratin_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwratin.getTwratin_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNTIN' USING TWRATIN.#INPUT-PARMS ( AD = O ) TWRATIN.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Decode_Company() throws Exception                                                                                                                    //Natural: DECODE-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrncomp.class , getCurrentProcessState(), pdaTwracomp.getPnd_Twracomp_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwracomp.getPnd_Twracomp_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNCOMP' USING #TWRACOMP.#INPUT-PARMS ( AD = O ) #TWRACOMP.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Decode_Country() throws Exception                                                                                                                    //Natural: DECODE-COUNTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        DbsUtil.callnat(Twrn5017.class , getCurrentProcessState(), pdaTwra5017.getPnd_Twra5017());                                                                        //Natural: CALLNAT 'TWRN5017' USING #TWRA5017
        if (condition(Global.isEscape())) return;
    }
    private void sub_Decode_Ira_Acct_Type() throws Exception                                                                                                              //Natural: DECODE-IRA-ACCT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        DbsUtil.callnat(Twrniraa.class , getCurrentProcessState(), pdaTwrairaa.getPnd_Twrairaa_Pnd_Input_Parms(), new AttributeParameter("O"), pdaTwrairaa.getPnd_Twrairaa_Pnd_Output_Data(),  //Natural: CALLNAT 'TWRNIRAA' USING #TWRAIRAA.#INPUT-PARMS ( AD = O ) #TWRAIRAA.#OUTPUT-DATA ( AD = M )
            new AttributeParameter("M"));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Input_Parms() throws Exception                                                                                                               //Natural: PROCESS-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("TWRP5102|sub_Process_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parms);                                                                               //Natural: INPUT #WS.#INPUT-PARMS
                if (condition(pnd_Ws_Pnd_Tax_Year.equals(getZero())))                                                                                                     //Natural: IF #WS.#TAX-YEAR = 0
                {
                    pnd_Ws_Pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Ws_Pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                           //Natural: ASSIGN #WS.#TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Hist_S_Start.setValueEdited(pnd_Ws_Pnd_Tax_Year,new ReportEditMask("9999"));                                                                   //Natural: MOVE EDITED #WS.#TAX-YEAR ( EM = 9999 ) TO #HIST-S-START
                setValueToSubstring("A",pnd_Ws_Pnd_Hist_S_Start,5,1);                                                                                                     //Natural: MOVE 'A' TO SUBSTR ( #HIST-S-START,5,1 )
                pnd_Ws_Pnd_Hist_S_End.setValue(pnd_Ws_Pnd_Hist_S_Start);                                                                                                  //Natural: ASSIGN #HIST-S-END := #HIST-S-START
                short decideConditionsMet1304 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #WS.#FORM;//Natural: VALUE '1099'
                if (condition((pnd_Ws_Pnd_Form.equals("1099"))))
                {
                    decideConditionsMet1304++;
                    pnd_Ws_Pnd_Ok_To_Link.getValue(1,":",2).setValue(true);                                                                                               //Natural: ASSIGN #WS.#OK-TO-LINK ( 1:2 ) := TRUE
                    setValueToSubstring("01",pnd_Ws_Pnd_Hist_S_Start,6,2);                                                                                                //Natural: MOVE '01' TO SUBSTR ( #HIST-S-START,6,2 )
                    setValueToSubstring("02",pnd_Ws_Pnd_Hist_S_End,6,2);                                                                                                  //Natural: MOVE '02' TO SUBSTR ( #HIST-S-END,6,2 )
                    getReports().definePrinter(3, "'CMPRT15'");                                                                                                           //Natural: DEFINE PRINTER ( 02 ) OUTPUT 'CMPRT15'
                    getReports().definePrinter(7, "'CMPRT16'");                                                                                                           //Natural: DEFINE PRINTER ( 06 ) OUTPUT 'CMPRT16'
                }                                                                                                                                                         //Natural: VALUE '1042-S'
                else if (condition((pnd_Ws_Pnd_Form.equals("1042-S"))))
                {
                    decideConditionsMet1304++;
                    pnd_Ws_Pnd_Ok_To_Link.getValue(3).setValue(true);                                                                                                     //Natural: ASSIGN #WS.#OK-TO-LINK ( 3 ) := TRUE
                    setValueToSubstring("03",pnd_Ws_Pnd_Hist_S_Start,6,2);                                                                                                //Natural: MOVE '03' TO SUBSTR ( #HIST-S-START,6,2 )
                    setValueToSubstring("03",pnd_Ws_Pnd_Hist_S_End,6,2);                                                                                                  //Natural: MOVE '03' TO SUBSTR ( #HIST-S-END,6,2 )
                    getReports().definePrinter(4, "'CMPRT15'");                                                                                                           //Natural: DEFINE PRINTER ( 03 ) OUTPUT 'CMPRT15'
                    getReports().definePrinter(8, "'CMPRT16'");                                                                                                           //Natural: DEFINE PRINTER ( 07 ) OUTPUT 'CMPRT16'
                }                                                                                                                                                         //Natural: VALUE '5498'
                else if (condition((pnd_Ws_Pnd_Form.equals("5498"))))
                {
                    decideConditionsMet1304++;
                    pnd_Ws_Pnd_Ok_To_Link.getValue(4).setValue(true);                                                                                                     //Natural: ASSIGN #WS.#OK-TO-LINK ( 4 ) := TRUE
                    setValueToSubstring("04",pnd_Ws_Pnd_Hist_S_Start,6,2);                                                                                                //Natural: MOVE '04' TO SUBSTR ( #HIST-S-START,6,2 )
                    setValueToSubstring("04",pnd_Ws_Pnd_Hist_S_End,6,2);                                                                                                  //Natural: MOVE '04' TO SUBSTR ( #HIST-S-END,6,2 )
                    getReports().definePrinter(5, "'CMPRT15'");                                                                                                           //Natural: DEFINE PRINTER ( 04 ) OUTPUT 'CMPRT15'
                    getReports().definePrinter(9, "'CMPRT16'");                                                                                                           //Natural: DEFINE PRINTER ( 08 ) OUTPUT 'CMPRT16'
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet1304 > 0))
                {
                    setValueToSubstring(pnd_Ws_Const_Low_Values,pnd_Ws_Pnd_Hist_S_Start,8,1);                                                                             //Natural: MOVE LOW-VALUES TO SUBSTR ( #HIST-S-START,8,1 )
                    setValueToSubstring(pnd_Ws_Const_High_Values,pnd_Ws_Pnd_Hist_S_End,8,1);                                                                              //Natural: MOVE HIGH-VALUES TO SUBSTR ( #HIST-S-END,8,1 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Unknown input parmameter:",pnd_Ws_Pnd_Form,new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 1 ) '***' 25T 'Unknown input parmameter:' #WS.#FORM 77T '***' / '***' 25T 'Valid values are:.......:"1099" - 1099-R & 1099-INT' 77T '***' / '***' 25T '                         "1042-S"' 77T '***' / '***' 25T '                         "5498"' 77T '***'
                        TabSetting(25),"Valid values are:.......:'1099' - 1099-R & 1099-INT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"                         '1042-S'",new 
                        TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"                         '5498'",new TabSetting(77),"***");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(101);  if (true) return;                                                                                                            //Natural: TERMINATE 101
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"'Link TIN' process parameters:",NEWLINE,new TabSetting(14),"Tax Year...........:",pnd_Ws_Pnd_Tax_Year,  //Natural: WRITE ( 1 ) 7T '"Link TIN" process parameters:' / 14T 'Tax Year...........:' #WS.#TAX-YEAR ( EM = 9999 )
                    new ReportEditMask ("9999"));
                if (Global.isEscape()) return;
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Tax_Year().setValue(pnd_Ws_Pnd_Tax_Year);                                                                                 //Natural: ASSIGN #TWRATBL4.#TAX-YEAR := #WS.#TAX-YEAR
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Abend_Ind().setValue(true);                                                                                               //Natural: ASSIGN #TWRATBL4.#ABEND-IND := TRUE
                pdaTwratbl4.getPnd_Twratbl4_Pnd_Display_Ind().setValue(true);                                                                                             //Natural: ASSIGN #TWRATBL4.#DISPLAY-IND := TRUE
                FOR04:                                                                                                                                                    //Natural: FOR #I = 1 TO 4
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(4)); pnd_Ws_Pnd_I.nadd(1))
                {
                    if (condition(pnd_Ws_Pnd_Ok_To_Link.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                             //Natural: IF #WS.#OK-TO-LINK ( #I )
                    {
                        pdaTwratbl4.getPnd_Twratbl4_Pnd_Form_Ind().setValue(pnd_Ws_Pnd_I);                                                                                //Natural: ASSIGN #TWRATBL4.#FORM-IND := #I
                        //*  01/30/08 - AY
                        DbsUtil.callnat(Twrntb4r.class , getCurrentProcessState(), pdaTwratbl4.getPnd_Twratbl4_Pnd_Input_Parms(), new AttributeParameter("O"),            //Natural: CALLNAT 'TWRNTB4R' USING #TWRATBL4.#INPUT-PARMS ( AD = O ) #TWRATBL4.#OUTPUT-DATA ( AD = M )
                            pdaTwratbl4.getPnd_Twratbl4_Pnd_Output_Data(), new AttributeParameter("M"));
                        if (condition(Global.isEscape())) return;
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(14),"Form...............:",pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name().getValue(pnd_Ws_Pnd_I.getInt() + 1)); //Natural: WRITE ( 1 ) / 14T 'Form...............:' #TWRAFRMN.#FORM-NAME ( #I )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*    IF #TWRATBL4.C-TIRCNTL-RPT-TBL-PE LT 1
                        //*      PERFORM  ERROR-DISPLAY-START
                        //*      WRITE(1)
                        //*        '***' 25T 'PERIOD PRIOR TO THE ORIGINAL REPORTING.' 77T '***'
                        //*      PERFORM  ERROR-DISPLAY-END
                        //*      TERMINATE 102
                        //*    END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Create_Ts.setValue(Global.getTIMX());                                                                                                          //Natural: ASSIGN #WS.#CREATE-TS := *TIMX
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE ( 1 ) NOTITLE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"Notify System Support",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Module:",Global.getPROGRAM(),new  //Natural: WRITE ( 1 ) NOTITLE '***' 25T 'Notify System Support' 77T '***' / '***' 25T 'Module:' *PROGRAM 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new 
            RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventH_Form() throws Exception {atBreakEventH_Form(false);}
    private void atBreakEventH_Form(boolean endOfData) throws Exception
    {
        boolean hist_Form_Tirf_Superde_923IsBreak = hist_Form_Tirf_Superde_9.isBreak(endOfData);
        boolean hist_Form_Tirf_Superde_97IsBreak = hist_Form_Tirf_Superde_9.isBreak(endOfData);
        if (condition(hist_Form_Tirf_Superde_923IsBreak || hist_Form_Tirf_Superde_97IsBreak))
        {
            if (condition(pnd_Ws_Pnd_I.greater(1)))                                                                                                                       //Natural: IF #I > 1
            {
                pnd_Ws_Pnd_S9_End.setValue(h_FORMTirf_Superde_9Old);                                                                                                      //Natural: ASSIGN #WS.#S9-END := #WS.#S9-START := OLD ( TIRF-SUPERDE-9 )
                pnd_Ws_Pnd_S9_Start.setValue(h_FORMTirf_Superde_9Old);
                                                                                                                                                                          //Natural: PERFORM READ-CASE
                sub_Read_Case();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_I.reset();                                                                                                                                         //Natural: RESET #WS.#I
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(hist_Form_Tirf_Superde_923IsBreak || hist_Form_Tirf_Superde_97IsBreak))
        {
            //*  NOT EOF
            //*  1099-I
            if (condition(hist_Form_Tirf_Superde_9.notEquals(h_FORMTirf_Superde_9Old) && hist_Form_Tirf_Superde_9.getSubstring(6,2).equals("02")))                        //Natural: IF TIRF-SUPERDE-9 NE OLD ( TIRF-SUPERDE-9 ) AND SUBSTR ( TIRF-SUPERDE-9,6,2 ) = '02'
            {
                getReports().definePrinter(6, "'CMPRT15'");                                                                                                               //Natural: DEFINE PRINTER ( 05 ) OUTPUT 'CMPRT15'
                getReports().definePrinter(10, "'CMPRT16'");                                                                                                              //Natural: DEFINE PRINTER ( 09 ) OUTPUT 'CMPRT16'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=80 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");
        Global.format(3, "PS=58 LS=133 ZP=ON");
        Global.format(4, "PS=58 LS=133 ZP=ON");
        Global.format(5, "PS=58 LS=133 ZP=ON");
        Global.format(6, "PS=58 LS=133 ZP=ON");
        Global.format(7, "PS=58 LS=133 ZP=ON");
        Global.format(8, "PS=58 LS=133 ZP=ON");
        Global.format(9, "PS=58 LS=133 ZP=ON");
        Global.format(15, "PS=58 LS=133 ZP=ON");
        Global.format(16, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(37),"TaxWaRS",new TabSetting(68),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(34),"Control Report",new TabSetting(68),"Report: RPT1",NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"Link TIN Error report",new TabSetting(120),"Report: RPT2",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"Link TIN Error report",new TabSetting(120),"Report: RPT3",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"Link TIN Error report",new TabSetting(120),"Report: RPT4",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"Link TIN Error report",new TabSetting(120),"Report: RPT5",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"Link TIN report",new TabSetting(120),"Report: RPT6",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"Link TIN report",new TabSetting(120),"Report: RPT7",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"Link TIN report",new TabSetting(120),"Report: RPT8",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);
        getReports().write(9, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(63),"TaxWaRS",new TabSetting(120),"Page:",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"Link TIN report",new TabSetting(120),"Report: RPT9",NEWLINE,new TabSetting(60),"Tax Year:",pnd_Ws_Pnd_Tax_Year, new ReportEditMask 
            ("9999"), new SignPosition (false),NEWLINE,new TabSetting(63),pdaTwrafrmn.getPnd_Twrafrmn_Pnd_Form_Name(),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(6, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),
            "Dist/Code",
        		ldaTwrl9705.getForm_U_Tirf_Distribution_Cde(), new FieldAttributes("LC=�"),"Dist/Cat",
        		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(),"P/T",
        		pnd_Ws_Pnd_Pt,"S/T",
        		pnd_Ws_Pnd_St,"/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
        		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Gross/Amount",
        		ldaTwrl9705.getForm_U_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Taxable/Amount",
        		ldaTwrl9705.getForm_U_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"IVC/Amount",
        		ldaTwrl9705.getForm_U_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fed/Tax",
        		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"# of/States",
        		ldaTwrl9705.getForm_U_Count_Casttirf_1099_R_State_Grp(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(9, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),
            "/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
        		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Interest/Amount",
        		ldaTwrl9705.getForm_U_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
        		ldaTwrl9705.getForm_U_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(7, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),
            "/Country",
        		pdaTwra5017.getPnd_Twra5017_Pnd_Country_Desc(), new AlphanumericLength (28),"/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
        		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Line/Count",
        		ldaTwrl9705.getForm_U_Count_Casttirf_1042_S_Line_Grp(),"Income/Code",
        		ldaTwrl9705.getForm_U_Tirf_1042_S_Income_Code(), new FieldAttributes("LC=��"),"Tax/Rate",
        		ldaTwrl9705.getForm_U_Tirf_Tax_Rate(),"Exempt/Code",
        		ldaTwrl9705.getForm_U_Tirf_Exempt_Code(), new FieldAttributes("LC=��"),"Gross/Income",
        		ldaTwrl9705.getForm_U_Tirf_Gross_Income(),"NRA/Tax",
        		ldaTwrl9705.getForm_U_Tirf_Nra_Tax_Wthld());
        getReports().setDisplayColumns(8, ldaTwrl9705.getForm_U_Tirf_Contract_Nbr(),"Pyee/Code",
        		ldaTwrl9705.getForm_U_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Link TIN",
        		ldaTwrl9705.getForm_U_Tirf_Link_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9705.getForm_U_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"IRA/Acct",
        		pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Short(),"/Contributions",
        		ldaTwrl9705.getForm_U_Tirf_Trad_Ira_Contrib(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rollover",
        		ldaTwrl9705.getForm_U_Tirf_Trad_Ira_Rollover(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rechar.",
        		ldaTwrl9705.getForm_U_Tirf_Ira_Rechar_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Roth/Conversion",
        		ldaTwrl9705.getForm_U_Tirf_Roth_Ira_Conversion(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/FMV",
        		ldaTwrl9705.getForm_U_Tirf_Account_Fmv(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(2, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"Dist/Code",
            
        		ldaTwrl9710.getForm_Tirf_Distribution_Cde(), new FieldAttributes("LC=�"),"Dist/Cat",
        		pdaTwradcat.getPnd_Twradcat_Pnd_Dcat_Short(),"P/T",
        		pnd_Ws_Pnd_Pt,"S/T",
        		pnd_Ws_Pnd_St,"/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Gross/Amount",
        		ldaTwrl9710.getForm_Tirf_Gross_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Taxable/Amount",
        		ldaTwrl9710.getForm_Tirf_Taxable_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"IVC/Amount",
        		ldaTwrl9710.getForm_Tirf_Ivc_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fed/Tax",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"# of/States",
        		ldaTwrl9710.getForm_Count_Casttirf_1099_R_State_Grp(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(5, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",
            
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Interest/Amount",
        		ldaTwrl9710.getForm_Tirf_Int_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Backup/Withholding",
        		ldaTwrl9710.getForm_Tirf_Fed_Tax_Wthld(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(3, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Country",
            
        		pdaTwra5017.getPnd_Twra5017_Pnd_Country_Desc(),"/Tax Id",
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"Line/Count",
        		ldaTwrl9710.getForm_Count_Casttirf_1042_S_Line_Grp(),"Income/Code",
        		ldaTwrl9710.getForm_Tirf_1042_S_Income_Code(), new FieldAttributes("LC=��"),"Tax/Rate",
        		ldaTwrl9710.getForm_Tirf_Tax_Rate(),"Exempt/Code",
        		ldaTwrl9710.getForm_Tirf_Exempt_Code(), new FieldAttributes("LC=��"),"Gross/Income",
        		ldaTwrl9710.getForm_Tirf_Gross_Income(),"NRA/Tax",
        		ldaTwrl9710.getForm_Tirf_Nra_Tax_Wthld());
        getReports().setDisplayColumns(4, ldaTwrl9710.getForm_Tirf_Contract_Nbr(),ldaTwrl9710.getForm_Tirf_Payee_Cde(), new FieldAttributes("LC=�"),"/Tax Id",
            
        		pdaTwratin.getTwratin_Pnd_O_Tin(),"/Comp",
        		pdaTwracomp.getPnd_Twracomp_Pnd_Comp_Short_Name(),"Zero/Form",
        		ldaTwrl9710.getForm_Tirf_Empty_Form().getBoolean(), new ReportEditMask ("/YES"),"IRA/Acct",
        		pdaTwrairaa.getPnd_Twrairaa_Pnd_Iraa_Short(),"/Contributions",
        		ldaTwrl9710.getForm_Tirf_Trad_Ira_Contrib(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rollover",
        		ldaTwrl9710.getForm_Tirf_Trad_Ira_Rollover(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Rechar",
        		ldaTwrl9710.getForm_Tirf_Ira_Rechar_Amt(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Roth/Conversion",
        		ldaTwrl9710.getForm_Tirf_Roth_Ira_Conversion(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/FMV",
        		ldaTwrl9710.getForm_Tirf_Account_Fmv(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData1211() throws Exception
    {
        if (condition(ldaTwrl9710.getVw_form().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Form_Type.setValue(ldaTwrl9710.getForm_Tirf_Form_Type());                                                                                          //Natural: ASSIGN #WS.#FORM-TYPE := FORM.TIRF-FORM-TYPE
        }                                                                                                                                                                 //Natural: END-START
    }
}
