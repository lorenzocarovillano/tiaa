/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:30:39 PM
**        * FROM NATURAL PROGRAM : Twrp0600
************************************************************
**        * FILE NAME            : Twrp0600.java
**        * CLASS NAME           : Twrp0600
**        * INSTANCE NAME        : Twrp0600
************************************************************
************************************************************************
*
* PROGRAM  : TWRP0600
* SYSTEM   : TAX - NEW TAX SYSTEM.
* TITLE    : UPDATES THE DATE CONTROL FILES.
* FUNCTION : TO CREATE DATE CONTROL FILES.  TO BE USED IN LOADING, AND
*            UPDATING PAYMENTS DATA.
*
* INPUT    : FILE 098 (TAX CONTROL FILE)
* OUTPUT   : 1) CONTROL RECORD BY ORIGIN CODE & FROM/TO DATES
*               - WILL CONTAIN THE LATEST DATE FOR WHICH PAYMENTS
*               WERE ADDED FOR AN ORIGIN CODE(DC,DS,MS,NZ,SS)
*               IN THE TAX CONTROL FILE.
*            2) CONTROL REPORT
*
* UPDATES  : USE INTERFACE-DATE FIELD FOR  THE LATEST DATE.
*            WHERE THE FIRST RECORD FOR SPECIFIED SOURCE-CODE ,
*            WITH BLANK COMPANY CODE, CONTAINS THE LATEST DATE
*
* HISTORY  :
* 01/04/04   M. SUPONITSKY
*            FOR QUARTERLY LOAN DEFAULTS JOB BYPASS WEEKEND TEST AND
*            LET JOB RUN ON THE WEEKEND.
* 06/11/03   F. ORTIZ
*            ADD NEW SOURCE CODE SI TO DAILY PROCESSING FOR
*            INVESTMENT SOLUTIONS PROJECT.
* 03/31/06   J. ROTHOLZ
*            ADD NEW SOURCE CODES "OP" AND "NV" TO DAILY PROCESSING
*            (FORMERLY ANNUAL PROCESSES).
* 09/20/06   A. YOUNG
*            ADD NEW SOURCE CODE "VL" FOR VARIABLE UNIVERSAL LIFE (VUL).
* 04/05/07   A. YOUNG
*            REVISED SOURCE CODE "VL" FROM ANNUAL TO MONTHLY PROCESSING.
* 09/15/11   M. BERLIN
*            ADD NEW SOURCE CODE "AM" FOR MCCAMISH FEED.
*            MODELLED FROM "NV"    /* 09/15/11
* 01/17/12   RSALGADO
*            ALLOW WEEK-END PROCESS IF IN ANY TEST ENVIRONMENT
* 04/11/18   'VL'ADDED WITH TWRT-SOURCE'AM' FOR DAILY PROCESS- VIKRAM2
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp0600 extends BLNatBase
{
    // Data Areas
    private LdaTwrl0600 ldaTwrl0600;
    private PdaTwra0001 pdaTwra0001;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Alpha_Date;

    private DbsGroup pnd_Alpha_Date__R_Field_1;
    private DbsField pnd_Alpha_Date_Pnd_Current_Year;
    private DbsField pnd_Alpha_Date_Pnd_Curent_Month;
    private DbsField pnd_Alpha_Date_Pnd_First_Day;
    private DbsField pnd_End_Of_Last_Month;
    private DbsField pnd_Work_Year;
    private DbsField pnd_Ws_Remainder;
    private DbsField pnd_Work_Ccyymmdd;

    private DbsGroup pnd_Work_Ccyymmdd__R_Field_2;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A;

    private DbsGroup pnd_Work_Ccyymmdd__R_Field_3;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Ccyy;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Mm;
    private DbsField pnd_Work_Ccyymmdd_Pnd_Work_Dd;
    private DbsField pnd_Datn;

    private DbsGroup pnd_Datn__R_Field_4;
    private DbsField pnd_Datn_Pnd_Datn_Year_Ccyy;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Day_Number;
    private DbsField pnd_System_Date;
    private DbsField i;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl0600 = new LdaTwrl0600();
        registerRecord(ldaTwrl0600);
        localVariables = new DbsRecord();
        pdaTwra0001 = new PdaTwra0001(localVariables);

        // Local Variables
        pnd_Alpha_Date = localVariables.newFieldInRecord("pnd_Alpha_Date", "#ALPHA-DATE", FieldType.STRING, 8);

        pnd_Alpha_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Alpha_Date__R_Field_1", "REDEFINE", pnd_Alpha_Date);
        pnd_Alpha_Date_Pnd_Current_Year = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_Current_Year", "#CURRENT-YEAR", FieldType.STRING, 
            4);
        pnd_Alpha_Date_Pnd_Curent_Month = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_Curent_Month", "#CURENT-MONTH", FieldType.STRING, 
            2);
        pnd_Alpha_Date_Pnd_First_Day = pnd_Alpha_Date__R_Field_1.newFieldInGroup("pnd_Alpha_Date_Pnd_First_Day", "#FIRST-DAY", FieldType.STRING, 2);
        pnd_End_Of_Last_Month = localVariables.newFieldInRecord("pnd_End_Of_Last_Month", "#END-OF-LAST-MONTH", FieldType.DATE);
        pnd_Work_Year = localVariables.newFieldInRecord("pnd_Work_Year", "#WORK-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Remainder = localVariables.newFieldInRecord("pnd_Ws_Remainder", "#WS-REMAINDER", FieldType.NUMERIC, 2);
        pnd_Work_Ccyymmdd = localVariables.newFieldInRecord("pnd_Work_Ccyymmdd", "#WORK-CCYYMMDD", FieldType.STRING, 8);

        pnd_Work_Ccyymmdd__R_Field_2 = localVariables.newGroupInRecord("pnd_Work_Ccyymmdd__R_Field_2", "REDEFINE", pnd_Work_Ccyymmdd);
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A", "#WORK-CCYY-A", FieldType.STRING, 
            4);

        pnd_Work_Ccyymmdd__R_Field_3 = pnd_Work_Ccyymmdd__R_Field_2.newGroupInGroup("pnd_Work_Ccyymmdd__R_Field_3", "REDEFINE", pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A);
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy = pnd_Work_Ccyymmdd__R_Field_3.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Ccyy", "#WORK-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Work_Ccyymmdd_Pnd_Work_Mm = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Mm", "#WORK-MM", FieldType.NUMERIC, 2);
        pnd_Work_Ccyymmdd_Pnd_Work_Dd = pnd_Work_Ccyymmdd__R_Field_2.newFieldInGroup("pnd_Work_Ccyymmdd_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Datn = localVariables.newFieldInRecord("pnd_Datn", "#DATN", FieldType.STRING, 8);

        pnd_Datn__R_Field_4 = localVariables.newGroupInRecord("pnd_Datn__R_Field_4", "REDEFINE", pnd_Datn);
        pnd_Datn_Pnd_Datn_Year_Ccyy = pnd_Datn__R_Field_4.newFieldInGroup("pnd_Datn_Pnd_Datn_Year_Ccyy", "#DATN-YEAR-CCYY", FieldType.STRING, 4);
        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 4);
        pnd_Day_Number = localVariables.newFieldInRecord("pnd_Day_Number", "#DAY-NUMBER", FieldType.STRING, 1);
        pnd_System_Date = localVariables.newFieldInRecord("pnd_System_Date", "#SYSTEM-DATE", FieldType.DATE);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl0600.initializeValues();

        localVariables.reset();
        pnd_Work_Year.setInitialValue(0);
        pnd_Ws_Remainder.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp0600() throws Exception
    {
        super("Twrp0600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *============**
        //*  MAIN PROGRAM *
        //* *============**
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #TWRP0600-CONTROL-RECORD
        while (condition(getWorkFiles().read(1, ldaTwrl0600.getPnd_Twrp0600_Control_Record())))
        {
            if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GS")))                                                            //Natural: IF #TWRP0600-ORIGIN-CODE = 'GS'
            {
                if (condition(Global.getINIT_USER().equals("P1965TWQ")))                                                                                                  //Natural: IF *INIT-USER = 'P1965TWQ'
                {
                    ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().setValue("GQ");                                                                 //Natural: ASSIGN #TWRP0600-ORIGIN-CODE := 'GQ'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(Global.getINIT_USER().equals("P2065TWD")))                                                                                              //Natural: IF *INIT-USER = 'P2065TWD'
                    {
                        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().setValue("GD");                                                             //Natural: ASSIGN #TWRP0600-ORIGIN-CODE := 'GD'
                        //*   'P1765TWR'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().setValue("GY");                                                             //Natural: ASSIGN #TWRP0600-ORIGIN-CODE := 'GY'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Ctr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CTR
            getReports().print(0, "BEFORE",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                 //Natural: PRINT 'BEFORE' #TWRP0600-CONTROL-RECORD
            //*  09/20/06
            //*  09/15/11
            if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AP") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("CP")  //Natural: IF #TWRP0600-ORIGIN-CODE = 'AP' OR = 'CP' OR = 'DC' OR = 'DS' OR = 'EW' OR = 'GD' OR = 'IA' OR = 'MS' OR = 'NZ' OR = 'SS' OR = 'GQ' OR = 'IP' OR = 'IS' OR = 'GY' OR = 'SI' OR = 'OP' OR = 'NV' OR = 'VL' OR = 'AM'
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("DC") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("DS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("EW") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GD") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IA") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("MS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NZ") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("SS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GQ") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IS") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GY") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("SI") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("OP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NV") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AM")))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  09/15/11
                //*  09/20/06
                getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Source Code",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid Source Codes","(AP/CP/DC/DS/EW/GS/IA/MS/NV/NZ/OP/SS/SI/IP/IS/AM)",NEWLINE,"***",new  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Source Code' 77T '***' / '***' 06T 'Valid Source Codes' '(AP/CP/DC/DS/EW/GS/IA/MS/NV/NZ/OP/SS/SI/IP/IS/AM)' / '***' 25T '(VL)' 72T #TWRP0600-ORIGIN-CODE 77T '***'
                    TabSetting(25),"(VL)",new TabSetting(72),ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(90);  if (true) return;                                                                                                                 //Natural: TERMINATE 90
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST #TWRP0600-ORIGIN-CODE
            short decideConditionsMet145 = 0;                                                                                                                             //Natural: VALUE 'DC', 'DS', 'EW', 'MS', 'NZ', 'SS', 'GD', 'SI', 'NV', 'OP','AM','VL'
            if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("DC") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("DS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("EW") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("MS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NZ") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("SS") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GD") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("SI") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("NV") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("OP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AM") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL"))))
            {
                decideConditionsMet145++;
                //*  04/05/07
                                                                                                                                                                          //Natural: PERFORM PROCESS-DAILY
                sub_Process_Daily();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'AP', 'CP', 'IA', 'IP', 'IS', 'VL'
            else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("AP") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("CP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IA") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IP") 
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IS") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL"))))
            {
                decideConditionsMet145++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-MONTHLY
                sub_Process_Monthly();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'GQ'
            else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GQ"))))
            {
                decideConditionsMet145++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-QUARTERLY
                sub_Process_Quarterly();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  09/20/06 - 'VL' WILL RUN AS ANNUAL IN 01/2007 ONLY.  AFTER THE INITIAL
                //*             RUN, CHANGE TO MONTHLY PROCESSING.
            }                                                                                                                                                             //Natural: VALUE 'GY'
            else if (condition((ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GY"))))
            {
                decideConditionsMet145++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-ANNUAL
                sub_Process_Annual();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  09/15/11
                getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Source Code",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Valid Source Codes","(AP/CP/DC/DS/EW/IA/MS/NV/NZ/OP/SS/SI/IP/IS/AM)",ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code(),new  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Source Code' 77T '***' / '***' 06T 'Valid Source Codes' '(AP/CP/DC/DS/EW/IA/MS/NV/NZ/OP/SS/SI/IP/IS/AM)' #TWRP0600-ORIGIN-CODE 77T '***'
                    TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(91);  if (true) return;                                                                                                                 //Natural: TERMINATE 91
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GD") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GQ")  //Natural: IF #TWRP0600-ORIGIN-CODE = 'GD' OR = 'GQ' OR = 'GY'
                || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GY")))
            {
                ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().setValue("GS");                                                                     //Natural: ASSIGN #TWRP0600-ORIGIN-CODE := 'GS'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                 //Natural: WRITE WORK FILE 02 #TWRP0600-CONTROL-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  END-OF-PROGRAM PROCESSING
        //* *-------------------------
        getReports().print(0, "=",pnd_Read_Ctr);                                                                                                                          //Natural: PRINT '=' #READ-CTR
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *------------------------------
        //* *------------
        //* *--------------------------------
        //* *------------
        //* *----------------------------------
        //* *------------
        //* *-------------------------------
        //* *------------
        //* *------------
        //* *---------                                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 )
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Process_Daily() throws Exception                                                                                                                     //Natural: PROCESS-DAILY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        if (condition(Global.getINIT_USER().equals("P1000TWD") || Global.getINIT_USER().equals("P1029TWD")))                                                              //Natural: IF *INIT-USER = 'P1000TWD' OR = 'P1029TWD'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WEEKEND-TEST
            sub_Weekend_Test();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Datn);                                                                       //Natural: ASSIGN #TWRP0600-FROM-CCYYMMDD := #DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Datn);                                                                         //Natural: ASSIGN #TWRP0600-TO-CCYYMMDD := #DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Datn);                                                                  //Natural: ASSIGN #TWRP0600-INTERFACE-CCYYMMDD := #DATN
        if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals(pnd_Datn_Pnd_Datn_Year_Ccyy)))                                       //Natural: IF #TWRP0600-TAX-YEAR-CCYY = #DATN-YEAR-CCYY
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValue(pnd_Datn_Pnd_Datn_Year_Ccyy);                                                //Natural: ASSIGN #TWRP0600-TAX-YEAR-CCYY := #DATN-YEAR-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        getReports().print(0, "AFTER-",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: PRINT 'AFTER-' #TWRP0600-CONTROL-RECORD
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( 00 ) 1
    }
    private void sub_Process_Monthly() throws Exception                                                                                                                   //Natural: PROCESS-MONTHLY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
                                                                                                                                                                          //Natural: PERFORM WEEKEND-TEST
        sub_Weekend_Test();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Datn);                                                                  //Natural: ASSIGN #TWRP0600-INTERFACE-CCYYMMDD := #DATN
        pnd_Work_Ccyymmdd.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                              //Natural: ASSIGN #WORK-CCYYMMDD := #TWRP0600-FROM-CCYYMMDD
        pnd_Work_Ccyymmdd_Pnd_Work_Mm.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WORK-MM
        if (condition(pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(13)))                                                                                                          //Natural: IF #WORK-MM = 13
        {
            pnd_Work_Ccyymmdd_Pnd_Work_Mm.setValue(1);                                                                                                                    //Natural: ASSIGN #WORK-MM := 1
            pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WORK-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals(pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A)))                                 //Natural: IF #TWRP0600-TAX-YEAR-CCYY = #WORK-CCYY-A
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValue(pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A);                                          //Natural: ASSIGN #TWRP0600-TAX-YEAR-CCYY := #WORK-CCYY-A
        }                                                                                                                                                                 //Natural: END-IF
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                              //Natural: ASSIGN #TWRP0600-FROM-CCYYMMDD := #WORK-CCYYMMDD
        //*  3-9-2001 FRANK
                                                                                                                                                                          //Natural: PERFORM SET-LAST-DAY-OF-THE-MONTH
        sub_Set_Last_Day_Of_The_Month();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                                //Natural: ASSIGN #TWRP0600-TO-CCYYMMDD := #WORK-CCYYMMDD
        getReports().print(0, "AFTER-",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: PRINT 'AFTER-' #TWRP0600-CONTROL-RECORD
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( 00 ) 1
    }
    private void sub_Process_Quarterly() throws Exception                                                                                                                 //Natural: PROCESS-QUARTERLY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Datn);                                                                  //Natural: ASSIGN #TWRP0600-INTERFACE-CCYYMMDD := #DATN
        pnd_Work_Ccyymmdd.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                              //Natural: ASSIGN #WORK-CCYYMMDD := #TWRP0600-FROM-CCYYMMDD
        pnd_Work_Ccyymmdd_Pnd_Work_Mm.nadd(3);                                                                                                                            //Natural: ADD 3 TO #WORK-MM
        //* *IF #WORK-MM  =  13                                  /* 08-25-03 FRANK
        //* *#WORK-MM  :=  1
        if (condition(pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(15)))                                                                                                          //Natural: IF #WORK-MM = 15
        {
            pnd_Work_Ccyymmdd_Pnd_Work_Mm.setValue(3);                                                                                                                    //Natural: ASSIGN #WORK-MM := 3
            pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.nadd(1);                                                                                                                      //Natural: ADD 1 TO #WORK-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().equals(pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A)))                                 //Natural: IF #TWRP0600-TAX-YEAR-CCYY = #WORK-CCYY-A
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValue(pnd_Work_Ccyymmdd_Pnd_Work_Ccyy_A);                                          //Natural: ASSIGN #TWRP0600-TAX-YEAR-CCYY := #WORK-CCYY-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  3-9-2001 FRANK
                                                                                                                                                                          //Natural: PERFORM SET-LAST-DAY-OF-THE-MONTH
        sub_Set_Last_Day_Of_The_Month();
        if (condition(Global.isEscape())) {return;}
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                              //Natural: ASSIGN #TWRP0600-FROM-CCYYMMDD := #WORK-CCYYMMDD
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                                //Natural: ASSIGN #TWRP0600-TO-CCYYMMDD := #WORK-CCYYMMDD
        getReports().print(0, "AFTER-",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: PRINT 'AFTER-' #TWRP0600-CONTROL-RECORD
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( 00 ) 1
    }
    private void sub_Process_Annual() throws Exception                                                                                                                    //Natural: PROCESS-ANNUAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datn.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATN := *DATN
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Interface_Ccyymmdd().setValue(pnd_Datn);                                                                  //Natural: ASSIGN #TWRP0600-INTERFACE-CCYYMMDD := #DATN
        pnd_Work_Ccyymmdd.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd());                                                              //Natural: ASSIGN #WORK-CCYYMMDD := #TWRP0600-FROM-CCYYMMDD
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WORK-CCYY
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_From_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                              //Natural: ASSIGN #TWRP0600-FROM-CCYYMMDD := #WORK-CCYYMMDD
        pnd_Work_Ccyymmdd.setValue(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd());                                                                //Natural: ASSIGN #WORK-CCYYMMDD := #TWRP0600-TO-CCYYMMDD
        pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WORK-CCYY
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_To_Ccyymmdd().setValue(pnd_Work_Ccyymmdd);                                                                //Natural: ASSIGN #TWRP0600-TO-CCYYMMDD := #WORK-CCYYMMDD
        ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Tax_Year_Ccyy().setValue(pnd_Work_Ccyymmdd_Pnd_Work_Ccyy);                                                //Natural: ASSIGN #TWRP0600-TAX-YEAR-CCYY := #WORK-CCYY
        getReports().print(0, "AFTER-",ldaTwrl0600.getPnd_Twrp0600_Control_Record());                                                                                     //Natural: PRINT 'AFTER-' #TWRP0600-CONTROL-RECORD
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( 00 ) 1
    }
    private void sub_Weekend_Test() throws Exception                                                                                                                      //Natural: WEEKEND-TEST
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        //*  DETERMINE IF 'PROD' OR 'TEST' ENVIRONMENT      /* RS012012 START
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"- Calling Sub-Program 'TWRN0001'...");                                                                                 //Natural: WRITE ( 00 ) *PROGRAM '- Calling Sub-Program "TWRN0001"...'
        if (Global.isEscape()) return;
        pdaTwra0001.getTwra0001().reset();                                                                                                                                //Natural: RESET TWRA0001
        DbsUtil.callnat(Twrn0001.class , getCurrentProcessState(), pdaTwra0001.getTwra0001());                                                                            //Natural: CALLNAT 'TWRN0001' TWRA0001
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Prod_Test());                                                                                               //Natural: WRITE ( 00 ) '=' TWRA0001.#PROD-TEST
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaTwra0001.getTwra0001_Pnd_Twrn0001_Return());                                                                                         //Natural: WRITE ( 00 ) '=' TWRA0001.#TWRN0001-RETURN
        if (Global.isEscape()) return;
        if (condition(pdaTwra0001.getTwra0001_Pnd_Prod_Test().equals("TEST")))                                                                                            //Natural: IF TWRA0001.#PROD-TEST = 'TEST'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                 /* RS012012 END
        pnd_System_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Datn);                                                                                          //Natural: MOVE EDITED #DATN TO #SYSTEM-DATE ( EM = YYYYMMDD )
        pnd_Day_Number.setValueEdited(pnd_System_Date,new ReportEditMask("O"));                                                                                           //Natural: MOVE EDITED #SYSTEM-DATE ( EM = O ) TO #DAY-NUMBER
        //*   SUNDAY  OR  SATURDAY
        if (condition(pnd_Day_Number.equals("1") || pnd_Day_Number.equals("7")))                                                                                          //Natural: IF #DAY-NUMBER = '1' OR = '7'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Control Record - Invalid Weekend Execution",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"Job Is Valid To Run Monday Thru Friday Only",pnd_Datn,  //Natural: WRITE ( 00 ) '***' 06T 'Control Record - Invalid Weekend Execution' 77T '***' / '***' 06T 'Job Is Valid To Run Monday Thru Friday Only' #DATN ( EM = XXXX/XX/XX ) 77T '***'
                new ReportEditMask ("XXXX/XX/XX"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Last_Day_Of_The_Month() throws Exception                                                                                                         //Natural: SET-LAST-DAY-OF-THE-MONTH
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        //*  04/05/07
        if (condition(ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("IS") || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("GQ")  //Natural: IF #TWRP0600-ORIGIN-CODE = 'IS' OR = 'GQ' OR = 'VL'
            || ldaTwrl0600.getPnd_Twrp0600_Control_Record_Pnd_Twrp0600_Origin_Code().equals("VL")))
        {
            if (condition(pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(1) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(3) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(5)                   //Natural: IF #WORK-MM = 01 OR = 03 OR = 05 OR = 07 OR = 08 OR = 10 OR = 12
                || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(7) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(8) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(10) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(12)))
            {
                pnd_Work_Ccyymmdd_Pnd_Work_Dd.setValue(31);                                                                                                               //Natural: ASSIGN #WORK-DD := 31
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(4) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(6) || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(9)               //Natural: IF #WORK-MM = 04 OR = 06 OR = 09 OR = 11
                    || pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(11)))
                {
                    pnd_Work_Ccyymmdd_Pnd_Work_Dd.setValue(30);                                                                                                           //Natural: ASSIGN #WORK-DD := 30
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_Ccyymmdd_Pnd_Work_Mm.equals(2)))                                                                                               //Natural: IF #WORK-MM = 02
                    {
                        pnd_Ws_Remainder.compute(new ComputeParameters(false, pnd_Ws_Remainder), pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.mod(4));                                 //Natural: DIVIDE 4 INTO #WORK-CCYY GIVING #WORK-YEAR REMAINDER #WS-REMAINDER
                        pnd_Work_Year.compute(new ComputeParameters(false, pnd_Work_Year), pnd_Work_Ccyymmdd_Pnd_Work_Ccyy.divide(4));
                        if (condition(pnd_Ws_Remainder.equals(getZero())))                                                                                                //Natural: IF #WS-REMAINDER = 0
                        {
                            pnd_Work_Ccyymmdd_Pnd_Work_Dd.setValue(29);                                                                                                   //Natural: ASSIGN #WORK-DD := 29
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_Ccyymmdd_Pnd_Work_Dd.setValue(28);                                                                                                   //Natural: ASSIGN #WORK-DD := 28
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(49),"Tax Withholding & Reporting System",new TabSetting(128),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 01 ) NOTITLE *PROGRAM 49T 'Tax Withholding & Reporting System' 128T *PAGE-NUMBER ( 01 ) ( EM = ZZZ9 ) / *DATX ( EM = LLL' 'DD','YY ) 49T '    Date Control File - Update    ' 124T *TIMX ( EM = HH':'II' 'AP ) / 49T '       Batch Control Report       ' ///
                        new ReportEditMask ("ZZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new TabSetting(49),"    Date Control File - Update    ",new 
                        TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(49),"       Batch Control Report       ",
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");
    }
}
