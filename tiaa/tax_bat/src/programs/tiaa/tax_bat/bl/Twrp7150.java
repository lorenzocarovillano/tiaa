/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:19 PM
**        * FROM NATURAL PROGRAM : Twrp7150
************************************************************
**        * FILE NAME            : Twrp7150.java
**        * CLASS NAME           : Twrp7150
**        * INSTANCE NAME        : Twrp7150
************************************************************
************************************************************************
**                                                                    **
** PROGRAM:  TWRP7150 - TAXWARRS EXTRACT FOR MDM  - 5498
** SYSTEM :  TAX WITHHOLDING & REPORTING SYSTEM
** AUTHOR :  ROXAN CARREON
** DATE   :  MAY 27, 2016
** PURPOSE:  EXTRACT FILE WILL BE SENT TO MDM AND WILL BE RETURNED  TO
**           TAXWARS TO UPDATE THE PARTICIPANT FILE
**           JOB WILL RUN BEFORE P1480TWR
**
**  11/24/2020 - VIKRAM  RESTOW FOR IRS REPORTING 2020 CHANGES
************************************************************************

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp7150 extends BLNatBase
{
    // Data Areas
    private LdaTwrl9615 ldaTwrl9615;
    private LdaTwrl462a ldaTwrl462a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Count;
    private DbsField pnd_Tax_Year;

    private DbsGroup pnd_Tax_Year__R_Field_1;
    private DbsField pnd_Tax_Year_Pnd_Tax_Year_A;
    private DbsField pnd_Save_Tin;
    private DbsField pnd_Save_Tin_Typ;
    private DbsField pnd_F1;
    private DbsField pnd_F2;
    private DbsField pnd_F3;
    private DbsField pnd_F4;
    private DbsField pnd_F5;
    private DbsField pnd_F6;
    private DbsField pnd_F7;
    private DbsField pnd_F8;
    private DbsField pnd_F9;
    private DbsField pnd_F10;
    private DbsField pnd_F11;
    private DbsField pnd_F12;
    private DbsField pnd_F13;
    private DbsField pnd_F14;
    private DbsField pnd_F15;
    private DbsField pnd_F16;
    private DbsField pnd_F17;
    private DbsField pnd_F18;
    private DbsField pnd_F19;
    private DbsField pnd_F20;
    private DbsField pnd_Omni_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaTwrl9615 = new LdaTwrl9615();
        registerRecord(ldaTwrl9615);
        registerRecord(ldaTwrl9615.getVw_twr_Ira());
        ldaTwrl462a = new LdaTwrl462a();
        registerRecord(ldaTwrl462a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Count = localVariables.newFieldInRecord("pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 11);
        pnd_Tax_Year = localVariables.newFieldInRecord("pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);

        pnd_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Tax_Year__R_Field_1", "REDEFINE", pnd_Tax_Year);
        pnd_Tax_Year_Pnd_Tax_Year_A = pnd_Tax_Year__R_Field_1.newFieldInGroup("pnd_Tax_Year_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 4);
        pnd_Save_Tin = localVariables.newFieldInRecord("pnd_Save_Tin", "#SAVE-TIN", FieldType.STRING, 9);
        pnd_Save_Tin_Typ = localVariables.newFieldInRecord("pnd_Save_Tin_Typ", "#SAVE-TIN-TYP", FieldType.STRING, 1);
        pnd_F1 = localVariables.newFieldInRecord("pnd_F1", "#F1", FieldType.STRING, 12);
        pnd_F2 = localVariables.newFieldInRecord("pnd_F2", "#F2", FieldType.STRING, 30);
        pnd_F3 = localVariables.newFieldInRecord("pnd_F3", "#F3", FieldType.STRING, 30);
        pnd_F4 = localVariables.newFieldInRecord("pnd_F4", "#F4", FieldType.STRING, 30);
        pnd_F5 = localVariables.newFieldInRecord("pnd_F5", "#F5", FieldType.STRING, 8);
        pnd_F6 = localVariables.newFieldInRecord("pnd_F6", "#F6", FieldType.STRING, 8);
        pnd_F7 = localVariables.newFieldInRecord("pnd_F7", "#F7", FieldType.STRING, 35);
        pnd_F8 = localVariables.newFieldInRecord("pnd_F8", "#F8", FieldType.STRING, 35);
        pnd_F9 = localVariables.newFieldInRecord("pnd_F9", "#F9", FieldType.STRING, 35);
        pnd_F10 = localVariables.newFieldInRecord("pnd_F10", "#F10", FieldType.STRING, 35);
        pnd_F11 = localVariables.newFieldInRecord("pnd_F11", "#F11", FieldType.STRING, 35);
        pnd_F12 = localVariables.newFieldInRecord("pnd_F12", "#F12", FieldType.STRING, 35);
        pnd_F13 = localVariables.newFieldInRecord("pnd_F13", "#F13", FieldType.STRING, 35);
        pnd_F14 = localVariables.newFieldInRecord("pnd_F14", "#F14", FieldType.STRING, 32);
        pnd_F15 = localVariables.newFieldInRecord("pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_F16 = localVariables.newFieldInRecord("pnd_F16", "#F16", FieldType.STRING, 2);
        pnd_F17 = localVariables.newFieldInRecord("pnd_F17", "#F17", FieldType.STRING, 1);
        pnd_F18 = localVariables.newFieldInRecord("pnd_F18", "#F18", FieldType.STRING, 8);
        pnd_F19 = localVariables.newFieldInRecord("pnd_F19", "#F19", FieldType.STRING, 70);
        pnd_F20 = localVariables.newFieldInRecord("pnd_F20", "#F20", FieldType.STRING, 8);
        pnd_Omni_Count = localVariables.newFieldInRecord("pnd_Omni_Count", "#OMNI-COUNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaTwrl9615.initializeValues();
        ldaTwrl462a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp7150() throws Exception
    {
        super("Twrp7150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp7150|Main");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Tax_Year);                                                                                         //Natural: INPUT #TAX-YEAR
                if (condition(pnd_Tax_Year.equals(getZero())))                                                                                                            //Natural: IF #TAX-YEAR = 0
                {
                    pnd_Tax_Year.compute(new ComputeParameters(false, pnd_Tax_Year), Global.getDATN().divide(10000).subtract(1));                                         //Natural: ASSIGN #TAX-YEAR := *DATN / 10000 - 1
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "PROCESSING TAX YEAR : ",pnd_Tax_Year);                                                                                             //Natural: WRITE 'PROCESSING TAX YEAR : ' #TAX-YEAR
                if (Global.isEscape()) return;
                //*    READ OMNI FILE
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 IRA-RECORD
                while (condition(getWorkFiles().read(1, ldaTwrl462a.getIra_Record())))
                {
                    pnd_Omni_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #OMNI-COUNT
                    getWorkFiles().write(2, false, ldaTwrl462a.getIra_Record_Ira_Tax_Year(), "~", ldaTwrl462a.getIra_Record_Ira_Tax_Id_Type(), "~", ldaTwrl462a.getIra_Record_Ira_Tax_Id(),  //Natural: WRITE WORK FILE 2 IRA-TAX-YEAR '~' IRA-TAX-ID-TYPE '~' IRA-TAX-ID '~' IRA-CONTRACT '~' IRA-PAYEE '~' #F1 '~' #F2 '~' #F3 '~' #F4 '~' #F5 '~' #F6 '~' #F7 '~' #F8 '~' #F9 '~' #F10 '~' #F11 '~' #F12 '~' #F13 '~' #F14 '~' #F15 '~' #F16 '~' #F17 '~' #F18 '~' #F19 '~' #F20 '~'
                        "~", ldaTwrl462a.getIra_Record_Ira_Contract(), "~", ldaTwrl462a.getIra_Record_Ira_Payee(), "~", pnd_F1, "~", pnd_F2, "~", pnd_F3, 
                        "~", pnd_F4, "~", pnd_F5, "~", pnd_F6, "~", pnd_F7, "~", pnd_F8, "~", pnd_F9, "~", pnd_F10, "~", pnd_F11, "~", pnd_F12, "~", pnd_F13, 
                        "~", pnd_F14, "~", pnd_F15, "~", pnd_F16, "~", pnd_F17, "~", pnd_F18, "~", pnd_F19, "~", pnd_F20, "~");
                    //*    DISPLAY
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                ldaTwrl9615.getVw_twr_Ira().startDatabaseRead                                                                                                             //Natural: READ TWR-IRA BY TWRC-S2-FORMS = #TAX-YEAR
                (
                "READ02",
                new Wc[] { new Wc("TWRC_S2_FORMS", ">=", pnd_Tax_Year, WcType.BY) },
                new Oc[] { new Oc("TWRC_S2_FORMS", "ASC") }
                );
                READ02:
                while (condition(ldaTwrl9615.getVw_twr_Ira().readNextRow("READ02")))
                {
                    if (condition(ldaTwrl9615.getTwr_Ira_Twrc_Tax_Year().notEquals(pnd_Tax_Year_Pnd_Tax_Year_A)))                                                         //Natural: IF TWRC-TAX-YEAR NE #TAX-YEAR-A
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Save_Tin.equals(ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id()) && pnd_Save_Tin_Typ.equals(ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id_Type())))       //Natural: IF #SAVE-TIN = TWRC-TAX-ID AND #SAVE-TIN-TYP = TWRC-TAX-ID-TYPE
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #READ-COUNT
                    pnd_Save_Tin.setValue(ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id());                                                                                          //Natural: ASSIGN #SAVE-TIN := TWRC-TAX-ID
                    pnd_Save_Tin_Typ.setValue(ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id_Type());                                                                                 //Natural: ASSIGN #SAVE-TIN-TYP := TWRC-TAX-ID-TYPE
                    getWorkFiles().write(2, false, ldaTwrl9615.getTwr_Ira_Twrc_Tax_Year(), "~", ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id_Type(), "~", ldaTwrl9615.getTwr_Ira_Twrc_Tax_Id(),  //Natural: WRITE WORK FILE 2 TWRC-TAX-YEAR '~' TWRC-TAX-ID-TYPE '~' TWRC-TAX-ID '~' TWRC-CONTRACT '~' TWRC-PAYEE '~' #F1 '~' #F2 '~' #F3 '~' #F4 '~' #F5 '~' #F6 '~' #F7 '~' #F8 '~' #F9 '~' #F10 '~' #F11 '~' #F12 '~' #F13 '~' #F14 '~' #F15 '~' #F16 '~' #F17 '~' #F18 '~' #F19 '~' #F20 '~'
                        "~", ldaTwrl9615.getTwr_Ira_Twrc_Contract(), "~", ldaTwrl9615.getTwr_Ira_Twrc_Payee(), "~", pnd_F1, "~", pnd_F2, "~", pnd_F3, "~", 
                        pnd_F4, "~", pnd_F5, "~", pnd_F6, "~", pnd_F7, "~", pnd_F8, "~", pnd_F9, "~", pnd_F10, "~", pnd_F11, "~", pnd_F12, "~", pnd_F13, 
                        "~", pnd_F14, "~", pnd_F15, "~", pnd_F16, "~", pnd_F17, "~", pnd_F18, "~", pnd_F19, "~", pnd_F20, "~");
                    //*    DISPLAY
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().write(0, "NUMBER OF PARTICIPANTS ",pnd_Read_Count);                                                                                          //Natural: WRITE 'NUMBER OF PARTICIPANTS ' #READ-COUNT
                if (Global.isEscape()) return;
                getReports().write(0, "OMNI RECORDS READ ",pnd_Omni_Count);                                                                                               //Natural: WRITE 'OMNI RECORDS READ ' #OMNI-COUNT
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //
}
