/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:44:34 PM
**        * FROM NATURAL PROGRAM : Twrp8130
************************************************************
**        * FILE NAME            : Twrp8130.java
**        * CLASS NAME           : Twrp8130
**        * INSTANCE NAME        : Twrp8130
************************************************************
************************************************************************
* PROGRAM     :  TWRP8130
* SYSTEM      :  TAX WITHHOLDINGS AND REPORTING SYSTEM.
* AUTHOR      :  RIAD A. LOUTFI
* PURPOSE     :  CREATE A TABLE OF NEW TAX YEAR DISTRIBUTION CODES FROM
*                AN OLD TAX YEAR DISTRIBUTION CODES TABLE.
*                TABLE NUMBER 1
* DATE        :  09/14/2000
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp8130 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rdist;
    private DbsField rdist_Tircntl_Tbl_Nbr;
    private DbsField rdist_Tircntl_Tax_Year;
    private DbsField rdist_Tircntl_Seq_Nbr;

    private DbsGroup rdist_Tircntl_Dist_Code_Tbl;
    private DbsField rdist_Tircntl_Type_Refer;
    private DbsField rdist_Tircntl_Settl_Type;
    private DbsField rdist_Tircntl_Paymt_Type;
    private DbsField rdist_Tircntl_Settl_Paym_Descr;
    private DbsField rdist_Tircntl_Over59_Age_Ind;
    private DbsField rdist_Tircntl_Dist_Code;
    private DbsField rdist_Tircntl_Dist_Code_Descr;
    private DbsField rdist_Tircntl_Dist_Method;
    private DbsField rdist_Tircntl_Paymt_Category;

    private DataAccessProgramView vw_udist;
    private DbsField udist_Tircntl_Tbl_Nbr;
    private DbsField udist_Tircntl_Tax_Year;
    private DbsField udist_Tircntl_Seq_Nbr;

    private DbsGroup udist_Tircntl_Dist_Code_Tbl;
    private DbsField udist_Tircntl_Type_Refer;
    private DbsField udist_Tircntl_Settl_Type;
    private DbsField udist_Tircntl_Paymt_Type;
    private DbsField udist_Tircntl_Settl_Paym_Descr;
    private DbsField udist_Tircntl_Over59_Age_Ind;
    private DbsField udist_Tircntl_Dist_Code;
    private DbsField udist_Tircntl_Dist_Code_Descr;
    private DbsField udist_Tircntl_Dist_Method;
    private DbsField udist_Tircntl_Paymt_Category;

    private DataAccessProgramView vw_sdist;
    private DbsField sdist_Tircntl_Tbl_Nbr;
    private DbsField sdist_Tircntl_Tax_Year;
    private DbsField sdist_Tircntl_Seq_Nbr;

    private DbsGroup sdist_Tircntl_Dist_Code_Tbl;
    private DbsField sdist_Tircntl_Type_Refer;
    private DbsField sdist_Tircntl_Settl_Type;
    private DbsField sdist_Tircntl_Paymt_Type;
    private DbsField sdist_Tircntl_Settl_Paym_Descr;
    private DbsField sdist_Tircntl_Over59_Age_Ind;
    private DbsField sdist_Tircntl_Dist_Code;
    private DbsField sdist_Tircntl_Dist_Code_Descr;
    private DbsField sdist_Tircntl_Dist_Method;
    private DbsField sdist_Tircntl_Paymt_Category;
    private DbsField pnd_Read1_Ctr;
    private DbsField pnd_Read2_Ctr;
    private DbsField pnd_Updt_Ctr;
    private DbsField pnd_Store_Ctr;
    private DbsField pnd_Old_Tax_Year;

    private DbsGroup pnd_Old_Tax_Year__R_Field_1;
    private DbsField pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N;
    private DbsField pnd_New_Tax_Year;

    private DbsGroup pnd_New_Tax_Year__R_Field_2;
    private DbsField pnd_New_Tax_Year_Pnd_New_Tax_Year_N;
    private DbsField pnd_Super1;

    private DbsGroup pnd_Super1__R_Field_3;
    private DbsField pnd_Super1_Pnd_S1_Tbl_Nbr;
    private DbsField pnd_Super1_Pnd_S1_Tax_Year;
    private DbsField pnd_Super1_Pnd_S1_Settle_Type;
    private DbsField pnd_Super1_Pnd_S1_Pay_Type;
    private DbsField pnd_Super1_Pnd_S1_Dist_Code;
    private DbsField pnd_Super2;

    private DbsGroup pnd_Super2__R_Field_4;
    private DbsField pnd_Super2_Pnd_S2_Tbl_Nbr;
    private DbsField pnd_Super2_Pnd_S2_Tax_Year;
    private DbsField pnd_Super2_Pnd_S2_Settle_Type;
    private DbsField pnd_Super2_Pnd_S2_Pay_Type;
    private DbsField pnd_Super2_Pnd_S2_Dist_Code;
    private DbsField pnd_New_Record_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_rdist = new DataAccessProgramView(new NameInfo("vw_rdist", "RDIST"), "TIRCNTL_DIST_CODE_TBL_VIEW", "TIR_CONTROL");
        rdist_Tircntl_Tbl_Nbr = vw_rdist.getRecord().newFieldInGroup("rdist_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        rdist_Tircntl_Tax_Year = vw_rdist.getRecord().newFieldInGroup("rdist_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        rdist_Tircntl_Seq_Nbr = vw_rdist.getRecord().newFieldInGroup("rdist_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        rdist_Tircntl_Dist_Code_Tbl = vw_rdist.getRecord().newGroupInGroup("RDIST_TIRCNTL_DIST_CODE_TBL", "TIRCNTL-DIST-CODE-TBL");
        rdist_Tircntl_Type_Refer = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Type_Refer", "TIRCNTL-TYPE-REFER", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TIRCNTL_TYPE_REFER");
        rdist_Tircntl_Settl_Type = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Settl_Type", "TIRCNTL-SETTL-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_SETTL_TYPE");
        rdist_Tircntl_Paymt_Type = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Paymt_Type", "TIRCNTL-PAYMT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_TYPE");
        rdist_Tircntl_Settl_Paym_Descr = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Settl_Paym_Descr", "TIRCNTL-SETTL-PAYM-DESCR", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "TIRCNTL_SETTL_PAYM_DESCR");
        rdist_Tircntl_Over59_Age_Ind = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Over59_Age_Ind", "TIRCNTL-OVER59-AGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_OVER59_AGE_IND");
        rdist_Tircntl_Dist_Code = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Dist_Code", "TIRCNTL-DIST-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_DIST_CODE");
        rdist_Tircntl_Dist_Code_Descr = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Dist_Code_Descr", "TIRCNTL-DIST-CODE-DESCR", FieldType.STRING, 
            70, RepeatingFieldStrategy.None, "TIRCNTL_DIST_CODE_DESCR");
        rdist_Tircntl_Dist_Method = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Dist_Method", "TIRCNTL-DIST-METHOD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_DIST_METHOD");
        rdist_Tircntl_Paymt_Category = rdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("rdist_Tircntl_Paymt_Category", "TIRCNTL-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_CATEGORY");
        registerRecord(vw_rdist);

        vw_udist = new DataAccessProgramView(new NameInfo("vw_udist", "UDIST"), "TIRCNTL_DIST_CODE_TBL_VIEW", "TIR_CONTROL");
        udist_Tircntl_Tbl_Nbr = vw_udist.getRecord().newFieldInGroup("udist_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        udist_Tircntl_Tax_Year = vw_udist.getRecord().newFieldInGroup("udist_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        udist_Tircntl_Seq_Nbr = vw_udist.getRecord().newFieldInGroup("udist_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        udist_Tircntl_Dist_Code_Tbl = vw_udist.getRecord().newGroupInGroup("UDIST_TIRCNTL_DIST_CODE_TBL", "TIRCNTL-DIST-CODE-TBL");
        udist_Tircntl_Type_Refer = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Type_Refer", "TIRCNTL-TYPE-REFER", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TIRCNTL_TYPE_REFER");
        udist_Tircntl_Settl_Type = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Settl_Type", "TIRCNTL-SETTL-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_SETTL_TYPE");
        udist_Tircntl_Paymt_Type = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Paymt_Type", "TIRCNTL-PAYMT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_TYPE");
        udist_Tircntl_Settl_Paym_Descr = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Settl_Paym_Descr", "TIRCNTL-SETTL-PAYM-DESCR", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "TIRCNTL_SETTL_PAYM_DESCR");
        udist_Tircntl_Over59_Age_Ind = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Over59_Age_Ind", "TIRCNTL-OVER59-AGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_OVER59_AGE_IND");
        udist_Tircntl_Dist_Code = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Dist_Code", "TIRCNTL-DIST-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_DIST_CODE");
        udist_Tircntl_Dist_Code_Descr = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Dist_Code_Descr", "TIRCNTL-DIST-CODE-DESCR", FieldType.STRING, 
            70, RepeatingFieldStrategy.None, "TIRCNTL_DIST_CODE_DESCR");
        udist_Tircntl_Dist_Method = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Dist_Method", "TIRCNTL-DIST-METHOD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_DIST_METHOD");
        udist_Tircntl_Paymt_Category = udist_Tircntl_Dist_Code_Tbl.newFieldInGroup("udist_Tircntl_Paymt_Category", "TIRCNTL-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_CATEGORY");
        registerRecord(vw_udist);

        vw_sdist = new DataAccessProgramView(new NameInfo("vw_sdist", "SDIST"), "TIRCNTL_DIST_CODE_TBL_VIEW", "TIR_CONTROL");
        sdist_Tircntl_Tbl_Nbr = vw_sdist.getRecord().newFieldInGroup("sdist_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        sdist_Tircntl_Tax_Year = vw_sdist.getRecord().newFieldInGroup("sdist_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        sdist_Tircntl_Seq_Nbr = vw_sdist.getRecord().newFieldInGroup("sdist_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");

        sdist_Tircntl_Dist_Code_Tbl = vw_sdist.getRecord().newGroupInGroup("SDIST_TIRCNTL_DIST_CODE_TBL", "TIRCNTL-DIST-CODE-TBL");
        sdist_Tircntl_Type_Refer = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Type_Refer", "TIRCNTL-TYPE-REFER", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TIRCNTL_TYPE_REFER");
        sdist_Tircntl_Settl_Type = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Settl_Type", "TIRCNTL-SETTL-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_SETTL_TYPE");
        sdist_Tircntl_Paymt_Type = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Paymt_Type", "TIRCNTL-PAYMT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_TYPE");
        sdist_Tircntl_Settl_Paym_Descr = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Settl_Paym_Descr", "TIRCNTL-SETTL-PAYM-DESCR", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "TIRCNTL_SETTL_PAYM_DESCR");
        sdist_Tircntl_Over59_Age_Ind = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Over59_Age_Ind", "TIRCNTL-OVER59-AGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_OVER59_AGE_IND");
        sdist_Tircntl_Dist_Code = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Dist_Code", "TIRCNTL-DIST-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_DIST_CODE");
        sdist_Tircntl_Dist_Code_Descr = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Dist_Code_Descr", "TIRCNTL-DIST-CODE-DESCR", FieldType.STRING, 
            70, RepeatingFieldStrategy.None, "TIRCNTL_DIST_CODE_DESCR");
        sdist_Tircntl_Dist_Method = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Dist_Method", "TIRCNTL-DIST-METHOD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_DIST_METHOD");
        sdist_Tircntl_Paymt_Category = sdist_Tircntl_Dist_Code_Tbl.newFieldInGroup("sdist_Tircntl_Paymt_Category", "TIRCNTL-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TIRCNTL_PAYMT_CATEGORY");
        registerRecord(vw_sdist);

        pnd_Read1_Ctr = localVariables.newFieldInRecord("pnd_Read1_Ctr", "#READ1-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Read2_Ctr = localVariables.newFieldInRecord("pnd_Read2_Ctr", "#READ2-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Updt_Ctr = localVariables.newFieldInRecord("pnd_Updt_Ctr", "#UPDT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Store_Ctr = localVariables.newFieldInRecord("pnd_Store_Ctr", "#STORE-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Tax_Year = localVariables.newFieldInRecord("pnd_Old_Tax_Year", "#OLD-TAX-YEAR", FieldType.STRING, 4);

        pnd_Old_Tax_Year__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Tax_Year__R_Field_1", "REDEFINE", pnd_Old_Tax_Year);
        pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N = pnd_Old_Tax_Year__R_Field_1.newFieldInGroup("pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N", "#OLD-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_New_Tax_Year = localVariables.newFieldInRecord("pnd_New_Tax_Year", "#NEW-TAX-YEAR", FieldType.STRING, 4);

        pnd_New_Tax_Year__R_Field_2 = localVariables.newGroupInRecord("pnd_New_Tax_Year__R_Field_2", "REDEFINE", pnd_New_Tax_Year);
        pnd_New_Tax_Year_Pnd_New_Tax_Year_N = pnd_New_Tax_Year__R_Field_2.newFieldInGroup("pnd_New_Tax_Year_Pnd_New_Tax_Year_N", "#NEW-TAX-YEAR-N", FieldType.NUMERIC, 
            4);
        pnd_Super1 = localVariables.newFieldInRecord("pnd_Super1", "#SUPER1", FieldType.STRING, 8);

        pnd_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Super1__R_Field_3", "REDEFINE", pnd_Super1);
        pnd_Super1_Pnd_S1_Tbl_Nbr = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tbl_Nbr", "#S1-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super1_Pnd_S1_Tax_Year = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Tax_Year", "#S1-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super1_Pnd_S1_Settle_Type = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Settle_Type", "#S1-SETTLE-TYPE", FieldType.STRING, 1);
        pnd_Super1_Pnd_S1_Pay_Type = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Pay_Type", "#S1-PAY-TYPE", FieldType.STRING, 1);
        pnd_Super1_Pnd_S1_Dist_Code = pnd_Super1__R_Field_3.newFieldInGroup("pnd_Super1_Pnd_S1_Dist_Code", "#S1-DIST-CODE", FieldType.STRING, 1);
        pnd_Super2 = localVariables.newFieldInRecord("pnd_Super2", "#SUPER2", FieldType.STRING, 8);

        pnd_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Super2__R_Field_4", "REDEFINE", pnd_Super2);
        pnd_Super2_Pnd_S2_Tbl_Nbr = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tbl_Nbr", "#S2-TBL-NBR", FieldType.NUMERIC, 1);
        pnd_Super2_Pnd_S2_Tax_Year = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Tax_Year", "#S2-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Super2_Pnd_S2_Settle_Type = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Settle_Type", "#S2-SETTLE-TYPE", FieldType.STRING, 1);
        pnd_Super2_Pnd_S2_Pay_Type = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Pay_Type", "#S2-PAY-TYPE", FieldType.STRING, 1);
        pnd_Super2_Pnd_S2_Dist_Code = pnd_Super2__R_Field_4.newFieldInGroup("pnd_Super2_Pnd_S2_Dist_Code", "#S2-DIST-CODE", FieldType.STRING, 1);
        pnd_New_Record_Found = localVariables.newFieldInRecord("pnd_New_Record_Found", "#NEW-RECORD-FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rdist.reset();
        vw_udist.reset();
        vw_sdist.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp8130() throws Exception
    {
        super("Twrp8130");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Twrp8130|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 60 LS = 133;//Natural: FORMAT ( 01 ) PS = 60 LS = 133
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                    //Natural: INPUT #OLD-TAX-YEAR #NEW-TAX-YEAR
                getReports().display(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);                                                                                               //Natural: DISPLAY ( 00 ) #OLD-TAX-YEAR #NEW-TAX-YEAR
                if (Global.isEscape()) return;
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(1);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 1
                pnd_Super1_Pnd_S1_Tax_Year.setValue(pnd_Old_Tax_Year_Pnd_Old_Tax_Year_N);                                                                                 //Natural: ASSIGN #S1-TAX-YEAR := #OLD-TAX-YEAR-N
                pnd_Super1_Pnd_S1_Settle_Type.setValue(" ");                                                                                                              //Natural: ASSIGN #S1-SETTLE-TYPE := ' '
                pnd_Super1_Pnd_S1_Pay_Type.setValue(" ");                                                                                                                 //Natural: ASSIGN #S1-PAY-TYPE := ' '
                pnd_Super1_Pnd_S1_Dist_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #S1-DIST-CODE := ' '
                vw_rdist.startDatabaseRead                                                                                                                                //Natural: READ RDIST WITH TIRCNTL-NBR-YEAR-DISTR-CODE = #SUPER1
                (
                "READ01",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") }
                );
                READ01:
                while (condition(vw_rdist.readNextRow("READ01")))
                {
                    if (condition(rdist_Tircntl_Tbl_Nbr.equals(pnd_Super1_Pnd_S1_Tbl_Nbr) && rdist_Tircntl_Tax_Year.equals(pnd_Super1_Pnd_S1_Tax_Year)))                  //Natural: IF RDIST.TIRCNTL-TBL-NBR = #S1-TBL-NBR AND RDIST.TIRCNTL-TAX-YEAR = #S1-TAX-YEAR
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read1_Ctr.nadd(1);                                                                                                                                //Natural: ADD 1 TO #READ1-CTR
                    pnd_New_Record_Found.setValue(false);                                                                                                                 //Natural: ASSIGN #NEW-RECORD-FOUND := FALSE
                    pnd_Super2_Pnd_S2_Tbl_Nbr.setValue(1);                                                                                                                //Natural: ASSIGN #S2-TBL-NBR := 1
                    pnd_Super2_Pnd_S2_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN #S2-TAX-YEAR := #NEW-TAX-YEAR-N
                    pnd_Super2_Pnd_S2_Settle_Type.setValue(rdist_Tircntl_Settl_Type);                                                                                     //Natural: ASSIGN #S2-SETTLE-TYPE := RDIST.TIRCNTL-SETTL-TYPE
                    pnd_Super2_Pnd_S2_Pay_Type.setValue(rdist_Tircntl_Paymt_Type);                                                                                        //Natural: ASSIGN #S2-PAY-TYPE := RDIST.TIRCNTL-PAYMT-TYPE
                    pnd_Super2_Pnd_S2_Dist_Code.setValue(rdist_Tircntl_Dist_Code);                                                                                        //Natural: ASSIGN #S2-DIST-CODE := RDIST.TIRCNTL-DIST-CODE
                    vw_udist.startDatabaseRead                                                                                                                            //Natural: READ UDIST WITH TIRCNTL-NBR-YEAR-DISTR-CODE = #SUPER2
                    (
                    "RL2",
                    new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Super2, WcType.BY) },
                    new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") }
                    );
                    RL2:
                    while (condition(vw_udist.readNextRow("RL2")))
                    {
                        if (condition(udist_Tircntl_Tbl_Nbr.equals(pnd_Super2_Pnd_S2_Tbl_Nbr) && udist_Tircntl_Tax_Year.equals(pnd_Super2_Pnd_S2_Tax_Year)                //Natural: IF UDIST.TIRCNTL-TBL-NBR = #S2-TBL-NBR AND UDIST.TIRCNTL-TAX-YEAR = #S2-TAX-YEAR AND UDIST.TIRCNTL-SETTL-TYPE = #S2-SETTLE-TYPE AND UDIST.TIRCNTL-PAYMT-TYPE = #S2-PAY-TYPE AND UDIST.TIRCNTL-DIST-CODE = #S2-DIST-CODE
                            && udist_Tircntl_Settl_Type.equals(pnd_Super2_Pnd_S2_Settle_Type) && udist_Tircntl_Paymt_Type.equals(pnd_Super2_Pnd_S2_Pay_Type) 
                            && udist_Tircntl_Dist_Code.equals(pnd_Super2_Pnd_S2_Dist_Code)))
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(udist_Tircntl_Type_Refer.equals(rdist_Tircntl_Type_Refer)))                                                                         //Natural: IF UDIST.TIRCNTL-TYPE-REFER = RDIST.TIRCNTL-TYPE-REFER
                        {
                            pnd_New_Record_Found.setValue(true);                                                                                                          //Natural: ASSIGN #NEW-RECORD-FOUND := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Read2_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #READ2-CTR
                        if (condition(udist_Tircntl_Seq_Nbr.equals(rdist_Tircntl_Seq_Nbr) && udist_Tircntl_Settl_Paym_Descr.equals(rdist_Tircntl_Settl_Paym_Descr)        //Natural: IF UDIST.TIRCNTL-SEQ-NBR = RDIST.TIRCNTL-SEQ-NBR AND UDIST.TIRCNTL-SETTL-PAYM-DESCR = RDIST.TIRCNTL-SETTL-PAYM-DESCR AND UDIST.TIRCNTL-OVER59-AGE-IND = RDIST.TIRCNTL-OVER59-AGE-IND AND UDIST.TIRCNTL-DIST-CODE-DESCR = RDIST.TIRCNTL-DIST-CODE-DESCR AND UDIST.TIRCNTL-DIST-METHOD = RDIST.TIRCNTL-DIST-METHOD AND UDIST.TIRCNTL-PAYMT-CATEGORY = RDIST.TIRCNTL-PAYMT-CATEGORY
                            && udist_Tircntl_Over59_Age_Ind.equals(rdist_Tircntl_Over59_Age_Ind) && udist_Tircntl_Dist_Code_Descr.equals(rdist_Tircntl_Dist_Code_Descr) 
                            && udist_Tircntl_Dist_Method.equals(rdist_Tircntl_Dist_Method) && udist_Tircntl_Paymt_Category.equals(rdist_Tircntl_Paymt_Category)))
                        {
                            //*   AND UDIST.TIRCNTL-TYPE-REFER        =  RDIST.TIRCNTL-TYPE-REFER
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            G1:                                                                                                                                           //Natural: GET UDIST *ISN ( RL2. )
                            vw_udist.readByID(vw_udist.getAstISN("RL2"), "G1");
                            udist_Tircntl_Seq_Nbr.setValue(rdist_Tircntl_Seq_Nbr);                                                                                        //Natural: ASSIGN UDIST.TIRCNTL-SEQ-NBR := RDIST.TIRCNTL-SEQ-NBR
                            udist_Tircntl_Type_Refer.setValue(rdist_Tircntl_Type_Refer);                                                                                  //Natural: ASSIGN UDIST.TIRCNTL-TYPE-REFER := RDIST.TIRCNTL-TYPE-REFER
                            udist_Tircntl_Settl_Paym_Descr.setValue(rdist_Tircntl_Settl_Paym_Descr);                                                                      //Natural: ASSIGN UDIST.TIRCNTL-SETTL-PAYM-DESCR := RDIST.TIRCNTL-SETTL-PAYM-DESCR
                            udist_Tircntl_Over59_Age_Ind.setValue(rdist_Tircntl_Over59_Age_Ind);                                                                          //Natural: ASSIGN UDIST.TIRCNTL-OVER59-AGE-IND := RDIST.TIRCNTL-OVER59-AGE-IND
                            udist_Tircntl_Dist_Code_Descr.setValue(rdist_Tircntl_Dist_Code_Descr);                                                                        //Natural: ASSIGN UDIST.TIRCNTL-DIST-CODE-DESCR := RDIST.TIRCNTL-DIST-CODE-DESCR
                            udist_Tircntl_Dist_Method.setValue(rdist_Tircntl_Dist_Method);                                                                                //Natural: ASSIGN UDIST.TIRCNTL-DIST-METHOD := RDIST.TIRCNTL-DIST-METHOD
                            udist_Tircntl_Paymt_Category.setValue(rdist_Tircntl_Paymt_Category);                                                                          //Natural: ASSIGN UDIST.TIRCNTL-PAYMT-CATEGORY := RDIST.TIRCNTL-PAYMT-CATEGORY
                            //*       UPDATE (G1.)
                            //*       END TRANSACTION
                            //*       ADD  1  TO  #UPDT-CTR
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (RL2.)
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_New_Record_Found.equals(false)))                                                                                                    //Natural: IF #NEW-RECORD-FOUND = FALSE
                    {
                        //*     PRINT '=' RDIST.TIRCNTL-TBL-NBR
                        //*     PRINT '=' #NEW-TAX-YEAR-N
                        //*     PRINT '=' RDIST.TIRCNTL-SEQ-NBR
                        //*     PRINT '=' RDIST.TIRCNTL-SETTL-TYPE
                        //*     PRINT '=' RDIST.TIRCNTL-PAYMT-TYPE
                        //*     PRINT '=' RDIST.TIRCNTL-DIST-CODE
                        //*     PRINT '=' RDIST.TIRCNTL-TYPE-REFER
                        //*     PRINT '=' RDIST.TIRCNTL-SETTL-PAYM-DESCR
                        //*     PRINT '=' RDIST.TIRCNTL-OVER59-AGE-IND
                        //*     PRINT '=' RDIST.TIRCNTL-DIST-CODE-DESCR
                        //*     PRINT '=' RDIST.TIRCNTL-DIST-METHOD
                        //*     PRINT '=' RDIST.TIRCNTL-PAYMT-CATEGORY
                        sdist_Tircntl_Tbl_Nbr.setValue(rdist_Tircntl_Tbl_Nbr);                                                                                            //Natural: ASSIGN SDIST.TIRCNTL-TBL-NBR := RDIST.TIRCNTL-TBL-NBR
                        sdist_Tircntl_Tax_Year.setValue(pnd_New_Tax_Year_Pnd_New_Tax_Year_N);                                                                             //Natural: ASSIGN SDIST.TIRCNTL-TAX-YEAR := #NEW-TAX-YEAR-N
                        sdist_Tircntl_Seq_Nbr.setValue(rdist_Tircntl_Seq_Nbr);                                                                                            //Natural: ASSIGN SDIST.TIRCNTL-SEQ-NBR := RDIST.TIRCNTL-SEQ-NBR
                        sdist_Tircntl_Settl_Type.setValue(rdist_Tircntl_Settl_Type);                                                                                      //Natural: ASSIGN SDIST.TIRCNTL-SETTL-TYPE := RDIST.TIRCNTL-SETTL-TYPE
                        sdist_Tircntl_Paymt_Type.setValue(rdist_Tircntl_Paymt_Type);                                                                                      //Natural: ASSIGN SDIST.TIRCNTL-PAYMT-TYPE := RDIST.TIRCNTL-PAYMT-TYPE
                        sdist_Tircntl_Dist_Code.setValue(rdist_Tircntl_Dist_Code);                                                                                        //Natural: ASSIGN SDIST.TIRCNTL-DIST-CODE := RDIST.TIRCNTL-DIST-CODE
                        sdist_Tircntl_Type_Refer.setValue(rdist_Tircntl_Type_Refer);                                                                                      //Natural: ASSIGN SDIST.TIRCNTL-TYPE-REFER := RDIST.TIRCNTL-TYPE-REFER
                        sdist_Tircntl_Settl_Paym_Descr.setValue(rdist_Tircntl_Settl_Paym_Descr);                                                                          //Natural: ASSIGN SDIST.TIRCNTL-SETTL-PAYM-DESCR := RDIST.TIRCNTL-SETTL-PAYM-DESCR
                        sdist_Tircntl_Over59_Age_Ind.setValue(rdist_Tircntl_Over59_Age_Ind);                                                                              //Natural: ASSIGN SDIST.TIRCNTL-OVER59-AGE-IND := RDIST.TIRCNTL-OVER59-AGE-IND
                        sdist_Tircntl_Dist_Code_Descr.setValue(rdist_Tircntl_Dist_Code_Descr);                                                                            //Natural: ASSIGN SDIST.TIRCNTL-DIST-CODE-DESCR := RDIST.TIRCNTL-DIST-CODE-DESCR
                        sdist_Tircntl_Dist_Method.setValue(rdist_Tircntl_Dist_Method);                                                                                    //Natural: ASSIGN SDIST.TIRCNTL-DIST-METHOD := RDIST.TIRCNTL-DIST-METHOD
                        sdist_Tircntl_Paymt_Category.setValue(rdist_Tircntl_Paymt_Category);                                                                              //Natural: ASSIGN SDIST.TIRCNTL-PAYMT-CATEGORY := RDIST.TIRCNTL-PAYMT-CATEGORY
                        vw_sdist.insertDBRow();                                                                                                                           //Natural: STORE SDIST
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Store_Ctr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #STORE-CTR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (RL1.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                pnd_Super1_Pnd_S1_Tbl_Nbr.setValue(1);                                                                                                                    //Natural: ASSIGN #S1-TBL-NBR := 1
                pnd_Super1_Pnd_S1_Tax_Year.setValue(0);                                                                                                                   //Natural: ASSIGN #S1-TAX-YEAR := 0
                pnd_Super1_Pnd_S1_Settle_Type.setValue(" ");                                                                                                              //Natural: ASSIGN #S1-SETTLE-TYPE := ' '
                pnd_Super1_Pnd_S1_Pay_Type.setValue(" ");                                                                                                                 //Natural: ASSIGN #S1-PAY-TYPE := ' '
                pnd_Super1_Pnd_S1_Dist_Code.setValue(" ");                                                                                                                //Natural: ASSIGN #S1-DIST-CODE := ' '
                vw_rdist.startDatabaseRead                                                                                                                                //Natural: READ RDIST WITH TIRCNTL-NBR-YEAR-DISTR-CODE = #SUPER1
                (
                "RL3",
                new Wc[] { new Wc("TIRCNTL_NBR_YEAR_DISTR_CODE", ">=", pnd_Super1, WcType.BY) },
                new Oc[] { new Oc("TIRCNTL_NBR_YEAR_DISTR_CODE", "ASC") }
                );
                RL3:
                while (condition(vw_rdist.readNextRow("RL3")))
                {
                    if (condition(rdist_Tircntl_Tbl_Nbr.equals(1)))                                                                                                       //Natural: IF RDIST.TIRCNTL-TBL-NBR = 1
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF *ISN > 12526
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),rdist_Tircntl_Tbl_Nbr,new TabSetting(5),rdist_Tircntl_Tax_Year,new    //Natural: WRITE ( 01 ) NOTITLE NOHDR 01T TIRCNTL-TBL-NBR 05T TIRCNTL-TAX-YEAR 11T TIRCNTL-SEQ-NBR 19T TIRCNTL-TYPE-REFER 26T TIRCNTL-PAYMT-TYPE 33T TIRCNTL-SETTL-TYPE 38T TIRCNTL-SETTL-PAYM-DESCR 83T TIRCNTL-OVER59-AGE-IND 94T TIRCNTL-DIST-CODE 108T TIRCNTL-DIST-METHOD 120T TIRCNTL-PAYMT-CATEGORY *ISN ( RL3. )
                        TabSetting(11),rdist_Tircntl_Seq_Nbr,new TabSetting(19),rdist_Tircntl_Type_Refer,new TabSetting(26),rdist_Tircntl_Paymt_Type,new 
                        TabSetting(33),rdist_Tircntl_Settl_Type,new TabSetting(38),rdist_Tircntl_Settl_Paym_Descr,new TabSetting(83),rdist_Tircntl_Over59_Age_Ind,new 
                        TabSetting(94),rdist_Tircntl_Dist_Code,new TabSetting(108),rdist_Tircntl_Dist_Method,new TabSetting(120),rdist_Tircntl_Paymt_Category,
                        vw_rdist.getAstISN("RL3"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RL3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(rdist_Tircntl_Dist_Code_Descr.equals(" ")))                                                                                             //Natural: IF TIRCNTL-DIST-CODE-DESCR = ' '
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(38),rdist_Tircntl_Dist_Code_Descr);                                  //Natural: WRITE ( 01 ) NOTITLE NOHDR 38T TIRCNTL-DIST-CODE-DESCR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RL3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RL3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  G2.
                    //*  GET UDIST *ISN (RL3.)
                    //*  DELETE (G2.)
                    //*  END TRANSACTION
                    //*  END-IF
                    //*  (RL3.)
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().skip(0, 4);                                                                                                                                  //Natural: SKIP ( 00 ) 4
                getReports().print(0, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 00 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(0, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(0, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 00 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(0, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 00 ) 'New Tax Year Records Stored..............' #STORE-CTR
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4
                getReports().print(1, "Old Tax Year Records Read................",pnd_Read1_Ctr);                                                                         //Natural: PRINT ( 01 ) 'Old Tax Year Records Read................' #READ1-CTR
                getReports().print(1, "New Tax Year Records Found...............",pnd_Read2_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Found...............' #READ2-CTR
                getReports().print(1, "New Tax Year Records Updated.............",pnd_Updt_Ctr);                                                                          //Natural: PRINT ( 01 ) 'New Tax Year Records Updated.............' #UPDT-CTR
                getReports().print(1, "New Tax Year Records Stored..............",pnd_Store_Ctr);                                                                         //Natural: PRINT ( 01 ) 'New Tax Year Records Stored..............' #STORE-CTR
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //* *---------                                                                                                                                            //Natural: AT TOP OF PAGE ( 01 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *-----------------
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(49),"Tax Withholding & Reporting System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 49T 'Tax Withholding & Reporting System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 )
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"));
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"Distribution Code Table Report",new      //Natural: WRITE ( 01 ) NOTITLE *INIT-USER '-' *PROGRAM 51T 'Distribution Code Table Report' 120T 'REPORT: RPT1'
                        TabSetting(120),"REPORT: RPT1");
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 01 ) 2 LINES
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TBL",new TabSetting(6),"Tax ",new TabSetting(12),"Seq",new TabSetting(17),"Refer",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'TBL' 06T 'Tax ' 12T 'Seq' 17T 'Refer' 24T 'Pymnt' 31T 'Settl' 80T 'Over 59' 89T 'Distribution' 103T 'Distribution' 117T 'Payment '
                        TabSetting(24),"Pymnt",new TabSetting(31),"Settl",new TabSetting(80),"Over 59",new TabSetting(89),"Distribution",new TabSetting(103),"Distribution",new 
                        TabSetting(117),"Payment ");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"NBR",new TabSetting(6),"Year",new TabSetting(12),"No.",new TabSetting(17),"Type ",new   //Natural: WRITE ( 01 ) NOTITLE 01T 'NBR' 06T 'Year' 12T 'No.' 17T 'Type ' 24T 'Type ' 31T 'Type ' 38T 'Payment / Settlement Description' 80T '  IND  ' 89T '    Code    ' 103T '   Method   ' 117T 'Category'
                        TabSetting(24),"Type ",new TabSetting(31),"Type ",new TabSetting(38),"Payment / Settlement Description",new TabSetting(80),"  IND  ",new 
                        TabSetting(89),"    Code    ",new TabSetting(103),"   Method   ",new TabSetting(117),"Category");
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"===",new TabSetting(6),"====",new TabSetting(12),"===",new TabSetting(17),"=====",new   //Natural: WRITE ( 01 ) NOTITLE 01T '===' 06T '====' 12T '===' 17T '=====' 24T '=====' 31T '=====' 38T '========================================' 80T '=======' 89T '============' 103T '============' 117T '========'
                        TabSetting(24),"=====",new TabSetting(31),"=====",new TabSetting(38),"========================================",new TabSetting(80),"=======",new 
                        TabSetting(89),"============",new TabSetting(103),"============",new TabSetting(117),"========");
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 01 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(0, pnd_Old_Tax_Year,pnd_New_Tax_Year);
    }
}
