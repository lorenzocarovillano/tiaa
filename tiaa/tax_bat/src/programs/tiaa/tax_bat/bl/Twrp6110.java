/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:43:33 PM
**        * FROM NATURAL PROGRAM : Twrp6110
************************************************************
**        * FILE NAME            : Twrp6110.java
**        * CLASS NAME           : Twrp6110
**        * INSTANCE NAME        : Twrp6110
************************************************************
************************************************************************
* PROGRAM  : TWRP6110 (UPDATED FROM RALPFR02)
* SYSTEM   : TAX - THE NEW TAX WITHHOLDING AND REPORTING SYSTEM.
* TITLE    : MOORE FORMS EXTRACT CONTROL TOTALS REPORTS.
* CREATED  : 09 / 11 / 1999.
*   BY     : RIAD LOUTFI.
* FUNCTION : PROGRAM READS IRA CONTRIBUTION DATA, AND PRODUCES CONTROL
*            TOTAL REPORTS FOR EACH ONE OF THE 'IRA' FORM TYPES
*           (I.E. 10, 20, AND 30).
************
* UPDATED  : 03/05/2005.
*   BY     : ROSE MA
*            TAKE OUT ALL HARD CODED #CREATE-YYYYMMDD AND TAX-YEAR.
*            THE SYSTEM HAD BEEN REDESIGNED TO UTILIZE AN EXISTING
*            MODULE TWRNTB4R TO EXTRACT 3 SETS OF DATES AND TAX YEARS
*            FROM THE DATABASE CONTROL TABLE (003/098), THEN OUTPUTS TO
*            3 SEPERATE WORK FILES. (BY PROGRAM TWRP6100)
*            THIS PROGRAM READS THOSE WORK FILES AND EXTRACTS FROM FORM
*            FILE (003/097) THE DATA FOR A SPECIFIC YEAR, THEN OUTPUTS
*            TO A WORK FILE FOR THE SUBSEQUENT PROGRAMS TO PRODUCE
*            NEEDED REPORTS.
*
* HISTORY:
* 06/05/2003 RM CHANGE CODE TO EXCLUDE REGENERATED 5498 RECORDS AFTER
*               NAAD CHANGES.
* 02/02/2004 RM CHANGE ALL HARD CODED MASS MAILING DATES AND THE TAX
*               REPORTING YEAR (2000-->2001)
* 02/01/2005 RM CHANGE ALL HARD CODED MASS MAILING DATES AND THE TAX
*               REPORTING YEAR (2001-->2004)
* 03/03/2005 RM UPDATE ALL HARD CODED MASS MAILING DATES FOR 1042-S,
*               480.6 AND NR4.
* 04/11/2005 RM COMMENT OUT CODES FOR 2 OUTPUT REPORTS SINCE THE SIMILAR
*               REPORTS WERE ALREADY PRODUCED BY SUBSEQUENT PROGRAMS.
* 10/05/18 - ARIVU - EIN CHANGES - TAG: EINCHG
*
************************************************************************
*

************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Twrp6110 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Record;

    private DbsGroup pnd_Work_Record__R_Field_1;
    private DbsField pnd_Work_Record_Pnd_Tax_Yr;

    private DbsGroup pnd_Work_Record__R_Field_2;
    private DbsField pnd_Work_Record_Pnd_Tax_Yr_A;

    private DbsGroup pnd_Work_Record_Pnd_Create_Dates;
    private DbsField pnd_Work_Record_Pnd_Form_Id;
    private DbsField pnd_Work_Record_Pnd_Eff_Dte;
    private DbsField pnd_Work_Record_Pnd_Mass_Mail_Dte;
    private DbsField pnd_Work_Record_Pnd_2nd_Mm_Dte;

    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Tax_Year;
    private DbsField form_Tirf_Form_Type;
    private DbsField form_Tirf_Company_Cde;
    private DbsField form_Tirf_Tin;
    private DbsField form_Tirf_Tax_Id_Type;
    private DbsField form_Tirf_Contract_Nbr;

    private DbsGroup form__R_Field_3;
    private DbsField form_Pnd_Contract_Prefix;
    private DbsField form_Tirf_Payee_Cde;
    private DbsField form_Tirf_Key;
    private DbsField form_Tirf_Form_Seq;
    private DbsField form_Tirf_Part_Rpt_Ind;
    private DbsField form_Tirf_Srce_Cde;
    private DbsField form_Tirf_Participant_Name;
    private DbsField form_Tirf_Create_Ts;
    private DbsField form_Tirf_Distribution_Cde;
    private DbsField form_Tirf_Create_User;

    private DataAccessProgramView vw_hist_Form;
    private DbsField hist_Form_Tirf_Superde_1;

    private DbsGroup hist_Form__R_Field_4;
    private DbsField hist_Form_Pnd_Tirf_Tax_Year;
    private DbsField hist_Form_Pnd_Tirf_Tin;
    private DbsField hist_Form_Pnd_Tirf_Form_Type;
    private DbsField hist_Form_Pnd_Tirf_Contract_Nbr;
    private DbsField hist_Form_Pnd_Tirf_Payee_Cde;
    private DbsField hist_Form_Pnd_Tirf_Key;
    private DbsField hist_Form_Pnd_Tirf_Invrse_Create_Ts;
    private DbsField pnd_Read_Ctr;
    private DbsField pnd_Accept_Ctr;
    private DbsField pnd_Company;
    private DbsField pnd_Create_Yyyymmdd;

    private DbsGroup pnd_Create_Yyyymmdd__R_Field_5;
    private DbsField pnd_Create_Yyyymmdd_Pnd_Create_Yyyymmdd_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Record = localVariables.newFieldInRecord("pnd_Work_Record", "#WORK-RECORD", FieldType.STRING, 200);

        pnd_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Record__R_Field_1", "REDEFINE", pnd_Work_Record);
        pnd_Work_Record_Pnd_Tax_Yr = pnd_Work_Record__R_Field_1.newFieldInGroup("pnd_Work_Record_Pnd_Tax_Yr", "#TAX-YR", FieldType.NUMERIC, 4);

        pnd_Work_Record__R_Field_2 = pnd_Work_Record__R_Field_1.newGroupInGroup("pnd_Work_Record__R_Field_2", "REDEFINE", pnd_Work_Record_Pnd_Tax_Yr);
        pnd_Work_Record_Pnd_Tax_Yr_A = pnd_Work_Record__R_Field_2.newFieldInGroup("pnd_Work_Record_Pnd_Tax_Yr_A", "#TAX-YR-A", FieldType.STRING, 4);

        pnd_Work_Record_Pnd_Create_Dates = pnd_Work_Record__R_Field_1.newGroupArrayInGroup("pnd_Work_Record_Pnd_Create_Dates", "#CREATE-DATES", new DbsArrayController(1, 
            7));
        pnd_Work_Record_Pnd_Form_Id = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Form_Id", "#FORM-ID", FieldType.STRING, 1);
        pnd_Work_Record_Pnd_Eff_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Eff_Dte", "#EFF-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_Pnd_Mass_Mail_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_Mass_Mail_Dte", "#MASS-MAIL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Work_Record_Pnd_2nd_Mm_Dte = pnd_Work_Record_Pnd_Create_Dates.newFieldInGroup("pnd_Work_Record_Pnd_2nd_Mm_Dte", "#2ND-MM-DTE", FieldType.NUMERIC, 
            8);

        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Tirf_Tax_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Tirf_Form_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Tirf_Form_Type.setDdmHeader("FORM");
        form_Tirf_Company_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_Tirf_Tin = vw_form.getRecord().newFieldInGroup("form_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Tirf_Tax_Id_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        form_Tirf_Contract_Nbr = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");

        form__R_Field_3 = vw_form.getRecord().newGroupInGroup("form__R_Field_3", "REDEFINE", form_Tirf_Contract_Nbr);
        form_Pnd_Contract_Prefix = form__R_Field_3.newFieldInGroup("form_Pnd_Contract_Prefix", "#CONTRACT-PREFIX", FieldType.STRING, 2);
        form_Tirf_Payee_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Tirf_Key = vw_form.getRecord().newFieldInGroup("form_Tirf_Key", "TIRF-KEY", FieldType.STRING, 5, RepeatingFieldStrategy.None, "TIRF_KEY");
        form_Tirf_Key.setDdmHeader("KEY");
        form_Tirf_Form_Seq = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Seq", "TIRF-FORM-SEQ", FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, 
            "TIRF_FORM_SEQ");
        form_Tirf_Form_Seq.setDdmHeader("FORM/SEQ/NUM");
        form_Tirf_Part_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Tirf_Srce_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Srce_Cde", "TIRF-SRCE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_SRCE_CDE");
        form_Tirf_Srce_Cde.setDdmHeader("SOURCE/CODE");
        form_Tirf_Participant_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Participant_Name", "TIRF-PARTICIPANT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "TIRF_PARTICIPANT_NAME");
        form_Tirf_Participant_Name.setDdmHeader("PARTICIPANT/NAME");
        form_Tirf_Create_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Create_Ts", "TIRF-CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_CREATE_TS");
        form_Tirf_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        form_Tirf_Distribution_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Distribution_Cde", "TIRF-DISTRIBUTION-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_DISTRIBUTION_CDE");
        form_Tirf_Distribution_Cde.setDdmHeader("DISTRI-/BUTION/CODE");
        form_Tirf_Create_User = vw_form.getRecord().newFieldInGroup("form_Tirf_Create_User", "TIRF-CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CREATE_USER");
        form_Tirf_Create_User.setDdmHeader("CREATE/USER/ID");
        registerRecord(vw_form);

        vw_hist_Form = new DataAccessProgramView(new NameInfo("vw_hist_Form", "HIST-FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        hist_Form_Tirf_Superde_1 = vw_hist_Form.getRecord().newFieldInGroup("hist_Form_Tirf_Superde_1", "TIRF-SUPERDE-1", FieldType.BINARY, 38, RepeatingFieldStrategy.None, 
            "TIRF_SUPERDE_1");
        hist_Form_Tirf_Superde_1.setSuperDescriptor(true);

        hist_Form__R_Field_4 = vw_hist_Form.getRecord().newGroupInGroup("hist_Form__R_Field_4", "REDEFINE", hist_Form_Tirf_Superde_1);
        hist_Form_Pnd_Tirf_Tax_Year = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        hist_Form_Pnd_Tirf_Tin = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        hist_Form_Pnd_Tirf_Form_Type = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Form_Type", "#TIRF-FORM-TYPE", FieldType.NUMERIC, 2);
        hist_Form_Pnd_Tirf_Contract_Nbr = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        hist_Form_Pnd_Tirf_Payee_Cde = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 2);
        hist_Form_Pnd_Tirf_Key = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Key", "#TIRF-KEY", FieldType.STRING, 5);
        hist_Form_Pnd_Tirf_Invrse_Create_Ts = hist_Form__R_Field_4.newFieldInGroup("hist_Form_Pnd_Tirf_Invrse_Create_Ts", "#TIRF-INVRSE-CREATE-TS", FieldType.TIME);
        registerRecord(vw_hist_Form);

        pnd_Read_Ctr = localVariables.newFieldInRecord("pnd_Read_Ctr", "#READ-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Accept_Ctr = localVariables.newFieldInRecord("pnd_Accept_Ctr", "#ACCEPT-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Company = localVariables.newFieldInRecord("pnd_Company", "#COMPANY", FieldType.STRING, 1);
        pnd_Create_Yyyymmdd = localVariables.newFieldInRecord("pnd_Create_Yyyymmdd", "#CREATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Create_Yyyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Create_Yyyymmdd__R_Field_5", "REDEFINE", pnd_Create_Yyyymmdd);
        pnd_Create_Yyyymmdd_Pnd_Create_Yyyymmdd_A = pnd_Create_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Create_Yyyymmdd_Pnd_Create_Yyyymmdd_A", "#CREATE-YYYYMMDD-A", 
            FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_form.reset();
        vw_hist_Form.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Twrp6110() throws Exception
    {
        super("Twrp6110");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("TWRP6110", onError);
        setupReports();
        //* *--------
        //*  FORMAT (01)  PS=60  LS=133                                                                                                                                   //Natural: FORMAT ( 00 ) PS = 60 LS = 133
        //*  FORMAT (02)  PS=60  LS=133
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #WORK-RECORD
        while (condition(getWorkFiles().read(1, pnd_Work_Record)))
        {
            vw_form.startDatabaseRead                                                                                                                                     //Natural: READ FORM WITH TIRF-SUPERDE-1 = #TAX-YR-A
            (
            "READ02",
            new Wc[] { new Wc("TIRF_SUPERDE_1", ">=", pnd_Work_Record_Pnd_Tax_Yr_A.getBinary(), WcType.BY) },
            new Oc[] { new Oc("TIRF_SUPERDE_1", "ASC") }
            );
            READ02:
            while (condition(vw_form.readNextRow("READ02")))
            {
                if (condition(form_Tirf_Tax_Year.equals(pnd_Work_Record_Pnd_Tax_Yr_A)))                                                                                   //Natural: IF TIRF-TAX-YEAR = #TAX-YR-A
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Read_Ctr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-CTR
                //*  IF TIRF-SRCE-CDE  =  'IRP2'  /*  5498 ANNUAL SECOND LOAD
                //*   5498 ANNUAL SECOND LOAD  6/5/03  RM
                if (condition(form_Tirf_Srce_Cde.equals("IR  ") || form_Tirf_Srce_Cde.equals(" ") && form_Tirf_Create_User.getSubstring(1,2).equals("P1")))               //Natural: IF TIRF-SRCE-CDE = 'IR  ' OR TIRF-SRCE-CDE = ' ' AND SUBSTR ( TIRF-CREATE-USER,1,2 ) = 'P1'
                {
                    //*  FORMS CREATED BY FIRST ROLLUP, DON't have source code and when
                    //*  THEY ARE INDIRECTLY REGENERATED BECAUSE OF NAAD CHANGE.
                    //*  INDIRECTLY MEANS THAT THE FINANCIAL INFORMATION ON 5498 WAS CHANGED
                    //*  AND ALL OTHER 5498 FORMS FOR THE PARTICIPANT WERE REGENERATED
                    //*  BECAUSE PARTICIPANT HAS A CHANGED NAAD.
                    //*                 OR =  'ED'    /*  ELIMINATE EXCESS DEFERALS
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(form_Tirf_Create_User.equals("P1000TXR")))                                                                                                  //Natural: IF TIRF-CREATE-USER = 'P1000TXR'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(form_Tirf_Form_Seq.equals(1))))                                                                                                           //Natural: ACCEPT IF TIRF-FORM-SEQ = 1
                {
                    continue;
                }
                pnd_Create_Yyyymmdd_Pnd_Create_Yyyymmdd_A.setValueEdited(form_Tirf_Create_Ts,new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED TIRF-CREATE-TS ( EM = YYYYMMDD ) TO #CREATE-YYYYMMDD-A
                if (condition((form_Tirf_Form_Type.equals(1) && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(1))) || (form_Tirf_Form_Type.equals(2)   //Natural: IF ( TIRF-FORM-TYPE = 01 AND #CREATE-YYYYMMDD > #EFF-DTE ( 1 ) ) OR ( TIRF-FORM-TYPE = 02 AND #CREATE-YYYYMMDD > #EFF-DTE ( 2 ) ) OR ( TIRF-FORM-TYPE = 03 AND #CREATE-YYYYMMDD > #EFF-DTE ( 3 ) ) OR ( TIRF-FORM-TYPE = 04 AND #CREATE-YYYYMMDD > #EFF-DTE ( 4 ) ) OR ( TIRF-FORM-TYPE = 05 AND #CREATE-YYYYMMDD > #EFF-DTE ( 5 ) ) OR ( TIRF-FORM-TYPE = 07 AND #CREATE-YYYYMMDD > #EFF-DTE ( 7 ) )
                    && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(2))) || (form_Tirf_Form_Type.equals(3) && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(3))) 
                    || (form_Tirf_Form_Type.equals(4) && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(4))) || (form_Tirf_Form_Type.equals(5) 
                    && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(5))) || (form_Tirf_Form_Type.equals(7) && pnd_Create_Yyyymmdd.greater(pnd_Work_Record_Pnd_Eff_Dte.getValue(7)))))
                {
                    pnd_Accept_Ctr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #ACCEPT-CTR
                    //*     DISPLAY (01)
                    //*             'Tax/Year'        TIRF-TAX-YEAR          /*  (A04)
                    //*             'Form/Type'       TIRF-FORM-TYPE         /*  (N02)
                    //*             'Create/Date'     #CREATE-YYYYMMDD       /*  (A08)
                    //*             'Comp/Code'       TIRF-COMPANY-CDE       /*  (A01)
                    //*             'Tin'             TIRF-TIN               /*  (A10)
                    //*             'Tin/Type'        TIRF-TAX-ID-TYPE       /*  (A01)
                    //*             'Contract'        TIRF-CONTRACT-NBR      /*  (A08)
                    //*             'Payee'           TIRF-PAYEE-CDE         /*  (A02)
                    //*             'Key'             TIRF-KEY               /*  (A05)
                    //*             'Form/Seq'        TIRF-FORM-SEQ          /*  (P03)
                    //*             'RPT/IND'         TIRF-PART-RPT-IND      /*  (A01)
                    //*             'SRCE/CODE'       TIRF-SRCE-CDE          /*  (A04)
                    //*             'Name'            TIRF-PARTICIPANT-NAME  /*  (A35)
                    //*  EINCHG
                    if (condition((form_Tirf_Company_Cde.equals("L") || ((form_Tirf_Company_Cde.equals("T") || form_Tirf_Company_Cde.equals("A")) && form_Pnd_Contract_Prefix.equals("08"))))) //Natural: IF TIRF-COMPANY-CDE = 'L' OR ( ( TIRF-COMPANY-CDE = 'T' OR = 'A' ) AND #CONTRACT-PREFIX = '08' )
                    {
                        pnd_Company.setValue("1");                                                                                                                        //Natural: ASSIGN #COMPANY := '1'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  EINCHG
                        if (condition(((form_Tirf_Company_Cde.equals("T") || form_Tirf_Company_Cde.equals("A")) && ((form_Pnd_Contract_Prefix.greaterOrEqual("G0")        //Natural: IF ( TIRF-COMPANY-CDE = 'T' OR = 'A' ) AND ( #CONTRACT-PREFIX = 'G0' THRU 'G8' OR #CONTRACT-PREFIX = '00' THRU '07' )
                            && form_Pnd_Contract_Prefix.lessOrEqual("G8")) || (form_Pnd_Contract_Prefix.greaterOrEqual("00") && form_Pnd_Contract_Prefix.lessOrEqual("07"))))))
                        {
                            pnd_Company.setValue("2");                                                                                                                    //Natural: ASSIGN #COMPANY := '2'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  EINCHG
                            if (condition(((form_Tirf_Company_Cde.equals("T") || form_Tirf_Company_Cde.equals("A")) && form_Pnd_Contract_Prefix.equals("DC"))))           //Natural: IF ( TIRF-COMPANY-CDE = 'T' OR = 'A' ) AND #CONTRACT-PREFIX = 'DC'
                            {
                                pnd_Company.setValue("4");                                                                                                                //Natural: ASSIGN #COMPANY := '4'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  EINCHG
                                if (condition(((form_Tirf_Company_Cde.equals("T") || form_Tirf_Company_Cde.equals("A")) && form_Pnd_Contract_Prefix.equals("09"))))       //Natural: IF ( TIRF-COMPANY-CDE = 'T' OR = 'A' ) AND #CONTRACT-PREFIX = '09'
                                {
                                    pnd_Company.setValue("5");                                                                                                            //Natural: ASSIGN #COMPANY := '5'
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Company.setValue("3");                                                                                                            //Natural: ASSIGN #COMPANY := '3'
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(form_Tirf_Company_Cde.equals("C")))                                                                                                     //Natural: IF TIRF-COMPANY-CDE = 'C'
                    {
                        pnd_Company.setValue("3");                                                                                                                        //Natural: ASSIGN #COMPANY := '3'
                    }                                                                                                                                                     //Natural: END-IF
                    //*   (A01)
                    //*   (A04)
                    //*   (N02)
                    //*   (A08)
                    //*   (A01)
                    //*   (A10)
                    //*   (A01)
                    //*   (A08)
                    //*   (A02)
                    //*   (A05)
                    //*   (P03)
                    //*   (A01)
                    //*   (A04)
                    //*   (A35)
                    //*   (A02)
                    getWorkFiles().write(2, false, pnd_Company, form_Tirf_Tax_Year, form_Tirf_Form_Type, pnd_Create_Yyyymmdd, form_Tirf_Company_Cde, form_Tirf_Tin,       //Natural: WRITE WORK FILE 02 #COMPANY TIRF-TAX-YEAR TIRF-FORM-TYPE #CREATE-YYYYMMDD TIRF-COMPANY-CDE TIRF-TIN TIRF-TAX-ID-TYPE TIRF-CONTRACT-NBR TIRF-PAYEE-CDE TIRF-KEY TIRF-FORM-SEQ TIRF-PART-RPT-IND TIRF-SRCE-CDE TIRF-PARTICIPANT-NAME TIRF-DISTRIBUTION-CDE
                        form_Tirf_Tax_Id_Type, form_Tirf_Contract_Nbr, form_Tirf_Payee_Cde, form_Tirf_Key, form_Tirf_Form_Seq, form_Tirf_Part_Rpt_Ind, form_Tirf_Srce_Cde, 
                        form_Tirf_Participant_Name, form_Tirf_Distribution_Cde);
                    //*                                                     (A96)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (condition(pnd_Read_Ctr.equals(getZero())))                                                                                                                    //Natural: IF #READ-CTR = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(6),"Form Data Base File is Empty",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(6),"PROGRAM...:",Global.getPROGRAM(),new  //Natural: WRITE ( 00 ) '***' 06T 'Form Data Base File is Empty' 77T '***' / '***' 06T 'PROGRAM...:' *PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-PROGRAM-PROCESSING
        //* *------------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  AT TOP OF PAGE (01)
        //* *-----------------
        //*   WRITE (01) NOTITLE
        //*         *DATU '-' *TIMX (EM=HH:IIAP)
        //*     49T 'Tax Withholding & Reporting System'
        //*    120T 'PAGE:' *PAGE-NUMBER (01) (EM=ZZ,ZZ9)
        //*       / *INIT-USER '-' *PROGRAM
        //*     51T 'Post Mass Mailing Forms Report'
        //*    120T 'REPORT: RPT1' //
        //*  END-TOPPAGE
        //* *---------
        //*  AT TOP OF PAGE (02)
        //* *-----------------
        //*  WRITE (02) NOTITLE
        //*        *DATU '-' *TIMX (EM=HH:IIAP)
        //*    49T 'Tax Withholding & Reporting System'
        //*   120T 'PAGE:' *PAGE-NUMBER (02) (EM=ZZ,ZZ9)
        //*      / *INIT-USER '-' *PROGRAM
        //*    47T 'IRA Contributions Moore Control Report'
        //*    120T 'REPORT: RPT2' //
        //*  END-TOPPAGE
        //* *---------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *-------                                                                                                                                                      //Natural: ON ERROR
    }
    private void sub_End_Of_Program_Processing() throws Exception                                                                                                         //Natural: END-OF-PROGRAM-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().skip(0, 4);                                                                                                                                          //Natural: SKIP ( 00 ) 4
        getReports().write(0, new TabSetting(1),"Total Number Of Form Records Found..............",pnd_Read_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new             //Natural: WRITE ( 00 ) 01T 'Total Number Of Form Records Found..............' #READ-CTR / 01T 'Total Number Of Form Records Accepted...........' #ACCEPT-CTR
            TabSetting(1),"Total Number Of Form Records Accepted...........",pnd_Accept_Ctr, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  SKIP (01) 4
        //*  WRITE (01)
        //*    01T 'Total Number Of Form Records Found..............'  #READ-CTR
        //*  / 01T 'Total Number Of Form Records Accepted...........'  #ACCEPT-CTR
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *------
                                                                                                                                                                          //Natural: PERFORM END-OF-PROGRAM-PROCESSING
        sub_End_Of_Program_Processing();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
    }
}
