/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:23 PM
**        * FROM NATURAL LDA     : TWRL810E
************************************************************
**        * FILE NAME            : LdaTwrl810e.java
**        * CLASS NAME           : LdaTwrl810e
**        * INSTANCE NAME        : LdaTwrl810e
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl810e extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Locality_Table;
    private DbsField pnd_Locality_Table_Pnd_Local_Code;
    private DbsField pnd_Locality_Table_Pnd_Local_Name;

    public DbsGroup getPnd_Locality_Table() { return pnd_Locality_Table; }

    public DbsField getPnd_Locality_Table_Pnd_Local_Code() { return pnd_Locality_Table_Pnd_Local_Code; }

    public DbsField getPnd_Locality_Table_Pnd_Local_Name() { return pnd_Locality_Table_Pnd_Local_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Locality_Table = newGroupArrayInRecord("pnd_Locality_Table", "#LOCALITY-TABLE", new DbsArrayController(1,39));
        pnd_Locality_Table_Pnd_Local_Code = pnd_Locality_Table.newFieldInGroup("pnd_Locality_Table_Pnd_Local_Code", "#LOCAL-CODE", FieldType.STRING, 3);
        pnd_Locality_Table_Pnd_Local_Name = pnd_Locality_Table.newFieldInGroup("pnd_Locality_Table_Pnd_Local_Name", "#LOCAL-NAME", FieldType.STRING, 35);

        this.setRecordName("LdaTwrl810e");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Locality_Table_Pnd_Local_Code.getValue(1).setInitialValue("BF1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(2).setInitialValue("BF2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(3).setInitialValue("JA1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(4).setInitialValue("JA2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(5).setInitialValue("NT1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(6).setInitialValue("NT2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(7).setInitialValue("NT3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(8).setInitialValue("PO1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(9).setInitialValue("PO2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(10).setInitialValue("MY1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(11).setInitialValue("MY2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(12).setInitialValue("RS1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(13).setInitialValue("RS2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(14).setInitialValue("SP1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(15).setInitialValue("SP2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(16).setInitialValue("SP3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(17).setInitialValue("SP3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(18).setInitialValue("TC1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(19).setInitialValue("TC1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(20).setInitialValue("TC2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(21).setInitialValue("TC3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(22).setInitialValue("UK1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(23).setInitialValue("UK2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(24).setInitialValue("UK3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(25).setInitialValue("UK4");
        pnd_Locality_Table_Pnd_Local_Code.getValue(26).setInitialValue("UK5");
        pnd_Locality_Table_Pnd_Local_Code.getValue(27).setInitialValue("UK6");
        pnd_Locality_Table_Pnd_Local_Code.getValue(28).setInitialValue("VC1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(29).setInitialValue("VC1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(30).setInitialValue("VC2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(31).setInitialValue("VI1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(32).setInitialValue("VI2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(33).setInitialValue("VI3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(34).setInitialValue("YO1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(35).setInitialValue("YO2");
        pnd_Locality_Table_Pnd_Local_Code.getValue(36).setInitialValue("YO3");
        pnd_Locality_Table_Pnd_Local_Code.getValue(37).setInitialValue("YO4");
        pnd_Locality_Table_Pnd_Local_Code.getValue(38).setInitialValue("CG1");
        pnd_Locality_Table_Pnd_Local_Code.getValue(39).setInitialValue("CG2");
        pnd_Locality_Table_Pnd_Local_Name.getValue(1).setInitialValue("BAHAMAS");
        pnd_Locality_Table_Pnd_Local_Name.getValue(2).setInitialValue("ELEUTHERA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(3).setInitialValue("JAPAN");
        pnd_Locality_Table_Pnd_Local_Name.getValue(4).setInitialValue("RYUKYU");
        pnd_Locality_Table_Pnd_Local_Name.getValue(5).setInitialValue("ANTILLES");
        pnd_Locality_Table_Pnd_Local_Name.getValue(6).setInitialValue("BONAIRE");
        pnd_Locality_Table_Pnd_Local_Name.getValue(7).setInitialValue("CURACAO");
        pnd_Locality_Table_Pnd_Local_Name.getValue(8).setInitialValue("PORTUGAL");
        pnd_Locality_Table_Pnd_Local_Name.getValue(9).setInitialValue("AZORES");
        pnd_Locality_Table_Pnd_Local_Name.getValue(10).setInitialValue("MALAYSIA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(11).setInitialValue("SARAWAK");
        pnd_Locality_Table_Pnd_Local_Name.getValue(12).setInitialValue("RUSSIA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(13).setInitialValue("KURILE");
        pnd_Locality_Table_Pnd_Local_Name.getValue(14).setInitialValue("SPAIN");
        pnd_Locality_Table_Pnd_Local_Name.getValue(15).setInitialValue("CANARY");
        pnd_Locality_Table_Pnd_Local_Name.getValue(16).setInitialValue("BALEARIC");
        pnd_Locality_Table_Pnd_Local_Name.getValue(17).setInitialValue("MALLORCA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(18).setInitialValue("EMIRATES");
        pnd_Locality_Table_Pnd_Local_Name.getValue(19).setInitialValue("UAE");
        pnd_Locality_Table_Pnd_Local_Name.getValue(20).setInitialValue("DHABI");
        pnd_Locality_Table_Pnd_Local_Name.getValue(21).setInitialValue("DUBAI");
        pnd_Locality_Table_Pnd_Local_Name.getValue(22).setInitialValue("UNITED KINGDOM");
        pnd_Locality_Table_Pnd_Local_Name.getValue(23).setInitialValue("IRELAND");
        pnd_Locality_Table_Pnd_Local_Name.getValue(24).setInitialValue("SCOTLAND");
        pnd_Locality_Table_Pnd_Local_Name.getValue(25).setInitialValue("WALES");
        pnd_Locality_Table_Pnd_Local_Name.getValue(26).setInitialValue("ENGLAND");
        pnd_Locality_Table_Pnd_Local_Name.getValue(27).setInitialValue("BRITAIN");
        pnd_Locality_Table_Pnd_Local_Name.getValue(28).setInitialValue("VINCENT");
        pnd_Locality_Table_Pnd_Local_Name.getValue(29).setInitialValue("GRENADINES");
        pnd_Locality_Table_Pnd_Local_Name.getValue(30).setInitialValue("WINDWARD");
        pnd_Locality_Table_Pnd_Local_Name.getValue(31).setInitialValue("VIRGIN");
        pnd_Locality_Table_Pnd_Local_Name.getValue(32).setInitialValue("REDONDA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(33).setInitialValue("TORTOLA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(34).setInitialValue("MONTENEGRO");
        pnd_Locality_Table_Pnd_Local_Name.getValue(35).setInitialValue("SERBIA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(36).setInitialValue("YUGOSLAVIA");
        pnd_Locality_Table_Pnd_Local_Name.getValue(37).setInitialValue("KOSOVO");
        pnd_Locality_Table_Pnd_Local_Name.getValue(38).setInitialValue("ZAIRE");
        pnd_Locality_Table_Pnd_Local_Name.getValue(39).setInitialValue("CONGO");
    }

    // Constructor
    public LdaTwrl810e() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
