/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:35 PM
**        * FROM NATURAL LDA     : TWRL3640
************************************************************
**        * FILE NAME            : LdaTwrl3640.java
**        * CLASS NAME           : LdaTwrl3640
**        * INSTANCE NAME        : LdaTwrl3640
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3640 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Out1_Ny_Record;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date;
    private DbsGroup pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_City;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_State;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank;
    private DbsGroup pnd_Out1_Ny_RecordRedef2;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_City;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_State;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind;
    private DbsGroup pnd_Out1_Ny_RecordRedef3;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5;
    private DbsGroup pnd_Out1_Ny_RecordRedef4;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3;
    private DbsGroup pnd_Out1_Ny_RecordRedef5;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank;
    private DbsGroup pnd_Out1_Ny_RecordRedef6;
    private DbsField pnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1;
    private DbsGroup pnd_Out1_Ny_RecordRedef7;
    private DbsField pnd_Out1_Ny_Record_Pnd_I100;
    private DbsField pnd_Out1_Ny_Record_Pnd_I28;
    private DbsGroup pnd_Out1_Ny_RecordRedef8;
    private DbsField pnd_Out1_Ny_Record_Pnd_D100;
    private DbsField pnd_Out1_Ny_Record_Pnd_D28;

    public DbsGroup getPnd_Out1_Ny_Record() { return pnd_Out1_Ny_Record; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date; }

    public DbsGroup getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_City() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_City; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_State() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_State; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank() { return pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef2() { return pnd_Out1_Ny_RecordRedef2; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_City() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_City; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_State() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_State; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind() { return pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef3() { return pnd_Out1_Ny_RecordRedef3; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5() { return pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef4() { return pnd_Out1_Ny_RecordRedef4; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3() { return pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef5() { return pnd_Out1_Ny_RecordRedef5; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id() { return pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e() { return pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w() { return pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank() { return pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef6() { return pnd_Out1_Ny_RecordRedef6; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1() { return pnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef7() { return pnd_Out1_Ny_RecordRedef7; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_I100() { return pnd_Out1_Ny_Record_Pnd_I100; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_I28() { return pnd_Out1_Ny_Record_Pnd_I28; }

    public DbsGroup getPnd_Out1_Ny_RecordRedef8() { return pnd_Out1_Ny_RecordRedef8; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_D100() { return pnd_Out1_Ny_Record_Pnd_D100; }

    public DbsField getPnd_Out1_Ny_Record_Pnd_D28() { return pnd_Out1_Ny_Record_Pnd_D28; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out1_Ny_Record = newGroupInRecord("pnd_Out1_Ny_Record", "#OUT1-NY-RECORD");
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id", "#OUT1A-NY-ID", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date", "#OUT1A-NY-CREATE-DATE", 
            FieldType.STRING, 6);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1 = pnd_Out1_Ny_Record.newGroupInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1", "Redefines", 
            pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm = pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Mm", 
            "#OUT1A-NY-CREATE-DATE-MM", FieldType.STRING, 2);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd = pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Dd", 
            "#OUT1A-NY-CREATE-DATE-DD", FieldType.STRING, 2);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy = pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Create_Date_Yy", 
            "#OUT1A-NY-CREATE-DATE-YY", FieldType.STRING, 2);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Id_Num", "#OUT1A-NY-ID-NUM", FieldType.STRING, 
            11);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Trans_Name", "#OUT1A-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Address", "#OUT1A-NY-ADDRESS", FieldType.STRING, 
            30);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_City = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_City", "#OUT1A-NY-CITY", FieldType.STRING, 
            25);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_State = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_State", "#OUT1A-NY-STATE", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Zip", "#OUT1A-NY-ZIP", FieldType.STRING, 
            9);
        pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank = pnd_Out1_Ny_Record.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1a_Ny_Blank", "#OUT1A-NY-BLANK", FieldType.STRING, 
            3);
        pnd_Out1_Ny_RecordRedef2 = newGroupInRecord("pnd_Out1_Ny_RecordRedef2", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Id", "#OUT1E-NY-ID", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Report_Quarter", "#OUT1E-NY-REPORT-QUARTER", 
            FieldType.STRING, 4);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Tin", "#OUT1E-NY-TIN", FieldType.STRING, 
            11);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1 = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank1", "#OUT1E-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Trans_Name", "#OUT1E-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2 = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank2", "#OUT1E-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Address", "#OUT1E-NY-ADDRESS", 
            FieldType.STRING, 30);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_City = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_City", "#OUT1E-NY-CITY", FieldType.STRING, 
            25);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_State = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_State", "#OUT1E-NY-STATE", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Zip", "#OUT1E-NY-ZIP", FieldType.STRING, 
            9);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Blank", "#OUT1E-NY-BLANK", FieldType.STRING, 
            1);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Ret_Type", "#OUT1E-NY-RET-TYPE", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind = pnd_Out1_Ny_RecordRedef2.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1e_Ny_Emp_Ind", "#OUT1E-NY-EMP-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_RecordRedef3 = newGroupInRecord("pnd_Out1_Ny_RecordRedef3", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Id", "#OUT1W-NY-ID", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tin", "#OUT1W-NY-TIN", FieldType.STRING, 
            9);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Name", "#OUT1W-NY-NAME", FieldType.STRING, 
            30);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1 = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank1", "#OUT1W-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Wages_Ind", "#OUT1W-NY-WAGES-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2 = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank2", "#OUT1W-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Gross", "#OUT1W-NY-GROSS", FieldType.DECIMAL, 
            14,2);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3 = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank3", "#OUT1W-NY-BLANK3", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Taxable_Amt", "#OUT1W-NY-TAXABLE-AMT", 
            FieldType.DECIMAL, 14,2);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4 = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank4", "#OUT1W-NY-BLANK4", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Tax_Wthld", "#OUT1W-NY-TAX-WTHLD", 
            FieldType.DECIMAL, 14,2);
        pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5 = pnd_Out1_Ny_RecordRedef3.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1w_Ny_Blank5", "#OUT1W-NY-BLANK5", 
            FieldType.STRING, 40);
        pnd_Out1_Ny_RecordRedef4 = newGroupInRecord("pnd_Out1_Ny_RecordRedef4", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Id", "#OUT1T-NY-ID", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_1w", "#OUT1T-NY-TOTAL-1W", 
            FieldType.NUMERIC, 7);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank", "#OUT1T-NY-BLANK", FieldType.STRING, 
            35);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Gross", "#OUT1T-NY-TOTAL-GROSS", 
            FieldType.DECIMAL, 14,2);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1 = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank1", "#OUT1T-NY-BLANK1", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Total_Taxable_Amt", 
            "#OUT1T-NY-TOTAL-TAXABLE-AMT", FieldType.DECIMAL, 14,2);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2 = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank2", "#OUT1T-NY-BLANK2", 
            FieldType.STRING, 1);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Tax_Wthld", "#OUT1T-NY-TAX-WTHLD", 
            FieldType.DECIMAL, 14,2);
        pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3 = pnd_Out1_Ny_RecordRedef4.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1t_Ny_Blank3", "#OUT1T-NY-BLANK3", 
            FieldType.STRING, 40);
        pnd_Out1_Ny_RecordRedef5 = newGroupInRecord("pnd_Out1_Ny_RecordRedef5", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id = pnd_Out1_Ny_RecordRedef5.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Id", "#OUT1F-NY-ID", FieldType.STRING, 
            2);
        pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e = pnd_Out1_Ny_RecordRedef5.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1e", "#OUT1F-NY-TOTAL-1E", 
            FieldType.NUMERIC, 10);
        pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w = pnd_Out1_Ny_RecordRedef5.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Total_1w", "#OUT1F-NY-TOTAL-1W", 
            FieldType.NUMERIC, 10);
        pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank = pnd_Out1_Ny_RecordRedef5.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1f_Ny_Blank", "#OUT1F-NY-BLANK", FieldType.STRING, 
            106);
        pnd_Out1_Ny_RecordRedef6 = newGroupInRecord("pnd_Out1_Ny_RecordRedef6", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1 = pnd_Out1_Ny_RecordRedef6.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_Out1_Ny_Rec1", "#OUT1-NY-REC1", FieldType.STRING, 
            128);
        pnd_Out1_Ny_RecordRedef7 = newGroupInRecord("pnd_Out1_Ny_RecordRedef7", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_I100 = pnd_Out1_Ny_RecordRedef7.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_I100", "#I100", FieldType.STRING, 100);
        pnd_Out1_Ny_Record_Pnd_I28 = pnd_Out1_Ny_RecordRedef7.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_I28", "#I28", FieldType.STRING, 28);
        pnd_Out1_Ny_RecordRedef8 = newGroupInRecord("pnd_Out1_Ny_RecordRedef8", "Redefines", pnd_Out1_Ny_Record);
        pnd_Out1_Ny_Record_Pnd_D100 = pnd_Out1_Ny_RecordRedef8.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_D100", "#D100", FieldType.STRING, 100);
        pnd_Out1_Ny_Record_Pnd_D28 = pnd_Out1_Ny_RecordRedef8.newFieldInGroup("pnd_Out1_Ny_Record_Pnd_D28", "#D28", FieldType.STRING, 28);

        this.setRecordName("LdaTwrl3640");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3640() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
