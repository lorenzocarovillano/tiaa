/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:12 PM
**        * FROM NATURAL LDA     : TWRL6345
************************************************************
**        * FILE NAME            : LdaTwrl6345.java
**        * CLASS NAME           : LdaTwrl6345
**        * INSTANCE NAME        : LdaTwrl6345
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl6345 extends DbsRecord
{
    // Properties
    private DbsField pnd_Version;
    private DbsField pnd_Forms;
    private DbsGroup pnd_Form_Constant;
    private DbsField pnd_Form_Constant_Pnd_Form_1099_Int;
    private DbsField pnd_Form_Constant_Pnd_Form_1099_R;
    private DbsField pnd_Form_Constant_Pnd_Form_1042_S;
    private DbsField pnd_Form_Constant_Pnd_Form_4806_A;
    private DbsField pnd_Form_Constant_Pnd_Form_5498_F;
    private DbsField pnd_Form_Constant_Pnd_Form_Nr4_N;
    private DbsField pnd_Form_Constant_Pnd_Form_Freeform;
    private DbsField pnd_Form_Constant_Pnd_Form_4806_B;
    private DbsField pnd_Form_Constant_Pnd_Form_5498_P;
    private DbsField pnd_Form_Constant_Pnd_Form_4807_C;
    private DbsField pnd_Form_Constant_Pnd_Form_1099_S;
    private DbsGroup pnd_Form_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F1099i_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F1099r_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F1042s_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F5498f_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F5498p_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F4806a_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F4806b_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_F4807c_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_Fnr4n_Occurs;
    private DbsField pnd_Form_Occurs_Pnd_Free_Form_Occurs;
    private DbsField no_Of_Addr_Lines;
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup cover_Letter_Data;
    private DbsField cover_Letter_Data_Cover_Rec_Id;
    private DbsField cover_Letter_Data_Cover_Data_Lgth;
    private DbsField cover_Letter_Data_Cover_Data_Occurs;
    private DbsField cover_Letter_Data_Cover_Data_Array;
    private DbsGroup cover_Letter_Data_Cover_Data_ArrayRedef1;
    private DbsGroup cover_Letter_Data_Letter_Data;
    private DbsField cover_Letter_Data_Year;
    private DbsField cover_Letter_Data_Letter_Id;
    private DbsGroup cover_Letter_Data_Lines;
    private DbsGroup cover_Letter_Data_Form_Lines;
    private DbsField cover_Letter_Data_Form_Id;
    private DbsField cover_Letter_Data_Contract;
    private DbsField cover_Letter_Data_Reason;
    private DbsField cover_Letter_Data_Mobius_Index;
    private DbsField cover_Letter_Data_Mobindx;
    private DbsField cover_Letter_Data_Dist_Cde;
    private DbsGroup tax_Form_Data;
    private DbsField tax_Form_Data_Tax_Form_Rec_Id;
    private DbsField tax_Form_Data_Tax_Form_Data_Lgth;
    private DbsField tax_Form_Data_Tax_Form_Data_Occurs;
    private DbsField tax_Form_Data_Tax_Form_Data_Array;
    private DbsGroup tax_Form_Data_Tax_Form_Data_ArrayRedef2;
    private DbsGroup tax_Form_Data_Form_Control_Dtls;
    private DbsField tax_Form_Data_Form_Type;
    private DbsField tax_Form_Data_Form_Year;
    private DbsField tax_Form_Data_Number_Of_Forms;
    private DbsField tax_Form_Data_Dont_Pull_1099r_Instruction;
    private DbsField tax_Form_Data_Dont_Pull_5498f_Instruction;
    private DbsField tax_Form_Data_Dont_Pull_5498p_Instruction;
    private DbsField tax_Form_Data_Dont_Pull_Nr4i_Instruction;
    private DbsField tax_Form_Data_Dont_Pull_4806a_Instruction;
    private DbsField tax_Form_Data_Dont_Pull_1042s_Instruction;
    private DbsField tax_Form_Data_Mobius_Index;
    private DbsField tax_Form_Data_Mobindx;
    private DbsField tax_Form_Data_Form_Data;
    private DbsGroup tax_Form_Data_Form_DataRedef3;
    private DbsGroup tax_Form_Data_F1099i_Data_Array;
    private DbsGroup tax_Form_Data_F1099i_Common;
    private DbsField tax_Form_Data_I_Corr;
    private DbsField tax_Form_Data_I_Payers_Rtn;
    private DbsField tax_Form_Data_I_Pay;
    private DbsField tax_Form_Data_I_Payid;
    private DbsField tax_Form_Data_I_Rec;
    private DbsField tax_Form_Data_I_Recid;
    private DbsField tax_Form_Data_I_Anum;
    private DbsGroup tax_Form_Data_I_Dollar_Amounts;
    private DbsField tax_Form_Data_I_Q1;
    private DbsField tax_Form_Data_I_Q2;
    private DbsField tax_Form_Data_I_Q3;
    private DbsField tax_Form_Data_I_Q4;
    private DbsField tax_Form_Data_I_Q5;
    private DbsField tax_Form_Data_I_Q6;
    private DbsField tax_Form_Data_I_Expansion1;
    private DbsField tax_Form_Data_I_Expansion2;
    private DbsField tax_Form_Data_I_Expansion3;
    private DbsField tax_Form_Data_I_Expansion4;
    private DbsGroup tax_Form_Data_I_Dollar_AmountsRedef4;
    private DbsField tax_Form_Data_I_Dollars;
    private DbsField tax_Form_Data_I_Q7;
    private DbsField tax_Form_Data_I_Expansion;
    private DbsGroup tax_Form_Data_Form_DataRedef5;
    private DbsGroup tax_Form_Data_F1099r_Form_Array;
    private DbsGroup tax_Form_Data_F1099r_Common;
    private DbsField tax_Form_Data_R_Corr;
    private DbsField tax_Form_Data_R_Payers_Rtn;
    private DbsField tax_Form_Data_R_Pay;
    private DbsField tax_Form_Data_R_Payid;
    private DbsField tax_Form_Data_R_Rec;
    private DbsField tax_Form_Data_R_Recid;
    private DbsField tax_Form_Data_R_Anum;
    private DbsField tax_Form_Data_R_Q7a;
    private DbsGroup tax_Form_Data_R_Indicators;
    private DbsField tax_Form_Data_R_Q2b;
    private DbsField tax_Form_Data_R_Q2c;
    private DbsField tax_Form_Data_R_Q7b;
    private DbsGroup tax_Form_Data_R_IndicatorsRedef6;
    private DbsField tax_Form_Data_R_Indicators_Array;
    private DbsGroup tax_Form_Data_R_Percent_Amounts;
    private DbsField tax_Form_Data_R_Q8b;
    private DbsField tax_Form_Data_R_Q9a;
    private DbsGroup tax_Form_Data_R_Dollar_Amounts;
    private DbsField tax_Form_Data_R_Q1;
    private DbsField tax_Form_Data_R_Q2a;
    private DbsField tax_Form_Data_R_Q3;
    private DbsField tax_Form_Data_R_Q4;
    private DbsField tax_Form_Data_R_Q5;
    private DbsField tax_Form_Data_R_Q6;
    private DbsField tax_Form_Data_R_Q8a;
    private DbsField tax_Form_Data_R_Q9b;
    private DbsField tax_Form_Data_R_Q10;
    private DbsField tax_Form_Data_R_Q11;
    private DbsField tax_Form_Data_R_Q12;
    private DbsField tax_Form_Data_R_Q13;
    private DbsField tax_Form_Data_R_Q14;
    private DbsField tax_Form_Data_R_Q15;
    private DbsField tax_Form_Data_R_Q10b;
    private DbsField tax_Form_Data_R_Q11b;
    private DbsField tax_Form_Data_R_Q12b;
    private DbsField tax_Form_Data_R_Expansion4;
    private DbsGroup tax_Form_Data_R_Dollar_AmountsRedef7;
    private DbsField tax_Form_Data_R_Dollar_Amount_Array;
    private DbsField tax_Form_Data_R_Expansion;
    private DbsGroup tax_Form_Data_R_ExpansionRedef8;
    private DbsField tax_Form_Data_R_Q16;
    private DbsField tax_Form_Data_R_Payment_Date;
    private DbsField tax_Form_Data_R_Expansion5;
    private DbsGroup tax_Form_Data_Form_DataRedef9;
    private DbsGroup tax_Form_Data_F1042s_Form_Array;
    private DbsField tax_Form_Data_S_Corr;
    private DbsGroup tax_Form_Data_S_Line_Dtls;
    private DbsField tax_Form_Data_S_Qa;
    private DbsField tax_Form_Data_S_Qb;
    private DbsField tax_Form_Data_S_Qc;
    private DbsField tax_Form_Data_S_Qd;
    private DbsField tax_Form_Data_S_Qe;
    private DbsField tax_Form_Data_S_Qf;
    private DbsField tax_Form_Data_S_Qg;
    private DbsField tax_Form_Data_S_Qh;
    private DbsField tax_Form_Data_S_Expansion;
    private DbsField tax_Form_Data_S_Q4;
    private DbsField tax_Form_Data_S_Recid;
    private DbsField tax_Form_Data_S_Anum;
    private DbsField tax_Form_Data_S_Rec;
    private DbsField tax_Form_Data_S_Q8;
    private DbsField tax_Form_Data_S_Wit;
    private DbsField tax_Form_Data_S_Witid;
    private DbsField tax_Form_Data_S_Pay;
    private DbsField tax_Form_Data_S_Q12;
    private DbsField tax_Form_Data_S_Q13;
    private DbsField tax_Form_Data_S_Q14;
    private DbsField tax_Form_Data_S_Svoid;
    private DbsField tax_Form_Data_S_Saein;
    private DbsField tax_Form_Data_S_Saeinx;
    private DbsField tax_Form_Data_S_Srtin;
    private DbsField tax_Form_Data_S_Srtinx;
    private DbsField tax_Form_Data_S_Sreinx;
    private DbsField tax_Form_Data_S_Srepaid;
    private DbsField tax_Form_Data_S_Expansion1;
    private DbsGroup tax_Form_Data_S_Expansion1Redef10;
    private DbsField tax_Form_Data_S_C3_Box;
    private DbsField tax_Form_Data_S_C4_Box;
    private DbsField tax_Form_Data_S_C4_Exempt;
    private DbsField tax_Form_Data_S_C4_Tax_Rate;
    private DbsField tax_Form_Data_S_C3_Status_Code;
    private DbsField tax_Form_Data_S_C4_Status_Code;
    private DbsField tax_Form_Data_S_C4_Giin;
    private DbsField tax_Form_Data_S_Birthdate;
    private DbsField tax_Form_Data_S_Fin_Number;
    private DbsField tax_Form_Data_S_Uin_Number;
    private DbsField tax_Form_Data_S_Amndment_Num;
    private DbsField tax_Form_Data_S_Lob_Code_13j;
    private DbsField tax_Form_Data_S_Payer_3_Status_Code_16d;
    private DbsField tax_Form_Data_S_Payer_4_Status_Code_16e;
    private DbsField tax_Form_Data_S_Expansion1_Filler;
    private DbsGroup tax_Form_Data_Form_DataRedef11;
    private DbsGroup tax_Form_Data_F5498_Form_Array;
    private DbsField tax_Form_Data_F_Corr;
    private DbsField tax_Form_Data_F_Tru;
    private DbsField tax_Form_Data_F_Truid;
    private DbsField tax_Form_Data_F_Pssn;
    private DbsField tax_Form_Data_F_Pay;
    private DbsField tax_Form_Data_F_Anum;
    private DbsGroup tax_Form_Data_F_Indicators;
    private DbsField tax_Form_Data_F_Q6a;
    private DbsField tax_Form_Data_F_Q6b;
    private DbsField tax_Form_Data_F_Q6c;
    private DbsField tax_Form_Data_F_Q6d;
    private DbsField tax_Form_Data_F_Q6e;
    private DbsField tax_Form_Data_F_Q6f;
    private DbsField tax_Form_Data_F_Q6_Expansion;
    private DbsGroup tax_Form_Data_F_IndicatorsRedef12;
    private DbsField tax_Form_Data_F_Q6_Ind;
    private DbsGroup tax_Form_Data_F_Dollar_Amounts;
    private DbsField tax_Form_Data_F_Q1;
    private DbsField tax_Form_Data_F_Q2;
    private DbsField tax_Form_Data_F_Q3;
    private DbsField tax_Form_Data_F_Q4;
    private DbsField tax_Form_Data_F_Q5;
    private DbsField tax_Form_Data_F_Q7;
    private DbsField tax_Form_Data_F_Q8;
    private DbsField tax_Form_Data_F_Q9;
    private DbsField tax_Form_Data_F_Q10;
    private DbsField tax_Form_Data_F_Frech;
    private DbsField tax_Form_Data_F_Expansion;
    private DbsGroup tax_Form_Data_F_Dollar_AmountsRedef13;
    private DbsField tax_Form_Data_F_Dollars;
    private DbsField tax_Form_Data_F_Expansion1;
    private DbsGroup tax_Form_Data_Form_DataRedef14;
    private DbsGroup tax_Form_Data_F5498p_Form_Array;
    private DbsField tax_Form_Data_F5498p_Tru;
    private DbsField tax_Form_Data_F5498p_Truid;
    private DbsField tax_Form_Data_F5498p_Pssn;
    private DbsField tax_Form_Data_F5498p_Pay;
    private DbsField tax_Form_Data_Coupon_Type;
    private DbsField tax_Form_Data_Coupon_Year;
    private DbsGroup tax_Form_Data_Acct_Info;
    private DbsField tax_Form_Data_Contr_Nbr;
    private DbsField tax_Form_Data_Corr_Ind;
    private DbsField tax_Form_Data_Postpn_Yr;
    private DbsField tax_Form_Data_Postpn_Code;
    private DbsGroup tax_Form_Data_Dollar_Amounts;
    private DbsField tax_Form_Data_Ira_Contr;
    private DbsField tax_Form_Data_Rollov_Contr;
    private DbsField tax_Form_Data_Roth_Conv_Amt;
    private DbsField tax_Form_Data_Rechar_Contrib;
    private DbsField tax_Form_Data_Fair_Mkt_Val;
    private DbsField tax_Form_Data_Roth_Ira_Contrib;
    private DbsField tax_Form_Data_Sep_Contrib;
    private DbsField tax_Form_Data_Postpn_Amt;
    private DbsGroup tax_Form_Data_Dollar_AmountsRedef15;
    private DbsField tax_Form_Data_Dollars;
    private DbsField tax_Form_Data_Ira_Type_Ind;
    private DbsField tax_Form_Data_Mdo_Required_Ind;
    private DbsField tax_Form_Data_Coup_Primary_Contr_Nbr;
    private DbsField tax_Form_Data_Coup_Secondary_Contr_Nbr;
    private DbsField tax_Form_Data_Coup_Ira_Type_Ind;
    private DbsField tax_Form_Data_Repayments_Amt;
    private DbsField tax_Form_Data_Repayments_Code;
    private DbsGroup tax_Form_Data_Form_DataRedef16;
    private DbsGroup tax_Form_Data_Fnr4_N_Form_Array;
    private DbsField tax_Form_Data_N_Q10;
    private DbsField tax_Form_Data_N_Q11;
    private DbsField tax_Form_Data_N_Q12a;
    private DbsField tax_Form_Data_N_Q12b;
    private DbsField tax_Form_Data_N_Q12c;
    private DbsField tax_Form_Data_N_Q13;
    private DbsGroup tax_Form_Data_N_Line_Dtls;
    private DbsField tax_Form_Data_N_Q14;
    private DbsField tax_Form_Data_N_Q15;
    private DbsField tax_Form_Data_N_Q16;
    private DbsField tax_Form_Data_N_Q17;
    private DbsField tax_Form_Data_N_Q18;
    private DbsField tax_Form_Data_N_Expansion;
    private DbsField tax_Form_Data_N_Rec;
    private DbsField tax_Form_Data_N_Pay;
    private DbsField tax_Form_Data_N_Expansion1;
    private DbsGroup tax_Form_Data_N_Expansion1Redef17;
    private DbsField tax_Form_Data_N_Corr;
    private DbsField tax_Form_Data_N_Expansion2;
    private DbsGroup tax_Form_Data_Form_DataRedef18;
    private DbsField tax_Form_Data_Free_Form_Lines;
    private DbsGroup tax_Form_Data_Form_DataRedef19;
    private DbsGroup tax_Form_Data_F4806_Form_Array;
    private DbsField tax_Form_Data_A_Payid;
    private DbsField tax_Form_Data_A_Pay;
    private DbsField tax_Form_Data_A_Recssn;
    private DbsField tax_Form_Data_A_Rec;
    private DbsGroup tax_Form_Data_A_Amounts_Paid;
    private DbsField tax_Form_Data_A_Q1;
    private DbsField tax_Form_Data_A_Q2;
    private DbsField tax_Form_Data_A_Q3;
    private DbsField tax_Form_Data_A_Q4;
    private DbsField tax_Form_Data_A_Q5;
    private DbsField tax_Form_Data_A_Q6;
    private DbsField tax_Form_Data_A_Q7;
    private DbsField tax_Form_Data_A_Q8;
    private DbsField tax_Form_Data_A_Q9;
    private DbsField tax_Form_Data_A_Expansion1;
    private DbsGroup tax_Form_Data_A_Amounts_PaidRedef20;
    private DbsField tax_Form_Data_A_Amounts;
    private DbsField tax_Form_Data_A_Q10;
    private DbsField tax_Form_Data_A_Expansion;
    private DbsGroup tax_Form_Data_A_ExpansionRedef21;
    private DbsField tax_Form_Data_A_Corr;
    private DbsField tax_Form_Data_A_Expansion2;
    private DbsGroup tax_Form_Data_Form_DataRedef22;
    private DbsGroup tax_Form_Data_F480b_Form_Array;
    private DbsField tax_Form_Data_B_Payid;
    private DbsField tax_Form_Data_B_Pay;
    private DbsField tax_Form_Data_B_Recssn;
    private DbsField tax_Form_Data_B_Rec;
    private DbsField tax_Form_Data_B_Bbnum;
    private DbsField tax_Form_Data_B_Bcnum;
    private DbsGroup tax_Form_Data_B_Amounts_Paid;
    private DbsField tax_Form_Data_B_Q1p;
    private DbsField tax_Form_Data_B_Q2p;
    private DbsField tax_Form_Data_B_Q3p;
    private DbsField tax_Form_Data_B_Q4p;
    private DbsField tax_Form_Data_B_Q5p;
    private DbsField tax_Form_Data_B_Q6p;
    private DbsField tax_Form_Data_B_Q7p;
    private DbsField tax_Form_Data_B_Q8p;
    private DbsField tax_Form_Data_B_Q9p;
    private DbsField tax_Form_Data_B_Q10p;
    private DbsField tax_Form_Data_B_Expansion1;
    private DbsGroup tax_Form_Data_B_Amounts_PaidRedef23;
    private DbsField tax_Form_Data_B_Amounts;
    private DbsGroup tax_Form_Data_B_Amounts_Withheld;
    private DbsField tax_Form_Data_B_Q1w;
    private DbsField tax_Form_Data_B_Q2w;
    private DbsField tax_Form_Data_B_Q3w;
    private DbsField tax_Form_Data_B_Q4w;
    private DbsField tax_Form_Data_B_Q5w;
    private DbsField tax_Form_Data_B_Q6w;
    private DbsField tax_Form_Data_B_Q7w;
    private DbsField tax_Form_Data_B_Q8w;
    private DbsField tax_Form_Data_B_Q9w;
    private DbsField tax_Form_Data_B_Q10w;
    private DbsField tax_Form_Data_B_Expansion2;
    private DbsGroup tax_Form_Data_B_Amounts_WithheldRedef24;
    private DbsField tax_Form_Data_B_Amounts_With;
    private DbsField tax_Form_Data_B_Expansion;
    private DbsGroup tax_Form_Data_B_ExpansionRedef25;
    private DbsField tax_Form_Data_B_Corr;
    private DbsField tax_Form_Data_B_Expansion3;
    private DbsGroup tax_Form_Data_Form_DataRedef26;
    private DbsGroup tax_Form_Data_F4807c_Form_Array;
    private DbsField tax_Form_Data_P_Corr;
    private DbsField tax_Form_Data_P_Payid;
    private DbsField tax_Form_Data_P_Pay;
    private DbsField tax_Form_Data_P_Recssn;
    private DbsField tax_Form_Data_P_Ein;
    private DbsField tax_Form_Data_P_Plan_Name;
    private DbsField tax_Form_Data_P_Plan_Sponsor;
    private DbsField tax_Form_Data_P_Rec;
    private DbsField tax_Form_Data_P_Lump_Ind;
    private DbsField tax_Form_Data_P_Anum;
    private DbsField tax_Form_Data_P_Cnum;
    private DbsField tax_Form_Data_P_Dcode;
    private DbsGroup tax_Form_Data_P_Amounts;
    private DbsField tax_Form_Data_P_Acost;
    private DbsField tax_Form_Data_P_Adist;
    private DbsField tax_Form_Data_P_Taxa;
    private DbsField tax_Form_Data_P_Dfamt;
    private DbsField tax_Form_Data_P_Roll_Cont;
    private DbsField tax_Form_Data_P_Roll_Dist;
    private DbsField tax_Form_Data_P_Amend_Date;
    private DbsField tax_Form_Data_P_Others;
    private DbsField tax_Form_Data_P_Total;
    private DbsField tax_Form_Data_P_Efile_Confirm_Nbr;
    private DbsField tax_Form_Data_P_Periodic_Ind;
    private DbsField tax_Form_Data_P_Partial_Ind;
    private DbsField tax_Form_Data_P_Annty_Ind;
    private DbsField tax_Form_Data_P_Qualpvt_Ind;
    private DbsField tax_Form_Data_P_Orgcnum;
    private DbsField tax_Form_Data_P_Adjresn;
    private DbsField tax_Form_Data_P_Exmpt_Dis;
    private DbsField tax_Form_Data_P_Taxa_Dis;
    private DbsField tax_Form_Data_P_Aftax_Dis;
    private DbsField tax_Form_Data_P_Total_Dis;
    private DbsField tax_Form_Data_P_Fedtx_Dis;
    private DbsField tax_Form_Data_P_Pension_Date;

    public DbsField getPnd_Version() { return pnd_Version; }

    public DbsField getPnd_Forms() { return pnd_Forms; }

    public DbsGroup getPnd_Form_Constant() { return pnd_Form_Constant; }

    public DbsField getPnd_Form_Constant_Pnd_Form_1099_Int() { return pnd_Form_Constant_Pnd_Form_1099_Int; }

    public DbsField getPnd_Form_Constant_Pnd_Form_1099_R() { return pnd_Form_Constant_Pnd_Form_1099_R; }

    public DbsField getPnd_Form_Constant_Pnd_Form_1042_S() { return pnd_Form_Constant_Pnd_Form_1042_S; }

    public DbsField getPnd_Form_Constant_Pnd_Form_4806_A() { return pnd_Form_Constant_Pnd_Form_4806_A; }

    public DbsField getPnd_Form_Constant_Pnd_Form_5498_F() { return pnd_Form_Constant_Pnd_Form_5498_F; }

    public DbsField getPnd_Form_Constant_Pnd_Form_Nr4_N() { return pnd_Form_Constant_Pnd_Form_Nr4_N; }

    public DbsField getPnd_Form_Constant_Pnd_Form_Freeform() { return pnd_Form_Constant_Pnd_Form_Freeform; }

    public DbsField getPnd_Form_Constant_Pnd_Form_4806_B() { return pnd_Form_Constant_Pnd_Form_4806_B; }

    public DbsField getPnd_Form_Constant_Pnd_Form_5498_P() { return pnd_Form_Constant_Pnd_Form_5498_P; }

    public DbsField getPnd_Form_Constant_Pnd_Form_4807_C() { return pnd_Form_Constant_Pnd_Form_4807_C; }

    public DbsField getPnd_Form_Constant_Pnd_Form_1099_S() { return pnd_Form_Constant_Pnd_Form_1099_S; }

    public DbsGroup getPnd_Form_Occurs() { return pnd_Form_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F1099i_Occurs() { return pnd_Form_Occurs_Pnd_F1099i_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F1099r_Occurs() { return pnd_Form_Occurs_Pnd_F1099r_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F1042s_Occurs() { return pnd_Form_Occurs_Pnd_F1042s_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F5498f_Occurs() { return pnd_Form_Occurs_Pnd_F5498f_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F5498p_Occurs() { return pnd_Form_Occurs_Pnd_F5498p_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F4806a_Occurs() { return pnd_Form_Occurs_Pnd_F4806a_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F4806b_Occurs() { return pnd_Form_Occurs_Pnd_F4806b_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_F4807c_Occurs() { return pnd_Form_Occurs_Pnd_F4807c_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_Fnr4n_Occurs() { return pnd_Form_Occurs_Pnd_Fnr4n_Occurs; }

    public DbsField getPnd_Form_Occurs_Pnd_Free_Form_Occurs() { return pnd_Form_Occurs_Pnd_Free_Form_Occurs; }

    public DbsField getNo_Of_Addr_Lines() { return no_Of_Addr_Lines; }

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getCover_Letter_Data() { return cover_Letter_Data; }

    public DbsField getCover_Letter_Data_Cover_Rec_Id() { return cover_Letter_Data_Cover_Rec_Id; }

    public DbsField getCover_Letter_Data_Cover_Data_Lgth() { return cover_Letter_Data_Cover_Data_Lgth; }

    public DbsField getCover_Letter_Data_Cover_Data_Occurs() { return cover_Letter_Data_Cover_Data_Occurs; }

    public DbsField getCover_Letter_Data_Cover_Data_Array() { return cover_Letter_Data_Cover_Data_Array; }

    public DbsGroup getCover_Letter_Data_Cover_Data_ArrayRedef1() { return cover_Letter_Data_Cover_Data_ArrayRedef1; }

    public DbsGroup getCover_Letter_Data_Letter_Data() { return cover_Letter_Data_Letter_Data; }

    public DbsField getCover_Letter_Data_Year() { return cover_Letter_Data_Year; }

    public DbsField getCover_Letter_Data_Letter_Id() { return cover_Letter_Data_Letter_Id; }

    public DbsGroup getCover_Letter_Data_Lines() { return cover_Letter_Data_Lines; }

    public DbsGroup getCover_Letter_Data_Form_Lines() { return cover_Letter_Data_Form_Lines; }

    public DbsField getCover_Letter_Data_Form_Id() { return cover_Letter_Data_Form_Id; }

    public DbsField getCover_Letter_Data_Contract() { return cover_Letter_Data_Contract; }

    public DbsField getCover_Letter_Data_Reason() { return cover_Letter_Data_Reason; }

    public DbsField getCover_Letter_Data_Mobius_Index() { return cover_Letter_Data_Mobius_Index; }

    public DbsField getCover_Letter_Data_Mobindx() { return cover_Letter_Data_Mobindx; }

    public DbsField getCover_Letter_Data_Dist_Cde() { return cover_Letter_Data_Dist_Cde; }

    public DbsGroup getTax_Form_Data() { return tax_Form_Data; }

    public DbsField getTax_Form_Data_Tax_Form_Rec_Id() { return tax_Form_Data_Tax_Form_Rec_Id; }

    public DbsField getTax_Form_Data_Tax_Form_Data_Lgth() { return tax_Form_Data_Tax_Form_Data_Lgth; }

    public DbsField getTax_Form_Data_Tax_Form_Data_Occurs() { return tax_Form_Data_Tax_Form_Data_Occurs; }

    public DbsField getTax_Form_Data_Tax_Form_Data_Array() { return tax_Form_Data_Tax_Form_Data_Array; }

    public DbsGroup getTax_Form_Data_Tax_Form_Data_ArrayRedef2() { return tax_Form_Data_Tax_Form_Data_ArrayRedef2; }

    public DbsGroup getTax_Form_Data_Form_Control_Dtls() { return tax_Form_Data_Form_Control_Dtls; }

    public DbsField getTax_Form_Data_Form_Type() { return tax_Form_Data_Form_Type; }

    public DbsField getTax_Form_Data_Form_Year() { return tax_Form_Data_Form_Year; }

    public DbsField getTax_Form_Data_Number_Of_Forms() { return tax_Form_Data_Number_Of_Forms; }

    public DbsField getTax_Form_Data_Dont_Pull_1099r_Instruction() { return tax_Form_Data_Dont_Pull_1099r_Instruction; }

    public DbsField getTax_Form_Data_Dont_Pull_5498f_Instruction() { return tax_Form_Data_Dont_Pull_5498f_Instruction; }

    public DbsField getTax_Form_Data_Dont_Pull_5498p_Instruction() { return tax_Form_Data_Dont_Pull_5498p_Instruction; }

    public DbsField getTax_Form_Data_Dont_Pull_Nr4i_Instruction() { return tax_Form_Data_Dont_Pull_Nr4i_Instruction; }

    public DbsField getTax_Form_Data_Dont_Pull_4806a_Instruction() { return tax_Form_Data_Dont_Pull_4806a_Instruction; }

    public DbsField getTax_Form_Data_Dont_Pull_1042s_Instruction() { return tax_Form_Data_Dont_Pull_1042s_Instruction; }

    public DbsField getTax_Form_Data_Mobius_Index() { return tax_Form_Data_Mobius_Index; }

    public DbsField getTax_Form_Data_Mobindx() { return tax_Form_Data_Mobindx; }

    public DbsField getTax_Form_Data_Form_Data() { return tax_Form_Data_Form_Data; }

    public DbsGroup getTax_Form_Data_Form_DataRedef3() { return tax_Form_Data_Form_DataRedef3; }

    public DbsGroup getTax_Form_Data_F1099i_Data_Array() { return tax_Form_Data_F1099i_Data_Array; }

    public DbsGroup getTax_Form_Data_F1099i_Common() { return tax_Form_Data_F1099i_Common; }

    public DbsField getTax_Form_Data_I_Corr() { return tax_Form_Data_I_Corr; }

    public DbsField getTax_Form_Data_I_Payers_Rtn() { return tax_Form_Data_I_Payers_Rtn; }

    public DbsField getTax_Form_Data_I_Pay() { return tax_Form_Data_I_Pay; }

    public DbsField getTax_Form_Data_I_Payid() { return tax_Form_Data_I_Payid; }

    public DbsField getTax_Form_Data_I_Rec() { return tax_Form_Data_I_Rec; }

    public DbsField getTax_Form_Data_I_Recid() { return tax_Form_Data_I_Recid; }

    public DbsField getTax_Form_Data_I_Anum() { return tax_Form_Data_I_Anum; }

    public DbsGroup getTax_Form_Data_I_Dollar_Amounts() { return tax_Form_Data_I_Dollar_Amounts; }

    public DbsField getTax_Form_Data_I_Q1() { return tax_Form_Data_I_Q1; }

    public DbsField getTax_Form_Data_I_Q2() { return tax_Form_Data_I_Q2; }

    public DbsField getTax_Form_Data_I_Q3() { return tax_Form_Data_I_Q3; }

    public DbsField getTax_Form_Data_I_Q4() { return tax_Form_Data_I_Q4; }

    public DbsField getTax_Form_Data_I_Q5() { return tax_Form_Data_I_Q5; }

    public DbsField getTax_Form_Data_I_Q6() { return tax_Form_Data_I_Q6; }

    public DbsField getTax_Form_Data_I_Expansion1() { return tax_Form_Data_I_Expansion1; }

    public DbsField getTax_Form_Data_I_Expansion2() { return tax_Form_Data_I_Expansion2; }

    public DbsField getTax_Form_Data_I_Expansion3() { return tax_Form_Data_I_Expansion3; }

    public DbsField getTax_Form_Data_I_Expansion4() { return tax_Form_Data_I_Expansion4; }

    public DbsGroup getTax_Form_Data_I_Dollar_AmountsRedef4() { return tax_Form_Data_I_Dollar_AmountsRedef4; }

    public DbsField getTax_Form_Data_I_Dollars() { return tax_Form_Data_I_Dollars; }

    public DbsField getTax_Form_Data_I_Q7() { return tax_Form_Data_I_Q7; }

    public DbsField getTax_Form_Data_I_Expansion() { return tax_Form_Data_I_Expansion; }

    public DbsGroup getTax_Form_Data_Form_DataRedef5() { return tax_Form_Data_Form_DataRedef5; }

    public DbsGroup getTax_Form_Data_F1099r_Form_Array() { return tax_Form_Data_F1099r_Form_Array; }

    public DbsGroup getTax_Form_Data_F1099r_Common() { return tax_Form_Data_F1099r_Common; }

    public DbsField getTax_Form_Data_R_Corr() { return tax_Form_Data_R_Corr; }

    public DbsField getTax_Form_Data_R_Payers_Rtn() { return tax_Form_Data_R_Payers_Rtn; }

    public DbsField getTax_Form_Data_R_Pay() { return tax_Form_Data_R_Pay; }

    public DbsField getTax_Form_Data_R_Payid() { return tax_Form_Data_R_Payid; }

    public DbsField getTax_Form_Data_R_Rec() { return tax_Form_Data_R_Rec; }

    public DbsField getTax_Form_Data_R_Recid() { return tax_Form_Data_R_Recid; }

    public DbsField getTax_Form_Data_R_Anum() { return tax_Form_Data_R_Anum; }

    public DbsField getTax_Form_Data_R_Q7a() { return tax_Form_Data_R_Q7a; }

    public DbsGroup getTax_Form_Data_R_Indicators() { return tax_Form_Data_R_Indicators; }

    public DbsField getTax_Form_Data_R_Q2b() { return tax_Form_Data_R_Q2b; }

    public DbsField getTax_Form_Data_R_Q2c() { return tax_Form_Data_R_Q2c; }

    public DbsField getTax_Form_Data_R_Q7b() { return tax_Form_Data_R_Q7b; }

    public DbsGroup getTax_Form_Data_R_IndicatorsRedef6() { return tax_Form_Data_R_IndicatorsRedef6; }

    public DbsField getTax_Form_Data_R_Indicators_Array() { return tax_Form_Data_R_Indicators_Array; }

    public DbsGroup getTax_Form_Data_R_Percent_Amounts() { return tax_Form_Data_R_Percent_Amounts; }

    public DbsField getTax_Form_Data_R_Q8b() { return tax_Form_Data_R_Q8b; }

    public DbsField getTax_Form_Data_R_Q9a() { return tax_Form_Data_R_Q9a; }

    public DbsGroup getTax_Form_Data_R_Dollar_Amounts() { return tax_Form_Data_R_Dollar_Amounts; }

    public DbsField getTax_Form_Data_R_Q1() { return tax_Form_Data_R_Q1; }

    public DbsField getTax_Form_Data_R_Q2a() { return tax_Form_Data_R_Q2a; }

    public DbsField getTax_Form_Data_R_Q3() { return tax_Form_Data_R_Q3; }

    public DbsField getTax_Form_Data_R_Q4() { return tax_Form_Data_R_Q4; }

    public DbsField getTax_Form_Data_R_Q5() { return tax_Form_Data_R_Q5; }

    public DbsField getTax_Form_Data_R_Q6() { return tax_Form_Data_R_Q6; }

    public DbsField getTax_Form_Data_R_Q8a() { return tax_Form_Data_R_Q8a; }

    public DbsField getTax_Form_Data_R_Q9b() { return tax_Form_Data_R_Q9b; }

    public DbsField getTax_Form_Data_R_Q10() { return tax_Form_Data_R_Q10; }

    public DbsField getTax_Form_Data_R_Q11() { return tax_Form_Data_R_Q11; }

    public DbsField getTax_Form_Data_R_Q12() { return tax_Form_Data_R_Q12; }

    public DbsField getTax_Form_Data_R_Q13() { return tax_Form_Data_R_Q13; }

    public DbsField getTax_Form_Data_R_Q14() { return tax_Form_Data_R_Q14; }

    public DbsField getTax_Form_Data_R_Q15() { return tax_Form_Data_R_Q15; }

    public DbsField getTax_Form_Data_R_Q10b() { return tax_Form_Data_R_Q10b; }

    public DbsField getTax_Form_Data_R_Q11b() { return tax_Form_Data_R_Q11b; }

    public DbsField getTax_Form_Data_R_Q12b() { return tax_Form_Data_R_Q12b; }

    public DbsField getTax_Form_Data_R_Expansion4() { return tax_Form_Data_R_Expansion4; }

    public DbsGroup getTax_Form_Data_R_Dollar_AmountsRedef7() { return tax_Form_Data_R_Dollar_AmountsRedef7; }

    public DbsField getTax_Form_Data_R_Dollar_Amount_Array() { return tax_Form_Data_R_Dollar_Amount_Array; }

    public DbsField getTax_Form_Data_R_Expansion() { return tax_Form_Data_R_Expansion; }

    public DbsGroup getTax_Form_Data_R_ExpansionRedef8() { return tax_Form_Data_R_ExpansionRedef8; }

    public DbsField getTax_Form_Data_R_Q16() { return tax_Form_Data_R_Q16; }

    public DbsField getTax_Form_Data_R_Payment_Date() { return tax_Form_Data_R_Payment_Date; }

    public DbsField getTax_Form_Data_R_Expansion5() { return tax_Form_Data_R_Expansion5; }

    public DbsGroup getTax_Form_Data_Form_DataRedef9() { return tax_Form_Data_Form_DataRedef9; }

    public DbsGroup getTax_Form_Data_F1042s_Form_Array() { return tax_Form_Data_F1042s_Form_Array; }

    public DbsField getTax_Form_Data_S_Corr() { return tax_Form_Data_S_Corr; }

    public DbsGroup getTax_Form_Data_S_Line_Dtls() { return tax_Form_Data_S_Line_Dtls; }

    public DbsField getTax_Form_Data_S_Qa() { return tax_Form_Data_S_Qa; }

    public DbsField getTax_Form_Data_S_Qb() { return tax_Form_Data_S_Qb; }

    public DbsField getTax_Form_Data_S_Qc() { return tax_Form_Data_S_Qc; }

    public DbsField getTax_Form_Data_S_Qd() { return tax_Form_Data_S_Qd; }

    public DbsField getTax_Form_Data_S_Qe() { return tax_Form_Data_S_Qe; }

    public DbsField getTax_Form_Data_S_Qf() { return tax_Form_Data_S_Qf; }

    public DbsField getTax_Form_Data_S_Qg() { return tax_Form_Data_S_Qg; }

    public DbsField getTax_Form_Data_S_Qh() { return tax_Form_Data_S_Qh; }

    public DbsField getTax_Form_Data_S_Expansion() { return tax_Form_Data_S_Expansion; }

    public DbsField getTax_Form_Data_S_Q4() { return tax_Form_Data_S_Q4; }

    public DbsField getTax_Form_Data_S_Recid() { return tax_Form_Data_S_Recid; }

    public DbsField getTax_Form_Data_S_Anum() { return tax_Form_Data_S_Anum; }

    public DbsField getTax_Form_Data_S_Rec() { return tax_Form_Data_S_Rec; }

    public DbsField getTax_Form_Data_S_Q8() { return tax_Form_Data_S_Q8; }

    public DbsField getTax_Form_Data_S_Wit() { return tax_Form_Data_S_Wit; }

    public DbsField getTax_Form_Data_S_Witid() { return tax_Form_Data_S_Witid; }

    public DbsField getTax_Form_Data_S_Pay() { return tax_Form_Data_S_Pay; }

    public DbsField getTax_Form_Data_S_Q12() { return tax_Form_Data_S_Q12; }

    public DbsField getTax_Form_Data_S_Q13() { return tax_Form_Data_S_Q13; }

    public DbsField getTax_Form_Data_S_Q14() { return tax_Form_Data_S_Q14; }

    public DbsField getTax_Form_Data_S_Svoid() { return tax_Form_Data_S_Svoid; }

    public DbsField getTax_Form_Data_S_Saein() { return tax_Form_Data_S_Saein; }

    public DbsField getTax_Form_Data_S_Saeinx() { return tax_Form_Data_S_Saeinx; }

    public DbsField getTax_Form_Data_S_Srtin() { return tax_Form_Data_S_Srtin; }

    public DbsField getTax_Form_Data_S_Srtinx() { return tax_Form_Data_S_Srtinx; }

    public DbsField getTax_Form_Data_S_Sreinx() { return tax_Form_Data_S_Sreinx; }

    public DbsField getTax_Form_Data_S_Srepaid() { return tax_Form_Data_S_Srepaid; }

    public DbsField getTax_Form_Data_S_Expansion1() { return tax_Form_Data_S_Expansion1; }

    public DbsGroup getTax_Form_Data_S_Expansion1Redef10() { return tax_Form_Data_S_Expansion1Redef10; }

    public DbsField getTax_Form_Data_S_C3_Box() { return tax_Form_Data_S_C3_Box; }

    public DbsField getTax_Form_Data_S_C4_Box() { return tax_Form_Data_S_C4_Box; }

    public DbsField getTax_Form_Data_S_C4_Exempt() { return tax_Form_Data_S_C4_Exempt; }

    public DbsField getTax_Form_Data_S_C4_Tax_Rate() { return tax_Form_Data_S_C4_Tax_Rate; }

    public DbsField getTax_Form_Data_S_C3_Status_Code() { return tax_Form_Data_S_C3_Status_Code; }

    public DbsField getTax_Form_Data_S_C4_Status_Code() { return tax_Form_Data_S_C4_Status_Code; }

    public DbsField getTax_Form_Data_S_C4_Giin() { return tax_Form_Data_S_C4_Giin; }

    public DbsField getTax_Form_Data_S_Birthdate() { return tax_Form_Data_S_Birthdate; }

    public DbsField getTax_Form_Data_S_Fin_Number() { return tax_Form_Data_S_Fin_Number; }

    public DbsField getTax_Form_Data_S_Uin_Number() { return tax_Form_Data_S_Uin_Number; }

    public DbsField getTax_Form_Data_S_Amndment_Num() { return tax_Form_Data_S_Amndment_Num; }

    public DbsField getTax_Form_Data_S_Lob_Code_13j() { return tax_Form_Data_S_Lob_Code_13j; }

    public DbsField getTax_Form_Data_S_Payer_3_Status_Code_16d() { return tax_Form_Data_S_Payer_3_Status_Code_16d; }

    public DbsField getTax_Form_Data_S_Payer_4_Status_Code_16e() { return tax_Form_Data_S_Payer_4_Status_Code_16e; }

    public DbsField getTax_Form_Data_S_Expansion1_Filler() { return tax_Form_Data_S_Expansion1_Filler; }

    public DbsGroup getTax_Form_Data_Form_DataRedef11() { return tax_Form_Data_Form_DataRedef11; }

    public DbsGroup getTax_Form_Data_F5498_Form_Array() { return tax_Form_Data_F5498_Form_Array; }

    public DbsField getTax_Form_Data_F_Corr() { return tax_Form_Data_F_Corr; }

    public DbsField getTax_Form_Data_F_Tru() { return tax_Form_Data_F_Tru; }

    public DbsField getTax_Form_Data_F_Truid() { return tax_Form_Data_F_Truid; }

    public DbsField getTax_Form_Data_F_Pssn() { return tax_Form_Data_F_Pssn; }

    public DbsField getTax_Form_Data_F_Pay() { return tax_Form_Data_F_Pay; }

    public DbsField getTax_Form_Data_F_Anum() { return tax_Form_Data_F_Anum; }

    public DbsGroup getTax_Form_Data_F_Indicators() { return tax_Form_Data_F_Indicators; }

    public DbsField getTax_Form_Data_F_Q6a() { return tax_Form_Data_F_Q6a; }

    public DbsField getTax_Form_Data_F_Q6b() { return tax_Form_Data_F_Q6b; }

    public DbsField getTax_Form_Data_F_Q6c() { return tax_Form_Data_F_Q6c; }

    public DbsField getTax_Form_Data_F_Q6d() { return tax_Form_Data_F_Q6d; }

    public DbsField getTax_Form_Data_F_Q6e() { return tax_Form_Data_F_Q6e; }

    public DbsField getTax_Form_Data_F_Q6f() { return tax_Form_Data_F_Q6f; }

    public DbsField getTax_Form_Data_F_Q6_Expansion() { return tax_Form_Data_F_Q6_Expansion; }

    public DbsGroup getTax_Form_Data_F_IndicatorsRedef12() { return tax_Form_Data_F_IndicatorsRedef12; }

    public DbsField getTax_Form_Data_F_Q6_Ind() { return tax_Form_Data_F_Q6_Ind; }

    public DbsGroup getTax_Form_Data_F_Dollar_Amounts() { return tax_Form_Data_F_Dollar_Amounts; }

    public DbsField getTax_Form_Data_F_Q1() { return tax_Form_Data_F_Q1; }

    public DbsField getTax_Form_Data_F_Q2() { return tax_Form_Data_F_Q2; }

    public DbsField getTax_Form_Data_F_Q3() { return tax_Form_Data_F_Q3; }

    public DbsField getTax_Form_Data_F_Q4() { return tax_Form_Data_F_Q4; }

    public DbsField getTax_Form_Data_F_Q5() { return tax_Form_Data_F_Q5; }

    public DbsField getTax_Form_Data_F_Q7() { return tax_Form_Data_F_Q7; }

    public DbsField getTax_Form_Data_F_Q8() { return tax_Form_Data_F_Q8; }

    public DbsField getTax_Form_Data_F_Q9() { return tax_Form_Data_F_Q9; }

    public DbsField getTax_Form_Data_F_Q10() { return tax_Form_Data_F_Q10; }

    public DbsField getTax_Form_Data_F_Frech() { return tax_Form_Data_F_Frech; }

    public DbsField getTax_Form_Data_F_Expansion() { return tax_Form_Data_F_Expansion; }

    public DbsGroup getTax_Form_Data_F_Dollar_AmountsRedef13() { return tax_Form_Data_F_Dollar_AmountsRedef13; }

    public DbsField getTax_Form_Data_F_Dollars() { return tax_Form_Data_F_Dollars; }

    public DbsField getTax_Form_Data_F_Expansion1() { return tax_Form_Data_F_Expansion1; }

    public DbsGroup getTax_Form_Data_Form_DataRedef14() { return tax_Form_Data_Form_DataRedef14; }

    public DbsGroup getTax_Form_Data_F5498p_Form_Array() { return tax_Form_Data_F5498p_Form_Array; }

    public DbsField getTax_Form_Data_F5498p_Tru() { return tax_Form_Data_F5498p_Tru; }

    public DbsField getTax_Form_Data_F5498p_Truid() { return tax_Form_Data_F5498p_Truid; }

    public DbsField getTax_Form_Data_F5498p_Pssn() { return tax_Form_Data_F5498p_Pssn; }

    public DbsField getTax_Form_Data_F5498p_Pay() { return tax_Form_Data_F5498p_Pay; }

    public DbsField getTax_Form_Data_Coupon_Type() { return tax_Form_Data_Coupon_Type; }

    public DbsField getTax_Form_Data_Coupon_Year() { return tax_Form_Data_Coupon_Year; }

    public DbsGroup getTax_Form_Data_Acct_Info() { return tax_Form_Data_Acct_Info; }

    public DbsField getTax_Form_Data_Contr_Nbr() { return tax_Form_Data_Contr_Nbr; }

    public DbsField getTax_Form_Data_Corr_Ind() { return tax_Form_Data_Corr_Ind; }

    public DbsField getTax_Form_Data_Postpn_Yr() { return tax_Form_Data_Postpn_Yr; }

    public DbsField getTax_Form_Data_Postpn_Code() { return tax_Form_Data_Postpn_Code; }

    public DbsGroup getTax_Form_Data_Dollar_Amounts() { return tax_Form_Data_Dollar_Amounts; }

    public DbsField getTax_Form_Data_Ira_Contr() { return tax_Form_Data_Ira_Contr; }

    public DbsField getTax_Form_Data_Rollov_Contr() { return tax_Form_Data_Rollov_Contr; }

    public DbsField getTax_Form_Data_Roth_Conv_Amt() { return tax_Form_Data_Roth_Conv_Amt; }

    public DbsField getTax_Form_Data_Rechar_Contrib() { return tax_Form_Data_Rechar_Contrib; }

    public DbsField getTax_Form_Data_Fair_Mkt_Val() { return tax_Form_Data_Fair_Mkt_Val; }

    public DbsField getTax_Form_Data_Roth_Ira_Contrib() { return tax_Form_Data_Roth_Ira_Contrib; }

    public DbsField getTax_Form_Data_Sep_Contrib() { return tax_Form_Data_Sep_Contrib; }

    public DbsField getTax_Form_Data_Postpn_Amt() { return tax_Form_Data_Postpn_Amt; }

    public DbsGroup getTax_Form_Data_Dollar_AmountsRedef15() { return tax_Form_Data_Dollar_AmountsRedef15; }

    public DbsField getTax_Form_Data_Dollars() { return tax_Form_Data_Dollars; }

    public DbsField getTax_Form_Data_Ira_Type_Ind() { return tax_Form_Data_Ira_Type_Ind; }

    public DbsField getTax_Form_Data_Mdo_Required_Ind() { return tax_Form_Data_Mdo_Required_Ind; }

    public DbsField getTax_Form_Data_Coup_Primary_Contr_Nbr() { return tax_Form_Data_Coup_Primary_Contr_Nbr; }

    public DbsField getTax_Form_Data_Coup_Secondary_Contr_Nbr() { return tax_Form_Data_Coup_Secondary_Contr_Nbr; }

    public DbsField getTax_Form_Data_Coup_Ira_Type_Ind() { return tax_Form_Data_Coup_Ira_Type_Ind; }

    public DbsField getTax_Form_Data_Repayments_Amt() { return tax_Form_Data_Repayments_Amt; }

    public DbsField getTax_Form_Data_Repayments_Code() { return tax_Form_Data_Repayments_Code; }

    public DbsGroup getTax_Form_Data_Form_DataRedef16() { return tax_Form_Data_Form_DataRedef16; }

    public DbsGroup getTax_Form_Data_Fnr4_N_Form_Array() { return tax_Form_Data_Fnr4_N_Form_Array; }

    public DbsField getTax_Form_Data_N_Q10() { return tax_Form_Data_N_Q10; }

    public DbsField getTax_Form_Data_N_Q11() { return tax_Form_Data_N_Q11; }

    public DbsField getTax_Form_Data_N_Q12a() { return tax_Form_Data_N_Q12a; }

    public DbsField getTax_Form_Data_N_Q12b() { return tax_Form_Data_N_Q12b; }

    public DbsField getTax_Form_Data_N_Q12c() { return tax_Form_Data_N_Q12c; }

    public DbsField getTax_Form_Data_N_Q13() { return tax_Form_Data_N_Q13; }

    public DbsGroup getTax_Form_Data_N_Line_Dtls() { return tax_Form_Data_N_Line_Dtls; }

    public DbsField getTax_Form_Data_N_Q14() { return tax_Form_Data_N_Q14; }

    public DbsField getTax_Form_Data_N_Q15() { return tax_Form_Data_N_Q15; }

    public DbsField getTax_Form_Data_N_Q16() { return tax_Form_Data_N_Q16; }

    public DbsField getTax_Form_Data_N_Q17() { return tax_Form_Data_N_Q17; }

    public DbsField getTax_Form_Data_N_Q18() { return tax_Form_Data_N_Q18; }

    public DbsField getTax_Form_Data_N_Expansion() { return tax_Form_Data_N_Expansion; }

    public DbsField getTax_Form_Data_N_Rec() { return tax_Form_Data_N_Rec; }

    public DbsField getTax_Form_Data_N_Pay() { return tax_Form_Data_N_Pay; }

    public DbsField getTax_Form_Data_N_Expansion1() { return tax_Form_Data_N_Expansion1; }

    public DbsGroup getTax_Form_Data_N_Expansion1Redef17() { return tax_Form_Data_N_Expansion1Redef17; }

    public DbsField getTax_Form_Data_N_Corr() { return tax_Form_Data_N_Corr; }

    public DbsField getTax_Form_Data_N_Expansion2() { return tax_Form_Data_N_Expansion2; }

    public DbsGroup getTax_Form_Data_Form_DataRedef18() { return tax_Form_Data_Form_DataRedef18; }

    public DbsField getTax_Form_Data_Free_Form_Lines() { return tax_Form_Data_Free_Form_Lines; }

    public DbsGroup getTax_Form_Data_Form_DataRedef19() { return tax_Form_Data_Form_DataRedef19; }

    public DbsGroup getTax_Form_Data_F4806_Form_Array() { return tax_Form_Data_F4806_Form_Array; }

    public DbsField getTax_Form_Data_A_Payid() { return tax_Form_Data_A_Payid; }

    public DbsField getTax_Form_Data_A_Pay() { return tax_Form_Data_A_Pay; }

    public DbsField getTax_Form_Data_A_Recssn() { return tax_Form_Data_A_Recssn; }

    public DbsField getTax_Form_Data_A_Rec() { return tax_Form_Data_A_Rec; }

    public DbsGroup getTax_Form_Data_A_Amounts_Paid() { return tax_Form_Data_A_Amounts_Paid; }

    public DbsField getTax_Form_Data_A_Q1() { return tax_Form_Data_A_Q1; }

    public DbsField getTax_Form_Data_A_Q2() { return tax_Form_Data_A_Q2; }

    public DbsField getTax_Form_Data_A_Q3() { return tax_Form_Data_A_Q3; }

    public DbsField getTax_Form_Data_A_Q4() { return tax_Form_Data_A_Q4; }

    public DbsField getTax_Form_Data_A_Q5() { return tax_Form_Data_A_Q5; }

    public DbsField getTax_Form_Data_A_Q6() { return tax_Form_Data_A_Q6; }

    public DbsField getTax_Form_Data_A_Q7() { return tax_Form_Data_A_Q7; }

    public DbsField getTax_Form_Data_A_Q8() { return tax_Form_Data_A_Q8; }

    public DbsField getTax_Form_Data_A_Q9() { return tax_Form_Data_A_Q9; }

    public DbsField getTax_Form_Data_A_Expansion1() { return tax_Form_Data_A_Expansion1; }

    public DbsGroup getTax_Form_Data_A_Amounts_PaidRedef20() { return tax_Form_Data_A_Amounts_PaidRedef20; }

    public DbsField getTax_Form_Data_A_Amounts() { return tax_Form_Data_A_Amounts; }

    public DbsField getTax_Form_Data_A_Q10() { return tax_Form_Data_A_Q10; }

    public DbsField getTax_Form_Data_A_Expansion() { return tax_Form_Data_A_Expansion; }

    public DbsGroup getTax_Form_Data_A_ExpansionRedef21() { return tax_Form_Data_A_ExpansionRedef21; }

    public DbsField getTax_Form_Data_A_Corr() { return tax_Form_Data_A_Corr; }

    public DbsField getTax_Form_Data_A_Expansion2() { return tax_Form_Data_A_Expansion2; }

    public DbsGroup getTax_Form_Data_Form_DataRedef22() { return tax_Form_Data_Form_DataRedef22; }

    public DbsGroup getTax_Form_Data_F480b_Form_Array() { return tax_Form_Data_F480b_Form_Array; }

    public DbsField getTax_Form_Data_B_Payid() { return tax_Form_Data_B_Payid; }

    public DbsField getTax_Form_Data_B_Pay() { return tax_Form_Data_B_Pay; }

    public DbsField getTax_Form_Data_B_Recssn() { return tax_Form_Data_B_Recssn; }

    public DbsField getTax_Form_Data_B_Rec() { return tax_Form_Data_B_Rec; }

    public DbsField getTax_Form_Data_B_Bbnum() { return tax_Form_Data_B_Bbnum; }

    public DbsField getTax_Form_Data_B_Bcnum() { return tax_Form_Data_B_Bcnum; }

    public DbsGroup getTax_Form_Data_B_Amounts_Paid() { return tax_Form_Data_B_Amounts_Paid; }

    public DbsField getTax_Form_Data_B_Q1p() { return tax_Form_Data_B_Q1p; }

    public DbsField getTax_Form_Data_B_Q2p() { return tax_Form_Data_B_Q2p; }

    public DbsField getTax_Form_Data_B_Q3p() { return tax_Form_Data_B_Q3p; }

    public DbsField getTax_Form_Data_B_Q4p() { return tax_Form_Data_B_Q4p; }

    public DbsField getTax_Form_Data_B_Q5p() { return tax_Form_Data_B_Q5p; }

    public DbsField getTax_Form_Data_B_Q6p() { return tax_Form_Data_B_Q6p; }

    public DbsField getTax_Form_Data_B_Q7p() { return tax_Form_Data_B_Q7p; }

    public DbsField getTax_Form_Data_B_Q8p() { return tax_Form_Data_B_Q8p; }

    public DbsField getTax_Form_Data_B_Q9p() { return tax_Form_Data_B_Q9p; }

    public DbsField getTax_Form_Data_B_Q10p() { return tax_Form_Data_B_Q10p; }

    public DbsField getTax_Form_Data_B_Expansion1() { return tax_Form_Data_B_Expansion1; }

    public DbsGroup getTax_Form_Data_B_Amounts_PaidRedef23() { return tax_Form_Data_B_Amounts_PaidRedef23; }

    public DbsField getTax_Form_Data_B_Amounts() { return tax_Form_Data_B_Amounts; }

    public DbsGroup getTax_Form_Data_B_Amounts_Withheld() { return tax_Form_Data_B_Amounts_Withheld; }

    public DbsField getTax_Form_Data_B_Q1w() { return tax_Form_Data_B_Q1w; }

    public DbsField getTax_Form_Data_B_Q2w() { return tax_Form_Data_B_Q2w; }

    public DbsField getTax_Form_Data_B_Q3w() { return tax_Form_Data_B_Q3w; }

    public DbsField getTax_Form_Data_B_Q4w() { return tax_Form_Data_B_Q4w; }

    public DbsField getTax_Form_Data_B_Q5w() { return tax_Form_Data_B_Q5w; }

    public DbsField getTax_Form_Data_B_Q6w() { return tax_Form_Data_B_Q6w; }

    public DbsField getTax_Form_Data_B_Q7w() { return tax_Form_Data_B_Q7w; }

    public DbsField getTax_Form_Data_B_Q8w() { return tax_Form_Data_B_Q8w; }

    public DbsField getTax_Form_Data_B_Q9w() { return tax_Form_Data_B_Q9w; }

    public DbsField getTax_Form_Data_B_Q10w() { return tax_Form_Data_B_Q10w; }

    public DbsField getTax_Form_Data_B_Expansion2() { return tax_Form_Data_B_Expansion2; }

    public DbsGroup getTax_Form_Data_B_Amounts_WithheldRedef24() { return tax_Form_Data_B_Amounts_WithheldRedef24; }

    public DbsField getTax_Form_Data_B_Amounts_With() { return tax_Form_Data_B_Amounts_With; }

    public DbsField getTax_Form_Data_B_Expansion() { return tax_Form_Data_B_Expansion; }

    public DbsGroup getTax_Form_Data_B_ExpansionRedef25() { return tax_Form_Data_B_ExpansionRedef25; }

    public DbsField getTax_Form_Data_B_Corr() { return tax_Form_Data_B_Corr; }

    public DbsField getTax_Form_Data_B_Expansion3() { return tax_Form_Data_B_Expansion3; }

    public DbsGroup getTax_Form_Data_Form_DataRedef26() { return tax_Form_Data_Form_DataRedef26; }

    public DbsGroup getTax_Form_Data_F4807c_Form_Array() { return tax_Form_Data_F4807c_Form_Array; }

    public DbsField getTax_Form_Data_P_Corr() { return tax_Form_Data_P_Corr; }

    public DbsField getTax_Form_Data_P_Payid() { return tax_Form_Data_P_Payid; }

    public DbsField getTax_Form_Data_P_Pay() { return tax_Form_Data_P_Pay; }

    public DbsField getTax_Form_Data_P_Recssn() { return tax_Form_Data_P_Recssn; }

    public DbsField getTax_Form_Data_P_Ein() { return tax_Form_Data_P_Ein; }

    public DbsField getTax_Form_Data_P_Plan_Name() { return tax_Form_Data_P_Plan_Name; }

    public DbsField getTax_Form_Data_P_Plan_Sponsor() { return tax_Form_Data_P_Plan_Sponsor; }

    public DbsField getTax_Form_Data_P_Rec() { return tax_Form_Data_P_Rec; }

    public DbsField getTax_Form_Data_P_Lump_Ind() { return tax_Form_Data_P_Lump_Ind; }

    public DbsField getTax_Form_Data_P_Anum() { return tax_Form_Data_P_Anum; }

    public DbsField getTax_Form_Data_P_Cnum() { return tax_Form_Data_P_Cnum; }

    public DbsField getTax_Form_Data_P_Dcode() { return tax_Form_Data_P_Dcode; }

    public DbsGroup getTax_Form_Data_P_Amounts() { return tax_Form_Data_P_Amounts; }

    public DbsField getTax_Form_Data_P_Acost() { return tax_Form_Data_P_Acost; }

    public DbsField getTax_Form_Data_P_Adist() { return tax_Form_Data_P_Adist; }

    public DbsField getTax_Form_Data_P_Taxa() { return tax_Form_Data_P_Taxa; }

    public DbsField getTax_Form_Data_P_Dfamt() { return tax_Form_Data_P_Dfamt; }

    public DbsField getTax_Form_Data_P_Roll_Cont() { return tax_Form_Data_P_Roll_Cont; }

    public DbsField getTax_Form_Data_P_Roll_Dist() { return tax_Form_Data_P_Roll_Dist; }

    public DbsField getTax_Form_Data_P_Amend_Date() { return tax_Form_Data_P_Amend_Date; }

    public DbsField getTax_Form_Data_P_Others() { return tax_Form_Data_P_Others; }

    public DbsField getTax_Form_Data_P_Total() { return tax_Form_Data_P_Total; }

    public DbsField getTax_Form_Data_P_Efile_Confirm_Nbr() { return tax_Form_Data_P_Efile_Confirm_Nbr; }

    public DbsField getTax_Form_Data_P_Periodic_Ind() { return tax_Form_Data_P_Periodic_Ind; }

    public DbsField getTax_Form_Data_P_Partial_Ind() { return tax_Form_Data_P_Partial_Ind; }

    public DbsField getTax_Form_Data_P_Annty_Ind() { return tax_Form_Data_P_Annty_Ind; }

    public DbsField getTax_Form_Data_P_Qualpvt_Ind() { return tax_Form_Data_P_Qualpvt_Ind; }

    public DbsField getTax_Form_Data_P_Orgcnum() { return tax_Form_Data_P_Orgcnum; }

    public DbsField getTax_Form_Data_P_Adjresn() { return tax_Form_Data_P_Adjresn; }

    public DbsField getTax_Form_Data_P_Exmpt_Dis() { return tax_Form_Data_P_Exmpt_Dis; }

    public DbsField getTax_Form_Data_P_Taxa_Dis() { return tax_Form_Data_P_Taxa_Dis; }

    public DbsField getTax_Form_Data_P_Aftax_Dis() { return tax_Form_Data_P_Aftax_Dis; }

    public DbsField getTax_Form_Data_P_Total_Dis() { return tax_Form_Data_P_Total_Dis; }

    public DbsField getTax_Form_Data_P_Fedtx_Dis() { return tax_Form_Data_P_Fedtx_Dis; }

    public DbsField getTax_Form_Data_P_Pension_Date() { return tax_Form_Data_P_Pension_Date; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        int int_pnd_Form_Occurs_Pnd_F1099i_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_F1099r_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_F1042s_Occurs = 3;
        int int_pnd_Form_Occurs_Pnd_F5498f_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_F5498p_Occurs = 2;
        int int_pnd_Form_Occurs_Pnd_F4806a_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_F4806b_Occurs = 3;
        int int_pnd_Form_Occurs_Pnd_F4807c_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_Fnr4n_Occurs = 4;
        int int_pnd_Form_Occurs_Pnd_Free_Form_Occurs = 60;
        int int_no_Of_Addr_Lines = 7;

        pnd_Version = newFieldInRecord("pnd_Version", "#VERSION", FieldType.STRING, 1);

        pnd_Forms = newFieldArrayInRecord("pnd_Forms", "#FORMS", FieldType.STRING, 10, new DbsArrayController(1,6));

        pnd_Form_Constant = newGroupInRecord("pnd_Form_Constant", "#FORM-CONSTANT");
        pnd_Form_Constant_Pnd_Form_1099_Int = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_1099_Int", "#FORM-1099-INT", FieldType.STRING, 
            1);
        pnd_Form_Constant_Pnd_Form_1099_R = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_1099_R", "#FORM-1099-R", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_1042_S = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_1042_S", "#FORM-1042-S", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_4806_A = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_4806_A", "#FORM-4806-A", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_5498_F = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_5498_F", "#FORM-5498-F", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_Nr4_N = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_Nr4_N", "#FORM-NR4-N", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_Freeform = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_Freeform", "#FORM-FREEFORM", FieldType.STRING, 
            1);
        pnd_Form_Constant_Pnd_Form_4806_B = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_4806_B", "#FORM-4806-B", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_5498_P = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_5498_P", "#FORM-5498-P", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_4807_C = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_4807_C", "#FORM-4807-C", FieldType.STRING, 1);
        pnd_Form_Constant_Pnd_Form_1099_S = pnd_Form_Constant.newFieldInGroup("pnd_Form_Constant_Pnd_Form_1099_S", "#FORM-1099-S", FieldType.STRING, 1);

        pnd_Form_Occurs = newGroupInRecord("pnd_Form_Occurs", "#FORM-OCCURS");
        pnd_Form_Occurs_Pnd_F1099i_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F1099i_Occurs", "#F1099I-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F1099r_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F1099r_Occurs", "#F1099R-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F1042s_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F1042s_Occurs", "#F1042S-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F5498f_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F5498f_Occurs", "#F5498F-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F5498p_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F5498p_Occurs", "#F5498P-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F4806a_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F4806a_Occurs", "#F4806A-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F4806b_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F4806b_Occurs", "#F4806B-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_F4807c_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_F4807c_Occurs", "#F4807C-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_Fnr4n_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_Fnr4n_Occurs", "#FNR4N-OCCURS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Form_Occurs_Pnd_Free_Form_Occurs = pnd_Form_Occurs.newFieldInGroup("pnd_Form_Occurs_Pnd_Free_Form_Occurs", "#FREE-FORM-OCCURS", FieldType.PACKED_DECIMAL, 
            3);

        no_Of_Addr_Lines = newFieldInRecord("no_Of_Addr_Lines", "NO-OF-ADDR-LINES", FieldType.PACKED_DECIMAL, 3);

        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        cover_Letter_Data = newGroupInRecord("cover_Letter_Data", "COVER-LETTER-DATA");
        cover_Letter_Data_Cover_Rec_Id = cover_Letter_Data.newFieldInGroup("cover_Letter_Data_Cover_Rec_Id", "COVER-REC-ID", FieldType.STRING, 2);
        cover_Letter_Data_Cover_Data_Lgth = cover_Letter_Data.newFieldInGroup("cover_Letter_Data_Cover_Data_Lgth", "COVER-DATA-LGTH", FieldType.NUMERIC, 
            5);
        cover_Letter_Data_Cover_Data_Occurs = cover_Letter_Data.newFieldInGroup("cover_Letter_Data_Cover_Data_Occurs", "COVER-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        cover_Letter_Data_Cover_Data_Array = cover_Letter_Data.newFieldArrayInGroup("cover_Letter_Data_Cover_Data_Array", "COVER-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,4465));
        cover_Letter_Data_Cover_Data_ArrayRedef1 = cover_Letter_Data.newGroupInGroup("cover_Letter_Data_Cover_Data_ArrayRedef1", "Redefines", cover_Letter_Data_Cover_Data_Array);
        cover_Letter_Data_Letter_Data = cover_Letter_Data_Cover_Data_ArrayRedef1.newGroupInGroup("cover_Letter_Data_Letter_Data", "LETTER-DATA");
        cover_Letter_Data_Year = cover_Letter_Data_Letter_Data.newFieldInGroup("cover_Letter_Data_Year", "YEAR", FieldType.STRING, 4);
        cover_Letter_Data_Letter_Id = cover_Letter_Data_Letter_Data.newFieldInGroup("cover_Letter_Data_Letter_Id", "LETTER-ID", FieldType.STRING, 4);
        cover_Letter_Data_Lines = cover_Letter_Data_Letter_Data.newGroupArrayInGroup("cover_Letter_Data_Lines", "LINES", new DbsArrayController(1,70));
        cover_Letter_Data_Form_Lines = cover_Letter_Data_Lines.newGroupInGroup("cover_Letter_Data_Form_Lines", "FORM-LINES");
        cover_Letter_Data_Form_Id = cover_Letter_Data_Form_Lines.newFieldInGroup("cover_Letter_Data_Form_Id", "FORM-ID", FieldType.STRING, 1);
        cover_Letter_Data_Contract = cover_Letter_Data_Form_Lines.newFieldInGroup("cover_Letter_Data_Contract", "CONTRACT", FieldType.STRING, 10);
        cover_Letter_Data_Reason = cover_Letter_Data_Form_Lines.newFieldInGroup("cover_Letter_Data_Reason", "REASON", FieldType.STRING, 50);
        cover_Letter_Data_Mobius_Index = cover_Letter_Data_Letter_Data.newFieldInGroup("cover_Letter_Data_Mobius_Index", "MOBIUS-INDEX", FieldType.STRING, 
            35);
        cover_Letter_Data_Mobindx = cover_Letter_Data_Letter_Data.newFieldInGroup("cover_Letter_Data_Mobindx", "MOBINDX", FieldType.STRING, 9);
        cover_Letter_Data_Dist_Cde = cover_Letter_Data_Letter_Data.newFieldArrayInGroup("cover_Letter_Data_Dist_Cde", "DIST-CDE", FieldType.STRING, 2, 
            new DbsArrayController(1,70));

        tax_Form_Data = newGroupInRecord("tax_Form_Data", "TAX-FORM-DATA");
        tax_Form_Data_Tax_Form_Rec_Id = tax_Form_Data.newFieldInGroup("tax_Form_Data_Tax_Form_Rec_Id", "TAX-FORM-REC-ID", FieldType.STRING, 2);
        tax_Form_Data_Tax_Form_Data_Lgth = tax_Form_Data.newFieldInGroup("tax_Form_Data_Tax_Form_Data_Lgth", "TAX-FORM-DATA-LGTH", FieldType.NUMERIC, 
            5);
        tax_Form_Data_Tax_Form_Data_Occurs = tax_Form_Data.newFieldInGroup("tax_Form_Data_Tax_Form_Data_Occurs", "TAX-FORM-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        tax_Form_Data_Tax_Form_Data_Array = tax_Form_Data.newFieldArrayInGroup("tax_Form_Data_Tax_Form_Data_Array", "TAX-FORM-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,4486));
        tax_Form_Data_Tax_Form_Data_ArrayRedef2 = tax_Form_Data.newGroupInGroup("tax_Form_Data_Tax_Form_Data_ArrayRedef2", "Redefines", tax_Form_Data_Tax_Form_Data_Array);
        tax_Form_Data_Form_Control_Dtls = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_Control_Dtls", "FORM-CONTROL-DTLS");
        tax_Form_Data_Form_Type = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Form_Type", "FORM-TYPE", FieldType.STRING, 1);
        tax_Form_Data_Form_Year = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Form_Year", "FORM-YEAR", FieldType.STRING, 2);
        tax_Form_Data_Number_Of_Forms = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Number_Of_Forms", "NUMBER-OF-FORMS", FieldType.NUMERIC, 
            3);
        tax_Form_Data_Dont_Pull_1099r_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_1099r_Instruction", "DONT-PULL-1099R-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Dont_Pull_5498f_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_5498f_Instruction", "DONT-PULL-5498F-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Dont_Pull_5498p_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_5498p_Instruction", "DONT-PULL-5498P-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Dont_Pull_Nr4i_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_Nr4i_Instruction", "DONT-PULL-NR4I-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Dont_Pull_4806a_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_4806a_Instruction", "DONT-PULL-4806A-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Dont_Pull_1042s_Instruction = tax_Form_Data_Form_Control_Dtls.newFieldInGroup("tax_Form_Data_Dont_Pull_1042s_Instruction", "DONT-PULL-1042S-INSTRUCTION", 
            FieldType.BOOLEAN);
        tax_Form_Data_Mobius_Index = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newFieldInGroup("tax_Form_Data_Mobius_Index", "MOBIUS-INDEX", FieldType.STRING, 
            35);
        tax_Form_Data_Mobindx = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newFieldInGroup("tax_Form_Data_Mobindx", "MOBINDX", FieldType.STRING, 9);
        tax_Form_Data_Form_Data = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newFieldArrayInGroup("tax_Form_Data_Form_Data", "FORM-DATA", FieldType.STRING, 
            1, new DbsArrayController(1,4360));
        tax_Form_Data_Form_DataRedef3 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef3", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F1099i_Data_Array = tax_Form_Data_Form_DataRedef3.newGroupArrayInGroup("tax_Form_Data_F1099i_Data_Array", "F1099I-DATA-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F1099i_Occurs));
        tax_Form_Data_F1099i_Common = tax_Form_Data_F1099i_Data_Array.newGroupInGroup("tax_Form_Data_F1099i_Common", "F1099I-COMMON");
        tax_Form_Data_I_Corr = tax_Form_Data_F1099i_Common.newFieldInGroup("tax_Form_Data_I_Corr", "I-CORR", FieldType.STRING, 1);
        tax_Form_Data_I_Payers_Rtn = tax_Form_Data_F1099i_Common.newFieldInGroup("tax_Form_Data_I_Payers_Rtn", "I-PAYERS-RTN", FieldType.STRING, 11);
        tax_Form_Data_I_Pay = tax_Form_Data_F1099i_Common.newFieldArrayInGroup("tax_Form_Data_I_Pay", "I-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_I_Payid = tax_Form_Data_F1099i_Common.newFieldInGroup("tax_Form_Data_I_Payid", "I-PAYID", FieldType.STRING, 11);
        tax_Form_Data_I_Rec = tax_Form_Data_F1099i_Common.newFieldArrayInGroup("tax_Form_Data_I_Rec", "I-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_I_Recid = tax_Form_Data_F1099i_Common.newFieldInGroup("tax_Form_Data_I_Recid", "I-RECID", FieldType.STRING, 11);
        tax_Form_Data_I_Anum = tax_Form_Data_F1099i_Common.newFieldInGroup("tax_Form_Data_I_Anum", "I-ANUM", FieldType.STRING, 15);
        tax_Form_Data_I_Dollar_Amounts = tax_Form_Data_F1099i_Data_Array.newGroupInGroup("tax_Form_Data_I_Dollar_Amounts", "I-DOLLAR-AMOUNTS");
        tax_Form_Data_I_Q1 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q1", "I-Q1", FieldType.STRING, 14);
        tax_Form_Data_I_Q2 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q2", "I-Q2", FieldType.STRING, 14);
        tax_Form_Data_I_Q3 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q3", "I-Q3", FieldType.STRING, 14);
        tax_Form_Data_I_Q4 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q4", "I-Q4", FieldType.STRING, 14);
        tax_Form_Data_I_Q5 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q5", "I-Q5", FieldType.STRING, 14);
        tax_Form_Data_I_Q6 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Q6", "I-Q6", FieldType.STRING, 14);
        tax_Form_Data_I_Expansion1 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Expansion1", "I-EXPANSION1", FieldType.STRING, 14);
        tax_Form_Data_I_Expansion2 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Expansion2", "I-EXPANSION2", FieldType.STRING, 14);
        tax_Form_Data_I_Expansion3 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Expansion3", "I-EXPANSION3", FieldType.STRING, 14);
        tax_Form_Data_I_Expansion4 = tax_Form_Data_I_Dollar_Amounts.newFieldInGroup("tax_Form_Data_I_Expansion4", "I-EXPANSION4", FieldType.STRING, 14);
        tax_Form_Data_I_Dollar_AmountsRedef4 = tax_Form_Data_F1099i_Data_Array.newGroupInGroup("tax_Form_Data_I_Dollar_AmountsRedef4", "Redefines", tax_Form_Data_I_Dollar_Amounts);
        tax_Form_Data_I_Dollars = tax_Form_Data_I_Dollar_AmountsRedef4.newFieldArrayInGroup("tax_Form_Data_I_Dollars", "I-DOLLARS", FieldType.STRING, 
            14, new DbsArrayController(1,10));
        tax_Form_Data_I_Q7 = tax_Form_Data_F1099i_Data_Array.newFieldInGroup("tax_Form_Data_I_Q7", "I-Q7", FieldType.STRING, 20);
        tax_Form_Data_I_Expansion = tax_Form_Data_F1099i_Data_Array.newFieldInGroup("tax_Form_Data_I_Expansion", "I-EXPANSION", FieldType.STRING, 100);
        tax_Form_Data_Form_DataRedef5 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef5", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F1099r_Form_Array = tax_Form_Data_Form_DataRedef5.newGroupArrayInGroup("tax_Form_Data_F1099r_Form_Array", "F1099R-FORM-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F1099r_Occurs));
        tax_Form_Data_F1099r_Common = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_F1099r_Common", "F1099R-COMMON");
        tax_Form_Data_R_Corr = tax_Form_Data_F1099r_Common.newFieldInGroup("tax_Form_Data_R_Corr", "R-CORR", FieldType.STRING, 1);
        tax_Form_Data_R_Payers_Rtn = tax_Form_Data_F1099r_Common.newFieldInGroup("tax_Form_Data_R_Payers_Rtn", "R-PAYERS-RTN", FieldType.STRING, 11);
        tax_Form_Data_R_Pay = tax_Form_Data_F1099r_Common.newFieldArrayInGroup("tax_Form_Data_R_Pay", "R-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_R_Payid = tax_Form_Data_F1099r_Common.newFieldInGroup("tax_Form_Data_R_Payid", "R-PAYID", FieldType.STRING, 11);
        tax_Form_Data_R_Rec = tax_Form_Data_F1099r_Common.newFieldArrayInGroup("tax_Form_Data_R_Rec", "R-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_R_Recid = tax_Form_Data_F1099r_Common.newFieldInGroup("tax_Form_Data_R_Recid", "R-RECID", FieldType.STRING, 11);
        tax_Form_Data_R_Anum = tax_Form_Data_F1099r_Common.newFieldInGroup("tax_Form_Data_R_Anum", "R-ANUM", FieldType.STRING, 15);
        tax_Form_Data_R_Q7a = tax_Form_Data_F1099r_Form_Array.newFieldInGroup("tax_Form_Data_R_Q7a", "R-Q7A", FieldType.STRING, 2);
        tax_Form_Data_R_Indicators = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_Indicators", "R-INDICATORS");
        tax_Form_Data_R_Q2b = tax_Form_Data_R_Indicators.newFieldInGroup("tax_Form_Data_R_Q2b", "R-Q2B", FieldType.STRING, 1);
        tax_Form_Data_R_Q2c = tax_Form_Data_R_Indicators.newFieldInGroup("tax_Form_Data_R_Q2c", "R-Q2C", FieldType.STRING, 1);
        tax_Form_Data_R_Q7b = tax_Form_Data_R_Indicators.newFieldInGroup("tax_Form_Data_R_Q7b", "R-Q7B", FieldType.STRING, 1);
        tax_Form_Data_R_IndicatorsRedef6 = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_IndicatorsRedef6", "Redefines", tax_Form_Data_R_Indicators);
        tax_Form_Data_R_Indicators_Array = tax_Form_Data_R_IndicatorsRedef6.newFieldArrayInGroup("tax_Form_Data_R_Indicators_Array", "R-INDICATORS-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        tax_Form_Data_R_Percent_Amounts = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_Percent_Amounts", "R-PERCENT-AMOUNTS");
        tax_Form_Data_R_Q8b = tax_Form_Data_R_Percent_Amounts.newFieldInGroup("tax_Form_Data_R_Q8b", "R-Q8B", FieldType.STRING, 5);
        tax_Form_Data_R_Q9a = tax_Form_Data_R_Percent_Amounts.newFieldInGroup("tax_Form_Data_R_Q9a", "R-Q9A", FieldType.STRING, 3);
        tax_Form_Data_R_Dollar_Amounts = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_Dollar_Amounts", "R-DOLLAR-AMOUNTS");
        tax_Form_Data_R_Q1 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q1", "R-Q1", FieldType.STRING, 18);
        tax_Form_Data_R_Q2a = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q2a", "R-Q2A", FieldType.STRING, 18);
        tax_Form_Data_R_Q3 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q3", "R-Q3", FieldType.STRING, 18);
        tax_Form_Data_R_Q4 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q4", "R-Q4", FieldType.STRING, 18);
        tax_Form_Data_R_Q5 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q5", "R-Q5", FieldType.STRING, 18);
        tax_Form_Data_R_Q6 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q6", "R-Q6", FieldType.STRING, 18);
        tax_Form_Data_R_Q8a = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q8a", "R-Q8A", FieldType.STRING, 18);
        tax_Form_Data_R_Q9b = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q9b", "R-Q9B", FieldType.STRING, 18);
        tax_Form_Data_R_Q10 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q10", "R-Q10", FieldType.STRING, 18);
        tax_Form_Data_R_Q11 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q11", "R-Q11", FieldType.STRING, 18);
        tax_Form_Data_R_Q12 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q12", "R-Q12", FieldType.STRING, 18);
        tax_Form_Data_R_Q13 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q13", "R-Q13", FieldType.STRING, 18);
        tax_Form_Data_R_Q14 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q14", "R-Q14", FieldType.STRING, 18);
        tax_Form_Data_R_Q15 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q15", "R-Q15", FieldType.STRING, 18);
        tax_Form_Data_R_Q10b = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q10b", "R-Q10B", FieldType.STRING, 18);
        tax_Form_Data_R_Q11b = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q11b", "R-Q11B", FieldType.STRING, 18);
        tax_Form_Data_R_Q12b = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Q12b", "R-Q12B", FieldType.STRING, 18);
        tax_Form_Data_R_Expansion4 = tax_Form_Data_R_Dollar_Amounts.newFieldInGroup("tax_Form_Data_R_Expansion4", "R-EXPANSION4", FieldType.STRING, 18);
        tax_Form_Data_R_Dollar_AmountsRedef7 = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_Dollar_AmountsRedef7", "Redefines", tax_Form_Data_R_Dollar_Amounts);
        tax_Form_Data_R_Dollar_Amount_Array = tax_Form_Data_R_Dollar_AmountsRedef7.newFieldArrayInGroup("tax_Form_Data_R_Dollar_Amount_Array", "R-DOLLAR-AMOUNT-ARRAY", 
            FieldType.STRING, 18, new DbsArrayController(1,17));
        tax_Form_Data_R_Expansion = tax_Form_Data_F1099r_Form_Array.newFieldInGroup("tax_Form_Data_R_Expansion", "R-EXPANSION", FieldType.STRING, 28);
        tax_Form_Data_R_ExpansionRedef8 = tax_Form_Data_F1099r_Form_Array.newGroupInGroup("tax_Form_Data_R_ExpansionRedef8", "Redefines", tax_Form_Data_R_Expansion);
        tax_Form_Data_R_Q16 = tax_Form_Data_R_ExpansionRedef8.newFieldInGroup("tax_Form_Data_R_Q16", "R-Q16", FieldType.STRING, 4);
        tax_Form_Data_R_Payment_Date = tax_Form_Data_R_ExpansionRedef8.newFieldInGroup("tax_Form_Data_R_Payment_Date", "R-PAYMENT-DATE", FieldType.STRING, 
            10);
        tax_Form_Data_R_Expansion5 = tax_Form_Data_R_ExpansionRedef8.newFieldInGroup("tax_Form_Data_R_Expansion5", "R-EXPANSION5", FieldType.STRING, 14);
        tax_Form_Data_Form_DataRedef9 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef9", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F1042s_Form_Array = tax_Form_Data_Form_DataRedef9.newGroupArrayInGroup("tax_Form_Data_F1042s_Form_Array", "F1042S-FORM-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F1042s_Occurs));
        tax_Form_Data_S_Corr = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Corr", "S-CORR", FieldType.STRING, 1);
        tax_Form_Data_S_Line_Dtls = tax_Form_Data_F1042s_Form_Array.newGroupArrayInGroup("tax_Form_Data_S_Line_Dtls", "S-LINE-DTLS", new DbsArrayController(1,
            3));
        tax_Form_Data_S_Qa = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qa", "S-QA", FieldType.STRING, 3);
        tax_Form_Data_S_Qb = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qb", "S-QB", FieldType.STRING, 14);
        tax_Form_Data_S_Qc = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qc", "S-QC", FieldType.STRING, 14);
        tax_Form_Data_S_Qd = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qd", "S-QD", FieldType.STRING, 14);
        tax_Form_Data_S_Qe = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qe", "S-QE", FieldType.STRING, 5);
        tax_Form_Data_S_Qf = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qf", "S-QF", FieldType.STRING, 3);
        tax_Form_Data_S_Qg = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qg", "S-QG", FieldType.STRING, 14);
        tax_Form_Data_S_Qh = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Qh", "S-QH", FieldType.STRING, 3);
        tax_Form_Data_S_Expansion = tax_Form_Data_S_Line_Dtls.newFieldInGroup("tax_Form_Data_S_Expansion", "S-EXPANSION", FieldType.STRING, 20);
        tax_Form_Data_S_Q4 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Q4", "S-Q4", FieldType.STRING, 11);
        tax_Form_Data_S_Recid = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Recid", "S-RECID", FieldType.STRING, 11);
        tax_Form_Data_S_Anum = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Anum", "S-ANUM", FieldType.STRING, 15);
        tax_Form_Data_S_Rec = tax_Form_Data_F1042s_Form_Array.newFieldArrayInGroup("tax_Form_Data_S_Rec", "S-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_S_Q8 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Q8", "S-Q8", FieldType.STRING, 40);
        tax_Form_Data_S_Wit = tax_Form_Data_F1042s_Form_Array.newFieldArrayInGroup("tax_Form_Data_S_Wit", "S-WIT", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_S_Witid = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Witid", "S-WITID", FieldType.STRING, 11);
        tax_Form_Data_S_Pay = tax_Form_Data_F1042s_Form_Array.newFieldArrayInGroup("tax_Form_Data_S_Pay", "S-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            2));
        tax_Form_Data_S_Q12 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Q12", "S-Q12", FieldType.STRING, 14);
        tax_Form_Data_S_Q13 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Q13", "S-Q13", FieldType.STRING, 11);
        tax_Form_Data_S_Q14 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Q14", "S-Q14", FieldType.STRING, 20);
        tax_Form_Data_S_Svoid = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Svoid", "S-SVOID", FieldType.STRING, 1);
        tax_Form_Data_S_Saein = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Saein", "S-SAEIN", FieldType.STRING, 11);
        tax_Form_Data_S_Saeinx = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Saeinx", "S-SAEINX", FieldType.STRING, 1);
        tax_Form_Data_S_Srtin = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Srtin", "S-SRTIN", FieldType.STRING, 11);
        tax_Form_Data_S_Srtinx = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Srtinx", "S-SRTINX", FieldType.STRING, 1);
        tax_Form_Data_S_Sreinx = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Sreinx", "S-SREINX", FieldType.STRING, 1);
        tax_Form_Data_S_Srepaid = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Srepaid", "S-SREPAID", FieldType.STRING, 14);
        tax_Form_Data_S_Expansion1 = tax_Form_Data_F1042s_Form_Array.newFieldInGroup("tax_Form_Data_S_Expansion1", "S-EXPANSION1", FieldType.STRING, 100);
        tax_Form_Data_S_Expansion1Redef10 = tax_Form_Data_F1042s_Form_Array.newGroupInGroup("tax_Form_Data_S_Expansion1Redef10", "Redefines", tax_Form_Data_S_Expansion1);
        tax_Form_Data_S_C3_Box = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C3_Box", "S-C3-BOX", FieldType.STRING, 1);
        tax_Form_Data_S_C4_Box = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C4_Box", "S-C4-BOX", FieldType.STRING, 1);
        tax_Form_Data_S_C4_Exempt = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C4_Exempt", "S-C4-EXEMPT", FieldType.STRING, 3);
        tax_Form_Data_S_C4_Tax_Rate = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C4_Tax_Rate", "S-C4-TAX-RATE", FieldType.STRING, 
            5);
        tax_Form_Data_S_C3_Status_Code = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C3_Status_Code", "S-C3-STATUS-CODE", FieldType.STRING, 
            2);
        tax_Form_Data_S_C4_Status_Code = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C4_Status_Code", "S-C4-STATUS-CODE", FieldType.STRING, 
            2);
        tax_Form_Data_S_C4_Giin = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_C4_Giin", "S-C4-GIIN", FieldType.STRING, 19);
        tax_Form_Data_S_Birthdate = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Birthdate", "S-BIRTHDATE", FieldType.STRING, 11);
        tax_Form_Data_S_Fin_Number = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Fin_Number", "S-FIN-NUMBER", FieldType.STRING, 
            22);
        tax_Form_Data_S_Uin_Number = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Uin_Number", "S-UIN-NUMBER", FieldType.STRING, 
            10);
        tax_Form_Data_S_Amndment_Num = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Amndment_Num", "S-AMNDMENT-NUM", FieldType.STRING, 
            2);
        tax_Form_Data_S_Lob_Code_13j = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Lob_Code_13j", "S-LOB-CODE-13J", FieldType.STRING, 
            2);
        tax_Form_Data_S_Payer_3_Status_Code_16d = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Payer_3_Status_Code_16d", "S-PAYER-3-STATUS-CODE-16D", 
            FieldType.STRING, 2);
        tax_Form_Data_S_Payer_4_Status_Code_16e = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Payer_4_Status_Code_16e", "S-PAYER-4-STATUS-CODE-16E", 
            FieldType.STRING, 2);
        tax_Form_Data_S_Expansion1_Filler = tax_Form_Data_S_Expansion1Redef10.newFieldInGroup("tax_Form_Data_S_Expansion1_Filler", "S-EXPANSION1-FILLER", 
            FieldType.STRING, 16);
        tax_Form_Data_Form_DataRedef11 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef11", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F5498_Form_Array = tax_Form_Data_Form_DataRedef11.newGroupArrayInGroup("tax_Form_Data_F5498_Form_Array", "F5498-FORM-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F5498f_Occurs));
        tax_Form_Data_F_Corr = tax_Form_Data_F5498_Form_Array.newFieldInGroup("tax_Form_Data_F_Corr", "F-CORR", FieldType.STRING, 1);
        tax_Form_Data_F_Tru = tax_Form_Data_F5498_Form_Array.newFieldArrayInGroup("tax_Form_Data_F_Tru", "F-TRU", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_F_Truid = tax_Form_Data_F5498_Form_Array.newFieldInGroup("tax_Form_Data_F_Truid", "F-TRUID", FieldType.STRING, 11);
        tax_Form_Data_F_Pssn = tax_Form_Data_F5498_Form_Array.newFieldInGroup("tax_Form_Data_F_Pssn", "F-PSSN", FieldType.STRING, 11);
        tax_Form_Data_F_Pay = tax_Form_Data_F5498_Form_Array.newFieldArrayInGroup("tax_Form_Data_F_Pay", "F-PAY", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_F_Anum = tax_Form_Data_F5498_Form_Array.newFieldInGroup("tax_Form_Data_F_Anum", "F-ANUM", FieldType.STRING, 15);
        tax_Form_Data_F_Indicators = tax_Form_Data_F5498_Form_Array.newGroupInGroup("tax_Form_Data_F_Indicators", "F-INDICATORS");
        tax_Form_Data_F_Q6a = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6a", "F-Q6A", FieldType.STRING, 1);
        tax_Form_Data_F_Q6b = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6b", "F-Q6B", FieldType.STRING, 1);
        tax_Form_Data_F_Q6c = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6c", "F-Q6C", FieldType.STRING, 1);
        tax_Form_Data_F_Q6d = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6d", "F-Q6D", FieldType.STRING, 1);
        tax_Form_Data_F_Q6e = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6e", "F-Q6E", FieldType.STRING, 1);
        tax_Form_Data_F_Q6f = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6f", "F-Q6F", FieldType.STRING, 1);
        tax_Form_Data_F_Q6_Expansion = tax_Form_Data_F_Indicators.newFieldInGroup("tax_Form_Data_F_Q6_Expansion", "F-Q6-EXPANSION", FieldType.STRING, 
            4);
        tax_Form_Data_F_IndicatorsRedef12 = tax_Form_Data_F5498_Form_Array.newGroupInGroup("tax_Form_Data_F_IndicatorsRedef12", "Redefines", tax_Form_Data_F_Indicators);
        tax_Form_Data_F_Q6_Ind = tax_Form_Data_F_IndicatorsRedef12.newFieldArrayInGroup("tax_Form_Data_F_Q6_Ind", "F-Q6-IND", FieldType.STRING, 1, new 
            DbsArrayController(1,6));
        tax_Form_Data_F_Dollar_Amounts = tax_Form_Data_F5498_Form_Array.newGroupInGroup("tax_Form_Data_F_Dollar_Amounts", "F-DOLLAR-AMOUNTS");
        tax_Form_Data_F_Q1 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q1", "F-Q1", FieldType.STRING, 14);
        tax_Form_Data_F_Q2 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q2", "F-Q2", FieldType.STRING, 14);
        tax_Form_Data_F_Q3 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q3", "F-Q3", FieldType.STRING, 14);
        tax_Form_Data_F_Q4 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q4", "F-Q4", FieldType.STRING, 14);
        tax_Form_Data_F_Q5 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q5", "F-Q5", FieldType.STRING, 14);
        tax_Form_Data_F_Q7 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q7", "F-Q7", FieldType.STRING, 14);
        tax_Form_Data_F_Q8 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q8", "F-Q8", FieldType.STRING, 14);
        tax_Form_Data_F_Q9 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q9", "F-Q9", FieldType.STRING, 14);
        tax_Form_Data_F_Q10 = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Q10", "F-Q10", FieldType.STRING, 14);
        tax_Form_Data_F_Frech = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Frech", "F-FRECH", FieldType.STRING, 14);
        tax_Form_Data_F_Expansion = tax_Form_Data_F_Dollar_Amounts.newFieldInGroup("tax_Form_Data_F_Expansion", "F-EXPANSION", FieldType.STRING, 42);
        tax_Form_Data_F_Dollar_AmountsRedef13 = tax_Form_Data_F5498_Form_Array.newGroupInGroup("tax_Form_Data_F_Dollar_AmountsRedef13", "Redefines", tax_Form_Data_F_Dollar_Amounts);
        tax_Form_Data_F_Dollars = tax_Form_Data_F_Dollar_AmountsRedef13.newFieldArrayInGroup("tax_Form_Data_F_Dollars", "F-DOLLARS", FieldType.STRING, 
            14, new DbsArrayController(1,10));
        tax_Form_Data_F_Expansion1 = tax_Form_Data_F5498_Form_Array.newFieldInGroup("tax_Form_Data_F_Expansion1", "F-EXPANSION1", FieldType.STRING, 100);
        tax_Form_Data_Form_DataRedef14 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef14", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F5498p_Form_Array = tax_Form_Data_Form_DataRedef14.newGroupArrayInGroup("tax_Form_Data_F5498p_Form_Array", "F5498P-FORM-ARRAY", 
            new DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F5498p_Occurs));
        tax_Form_Data_F5498p_Tru = tax_Form_Data_F5498p_Form_Array.newFieldArrayInGroup("tax_Form_Data_F5498p_Tru", "F5498P-TRU", FieldType.STRING, 55, 
            new DbsArrayController(1,int_no_Of_Addr_Lines));
        tax_Form_Data_F5498p_Truid = tax_Form_Data_F5498p_Form_Array.newFieldInGroup("tax_Form_Data_F5498p_Truid", "F5498P-TRUID", FieldType.STRING, 11);
        tax_Form_Data_F5498p_Pssn = tax_Form_Data_F5498p_Form_Array.newFieldInGroup("tax_Form_Data_F5498p_Pssn", "F5498P-PSSN", FieldType.STRING, 11);
        tax_Form_Data_F5498p_Pay = tax_Form_Data_F5498p_Form_Array.newFieldArrayInGroup("tax_Form_Data_F5498p_Pay", "F5498P-PAY", FieldType.STRING, 40, 
            new DbsArrayController(1,int_no_Of_Addr_Lines));
        tax_Form_Data_Coupon_Type = tax_Form_Data_F5498p_Form_Array.newFieldInGroup("tax_Form_Data_Coupon_Type", "COUPON-TYPE", FieldType.STRING, 2);
        tax_Form_Data_Coupon_Year = tax_Form_Data_F5498p_Form_Array.newFieldInGroup("tax_Form_Data_Coupon_Year", "COUPON-YEAR", FieldType.STRING, 4);
        tax_Form_Data_Acct_Info = tax_Form_Data_F5498p_Form_Array.newGroupArrayInGroup("tax_Form_Data_Acct_Info", "ACCT-INFO", new DbsArrayController(1,
            8));
        tax_Form_Data_Contr_Nbr = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Contr_Nbr", "CONTR-NBR", FieldType.STRING, 15);
        tax_Form_Data_Corr_Ind = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Corr_Ind", "CORR-IND", FieldType.STRING, 1);
        tax_Form_Data_Postpn_Yr = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Postpn_Yr", "POSTPN-YR", FieldType.STRING, 4);
        tax_Form_Data_Postpn_Code = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Postpn_Code", "POSTPN-CODE", FieldType.STRING, 2);
        tax_Form_Data_Dollar_Amounts = tax_Form_Data_Acct_Info.newGroupInGroup("tax_Form_Data_Dollar_Amounts", "DOLLAR-AMOUNTS");
        tax_Form_Data_Ira_Contr = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Ira_Contr", "IRA-CONTR", FieldType.STRING, 14);
        tax_Form_Data_Rollov_Contr = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Rollov_Contr", "ROLLOV-CONTR", FieldType.STRING, 14);
        tax_Form_Data_Roth_Conv_Amt = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Roth_Conv_Amt", "ROTH-CONV-AMT", FieldType.STRING, 14);
        tax_Form_Data_Rechar_Contrib = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Rechar_Contrib", "RECHAR-CONTRIB", FieldType.STRING, 
            14);
        tax_Form_Data_Fair_Mkt_Val = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Fair_Mkt_Val", "FAIR-MKT-VAL", FieldType.STRING, 14);
        tax_Form_Data_Roth_Ira_Contrib = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Roth_Ira_Contrib", "ROTH-IRA-CONTRIB", FieldType.STRING, 
            14);
        tax_Form_Data_Sep_Contrib = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Sep_Contrib", "SEP-CONTRIB", FieldType.STRING, 14);
        tax_Form_Data_Postpn_Amt = tax_Form_Data_Dollar_Amounts.newFieldInGroup("tax_Form_Data_Postpn_Amt", "POSTPN-AMT", FieldType.STRING, 14);
        tax_Form_Data_Dollar_AmountsRedef15 = tax_Form_Data_Acct_Info.newGroupInGroup("tax_Form_Data_Dollar_AmountsRedef15", "Redefines", tax_Form_Data_Dollar_Amounts);
        tax_Form_Data_Dollars = tax_Form_Data_Dollar_AmountsRedef15.newFieldArrayInGroup("tax_Form_Data_Dollars", "DOLLARS", FieldType.STRING, 14, new 
            DbsArrayController(1,8));
        tax_Form_Data_Ira_Type_Ind = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Ira_Type_Ind", "IRA-TYPE-IND", FieldType.STRING, 1);
        tax_Form_Data_Mdo_Required_Ind = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Mdo_Required_Ind", "MDO-REQUIRED-IND", FieldType.STRING, 
            1);
        tax_Form_Data_Coup_Primary_Contr_Nbr = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Coup_Primary_Contr_Nbr", "COUP-PRIMARY-CONTR-NBR", 
            FieldType.STRING, 15);
        tax_Form_Data_Coup_Secondary_Contr_Nbr = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Coup_Secondary_Contr_Nbr", "COUP-SECONDARY-CONTR-NBR", 
            FieldType.STRING, 15);
        tax_Form_Data_Coup_Ira_Type_Ind = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Coup_Ira_Type_Ind", "COUP-IRA-TYPE-IND", FieldType.STRING, 
            1);
        tax_Form_Data_Repayments_Amt = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Repayments_Amt", "REPAYMENTS-AMT", FieldType.STRING, 14);
        tax_Form_Data_Repayments_Code = tax_Form_Data_Acct_Info.newFieldInGroup("tax_Form_Data_Repayments_Code", "REPAYMENTS-CODE", FieldType.STRING, 
            2);
        tax_Form_Data_Form_DataRedef16 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef16", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_Fnr4_N_Form_Array = tax_Form_Data_Form_DataRedef16.newGroupArrayInGroup("tax_Form_Data_Fnr4_N_Form_Array", "FNR4-N-FORM-ARRAY", 
            new DbsArrayController(1,int_pnd_Form_Occurs_Pnd_Fnr4n_Occurs));
        tax_Form_Data_N_Q10 = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q10", "N-Q10", FieldType.STRING, 2);
        tax_Form_Data_N_Q11 = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q11", "N-Q11", FieldType.STRING, 3);
        tax_Form_Data_N_Q12a = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q12a", "N-Q12A", FieldType.STRING, 3);
        tax_Form_Data_N_Q12b = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q12b", "N-Q12B", FieldType.STRING, 11);
        tax_Form_Data_N_Q12c = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q12c", "N-Q12C", FieldType.STRING, 15);
        tax_Form_Data_N_Q13 = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Q13", "N-Q13", FieldType.STRING, 13);
        tax_Form_Data_N_Line_Dtls = tax_Form_Data_Fnr4_N_Form_Array.newGroupArrayInGroup("tax_Form_Data_N_Line_Dtls", "N-LINE-DTLS", new DbsArrayController(1,
            2));
        tax_Form_Data_N_Q14 = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Q14", "N-Q14", FieldType.STRING, 3);
        tax_Form_Data_N_Q15 = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Q15", "N-Q15", FieldType.STRING, 3);
        tax_Form_Data_N_Q16 = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Q16", "N-Q16", FieldType.STRING, 14);
        tax_Form_Data_N_Q17 = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Q17", "N-Q17", FieldType.STRING, 14);
        tax_Form_Data_N_Q18 = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Q18", "N-Q18", FieldType.STRING, 3);
        tax_Form_Data_N_Expansion = tax_Form_Data_N_Line_Dtls.newFieldInGroup("tax_Form_Data_N_Expansion", "N-EXPANSION", FieldType.STRING, 20);
        tax_Form_Data_N_Rec = tax_Form_Data_Fnr4_N_Form_Array.newFieldArrayInGroup("tax_Form_Data_N_Rec", "N-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_N_Pay = tax_Form_Data_Fnr4_N_Form_Array.newFieldArrayInGroup("tax_Form_Data_N_Pay", "N-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_N_Expansion1 = tax_Form_Data_Fnr4_N_Form_Array.newFieldInGroup("tax_Form_Data_N_Expansion1", "N-EXPANSION1", FieldType.STRING, 100);
        tax_Form_Data_N_Expansion1Redef17 = tax_Form_Data_Fnr4_N_Form_Array.newGroupInGroup("tax_Form_Data_N_Expansion1Redef17", "Redefines", tax_Form_Data_N_Expansion1);
        tax_Form_Data_N_Corr = tax_Form_Data_N_Expansion1Redef17.newFieldInGroup("tax_Form_Data_N_Corr", "N-CORR", FieldType.STRING, 1);
        tax_Form_Data_N_Expansion2 = tax_Form_Data_N_Expansion1Redef17.newFieldInGroup("tax_Form_Data_N_Expansion2", "N-EXPANSION2", FieldType.STRING, 
            99);
        tax_Form_Data_Form_DataRedef18 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef18", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_Free_Form_Lines = tax_Form_Data_Form_DataRedef18.newFieldArrayInGroup("tax_Form_Data_Free_Form_Lines", "FREE-FORM-LINES", FieldType.STRING, 
            66, new DbsArrayController(1,int_pnd_Form_Occurs_Pnd_Free_Form_Occurs));
        tax_Form_Data_Form_DataRedef19 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef19", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F4806_Form_Array = tax_Form_Data_Form_DataRedef19.newGroupArrayInGroup("tax_Form_Data_F4806_Form_Array", "F4806-FORM-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F4806a_Occurs));
        tax_Form_Data_A_Payid = tax_Form_Data_F4806_Form_Array.newFieldInGroup("tax_Form_Data_A_Payid", "A-PAYID", FieldType.STRING, 11);
        tax_Form_Data_A_Pay = tax_Form_Data_F4806_Form_Array.newFieldArrayInGroup("tax_Form_Data_A_Pay", "A-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_A_Recssn = tax_Form_Data_F4806_Form_Array.newFieldInGroup("tax_Form_Data_A_Recssn", "A-RECSSN", FieldType.STRING, 11);
        tax_Form_Data_A_Rec = tax_Form_Data_F4806_Form_Array.newFieldArrayInGroup("tax_Form_Data_A_Rec", "A-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_A_Amounts_Paid = tax_Form_Data_F4806_Form_Array.newGroupInGroup("tax_Form_Data_A_Amounts_Paid", "A-AMOUNTS-PAID");
        tax_Form_Data_A_Q1 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q1", "A-Q1", FieldType.STRING, 14);
        tax_Form_Data_A_Q2 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q2", "A-Q2", FieldType.STRING, 14);
        tax_Form_Data_A_Q3 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q3", "A-Q3", FieldType.STRING, 14);
        tax_Form_Data_A_Q4 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q4", "A-Q4", FieldType.STRING, 14);
        tax_Form_Data_A_Q5 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q5", "A-Q5", FieldType.STRING, 14);
        tax_Form_Data_A_Q6 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q6", "A-Q6", FieldType.STRING, 14);
        tax_Form_Data_A_Q7 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q7", "A-Q7", FieldType.STRING, 14);
        tax_Form_Data_A_Q8 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q8", "A-Q8", FieldType.STRING, 14);
        tax_Form_Data_A_Q9 = tax_Form_Data_A_Amounts_Paid.newFieldInGroup("tax_Form_Data_A_Q9", "A-Q9", FieldType.STRING, 14);
        tax_Form_Data_A_Expansion1 = tax_Form_Data_A_Amounts_Paid.newFieldArrayInGroup("tax_Form_Data_A_Expansion1", "A-EXPANSION1", FieldType.STRING, 
            14, new DbsArrayController(1,5));
        tax_Form_Data_A_Amounts_PaidRedef20 = tax_Form_Data_F4806_Form_Array.newGroupInGroup("tax_Form_Data_A_Amounts_PaidRedef20", "Redefines", tax_Form_Data_A_Amounts_Paid);
        tax_Form_Data_A_Amounts = tax_Form_Data_A_Amounts_PaidRedef20.newFieldArrayInGroup("tax_Form_Data_A_Amounts", "A-AMOUNTS", FieldType.STRING, 14, 
            new DbsArrayController(1,9));
        tax_Form_Data_A_Q10 = tax_Form_Data_F4806_Form_Array.newFieldInGroup("tax_Form_Data_A_Q10", "A-Q10", FieldType.STRING, 40);
        tax_Form_Data_A_Expansion = tax_Form_Data_F4806_Form_Array.newFieldInGroup("tax_Form_Data_A_Expansion", "A-EXPANSION", FieldType.STRING, 100);
        tax_Form_Data_A_ExpansionRedef21 = tax_Form_Data_F4806_Form_Array.newGroupInGroup("tax_Form_Data_A_ExpansionRedef21", "Redefines", tax_Form_Data_A_Expansion);
        tax_Form_Data_A_Corr = tax_Form_Data_A_ExpansionRedef21.newFieldInGroup("tax_Form_Data_A_Corr", "A-CORR", FieldType.STRING, 1);
        tax_Form_Data_A_Expansion2 = tax_Form_Data_A_ExpansionRedef21.newFieldInGroup("tax_Form_Data_A_Expansion2", "A-EXPANSION2", FieldType.STRING, 
            99);
        tax_Form_Data_Form_DataRedef22 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef22", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F480b_Form_Array = tax_Form_Data_Form_DataRedef22.newGroupArrayInGroup("tax_Form_Data_F480b_Form_Array", "F480B-FORM-ARRAY", new 
            DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F4806b_Occurs));
        tax_Form_Data_B_Payid = tax_Form_Data_F480b_Form_Array.newFieldInGroup("tax_Form_Data_B_Payid", "B-PAYID", FieldType.STRING, 11);
        tax_Form_Data_B_Pay = tax_Form_Data_F480b_Form_Array.newFieldArrayInGroup("tax_Form_Data_B_Pay", "B-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_B_Recssn = tax_Form_Data_F480b_Form_Array.newFieldInGroup("tax_Form_Data_B_Recssn", "B-RECSSN", FieldType.STRING, 11);
        tax_Form_Data_B_Rec = tax_Form_Data_F480b_Form_Array.newFieldArrayInGroup("tax_Form_Data_B_Rec", "B-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_B_Bbnum = tax_Form_Data_F480b_Form_Array.newFieldInGroup("tax_Form_Data_B_Bbnum", "B-BBNUM", FieldType.STRING, 14);
        tax_Form_Data_B_Bcnum = tax_Form_Data_F480b_Form_Array.newFieldInGroup("tax_Form_Data_B_Bcnum", "B-BCNUM", FieldType.STRING, 14);
        tax_Form_Data_B_Amounts_Paid = tax_Form_Data_F480b_Form_Array.newGroupInGroup("tax_Form_Data_B_Amounts_Paid", "B-AMOUNTS-PAID");
        tax_Form_Data_B_Q1p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q1p", "B-Q1P", FieldType.STRING, 14);
        tax_Form_Data_B_Q2p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q2p", "B-Q2P", FieldType.STRING, 14);
        tax_Form_Data_B_Q3p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q3p", "B-Q3P", FieldType.STRING, 14);
        tax_Form_Data_B_Q4p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q4p", "B-Q4P", FieldType.STRING, 14);
        tax_Form_Data_B_Q5p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q5p", "B-Q5P", FieldType.STRING, 14);
        tax_Form_Data_B_Q6p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q6p", "B-Q6P", FieldType.STRING, 14);
        tax_Form_Data_B_Q7p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q7p", "B-Q7P", FieldType.STRING, 14);
        tax_Form_Data_B_Q8p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q8p", "B-Q8P", FieldType.STRING, 14);
        tax_Form_Data_B_Q9p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q9p", "B-Q9P", FieldType.STRING, 14);
        tax_Form_Data_B_Q10p = tax_Form_Data_B_Amounts_Paid.newFieldInGroup("tax_Form_Data_B_Q10p", "B-Q10P", FieldType.STRING, 14);
        tax_Form_Data_B_Expansion1 = tax_Form_Data_B_Amounts_Paid.newFieldArrayInGroup("tax_Form_Data_B_Expansion1", "B-EXPANSION1", FieldType.STRING, 
            14, new DbsArrayController(1,5));
        tax_Form_Data_B_Amounts_PaidRedef23 = tax_Form_Data_F480b_Form_Array.newGroupInGroup("tax_Form_Data_B_Amounts_PaidRedef23", "Redefines", tax_Form_Data_B_Amounts_Paid);
        tax_Form_Data_B_Amounts = tax_Form_Data_B_Amounts_PaidRedef23.newFieldArrayInGroup("tax_Form_Data_B_Amounts", "B-AMOUNTS", FieldType.STRING, 14, 
            new DbsArrayController(1,10));
        tax_Form_Data_B_Amounts_Withheld = tax_Form_Data_F480b_Form_Array.newGroupInGroup("tax_Form_Data_B_Amounts_Withheld", "B-AMOUNTS-WITHHELD");
        tax_Form_Data_B_Q1w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q1w", "B-Q1W", FieldType.STRING, 14);
        tax_Form_Data_B_Q2w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q2w", "B-Q2W", FieldType.STRING, 14);
        tax_Form_Data_B_Q3w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q3w", "B-Q3W", FieldType.STRING, 14);
        tax_Form_Data_B_Q4w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q4w", "B-Q4W", FieldType.STRING, 14);
        tax_Form_Data_B_Q5w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q5w", "B-Q5W", FieldType.STRING, 14);
        tax_Form_Data_B_Q6w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q6w", "B-Q6W", FieldType.STRING, 14);
        tax_Form_Data_B_Q7w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q7w", "B-Q7W", FieldType.STRING, 14);
        tax_Form_Data_B_Q8w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q8w", "B-Q8W", FieldType.STRING, 14);
        tax_Form_Data_B_Q9w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q9w", "B-Q9W", FieldType.STRING, 14);
        tax_Form_Data_B_Q10w = tax_Form_Data_B_Amounts_Withheld.newFieldInGroup("tax_Form_Data_B_Q10w", "B-Q10W", FieldType.STRING, 14);
        tax_Form_Data_B_Expansion2 = tax_Form_Data_B_Amounts_Withheld.newFieldArrayInGroup("tax_Form_Data_B_Expansion2", "B-EXPANSION2", FieldType.STRING, 
            14, new DbsArrayController(1,5));
        tax_Form_Data_B_Amounts_WithheldRedef24 = tax_Form_Data_F480b_Form_Array.newGroupInGroup("tax_Form_Data_B_Amounts_WithheldRedef24", "Redefines", 
            tax_Form_Data_B_Amounts_Withheld);
        tax_Form_Data_B_Amounts_With = tax_Form_Data_B_Amounts_WithheldRedef24.newFieldArrayInGroup("tax_Form_Data_B_Amounts_With", "B-AMOUNTS-WITH", 
            FieldType.STRING, 14, new DbsArrayController(1,10));
        tax_Form_Data_B_Expansion = tax_Form_Data_F480b_Form_Array.newFieldInGroup("tax_Form_Data_B_Expansion", "B-EXPANSION", FieldType.STRING, 100);
        tax_Form_Data_B_ExpansionRedef25 = tax_Form_Data_F480b_Form_Array.newGroupInGroup("tax_Form_Data_B_ExpansionRedef25", "Redefines", tax_Form_Data_B_Expansion);
        tax_Form_Data_B_Corr = tax_Form_Data_B_ExpansionRedef25.newFieldInGroup("tax_Form_Data_B_Corr", "B-CORR", FieldType.STRING, 1);
        tax_Form_Data_B_Expansion3 = tax_Form_Data_B_ExpansionRedef25.newFieldInGroup("tax_Form_Data_B_Expansion3", "B-EXPANSION3", FieldType.STRING, 
            99);
        tax_Form_Data_Form_DataRedef26 = tax_Form_Data_Tax_Form_Data_ArrayRedef2.newGroupInGroup("tax_Form_Data_Form_DataRedef26", "Redefines", tax_Form_Data_Form_Data);
        tax_Form_Data_F4807c_Form_Array = tax_Form_Data_Form_DataRedef26.newGroupArrayInGroup("tax_Form_Data_F4807c_Form_Array", "F4807C-FORM-ARRAY", 
            new DbsArrayController(1,int_pnd_Form_Occurs_Pnd_F4807c_Occurs));
        tax_Form_Data_P_Corr = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Corr", "P-CORR", FieldType.STRING, 1);
        tax_Form_Data_P_Payid = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Payid", "P-PAYID", FieldType.STRING, 11);
        tax_Form_Data_P_Pay = tax_Form_Data_F4807c_Form_Array.newFieldArrayInGroup("tax_Form_Data_P_Pay", "P-PAY", FieldType.STRING, 55, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_P_Recssn = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Recssn", "P-RECSSN", FieldType.STRING, 11);
        tax_Form_Data_P_Ein = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Ein", "P-EIN", FieldType.STRING, 9);
        tax_Form_Data_P_Plan_Name = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Plan_Name", "P-PLAN-NAME", FieldType.STRING, 40);
        tax_Form_Data_P_Plan_Sponsor = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Plan_Sponsor", "P-PLAN-SPONSOR", FieldType.STRING, 
            40);
        tax_Form_Data_P_Rec = tax_Form_Data_F4807c_Form_Array.newFieldArrayInGroup("tax_Form_Data_P_Rec", "P-REC", FieldType.STRING, 40, new DbsArrayController(1,
            int_no_Of_Addr_Lines));
        tax_Form_Data_P_Lump_Ind = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Lump_Ind", "P-LUMP-IND", FieldType.STRING, 1);
        tax_Form_Data_P_Anum = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Anum", "P-ANUM", FieldType.STRING, 15);
        tax_Form_Data_P_Cnum = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Cnum", "P-CNUM", FieldType.STRING, 15);
        tax_Form_Data_P_Dcode = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Dcode", "P-DCODE", FieldType.STRING, 1);
        tax_Form_Data_P_Amounts = tax_Form_Data_F4807c_Form_Array.newGroupInGroup("tax_Form_Data_P_Amounts", "P-AMOUNTS");
        tax_Form_Data_P_Acost = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Acost", "P-ACOST", FieldType.STRING, 15);
        tax_Form_Data_P_Adist = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Adist", "P-ADIST", FieldType.STRING, 15);
        tax_Form_Data_P_Taxa = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Taxa", "P-TAXA", FieldType.STRING, 15);
        tax_Form_Data_P_Dfamt = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Dfamt", "P-DFAMT", FieldType.STRING, 15);
        tax_Form_Data_P_Roll_Cont = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Roll_Cont", "P-ROLL-CONT", FieldType.STRING, 15);
        tax_Form_Data_P_Roll_Dist = tax_Form_Data_P_Amounts.newFieldInGroup("tax_Form_Data_P_Roll_Dist", "P-ROLL-DIST", FieldType.STRING, 15);
        tax_Form_Data_P_Amend_Date = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Amend_Date", "P-AMEND-DATE", FieldType.STRING, 10);
        tax_Form_Data_P_Others = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Others", "P-OTHERS", FieldType.STRING, 15);
        tax_Form_Data_P_Total = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Total", "P-TOTAL", FieldType.STRING, 15);
        tax_Form_Data_P_Efile_Confirm_Nbr = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Efile_Confirm_Nbr", "P-EFILE-CONFIRM-NBR", 
            FieldType.STRING, 11);
        tax_Form_Data_P_Periodic_Ind = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Periodic_Ind", "P-PERIODIC-IND", FieldType.STRING, 
            1);
        tax_Form_Data_P_Partial_Ind = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Partial_Ind", "P-PARTIAL-IND", FieldType.STRING, 
            1);
        tax_Form_Data_P_Annty_Ind = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Annty_Ind", "P-ANNTY-IND", FieldType.STRING, 1);
        tax_Form_Data_P_Qualpvt_Ind = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Qualpvt_Ind", "P-QUALPVT-IND", FieldType.STRING, 
            1);
        tax_Form_Data_P_Orgcnum = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Orgcnum", "P-ORGCNUM", FieldType.STRING, 10);
        tax_Form_Data_P_Adjresn = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Adjresn", "P-ADJRESN", FieldType.STRING, 40);
        tax_Form_Data_P_Exmpt_Dis = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Exmpt_Dis", "P-EXMPT-DIS", FieldType.STRING, 15);
        tax_Form_Data_P_Taxa_Dis = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Taxa_Dis", "P-TAXA-DIS", FieldType.STRING, 15);
        tax_Form_Data_P_Aftax_Dis = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Aftax_Dis", "P-AFTAX-DIS", FieldType.STRING, 15);
        tax_Form_Data_P_Total_Dis = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Total_Dis", "P-TOTAL-DIS", FieldType.STRING, 15);
        tax_Form_Data_P_Fedtx_Dis = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Fedtx_Dis", "P-FEDTX-DIS", FieldType.STRING, 15);
        tax_Form_Data_P_Pension_Date = tax_Form_Data_F4807c_Form_Array.newFieldInGroup("tax_Form_Data_P_Pension_Date", "P-PENSION-DATE", FieldType.STRING, 
            10);

        this.setRecordName("LdaTwrl6345");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Version.setInitialValue("1");
        pnd_Form_Constant_Pnd_Form_1099_Int.setInitialValue("1");
        pnd_Form_Constant_Pnd_Form_1099_R.setInitialValue("2");
        pnd_Form_Constant_Pnd_Form_1042_S.setInitialValue("3");
        pnd_Form_Constant_Pnd_Form_4806_A.setInitialValue("4");
        pnd_Form_Constant_Pnd_Form_5498_F.setInitialValue("5");
        pnd_Form_Constant_Pnd_Form_Nr4_N.setInitialValue("6");
        pnd_Form_Constant_Pnd_Form_Freeform.setInitialValue("7");
        pnd_Form_Constant_Pnd_Form_4806_B.setInitialValue("8");
        pnd_Form_Constant_Pnd_Form_5498_P.setInitialValue("9");
        pnd_Form_Constant_Pnd_Form_4807_C.setInitialValue("A");
        pnd_Form_Constant_Pnd_Form_1099_S.setInitialValue("B");
        pnd_Form_Occurs_Pnd_F1099i_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_F1099r_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_F1042s_Occurs.setInitialValue(3);
        pnd_Form_Occurs_Pnd_F5498f_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_F5498p_Occurs.setInitialValue(2);
        pnd_Form_Occurs_Pnd_F4806a_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_F4806b_Occurs.setInitialValue(3);
        pnd_Form_Occurs_Pnd_F4807c_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_Fnr4n_Occurs.setInitialValue(4);
        pnd_Form_Occurs_Pnd_Free_Form_Occurs.setInitialValue(60);
        no_Of_Addr_Lines.setInitialValue(7);
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        cover_Letter_Data_Cover_Rec_Id.setInitialValue("a");
        cover_Letter_Data_Cover_Data_Lgth.setInitialValue(4465);
        cover_Letter_Data_Cover_Data_Occurs.setInitialValue(1);
        tax_Form_Data_Tax_Form_Rec_Id.setInitialValue("b");
        tax_Form_Data_Tax_Form_Data_Lgth.setInitialValue(4484);
        tax_Form_Data_Tax_Form_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaTwrl6345() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
