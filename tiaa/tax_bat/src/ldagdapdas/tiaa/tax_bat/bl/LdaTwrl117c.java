/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:21 PM
**        * FROM NATURAL LDA     : TWRL117C
************************************************************
**        * FILE NAME            : LdaTwrl117c.java
**        * CLASS NAME           : LdaTwrl117c
**        * INSTANCE NAME        : LdaTwrl117c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl117c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl117c;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Control_Number;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Filler;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Control_Nbr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Form_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Rec_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Doc_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Filler2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Year;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill3;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Filler3;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Payers_Information;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Id;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Town;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Ext;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Filler4;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Payee_Information;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Acct;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Town;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Ext;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Filler5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_20;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Rs_10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Nd_Ira_10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Non_Resdt;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill15;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill16;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Distr_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Qlfd_Roll;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_Dist;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill17;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill18;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Othr_Distr_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill6;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mid_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mm_Last_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Non_Qa_Plan;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Ann;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Ein;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Plan_Nm;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_A_Exempt;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_B_Taxable;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_E_Total;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Amt_Dist_Exempt_Incm;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill7;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill8;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill9;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill11;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill12;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Fill13;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Gov_Ret_Fund;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_App;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Cntl_Nbr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Reason_For_Chg;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_Amended_Dte;
    private DbsGroup pnd_Twrl117cRedef4;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Control_Number;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Filler;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Control_Nbr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Filler1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Form_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Filler2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Filler3;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Ext;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Filler4;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill6;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill7;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill8;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill9;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill11;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill12;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Fill13;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Reason_For_Chg;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_S_Amended_Date;
    private DbsGroup pnd_Twrl117cRedef7;
    private DbsField filler01;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Control_Nbr;
    private DbsField filler02;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Form_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Rec_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Doc_Type;
    private DbsField filler03;
    private DbsField filler04;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Tax_Year;
    private DbsField filler05;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Indentification_No;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Busins_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Town;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Ext;
    private DbsField filler06;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_A_Town;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_A_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_A_Zip;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Ext;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Agent_Email;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Other_Distribution;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Total;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Total_Forms;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill3;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill4;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill6;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill7;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fill8;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg;
    private DbsField pnd_Twrl117c_Pnd_Twrl117t_Fillr1a;
    private DbsGroup pnd_Twrl117cRedef11;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1a;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1b;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1c;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1d;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1e;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1f;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1g;
    private DbsField pnd_Twrl117c_Pnd_Twrl117a_1_Filler;
    private DbsGroup pnd_Twrl117cRedef12;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Rec_Id;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Tax_Year;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Ein;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Frm_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Est_Num;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_File_Type;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Filler1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Emp_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Loc_Add;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Del_Add;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_City;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Zip;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Zip1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Zip2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Filler2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Cntct_Email;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id;
    private DbsField pnd_Twrl117c_Pnd_Twrl117p_Filler3;
    private DbsGroup pnd_Twrl117cRedef14;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Rec_Id;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Resub_Ind;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Software_Cde;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Comp_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Comp_Loc;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_City;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Zip1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Zip2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Fill2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_City;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_State;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full;
    private DbsGroup pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip2;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Fill3;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Contact_Name;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Fill4;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Contact_Email;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Fill5;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Contact_Fax;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Prepares_Code;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ;
    private DbsField pnd_Twrl117c_Pnd_Twrl117h_Fill6;

    public DbsGroup getPnd_Twrl117c() { return pnd_Twrl117c; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Control_Number() { return pnd_Twrl117c_Pnd_Twrl117a_Control_Number; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1() { return pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Filler() { return pnd_Twrl117c_Pnd_Twrl117a_Filler; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Control_Nbr() { return pnd_Twrl117c_Pnd_Twrl117a_Control_Nbr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee() { return pnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Form_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Form_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Rec_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Rec_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Doc_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Doc_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Filler2() { return pnd_Twrl117c_Pnd_Twrl117a_Filler2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Year() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Year; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill3() { return pnd_Twrl117c_Pnd_Twrl117a_Fill3; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Filler3() { return pnd_Twrl117c_Pnd_Twrl117a_Filler3; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Payers_Information() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Information; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Id() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Id; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Address_2() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Town() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Town; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_State() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Zip() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Ext() { return pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Ext; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Filler4() { return pnd_Twrl117c_Pnd_Twrl117a_Filler4; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Payee_Information() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Information; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Acct() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Acct; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Town() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Town; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_State() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Ext() { return pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Ext; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Filler5() { return pnd_Twrl117c_Pnd_Twrl117a_Filler5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr() { return pnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type() { return pnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr() { return pnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr() { return pnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost() { return pnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_20() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_20; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_10() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Rs_10() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Rs_10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Nd_Ira_10() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Nd_Ira_10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Non_Resdt() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Non_Resdt; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt() { return pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt() { return pnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt() { return pnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn() { return pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill5() { return pnd_Twrl117c_Pnd_Twrl117a_Fill5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill15() { return pnd_Twrl117c_Pnd_Twrl117a_Fill15; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax() { return pnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill16() { return pnd_Twrl117c_Pnd_Twrl117a_Fill16; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Distr_Cde() { return pnd_Twrl117c_Pnd_Twrl117a_Distr_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Qlfd_Roll() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Qlfd_Roll; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_Dist() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_Dist; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill17() { return pnd_Twrl117c_Pnd_Twrl117a_Fill17; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_10() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill18() { return pnd_Twrl117c_Pnd_Twrl117a_Fill18; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Othr_Distr_Cde() { return pnd_Twrl117c_Pnd_Twrl117a_Othr_Distr_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill6() { return pnd_Twrl117c_Pnd_Twrl117a_Fill6; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payeee_Mid_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mid_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Payeee_Mm_Last_Name() { return pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mm_Last_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Non_Qa_Plan() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Non_Qa_Plan; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Ann() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Ann; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Ein() { return pnd_Twrl117c_Pnd_Twrl117a_Ein; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Plan_Nm() { return pnd_Twrl117c_Pnd_Twrl117a_Plan_Nm; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr() { return pnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_A_Exempt() { return pnd_Twrl117c_Pnd_Twrl117a_A_Exempt; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_B_Taxable() { return pnd_Twrl117c_Pnd_Twrl117a_B_Taxable; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep() { return pnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr() { return pnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_E_Total() { return pnd_Twrl117c_Pnd_Twrl117a_E_Total; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Amt_Dist_Exempt_Incm() { return pnd_Twrl117c_Pnd_Twrl117a_Amt_Dist_Exempt_Incm; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill7() { return pnd_Twrl117c_Pnd_Twrl117a_Fill7; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill8() { return pnd_Twrl117c_Pnd_Twrl117a_Fill8; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill9() { return pnd_Twrl117c_Pnd_Twrl117a_Fill9; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill10() { return pnd_Twrl117c_Pnd_Twrl117a_Fill10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill11() { return pnd_Twrl117c_Pnd_Twrl117a_Fill11; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill12() { return pnd_Twrl117c_Pnd_Twrl117a_Fill12; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Fill13() { return pnd_Twrl117c_Pnd_Twrl117a_Fill13; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Gov_Ret_Fund() { return pnd_Twrl117c_Pnd_Twrl117a_Gov_Ret_Fund; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_App() { return pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_App; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn() { return pnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Cntl_Nbr() { return pnd_Twrl117c_Pnd_Twrl117a_Cntl_Nbr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Reason_For_Chg() { return pnd_Twrl117c_Pnd_Twrl117a_Reason_For_Chg; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_Amended_Dte() { return pnd_Twrl117c_Pnd_Twrl117a_Amended_Dte; }

    public DbsGroup getPnd_Twrl117cRedef4() { return pnd_Twrl117cRedef4; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Control_Number() { return pnd_Twrl117c_Pnd_Twrl117a_S_Control_Number; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5() { return pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Filler() { return pnd_Twrl117c_Pnd_Twrl117a_S_Filler; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Control_Nbr() { return pnd_Twrl117c_Pnd_Twrl117a_S_Control_Nbr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Filler1() { return pnd_Twrl117c_Pnd_Twrl117a_S_Filler1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Form_Type() { return pnd_Twrl117c_Pnd_Twrl117a_S_Form_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type() { return pnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type() { return pnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Filler2() { return pnd_Twrl117c_Pnd_Twrl117a_S_Filler2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year() { return pnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Filler3() { return pnd_Twrl117c_Pnd_Twrl117a_S_Filler3; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_2() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_State() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Ext() { return pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Ext; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Filler4() { return pnd_Twrl117c_Pnd_Twrl117a_S_Filler4; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc() { return pnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld() { return pnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid() { return pnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type() { return pnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill5() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill6() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill6; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill7() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill7; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill8() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill8; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill9() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill9; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill10() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill11() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill11; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill12() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill12; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Fill13() { return pnd_Twrl117c_Pnd_Twrl117a_S_Fill13; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr() { return pnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Reason_For_Chg() { return pnd_Twrl117c_Pnd_Twrl117a_S_Reason_For_Chg; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_S_Amended_Date() { return pnd_Twrl117c_Pnd_Twrl117a_S_Amended_Date; }

    public DbsGroup getPnd_Twrl117cRedef7() { return pnd_Twrl117cRedef7; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Control_Nbr() { return pnd_Twrl117c_Pnd_Twrl117t_Control_Nbr; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Form_Type() { return pnd_Twrl117c_Pnd_Twrl117t_Form_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Rec_Type() { return pnd_Twrl117c_Pnd_Twrl117t_Rec_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Doc_Type() { return pnd_Twrl117c_Pnd_Twrl117t_Doc_Type; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Tax_Year() { return pnd_Twrl117c_Pnd_Twrl117t_Tax_Year; }

    public DbsField getFiller05() { return filler05; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm() { return pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type() { return pnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins() { return pnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Indentification_No() { return pnd_Twrl117c_Pnd_Twrl117t_Indentification_No; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Busins_Name() { return pnd_Twrl117c_Pnd_Twrl117t_Busins_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme() { return pnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Town() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Town; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_State() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Ext() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Ext; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1() { return pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2() { return pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_A_Town() { return pnd_Twrl117c_Pnd_Twrl117t_A_Town; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_A_State() { return pnd_Twrl117c_Pnd_Twrl117t_A_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10() { return pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_A_Zip() { return pnd_Twrl117c_Pnd_Twrl117t_A_Zip; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_A_Zip_Ext() { return pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Ext; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address() { return pnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Agent_Email() { return pnd_Twrl117c_Pnd_Twrl117t_Agent_Email; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld() { return pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov() { return pnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20() { return pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10() { return pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified() { return pnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10() { return pnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost() { return pnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira() { return pnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10() { return pnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10() { return pnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri() { return pnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Other_Distribution() { return pnd_Twrl117c_Pnd_Twrl117t_Other_Distribution; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria() { return pnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Total() { return pnd_Twrl117c_Pnd_Twrl117t_Total; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Total_Forms() { return pnd_Twrl117c_Pnd_Twrl117t_Total_Forms; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill1() { return pnd_Twrl117c_Pnd_Twrl117t_Fill1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill2() { return pnd_Twrl117c_Pnd_Twrl117t_Fill2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill3() { return pnd_Twrl117c_Pnd_Twrl117t_Fill3; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill4() { return pnd_Twrl117c_Pnd_Twrl117t_Fill4; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill5() { return pnd_Twrl117c_Pnd_Twrl117t_Fill5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill6() { return pnd_Twrl117c_Pnd_Twrl117t_Fill6; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill7() { return pnd_Twrl117c_Pnd_Twrl117t_Fill7; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fill8() { return pnd_Twrl117c_Pnd_Twrl117t_Fill8; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg() { return pnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117t_Fillr1a() { return pnd_Twrl117c_Pnd_Twrl117t_Fillr1a; }

    public DbsGroup getPnd_Twrl117cRedef11() { return pnd_Twrl117cRedef11; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1a() { return pnd_Twrl117c_Pnd_Twrl117a_1a; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1b() { return pnd_Twrl117c_Pnd_Twrl117a_1b; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1c() { return pnd_Twrl117c_Pnd_Twrl117a_1c; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1d() { return pnd_Twrl117c_Pnd_Twrl117a_1d; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1e() { return pnd_Twrl117c_Pnd_Twrl117a_1e; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1f() { return pnd_Twrl117c_Pnd_Twrl117a_1f; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1g() { return pnd_Twrl117c_Pnd_Twrl117a_1g; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117a_1_Filler() { return pnd_Twrl117c_Pnd_Twrl117a_1_Filler; }

    public DbsGroup getPnd_Twrl117cRedef12() { return pnd_Twrl117cRedef12; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Rec_Id() { return pnd_Twrl117c_Pnd_Twrl117p_Rec_Id; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Tax_Year() { return pnd_Twrl117c_Pnd_Twrl117p_Tax_Year; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde() { return pnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Ein() { return pnd_Twrl117c_Pnd_Twrl117p_Ein; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Frm_Type() { return pnd_Twrl117c_Pnd_Twrl117p_Frm_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Est_Num() { return pnd_Twrl117c_Pnd_Twrl117p_Est_Num; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_File_Type() { return pnd_Twrl117c_Pnd_Twrl117p_File_Type; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Filler1() { return pnd_Twrl117c_Pnd_Twrl117p_Filler1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Emp_Name() { return pnd_Twrl117c_Pnd_Twrl117p_Emp_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Loc_Add() { return pnd_Twrl117c_Pnd_Twrl117p_Loc_Add; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Del_Add() { return pnd_Twrl117c_Pnd_Twrl117p_Del_Add; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_City() { return pnd_Twrl117c_Pnd_Twrl117p_City; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_State() { return pnd_Twrl117c_Pnd_Twrl117p_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Zip() { return pnd_Twrl117c_Pnd_Twrl117p_Zip; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117p_ZipRedef13() { return pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Zip1() { return pnd_Twrl117c_Pnd_Twrl117p_Zip1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Zip2() { return pnd_Twrl117c_Pnd_Twrl117p_Zip2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Filler2() { return pnd_Twrl117c_Pnd_Twrl117p_Filler2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc() { return pnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde() { return pnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde() { return pnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Cntct_Email() { return pnd_Twrl117c_Pnd_Twrl117p_Cntct_Email; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id() { return pnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117p_Filler3() { return pnd_Twrl117c_Pnd_Twrl117p_Filler3; }

    public DbsGroup getPnd_Twrl117cRedef14() { return pnd_Twrl117cRedef14; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Rec_Id() { return pnd_Twrl117c_Pnd_Twrl117h_Rec_Id; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein() { return pnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Resub_Ind() { return pnd_Twrl117c_Pnd_Twrl117h_Resub_Ind; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Software_Cde() { return pnd_Twrl117c_Pnd_Twrl117h_Software_Cde; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Comp_Name() { return pnd_Twrl117c_Pnd_Twrl117h_Comp_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Comp_Loc() { return pnd_Twrl117c_Pnd_Twrl117h_Comp_Loc; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr() { return pnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_City() { return pnd_Twrl117c_Pnd_Twrl117h_City; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_State() { return pnd_Twrl117c_Pnd_Twrl117h_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117h_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15() { return pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Zip1() { return pnd_Twrl117c_Pnd_Twrl117h_Zip1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Zip2() { return pnd_Twrl117c_Pnd_Twrl117h_Zip2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Fill2() { return pnd_Twrl117c_Pnd_Twrl117h_Fill2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_City() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_City; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_State() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_State; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full; }

    public DbsGroup getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip1() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip2() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip2; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Fill3() { return pnd_Twrl117c_Pnd_Twrl117h_Fill3; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1() { return pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Contact_Name() { return pnd_Twrl117c_Pnd_Twrl117h_Contact_Name; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Contact_Phone() { return pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext() { return pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Fill4() { return pnd_Twrl117c_Pnd_Twrl117h_Fill4; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Contact_Email() { return pnd_Twrl117c_Pnd_Twrl117h_Contact_Email; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Fill5() { return pnd_Twrl117c_Pnd_Twrl117h_Fill5; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Contact_Fax() { return pnd_Twrl117c_Pnd_Twrl117h_Contact_Fax; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice() { return pnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Prepares_Code() { return pnd_Twrl117c_Pnd_Twrl117h_Prepares_Code; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ() { return pnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ; }

    public DbsField getPnd_Twrl117c_Pnd_Twrl117h_Fill6() { return pnd_Twrl117c_Pnd_Twrl117h_Fill6; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl117c = newGroupInRecord("pnd_Twrl117c", "#TWRL117C");
        pnd_Twrl117c_Pnd_Twrl117a_Control_Number = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Control_Number", "#TWRL117A-CONTROL-NUMBER", 
            FieldType.STRING, 10);
        pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1 = pnd_Twrl117c.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1", "Redefines", pnd_Twrl117c_Pnd_Twrl117a_Control_Number);
        pnd_Twrl117c_Pnd_Twrl117a_Filler = pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Filler", "#TWRL117A-FILLER", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_Control_Nbr = pnd_Twrl117c_Pnd_Twrl117a_Control_NumberRedef1.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Control_Nbr", 
            "#TWRL117A-CONTROL-NBR", FieldType.NUMERIC, 8);
        pnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Type_Id_Payee", "#TWRL117A-TYPE-ID-PAYEE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Resident_Type", "#TWRL117A-PAYEE-RESIDENT-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Form_Type = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Form_Type", "#TWRL117A-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_Rec_Type = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Rec_Type", "#TWRL117A-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_Doc_Type = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Doc_Type", "#TWRL117A-DOC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_Filler2 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Filler2", "#TWRL117A-FILLER2", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Year = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Year", "#TWRL117A-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl117c_Pnd_Twrl117a_Fill3 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill3", "#TWRL117A-FILL3", FieldType.STRING, 8);
        pnd_Twrl117c_Pnd_Twrl117a_Filler3 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Filler3", "#TWRL117A-FILLER3", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Information = pnd_Twrl117c.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Information", "#TWRL117A-PAYERS-INFORMATION");
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Id_Type", 
            "#TWRL117A-PAYERS-ID-TYPE", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Id = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Id", "#TWRL117A-PAYERS-ID", 
            FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Name = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Name", 
            "#TWRL117A-PAYERS-NAME", FieldType.STRING, 30);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1 = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_1", 
            "#TWRL117A-PAYERS-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_2 = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Address_2", 
            "#TWRL117A-PAYERS-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Town = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Town", 
            "#TWRL117A-PAYERS-TOWN", FieldType.STRING, 13);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_State = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_State", 
            "#TWRL117A-PAYERS-STATE", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full", 
            "#TWRL117A-PAYERS-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2 = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip = pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip", 
            "#TWRL117A-PAYERS-ZIP", FieldType.NUMERIC, 5);
        pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Ext = pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payers_Zip_Ext", 
            "#TWRL117A-PAYERS-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117c_Pnd_Twrl117a_Filler4 = pnd_Twrl117c_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Filler4", "#TWRL117A-FILLER4", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Information = pnd_Twrl117c.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Information", "#TWRL117A-PAYEE-INFORMATION");
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Ssn", "#TWRL117A-PAYEE-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Acct = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Acct", "#TWRL117A-PAYEE-ACCT", 
            FieldType.STRING, 20);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Name = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Name", "#TWRL117A-PAYEE-NAME", 
            FieldType.STRING, 30);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_1", 
            "#TWRL117A-PAYEE-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Address_2", 
            "#TWRL117A-PAYEE-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Town = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Town", "#TWRL117A-PAYEE-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_State = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_State", "#TWRL117A-PAYEE-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full", 
            "#TWRL117A-PAYEE-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip = pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip", "#TWRL117A-PAYEE-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Ext = pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_FullRedef3.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payee_Zip_Ext", 
            "#TWRL117A-PAYEE-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117c_Pnd_Twrl117a_Filler5 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Filler5", "#TWRL117A-FILLER5", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Form_Of_Distr", 
            "#TWRL117A-FORM-OF-DISTR", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Plan_Or_Ann_Type", 
            "#TWRL117A-PLAN-OR-ANN-TYPE", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Rollover_Contr", 
            "#TWRL117A-ROLLOVER-CONTR", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Rollover_Distr", 
            "#TWRL117A-ROLLOVER-DISTR", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Annuity_Cost", 
            "#TWRL117A-ANNUITY-COST", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_20 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_20", 
            "#TWRL117A-TAX-WHLD-LS-20", FieldType.NUMERIC, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_10 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Ls_10", 
            "#TWRL117A-TAX-WHLD-LS-10", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Rs_10 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Rs_10", 
            "#TWRL117A-TAX-WHLD-RS-10", FieldType.NUMERIC, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Nd_Ira_10 = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Nd_Ira_10", 
            "#TWRL117A-TAX-WHLD-ND-IRA-10", FieldType.NUMERIC, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Non_Resdt = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Non_Resdt", 
            "#TWRL117A-TAX-WHLD-NON-RESDT", FieldType.NUMERIC, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt", "#TWRL117A-DISTR-AMT", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Prepaid_Amt", "#TWRL117A-PREPAID-AMT", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt = pnd_Twrl117c_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Taxable_Amt", "#TWRL117A-TAXABLE-AMT", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn = pnd_Twrl117c.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn", "#TWRL117A-DISTR-AMT-BRKDN");
        pnd_Twrl117c_Pnd_Twrl117a_Fill5 = pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill5", "#TWRL117A-FILL5", 
            FieldType.STRING, 24);
        pnd_Twrl117c_Pnd_Twrl117a_Fill15 = pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill15", "#TWRL117A-FILL15", 
            FieldType.STRING, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax = pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Contr_Aft_Tax", 
            "#TWRL117A-CONTR-AFT-TAX", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Fill16 = pnd_Twrl117c_Pnd_Twrl117a_Distr_Amt_Brkdn.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill16", "#TWRL117A-FILL16", 
            FieldType.STRING, 24);
        pnd_Twrl117c_Pnd_Twrl117a_Distr_Cde = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Distr_Cde", "#TWRL117A-DISTR-CDE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Qlfd_Roll = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Qlfd_Roll", "#TWRL117A-TAX-WHLD-QLFD-ROLL", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_Dist = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_Dist", "#TWRL117A-TAX-WHLD-OTHER-DIST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Fill17 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill17", "#TWRL117A-FILL17", FieldType.STRING, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_10 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Whld_Other_10", "#TWRL117A-TAX-WHLD-OTHER-10", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Fill18 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill18", "#TWRL117A-FILL18", FieldType.NUMERIC, 24);
        pnd_Twrl117c_Pnd_Twrl117a_Othr_Distr_Cde = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Othr_Distr_Cde", "#TWRL117A-OTHR-DISTR-CDE", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_Fill6 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill6", "#TWRL117A-FILL6", FieldType.STRING, 161);
        pnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payeee_First_Name", "#TWRL117A-PAYEEE-FIRST-NAME", 
            FieldType.STRING, 15);
        pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mid_Name = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mid_Name", "#TWRL117A-PAYEEE-MID-NAME", 
            FieldType.STRING, 15);
        pnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payeee_Last_Name", "#TWRL117A-PAYEEE-LAST-NAME", 
            FieldType.STRING, 20);
        pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mm_Last_Name = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Payeee_Mm_Last_Name", "#TWRL117A-PAYEEE-MM-LAST-NAME", 
            FieldType.STRING, 20);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Non_Qa_Plan = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Non_Qa_Plan", "#TWRL117A-TAX-WTHLD-NON-QA-PLAN", 
            FieldType.NUMERIC, 12);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Ann = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_Ann", "#TWRL117A-TAX-WTHLD-ANN", FieldType.NUMERIC, 
            12);
        pnd_Twrl117c_Pnd_Twrl117a_Ein = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Ein", "#TWRL117A-EIN", FieldType.STRING, 9);
        pnd_Twrl117c_Pnd_Twrl117a_Plan_Nm = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Plan_Nm", "#TWRL117A-PLAN-NM", FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Plan_Spnsr", "#TWRL117A-PLAN-SPNSR", FieldType.STRING, 
            40);
        pnd_Twrl117c_Pnd_Twrl117a_A_Exempt = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_A_Exempt", "#TWRL117A-A-EXEMPT", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_B_Taxable = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_B_Taxable", "#TWRL117A-B-TAXABLE", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_C_Amt_Prep", "#TWRL117A-C-AMT-PREP", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_D_Aft_Tax_Cntr", "#TWRL117A-D-AFT-TAX-CNTR", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_E_Total = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_E_Total", "#TWRL117A-E-TOTAL", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Wth_Elg_Dist", "#TWRL117A-TAX-WTH-ELG-DIST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Amt_Dist_Exempt_Incm = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Amt_Dist_Exempt_Incm", "#TWRL117A-AMT-DIST-EXEMPT-INCM", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Fill7 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill7", "#TWRL117A-FILL7", FieldType.STRING, 5);
        pnd_Twrl117c_Pnd_Twrl117a_Fill8 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill8", "#TWRL117A-FILL8", FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_Fill9 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill9", "#TWRL117A-FILL9", FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_Fill10 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill10", "#TWRL117A-FILL10", FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_Fill11 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill11", "#TWRL117A-FILL11", FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_Fill12 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill12", "#TWRL117A-FILL12", FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_Fill13 = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Fill13", "#TWRL117A-FILL13", FieldType.STRING, 130);
        pnd_Twrl117c_Pnd_Twrl117a_Gov_Ret_Fund = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Gov_Ret_Fund", "#TWRL117A-GOV-RET-FUND", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_App = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Tax_Wthld_App", "#TWRL117A-TAX-WTHLD-APP", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Dte_Receive_Pnsn", "#TWRL117A-DTE-RECEIVE-PNSN", 
            FieldType.STRING, 8);
        pnd_Twrl117c_Pnd_Twrl117a_Cntl_Nbr = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Cntl_Nbr", "#TWRL117A-CNTL-NBR", FieldType.STRING, 
            9);
        pnd_Twrl117c_Pnd_Twrl117a_Reason_For_Chg = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Reason_For_Chg", "#TWRL117A-REASON-FOR-CHG", 
            FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117a_Amended_Dte = pnd_Twrl117c.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_Amended_Dte", "#TWRL117A-AMENDED-DTE", FieldType.NUMERIC, 
            6);
        pnd_Twrl117cRedef4 = newGroupInRecord("pnd_Twrl117cRedef4", "Redefines", pnd_Twrl117c);
        pnd_Twrl117c_Pnd_Twrl117a_S_Control_Number = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Control_Number", "#TWRL117A-S-CONTROL-NUMBER", 
            FieldType.STRING, 10);
        pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5 = pnd_Twrl117cRedef4.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5", "Redefines", 
            pnd_Twrl117c_Pnd_Twrl117a_S_Control_Number);
        pnd_Twrl117c_Pnd_Twrl117a_S_Filler = pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Filler", "#TWRL117A-S-FILLER", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Control_Nbr = pnd_Twrl117c_Pnd_Twrl117a_S_Control_NumberRedef5.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Control_Nbr", 
            "#TWRL117A-S-CONTROL-NBR", FieldType.NUMERIC, 8);
        pnd_Twrl117c_Pnd_Twrl117a_S_Filler1 = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Filler1", "#TWRL117A-S-FILLER1", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Form_Type = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Form_Type", "#TWRL117A-S-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Rec_Type", "#TWRL117A-S-REC-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Doc_Type", "#TWRL117A-S-DOC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Filler2 = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Filler2", "#TWRL117A-S-FILLER2", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Tax_Year", "#TWRL117A-S-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl117c_Pnd_Twrl117a_S_Filler3 = pnd_Twrl117cRedef4.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Filler3", "#TWRL117A-S-FILLER3", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information = pnd_Twrl117cRedef4.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information", "#TWRL117A-S-PAYERS-INFORMATION");
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id_Typ", 
            "#TWRL117A-S-PAYERS-ID-TYP", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Id", 
            "#TWRL117A-S-PAYERS-ID", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Name", 
            "#TWRL117A-S-PAYERS-NAME", FieldType.STRING, 30);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_1", 
            "#TWRL117A-S-PAYERS-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_2 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Address_2", 
            "#TWRL117A-S-PAYERS-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Town", 
            "#TWRL117A-S-PAYERS-TOWN", FieldType.STRING, 13);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_State = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_State", 
            "#TWRL117A-S-PAYERS-STATE", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full", 
            "#TWRL117A-S-PAYERS-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip", 
            "#TWRL117A-S-PAYERS-ZIP", FieldType.NUMERIC, 5);
        pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Ext = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_FullRedef6.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Zip_Ext", 
            "#TWRL117A-S-PAYERS-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117c_Pnd_Twrl117a_S_Filler4 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Filler4", "#TWRL117A-S-FILLER4", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Num_Of_Doc", 
            "#TWRL117A-S-NUM-OF-DOC", FieldType.NUMERIC, 10);
        pnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Amt_Withld", 
            "#TWRL117A-S-AMT-WITHLD", FieldType.DECIMAL, 15,2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Amout_Paid", 
            "#TWRL117A-S-AMOUT-PAID", FieldType.DECIMAL, 15,2);
        pnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Taxpayer_Type", 
            "#TWRL117A-S-TAXPAYER-TYPE", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill5 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill5", "#TWRL117A-S-FILL5", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill6 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill6", "#TWRL117A-S-FILL6", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill7 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill7", "#TWRL117A-S-FILL7", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill8 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill8", "#TWRL117A-S-FILL8", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill9 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill9", "#TWRL117A-S-FILL9", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill10 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill10", "#TWRL117A-S-FILL10", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill11 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill11", "#TWRL117A-S-FILL11", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill12 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill12", "#TWRL117A-S-FILL12", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117a_S_Fill13 = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Fill13", "#TWRL117A-S-FILL13", 
            FieldType.STRING, 246);
        pnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Cntl_Nbr", 
            "#TWRL117A-S-CNTL-NBR", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117a_S_Reason_For_Chg = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Reason_For_Chg", 
            "#TWRL117A-S-REASON-FOR-CHG", FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117a_S_Amended_Date = pnd_Twrl117c_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_S_Amended_Date", 
            "#TWRL117A-S-AMENDED-DATE", FieldType.NUMERIC, 6);
        pnd_Twrl117cRedef7 = newGroupInRecord("pnd_Twrl117cRedef7", "Redefines", pnd_Twrl117c);
        filler01 = pnd_Twrl117cRedef7.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117t_Control_Nbr = pnd_Twrl117cRedef7.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Control_Nbr", "#TWRL117T-CONTROL-NBR", FieldType.NUMERIC, 
            9);
        filler02 = pnd_Twrl117cRedef7.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117t_Form_Type = pnd_Twrl117cRedef7.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Form_Type", "#TWRL117T-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117t_Rec_Type = pnd_Twrl117cRedef7.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Rec_Type", "#TWRL117T-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117t_Doc_Type = pnd_Twrl117cRedef7.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Doc_Type", "#TWRL117T-DOC-TYPE", FieldType.STRING, 
            1);
        filler03 = pnd_Twrl117cRedef7.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 1);
        filler04 = pnd_Twrl117cRedef7.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117t_Tax_Year = pnd_Twrl117cRedef7.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Tax_Year", "#TWRL117T-TAX-YEAR", FieldType.NUMERIC, 
            4);
        filler05 = pnd_Twrl117cRedef7.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 5);
        pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm = pnd_Twrl117cRedef7.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm", "#TWRL117T-WITHHLDNG-AGNT-INFRM");
        pnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Payers_Id_Type", 
            "#TWRL117T-PAYERS-ID-TYPE", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Typ_Industry_Busins", 
            "#TWRL117T-TYP-INDUSTRY-BUSINS", FieldType.STRING, 20);
        pnd_Twrl117c_Pnd_Twrl117t_Indentification_No = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Indentification_No", 
            "#TWRL117T-INDENTIFICATION-NO", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117t_Busins_Name = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Busins_Name", 
            "#TWRL117T-BUSINS-NAME", FieldType.STRING, 30);
        pnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Withhld_Agent_Nme", 
            "#TWRL117T-WITHHLD-AGENT-NME", FieldType.STRING, 30);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A", 
            "#TWRL117T-AGENT-TELEPHONE-A", FieldType.STRING, 10);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_A);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone = pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone_ARedef8.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Telephone", 
            "#TWRL117T-AGENT-TELEPHONE", FieldType.NUMERIC, 10);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_1", 
            "#TWRL117T-AGENT-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Address_2", 
            "#TWRL117T-AGENT-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Town = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Town", 
            "#TWRL117T-AGENT-TOWN", FieldType.STRING, 13);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_State = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_State", 
            "#TWRL117T-AGENT-STATE", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full", 
            "#TWRL117T-AGENT-ZIP-FULL", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip = pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip", "#TWRL117T-AGENT-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Ext = pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_FullRedef9.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Zip_Ext", 
            "#TWRL117T-AGENT-ZIP-EXT", FieldType.NUMERIC, 4);
        filler06 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("filler06", "FILLER", FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_1", 
            "#TWRL117T-PHYSICAL-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Physical_Address_2", 
            "#TWRL117T-PHYSICAL-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117c_Pnd_Twrl117t_A_Town = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_Town", "#TWRL117T-A-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl117c_Pnd_Twrl117t_A_State = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_State", "#TWRL117T-A-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full", 
            "#TWRL117T-A-ZIP-FULL", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10 = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10", 
            "Redefines", pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117t_A_Zip = pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_Zip", "#TWRL117T-A-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Ext = pnd_Twrl117c_Pnd_Twrl117t_A_Zip_FullRedef10.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_A_Zip_Ext", "#TWRL117T-A-ZIP-EXT", 
            FieldType.NUMERIC, 4);
        pnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Chng_Of_Address", 
            "#TWRL117T-CHNG-OF-ADDRESS", FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117t_Agent_Email = pnd_Twrl117c_Pnd_Twrl117t_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Agent_Email", 
            "#TWRL117T-AGENT-EMAIL", FieldType.STRING, 50);
        pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld = pnd_Twrl117cRedef7.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld", "#TWRL117T-TAX-WITHHELD");
        pnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Prdc_Pymnt_Quali_Gov", 
            "#TWRL117T-PRDC-PYMNT-QUALI-GOV", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri20", 
            "#TWRL117T-LUMPSUM-DISTRI20", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Lumpsum_Distri10", 
            "#TWRL117T-LUMPSUM-DISTRI10", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Distri_Nonqualified", 
            "#TWRL117T-DISTRI-NONQUALIFIED", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Othr_Incm_Qualifd10", 
            "#TWRL117T-OTHR-INCM-QUALIFD10", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Annuity_Cost", "#TWRL117T-ANNUITY-COST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Rolovr_Qual_Nondtira", 
            "#TWRL117T-ROLOVR-QUAL-NONDTIRA", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Distri_Rtrmnt_Sav10", 
            "#TWRL117T-DISTRI-RTRMNT-SAV10", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Rlvr_Non_Dtira10", 
            "#TWRL117T-RLVR-NON-DTIRA10", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Nonres_Dsitri", "#TWRL117T-NONRES-DSITRI", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Other_Distribution = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Other_Distribution", 
            "#TWRL117T-OTHER-DISTRIBUTION", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Econo_Ergncy_Maria", 
            "#TWRL117T-ECONO-ERGNCY-MARIA", FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Total = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Total", "#TWRL117T-TOTAL", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117c_Pnd_Twrl117t_Total_Forms = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Total_Forms", "#TWRL117T-TOTAL-FORMS", 
            FieldType.NUMERIC, 10);
        pnd_Twrl117c_Pnd_Twrl117t_Fill1 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill1", "#TWRL117T-FILL1", 
            FieldType.STRING, 171);
        pnd_Twrl117c_Pnd_Twrl117t_Fill2 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill2", "#TWRL117T-FILL2", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill3 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill3", "#TWRL117T-FILL3", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill4 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill4", "#TWRL117T-FILL4", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill5 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill5", "#TWRL117T-FILL5", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill6 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill6", "#TWRL117T-FILL6", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill7 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill7", "#TWRL117T-FILL7", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Fill8 = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fill8", "#TWRL117T-FILL8", 
            FieldType.STRING, 250);
        pnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Reason_For_Chg", 
            "#TWRL117T-REASON-FOR-CHG", FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117t_Fillr1a = pnd_Twrl117c_Pnd_Twrl117t_Tax_Withheld.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117t_Fillr1a", "#TWRL117T-FILLR1A", 
            FieldType.NUMERIC, 6);
        pnd_Twrl117cRedef11 = newGroupInRecord("pnd_Twrl117cRedef11", "Redefines", pnd_Twrl117c);
        pnd_Twrl117c_Pnd_Twrl117a_1a = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1a", "#TWRL117A-1A", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1b = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1b", "#TWRL117A-1B", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1c = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1c", "#TWRL117A-1C", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1d = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1d", "#TWRL117A-1D", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1e = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1e", "#TWRL117A-1E", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1f = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1f", "#TWRL117A-1F", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1g = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1g", "#TWRL117A-1G", FieldType.STRING, 100);
        pnd_Twrl117c_Pnd_Twrl117a_1_Filler = pnd_Twrl117cRedef11.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117a_1_Filler", "#TWRL117A-1-FILLER", FieldType.STRING, 
            250);
        pnd_Twrl117cRedef12 = newGroupInRecord("pnd_Twrl117cRedef12", "Redefines", pnd_Twrl117c);
        pnd_Twrl117c_Pnd_Twrl117p_Rec_Id = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Rec_Id", "#TWRL117P-REC-ID", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117p_Tax_Year = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Tax_Year", "#TWRL117P-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Agent_Ind_Cde", "#TWRL117P-AGENT-IND-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl117c_Pnd_Twrl117p_Ein = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Ein", "#TWRL117P-EIN", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117p_Frm_Type = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Frm_Type", "#TWRL117P-FRM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117p_Est_Num = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Est_Num", "#TWRL117P-EST-NUM", FieldType.STRING, 
            4);
        pnd_Twrl117c_Pnd_Twrl117p_File_Type = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_File_Type", "#TWRL117P-FILE-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117c_Pnd_Twrl117p_Filler1 = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Filler1", "#TWRL117P-FILLER1", FieldType.STRING, 
            17);
        pnd_Twrl117c_Pnd_Twrl117p_Emp_Name = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Emp_Name", "#TWRL117P-EMP-NAME", FieldType.STRING, 
            57);
        pnd_Twrl117c_Pnd_Twrl117p_Loc_Add = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Loc_Add", "#TWRL117P-LOC-ADD", FieldType.STRING, 
            22);
        pnd_Twrl117c_Pnd_Twrl117p_Del_Add = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Del_Add", "#TWRL117P-DEL-ADD", FieldType.STRING, 
            22);
        pnd_Twrl117c_Pnd_Twrl117p_City = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_City", "#TWRL117P-CITY", FieldType.STRING, 22);
        pnd_Twrl117c_Pnd_Twrl117p_State = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_State", "#TWRL117P-STATE", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117p_Zip = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Zip", "#TWRL117P-ZIP", FieldType.NUMERIC, 9);
        pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13 = pnd_Twrl117cRedef12.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13", "Redefines", pnd_Twrl117c_Pnd_Twrl117p_Zip);
        pnd_Twrl117c_Pnd_Twrl117p_Zip1 = pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Zip1", "#TWRL117P-ZIP1", FieldType.NUMERIC, 
            5);
        pnd_Twrl117c_Pnd_Twrl117p_Zip2 = pnd_Twrl117c_Pnd_Twrl117p_ZipRedef13.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Zip2", "#TWRL117P-ZIP2", FieldType.NUMERIC, 
            4);
        pnd_Twrl117c_Pnd_Twrl117p_Filler2 = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Filler2", "#TWRL117P-FILLER2", FieldType.STRING, 
            5);
        pnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Frgn_State_Prvnc", "#TWRL117P-FRGN-STATE-PRVNC", 
            FieldType.STRING, 23);
        pnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Frgn_Postal_Cde", "#TWRL117P-FRGN-POSTAL-CDE", 
            FieldType.STRING, 15);
        pnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Cntry_Cde", "#TWRL117P-CNTRY-CDE", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117p_Cntct_Email = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Cntct_Email", "#TWRL117P-CNTCT-EMAIL", 
            FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Agent_Type_Id", "#TWRL117P-AGENT-TYPE-ID", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117p_Filler3 = pnd_Twrl117cRedef12.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117p_Filler3", "#TWRL117P-FILLER3", FieldType.STRING, 
            2241);
        pnd_Twrl117cRedef14 = newGroupInRecord("pnd_Twrl117cRedef14", "Redefines", pnd_Twrl117c);
        pnd_Twrl117c_Pnd_Twrl117h_Rec_Id = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Rec_Id", "#TWRL117H-REC-ID", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Submtr_Ein", "#TWRL117H-SUBMTR-EIN", FieldType.NUMERIC, 
            9);
        pnd_Twrl117c_Pnd_Twrl117h_Resub_Ind = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Resub_Ind", "#TWRL117H-RESUB-IND", FieldType.NUMERIC, 
            1);
        pnd_Twrl117c_Pnd_Twrl117h_Software_Cde = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Software_Cde", "#TWRL117H-SOFTWARE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Twrl117c_Pnd_Twrl117h_Comp_Name = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Comp_Name", "#TWRL117H-COMP-NAME", FieldType.STRING, 
            57);
        pnd_Twrl117c_Pnd_Twrl117h_Comp_Loc = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Comp_Loc", "#TWRL117H-COMP-LOC", FieldType.STRING, 
            22);
        pnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Delivery_Addr", "#TWRL117H-DELIVERY-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl117c_Pnd_Twrl117h_City = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_City", "#TWRL117H-CITY", FieldType.STRING, 22);
        pnd_Twrl117c_Pnd_Twrl117h_State = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_State", "#TWRL117H-STATE", FieldType.STRING, 
            2);
        pnd_Twrl117c_Pnd_Twrl117h_Zip_Full = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Zip_Full", "#TWRL117H-ZIP-FULL", FieldType.STRING, 
            9);
        pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15 = pnd_Twrl117cRedef14.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15", "Redefines", pnd_Twrl117c_Pnd_Twrl117h_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117h_Zip1 = pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Zip1", "#TWRL117H-ZIP1", 
            FieldType.STRING, 5);
        pnd_Twrl117c_Pnd_Twrl117h_Zip2 = pnd_Twrl117c_Pnd_Twrl117h_Zip_FullRedef15.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Zip2", "#TWRL117H-ZIP2", 
            FieldType.STRING, 4);
        pnd_Twrl117c_Pnd_Twrl117h_Fill2 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Fill2", "#TWRL117H-FILL2", FieldType.STRING, 
            17);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St", "#TWRL117H-SBMTER-FORGN-ST", 
            FieldType.STRING, 23);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post", "#TWRL117H-SBMTER-FORGN-POST", 
            FieldType.STRING, 15);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code", "#TWRL117H-SBMTER-COUNTRY-CODE", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Name", "#TWRL117H-SBMTER-NAME", 
            FieldType.STRING, 57);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Loc", "#TWRL117H-SBMTER-LOC", FieldType.STRING, 
            22);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Addr", "#TWRL117H-SBMTER-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_City = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_City", "#TWRL117H-SBMTER-CITY", 
            FieldType.STRING, 22);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_State = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_State", "#TWRL117H-SBMTER-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full", "#TWRL117H-SBMTER-ZIP-FULL", 
            FieldType.STRING, 9);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16 = pnd_Twrl117cRedef14.newGroupInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16", "Redefines", 
            pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_Full);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip1 = pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip1", 
            "#TWRL117H-SBMTER-ZIP1", FieldType.STRING, 5);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip2 = pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip_FullRedef16.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Zip2", 
            "#TWRL117H-SBMTER-ZIP2", FieldType.STRING, 4);
        pnd_Twrl117c_Pnd_Twrl117h_Fill3 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Fill3", "#TWRL117H-FILL3", FieldType.STRING, 
            5);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_St_1", "#TWRL117H-SBMTER-FORGN-ST-1", 
            FieldType.STRING, 23);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Forgn_Post_1", "#TWRL117H-SBMTER-FORGN-POST-1", 
            FieldType.STRING, 15);
        pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Sbmter_Country_Code_1", "#TWRL117H-SBMTER-COUNTRY-CODE-1", 
            FieldType.STRING, 2);
        pnd_Twrl117c_Pnd_Twrl117h_Contact_Name = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Contact_Name", "#TWRL117H-CONTACT-NAME", 
            FieldType.STRING, 27);
        pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone", "#TWRL117H-CONTACT-PHONE", 
            FieldType.NUMERIC, 15);
        pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Contact_Phone_Ext", "#TWRL117H-CONTACT-PHONE-EXT", 
            FieldType.STRING, 5);
        pnd_Twrl117c_Pnd_Twrl117h_Fill4 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Fill4", "#TWRL117H-FILL4", FieldType.STRING, 
            3);
        pnd_Twrl117c_Pnd_Twrl117h_Contact_Email = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Contact_Email", "#TWRL117H-CONTACT-EMAIL", 
            FieldType.STRING, 40);
        pnd_Twrl117c_Pnd_Twrl117h_Fill5 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Fill5", "#TWRL117H-FILL5", FieldType.STRING, 
            3);
        pnd_Twrl117c_Pnd_Twrl117h_Contact_Fax = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Contact_Fax", "#TWRL117H-CONTACT-FAX", 
            FieldType.STRING, 10);
        pnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Mthd_Prblm_Notice", "#TWRL117H-MTHD-PRBLM-NOTICE", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117h_Prepares_Code = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Prepares_Code", "#TWRL117H-PREPARES-CODE", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Submtr_Iden_No_Typ", "#TWRL117H-SUBMTR-IDEN-NO-TYP", 
            FieldType.STRING, 1);
        pnd_Twrl117c_Pnd_Twrl117h_Fill6 = pnd_Twrl117cRedef14.newFieldInGroup("pnd_Twrl117c_Pnd_Twrl117h_Fill6", "#TWRL117H-FILL6", FieldType.STRING, 
            2010);

        this.setRecordName("LdaTwrl117c");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl117c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
