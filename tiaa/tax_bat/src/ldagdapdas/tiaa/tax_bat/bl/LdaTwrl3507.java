/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:32 PM
**        * FROM NATURAL LDA     : TWRL3507
************************************************************
**        * FILE NAME            : LdaTwrl3507.java
**        * CLASS NAME           : LdaTwrl3507
**        * INSTANCE NAME        : LdaTwrl3507
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3507 extends DbsRecord
{
    // Properties
    private DbsGroup irs_B_Rec;
    private DbsGroup irs_B_Rec_Irs_B_Common_Area;
    private DbsField irs_B_Rec_Irs_B_Record_Type;
    private DbsField irs_B_Rec_Irs_B_Year;
    private DbsField irs_B_Rec_Irs_B_Corr_Ind;
    private DbsField irs_B_Rec_Irs_B_Name_Control;
    private DbsField irs_B_Rec_Irs_B_Tin_Type;
    private DbsField irs_B_Rec_Irs_B_Tin;
    private DbsField irs_B_Rec_Irs_B_Contract_Payee;
    private DbsField irs_B_Rec_Irs_B_Payers_Office_Code;
    private DbsField irs_B_Rec_Irs_B_Filler_1;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_1;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_2;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_3;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_4;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_5;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_6;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_7;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_8;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_9;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_A;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_B;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_C;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_D;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_E;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_F;
    private DbsField irs_B_Rec_Irs_B_Payment_Amt_G;
    private DbsField irs_B_Rec_Irs_B_Foreign_Ind;
    private DbsField irs_B_Rec_Irs_B_Name_Line_1;
    private DbsField irs_B_Rec_Irs_B_Name_Line_2;
    private DbsField irs_B_Rec_Irs_B_Filler_3;
    private DbsField irs_B_Rec_Irs_B_Address_Line;
    private DbsField irs_B_Rec_Irs_B_Filler_4;
    private DbsField irs_B_Rec_Irs_B_City_State_Zip;
    private DbsGroup irs_B_Rec_Irs_B_City_State_ZipRedef1;
    private DbsField irs_B_Rec_Irs_B_City;
    private DbsField irs_B_Rec_Irs_B_State;
    private DbsField irs_B_Rec_Irs_B_Zip;
    private DbsField irs_B_Rec_Irs_B_Filler_5;
    private DbsField irs_B_Rec_Irs_B_Record_Sequence_No;
    private DbsField irs_B_Rec_Irs_B_Filler_6;
    private DbsField irs_B_Rec_Irs_B_1099_Area;
    private DbsGroup irs_B_Rec_Irs_B_1099_AreaRedef2;
    private DbsGroup irs_B_Rec_Irs_B_1099_Int_Area;
    private DbsField irs_B_Rec_Irs_B_2nd_Tin_Notice;
    private DbsField filler01;
    private DbsField irs_B_Rec_Irs_B_Foreign_Tax_Country;
    private DbsGroup irs_B_Rec_Irs_B_1099_AreaRedef3;
    private DbsGroup irs_B_Rec_Irs_B_1099_R_Area;
    private DbsField filler02;
    private DbsField irs_B_Rec_Irs_B_Dist_Code;
    private DbsField irs_B_Rec_Irs_B_Taxable_Not_Det;
    private DbsField irs_B_Rec_Irs_B_Ira_Sep_Ind;
    private DbsField irs_B_Rec_Irs_B_Total_Dist;
    private DbsField irs_B_Rec_Irs_B_Total_Dist_Pct;
    private DbsGroup irs_B_Rec_Irs_B_Total_Dist_PctRedef4;
    private DbsField irs_B_Rec_Irs_B_Total_Dist_Pct_Num;
    private DbsField irs_B_Rec_Irs_B_1st_Year_Roth_Contrib;
    private DbsField irs_B_Rec_Irs_B_Fatca_Filing_Ind;
    private DbsField irs_B_Rec_Irs_B_Date_Of_Payment;
    private DbsField filler03;
    private DbsField irs_B_Rec_Irs_B_Special_Data_Entries;
    private DbsGroup irs_B_Rec_Irs_B_Special_Data_EntriesRedef5;
    private DbsField filler04;
    private DbsField irs_B_Rec_Irs_B_Md_Cntral_Registratn_No;
    private DbsGroup irs_B_Rec_Irs_B_Special_Data_EntriesRedef6;
    private DbsField filler05;
    private DbsField irs_B_Rec_Irs_B_Nm_Cntral_Registratn_No;
    private DbsField irs_B_Rec_Irs_B_State_Tax;
    private DbsGroup irs_B_Rec_Irs_B_State_TaxRedef7;
    private DbsField irs_B_Rec_Irs_B_State_Tax_Num;
    private DbsField irs_B_Rec_Irs_B_Local_Tax;
    private DbsGroup irs_B_Rec_Irs_B_Local_TaxRedef8;
    private DbsField irs_B_Rec_Irs_B_Local_Tax_Num;
    private DbsField irs_B_Rec_Irs_B_Federal_State_Code;
    private DbsGroup irs_B_RecRedef9;
    private DbsField irs_B_Rec_Irs_B_Rec_Array;

    public DbsGroup getIrs_B_Rec() { return irs_B_Rec; }

    public DbsGroup getIrs_B_Rec_Irs_B_Common_Area() { return irs_B_Rec_Irs_B_Common_Area; }

    public DbsField getIrs_B_Rec_Irs_B_Record_Type() { return irs_B_Rec_Irs_B_Record_Type; }

    public DbsField getIrs_B_Rec_Irs_B_Year() { return irs_B_Rec_Irs_B_Year; }

    public DbsField getIrs_B_Rec_Irs_B_Corr_Ind() { return irs_B_Rec_Irs_B_Corr_Ind; }

    public DbsField getIrs_B_Rec_Irs_B_Name_Control() { return irs_B_Rec_Irs_B_Name_Control; }

    public DbsField getIrs_B_Rec_Irs_B_Tin_Type() { return irs_B_Rec_Irs_B_Tin_Type; }

    public DbsField getIrs_B_Rec_Irs_B_Tin() { return irs_B_Rec_Irs_B_Tin; }

    public DbsField getIrs_B_Rec_Irs_B_Contract_Payee() { return irs_B_Rec_Irs_B_Contract_Payee; }

    public DbsField getIrs_B_Rec_Irs_B_Payers_Office_Code() { return irs_B_Rec_Irs_B_Payers_Office_Code; }

    public DbsField getIrs_B_Rec_Irs_B_Filler_1() { return irs_B_Rec_Irs_B_Filler_1; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_1() { return irs_B_Rec_Irs_B_Payment_Amt_1; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_2() { return irs_B_Rec_Irs_B_Payment_Amt_2; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_3() { return irs_B_Rec_Irs_B_Payment_Amt_3; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_4() { return irs_B_Rec_Irs_B_Payment_Amt_4; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_5() { return irs_B_Rec_Irs_B_Payment_Amt_5; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_6() { return irs_B_Rec_Irs_B_Payment_Amt_6; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_7() { return irs_B_Rec_Irs_B_Payment_Amt_7; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_8() { return irs_B_Rec_Irs_B_Payment_Amt_8; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_9() { return irs_B_Rec_Irs_B_Payment_Amt_9; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_A() { return irs_B_Rec_Irs_B_Payment_Amt_A; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_B() { return irs_B_Rec_Irs_B_Payment_Amt_B; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_C() { return irs_B_Rec_Irs_B_Payment_Amt_C; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_D() { return irs_B_Rec_Irs_B_Payment_Amt_D; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_E() { return irs_B_Rec_Irs_B_Payment_Amt_E; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_F() { return irs_B_Rec_Irs_B_Payment_Amt_F; }

    public DbsField getIrs_B_Rec_Irs_B_Payment_Amt_G() { return irs_B_Rec_Irs_B_Payment_Amt_G; }

    public DbsField getIrs_B_Rec_Irs_B_Foreign_Ind() { return irs_B_Rec_Irs_B_Foreign_Ind; }

    public DbsField getIrs_B_Rec_Irs_B_Name_Line_1() { return irs_B_Rec_Irs_B_Name_Line_1; }

    public DbsField getIrs_B_Rec_Irs_B_Name_Line_2() { return irs_B_Rec_Irs_B_Name_Line_2; }

    public DbsField getIrs_B_Rec_Irs_B_Filler_3() { return irs_B_Rec_Irs_B_Filler_3; }

    public DbsField getIrs_B_Rec_Irs_B_Address_Line() { return irs_B_Rec_Irs_B_Address_Line; }

    public DbsField getIrs_B_Rec_Irs_B_Filler_4() { return irs_B_Rec_Irs_B_Filler_4; }

    public DbsField getIrs_B_Rec_Irs_B_City_State_Zip() { return irs_B_Rec_Irs_B_City_State_Zip; }

    public DbsGroup getIrs_B_Rec_Irs_B_City_State_ZipRedef1() { return irs_B_Rec_Irs_B_City_State_ZipRedef1; }

    public DbsField getIrs_B_Rec_Irs_B_City() { return irs_B_Rec_Irs_B_City; }

    public DbsField getIrs_B_Rec_Irs_B_State() { return irs_B_Rec_Irs_B_State; }

    public DbsField getIrs_B_Rec_Irs_B_Zip() { return irs_B_Rec_Irs_B_Zip; }

    public DbsField getIrs_B_Rec_Irs_B_Filler_5() { return irs_B_Rec_Irs_B_Filler_5; }

    public DbsField getIrs_B_Rec_Irs_B_Record_Sequence_No() { return irs_B_Rec_Irs_B_Record_Sequence_No; }

    public DbsField getIrs_B_Rec_Irs_B_Filler_6() { return irs_B_Rec_Irs_B_Filler_6; }

    public DbsField getIrs_B_Rec_Irs_B_1099_Area() { return irs_B_Rec_Irs_B_1099_Area; }

    public DbsGroup getIrs_B_Rec_Irs_B_1099_AreaRedef2() { return irs_B_Rec_Irs_B_1099_AreaRedef2; }

    public DbsGroup getIrs_B_Rec_Irs_B_1099_Int_Area() { return irs_B_Rec_Irs_B_1099_Int_Area; }

    public DbsField getIrs_B_Rec_Irs_B_2nd_Tin_Notice() { return irs_B_Rec_Irs_B_2nd_Tin_Notice; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getIrs_B_Rec_Irs_B_Foreign_Tax_Country() { return irs_B_Rec_Irs_B_Foreign_Tax_Country; }

    public DbsGroup getIrs_B_Rec_Irs_B_1099_AreaRedef3() { return irs_B_Rec_Irs_B_1099_AreaRedef3; }

    public DbsGroup getIrs_B_Rec_Irs_B_1099_R_Area() { return irs_B_Rec_Irs_B_1099_R_Area; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getIrs_B_Rec_Irs_B_Dist_Code() { return irs_B_Rec_Irs_B_Dist_Code; }

    public DbsField getIrs_B_Rec_Irs_B_Taxable_Not_Det() { return irs_B_Rec_Irs_B_Taxable_Not_Det; }

    public DbsField getIrs_B_Rec_Irs_B_Ira_Sep_Ind() { return irs_B_Rec_Irs_B_Ira_Sep_Ind; }

    public DbsField getIrs_B_Rec_Irs_B_Total_Dist() { return irs_B_Rec_Irs_B_Total_Dist; }

    public DbsField getIrs_B_Rec_Irs_B_Total_Dist_Pct() { return irs_B_Rec_Irs_B_Total_Dist_Pct; }

    public DbsGroup getIrs_B_Rec_Irs_B_Total_Dist_PctRedef4() { return irs_B_Rec_Irs_B_Total_Dist_PctRedef4; }

    public DbsField getIrs_B_Rec_Irs_B_Total_Dist_Pct_Num() { return irs_B_Rec_Irs_B_Total_Dist_Pct_Num; }

    public DbsField getIrs_B_Rec_Irs_B_1st_Year_Roth_Contrib() { return irs_B_Rec_Irs_B_1st_Year_Roth_Contrib; }

    public DbsField getIrs_B_Rec_Irs_B_Fatca_Filing_Ind() { return irs_B_Rec_Irs_B_Fatca_Filing_Ind; }

    public DbsField getIrs_B_Rec_Irs_B_Date_Of_Payment() { return irs_B_Rec_Irs_B_Date_Of_Payment; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getIrs_B_Rec_Irs_B_Special_Data_Entries() { return irs_B_Rec_Irs_B_Special_Data_Entries; }

    public DbsGroup getIrs_B_Rec_Irs_B_Special_Data_EntriesRedef5() { return irs_B_Rec_Irs_B_Special_Data_EntriesRedef5; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getIrs_B_Rec_Irs_B_Md_Cntral_Registratn_No() { return irs_B_Rec_Irs_B_Md_Cntral_Registratn_No; }

    public DbsGroup getIrs_B_Rec_Irs_B_Special_Data_EntriesRedef6() { return irs_B_Rec_Irs_B_Special_Data_EntriesRedef6; }

    public DbsField getFiller05() { return filler05; }

    public DbsField getIrs_B_Rec_Irs_B_Nm_Cntral_Registratn_No() { return irs_B_Rec_Irs_B_Nm_Cntral_Registratn_No; }

    public DbsField getIrs_B_Rec_Irs_B_State_Tax() { return irs_B_Rec_Irs_B_State_Tax; }

    public DbsGroup getIrs_B_Rec_Irs_B_State_TaxRedef7() { return irs_B_Rec_Irs_B_State_TaxRedef7; }

    public DbsField getIrs_B_Rec_Irs_B_State_Tax_Num() { return irs_B_Rec_Irs_B_State_Tax_Num; }

    public DbsField getIrs_B_Rec_Irs_B_Local_Tax() { return irs_B_Rec_Irs_B_Local_Tax; }

    public DbsGroup getIrs_B_Rec_Irs_B_Local_TaxRedef8() { return irs_B_Rec_Irs_B_Local_TaxRedef8; }

    public DbsField getIrs_B_Rec_Irs_B_Local_Tax_Num() { return irs_B_Rec_Irs_B_Local_Tax_Num; }

    public DbsField getIrs_B_Rec_Irs_B_Federal_State_Code() { return irs_B_Rec_Irs_B_Federal_State_Code; }

    public DbsGroup getIrs_B_RecRedef9() { return irs_B_RecRedef9; }

    public DbsField getIrs_B_Rec_Irs_B_Rec_Array() { return irs_B_Rec_Irs_B_Rec_Array; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_B_Rec = newGroupInRecord("irs_B_Rec", "IRS-B-REC");
        irs_B_Rec_Irs_B_Common_Area = irs_B_Rec.newGroupInGroup("irs_B_Rec_Irs_B_Common_Area", "IRS-B-COMMON-AREA");
        irs_B_Rec_Irs_B_Record_Type = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Record_Type", "IRS-B-RECORD-TYPE", FieldType.STRING, 
            1);
        irs_B_Rec_Irs_B_Year = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Year", "IRS-B-YEAR", FieldType.STRING, 4);
        irs_B_Rec_Irs_B_Corr_Ind = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Corr_Ind", "IRS-B-CORR-IND", FieldType.STRING, 1);
        irs_B_Rec_Irs_B_Name_Control = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Name_Control", "IRS-B-NAME-CONTROL", FieldType.STRING, 
            4);
        irs_B_Rec_Irs_B_Tin_Type = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Tin_Type", "IRS-B-TIN-TYPE", FieldType.STRING, 1);
        irs_B_Rec_Irs_B_Tin = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Tin", "IRS-B-TIN", FieldType.STRING, 9);
        irs_B_Rec_Irs_B_Contract_Payee = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Contract_Payee", "IRS-B-CONTRACT-PAYEE", FieldType.STRING, 
            20);
        irs_B_Rec_Irs_B_Payers_Office_Code = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payers_Office_Code", "IRS-B-PAYERS-OFFICE-CODE", 
            FieldType.STRING, 4);
        irs_B_Rec_Irs_B_Filler_1 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Filler_1", "IRS-B-FILLER-1", FieldType.STRING, 10);
        irs_B_Rec_Irs_B_Payment_Amt_1 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_1", "IRS-B-PAYMENT-AMT-1", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_2 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_2", "IRS-B-PAYMENT-AMT-2", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_3 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_3", "IRS-B-PAYMENT-AMT-3", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_4 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_4", "IRS-B-PAYMENT-AMT-4", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_5 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_5", "IRS-B-PAYMENT-AMT-5", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_6 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_6", "IRS-B-PAYMENT-AMT-6", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_7 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_7", "IRS-B-PAYMENT-AMT-7", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_8 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_8", "IRS-B-PAYMENT-AMT-8", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_9 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_9", "IRS-B-PAYMENT-AMT-9", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_A = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_A", "IRS-B-PAYMENT-AMT-A", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_B = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_B", "IRS-B-PAYMENT-AMT-B", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_C = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_C", "IRS-B-PAYMENT-AMT-C", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_D = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_D", "IRS-B-PAYMENT-AMT-D", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_E = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_E", "IRS-B-PAYMENT-AMT-E", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_F = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_F", "IRS-B-PAYMENT-AMT-F", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Payment_Amt_G = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Payment_Amt_G", "IRS-B-PAYMENT-AMT-G", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Foreign_Ind = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Foreign_Ind", "IRS-B-FOREIGN-IND", FieldType.STRING, 
            1);
        irs_B_Rec_Irs_B_Name_Line_1 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Name_Line_1", "IRS-B-NAME-LINE-1", FieldType.STRING, 
            40);
        irs_B_Rec_Irs_B_Name_Line_2 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Name_Line_2", "IRS-B-NAME-LINE-2", FieldType.STRING, 
            40);
        irs_B_Rec_Irs_B_Filler_3 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Filler_3", "IRS-B-FILLER-3", FieldType.STRING, 40);
        irs_B_Rec_Irs_B_Address_Line = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Address_Line", "IRS-B-ADDRESS-LINE", FieldType.STRING, 
            40);
        irs_B_Rec_Irs_B_Filler_4 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Filler_4", "IRS-B-FILLER-4", FieldType.STRING, 40);
        irs_B_Rec_Irs_B_City_State_Zip = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_City_State_Zip", "IRS-B-CITY-STATE-ZIP", FieldType.STRING, 
            51);
        irs_B_Rec_Irs_B_City_State_ZipRedef1 = irs_B_Rec_Irs_B_Common_Area.newGroupInGroup("irs_B_Rec_Irs_B_City_State_ZipRedef1", "Redefines", irs_B_Rec_Irs_B_City_State_Zip);
        irs_B_Rec_Irs_B_City = irs_B_Rec_Irs_B_City_State_ZipRedef1.newFieldInGroup("irs_B_Rec_Irs_B_City", "IRS-B-CITY", FieldType.STRING, 40);
        irs_B_Rec_Irs_B_State = irs_B_Rec_Irs_B_City_State_ZipRedef1.newFieldInGroup("irs_B_Rec_Irs_B_State", "IRS-B-STATE", FieldType.STRING, 2);
        irs_B_Rec_Irs_B_Zip = irs_B_Rec_Irs_B_City_State_ZipRedef1.newFieldInGroup("irs_B_Rec_Irs_B_Zip", "IRS-B-ZIP", FieldType.STRING, 9);
        irs_B_Rec_Irs_B_Filler_5 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Filler_5", "IRS-B-FILLER-5", FieldType.STRING, 1);
        irs_B_Rec_Irs_B_Record_Sequence_No = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Record_Sequence_No", "IRS-B-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_B_Rec_Irs_B_Filler_6 = irs_B_Rec_Irs_B_Common_Area.newFieldInGroup("irs_B_Rec_Irs_B_Filler_6", "IRS-B-FILLER-6", FieldType.STRING, 36);
        irs_B_Rec_Irs_B_1099_Area = irs_B_Rec.newFieldInGroup("irs_B_Rec_Irs_B_1099_Area", "IRS-B-1099-AREA", FieldType.STRING, 207);
        irs_B_Rec_Irs_B_1099_AreaRedef2 = irs_B_Rec.newGroupInGroup("irs_B_Rec_Irs_B_1099_AreaRedef2", "Redefines", irs_B_Rec_Irs_B_1099_Area);
        irs_B_Rec_Irs_B_1099_Int_Area = irs_B_Rec_Irs_B_1099_AreaRedef2.newGroupInGroup("irs_B_Rec_Irs_B_1099_Int_Area", "IRS-B-1099-INT-AREA");
        irs_B_Rec_Irs_B_2nd_Tin_Notice = irs_B_Rec_Irs_B_1099_Int_Area.newFieldInGroup("irs_B_Rec_Irs_B_2nd_Tin_Notice", "IRS-B-2ND-TIN-NOTICE", FieldType.STRING, 
            1);
        filler01 = irs_B_Rec_Irs_B_1099_Int_Area.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 2);
        irs_B_Rec_Irs_B_Foreign_Tax_Country = irs_B_Rec_Irs_B_1099_Int_Area.newFieldInGroup("irs_B_Rec_Irs_B_Foreign_Tax_Country", "IRS-B-FOREIGN-TAX-COUNTRY", 
            FieldType.STRING, 40);
        irs_B_Rec_Irs_B_1099_AreaRedef3 = irs_B_Rec.newGroupInGroup("irs_B_Rec_Irs_B_1099_AreaRedef3", "Redefines", irs_B_Rec_Irs_B_1099_Area);
        irs_B_Rec_Irs_B_1099_R_Area = irs_B_Rec_Irs_B_1099_AreaRedef3.newGroupInGroup("irs_B_Rec_Irs_B_1099_R_Area", "IRS-B-1099-R-AREA");
        filler02 = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 1);
        irs_B_Rec_Irs_B_Dist_Code = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Dist_Code", "IRS-B-DIST-CODE", FieldType.STRING, 2);
        irs_B_Rec_Irs_B_Taxable_Not_Det = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Taxable_Not_Det", "IRS-B-TAXABLE-NOT-DET", FieldType.STRING, 
            1);
        irs_B_Rec_Irs_B_Ira_Sep_Ind = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Ira_Sep_Ind", "IRS-B-IRA-SEP-IND", FieldType.STRING, 
            1);
        irs_B_Rec_Irs_B_Total_Dist = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Total_Dist", "IRS-B-TOTAL-DIST", FieldType.STRING, 1);
        irs_B_Rec_Irs_B_Total_Dist_Pct = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Total_Dist_Pct", "IRS-B-TOTAL-DIST-PCT", FieldType.STRING, 
            2);
        irs_B_Rec_Irs_B_Total_Dist_PctRedef4 = irs_B_Rec_Irs_B_1099_R_Area.newGroupInGroup("irs_B_Rec_Irs_B_Total_Dist_PctRedef4", "Redefines", irs_B_Rec_Irs_B_Total_Dist_Pct);
        irs_B_Rec_Irs_B_Total_Dist_Pct_Num = irs_B_Rec_Irs_B_Total_Dist_PctRedef4.newFieldInGroup("irs_B_Rec_Irs_B_Total_Dist_Pct_Num", "IRS-B-TOTAL-DIST-PCT-NUM", 
            FieldType.NUMERIC, 2);
        irs_B_Rec_Irs_B_1st_Year_Roth_Contrib = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_1st_Year_Roth_Contrib", "IRS-B-1ST-YEAR-ROTH-CONTRIB", 
            FieldType.STRING, 4);
        irs_B_Rec_Irs_B_Fatca_Filing_Ind = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Fatca_Filing_Ind", "IRS-B-FATCA-FILING-IND", FieldType.STRING, 
            1);
        irs_B_Rec_Irs_B_Date_Of_Payment = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Date_Of_Payment", "IRS-B-DATE-OF-PAYMENT", FieldType.STRING, 
            8);
        filler03 = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 98);
        irs_B_Rec_Irs_B_Special_Data_Entries = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Special_Data_Entries", "IRS-B-SPECIAL-DATA-ENTRIES", 
            FieldType.STRING, 60);
        irs_B_Rec_Irs_B_Special_Data_EntriesRedef5 = irs_B_Rec_Irs_B_1099_R_Area.newGroupInGroup("irs_B_Rec_Irs_B_Special_Data_EntriesRedef5", "Redefines", 
            irs_B_Rec_Irs_B_Special_Data_Entries);
        filler04 = irs_B_Rec_Irs_B_Special_Data_EntriesRedef5.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 52);
        irs_B_Rec_Irs_B_Md_Cntral_Registratn_No = irs_B_Rec_Irs_B_Special_Data_EntriesRedef5.newFieldInGroup("irs_B_Rec_Irs_B_Md_Cntral_Registratn_No", 
            "IRS-B-MD-CNTRAL-REGISTRATN-NO", FieldType.STRING, 8);
        irs_B_Rec_Irs_B_Special_Data_EntriesRedef6 = irs_B_Rec_Irs_B_1099_R_Area.newGroupInGroup("irs_B_Rec_Irs_B_Special_Data_EntriesRedef6", "Redefines", 
            irs_B_Rec_Irs_B_Special_Data_Entries);
        filler05 = irs_B_Rec_Irs_B_Special_Data_EntriesRedef6.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 49);
        irs_B_Rec_Irs_B_Nm_Cntral_Registratn_No = irs_B_Rec_Irs_B_Special_Data_EntriesRedef6.newFieldInGroup("irs_B_Rec_Irs_B_Nm_Cntral_Registratn_No", 
            "IRS-B-NM-CNTRAL-REGISTRATN-NO", FieldType.STRING, 11);
        irs_B_Rec_Irs_B_State_Tax = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_State_Tax", "IRS-B-STATE-TAX", FieldType.STRING, 12);
        irs_B_Rec_Irs_B_State_TaxRedef7 = irs_B_Rec_Irs_B_1099_R_Area.newGroupInGroup("irs_B_Rec_Irs_B_State_TaxRedef7", "Redefines", irs_B_Rec_Irs_B_State_Tax);
        irs_B_Rec_Irs_B_State_Tax_Num = irs_B_Rec_Irs_B_State_TaxRedef7.newFieldInGroup("irs_B_Rec_Irs_B_State_Tax_Num", "IRS-B-STATE-TAX-NUM", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Local_Tax = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Local_Tax", "IRS-B-LOCAL-TAX", FieldType.STRING, 12);
        irs_B_Rec_Irs_B_Local_TaxRedef8 = irs_B_Rec_Irs_B_1099_R_Area.newGroupInGroup("irs_B_Rec_Irs_B_Local_TaxRedef8", "Redefines", irs_B_Rec_Irs_B_Local_Tax);
        irs_B_Rec_Irs_B_Local_Tax_Num = irs_B_Rec_Irs_B_Local_TaxRedef8.newFieldInGroup("irs_B_Rec_Irs_B_Local_Tax_Num", "IRS-B-LOCAL-TAX-NUM", FieldType.DECIMAL, 
            12,2);
        irs_B_Rec_Irs_B_Federal_State_Code = irs_B_Rec_Irs_B_1099_R_Area.newFieldInGroup("irs_B_Rec_Irs_B_Federal_State_Code", "IRS-B-FEDERAL-STATE-CODE", 
            FieldType.STRING, 2);
        irs_B_RecRedef9 = newGroupInRecord("irs_B_RecRedef9", "Redefines", irs_B_Rec);
        irs_B_Rec_Irs_B_Rec_Array = irs_B_RecRedef9.newFieldArrayInGroup("irs_B_Rec_Irs_B_Rec_Array", "IRS-B-REC-ARRAY", FieldType.STRING, 250, new DbsArrayController(1,
            3));

        this.setRecordName("LdaTwrl3507");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_B_Rec_Irs_B_Record_Type.setInitialValue("B");
    }

    // Constructor
    public LdaTwrl3507() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
