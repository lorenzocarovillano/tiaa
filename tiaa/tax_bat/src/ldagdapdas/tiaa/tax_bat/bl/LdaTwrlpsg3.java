/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:15:19 PM
**        * FROM NATURAL LDA     : TWRLPSG3
************************************************************
**        * FILE NAME            : LdaTwrlpsg3.java
**        * CLASS NAME           : LdaTwrlpsg3
**        * INSTANCE NAME        : LdaTwrlpsg3
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrlpsg3 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Psgm003;
    private DbsGroup pnd_Psgm003_Pnd_Psgm003_Input;
    private DbsField pnd_Psgm003_Pnd_In_Function;
    private DbsGroup pnd_Psgm003_Pnd_In_Data;
    private DbsField pnd_Psgm003_Pnd_In_Contract_Num_Typ;
    private DbsField pnd_Psgm003_Pnd_In_Type;
    private DbsField pnd_Psgm003_Pnd_In_Ssnpin;
    private DbsField pnd_Psgm003_Pnd_In_Contract;
    private DbsField pnd_Psgm003_Pnd_In_Payee_Cd;
    private DbsGroup pnd_Psgm003_Pnd_Rt_Area;
    private DbsField pnd_Psgm003_Pnd_Rt_Ret_Code;
    private DbsGroup pnd_Psgm003_Pnd_Rt_Record;
    private DbsField pnd_Psgm003_Pnd_Rt_Input_String;
    private DbsField pnd_Psgm003_Pnd_Rt_Ssn;
    private DbsField pnd_Psgm003_Pnd_Rt_Pin;
    private DbsField pnd_Psgm003_Pnd_Rt_Tiaa_Cntrct_Nmbr;
    private DbsField pnd_Psgm003_Pnd_Rt_Xpayee_Cde;
    private DbsField pnd_Psgm003_Pnd_Rt_Cntrct_Status_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Ctrct_Status_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Cntrct_Issue_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Lob;
    private DbsField pnd_Psgm003_Pnd_Rt_Cref_Contract_Nbr;
    private DbsField pnd_Psgm003_Pnd_Rt_Cref_Issued_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Ownership_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Product_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Option_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Plan_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Fund_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Social_Code;
    private DbsField pnd_Psgm003_Pnd_Rt_Custodial_Agreement;
    private DbsField pnd_Psgm003_Pnd_Rt_Aas_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Platform_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Multi_Plan_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Tiaa_Mailed_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Cref_Mailed_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Dflt_Enrollment_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Time;
    private DbsField pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Usrid;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Line_One;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Two;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Three;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Four;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Five;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_City;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_State;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Zip;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Country;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Status;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Per_Addr_Ind;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Type_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_One;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Two;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Three;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Four;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Five;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_City;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_State;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Zip;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Country;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Ch_Saving_Cde;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Eft_Pay_Type_Cde;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Postal_Data;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Geo_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Status;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Status_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Dt;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Time;
    private DbsField pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid;
    private DbsField pnd_Psgm003_Pnd_Rt_Prefix;
    private DbsField pnd_Psgm003_Pnd_Rt_Last_Name;
    private DbsField pnd_Psgm003_Pnd_Rt_First_Name;
    private DbsField pnd_Psgm003_Pnd_Rt_Given_Name_Two;
    private DbsField pnd_Psgm003_Pnd_Rt_Suffix_Desc;
    private DbsField pnd_Psgm003_Pnd_Rt_Record_Status;

    public DbsGroup getPnd_Psgm003() { return pnd_Psgm003; }

    public DbsGroup getPnd_Psgm003_Pnd_Psgm003_Input() { return pnd_Psgm003_Pnd_Psgm003_Input; }

    public DbsField getPnd_Psgm003_Pnd_In_Function() { return pnd_Psgm003_Pnd_In_Function; }

    public DbsGroup getPnd_Psgm003_Pnd_In_Data() { return pnd_Psgm003_Pnd_In_Data; }

    public DbsField getPnd_Psgm003_Pnd_In_Contract_Num_Typ() { return pnd_Psgm003_Pnd_In_Contract_Num_Typ; }

    public DbsField getPnd_Psgm003_Pnd_In_Type() { return pnd_Psgm003_Pnd_In_Type; }

    public DbsField getPnd_Psgm003_Pnd_In_Ssnpin() { return pnd_Psgm003_Pnd_In_Ssnpin; }

    public DbsField getPnd_Psgm003_Pnd_In_Contract() { return pnd_Psgm003_Pnd_In_Contract; }

    public DbsField getPnd_Psgm003_Pnd_In_Payee_Cd() { return pnd_Psgm003_Pnd_In_Payee_Cd; }

    public DbsGroup getPnd_Psgm003_Pnd_Rt_Area() { return pnd_Psgm003_Pnd_Rt_Area; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Ret_Code() { return pnd_Psgm003_Pnd_Rt_Ret_Code; }

    public DbsGroup getPnd_Psgm003_Pnd_Rt_Record() { return pnd_Psgm003_Pnd_Rt_Record; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Input_String() { return pnd_Psgm003_Pnd_Rt_Input_String; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Ssn() { return pnd_Psgm003_Pnd_Rt_Ssn; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Pin() { return pnd_Psgm003_Pnd_Rt_Pin; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Tiaa_Cntrct_Nmbr() { return pnd_Psgm003_Pnd_Rt_Tiaa_Cntrct_Nmbr; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Xpayee_Cde() { return pnd_Psgm003_Pnd_Rt_Xpayee_Cde; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cntrct_Status_Code() { return pnd_Psgm003_Pnd_Rt_Cntrct_Status_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Ctrct_Status_Dt() { return pnd_Psgm003_Pnd_Rt_Ctrct_Status_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cntrct_Issue_Dt() { return pnd_Psgm003_Pnd_Rt_Cntrct_Issue_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Lob() { return pnd_Psgm003_Pnd_Rt_Lob; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cref_Contract_Nbr() { return pnd_Psgm003_Pnd_Rt_Cref_Contract_Nbr; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cref_Issued_Ind() { return pnd_Psgm003_Pnd_Rt_Cref_Issued_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Ownership_Code() { return pnd_Psgm003_Pnd_Rt_Ownership_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Product_Code() { return pnd_Psgm003_Pnd_Rt_Product_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Option_Code() { return pnd_Psgm003_Pnd_Rt_Option_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Plan_Code() { return pnd_Psgm003_Pnd_Rt_Plan_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Fund_Code() { return pnd_Psgm003_Pnd_Rt_Fund_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Social_Code() { return pnd_Psgm003_Pnd_Rt_Social_Code; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Custodial_Agreement() { return pnd_Psgm003_Pnd_Rt_Custodial_Agreement; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Aas_Ind() { return pnd_Psgm003_Pnd_Rt_Aas_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Platform_Ind() { return pnd_Psgm003_Pnd_Rt_Platform_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Multi_Plan_Ind() { return pnd_Psgm003_Pnd_Rt_Multi_Plan_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Tiaa_Mailed_Ind() { return pnd_Psgm003_Pnd_Rt_Tiaa_Mailed_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cref_Mailed_Ind() { return pnd_Psgm003_Pnd_Rt_Cref_Mailed_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Dflt_Enrollment_Ind() { return pnd_Psgm003_Pnd_Rt_Dflt_Enrollment_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Dt() { return pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Time() { return pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Time; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Usrid() { return pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Usrid; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Per_Addr_Ind() { return pnd_Psgm003_Pnd_Rt_Co_Per_Addr_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Type_Cd() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Type_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Line_One() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Line_One; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Line_Two() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Two; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Line_Three() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Three; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Line_Four() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Four; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Line_Five() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Five; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_City() { return pnd_Psgm003_Pnd_Rt_Co_Addr_City; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_State() { return pnd_Psgm003_Pnd_Rt_Co_Addr_State; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Zip() { return pnd_Psgm003_Pnd_Rt_Co_Zip; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Country() { return pnd_Psgm003_Pnd_Rt_Co_Country; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Postal_Data() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Postal_Data; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Geo_Cd() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Geo_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Status() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Status; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Status_Dt() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Status_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Dt() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Time() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Time; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Usrid() { return pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Usrid; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Per_Addr_Ind() { return pnd_Psgm003_Pnd_Rt_Cm_Per_Addr_Ind; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Type_Cd() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Type_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Line_One() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_One; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Two() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Two; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Three() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Three; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Four() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Four; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Five() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Five; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_City() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_City; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_State() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_State; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Zip() { return pnd_Psgm003_Pnd_Rt_Cm_Zip; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Country() { return pnd_Psgm003_Pnd_Rt_Cm_Country; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Ch_Saving_Cde() { return pnd_Psgm003_Pnd_Rt_Cm_Ch_Saving_Cde; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr() { return pnd_Psgm003_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr() { return pnd_Psgm003_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Eft_Pay_Type_Cde() { return pnd_Psgm003_Pnd_Rt_Cm_Eft_Pay_Type_Cde; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Postal_Data() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Postal_Data; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Geo_Cd() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Geo_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Status() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Status; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Status_Dt() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Status_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Dt() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Dt; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Time() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Time; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid() { return pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Prefix() { return pnd_Psgm003_Pnd_Rt_Prefix; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Last_Name() { return pnd_Psgm003_Pnd_Rt_Last_Name; }

    public DbsField getPnd_Psgm003_Pnd_Rt_First_Name() { return pnd_Psgm003_Pnd_Rt_First_Name; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Given_Name_Two() { return pnd_Psgm003_Pnd_Rt_Given_Name_Two; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Suffix_Desc() { return pnd_Psgm003_Pnd_Rt_Suffix_Desc; }

    public DbsField getPnd_Psgm003_Pnd_Rt_Record_Status() { return pnd_Psgm003_Pnd_Rt_Record_Status; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Psgm003 = newGroupInRecord("pnd_Psgm003", "#PSGM003");
        pnd_Psgm003_Pnd_Psgm003_Input = pnd_Psgm003.newGroupInGroup("pnd_Psgm003_Pnd_Psgm003_Input", "#PSGM003-INPUT");
        pnd_Psgm003_Pnd_In_Function = pnd_Psgm003_Pnd_Psgm003_Input.newFieldInGroup("pnd_Psgm003_Pnd_In_Function", "#IN-FUNCTION", FieldType.STRING, 2);
        pnd_Psgm003_Pnd_In_Data = pnd_Psgm003_Pnd_Psgm003_Input.newGroupInGroup("pnd_Psgm003_Pnd_In_Data", "#IN-DATA");
        pnd_Psgm003_Pnd_In_Contract_Num_Typ = pnd_Psgm003_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Pnd_In_Contract_Num_Typ", "#IN-CONTRACT-NUM-TYP", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_In_Type = pnd_Psgm003_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Pnd_In_Type", "#IN-TYPE", FieldType.STRING, 3);
        pnd_Psgm003_Pnd_In_Ssnpin = pnd_Psgm003_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Pnd_In_Ssnpin", "#IN-SSNPIN", FieldType.STRING, 9);
        pnd_Psgm003_Pnd_In_Contract = pnd_Psgm003_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Pnd_In_Contract", "#IN-CONTRACT", FieldType.STRING, 10);
        pnd_Psgm003_Pnd_In_Payee_Cd = pnd_Psgm003_Pnd_In_Data.newFieldInGroup("pnd_Psgm003_Pnd_In_Payee_Cd", "#IN-PAYEE-CD", FieldType.STRING, 2);
        pnd_Psgm003_Pnd_Rt_Area = pnd_Psgm003.newGroupInGroup("pnd_Psgm003_Pnd_Rt_Area", "#RT-AREA");
        pnd_Psgm003_Pnd_Rt_Ret_Code = pnd_Psgm003_Pnd_Rt_Area.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Ret_Code", "#RT-RET-CODE", FieldType.STRING, 2);
        pnd_Psgm003_Pnd_Rt_Record = pnd_Psgm003_Pnd_Rt_Area.newGroupInGroup("pnd_Psgm003_Pnd_Rt_Record", "#RT-RECORD");
        pnd_Psgm003_Pnd_Rt_Input_String = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Input_String", "#RT-INPUT-STRING", FieldType.STRING, 
            100);
        pnd_Psgm003_Pnd_Rt_Ssn = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Ssn", "#RT-SSN", FieldType.STRING, 50);
        pnd_Psgm003_Pnd_Rt_Pin = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Pin", "#RT-PIN", FieldType.STRING, 50);
        pnd_Psgm003_Pnd_Rt_Tiaa_Cntrct_Nmbr = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Tiaa_Cntrct_Nmbr", "#RT-TIAA-CNTRCT-NMBR", 
            FieldType.STRING, 150);
        pnd_Psgm003_Pnd_Rt_Xpayee_Cde = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Xpayee_Cde", "#RT-XPAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Cntrct_Status_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cntrct_Status_Code", "#RT-CNTRCT-STATUS-CODE", 
            FieldType.STRING, 120);
        pnd_Psgm003_Pnd_Rt_Ctrct_Status_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Ctrct_Status_Dt", "#RT-CTRCT-STATUS-DT", FieldType.STRING, 
            8);
        pnd_Psgm003_Pnd_Rt_Cntrct_Issue_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cntrct_Issue_Dt", "#RT-CNTRCT-ISSUE-DT", FieldType.STRING, 
            8);
        pnd_Psgm003_Pnd_Rt_Lob = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Lob", "#RT-LOB", FieldType.STRING, 120);
        pnd_Psgm003_Pnd_Rt_Cref_Contract_Nbr = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cref_Contract_Nbr", "#RT-CREF-CONTRACT-NBR", 
            FieldType.STRING, 150);
        pnd_Psgm003_Pnd_Rt_Cref_Issued_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cref_Issued_Ind", "#RT-CREF-ISSUED-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Ownership_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Ownership_Code", "#RT-OWNERSHIP-CODE", FieldType.STRING, 
            120);
        pnd_Psgm003_Pnd_Rt_Product_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Product_Code", "#RT-PRODUCT-CODE", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Option_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Option_Code", "#RT-OPTION-CODE", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Plan_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Plan_Code", "#RT-PLAN-CODE", FieldType.STRING, 5);
        pnd_Psgm003_Pnd_Rt_Fund_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Fund_Code", "#RT-FUND-CODE", FieldType.STRING, 120);
        pnd_Psgm003_Pnd_Rt_Social_Code = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Social_Code", "#RT-SOCIAL-CODE", FieldType.STRING, 
            120);
        pnd_Psgm003_Pnd_Rt_Custodial_Agreement = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Custodial_Agreement", "#RT-CUSTODIAL-AGREEMENT", 
            FieldType.STRING, 1);
        pnd_Psgm003_Pnd_Rt_Aas_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Aas_Ind", "#RT-AAS-IND", FieldType.STRING, 1);
        pnd_Psgm003_Pnd_Rt_Platform_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Platform_Ind", "#RT-PLATFORM-IND", FieldType.STRING, 
            120);
        pnd_Psgm003_Pnd_Rt_Multi_Plan_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Multi_Plan_Ind", "#RT-MULTI-PLAN-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Tiaa_Mailed_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Tiaa_Mailed_Ind", "#RT-TIAA-MAILED-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Cref_Mailed_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cref_Mailed_Ind", "#RT-CREF-MAILED-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Dflt_Enrollment_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Dflt_Enrollment_Ind", "#RT-DFLT-ENROLLMENT-IND", 
            FieldType.STRING, 1);
        pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Dt", "#RT-CNTRCT-LST-UPDT-DT", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Time = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Time", "#RT-CNTRCT-LST-UPDT-TIME", 
            FieldType.STRING, 7);
        pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Usrid = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cntrct_Lst_Updt_Usrid", "#RT-CNTRCT-LST-UPDT-USRID", 
            FieldType.STRING, 20);
        pnd_Psgm003_Pnd_Rt_Co_Per_Addr_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Per_Addr_Ind", "#RT-CO-PER-ADDR-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Type_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Type_Cd", "#RT-CO-ADDR-TYPE-CD", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Line_One = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Line_One", "#RT-CO-ADDR-LINE-ONE", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Two = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Two", "#RT-CO-ADDR-LINE-TWO", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Three = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Three", "#RT-CO-ADDR-LINE-THREE", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Four = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Four", "#RT-CO-ADDR-LINE-FOUR", 
            FieldType.STRING, 250);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Five = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Line_Five", "#RT-CO-ADDR-LINE-FIVE", 
            FieldType.STRING, 250);
        pnd_Psgm003_Pnd_Rt_Co_Addr_City = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_City", "#RT-CO-ADDR-CITY", FieldType.STRING, 
            35);
        pnd_Psgm003_Pnd_Rt_Co_Addr_State = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_State", "#RT-CO-ADDR-STATE", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Co_Zip = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Zip", "#RT-CO-ZIP", FieldType.STRING, 25);
        pnd_Psgm003_Pnd_Rt_Co_Country = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Country", "#RT-CO-COUNTRY", FieldType.STRING, 
            120);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Postal_Data = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Postal_Data", "#RT-CO-ADDR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Geo_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Geo_Cd", "#RT-CO-ADDR-GEO-CD", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Status = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Status", "#RT-CO-ADDR-STATUS", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Status_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Status_Dt", "#RT-CO-ADDR-STATUS-DT", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Src_Cd", "#RT-CO-ADDR-LST-CH-SRC-CD", 
            FieldType.STRING, 3);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Dt", "#RT-CO-ADDR-LST-CH-DT", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Time = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Time", "#RT-CO-ADDR-LST-CH-TIME", 
            FieldType.STRING, 7);
        pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Usrid = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Co_Addr_Lst_Ch_Usrid", "#RT-CO-ADDR-LST-CH-USRID", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Cm_Per_Addr_Ind = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Per_Addr_Ind", "#RT-CM-PER-ADDR-IND", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Type_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Type_Cd", "#RT-CM-ADDR-TYPE-CD", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_One = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_One", "#RT-CM-ADDR-LINE-ONE", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Two = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Two", "#RT-CM-ADDR-LINE-TWO", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Three = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Three", "#RT-CM-ADDR-LINE-THREE", 
            FieldType.STRING, 100);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Four = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Four", "#RT-CM-ADDR-LINE-FOUR", 
            FieldType.STRING, 250);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Five = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Line_Five", "#RT-CM-ADDR-LINE-FIVE", 
            FieldType.STRING, 250);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_City = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_City", "#RT-CM-ADDR-CITY", FieldType.STRING, 
            35);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_State = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_State", "#RT-CM-ADDR-STATE", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Cm_Zip = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Zip", "#RT-CM-ZIP", FieldType.STRING, 25);
        pnd_Psgm003_Pnd_Rt_Cm_Country = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Country", "#RT-CM-COUNTRY", FieldType.STRING, 
            120);
        pnd_Psgm003_Pnd_Rt_Cm_Ch_Saving_Cde = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Ch_Saving_Cde", "#RT-CM-CH-SAVING-CDE", 
            FieldType.STRING, 1);
        pnd_Psgm003_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Ph_Bnk_Pt_Acct_Nbr", "#RT-CM-PH-BNK-PT-ACCT-NBR", 
            FieldType.STRING, 35);
        pnd_Psgm003_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Bnk_Aba_Eft_Nbr", "#RT-CM-BNK-ABA-EFT-NBR", 
            FieldType.STRING, 35);
        pnd_Psgm003_Pnd_Rt_Cm_Eft_Pay_Type_Cde = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Eft_Pay_Type_Cde", "#RT-CM-EFT-PAY-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Postal_Data = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Postal_Data", "#RT-CM-ADDR-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Geo_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Geo_Cd", "#RT-CM-ADDR-GEO-CD", FieldType.STRING, 
            2);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Status = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Status", "#RT-CM-ADDR-STATUS", FieldType.STRING, 
            1);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Status_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Status_Dt", "#RT-CM-ADDR-STATUS-DT", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Src_Cd", "#RT-CM-ADDR-LST-CH-SRC-CD", 
            FieldType.STRING, 3);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Dt = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Dt", "#RT-CM-ADDR-LST-CH-DT", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Time = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Time", "#RT-CM-ADDR-LST-CH-TIME", 
            FieldType.STRING, 7);
        pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Cm_Addr_Lst_Ch_Usrid", "#RT-CM-ADDR-LST-CH-USRID", 
            FieldType.STRING, 8);
        pnd_Psgm003_Pnd_Rt_Prefix = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Prefix", "#RT-PREFIX", FieldType.STRING, 120);
        pnd_Psgm003_Pnd_Rt_Last_Name = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Last_Name", "#RT-LAST-NAME", FieldType.STRING, 35);
        pnd_Psgm003_Pnd_Rt_First_Name = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_First_Name", "#RT-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Psgm003_Pnd_Rt_Given_Name_Two = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Given_Name_Two", "#RT-GIVEN-NAME-TWO", FieldType.STRING, 
            30);
        pnd_Psgm003_Pnd_Rt_Suffix_Desc = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Suffix_Desc", "#RT-SUFFIX-DESC", FieldType.STRING, 
            20);
        pnd_Psgm003_Pnd_Rt_Record_Status = pnd_Psgm003_Pnd_Rt_Record.newFieldInGroup("pnd_Psgm003_Pnd_Rt_Record_Status", "#RT-RECORD-STATUS", FieldType.STRING, 
            1);

        this.setRecordName("LdaTwrlpsg3");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrlpsg3() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
