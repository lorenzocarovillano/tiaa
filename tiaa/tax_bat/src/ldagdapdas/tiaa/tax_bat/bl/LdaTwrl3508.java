/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:33 PM
**        * FROM NATURAL LDA     : TWRL3508
************************************************************
**        * FILE NAME            : LdaTwrl3508.java
**        * CLASS NAME           : LdaTwrl3508
**        * INSTANCE NAME        : LdaTwrl3508
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3508 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl3508;
    private DbsField pnd_Twrl3508_Pnd_Tax_Year;
    private DbsField pnd_Twrl3508_Pnd_Company_Code;
    private DbsField pnd_Twrl3508_Pnd_Corr_Ind;
    private DbsField pnd_Twrl3508_Pnd_Form;
    private DbsField pnd_Twrl3508_Pnd_Seq_Num;
    private DbsField pnd_Twrl3508_Pnd_Irs_B_State;
    private DbsField pnd_Twrl3508_Pnd_Irs_K_State;

    public DbsGroup getPnd_Twrl3508() { return pnd_Twrl3508; }

    public DbsField getPnd_Twrl3508_Pnd_Tax_Year() { return pnd_Twrl3508_Pnd_Tax_Year; }

    public DbsField getPnd_Twrl3508_Pnd_Company_Code() { return pnd_Twrl3508_Pnd_Company_Code; }

    public DbsField getPnd_Twrl3508_Pnd_Corr_Ind() { return pnd_Twrl3508_Pnd_Corr_Ind; }

    public DbsField getPnd_Twrl3508_Pnd_Form() { return pnd_Twrl3508_Pnd_Form; }

    public DbsField getPnd_Twrl3508_Pnd_Seq_Num() { return pnd_Twrl3508_Pnd_Seq_Num; }

    public DbsField getPnd_Twrl3508_Pnd_Irs_B_State() { return pnd_Twrl3508_Pnd_Irs_B_State; }

    public DbsField getPnd_Twrl3508_Pnd_Irs_K_State() { return pnd_Twrl3508_Pnd_Irs_K_State; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl3508 = newGroupInRecord("pnd_Twrl3508", "#TWRL3508");
        pnd_Twrl3508_Pnd_Tax_Year = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        pnd_Twrl3508_Pnd_Company_Code = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Company_Code", "#COMPANY-CODE", FieldType.STRING, 1);
        pnd_Twrl3508_Pnd_Corr_Ind = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Corr_Ind", "#CORR-IND", FieldType.STRING, 1);
        pnd_Twrl3508_Pnd_Form = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Form", "#FORM", FieldType.NUMERIC, 2);
        pnd_Twrl3508_Pnd_Seq_Num = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Seq_Num", "#SEQ-NUM", FieldType.PACKED_DECIMAL, 9);
        pnd_Twrl3508_Pnd_Irs_B_State = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Irs_B_State", "#IRS-B-STATE", FieldType.NUMERIC, 2);
        pnd_Twrl3508_Pnd_Irs_K_State = pnd_Twrl3508.newFieldInGroup("pnd_Twrl3508_Pnd_Irs_K_State", "#IRS-K-STATE", FieldType.NUMERIC, 2);

        this.setRecordName("LdaTwrl3508");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3508() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
