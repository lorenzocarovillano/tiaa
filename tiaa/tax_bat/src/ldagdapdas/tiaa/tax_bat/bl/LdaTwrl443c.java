/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:39 PM
**        * FROM NATURAL LDA     : TWRL443C
************************************************************
**        * FILE NAME            : LdaTwrl443c.java
**        * CLASS NAME           : LdaTwrl443c
**        * INSTANCE NAME        : LdaTwrl443c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_B_Tape;
    private DbsField pnd_B_Tape_Pnd_B_Record_Type;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Year;
    private DbsField pnd_B_Tape_Pnd_B_Return_Indicator;
    private DbsField pnd_B_Tape_Pnd_B_Name_Control;
    private DbsField pnd_B_Tape_Pnd_B_Type_Of_Tin;
    private DbsField pnd_B_Tape_Pnd_B_Taxpayer_Id_Numb;
    private DbsField pnd_B_Tape_Pnd_B_Unique_Acct_Nbr;
    private DbsGroup pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1;
    private DbsField pnd_B_Tape_Pnd_B_Unique_Acct_Num;
    private DbsGroup pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2;
    private DbsField pnd_B_Tape_Pnd_B_Tin_Last_4;
    private DbsField pnd_B_Tape_Pnd_B_Contract_Payee;
    private DbsField pnd_B_Tape_Pnd_B_Unique_Acct_Seq;
    private DbsField pnd_B_Tape_Pnd_B_Unique_Acct_Filler;
    private DbsField pnd_B_Tape_Pnd_B_Payers_Office_Code;
    private DbsField pnd_B_Tape_Pnd_B_Filler_1;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_1;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_2;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_3;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_4;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_5;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_6;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_7;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_8;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_9;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_A;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_B;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_C;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_D;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_E;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_F;
    private DbsField pnd_B_Tape_Pnd_B_Payment_Amount_G;
    private DbsField pnd_B_Tape_Pnd_B_Foreign_Indicator;
    private DbsField pnd_B_Tape_Pnd_B_Name_Line_1;
    private DbsField pnd_B_Tape_Pnd_B_Name_Line_2;
    private DbsField pnd_B_Tape_Pnd_B_Filler_3;
    private DbsField pnd_B_Tape_Pnd_B_Address_Line;
    private DbsField pnd_B_Tape_Pnd_B_Filler_4;
    private DbsField pnd_B_Tape_Pnd_B_Foreign_Address_Line;
    private DbsGroup pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3;
    private DbsField pnd_B_Tape_Pnd_B_City;
    private DbsField pnd_B_Tape_Pnd_B_State;
    private DbsField pnd_B_Tape_Pnd_B_Zip;
    private DbsField pnd_B_Tape_Pnd_B_Filler_5;
    private DbsField pnd_B_Tape_Pnd_B_Sequence_Number;
    private DbsField pnd_B_Tape_Pnd_B_Filler_6;
    private DbsField pnd_B_Tape_Pnd_B_Ira_Ind;
    private DbsField pnd_B_Tape_Pnd_B_Sep_Ind;
    private DbsField pnd_B_Tape_Pnd_B_Simple_Ind;
    private DbsField pnd_B_Tape_Pnd_B_Roth_Ind;
    private DbsField pnd_B_Tape_Pnd_B_Rmd_Ind;
    private DbsField pnd_B_Tape_Pnd_B_Yr_Pst_Cnt;
    private DbsField pnd_B_Tape_Pnd_B_Postpn_Code;
    private DbsField pnd_B_Tape_Pnd_B_Postpn_Rsn;
    private DbsField pnd_B_Tape_Pnd_B_Rep_Code;
    private DbsField pnd_B_Tape_Pnd_B_Rmd_Date;
    private DbsField pnd_B_Tape_Pnd_B_Codes;
    private DbsField pnd_B_Tape_Pnd_B_Filler_8;
    private DbsField pnd_B_Tape_Pnd_B_Special_Data_Entries;
    private DbsField pnd_B_Tape_Pnd_B_Filler_9;
    private DbsField pnd_B_Tape_Pnd_B_Combined_State_Code;
    private DbsField pnd_B_Tape_Pnd_B_Filler_A;
    private DbsGroup pnd_B_TapeRedef4;
    private DbsField pnd_B_Tape_Pnd_B_Move_1;
    private DbsField pnd_B_Tape_Pnd_B_Move_2;
    private DbsField pnd_B_Tape_Pnd_B_Move_3;

    public DbsGroup getPnd_B_Tape() { return pnd_B_Tape; }

    public DbsField getPnd_B_Tape_Pnd_B_Record_Type() { return pnd_B_Tape_Pnd_B_Record_Type; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Year() { return pnd_B_Tape_Pnd_B_Payment_Year; }

    public DbsField getPnd_B_Tape_Pnd_B_Return_Indicator() { return pnd_B_Tape_Pnd_B_Return_Indicator; }

    public DbsField getPnd_B_Tape_Pnd_B_Name_Control() { return pnd_B_Tape_Pnd_B_Name_Control; }

    public DbsField getPnd_B_Tape_Pnd_B_Type_Of_Tin() { return pnd_B_Tape_Pnd_B_Type_Of_Tin; }

    public DbsField getPnd_B_Tape_Pnd_B_Taxpayer_Id_Numb() { return pnd_B_Tape_Pnd_B_Taxpayer_Id_Numb; }

    public DbsField getPnd_B_Tape_Pnd_B_Unique_Acct_Nbr() { return pnd_B_Tape_Pnd_B_Unique_Acct_Nbr; }

    public DbsGroup getPnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1() { return pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1; }

    public DbsField getPnd_B_Tape_Pnd_B_Unique_Acct_Num() { return pnd_B_Tape_Pnd_B_Unique_Acct_Num; }

    public DbsGroup getPnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2() { return pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2; }

    public DbsField getPnd_B_Tape_Pnd_B_Tin_Last_4() { return pnd_B_Tape_Pnd_B_Tin_Last_4; }

    public DbsField getPnd_B_Tape_Pnd_B_Contract_Payee() { return pnd_B_Tape_Pnd_B_Contract_Payee; }

    public DbsField getPnd_B_Tape_Pnd_B_Unique_Acct_Seq() { return pnd_B_Tape_Pnd_B_Unique_Acct_Seq; }

    public DbsField getPnd_B_Tape_Pnd_B_Unique_Acct_Filler() { return pnd_B_Tape_Pnd_B_Unique_Acct_Filler; }

    public DbsField getPnd_B_Tape_Pnd_B_Payers_Office_Code() { return pnd_B_Tape_Pnd_B_Payers_Office_Code; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_1() { return pnd_B_Tape_Pnd_B_Filler_1; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_1() { return pnd_B_Tape_Pnd_B_Payment_Amount_1; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_2() { return pnd_B_Tape_Pnd_B_Payment_Amount_2; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_3() { return pnd_B_Tape_Pnd_B_Payment_Amount_3; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_4() { return pnd_B_Tape_Pnd_B_Payment_Amount_4; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_5() { return pnd_B_Tape_Pnd_B_Payment_Amount_5; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_6() { return pnd_B_Tape_Pnd_B_Payment_Amount_6; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_7() { return pnd_B_Tape_Pnd_B_Payment_Amount_7; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_8() { return pnd_B_Tape_Pnd_B_Payment_Amount_8; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_9() { return pnd_B_Tape_Pnd_B_Payment_Amount_9; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_A() { return pnd_B_Tape_Pnd_B_Payment_Amount_A; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_B() { return pnd_B_Tape_Pnd_B_Payment_Amount_B; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_C() { return pnd_B_Tape_Pnd_B_Payment_Amount_C; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_D() { return pnd_B_Tape_Pnd_B_Payment_Amount_D; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_E() { return pnd_B_Tape_Pnd_B_Payment_Amount_E; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_F() { return pnd_B_Tape_Pnd_B_Payment_Amount_F; }

    public DbsField getPnd_B_Tape_Pnd_B_Payment_Amount_G() { return pnd_B_Tape_Pnd_B_Payment_Amount_G; }

    public DbsField getPnd_B_Tape_Pnd_B_Foreign_Indicator() { return pnd_B_Tape_Pnd_B_Foreign_Indicator; }

    public DbsField getPnd_B_Tape_Pnd_B_Name_Line_1() { return pnd_B_Tape_Pnd_B_Name_Line_1; }

    public DbsField getPnd_B_Tape_Pnd_B_Name_Line_2() { return pnd_B_Tape_Pnd_B_Name_Line_2; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_3() { return pnd_B_Tape_Pnd_B_Filler_3; }

    public DbsField getPnd_B_Tape_Pnd_B_Address_Line() { return pnd_B_Tape_Pnd_B_Address_Line; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_4() { return pnd_B_Tape_Pnd_B_Filler_4; }

    public DbsField getPnd_B_Tape_Pnd_B_Foreign_Address_Line() { return pnd_B_Tape_Pnd_B_Foreign_Address_Line; }

    public DbsGroup getPnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3() { return pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3; }

    public DbsField getPnd_B_Tape_Pnd_B_City() { return pnd_B_Tape_Pnd_B_City; }

    public DbsField getPnd_B_Tape_Pnd_B_State() { return pnd_B_Tape_Pnd_B_State; }

    public DbsField getPnd_B_Tape_Pnd_B_Zip() { return pnd_B_Tape_Pnd_B_Zip; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_5() { return pnd_B_Tape_Pnd_B_Filler_5; }

    public DbsField getPnd_B_Tape_Pnd_B_Sequence_Number() { return pnd_B_Tape_Pnd_B_Sequence_Number; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_6() { return pnd_B_Tape_Pnd_B_Filler_6; }

    public DbsField getPnd_B_Tape_Pnd_B_Ira_Ind() { return pnd_B_Tape_Pnd_B_Ira_Ind; }

    public DbsField getPnd_B_Tape_Pnd_B_Sep_Ind() { return pnd_B_Tape_Pnd_B_Sep_Ind; }

    public DbsField getPnd_B_Tape_Pnd_B_Simple_Ind() { return pnd_B_Tape_Pnd_B_Simple_Ind; }

    public DbsField getPnd_B_Tape_Pnd_B_Roth_Ind() { return pnd_B_Tape_Pnd_B_Roth_Ind; }

    public DbsField getPnd_B_Tape_Pnd_B_Rmd_Ind() { return pnd_B_Tape_Pnd_B_Rmd_Ind; }

    public DbsField getPnd_B_Tape_Pnd_B_Yr_Pst_Cnt() { return pnd_B_Tape_Pnd_B_Yr_Pst_Cnt; }

    public DbsField getPnd_B_Tape_Pnd_B_Postpn_Code() { return pnd_B_Tape_Pnd_B_Postpn_Code; }

    public DbsField getPnd_B_Tape_Pnd_B_Postpn_Rsn() { return pnd_B_Tape_Pnd_B_Postpn_Rsn; }

    public DbsField getPnd_B_Tape_Pnd_B_Rep_Code() { return pnd_B_Tape_Pnd_B_Rep_Code; }

    public DbsField getPnd_B_Tape_Pnd_B_Rmd_Date() { return pnd_B_Tape_Pnd_B_Rmd_Date; }

    public DbsField getPnd_B_Tape_Pnd_B_Codes() { return pnd_B_Tape_Pnd_B_Codes; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_8() { return pnd_B_Tape_Pnd_B_Filler_8; }

    public DbsField getPnd_B_Tape_Pnd_B_Special_Data_Entries() { return pnd_B_Tape_Pnd_B_Special_Data_Entries; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_9() { return pnd_B_Tape_Pnd_B_Filler_9; }

    public DbsField getPnd_B_Tape_Pnd_B_Combined_State_Code() { return pnd_B_Tape_Pnd_B_Combined_State_Code; }

    public DbsField getPnd_B_Tape_Pnd_B_Filler_A() { return pnd_B_Tape_Pnd_B_Filler_A; }

    public DbsGroup getPnd_B_TapeRedef4() { return pnd_B_TapeRedef4; }

    public DbsField getPnd_B_Tape_Pnd_B_Move_1() { return pnd_B_Tape_Pnd_B_Move_1; }

    public DbsField getPnd_B_Tape_Pnd_B_Move_2() { return pnd_B_Tape_Pnd_B_Move_2; }

    public DbsField getPnd_B_Tape_Pnd_B_Move_3() { return pnd_B_Tape_Pnd_B_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_B_Tape = newGroupInRecord("pnd_B_Tape", "#B-TAPE");
        pnd_B_Tape_Pnd_B_Record_Type = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Record_Type", "#B-RECORD-TYPE", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Payment_Year = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Year", "#B-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_B_Tape_Pnd_B_Return_Indicator = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Return_Indicator", "#B-RETURN-INDICATOR", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Name_Control = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Name_Control", "#B-NAME-CONTROL", FieldType.STRING, 4);
        pnd_B_Tape_Pnd_B_Type_Of_Tin = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Type_Of_Tin", "#B-TYPE-OF-TIN", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Taxpayer_Id_Numb = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Taxpayer_Id_Numb", "#B-TAXPAYER-ID-NUMB", FieldType.STRING, 9);
        pnd_B_Tape_Pnd_B_Unique_Acct_Nbr = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_Nbr", "#B-UNIQUE-ACCT-NBR", FieldType.STRING, 20);
        pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1 = pnd_B_Tape.newGroupInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1", "Redefines", pnd_B_Tape_Pnd_B_Unique_Acct_Nbr);
        pnd_B_Tape_Pnd_B_Unique_Acct_Num = pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1.newFieldInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_Num", "#B-UNIQUE-ACCT-NUM", 
            FieldType.STRING, 14);
        pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2 = pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1.newGroupInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2", "Redefines", 
            pnd_B_Tape_Pnd_B_Unique_Acct_Num);
        pnd_B_Tape_Pnd_B_Tin_Last_4 = pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2.newFieldInGroup("pnd_B_Tape_Pnd_B_Tin_Last_4", "#B-TIN-LAST-4", FieldType.NUMERIC, 
            4);
        pnd_B_Tape_Pnd_B_Contract_Payee = pnd_B_Tape_Pnd_B_Unique_Acct_NumRedef2.newFieldInGroup("pnd_B_Tape_Pnd_B_Contract_Payee", "#B-CONTRACT-PAYEE", 
            FieldType.STRING, 10);
        pnd_B_Tape_Pnd_B_Unique_Acct_Seq = pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1.newFieldInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_Seq", "#B-UNIQUE-ACCT-SEQ", 
            FieldType.NUMERIC, 3);
        pnd_B_Tape_Pnd_B_Unique_Acct_Filler = pnd_B_Tape_Pnd_B_Unique_Acct_NbrRedef1.newFieldInGroup("pnd_B_Tape_Pnd_B_Unique_Acct_Filler", "#B-UNIQUE-ACCT-FILLER", 
            FieldType.STRING, 3);
        pnd_B_Tape_Pnd_B_Payers_Office_Code = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payers_Office_Code", "#B-PAYERS-OFFICE-CODE", FieldType.STRING, 
            4);
        pnd_B_Tape_Pnd_B_Filler_1 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_1", "#B-FILLER-1", FieldType.STRING, 10);
        pnd_B_Tape_Pnd_B_Payment_Amount_1 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_1", "#B-PAYMENT-AMOUNT-1", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_2 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_2", "#B-PAYMENT-AMOUNT-2", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_3 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_3", "#B-PAYMENT-AMOUNT-3", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_4 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_4", "#B-PAYMENT-AMOUNT-4", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_5 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_5", "#B-PAYMENT-AMOUNT-5", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_6 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_6", "#B-PAYMENT-AMOUNT-6", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_7 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_7", "#B-PAYMENT-AMOUNT-7", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_8 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_8", "#B-PAYMENT-AMOUNT-8", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_9 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_9", "#B-PAYMENT-AMOUNT-9", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_A = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_A", "#B-PAYMENT-AMOUNT-A", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_B = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_B", "#B-PAYMENT-AMOUNT-B", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_C = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_C", "#B-PAYMENT-AMOUNT-C", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_D = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_D", "#B-PAYMENT-AMOUNT-D", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_E = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_E", "#B-PAYMENT-AMOUNT-E", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_F = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_F", "#B-PAYMENT-AMOUNT-F", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Payment_Amount_G = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Payment_Amount_G", "#B-PAYMENT-AMOUNT-G", FieldType.DECIMAL, 
            12,2);
        pnd_B_Tape_Pnd_B_Foreign_Indicator = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Foreign_Indicator", "#B-FOREIGN-INDICATOR", FieldType.STRING, 
            1);
        pnd_B_Tape_Pnd_B_Name_Line_1 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Name_Line_1", "#B-NAME-LINE-1", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_Name_Line_2 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Name_Line_2", "#B-NAME-LINE-2", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_Filler_3 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_3", "#B-FILLER-3", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_Address_Line = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Address_Line", "#B-ADDRESS-LINE", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_Filler_4 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_4", "#B-FILLER-4", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_Foreign_Address_Line = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Foreign_Address_Line", "#B-FOREIGN-ADDRESS-LINE", FieldType.STRING, 
            51);
        pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3 = pnd_B_Tape.newGroupInGroup("pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3", "Redefines", pnd_B_Tape_Pnd_B_Foreign_Address_Line);
        pnd_B_Tape_Pnd_B_City = pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3.newFieldInGroup("pnd_B_Tape_Pnd_B_City", "#B-CITY", FieldType.STRING, 40);
        pnd_B_Tape_Pnd_B_State = pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3.newFieldInGroup("pnd_B_Tape_Pnd_B_State", "#B-STATE", FieldType.STRING, 2);
        pnd_B_Tape_Pnd_B_Zip = pnd_B_Tape_Pnd_B_Foreign_Address_LineRedef3.newFieldInGroup("pnd_B_Tape_Pnd_B_Zip", "#B-ZIP", FieldType.STRING, 9);
        pnd_B_Tape_Pnd_B_Filler_5 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_5", "#B-FILLER-5", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Sequence_Number = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Sequence_Number", "#B-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_B_Tape_Pnd_B_Filler_6 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_6", "#B-FILLER-6", FieldType.STRING, 39);
        pnd_B_Tape_Pnd_B_Ira_Ind = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Ira_Ind", "#B-IRA-IND", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Sep_Ind = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Sep_Ind", "#B-SEP-IND", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Simple_Ind = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Simple_Ind", "#B-SIMPLE-IND", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Roth_Ind = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Roth_Ind", "#B-ROTH-IND", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Rmd_Ind = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Rmd_Ind", "#B-RMD-IND", FieldType.STRING, 1);
        pnd_B_Tape_Pnd_B_Yr_Pst_Cnt = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Yr_Pst_Cnt", "#B-YR-PST-CNT", FieldType.STRING, 4);
        pnd_B_Tape_Pnd_B_Postpn_Code = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Postpn_Code", "#B-POSTPN-CODE", FieldType.STRING, 2);
        pnd_B_Tape_Pnd_B_Postpn_Rsn = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Postpn_Rsn", "#B-POSTPN-RSN", FieldType.STRING, 6);
        pnd_B_Tape_Pnd_B_Rep_Code = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Rep_Code", "#B-REP-CODE", FieldType.STRING, 2);
        pnd_B_Tape_Pnd_B_Rmd_Date = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Rmd_Date", "#B-RMD-DATE", FieldType.STRING, 8);
        pnd_B_Tape_Pnd_B_Codes = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Codes", "#B-CODES", FieldType.STRING, 2);
        pnd_B_Tape_Pnd_B_Filler_8 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_8", "#B-FILLER-8", FieldType.STRING, 87);
        pnd_B_Tape_Pnd_B_Special_Data_Entries = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Special_Data_Entries", "#B-SPECIAL-DATA-ENTRIES", FieldType.STRING, 
            60);
        pnd_B_Tape_Pnd_B_Filler_9 = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_9", "#B-FILLER-9", FieldType.STRING, 24);
        pnd_B_Tape_Pnd_B_Combined_State_Code = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Combined_State_Code", "#B-COMBINED-STATE-CODE", FieldType.STRING, 
            2);
        pnd_B_Tape_Pnd_B_Filler_A = pnd_B_Tape.newFieldInGroup("pnd_B_Tape_Pnd_B_Filler_A", "#B-FILLER-A", FieldType.STRING, 2);
        pnd_B_TapeRedef4 = newGroupInRecord("pnd_B_TapeRedef4", "Redefines", pnd_B_Tape);
        pnd_B_Tape_Pnd_B_Move_1 = pnd_B_TapeRedef4.newFieldInGroup("pnd_B_Tape_Pnd_B_Move_1", "#B-MOVE-1", FieldType.STRING, 250);
        pnd_B_Tape_Pnd_B_Move_2 = pnd_B_TapeRedef4.newFieldInGroup("pnd_B_Tape_Pnd_B_Move_2", "#B-MOVE-2", FieldType.STRING, 250);
        pnd_B_Tape_Pnd_B_Move_3 = pnd_B_TapeRedef4.newFieldInGroup("pnd_B_Tape_Pnd_B_Move_3", "#B-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_B_Tape_Pnd_B_Record_Type.setInitialValue("B");
        pnd_B_Tape_Pnd_B_Return_Indicator.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Name_Control.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Payers_Office_Code.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_1.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Payment_Amount_1.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_2.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_3.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_4.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_5.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_6.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_7.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_8.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_9.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_A.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_B.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_C.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_D.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_E.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_F.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Payment_Amount_G.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Filler_3.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_4.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_5.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Sequence_Number.setInitialValue(0);
        pnd_B_Tape_Pnd_B_Filler_6.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Ira_Ind.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Sep_Ind.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Simple_Ind.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Roth_Ind.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Rmd_Ind.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Yr_Pst_Cnt.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Postpn_Code.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Postpn_Rsn.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Rep_Code.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Rmd_Date.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Codes.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_8.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Special_Data_Entries.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_9.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Combined_State_Code.setInitialValue(" ");
        pnd_B_Tape_Pnd_B_Filler_A.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
