/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:11 PM
**        * FROM NATURAL LDA     : TWRL5951
************************************************************
**        * FILE NAME            : LdaTwrl5951.java
**        * CLASS NAME           : LdaTwrl5951
**        * INSTANCE NAME        : LdaTwrl5951
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl5951 extends DbsRecord
{
    // Properties
    private DbsGroup twrl5951;
    private DbsField twrl5951_Pnd_Tax_Year;
    private DbsField twrl5951_Pnd_Form_Type;
    private DbsField twrl5951_Pnd_Comp_Short_Name;
    private DbsField twrl5951_Pnd_Tin;
    private DbsField twrl5951_Pnd_Contract_Nbr;
    private DbsField twrl5951_Pnd_Payee_Cde;
    private DbsField twrl5951_Pnd_Err_Cde;
    private DbsField twrl5951_Pnd_Moore_Mail;
    private DbsField twrl5951_Pnd_Vol_Printable;
    private DbsField twrl5951_Pnd_Irs_Reportable;
    private DbsField twrl5951_Pnd_In_House_Mail;

    public DbsGroup getTwrl5951() { return twrl5951; }

    public DbsField getTwrl5951_Pnd_Tax_Year() { return twrl5951_Pnd_Tax_Year; }

    public DbsField getTwrl5951_Pnd_Form_Type() { return twrl5951_Pnd_Form_Type; }

    public DbsField getTwrl5951_Pnd_Comp_Short_Name() { return twrl5951_Pnd_Comp_Short_Name; }

    public DbsField getTwrl5951_Pnd_Tin() { return twrl5951_Pnd_Tin; }

    public DbsField getTwrl5951_Pnd_Contract_Nbr() { return twrl5951_Pnd_Contract_Nbr; }

    public DbsField getTwrl5951_Pnd_Payee_Cde() { return twrl5951_Pnd_Payee_Cde; }

    public DbsField getTwrl5951_Pnd_Err_Cde() { return twrl5951_Pnd_Err_Cde; }

    public DbsField getTwrl5951_Pnd_Moore_Mail() { return twrl5951_Pnd_Moore_Mail; }

    public DbsField getTwrl5951_Pnd_Vol_Printable() { return twrl5951_Pnd_Vol_Printable; }

    public DbsField getTwrl5951_Pnd_Irs_Reportable() { return twrl5951_Pnd_Irs_Reportable; }

    public DbsField getTwrl5951_Pnd_In_House_Mail() { return twrl5951_Pnd_In_House_Mail; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        twrl5951 = newGroupInRecord("twrl5951", "TWRL5951");
        twrl5951_Pnd_Tax_Year = twrl5951.newFieldInGroup("twrl5951_Pnd_Tax_Year", "#TAX-YEAR", FieldType.STRING, 4);
        twrl5951_Pnd_Form_Type = twrl5951.newFieldInGroup("twrl5951_Pnd_Form_Type", "#FORM-TYPE", FieldType.NUMERIC, 2);
        twrl5951_Pnd_Comp_Short_Name = twrl5951.newFieldInGroup("twrl5951_Pnd_Comp_Short_Name", "#COMP-SHORT-NAME", FieldType.STRING, 4);
        twrl5951_Pnd_Tin = twrl5951.newFieldInGroup("twrl5951_Pnd_Tin", "#TIN", FieldType.STRING, 10);
        twrl5951_Pnd_Contract_Nbr = twrl5951.newFieldInGroup("twrl5951_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 8);
        twrl5951_Pnd_Payee_Cde = twrl5951.newFieldInGroup("twrl5951_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 2);
        twrl5951_Pnd_Err_Cde = twrl5951.newFieldInGroup("twrl5951_Pnd_Err_Cde", "#ERR-CDE", FieldType.NUMERIC, 3);
        twrl5951_Pnd_Moore_Mail = twrl5951.newFieldInGroup("twrl5951_Pnd_Moore_Mail", "#MOORE-MAIL", FieldType.STRING, 1);
        twrl5951_Pnd_Vol_Printable = twrl5951.newFieldInGroup("twrl5951_Pnd_Vol_Printable", "#VOL-PRINTABLE", FieldType.STRING, 1);
        twrl5951_Pnd_Irs_Reportable = twrl5951.newFieldInGroup("twrl5951_Pnd_Irs_Reportable", "#IRS-REPORTABLE", FieldType.STRING, 1);
        twrl5951_Pnd_In_House_Mail = twrl5951.newFieldInGroup("twrl5951_Pnd_In_House_Mail", "#IN-HOUSE-MAIL", FieldType.STRING, 1);

        this.setRecordName("LdaTwrl5951");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl5951() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
