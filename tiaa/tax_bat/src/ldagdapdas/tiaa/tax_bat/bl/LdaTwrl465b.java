/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:02 PM
**        * FROM NATURAL LDA     : TWRL465B
************************************************************
**        * FILE NAME            : LdaTwrl465b.java
**        * CLASS NAME           : LdaTwrl465b
**        * INSTANCE NAME        : LdaTwrl465b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl465b extends DbsRecord
{
    // Properties
    private DbsGroup ira_Sum_Record;
    private DbsField ira_Sum_Record_Ira_Sum_Id;
    private DbsField ira_Sum_Record_Ira_Sum_Company_Code;
    private DbsField ira_Sum_Record_Ira_Sum_Feed_Period;
    private DbsField ira_Sum_Record_Ira_Sum_Feed_Date;
    private DbsField ira_Sum_Record_Ira_Sum_Records_Count;
    private DbsField ira_Sum_Record_Ira_Sum_Classic_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Roth_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Rollover_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Recharacter_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Education_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Fmv_Tot;
    private DbsField ira_Sum_Record_Ira_Sum_Roth_Conv_Tot;

    public DbsGroup getIra_Sum_Record() { return ira_Sum_Record; }

    public DbsField getIra_Sum_Record_Ira_Sum_Id() { return ira_Sum_Record_Ira_Sum_Id; }

    public DbsField getIra_Sum_Record_Ira_Sum_Company_Code() { return ira_Sum_Record_Ira_Sum_Company_Code; }

    public DbsField getIra_Sum_Record_Ira_Sum_Feed_Period() { return ira_Sum_Record_Ira_Sum_Feed_Period; }

    public DbsField getIra_Sum_Record_Ira_Sum_Feed_Date() { return ira_Sum_Record_Ira_Sum_Feed_Date; }

    public DbsField getIra_Sum_Record_Ira_Sum_Records_Count() { return ira_Sum_Record_Ira_Sum_Records_Count; }

    public DbsField getIra_Sum_Record_Ira_Sum_Classic_Tot() { return ira_Sum_Record_Ira_Sum_Classic_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Roth_Tot() { return ira_Sum_Record_Ira_Sum_Roth_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Rollover_Tot() { return ira_Sum_Record_Ira_Sum_Rollover_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Recharacter_Tot() { return ira_Sum_Record_Ira_Sum_Recharacter_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Education_Tot() { return ira_Sum_Record_Ira_Sum_Education_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Fmv_Tot() { return ira_Sum_Record_Ira_Sum_Fmv_Tot; }

    public DbsField getIra_Sum_Record_Ira_Sum_Roth_Conv_Tot() { return ira_Sum_Record_Ira_Sum_Roth_Conv_Tot; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ira_Sum_Record = newGroupInRecord("ira_Sum_Record", "IRA-SUM-RECORD");
        ira_Sum_Record_Ira_Sum_Id = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Id", "IRA-SUM-ID", FieldType.STRING, 3);
        ira_Sum_Record_Ira_Sum_Company_Code = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Company_Code", "IRA-SUM-COMPANY-CODE", FieldType.STRING, 
            1);
        ira_Sum_Record_Ira_Sum_Feed_Period = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Feed_Period", "IRA-SUM-FEED-PERIOD", FieldType.STRING, 
            3);
        ira_Sum_Record_Ira_Sum_Feed_Date = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Feed_Date", "IRA-SUM-FEED-DATE", FieldType.STRING, 8);
        ira_Sum_Record_Ira_Sum_Records_Count = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Records_Count", "IRA-SUM-RECORDS-COUNT", FieldType.NUMERIC, 
            10);
        ira_Sum_Record_Ira_Sum_Classic_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Classic_Tot", "IRA-SUM-CLASSIC-TOT", FieldType.DECIMAL, 
            17,2);
        ira_Sum_Record_Ira_Sum_Roth_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Roth_Tot", "IRA-SUM-ROTH-TOT", FieldType.DECIMAL, 17,
            2);
        ira_Sum_Record_Ira_Sum_Rollover_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Rollover_Tot", "IRA-SUM-ROLLOVER-TOT", FieldType.DECIMAL, 
            17,2);
        ira_Sum_Record_Ira_Sum_Recharacter_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Recharacter_Tot", "IRA-SUM-RECHARACTER-TOT", FieldType.DECIMAL, 
            17,2);
        ira_Sum_Record_Ira_Sum_Education_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Education_Tot", "IRA-SUM-EDUCATION-TOT", FieldType.DECIMAL, 
            17,2);
        ira_Sum_Record_Ira_Sum_Fmv_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Fmv_Tot", "IRA-SUM-FMV-TOT", FieldType.DECIMAL, 17,2);
        ira_Sum_Record_Ira_Sum_Roth_Conv_Tot = ira_Sum_Record.newFieldInGroup("ira_Sum_Record_Ira_Sum_Roth_Conv_Tot", "IRA-SUM-ROTH-CONV-TOT", FieldType.DECIMAL, 
            17,2);

        this.setRecordName("LdaTwrl465b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl465b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
