/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:26 PM
**        * FROM NATURAL LDA     : TWRL190A
************************************************************
**        * FILE NAME            : LdaTwrl190a.java
**        * CLASS NAME           : LdaTwrl190a
**        * INSTANCE NAME        : LdaTwrl190a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl190a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_F96_Ff1;
    private DbsField pnd_F96_Ff1_Twrc_Tax_Year;
    private DbsField pnd_F96_Ff1_Twrc_Company_Cde;
    private DbsField pnd_F96_Ff1_Twrc_Status;
    private DbsField pnd_F96_Ff1_Twrc_Tax_Id;
    private DbsField pnd_F96_Ff1_Twrc_Contract;
    private DbsField pnd_F96_Ff1_Twrc_Payee;
    private DbsField pnd_F96_Ff1_Pnd_Twrc_Source_Upd;
    private DbsGroup pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1;
    private DbsField pnd_F96_Ff1_Pnd_Twrc_Orig_Sc;
    private DbsField pnd_F96_Ff1_Pnd_Twrc_Updt_Sc;
    private DbsField pnd_F96_Ff1_Twrc_Classic_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Roth_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Rollover_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Rechar_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Sep_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Roth_Conversion_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Postpn_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Repayments_Amt;
    private DbsField pnd_F96_Ff1_Twrc_Create_Date;
    private DbsField pnd_F96_Ff1_Twrc_Update_Date;
    private DbsField pnd_F96_Ff1_Twrc_Update_Time;

    public DbsGroup getPnd_F96_Ff1() { return pnd_F96_Ff1; }

    public DbsField getPnd_F96_Ff1_Twrc_Tax_Year() { return pnd_F96_Ff1_Twrc_Tax_Year; }

    public DbsField getPnd_F96_Ff1_Twrc_Company_Cde() { return pnd_F96_Ff1_Twrc_Company_Cde; }

    public DbsField getPnd_F96_Ff1_Twrc_Status() { return pnd_F96_Ff1_Twrc_Status; }

    public DbsField getPnd_F96_Ff1_Twrc_Tax_Id() { return pnd_F96_Ff1_Twrc_Tax_Id; }

    public DbsField getPnd_F96_Ff1_Twrc_Contract() { return pnd_F96_Ff1_Twrc_Contract; }

    public DbsField getPnd_F96_Ff1_Twrc_Payee() { return pnd_F96_Ff1_Twrc_Payee; }

    public DbsField getPnd_F96_Ff1_Pnd_Twrc_Source_Upd() { return pnd_F96_Ff1_Pnd_Twrc_Source_Upd; }

    public DbsGroup getPnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1() { return pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1; }

    public DbsField getPnd_F96_Ff1_Pnd_Twrc_Orig_Sc() { return pnd_F96_Ff1_Pnd_Twrc_Orig_Sc; }

    public DbsField getPnd_F96_Ff1_Pnd_Twrc_Updt_Sc() { return pnd_F96_Ff1_Pnd_Twrc_Updt_Sc; }

    public DbsField getPnd_F96_Ff1_Twrc_Classic_Amt() { return pnd_F96_Ff1_Twrc_Classic_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Roth_Amt() { return pnd_F96_Ff1_Twrc_Roth_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Rollover_Amt() { return pnd_F96_Ff1_Twrc_Rollover_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Rechar_Amt() { return pnd_F96_Ff1_Twrc_Rechar_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Sep_Amt() { return pnd_F96_Ff1_Twrc_Sep_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt() { return pnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Roth_Conversion_Amt() { return pnd_F96_Ff1_Twrc_Roth_Conversion_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Postpn_Amt() { return pnd_F96_Ff1_Twrc_Postpn_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Repayments_Amt() { return pnd_F96_Ff1_Twrc_Repayments_Amt; }

    public DbsField getPnd_F96_Ff1_Twrc_Create_Date() { return pnd_F96_Ff1_Twrc_Create_Date; }

    public DbsField getPnd_F96_Ff1_Twrc_Update_Date() { return pnd_F96_Ff1_Twrc_Update_Date; }

    public DbsField getPnd_F96_Ff1_Twrc_Update_Time() { return pnd_F96_Ff1_Twrc_Update_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_F96_Ff1 = newGroupInRecord("pnd_F96_Ff1", "#F96-FF1");
        pnd_F96_Ff1_Twrc_Tax_Year = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4);
        pnd_F96_Ff1_Twrc_Company_Cde = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Company_Cde", "TWRC-COMPANY-CDE", FieldType.STRING, 1);
        pnd_F96_Ff1_Twrc_Status = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1);
        pnd_F96_Ff1_Twrc_Tax_Id = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10);
        pnd_F96_Ff1_Twrc_Contract = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8);
        pnd_F96_Ff1_Twrc_Payee = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2);
        pnd_F96_Ff1_Pnd_Twrc_Source_Upd = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Pnd_Twrc_Source_Upd", "#TWRC-SOURCE-UPD", FieldType.STRING, 6);
        pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1 = pnd_F96_Ff1.newGroupInGroup("pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1", "Redefines", pnd_F96_Ff1_Pnd_Twrc_Source_Upd);
        pnd_F96_Ff1_Pnd_Twrc_Orig_Sc = pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1.newFieldInGroup("pnd_F96_Ff1_Pnd_Twrc_Orig_Sc", "#TWRC-ORIG-SC", FieldType.STRING, 
            3);
        pnd_F96_Ff1_Pnd_Twrc_Updt_Sc = pnd_F96_Ff1_Pnd_Twrc_Source_UpdRedef1.newFieldInGroup("pnd_F96_Ff1_Pnd_Twrc_Updt_Sc", "#TWRC-UPDT-SC", FieldType.STRING, 
            3);
        pnd_F96_Ff1_Twrc_Classic_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Classic_Amt", "TWRC-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_F96_Ff1_Twrc_Roth_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Roth_Amt", "TWRC-ROTH-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_F96_Ff1_Twrc_Rollover_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Rollover_Amt", "TWRC-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_F96_Ff1_Twrc_Rechar_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Rechar_Amt", "TWRC-RECHAR-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_F96_Ff1_Twrc_Sep_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Sep_Amt", "TWRC-SEP-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Fair_Mkt_Val_Amt", "TWRC-FAIR-MKT-VAL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_F96_Ff1_Twrc_Roth_Conversion_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Roth_Conversion_Amt", "TWRC-ROTH-CONVERSION-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_F96_Ff1_Twrc_Postpn_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Postpn_Amt", "TWRC-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_F96_Ff1_Twrc_Repayments_Amt = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Repayments_Amt", "TWRC-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_F96_Ff1_Twrc_Create_Date = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Create_Date", "TWRC-CREATE-DATE", FieldType.STRING, 8);
        pnd_F96_Ff1_Twrc_Update_Date = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Update_Date", "TWRC-UPDATE-DATE", FieldType.STRING, 8);
        pnd_F96_Ff1_Twrc_Update_Time = pnd_F96_Ff1.newFieldInGroup("pnd_F96_Ff1_Twrc_Update_Time", "TWRC-UPDATE-TIME", FieldType.STRING, 7);

        this.setRecordName("LdaTwrl190a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl190a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
