/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:31 PM
**        * FROM NATURAL LDA     : TWRL3327
************************************************************
**        * FILE NAME            : LdaTwrl3327.java
**        * CLASS NAME           : LdaTwrl3327
**        * INSTANCE NAME        : LdaTwrl3327
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3327 extends DbsRecord
{
    // Properties
    private DbsGroup irs_F_Input_Tape;
    private DbsField irs_F_Input_Tape_Irs_F_Record_Type;
    private DbsField irs_F_Input_Tape_Irs_F_Number_Of_A_Records;
    private DbsField irs_F_Input_Tape_Irs_F_Filler_1;
    private DbsField irs_F_Input_Tape_Irs_F_Filler_2;
    private DbsField irs_F_Input_Tape_Irs_F_Number_Of_B_Records;
    private DbsField irs_F_Input_Tape_Irs_F_Filler_3;
    private DbsField irs_F_Input_Tape_Irs_F_Filler_3a;
    private DbsField irs_F_Input_Tape_Irs_F_Record_Sequence_No;
    private DbsField irs_F_Input_Tape_Irs_F_Filler_4;
    private DbsField irs_F_Input_Tape_Irs_F_Blank_Cr_Lf;
    private DbsGroup irs_F_Input_TapeRedef1;
    private DbsField irs_F_Input_Tape_Irs_F_Move_1;
    private DbsField irs_F_Input_Tape_Irs_F_Move_2;
    private DbsField irs_F_Input_Tape_Rec_Seq_No;
    private DbsField irs_F_Input_Tape_Irs_F_Move_3;

    public DbsGroup getIrs_F_Input_Tape() { return irs_F_Input_Tape; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Record_Type() { return irs_F_Input_Tape_Irs_F_Record_Type; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Number_Of_A_Records() { return irs_F_Input_Tape_Irs_F_Number_Of_A_Records; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Filler_1() { return irs_F_Input_Tape_Irs_F_Filler_1; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Filler_2() { return irs_F_Input_Tape_Irs_F_Filler_2; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Number_Of_B_Records() { return irs_F_Input_Tape_Irs_F_Number_Of_B_Records; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Filler_3() { return irs_F_Input_Tape_Irs_F_Filler_3; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Filler_3a() { return irs_F_Input_Tape_Irs_F_Filler_3a; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Record_Sequence_No() { return irs_F_Input_Tape_Irs_F_Record_Sequence_No; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Filler_4() { return irs_F_Input_Tape_Irs_F_Filler_4; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Blank_Cr_Lf() { return irs_F_Input_Tape_Irs_F_Blank_Cr_Lf; }

    public DbsGroup getIrs_F_Input_TapeRedef1() { return irs_F_Input_TapeRedef1; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Move_1() { return irs_F_Input_Tape_Irs_F_Move_1; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Move_2() { return irs_F_Input_Tape_Irs_F_Move_2; }

    public DbsField getIrs_F_Input_Tape_Rec_Seq_No() { return irs_F_Input_Tape_Rec_Seq_No; }

    public DbsField getIrs_F_Input_Tape_Irs_F_Move_3() { return irs_F_Input_Tape_Irs_F_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_F_Input_Tape = newGroupInRecord("irs_F_Input_Tape", "IRS-F-INPUT-TAPE");
        irs_F_Input_Tape_Irs_F_Record_Type = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Record_Type", "IRS-F-RECORD-TYPE", FieldType.STRING, 
            1);
        irs_F_Input_Tape_Irs_F_Number_Of_A_Records = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Number_Of_A_Records", "IRS-F-NUMBER-OF-A-RECORDS", 
            FieldType.NUMERIC, 8);
        irs_F_Input_Tape_Irs_F_Filler_1 = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Filler_1", "IRS-F-FILLER-1", FieldType.NUMERIC, 21);
        irs_F_Input_Tape_Irs_F_Filler_2 = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Filler_2", "IRS-F-FILLER-2", FieldType.STRING, 19);
        irs_F_Input_Tape_Irs_F_Number_Of_B_Records = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Number_Of_B_Records", "IRS-F-NUMBER-OF-B-RECORDS", 
            FieldType.NUMERIC, 8);
        irs_F_Input_Tape_Irs_F_Filler_3 = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Filler_3", "IRS-F-FILLER-3", FieldType.STRING, 250);
        irs_F_Input_Tape_Irs_F_Filler_3a = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Filler_3a", "IRS-F-FILLER-3A", FieldType.STRING, 192);
        irs_F_Input_Tape_Irs_F_Record_Sequence_No = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Record_Sequence_No", "IRS-F-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_F_Input_Tape_Irs_F_Filler_4 = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Filler_4", "IRS-F-FILLER-4", FieldType.STRING, 241);
        irs_F_Input_Tape_Irs_F_Blank_Cr_Lf = irs_F_Input_Tape.newFieldInGroup("irs_F_Input_Tape_Irs_F_Blank_Cr_Lf", "IRS-F-BLANK-CR-LF", FieldType.STRING, 
            2);
        irs_F_Input_TapeRedef1 = newGroupInRecord("irs_F_Input_TapeRedef1", "Redefines", irs_F_Input_Tape);
        irs_F_Input_Tape_Irs_F_Move_1 = irs_F_Input_TapeRedef1.newFieldInGroup("irs_F_Input_Tape_Irs_F_Move_1", "IRS-F-MOVE-1", FieldType.STRING, 250);
        irs_F_Input_Tape_Irs_F_Move_2 = irs_F_Input_TapeRedef1.newFieldInGroup("irs_F_Input_Tape_Irs_F_Move_2", "IRS-F-MOVE-2", FieldType.STRING, 249);
        irs_F_Input_Tape_Rec_Seq_No = irs_F_Input_TapeRedef1.newFieldInGroup("irs_F_Input_Tape_Rec_Seq_No", "REC-SEQ-NO", FieldType.NUMERIC, 8);
        irs_F_Input_Tape_Irs_F_Move_3 = irs_F_Input_TapeRedef1.newFieldInGroup("irs_F_Input_Tape_Irs_F_Move_3", "IRS-F-MOVE-3", FieldType.STRING, 243);

        this.setRecordName("LdaTwrl3327");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_F_Input_Tape_Irs_F_Record_Type.setInitialValue("F");
    }

    // Constructor
    public LdaTwrl3327() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
