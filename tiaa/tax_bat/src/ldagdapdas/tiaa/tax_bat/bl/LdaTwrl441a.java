/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:36 PM
**        * FROM NATURAL LDA     : TWRL441A
************************************************************
**        * FILE NAME            : LdaTwrl441a.java
**        * CLASS NAME           : LdaTwrl441a
**        * INSTANCE NAME        : LdaTwrl441a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl441a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_form;
    private DbsField form_Tirf_Tax_Year;
    private DbsField form_Tirf_Form_Type;
    private DbsField form_Tirf_Company_Cde;
    private DbsField form_Tirf_Tax_Id_Type;
    private DbsField form_Tirf_Tin;
    private DbsField form_Tirf_Contract_Nbr;
    private DbsField form_Tirf_Payee_Cde;
    private DbsField form_Tirf_Create_User;
    private DbsField form_Tirf_Lu_User;
    private DbsField form_Tirf_Part_Rpt_Ind;
    private DbsField form_Tirf_Form_Issue_Type;
    private DbsField form_Tirf_Moore_Test_Ind;
    private DbsField form_Tirf_Moore_Hold_Ind;
    private DbsField form_Tirf_Print_Hold_Ind;
    private DbsField form_Tirf_Irs_Reject_Ind;
    private DbsField form_Tirf_5498_Rechar_Ind;
    private DbsField form_Tirf_Ira_Acct_Type;
    private DbsField form_Tirf_Foreign_Addr;
    private DbsField form_Tirf_Deceased_Ind;
    private DbsField form_Tirf_Empty_Form;
    private DbsField form_Tirf_Lu_Ts;
    private DbsField form_Tirf_Create_Ts;
    private DbsField form_Tirf_Invrse_Create_Ts;
    private DbsField form_Tirf_Active_Ind;
    private DbsField form_Tirf_Irs_Rpt_Ind;
    private DbsField form_Tirf_Part_Rpt_Date;
    private DbsField form_Tirf_Irs_Rpt_Date;
    private DbsField form_Tirf_Srce_Date;
    private DbsField form_Tirf_Srce_Cde;
    private DbsField form_Tirf_Form_Seq;
    private DbsField form_Tirf_Participant_Name;
    private DbsGroup form_Tirf_Part_Name;
    private DbsField form_Tirf_Part_Last_Nme;
    private DbsField form_Tirf_Part_First_Nme;
    private DbsField form_Tirf_Part_Mddle_Nme;
    private DbsField form_Tirf_Addr_Ln1;
    private DbsField form_Tirf_Addr_Ln2;
    private DbsField form_Tirf_Addr_Ln3;
    private DbsField form_Tirf_Addr_Ln4;
    private DbsField form_Tirf_Addr_Ln5;
    private DbsField form_Tirf_Addr_Ln6;
    private DbsField form_Tirf_Street_Addr;
    private DbsField form_Tirf_City;
    private DbsField form_Tirf_Geo_Cde;
    private DbsField form_Tirf_Apo_Geo_Cde;
    private DbsField form_Tirf_Country_Name;
    private DbsField form_Tirf_Province_Code;
    private DbsField form_Tirf_Zip;
    private DbsField form_Tirf_Walk_Rte;
    private DbsField form_Tirf_Contract_Xref;
    private DbsField form_Tirf_Trad_Ira_Contrib;
    private DbsField form_Tirf_Roth_Ira_Contrib;
    private DbsField form_Tirf_Trad_Ira_Rollover;
    private DbsField form_Tirf_Roth_Ira_Conversion;
    private DbsField form_Tirf_Ira_Rechar_Amt;
    private DbsField form_Tirf_Account_Fmv;
    private DbsField form_Tirf_Postpn_Amt;
    private DbsField form_Tirf_Postpn_Yr;
    private DbsField form_Tirf_Postpn_Code;
    private DbsField form_Tirf_Repayments_Amt;
    private DbsField form_Tirf_Repayments_Code;
    private DbsField form_Tirf_Md_Ind;
    private DbsField form_Count_Casttirf_Sys_Err;
    private DbsGroup form_Tirf_Sys_ErrMuGroup;
    private DbsField form_Tirf_Sys_Err;
    private DbsField form_Tirf_Sep_Amt;

    public DataAccessProgramView getVw_form() { return vw_form; }

    public DbsField getForm_Tirf_Tax_Year() { return form_Tirf_Tax_Year; }

    public DbsField getForm_Tirf_Form_Type() { return form_Tirf_Form_Type; }

    public DbsField getForm_Tirf_Company_Cde() { return form_Tirf_Company_Cde; }

    public DbsField getForm_Tirf_Tax_Id_Type() { return form_Tirf_Tax_Id_Type; }

    public DbsField getForm_Tirf_Tin() { return form_Tirf_Tin; }

    public DbsField getForm_Tirf_Contract_Nbr() { return form_Tirf_Contract_Nbr; }

    public DbsField getForm_Tirf_Payee_Cde() { return form_Tirf_Payee_Cde; }

    public DbsField getForm_Tirf_Create_User() { return form_Tirf_Create_User; }

    public DbsField getForm_Tirf_Lu_User() { return form_Tirf_Lu_User; }

    public DbsField getForm_Tirf_Part_Rpt_Ind() { return form_Tirf_Part_Rpt_Ind; }

    public DbsField getForm_Tirf_Form_Issue_Type() { return form_Tirf_Form_Issue_Type; }

    public DbsField getForm_Tirf_Moore_Test_Ind() { return form_Tirf_Moore_Test_Ind; }

    public DbsField getForm_Tirf_Moore_Hold_Ind() { return form_Tirf_Moore_Hold_Ind; }

    public DbsField getForm_Tirf_Print_Hold_Ind() { return form_Tirf_Print_Hold_Ind; }

    public DbsField getForm_Tirf_Irs_Reject_Ind() { return form_Tirf_Irs_Reject_Ind; }

    public DbsField getForm_Tirf_5498_Rechar_Ind() { return form_Tirf_5498_Rechar_Ind; }

    public DbsField getForm_Tirf_Ira_Acct_Type() { return form_Tirf_Ira_Acct_Type; }

    public DbsField getForm_Tirf_Foreign_Addr() { return form_Tirf_Foreign_Addr; }

    public DbsField getForm_Tirf_Deceased_Ind() { return form_Tirf_Deceased_Ind; }

    public DbsField getForm_Tirf_Empty_Form() { return form_Tirf_Empty_Form; }

    public DbsField getForm_Tirf_Lu_Ts() { return form_Tirf_Lu_Ts; }

    public DbsField getForm_Tirf_Create_Ts() { return form_Tirf_Create_Ts; }

    public DbsField getForm_Tirf_Invrse_Create_Ts() { return form_Tirf_Invrse_Create_Ts; }

    public DbsField getForm_Tirf_Active_Ind() { return form_Tirf_Active_Ind; }

    public DbsField getForm_Tirf_Irs_Rpt_Ind() { return form_Tirf_Irs_Rpt_Ind; }

    public DbsField getForm_Tirf_Part_Rpt_Date() { return form_Tirf_Part_Rpt_Date; }

    public DbsField getForm_Tirf_Irs_Rpt_Date() { return form_Tirf_Irs_Rpt_Date; }

    public DbsField getForm_Tirf_Srce_Date() { return form_Tirf_Srce_Date; }

    public DbsField getForm_Tirf_Srce_Cde() { return form_Tirf_Srce_Cde; }

    public DbsField getForm_Tirf_Form_Seq() { return form_Tirf_Form_Seq; }

    public DbsField getForm_Tirf_Participant_Name() { return form_Tirf_Participant_Name; }

    public DbsGroup getForm_Tirf_Part_Name() { return form_Tirf_Part_Name; }

    public DbsField getForm_Tirf_Part_Last_Nme() { return form_Tirf_Part_Last_Nme; }

    public DbsField getForm_Tirf_Part_First_Nme() { return form_Tirf_Part_First_Nme; }

    public DbsField getForm_Tirf_Part_Mddle_Nme() { return form_Tirf_Part_Mddle_Nme; }

    public DbsField getForm_Tirf_Addr_Ln1() { return form_Tirf_Addr_Ln1; }

    public DbsField getForm_Tirf_Addr_Ln2() { return form_Tirf_Addr_Ln2; }

    public DbsField getForm_Tirf_Addr_Ln3() { return form_Tirf_Addr_Ln3; }

    public DbsField getForm_Tirf_Addr_Ln4() { return form_Tirf_Addr_Ln4; }

    public DbsField getForm_Tirf_Addr_Ln5() { return form_Tirf_Addr_Ln5; }

    public DbsField getForm_Tirf_Addr_Ln6() { return form_Tirf_Addr_Ln6; }

    public DbsField getForm_Tirf_Street_Addr() { return form_Tirf_Street_Addr; }

    public DbsField getForm_Tirf_City() { return form_Tirf_City; }

    public DbsField getForm_Tirf_Geo_Cde() { return form_Tirf_Geo_Cde; }

    public DbsField getForm_Tirf_Apo_Geo_Cde() { return form_Tirf_Apo_Geo_Cde; }

    public DbsField getForm_Tirf_Country_Name() { return form_Tirf_Country_Name; }

    public DbsField getForm_Tirf_Province_Code() { return form_Tirf_Province_Code; }

    public DbsField getForm_Tirf_Zip() { return form_Tirf_Zip; }

    public DbsField getForm_Tirf_Walk_Rte() { return form_Tirf_Walk_Rte; }

    public DbsField getForm_Tirf_Contract_Xref() { return form_Tirf_Contract_Xref; }

    public DbsField getForm_Tirf_Trad_Ira_Contrib() { return form_Tirf_Trad_Ira_Contrib; }

    public DbsField getForm_Tirf_Roth_Ira_Contrib() { return form_Tirf_Roth_Ira_Contrib; }

    public DbsField getForm_Tirf_Trad_Ira_Rollover() { return form_Tirf_Trad_Ira_Rollover; }

    public DbsField getForm_Tirf_Roth_Ira_Conversion() { return form_Tirf_Roth_Ira_Conversion; }

    public DbsField getForm_Tirf_Ira_Rechar_Amt() { return form_Tirf_Ira_Rechar_Amt; }

    public DbsField getForm_Tirf_Account_Fmv() { return form_Tirf_Account_Fmv; }

    public DbsField getForm_Tirf_Postpn_Amt() { return form_Tirf_Postpn_Amt; }

    public DbsField getForm_Tirf_Postpn_Yr() { return form_Tirf_Postpn_Yr; }

    public DbsField getForm_Tirf_Postpn_Code() { return form_Tirf_Postpn_Code; }

    public DbsField getForm_Tirf_Repayments_Amt() { return form_Tirf_Repayments_Amt; }

    public DbsField getForm_Tirf_Repayments_Code() { return form_Tirf_Repayments_Code; }

    public DbsField getForm_Tirf_Md_Ind() { return form_Tirf_Md_Ind; }

    public DbsField getForm_Count_Casttirf_Sys_Err() { return form_Count_Casttirf_Sys_Err; }

    public DbsGroup getForm_Tirf_Sys_ErrMuGroup() { return form_Tirf_Sys_ErrMuGroup; }

    public DbsField getForm_Tirf_Sys_Err() { return form_Tirf_Sys_Err; }

    public DbsField getForm_Tirf_Sep_Amt() { return form_Tirf_Sep_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_form = new DataAccessProgramView(new NameInfo("vw_form", "FORM"), "TWRFRM_FORM_FILE", "TWRFRM_FORM_FILE");
        form_Tirf_Tax_Year = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Year", "TIRF-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_TAX_YEAR");
        form_Tirf_Tax_Year.setDdmHeader("TAX/YEAR");
        form_Tirf_Form_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Type", "TIRF-FORM-TYPE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "TIRF_FORM_TYPE");
        form_Tirf_Form_Type.setDdmHeader("FORM");
        form_Tirf_Company_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Company_Cde", "TIRF-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_COMPANY_CDE");
        form_Tirf_Company_Cde.setDdmHeader("COM-/PANY/CODE");
        form_Tirf_Tax_Id_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Tax_Id_Type", "TIRF-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_TAX_ID_TYPE");
        form_Tirf_Tin = vw_form.getRecord().newFieldInGroup("form_Tirf_Tin", "TIRF-TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIRF_TIN");
        form_Tirf_Tin.setDdmHeader("TAX/ID/NUMBER");
        form_Tirf_Contract_Nbr = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Nbr", "TIRF-CONTRACT-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_NBR");
        form_Tirf_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        form_Tirf_Payee_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Payee_Cde", "TIRF-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_PAYEE_CDE");
        form_Tirf_Payee_Cde.setDdmHeader("PAYEE/CODE");
        form_Tirf_Create_User = vw_form.getRecord().newFieldInGroup("form_Tirf_Create_User", "TIRF-CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CREATE_USER");
        form_Tirf_Create_User.setDdmHeader("CREATE/USER/ID");
        form_Tirf_Lu_User = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_User", "TIRF-LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_LU_USER");
        form_Tirf_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        form_Tirf_Part_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Part_Rpt_Ind", "TIRF-PART-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_IND");
        form_Tirf_Part_Rpt_Ind.setDdmHeader("PART/RPT/IND");
        form_Tirf_Form_Issue_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Issue_Type", "TIRF-FORM-ISSUE-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_FORM_ISSUE_TYPE");
        form_Tirf_Form_Issue_Type.setDdmHeader("FORM/ISSUE/TYPE");
        form_Tirf_Moore_Test_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Moore_Test_Ind", "TIRF-MOORE-TEST-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_TEST_IND");
        form_Tirf_Moore_Test_Ind.setDdmHeader("MOORE/TEST/IND");
        form_Tirf_Moore_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Moore_Hold_Ind", "TIRF-MOORE-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_MOORE_HOLD_IND");
        form_Tirf_Moore_Hold_Ind.setDdmHeader("MOORE/HOLD/IND");
        form_Tirf_Print_Hold_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Print_Hold_Ind", "TIRF-PRINT-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_PRINT_HOLD_IND");
        form_Tirf_Print_Hold_Ind.setDdmHeader("PRINT/HOLD/IND");
        form_Tirf_Irs_Reject_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Reject_Ind", "TIRF-IRS-REJECT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_REJECT_IND");
        form_Tirf_Irs_Reject_Ind.setDdmHeader("IRS/REJ/IND");
        form_Tirf_5498_Rechar_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_5498_Rechar_Ind", "TIRF-5498-RECHAR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_5498_RECHAR_IND");
        form_Tirf_5498_Rechar_Ind.setDdmHeader("RECHARAC-/TERATION/IND");
        form_Tirf_Ira_Acct_Type = vw_form.getRecord().newFieldInGroup("form_Tirf_Ira_Acct_Type", "TIRF-IRA-ACCT-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_IRA_ACCT_TYPE");
        form_Tirf_Ira_Acct_Type.setDdmHeader("IRA/ACCOUNT/TYPE");
        form_Tirf_Foreign_Addr = vw_form.getRecord().newFieldInGroup("form_Tirf_Foreign_Addr", "TIRF-FOREIGN-ADDR", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_FOREIGN_ADDR");
        form_Tirf_Foreign_Addr.setDdmHeader("FORE-/IGN/ADDR");
        form_Tirf_Deceased_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Deceased_Ind", "TIRF-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_DECEASED_IND");
        form_Tirf_Deceased_Ind.setDdmHeader("DECE-/ASED/IND");
        form_Tirf_Empty_Form = vw_form.getRecord().newFieldInGroup("form_Tirf_Empty_Form", "TIRF-EMPTY-FORM", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "TIRF_EMPTY_FORM");
        form_Tirf_Empty_Form.setDdmHeader("EMPTY/FORM");
        form_Tirf_Lu_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Lu_Ts", "TIRF-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TIRF_LU_TS");
        form_Tirf_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");
        form_Tirf_Create_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Create_Ts", "TIRF-CREATE-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TIRF_CREATE_TS");
        form_Tirf_Create_Ts.setDdmHeader("CREATE/TIME/STAMP");
        form_Tirf_Invrse_Create_Ts = vw_form.getRecord().newFieldInGroup("form_Tirf_Invrse_Create_Ts", "TIRF-INVRSE-CREATE-TS", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "TIRF_INVRSE_CREATE_TS");
        form_Tirf_Invrse_Create_Ts.setDdmHeader("INVERSE/CREATE/TIMESTAMP");
        form_Tirf_Active_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Active_Ind", "TIRF-ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_ACTIVE_IND");
        form_Tirf_Active_Ind.setDdmHeader("ACT/IND");
        form_Tirf_Irs_Rpt_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Ind", "TIRF-IRS-RPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_IND");
        form_Tirf_Irs_Rpt_Ind.setDdmHeader("IRS/RPT/IND");
        form_Tirf_Part_Rpt_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Part_Rpt_Date", "TIRF-PART-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_PART_RPT_DATE");
        form_Tirf_Part_Rpt_Date.setDdmHeader("PART/RPT/DATE");
        form_Tirf_Irs_Rpt_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Irs_Rpt_Date", "TIRF-IRS-RPT-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_IRS_RPT_DATE");
        form_Tirf_Irs_Rpt_Date.setDdmHeader("IRS/RPT/DATE");
        form_Tirf_Srce_Date = vw_form.getRecord().newFieldInGroup("form_Tirf_Srce_Date", "TIRF-SRCE-DATE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "TIRF_SRCE_DATE");
        form_Tirf_Srce_Date.setDdmHeader("SOURCE/DATE");
        form_Tirf_Srce_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Srce_Cde", "TIRF-SRCE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_SRCE_CDE");
        form_Tirf_Srce_Cde.setDdmHeader("SOURCE/CODE");
        form_Tirf_Form_Seq = vw_form.getRecord().newFieldInGroup("form_Tirf_Form_Seq", "TIRF-FORM-SEQ", FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, 
            "TIRF_FORM_SEQ");
        form_Tirf_Form_Seq.setDdmHeader("FORM/SEQ/NUM");
        form_Tirf_Participant_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Participant_Name", "TIRF-PARTICIPANT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "TIRF_PARTICIPANT_NAME");
        form_Tirf_Participant_Name.setDdmHeader("PARTICIPANT/NAME");
        form_Tirf_Part_Name = vw_form.getRecord().newGroupInGroup("form_Tirf_Part_Name", "TIRF-PART-NAME");
        form_Tirf_Part_Last_Nme = form_Tirf_Part_Name.newFieldInGroup("form_Tirf_Part_Last_Nme", "TIRF-PART-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TIRF_PART_LAST_NME");
        form_Tirf_Part_Last_Nme.setDdmHeader("LAST NAME");
        form_Tirf_Part_First_Nme = form_Tirf_Part_Name.newFieldInGroup("form_Tirf_Part_First_Nme", "TIRF-PART-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TIRF_PART_FIRST_NME");
        form_Tirf_Part_First_Nme.setDdmHeader("FIRST NAME");
        form_Tirf_Part_Mddle_Nme = form_Tirf_Part_Name.newFieldInGroup("form_Tirf_Part_Mddle_Nme", "TIRF-PART-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "TIRF_PART_MDDLE_NME");
        form_Tirf_Part_Mddle_Nme.setDdmHeader("MIDDLE NAME");
        form_Tirf_Addr_Ln1 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln1", "TIRF-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN1");
        form_Tirf_Addr_Ln1.setDdmHeader("ADDRESS/LINE 1");
        form_Tirf_Addr_Ln2 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln2", "TIRF-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN2");
        form_Tirf_Addr_Ln2.setDdmHeader("ADDRESS/LINE 2");
        form_Tirf_Addr_Ln3 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln3", "TIRF-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN3");
        form_Tirf_Addr_Ln3.setDdmHeader("ADDRESS/LINE 3");
        form_Tirf_Addr_Ln4 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln4", "TIRF-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN4");
        form_Tirf_Addr_Ln4.setDdmHeader("ADDRESS/LINE 4");
        form_Tirf_Addr_Ln5 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln5", "TIRF-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN5");
        form_Tirf_Addr_Ln5.setDdmHeader("ADDRESS/LINE 5");
        form_Tirf_Addr_Ln6 = vw_form.getRecord().newFieldInGroup("form_Tirf_Addr_Ln6", "TIRF-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TIRF_ADDR_LN6");
        form_Tirf_Addr_Ln6.setDdmHeader("ADDRESS/LINE 6");
        form_Tirf_Street_Addr = vw_form.getRecord().newFieldInGroup("form_Tirf_Street_Addr", "TIRF-STREET-ADDR", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "TIRF_STREET_ADDR");
        form_Tirf_Street_Addr.setDdmHeader("STREET");
        form_Tirf_City = vw_form.getRecord().newFieldInGroup("form_Tirf_City", "TIRF-CITY", FieldType.STRING, 25, RepeatingFieldStrategy.None, "TIRF_CITY");
        form_Tirf_City.setDdmHeader("CITY");
        form_Tirf_Geo_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Geo_Cde", "TIRF-GEO-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_GEO_CDE");
        form_Tirf_Geo_Cde.setDdmHeader("GEO/CODE");
        form_Tirf_Apo_Geo_Cde = vw_form.getRecord().newFieldInGroup("form_Tirf_Apo_Geo_Cde", "TIRF-APO-GEO-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_APO_GEO_CDE");
        form_Tirf_Apo_Geo_Cde.setDdmHeader("APO/GEO/CODE");
        form_Tirf_Country_Name = vw_form.getRecord().newFieldInGroup("form_Tirf_Country_Name", "TIRF-COUNTRY-NAME", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TIRF_COUNTRY_NAME");
        form_Tirf_Country_Name.setDdmHeader("COUNTRY NAME");
        form_Tirf_Province_Code = vw_form.getRecord().newFieldInGroup("form_Tirf_Province_Code", "TIRF-PROVINCE-CODE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TIRF_PROVINCE_CODE");
        form_Tirf_Province_Code.setDdmHeader("PROVINCE/CODE");
        form_Tirf_Zip = vw_form.getRecord().newFieldInGroup("form_Tirf_Zip", "TIRF-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "TIRF_ZIP");
        form_Tirf_Zip.setDdmHeader("ZIP");
        form_Tirf_Walk_Rte = vw_form.getRecord().newFieldInGroup("form_Tirf_Walk_Rte", "TIRF-WALK-RTE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_WALK_RTE");
        form_Tirf_Walk_Rte.setDdmHeader("WALK/ROUTE");
        form_Tirf_Contract_Xref = vw_form.getRecord().newFieldInGroup("form_Tirf_Contract_Xref", "TIRF-CONTRACT-XREF", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TIRF_CONTRACT_XREF");
        form_Tirf_Contract_Xref.setDdmHeader("CONT-/RACT/XREF");
        form_Tirf_Trad_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Trad_Ira_Contrib", "TIRF-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_CONTRIB");
        form_Tirf_Trad_Ira_Contrib.setDdmHeader("TRADITIONAL/IRA/CONTRIBUTION");
        form_Tirf_Roth_Ira_Contrib = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Ira_Contrib", "TIRF-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONTRIB");
        form_Tirf_Roth_Ira_Contrib.setDdmHeader("ROTH/IRA/CONTRIBUTION");
        form_Tirf_Trad_Ira_Rollover = vw_form.getRecord().newFieldInGroup("form_Tirf_Trad_Ira_Rollover", "TIRF-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_TRAD_IRA_ROLLOVER");
        form_Tirf_Trad_Ira_Rollover.setDdmHeader("TRADITIONAL/IRA/ROLLOVERS");
        form_Tirf_Roth_Ira_Conversion = vw_form.getRecord().newFieldInGroup("form_Tirf_Roth_Ira_Conversion", "TIRF-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TIRF_ROTH_IRA_CONVERSION");
        form_Tirf_Roth_Ira_Conversion.setDdmHeader("ROTH/IRA/CONVERSION");
        form_Tirf_Ira_Rechar_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Ira_Rechar_Amt", "TIRF-IRA-RECHAR-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TIRF_IRA_RECHAR_AMT");
        form_Tirf_Ira_Rechar_Amt.setDdmHeader("IRA/RECHAR./AMOUNT");
        form_Tirf_Account_Fmv = vw_form.getRecord().newFieldInGroup("form_Tirf_Account_Fmv", "TIRF-ACCOUNT-FMV", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_ACCOUNT_FMV");
        form_Tirf_Account_Fmv.setDdmHeader("FAIR/MARKET/VALUE");
        form_Tirf_Postpn_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Postpn_Amt", "TIRF-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_AMT");
        form_Tirf_Postpn_Yr = vw_form.getRecord().newFieldInGroup("form_Tirf_Postpn_Yr", "TIRF-POSTPN-YR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_YR");
        form_Tirf_Postpn_Code = vw_form.getRecord().newFieldInGroup("form_Tirf_Postpn_Code", "TIRF-POSTPN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_POSTPN_CODE");
        form_Tirf_Repayments_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Repayments_Amt", "TIRF-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, RepeatingFieldStrategy.None, "TIRF_REPAYMENTS_AMT");
        form_Tirf_Repayments_Code = vw_form.getRecord().newFieldInGroup("form_Tirf_Repayments_Code", "TIRF-REPAYMENTS-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TIRF_REPAYMENTS_CODE");
        form_Tirf_Md_Ind = vw_form.getRecord().newFieldInGroup("form_Tirf_Md_Ind", "TIRF-MD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TIRF_MD_IND");
        form_Tirf_Md_Ind.setDdmHeader("MD/IND");
        form_Count_Casttirf_Sys_Err = vw_form.getRecord().newFieldInGroup("form_Count_Casttirf_Sys_Err", "C*TIRF-SYS-ERR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, 
            "TWRFRM_FORM_FILE_TIRF_SYS_ERR");
        form_Tirf_Sys_ErrMuGroup = vw_form.getRecord().newGroupInGroup("form_Tirf_Sys_ErrMuGroup", "TIRF_SYS_ERRMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "TWRFRM_FORM_FILE_TIRF_SYS_ERR");
        form_Tirf_Sys_Err = form_Tirf_Sys_ErrMuGroup.newFieldArrayInGroup("form_Tirf_Sys_Err", "TIRF-SYS-ERR", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1,14), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIRF_SYS_ERR");
        form_Tirf_Sys_Err.setDdmHeader("SYSTEM/ERROR/CODE");
        form_Tirf_Sep_Amt = vw_form.getRecord().newFieldInGroup("form_Tirf_Sep_Amt", "TIRF-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TIRF_SEP_AMT");
        form_Tirf_Sep_Amt.setDdmHeader("SEP IRA/AMT");

        this.setRecordName("LdaTwrl441a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_form.reset();
    }

    // Constructor
    public LdaTwrl441a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
