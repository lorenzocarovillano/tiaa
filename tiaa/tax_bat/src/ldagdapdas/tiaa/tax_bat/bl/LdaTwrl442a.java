/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:39 PM
**        * FROM NATURAL LDA     : TWRL442A
************************************************************
**        * FILE NAME            : LdaTwrl442a.java
**        * CLASS NAME           : LdaTwrl442a
**        * INSTANCE NAME        : LdaTwrl442a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl442a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Form;
    private DbsField pnd_Form_Pnd_F_Ira_Type;
    private DbsField pnd_Form_Pnd_F_Tax_Year;
    private DbsField pnd_Form_Pnd_F_Form_Type;
    private DbsField pnd_Form_Pnd_F_Company_Cde;
    private DbsField pnd_Form_Pnd_F_Tax_Id_Type;
    private DbsField pnd_Form_Pnd_F_Tin;
    private DbsField pnd_Form_Pnd_F_Contract_Nbr;
    private DbsField pnd_Form_Pnd_F_Payee_Cde;
    private DbsField pnd_Form_Pnd_F_Create_User;
    private DbsField pnd_Form_Pnd_F_Lu_User;
    private DbsField pnd_Form_Pnd_F_Part_Rpt_Ind;
    private DbsField pnd_Form_Pnd_F_Form_Issue_Type;
    private DbsField pnd_Form_Pnd_F_Moore_Test_Ind;
    private DbsField pnd_Form_Pnd_F_Moore_Hold_Ind;
    private DbsField pnd_Form_Pnd_F_Print_Hold_Ind;
    private DbsField pnd_Form_Pnd_F_Irs_Reject_Ind;
    private DbsField pnd_Form_Pnd_F_5498_Rechar_Ind;
    private DbsField pnd_Form_Pnd_F_Ira_Acct_Type;
    private DbsField pnd_Form_Pnd_F_Foreign_Addr;
    private DbsField pnd_Form_Pnd_F_Deceased_Ind;
    private DbsField pnd_Form_Pnd_F_Empty_Form;
    private DbsField pnd_Form_Pnd_F_Lu_Ts;
    private DbsField pnd_Form_Pnd_F_Create_Ts;
    private DbsField pnd_Form_Pnd_F_Invrse_Create_Ts;
    private DbsField pnd_Form_Pnd_F_Active_Ind;
    private DbsField pnd_Form_Pnd_F_Irs_Rpt_Ind;
    private DbsField pnd_Form_Pnd_F_Part_Rpt_Date;
    private DbsField pnd_Form_Pnd_F_Irs_Rpt_Date;
    private DbsField pnd_Form_Pnd_F_Srce_Date;
    private DbsField pnd_Form_Pnd_F_Srce_Cde;
    private DbsField pnd_Form_Pnd_F_Form_Seq;
    private DbsField pnd_Form_Pnd_F_Participant_Name;
    private DbsField pnd_Form_Pnd_F_Part_Last_Nme;
    private DbsGroup pnd_Form_Pnd_F_Part_Last_NmeRedef1;
    private DbsField pnd_Form_Pnd_F_4_Char_Last_Name;
    private DbsField pnd_Form_Pnd_F_Part_First_Nme;
    private DbsField pnd_Form_Pnd_F_Part_Mddle_Nme;
    private DbsField pnd_Form_Pnd_F_Addr_Ln1;
    private DbsField pnd_Form_Pnd_F_Addr_Ln2;
    private DbsField pnd_Form_Pnd_F_Addr_Ln3;
    private DbsField pnd_Form_Pnd_F_Addr_Ln4;
    private DbsField pnd_Form_Pnd_F_Addr_Ln5;
    private DbsField pnd_Form_Pnd_F_Addr_Ln6;
    private DbsField pnd_Form_Pnd_F_Street_Addr;
    private DbsField pnd_Form_Pnd_F_City;
    private DbsField pnd_Form_Pnd_F_Geo_Cde;
    private DbsField pnd_Form_Pnd_F_Apo_Geo_Code;
    private DbsField pnd_Form_Pnd_F_Country_Name;
    private DbsField pnd_Form_Pnd_F_Province_Code;
    private DbsField pnd_Form_Pnd_F_Zip;
    private DbsField pnd_Form_Pnd_F_Walk_Rte;
    private DbsField pnd_Form_Pnd_F_Contract_Xref;
    private DbsField pnd_Form_Pnd_F_Trad_Ira_Contrib;
    private DbsField pnd_Form_Pnd_F_Roth_Ira_Contrib;
    private DbsField pnd_Form_Pnd_F_Trad_Ira_Rollover;
    private DbsField pnd_Form_Pnd_F_Roth_Ira_Conversion;
    private DbsField pnd_Form_Pnd_F_Ira_Rechar;
    private DbsField pnd_Form_Pnd_F_Account_Fmv;
    private DbsField pnd_Form_Pnd_F_Tirf_Postpn_Amt;
    private DbsField pnd_Form_Pnd_F_Tirf_Postpn_Yr;
    private DbsField pnd_Form_Pnd_F_Tirf_Postpn_Code;
    private DbsField pnd_Form_Pnd_F_Tirf_Repayments_Amt;
    private DbsField pnd_Form_Pnd_F_Tirf_Repayments_Code;
    private DbsField pnd_Form_Pnd_F_Md_Ind;
    private DbsField pnd_Form_Pnd_F_Sys_Err_Count;
    private DbsField pnd_Form_Pnd_F_Sys_Err;
    private DbsField pnd_Form_Pnd_F_Sep_Contrib;
    private DbsField pnd_Form_Pnd_F_Isn;

    public DbsGroup getPnd_Form() { return pnd_Form; }

    public DbsField getPnd_Form_Pnd_F_Ira_Type() { return pnd_Form_Pnd_F_Ira_Type; }

    public DbsField getPnd_Form_Pnd_F_Tax_Year() { return pnd_Form_Pnd_F_Tax_Year; }

    public DbsField getPnd_Form_Pnd_F_Form_Type() { return pnd_Form_Pnd_F_Form_Type; }

    public DbsField getPnd_Form_Pnd_F_Company_Cde() { return pnd_Form_Pnd_F_Company_Cde; }

    public DbsField getPnd_Form_Pnd_F_Tax_Id_Type() { return pnd_Form_Pnd_F_Tax_Id_Type; }

    public DbsField getPnd_Form_Pnd_F_Tin() { return pnd_Form_Pnd_F_Tin; }

    public DbsField getPnd_Form_Pnd_F_Contract_Nbr() { return pnd_Form_Pnd_F_Contract_Nbr; }

    public DbsField getPnd_Form_Pnd_F_Payee_Cde() { return pnd_Form_Pnd_F_Payee_Cde; }

    public DbsField getPnd_Form_Pnd_F_Create_User() { return pnd_Form_Pnd_F_Create_User; }

    public DbsField getPnd_Form_Pnd_F_Lu_User() { return pnd_Form_Pnd_F_Lu_User; }

    public DbsField getPnd_Form_Pnd_F_Part_Rpt_Ind() { return pnd_Form_Pnd_F_Part_Rpt_Ind; }

    public DbsField getPnd_Form_Pnd_F_Form_Issue_Type() { return pnd_Form_Pnd_F_Form_Issue_Type; }

    public DbsField getPnd_Form_Pnd_F_Moore_Test_Ind() { return pnd_Form_Pnd_F_Moore_Test_Ind; }

    public DbsField getPnd_Form_Pnd_F_Moore_Hold_Ind() { return pnd_Form_Pnd_F_Moore_Hold_Ind; }

    public DbsField getPnd_Form_Pnd_F_Print_Hold_Ind() { return pnd_Form_Pnd_F_Print_Hold_Ind; }

    public DbsField getPnd_Form_Pnd_F_Irs_Reject_Ind() { return pnd_Form_Pnd_F_Irs_Reject_Ind; }

    public DbsField getPnd_Form_Pnd_F_5498_Rechar_Ind() { return pnd_Form_Pnd_F_5498_Rechar_Ind; }

    public DbsField getPnd_Form_Pnd_F_Ira_Acct_Type() { return pnd_Form_Pnd_F_Ira_Acct_Type; }

    public DbsField getPnd_Form_Pnd_F_Foreign_Addr() { return pnd_Form_Pnd_F_Foreign_Addr; }

    public DbsField getPnd_Form_Pnd_F_Deceased_Ind() { return pnd_Form_Pnd_F_Deceased_Ind; }

    public DbsField getPnd_Form_Pnd_F_Empty_Form() { return pnd_Form_Pnd_F_Empty_Form; }

    public DbsField getPnd_Form_Pnd_F_Lu_Ts() { return pnd_Form_Pnd_F_Lu_Ts; }

    public DbsField getPnd_Form_Pnd_F_Create_Ts() { return pnd_Form_Pnd_F_Create_Ts; }

    public DbsField getPnd_Form_Pnd_F_Invrse_Create_Ts() { return pnd_Form_Pnd_F_Invrse_Create_Ts; }

    public DbsField getPnd_Form_Pnd_F_Active_Ind() { return pnd_Form_Pnd_F_Active_Ind; }

    public DbsField getPnd_Form_Pnd_F_Irs_Rpt_Ind() { return pnd_Form_Pnd_F_Irs_Rpt_Ind; }

    public DbsField getPnd_Form_Pnd_F_Part_Rpt_Date() { return pnd_Form_Pnd_F_Part_Rpt_Date; }

    public DbsField getPnd_Form_Pnd_F_Irs_Rpt_Date() { return pnd_Form_Pnd_F_Irs_Rpt_Date; }

    public DbsField getPnd_Form_Pnd_F_Srce_Date() { return pnd_Form_Pnd_F_Srce_Date; }

    public DbsField getPnd_Form_Pnd_F_Srce_Cde() { return pnd_Form_Pnd_F_Srce_Cde; }

    public DbsField getPnd_Form_Pnd_F_Form_Seq() { return pnd_Form_Pnd_F_Form_Seq; }

    public DbsField getPnd_Form_Pnd_F_Participant_Name() { return pnd_Form_Pnd_F_Participant_Name; }

    public DbsField getPnd_Form_Pnd_F_Part_Last_Nme() { return pnd_Form_Pnd_F_Part_Last_Nme; }

    public DbsGroup getPnd_Form_Pnd_F_Part_Last_NmeRedef1() { return pnd_Form_Pnd_F_Part_Last_NmeRedef1; }

    public DbsField getPnd_Form_Pnd_F_4_Char_Last_Name() { return pnd_Form_Pnd_F_4_Char_Last_Name; }

    public DbsField getPnd_Form_Pnd_F_Part_First_Nme() { return pnd_Form_Pnd_F_Part_First_Nme; }

    public DbsField getPnd_Form_Pnd_F_Part_Mddle_Nme() { return pnd_Form_Pnd_F_Part_Mddle_Nme; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln1() { return pnd_Form_Pnd_F_Addr_Ln1; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln2() { return pnd_Form_Pnd_F_Addr_Ln2; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln3() { return pnd_Form_Pnd_F_Addr_Ln3; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln4() { return pnd_Form_Pnd_F_Addr_Ln4; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln5() { return pnd_Form_Pnd_F_Addr_Ln5; }

    public DbsField getPnd_Form_Pnd_F_Addr_Ln6() { return pnd_Form_Pnd_F_Addr_Ln6; }

    public DbsField getPnd_Form_Pnd_F_Street_Addr() { return pnd_Form_Pnd_F_Street_Addr; }

    public DbsField getPnd_Form_Pnd_F_City() { return pnd_Form_Pnd_F_City; }

    public DbsField getPnd_Form_Pnd_F_Geo_Cde() { return pnd_Form_Pnd_F_Geo_Cde; }

    public DbsField getPnd_Form_Pnd_F_Apo_Geo_Code() { return pnd_Form_Pnd_F_Apo_Geo_Code; }

    public DbsField getPnd_Form_Pnd_F_Country_Name() { return pnd_Form_Pnd_F_Country_Name; }

    public DbsField getPnd_Form_Pnd_F_Province_Code() { return pnd_Form_Pnd_F_Province_Code; }

    public DbsField getPnd_Form_Pnd_F_Zip() { return pnd_Form_Pnd_F_Zip; }

    public DbsField getPnd_Form_Pnd_F_Walk_Rte() { return pnd_Form_Pnd_F_Walk_Rte; }

    public DbsField getPnd_Form_Pnd_F_Contract_Xref() { return pnd_Form_Pnd_F_Contract_Xref; }

    public DbsField getPnd_Form_Pnd_F_Trad_Ira_Contrib() { return pnd_Form_Pnd_F_Trad_Ira_Contrib; }

    public DbsField getPnd_Form_Pnd_F_Roth_Ira_Contrib() { return pnd_Form_Pnd_F_Roth_Ira_Contrib; }

    public DbsField getPnd_Form_Pnd_F_Trad_Ira_Rollover() { return pnd_Form_Pnd_F_Trad_Ira_Rollover; }

    public DbsField getPnd_Form_Pnd_F_Roth_Ira_Conversion() { return pnd_Form_Pnd_F_Roth_Ira_Conversion; }

    public DbsField getPnd_Form_Pnd_F_Ira_Rechar() { return pnd_Form_Pnd_F_Ira_Rechar; }

    public DbsField getPnd_Form_Pnd_F_Account_Fmv() { return pnd_Form_Pnd_F_Account_Fmv; }

    public DbsField getPnd_Form_Pnd_F_Tirf_Postpn_Amt() { return pnd_Form_Pnd_F_Tirf_Postpn_Amt; }

    public DbsField getPnd_Form_Pnd_F_Tirf_Postpn_Yr() { return pnd_Form_Pnd_F_Tirf_Postpn_Yr; }

    public DbsField getPnd_Form_Pnd_F_Tirf_Postpn_Code() { return pnd_Form_Pnd_F_Tirf_Postpn_Code; }

    public DbsField getPnd_Form_Pnd_F_Tirf_Repayments_Amt() { return pnd_Form_Pnd_F_Tirf_Repayments_Amt; }

    public DbsField getPnd_Form_Pnd_F_Tirf_Repayments_Code() { return pnd_Form_Pnd_F_Tirf_Repayments_Code; }

    public DbsField getPnd_Form_Pnd_F_Md_Ind() { return pnd_Form_Pnd_F_Md_Ind; }

    public DbsField getPnd_Form_Pnd_F_Sys_Err_Count() { return pnd_Form_Pnd_F_Sys_Err_Count; }

    public DbsField getPnd_Form_Pnd_F_Sys_Err() { return pnd_Form_Pnd_F_Sys_Err; }

    public DbsField getPnd_Form_Pnd_F_Sep_Contrib() { return pnd_Form_Pnd_F_Sep_Contrib; }

    public DbsField getPnd_Form_Pnd_F_Isn() { return pnd_Form_Pnd_F_Isn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Form = newGroupInRecord("pnd_Form", "#FORM");
        pnd_Form_Pnd_F_Ira_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Type", "#F-IRA-TYPE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Tax_Year = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tax_Year", "#F-TAX-YEAR", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Form_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Form_Type", "#F-FORM-TYPE", FieldType.NUMERIC, 2);
        pnd_Form_Pnd_F_Company_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Company_Cde", "#F-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Tax_Id_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tax_Id_Type", "#F-TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Tin = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tin", "#F-TIN", FieldType.STRING, 10);
        pnd_Form_Pnd_F_Contract_Nbr = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Contract_Nbr", "#F-CONTRACT-NBR", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Payee_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Payee_Cde", "#F-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Create_User = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Create_User", "#F-CREATE-USER", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Lu_User = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Lu_User", "#F-LU-USER", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Part_Rpt_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Part_Rpt_Ind", "#F-PART-RPT-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Form_Issue_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Form_Issue_Type", "#F-FORM-ISSUE-TYPE", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Moore_Test_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Moore_Test_Ind", "#F-MOORE-TEST-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Moore_Hold_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Moore_Hold_Ind", "#F-MOORE-HOLD-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Print_Hold_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Print_Hold_Ind", "#F-PRINT-HOLD-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Irs_Reject_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Irs_Reject_Ind", "#F-IRS-REJECT-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_5498_Rechar_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_5498_Rechar_Ind", "#F-5498-RECHAR-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Ira_Acct_Type = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Acct_Type", "#F-IRA-ACCT-TYPE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Foreign_Addr = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Foreign_Addr", "#F-FOREIGN-ADDR", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Deceased_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Deceased_Ind", "#F-DECEASED-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Empty_Form = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Empty_Form", "#F-EMPTY-FORM", FieldType.BOOLEAN);
        pnd_Form_Pnd_F_Lu_Ts = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Lu_Ts", "#F-LU-TS", FieldType.TIME);
        pnd_Form_Pnd_F_Create_Ts = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Create_Ts", "#F-CREATE-TS", FieldType.TIME);
        pnd_Form_Pnd_F_Invrse_Create_Ts = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Invrse_Create_Ts", "#F-INVRSE-CREATE-TS", FieldType.PACKED_DECIMAL, 
            13);
        pnd_Form_Pnd_F_Active_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Active_Ind", "#F-ACTIVE-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Irs_Rpt_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Irs_Rpt_Ind", "#F-IRS-RPT-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Part_Rpt_Date = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Part_Rpt_Date", "#F-PART-RPT-DATE", FieldType.DATE);
        pnd_Form_Pnd_F_Irs_Rpt_Date = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Irs_Rpt_Date", "#F-IRS-RPT-DATE", FieldType.DATE);
        pnd_Form_Pnd_F_Srce_Date = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Srce_Date", "#F-SRCE-DATE", FieldType.DATE);
        pnd_Form_Pnd_F_Srce_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Srce_Cde", "#F-SRCE-CDE", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Form_Seq = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Form_Seq", "#F-FORM-SEQ", FieldType.PACKED_DECIMAL, 3);
        pnd_Form_Pnd_F_Participant_Name = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Participant_Name", "#F-PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Part_Last_Nme = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Part_Last_Nme", "#F-PART-LAST-NME", FieldType.STRING, 30);
        pnd_Form_Pnd_F_Part_Last_NmeRedef1 = pnd_Form.newGroupInGroup("pnd_Form_Pnd_F_Part_Last_NmeRedef1", "Redefines", pnd_Form_Pnd_F_Part_Last_Nme);
        pnd_Form_Pnd_F_4_Char_Last_Name = pnd_Form_Pnd_F_Part_Last_NmeRedef1.newFieldInGroup("pnd_Form_Pnd_F_4_Char_Last_Name", "#F-4-CHAR-LAST-NAME", 
            FieldType.STRING, 4);
        pnd_Form_Pnd_F_Part_First_Nme = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Part_First_Nme", "#F-PART-FIRST-NME", FieldType.STRING, 30);
        pnd_Form_Pnd_F_Part_Mddle_Nme = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Part_Mddle_Nme", "#F-PART-MDDLE-NME", FieldType.STRING, 30);
        pnd_Form_Pnd_F_Addr_Ln1 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln1", "#F-ADDR-LN1", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Addr_Ln2 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln2", "#F-ADDR-LN2", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Addr_Ln3 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln3", "#F-ADDR-LN3", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Addr_Ln4 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln4", "#F-ADDR-LN4", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Addr_Ln5 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln5", "#F-ADDR-LN5", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Addr_Ln6 = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Addr_Ln6", "#F-ADDR-LN6", FieldType.STRING, 35);
        pnd_Form_Pnd_F_Street_Addr = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Street_Addr", "#F-STREET-ADDR", FieldType.STRING, 40);
        pnd_Form_Pnd_F_City = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_City", "#F-CITY", FieldType.STRING, 25);
        pnd_Form_Pnd_F_Geo_Cde = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Geo_Cde", "#F-GEO-CDE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Apo_Geo_Code = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Apo_Geo_Code", "#F-APO-GEO-CODE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Country_Name = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Country_Name", "#F-COUNTRY-NAME", FieldType.STRING, 20);
        pnd_Form_Pnd_F_Province_Code = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Province_Code", "#F-PROVINCE-CODE", FieldType.STRING, 3);
        pnd_Form_Pnd_F_Zip = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Zip", "#F-ZIP", FieldType.STRING, 9);
        pnd_Form_Pnd_F_Walk_Rte = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Walk_Rte", "#F-WALK-RTE", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Contract_Xref = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Contract_Xref", "#F-CONTRACT-XREF", FieldType.STRING, 8);
        pnd_Form_Pnd_F_Trad_Ira_Contrib = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Trad_Ira_Contrib", "#F-TRAD-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Form_Pnd_F_Roth_Ira_Contrib = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Roth_Ira_Contrib", "#F-ROTH-IRA-CONTRIB", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Form_Pnd_F_Trad_Ira_Rollover = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Trad_Ira_Rollover", "#F-TRAD-IRA-ROLLOVER", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Form_Pnd_F_Roth_Ira_Conversion = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Roth_Ira_Conversion", "#F-ROTH-IRA-CONVERSION", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Form_Pnd_F_Ira_Rechar = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Ira_Rechar", "#F-IRA-RECHAR", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Form_Pnd_F_Account_Fmv = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Account_Fmv", "#F-ACCOUNT-FMV", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Form_Pnd_F_Tirf_Postpn_Amt = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tirf_Postpn_Amt", "#F-TIRF-POSTPN-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_Form_Pnd_F_Tirf_Postpn_Yr = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tirf_Postpn_Yr", "#F-TIRF-POSTPN-YR", FieldType.STRING, 4);
        pnd_Form_Pnd_F_Tirf_Postpn_Code = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tirf_Postpn_Code", "#F-TIRF-POSTPN-CODE", FieldType.STRING, 2);
        pnd_Form_Pnd_F_Tirf_Repayments_Amt = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tirf_Repayments_Amt", "#F-TIRF-REPAYMENTS-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Form_Pnd_F_Tirf_Repayments_Code = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Tirf_Repayments_Code", "#F-TIRF-REPAYMENTS-CODE", FieldType.STRING, 
            2);
        pnd_Form_Pnd_F_Md_Ind = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Md_Ind", "#F-MD-IND", FieldType.STRING, 1);
        pnd_Form_Pnd_F_Sys_Err_Count = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Sys_Err_Count", "#F-SYS-ERR-COUNT", FieldType.NUMERIC, 3);
        pnd_Form_Pnd_F_Sys_Err = pnd_Form.newFieldArrayInGroup("pnd_Form_Pnd_F_Sys_Err", "#F-SYS-ERR", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1,
            14));
        pnd_Form_Pnd_F_Sep_Contrib = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Sep_Contrib", "#F-SEP-CONTRIB", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Form_Pnd_F_Isn = pnd_Form.newFieldInGroup("pnd_Form_Pnd_F_Isn", "#F-ISN", FieldType.PACKED_DECIMAL, 11);

        this.setRecordName("LdaTwrl442a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl442a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
