/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:24 PM
**        * FROM NATURAL LDA     : TWRL820A
************************************************************
**        * FILE NAME            : LdaTwrl820a.java
**        * CLASS NAME           : LdaTwrl820a
**        * INSTANCE NAME        : LdaTwrl820a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl820a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_newpay;
    private DbsField newpay_Twrpymnt_Tax_Year;
    private DbsField newpay_Twrpymnt_Tax_Id_Type;
    private DbsField newpay_Twrpymnt_Tax_Id_Nbr;
    private DbsField newpay_Twrpymnt_Company_Cde;
    private DbsField newpay_Twrpymnt_Citizen_Cde;
    private DbsField newpay_Twrpymnt_Tax_Citizenship;
    private DbsField newpay_Twrpymnt_Contract_Nbr;
    private DbsField newpay_Twrpymnt_Payee_Cde;
    private DbsField newpay_Twrpymnt_Distribution_Cde;
    private DbsField newpay_Twrpymnt_Residency_Type;
    private DbsField newpay_Twrpymnt_Residency_Code;
    private DbsField newpay_Twrpymnt_Ivc_Indicator;
    private DbsField newpay_Twrpymnt_Currency_Cde;
    private DbsField newpay_Twrpymnt_Paymt_Category;
    private DbsField newpay_Twrpymnt_Contract_Lob_Cde;
    private DbsField newpay_Twrpymnt_Updt_Dte;
    private DbsField newpay_Twrpymnt_Updt_Time;
    private DbsField newpay_Twrpymnt_Lu_Ts;
    private DbsField newpay_Twrpymnt_Locality_Cde;
    private DbsField newpay_Twrpymnt_Contract_Ppcn_Nbr;
    private DbsField newpay_Twrpymnt_Tot_Contractual_Ivc;
    private DbsField newpay_Twrpymnt_Contract_Issue_Dte;
    private DbsField newpay_Twrpymnt_Wpid;
    private DbsGroup newpay_Twrpymnt_Summary_Information;
    private DbsField newpay_Twrpymnt_Sum_Gross_Amt;
    private DbsField newpay_Twrpymnt_Sum_Ivc_Amt;
    private DbsField newpay_Twrpymnt_Sum_Int_Amt;
    private DbsField newpay_Twrpymnt_Sum_Fed_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Nra_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Can_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Int_Bkup_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Soc_Sec_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Medicare_Wthld;
    private DbsField newpay_Twrpymnt_Sum_State_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Local_Wthld;
    private DbsField newpay_Twrpymnt_Sum_Refund_Due;
    private DbsField newpay_Twrpymnt_Sum_Under_Wthld;
    private DbsField newpay_Twrpymnt_Tot_Dist;
    private DbsField newpay_Twrpymnt_Pct_Of_Tot;
    private DbsField newpay_Twrpymnt_Dist_Updte_User;
    private DbsField newpay_Twrpymnt_Dist_Updte_Dte;
    private DbsField newpay_Twrpymnt_Dist_Updte_Time;
    private DbsField newpay_Twrpymnt_Sum_Dist_Comments;
    private DbsField newpay_Count_Casttwrpymnt_Payments;
    private DbsGroup newpay_Twrpymnt_Payments;
    private DbsField newpay_Twrpymnt_Pymnt_Dte;
    private DbsField newpay_Twrpymnt_Pymnt_Status;
    private DbsField newpay_Twrpymnt_Account_Dte;
    private DbsField newpay_Twrpymnt_Effective_Dte;
    private DbsField newpay_Twrpymnt_Pymnt_Intfce_Dte;
    private DbsField newpay_Twrpymnt_Updte_Srce_Cde;
    private DbsField newpay_Twrpymnt_Pymnt_Refer_Nbr;
    private DbsGroup newpay_Twrpymnt_Pymnt_Refer_NbrRedef1;
    private DbsField newpay_Twrpymnt_Pymnt_Refer_Nbr_Num;
    private DbsField newpay_Twrpymnt_Dist_Method;
    private DbsField newpay_Twrpymnt_Orgn_Srce_Cde;
    private DbsField newpay_Twrpymnt_Pymnt_Wpid;
    private DbsField newpay_Twrpymnt_Additions_3;
    private DbsGroup newpay_Twrpymnt_Additions_3Redef2;
    private DbsGroup newpay_Twrpymnt_Additions_3_Detail;
    private DbsField newpay_Twrpymnt_Irc_From;
    private DbsField filler01;
    private DbsField newpay_Twrpymnt_Additions_5;
    private DbsGroup newpay_Twrpymnt_Additions_5Redef3;
    private DbsGroup newpay_Twrpymnt_Additions_5_Detail;
    private DbsField newpay_Twrpymnt_Orig_Subplan;
    private DbsField newpay_Twrpymnt_Orig_Contract_Nbr;
    private DbsField filler02;
    private DbsField newpay_Twrpymnt_Payment_Type;
    private DbsField newpay_Twrpymnt_Settle_Type;
    private DbsField newpay_Twrpymnt_Ivc_Ind;
    private DbsField newpay_Twrpymnt_Ivc_Protect;
    private DbsField newpay_Twrpymnt_Frm1001_Ind;
    private DbsField newpay_Twrpymnt_Frm1078_Ind;
    private DbsField newpay_Twrpymnt_Gross_Amt;
    private DbsField newpay_Twrpymnt_Ivc_Amt;
    private DbsField newpay_Twrpymnt_Int_Amt;
    private DbsField newpay_Twrpymnt_Int_Bkup_Wthld;
    private DbsField newpay_Twrpymnt_Fed_Wthld_Amt;
    private DbsField newpay_Twrpymnt_Nra_Wthld_Amt;
    private DbsField newpay_Twrpymnt_Can_Wthld_Amt;
    private DbsField newpay_Twrpymnt_State_Wthld;
    private DbsField newpay_Twrpymnt_Local_Wthld;
    private DbsField newpay_Twrpymnt_Soc_Sec_Wthld;
    private DbsField newpay_Twrpymnt_Medicare_Wthld;
    private DbsField newpay_Twrpymnt_Ivc_Calc_Method;
    private DbsField newpay_Twrpymnt_Additions_1;
    private DbsGroup newpay_Twrpymnt_Additions_1Redef4;
    private DbsGroup newpay_Twrpymnt_Additions_1_Detail;
    private DbsField newpay_Twrpymnt_Contract_Type;
    private DbsField newpay_Twrpymnt_Roth_Qual_Ind;
    private DbsField filler03;
    private DbsField newpay_Twrpymnt_Elc_Cde;
    private DbsField newpay_Twrpymnt_Roth_Year;
    private DbsField newpay_Twrpymnt_Additions_2;
    private DbsGroup newpay_Twrpymnt_Additions_2Redef5;
    private DbsGroup newpay_Twrpymnt_Additions_2_Detail;
    private DbsField newpay_Twrpymnt_Nra_Refund_Amt;
    private DbsField newpay_Twrpymnt_Irc_To;
    private DbsField newpay_Twrpymnt_Omni_Plan;
    private DbsField filler04;
    private DbsField newpay_Twrpymnt_Additions_Tops;
    private DbsGroup newpay_Twrpymnt_Additions_TopsRedef6;
    private DbsGroup newpay_Twrpymnt_Additions_Tops_Detail;
    private DbsField newpay_Twrpymnt_Dist_Type;
    private DbsField newpay_Twrpymnt_Check_Reason;
    private DbsField newpay_Twrpymnt_Plan_Type;
    private DbsField newpay_Twrpymnt_Sub_Plan_Type_Not_Used;
    private DbsField newpay_Twrpymnt_Tax_Type;
    private DbsField newpay_Twrpymnt_Trade_Date;
    private DbsField filler05;
    private DbsField newpay_Twrpymnt_Additions_4;
    private DbsGroup newpay_Twrpymnt_Additions_4Redef7;
    private DbsGroup newpay_Twrpymnt_Additions_4_Detail;
    private DbsField newpay_Twrpymnt_Irr_Amt;
    private DbsField newpay_Twrpymnt_Subplan;
    private DbsField filler06;
    private DbsField newpay_Twrpymnt_Rptd_To_Part_Can_Stamp;
    private DbsField newpay_Twrpymnt_Pymnt_Comments;
    private DbsField newpay_Twrpymnt_Updte_User;
    private DbsField newpay_Twrpymnt_Sys_Dte_Time;
    private DbsField newpay_Twrpymnt_Gtn_Dte_Time;
    private DbsField newpay_Twrpymnt_Gtn_Ts;
    private DbsField newpay_Twrpymnt_Contract_Seq_No;

    public DataAccessProgramView getVw_newpay() { return vw_newpay; }

    public DbsField getNewpay_Twrpymnt_Tax_Year() { return newpay_Twrpymnt_Tax_Year; }

    public DbsField getNewpay_Twrpymnt_Tax_Id_Type() { return newpay_Twrpymnt_Tax_Id_Type; }

    public DbsField getNewpay_Twrpymnt_Tax_Id_Nbr() { return newpay_Twrpymnt_Tax_Id_Nbr; }

    public DbsField getNewpay_Twrpymnt_Company_Cde() { return newpay_Twrpymnt_Company_Cde; }

    public DbsField getNewpay_Twrpymnt_Citizen_Cde() { return newpay_Twrpymnt_Citizen_Cde; }

    public DbsField getNewpay_Twrpymnt_Tax_Citizenship() { return newpay_Twrpymnt_Tax_Citizenship; }

    public DbsField getNewpay_Twrpymnt_Contract_Nbr() { return newpay_Twrpymnt_Contract_Nbr; }

    public DbsField getNewpay_Twrpymnt_Payee_Cde() { return newpay_Twrpymnt_Payee_Cde; }

    public DbsField getNewpay_Twrpymnt_Distribution_Cde() { return newpay_Twrpymnt_Distribution_Cde; }

    public DbsField getNewpay_Twrpymnt_Residency_Type() { return newpay_Twrpymnt_Residency_Type; }

    public DbsField getNewpay_Twrpymnt_Residency_Code() { return newpay_Twrpymnt_Residency_Code; }

    public DbsField getNewpay_Twrpymnt_Ivc_Indicator() { return newpay_Twrpymnt_Ivc_Indicator; }

    public DbsField getNewpay_Twrpymnt_Currency_Cde() { return newpay_Twrpymnt_Currency_Cde; }

    public DbsField getNewpay_Twrpymnt_Paymt_Category() { return newpay_Twrpymnt_Paymt_Category; }

    public DbsField getNewpay_Twrpymnt_Contract_Lob_Cde() { return newpay_Twrpymnt_Contract_Lob_Cde; }

    public DbsField getNewpay_Twrpymnt_Updt_Dte() { return newpay_Twrpymnt_Updt_Dte; }

    public DbsField getNewpay_Twrpymnt_Updt_Time() { return newpay_Twrpymnt_Updt_Time; }

    public DbsField getNewpay_Twrpymnt_Lu_Ts() { return newpay_Twrpymnt_Lu_Ts; }

    public DbsField getNewpay_Twrpymnt_Locality_Cde() { return newpay_Twrpymnt_Locality_Cde; }

    public DbsField getNewpay_Twrpymnt_Contract_Ppcn_Nbr() { return newpay_Twrpymnt_Contract_Ppcn_Nbr; }

    public DbsField getNewpay_Twrpymnt_Tot_Contractual_Ivc() { return newpay_Twrpymnt_Tot_Contractual_Ivc; }

    public DbsField getNewpay_Twrpymnt_Contract_Issue_Dte() { return newpay_Twrpymnt_Contract_Issue_Dte; }

    public DbsField getNewpay_Twrpymnt_Wpid() { return newpay_Twrpymnt_Wpid; }

    public DbsGroup getNewpay_Twrpymnt_Summary_Information() { return newpay_Twrpymnt_Summary_Information; }

    public DbsField getNewpay_Twrpymnt_Sum_Gross_Amt() { return newpay_Twrpymnt_Sum_Gross_Amt; }

    public DbsField getNewpay_Twrpymnt_Sum_Ivc_Amt() { return newpay_Twrpymnt_Sum_Ivc_Amt; }

    public DbsField getNewpay_Twrpymnt_Sum_Int_Amt() { return newpay_Twrpymnt_Sum_Int_Amt; }

    public DbsField getNewpay_Twrpymnt_Sum_Fed_Wthld() { return newpay_Twrpymnt_Sum_Fed_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Nra_Wthld() { return newpay_Twrpymnt_Sum_Nra_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Can_Wthld() { return newpay_Twrpymnt_Sum_Can_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Int_Bkup_Wthld() { return newpay_Twrpymnt_Sum_Int_Bkup_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Soc_Sec_Wthld() { return newpay_Twrpymnt_Sum_Soc_Sec_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Medicare_Wthld() { return newpay_Twrpymnt_Sum_Medicare_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_State_Wthld() { return newpay_Twrpymnt_Sum_State_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Local_Wthld() { return newpay_Twrpymnt_Sum_Local_Wthld; }

    public DbsField getNewpay_Twrpymnt_Sum_Refund_Due() { return newpay_Twrpymnt_Sum_Refund_Due; }

    public DbsField getNewpay_Twrpymnt_Sum_Under_Wthld() { return newpay_Twrpymnt_Sum_Under_Wthld; }

    public DbsField getNewpay_Twrpymnt_Tot_Dist() { return newpay_Twrpymnt_Tot_Dist; }

    public DbsField getNewpay_Twrpymnt_Pct_Of_Tot() { return newpay_Twrpymnt_Pct_Of_Tot; }

    public DbsField getNewpay_Twrpymnt_Dist_Updte_User() { return newpay_Twrpymnt_Dist_Updte_User; }

    public DbsField getNewpay_Twrpymnt_Dist_Updte_Dte() { return newpay_Twrpymnt_Dist_Updte_Dte; }

    public DbsField getNewpay_Twrpymnt_Dist_Updte_Time() { return newpay_Twrpymnt_Dist_Updte_Time; }

    public DbsField getNewpay_Twrpymnt_Sum_Dist_Comments() { return newpay_Twrpymnt_Sum_Dist_Comments; }

    public DbsField getNewpay_Count_Casttwrpymnt_Payments() { return newpay_Count_Casttwrpymnt_Payments; }

    public DbsGroup getNewpay_Twrpymnt_Payments() { return newpay_Twrpymnt_Payments; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Dte() { return newpay_Twrpymnt_Pymnt_Dte; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Status() { return newpay_Twrpymnt_Pymnt_Status; }

    public DbsField getNewpay_Twrpymnt_Account_Dte() { return newpay_Twrpymnt_Account_Dte; }

    public DbsField getNewpay_Twrpymnt_Effective_Dte() { return newpay_Twrpymnt_Effective_Dte; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Intfce_Dte() { return newpay_Twrpymnt_Pymnt_Intfce_Dte; }

    public DbsField getNewpay_Twrpymnt_Updte_Srce_Cde() { return newpay_Twrpymnt_Updte_Srce_Cde; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Refer_Nbr() { return newpay_Twrpymnt_Pymnt_Refer_Nbr; }

    public DbsGroup getNewpay_Twrpymnt_Pymnt_Refer_NbrRedef1() { return newpay_Twrpymnt_Pymnt_Refer_NbrRedef1; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Refer_Nbr_Num() { return newpay_Twrpymnt_Pymnt_Refer_Nbr_Num; }

    public DbsField getNewpay_Twrpymnt_Dist_Method() { return newpay_Twrpymnt_Dist_Method; }

    public DbsField getNewpay_Twrpymnt_Orgn_Srce_Cde() { return newpay_Twrpymnt_Orgn_Srce_Cde; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Wpid() { return newpay_Twrpymnt_Pymnt_Wpid; }

    public DbsField getNewpay_Twrpymnt_Additions_3() { return newpay_Twrpymnt_Additions_3; }

    public DbsGroup getNewpay_Twrpymnt_Additions_3Redef2() { return newpay_Twrpymnt_Additions_3Redef2; }

    public DbsGroup getNewpay_Twrpymnt_Additions_3_Detail() { return newpay_Twrpymnt_Additions_3_Detail; }

    public DbsField getNewpay_Twrpymnt_Irc_From() { return newpay_Twrpymnt_Irc_From; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getNewpay_Twrpymnt_Additions_5() { return newpay_Twrpymnt_Additions_5; }

    public DbsGroup getNewpay_Twrpymnt_Additions_5Redef3() { return newpay_Twrpymnt_Additions_5Redef3; }

    public DbsGroup getNewpay_Twrpymnt_Additions_5_Detail() { return newpay_Twrpymnt_Additions_5_Detail; }

    public DbsField getNewpay_Twrpymnt_Orig_Subplan() { return newpay_Twrpymnt_Orig_Subplan; }

    public DbsField getNewpay_Twrpymnt_Orig_Contract_Nbr() { return newpay_Twrpymnt_Orig_Contract_Nbr; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getNewpay_Twrpymnt_Payment_Type() { return newpay_Twrpymnt_Payment_Type; }

    public DbsField getNewpay_Twrpymnt_Settle_Type() { return newpay_Twrpymnt_Settle_Type; }

    public DbsField getNewpay_Twrpymnt_Ivc_Ind() { return newpay_Twrpymnt_Ivc_Ind; }

    public DbsField getNewpay_Twrpymnt_Ivc_Protect() { return newpay_Twrpymnt_Ivc_Protect; }

    public DbsField getNewpay_Twrpymnt_Frm1001_Ind() { return newpay_Twrpymnt_Frm1001_Ind; }

    public DbsField getNewpay_Twrpymnt_Frm1078_Ind() { return newpay_Twrpymnt_Frm1078_Ind; }

    public DbsField getNewpay_Twrpymnt_Gross_Amt() { return newpay_Twrpymnt_Gross_Amt; }

    public DbsField getNewpay_Twrpymnt_Ivc_Amt() { return newpay_Twrpymnt_Ivc_Amt; }

    public DbsField getNewpay_Twrpymnt_Int_Amt() { return newpay_Twrpymnt_Int_Amt; }

    public DbsField getNewpay_Twrpymnt_Int_Bkup_Wthld() { return newpay_Twrpymnt_Int_Bkup_Wthld; }

    public DbsField getNewpay_Twrpymnt_Fed_Wthld_Amt() { return newpay_Twrpymnt_Fed_Wthld_Amt; }

    public DbsField getNewpay_Twrpymnt_Nra_Wthld_Amt() { return newpay_Twrpymnt_Nra_Wthld_Amt; }

    public DbsField getNewpay_Twrpymnt_Can_Wthld_Amt() { return newpay_Twrpymnt_Can_Wthld_Amt; }

    public DbsField getNewpay_Twrpymnt_State_Wthld() { return newpay_Twrpymnt_State_Wthld; }

    public DbsField getNewpay_Twrpymnt_Local_Wthld() { return newpay_Twrpymnt_Local_Wthld; }

    public DbsField getNewpay_Twrpymnt_Soc_Sec_Wthld() { return newpay_Twrpymnt_Soc_Sec_Wthld; }

    public DbsField getNewpay_Twrpymnt_Medicare_Wthld() { return newpay_Twrpymnt_Medicare_Wthld; }

    public DbsField getNewpay_Twrpymnt_Ivc_Calc_Method() { return newpay_Twrpymnt_Ivc_Calc_Method; }

    public DbsField getNewpay_Twrpymnt_Additions_1() { return newpay_Twrpymnt_Additions_1; }

    public DbsGroup getNewpay_Twrpymnt_Additions_1Redef4() { return newpay_Twrpymnt_Additions_1Redef4; }

    public DbsGroup getNewpay_Twrpymnt_Additions_1_Detail() { return newpay_Twrpymnt_Additions_1_Detail; }

    public DbsField getNewpay_Twrpymnt_Contract_Type() { return newpay_Twrpymnt_Contract_Type; }

    public DbsField getNewpay_Twrpymnt_Roth_Qual_Ind() { return newpay_Twrpymnt_Roth_Qual_Ind; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getNewpay_Twrpymnt_Elc_Cde() { return newpay_Twrpymnt_Elc_Cde; }

    public DbsField getNewpay_Twrpymnt_Roth_Year() { return newpay_Twrpymnt_Roth_Year; }

    public DbsField getNewpay_Twrpymnt_Additions_2() { return newpay_Twrpymnt_Additions_2; }

    public DbsGroup getNewpay_Twrpymnt_Additions_2Redef5() { return newpay_Twrpymnt_Additions_2Redef5; }

    public DbsGroup getNewpay_Twrpymnt_Additions_2_Detail() { return newpay_Twrpymnt_Additions_2_Detail; }

    public DbsField getNewpay_Twrpymnt_Nra_Refund_Amt() { return newpay_Twrpymnt_Nra_Refund_Amt; }

    public DbsField getNewpay_Twrpymnt_Irc_To() { return newpay_Twrpymnt_Irc_To; }

    public DbsField getNewpay_Twrpymnt_Omni_Plan() { return newpay_Twrpymnt_Omni_Plan; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getNewpay_Twrpymnt_Additions_Tops() { return newpay_Twrpymnt_Additions_Tops; }

    public DbsGroup getNewpay_Twrpymnt_Additions_TopsRedef6() { return newpay_Twrpymnt_Additions_TopsRedef6; }

    public DbsGroup getNewpay_Twrpymnt_Additions_Tops_Detail() { return newpay_Twrpymnt_Additions_Tops_Detail; }

    public DbsField getNewpay_Twrpymnt_Dist_Type() { return newpay_Twrpymnt_Dist_Type; }

    public DbsField getNewpay_Twrpymnt_Check_Reason() { return newpay_Twrpymnt_Check_Reason; }

    public DbsField getNewpay_Twrpymnt_Plan_Type() { return newpay_Twrpymnt_Plan_Type; }

    public DbsField getNewpay_Twrpymnt_Sub_Plan_Type_Not_Used() { return newpay_Twrpymnt_Sub_Plan_Type_Not_Used; }

    public DbsField getNewpay_Twrpymnt_Tax_Type() { return newpay_Twrpymnt_Tax_Type; }

    public DbsField getNewpay_Twrpymnt_Trade_Date() { return newpay_Twrpymnt_Trade_Date; }

    public DbsField getFiller05() { return filler05; }

    public DbsField getNewpay_Twrpymnt_Additions_4() { return newpay_Twrpymnt_Additions_4; }

    public DbsGroup getNewpay_Twrpymnt_Additions_4Redef7() { return newpay_Twrpymnt_Additions_4Redef7; }

    public DbsGroup getNewpay_Twrpymnt_Additions_4_Detail() { return newpay_Twrpymnt_Additions_4_Detail; }

    public DbsField getNewpay_Twrpymnt_Irr_Amt() { return newpay_Twrpymnt_Irr_Amt; }

    public DbsField getNewpay_Twrpymnt_Subplan() { return newpay_Twrpymnt_Subplan; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getNewpay_Twrpymnt_Rptd_To_Part_Can_Stamp() { return newpay_Twrpymnt_Rptd_To_Part_Can_Stamp; }

    public DbsField getNewpay_Twrpymnt_Pymnt_Comments() { return newpay_Twrpymnt_Pymnt_Comments; }

    public DbsField getNewpay_Twrpymnt_Updte_User() { return newpay_Twrpymnt_Updte_User; }

    public DbsField getNewpay_Twrpymnt_Sys_Dte_Time() { return newpay_Twrpymnt_Sys_Dte_Time; }

    public DbsField getNewpay_Twrpymnt_Gtn_Dte_Time() { return newpay_Twrpymnt_Gtn_Dte_Time; }

    public DbsField getNewpay_Twrpymnt_Gtn_Ts() { return newpay_Twrpymnt_Gtn_Ts; }

    public DbsField getNewpay_Twrpymnt_Contract_Seq_No() { return newpay_Twrpymnt_Contract_Seq_No; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_newpay = new DataAccessProgramView(new NameInfo("vw_newpay", "NEWPAY"), "TWRPYMNT_PAYMENT_FILE", "TIR_PAYMENT", DdmPeriodicGroups.getInstance().getGroups("TWRPYMNT_PAYMENT_FILE"));
        newpay_Twrpymnt_Tax_Year = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TWRPYMNT_TAX_YEAR");
        newpay_Twrpymnt_Tax_Id_Type = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Tax_Id_Type", "TWRPYMNT-TAX-ID-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPYMNT_TAX_ID_TYPE");
        newpay_Twrpymnt_Tax_Id_Type.setDdmHeader("TAX IDEN-/TIFICATION/NUMBER");
        newpay_Twrpymnt_Tax_Id_Nbr = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "TWRPYMNT_TAX_ID_NBR");
        newpay_Twrpymnt_Company_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPYMNT_COMPANY_CDE");
        newpay_Twrpymnt_Citizen_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Citizen_Cde", "TWRPYMNT-CITIZEN-CDE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TWRPYMNT_CITIZEN_CDE");
        newpay_Twrpymnt_Tax_Citizenship = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Tax_Citizenship", "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_TAX_CITIZENSHIP");
        newpay_Twrpymnt_Contract_Nbr = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_NBR");
        newpay_Twrpymnt_Payee_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPYMNT_PAYEE_CDE");
        newpay_Twrpymnt_Distribution_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_DISTRIBUTION_CDE");
        newpay_Twrpymnt_Residency_Type = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_TYPE");
        newpay_Twrpymnt_Residency_Code = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_RESIDENCY_CODE");
        newpay_Twrpymnt_Ivc_Indicator = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Ivc_Indicator", "TWRPYMNT-IVC-INDICATOR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_IVC_INDICATOR");
        newpay_Twrpymnt_Currency_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Currency_Cde", "TWRPYMNT-CURRENCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_CURRENCY_CDE");
        newpay_Twrpymnt_Paymt_Category = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Paymt_Category", "TWRPYMNT-PAYMT-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_PAYMT_CATEGORY");
        newpay_Twrpymnt_Contract_Lob_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Contract_Lob_Cde", "TWRPYMNT-CONTRACT-LOB-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_LOB_CDE");
        newpay_Twrpymnt_Contract_Lob_Cde.setDdmHeader("LINE/OF/BUSINESS");
        newpay_Twrpymnt_Updt_Dte = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Updt_Dte", "TWRPYMNT-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPYMNT_UPDT_DTE");
        newpay_Twrpymnt_Updt_Time = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Updt_Time", "TWRPYMNT-UPDT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRPYMNT_UPDT_TIME");
        newpay_Twrpymnt_Lu_Ts = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Lu_Ts", "TWRPYMNT-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TWRPYMNT_LU_TS");
        newpay_Twrpymnt_Lu_Ts.setDdmHeader("LAST/UPDATE/TIME STAMP");
        newpay_Twrpymnt_Locality_Cde = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Locality_Cde", "TWRPYMNT-LOCALITY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TWRPYMNT_LOCALITY_CDE");
        newpay_Twrpymnt_Contract_Ppcn_Nbr = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Contract_Ppcn_Nbr", "TWRPYMNT-CONTRACT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_PPCN_NBR");
        newpay_Twrpymnt_Contract_Ppcn_Nbr.setDdmHeader("CONTRACT/TIAA/NUMBER");
        newpay_Twrpymnt_Tot_Contractual_Ivc = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Tot_Contractual_Ivc", "TWRPYMNT-TOT-CONTRACTUAL-IVC", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_TOT_CONTRACTUAL_IVC");
        newpay_Twrpymnt_Contract_Issue_Dte = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Contract_Issue_Dte", "TWRPYMNT-CONTRACT-ISSUE-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_ISSUE_DTE");
        newpay_Twrpymnt_Wpid = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Wpid", "TWRPYMNT-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TWRPYMNT_WPID");
        newpay_Twrpymnt_Summary_Information = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Summary_Information", "TWRPYMNT-SUMMARY-INFORMATION");
        newpay_Twrpymnt_Sum_Gross_Amt = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Gross_Amt", "TWRPYMNT-SUM-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_GROSS_AMT");
        newpay_Twrpymnt_Sum_Ivc_Amt = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Ivc_Amt", "TWRPYMNT-SUM-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_IVC_AMT");
        newpay_Twrpymnt_Sum_Int_Amt = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Int_Amt", "TWRPYMNT-SUM-INT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_INT_AMT");
        newpay_Twrpymnt_Sum_Fed_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Fed_Wthld", "TWRPYMNT-SUM-FED-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_FED_WTHLD");
        newpay_Twrpymnt_Sum_Nra_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Nra_Wthld", "TWRPYMNT-SUM-NRA-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_NRA_WTHLD");
        newpay_Twrpymnt_Sum_Can_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Can_Wthld", "TWRPYMNT-SUM-CAN-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_CAN_WTHLD");
        newpay_Twrpymnt_Sum_Int_Bkup_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Int_Bkup_Wthld", "TWRPYMNT-SUM-INT-BKUP-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_INT_BKUP_WTHLD");
        newpay_Twrpymnt_Sum_Soc_Sec_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Soc_Sec_Wthld", "TWRPYMNT-SUM-SOC-SEC-WTHLD", 
            FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_SOC_SEC_WTHLD");
        newpay_Twrpymnt_Sum_Medicare_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Medicare_Wthld", "TWRPYMNT-SUM-MEDICARE-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_MEDICARE_WTHLD");
        newpay_Twrpymnt_Sum_State_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_State_Wthld", "TWRPYMNT-SUM-STATE-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_STATE_WTHLD");
        newpay_Twrpymnt_Sum_Local_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Local_Wthld", "TWRPYMNT-SUM-LOCAL-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_LOCAL_WTHLD");
        newpay_Twrpymnt_Sum_Refund_Due = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Refund_Due", "TWRPYMNT-SUM-REFUND-DUE", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_REFUND_DUE");
        newpay_Twrpymnt_Sum_Under_Wthld = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Under_Wthld", "TWRPYMNT-SUM-UNDER-WTHLD", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_UNDER_WTHLD");
        newpay_Twrpymnt_Tot_Dist = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Tot_Dist", "TWRPYMNT-TOT-DIST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPYMNT_TOT_DIST");
        newpay_Twrpymnt_Pct_Of_Tot = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Pct_Of_Tot", "TWRPYMNT-PCT-OF-TOT", FieldType.PACKED_DECIMAL, 
            3, RepeatingFieldStrategy.None, "TWRPYMNT_PCT_OF_TOT");
        newpay_Twrpymnt_Dist_Updte_User = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Dist_Updte_User", "TWRPYMNT-DIST-UPDTE-USER", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPYMNT_DIST_UPDTE_USER");
        newpay_Twrpymnt_Dist_Updte_Dte = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Dist_Updte_Dte", "TWRPYMNT-DIST-UPDTE-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPYMNT_DIST_UPDTE_DTE");
        newpay_Twrpymnt_Dist_Updte_Time = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Dist_Updte_Time", "TWRPYMNT-DIST-UPDTE-TIME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "TWRPYMNT_DIST_UPDTE_TIME");
        newpay_Twrpymnt_Sum_Dist_Comments = newpay_Twrpymnt_Summary_Information.newFieldInGroup("newpay_Twrpymnt_Sum_Dist_Comments", "TWRPYMNT-SUM-DIST-COMMENTS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "TWRPYMNT_SUM_DIST_COMMENTS");
        newpay_Count_Casttwrpymnt_Payments = vw_newpay.getRecord().newFieldInGroup("newpay_Count_Casttwrpymnt_Payments", "C*TWRPYMNT-PAYMENTS", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Payments = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Dte = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Dte", "TWRPYMNT-PYMNT-DTE", FieldType.STRING, 
            8, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Status = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Status", "TWRPYMNT-PYMNT-STATUS", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_STATUS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Account_Dte = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Account_Dte", "TWRPYMNT-ACCOUNT-DTE", FieldType.STRING, 
            8, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ACCOUNT_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Effective_Dte = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Effective_Dte", "TWRPYMNT-EFFECTIVE-DTE", FieldType.STRING, 
            8, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_EFFECTIVE_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Intfce_Dte = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Intfce_Dte", "TWRPYMNT-PYMNT-INTFCE-DTE", 
            FieldType.STRING, 8, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_INTFCE_DTE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Updte_Srce_Cde = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Updte_Srce_Cde", "TWRPYMNT-UPDTE-SRCE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_UPDTE_SRCE_CDE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Refer_Nbr = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Refer_Nbr", "TWRPYMNT-PYMNT-REFER-NBR", 
            FieldType.STRING, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_REFER_NBR", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Refer_Nbr.setDdmHeader("SOURCE/OF/DATA");
        newpay_Twrpymnt_Pymnt_Refer_NbrRedef1 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Pymnt_Refer_NbrRedef1", "Redefines", newpay_Twrpymnt_Pymnt_Refer_Nbr);
        newpay_Twrpymnt_Pymnt_Refer_Nbr_Num = newpay_Twrpymnt_Pymnt_Refer_NbrRedef1.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Refer_Nbr_Num", "TWRPYMNT-PYMNT-REFER-NBR-NUM", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,1));
        newpay_Twrpymnt_Dist_Method = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Dist_Method", "TWRPYMNT-DIST-METHOD", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_DIST_METHOD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Orgn_Srce_Cde = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Orgn_Srce_Cde", "TWRPYMNT-ORGN-SRCE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ORGN_SRCE_CDE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Wpid = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Wpid", "TWRPYMNT-PYMNT-WPID", FieldType.STRING, 
            6, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_WPID", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_3 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_3", "TWRPYMNT-ADDITIONS-3", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_3", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_3Redef2 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_3Redef2", "Redefines", newpay_Twrpymnt_Additions_3);
        newpay_Twrpymnt_Additions_3_Detail = newpay_Twrpymnt_Additions_3Redef2.newGroupArrayInGroup("newpay_Twrpymnt_Additions_3_Detail", "TWRPYMNT-ADDITIONS-3-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_3 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_3", "TWRPYMNT-ADDITIONS-3", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_3", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_3 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_3", "TWRPYMNT-ADDITIONS-3", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_3", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_5 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_5", "TWRPYMNT-ADDITIONS-5", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_5", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_5Redef3 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_5Redef3", "Redefines", newpay_Twrpymnt_Additions_5);
        newpay_Twrpymnt_Additions_5_Detail = newpay_Twrpymnt_Additions_5Redef3.newGroupArrayInGroup("newpay_Twrpymnt_Additions_5_Detail", "TWRPYMNT-ADDITIONS-5-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_5 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_5", "TWRPYMNT-ADDITIONS-5", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_5", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_5 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_5", "TWRPYMNT-ADDITIONS-5", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_5", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_5 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_5", "TWRPYMNT-ADDITIONS-5", FieldType.STRING, 
            21, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_5", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Payment_Type = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Payment_Type", "TWRPYMNT-PAYMENT-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PAYMENT_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Settle_Type = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Settle_Type", "TWRPYMNT-SETTLE-TYPE", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_SETTLE_TYPE", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Ivc_Ind = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Ivc_Ind", "TWRPYMNT-IVC-IND", FieldType.STRING, 1, new 
            DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_IND", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Ivc_Protect = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Ivc_Protect", "TWRPYMNT-IVC-PROTECT", FieldType.BOOLEAN, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_PROTECT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Frm1001_Ind = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Frm1001_Ind", "TWRPYMNT-FRM1001-IND", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_FRM1001_IND", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Frm1078_Ind = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Frm1078_Ind", "TWRPYMNT-FRM1078-IND", FieldType.STRING, 
            1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_FRM1078_IND", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Gross_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Gross_Amt", "TWRPYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GROSS_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Ivc_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Ivc_Amt", "TWRPYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Int_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Int_Amt", "TWRPYMNT-INT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_INT_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Int_Bkup_Wthld = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Int_Bkup_Wthld", "TWRPYMNT-INT-BKUP-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_INT_BKUP_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Fed_Wthld_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Fed_Wthld_Amt", "TWRPYMNT-FED-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_FED_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Nra_Wthld_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Nra_Wthld_Amt", "TWRPYMNT-NRA-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_NRA_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Can_Wthld_Amt = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Can_Wthld_Amt", "TWRPYMNT-CAN-WTHLD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_CAN_WTHLD_AMT", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_State_Wthld = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_State_Wthld", "TWRPYMNT-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_STATE_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Local_Wthld = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Local_Wthld", "TWRPYMNT-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_LOCAL_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Soc_Sec_Wthld = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Soc_Sec_Wthld", "TWRPYMNT-SOC-SEC-WTHLD", FieldType.PACKED_DECIMAL, 
            7, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_SOC_SEC_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Medicare_Wthld = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Medicare_Wthld", "TWRPYMNT-MEDICARE-WTHLD", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_MEDICARE_WTHLD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Ivc_Calc_Method = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Ivc_Calc_Method", "TWRPYMNT-IVC-CALC-METHOD", 
            FieldType.STRING, 1, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_IVC_CALC_METHOD", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1Redef4 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_1Redef4", "Redefines", newpay_Twrpymnt_Additions_1);
        newpay_Twrpymnt_Additions_1_Detail = newpay_Twrpymnt_Additions_1Redef4.newGroupArrayInGroup("newpay_Twrpymnt_Additions_1_Detail", "TWRPYMNT-ADDITIONS-1-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_1 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_1", "TWRPYMNT-ADDITIONS-1", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_1", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_2 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_2", "TWRPYMNT-ADDITIONS-2", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_2", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_2Redef5 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_2Redef5", "Redefines", newpay_Twrpymnt_Additions_2);
        newpay_Twrpymnt_Additions_2_Detail = newpay_Twrpymnt_Additions_2Redef5.newGroupArrayInGroup("newpay_Twrpymnt_Additions_2_Detail", "TWRPYMNT-ADDITIONS-2-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_2 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_2", "TWRPYMNT-ADDITIONS-2", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_2", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_2 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_2", "TWRPYMNT-ADDITIONS-2", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_2", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_2 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_2", "TWRPYMNT-ADDITIONS-2", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_2", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_2 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_2", "TWRPYMNT-ADDITIONS-2", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_2", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_TopsRedef6 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_TopsRedef6", "Redefines", newpay_Twrpymnt_Additions_Tops);
        newpay_Twrpymnt_Additions_Tops_Detail = newpay_Twrpymnt_Additions_TopsRedef6.newGroupArrayInGroup("newpay_Twrpymnt_Additions_Tops_Detail", "TWRPYMNT-ADDITIONS-TOPS-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_Tops = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_Tops", "TWRPYMNT-ADDITIONS-TOPS", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_TOPS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_4 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_4", "TWRPYMNT-ADDITIONS-4", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_4", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_4Redef7 = vw_newpay.getRecord().newGroupInGroup("newpay_Twrpymnt_Additions_4Redef7", "Redefines", newpay_Twrpymnt_Additions_4);
        newpay_Twrpymnt_Additions_4_Detail = newpay_Twrpymnt_Additions_4Redef7.newGroupArrayInGroup("newpay_Twrpymnt_Additions_4_Detail", "TWRPYMNT-ADDITIONS-4-DETAIL", 
            new DbsArrayController(1,1));
        newpay_Twrpymnt_Additions_4 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_4", "TWRPYMNT-ADDITIONS-4", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_4", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_4 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_4", "TWRPYMNT-ADDITIONS-4", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_4", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Additions_4 = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Additions_4", "TWRPYMNT-ADDITIONS-4", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_ADDITIONS_4", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Rptd_To_Part_Can_Stamp = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Rptd_To_Part_Can_Stamp", "TWRPYMNT-RPTD-TO-PART-CAN-STAMP", 
            FieldType.STRING, 15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_RPTD_TO_PART_CAN_STAMP", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Pymnt_Comments = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Pymnt_Comments", "TWRPYMNT-PYMNT-COMMENTS", FieldType.STRING, 
            50, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_PYMNT_COMMENTS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Updte_User = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Updte_User", "TWRPYMNT-UPDTE-USER", FieldType.STRING, 
            8, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_UPDTE_USER", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Sys_Dte_Time = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Sys_Dte_Time", "TWRPYMNT-SYS-DTE-TIME", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_SYS_DTE_TIME", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Gtn_Dte_Time = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Gtn_Dte_Time", "TWRPYMNT-GTN-DTE-TIME", FieldType.STRING, 
            15, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GTN_DTE_TIME", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Gtn_Ts = newpay_Twrpymnt_Payments.newFieldArrayInGroup("newpay_Twrpymnt_Gtn_Ts", "TWRPYMNT-GTN-TS", FieldType.TIME, new DbsArrayController(1,1) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TWRPYMNT_GTN_TS", "TIR_PAYMENT_TWRPYMNT_PAYMENTS");
        newpay_Twrpymnt_Contract_Seq_No = vw_newpay.getRecord().newFieldInGroup("newpay_Twrpymnt_Contract_Seq_No", "TWRPYMNT-CONTRACT-SEQ-NO", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TWRPYMNT_CONTRACT_SEQ_NO");
        vw_newpay.setUniquePeList();

        this.setRecordName("LdaTwrl820a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_newpay.reset();
    }

    // Constructor
    public LdaTwrl820a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
