/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:30 PM
**        * FROM NATURAL LDA     : TWRL3323
************************************************************
**        * FILE NAME            : LdaTwrl3323.java
**        * CLASS NAME           : LdaTwrl3323
**        * INSTANCE NAME        : LdaTwrl3323
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3323 extends DbsRecord
{
    // Properties
    private DbsGroup irs_C_Out_Tape;
    private DbsField irs_C_Out_Tape_Irs_C_Record_Type;
    private DbsField irs_C_Out_Tape_Irs_C_Number_Of_Payees;
    private DbsField irs_C_Out_Tape_Irs_C_Filler_1;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_1;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_2;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_3;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_4;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_5;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_6;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_7;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_8;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_9;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_A;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_B;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_C;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_D;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_E;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_F;
    private DbsField irs_C_Out_Tape_Irs_C_Control_Total_G;
    private DbsField irs_C_Out_Tape_Irs_C_Filler_2;
    private DbsField irs_C_Out_Tape_Irs_C_Record_Sequence_No;
    private DbsField irs_C_Out_Tape_Irs_C_Filler_3;
    private DbsField irs_C_Out_Tape_Irs_C_Blank_Cr_Lf;
    private DbsGroup irs_C_Out_TapeRedef1;
    private DbsField irs_C_Out_Tape_Irs_C_Move_1;
    private DbsField irs_C_Out_Tape_Irs_C_Move_2;
    private DbsField irs_C_Out_Tape_Irs_C_Move_3;

    public DbsGroup getIrs_C_Out_Tape() { return irs_C_Out_Tape; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Record_Type() { return irs_C_Out_Tape_Irs_C_Record_Type; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Number_Of_Payees() { return irs_C_Out_Tape_Irs_C_Number_Of_Payees; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Filler_1() { return irs_C_Out_Tape_Irs_C_Filler_1; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_1() { return irs_C_Out_Tape_Irs_C_Control_Total_1; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_2() { return irs_C_Out_Tape_Irs_C_Control_Total_2; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_3() { return irs_C_Out_Tape_Irs_C_Control_Total_3; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_4() { return irs_C_Out_Tape_Irs_C_Control_Total_4; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_5() { return irs_C_Out_Tape_Irs_C_Control_Total_5; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_6() { return irs_C_Out_Tape_Irs_C_Control_Total_6; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_7() { return irs_C_Out_Tape_Irs_C_Control_Total_7; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_8() { return irs_C_Out_Tape_Irs_C_Control_Total_8; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_9() { return irs_C_Out_Tape_Irs_C_Control_Total_9; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_A() { return irs_C_Out_Tape_Irs_C_Control_Total_A; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_B() { return irs_C_Out_Tape_Irs_C_Control_Total_B; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_C() { return irs_C_Out_Tape_Irs_C_Control_Total_C; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_D() { return irs_C_Out_Tape_Irs_C_Control_Total_D; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_E() { return irs_C_Out_Tape_Irs_C_Control_Total_E; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_F() { return irs_C_Out_Tape_Irs_C_Control_Total_F; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Control_Total_G() { return irs_C_Out_Tape_Irs_C_Control_Total_G; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Filler_2() { return irs_C_Out_Tape_Irs_C_Filler_2; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Record_Sequence_No() { return irs_C_Out_Tape_Irs_C_Record_Sequence_No; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Filler_3() { return irs_C_Out_Tape_Irs_C_Filler_3; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Blank_Cr_Lf() { return irs_C_Out_Tape_Irs_C_Blank_Cr_Lf; }

    public DbsGroup getIrs_C_Out_TapeRedef1() { return irs_C_Out_TapeRedef1; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Move_1() { return irs_C_Out_Tape_Irs_C_Move_1; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Move_2() { return irs_C_Out_Tape_Irs_C_Move_2; }

    public DbsField getIrs_C_Out_Tape_Irs_C_Move_3() { return irs_C_Out_Tape_Irs_C_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_C_Out_Tape = newGroupInRecord("irs_C_Out_Tape", "IRS-C-OUT-TAPE");
        irs_C_Out_Tape_Irs_C_Record_Type = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Record_Type", "IRS-C-RECORD-TYPE", FieldType.STRING, 1);
        irs_C_Out_Tape_Irs_C_Number_Of_Payees = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Number_Of_Payees", "IRS-C-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        irs_C_Out_Tape_Irs_C_Filler_1 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Filler_1", "IRS-C-FILLER-1", FieldType.STRING, 6);
        irs_C_Out_Tape_Irs_C_Control_Total_1 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_1", "IRS-C-CONTROL-TOTAL-1", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_2 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_2", "IRS-C-CONTROL-TOTAL-2", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_3 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_3", "IRS-C-CONTROL-TOTAL-3", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_4 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_4", "IRS-C-CONTROL-TOTAL-4", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_5 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_5", "IRS-C-CONTROL-TOTAL-5", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_6 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_6", "IRS-C-CONTROL-TOTAL-6", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_7 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_7", "IRS-C-CONTROL-TOTAL-7", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_8 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_8", "IRS-C-CONTROL-TOTAL-8", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_9 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_9", "IRS-C-CONTROL-TOTAL-9", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_A = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_A", "IRS-C-CONTROL-TOTAL-A", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_B = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_B", "IRS-C-CONTROL-TOTAL-B", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_C = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_C", "IRS-C-CONTROL-TOTAL-C", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_D = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_D", "IRS-C-CONTROL-TOTAL-D", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_E = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_E", "IRS-C-CONTROL-TOTAL-E", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_F = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_F", "IRS-C-CONTROL-TOTAL-F", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Control_Total_G = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Control_Total_G", "IRS-C-CONTROL-TOTAL-G", FieldType.DECIMAL, 
            18,2);
        irs_C_Out_Tape_Irs_C_Filler_2 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Filler_2", "IRS-C-FILLER-2", FieldType.STRING, 196);
        irs_C_Out_Tape_Irs_C_Record_Sequence_No = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Record_Sequence_No", "IRS-C-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_C_Out_Tape_Irs_C_Filler_3 = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Filler_3", "IRS-C-FILLER-3", FieldType.STRING, 241);
        irs_C_Out_Tape_Irs_C_Blank_Cr_Lf = irs_C_Out_Tape.newFieldInGroup("irs_C_Out_Tape_Irs_C_Blank_Cr_Lf", "IRS-C-BLANK-CR-LF", FieldType.STRING, 2);
        irs_C_Out_TapeRedef1 = newGroupInRecord("irs_C_Out_TapeRedef1", "Redefines", irs_C_Out_Tape);
        irs_C_Out_Tape_Irs_C_Move_1 = irs_C_Out_TapeRedef1.newFieldInGroup("irs_C_Out_Tape_Irs_C_Move_1", "IRS-C-MOVE-1", FieldType.STRING, 250);
        irs_C_Out_Tape_Irs_C_Move_2 = irs_C_Out_TapeRedef1.newFieldInGroup("irs_C_Out_Tape_Irs_C_Move_2", "IRS-C-MOVE-2", FieldType.STRING, 250);
        irs_C_Out_Tape_Irs_C_Move_3 = irs_C_Out_TapeRedef1.newFieldInGroup("irs_C_Out_Tape_Irs_C_Move_3", "IRS-C-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl3323");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_C_Out_Tape_Irs_C_Record_Type.setInitialValue("C");
    }

    // Constructor
    public LdaTwrl3323() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
