/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:30 PM
**        * FROM NATURAL LDA     : TWRL3325
************************************************************
**        * FILE NAME            : LdaTwrl3325.java
**        * CLASS NAME           : LdaTwrl3325
**        * INSTANCE NAME        : LdaTwrl3325
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3325 extends DbsRecord
{
    // Properties
    private DbsGroup irs_K_Out_Tape;
    private DbsField irs_K_Out_Tape_Irs_K_Record_Type;
    private DbsField irs_K_Out_Tape_Irs_K_Number_Of_Payees;
    private DbsField irs_K_Out_Tape_Irs_K_Filler_1;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_1;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_2;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_3;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_4;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_5;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_6;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_7;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_8;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_9;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_A;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_B;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_C;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_D;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_E;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_F;
    private DbsField irs_K_Out_Tape_Irs_K_Control_Total_G;
    private DbsField irs_K_Out_Tape_Irs_K_Filler_2;
    private DbsField irs_K_Out_Tape_Irs_K_Record_Sequence_No;
    private DbsField irs_K_Out_Tape_Irs_K_Filler_3;
    private DbsField irs_K_Out_Tape_Irs_K_St_Tax_Withheld_A;
    private DbsGroup irs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1;
    private DbsField irs_K_Out_Tape_Irs_K_St_Tax_Withheld;
    private DbsField irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A;
    private DbsGroup irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2;
    private DbsField irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld;
    private DbsField irs_K_Out_Tape_Irs_K_Filler_4;
    private DbsField irs_K_Out_Tape_Irs_K_State_Code;
    private DbsField irs_K_Out_Tape_Irs_K_Blank_Cr_Lf;
    private DbsGroup irs_K_Out_TapeRedef3;
    private DbsField irs_K_Out_Tape_Irs_K_Move_1;
    private DbsField irs_K_Out_Tape_Irs_K_Move_2;
    private DbsField irs_K_Out_Tape_Irs_K_Move_3;

    public DbsGroup getIrs_K_Out_Tape() { return irs_K_Out_Tape; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Record_Type() { return irs_K_Out_Tape_Irs_K_Record_Type; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Number_Of_Payees() { return irs_K_Out_Tape_Irs_K_Number_Of_Payees; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Filler_1() { return irs_K_Out_Tape_Irs_K_Filler_1; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_1() { return irs_K_Out_Tape_Irs_K_Control_Total_1; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_2() { return irs_K_Out_Tape_Irs_K_Control_Total_2; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_3() { return irs_K_Out_Tape_Irs_K_Control_Total_3; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_4() { return irs_K_Out_Tape_Irs_K_Control_Total_4; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_5() { return irs_K_Out_Tape_Irs_K_Control_Total_5; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_6() { return irs_K_Out_Tape_Irs_K_Control_Total_6; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_7() { return irs_K_Out_Tape_Irs_K_Control_Total_7; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_8() { return irs_K_Out_Tape_Irs_K_Control_Total_8; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_9() { return irs_K_Out_Tape_Irs_K_Control_Total_9; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_A() { return irs_K_Out_Tape_Irs_K_Control_Total_A; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_B() { return irs_K_Out_Tape_Irs_K_Control_Total_B; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_C() { return irs_K_Out_Tape_Irs_K_Control_Total_C; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_D() { return irs_K_Out_Tape_Irs_K_Control_Total_D; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_E() { return irs_K_Out_Tape_Irs_K_Control_Total_E; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_F() { return irs_K_Out_Tape_Irs_K_Control_Total_F; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Control_Total_G() { return irs_K_Out_Tape_Irs_K_Control_Total_G; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Filler_2() { return irs_K_Out_Tape_Irs_K_Filler_2; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Record_Sequence_No() { return irs_K_Out_Tape_Irs_K_Record_Sequence_No; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Filler_3() { return irs_K_Out_Tape_Irs_K_Filler_3; }

    public DbsField getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld_A() { return irs_K_Out_Tape_Irs_K_St_Tax_Withheld_A; }

    public DbsGroup getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1() { return irs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1; }

    public DbsField getIrs_K_Out_Tape_Irs_K_St_Tax_Withheld() { return irs_K_Out_Tape_Irs_K_St_Tax_Withheld; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A() { return irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A; }

    public DbsGroup getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2() { return irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Lc_Tax_Withheld() { return irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Filler_4() { return irs_K_Out_Tape_Irs_K_Filler_4; }

    public DbsField getIrs_K_Out_Tape_Irs_K_State_Code() { return irs_K_Out_Tape_Irs_K_State_Code; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Blank_Cr_Lf() { return irs_K_Out_Tape_Irs_K_Blank_Cr_Lf; }

    public DbsGroup getIrs_K_Out_TapeRedef3() { return irs_K_Out_TapeRedef3; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Move_1() { return irs_K_Out_Tape_Irs_K_Move_1; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Move_2() { return irs_K_Out_Tape_Irs_K_Move_2; }

    public DbsField getIrs_K_Out_Tape_Irs_K_Move_3() { return irs_K_Out_Tape_Irs_K_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_K_Out_Tape = newGroupInRecord("irs_K_Out_Tape", "IRS-K-OUT-TAPE");
        irs_K_Out_Tape_Irs_K_Record_Type = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Record_Type", "IRS-K-RECORD-TYPE", FieldType.STRING, 1);
        irs_K_Out_Tape_Irs_K_Number_Of_Payees = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Number_Of_Payees", "IRS-K-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        irs_K_Out_Tape_Irs_K_Filler_1 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Filler_1", "IRS-K-FILLER-1", FieldType.STRING, 6);
        irs_K_Out_Tape_Irs_K_Control_Total_1 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_1", "IRS-K-CONTROL-TOTAL-1", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_2 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_2", "IRS-K-CONTROL-TOTAL-2", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_3 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_3", "IRS-K-CONTROL-TOTAL-3", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_4 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_4", "IRS-K-CONTROL-TOTAL-4", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_5 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_5", "IRS-K-CONTROL-TOTAL-5", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_6 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_6", "IRS-K-CONTROL-TOTAL-6", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_7 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_7", "IRS-K-CONTROL-TOTAL-7", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_8 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_8", "IRS-K-CONTROL-TOTAL-8", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_9 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_9", "IRS-K-CONTROL-TOTAL-9", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_A = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_A", "IRS-K-CONTROL-TOTAL-A", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_B = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_B", "IRS-K-CONTROL-TOTAL-B", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_C = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_C", "IRS-K-CONTROL-TOTAL-C", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_D = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_D", "IRS-K-CONTROL-TOTAL-D", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_E = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_E", "IRS-K-CONTROL-TOTAL-E", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_F = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_F", "IRS-K-CONTROL-TOTAL-F", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Control_Total_G = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Control_Total_G", "IRS-K-CONTROL-TOTAL-G", FieldType.DECIMAL, 
            18,2);
        irs_K_Out_Tape_Irs_K_Filler_2 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Filler_2", "IRS-K-FILLER-2", FieldType.STRING, 196);
        irs_K_Out_Tape_Irs_K_Record_Sequence_No = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Record_Sequence_No", "IRS-K-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_K_Out_Tape_Irs_K_Filler_3 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Filler_3", "IRS-K-FILLER-3", FieldType.STRING, 199);
        irs_K_Out_Tape_Irs_K_St_Tax_Withheld_A = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_St_Tax_Withheld_A", "IRS-K-ST-TAX-WITHHELD-A", FieldType.STRING, 
            18);
        irs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1 = irs_K_Out_Tape.newGroupInGroup("irs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1", "Redefines", irs_K_Out_Tape_Irs_K_St_Tax_Withheld_A);
        irs_K_Out_Tape_Irs_K_St_Tax_Withheld = irs_K_Out_Tape_Irs_K_St_Tax_Withheld_ARedef1.newFieldInGroup("irs_K_Out_Tape_Irs_K_St_Tax_Withheld", "IRS-K-ST-TAX-WITHHELD", 
            FieldType.DECIMAL, 18,2);
        irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A", "IRS-K-LC-TAX-WITHHELD-A", FieldType.STRING, 
            18);
        irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2 = irs_K_Out_Tape.newGroupInGroup("irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2", "Redefines", irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_A);
        irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld = irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld_ARedef2.newFieldInGroup("irs_K_Out_Tape_Irs_K_Lc_Tax_Withheld", "IRS-K-LC-TAX-WITHHELD", 
            FieldType.DECIMAL, 18,2);
        irs_K_Out_Tape_Irs_K_Filler_4 = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Filler_4", "IRS-K-FILLER-4", FieldType.STRING, 4);
        irs_K_Out_Tape_Irs_K_State_Code = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_State_Code", "IRS-K-STATE-CODE", FieldType.STRING, 2);
        irs_K_Out_Tape_Irs_K_Blank_Cr_Lf = irs_K_Out_Tape.newFieldInGroup("irs_K_Out_Tape_Irs_K_Blank_Cr_Lf", "IRS-K-BLANK-CR-LF", FieldType.STRING, 2);
        irs_K_Out_TapeRedef3 = newGroupInRecord("irs_K_Out_TapeRedef3", "Redefines", irs_K_Out_Tape);
        irs_K_Out_Tape_Irs_K_Move_1 = irs_K_Out_TapeRedef3.newFieldInGroup("irs_K_Out_Tape_Irs_K_Move_1", "IRS-K-MOVE-1", FieldType.STRING, 250);
        irs_K_Out_Tape_Irs_K_Move_2 = irs_K_Out_TapeRedef3.newFieldInGroup("irs_K_Out_Tape_Irs_K_Move_2", "IRS-K-MOVE-2", FieldType.STRING, 250);
        irs_K_Out_Tape_Irs_K_Move_3 = irs_K_Out_TapeRedef3.newFieldInGroup("irs_K_Out_Tape_Irs_K_Move_3", "IRS-K-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl3325");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_K_Out_Tape_Irs_K_Record_Type.setInitialValue("K");
    }

    // Constructor
    public LdaTwrl3325() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
