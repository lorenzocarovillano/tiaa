/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:39 PM
**        * FROM NATURAL LDA     : TWRL443A
************************************************************
**        * FILE NAME            : LdaTwrl443a.java
**        * CLASS NAME           : LdaTwrl443a
**        * INSTANCE NAME        : LdaTwrl443a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_T_Tape;
    private DbsField pnd_T_Tape_Pnd_T_Record_Type;
    private DbsField pnd_T_Tape_Pnd_T_Payment_Year;
    private DbsField pnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind;
    private DbsField pnd_T_Tape_Pnd_T_Transmitters_Tin;
    private DbsField pnd_T_Tape_Pnd_T_Transmitter_Control_Code;
    private DbsField pnd_T_Tape_Pnd_T_Filler_1;
    private DbsField pnd_T_Tape_Pnd_T_Test_File_Indicator;
    private DbsField pnd_T_Tape_Pnd_T_Foreign_Entity_Ind;
    private DbsField pnd_T_Tape_Pnd_T_Transmitter_Name;
    private DbsField pnd_T_Tape_Pnd_T_Transmitter_Name_Continued;
    private DbsField pnd_T_Tape_Pnd_T_Company_Name;
    private DbsField pnd_T_Tape_Pnd_T_Company_Name_Continued;
    private DbsField pnd_T_Tape_Pnd_T_Company_Mailing_Address;
    private DbsField pnd_T_Tape_Pnd_T_Company_City;
    private DbsField pnd_T_Tape_Pnd_T_Company_State;
    private DbsField pnd_T_Tape_Pnd_T_Company_Zip_Code;
    private DbsField pnd_T_Tape_Pnd_T_Filler_2;
    private DbsField pnd_T_Tape_Pnd_T_Total_Number_Of_Payees;
    private DbsField pnd_T_Tape_Pnd_T_Contact_Name;
    private DbsField pnd_T_Tape_Pnd_T_Contact_Phone_N_Extension;
    private DbsField pnd_T_Tape_Pnd_T_Contact_E_Mail_Address;
    private DbsField pnd_T_Tape_Pnd_T_Filler_3;
    private DbsField pnd_T_Tape_Pnd_T_Sequence_Number;
    private DbsField pnd_T_Tape_Pnd_T_Filler_4;
    private DbsField pnd_T_Tape_Pnd_T_Vendor_Indicator;
    private DbsField pnd_T_Tape_Pnd_T_Filler_5;
    private DbsField pnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind;
    private DbsField pnd_T_Tape_Pnd_T_Filler_6;
    private DbsField pnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf;
    private DbsGroup pnd_T_TapeRedef1;
    private DbsField pnd_T_Tape_Pnd_T_Move_1;
    private DbsField pnd_T_Tape_Pnd_T_Move_2;
    private DbsField pnd_T_Tape_Pnd_T_Move_3;

    public DbsGroup getPnd_T_Tape() { return pnd_T_Tape; }

    public DbsField getPnd_T_Tape_Pnd_T_Record_Type() { return pnd_T_Tape_Pnd_T_Record_Type; }

    public DbsField getPnd_T_Tape_Pnd_T_Payment_Year() { return pnd_T_Tape_Pnd_T_Payment_Year; }

    public DbsField getPnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind() { return pnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind; }

    public DbsField getPnd_T_Tape_Pnd_T_Transmitters_Tin() { return pnd_T_Tape_Pnd_T_Transmitters_Tin; }

    public DbsField getPnd_T_Tape_Pnd_T_Transmitter_Control_Code() { return pnd_T_Tape_Pnd_T_Transmitter_Control_Code; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_1() { return pnd_T_Tape_Pnd_T_Filler_1; }

    public DbsField getPnd_T_Tape_Pnd_T_Test_File_Indicator() { return pnd_T_Tape_Pnd_T_Test_File_Indicator; }

    public DbsField getPnd_T_Tape_Pnd_T_Foreign_Entity_Ind() { return pnd_T_Tape_Pnd_T_Foreign_Entity_Ind; }

    public DbsField getPnd_T_Tape_Pnd_T_Transmitter_Name() { return pnd_T_Tape_Pnd_T_Transmitter_Name; }

    public DbsField getPnd_T_Tape_Pnd_T_Transmitter_Name_Continued() { return pnd_T_Tape_Pnd_T_Transmitter_Name_Continued; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_Name() { return pnd_T_Tape_Pnd_T_Company_Name; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_Name_Continued() { return pnd_T_Tape_Pnd_T_Company_Name_Continued; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_Mailing_Address() { return pnd_T_Tape_Pnd_T_Company_Mailing_Address; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_City() { return pnd_T_Tape_Pnd_T_Company_City; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_State() { return pnd_T_Tape_Pnd_T_Company_State; }

    public DbsField getPnd_T_Tape_Pnd_T_Company_Zip_Code() { return pnd_T_Tape_Pnd_T_Company_Zip_Code; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_2() { return pnd_T_Tape_Pnd_T_Filler_2; }

    public DbsField getPnd_T_Tape_Pnd_T_Total_Number_Of_Payees() { return pnd_T_Tape_Pnd_T_Total_Number_Of_Payees; }

    public DbsField getPnd_T_Tape_Pnd_T_Contact_Name() { return pnd_T_Tape_Pnd_T_Contact_Name; }

    public DbsField getPnd_T_Tape_Pnd_T_Contact_Phone_N_Extension() { return pnd_T_Tape_Pnd_T_Contact_Phone_N_Extension; }

    public DbsField getPnd_T_Tape_Pnd_T_Contact_E_Mail_Address() { return pnd_T_Tape_Pnd_T_Contact_E_Mail_Address; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_3() { return pnd_T_Tape_Pnd_T_Filler_3; }

    public DbsField getPnd_T_Tape_Pnd_T_Sequence_Number() { return pnd_T_Tape_Pnd_T_Sequence_Number; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_4() { return pnd_T_Tape_Pnd_T_Filler_4; }

    public DbsField getPnd_T_Tape_Pnd_T_Vendor_Indicator() { return pnd_T_Tape_Pnd_T_Vendor_Indicator; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_5() { return pnd_T_Tape_Pnd_T_Filler_5; }

    public DbsField getPnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind() { return pnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind; }

    public DbsField getPnd_T_Tape_Pnd_T_Filler_6() { return pnd_T_Tape_Pnd_T_Filler_6; }

    public DbsField getPnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf() { return pnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf; }

    public DbsGroup getPnd_T_TapeRedef1() { return pnd_T_TapeRedef1; }

    public DbsField getPnd_T_Tape_Pnd_T_Move_1() { return pnd_T_Tape_Pnd_T_Move_1; }

    public DbsField getPnd_T_Tape_Pnd_T_Move_2() { return pnd_T_Tape_Pnd_T_Move_2; }

    public DbsField getPnd_T_Tape_Pnd_T_Move_3() { return pnd_T_Tape_Pnd_T_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_T_Tape = newGroupInRecord("pnd_T_Tape", "#T-TAPE");
        pnd_T_Tape_Pnd_T_Record_Type = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Record_Type", "#T-RECORD-TYPE", FieldType.STRING, 1);
        pnd_T_Tape_Pnd_T_Payment_Year = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Payment_Year", "#T-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind", "#T-PRIOR-YR-DATA-IND", FieldType.STRING, 
            1);
        pnd_T_Tape_Pnd_T_Transmitters_Tin = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Transmitters_Tin", "#T-TRANSMITTERS-TIN", FieldType.STRING, 9);
        pnd_T_Tape_Pnd_T_Transmitter_Control_Code = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Transmitter_Control_Code", "#T-TRANSMITTER-CONTROL-CODE", 
            FieldType.STRING, 5);
        pnd_T_Tape_Pnd_T_Filler_1 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_1", "#T-FILLER-1", FieldType.STRING, 7);
        pnd_T_Tape_Pnd_T_Test_File_Indicator = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Test_File_Indicator", "#T-TEST-FILE-INDICATOR", FieldType.STRING, 
            1);
        pnd_T_Tape_Pnd_T_Foreign_Entity_Ind = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Foreign_Entity_Ind", "#T-FOREIGN-ENTITY-IND", FieldType.STRING, 
            1);
        pnd_T_Tape_Pnd_T_Transmitter_Name = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Transmitter_Name", "#T-TRANSMITTER-NAME", FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Transmitter_Name_Continued = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Transmitter_Name_Continued", "#T-TRANSMITTER-NAME-CONTINUED", 
            FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Company_Name = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_Name", "#T-COMPANY-NAME", FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Company_Name_Continued = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_Name_Continued", "#T-COMPANY-NAME-CONTINUED", FieldType.STRING, 
            40);
        pnd_T_Tape_Pnd_T_Company_Mailing_Address = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_Mailing_Address", "#T-COMPANY-MAILING-ADDRESS", 
            FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Company_City = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_City", "#T-COMPANY-CITY", FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Company_State = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_State", "#T-COMPANY-STATE", FieldType.STRING, 2);
        pnd_T_Tape_Pnd_T_Company_Zip_Code = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Company_Zip_Code", "#T-COMPANY-ZIP-CODE", FieldType.STRING, 9);
        pnd_T_Tape_Pnd_T_Filler_2 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_2", "#T-FILLER-2", FieldType.STRING, 15);
        pnd_T_Tape_Pnd_T_Total_Number_Of_Payees = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Total_Number_Of_Payees", "#T-TOTAL-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        pnd_T_Tape_Pnd_T_Contact_Name = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Contact_Name", "#T-CONTACT-NAME", FieldType.STRING, 40);
        pnd_T_Tape_Pnd_T_Contact_Phone_N_Extension = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Contact_Phone_N_Extension", "#T-CONTACT-PHONE-N-EXTENSION", 
            FieldType.STRING, 15);
        pnd_T_Tape_Pnd_T_Contact_E_Mail_Address = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Contact_E_Mail_Address", "#T-CONTACT-E-MAIL-ADDRESS", FieldType.STRING, 
            50);
        pnd_T_Tape_Pnd_T_Filler_3 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_3", "#T-FILLER-3", FieldType.STRING, 91);
        pnd_T_Tape_Pnd_T_Sequence_Number = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Sequence_Number", "#T-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_T_Tape_Pnd_T_Filler_4 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_4", "#T-FILLER-4", FieldType.STRING, 10);
        pnd_T_Tape_Pnd_T_Vendor_Indicator = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Vendor_Indicator", "#T-VENDOR-INDICATOR", FieldType.STRING, 1);
        pnd_T_Tape_Pnd_T_Filler_5 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_5", "#T-FILLER-5", FieldType.STRING, 221);
        pnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind", "#T-VENDOR-FOREIGN-ENTITY-IND", 
            FieldType.STRING, 1);
        pnd_T_Tape_Pnd_T_Filler_6 = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Filler_6", "#T-FILLER-6", FieldType.STRING, 8);
        pnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf = pnd_T_Tape.newFieldInGroup("pnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf", "#T-BLANK-OR-CR-LF", FieldType.STRING, 2);
        pnd_T_TapeRedef1 = newGroupInRecord("pnd_T_TapeRedef1", "Redefines", pnd_T_Tape);
        pnd_T_Tape_Pnd_T_Move_1 = pnd_T_TapeRedef1.newFieldInGroup("pnd_T_Tape_Pnd_T_Move_1", "#T-MOVE-1", FieldType.STRING, 250);
        pnd_T_Tape_Pnd_T_Move_2 = pnd_T_TapeRedef1.newFieldInGroup("pnd_T_Tape_Pnd_T_Move_2", "#T-MOVE-2", FieldType.STRING, 250);
        pnd_T_Tape_Pnd_T_Move_3 = pnd_T_TapeRedef1.newFieldInGroup("pnd_T_Tape_Pnd_T_Move_3", "#T-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_T_Tape_Pnd_T_Record_Type.setInitialValue("T");
        pnd_T_Tape_Pnd_T_Prior_Yr_Data_Ind.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Transmitters_Tin.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Transmitter_Control_Code.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Test_File_Indicator.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Foreign_Entity_Ind.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Transmitter_Name.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Transmitter_Name_Continued.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_Name.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_Name_Continued.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_Mailing_Address.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_City.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_State.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Company_Zip_Code.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Contact_Name.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Contact_Phone_N_Extension.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Contact_E_Mail_Address.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Filler_3.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Sequence_Number.setInitialValue(0);
        pnd_T_Tape_Pnd_T_Filler_4.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Vendor_Indicator.setInitialValue("I");
        pnd_T_Tape_Pnd_T_Filler_5.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Vendor_Foreign_Entity_Ind.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Filler_6.setInitialValue(" ");
        pnd_T_Tape_Pnd_T_Blank_Or_Cr_Lf.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
