/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:42 PM
**        * FROM NATURAL PDA     : TWRAIRAA
************************************************************
**        * FILE NAME            : PdaTwrairaa.java
**        * CLASS NAME           : PdaTwrairaa
**        * INSTANCE NAME        : PdaTwrairaa
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwrairaa extends PdaBase
{
    // Properties
    private DbsGroup pnd_Twrairaa;
    private DbsGroup pnd_Twrairaa_Pnd_Input_Parms;
    private DbsField pnd_Twrairaa_Pnd_Abend_Ind;
    private DbsField pnd_Twrairaa_Pnd_Display_Ind;
    private DbsField pnd_Twrairaa_Pnd_Ira_Acct_Type;
    private DbsGroup pnd_Twrairaa_Pnd_Output_Data;
    private DbsField pnd_Twrairaa_Pnd_Ret_Code;
    private DbsField pnd_Twrairaa_Pnd_Ret_Msg;
    private DbsField pnd_Twrairaa_Pnd_Iraa_Short;
    private DbsField pnd_Twrairaa_Pnd_Iraa_Desc;

    public DbsGroup getPnd_Twrairaa() { return pnd_Twrairaa; }

    public DbsGroup getPnd_Twrairaa_Pnd_Input_Parms() { return pnd_Twrairaa_Pnd_Input_Parms; }

    public DbsField getPnd_Twrairaa_Pnd_Abend_Ind() { return pnd_Twrairaa_Pnd_Abend_Ind; }

    public DbsField getPnd_Twrairaa_Pnd_Display_Ind() { return pnd_Twrairaa_Pnd_Display_Ind; }

    public DbsField getPnd_Twrairaa_Pnd_Ira_Acct_Type() { return pnd_Twrairaa_Pnd_Ira_Acct_Type; }

    public DbsGroup getPnd_Twrairaa_Pnd_Output_Data() { return pnd_Twrairaa_Pnd_Output_Data; }

    public DbsField getPnd_Twrairaa_Pnd_Ret_Code() { return pnd_Twrairaa_Pnd_Ret_Code; }

    public DbsField getPnd_Twrairaa_Pnd_Ret_Msg() { return pnd_Twrairaa_Pnd_Ret_Msg; }

    public DbsField getPnd_Twrairaa_Pnd_Iraa_Short() { return pnd_Twrairaa_Pnd_Iraa_Short; }

    public DbsField getPnd_Twrairaa_Pnd_Iraa_Desc() { return pnd_Twrairaa_Pnd_Iraa_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrairaa = dbsRecord.newGroupInRecord("pnd_Twrairaa", "#TWRAIRAA");
        pnd_Twrairaa.setParameterOption(ParameterOption.ByReference);
        pnd_Twrairaa_Pnd_Input_Parms = pnd_Twrairaa.newGroupInGroup("pnd_Twrairaa_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Twrairaa_Pnd_Abend_Ind = pnd_Twrairaa_Pnd_Input_Parms.newFieldInGroup("pnd_Twrairaa_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN);
        pnd_Twrairaa_Pnd_Display_Ind = pnd_Twrairaa_Pnd_Input_Parms.newFieldInGroup("pnd_Twrairaa_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN);
        pnd_Twrairaa_Pnd_Ira_Acct_Type = pnd_Twrairaa_Pnd_Input_Parms.newFieldInGroup("pnd_Twrairaa_Pnd_Ira_Acct_Type", "#IRA-ACCT-TYPE", FieldType.STRING, 
            2);
        pnd_Twrairaa_Pnd_Output_Data = pnd_Twrairaa.newGroupInGroup("pnd_Twrairaa_Pnd_Output_Data", "#OUTPUT-DATA");
        pnd_Twrairaa_Pnd_Ret_Code = pnd_Twrairaa_Pnd_Output_Data.newFieldInGroup("pnd_Twrairaa_Pnd_Ret_Code", "#RET-CODE", FieldType.BOOLEAN);
        pnd_Twrairaa_Pnd_Ret_Msg = pnd_Twrairaa_Pnd_Output_Data.newFieldInGroup("pnd_Twrairaa_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);
        pnd_Twrairaa_Pnd_Iraa_Short = pnd_Twrairaa_Pnd_Output_Data.newFieldInGroup("pnd_Twrairaa_Pnd_Iraa_Short", "#IRAA-SHORT", FieldType.STRING, 4);
        pnd_Twrairaa_Pnd_Iraa_Desc = pnd_Twrairaa_Pnd_Output_Data.newFieldInGroup("pnd_Twrairaa_Pnd_Iraa_Desc", "#IRAA-DESC", FieldType.STRING, 11);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwrairaa(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

