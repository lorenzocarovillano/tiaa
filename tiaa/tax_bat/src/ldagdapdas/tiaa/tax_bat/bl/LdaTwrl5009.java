/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:09 PM
**        * FROM NATURAL LDA     : TWRL5009
************************************************************
**        * FILE NAME            : LdaTwrl5009.java
**        * CLASS NAME           : LdaTwrl5009
**        * INSTANCE NAME        : LdaTwrl5009
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl5009 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl5009;
    private DbsField pnd_Twrl5009_Pnd_Res_Max;
    private DbsGroup pnd_Twrl5009_Pnd_Country_Table;
    private DbsField pnd_Twrl5009_Pnd_Country_Code;
    private DbsField pnd_Twrl5009_Pnd_Nr4_Country;
    private DbsField pnd_Twrl5009_Pnd_Nr4_Error;
    private DbsField pnd_Twrl5009_Pnd_Treaty_Ind;
    private DbsField pnd_Twrl5009_Pnd_Pp_Per;
    private DbsField pnd_Twrl5009_Pnd_Np_Per;
    private DbsField pnd_Twrl5009_Pnd_Dpi_Per;
    private DbsField pnd_Twrl5009_Pnd_Pa_Pp_Per;
    private DbsField pnd_Twrl5009_Pnd_Pa_Np_Per;
    private DbsField pnd_Twrl5009_Pnd_Ira_Pp_Per;
    private DbsField pnd_Twrl5009_Pnd_Ira_Np_Per;
    private DbsField pnd_Twrl5009_Pnd_Early_Distrib_Ind;

    public DbsGroup getPnd_Twrl5009() { return pnd_Twrl5009; }

    public DbsField getPnd_Twrl5009_Pnd_Res_Max() { return pnd_Twrl5009_Pnd_Res_Max; }

    public DbsGroup getPnd_Twrl5009_Pnd_Country_Table() { return pnd_Twrl5009_Pnd_Country_Table; }

    public DbsField getPnd_Twrl5009_Pnd_Country_Code() { return pnd_Twrl5009_Pnd_Country_Code; }

    public DbsField getPnd_Twrl5009_Pnd_Nr4_Country() { return pnd_Twrl5009_Pnd_Nr4_Country; }

    public DbsField getPnd_Twrl5009_Pnd_Nr4_Error() { return pnd_Twrl5009_Pnd_Nr4_Error; }

    public DbsField getPnd_Twrl5009_Pnd_Treaty_Ind() { return pnd_Twrl5009_Pnd_Treaty_Ind; }

    public DbsField getPnd_Twrl5009_Pnd_Pp_Per() { return pnd_Twrl5009_Pnd_Pp_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Np_Per() { return pnd_Twrl5009_Pnd_Np_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Dpi_Per() { return pnd_Twrl5009_Pnd_Dpi_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Pa_Pp_Per() { return pnd_Twrl5009_Pnd_Pa_Pp_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Pa_Np_Per() { return pnd_Twrl5009_Pnd_Pa_Np_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Ira_Pp_Per() { return pnd_Twrl5009_Pnd_Ira_Pp_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Ira_Np_Per() { return pnd_Twrl5009_Pnd_Ira_Np_Per; }

    public DbsField getPnd_Twrl5009_Pnd_Early_Distrib_Ind() { return pnd_Twrl5009_Pnd_Early_Distrib_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl5009 = newGroupInRecord("pnd_Twrl5009", "#TWRL5009");
        pnd_Twrl5009_Pnd_Res_Max = pnd_Twrl5009.newFieldInGroup("pnd_Twrl5009_Pnd_Res_Max", "#RES-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Twrl5009_Pnd_Country_Table = pnd_Twrl5009.newGroupArrayInGroup("pnd_Twrl5009_Pnd_Country_Table", "#COUNTRY-TABLE", new DbsArrayController(1,
            150));
        pnd_Twrl5009_Pnd_Country_Code = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Country_Code", "#COUNTRY-CODE", FieldType.STRING, 
            3);
        pnd_Twrl5009_Pnd_Nr4_Country = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Nr4_Country", "#NR4-COUNTRY", FieldType.STRING, 
            3);
        pnd_Twrl5009_Pnd_Nr4_Error = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Nr4_Error", "#NR4-ERROR", FieldType.BOOLEAN);
        pnd_Twrl5009_Pnd_Treaty_Ind = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Treaty_Ind", "#TREATY-IND", FieldType.BOOLEAN);
        pnd_Twrl5009_Pnd_Pp_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Pp_Per", "#PP-PER", FieldType.PACKED_DECIMAL, 5,2);
        pnd_Twrl5009_Pnd_Np_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Np_Per", "#NP-PER", FieldType.PACKED_DECIMAL, 5,2);
        pnd_Twrl5009_Pnd_Dpi_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Dpi_Per", "#DPI-PER", FieldType.PACKED_DECIMAL, 5,2);
        pnd_Twrl5009_Pnd_Pa_Pp_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Pa_Pp_Per", "#PA-PP-PER", FieldType.PACKED_DECIMAL, 
            5,2);
        pnd_Twrl5009_Pnd_Pa_Np_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Pa_Np_Per", "#PA-NP-PER", FieldType.PACKED_DECIMAL, 
            5,2);
        pnd_Twrl5009_Pnd_Ira_Pp_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Ira_Pp_Per", "#IRA-PP-PER", FieldType.PACKED_DECIMAL, 
            5,2);
        pnd_Twrl5009_Pnd_Ira_Np_Per = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Ira_Np_Per", "#IRA-NP-PER", FieldType.PACKED_DECIMAL, 
            5,2);
        pnd_Twrl5009_Pnd_Early_Distrib_Ind = pnd_Twrl5009_Pnd_Country_Table.newFieldInGroup("pnd_Twrl5009_Pnd_Early_Distrib_Ind", "#EARLY-DISTRIB-IND", 
            FieldType.BOOLEAN);

        this.setRecordName("LdaTwrl5009");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Twrl5009_Pnd_Res_Max.setInitialValue(150);
    }

    // Constructor
    public LdaTwrl5009() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
