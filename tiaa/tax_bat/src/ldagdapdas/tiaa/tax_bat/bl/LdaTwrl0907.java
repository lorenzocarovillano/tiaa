/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:14 PM
**        * FROM NATURAL LDA     : TWRL0907
************************************************************
**        * FILE NAME            : LdaTwrl0907.java
**        * CLASS NAME           : LdaTwrl0907
**        * INSTANCE NAME        : LdaTwrl0907
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl0907 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Tx_Dw_Distr;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Tax_Yr;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep01;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Company_Cd;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep02;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Company_Short_Nm;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep03;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Tin;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep04;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Tin_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep05;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Tin_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep06;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Contract;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep07;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payee;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep08;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Cat;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep09;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Short_Nm;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep10;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep11;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Dt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep12;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Distrib_Cd;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep13;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep14;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Settle_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep15;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Type_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep16;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Residency_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep17;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Residency_Type_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep18;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Residency_Cd;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep19;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Residency_Nm;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep20;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Locality_Cd;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep21;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Citizenship_Cd;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep22;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Citizenship_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep23;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Tax_Citizenship;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep24;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Srce_Orig;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep25;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Srce_Updt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep26;
    private DbsField pnd_Tx_Dw_Distr_Pnd_W8_Ben_1001;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep27;
    private DbsField pnd_Tx_Dw_Distr_Pnd_W9_1078;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep28;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Omni_Plan;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep29;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep30;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Payment_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep31;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Taxable_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep32;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Ivc_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep33;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Ivc_Ind;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep34;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Interest_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep35;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep36;
    private DbsField pnd_Tx_Dw_Distr_Pnd_State_Wh_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep37;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep38;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep39;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep40;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Updt_Dt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep41;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Updt_User;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep42;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Intfce_Dt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep43;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Origin_Area;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep44;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Origin_Area_Short_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep45;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Origin_Area_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep46;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Error_Reason_Code;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep47;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Error_Reason_Desc;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep48;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Name;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep49;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep50;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type_Name;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep51;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_State;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep52;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep53;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Name;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep54;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep55;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep56;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan_Name;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep57;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Hybrid_Plan_Ind;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep58;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep59;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep60;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Sep61;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Contract_Type;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Ivc_Protect;
    private DbsGroup pnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Ivc_Protect_A;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Status;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind;
    private DbsField pnd_Tx_Dw_Distr_Pnd_Filler;

    public DbsGroup getPnd_Tx_Dw_Distr() { return pnd_Tx_Dw_Distr; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Tax_Yr() { return pnd_Tx_Dw_Distr_Pnd_Tax_Yr; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep01() { return pnd_Tx_Dw_Distr_Pnd_Sep01; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Company_Cd() { return pnd_Tx_Dw_Distr_Pnd_Company_Cd; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep02() { return pnd_Tx_Dw_Distr_Pnd_Sep02; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Company_Short_Nm() { return pnd_Tx_Dw_Distr_Pnd_Company_Short_Nm; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep03() { return pnd_Tx_Dw_Distr_Pnd_Sep03; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Tin() { return pnd_Tx_Dw_Distr_Pnd_Tin; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep04() { return pnd_Tx_Dw_Distr_Pnd_Sep04; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Tin_Type() { return pnd_Tx_Dw_Distr_Pnd_Tin_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep05() { return pnd_Tx_Dw_Distr_Pnd_Sep05; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Tin_Desc() { return pnd_Tx_Dw_Distr_Pnd_Tin_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep06() { return pnd_Tx_Dw_Distr_Pnd_Sep06; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Contract() { return pnd_Tx_Dw_Distr_Pnd_Contract; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep07() { return pnd_Tx_Dw_Distr_Pnd_Sep07; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payee() { return pnd_Tx_Dw_Distr_Pnd_Payee; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep08() { return pnd_Tx_Dw_Distr_Pnd_Sep08; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Cat() { return pnd_Tx_Dw_Distr_Pnd_Payment_Cat; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep09() { return pnd_Tx_Dw_Distr_Pnd_Sep09; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Cat_Short_Nm() { return pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Short_Nm; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep10() { return pnd_Tx_Dw_Distr_Pnd_Sep10; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Cat_Desc() { return pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep11() { return pnd_Tx_Dw_Distr_Pnd_Sep11; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Dt() { return pnd_Tx_Dw_Distr_Pnd_Payment_Dt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep12() { return pnd_Tx_Dw_Distr_Pnd_Sep12; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Distrib_Cd() { return pnd_Tx_Dw_Distr_Pnd_Distrib_Cd; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep13() { return pnd_Tx_Dw_Distr_Pnd_Sep13; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Type() { return pnd_Tx_Dw_Distr_Pnd_Payment_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep14() { return pnd_Tx_Dw_Distr_Pnd_Sep14; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Settle_Type() { return pnd_Tx_Dw_Distr_Pnd_Settle_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep15() { return pnd_Tx_Dw_Distr_Pnd_Sep15; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Type_Desc() { return pnd_Tx_Dw_Distr_Pnd_Payment_Type_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep16() { return pnd_Tx_Dw_Distr_Pnd_Sep16; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Residency_Type() { return pnd_Tx_Dw_Distr_Pnd_Residency_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep17() { return pnd_Tx_Dw_Distr_Pnd_Sep17; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Residency_Type_Desc() { return pnd_Tx_Dw_Distr_Pnd_Residency_Type_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep18() { return pnd_Tx_Dw_Distr_Pnd_Sep18; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Residency_Cd() { return pnd_Tx_Dw_Distr_Pnd_Residency_Cd; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep19() { return pnd_Tx_Dw_Distr_Pnd_Sep19; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Residency_Nm() { return pnd_Tx_Dw_Distr_Pnd_Residency_Nm; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep20() { return pnd_Tx_Dw_Distr_Pnd_Sep20; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Locality_Cd() { return pnd_Tx_Dw_Distr_Pnd_Locality_Cd; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep21() { return pnd_Tx_Dw_Distr_Pnd_Sep21; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Citizenship_Cd() { return pnd_Tx_Dw_Distr_Pnd_Citizenship_Cd; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep22() { return pnd_Tx_Dw_Distr_Pnd_Sep22; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Citizenship_Desc() { return pnd_Tx_Dw_Distr_Pnd_Citizenship_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep23() { return pnd_Tx_Dw_Distr_Pnd_Sep23; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Tax_Citizenship() { return pnd_Tx_Dw_Distr_Pnd_Tax_Citizenship; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep24() { return pnd_Tx_Dw_Distr_Pnd_Sep24; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Srce_Orig() { return pnd_Tx_Dw_Distr_Pnd_Srce_Orig; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep25() { return pnd_Tx_Dw_Distr_Pnd_Sep25; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Srce_Updt() { return pnd_Tx_Dw_Distr_Pnd_Srce_Updt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep26() { return pnd_Tx_Dw_Distr_Pnd_Sep26; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_W8_Ben_1001() { return pnd_Tx_Dw_Distr_Pnd_W8_Ben_1001; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep27() { return pnd_Tx_Dw_Distr_Pnd_Sep27; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_W9_1078() { return pnd_Tx_Dw_Distr_Pnd_W9_1078; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep28() { return pnd_Tx_Dw_Distr_Pnd_Sep28; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Omni_Plan() { return pnd_Tx_Dw_Distr_Pnd_Omni_Plan; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep29() { return pnd_Tx_Dw_Distr_Pnd_Sep29; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan() { return pnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep30() { return pnd_Tx_Dw_Distr_Pnd_Sep30; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Payment_Amt() { return pnd_Tx_Dw_Distr_Pnd_Payment_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep31() { return pnd_Tx_Dw_Distr_Pnd_Sep31; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Taxable_Amt() { return pnd_Tx_Dw_Distr_Pnd_Taxable_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep32() { return pnd_Tx_Dw_Distr_Pnd_Sep32; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Ivc_Amt() { return pnd_Tx_Dw_Distr_Pnd_Ivc_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep33() { return pnd_Tx_Dw_Distr_Pnd_Sep33; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Ivc_Ind() { return pnd_Tx_Dw_Distr_Pnd_Ivc_Ind; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep34() { return pnd_Tx_Dw_Distr_Pnd_Sep34; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Interest_Amt() { return pnd_Tx_Dw_Distr_Pnd_Interest_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep35() { return pnd_Tx_Dw_Distr_Pnd_Sep35; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt() { return pnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep36() { return pnd_Tx_Dw_Distr_Pnd_Sep36; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_State_Wh_Amt() { return pnd_Tx_Dw_Distr_Pnd_State_Wh_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep37() { return pnd_Tx_Dw_Distr_Pnd_Sep37; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt() { return pnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep38() { return pnd_Tx_Dw_Distr_Pnd_Sep38; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt() { return pnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep39() { return pnd_Tx_Dw_Distr_Pnd_Sep39; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt() { return pnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep40() { return pnd_Tx_Dw_Distr_Pnd_Sep40; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Updt_Dt() { return pnd_Tx_Dw_Distr_Pnd_Updt_Dt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep41() { return pnd_Tx_Dw_Distr_Pnd_Sep41; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Updt_User() { return pnd_Tx_Dw_Distr_Pnd_Updt_User; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep42() { return pnd_Tx_Dw_Distr_Pnd_Sep42; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Intfce_Dt() { return pnd_Tx_Dw_Distr_Pnd_Intfce_Dt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep43() { return pnd_Tx_Dw_Distr_Pnd_Sep43; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Origin_Area() { return pnd_Tx_Dw_Distr_Pnd_Origin_Area; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep44() { return pnd_Tx_Dw_Distr_Pnd_Sep44; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Origin_Area_Short_Desc() { return pnd_Tx_Dw_Distr_Pnd_Origin_Area_Short_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep45() { return pnd_Tx_Dw_Distr_Pnd_Sep45; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Origin_Area_Desc() { return pnd_Tx_Dw_Distr_Pnd_Origin_Area_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep46() { return pnd_Tx_Dw_Distr_Pnd_Sep46; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Error_Reason_Code() { return pnd_Tx_Dw_Distr_Pnd_Error_Reason_Code; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep47() { return pnd_Tx_Dw_Distr_Pnd_Sep47; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Error_Reason_Desc() { return pnd_Tx_Dw_Distr_Pnd_Error_Reason_Desc; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep48() { return pnd_Tx_Dw_Distr_Pnd_Sep48; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Name() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Name; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep49() { return pnd_Tx_Dw_Distr_Pnd_Sep49; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep50() { return pnd_Tx_Dw_Distr_Pnd_Sep50; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type_Name() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type_Name; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep51() { return pnd_Tx_Dw_Distr_Pnd_Sep51; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_State() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_State; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep52() { return pnd_Tx_Dw_Distr_Pnd_Sep52; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep53() { return pnd_Tx_Dw_Distr_Pnd_Sep53; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Name() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Name; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep54() { return pnd_Tx_Dw_Distr_Pnd_Sep54; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep55() { return pnd_Tx_Dw_Distr_Pnd_Sep55; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep56() { return pnd_Tx_Dw_Distr_Pnd_Sep56; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan_Name() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan_Name; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep57() { return pnd_Tx_Dw_Distr_Pnd_Sep57; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Hybrid_Plan_Ind() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Hybrid_Plan_Ind; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep58() { return pnd_Tx_Dw_Distr_Pnd_Sep58; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep59() { return pnd_Tx_Dw_Distr_Pnd_Sep59; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep60() { return pnd_Tx_Dw_Distr_Pnd_Sep60; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Sep61() { return pnd_Tx_Dw_Distr_Pnd_Sep61; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Contract_Type() { return pnd_Tx_Dw_Distr_Pnd_Contract_Type; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Ivc_Protect() { return pnd_Tx_Dw_Distr_Pnd_Ivc_Protect; }

    public DbsGroup getPnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1() { return pnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Ivc_Protect_A() { return pnd_Tx_Dw_Distr_Pnd_Ivc_Protect_A; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Status() { return pnd_Tx_Dw_Distr_Pnd_Status; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind() { return pnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind() { return pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind; }

    public DbsField getPnd_Tx_Dw_Distr_Pnd_Filler() { return pnd_Tx_Dw_Distr_Pnd_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Tx_Dw_Distr = newGroupInRecord("pnd_Tx_Dw_Distr", "#TX-DW-DISTR");
        pnd_Tx_Dw_Distr_Pnd_Tax_Yr = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Tax_Yr", "#TAX-YR", FieldType.STRING, 4);
        pnd_Tx_Dw_Distr_Pnd_Sep01 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep01", "#SEP01", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Company_Cd = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Company_Cd", "#COMPANY-CD", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep02 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep02", "#SEP02", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Company_Short_Nm = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Company_Short_Nm", "#COMPANY-SHORT-NM", FieldType.STRING, 
            4);
        pnd_Tx_Dw_Distr_Pnd_Sep03 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep03", "#SEP03", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Tin = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Tin", "#TIN", FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep04 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep04", "#SEP04", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Tin_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Tin_Type", "#TIN-TYPE", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep05 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep05", "#SEP05", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Tin_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Tin_Desc", "#TIN-DESC", FieldType.STRING, 7);
        pnd_Tx_Dw_Distr_Pnd_Sep06 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep06", "#SEP06", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Contract = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep07 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep07", "#SEP07", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payee = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payee", "#PAYEE", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep08 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep08", "#SEP08", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Cat = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Cat", "#PAYMENT-CAT", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep09 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep09", "#SEP09", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Short_Nm = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Short_Nm", "#PAYMENT-CAT-SHORT-NM", 
            FieldType.STRING, 3);
        pnd_Tx_Dw_Distr_Pnd_Sep10 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep10", "#SEP10", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Cat_Desc", "#PAYMENT-CAT-DESC", FieldType.STRING, 
            8);
        pnd_Tx_Dw_Distr_Pnd_Sep11 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep11", "#SEP11", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Dt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Dt", "#PAYMENT-DT", FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep12 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep12", "#SEP12", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Distrib_Cd = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Distrib_Cd", "#DISTRIB-CD", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep13 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep13", "#SEP13", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Type", "#PAYMENT-TYPE", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep14 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep14", "#SEP14", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Settle_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Settle_Type", "#SETTLE-TYPE", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep15 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep15", "#SEP15", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Type_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Type_Desc", "#PAYMENT-TYPE-DESC", FieldType.STRING, 
            40);
        pnd_Tx_Dw_Distr_Pnd_Sep16 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep16", "#SEP16", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Residency_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Residency_Type", "#RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Tx_Dw_Distr_Pnd_Sep17 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep17", "#SEP17", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Residency_Type_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Residency_Type_Desc", "#RESIDENCY-TYPE-DESC", FieldType.STRING, 
            11);
        pnd_Tx_Dw_Distr_Pnd_Sep18 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep18", "#SEP18", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Residency_Cd = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Residency_Cd", "#RESIDENCY-CD", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep19 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep19", "#SEP19", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Residency_Nm = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Residency_Nm", "#RESIDENCY-NM", FieldType.STRING, 35);
        pnd_Tx_Dw_Distr_Pnd_Sep20 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep20", "#SEP20", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Locality_Cd = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Locality_Cd", "#LOCALITY-CD", FieldType.STRING, 3);
        pnd_Tx_Dw_Distr_Pnd_Sep21 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep21", "#SEP21", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Citizenship_Cd = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Citizenship_Cd", "#CITIZENSHIP-CD", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Distr_Pnd_Sep22 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep22", "#SEP22", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Citizenship_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Citizenship_Desc", "#CITIZENSHIP-DESC", FieldType.STRING, 
            7);
        pnd_Tx_Dw_Distr_Pnd_Sep23 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep23", "#SEP23", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Tax_Citizenship = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Tax_Citizenship", "#TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Tx_Dw_Distr_Pnd_Sep24 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep24", "#SEP24", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Srce_Orig = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Srce_Orig", "#SRCE-ORIG", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep25 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep25", "#SEP25", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Srce_Updt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Srce_Updt", "#SRCE-UPDT", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep26 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep26", "#SEP26", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_W8_Ben_1001 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_W8_Ben_1001", "#W8-BEN-1001", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep27 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep27", "#SEP27", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_W9_1078 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_W9_1078", "#W9-1078", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep28 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep28", "#SEP28", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Omni_Plan = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Omni_Plan", "#OMNI-PLAN", FieldType.STRING, 6);
        pnd_Tx_Dw_Distr_Pnd_Sep29 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep29", "#SEP29", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Omni_Sub_Plan", "#OMNI-SUB-PLAN", FieldType.STRING, 3);
        pnd_Tx_Dw_Distr_Pnd_Sep30 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep30", "#SEP30", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Payment_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Payment_Amt", "#PAYMENT-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep31 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep31", "#SEP31", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Taxable_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep32 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep32", "#SEP32", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Ivc_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep33 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep33", "#SEP33", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Ivc_Ind = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Ivc_Ind", "#IVC-IND", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep34 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep34", "#SEP34", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Interest_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Interest_Amt", "#INTEREST-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep35 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep35", "#SEP35", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Fed_Wh_Amt", "#FED-WH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep36 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep36", "#SEP36", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_State_Wh_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_State_Wh_Amt", "#STATE-WH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep37 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep37", "#SEP37", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Local_Wh_Amt", "#LOCAL-WH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep38 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep38", "#SEP38", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Nra_Wh_Amt", "#NRA-WH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep39 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep39", "#SEP39", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Canada_Wh_Amt", "#CANADA-WH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Distr_Pnd_Sep40 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep40", "#SEP40", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Updt_Dt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Updt_Dt", "#UPDT-DT", FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep41 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep41", "#SEP41", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Updt_User = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Updt_User", "#UPDT-USER", FieldType.STRING, 8);
        pnd_Tx_Dw_Distr_Pnd_Sep42 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep42", "#SEP42", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Intfce_Dt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Intfce_Dt", "#INTFCE-DT", FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep43 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep43", "#SEP43", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Origin_Area = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Origin_Area", "#ORIGIN-AREA", FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Sep44 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep44", "#SEP44", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Origin_Area_Short_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Origin_Area_Short_Desc", "#ORIGIN-AREA-SHORT-DESC", 
            FieldType.STRING, 6);
        pnd_Tx_Dw_Distr_Pnd_Sep45 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep45", "#SEP45", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Origin_Area_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Origin_Area_Desc", "#ORIGIN-AREA-DESC", FieldType.STRING, 
            30);
        pnd_Tx_Dw_Distr_Pnd_Sep46 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep46", "#SEP46", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Error_Reason_Code = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Error_Reason_Code", "#ERROR-REASON-CODE", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Distr_Pnd_Sep47 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep47", "#SEP47", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Error_Reason_Desc = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Error_Reason_Desc", "#ERROR-REASON-DESC", FieldType.STRING, 
            30);
        pnd_Tx_Dw_Distr_Pnd_Sep48 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep48", "#SEP48", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Name = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Name", "#TWRPYMNT-PLAN-NAME", FieldType.STRING, 
            64);
        pnd_Tx_Dw_Distr_Pnd_Sep49 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep49", "#SEP49", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type", "#TWRPYMNT-PLAN-TYPE", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Distr_Pnd_Sep50 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep50", "#SEP50", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type_Name = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_Type_Name", "#TWRPYMNT-PLAN-TYPE-NAME", 
            FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep51 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep51", "#SEP51", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_State = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Plan_State", "#TWRPYMNT-PLAN-STATE", FieldType.STRING, 
            3);
        pnd_Tx_Dw_Distr_Pnd_Sep52 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep52", "#SEP52", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Nbr", "#TWRPYMNT-SUBPLAN-NBR", 
            FieldType.STRING, 6);
        pnd_Tx_Dw_Distr_Pnd_Sep53 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep53", "#SEP53", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Name = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Subplan_Name", "#TWRPYMNT-SUBPLAN-NAME", 
            FieldType.STRING, 32);
        pnd_Tx_Dw_Distr_Pnd_Sep54 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep54", "#SEP54", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Contract", "#TWRPYMNT-ORGNTNG-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Tx_Dw_Distr_Pnd_Sep55 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep55", "#SEP55", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan", "#TWRPYMNT-ORGNTNG-SUBPLAN", 
            FieldType.STRING, 6);
        pnd_Tx_Dw_Distr_Pnd_Sep56 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep56", "#SEP56", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan_Name = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Orgntng_Subplan_Name", "#TWRPYMNT-ORGNTNG-SUBPLAN-NAME", 
            FieldType.STRING, 32);
        pnd_Tx_Dw_Distr_Pnd_Sep57 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep57", "#SEP57", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Hybrid_Plan_Ind = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Hybrid_Plan_Ind", "#TWRPYMNT-HYBRID-PLAN-IND", 
            FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Sep58 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep58", "#SEP58", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Irr_Amt", "#TWRPYMNT-IRR-AMT", FieldType.STRING, 
            15);
        pnd_Tx_Dw_Distr_Pnd_Sep59 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep59", "#SEP59", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Giin", "#TWRPYMNT-GIIN", FieldType.STRING, 19);
        pnd_Tx_Dw_Distr_Pnd_Sep60 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep60", "#SEP60", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Tax_Rate", "#TWRPYMNT-CHPTR4-TAX-RATE", 
            FieldType.STRING, 3);
        pnd_Tx_Dw_Distr_Pnd_Sep61 = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Sep61", "#SEP61", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Chptr4_Exempt_Cde", "#TWRPYMNT-CHPTR4-EXEMPT-CDE", 
            FieldType.STRING, 2);
        pnd_Tx_Dw_Distr_Pnd_Contract_Type = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 5);
        pnd_Tx_Dw_Distr_Pnd_Ivc_Protect = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Ivc_Protect", "#IVC-PROTECT", FieldType.BOOLEAN);
        pnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1 = pnd_Tx_Dw_Distr.newGroupInGroup("pnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1", "Redefines", pnd_Tx_Dw_Distr_Pnd_Ivc_Protect);
        pnd_Tx_Dw_Distr_Pnd_Ivc_Protect_A = pnd_Tx_Dw_Distr_Pnd_Ivc_ProtectRedef1.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Ivc_Protect_A", "#IVC-PROTECT-A", 
            FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Status = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Status", "#STATUS", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Roth_Qual_Ind", "#ROTH-QUAL-IND", FieldType.STRING, 1);
        pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Twrpymnt_Fatca_Ind", "#TWRPYMNT-FATCA-IND", FieldType.STRING, 
            1);
        pnd_Tx_Dw_Distr_Pnd_Filler = pnd_Tx_Dw_Distr.newFieldInGroup("pnd_Tx_Dw_Distr_Pnd_Filler", "#FILLER", FieldType.STRING, 39);

        this.setRecordName("LdaTwrl0907");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Tx_Dw_Distr_Pnd_Sep01.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep02.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep03.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep04.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep05.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep06.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep07.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep08.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep09.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep10.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep11.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep12.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep13.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep14.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep15.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep16.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep17.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep18.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep19.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep20.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep21.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep22.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep23.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep24.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep25.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep26.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep27.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep28.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep29.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep30.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep31.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep32.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep33.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep34.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep35.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep36.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep37.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep38.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep39.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep40.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep41.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep42.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep43.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep44.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep45.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep46.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep47.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep48.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep49.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep50.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep51.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep52.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep53.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep54.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep55.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep56.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep57.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep58.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep59.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep60.setInitialValue(";");
        pnd_Tx_Dw_Distr_Pnd_Sep61.setInitialValue(";");
    }

    // Constructor
    public LdaTwrl0907() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
