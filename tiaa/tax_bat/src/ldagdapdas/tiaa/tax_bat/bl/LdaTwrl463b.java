/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:52 PM
**        * FROM NATURAL LDA     : TWRL463B
************************************************************
**        * FILE NAME            : LdaTwrl463b.java
**        * CLASS NAME           : LdaTwrl463b
**        * INSTANCE NAME        : LdaTwrl463b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl463b extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cntl;
    private DbsField cntl_Tircntl_Tbl_Nbr;
    private DbsField cntl_Tircntl_Tax_Year;
    private DbsField cntl_Tircntl_Seq_Nbr;
    private DbsGroup cntl_Tircntl_Feeder_Sys_Tbl;
    private DbsField cntl_Tircntl_Rpt_Source_Code;
    private DbsField cntl_Tircntl_Company_Cde;
    private DbsField cntl_Tircntl_Frm_Intrfce_Dte;
    private DbsField cntl_Tircntl_To_Intrfce_Dte;
    private DbsField cntl_Tircntl_Input_Recs_Tot;
    private DbsField cntl_Tircntl_Input_Gross_Amt;
    private DbsField cntl_Tircntl_Input_Ivt_Amt;
    private DbsField cntl_Tircntl_Input_Int_Amt;
    private DbsField cntl_Tircntl_Input_Nra_Amt;
    private DbsField cntl_Tircntl_Input_Can_Amt;
    private DbsField cntl_Tircntl_Input_Fed_Wthldg_Amt;
    private DbsField cntl_Tircntl_Input_State_Wthldg_Amt;
    private DbsField cntl_Tircntl_Input_Local_Wthldg_Amt;
    private DbsField cntl_Tircntl_Rpt_Source_Code_Desc;
    private DbsField cntl_Tircntl_Rpt_Update_Dte_Time;

    public DataAccessProgramView getVw_cntl() { return vw_cntl; }

    public DbsField getCntl_Tircntl_Tbl_Nbr() { return cntl_Tircntl_Tbl_Nbr; }

    public DbsField getCntl_Tircntl_Tax_Year() { return cntl_Tircntl_Tax_Year; }

    public DbsField getCntl_Tircntl_Seq_Nbr() { return cntl_Tircntl_Seq_Nbr; }

    public DbsGroup getCntl_Tircntl_Feeder_Sys_Tbl() { return cntl_Tircntl_Feeder_Sys_Tbl; }

    public DbsField getCntl_Tircntl_Rpt_Source_Code() { return cntl_Tircntl_Rpt_Source_Code; }

    public DbsField getCntl_Tircntl_Company_Cde() { return cntl_Tircntl_Company_Cde; }

    public DbsField getCntl_Tircntl_Frm_Intrfce_Dte() { return cntl_Tircntl_Frm_Intrfce_Dte; }

    public DbsField getCntl_Tircntl_To_Intrfce_Dte() { return cntl_Tircntl_To_Intrfce_Dte; }

    public DbsField getCntl_Tircntl_Input_Recs_Tot() { return cntl_Tircntl_Input_Recs_Tot; }

    public DbsField getCntl_Tircntl_Input_Gross_Amt() { return cntl_Tircntl_Input_Gross_Amt; }

    public DbsField getCntl_Tircntl_Input_Ivt_Amt() { return cntl_Tircntl_Input_Ivt_Amt; }

    public DbsField getCntl_Tircntl_Input_Int_Amt() { return cntl_Tircntl_Input_Int_Amt; }

    public DbsField getCntl_Tircntl_Input_Nra_Amt() { return cntl_Tircntl_Input_Nra_Amt; }

    public DbsField getCntl_Tircntl_Input_Can_Amt() { return cntl_Tircntl_Input_Can_Amt; }

    public DbsField getCntl_Tircntl_Input_Fed_Wthldg_Amt() { return cntl_Tircntl_Input_Fed_Wthldg_Amt; }

    public DbsField getCntl_Tircntl_Input_State_Wthldg_Amt() { return cntl_Tircntl_Input_State_Wthldg_Amt; }

    public DbsField getCntl_Tircntl_Input_Local_Wthldg_Amt() { return cntl_Tircntl_Input_Local_Wthldg_Amt; }

    public DbsField getCntl_Tircntl_Rpt_Source_Code_Desc() { return cntl_Tircntl_Rpt_Source_Code_Desc; }

    public DbsField getCntl_Tircntl_Rpt_Update_Dte_Time() { return cntl_Tircntl_Rpt_Update_Dte_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cntl = new DataAccessProgramView(new NameInfo("vw_cntl", "CNTL"), "TIRCNTL_FEEDER_SYS_TBL_VIEW", "TIR_CONTROL");
        cntl_Tircntl_Tbl_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tbl_Nbr", "TIRCNTL-TBL-NBR", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TIRCNTL_TBL_NBR");
        cntl_Tircntl_Tax_Year = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Tax_Year", "TIRCNTL-TAX-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "TIRCNTL_TAX_YEAR");
        cntl_Tircntl_Seq_Nbr = vw_cntl.getRecord().newFieldInGroup("cntl_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TIRCNTL_SEQ_NBR");
        cntl_Tircntl_Feeder_Sys_Tbl = vw_cntl.getRecord().newGroupInGroup("cntl_Tircntl_Feeder_Sys_Tbl", "TIRCNTL-FEEDER-SYS-TBL");
        cntl_Tircntl_Rpt_Source_Code = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Rpt_Source_Code", "TIRCNTL-RPT-SOURCE-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE");
        cntl_Tircntl_Company_Cde = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Company_Cde", "TIRCNTL-COMPANY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TIRCNTL_COMPANY_CDE");
        cntl_Tircntl_Frm_Intrfce_Dte = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Frm_Intrfce_Dte", "TIRCNTL-FRM-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_FRM_INTRFCE_DTE");
        cntl_Tircntl_To_Intrfce_Dte = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_To_Intrfce_Dte", "TIRCNTL-TO-INTRFCE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TIRCNTL_TO_INTRFCE_DTE");
        cntl_Tircntl_Input_Recs_Tot = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Recs_Tot", "TIRCNTL-INPUT-RECS-TOT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_RECS_TOT");
        cntl_Tircntl_Input_Gross_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Gross_Amt", "TIRCNTL-INPUT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_GROSS_AMT");
        cntl_Tircntl_Input_Ivt_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Ivt_Amt", "TIRCNTL-INPUT-IVT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_IVT_AMT");
        cntl_Tircntl_Input_Int_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Int_Amt", "TIRCNTL-INPUT-INT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_INT_AMT");
        cntl_Tircntl_Input_Nra_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Nra_Amt", "TIRCNTL-INPUT-NRA-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_NRA_AMT");
        cntl_Tircntl_Input_Can_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Can_Amt", "TIRCNTL-INPUT-CAN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_CAN_AMT");
        cntl_Tircntl_Input_Fed_Wthldg_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Fed_Wthldg_Amt", "TIRCNTL-INPUT-FED-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_FED_WTHLDG_AMT");
        cntl_Tircntl_Input_State_Wthldg_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_State_Wthldg_Amt", "TIRCNTL-INPUT-STATE-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_STATE_WTHLDG_AMT");
        cntl_Tircntl_Input_Local_Wthldg_Amt = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Input_Local_Wthldg_Amt", "TIRCNTL-INPUT-LOCAL-WTHLDG-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "TIRCNTL_INPUT_LOCAL_WTHLDG_AMT");
        cntl_Tircntl_Rpt_Source_Code_Desc = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Rpt_Source_Code_Desc", "TIRCNTL-RPT-SOURCE-CODE-DESC", 
            FieldType.STRING, 23, RepeatingFieldStrategy.None, "TIRCNTL_RPT_SOURCE_CODE_DESC");
        cntl_Tircntl_Rpt_Update_Dte_Time = cntl_Tircntl_Feeder_Sys_Tbl.newFieldInGroup("cntl_Tircntl_Rpt_Update_Dte_Time", "TIRCNTL-RPT-UPDATE-DTE-TIME", 
            FieldType.NUMERIC, 15, RepeatingFieldStrategy.None, "TIRCNTL_RPT_UPDATE_DTE_TIME");

        this.setRecordName("LdaTwrl463b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cntl.reset();
    }

    // Constructor
    public LdaTwrl463b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
