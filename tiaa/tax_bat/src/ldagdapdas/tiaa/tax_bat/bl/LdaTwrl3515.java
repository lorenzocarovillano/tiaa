/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:34 PM
**        * FROM NATURAL LDA     : TWRL3515
************************************************************
**        * FILE NAME            : LdaTwrl3515.java
**        * CLASS NAME           : LdaTwrl3515
**        * INSTANCE NAME        : LdaTwrl3515
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3515 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Out_Ny_Record;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date;
    private DbsGroup pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Mm;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Dd;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Yy;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Id_Num;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Trans_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Address;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_City;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_State;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Zip;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank;
    private DbsGroup pnd_Out_Ny_RecordRedef2;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Report_Quarter;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Trans_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Address;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_City;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_State;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Zip;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Ret_Type;
    private DbsField pnd_Out_Ny_Record_Pnd_Oute_Ny_Emp_Ind;
    private DbsGroup pnd_Out_Ny_RecordRedef3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross;
    private DbsGroup pnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_A;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5;
    private DbsGroup pnd_Out_Ny_RecordRedef5;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_1w;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Gross;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Taxable_Amt;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Withhld;
    private DbsField pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank3;
    private DbsGroup pnd_Out_Ny_RecordRedef6;
    private DbsField pnd_Out_Ny_Record_Pnd_Outf_Ny_Id;
    private DbsField pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1e;
    private DbsField pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1w;
    private DbsField pnd_Out_Ny_Record_Pnd_Outf_Ny_Blank;
    private DbsGroup pnd_Out_Ny_RecordRedef7;
    private DbsField pnd_Out_Ny_Record_Pnd_Out_Ny_Rec1;

    public DbsGroup getPnd_Out_Ny_Record() { return pnd_Out_Ny_Record; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Id() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Id; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date; }

    public DbsGroup getPnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Mm() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Mm; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Dd() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Dd; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Yy() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Yy; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Id_Num() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Id_Num; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Trans_Name() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Trans_Name; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Address() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Address; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_City() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_City; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_State() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_State; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Zip() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Zip; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Blank() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank; }

    public DbsGroup getPnd_Out_Ny_RecordRedef2() { return pnd_Out_Ny_RecordRedef2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Id() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Id; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Report_Quarter() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Report_Quarter; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Tin() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Tin; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Blank1() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Trans_Name() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Trans_Name; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Blank2() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Address() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Address; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_City() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_City; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_State() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_State; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Zip() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Zip; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Blank() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Ret_Type() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Ret_Type; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Oute_Ny_Emp_Ind() { return pnd_Out_Ny_Record_Pnd_Oute_Ny_Emp_Ind; }

    public DbsGroup getPnd_Out_Ny_RecordRedef3() { return pnd_Out_Ny_RecordRedef3; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Id() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Id; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Name() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Name; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross; }

    public DbsGroup getPnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_A() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_A; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5; }

    public DbsGroup getPnd_Out_Ny_RecordRedef5() { return pnd_Out_Ny_RecordRedef5; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Id() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Id; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Total_1w() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_1w; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Blank() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Gross() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Gross; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Blank1() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Taxable_Amt() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Taxable_Amt; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Blank2() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Withhld() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Withhld; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outt_Ny_Blank3() { return pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank3; }

    public DbsGroup getPnd_Out_Ny_RecordRedef6() { return pnd_Out_Ny_RecordRedef6; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outf_Ny_Id() { return pnd_Out_Ny_Record_Pnd_Outf_Ny_Id; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1e() { return pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1e; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1w() { return pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1w; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outf_Ny_Blank() { return pnd_Out_Ny_Record_Pnd_Outf_Ny_Blank; }

    public DbsGroup getPnd_Out_Ny_RecordRedef7() { return pnd_Out_Ny_RecordRedef7; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Out_Ny_Rec1() { return pnd_Out_Ny_Record_Pnd_Out_Ny_Rec1; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out_Ny_Record = newGroupInRecord("pnd_Out_Ny_Record", "#OUT-NY-RECORD");
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Id = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Id", "#OUTA-NY-ID", FieldType.STRING, 2);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date", "#OUTA-NY-CREATE-DATE", 
            FieldType.STRING, 6);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1 = pnd_Out_Ny_Record.newGroupInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1", "Redefines", 
            pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Mm = pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Mm", 
            "#OUTA-NY-CREATE-DATE-MM", FieldType.STRING, 2);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Dd = pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Dd", 
            "#OUTA-NY-CREATE-DATE-DD", FieldType.STRING, 2);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Yy = pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_DateRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Create_Date_Yy", 
            "#OUTA-NY-CREATE-DATE-YY", FieldType.STRING, 2);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Id_Num = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Id_Num", "#OUTA-NY-ID-NUM", FieldType.STRING, 
            11);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Trans_Name = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Trans_Name", "#OUTA-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Address = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Address", "#OUTA-NY-ADDRESS", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_City = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_City", "#OUTA-NY-CITY", FieldType.STRING, 
            25);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_State = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_State", "#OUTA-NY-STATE", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Zip = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Zip", "#OUTA-NY-ZIP", FieldType.STRING, 9);
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank", "#OUTA-NY-BLANK", FieldType.STRING, 
            3);
        pnd_Out_Ny_RecordRedef2 = newGroupInRecord("pnd_Out_Ny_RecordRedef2", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Id = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Id", "#OUTE-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Report_Quarter = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Report_Quarter", "#OUTE-NY-REPORT-QUARTER", 
            FieldType.STRING, 4);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Tin = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Tin", "#OUTE-NY-TIN", FieldType.STRING, 
            11);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank1 = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank1", "#OUTE-NY-BLANK1", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Trans_Name = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Trans_Name", "#OUTE-NY-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank2 = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank2", "#OUTE-NY-BLANK2", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Address = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Address", "#OUTE-NY-ADDRESS", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_City = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_City", "#OUTE-NY-CITY", FieldType.STRING, 
            25);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_State = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_State", "#OUTE-NY-STATE", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Zip = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Zip", "#OUTE-NY-ZIP", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Blank", "#OUTE-NY-BLANK", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Ret_Type = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Ret_Type", "#OUTE-NY-RET-TYPE", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Oute_Ny_Emp_Ind = pnd_Out_Ny_RecordRedef2.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Oute_Ny_Emp_Ind", "#OUTE-NY-EMP-IND", FieldType.STRING, 
            1);
        pnd_Out_Ny_RecordRedef3 = newGroupInRecord("pnd_Out_Ny_RecordRedef3", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Id = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Id", "#OUTW-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin", "#OUTW-NY-TIN", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Name = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Name", "#OUTW-NY-NAME", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1 = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank1", "#OUTW-NY-BLANK1", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Wages_Ind", "#OUTW-NY-WAGES-IND", 
            FieldType.STRING, 1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2 = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank2", "#OUTW-NY-BLANK2", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross", "#OUTW-NY-GROSS", FieldType.DECIMAL, 
            14,2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4 = pnd_Out_Ny_RecordRedef3.newGroupInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4", "Redefines", 
            pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_A = pnd_Out_Ny_Record_Pnd_Outw_Ny_GrossRedef4.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_A", "#OUTW-NY-GROSS-A", 
            FieldType.STRING, 14);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3 = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank3", "#OUTW-NY-BLANK3", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Taxable_Amt", "#OUTW-NY-TAXABLE-AMT", 
            FieldType.DECIMAL, 14,2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4 = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank4", "#OUTW-NY-BLANK4", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tax_Wthld", "#OUTW-NY-TAX-WTHLD", 
            FieldType.DECIMAL, 14,2);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5 = pnd_Out_Ny_RecordRedef3.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Blank5", "#OUTW-NY-BLANK5", FieldType.STRING, 
            40);
        pnd_Out_Ny_RecordRedef5 = newGroupInRecord("pnd_Out_Ny_RecordRedef5", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Id = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Id", "#OUTT-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_1w = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_1w", "#OUTT-NY-TOTAL-1W", 
            FieldType.NUMERIC, 7);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank", "#OUTT-NY-BLANK", FieldType.STRING, 
            35);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Gross = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Gross", "#OUTT-NY-TOTAL-GROSS", 
            FieldType.DECIMAL, 14,2);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank1 = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank1", "#OUTT-NY-BLANK1", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Taxable_Amt = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Taxable_Amt", "#OUTT-NY-TOTAL-TAXABLE-AMT", 
            FieldType.DECIMAL, 14,2);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank2 = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank2", "#OUTT-NY-BLANK2", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Withhld = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Total_Withhld", "#OUTT-NY-TOTAL-WITHHLD", 
            FieldType.DECIMAL, 14,2);
        pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank3 = pnd_Out_Ny_RecordRedef5.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outt_Ny_Blank3", "#OUTT-NY-BLANK3", FieldType.STRING, 
            40);
        pnd_Out_Ny_RecordRedef6 = newGroupInRecord("pnd_Out_Ny_RecordRedef6", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outf_Ny_Id = pnd_Out_Ny_RecordRedef6.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outf_Ny_Id", "#OUTF-NY-ID", FieldType.STRING, 
            2);
        pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1e = pnd_Out_Ny_RecordRedef6.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1e", "#OUTF-NY-TOTAL-1E", 
            FieldType.NUMERIC, 10);
        pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1w = pnd_Out_Ny_RecordRedef6.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outf_Ny_Total_1w", "#OUTF-NY-TOTAL-1W", 
            FieldType.NUMERIC, 10);
        pnd_Out_Ny_Record_Pnd_Outf_Ny_Blank = pnd_Out_Ny_RecordRedef6.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outf_Ny_Blank", "#OUTF-NY-BLANK", FieldType.STRING, 
            106);
        pnd_Out_Ny_RecordRedef7 = newGroupInRecord("pnd_Out_Ny_RecordRedef7", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Out_Ny_Rec1 = pnd_Out_Ny_RecordRedef7.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Out_Ny_Rec1", "#OUT-NY-REC1", FieldType.STRING, 
            128);

        this.setRecordName("LdaTwrl3515");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3515() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
