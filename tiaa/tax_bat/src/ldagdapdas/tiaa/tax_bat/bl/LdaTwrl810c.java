/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:19 PM
**        * FROM NATURAL LDA     : TWRL810C
************************************************************
**        * FILE NAME            : LdaTwrl810c.java
**        * CLASS NAME           : LdaTwrl810c
**        * INSTANCE NAME        : LdaTwrl810c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl810c extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_newpar;
    private DbsField newpar_Twrparti_Status;
    private DbsField newpar_Twrparti_Tax_Id_Type;
    private DbsField newpar_Twrparti_Tax_Id;
    private DbsField newpar_Twrparti_Citation_Ind;
    private DbsField newpar_Twrparti_Part_Eff_Start_Dte;
    private DbsField newpar_Twrparti_Part_Eff_End_Dte;
    private DbsField newpar_Twrparti_Part_Invrse_End_Dte;
    private DbsField newpar_Twrparti_Hold_Cde;
    private DbsField newpar_Twrparti_Test_Rec_Ind;
    private DbsField newpar_Twrparti_Citizen_Cde;
    private DbsField newpar_Twrparti_Residency_Over_Ind;
    private DbsField newpar_Twrparti_Residency_Type;
    private DbsField newpar_Twrparti_Residency_Code;
    private DbsField newpar_Twrparti_1st_Bnote_Ind;
    private DbsField newpar_Twrparti_2nd_Bnote_Ind;
    private DbsField newpar_Twrparti_1st_Sol_Ind;
    private DbsField newpar_Twrparti_2nd_Sol_Ind;
    private DbsField newpar_Twrparti_Cnote_Ind;
    private DbsField newpar_Twrparti_Frmw8_Ind;
    private DbsField newpar_Twrparti_Frmw9_Ind;
    private DbsField newpar_Twrparti_Frm1001_Ind;
    private DbsField newpar_Twrparti_Frm1078_Ind;
    private DbsField newpar_Twrparti_Updt_User;
    private DbsField newpar_Twrparti_Updt_Dte;
    private DbsField newpar_Twrparti_Updt_Time;
    private DbsField newpar_Twrparti_Lu_Ts;
    private DbsField newpar_Twrparti_Alt_Addr_Ind;
    private DbsField newpar_Twrparti_Addr_Source;
    private DbsField newpar_Twrparti_Origin;
    private DbsField newpar_Twrparti_Pin;
    private DbsField newpar_Twrparti_Participant_Name;
    private DbsField newpar_Twrparti_Addr_Ln1;
    private DbsField newpar_Twrparti_Addr_Ln2;
    private DbsField newpar_Twrparti_Addr_Ln3;
    private DbsField newpar_Twrparti_Addr_Ln4;
    private DbsField newpar_Twrparti_Addr_Ln5;
    private DbsField newpar_Twrparti_Addr_Ln6;
    private DbsField newpar_Twrparti_Participant_Dob;
    private DbsField newpar_Twrparti_Participant_Dod;
    private DbsField newpar_Twrparti_Wpid;
    private DbsField newpar_Twrparti_Locality_Cde;
    private DbsField newpar_Twrparti_1st_Bnote_Eff_Dte;
    private DbsField newpar_Twrparti_2nd_Bnote_Eff_Dte;
    private DbsField newpar_Twrparti_1st_Sol_Eff_Dte;
    private DbsField newpar_Twrparti_2nd_Sol_Eff_Dte;
    private DbsField newpar_Twrparti_Cnote_Eff_Dte;
    private DbsField newpar_Twrparti_Frmw8_Eff_Dte;
    private DbsField newpar_Twrparti_Frmw9_Eff_Dte;
    private DbsField newpar_Twrparti_Frm1001_Eff_Dte;
    private DbsField newpar_Twrparti_Frm1078_Eff_Dte;
    private DbsField newpar_Twrparti_Comment;
    private DbsField newpar_Twrparti_Country_Of_Res;
    private DbsField newpar_Twrparti_Country_Desc;
    private DbsField newpar_Twrparti_Alt_Addr_Eff_Dte;
    private DbsField newpar_Twrparti_Alt_Addr_Ln1;
    private DbsField newpar_Twrparti_Alt_Addr_Ln2;
    private DbsField newpar_Twrparti_Alt_Addr_Ln3;
    private DbsField newpar_Twrparti_Alt_Addr_Ln4;
    private DbsField newpar_Twrparti_Alt_Addr_Ln5;
    private DbsField newpar_Twrparti_Alt_Addr_Ln6;
    private DbsField newpar_Twrparti_Tran_From;
    private DbsGroup newpar_Twrparti_Tran_FromRedef1;
    private DbsField newpar_Twrparti_Tr_Fr_Tin_Type;
    private DbsField newpar_Twrparti_Tr_Fr_Tin;
    private DbsField newpar_Twrparti_Tran_To;
    private DbsGroup newpar_Twrparti_Tran_ToRedef2;
    private DbsField newpar_Twrparti_Tr_To_Tin_Type;
    private DbsField newpar_Twrparti_Tr_To_Tin;

    public DataAccessProgramView getVw_newpar() { return vw_newpar; }

    public DbsField getNewpar_Twrparti_Status() { return newpar_Twrparti_Status; }

    public DbsField getNewpar_Twrparti_Tax_Id_Type() { return newpar_Twrparti_Tax_Id_Type; }

    public DbsField getNewpar_Twrparti_Tax_Id() { return newpar_Twrparti_Tax_Id; }

    public DbsField getNewpar_Twrparti_Citation_Ind() { return newpar_Twrparti_Citation_Ind; }

    public DbsField getNewpar_Twrparti_Part_Eff_Start_Dte() { return newpar_Twrparti_Part_Eff_Start_Dte; }

    public DbsField getNewpar_Twrparti_Part_Eff_End_Dte() { return newpar_Twrparti_Part_Eff_End_Dte; }

    public DbsField getNewpar_Twrparti_Part_Invrse_End_Dte() { return newpar_Twrparti_Part_Invrse_End_Dte; }

    public DbsField getNewpar_Twrparti_Hold_Cde() { return newpar_Twrparti_Hold_Cde; }

    public DbsField getNewpar_Twrparti_Test_Rec_Ind() { return newpar_Twrparti_Test_Rec_Ind; }

    public DbsField getNewpar_Twrparti_Citizen_Cde() { return newpar_Twrparti_Citizen_Cde; }

    public DbsField getNewpar_Twrparti_Residency_Over_Ind() { return newpar_Twrparti_Residency_Over_Ind; }

    public DbsField getNewpar_Twrparti_Residency_Type() { return newpar_Twrparti_Residency_Type; }

    public DbsField getNewpar_Twrparti_Residency_Code() { return newpar_Twrparti_Residency_Code; }

    public DbsField getNewpar_Twrparti_1st_Bnote_Ind() { return newpar_Twrparti_1st_Bnote_Ind; }

    public DbsField getNewpar_Twrparti_2nd_Bnote_Ind() { return newpar_Twrparti_2nd_Bnote_Ind; }

    public DbsField getNewpar_Twrparti_1st_Sol_Ind() { return newpar_Twrparti_1st_Sol_Ind; }

    public DbsField getNewpar_Twrparti_2nd_Sol_Ind() { return newpar_Twrparti_2nd_Sol_Ind; }

    public DbsField getNewpar_Twrparti_Cnote_Ind() { return newpar_Twrparti_Cnote_Ind; }

    public DbsField getNewpar_Twrparti_Frmw8_Ind() { return newpar_Twrparti_Frmw8_Ind; }

    public DbsField getNewpar_Twrparti_Frmw9_Ind() { return newpar_Twrparti_Frmw9_Ind; }

    public DbsField getNewpar_Twrparti_Frm1001_Ind() { return newpar_Twrparti_Frm1001_Ind; }

    public DbsField getNewpar_Twrparti_Frm1078_Ind() { return newpar_Twrparti_Frm1078_Ind; }

    public DbsField getNewpar_Twrparti_Updt_User() { return newpar_Twrparti_Updt_User; }

    public DbsField getNewpar_Twrparti_Updt_Dte() { return newpar_Twrparti_Updt_Dte; }

    public DbsField getNewpar_Twrparti_Updt_Time() { return newpar_Twrparti_Updt_Time; }

    public DbsField getNewpar_Twrparti_Lu_Ts() { return newpar_Twrparti_Lu_Ts; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ind() { return newpar_Twrparti_Alt_Addr_Ind; }

    public DbsField getNewpar_Twrparti_Addr_Source() { return newpar_Twrparti_Addr_Source; }

    public DbsField getNewpar_Twrparti_Origin() { return newpar_Twrparti_Origin; }

    public DbsField getNewpar_Twrparti_Pin() { return newpar_Twrparti_Pin; }

    public DbsField getNewpar_Twrparti_Participant_Name() { return newpar_Twrparti_Participant_Name; }

    public DbsField getNewpar_Twrparti_Addr_Ln1() { return newpar_Twrparti_Addr_Ln1; }

    public DbsField getNewpar_Twrparti_Addr_Ln2() { return newpar_Twrparti_Addr_Ln2; }

    public DbsField getNewpar_Twrparti_Addr_Ln3() { return newpar_Twrparti_Addr_Ln3; }

    public DbsField getNewpar_Twrparti_Addr_Ln4() { return newpar_Twrparti_Addr_Ln4; }

    public DbsField getNewpar_Twrparti_Addr_Ln5() { return newpar_Twrparti_Addr_Ln5; }

    public DbsField getNewpar_Twrparti_Addr_Ln6() { return newpar_Twrparti_Addr_Ln6; }

    public DbsField getNewpar_Twrparti_Participant_Dob() { return newpar_Twrparti_Participant_Dob; }

    public DbsField getNewpar_Twrparti_Participant_Dod() { return newpar_Twrparti_Participant_Dod; }

    public DbsField getNewpar_Twrparti_Wpid() { return newpar_Twrparti_Wpid; }

    public DbsField getNewpar_Twrparti_Locality_Cde() { return newpar_Twrparti_Locality_Cde; }

    public DbsField getNewpar_Twrparti_1st_Bnote_Eff_Dte() { return newpar_Twrparti_1st_Bnote_Eff_Dte; }

    public DbsField getNewpar_Twrparti_2nd_Bnote_Eff_Dte() { return newpar_Twrparti_2nd_Bnote_Eff_Dte; }

    public DbsField getNewpar_Twrparti_1st_Sol_Eff_Dte() { return newpar_Twrparti_1st_Sol_Eff_Dte; }

    public DbsField getNewpar_Twrparti_2nd_Sol_Eff_Dte() { return newpar_Twrparti_2nd_Sol_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Cnote_Eff_Dte() { return newpar_Twrparti_Cnote_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Frmw8_Eff_Dte() { return newpar_Twrparti_Frmw8_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Frmw9_Eff_Dte() { return newpar_Twrparti_Frmw9_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Frm1001_Eff_Dte() { return newpar_Twrparti_Frm1001_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Frm1078_Eff_Dte() { return newpar_Twrparti_Frm1078_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Comment() { return newpar_Twrparti_Comment; }

    public DbsField getNewpar_Twrparti_Country_Of_Res() { return newpar_Twrparti_Country_Of_Res; }

    public DbsField getNewpar_Twrparti_Country_Desc() { return newpar_Twrparti_Country_Desc; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Eff_Dte() { return newpar_Twrparti_Alt_Addr_Eff_Dte; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln1() { return newpar_Twrparti_Alt_Addr_Ln1; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln2() { return newpar_Twrparti_Alt_Addr_Ln2; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln3() { return newpar_Twrparti_Alt_Addr_Ln3; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln4() { return newpar_Twrparti_Alt_Addr_Ln4; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln5() { return newpar_Twrparti_Alt_Addr_Ln5; }

    public DbsField getNewpar_Twrparti_Alt_Addr_Ln6() { return newpar_Twrparti_Alt_Addr_Ln6; }

    public DbsField getNewpar_Twrparti_Tran_From() { return newpar_Twrparti_Tran_From; }

    public DbsGroup getNewpar_Twrparti_Tran_FromRedef1() { return newpar_Twrparti_Tran_FromRedef1; }

    public DbsField getNewpar_Twrparti_Tr_Fr_Tin_Type() { return newpar_Twrparti_Tr_Fr_Tin_Type; }

    public DbsField getNewpar_Twrparti_Tr_Fr_Tin() { return newpar_Twrparti_Tr_Fr_Tin; }

    public DbsField getNewpar_Twrparti_Tran_To() { return newpar_Twrparti_Tran_To; }

    public DbsGroup getNewpar_Twrparti_Tran_ToRedef2() { return newpar_Twrparti_Tran_ToRedef2; }

    public DbsField getNewpar_Twrparti_Tr_To_Tin_Type() { return newpar_Twrparti_Tr_To_Tin_Type; }

    public DbsField getNewpar_Twrparti_Tr_To_Tin() { return newpar_Twrparti_Tr_To_Tin; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_newpar = new DataAccessProgramView(new NameInfo("vw_newpar", "NEWPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        newpar_Twrparti_Status = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        newpar_Twrparti_Tax_Id_Type = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Tax_Id_Type", "TWRPARTI-TAX-ID-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_TAX_ID_TYPE");
        newpar_Twrparti_Tax_Id = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Tax_Id", "TWRPARTI-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TWRPARTI_TAX_ID");
        newpar_Twrparti_Citation_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Citation_Ind", "TWRPARTI-CITATION-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_CITATION_IND");
        newpar_Twrparti_Part_Eff_Start_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Part_Eff_Start_Dte", "TWRPARTI-PART-EFF-START-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_START_DTE");
        newpar_Twrparti_Part_Eff_End_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Part_Eff_End_Dte", "TWRPARTI-PART-EFF-END-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PART_EFF_END_DTE");
        newpar_Twrparti_Part_Invrse_End_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Part_Invrse_End_Dte", "TWRPARTI-PART-INVRSE-END-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TWRPARTI_PART_INVRSE_END_DTE");
        newpar_Twrparti_Hold_Cde = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Hold_Cde", "TWRPARTI-HOLD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_HOLD_CDE");
        newpar_Twrparti_Test_Rec_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Test_Rec_Ind", "TWRPARTI-TEST-REC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_TEST_REC_IND");
        newpar_Twrparti_Citizen_Cde = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Citizen_Cde", "TWRPARTI-CITIZEN-CDE", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "TWRPARTI_CITIZEN_CDE");
        newpar_Twrparti_Residency_Over_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Residency_Over_Ind", "TWRPARTI-RESIDENCY-OVER-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_OVER_IND");
        newpar_Twrparti_Residency_Type = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Residency_Type", "TWRPARTI-RESIDENCY-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_TYPE");
        newpar_Twrparti_Residency_Code = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Residency_Code", "TWRPARTI-RESIDENCY-CODE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "TWRPARTI_RESIDENCY_CODE");
        newpar_Twrparti_1st_Bnote_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_1st_Bnote_Ind", "TWRPARTI-1ST-BNOTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_1ST_BNOTE_IND");
        newpar_Twrparti_2nd_Bnote_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_2nd_Bnote_Ind", "TWRPARTI-2ND-BNOTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_2ND_BNOTE_IND");
        newpar_Twrparti_1st_Sol_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_1st_Sol_Ind", "TWRPARTI-1ST-SOL-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_1ST_SOL_IND");
        newpar_Twrparti_2nd_Sol_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_2nd_Sol_Ind", "TWRPARTI-2ND-SOL-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_2ND_SOL_IND");
        newpar_Twrparti_Cnote_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Cnote_Ind", "TWRPARTI-CNOTE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_CNOTE_IND");
        newpar_Twrparti_Frmw8_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frmw8_Ind", "TWRPARTI-FRMW8-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW8_IND");
        newpar_Twrparti_Frmw9_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frmw9_Ind", "TWRPARTI-FRMW9-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_FRMW9_IND");
        newpar_Twrparti_Frm1001_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frm1001_Ind", "TWRPARTI-FRM1001-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_FRM1001_IND");
        newpar_Twrparti_Frm1078_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frm1078_Ind", "TWRPARTI-FRM1078-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_FRM1078_IND");
        newpar_Twrparti_Updt_User = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Updt_User", "TWRPARTI-UPDT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_USER");
        newpar_Twrparti_Updt_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Updt_Dte", "TWRPARTI-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_DTE");
        newpar_Twrparti_Updt_Time = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Updt_Time", "TWRPARTI-UPDT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_TIME");
        newpar_Twrparti_Lu_Ts = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Lu_Ts", "TWRPARTI-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TWRPARTI_LU_TS");
        newpar_Twrparti_Alt_Addr_Ind = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ind", "TWRPARTI-ALT-ADDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_IND");
        newpar_Twrparti_Addr_Source = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Source", "TWRPARTI-ADDR-SOURCE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_ADDR_SOURCE");
        newpar_Twrparti_Origin = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Origin", "TWRPARTI-ORIGIN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPARTI_ORIGIN");
        newpar_Twrparti_Pin = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "TWRPARTI_PIN");
        newpar_Twrparti_Participant_Name = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        newpar_Twrparti_Addr_Ln1 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln1", "TWRPARTI-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN1");
        newpar_Twrparti_Addr_Ln2 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln2", "TWRPARTI-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN2");
        newpar_Twrparti_Addr_Ln3 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln3", "TWRPARTI-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN3");
        newpar_Twrparti_Addr_Ln4 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln4", "TWRPARTI-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN4");
        newpar_Twrparti_Addr_Ln5 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln5", "TWRPARTI-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN5");
        newpar_Twrparti_Addr_Ln6 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Addr_Ln6", "TWRPARTI-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN6");
        newpar_Twrparti_Participant_Dob = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Participant_Dob", "TWRPARTI-PARTICIPANT-DOB", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOB");
        newpar_Twrparti_Participant_Dod = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Participant_Dod", "TWRPARTI-PARTICIPANT-DOD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOD");
        newpar_Twrparti_Wpid = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Wpid", "TWRPARTI-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "TWRPARTI_WPID");
        newpar_Twrparti_Locality_Cde = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Locality_Cde", "TWRPARTI-LOCALITY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TWRPARTI_LOCALITY_CDE");
        newpar_Twrparti_1st_Bnote_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_1st_Bnote_Eff_Dte", "TWRPARTI-1ST-BNOTE-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_1ST_BNOTE_EFF_DTE");
        newpar_Twrparti_2nd_Bnote_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_2nd_Bnote_Eff_Dte", "TWRPARTI-2ND-BNOTE-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_2ND_BNOTE_EFF_DTE");
        newpar_Twrparti_1st_Sol_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_1st_Sol_Eff_Dte", "TWRPARTI-1ST-SOL-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_1ST_SOL_EFF_DTE");
        newpar_Twrparti_2nd_Sol_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_2nd_Sol_Eff_Dte", "TWRPARTI-2ND-SOL-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_2ND_SOL_EFF_DTE");
        newpar_Twrparti_Cnote_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Cnote_Eff_Dte", "TWRPARTI-CNOTE-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_CNOTE_EFF_DTE");
        newpar_Twrparti_Frmw8_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frmw8_Eff_Dte", "TWRPARTI-FRMW8-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRMW8_EFF_DTE");
        newpar_Twrparti_Frmw9_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frmw9_Eff_Dte", "TWRPARTI-FRMW9-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRMW9_EFF_DTE");
        newpar_Twrparti_Frm1001_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frm1001_Eff_Dte", "TWRPARTI-FRM1001-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRM1001_EFF_DTE");
        newpar_Twrparti_Frm1078_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Frm1078_Eff_Dte", "TWRPARTI-FRM1078-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_FRM1078_EFF_DTE");
        newpar_Twrparti_Comment = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Comment", "TWRPARTI-COMMENT", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "TWRPARTI_COMMENT");
        newpar_Twrparti_Country_Of_Res = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Country_Of_Res", "TWRPARTI-COUNTRY-OF-RES", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TWRPARTI_COUNTRY_OF_RES");
        newpar_Twrparti_Country_Desc = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Country_Desc", "TWRPARTI-COUNTRY-DESC", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_COUNTRY_DESC");
        newpar_Twrparti_Alt_Addr_Eff_Dte = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Eff_Dte", "TWRPARTI-ALT-ADDR-EFF-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_EFF_DTE");
        newpar_Twrparti_Alt_Addr_Ln1 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln1", "TWRPARTI-ALT-ADDR-LN1", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN1");
        newpar_Twrparti_Alt_Addr_Ln2 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln2", "TWRPARTI-ALT-ADDR-LN2", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN2");
        newpar_Twrparti_Alt_Addr_Ln3 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln3", "TWRPARTI-ALT-ADDR-LN3", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN3");
        newpar_Twrparti_Alt_Addr_Ln4 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln4", "TWRPARTI-ALT-ADDR-LN4", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN4");
        newpar_Twrparti_Alt_Addr_Ln5 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln5", "TWRPARTI-ALT-ADDR-LN5", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN5");
        newpar_Twrparti_Alt_Addr_Ln6 = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Alt_Addr_Ln6", "TWRPARTI-ALT-ADDR-LN6", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_ALT_ADDR_LN6");
        newpar_Twrparti_Tran_From = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Tran_From", "TWRPARTI-TRAN-FROM", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "TWRPARTI_TRAN_FROM");
        newpar_Twrparti_Tran_FromRedef1 = vw_newpar.getRecord().newGroupInGroup("newpar_Twrparti_Tran_FromRedef1", "Redefines", newpar_Twrparti_Tran_From);
        newpar_Twrparti_Tr_Fr_Tin_Type = newpar_Twrparti_Tran_FromRedef1.newFieldInGroup("newpar_Twrparti_Tr_Fr_Tin_Type", "TWRPARTI-TR-FR-TIN-TYPE", 
            FieldType.STRING, 1);
        newpar_Twrparti_Tr_Fr_Tin = newpar_Twrparti_Tran_FromRedef1.newFieldInGroup("newpar_Twrparti_Tr_Fr_Tin", "TWRPARTI-TR-FR-TIN", FieldType.STRING, 
            10);
        newpar_Twrparti_Tran_To = vw_newpar.getRecord().newFieldInGroup("newpar_Twrparti_Tran_To", "TWRPARTI-TRAN-TO", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "TWRPARTI_TRAN_TO");
        newpar_Twrparti_Tran_ToRedef2 = vw_newpar.getRecord().newGroupInGroup("newpar_Twrparti_Tran_ToRedef2", "Redefines", newpar_Twrparti_Tran_To);
        newpar_Twrparti_Tr_To_Tin_Type = newpar_Twrparti_Tran_ToRedef2.newFieldInGroup("newpar_Twrparti_Tr_To_Tin_Type", "TWRPARTI-TR-TO-TIN-TYPE", FieldType.STRING, 
            1);
        newpar_Twrparti_Tr_To_Tin = newpar_Twrparti_Tran_ToRedef2.newFieldInGroup("newpar_Twrparti_Tr_To_Tin", "TWRPARTI-TR-TO-TIN", FieldType.STRING, 
            10);

        this.setRecordName("LdaTwrl810c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_newpar.reset();
    }

    // Constructor
    public LdaTwrl810c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
