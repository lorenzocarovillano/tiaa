/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:20 PM
**        * FROM NATURAL LDA     : TWRL117A
************************************************************
**        * FILE NAME            : LdaTwrl117a.java
**        * CLASS NAME           : LdaTwrl117a
**        * INSTANCE NAME        : LdaTwrl117a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl117a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl117a;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Control_Number;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Filler1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Form_Type;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Rec_Type;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Trans_Code;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Filler2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Tax_Year;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill3;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Filler3;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_Payers_Information;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Id;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Name;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Town;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_State;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Ext;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Filler4;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_Payee_Information;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Bank;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Name;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Town;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_State;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Ext;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Filler5;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Ser_Corp;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Commission;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Rats;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Interst;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_InterstRedef3;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Interst_N;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Part_Disrib;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Dividend;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng_N;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Other_Payment;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Gross_Proceeds;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Dist_Rsa_No_Wthldng;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill6;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill7;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill8;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill9;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill10;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill11;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill12;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill13;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_Fill14;
    private DbsGroup pnd_Twrl117aRedef5;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Control_Number;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Filler1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Form_Type;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Filler2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Filler3;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_State;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full;
    private DbsGroup pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Ext;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Filler4;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill5;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill6;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill7;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill8;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill9;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill10;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill11;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill12;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill13;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_S_Fill14;
    private DbsGroup pnd_Twrl117aRedef7;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1a;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1b;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1c;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1d;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1e;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1f;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1g;
    private DbsField pnd_Twrl117a_Pnd_Twrl117a_1_Filler;

    public DbsGroup getPnd_Twrl117a() { return pnd_Twrl117a; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Control_Number() { return pnd_Twrl117a_Pnd_Twrl117a_Control_Number; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Filler1() { return pnd_Twrl117a_Pnd_Twrl117a_Filler1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Form_Type() { return pnd_Twrl117a_Pnd_Twrl117a_Form_Type; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Rec_Type() { return pnd_Twrl117a_Pnd_Twrl117a_Rec_Type; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Trans_Code() { return pnd_Twrl117a_Pnd_Twrl117a_Trans_Code; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Filler2() { return pnd_Twrl117a_Pnd_Twrl117a_Filler2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Tax_Year() { return pnd_Twrl117a_Pnd_Twrl117a_Tax_Year; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill3() { return pnd_Twrl117a_Pnd_Twrl117a_Fill3; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Filler3() { return pnd_Twrl117a_Pnd_Twrl117a_Filler3; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_Payers_Information() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Information; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Id() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Id; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Name() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Name; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Town() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Town; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_State() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_State; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Zip() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Ext() { return pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Ext; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Filler4() { return pnd_Twrl117a_Pnd_Twrl117a_Filler4; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_Payee_Information() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Information; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Bank() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Bank; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Name() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Name; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Town() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Town; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_State() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_State; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Zip() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Ext() { return pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Ext; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Filler5() { return pnd_Twrl117a_Pnd_Twrl117a_Filler5; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem() { return pnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Ser_Corp() { return pnd_Twrl117a_Pnd_Twrl117a_Ser_Corp; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Commission() { return pnd_Twrl117a_Pnd_Twrl117a_Commission; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Rats() { return pnd_Twrl117a_Pnd_Twrl117a_Rats; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Interst() { return pnd_Twrl117a_Pnd_Twrl117a_Interst; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_InterstRedef3() { return pnd_Twrl117a_Pnd_Twrl117a_InterstRedef3; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Interst_N() { return pnd_Twrl117a_Pnd_Twrl117a_Interst_N; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Part_Disrib() { return pnd_Twrl117a_Pnd_Twrl117a_Part_Disrib; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Dividend() { return pnd_Twrl117a_Pnd_Twrl117a_Dividend; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng() { return pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4() { return pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng_N() { return pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng_N; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Other_Payment() { return pnd_Twrl117a_Pnd_Twrl117a_Other_Payment; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Gross_Proceeds() { return pnd_Twrl117a_Pnd_Twrl117a_Gross_Proceeds; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Dist_Rsa_No_Wthldng() { return pnd_Twrl117a_Pnd_Twrl117a_Dist_Rsa_No_Wthldng; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill6() { return pnd_Twrl117a_Pnd_Twrl117a_Fill6; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill7() { return pnd_Twrl117a_Pnd_Twrl117a_Fill7; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill8() { return pnd_Twrl117a_Pnd_Twrl117a_Fill8; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill9() { return pnd_Twrl117a_Pnd_Twrl117a_Fill9; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill10() { return pnd_Twrl117a_Pnd_Twrl117a_Fill10; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill11() { return pnd_Twrl117a_Pnd_Twrl117a_Fill11; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill12() { return pnd_Twrl117a_Pnd_Twrl117a_Fill12; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill13() { return pnd_Twrl117a_Pnd_Twrl117a_Fill13; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_Fill14() { return pnd_Twrl117a_Pnd_Twrl117a_Fill14; }

    public DbsGroup getPnd_Twrl117aRedef5() { return pnd_Twrl117aRedef5; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Control_Number() { return pnd_Twrl117a_Pnd_Twrl117a_S_Control_Number; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Filler1() { return pnd_Twrl117a_Pnd_Twrl117a_S_Filler1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Form_Type() { return pnd_Twrl117a_Pnd_Twrl117a_S_Form_Type; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type() { return pnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code() { return pnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Filler2() { return pnd_Twrl117a_Pnd_Twrl117a_S_Filler2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year() { return pnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Filler3() { return pnd_Twrl117a_Pnd_Twrl117a_S_Filler3; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_State() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_State; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full; }

    public DbsGroup getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Ext() { return pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Ext; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Filler4() { return pnd_Twrl117a_Pnd_Twrl117a_S_Filler4; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc() { return pnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld() { return pnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid() { return pnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type() { return pnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill5() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill5; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill6() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill6; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill7() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill7; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill8() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill8; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill9() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill9; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill10() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill10; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill11() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill11; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill12() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill12; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill13() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill13; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_S_Fill14() { return pnd_Twrl117a_Pnd_Twrl117a_S_Fill14; }

    public DbsGroup getPnd_Twrl117aRedef7() { return pnd_Twrl117aRedef7; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1a() { return pnd_Twrl117a_Pnd_Twrl117a_1a; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1b() { return pnd_Twrl117a_Pnd_Twrl117a_1b; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1c() { return pnd_Twrl117a_Pnd_Twrl117a_1c; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1d() { return pnd_Twrl117a_Pnd_Twrl117a_1d; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1e() { return pnd_Twrl117a_Pnd_Twrl117a_1e; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1f() { return pnd_Twrl117a_Pnd_Twrl117a_1f; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1g() { return pnd_Twrl117a_Pnd_Twrl117a_1g; }

    public DbsField getPnd_Twrl117a_Pnd_Twrl117a_1_Filler() { return pnd_Twrl117a_Pnd_Twrl117a_1_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl117a = newGroupInRecord("pnd_Twrl117a", "#TWRL117A");
        pnd_Twrl117a_Pnd_Twrl117a_Control_Number = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Control_Number", "#TWRL117A-CONTROL-NUMBER", 
            FieldType.STRING, 10);
        pnd_Twrl117a_Pnd_Twrl117a_Filler1 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Filler1", "#TWRL117A-FILLER1", FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Form_Type = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Form_Type", "#TWRL117A-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117a_Pnd_Twrl117a_Rec_Type = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Rec_Type", "#TWRL117A-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117a_Pnd_Twrl117a_Trans_Code = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Trans_Code", "#TWRL117A-TRANS-CODE", FieldType.STRING, 
            1);
        pnd_Twrl117a_Pnd_Twrl117a_Filler2 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Filler2", "#TWRL117A-FILLER2", FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Tax_Year = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Tax_Year", "#TWRL117A-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl117a_Pnd_Twrl117a_Fill3 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill3", "#TWRL117A-FILL3", FieldType.STRING, 8);
        pnd_Twrl117a_Pnd_Twrl117a_Filler3 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Filler3", "#TWRL117A-FILLER3", FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Information = pnd_Twrl117a.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Information", "#TWRL117A-PAYERS-INFORMATION");
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Id = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Id", "#TWRL117A-PAYERS-ID", 
            FieldType.NUMERIC, 9);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Name = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Name", 
            "#TWRL117A-PAYERS-NAME", FieldType.STRING, 30);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1 = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_1", 
            "#TWRL117A-PAYERS-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2 = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Address_2", 
            "#TWRL117A-PAYERS-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Town = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Town", 
            "#TWRL117A-PAYERS-TOWN", FieldType.STRING, 13);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_State = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_State", 
            "#TWRL117A-PAYERS-STATE", FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full", 
            "#TWRL117A-PAYERS-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1 = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1", 
            "Redefines", pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Full);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip = pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip", 
            "#TWRL117A-PAYERS-ZIP", FieldType.NUMERIC, 5);
        pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Ext = pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_FullRedef1.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payers_Zip_Ext", 
            "#TWRL117A-PAYERS-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117a_Pnd_Twrl117a_Filler4 = pnd_Twrl117a_Pnd_Twrl117a_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Filler4", "#TWRL117A-FILLER4", 
            FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Information = pnd_Twrl117a.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Information", "#TWRL117A-PAYEE-INFORMATION");
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Ssn", "#TWRL117A-PAYEE-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Bank = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Bank", "#TWRL117A-PAYEE-BANK", 
            FieldType.STRING, 20);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Name = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Name", "#TWRL117A-PAYEE-NAME", 
            FieldType.STRING, 30);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1 = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_1", 
            "#TWRL117A-PAYEE-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2 = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Address_2", 
            "#TWRL117A-PAYEE-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Town = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Town", "#TWRL117A-PAYEE-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_State = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_State", "#TWRL117A-PAYEE-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full", 
            "#TWRL117A-PAYEE-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2 = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2", 
            "Redefines", pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Full);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip = pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip", "#TWRL117A-PAYEE-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Ext = pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Payee_Zip_Ext", 
            "#TWRL117A-PAYEE-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117a_Pnd_Twrl117a_Filler5 = pnd_Twrl117a_Pnd_Twrl117a_Payee_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Filler5", "#TWRL117A-FILLER5", 
            FieldType.STRING, 1);
        pnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Ser_Redeem", "#TWRL117A-SER-REDEEM", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Ser_Corp = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Ser_Corp", "#TWRL117A-SER-CORP", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Commission = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Commission", "#TWRL117A-COMMISSION", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Rats = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Rats", "#TWRL117A-RATS", FieldType.NUMERIC, 12);
        pnd_Twrl117a_Pnd_Twrl117a_Interst = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Interst", "#TWRL117A-INTERST", FieldType.STRING, 12);
        pnd_Twrl117a_Pnd_Twrl117a_InterstRedef3 = pnd_Twrl117a.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_InterstRedef3", "Redefines", pnd_Twrl117a_Pnd_Twrl117a_Interst);
        pnd_Twrl117a_Pnd_Twrl117a_Interst_N = pnd_Twrl117a_Pnd_Twrl117a_InterstRedef3.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Interst_N", "#TWRL117A-INTERST-N", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117a_Pnd_Twrl117a_Part_Disrib = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Part_Disrib", "#TWRL117A-PART-DISRIB", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Dividend = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Dividend", "#TWRL117A-DIVIDEND", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng", "#TWRL117A-PPD-NO-WTHLDNG", 
            FieldType.STRING, 12);
        pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4 = pnd_Twrl117a.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4", "Redefines", pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng);
        pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng_N = pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_WthldngRedef4.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Ppd_No_Wthldng_N", 
            "#TWRL117A-PPD-NO-WTHLDNG-N", FieldType.DECIMAL, 12,2);
        pnd_Twrl117a_Pnd_Twrl117a_Other_Payment = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Other_Payment", "#TWRL117A-OTHER-PAYMENT", FieldType.NUMERIC, 
            12);
        pnd_Twrl117a_Pnd_Twrl117a_Gross_Proceeds = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Gross_Proceeds", "#TWRL117A-GROSS-PROCEEDS", 
            FieldType.NUMERIC, 12);
        pnd_Twrl117a_Pnd_Twrl117a_Dist_Rsa_No_Wthldng = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Dist_Rsa_No_Wthldng", "#TWRL117A-DIST-RSA-NO-WTHLDNG", 
            FieldType.NUMERIC, 12);
        pnd_Twrl117a_Pnd_Twrl117a_Fill6 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill6", "#TWRL117A-FILL6", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill7 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill7", "#TWRL117A-FILL7", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill8 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill8", "#TWRL117A-FILL8", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill9 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill9", "#TWRL117A-FILL9", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill10 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill10", "#TWRL117A-FILL10", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill11 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill11", "#TWRL117A-FILL11", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill12 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill12", "#TWRL117A-FILL12", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill13 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill13", "#TWRL117A-FILL13", FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_Fill14 = pnd_Twrl117a.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_Fill14", "#TWRL117A-FILL14", FieldType.STRING, 48);
        pnd_Twrl117aRedef5 = newGroupInRecord("pnd_Twrl117aRedef5", "Redefines", pnd_Twrl117a);
        pnd_Twrl117a_Pnd_Twrl117a_S_Control_Number = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Control_Number", "#TWRL117A-S-CONTROL-NUMBER", 
            FieldType.NUMERIC, 10);
        pnd_Twrl117a_Pnd_Twrl117a_S_Filler1 = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Filler1", "#TWRL117A-S-FILLER1", FieldType.STRING, 
            2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Form_Type = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Form_Type", "#TWRL117A-S-FORM-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Rec_Type", "#TWRL117A-S-REC-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Trans_Code", "#TWRL117A-S-TRANS-CODE", 
            FieldType.STRING, 1);
        pnd_Twrl117a_Pnd_Twrl117a_S_Filler2 = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Filler2", "#TWRL117A-S-FILLER2", FieldType.STRING, 
            2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Tax_Year", "#TWRL117A-S-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl117a_Pnd_Twrl117a_S_Filler3 = pnd_Twrl117aRedef5.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Filler3", "#TWRL117A-S-FILLER3", FieldType.STRING, 
            2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information = pnd_Twrl117aRedef5.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information", "#TWRL117A-S-PAYERS-INFORMATION");
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Id", 
            "#TWRL117A-S-PAYERS-ID", FieldType.NUMERIC, 9);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Name", 
            "#TWRL117A-S-PAYERS-NAME", FieldType.STRING, 30);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_1", 
            "#TWRL117A-S-PAYERS-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Address_2", 
            "#TWRL117A-S-PAYERS-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Town", 
            "#TWRL117A-S-PAYERS-TOWN", FieldType.STRING, 13);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_State = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_State", 
            "#TWRL117A-S-PAYERS-STATE", FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full", 
            "#TWRL117A-S-PAYERS-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newGroupInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6", 
            "Redefines", pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Full);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip", 
            "#TWRL117A-S-PAYERS-ZIP", FieldType.NUMERIC, 5);
        pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Ext = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_FullRedef6.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Zip_Ext", 
            "#TWRL117A-S-PAYERS-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117a_Pnd_Twrl117a_S_Filler4 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Filler4", "#TWRL117A-S-FILLER4", 
            FieldType.STRING, 2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Num_Of_Doc", 
            "#TWRL117A-S-NUM-OF-DOC", FieldType.STRING, 10);
        pnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Amt_Withld", 
            "#TWRL117A-S-AMT-WITHLD", FieldType.DECIMAL, 15,2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Amout_Paid", 
            "#TWRL117A-S-AMOUT-PAID", FieldType.DECIMAL, 15,2);
        pnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Taxpayer_Type", 
            "#TWRL117A-S-TAXPAYER-TYPE", FieldType.STRING, 1);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill5 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill5", "#TWRL117A-S-FILL5", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill6 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill6", "#TWRL117A-S-FILL6", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill7 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill7", "#TWRL117A-S-FILL7", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill8 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill8", "#TWRL117A-S-FILL8", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill9 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill9", "#TWRL117A-S-FILL9", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill10 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill10", "#TWRL117A-S-FILL10", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill11 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill11", "#TWRL117A-S-FILL11", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill12 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill12", "#TWRL117A-S-FILL12", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill13 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill13", "#TWRL117A-S-FILL13", 
            FieldType.STRING, 250);
        pnd_Twrl117a_Pnd_Twrl117a_S_Fill14 = pnd_Twrl117a_Pnd_Twrl117a_S_Payers_Information.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_S_Fill14", "#TWRL117A-S-FILL14", 
            FieldType.STRING, 51);
        pnd_Twrl117aRedef7 = newGroupInRecord("pnd_Twrl117aRedef7", "Redefines", pnd_Twrl117a);
        pnd_Twrl117a_Pnd_Twrl117a_1a = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1a", "#TWRL117A-1A", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1b = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1b", "#TWRL117A-1B", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1c = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1c", "#TWRL117A-1C", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1d = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1d", "#TWRL117A-1D", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1e = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1e", "#TWRL117A-1E", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1f = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1f", "#TWRL117A-1F", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1g = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1g", "#TWRL117A-1G", FieldType.STRING, 100);
        pnd_Twrl117a_Pnd_Twrl117a_1_Filler = pnd_Twrl117aRedef7.newFieldInGroup("pnd_Twrl117a_Pnd_Twrl117a_1_Filler", "#TWRL117A-1-FILLER", FieldType.STRING, 
            250);

        this.setRecordName("LdaTwrl117a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl117a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
