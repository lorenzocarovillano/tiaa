/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:15:16 PM
**        * FROM NATURAL LDA     : TWRLISO
************************************************************
**        * FILE NAME            : LdaTwrliso.java
**        * CLASS NAME           : LdaTwrliso
**        * INSTANCE NAME        : LdaTwrliso
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrliso extends DbsRecord
{
    // Properties
    private DbsField pnd_Iso_Countries;
    private DbsField pnd_Irs_Countries;
    private DbsGroup pnd_Country_Names;
    private DbsField pnd_Country_Names_Pnd_Country_Alias;

    public DbsField getPnd_Iso_Countries() { return pnd_Iso_Countries; }

    public DbsField getPnd_Irs_Countries() { return pnd_Irs_Countries; }

    public DbsGroup getPnd_Country_Names() { return pnd_Country_Names; }

    public DbsField getPnd_Country_Names_Pnd_Country_Alias() { return pnd_Country_Names_Pnd_Country_Alias; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Iso_Countries = newFieldArrayInRecord("pnd_Iso_Countries", "#ISO-COUNTRIES", FieldType.STRING, 2, new DbsArrayController(1,100));

        pnd_Irs_Countries = newFieldArrayInRecord("pnd_Irs_Countries", "#IRS-COUNTRIES", FieldType.STRING, 2, new DbsArrayController(1,100));

        pnd_Country_Names = newGroupArrayInRecord("pnd_Country_Names", "#COUNTRY-NAMES", new DbsArrayController(1,100));
        pnd_Country_Names_Pnd_Country_Alias = pnd_Country_Names.newFieldArrayInGroup("pnd_Country_Names_Pnd_Country_Alias", "#COUNTRY-ALIAS", FieldType.STRING, 
            50, new DbsArrayController(1,7));

        this.setRecordName("LdaTwrliso");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrliso() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
