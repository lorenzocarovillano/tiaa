/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:29 PM
**        * FROM NATURAL LDA     : TWRL3041
************************************************************
**        * FILE NAME            : LdaTwrl3041.java
**        * CLASS NAME           : LdaTwrl3041
**        * INSTANCE NAME        : LdaTwrl3041
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3041 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Ext_Calif;
    private DbsField pnd_Ext_Calif_Twrpymnt_Tax_Year;
    private DbsField pnd_Ext_Calif_Twrpymnt_Tax_Id_Type;
    private DbsField pnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr;
    private DbsField pnd_Ext_Calif_Twrpymnt_Company_Cde;
    private DbsField pnd_Ext_Calif_Twrpymnt_Tax_Citizenship;
    private DbsField pnd_Ext_Calif_Twrpymnt_Contract_Nbr;
    private DbsField pnd_Ext_Calif_Twrpymnt_Payee_Cde;
    private DbsField pnd_Ext_Calif_Twrpymnt_Distribution_Cde;
    private DbsField pnd_Ext_Calif_Twrpymnt_Residency_Type;
    private DbsField pnd_Ext_Calif_Twrpymnt_Residency_Code;
    private DbsField pnd_Ext_Calif_Twrpymnt_Ivc_Indicator;
    private DbsField pnd_Ext_Calif_Pnd_C_Twrpymnt_Payments;
    private DbsGroup pnd_Ext_Calif_Twrpymnt_Payments;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld;
    private DbsField pnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld;

    public DbsGroup getPnd_Ext_Calif() { return pnd_Ext_Calif; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Tax_Year() { return pnd_Ext_Calif_Twrpymnt_Tax_Year; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Tax_Id_Type() { return pnd_Ext_Calif_Twrpymnt_Tax_Id_Type; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr() { return pnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Company_Cde() { return pnd_Ext_Calif_Twrpymnt_Company_Cde; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Tax_Citizenship() { return pnd_Ext_Calif_Twrpymnt_Tax_Citizenship; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Contract_Nbr() { return pnd_Ext_Calif_Twrpymnt_Contract_Nbr; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Payee_Cde() { return pnd_Ext_Calif_Twrpymnt_Payee_Cde; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Distribution_Cde() { return pnd_Ext_Calif_Twrpymnt_Distribution_Cde; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Residency_Type() { return pnd_Ext_Calif_Twrpymnt_Residency_Type; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Residency_Code() { return pnd_Ext_Calif_Twrpymnt_Residency_Code; }

    public DbsField getPnd_Ext_Calif_Twrpymnt_Ivc_Indicator() { return pnd_Ext_Calif_Twrpymnt_Ivc_Indicator; }

    public DbsField getPnd_Ext_Calif_Pnd_C_Twrpymnt_Payments() { return pnd_Ext_Calif_Pnd_C_Twrpymnt_Payments; }

    public DbsGroup getPnd_Ext_Calif_Twrpymnt_Payments() { return pnd_Ext_Calif_Twrpymnt_Payments; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte() { return pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte() { return pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde() { return pnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde() { return pnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type() { return pnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type() { return pnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt() { return pnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt() { return pnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt() { return pnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld() { return pnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld; }

    public DbsField getPnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld() { return pnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ext_Calif = newGroupInRecord("pnd_Ext_Calif", "#EXT-CALIF");
        pnd_Ext_Calif_Twrpymnt_Tax_Year = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Tax_Year", "TWRPYMNT-TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Ext_Calif_Twrpymnt_Tax_Id_Type = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Tax_Id_Type", "TWRPYMNT-TAX-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Tax_Id_Nbr", "TWRPYMNT-TAX-ID-NBR", FieldType.STRING, 
            10);
        pnd_Ext_Calif_Twrpymnt_Company_Cde = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Company_Cde", "TWRPYMNT-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Ext_Calif_Twrpymnt_Tax_Citizenship = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Tax_Citizenship", "TWRPYMNT-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Ext_Calif_Twrpymnt_Contract_Nbr = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Contract_Nbr", "TWRPYMNT-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Ext_Calif_Twrpymnt_Payee_Cde = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Payee_Cde", "TWRPYMNT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Ext_Calif_Twrpymnt_Distribution_Cde = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Distribution_Cde", "TWRPYMNT-DISTRIBUTION-CDE", 
            FieldType.STRING, 2);
        pnd_Ext_Calif_Twrpymnt_Residency_Type = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Residency_Type", "TWRPYMNT-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Ext_Calif_Twrpymnt_Residency_Code = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Residency_Code", "TWRPYMNT-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Ext_Calif_Twrpymnt_Ivc_Indicator = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Twrpymnt_Ivc_Indicator", "TWRPYMNT-IVC-INDICATOR", FieldType.STRING, 
            1);
        pnd_Ext_Calif_Pnd_C_Twrpymnt_Payments = pnd_Ext_Calif.newFieldInGroup("pnd_Ext_Calif_Pnd_C_Twrpymnt_Payments", "#C-TWRPYMNT-PAYMENTS", FieldType.NUMERIC, 
            3);
        pnd_Ext_Calif_Twrpymnt_Payments = pnd_Ext_Calif.newGroupInGroup("pnd_Ext_Calif_Twrpymnt_Payments", "TWRPYMNT-PAYMENTS");
        pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Dte", "#TWRPYMNT-PYMNT-DTE", 
            FieldType.STRING, 8, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Pymnt_Intfce_Dte", 
            "#TWRPYMNT-PYMNT-INTFCE-DTE", FieldType.STRING, 8, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Updte_Srce_Cde", 
            "#TWRPYMNT-UPDTE-SRCE-CDE", FieldType.STRING, 2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Orgn_Srce_Cde", "#TWRPYMNT-ORGN-SRCE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Payment_Type", "#TWRPYMNT-PAYMENT-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Settle_Type", "#TWRPYMNT-SETTLE-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Gross_Amt", "#TWRPYMNT-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Ivc_Amt", "#TWRPYMNT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Fed_Wthld_Amt", "#TWRPYMNT-FED-WTHLD-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_State_Wthld", "#TWRPYMNT-STATE-WTHLD", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,24));
        pnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld = pnd_Ext_Calif_Twrpymnt_Payments.newFieldArrayInGroup("pnd_Ext_Calif_Pnd_Twrpymnt_Local_Wthld", "#TWRPYMNT-LOCAL-WTHLD", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,24));

        this.setRecordName("LdaTwrl3041");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3041() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
