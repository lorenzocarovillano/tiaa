/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:40 PM
**        * FROM NATURAL LDA     : TWRL443D
************************************************************
**        * FILE NAME            : LdaTwrl443d.java
**        * CLASS NAME           : LdaTwrl443d
**        * INSTANCE NAME        : LdaTwrl443d
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443d extends DbsRecord
{
    // Properties
    private DbsGroup pnd_C_Tape;
    private DbsField pnd_C_Tape_Pnd_C_Record_Type;
    private DbsField pnd_C_Tape_Pnd_C_Number_Of_Payees;
    private DbsField pnd_C_Tape_Pnd_C_Filler_1;
    private DbsField pnd_C_Tape_Pnd_C_Total_1;
    private DbsField pnd_C_Tape_Pnd_C_Total_2;
    private DbsField pnd_C_Tape_Pnd_C_Total_3;
    private DbsField pnd_C_Tape_Pnd_C_Total_4;
    private DbsField pnd_C_Tape_Pnd_C_Total_5;
    private DbsField pnd_C_Tape_Pnd_C_Total_6;
    private DbsField pnd_C_Tape_Pnd_C_Total_7;
    private DbsField pnd_C_Tape_Pnd_C_Total_8;
    private DbsField pnd_C_Tape_Pnd_C_Total_9;
    private DbsField pnd_C_Tape_Pnd_C_Total_A;
    private DbsField pnd_C_Tape_Pnd_C_Total_B;
    private DbsField pnd_C_Tape_Pnd_C_Total_C;
    private DbsField pnd_C_Tape_Pnd_C_Total_D;
    private DbsField pnd_C_Tape_Pnd_C_Total_E;
    private DbsField pnd_C_Tape_Pnd_C_Total_F;
    private DbsField pnd_C_Tape_Pnd_C_Total_G;
    private DbsField pnd_C_Tape_Pnd_C_Filler_2;
    private DbsField pnd_C_Tape_Pnd_C_Sequence_Number;
    private DbsField pnd_C_Tape_Pnd_C_Filler_4;
    private DbsField pnd_C_Tape_Pnd_C_Blank_Cr_Lf;
    private DbsGroup pnd_C_TapeRedef1;
    private DbsField pnd_C_Tape_Pnd_C_Move_1;
    private DbsField pnd_C_Tape_Pnd_C_Move_2;
    private DbsField pnd_C_Tape_Pnd_C_Move_3;

    public DbsGroup getPnd_C_Tape() { return pnd_C_Tape; }

    public DbsField getPnd_C_Tape_Pnd_C_Record_Type() { return pnd_C_Tape_Pnd_C_Record_Type; }

    public DbsField getPnd_C_Tape_Pnd_C_Number_Of_Payees() { return pnd_C_Tape_Pnd_C_Number_Of_Payees; }

    public DbsField getPnd_C_Tape_Pnd_C_Filler_1() { return pnd_C_Tape_Pnd_C_Filler_1; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_1() { return pnd_C_Tape_Pnd_C_Total_1; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_2() { return pnd_C_Tape_Pnd_C_Total_2; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_3() { return pnd_C_Tape_Pnd_C_Total_3; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_4() { return pnd_C_Tape_Pnd_C_Total_4; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_5() { return pnd_C_Tape_Pnd_C_Total_5; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_6() { return pnd_C_Tape_Pnd_C_Total_6; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_7() { return pnd_C_Tape_Pnd_C_Total_7; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_8() { return pnd_C_Tape_Pnd_C_Total_8; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_9() { return pnd_C_Tape_Pnd_C_Total_9; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_A() { return pnd_C_Tape_Pnd_C_Total_A; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_B() { return pnd_C_Tape_Pnd_C_Total_B; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_C() { return pnd_C_Tape_Pnd_C_Total_C; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_D() { return pnd_C_Tape_Pnd_C_Total_D; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_E() { return pnd_C_Tape_Pnd_C_Total_E; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_F() { return pnd_C_Tape_Pnd_C_Total_F; }

    public DbsField getPnd_C_Tape_Pnd_C_Total_G() { return pnd_C_Tape_Pnd_C_Total_G; }

    public DbsField getPnd_C_Tape_Pnd_C_Filler_2() { return pnd_C_Tape_Pnd_C_Filler_2; }

    public DbsField getPnd_C_Tape_Pnd_C_Sequence_Number() { return pnd_C_Tape_Pnd_C_Sequence_Number; }

    public DbsField getPnd_C_Tape_Pnd_C_Filler_4() { return pnd_C_Tape_Pnd_C_Filler_4; }

    public DbsField getPnd_C_Tape_Pnd_C_Blank_Cr_Lf() { return pnd_C_Tape_Pnd_C_Blank_Cr_Lf; }

    public DbsGroup getPnd_C_TapeRedef1() { return pnd_C_TapeRedef1; }

    public DbsField getPnd_C_Tape_Pnd_C_Move_1() { return pnd_C_Tape_Pnd_C_Move_1; }

    public DbsField getPnd_C_Tape_Pnd_C_Move_2() { return pnd_C_Tape_Pnd_C_Move_2; }

    public DbsField getPnd_C_Tape_Pnd_C_Move_3() { return pnd_C_Tape_Pnd_C_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_C_Tape = newGroupInRecord("pnd_C_Tape", "#C-TAPE");
        pnd_C_Tape_Pnd_C_Record_Type = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Record_Type", "#C-RECORD-TYPE", FieldType.STRING, 1);
        pnd_C_Tape_Pnd_C_Number_Of_Payees = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Number_Of_Payees", "#C-NUMBER-OF-PAYEES", FieldType.NUMERIC, 
            8);
        pnd_C_Tape_Pnd_C_Filler_1 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Filler_1", "#C-FILLER-1", FieldType.STRING, 6);
        pnd_C_Tape_Pnd_C_Total_1 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_1", "#C-TOTAL-1", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_2 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_2", "#C-TOTAL-2", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_3 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_3", "#C-TOTAL-3", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_4 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_4", "#C-TOTAL-4", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_5 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_5", "#C-TOTAL-5", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_6 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_6", "#C-TOTAL-6", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_7 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_7", "#C-TOTAL-7", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_8 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_8", "#C-TOTAL-8", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_9 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_9", "#C-TOTAL-9", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_A = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_A", "#C-TOTAL-A", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_B = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_B", "#C-TOTAL-B", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_C = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_C", "#C-TOTAL-C", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_D = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_D", "#C-TOTAL-D", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_E = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_E", "#C-TOTAL-E", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_F = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_F", "#C-TOTAL-F", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Total_G = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Total_G", "#C-TOTAL-G", FieldType.DECIMAL, 18,2);
        pnd_C_Tape_Pnd_C_Filler_2 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Filler_2", "#C-FILLER-2", FieldType.STRING, 196);
        pnd_C_Tape_Pnd_C_Sequence_Number = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Sequence_Number", "#C-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_C_Tape_Pnd_C_Filler_4 = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Filler_4", "#C-FILLER-4", FieldType.STRING, 241);
        pnd_C_Tape_Pnd_C_Blank_Cr_Lf = pnd_C_Tape.newFieldInGroup("pnd_C_Tape_Pnd_C_Blank_Cr_Lf", "#C-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_C_TapeRedef1 = newGroupInRecord("pnd_C_TapeRedef1", "Redefines", pnd_C_Tape);
        pnd_C_Tape_Pnd_C_Move_1 = pnd_C_TapeRedef1.newFieldInGroup("pnd_C_Tape_Pnd_C_Move_1", "#C-MOVE-1", FieldType.STRING, 250);
        pnd_C_Tape_Pnd_C_Move_2 = pnd_C_TapeRedef1.newFieldInGroup("pnd_C_Tape_Pnd_C_Move_2", "#C-MOVE-2", FieldType.STRING, 250);
        pnd_C_Tape_Pnd_C_Move_3 = pnd_C_TapeRedef1.newFieldInGroup("pnd_C_Tape_Pnd_C_Move_3", "#C-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443d");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_C_Tape_Pnd_C_Record_Type.setInitialValue("C");
        pnd_C_Tape_Pnd_C_Filler_1.setInitialValue(" ");
        pnd_C_Tape_Pnd_C_Total_1.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_2.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_3.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_4.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_5.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_6.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_7.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_8.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_9.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_A.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_B.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_C.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_D.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_E.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_F.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Total_G.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Filler_2.setInitialValue(" ");
        pnd_C_Tape_Pnd_C_Sequence_Number.setInitialValue(0);
        pnd_C_Tape_Pnd_C_Filler_4.setInitialValue(" ");
        pnd_C_Tape_Pnd_C_Blank_Cr_Lf.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443d() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
