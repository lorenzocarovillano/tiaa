/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:31 PM
**        * FROM NATURAL LDA     : TWRL3502
************************************************************
**        * FILE NAME            : LdaTwrl3502.java
**        * CLASS NAME           : LdaTwrl3502
**        * INSTANCE NAME        : LdaTwrl3502
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3502 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Out_Irs_Record;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Rec_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Pay_Year;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Combine;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Filler_1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Tin;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Control_Code;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Filing_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Ret_Type;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Amount_Code;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Blank1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Foreign_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Trans_Name;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Trans_Name1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Trans_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Company_Address;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Company_City;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Company_State;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Company_Zip;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Phone;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Blank2b;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_State_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Blank3;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Blank4;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Seq_Num;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Blank5;
    private DbsField pnd_Out_Irs_Record_Pnd_Outa_Crlf;
    private DbsGroup pnd_Out_Irs_RecordRedef1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Rec_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Year;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Corr_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Name_Control;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Tin_Type;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Tin;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Acct;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Office_Cde;
    private DbsField filler01;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amta;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amte;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name;
    private DbsField filler02;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Payee_Address;
    private DbsField filler03;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Payee_City;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Payee_State;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Payee_Zip;
    private DbsField filler04;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Seq_Num;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2;
    private DbsField filler05;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Tax_Id_Removed;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Dist_Code;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Total_Dist;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Per_Dist;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Fatca_Filing_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment;
    private DbsField filler06;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Special_Data;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Filler;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_State_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_Comp_Acct_No;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_Personal_Ser;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_Vested;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_Country;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_Prim_Res;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Winning;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Income;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Oh_Nd_Mt_State_Ind;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Md_Fill1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Md_Fill2;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Or_State_Wthhld_Ssn;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind;
    private DbsField filler07;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id;
    private DbsField filler08;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill3;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_In_County;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt;
    private DbsField filler09;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id;
    private DbsField filler10;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34;
    private DbsField filler11;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt;
    private DbsField filler12;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_State_Tax;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_State_Tax_A;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Local_Tax;
    private DbsGroup pnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A;
    private DbsField pnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code;
    private DbsField filler13;
    private DbsGroup pnd_Out_Irs_RecordRedef39;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Rec_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_No_Of_Payer;
    private DbsField filler14;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt2;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt3;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt4;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt5;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt6;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt7;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt8;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt9;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amta;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtb;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtc;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtd;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amte;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtf;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtg;
    private DbsField filler15;
    private DbsField pnd_Out_Irs_Record_Pnd_Outc_Seq_Num;
    private DbsField filler16;
    private DbsField filler17;
    private DbsGroup pnd_Out_Irs_RecordRedef40;
    private DbsField pnd_Out_Irs_Record_Pnd_Outf_Rec_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outf_Num_Of_A;
    private DbsField pnd_Out_Irs_Record_Pnd_Outf_Zero;
    private DbsField filler18;
    private DbsField pnd_Out_Irs_Record_Pnd_Outf_Tot_Payees;
    private DbsField filler19;
    private DbsField filler20;
    private DbsField pnd_Out_Irs_Record_Pnd_Outf_Seq_Num;
    private DbsField filler21;
    private DbsField filler22;
    private DbsGroup pnd_Out_Irs_RecordRedef41;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Rec_Id;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Pay_Year;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Prior_Year_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Tin;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Control_Code;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Replacement;
    private DbsField filler23;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Test_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Foreign_Ind;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Trans_Name;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Trans_Name1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_Name;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_Name1;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_Address;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_City;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_State;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Company_Zip;
    private DbsField filler24;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Tot_Payer;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Contact_Name;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Contact_Phone;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Contact_Email_Addr;
    private DbsField filler25;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Seq_Num;
    private DbsField filler26;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Vendor_Ind;
    private DbsField filler27;
    private DbsField pnd_Out_Irs_Record_Pnd_Outt_Crlf;
    private DbsGroup pnd_Out_Irs_RecordRedef42;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec1;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec2;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec3;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec4;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec5;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec6;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec7;
    private DbsField pnd_Out_Irs_Record_Pnd_Out_Irs_Rec8;

    public DbsGroup getPnd_Out_Irs_Record() { return pnd_Out_Irs_Record; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Rec_Id() { return pnd_Out_Irs_Record_Pnd_Outa_Rec_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Pay_Year() { return pnd_Out_Irs_Record_Pnd_Outa_Pay_Year; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Combine() { return pnd_Out_Irs_Record_Pnd_Outa_Combine; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Filler_1() { return pnd_Out_Irs_Record_Pnd_Outa_Filler_1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Tin() { return pnd_Out_Irs_Record_Pnd_Outa_Tin; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Control_Code() { return pnd_Out_Irs_Record_Pnd_Outa_Control_Code; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Filing_Ind() { return pnd_Out_Irs_Record_Pnd_Outa_Filing_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Ret_Type() { return pnd_Out_Irs_Record_Pnd_Outa_Ret_Type; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Amount_Code() { return pnd_Out_Irs_Record_Pnd_Outa_Amount_Code; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Blank1() { return pnd_Out_Irs_Record_Pnd_Outa_Blank1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Foreign_Ind() { return pnd_Out_Irs_Record_Pnd_Outa_Foreign_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Trans_Name() { return pnd_Out_Irs_Record_Pnd_Outa_Trans_Name; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Trans_Name1() { return pnd_Out_Irs_Record_Pnd_Outa_Trans_Name1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Trans_Ind() { return pnd_Out_Irs_Record_Pnd_Outa_Trans_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Company_Address() { return pnd_Out_Irs_Record_Pnd_Outa_Company_Address; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Company_City() { return pnd_Out_Irs_Record_Pnd_Outa_Company_City; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Company_State() { return pnd_Out_Irs_Record_Pnd_Outa_Company_State; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Company_Zip() { return pnd_Out_Irs_Record_Pnd_Outa_Company_Zip; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Phone() { return pnd_Out_Irs_Record_Pnd_Outa_Phone; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Blank2b() { return pnd_Out_Irs_Record_Pnd_Outa_Blank2b; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_State_Id() { return pnd_Out_Irs_Record_Pnd_Outa_State_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Blank3() { return pnd_Out_Irs_Record_Pnd_Outa_Blank3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Blank4() { return pnd_Out_Irs_Record_Pnd_Outa_Blank4; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Seq_Num() { return pnd_Out_Irs_Record_Pnd_Outa_Seq_Num; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Blank5() { return pnd_Out_Irs_Record_Pnd_Outa_Blank5; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outa_Crlf() { return pnd_Out_Irs_Record_Pnd_Outa_Crlf; }

    public DbsGroup getPnd_Out_Irs_RecordRedef1() { return pnd_Out_Irs_RecordRedef1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Rec_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Rec_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Year() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Year; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Corr_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Corr_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Name_Control() { return pnd_Out_Irs_Record_Pnd_Outb_Name_Control; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Tin_Type() { return pnd_Out_Irs_Record_Pnd_Outb_Tin_Type; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Tin() { return pnd_Out_Irs_Record_Pnd_Outb_Tin; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Acct() { return pnd_Out_Irs_Record_Pnd_Outb_Acct; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Office_Cde() { return pnd_Out_Irs_Record_Pnd_Outb_Office_Cde; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amta() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amta; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amte() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amte; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name() { return pnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name() { return pnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Payee_Address() { return pnd_Out_Irs_Record_Pnd_Outb_Payee_Address; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Payee_City() { return pnd_Out_Irs_Record_Pnd_Outb_Payee_City; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Payee_State() { return pnd_Out_Irs_Record_Pnd_Outb_Payee_State; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Payee_Zip() { return pnd_Out_Irs_Record_Pnd_Outb_Payee_Zip; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Seq_Num() { return pnd_Out_Irs_Record_Pnd_Outb_Seq_Num; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Special_Data_1() { return pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2() { return pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2; }

    public DbsField getFiller05() { return filler05; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_State_Tax_Id_Removed() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Tax_Id_Removed; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Dist_Code() { return pnd_Out_Irs_Record_Pnd_Outb_Dist_Code; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det() { return pnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Total_Dist() { return pnd_Out_Irs_Record_Pnd_Outb_Total_Dist; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Per_Dist() { return pnd_Out_Irs_Record_Pnd_Outb_Per_Dist; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib() { return pnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Fatca_Filing_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Fatca_Filing_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment() { return pnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Special_Data() { return pnd_Out_Irs_Record_Pnd_Outb_Special_Data; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Filler() { return pnd_Out_Irs_Record_Pnd_Outb_Filler; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_State_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_Comp_Acct_No() { return pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_Comp_Acct_No; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_Personal_Ser() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_Personal_Ser; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_Vested() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_Vested; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_Country() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_Country; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_Prim_Res() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_Prim_Res; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_State_Winning() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Winning; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Nc_State_Income() { return pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Income; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Oh_Nd_Mt_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Oh_Nd_Mt_State_Ind; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Md_Fill1() { return pnd_Out_Irs_Record_Pnd_Outb_Md_Fill1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup() { return pnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Md_Fill2() { return pnd_Out_Irs_Record_Pnd_Outb_Md_Fill2; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Or_State_Wthhld_Ssn() { return pnd_Out_Irs_Record_Pnd_Outb_Or_State_Wthhld_Ssn; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind; }

    public DbsField getFiller07() { return filler07; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id; }

    public DbsField getFiller08() { return filler08; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde() { return pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1() { return pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2() { return pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ne_Fill3() { return pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind() { return pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib() { return pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_In_County() { return pnd_Out_Irs_Record_Pnd_Outb_In_County; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No() { return pnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt() { return pnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt; }

    public DbsField getFiller09() { return filler09; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code() { return pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id; }

    public DbsField getFiller10() { return filler10; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34; }

    public DbsField getFiller11() { return filler11; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id() { return pnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36() { return pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt() { return pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt; }

    public DbsField getFiller12() { return filler12; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld() { return pnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries() { return pnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_State_Tax() { return pnd_Out_Irs_Record_Pnd_Outb_State_Tax; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37() { return pnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_State_Tax_A() { return pnd_Out_Irs_Record_Pnd_Outb_State_Tax_A; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax() { return pnd_Out_Irs_Record_Pnd_Outb_Local_Tax; }

    public DbsGroup getPnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38() { return pnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A() { return pnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code() { return pnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code; }

    public DbsField getFiller13() { return filler13; }

    public DbsGroup getPnd_Out_Irs_RecordRedef39() { return pnd_Out_Irs_RecordRedef39; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Rec_Id() { return pnd_Out_Irs_Record_Pnd_Outc_Rec_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_No_Of_Payer() { return pnd_Out_Irs_Record_Pnd_Outc_No_Of_Payer; }

    public DbsField getFiller14() { return filler14; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt1() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt2() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt2; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt3() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt4() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt4; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt5() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt5; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt6() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt6; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt7() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt7; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt8() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt8; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amt9() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt9; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amta() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amta; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amtb() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtb; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amtc() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtc; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amtd() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtd; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amte() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amte; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amtf() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtf; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Pay_Amtg() { return pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtg; }

    public DbsField getFiller15() { return filler15; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outc_Seq_Num() { return pnd_Out_Irs_Record_Pnd_Outc_Seq_Num; }

    public DbsField getFiller16() { return filler16; }

    public DbsField getFiller17() { return filler17; }

    public DbsGroup getPnd_Out_Irs_RecordRedef40() { return pnd_Out_Irs_RecordRedef40; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outf_Rec_Id() { return pnd_Out_Irs_Record_Pnd_Outf_Rec_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outf_Num_Of_A() { return pnd_Out_Irs_Record_Pnd_Outf_Num_Of_A; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outf_Zero() { return pnd_Out_Irs_Record_Pnd_Outf_Zero; }

    public DbsField getFiller18() { return filler18; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outf_Tot_Payees() { return pnd_Out_Irs_Record_Pnd_Outf_Tot_Payees; }

    public DbsField getFiller19() { return filler19; }

    public DbsField getFiller20() { return filler20; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outf_Seq_Num() { return pnd_Out_Irs_Record_Pnd_Outf_Seq_Num; }

    public DbsField getFiller21() { return filler21; }

    public DbsField getFiller22() { return filler22; }

    public DbsGroup getPnd_Out_Irs_RecordRedef41() { return pnd_Out_Irs_RecordRedef41; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Rec_Id() { return pnd_Out_Irs_Record_Pnd_Outt_Rec_Id; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Pay_Year() { return pnd_Out_Irs_Record_Pnd_Outt_Pay_Year; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Prior_Year_Ind() { return pnd_Out_Irs_Record_Pnd_Outt_Prior_Year_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Tin() { return pnd_Out_Irs_Record_Pnd_Outt_Tin; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Control_Code() { return pnd_Out_Irs_Record_Pnd_Outt_Control_Code; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Replacement() { return pnd_Out_Irs_Record_Pnd_Outt_Replacement; }

    public DbsField getFiller23() { return filler23; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Test_Ind() { return pnd_Out_Irs_Record_Pnd_Outt_Test_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Foreign_Ind() { return pnd_Out_Irs_Record_Pnd_Outt_Foreign_Ind; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Trans_Name() { return pnd_Out_Irs_Record_Pnd_Outt_Trans_Name; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Trans_Name1() { return pnd_Out_Irs_Record_Pnd_Outt_Trans_Name1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_Name() { return pnd_Out_Irs_Record_Pnd_Outt_Company_Name; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_Name1() { return pnd_Out_Irs_Record_Pnd_Outt_Company_Name1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_Address() { return pnd_Out_Irs_Record_Pnd_Outt_Company_Address; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_City() { return pnd_Out_Irs_Record_Pnd_Outt_Company_City; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_State() { return pnd_Out_Irs_Record_Pnd_Outt_Company_State; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Company_Zip() { return pnd_Out_Irs_Record_Pnd_Outt_Company_Zip; }

    public DbsField getFiller24() { return filler24; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Tot_Payer() { return pnd_Out_Irs_Record_Pnd_Outt_Tot_Payer; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Contact_Name() { return pnd_Out_Irs_Record_Pnd_Outt_Contact_Name; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Contact_Phone() { return pnd_Out_Irs_Record_Pnd_Outt_Contact_Phone; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Contact_Email_Addr() { return pnd_Out_Irs_Record_Pnd_Outt_Contact_Email_Addr; }

    public DbsField getFiller25() { return filler25; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Seq_Num() { return pnd_Out_Irs_Record_Pnd_Outt_Seq_Num; }

    public DbsField getFiller26() { return filler26; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Vendor_Ind() { return pnd_Out_Irs_Record_Pnd_Outt_Vendor_Ind; }

    public DbsField getFiller27() { return filler27; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Outt_Crlf() { return pnd_Out_Irs_Record_Pnd_Outt_Crlf; }

    public DbsGroup getPnd_Out_Irs_RecordRedef42() { return pnd_Out_Irs_RecordRedef42; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec1() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec1; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec2() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec2; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec3() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec3; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec4() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec4; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec5() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec5; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec6() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec6; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec7() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec7; }

    public DbsField getPnd_Out_Irs_Record_Pnd_Out_Irs_Rec8() { return pnd_Out_Irs_Record_Pnd_Out_Irs_Rec8; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out_Irs_Record = newGroupInRecord("pnd_Out_Irs_Record", "#OUT-IRS-RECORD");
        pnd_Out_Irs_Record_Pnd_Outa_Rec_Id = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Rec_Id", "#OUTA-REC-ID", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outa_Pay_Year = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Pay_Year", "#OUTA-PAY-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Out_Irs_Record_Pnd_Outa_Combine = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Combine", "#OUTA-COMBINE", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outa_Filler_1 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Filler_1", "#OUTA-FILLER-1", FieldType.STRING, 
            5);
        pnd_Out_Irs_Record_Pnd_Outa_Tin = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Tin", "#OUTA-TIN", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outa_Control_Code = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Control_Code", "#OUTA-CONTROL-CODE", 
            FieldType.STRING, 4);
        pnd_Out_Irs_Record_Pnd_Outa_Filing_Ind = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Filing_Ind", "#OUTA-FILING-IND", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outa_Ret_Type = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Ret_Type", "#OUTA-RET-TYPE", FieldType.STRING, 
            2);
        pnd_Out_Irs_Record_Pnd_Outa_Amount_Code = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Amount_Code", "#OUTA-AMOUNT-CODE", FieldType.STRING, 
            16);
        pnd_Out_Irs_Record_Pnd_Outa_Blank1 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Blank1", "#OUTA-BLANK1", FieldType.STRING, 
            8);
        pnd_Out_Irs_Record_Pnd_Outa_Foreign_Ind = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Foreign_Ind", "#OUTA-FOREIGN-IND", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outa_Trans_Name = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Trans_Name", "#OUTA-TRANS-NAME", FieldType.STRING, 
            40);
        pnd_Out_Irs_Record_Pnd_Outa_Trans_Name1 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Trans_Name1", "#OUTA-TRANS-NAME1", FieldType.STRING, 
            40);
        pnd_Out_Irs_Record_Pnd_Outa_Trans_Ind = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Trans_Ind", "#OUTA-TRANS-IND", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outa_Company_Address = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Company_Address", "#OUTA-COMPANY-ADDRESS", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outa_Company_City = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Company_City", "#OUTA-COMPANY-CITY", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outa_Company_State = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Company_State", "#OUTA-COMPANY-STATE", 
            FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outa_Company_Zip = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Company_Zip", "#OUTA-COMPANY-ZIP", FieldType.STRING, 
            9);
        pnd_Out_Irs_Record_Pnd_Outa_Phone = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Phone", "#OUTA-PHONE", FieldType.STRING, 15);
        pnd_Out_Irs_Record_Pnd_Outa_Blank2b = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Blank2b", "#OUTA-BLANK2B", FieldType.STRING, 
            131);
        pnd_Out_Irs_Record_Pnd_Outa_State_Id = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_State_Id", "#OUTA-STATE-ID", FieldType.STRING, 
            9);
        pnd_Out_Irs_Record_Pnd_Outa_Blank3 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Blank3", "#OUTA-BLANK3", FieldType.STRING, 
            110);
        pnd_Out_Irs_Record_Pnd_Outa_Blank4 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Blank4", "#OUTA-BLANK4", FieldType.STRING, 
            10);
        pnd_Out_Irs_Record_Pnd_Outa_Seq_Num = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Seq_Num", "#OUTA-SEQ-NUM", FieldType.NUMERIC, 
            8);
        pnd_Out_Irs_Record_Pnd_Outa_Blank5 = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Blank5", "#OUTA-BLANK5", FieldType.STRING, 
            241);
        pnd_Out_Irs_Record_Pnd_Outa_Crlf = pnd_Out_Irs_Record.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outa_Crlf", "#OUTA-CRLF", FieldType.STRING, 2);
        pnd_Out_Irs_RecordRedef1 = newGroupInRecord("pnd_Out_Irs_RecordRedef1", "Redefines", pnd_Out_Irs_Record);
        pnd_Out_Irs_Record_Pnd_Outb_Rec_Id = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Rec_Id", "#OUTB-REC-ID", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Year = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Year", "#OUTB-PAY-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Out_Irs_Record_Pnd_Outb_Corr_Ind = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Corr_Ind", "#OUTB-CORR-IND", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outb_Name_Control = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Name_Control", "#OUTB-NAME-CONTROL", 
            FieldType.STRING, 4);
        pnd_Out_Irs_Record_Pnd_Outb_Tin_Type = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Tin_Type", "#OUTB-TIN-TYPE", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outb_Tin = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Tin", "#OUTB-TIN", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Acct = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Acct", "#OUTB-ACCT", FieldType.STRING, 
            20);
        pnd_Out_Irs_Record_Pnd_Outb_Office_Cde = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Office_Cde", "#OUTB-OFFICE-CDE", 
            FieldType.STRING, 4);
        filler01 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 10);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1", "#OUTB-PAY-AMT1", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt2", "#OUTB-PAY-AMT2", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt3", "#OUTB-PAY-AMT3", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt4", "#OUTB-PAY-AMT4", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt5", "#OUTB-PAY-AMT5", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt6", "#OUTB-PAY-AMT6", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt7", "#OUTB-PAY-AMT7", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt8", "#OUTB-PAY-AMT8", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt9", "#OUTB-PAY-AMT9", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amta = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amta", "#OUTB-PAY-AMTA", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtb", "#OUTB-PAY-AMTB", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtc", "#OUTB-PAY-AMTC", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtd", "#OUTB-PAY-AMTD", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amte = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amte", "#OUTB-PAY-AMTE", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtf", "#OUTB-PAY-AMTF", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amtg", "#OUTB-PAY-AMTG", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Foreign_Ind", "#OUTB-FOREIGN-IND", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_First_Payee_Name", "#OUTB-FIRST-PAYEE-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Sec_Payee_Name", "#OUTB-SEC-PAYEE-NAME", 
            FieldType.STRING, 40);
        filler02 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outb_Payee_Address = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Payee_Address", "#OUTB-PAYEE-ADDRESS", 
            FieldType.STRING, 40);
        filler03 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outb_Payee_City = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Payee_City", "#OUTB-PAYEE-CITY", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outb_Payee_State = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Payee_State", "#OUTB-PAYEE-STATE", 
            FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Payee_Zip = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Payee_Zip", "#OUTB-PAYEE-ZIP", FieldType.STRING, 
            9);
        filler04 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Seq_Num = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Seq_Num", "#OUTB-SEQ-NUM", FieldType.NUMERIC, 
            8);
        pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1", "#OUTB-SPECIAL-DATA-1", 
            FieldType.STRING, 37);
        pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1);
        filler05 = pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Tax_Id_Removed = pnd_Out_Irs_Record_Pnd_Outb_Special_Data_1Redef2.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Tax_Id_Removed", 
            "#OUTB-NC-STATE-TAX-ID-REMOVED", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Dist_Code = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Dist_Code", "#OUTB-DIST-CODE", FieldType.STRING, 
            2);
        pnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Taxable_Not_Det", "#OUTB-TAXABLE-NOT-DET", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ira_Sep_Ind", "#OUTB-IRA-SEP-IND", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Total_Dist = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Total_Dist", "#OUTB-TOTAL-DIST", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Per_Dist = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Per_Dist", "#OUTB-PER-DIST", FieldType.STRING, 
            2);
        pnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_1st_Year_Roth_Contrib", 
            "#OUTB-1ST-YEAR-ROTH-CONTRIB", FieldType.STRING, 4);
        pnd_Out_Irs_Record_Pnd_Outb_Fatca_Filing_Ind = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Fatca_Filing_Ind", "#OUTB-FATCA-FILING-IND", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Date_Of_Payment", "#OUTB-DATE-OF-PAYMENT", 
            FieldType.STRING, 8);
        filler06 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler06", "FILLER", FieldType.STRING, 98);
        pnd_Out_Irs_Record_Pnd_Outb_Special_Data = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_Data", "#OUTB-SPECIAL-DATA", 
            FieldType.STRING, 60);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Filler = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Filler", "#OUTB-FILLER", 
            FieldType.STRING, 51);
        pnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef3.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Sc_State_Ind", 
            "#OUTB-SC-STATE-IND", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef4.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Dc_State_Tax_Id", 
            "#OUTB-DC-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef5.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Mn_State_Tax_Id", 
            "#OUTB-MN-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef6.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_De_State_Tax_Id", 
            "#OUTB-DE-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef7.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Mt_State_Tax_Id", 
            "#OUTB-MT-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef8.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ga_State_Tax_Id", 
            "#OUTB-GA-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef9.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_La_State_Tax_Id", 
            "#OUTB-LA-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef10.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Oh_State_Tax_Id", 
            "#OUTB-OH-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef11.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Wv_State_Tax_Id", 
            "#OUTB-WV-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef12.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nd_State_Tax_Id", 
            "#OUTB-ND-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef13.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Va_State_Tax_Id", 
            "#OUTB-VA-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef14.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ms_State_Tax_Id", 
            "#OUTB-MS-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef15.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ks_State_Tax_Id", 
            "#OUTB-KS-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef16.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Co_State_Tax_Id", 
            "#OUTB-CO-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef17.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Az_State_Tax_Id", 
            "#OUTB-AZ-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_State_Ind", 
            "#OUTB-MI-MN-STATE-IND", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_Comp_Acct_No = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef18.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Mi_Mn_Comp_Acct_No", 
            "#OUTB-MI-MN-COMP-ACCT-NO", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef19.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Mi_State_Tax_Id", 
            "#OUTB-MI-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_Personal_Ser = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_Personal_Ser", 
            "#OUTB-NC-PERSONAL-SER", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_Comp_Acct_No", 
            "#OUTB-NC-COMP-ACCT-NO", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_Vested = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_Vested", 
            "#OUTB-NC-VESTED", FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_Country = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_Country", 
            "#OUTB-NC-COUNTRY", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_Prim_Res = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_Prim_Res", 
            "#OUTB-NC-PRIM-RES", FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Ind", 
            "#OUTB-NC-STATE-IND", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Winning = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Winning", 
            "#OUTB-NC-STATE-WINNING", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Income = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef20.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Nc_State_Income", 
            "#OUTB-NC-STATE-INCOME", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef21.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ok_State_Ind", 
            "#OUTB-OK-STATE-IND", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Oh_Nd_Mt_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef22.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Oh_Nd_Mt_State_Ind", 
            "#OUTB-OH-ND-MT-STATE-IND", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Md_Fill1 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Md_Fill1", 
            "#OUTB-MD-FILL1", FieldType.STRING, 38);
        pnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Md_State_Pickup", 
            "#OUTB-MD-STATE-PICKUP", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Md_Fill2 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Md_Fill2", 
            "#OUTB-MD-FILL2", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef23.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Md_State_Tax_Id", 
            "#OUTB-MD-STATE-TAX-ID", FieldType.STRING, 8);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef24.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ma_State_Tax_Id", 
            "#OUTB-MA-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Or_State_Tax_Id", 
            "#OUTB-OR-STATE-TAX-ID", FieldType.NUMERIC, 8);
        pnd_Out_Irs_Record_Pnd_Outb_Or_State_Wthhld_Ssn = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Or_State_Wthhld_Ssn", 
            "#OUTB-OR-STATE-WTHHLD-SSN", FieldType.STRING, 9);
        pnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Or_State_Ind", 
            "#OUTB-OR-STATE-IND", FieldType.STRING, 2);
        filler07 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef25.newFieldInGroup("filler07", "FILLER", FieldType.STRING, 41);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Tax_Id", 
            "#OUTB-WI-STATE-TAX-ID", FieldType.STRING, 15);
        filler08 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26.newFieldInGroup("filler08", "FILLER", FieldType.STRING, 5);
        pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef26.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Wi_State_Cde", 
            "#OUTB-WI-STATE-CDE", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef27.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ar_State_Tax_Id", 
            "#OUTB-AR-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef28.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Me_State_Tax_Id", 
            "#OUTB-ME-STATE-TAX-ID", FieldType.STRING, 20);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill1", 
            "#OUTB-NE-FILL1", FieldType.NUMERIC, 10);
        pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill2", 
            "#OUTB-NE-FILL2", FieldType.DECIMAL, 10,2);
        pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill3 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ne_Fill3", 
            "#OUTB-NE-FILL3", FieldType.NUMERIC, 10);
        pnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef29.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ne_State_Tax_Id", 
            "#OUTB-NE-STATE-TAX-ID", FieldType.STRING, 30);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Ind", 
            "#OUTB-UT-STATE-IND", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Tax_Id", 
            "#OUTB-UT-STATE-TAX-ID", FieldType.STRING, 14);
        pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef30.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ut_State_Distrib", 
            "#OUTB-UT-STATE-DISTRIB", FieldType.DECIMAL, 13,2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_In_County = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef31.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_In_County", 
            "#OUTB-IN-COUNTY", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ia_Withheld_Permit_No", 
            "#OUTB-IA-WITHHELD-PERMIT-NO", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ia_Other_Iowa_Amt", 
            "#OUTB-IA-OTHER-IOWA-AMT", FieldType.DECIMAL, 12,2);
        filler09 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef32.newFieldInGroup("filler09", "FILLER", FieldType.STRING, 36);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Code", 
            "#OUTB-KY-STATE-CODE", FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ky_State_Tax_Id", 
            "#OUTB-KY-STATE-TAX-ID", FieldType.STRING, 20);
        filler10 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef33.newFieldInGroup("filler10", "FILLER", FieldType.STRING, 38);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        filler11 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34.newFieldInGroup("filler11", "FILLER", FieldType.STRING, 49);
        pnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef34.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Ct_State_Tax_Id", 
            "#OUTB-CT-STATE-TAX-ID", FieldType.STRING, 11);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef35.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Vt_State_Tax_Id", 
            "#OUTB-VT-STATE-TAX-ID", FieldType.STRING, 15);
        pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36", 
            "Redefines", pnd_Out_Irs_Record_Pnd_Outb_Special_Data);
        pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Pay_Amt1_Alt", 
            "#OUTB-PAY-AMT1-ALT", FieldType.DECIMAL, 10,2);
        filler12 = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36.newFieldInGroup("filler12", "FILLER", FieldType.STRING, 10);
        pnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_State_Tax_Withheld", 
            "#OUTB-STATE-TAX-WITHHELD", FieldType.DECIMAL, 10,2);
        pnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries = pnd_Out_Irs_Record_Pnd_Outb_Special_DataRedef36.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Special_Data_Entries", 
            "#OUTB-SPECIAL-DATA-ENTRIES", FieldType.STRING, 30);
        pnd_Out_Irs_Record_Pnd_Outb_State_Tax = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_State_Tax", "#OUTB-STATE-TAX", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_State_Tax);
        pnd_Out_Irs_Record_Pnd_Outb_State_Tax_A = pnd_Out_Irs_Record_Pnd_Outb_State_TaxRedef37.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_State_Tax_A", 
            "#OUTB-STATE-TAX-A", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Local_Tax = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Local_Tax", "#OUTB-LOCAL-TAX", FieldType.DECIMAL, 
            12,2);
        pnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38 = pnd_Out_Irs_RecordRedef1.newGroupInGroup("pnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38", "Redefines", 
            pnd_Out_Irs_Record_Pnd_Outb_Local_Tax);
        pnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A = pnd_Out_Irs_Record_Pnd_Outb_Local_TaxRedef38.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Local_Tax_A", 
            "#OUTB-LOCAL-TAX-A", FieldType.STRING, 12);
        pnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code = pnd_Out_Irs_RecordRedef1.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outb_Cmb_State_Code", "#OUTB-CMB-STATE-CODE", 
            FieldType.STRING, 2);
        filler13 = pnd_Out_Irs_RecordRedef1.newFieldInGroup("filler13", "FILLER", FieldType.STRING, 2);
        pnd_Out_Irs_RecordRedef39 = newGroupInRecord("pnd_Out_Irs_RecordRedef39", "Redefines", pnd_Out_Irs_Record);
        pnd_Out_Irs_Record_Pnd_Outc_Rec_Id = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Rec_Id", "#OUTC-REC-ID", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outc_No_Of_Payer = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_No_Of_Payer", "#OUTC-NO-OF-PAYER", 
            FieldType.NUMERIC, 8);
        filler14 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("filler14", "FILLER", FieldType.STRING, 6);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt1 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt1", "#OUTC-PAY-AMT1", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt2 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt2", "#OUTC-PAY-AMT2", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt3 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt3", "#OUTC-PAY-AMT3", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt4 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt4", "#OUTC-PAY-AMT4", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt5 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt5", "#OUTC-PAY-AMT5", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt6 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt6", "#OUTC-PAY-AMT6", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt7 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt7", "#OUTC-PAY-AMT7", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt8 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt8", "#OUTC-PAY-AMT8", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt9 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amt9", "#OUTC-PAY-AMT9", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amta = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amta", "#OUTC-PAY-AMTA", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtb = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtb", "#OUTC-PAY-AMTB", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtc = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtc", "#OUTC-PAY-AMTC", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtd = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtd", "#OUTC-PAY-AMTD", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amte = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amte", "#OUTC-PAY-AMTE", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtf = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtf", "#OUTC-PAY-AMTF", FieldType.DECIMAL, 
            18,2);
        pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtg = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Pay_Amtg", "#OUTC-PAY-AMTG", FieldType.DECIMAL, 
            18,2);
        filler15 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("filler15", "FILLER", FieldType.STRING, 196);
        pnd_Out_Irs_Record_Pnd_Outc_Seq_Num = pnd_Out_Irs_RecordRedef39.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outc_Seq_Num", "#OUTC-SEQ-NUM", FieldType.NUMERIC, 
            8);
        filler16 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("filler16", "FILLER", FieldType.STRING, 241);
        filler17 = pnd_Out_Irs_RecordRedef39.newFieldInGroup("filler17", "FILLER", FieldType.STRING, 2);
        pnd_Out_Irs_RecordRedef40 = newGroupInRecord("pnd_Out_Irs_RecordRedef40", "Redefines", pnd_Out_Irs_Record);
        pnd_Out_Irs_Record_Pnd_Outf_Rec_Id = pnd_Out_Irs_RecordRedef40.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outf_Rec_Id", "#OUTF-REC-ID", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outf_Num_Of_A = pnd_Out_Irs_RecordRedef40.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outf_Num_Of_A", "#OUTF-NUM-OF-A", FieldType.NUMERIC, 
            8);
        pnd_Out_Irs_Record_Pnd_Outf_Zero = pnd_Out_Irs_RecordRedef40.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outf_Zero", "#OUTF-ZERO", FieldType.STRING, 
            21);
        filler18 = pnd_Out_Irs_RecordRedef40.newFieldInGroup("filler18", "FILLER", FieldType.STRING, 19);
        pnd_Out_Irs_Record_Pnd_Outf_Tot_Payees = pnd_Out_Irs_RecordRedef40.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outf_Tot_Payees", "#OUTF-TOT-PAYEES", 
            FieldType.NUMERIC, 8);
        filler19 = pnd_Out_Irs_RecordRedef40.newFieldInGroup("filler19", "FILLER", FieldType.STRING, 200);
        filler20 = pnd_Out_Irs_RecordRedef40.newFieldInGroup("filler20", "FILLER", FieldType.STRING, 242);
        pnd_Out_Irs_Record_Pnd_Outf_Seq_Num = pnd_Out_Irs_RecordRedef40.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outf_Seq_Num", "#OUTF-SEQ-NUM", FieldType.NUMERIC, 
            8);
        filler21 = pnd_Out_Irs_RecordRedef40.newFieldInGroup("filler21", "FILLER", FieldType.STRING, 241);
        filler22 = pnd_Out_Irs_RecordRedef40.newFieldInGroup("filler22", "FILLER", FieldType.STRING, 2);
        pnd_Out_Irs_RecordRedef41 = newGroupInRecord("pnd_Out_Irs_RecordRedef41", "Redefines", pnd_Out_Irs_Record);
        pnd_Out_Irs_Record_Pnd_Outt_Rec_Id = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Rec_Id", "#OUTT-REC-ID", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outt_Pay_Year = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Pay_Year", "#OUTT-PAY-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Out_Irs_Record_Pnd_Outt_Prior_Year_Ind = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Prior_Year_Ind", "#OUTT-PRIOR-YEAR-IND", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outt_Tin = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Tin", "#OUTT-TIN", FieldType.STRING, 
            9);
        pnd_Out_Irs_Record_Pnd_Outt_Control_Code = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Control_Code", "#OUTT-CONTROL-CODE", 
            FieldType.STRING, 5);
        pnd_Out_Irs_Record_Pnd_Outt_Replacement = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Replacement", "#OUTT-REPLACEMENT", 
            FieldType.STRING, 2);
        filler23 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("filler23", "FILLER", FieldType.STRING, 5);
        pnd_Out_Irs_Record_Pnd_Outt_Test_Ind = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Test_Ind", "#OUTT-TEST-IND", FieldType.STRING, 
            1);
        pnd_Out_Irs_Record_Pnd_Outt_Foreign_Ind = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Foreign_Ind", "#OUTT-FOREIGN-IND", 
            FieldType.STRING, 1);
        pnd_Out_Irs_Record_Pnd_Outt_Trans_Name = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Trans_Name", "#OUTT-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Trans_Name1 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Trans_Name1", "#OUTT-TRANS-NAME1", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Company_Name = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_Name", "#OUTT-COMPANY-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Company_Name1 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_Name1", "#OUTT-COMPANY-NAME1", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Company_Address = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_Address", "#OUTT-COMPANY-ADDRESS", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Company_City = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_City", "#OUTT-COMPANY-CITY", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Company_State = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_State", "#OUTT-COMPANY-STATE", 
            FieldType.STRING, 2);
        pnd_Out_Irs_Record_Pnd_Outt_Company_Zip = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Company_Zip", "#OUTT-COMPANY-ZIP", 
            FieldType.STRING, 9);
        filler24 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("filler24", "FILLER", FieldType.STRING, 15);
        pnd_Out_Irs_Record_Pnd_Outt_Tot_Payer = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Tot_Payer", "#OUTT-TOT-PAYER", 
            FieldType.NUMERIC, 8);
        pnd_Out_Irs_Record_Pnd_Outt_Contact_Name = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Contact_Name", "#OUTT-CONTACT-NAME", 
            FieldType.STRING, 40);
        pnd_Out_Irs_Record_Pnd_Outt_Contact_Phone = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Contact_Phone", "#OUTT-CONTACT-PHONE", 
            FieldType.STRING, 15);
        pnd_Out_Irs_Record_Pnd_Outt_Contact_Email_Addr = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Contact_Email_Addr", "#OUTT-CONTACT-EMAIL-ADDR", 
            FieldType.STRING, 50);
        filler25 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("filler25", "FILLER", FieldType.STRING, 91);
        pnd_Out_Irs_Record_Pnd_Outt_Seq_Num = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Seq_Num", "#OUTT-SEQ-NUM", FieldType.NUMERIC, 
            8);
        filler26 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("filler26", "FILLER", FieldType.STRING, 10);
        pnd_Out_Irs_Record_Pnd_Outt_Vendor_Ind = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Vendor_Ind", "#OUTT-VENDOR-IND", 
            FieldType.STRING, 1);
        filler27 = pnd_Out_Irs_RecordRedef41.newFieldInGroup("filler27", "FILLER", FieldType.STRING, 230);
        pnd_Out_Irs_Record_Pnd_Outt_Crlf = pnd_Out_Irs_RecordRedef41.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Outt_Crlf", "#OUTT-CRLF", FieldType.STRING, 
            2);
        pnd_Out_Irs_RecordRedef42 = newGroupInRecord("pnd_Out_Irs_RecordRedef42", "Redefines", pnd_Out_Irs_Record);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec1 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec1", "#OUT-IRS-REC1", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec2 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec2", "#OUT-IRS-REC2", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec3 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec3", "#OUT-IRS-REC3", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec4 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec4", "#OUT-IRS-REC4", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec5 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec5", "#OUT-IRS-REC5", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec6 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec6", "#OUT-IRS-REC6", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec7 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec7", "#OUT-IRS-REC7", FieldType.STRING, 
            100);
        pnd_Out_Irs_Record_Pnd_Out_Irs_Rec8 = pnd_Out_Irs_RecordRedef42.newFieldInGroup("pnd_Out_Irs_Record_Pnd_Out_Irs_Rec8", "#OUT-IRS-REC8", FieldType.STRING, 
            50);

        this.setRecordName("LdaTwrl3502");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3502() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
