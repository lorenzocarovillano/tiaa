/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:48 PM
**        * FROM NATURAL LDA     : TWRL462C
************************************************************
**        * FILE NAME            : LdaTwrl462c.java
**        * CLASS NAME           : LdaTwrl462c
**        * INSTANCE NAME        : LdaTwrl462c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl462c extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ira;
    private DbsField ira_Twrc_Tax_Year;
    private DbsField ira_Twrc_Company_Cde;
    private DbsField ira_Twrc_Status;
    private DbsField ira_Twrc_Simulation_Date;
    private DbsField ira_Twrc_Citation_Ind;
    private DbsGroup ira_Twrc_Contributor;
    private DbsField ira_Twrc_Tax_Id_Type;
    private DbsField ira_Twrc_Tax_Id;
    private DbsField ira_Twrc_Contract;
    private DbsField ira_Twrc_Payee;
    private DbsField ira_Twrc_Contract_Xref;
    private DbsField ira_Twrc_Citizen_Code;
    private DbsField ira_Twrc_Residency_Code;
    private DbsField ira_Twrc_Residency_Type;
    private DbsField ira_Twrc_Locality_Cde;
    private DbsField ira_Twrc_Dob;
    private DbsField ira_Twrc_Dod;
    private DbsGroup ira_Twrc_Form_Info;
    private DbsField ira_Twrc_Pin;
    private DbsField ira_Twrc_Ira_Contract_Type;
    private DbsField ira_Twrc_Source;
    private DbsField ira_Twrc_Update_Source;
    private DbsField ira_Twrc_Form_Period;
    private DbsField ira_Twrc_Additions_1;
    private DbsGroup ira_Twrc_Additions_1Redef1;
    private DbsField ira_Twrc_Error_Reason;
    private DbsField ira_Twrc_Orign_Area;
    private DbsGroup ira_Twrc_Contributions;
    private DbsField ira_Twrc_Classic_Amt;
    private DbsField ira_Twrc_Roth_Amt;
    private DbsField ira_Twrc_Rollover_Amt;
    private DbsField ira_Twrc_Rechar_Amt;
    private DbsField ira_Twrc_Sep_Amt;
    private DbsField ira_Twrc_Fair_Mkt_Val_Amt;
    private DbsField ira_Twrc_Roth_Conversion_Amt;
    private DbsGroup ira_Twrc_Name_Addr;
    private DbsField ira_Twrc_Name;
    private DbsField ira_Twrc_Addr_1;
    private DbsField ira_Twrc_Addr_2;
    private DbsField ira_Twrc_Addr_3;
    private DbsField ira_Twrc_Addr_4;
    private DbsField ira_Twrc_Addr_5;
    private DbsField ira_Twrc_Addr_6;
    private DbsField ira_Twrc_City;
    private DbsField ira_Twrc_State;
    private DbsField ira_Twrc_Zip_Code;
    private DbsField ira_Twrc_Addr_Foreign;
    private DbsGroup ira_Twrc_Audit_Trail;
    private DbsField ira_Twrc_Comment1;
    private DbsField ira_Twrc_Comment2;
    private DbsField ira_Twrc_Comment_User;
    private DbsField ira_Twrc_Comment_Date;
    private DbsField ira_Twrc_Comment_Time;
    private DbsField ira_Twrc_Create_User;
    private DbsField ira_Twrc_Create_Date;
    private DbsField ira_Twrc_Create_Date_Inv;
    private DbsField ira_Twrc_Create_Time;
    private DbsField ira_Twrc_Create_Time_Inv;
    private DbsField ira_Twrc_Update_User;
    private DbsField ira_Twrc_Update_Date;
    private DbsField ira_Twrc_Update_Time;
    private DbsField ira_Twrc_Unique_Id;
    private DbsField ira_Twrc_Transfer_From;
    private DbsField ira_Twrc_Transfer_To;
    private DbsField ira_Twrc_Wpid;
    private DbsField ira_Twrc_Lu_Ts;
    private DbsField ira_Twrc_Rmd_Required_Flag;
    private DbsField ira_Twrc_Rmd_Decedent_Name;

    public DataAccessProgramView getVw_ira() { return vw_ira; }

    public DbsField getIra_Twrc_Tax_Year() { return ira_Twrc_Tax_Year; }

    public DbsField getIra_Twrc_Company_Cde() { return ira_Twrc_Company_Cde; }

    public DbsField getIra_Twrc_Status() { return ira_Twrc_Status; }

    public DbsField getIra_Twrc_Simulation_Date() { return ira_Twrc_Simulation_Date; }

    public DbsField getIra_Twrc_Citation_Ind() { return ira_Twrc_Citation_Ind; }

    public DbsGroup getIra_Twrc_Contributor() { return ira_Twrc_Contributor; }

    public DbsField getIra_Twrc_Tax_Id_Type() { return ira_Twrc_Tax_Id_Type; }

    public DbsField getIra_Twrc_Tax_Id() { return ira_Twrc_Tax_Id; }

    public DbsField getIra_Twrc_Contract() { return ira_Twrc_Contract; }

    public DbsField getIra_Twrc_Payee() { return ira_Twrc_Payee; }

    public DbsField getIra_Twrc_Contract_Xref() { return ira_Twrc_Contract_Xref; }

    public DbsField getIra_Twrc_Citizen_Code() { return ira_Twrc_Citizen_Code; }

    public DbsField getIra_Twrc_Residency_Code() { return ira_Twrc_Residency_Code; }

    public DbsField getIra_Twrc_Residency_Type() { return ira_Twrc_Residency_Type; }

    public DbsField getIra_Twrc_Locality_Cde() { return ira_Twrc_Locality_Cde; }

    public DbsField getIra_Twrc_Dob() { return ira_Twrc_Dob; }

    public DbsField getIra_Twrc_Dod() { return ira_Twrc_Dod; }

    public DbsGroup getIra_Twrc_Form_Info() { return ira_Twrc_Form_Info; }

    public DbsField getIra_Twrc_Pin() { return ira_Twrc_Pin; }

    public DbsField getIra_Twrc_Ira_Contract_Type() { return ira_Twrc_Ira_Contract_Type; }

    public DbsField getIra_Twrc_Source() { return ira_Twrc_Source; }

    public DbsField getIra_Twrc_Update_Source() { return ira_Twrc_Update_Source; }

    public DbsField getIra_Twrc_Form_Period() { return ira_Twrc_Form_Period; }

    public DbsField getIra_Twrc_Additions_1() { return ira_Twrc_Additions_1; }

    public DbsGroup getIra_Twrc_Additions_1Redef1() { return ira_Twrc_Additions_1Redef1; }

    public DbsField getIra_Twrc_Error_Reason() { return ira_Twrc_Error_Reason; }

    public DbsField getIra_Twrc_Orign_Area() { return ira_Twrc_Orign_Area; }

    public DbsGroup getIra_Twrc_Contributions() { return ira_Twrc_Contributions; }

    public DbsField getIra_Twrc_Classic_Amt() { return ira_Twrc_Classic_Amt; }

    public DbsField getIra_Twrc_Roth_Amt() { return ira_Twrc_Roth_Amt; }

    public DbsField getIra_Twrc_Rollover_Amt() { return ira_Twrc_Rollover_Amt; }

    public DbsField getIra_Twrc_Rechar_Amt() { return ira_Twrc_Rechar_Amt; }

    public DbsField getIra_Twrc_Sep_Amt() { return ira_Twrc_Sep_Amt; }

    public DbsField getIra_Twrc_Fair_Mkt_Val_Amt() { return ira_Twrc_Fair_Mkt_Val_Amt; }

    public DbsField getIra_Twrc_Roth_Conversion_Amt() { return ira_Twrc_Roth_Conversion_Amt; }

    public DbsGroup getIra_Twrc_Name_Addr() { return ira_Twrc_Name_Addr; }

    public DbsField getIra_Twrc_Name() { return ira_Twrc_Name; }

    public DbsField getIra_Twrc_Addr_1() { return ira_Twrc_Addr_1; }

    public DbsField getIra_Twrc_Addr_2() { return ira_Twrc_Addr_2; }

    public DbsField getIra_Twrc_Addr_3() { return ira_Twrc_Addr_3; }

    public DbsField getIra_Twrc_Addr_4() { return ira_Twrc_Addr_4; }

    public DbsField getIra_Twrc_Addr_5() { return ira_Twrc_Addr_5; }

    public DbsField getIra_Twrc_Addr_6() { return ira_Twrc_Addr_6; }

    public DbsField getIra_Twrc_City() { return ira_Twrc_City; }

    public DbsField getIra_Twrc_State() { return ira_Twrc_State; }

    public DbsField getIra_Twrc_Zip_Code() { return ira_Twrc_Zip_Code; }

    public DbsField getIra_Twrc_Addr_Foreign() { return ira_Twrc_Addr_Foreign; }

    public DbsGroup getIra_Twrc_Audit_Trail() { return ira_Twrc_Audit_Trail; }

    public DbsField getIra_Twrc_Comment1() { return ira_Twrc_Comment1; }

    public DbsField getIra_Twrc_Comment2() { return ira_Twrc_Comment2; }

    public DbsField getIra_Twrc_Comment_User() { return ira_Twrc_Comment_User; }

    public DbsField getIra_Twrc_Comment_Date() { return ira_Twrc_Comment_Date; }

    public DbsField getIra_Twrc_Comment_Time() { return ira_Twrc_Comment_Time; }

    public DbsField getIra_Twrc_Create_User() { return ira_Twrc_Create_User; }

    public DbsField getIra_Twrc_Create_Date() { return ira_Twrc_Create_Date; }

    public DbsField getIra_Twrc_Create_Date_Inv() { return ira_Twrc_Create_Date_Inv; }

    public DbsField getIra_Twrc_Create_Time() { return ira_Twrc_Create_Time; }

    public DbsField getIra_Twrc_Create_Time_Inv() { return ira_Twrc_Create_Time_Inv; }

    public DbsField getIra_Twrc_Update_User() { return ira_Twrc_Update_User; }

    public DbsField getIra_Twrc_Update_Date() { return ira_Twrc_Update_Date; }

    public DbsField getIra_Twrc_Update_Time() { return ira_Twrc_Update_Time; }

    public DbsField getIra_Twrc_Unique_Id() { return ira_Twrc_Unique_Id; }

    public DbsField getIra_Twrc_Transfer_From() { return ira_Twrc_Transfer_From; }

    public DbsField getIra_Twrc_Transfer_To() { return ira_Twrc_Transfer_To; }

    public DbsField getIra_Twrc_Wpid() { return ira_Twrc_Wpid; }

    public DbsField getIra_Twrc_Lu_Ts() { return ira_Twrc_Lu_Ts; }

    public DbsField getIra_Twrc_Rmd_Required_Flag() { return ira_Twrc_Rmd_Required_Flag; }

    public DbsField getIra_Twrc_Rmd_Decedent_Name() { return ira_Twrc_Rmd_Decedent_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ira = new DataAccessProgramView(new NameInfo("vw_ira", "IRA"), "TWR_IRA", "TWR_IRA");
        ira_Twrc_Tax_Year = vw_ira.getRecord().newFieldInGroup("ira_Twrc_Tax_Year", "TWRC-TAX-YEAR", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "TWRC_TAX_YEAR");
        ira_Twrc_Company_Cde = vw_ira.getRecord().newFieldInGroup("ira_Twrc_Company_Cde", "TWRC-COMPANY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_COMPANY_CDE");
        ira_Twrc_Status = vw_ira.getRecord().newFieldInGroup("ira_Twrc_Status", "TWRC-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "TWRC_STATUS");
        ira_Twrc_Simulation_Date = vw_ira.getRecord().newFieldInGroup("ira_Twrc_Simulation_Date", "TWRC-SIMULATION-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_SIMULATION_DATE");
        ira_Twrc_Citation_Ind = vw_ira.getRecord().newFieldInGroup("ira_Twrc_Citation_Ind", "TWRC-CITATION-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_CITATION_IND");
        ira_Twrc_Contributor = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Contributor", "TWRC-CONTRIBUTOR");
        ira_Twrc_Tax_Id_Type = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Tax_Id_Type", "TWRC-TAX-ID-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_TAX_ID_TYPE");
        ira_Twrc_Tax_Id = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Tax_Id", "TWRC-TAX-ID", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TWRC_TAX_ID");
        ira_Twrc_Contract = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Contract", "TWRC-CONTRACT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT");
        ira_Twrc_Payee = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Payee", "TWRC-PAYEE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TWRC_PAYEE");
        ira_Twrc_Contract_Xref = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Contract_Xref", "TWRC-CONTRACT-XREF", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CONTRACT_XREF");
        ira_Twrc_Citizen_Code = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Citizen_Code", "TWRC-CITIZEN-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_CITIZEN_CODE");
        ira_Twrc_Residency_Code = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Residency_Code", "TWRC-RESIDENCY-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_RESIDENCY_CODE");
        ira_Twrc_Residency_Type = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Residency_Type", "TWRC-RESIDENCY-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_RESIDENCY_TYPE");
        ira_Twrc_Locality_Cde = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Locality_Cde", "TWRC-LOCALITY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_LOCALITY_CDE");
        ira_Twrc_Dob = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Dob", "TWRC-DOB", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRC_DOB");
        ira_Twrc_Dod = ira_Twrc_Contributor.newFieldInGroup("ira_Twrc_Dod", "TWRC-DOD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TWRC_DOD");
        ira_Twrc_Form_Info = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Form_Info", "TWRC-FORM-INFO");
        ira_Twrc_Pin = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Pin", "TWRC-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, "TWRC_PIN");
        ira_Twrc_Ira_Contract_Type = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Ira_Contract_Type", "TWRC-IRA-CONTRACT-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_IRA_CONTRACT_TYPE");
        ira_Twrc_Source = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Source", "TWRC-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TWRC_SOURCE");
        ira_Twrc_Update_Source = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Update_Source", "TWRC-UPDATE-SOURCE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_SOURCE");
        ira_Twrc_Form_Period = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Form_Period", "TWRC-FORM-PERIOD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRC_FORM_PERIOD");
        ira_Twrc_Additions_1 = ira_Twrc_Form_Info.newFieldInGroup("ira_Twrc_Additions_1", "TWRC-ADDITIONS-1", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_ADDITIONS_1");
        ira_Twrc_Additions_1Redef1 = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Additions_1Redef1", "Redefines", ira_Twrc_Additions_1);
        ira_Twrc_Error_Reason = ira_Twrc_Additions_1Redef1.newFieldInGroup("ira_Twrc_Error_Reason", "TWRC-ERROR-REASON", FieldType.NUMERIC, 2);
        ira_Twrc_Orign_Area = ira_Twrc_Additions_1Redef1.newFieldInGroup("ira_Twrc_Orign_Area", "TWRC-ORIGN-AREA", FieldType.NUMERIC, 2);
        ira_Twrc_Contributions = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Contributions", "TWRC-CONTRIBUTIONS");
        ira_Twrc_Classic_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Classic_Amt", "TWRC-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_CLASSIC_AMT");
        ira_Twrc_Roth_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Roth_Amt", "TWRC-ROTH-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_ROTH_AMT");
        ira_Twrc_Rollover_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Rollover_Amt", "TWRC-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_ROLLOVER_AMT");
        ira_Twrc_Rechar_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Rechar_Amt", "TWRC-RECHAR-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_RECHAR_AMT");
        ira_Twrc_Sep_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Sep_Amt", "TWRC-SEP-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "TWRC_SEP_AMT");
        ira_Twrc_Fair_Mkt_Val_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Fair_Mkt_Val_Amt", "TWRC-FAIR-MKT-VAL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_FAIR_MKT_VAL_AMT");
        ira_Twrc_Roth_Conversion_Amt = ira_Twrc_Contributions.newFieldInGroup("ira_Twrc_Roth_Conversion_Amt", "TWRC-ROTH-CONVERSION-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "TWRC_ROTH_CONVERSION_AMT");
        ira_Twrc_Name_Addr = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Name_Addr", "TWRC-NAME-ADDR");
        ira_Twrc_Name = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Name", "TWRC-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_NAME");
        ira_Twrc_Addr_1 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_1", "TWRC-ADDR-1", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_1");
        ira_Twrc_Addr_2 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_2", "TWRC-ADDR-2", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_2");
        ira_Twrc_Addr_3 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_3", "TWRC-ADDR-3", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_3");
        ira_Twrc_Addr_4 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_4", "TWRC-ADDR-4", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_4");
        ira_Twrc_Addr_5 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_5", "TWRC-ADDR-5", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_5");
        ira_Twrc_Addr_6 = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_6", "TWRC-ADDR-6", FieldType.STRING, 35, RepeatingFieldStrategy.None, "TWRC_ADDR_6");
        ira_Twrc_City = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_City", "TWRC-CITY", FieldType.STRING, 30, RepeatingFieldStrategy.None, "TWRC_CITY");
        ira_Twrc_State = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_State", "TWRC-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TWRC_STATE");
        ira_Twrc_Zip_Code = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Zip_Code", "TWRC-ZIP-CODE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "TWRC_ZIP_CODE");
        ira_Twrc_Addr_Foreign = ira_Twrc_Name_Addr.newFieldInGroup("ira_Twrc_Addr_Foreign", "TWRC-ADDR-FOREIGN", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRC_ADDR_FOREIGN");
        ira_Twrc_Audit_Trail = vw_ira.getRecord().newGroupInGroup("ira_Twrc_Audit_Trail", "TWRC-AUDIT-TRAIL");
        ira_Twrc_Comment1 = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Comment1", "TWRC-COMMENT1", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT1");
        ira_Twrc_Comment2 = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Comment2", "TWRC-COMMENT2", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT2");
        ira_Twrc_Comment_User = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Comment_User", "TWRC-COMMENT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_USER");
        ira_Twrc_Comment_Date = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Comment_Date", "TWRC-COMMENT-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_DATE");
        ira_Twrc_Comment_Time = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Comment_Time", "TWRC-COMMENT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_COMMENT_TIME");
        ira_Twrc_Create_User = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Create_User", "TWRC-CREATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_USER");
        ira_Twrc_Create_Date = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Create_Date", "TWRC-CREATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_DATE");
        ira_Twrc_Create_Date_Inv = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Create_Date_Inv", "TWRC-CREATE-DATE-INV", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_DATE_INV");
        ira_Twrc_Create_Time = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Create_Time", "TWRC-CREATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_TIME");
        ira_Twrc_Create_Time_Inv = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Create_Time_Inv", "TWRC-CREATE-TIME-INV", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_CREATE_TIME_INV");
        ira_Twrc_Update_User = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Update_User", "TWRC-UPDATE-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_USER");
        ira_Twrc_Update_Date = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Update_Date", "TWRC-UPDATE-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_DATE");
        ira_Twrc_Update_Time = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Update_Time", "TWRC-UPDATE-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRC_UPDATE_TIME");
        ira_Twrc_Unique_Id = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Unique_Id", "TWRC-UNIQUE-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "TWRC_UNIQUE_ID");
        ira_Twrc_Transfer_From = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Transfer_From", "TWRC-TRANSFER-FROM", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TWRC_TRANSFER_FROM");
        ira_Twrc_Transfer_To = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Transfer_To", "TWRC-TRANSFER-TO", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "TWRC_TRANSFER_TO");
        ira_Twrc_Wpid = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Wpid", "TWRC-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, "TWRC_WPID");
        ira_Twrc_Lu_Ts = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Lu_Ts", "TWRC-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "TWRC_LU_TS");
        ira_Twrc_Rmd_Required_Flag = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Rmd_Required_Flag", "TWRC-RMD-REQUIRED-FLAG", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRC_RMD_REQUIRED_FLAG");
        ira_Twrc_Rmd_Decedent_Name = ira_Twrc_Audit_Trail.newFieldInGroup("ira_Twrc_Rmd_Decedent_Name", "TWRC-RMD-DECEDENT-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "TWRC_RMD_DECEDENT_NAME");

        this.setRecordName("LdaTwrl462c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ira.reset();
    }

    // Constructor
    public LdaTwrl462c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
