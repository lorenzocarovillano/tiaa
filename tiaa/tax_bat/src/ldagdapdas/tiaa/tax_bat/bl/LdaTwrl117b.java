/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:20 PM
**        * FROM NATURAL LDA     : TWRL117B
************************************************************
**        * FILE NAME            : LdaTwrl117b.java
**        * CLASS NAME           : LdaTwrl117b
**        * INSTANCE NAME        : LdaTwrl117b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl117b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl117b;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Control_Number;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler1;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Form_Type;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Rec_Type;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Trans_Code;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler2;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Tax_Year;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Fill3;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler3;
    private DbsGroup pnd_Twrl117b_Pnd_Twrl117b_Agent_Information;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Id;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Name;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Town;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_State;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full;
    private DbsGroup pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler4;
    private DbsGroup pnd_Twrl117b_Pnd_Twrl117b_Payee_Information;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Bank;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Name;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Town;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_State;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full;
    private DbsGroup pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Ext;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler5;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler6;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler7;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler8;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler9;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler10;
    private DbsField pnd_Twrl117b_Pnd_Twrl117b_Filler11;

    public DbsGroup getPnd_Twrl117b() { return pnd_Twrl117b; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Control_Number() { return pnd_Twrl117b_Pnd_Twrl117b_Control_Number; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler1() { return pnd_Twrl117b_Pnd_Twrl117b_Filler1; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Form_Type() { return pnd_Twrl117b_Pnd_Twrl117b_Form_Type; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Rec_Type() { return pnd_Twrl117b_Pnd_Twrl117b_Rec_Type; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Trans_Code() { return pnd_Twrl117b_Pnd_Twrl117b_Trans_Code; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler2() { return pnd_Twrl117b_Pnd_Twrl117b_Filler2; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Tax_Year() { return pnd_Twrl117b_Pnd_Twrl117b_Tax_Year; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Fill3() { return pnd_Twrl117b_Pnd_Twrl117b_Fill3; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler3() { return pnd_Twrl117b_Pnd_Twrl117b_Filler3; }

    public DbsGroup getPnd_Twrl117b_Pnd_Twrl117b_Agent_Information() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Information; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Id() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Id; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Name() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Name; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Town() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Town; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_State() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_State; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full; }

    public DbsGroup getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext() { return pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler4() { return pnd_Twrl117b_Pnd_Twrl117b_Filler4; }

    public DbsGroup getPnd_Twrl117b_Pnd_Twrl117b_Payee_Information() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Information; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Bank() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Bank; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Name() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Name; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Town() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Town; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_State() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_State; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full; }

    public DbsGroup getPnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Zip() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Ext() { return pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Ext; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler5() { return pnd_Twrl117b_Pnd_Twrl117b_Filler5; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others() { return pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler6() { return pnd_Twrl117b_Pnd_Twrl117b_Filler6; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler7() { return pnd_Twrl117b_Pnd_Twrl117b_Filler7; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler8() { return pnd_Twrl117b_Pnd_Twrl117b_Filler8; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler9() { return pnd_Twrl117b_Pnd_Twrl117b_Filler9; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler10() { return pnd_Twrl117b_Pnd_Twrl117b_Filler10; }

    public DbsField getPnd_Twrl117b_Pnd_Twrl117b_Filler11() { return pnd_Twrl117b_Pnd_Twrl117b_Filler11; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl117b = newGroupInRecord("pnd_Twrl117b", "#TWRL117B");
        pnd_Twrl117b_Pnd_Twrl117b_Control_Number = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Control_Number", "#TWRL117B-CONTROL-NUMBER", 
            FieldType.STRING, 10);
        pnd_Twrl117b_Pnd_Twrl117b_Filler1 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler1", "#TWRL117B-FILLER1", FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Form_Type = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Form_Type", "#TWRL117B-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117b_Pnd_Twrl117b_Rec_Type = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Rec_Type", "#TWRL117B-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl117b_Pnd_Twrl117b_Trans_Code = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Trans_Code", "#TWRL117B-TRANS-CODE", FieldType.STRING, 
            1);
        pnd_Twrl117b_Pnd_Twrl117b_Filler2 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler2", "#TWRL117B-FILLER2", FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Tax_Year = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Tax_Year", "#TWRL117B-TAX-YEAR", FieldType.STRING, 
            4);
        pnd_Twrl117b_Pnd_Twrl117b_Fill3 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Fill3", "#TWRL117B-FILL3", FieldType.STRING, 8);
        pnd_Twrl117b_Pnd_Twrl117b_Filler3 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler3", "#TWRL117B-FILLER3", FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Information = pnd_Twrl117b.newGroupInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Information", "#TWRL117B-AGENT-INFORMATION");
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Id = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Id", "#TWRL117B-AGENT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Name = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Name", "#TWRL117B-AGENT-NAME", 
            FieldType.STRING, 30);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1 = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_1", 
            "#TWRL117B-AGENT-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2 = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Address_2", 
            "#TWRL117B-AGENT-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Town = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Town", "#TWRL117B-AGENT-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_State = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_State", "#TWRL117B-AGENT-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full", 
            "#TWRL117B-AGENT-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1 = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newGroupInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1", 
            "Redefines", pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Full);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip = pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip", "#TWRL117B-AGENT-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext = pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_FullRedef1.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Agent_Zip_Ext", 
            "#TWRL117B-AGENT-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117b_Pnd_Twrl117b_Filler4 = pnd_Twrl117b_Pnd_Twrl117b_Agent_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler4", "#TWRL117B-FILLER4", 
            FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Information = pnd_Twrl117b.newGroupInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Information", "#TWRL117B-PAYEE-INFORMATION");
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Ssn", "#TWRL117B-PAYEE-SSN", 
            FieldType.STRING, 9);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Bank = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Bank", "#TWRL117B-PAYEE-BANK", 
            FieldType.STRING, 20);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Name = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Name", "#TWRL117B-PAYEE-NAME", 
            FieldType.STRING, 30);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1 = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_1", 
            "#TWRL117B-PAYEE-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2 = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Address_2", 
            "#TWRL117B-PAYEE-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Town = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Town", "#TWRL117B-PAYEE-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_State = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_State", "#TWRL117B-PAYEE-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full", 
            "#TWRL117B-PAYEE-ZIP-FULL", FieldType.STRING, 9);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2 = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newGroupInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2", 
            "Redefines", pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Full);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip = pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip", "#TWRL117B-PAYEE-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Ext = pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_FullRedef2.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Payee_Zip_Ext", 
            "#TWRL117B-PAYEE-ZIP-EXT", FieldType.NUMERIC, 4);
        pnd_Twrl117b_Pnd_Twrl117b_Filler5 = pnd_Twrl117b_Pnd_Twrl117b_Payee_Information.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler5", "#TWRL117B-FILLER5", 
            FieldType.STRING, 1);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Indev", "#TWRL117B-AMT-PAID-INDEV", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Indev", "#TWRL117B-AMT-WTHLD-INDEV", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part", "#TWRL117B-AMT-PAID-PART", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part", "#TWRL117B-AMT-WTHLD-PART", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Extra", "#TWRL117B-AMT-PAID-EXTRA", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Extra", "#TWRL117B-AMT-WTHLD-EXTRA", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Dividend", "#TWRL117B-AMT-PAID-DIVIDEND", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Dividend", "#TWRL117B-AMT-WTHLD-DIVIDEND", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Part_Dis", "#TWRL117B-AMT-PAID-PART-DIS", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Part_Dis", "#TWRL117B-AMT-WTHLD-PART-DIS", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Interst", "#TWRL117B-AMT-PAID-INTERST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Interst", "#TWRL117B-AMT-WTHLD-INTERST", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_26", "#TWRL117B-AMT-PAID-ACT-26", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_26", "#TWRL117B-AMT-WTHLD-ACT-26", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Act_8", "#TWRL117B-AMT-PAID-ACT-8", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Act_8", "#TWRL117B-AMT-WTHLD-ACT-8", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Pension", "#TWRL117B-AMT-PAID-PENSION", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Pension", "#TWRL117B-AMT-WTHLD-PENSION", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Paid_Others", "#TWRL117B-AMT-PAID-OTHERS", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Amt_Wthld_Others", "#TWRL117B-AMT-WTHLD-OTHERS", 
            FieldType.DECIMAL, 10,2);
        pnd_Twrl117b_Pnd_Twrl117b_Filler6 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler6", "#TWRL117B-FILLER6", FieldType.STRING, 250);
        pnd_Twrl117b_Pnd_Twrl117b_Filler7 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler7", "#TWRL117B-FILLER7", FieldType.STRING, 250);
        pnd_Twrl117b_Pnd_Twrl117b_Filler8 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler8", "#TWRL117B-FILLER8", FieldType.STRING, 250);
        pnd_Twrl117b_Pnd_Twrl117b_Filler9 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler9", "#TWRL117B-FILLER9", FieldType.STRING, 250);
        pnd_Twrl117b_Pnd_Twrl117b_Filler10 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler10", "#TWRL117B-FILLER10", FieldType.STRING, 
            250);
        pnd_Twrl117b_Pnd_Twrl117b_Filler11 = pnd_Twrl117b.newFieldInGroup("pnd_Twrl117b_Pnd_Twrl117b_Filler11", "#TWRL117B-FILLER11", FieldType.STRING, 
            210);

        this.setRecordName("LdaTwrl117b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl117b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
