/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:13 PM
**        * FROM NATURAL LDA     : TWRL6400
************************************************************
**        * FILE NAME            : LdaTwrl6400.java
**        * CLASS NAME           : LdaTwrl6400
**        * INSTANCE NAME        : LdaTwrl6400
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl6400 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Npd_Tran;
    private DbsField pnd_Npd_Tran_Pnd_I_Product_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Cntrct_Nbr;
    private DbsField pnd_Npd_Tran_Pnd_I_Payee_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Tax_Id_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Tax_Id;
    private DbsField pnd_Npd_Tran_Pnd_I_Source;
    private DbsField pnd_Npd_Tran_Pnd_I_Pymnt_Month;
    private DbsField pnd_Npd_Tran_Pnd_I_Trans_Dte;
    private DbsField pnd_Npd_Tran_Pnd_I_Data_Type;
    private DbsField pnd_Npd_Tran_Pnd_I_Case_Type;
    private DbsField pnd_Npd_Tran_Pnd_I_Currence_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Geographic_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Rollover_Ind;
    private DbsField pnd_Npd_Tran_Pnd_I_State_Rsdncy;
    private DbsField pnd_Npd_Tran_Pnd_I_Citizenship;
    private DbsField pnd_Npd_Tran_Pnd_I_Dob;
    private DbsField pnd_Npd_Tran_Pnd_I_Gross_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Fed_Whhld_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_State_Whhld_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Interest_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Ivc_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Ivc_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Co_Nmbr;
    private DbsField pnd_Npd_Tran_Pnd_I_Recs;
    private DbsField pnd_Npd_Tran_Pnd_I_Sys_Rsdncy_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Filler;
    private DbsField pnd_Npd_Tran_Pnd_I_Na_Line_Cnt;
    private DbsField pnd_Npd_Tran_Pnd_I_Na_Line;
    private DbsGroup pnd_Npd_Tran_Pnd_I_Na_LineRedef1;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne1;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne2;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne3;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne4;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne5;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne6;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne7;
    private DbsField pnd_Npd_Tran_Pnd_I_Lne8;
    private DbsField pnd_Npd_Tran_Pnd_I_Tax_Elct_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Maint_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Per_Ivc_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Per_Ivc_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Tax_Id;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Dob_F;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Dob_S;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Dod_F;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Dod_S;
    private DbsField pnd_Npd_Tran_Pnd_I_New_Ctzn_Cde;
    private DbsField pnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt;
    private DbsField pnd_Npd_Tran_Pnd_I_First_Pymnt_Dte;
    private DbsField pnd_Npd_Tran_Pnd_I_Filler2;
    private DbsField pnd_Npd_Tran_Pnd_I_Trans_Cde;
    private DbsGroup pnd_Header_Record;
    private DbsField pnd_Header_Record_Pnd_Timestmp;
    private DbsField pnd_Header_Record_Pnd_Interface_Dte;
    private DbsField pnd_Header_Record_Pnd_Title;
    private DbsField pnd_Header_Record_Pnd_Rec_Count;
    private DbsField pnd_Header_Record_Pnd_Filler1;
    private DbsField pnd_Header_Record_Pnd_Filler2;
    private DbsField pnd_Header_Record_Pnd_Header_Special_Run_Ind;
    private DbsField pnd_Header_Record_Pnd_Header_Type;

    public DbsGroup getPnd_Npd_Tran() { return pnd_Npd_Tran; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Product_Cde() { return pnd_Npd_Tran_Pnd_I_Product_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Cntrct_Nbr() { return pnd_Npd_Tran_Pnd_I_Cntrct_Nbr; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Payee_Cde() { return pnd_Npd_Tran_Pnd_I_Payee_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Tax_Id_Cde() { return pnd_Npd_Tran_Pnd_I_Tax_Id_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Tax_Id() { return pnd_Npd_Tran_Pnd_I_Tax_Id; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Source() { return pnd_Npd_Tran_Pnd_I_Source; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Pymnt_Month() { return pnd_Npd_Tran_Pnd_I_Pymnt_Month; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Trans_Dte() { return pnd_Npd_Tran_Pnd_I_Trans_Dte; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Data_Type() { return pnd_Npd_Tran_Pnd_I_Data_Type; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Case_Type() { return pnd_Npd_Tran_Pnd_I_Case_Type; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Currence_Cde() { return pnd_Npd_Tran_Pnd_I_Currence_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Geographic_Cde() { return pnd_Npd_Tran_Pnd_I_Geographic_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Rollover_Ind() { return pnd_Npd_Tran_Pnd_I_Rollover_Ind; }

    public DbsField getPnd_Npd_Tran_Pnd_I_State_Rsdncy() { return pnd_Npd_Tran_Pnd_I_State_Rsdncy; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Citizenship() { return pnd_Npd_Tran_Pnd_I_Citizenship; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Dob() { return pnd_Npd_Tran_Pnd_I_Dob; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Gross_Amt() { return pnd_Npd_Tran_Pnd_I_Gross_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Fed_Whhld_Amt() { return pnd_Npd_Tran_Pnd_I_Fed_Whhld_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_State_Whhld_Amt() { return pnd_Npd_Tran_Pnd_I_State_Whhld_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Interest_Amt() { return pnd_Npd_Tran_Pnd_I_Interest_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Ivc_Amt() { return pnd_Npd_Tran_Pnd_I_Ivc_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Ivc_Cde() { return pnd_Npd_Tran_Pnd_I_Ivc_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Co_Nmbr() { return pnd_Npd_Tran_Pnd_I_Co_Nmbr; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Recs() { return pnd_Npd_Tran_Pnd_I_Recs; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Sys_Rsdncy_Cde() { return pnd_Npd_Tran_Pnd_I_Sys_Rsdncy_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Filler() { return pnd_Npd_Tran_Pnd_I_Filler; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Na_Line_Cnt() { return pnd_Npd_Tran_Pnd_I_Na_Line_Cnt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Na_Line() { return pnd_Npd_Tran_Pnd_I_Na_Line; }

    public DbsGroup getPnd_Npd_Tran_Pnd_I_Na_LineRedef1() { return pnd_Npd_Tran_Pnd_I_Na_LineRedef1; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne1() { return pnd_Npd_Tran_Pnd_I_Lne1; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne2() { return pnd_Npd_Tran_Pnd_I_Lne2; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne3() { return pnd_Npd_Tran_Pnd_I_Lne3; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne4() { return pnd_Npd_Tran_Pnd_I_Lne4; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne5() { return pnd_Npd_Tran_Pnd_I_Lne5; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne6() { return pnd_Npd_Tran_Pnd_I_Lne6; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne7() { return pnd_Npd_Tran_Pnd_I_Lne7; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Lne8() { return pnd_Npd_Tran_Pnd_I_Lne8; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Tax_Elct_Cde() { return pnd_Npd_Tran_Pnd_I_Tax_Elct_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Maint_Cde() { return pnd_Npd_Tran_Pnd_I_Maint_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Per_Ivc_Cde() { return pnd_Npd_Tran_Pnd_I_Per_Ivc_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Per_Ivc_Amt() { return pnd_Npd_Tran_Pnd_I_Per_Ivc_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde() { return pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Rtb_Ivc_Amt() { return pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde() { return pnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Tax_Id() { return pnd_Npd_Tran_Pnd_I_New_Tax_Id; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Dob_F() { return pnd_Npd_Tran_Pnd_I_New_Dob_F; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Dob_S() { return pnd_Npd_Tran_Pnd_I_New_Dob_S; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Dod_F() { return pnd_Npd_Tran_Pnd_I_New_Dod_F; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Dod_S() { return pnd_Npd_Tran_Pnd_I_New_Dod_S; }

    public DbsField getPnd_Npd_Tran_Pnd_I_New_Ctzn_Cde() { return pnd_Npd_Tran_Pnd_I_New_Ctzn_Cde; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt() { return pnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt() { return pnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt; }

    public DbsField getPnd_Npd_Tran_Pnd_I_First_Pymnt_Dte() { return pnd_Npd_Tran_Pnd_I_First_Pymnt_Dte; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Filler2() { return pnd_Npd_Tran_Pnd_I_Filler2; }

    public DbsField getPnd_Npd_Tran_Pnd_I_Trans_Cde() { return pnd_Npd_Tran_Pnd_I_Trans_Cde; }

    public DbsGroup getPnd_Header_Record() { return pnd_Header_Record; }

    public DbsField getPnd_Header_Record_Pnd_Timestmp() { return pnd_Header_Record_Pnd_Timestmp; }

    public DbsField getPnd_Header_Record_Pnd_Interface_Dte() { return pnd_Header_Record_Pnd_Interface_Dte; }

    public DbsField getPnd_Header_Record_Pnd_Title() { return pnd_Header_Record_Pnd_Title; }

    public DbsField getPnd_Header_Record_Pnd_Rec_Count() { return pnd_Header_Record_Pnd_Rec_Count; }

    public DbsField getPnd_Header_Record_Pnd_Filler1() { return pnd_Header_Record_Pnd_Filler1; }

    public DbsField getPnd_Header_Record_Pnd_Filler2() { return pnd_Header_Record_Pnd_Filler2; }

    public DbsField getPnd_Header_Record_Pnd_Header_Special_Run_Ind() { return pnd_Header_Record_Pnd_Header_Special_Run_Ind; }

    public DbsField getPnd_Header_Record_Pnd_Header_Type() { return pnd_Header_Record_Pnd_Header_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Npd_Tran = newGroupInRecord("pnd_Npd_Tran", "#NPD-TRAN");
        pnd_Npd_Tran_Pnd_I_Product_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Product_Cde", "#I-PRODUCT-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Cntrct_Nbr = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Cntrct_Nbr", "#I-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Npd_Tran_Pnd_I_Payee_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Payee_Cde", "#I-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Npd_Tran_Pnd_I_Tax_Id_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Tax_Id_Cde", "#I-TAX-ID-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Tax_Id = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Tax_Id", "#I-TAX-ID", FieldType.NUMERIC, 9);
        pnd_Npd_Tran_Pnd_I_Source = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Source", "#I-SOURCE", FieldType.STRING, 3);
        pnd_Npd_Tran_Pnd_I_Pymnt_Month = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Pymnt_Month", "#I-PYMNT-MONTH", FieldType.STRING, 2);
        pnd_Npd_Tran_Pnd_I_Trans_Dte = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Trans_Dte", "#I-TRANS-DTE", FieldType.TIME);
        pnd_Npd_Tran_Pnd_I_Data_Type = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Data_Type", "#I-DATA-TYPE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Case_Type = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Case_Type", "#I-CASE-TYPE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Currence_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Currence_Cde", "#I-CURRENCE-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Geographic_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Geographic_Cde", "#I-GEOGRAPHIC-CDE", FieldType.NUMERIC, 
            2);
        pnd_Npd_Tran_Pnd_I_Rollover_Ind = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Rollover_Ind", "#I-ROLLOVER-IND", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_State_Rsdncy = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_State_Rsdncy", "#I-STATE-RSDNCY", FieldType.STRING, 2);
        pnd_Npd_Tran_Pnd_I_Citizenship = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Citizenship", "#I-CITIZENSHIP", FieldType.NUMERIC, 2);
        pnd_Npd_Tran_Pnd_I_Dob = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Dob", "#I-DOB", FieldType.PACKED_DECIMAL, 6);
        pnd_Npd_Tran_Pnd_I_Gross_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Gross_Amt", "#I-GROSS-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_Fed_Whhld_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Fed_Whhld_Amt", "#I-FED-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Npd_Tran_Pnd_I_State_Whhld_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_State_Whhld_Amt", "#I-STATE-WHHLD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Npd_Tran_Pnd_I_Interest_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Interest_Amt", "#I-INTEREST-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Npd_Tran_Pnd_I_Ivc_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Ivc_Amt", "#I-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_Ivc_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Ivc_Cde", "#I-IVC-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Co_Nmbr = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Co_Nmbr", "#I-CO-NMBR", FieldType.NUMERIC, 1);
        pnd_Npd_Tran_Pnd_I_Recs = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Recs", "#I-RECS", FieldType.STRING, 3);
        pnd_Npd_Tran_Pnd_I_Sys_Rsdncy_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Sys_Rsdncy_Cde", "#I-SYS-RSDNCY-CDE", FieldType.NUMERIC, 
            2);
        pnd_Npd_Tran_Pnd_I_Filler = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Filler", "#I-FILLER", FieldType.STRING, 11);
        pnd_Npd_Tran_Pnd_I_Na_Line_Cnt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Na_Line_Cnt", "#I-NA-LINE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Npd_Tran_Pnd_I_Na_Line = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Na_Line", "#I-NA-LINE", FieldType.STRING, 245);
        pnd_Npd_Tran_Pnd_I_Na_LineRedef1 = pnd_Npd_Tran.newGroupInGroup("pnd_Npd_Tran_Pnd_I_Na_LineRedef1", "Redefines", pnd_Npd_Tran_Pnd_I_Na_Line);
        pnd_Npd_Tran_Pnd_I_Lne1 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne1", "#I-LNE1", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne2 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne2", "#I-LNE2", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne3 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne3", "#I-LNE3", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne4 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne4", "#I-LNE4", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne5 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne5", "#I-LNE5", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne6 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne6", "#I-LNE6", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne7 = pnd_Npd_Tran_Pnd_I_Na_LineRedef1.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne7", "#I-LNE7", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Lne8 = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Lne8", "#I-LNE8", FieldType.STRING, 35);
        pnd_Npd_Tran_Pnd_I_Tax_Elct_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Tax_Elct_Cde", "#I-TAX-ELCT-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Maint_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Maint_Cde", "#I-MAINT-CDE", FieldType.STRING, 3);
        pnd_Npd_Tran_Pnd_I_Per_Ivc_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Per_Ivc_Cde", "#I-PER-IVC-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Per_Ivc_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Per_Ivc_Amt", "#I-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Cde", "#I-RTB-IVC-CDE", FieldType.STRING, 1);
        pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Rtb_Ivc_Amt", "#I-RTB-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Rsdncy_Cde", "#I-NEW-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Npd_Tran_Pnd_I_New_Tax_Id = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Tax_Id", "#I-NEW-TAX-ID", FieldType.NUMERIC, 9);
        pnd_Npd_Tran_Pnd_I_New_Dob_F = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Dob_F", "#I-NEW-DOB-F", FieldType.PACKED_DECIMAL, 9);
        pnd_Npd_Tran_Pnd_I_New_Dob_S = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Dob_S", "#I-NEW-DOB-S", FieldType.PACKED_DECIMAL, 9);
        pnd_Npd_Tran_Pnd_I_New_Dod_F = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Dod_F", "#I-NEW-DOD-F", FieldType.PACKED_DECIMAL, 7);
        pnd_Npd_Tran_Pnd_I_New_Dod_S = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Dod_S", "#I-NEW-DOD-S", FieldType.PACKED_DECIMAL, 7);
        pnd_Npd_Tran_Pnd_I_New_Ctzn_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_New_Ctzn_Cde", "#I-NEW-CTZN-CDE", FieldType.STRING, 2);
        pnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Tot_Ivc_Amt", "#I-TOT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Ytd_Ivc_Amt", "#I-YTD-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Npd_Tran_Pnd_I_First_Pymnt_Dte = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_First_Pymnt_Dte", "#I-FIRST-PYMNT-DTE", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Npd_Tran_Pnd_I_Filler2 = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Filler2", "#I-FILLER2", FieldType.STRING, 21);
        pnd_Npd_Tran_Pnd_I_Trans_Cde = pnd_Npd_Tran.newFieldInGroup("pnd_Npd_Tran_Pnd_I_Trans_Cde", "#I-TRANS-CDE", FieldType.NUMERIC, 3);

        pnd_Header_Record = newGroupInRecord("pnd_Header_Record", "#HEADER-RECORD");
        pnd_Header_Record_Pnd_Timestmp = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Timestmp", "#TIMESTMP", FieldType.BINARY, 8);
        pnd_Header_Record_Pnd_Interface_Dte = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Interface_Dte", "#INTERFACE-DTE", FieldType.NUMERIC, 
            6);
        pnd_Header_Record_Pnd_Title = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Title", "#TITLE", FieldType.STRING, 25);
        pnd_Header_Record_Pnd_Rec_Count = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Rec_Count", "#REC-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Header_Record_Pnd_Filler1 = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Filler1", "#FILLER1", FieldType.STRING, 230);
        pnd_Header_Record_Pnd_Filler2 = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Filler2", "#FILLER2", FieldType.STRING, 92);
        pnd_Header_Record_Pnd_Header_Special_Run_Ind = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Header_Special_Run_Ind", "#HEADER-SPECIAL-RUN-IND", 
            FieldType.STRING, 8);
        pnd_Header_Record_Pnd_Header_Type = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Header_Type", "#HEADER-TYPE", FieldType.STRING, 3);

        this.setRecordName("LdaTwrl6400");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl6400() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
