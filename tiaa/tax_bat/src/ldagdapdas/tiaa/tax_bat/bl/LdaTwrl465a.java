/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:02 PM
**        * FROM NATURAL LDA     : TWRL465A
************************************************************
**        * FILE NAME            : LdaTwrl465a.java
**        * CLASS NAME           : LdaTwrl465a
**        * INSTANCE NAME        : LdaTwrl465a
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl465a extends DbsRecord
{
    // Properties
    private DbsGroup ira_Record;
    private DbsField ira_Record_Ira_Tax_Year;
    private DbsField ira_Record_Ira_Company_Code;
    private DbsField ira_Record_Ira_Simulation_Date;
    private DbsField ira_Record_Ira_Tax_Id;
    private DbsField ira_Record_Ira_Tax_Id_Type;
    private DbsField ira_Record_Ira_Contract;
    private DbsField ira_Record_Ira_Payee;
    private DbsField ira_Record_Ira_Contract_Xref;
    private DbsField ira_Record_Ira_Payee_Xref;
    private DbsField ira_Record_Ira_Pin;
    private DbsField ira_Record_Ira_Citizenship_Code;
    private DbsField ira_Record_Ira_Residency_Code;
    private DbsField ira_Record_Ira_Residency_Type;
    private DbsField ira_Record_Ira_Locality_Code;
    private DbsField ira_Record_Ira_Dob;
    private DbsField ira_Record_Ira_Dod;
    private DbsField ira_Record_Ira_Form;
    private DbsField ira_Record_Ira_Form_Type;
    private DbsField ira_Record_Ira_Form_Ocv;
    private DbsField ira_Record_Ira_Rollover;
    private DbsField ira_Record_Ira_Recharacter;
    private DbsField ira_Record_Ira_Form_Period;
    private DbsField ira_Record_Ira_Form_1078;
    private DbsField ira_Record_Ira_Form_1078_Eff_Date;
    private DbsField ira_Record_Ira_Form_1001;
    private DbsField ira_Record_Ira_Form_1001_Eff_Date;
    private DbsField ira_Record_Ira_Classic_Amt;
    private DbsField ira_Record_Ira_Roth_Amt;
    private DbsField ira_Record_Ira_Rollover_Amt;
    private DbsField ira_Record_Ira_Recharacter_Amt;
    private DbsField ira_Record_Ira_Educational_Amt;
    private DbsField ira_Record_Ira_Fmv_Amt;
    private DbsField ira_Record_Ira_Roth_Conv_Amt;
    private DbsField ira_Record_Ira_Name;
    private DbsField ira_Record_Ira_Addr_1;
    private DbsField ira_Record_Ira_Addr_2;
    private DbsField ira_Record_Ira_Addr_3;
    private DbsField ira_Record_Ira_Addr_4;
    private DbsField ira_Record_Ira_Addr_5;
    private DbsField ira_Record_Ira_Addr_6;
    private DbsField ira_Record_Ira_City;
    private DbsField ira_Record_Ira_State;
    private DbsField ira_Record_Ira_Zip_Code;
    private DbsField ira_Record_Ira_Foreign_Address;
    private DbsField ira_Record_Ira_Foreign_Country;
    private DbsField ira_Record_Ira_Rmd_Required_Flag;
    private DbsField ira_Record_Ira_Rmd_Decedent_Name;
    private DbsField ira_Record_Ira_Unique_Id;

    public DbsGroup getIra_Record() { return ira_Record; }

    public DbsField getIra_Record_Ira_Tax_Year() { return ira_Record_Ira_Tax_Year; }

    public DbsField getIra_Record_Ira_Company_Code() { return ira_Record_Ira_Company_Code; }

    public DbsField getIra_Record_Ira_Simulation_Date() { return ira_Record_Ira_Simulation_Date; }

    public DbsField getIra_Record_Ira_Tax_Id() { return ira_Record_Ira_Tax_Id; }

    public DbsField getIra_Record_Ira_Tax_Id_Type() { return ira_Record_Ira_Tax_Id_Type; }

    public DbsField getIra_Record_Ira_Contract() { return ira_Record_Ira_Contract; }

    public DbsField getIra_Record_Ira_Payee() { return ira_Record_Ira_Payee; }

    public DbsField getIra_Record_Ira_Contract_Xref() { return ira_Record_Ira_Contract_Xref; }

    public DbsField getIra_Record_Ira_Payee_Xref() { return ira_Record_Ira_Payee_Xref; }

    public DbsField getIra_Record_Ira_Pin() { return ira_Record_Ira_Pin; }

    public DbsField getIra_Record_Ira_Citizenship_Code() { return ira_Record_Ira_Citizenship_Code; }

    public DbsField getIra_Record_Ira_Residency_Code() { return ira_Record_Ira_Residency_Code; }

    public DbsField getIra_Record_Ira_Residency_Type() { return ira_Record_Ira_Residency_Type; }

    public DbsField getIra_Record_Ira_Locality_Code() { return ira_Record_Ira_Locality_Code; }

    public DbsField getIra_Record_Ira_Dob() { return ira_Record_Ira_Dob; }

    public DbsField getIra_Record_Ira_Dod() { return ira_Record_Ira_Dod; }

    public DbsField getIra_Record_Ira_Form() { return ira_Record_Ira_Form; }

    public DbsField getIra_Record_Ira_Form_Type() { return ira_Record_Ira_Form_Type; }

    public DbsField getIra_Record_Ira_Form_Ocv() { return ira_Record_Ira_Form_Ocv; }

    public DbsField getIra_Record_Ira_Rollover() { return ira_Record_Ira_Rollover; }

    public DbsField getIra_Record_Ira_Recharacter() { return ira_Record_Ira_Recharacter; }

    public DbsField getIra_Record_Ira_Form_Period() { return ira_Record_Ira_Form_Period; }

    public DbsField getIra_Record_Ira_Form_1078() { return ira_Record_Ira_Form_1078; }

    public DbsField getIra_Record_Ira_Form_1078_Eff_Date() { return ira_Record_Ira_Form_1078_Eff_Date; }

    public DbsField getIra_Record_Ira_Form_1001() { return ira_Record_Ira_Form_1001; }

    public DbsField getIra_Record_Ira_Form_1001_Eff_Date() { return ira_Record_Ira_Form_1001_Eff_Date; }

    public DbsField getIra_Record_Ira_Classic_Amt() { return ira_Record_Ira_Classic_Amt; }

    public DbsField getIra_Record_Ira_Roth_Amt() { return ira_Record_Ira_Roth_Amt; }

    public DbsField getIra_Record_Ira_Rollover_Amt() { return ira_Record_Ira_Rollover_Amt; }

    public DbsField getIra_Record_Ira_Recharacter_Amt() { return ira_Record_Ira_Recharacter_Amt; }

    public DbsField getIra_Record_Ira_Educational_Amt() { return ira_Record_Ira_Educational_Amt; }

    public DbsField getIra_Record_Ira_Fmv_Amt() { return ira_Record_Ira_Fmv_Amt; }

    public DbsField getIra_Record_Ira_Roth_Conv_Amt() { return ira_Record_Ira_Roth_Conv_Amt; }

    public DbsField getIra_Record_Ira_Name() { return ira_Record_Ira_Name; }

    public DbsField getIra_Record_Ira_Addr_1() { return ira_Record_Ira_Addr_1; }

    public DbsField getIra_Record_Ira_Addr_2() { return ira_Record_Ira_Addr_2; }

    public DbsField getIra_Record_Ira_Addr_3() { return ira_Record_Ira_Addr_3; }

    public DbsField getIra_Record_Ira_Addr_4() { return ira_Record_Ira_Addr_4; }

    public DbsField getIra_Record_Ira_Addr_5() { return ira_Record_Ira_Addr_5; }

    public DbsField getIra_Record_Ira_Addr_6() { return ira_Record_Ira_Addr_6; }

    public DbsField getIra_Record_Ira_City() { return ira_Record_Ira_City; }

    public DbsField getIra_Record_Ira_State() { return ira_Record_Ira_State; }

    public DbsField getIra_Record_Ira_Zip_Code() { return ira_Record_Ira_Zip_Code; }

    public DbsField getIra_Record_Ira_Foreign_Address() { return ira_Record_Ira_Foreign_Address; }

    public DbsField getIra_Record_Ira_Foreign_Country() { return ira_Record_Ira_Foreign_Country; }

    public DbsField getIra_Record_Ira_Rmd_Required_Flag() { return ira_Record_Ira_Rmd_Required_Flag; }

    public DbsField getIra_Record_Ira_Rmd_Decedent_Name() { return ira_Record_Ira_Rmd_Decedent_Name; }

    public DbsField getIra_Record_Ira_Unique_Id() { return ira_Record_Ira_Unique_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ira_Record = newGroupInRecord("ira_Record", "IRA-RECORD");
        ira_Record_Ira_Tax_Year = ira_Record.newFieldInGroup("ira_Record_Ira_Tax_Year", "IRA-TAX-YEAR", FieldType.STRING, 4);
        ira_Record_Ira_Company_Code = ira_Record.newFieldInGroup("ira_Record_Ira_Company_Code", "IRA-COMPANY-CODE", FieldType.STRING, 1);
        ira_Record_Ira_Simulation_Date = ira_Record.newFieldInGroup("ira_Record_Ira_Simulation_Date", "IRA-SIMULATION-DATE", FieldType.STRING, 8);
        ira_Record_Ira_Tax_Id = ira_Record.newFieldInGroup("ira_Record_Ira_Tax_Id", "IRA-TAX-ID", FieldType.STRING, 10);
        ira_Record_Ira_Tax_Id_Type = ira_Record.newFieldInGroup("ira_Record_Ira_Tax_Id_Type", "IRA-TAX-ID-TYPE", FieldType.STRING, 1);
        ira_Record_Ira_Contract = ira_Record.newFieldInGroup("ira_Record_Ira_Contract", "IRA-CONTRACT", FieldType.STRING, 8);
        ira_Record_Ira_Payee = ira_Record.newFieldInGroup("ira_Record_Ira_Payee", "IRA-PAYEE", FieldType.STRING, 2);
        ira_Record_Ira_Contract_Xref = ira_Record.newFieldInGroup("ira_Record_Ira_Contract_Xref", "IRA-CONTRACT-XREF", FieldType.STRING, 8);
        ira_Record_Ira_Payee_Xref = ira_Record.newFieldInGroup("ira_Record_Ira_Payee_Xref", "IRA-PAYEE-XREF", FieldType.STRING, 2);
        ira_Record_Ira_Pin = ira_Record.newFieldInGroup("ira_Record_Ira_Pin", "IRA-PIN", FieldType.STRING, 12);
        ira_Record_Ira_Citizenship_Code = ira_Record.newFieldInGroup("ira_Record_Ira_Citizenship_Code", "IRA-CITIZENSHIP-CODE", FieldType.STRING, 2);
        ira_Record_Ira_Residency_Code = ira_Record.newFieldInGroup("ira_Record_Ira_Residency_Code", "IRA-RESIDENCY-CODE", FieldType.STRING, 2);
        ira_Record_Ira_Residency_Type = ira_Record.newFieldInGroup("ira_Record_Ira_Residency_Type", "IRA-RESIDENCY-TYPE", FieldType.STRING, 1);
        ira_Record_Ira_Locality_Code = ira_Record.newFieldInGroup("ira_Record_Ira_Locality_Code", "IRA-LOCALITY-CODE", FieldType.STRING, 3);
        ira_Record_Ira_Dob = ira_Record.newFieldInGroup("ira_Record_Ira_Dob", "IRA-DOB", FieldType.STRING, 8);
        ira_Record_Ira_Dod = ira_Record.newFieldInGroup("ira_Record_Ira_Dod", "IRA-DOD", FieldType.STRING, 8);
        ira_Record_Ira_Form = ira_Record.newFieldInGroup("ira_Record_Ira_Form", "IRA-FORM", FieldType.STRING, 10);
        ira_Record_Ira_Form_Type = ira_Record.newFieldInGroup("ira_Record_Ira_Form_Type", "IRA-FORM-TYPE", FieldType.STRING, 2);
        ira_Record_Ira_Form_Ocv = ira_Record.newFieldInGroup("ira_Record_Ira_Form_Ocv", "IRA-FORM-OCV", FieldType.STRING, 1);
        ira_Record_Ira_Rollover = ira_Record.newFieldInGroup("ira_Record_Ira_Rollover", "IRA-ROLLOVER", FieldType.STRING, 1);
        ira_Record_Ira_Recharacter = ira_Record.newFieldInGroup("ira_Record_Ira_Recharacter", "IRA-RECHARACTER", FieldType.STRING, 1);
        ira_Record_Ira_Form_Period = ira_Record.newFieldInGroup("ira_Record_Ira_Form_Period", "IRA-FORM-PERIOD", FieldType.STRING, 3);
        ira_Record_Ira_Form_1078 = ira_Record.newFieldInGroup("ira_Record_Ira_Form_1078", "IRA-FORM-1078", FieldType.STRING, 1);
        ira_Record_Ira_Form_1078_Eff_Date = ira_Record.newFieldInGroup("ira_Record_Ira_Form_1078_Eff_Date", "IRA-FORM-1078-EFF-DATE", FieldType.STRING, 
            8);
        ira_Record_Ira_Form_1001 = ira_Record.newFieldInGroup("ira_Record_Ira_Form_1001", "IRA-FORM-1001", FieldType.STRING, 1);
        ira_Record_Ira_Form_1001_Eff_Date = ira_Record.newFieldInGroup("ira_Record_Ira_Form_1001_Eff_Date", "IRA-FORM-1001-EFF-DATE", FieldType.STRING, 
            8);
        ira_Record_Ira_Classic_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Classic_Amt", "IRA-CLASSIC-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Roth_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Roth_Amt", "IRA-ROTH-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Rollover_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Rollover_Amt", "IRA-ROLLOVER-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Recharacter_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Recharacter_Amt", "IRA-RECHARACTER-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Educational_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Educational_Amt", "IRA-EDUCATIONAL-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Fmv_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Fmv_Amt", "IRA-FMV-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Roth_Conv_Amt = ira_Record.newFieldInGroup("ira_Record_Ira_Roth_Conv_Amt", "IRA-ROTH-CONV-AMT", FieldType.DECIMAL, 11,2);
        ira_Record_Ira_Name = ira_Record.newFieldInGroup("ira_Record_Ira_Name", "IRA-NAME", FieldType.STRING, 35);
        ira_Record_Ira_Addr_1 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_1", "IRA-ADDR-1", FieldType.STRING, 35);
        ira_Record_Ira_Addr_2 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_2", "IRA-ADDR-2", FieldType.STRING, 35);
        ira_Record_Ira_Addr_3 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_3", "IRA-ADDR-3", FieldType.STRING, 35);
        ira_Record_Ira_Addr_4 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_4", "IRA-ADDR-4", FieldType.STRING, 35);
        ira_Record_Ira_Addr_5 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_5", "IRA-ADDR-5", FieldType.STRING, 35);
        ira_Record_Ira_Addr_6 = ira_Record.newFieldInGroup("ira_Record_Ira_Addr_6", "IRA-ADDR-6", FieldType.STRING, 35);
        ira_Record_Ira_City = ira_Record.newFieldInGroup("ira_Record_Ira_City", "IRA-CITY", FieldType.STRING, 35);
        ira_Record_Ira_State = ira_Record.newFieldInGroup("ira_Record_Ira_State", "IRA-STATE", FieldType.STRING, 2);
        ira_Record_Ira_Zip_Code = ira_Record.newFieldInGroup("ira_Record_Ira_Zip_Code", "IRA-ZIP-CODE", FieldType.STRING, 9);
        ira_Record_Ira_Foreign_Address = ira_Record.newFieldInGroup("ira_Record_Ira_Foreign_Address", "IRA-FOREIGN-ADDRESS", FieldType.STRING, 1);
        ira_Record_Ira_Foreign_Country = ira_Record.newFieldInGroup("ira_Record_Ira_Foreign_Country", "IRA-FOREIGN-COUNTRY", FieldType.STRING, 35);
        ira_Record_Ira_Rmd_Required_Flag = ira_Record.newFieldInGroup("ira_Record_Ira_Rmd_Required_Flag", "IRA-RMD-REQUIRED-FLAG", FieldType.STRING, 1);
        ira_Record_Ira_Rmd_Decedent_Name = ira_Record.newFieldInGroup("ira_Record_Ira_Rmd_Decedent_Name", "IRA-RMD-DECEDENT-NAME", FieldType.STRING, 35);
        ira_Record_Ira_Unique_Id = ira_Record.newFieldInGroup("ira_Record_Ira_Unique_Id", "IRA-UNIQUE-ID", FieldType.STRING, 15);

        this.setRecordName("LdaTwrl465a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl465a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
