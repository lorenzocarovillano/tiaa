/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:40 PM
**        * FROM NATURAL LDA     : TWRL443F
************************************************************
**        * FILE NAME            : LdaTwrl443f.java
**        * CLASS NAME           : LdaTwrl443f
**        * INSTANCE NAME        : LdaTwrl443f
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443f extends DbsRecord
{
    // Properties
    private DbsGroup pnd_F_Tape;
    private DbsField pnd_F_Tape_Pnd_F_Record_Type;
    private DbsField pnd_F_Tape_Pnd_F_Number_Of_A_Records;
    private DbsField pnd_F_Tape_Pnd_F_Filler_1;
    private DbsField pnd_F_Tape_Pnd_F_Filler_2;
    private DbsField pnd_F_Tape_Pnd_F_Total_B_Records;
    private DbsField pnd_F_Tape_Pnd_F_Filler_3;
    private DbsField pnd_F_Tape_Pnd_F_Filler_4;
    private DbsField pnd_F_Tape_Pnd_F_Sequence_Number;
    private DbsField pnd_F_Tape_Pnd_F_Filler_5;
    private DbsField pnd_F_Tape_Pnd_F_Blank_Cr_Lf;
    private DbsGroup pnd_F_TapeRedef1;
    private DbsField pnd_F_Tape_Pnd_F_Move_1;
    private DbsField pnd_F_Tape_Pnd_F_Move_2;
    private DbsField pnd_F_Tape_Pnd_F_Move_3;

    public DbsGroup getPnd_F_Tape() { return pnd_F_Tape; }

    public DbsField getPnd_F_Tape_Pnd_F_Record_Type() { return pnd_F_Tape_Pnd_F_Record_Type; }

    public DbsField getPnd_F_Tape_Pnd_F_Number_Of_A_Records() { return pnd_F_Tape_Pnd_F_Number_Of_A_Records; }

    public DbsField getPnd_F_Tape_Pnd_F_Filler_1() { return pnd_F_Tape_Pnd_F_Filler_1; }

    public DbsField getPnd_F_Tape_Pnd_F_Filler_2() { return pnd_F_Tape_Pnd_F_Filler_2; }

    public DbsField getPnd_F_Tape_Pnd_F_Total_B_Records() { return pnd_F_Tape_Pnd_F_Total_B_Records; }

    public DbsField getPnd_F_Tape_Pnd_F_Filler_3() { return pnd_F_Tape_Pnd_F_Filler_3; }

    public DbsField getPnd_F_Tape_Pnd_F_Filler_4() { return pnd_F_Tape_Pnd_F_Filler_4; }

    public DbsField getPnd_F_Tape_Pnd_F_Sequence_Number() { return pnd_F_Tape_Pnd_F_Sequence_Number; }

    public DbsField getPnd_F_Tape_Pnd_F_Filler_5() { return pnd_F_Tape_Pnd_F_Filler_5; }

    public DbsField getPnd_F_Tape_Pnd_F_Blank_Cr_Lf() { return pnd_F_Tape_Pnd_F_Blank_Cr_Lf; }

    public DbsGroup getPnd_F_TapeRedef1() { return pnd_F_TapeRedef1; }

    public DbsField getPnd_F_Tape_Pnd_F_Move_1() { return pnd_F_Tape_Pnd_F_Move_1; }

    public DbsField getPnd_F_Tape_Pnd_F_Move_2() { return pnd_F_Tape_Pnd_F_Move_2; }

    public DbsField getPnd_F_Tape_Pnd_F_Move_3() { return pnd_F_Tape_Pnd_F_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_F_Tape = newGroupInRecord("pnd_F_Tape", "#F-TAPE");
        pnd_F_Tape_Pnd_F_Record_Type = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Record_Type", "#F-RECORD-TYPE", FieldType.STRING, 1);
        pnd_F_Tape_Pnd_F_Number_Of_A_Records = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Number_Of_A_Records", "#F-NUMBER-OF-A-RECORDS", FieldType.NUMERIC, 
            8);
        pnd_F_Tape_Pnd_F_Filler_1 = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Filler_1", "#F-FILLER-1", FieldType.NUMERIC, 21);
        pnd_F_Tape_Pnd_F_Filler_2 = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Filler_2", "#F-FILLER-2", FieldType.STRING, 19);
        pnd_F_Tape_Pnd_F_Total_B_Records = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Total_B_Records", "#F-TOTAL-B-RECORDS", FieldType.NUMERIC, 8);
        pnd_F_Tape_Pnd_F_Filler_3 = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Filler_3", "#F-FILLER-3", FieldType.STRING, 250);
        pnd_F_Tape_Pnd_F_Filler_4 = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Filler_4", "#F-FILLER-4", FieldType.STRING, 192);
        pnd_F_Tape_Pnd_F_Sequence_Number = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Sequence_Number", "#F-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_F_Tape_Pnd_F_Filler_5 = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Filler_5", "#F-FILLER-5", FieldType.STRING, 241);
        pnd_F_Tape_Pnd_F_Blank_Cr_Lf = pnd_F_Tape.newFieldInGroup("pnd_F_Tape_Pnd_F_Blank_Cr_Lf", "#F-BLANK-CR-LF", FieldType.STRING, 2);
        pnd_F_TapeRedef1 = newGroupInRecord("pnd_F_TapeRedef1", "Redefines", pnd_F_Tape);
        pnd_F_Tape_Pnd_F_Move_1 = pnd_F_TapeRedef1.newFieldInGroup("pnd_F_Tape_Pnd_F_Move_1", "#F-MOVE-1", FieldType.STRING, 250);
        pnd_F_Tape_Pnd_F_Move_2 = pnd_F_TapeRedef1.newFieldInGroup("pnd_F_Tape_Pnd_F_Move_2", "#F-MOVE-2", FieldType.STRING, 250);
        pnd_F_Tape_Pnd_F_Move_3 = pnd_F_TapeRedef1.newFieldInGroup("pnd_F_Tape_Pnd_F_Move_3", "#F-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443f");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_F_Tape_Pnd_F_Record_Type.setInitialValue("F");
        pnd_F_Tape_Pnd_F_Filler_1.setInitialValue(0);
        pnd_F_Tape_Pnd_F_Filler_2.setInitialValue(" ");
        pnd_F_Tape_Pnd_F_Total_B_Records.setInitialValue(0);
        pnd_F_Tape_Pnd_F_Filler_3.setInitialValue(" ");
        pnd_F_Tape_Pnd_F_Filler_4.setInitialValue(" ");
        pnd_F_Tape_Pnd_F_Sequence_Number.setInitialValue(0);
        pnd_F_Tape_Pnd_F_Filler_5.setInitialValue(" ");
        pnd_F_Tape_Pnd_F_Blank_Cr_Lf.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443f() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
