/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:35 PM
**        * FROM NATURAL LDA     : TWRL3600
************************************************************
**        * FILE NAME            : LdaTwrl3600.java
**        * CLASS NAME           : LdaTwrl3600
**        * INSTANCE NAME        : LdaTwrl3600
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3600 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl3600;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Tax_Year;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Company_Cde;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Tax_Id_Type;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Tin;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Contract_Nbr;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Payee_Cde;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Empty_Form;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Part_Last_Nme;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Part_First_Nme;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln1;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln2;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln3;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln4;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln5;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Addr_Ln6;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Sys_Err;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Code;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Distr;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Loc_Code;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Loc_Distr;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Isn;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Sort_Ind;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Process_Ind;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind;
    private DbsField pnd_Twrl3600_Pnd_Tirf_State_Reporting;
    private DbsField pnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt;

    public DbsGroup getPnd_Twrl3600() { return pnd_Twrl3600; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Tax_Year() { return pnd_Twrl3600_Pnd_Tirf_Tax_Year; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Company_Cde() { return pnd_Twrl3600_Pnd_Tirf_Company_Cde; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Tax_Id_Type() { return pnd_Twrl3600_Pnd_Tirf_Tax_Id_Type; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Tin() { return pnd_Twrl3600_Pnd_Tirf_Tin; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Contract_Nbr() { return pnd_Twrl3600_Pnd_Tirf_Contract_Nbr; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Payee_Cde() { return pnd_Twrl3600_Pnd_Tirf_Payee_Cde; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Empty_Form() { return pnd_Twrl3600_Pnd_Tirf_Empty_Form; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Part_Last_Nme() { return pnd_Twrl3600_Pnd_Tirf_Part_Last_Nme; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Part_First_Nme() { return pnd_Twrl3600_Pnd_Tirf_Part_First_Nme; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme() { return pnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln1() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln1; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln2() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln2; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln3() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln3; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln4() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln4; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln5() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln5; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Addr_Ln6() { return pnd_Twrl3600_Pnd_Tirf_Addr_Ln6; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Sys_Err() { return pnd_Twrl3600_Pnd_Tirf_Sys_Err; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Code() { return pnd_Twrl3600_Pnd_Tirf_State_Code; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Distr() { return pnd_Twrl3600_Pnd_Tirf_State_Distr; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld() { return pnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Loc_Code() { return pnd_Twrl3600_Pnd_Tirf_Loc_Code; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Loc_Distr() { return pnd_Twrl3600_Pnd_Tirf_Loc_Distr; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld() { return pnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Isn() { return pnd_Twrl3600_Pnd_Tirf_Isn; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Sort_Ind() { return pnd_Twrl3600_Pnd_Tirf_Sort_Ind; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Process_Ind() { return pnd_Twrl3600_Pnd_Tirf_Process_Ind; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind() { return pnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind() { return pnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind() { return pnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_State_Reporting() { return pnd_Twrl3600_Pnd_Tirf_State_Reporting; }

    public DbsField getPnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt() { return pnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl3600 = newGroupInRecord("pnd_Twrl3600", "#TWRL3600");
        pnd_Twrl3600_Pnd_Tirf_Tax_Year = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Tax_Year", "#TIRF-TAX-YEAR", FieldType.STRING, 4);
        pnd_Twrl3600_Pnd_Tirf_Company_Cde = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Company_Cde", "#TIRF-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_Tax_Id_Type = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Tax_Id_Type", "#TIRF-TAX-ID-TYPE", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_Tin = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Tin", "#TIRF-TIN", FieldType.STRING, 10);
        pnd_Twrl3600_Pnd_Tirf_Contract_Nbr = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Contract_Nbr", "#TIRF-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Twrl3600_Pnd_Tirf_Payee_Cde = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Payee_Cde", "#TIRF-PAYEE-CDE", FieldType.STRING, 2);
        pnd_Twrl3600_Pnd_Tirf_Empty_Form = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Empty_Form", "#TIRF-EMPTY-FORM", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_Part_Last_Nme = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Part_Last_Nme", "#TIRF-PART-LAST-NME", FieldType.STRING, 
            30);
        pnd_Twrl3600_Pnd_Tirf_Part_First_Nme = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Part_First_Nme", "#TIRF-PART-FIRST-NME", FieldType.STRING, 
            30);
        pnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Part_Mddle_Nme", "#TIRF-PART-MDDLE-NME", FieldType.STRING, 
            30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln1 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln1", "#TIRF-ADDR-LN1", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln2 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln2", "#TIRF-ADDR-LN2", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln3 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln3", "#TIRF-ADDR-LN3", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln4 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln4", "#TIRF-ADDR-LN4", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln5 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln5", "#TIRF-ADDR-LN5", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Addr_Ln6 = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Addr_Ln6", "#TIRF-ADDR-LN6", FieldType.STRING, 30);
        pnd_Twrl3600_Pnd_Tirf_Sys_Err = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Sys_Err", "#TIRF-SYS-ERR", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_State_Code = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Code", "#TIRF-STATE-CODE", FieldType.STRING, 2);
        pnd_Twrl3600_Pnd_Tirf_State_Distr = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Distr", "#TIRF-STATE-DISTR", FieldType.DECIMAL, 
            11,2);
        pnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Tax_Wthld", "#TIRF-STATE-TAX-WTHLD", FieldType.DECIMAL, 
            9,2);
        pnd_Twrl3600_Pnd_Tirf_Loc_Code = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Loc_Code", "#TIRF-LOC-CODE", FieldType.STRING, 5);
        pnd_Twrl3600_Pnd_Tirf_Loc_Distr = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Loc_Distr", "#TIRF-LOC-DISTR", FieldType.DECIMAL, 11,2);
        pnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Loc_Tax_Wthld", "#TIRF-LOC-TAX-WTHLD", FieldType.DECIMAL, 
            9,2);
        pnd_Twrl3600_Pnd_Tirf_Isn = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Isn", "#TIRF-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Twrl3600_Pnd_Tirf_Sort_Ind = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Sort_Ind", "#TIRF-SORT-IND", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_Process_Ind = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Process_Ind", "#TIRF-PROCESS-IND", FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Rpt_Ind", "#TIRF-STATE-RPT-IND", FieldType.STRING, 
            1);
        pnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Hardcopy_Ind", "#TIRF-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1);
        pnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Res_Reject_Ind", "#TIRF-RES-REJECT-IND", FieldType.STRING, 
            1);
        pnd_Twrl3600_Pnd_Tirf_State_Reporting = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_State_Reporting", "#TIRF-STATE-REPORTING", FieldType.STRING, 
            1);
        pnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt = pnd_Twrl3600.newFieldInGroup("pnd_Twrl3600_Pnd_Tirf_Res_Irr_Amt", "#TIRF-RES-IRR-AMT", FieldType.PACKED_DECIMAL, 
            11,2);

        this.setRecordName("LdaTwrl3600");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3600() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
