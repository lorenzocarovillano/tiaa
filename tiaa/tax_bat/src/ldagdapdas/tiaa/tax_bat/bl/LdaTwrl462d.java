/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:51 PM
**        * FROM NATURAL LDA     : TWRL462D
************************************************************
**        * FILE NAME            : LdaTwrl462d.java
**        * CLASS NAME           : LdaTwrl462d
**        * INSTANCE NAME        : LdaTwrl462d
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl462d extends DbsRecord
{
    // Properties
    private DbsGroup pnd_T;
    private DbsField pnd_T_Irat_Source;
    private DbsField pnd_T_Irat_Company_Code;
    private DbsField pnd_T_Irat_Tax_Year;
    private DbsField pnd_T_Irat_Feed_Period;
    private DbsField pnd_T_Irat_Feed_Date;
    private DbsField pnd_T_Irat_Records_Tot;
    private DbsField pnd_T_Irat_Classic_Tot;
    private DbsField pnd_T_Irat_Roth_Tot;
    private DbsField pnd_T_Irat_Rollover_Tot;
    private DbsField pnd_T_Irat_Rechar_Tot;
    private DbsField pnd_T_Irat_Sep_Tot;
    private DbsField pnd_T_Irat_Fmv_Tot;
    private DbsField pnd_T_Irat_Classic_To_Roth_Tot;
    private DbsGroup pnd_C;
    private DbsField pnd_C_Irat_Source;
    private DbsField pnd_C_Irat_Company_Code;
    private DbsField pnd_C_Irat_Tax_Year;
    private DbsField pnd_C_Irat_Feed_Period;
    private DbsField pnd_C_Irat_Feed_Date;
    private DbsField pnd_C_Irat_Records_Tot;
    private DbsField pnd_C_Irat_Classic_Tot;
    private DbsField pnd_C_Irat_Roth_Tot;
    private DbsField pnd_C_Irat_Rollover_Tot;
    private DbsField pnd_C_Irat_Rechar_Tot;
    private DbsField pnd_C_Irat_Sep_Tot;
    private DbsField pnd_C_Irat_Fmv_Tot;
    private DbsField pnd_C_Irat_Classic_To_Roth_Tot;
    private DbsGroup pnd_L;
    private DbsField pnd_L_Irat_Source;
    private DbsField pnd_L_Irat_Company_Code;
    private DbsField pnd_L_Irat_Tax_Year;
    private DbsField pnd_L_Irat_Feed_Period;
    private DbsField pnd_L_Irat_Feed_Date;
    private DbsField pnd_L_Irat_Records_Tot;
    private DbsField pnd_L_Irat_Classic_Tot;
    private DbsField pnd_L_Irat_Roth_Tot;
    private DbsField pnd_L_Irat_Rollover_Tot;
    private DbsField pnd_L_Irat_Rechar_Tot;
    private DbsField pnd_L_Irat_Sep_Tot;
    private DbsField pnd_L_Irat_Fmv_Tot;
    private DbsField pnd_L_Irat_Classic_To_Roth_Tot;
    private DbsGroup pnd_S;
    private DbsField pnd_S_Irat_Source;
    private DbsField pnd_S_Irat_Company_Code;
    private DbsField pnd_S_Irat_Tax_Year;
    private DbsField pnd_S_Irat_Feed_Period;
    private DbsField pnd_S_Irat_Feed_Date;
    private DbsField pnd_S_Irat_Records_Tot;
    private DbsField pnd_S_Irat_Classic_Tot;
    private DbsField pnd_S_Irat_Roth_Tot;
    private DbsField pnd_S_Irat_Rollover_Tot;
    private DbsField pnd_S_Irat_Rechar_Tot;
    private DbsField pnd_S_Irat_Sep_Tot;
    private DbsField pnd_S_Irat_Fmv_Tot;
    private DbsField pnd_S_Irat_Classic_To_Roth_Tot;

    public DbsGroup getPnd_T() { return pnd_T; }

    public DbsField getPnd_T_Irat_Source() { return pnd_T_Irat_Source; }

    public DbsField getPnd_T_Irat_Company_Code() { return pnd_T_Irat_Company_Code; }

    public DbsField getPnd_T_Irat_Tax_Year() { return pnd_T_Irat_Tax_Year; }

    public DbsField getPnd_T_Irat_Feed_Period() { return pnd_T_Irat_Feed_Period; }

    public DbsField getPnd_T_Irat_Feed_Date() { return pnd_T_Irat_Feed_Date; }

    public DbsField getPnd_T_Irat_Records_Tot() { return pnd_T_Irat_Records_Tot; }

    public DbsField getPnd_T_Irat_Classic_Tot() { return pnd_T_Irat_Classic_Tot; }

    public DbsField getPnd_T_Irat_Roth_Tot() { return pnd_T_Irat_Roth_Tot; }

    public DbsField getPnd_T_Irat_Rollover_Tot() { return pnd_T_Irat_Rollover_Tot; }

    public DbsField getPnd_T_Irat_Rechar_Tot() { return pnd_T_Irat_Rechar_Tot; }

    public DbsField getPnd_T_Irat_Sep_Tot() { return pnd_T_Irat_Sep_Tot; }

    public DbsField getPnd_T_Irat_Fmv_Tot() { return pnd_T_Irat_Fmv_Tot; }

    public DbsField getPnd_T_Irat_Classic_To_Roth_Tot() { return pnd_T_Irat_Classic_To_Roth_Tot; }

    public DbsGroup getPnd_C() { return pnd_C; }

    public DbsField getPnd_C_Irat_Source() { return pnd_C_Irat_Source; }

    public DbsField getPnd_C_Irat_Company_Code() { return pnd_C_Irat_Company_Code; }

    public DbsField getPnd_C_Irat_Tax_Year() { return pnd_C_Irat_Tax_Year; }

    public DbsField getPnd_C_Irat_Feed_Period() { return pnd_C_Irat_Feed_Period; }

    public DbsField getPnd_C_Irat_Feed_Date() { return pnd_C_Irat_Feed_Date; }

    public DbsField getPnd_C_Irat_Records_Tot() { return pnd_C_Irat_Records_Tot; }

    public DbsField getPnd_C_Irat_Classic_Tot() { return pnd_C_Irat_Classic_Tot; }

    public DbsField getPnd_C_Irat_Roth_Tot() { return pnd_C_Irat_Roth_Tot; }

    public DbsField getPnd_C_Irat_Rollover_Tot() { return pnd_C_Irat_Rollover_Tot; }

    public DbsField getPnd_C_Irat_Rechar_Tot() { return pnd_C_Irat_Rechar_Tot; }

    public DbsField getPnd_C_Irat_Sep_Tot() { return pnd_C_Irat_Sep_Tot; }

    public DbsField getPnd_C_Irat_Fmv_Tot() { return pnd_C_Irat_Fmv_Tot; }

    public DbsField getPnd_C_Irat_Classic_To_Roth_Tot() { return pnd_C_Irat_Classic_To_Roth_Tot; }

    public DbsGroup getPnd_L() { return pnd_L; }

    public DbsField getPnd_L_Irat_Source() { return pnd_L_Irat_Source; }

    public DbsField getPnd_L_Irat_Company_Code() { return pnd_L_Irat_Company_Code; }

    public DbsField getPnd_L_Irat_Tax_Year() { return pnd_L_Irat_Tax_Year; }

    public DbsField getPnd_L_Irat_Feed_Period() { return pnd_L_Irat_Feed_Period; }

    public DbsField getPnd_L_Irat_Feed_Date() { return pnd_L_Irat_Feed_Date; }

    public DbsField getPnd_L_Irat_Records_Tot() { return pnd_L_Irat_Records_Tot; }

    public DbsField getPnd_L_Irat_Classic_Tot() { return pnd_L_Irat_Classic_Tot; }

    public DbsField getPnd_L_Irat_Roth_Tot() { return pnd_L_Irat_Roth_Tot; }

    public DbsField getPnd_L_Irat_Rollover_Tot() { return pnd_L_Irat_Rollover_Tot; }

    public DbsField getPnd_L_Irat_Rechar_Tot() { return pnd_L_Irat_Rechar_Tot; }

    public DbsField getPnd_L_Irat_Sep_Tot() { return pnd_L_Irat_Sep_Tot; }

    public DbsField getPnd_L_Irat_Fmv_Tot() { return pnd_L_Irat_Fmv_Tot; }

    public DbsField getPnd_L_Irat_Classic_To_Roth_Tot() { return pnd_L_Irat_Classic_To_Roth_Tot; }

    public DbsGroup getPnd_S() { return pnd_S; }

    public DbsField getPnd_S_Irat_Source() { return pnd_S_Irat_Source; }

    public DbsField getPnd_S_Irat_Company_Code() { return pnd_S_Irat_Company_Code; }

    public DbsField getPnd_S_Irat_Tax_Year() { return pnd_S_Irat_Tax_Year; }

    public DbsField getPnd_S_Irat_Feed_Period() { return pnd_S_Irat_Feed_Period; }

    public DbsField getPnd_S_Irat_Feed_Date() { return pnd_S_Irat_Feed_Date; }

    public DbsField getPnd_S_Irat_Records_Tot() { return pnd_S_Irat_Records_Tot; }

    public DbsField getPnd_S_Irat_Classic_Tot() { return pnd_S_Irat_Classic_Tot; }

    public DbsField getPnd_S_Irat_Roth_Tot() { return pnd_S_Irat_Roth_Tot; }

    public DbsField getPnd_S_Irat_Rollover_Tot() { return pnd_S_Irat_Rollover_Tot; }

    public DbsField getPnd_S_Irat_Rechar_Tot() { return pnd_S_Irat_Rechar_Tot; }

    public DbsField getPnd_S_Irat_Sep_Tot() { return pnd_S_Irat_Sep_Tot; }

    public DbsField getPnd_S_Irat_Fmv_Tot() { return pnd_S_Irat_Fmv_Tot; }

    public DbsField getPnd_S_Irat_Classic_To_Roth_Tot() { return pnd_S_Irat_Classic_To_Roth_Tot; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_T = newGroupInRecord("pnd_T", "#T");
        pnd_T_Irat_Source = pnd_T.newFieldInGroup("pnd_T_Irat_Source", "IRAT-SOURCE", FieldType.STRING, 3);
        pnd_T_Irat_Company_Code = pnd_T.newFieldInGroup("pnd_T_Irat_Company_Code", "IRAT-COMPANY-CODE", FieldType.STRING, 1);
        pnd_T_Irat_Tax_Year = pnd_T.newFieldInGroup("pnd_T_Irat_Tax_Year", "IRAT-TAX-YEAR", FieldType.STRING, 4);
        pnd_T_Irat_Feed_Period = pnd_T.newFieldInGroup("pnd_T_Irat_Feed_Period", "IRAT-FEED-PERIOD", FieldType.STRING, 3);
        pnd_T_Irat_Feed_Date = pnd_T.newFieldInGroup("pnd_T_Irat_Feed_Date", "IRAT-FEED-DATE", FieldType.STRING, 8);
        pnd_T_Irat_Records_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Records_Tot", "IRAT-RECORDS-TOT", FieldType.NUMERIC, 10);
        pnd_T_Irat_Classic_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Classic_Tot", "IRAT-CLASSIC-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Roth_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Roth_Tot", "IRAT-ROTH-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Rollover_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Rollover_Tot", "IRAT-ROLLOVER-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Rechar_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Rechar_Tot", "IRAT-RECHAR-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Sep_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Sep_Tot", "IRAT-SEP-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Fmv_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Fmv_Tot", "IRAT-FMV-TOT", FieldType.DECIMAL, 17,2);
        pnd_T_Irat_Classic_To_Roth_Tot = pnd_T.newFieldInGroup("pnd_T_Irat_Classic_To_Roth_Tot", "IRAT-CLASSIC-TO-ROTH-TOT", FieldType.DECIMAL, 17,2);

        pnd_C = newGroupInRecord("pnd_C", "#C");
        pnd_C_Irat_Source = pnd_C.newFieldInGroup("pnd_C_Irat_Source", "IRAT-SOURCE", FieldType.STRING, 3);
        pnd_C_Irat_Company_Code = pnd_C.newFieldInGroup("pnd_C_Irat_Company_Code", "IRAT-COMPANY-CODE", FieldType.STRING, 1);
        pnd_C_Irat_Tax_Year = pnd_C.newFieldInGroup("pnd_C_Irat_Tax_Year", "IRAT-TAX-YEAR", FieldType.STRING, 4);
        pnd_C_Irat_Feed_Period = pnd_C.newFieldInGroup("pnd_C_Irat_Feed_Period", "IRAT-FEED-PERIOD", FieldType.STRING, 3);
        pnd_C_Irat_Feed_Date = pnd_C.newFieldInGroup("pnd_C_Irat_Feed_Date", "IRAT-FEED-DATE", FieldType.STRING, 8);
        pnd_C_Irat_Records_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Records_Tot", "IRAT-RECORDS-TOT", FieldType.NUMERIC, 10);
        pnd_C_Irat_Classic_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Classic_Tot", "IRAT-CLASSIC-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Roth_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Roth_Tot", "IRAT-ROTH-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Rollover_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Rollover_Tot", "IRAT-ROLLOVER-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Rechar_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Rechar_Tot", "IRAT-RECHAR-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Sep_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Sep_Tot", "IRAT-SEP-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Fmv_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Fmv_Tot", "IRAT-FMV-TOT", FieldType.DECIMAL, 17,2);
        pnd_C_Irat_Classic_To_Roth_Tot = pnd_C.newFieldInGroup("pnd_C_Irat_Classic_To_Roth_Tot", "IRAT-CLASSIC-TO-ROTH-TOT", FieldType.DECIMAL, 17,2);

        pnd_L = newGroupInRecord("pnd_L", "#L");
        pnd_L_Irat_Source = pnd_L.newFieldInGroup("pnd_L_Irat_Source", "IRAT-SOURCE", FieldType.STRING, 3);
        pnd_L_Irat_Company_Code = pnd_L.newFieldInGroup("pnd_L_Irat_Company_Code", "IRAT-COMPANY-CODE", FieldType.STRING, 1);
        pnd_L_Irat_Tax_Year = pnd_L.newFieldInGroup("pnd_L_Irat_Tax_Year", "IRAT-TAX-YEAR", FieldType.STRING, 4);
        pnd_L_Irat_Feed_Period = pnd_L.newFieldInGroup("pnd_L_Irat_Feed_Period", "IRAT-FEED-PERIOD", FieldType.STRING, 3);
        pnd_L_Irat_Feed_Date = pnd_L.newFieldInGroup("pnd_L_Irat_Feed_Date", "IRAT-FEED-DATE", FieldType.STRING, 8);
        pnd_L_Irat_Records_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Records_Tot", "IRAT-RECORDS-TOT", FieldType.NUMERIC, 10);
        pnd_L_Irat_Classic_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Classic_Tot", "IRAT-CLASSIC-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Roth_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Roth_Tot", "IRAT-ROTH-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Rollover_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Rollover_Tot", "IRAT-ROLLOVER-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Rechar_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Rechar_Tot", "IRAT-RECHAR-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Sep_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Sep_Tot", "IRAT-SEP-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Fmv_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Fmv_Tot", "IRAT-FMV-TOT", FieldType.DECIMAL, 17,2);
        pnd_L_Irat_Classic_To_Roth_Tot = pnd_L.newFieldInGroup("pnd_L_Irat_Classic_To_Roth_Tot", "IRAT-CLASSIC-TO-ROTH-TOT", FieldType.DECIMAL, 17,2);

        pnd_S = newGroupInRecord("pnd_S", "#S");
        pnd_S_Irat_Source = pnd_S.newFieldInGroup("pnd_S_Irat_Source", "IRAT-SOURCE", FieldType.STRING, 3);
        pnd_S_Irat_Company_Code = pnd_S.newFieldInGroup("pnd_S_Irat_Company_Code", "IRAT-COMPANY-CODE", FieldType.STRING, 1);
        pnd_S_Irat_Tax_Year = pnd_S.newFieldInGroup("pnd_S_Irat_Tax_Year", "IRAT-TAX-YEAR", FieldType.STRING, 4);
        pnd_S_Irat_Feed_Period = pnd_S.newFieldInGroup("pnd_S_Irat_Feed_Period", "IRAT-FEED-PERIOD", FieldType.STRING, 3);
        pnd_S_Irat_Feed_Date = pnd_S.newFieldInGroup("pnd_S_Irat_Feed_Date", "IRAT-FEED-DATE", FieldType.STRING, 8);
        pnd_S_Irat_Records_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Records_Tot", "IRAT-RECORDS-TOT", FieldType.NUMERIC, 10);
        pnd_S_Irat_Classic_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Classic_Tot", "IRAT-CLASSIC-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Roth_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Roth_Tot", "IRAT-ROTH-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Rollover_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Rollover_Tot", "IRAT-ROLLOVER-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Rechar_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Rechar_Tot", "IRAT-RECHAR-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Sep_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Sep_Tot", "IRAT-SEP-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Fmv_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Fmv_Tot", "IRAT-FMV-TOT", FieldType.DECIMAL, 17,2);
        pnd_S_Irat_Classic_To_Roth_Tot = pnd_S.newFieldInGroup("pnd_S_Irat_Classic_To_Roth_Tot", "IRAT-CLASSIC-TO-ROTH-TOT", FieldType.DECIMAL, 17,2);

        this.setRecordName("LdaTwrl462d");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl462d() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
