/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:26 PM
**        * FROM NATURAL LDA     : TWRL1905
************************************************************
**        * FILE NAME            : LdaTwrl1905.java
**        * CLASS NAME           : LdaTwrl1905
**        * INSTANCE NAME        : LdaTwrl1905
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl1905 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Tx_Dw_Contr;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Tax_Yr;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep01;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Company_Cd;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep02;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Company_Short_Nm;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep03;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Tin;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep04;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Tin_Type;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep05;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Tin_Desc;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep06;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Contract;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep07;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Payee;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep08;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Srce_Orig;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep09;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Srce_Updt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep10;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep11;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Desc;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep12;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Classic_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep13;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep14;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Roth_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep15;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Rollover_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep16;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Rechar_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep17;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep18;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Fmv_Amt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep19;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Create_Dt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep20;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Updt_Dt;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep21;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Create_User;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep22;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Updt_User;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep23;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Origin_Area_Num;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep24;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Origin_Area_Short_Desc;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep25;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Origin_Area_Desc;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep26;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Error_Reason_Code;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Sep27;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Error_Reason_Desc;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Filler;
    private DbsGroup pnd_Tx_Dw_Contr_Pnd_FillerRedef1;
    private DbsField pnd_Tx_Dw_Contr_Pnd_Status;

    public DbsGroup getPnd_Tx_Dw_Contr() { return pnd_Tx_Dw_Contr; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Tax_Yr() { return pnd_Tx_Dw_Contr_Pnd_Tax_Yr; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep01() { return pnd_Tx_Dw_Contr_Pnd_Sep01; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Company_Cd() { return pnd_Tx_Dw_Contr_Pnd_Company_Cd; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep02() { return pnd_Tx_Dw_Contr_Pnd_Sep02; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Company_Short_Nm() { return pnd_Tx_Dw_Contr_Pnd_Company_Short_Nm; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep03() { return pnd_Tx_Dw_Contr_Pnd_Sep03; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Tin() { return pnd_Tx_Dw_Contr_Pnd_Tin; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep04() { return pnd_Tx_Dw_Contr_Pnd_Sep04; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Tin_Type() { return pnd_Tx_Dw_Contr_Pnd_Tin_Type; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep05() { return pnd_Tx_Dw_Contr_Pnd_Sep05; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Tin_Desc() { return pnd_Tx_Dw_Contr_Pnd_Tin_Desc; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep06() { return pnd_Tx_Dw_Contr_Pnd_Sep06; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Contract() { return pnd_Tx_Dw_Contr_Pnd_Contract; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep07() { return pnd_Tx_Dw_Contr_Pnd_Sep07; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Payee() { return pnd_Tx_Dw_Contr_Pnd_Payee; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep08() { return pnd_Tx_Dw_Contr_Pnd_Sep08; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Srce_Orig() { return pnd_Tx_Dw_Contr_Pnd_Srce_Orig; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep09() { return pnd_Tx_Dw_Contr_Pnd_Sep09; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Srce_Updt() { return pnd_Tx_Dw_Contr_Pnd_Srce_Updt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep10() { return pnd_Tx_Dw_Contr_Pnd_Sep10; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type() { return pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep11() { return pnd_Tx_Dw_Contr_Pnd_Sep11; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Ira_Contract_Desc() { return pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Desc; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep12() { return pnd_Tx_Dw_Contr_Pnd_Sep12; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Classic_Amt() { return pnd_Tx_Dw_Contr_Pnd_Classic_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep13() { return pnd_Tx_Dw_Contr_Pnd_Sep13; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep_Amt() { return pnd_Tx_Dw_Contr_Pnd_Sep_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep14() { return pnd_Tx_Dw_Contr_Pnd_Sep14; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Roth_Amt() { return pnd_Tx_Dw_Contr_Pnd_Roth_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep15() { return pnd_Tx_Dw_Contr_Pnd_Sep15; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Rollover_Amt() { return pnd_Tx_Dw_Contr_Pnd_Rollover_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep16() { return pnd_Tx_Dw_Contr_Pnd_Sep16; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Rechar_Amt() { return pnd_Tx_Dw_Contr_Pnd_Rechar_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep17() { return pnd_Tx_Dw_Contr_Pnd_Sep17; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt() { return pnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep18() { return pnd_Tx_Dw_Contr_Pnd_Sep18; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Fmv_Amt() { return pnd_Tx_Dw_Contr_Pnd_Fmv_Amt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep19() { return pnd_Tx_Dw_Contr_Pnd_Sep19; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Create_Dt() { return pnd_Tx_Dw_Contr_Pnd_Create_Dt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep20() { return pnd_Tx_Dw_Contr_Pnd_Sep20; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Updt_Dt() { return pnd_Tx_Dw_Contr_Pnd_Updt_Dt; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep21() { return pnd_Tx_Dw_Contr_Pnd_Sep21; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Create_User() { return pnd_Tx_Dw_Contr_Pnd_Create_User; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep22() { return pnd_Tx_Dw_Contr_Pnd_Sep22; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Updt_User() { return pnd_Tx_Dw_Contr_Pnd_Updt_User; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep23() { return pnd_Tx_Dw_Contr_Pnd_Sep23; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Origin_Area_Num() { return pnd_Tx_Dw_Contr_Pnd_Origin_Area_Num; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep24() { return pnd_Tx_Dw_Contr_Pnd_Sep24; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Origin_Area_Short_Desc() { return pnd_Tx_Dw_Contr_Pnd_Origin_Area_Short_Desc; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep25() { return pnd_Tx_Dw_Contr_Pnd_Sep25; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Origin_Area_Desc() { return pnd_Tx_Dw_Contr_Pnd_Origin_Area_Desc; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep26() { return pnd_Tx_Dw_Contr_Pnd_Sep26; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Error_Reason_Code() { return pnd_Tx_Dw_Contr_Pnd_Error_Reason_Code; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Sep27() { return pnd_Tx_Dw_Contr_Pnd_Sep27; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Error_Reason_Desc() { return pnd_Tx_Dw_Contr_Pnd_Error_Reason_Desc; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Filler() { return pnd_Tx_Dw_Contr_Pnd_Filler; }

    public DbsGroup getPnd_Tx_Dw_Contr_Pnd_FillerRedef1() { return pnd_Tx_Dw_Contr_Pnd_FillerRedef1; }

    public DbsField getPnd_Tx_Dw_Contr_Pnd_Status() { return pnd_Tx_Dw_Contr_Pnd_Status; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Tx_Dw_Contr = newGroupInRecord("pnd_Tx_Dw_Contr", "#TX-DW-CONTR");
        pnd_Tx_Dw_Contr_Pnd_Tax_Yr = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Tax_Yr", "#TAX-YR", FieldType.STRING, 4);
        pnd_Tx_Dw_Contr_Pnd_Sep01 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep01", "#SEP01", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Company_Cd = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Company_Cd", "#COMPANY-CD", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Sep02 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep02", "#SEP02", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Company_Short_Nm = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Company_Short_Nm", "#COMPANY-SHORT-NM", FieldType.STRING, 
            4);
        pnd_Tx_Dw_Contr_Pnd_Sep03 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep03", "#SEP03", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Tin = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Tin", "#TIN", FieldType.STRING, 10);
        pnd_Tx_Dw_Contr_Pnd_Sep04 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep04", "#SEP04", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Tin_Type = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Tin_Type", "#TIN-TYPE", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Sep05 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep05", "#SEP05", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Tin_Desc = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Tin_Desc", "#TIN-DESC", FieldType.STRING, 7);
        pnd_Tx_Dw_Contr_Pnd_Sep06 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep06", "#SEP06", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Contract = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Tx_Dw_Contr_Pnd_Sep07 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep07", "#SEP07", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Payee = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Payee", "#PAYEE", FieldType.STRING, 2);
        pnd_Tx_Dw_Contr_Pnd_Sep08 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep08", "#SEP08", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Srce_Orig = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Srce_Orig", "#SRCE-ORIG", FieldType.STRING, 3);
        pnd_Tx_Dw_Contr_Pnd_Sep09 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep09", "#SEP09", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Srce_Updt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Srce_Updt", "#SRCE-UPDT", FieldType.STRING, 3);
        pnd_Tx_Dw_Contr_Pnd_Sep10 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep10", "#SEP10", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Type", "#IRA-CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Contr_Pnd_Sep11 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep11", "#SEP11", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Desc = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Ira_Contract_Desc", "#IRA-CONTRACT-DESC", FieldType.STRING, 
            12);
        pnd_Tx_Dw_Contr_Pnd_Sep12 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep12", "#SEP12", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Classic_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Classic_Amt", "#CLASSIC-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep13 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep13", "#SEP13", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Sep_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep_Amt", "#SEP-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep14 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep14", "#SEP14", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Roth_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Roth_Amt", "#ROTH-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep15 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep15", "#SEP15", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Rollover_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Rollover_Amt", "#ROLLOVER-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep16 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep16", "#SEP16", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Rechar_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Rechar_Amt", "#RECHAR-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep17 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep17", "#SEP17", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Roth_Conv_Amt", "#ROTH-CONV-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep18 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep18", "#SEP18", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Fmv_Amt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Fmv_Amt", "#FMV-AMT", FieldType.STRING, 15);
        pnd_Tx_Dw_Contr_Pnd_Sep19 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep19", "#SEP19", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Create_Dt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Create_Dt", "#CREATE-DT", FieldType.STRING, 10);
        pnd_Tx_Dw_Contr_Pnd_Sep20 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep20", "#SEP20", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Updt_Dt = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Updt_Dt", "#UPDT-DT", FieldType.STRING, 10);
        pnd_Tx_Dw_Contr_Pnd_Sep21 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep21", "#SEP21", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Create_User = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Create_User", "#CREATE-USER", FieldType.STRING, 8);
        pnd_Tx_Dw_Contr_Pnd_Sep22 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep22", "#SEP22", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Updt_User = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Updt_User", "#UPDT-USER", FieldType.STRING, 8);
        pnd_Tx_Dw_Contr_Pnd_Sep23 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep23", "#SEP23", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Origin_Area_Num = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Origin_Area_Num", "#ORIGIN-AREA-NUM", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Contr_Pnd_Sep24 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep24", "#SEP24", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Origin_Area_Short_Desc = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Origin_Area_Short_Desc", "#ORIGIN-AREA-SHORT-DESC", 
            FieldType.STRING, 6);
        pnd_Tx_Dw_Contr_Pnd_Sep25 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep25", "#SEP25", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Origin_Area_Desc = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Origin_Area_Desc", "#ORIGIN-AREA-DESC", FieldType.STRING, 
            30);
        pnd_Tx_Dw_Contr_Pnd_Sep26 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep26", "#SEP26", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Error_Reason_Code = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Error_Reason_Code", "#ERROR-REASON-CODE", FieldType.STRING, 
            2);
        pnd_Tx_Dw_Contr_Pnd_Sep27 = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Sep27", "#SEP27", FieldType.STRING, 1);
        pnd_Tx_Dw_Contr_Pnd_Error_Reason_Desc = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Error_Reason_Desc", "#ERROR-REASON-DESC", FieldType.STRING, 
            30);
        pnd_Tx_Dw_Contr_Pnd_Filler = pnd_Tx_Dw_Contr.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Filler", "#FILLER", FieldType.STRING, 28);
        pnd_Tx_Dw_Contr_Pnd_FillerRedef1 = pnd_Tx_Dw_Contr.newGroupInGroup("pnd_Tx_Dw_Contr_Pnd_FillerRedef1", "Redefines", pnd_Tx_Dw_Contr_Pnd_Filler);
        pnd_Tx_Dw_Contr_Pnd_Status = pnd_Tx_Dw_Contr_Pnd_FillerRedef1.newFieldInGroup("pnd_Tx_Dw_Contr_Pnd_Status", "#STATUS", FieldType.STRING, 1);

        this.setRecordName("LdaTwrl1905");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Tx_Dw_Contr_Pnd_Sep01.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep02.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep03.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep04.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep05.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep06.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep07.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep08.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep09.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep10.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep11.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep12.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep13.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep14.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep15.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep16.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep17.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep18.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep19.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep20.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep21.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep22.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep23.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep24.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep25.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep26.setInitialValue(";");
        pnd_Tx_Dw_Contr_Pnd_Sep27.setInitialValue(";");
    }

    // Constructor
    public LdaTwrl1905() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
