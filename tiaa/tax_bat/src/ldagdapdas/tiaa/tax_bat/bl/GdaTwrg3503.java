/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:08 PM
**        * FROM NATURAL GDA     : TWRG3503
************************************************************
**        * FILE NAME            : GdaTwrg3503.java
**        * CLASS NAME           : GdaTwrg3503
**        * INSTANCE NAME        : GdaTwrg3503
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaTwrg3503 extends DbsRecord
{
    private static ThreadLocal<List<GdaTwrg3503>> _instance = new ThreadLocal<List<GdaTwrg3503>>();

    // Properties
    private DbsGroup pnd_Gl_Const;
    private DbsField pnd_Gl_Const_Pnd_Prov_Min;
    private DbsField pnd_Gl_Const_Pnd_Prov_Max;
    private DbsField pnd_Gl_Const_Pnd_State_Max;
    private DbsField pnd_Gl_Const_Pnd_State_Max_Alpha;
    private DbsField pnd_Gl_Const_Pnd_Irsr_Max_Lines;
    private DbsField pnd_Gl_Const_Pnd_Irsw_Max_Lines;
    private DbsField pnd_Gl_Const_Pnd_Irsg_Max_Lines;
    private DbsField pnd_Gl_Const_Pnd_Corr_Ind;
    private DbsGroup pnd_A_Rec;
    private DbsField pnd_A_Rec_Pnd_Generate;
    private DbsGroup pnd_C_Rec;
    private DbsField pnd_C_Rec_Pnd_Generate;
    private DbsField pnd_C_Rec_Pnd_Num_Payees;
    private DbsField pnd_C_Rec_Pnd_Amt_1;
    private DbsField pnd_C_Rec_Pnd_Amt_2;
    private DbsField pnd_C_Rec_Pnd_Amt_4;
    private DbsField pnd_C_Rec_Pnd_Amt_5;
    private DbsField pnd_C_Rec_Pnd_Amt_9;
    private DbsField pnd_C_Rec_Pnd_Amt_A;
    private DbsGroup pnd_K_Rec;
    private DbsField pnd_K_Rec_Pnd_Generate;
    private DbsGroup pnd_K_Rec_Pnd_K_Rec_Array;
    private DbsField pnd_K_Rec_Pnd_Num_Payees;
    private DbsField pnd_K_Rec_Pnd_Amt_1;
    private DbsField pnd_K_Rec_Pnd_Amt_2;
    private DbsField pnd_K_Rec_Pnd_Amt_4;
    private DbsField pnd_K_Rec_Pnd_Amt_5;
    private DbsField pnd_K_Rec_Pnd_Amt_9;
    private DbsField pnd_K_Rec_Pnd_Amt_A;
    private DbsField pnd_K_Rec_Pnd_State_Tax;
    private DbsField pnd_K_Rec_Pnd_Local_Tax;
    private DbsGroup pnd_State_Table;
    private DbsField pnd_State_Table_Tircntl_Seq_Nbr;
    private DbsField pnd_State_Table_Tircntl_State_Alpha_Code;
    private DbsField pnd_State_Table_Tircntl_Comb_Fed_Ind;
    private DbsGroup pnd_Prov_Table;
    private DbsField pnd_Prov_Table_Tircntl_State_Alpha_Code;
    private DbsGroup pnd_Report_Indexes;
    private DbsField pnd_Report_Indexes_Pnd_Irsr1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw1;
    private DbsField pnd_Report_Indexes_Pnd_Irsw2;
    private DbsField pnd_Report_Indexes_Pnd_Irsg1;
    private DbsGroup pnd_Irsr;
    private DbsField pnd_Irsr_Pnd_Cmb_State_Tax;
    private DbsField pnd_Irsr_Pnd_Cmb_Local_Tax;
    private DbsGroup pnd_Irsr_Pnd_Irsr_Lines;
    private DbsField pnd_Irsr_Pnd_Header1;
    private DbsField pnd_Irsr_Pnd_Header2;
    private DbsGroup pnd_Irsr_Pnd_Totals;
    private DbsField pnd_Irsr_Pnd_Form_Cnt;
    private DbsField pnd_Irsr_Pnd_Gross_Amt;
    private DbsField pnd_Irsr_Pnd_Ivc_Amt;
    private DbsField pnd_Irsr_Pnd_Taxable_Amt;
    private DbsField pnd_Irsr_Pnd_Int_Amt;
    private DbsField pnd_Irsr_Pnd_Fed_Tax;
    private DbsField pnd_Irsr_Pnd_State_Tax;
    private DbsField pnd_Irsr_Pnd_Local_Tax;
    private DbsGroup pnd_Irsw;
    private DbsField pnd_Irsw_Pnd_Header;
    private DbsGroup pnd_Irsw_Pnd_Totals;
    private DbsField pnd_Irsw_Pnd_Form_Cnt;
    private DbsField pnd_Irsw_Pnd_Gross_Amt;
    private DbsField pnd_Irsw_Pnd_Ivc_Amt;
    private DbsField pnd_Irsw_Pnd_Taxable_Amt;
    private DbsField pnd_Irsw_Pnd_Int_Amt;
    private DbsField pnd_Irsw_Pnd_Fed_Tax;
    private DbsGroup pnd_Irsg;
    private DbsField pnd_Irsg_Pnd_Header;
    private DbsGroup pnd_Irsg_Pnd_Totals;
    private DbsField pnd_Irsg_Pnd_Form_Cnt;
    private DbsField pnd_Irsg_Pnd_Gross_Amt;
    private DbsField pnd_Irsg_Pnd_Ivc_Amt;
    private DbsField pnd_Irsg_Pnd_Taxable_Amt;
    private DbsField pnd_Irsg_Pnd_Int_Amt;
    private DbsField pnd_Irsg_Pnd_Fed_Tax;
    private DbsGroup pnd_Recon_Case;
    private DbsField pnd_Recon_Case_Pnd_Header;
    private DbsField pnd_Recon_Case_Pnd_Tin;
    private DbsField pnd_Recon_Case_Pnd_Cntrct_Py;
    private DbsField pnd_Recon_Case_Pnd_Dist_Code;
    private DbsField pnd_Recon_Case_Pnd_Form_Cnt;
    private DbsField pnd_Recon_Case_Pnd_Gross_Amt;
    private DbsField pnd_Recon_Case_Pnd_Ivc_Amt;
    private DbsField pnd_Recon_Case_Pnd_Taxable_Amt;
    private DbsField pnd_Recon_Case_Pnd_Int_Amt;
    private DbsField pnd_Recon_Case_Pnd_Fed_Tax;

    public DbsGroup getPnd_Gl_Const() { return pnd_Gl_Const; }

    public DbsField getPnd_Gl_Const_Pnd_Prov_Min() { return pnd_Gl_Const_Pnd_Prov_Min; }

    public DbsField getPnd_Gl_Const_Pnd_Prov_Max() { return pnd_Gl_Const_Pnd_Prov_Max; }

    public DbsField getPnd_Gl_Const_Pnd_State_Max() { return pnd_Gl_Const_Pnd_State_Max; }

    public DbsField getPnd_Gl_Const_Pnd_State_Max_Alpha() { return pnd_Gl_Const_Pnd_State_Max_Alpha; }

    public DbsField getPnd_Gl_Const_Pnd_Irsr_Max_Lines() { return pnd_Gl_Const_Pnd_Irsr_Max_Lines; }

    public DbsField getPnd_Gl_Const_Pnd_Irsw_Max_Lines() { return pnd_Gl_Const_Pnd_Irsw_Max_Lines; }

    public DbsField getPnd_Gl_Const_Pnd_Irsg_Max_Lines() { return pnd_Gl_Const_Pnd_Irsg_Max_Lines; }

    public DbsField getPnd_Gl_Const_Pnd_Corr_Ind() { return pnd_Gl_Const_Pnd_Corr_Ind; }

    public DbsGroup getPnd_A_Rec() { return pnd_A_Rec; }

    public DbsField getPnd_A_Rec_Pnd_Generate() { return pnd_A_Rec_Pnd_Generate; }

    public DbsGroup getPnd_C_Rec() { return pnd_C_Rec; }

    public DbsField getPnd_C_Rec_Pnd_Generate() { return pnd_C_Rec_Pnd_Generate; }

    public DbsField getPnd_C_Rec_Pnd_Num_Payees() { return pnd_C_Rec_Pnd_Num_Payees; }

    public DbsField getPnd_C_Rec_Pnd_Amt_1() { return pnd_C_Rec_Pnd_Amt_1; }

    public DbsField getPnd_C_Rec_Pnd_Amt_2() { return pnd_C_Rec_Pnd_Amt_2; }

    public DbsField getPnd_C_Rec_Pnd_Amt_4() { return pnd_C_Rec_Pnd_Amt_4; }

    public DbsField getPnd_C_Rec_Pnd_Amt_5() { return pnd_C_Rec_Pnd_Amt_5; }

    public DbsField getPnd_C_Rec_Pnd_Amt_9() { return pnd_C_Rec_Pnd_Amt_9; }

    public DbsField getPnd_C_Rec_Pnd_Amt_A() { return pnd_C_Rec_Pnd_Amt_A; }

    public DbsGroup getPnd_K_Rec() { return pnd_K_Rec; }

    public DbsField getPnd_K_Rec_Pnd_Generate() { return pnd_K_Rec_Pnd_Generate; }

    public DbsGroup getPnd_K_Rec_Pnd_K_Rec_Array() { return pnd_K_Rec_Pnd_K_Rec_Array; }

    public DbsField getPnd_K_Rec_Pnd_Num_Payees() { return pnd_K_Rec_Pnd_Num_Payees; }

    public DbsField getPnd_K_Rec_Pnd_Amt_1() { return pnd_K_Rec_Pnd_Amt_1; }

    public DbsField getPnd_K_Rec_Pnd_Amt_2() { return pnd_K_Rec_Pnd_Amt_2; }

    public DbsField getPnd_K_Rec_Pnd_Amt_4() { return pnd_K_Rec_Pnd_Amt_4; }

    public DbsField getPnd_K_Rec_Pnd_Amt_5() { return pnd_K_Rec_Pnd_Amt_5; }

    public DbsField getPnd_K_Rec_Pnd_Amt_9() { return pnd_K_Rec_Pnd_Amt_9; }

    public DbsField getPnd_K_Rec_Pnd_Amt_A() { return pnd_K_Rec_Pnd_Amt_A; }

    public DbsField getPnd_K_Rec_Pnd_State_Tax() { return pnd_K_Rec_Pnd_State_Tax; }

    public DbsField getPnd_K_Rec_Pnd_Local_Tax() { return pnd_K_Rec_Pnd_Local_Tax; }

    public DbsGroup getPnd_State_Table() { return pnd_State_Table; }

    public DbsField getPnd_State_Table_Tircntl_Seq_Nbr() { return pnd_State_Table_Tircntl_Seq_Nbr; }

    public DbsField getPnd_State_Table_Tircntl_State_Alpha_Code() { return pnd_State_Table_Tircntl_State_Alpha_Code; }

    public DbsField getPnd_State_Table_Tircntl_Comb_Fed_Ind() { return pnd_State_Table_Tircntl_Comb_Fed_Ind; }

    public DbsGroup getPnd_Prov_Table() { return pnd_Prov_Table; }

    public DbsField getPnd_Prov_Table_Tircntl_State_Alpha_Code() { return pnd_Prov_Table_Tircntl_State_Alpha_Code; }

    public DbsGroup getPnd_Report_Indexes() { return pnd_Report_Indexes; }

    public DbsField getPnd_Report_Indexes_Pnd_Irsr1() { return pnd_Report_Indexes_Pnd_Irsr1; }

    public DbsField getPnd_Report_Indexes_Pnd_Irsw1() { return pnd_Report_Indexes_Pnd_Irsw1; }

    public DbsField getPnd_Report_Indexes_Pnd_Irsw2() { return pnd_Report_Indexes_Pnd_Irsw2; }

    public DbsField getPnd_Report_Indexes_Pnd_Irsg1() { return pnd_Report_Indexes_Pnd_Irsg1; }

    public DbsGroup getPnd_Irsr() { return pnd_Irsr; }

    public DbsField getPnd_Irsr_Pnd_Cmb_State_Tax() { return pnd_Irsr_Pnd_Cmb_State_Tax; }

    public DbsField getPnd_Irsr_Pnd_Cmb_Local_Tax() { return pnd_Irsr_Pnd_Cmb_Local_Tax; }

    public DbsGroup getPnd_Irsr_Pnd_Irsr_Lines() { return pnd_Irsr_Pnd_Irsr_Lines; }

    public DbsField getPnd_Irsr_Pnd_Header1() { return pnd_Irsr_Pnd_Header1; }

    public DbsField getPnd_Irsr_Pnd_Header2() { return pnd_Irsr_Pnd_Header2; }

    public DbsGroup getPnd_Irsr_Pnd_Totals() { return pnd_Irsr_Pnd_Totals; }

    public DbsField getPnd_Irsr_Pnd_Form_Cnt() { return pnd_Irsr_Pnd_Form_Cnt; }

    public DbsField getPnd_Irsr_Pnd_Gross_Amt() { return pnd_Irsr_Pnd_Gross_Amt; }

    public DbsField getPnd_Irsr_Pnd_Ivc_Amt() { return pnd_Irsr_Pnd_Ivc_Amt; }

    public DbsField getPnd_Irsr_Pnd_Taxable_Amt() { return pnd_Irsr_Pnd_Taxable_Amt; }

    public DbsField getPnd_Irsr_Pnd_Int_Amt() { return pnd_Irsr_Pnd_Int_Amt; }

    public DbsField getPnd_Irsr_Pnd_Fed_Tax() { return pnd_Irsr_Pnd_Fed_Tax; }

    public DbsField getPnd_Irsr_Pnd_State_Tax() { return pnd_Irsr_Pnd_State_Tax; }

    public DbsField getPnd_Irsr_Pnd_Local_Tax() { return pnd_Irsr_Pnd_Local_Tax; }

    public DbsGroup getPnd_Irsw() { return pnd_Irsw; }

    public DbsField getPnd_Irsw_Pnd_Header() { return pnd_Irsw_Pnd_Header; }

    public DbsGroup getPnd_Irsw_Pnd_Totals() { return pnd_Irsw_Pnd_Totals; }

    public DbsField getPnd_Irsw_Pnd_Form_Cnt() { return pnd_Irsw_Pnd_Form_Cnt; }

    public DbsField getPnd_Irsw_Pnd_Gross_Amt() { return pnd_Irsw_Pnd_Gross_Amt; }

    public DbsField getPnd_Irsw_Pnd_Ivc_Amt() { return pnd_Irsw_Pnd_Ivc_Amt; }

    public DbsField getPnd_Irsw_Pnd_Taxable_Amt() { return pnd_Irsw_Pnd_Taxable_Amt; }

    public DbsField getPnd_Irsw_Pnd_Int_Amt() { return pnd_Irsw_Pnd_Int_Amt; }

    public DbsField getPnd_Irsw_Pnd_Fed_Tax() { return pnd_Irsw_Pnd_Fed_Tax; }

    public DbsGroup getPnd_Irsg() { return pnd_Irsg; }

    public DbsField getPnd_Irsg_Pnd_Header() { return pnd_Irsg_Pnd_Header; }

    public DbsGroup getPnd_Irsg_Pnd_Totals() { return pnd_Irsg_Pnd_Totals; }

    public DbsField getPnd_Irsg_Pnd_Form_Cnt() { return pnd_Irsg_Pnd_Form_Cnt; }

    public DbsField getPnd_Irsg_Pnd_Gross_Amt() { return pnd_Irsg_Pnd_Gross_Amt; }

    public DbsField getPnd_Irsg_Pnd_Ivc_Amt() { return pnd_Irsg_Pnd_Ivc_Amt; }

    public DbsField getPnd_Irsg_Pnd_Taxable_Amt() { return pnd_Irsg_Pnd_Taxable_Amt; }

    public DbsField getPnd_Irsg_Pnd_Int_Amt() { return pnd_Irsg_Pnd_Int_Amt; }

    public DbsField getPnd_Irsg_Pnd_Fed_Tax() { return pnd_Irsg_Pnd_Fed_Tax; }

    public DbsGroup getPnd_Recon_Case() { return pnd_Recon_Case; }

    public DbsField getPnd_Recon_Case_Pnd_Header() { return pnd_Recon_Case_Pnd_Header; }

    public DbsField getPnd_Recon_Case_Pnd_Tin() { return pnd_Recon_Case_Pnd_Tin; }

    public DbsField getPnd_Recon_Case_Pnd_Cntrct_Py() { return pnd_Recon_Case_Pnd_Cntrct_Py; }

    public DbsField getPnd_Recon_Case_Pnd_Dist_Code() { return pnd_Recon_Case_Pnd_Dist_Code; }

    public DbsField getPnd_Recon_Case_Pnd_Form_Cnt() { return pnd_Recon_Case_Pnd_Form_Cnt; }

    public DbsField getPnd_Recon_Case_Pnd_Gross_Amt() { return pnd_Recon_Case_Pnd_Gross_Amt; }

    public DbsField getPnd_Recon_Case_Pnd_Ivc_Amt() { return pnd_Recon_Case_Pnd_Ivc_Amt; }

    public DbsField getPnd_Recon_Case_Pnd_Taxable_Amt() { return pnd_Recon_Case_Pnd_Taxable_Amt; }

    public DbsField getPnd_Recon_Case_Pnd_Int_Amt() { return pnd_Recon_Case_Pnd_Int_Amt; }

    public DbsField getPnd_Recon_Case_Pnd_Fed_Tax() { return pnd_Recon_Case_Pnd_Fed_Tax; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        int int_pnd_Gl_Const_Pnd_Prov_Min = 74;
        int int_pnd_Gl_Const_Pnd_Prov_Max = 88;
        int int_pnd_Gl_Const_Pnd_State_Max = 57;
        int int_pnd_Gl_Const_Pnd_Irsr_Max_Lines = 9;
        int int_pnd_Gl_Const_Pnd_Irsw_Max_Lines = 6;
        int int_pnd_Gl_Const_Pnd_Irsg_Max_Lines = 3;

        pnd_Gl_Const = newGroupInRecord("pnd_Gl_Const", "#GL-CONST");
        pnd_Gl_Const_Pnd_Prov_Min = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_Prov_Min", "#PROV-MIN", FieldType.PACKED_DECIMAL, 3);
        pnd_Gl_Const_Pnd_Prov_Max = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_Prov_Max", "#PROV-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Gl_Const_Pnd_State_Max = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_State_Max", "#STATE-MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Gl_Const_Pnd_State_Max_Alpha = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_State_Max_Alpha", "#STATE-MAX-ALPHA", FieldType.STRING, 2);
        pnd_Gl_Const_Pnd_Irsr_Max_Lines = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_Irsr_Max_Lines", "#IRSR-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Gl_Const_Pnd_Irsw_Max_Lines = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_Irsw_Max_Lines", "#IRSW-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Gl_Const_Pnd_Irsg_Max_Lines = pnd_Gl_Const.newFieldInGroup("pnd_Gl_Const_Pnd_Irsg_Max_Lines", "#IRSG-MAX-LINES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Gl_Const_Pnd_Corr_Ind = pnd_Gl_Const.newFieldArrayInGroup("pnd_Gl_Const_Pnd_Corr_Ind", "#CORR-IND", FieldType.STRING, 1, new DbsArrayController(1,
            3));

        pnd_A_Rec = newGroupArrayInRecord("pnd_A_Rec", "#A-REC", new DbsArrayController(1,3));
        pnd_A_Rec_Pnd_Generate = pnd_A_Rec.newFieldInGroup("pnd_A_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN);

        pnd_C_Rec = newGroupArrayInRecord("pnd_C_Rec", "#C-REC", new DbsArrayController(1,3));
        pnd_C_Rec_Pnd_Generate = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN);
        pnd_C_Rec_Pnd_Num_Payees = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Num_Payees", "#NUM-PAYEES", FieldType.PACKED_DECIMAL, 8);
        pnd_C_Rec_Pnd_Amt_1 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_1", "#AMT-1", FieldType.PACKED_DECIMAL, 18,2);
        pnd_C_Rec_Pnd_Amt_2 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_2", "#AMT-2", FieldType.PACKED_DECIMAL, 18,2);
        pnd_C_Rec_Pnd_Amt_4 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_4", "#AMT-4", FieldType.PACKED_DECIMAL, 18,2);
        pnd_C_Rec_Pnd_Amt_5 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_5", "#AMT-5", FieldType.PACKED_DECIMAL, 18,2);
        pnd_C_Rec_Pnd_Amt_9 = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_9", "#AMT-9", FieldType.PACKED_DECIMAL, 18,2);
        pnd_C_Rec_Pnd_Amt_A = pnd_C_Rec.newFieldInGroup("pnd_C_Rec_Pnd_Amt_A", "#AMT-A", FieldType.PACKED_DECIMAL, 18,2);

        pnd_K_Rec = newGroupArrayInRecord("pnd_K_Rec", "#K-REC", new DbsArrayController(1,3));
        pnd_K_Rec_Pnd_Generate = pnd_K_Rec.newFieldInGroup("pnd_K_Rec_Pnd_Generate", "#GENERATE", FieldType.BOOLEAN);
        pnd_K_Rec_Pnd_K_Rec_Array = pnd_K_Rec.newGroupArrayInGroup("pnd_K_Rec_Pnd_K_Rec_Array", "#K-REC-ARRAY", new DbsArrayController(1,int_pnd_Gl_Const_Pnd_State_Max));
        pnd_K_Rec_Pnd_Num_Payees = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Num_Payees", "#NUM-PAYEES", FieldType.PACKED_DECIMAL, 8);
        pnd_K_Rec_Pnd_Amt_1 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_1", "#AMT-1", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Amt_2 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_2", "#AMT-2", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Amt_4 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_4", "#AMT-4", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Amt_5 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_5", "#AMT-5", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Amt_9 = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_9", "#AMT-9", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Amt_A = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Amt_A", "#AMT-A", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_State_Tax = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 18,2);
        pnd_K_Rec_Pnd_Local_Tax = pnd_K_Rec_Pnd_K_Rec_Array.newFieldInGroup("pnd_K_Rec_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 18,2);

        pnd_State_Table = newGroupArrayInRecord("pnd_State_Table", "#STATE-TABLE", new DbsArrayController(1,int_pnd_Gl_Const_Pnd_State_Max + 2));
        pnd_State_Table_Tircntl_Seq_Nbr = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Seq_Nbr", "TIRCNTL-SEQ-NBR", FieldType.NUMERIC, 2);
        pnd_State_Table_Tircntl_State_Alpha_Code = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);
        pnd_State_Table_Tircntl_Comb_Fed_Ind = pnd_State_Table.newFieldInGroup("pnd_State_Table_Tircntl_Comb_Fed_Ind", "TIRCNTL-COMB-FED-IND", FieldType.STRING, 
            1);

        pnd_Prov_Table = newGroupArrayInRecord("pnd_Prov_Table", "#PROV-TABLE", new DbsArrayController(int_pnd_Gl_Const_Pnd_Prov_Min,int_pnd_Gl_Const_Pnd_Prov_Max));
        pnd_Prov_Table_Tircntl_State_Alpha_Code = pnd_Prov_Table.newFieldInGroup("pnd_Prov_Table_Tircntl_State_Alpha_Code", "TIRCNTL-STATE-ALPHA-CODE", 
            FieldType.STRING, 3);

        pnd_Report_Indexes = newGroupInRecord("pnd_Report_Indexes", "#REPORT-INDEXES");
        pnd_Report_Indexes_Pnd_Irsr1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsr1", "#IRSR1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw1", "#IRSW1", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsw2 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsw2", "#IRSW2", FieldType.PACKED_DECIMAL, 7);
        pnd_Report_Indexes_Pnd_Irsg1 = pnd_Report_Indexes.newFieldInGroup("pnd_Report_Indexes_Pnd_Irsg1", "#IRSG1", FieldType.PACKED_DECIMAL, 7);

        pnd_Irsr = newGroupInRecord("pnd_Irsr", "#IRSR");
        pnd_Irsr_Pnd_Cmb_State_Tax = pnd_Irsr.newFieldArrayInGroup("pnd_Irsr_Pnd_Cmb_State_Tax", "#CMB-STATE-TAX", FieldType.PACKED_DECIMAL, 11,2, new 
            DbsArrayController(1,2));
        pnd_Irsr_Pnd_Cmb_Local_Tax = pnd_Irsr.newFieldArrayInGroup("pnd_Irsr_Pnd_Cmb_Local_Tax", "#CMB-LOCAL-TAX", FieldType.PACKED_DECIMAL, 11,2, new 
            DbsArrayController(1,2));
        pnd_Irsr_Pnd_Irsr_Lines = pnd_Irsr.newGroupArrayInGroup("pnd_Irsr_Pnd_Irsr_Lines", "#IRSR-LINES", new DbsArrayController(1,int_pnd_Gl_Const_Pnd_Irsr_Max_Lines));
        pnd_Irsr_Pnd_Header1 = pnd_Irsr_Pnd_Irsr_Lines.newFieldInGroup("pnd_Irsr_Pnd_Header1", "#HEADER1", FieldType.STRING, 19);
        pnd_Irsr_Pnd_Header2 = pnd_Irsr_Pnd_Irsr_Lines.newFieldInGroup("pnd_Irsr_Pnd_Header2", "#HEADER2", FieldType.STRING, 19);
        pnd_Irsr_Pnd_Totals = pnd_Irsr_Pnd_Irsr_Lines.newGroupArrayInGroup("pnd_Irsr_Pnd_Totals", "#TOTALS", new DbsArrayController(1,2));
        pnd_Irsr_Pnd_Form_Cnt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsr_Pnd_Gross_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsr_Pnd_Ivc_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsr_Pnd_Taxable_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsr_Pnd_Int_Amt = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsr_Pnd_Fed_Tax = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsr_Pnd_State_Tax = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_State_Tax", "#STATE-TAX", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsr_Pnd_Local_Tax = pnd_Irsr_Pnd_Totals.newFieldInGroup("pnd_Irsr_Pnd_Local_Tax", "#LOCAL-TAX", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Irsw = newGroupArrayInRecord("pnd_Irsw", "#IRSW", new DbsArrayController(1,int_pnd_Gl_Const_Pnd_Irsw_Max_Lines));
        pnd_Irsw_Pnd_Header = pnd_Irsw.newFieldInGroup("pnd_Irsw_Pnd_Header", "#HEADER", FieldType.STRING, 14);
        pnd_Irsw_Pnd_Totals = pnd_Irsw.newGroupArrayInGroup("pnd_Irsw_Pnd_Totals", "#TOTALS", new DbsArrayController(1,2));
        pnd_Irsw_Pnd_Form_Cnt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsw_Pnd_Gross_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsw_Pnd_Ivc_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsw_Pnd_Taxable_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsw_Pnd_Int_Amt = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsw_Pnd_Fed_Tax = pnd_Irsw_Pnd_Totals.newFieldInGroup("pnd_Irsw_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Irsg = newGroupArrayInRecord("pnd_Irsg", "#IRSG", new DbsArrayController(1,int_pnd_Gl_Const_Pnd_Irsg_Max_Lines));
        pnd_Irsg_Pnd_Header = pnd_Irsg.newFieldInGroup("pnd_Irsg_Pnd_Header", "#HEADER", FieldType.STRING, 19);
        pnd_Irsg_Pnd_Totals = pnd_Irsg.newGroupArrayInGroup("pnd_Irsg_Pnd_Totals", "#TOTALS", new DbsArrayController(1,2));
        pnd_Irsg_Pnd_Form_Cnt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irsg_Pnd_Gross_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsg_Pnd_Ivc_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsg_Pnd_Taxable_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Irsg_Pnd_Int_Amt = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Irsg_Pnd_Fed_Tax = pnd_Irsg_Pnd_Totals.newFieldInGroup("pnd_Irsg_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Recon_Case = newGroupArrayInRecord("pnd_Recon_Case", "#RECON-CASE", new DbsArrayController(1,3));
        pnd_Recon_Case_Pnd_Header = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Header", "#HEADER", FieldType.STRING, 10);
        pnd_Recon_Case_Pnd_Tin = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Tin", "#TIN", FieldType.STRING, 11);
        pnd_Recon_Case_Pnd_Cntrct_Py = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Cntrct_Py", "#CNTRCT-PY", FieldType.STRING, 12);
        pnd_Recon_Case_Pnd_Dist_Code = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Dist_Code", "#DIST-CODE", FieldType.STRING, 2);
        pnd_Recon_Case_Pnd_Form_Cnt = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Form_Cnt", "#FORM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Recon_Case_Pnd_Gross_Amt = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Recon_Case_Pnd_Ivc_Amt = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Recon_Case_Pnd_Taxable_Amt = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Taxable_Amt", "#TAXABLE-AMT", FieldType.PACKED_DECIMAL, 14,
            2);
        pnd_Recon_Case_Pnd_Int_Amt = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Int_Amt", "#INT-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Recon_Case_Pnd_Fed_Tax = pnd_Recon_Case.newFieldInGroup("pnd_Recon_Case_Pnd_Fed_Tax", "#FED-TAX", FieldType.PACKED_DECIMAL, 11,2);

        this.setRecordName("GdaTwrg3503");
    }
    public void initializeValues() throws Exception
    {
        reset();

        pnd_Gl_Const_Pnd_Prov_Min.setInitialValue(74);
        pnd_Gl_Const_Pnd_Prov_Max.setInitialValue(88);
        pnd_Gl_Const_Pnd_State_Max.setInitialValue(57);
        pnd_Gl_Const_Pnd_State_Max_Alpha.setInitialValue("57");
        pnd_Gl_Const_Pnd_Irsr_Max_Lines.setInitialValue(9);
        pnd_Gl_Const_Pnd_Irsw_Max_Lines.setInitialValue(6);
        pnd_Gl_Const_Pnd_Irsg_Max_Lines.setInitialValue(3);
        pnd_Gl_Const_Pnd_Corr_Ind.getValue(1).setInitialValue(" ");
        pnd_Gl_Const_Pnd_Corr_Ind.getValue(2).setInitialValue("C");
        pnd_Gl_Const_Pnd_Corr_Ind.getValue(3).setInitialValue("G");
        pnd_State_Table_Tircntl_State_Alpha_Code.getValue(-1).setInitialValue("???");
        pnd_State_Table_Tircntl_Comb_Fed_Ind.getValue(-1).setInitialValue("?");
        pnd_Irsr_Pnd_Header1.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsr_Pnd_Header1.getValue(2).setInitialValue("Actve Records");
        pnd_Irsr_Pnd_Header1.getValue(3).setInitialValue("No Change");
        pnd_Irsr_Pnd_Header1.getValue(4).setInitialValue("Non Rept Zero Forms");
        pnd_Irsr_Pnd_Header1.getValue(5).setInitialValue("Link TIN Zero Forms");
        pnd_Irsr_Pnd_Header1.getValue(6).setInitialValue("Prev Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(7).setInitialValue("New  Rejected Forms");
        pnd_Irsr_Pnd_Header1.getValue(8).setInitialValue("Accepted Forms");
        pnd_Irsr_Pnd_Header1.getValue(9).setInitialValue("First part of two");
        pnd_Irsr_Pnd_Header2.getValue(1).setInitialValue("Accepted");
        pnd_Irsr_Pnd_Header2.getValue(2).setInitialValue("Prev. Reported");
        pnd_Irsr_Pnd_Header2.getValue(3).setInitialValue("Net Change");
        pnd_Irsr_Pnd_Header2.getValue(4).setInitialValue("Rejected");
        pnd_Irsr_Pnd_Header2.getValue(5).setInitialValue("Prev. Reported");
        pnd_Irsr_Pnd_Header2.getValue(6).setInitialValue("Net Change");
        pnd_Irsw_Pnd_Header.getValue(1).setInitialValue("Accepted");
        pnd_Irsw_Pnd_Header.getValue(2).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(3).setInitialValue("Net Change");
        pnd_Irsw_Pnd_Header.getValue(4).setInitialValue("Rejected");
        pnd_Irsw_Pnd_Header.getValue(5).setInitialValue("Prev. Reported");
        pnd_Irsw_Pnd_Header.getValue(6).setInitialValue("Net Change");
        pnd_Irsg_Pnd_Header.getValue(1).setInitialValue("Form Database Total");
        pnd_Irsg_Pnd_Header.getValue(2).setInitialValue("Prev. Reconciled");
        pnd_Irsg_Pnd_Header.getValue(3).setInitialValue("Net Change");
    }

    // Constructor
    private GdaTwrg3503() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaTwrg3503 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaTwrg3503>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaTwrg3503());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

