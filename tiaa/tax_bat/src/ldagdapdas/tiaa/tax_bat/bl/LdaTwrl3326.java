/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:30 PM
**        * FROM NATURAL LDA     : TWRL3326
************************************************************
**        * FILE NAME            : LdaTwrl3326.java
**        * CLASS NAME           : LdaTwrl3326
**        * INSTANCE NAME        : LdaTwrl3326
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3326 extends DbsRecord
{
    // Properties
    private DbsGroup irs_T_Out_Tape;
    private DbsField irs_T_Out_Tape_Irs_T_Record_Type;
    private DbsField irs_T_Out_Tape_Irs_T_Payment_Year;
    private DbsField irs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind;
    private DbsField irs_T_Out_Tape_Irs_T_Transmitters_Tin;
    private DbsField irs_T_Out_Tape_Irs_T_Transmitter_Control_Code;
    private DbsField irs_T_Out_Tape_Irs_T_Filler_1;
    private DbsField irs_T_Out_Tape_Irs_T_Test_File_Indicator;
    private DbsField irs_T_Out_Tape_Irs_T_Foreign_Entity_Ind;
    private DbsField irs_T_Out_Tape_Irs_T_Transmitter_Name;
    private DbsField irs_T_Out_Tape_Irs_T_Transmitter_Name_Continued;
    private DbsField irs_T_Out_Tape_Irs_T_Company_Name;
    private DbsField irs_T_Out_Tape_Irs_T_Company_Name_Continued;
    private DbsField irs_T_Out_Tape_Irs_T_Company_Mailing_Address;
    private DbsField irs_T_Out_Tape_Irs_T_Company_City;
    private DbsField irs_T_Out_Tape_Irs_T_Company_State;
    private DbsField irs_T_Out_Tape_Irs_T_Company_Zip_Code;
    private DbsField irs_T_Out_Tape_Irs_T_Filler_2;
    private DbsField irs_T_Out_Tape_Irs_T_Total_Number_Of_Payees;
    private DbsField irs_T_Out_Tape_Irs_T_Contact_Name;
    private DbsField irs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension;
    private DbsField irs_T_Out_Tape_Irs_T_Contact_Email_Address;
    private DbsField irs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind;
    private DbsField irs_T_Out_Tape_Irs_T_Media_Number;
    private DbsField irs_T_Out_Tape_Irs_T_Filler_2a;
    private DbsField irs_T_Out_Tape_Irs_T_Record_Sequence_No;
    private DbsField irs_T_Out_Tape_Irs_T_Filler_2b;
    private DbsField irs_T_Out_Tape_Irs_T_Vendor_Ind;
    private DbsField irs_T_Out_Tape_Irs_T_Vendor_Information;
    private DbsField irs_T_Out_Tape_Irs_T_Blank_Or_Cr_Lf;
    private DbsGroup irs_T_Out_TapeRedef1;
    private DbsField irs_T_Out_Tape_Irs_T_Move_1;
    private DbsField irs_T_Out_Tape_Irs_T_Move_2;
    private DbsField irs_T_Out_Tape_Irs_T_Move_3;

    public DbsGroup getIrs_T_Out_Tape() { return irs_T_Out_Tape; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Record_Type() { return irs_T_Out_Tape_Irs_T_Record_Type; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Payment_Year() { return irs_T_Out_Tape_Irs_T_Payment_Year; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind() { return irs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Transmitters_Tin() { return irs_T_Out_Tape_Irs_T_Transmitters_Tin; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Transmitter_Control_Code() { return irs_T_Out_Tape_Irs_T_Transmitter_Control_Code; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Filler_1() { return irs_T_Out_Tape_Irs_T_Filler_1; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Test_File_Indicator() { return irs_T_Out_Tape_Irs_T_Test_File_Indicator; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Foreign_Entity_Ind() { return irs_T_Out_Tape_Irs_T_Foreign_Entity_Ind; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Transmitter_Name() { return irs_T_Out_Tape_Irs_T_Transmitter_Name; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Transmitter_Name_Continued() { return irs_T_Out_Tape_Irs_T_Transmitter_Name_Continued; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_Name() { return irs_T_Out_Tape_Irs_T_Company_Name; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_Name_Continued() { return irs_T_Out_Tape_Irs_T_Company_Name_Continued; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_Mailing_Address() { return irs_T_Out_Tape_Irs_T_Company_Mailing_Address; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_City() { return irs_T_Out_Tape_Irs_T_Company_City; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_State() { return irs_T_Out_Tape_Irs_T_Company_State; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Company_Zip_Code() { return irs_T_Out_Tape_Irs_T_Company_Zip_Code; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Filler_2() { return irs_T_Out_Tape_Irs_T_Filler_2; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Total_Number_Of_Payees() { return irs_T_Out_Tape_Irs_T_Total_Number_Of_Payees; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Contact_Name() { return irs_T_Out_Tape_Irs_T_Contact_Name; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension() { return irs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Contact_Email_Address() { return irs_T_Out_Tape_Irs_T_Contact_Email_Address; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind() { return irs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Media_Number() { return irs_T_Out_Tape_Irs_T_Media_Number; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Filler_2a() { return irs_T_Out_Tape_Irs_T_Filler_2a; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Record_Sequence_No() { return irs_T_Out_Tape_Irs_T_Record_Sequence_No; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Filler_2b() { return irs_T_Out_Tape_Irs_T_Filler_2b; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Vendor_Ind() { return irs_T_Out_Tape_Irs_T_Vendor_Ind; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Vendor_Information() { return irs_T_Out_Tape_Irs_T_Vendor_Information; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Blank_Or_Cr_Lf() { return irs_T_Out_Tape_Irs_T_Blank_Or_Cr_Lf; }

    public DbsGroup getIrs_T_Out_TapeRedef1() { return irs_T_Out_TapeRedef1; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Move_1() { return irs_T_Out_Tape_Irs_T_Move_1; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Move_2() { return irs_T_Out_Tape_Irs_T_Move_2; }

    public DbsField getIrs_T_Out_Tape_Irs_T_Move_3() { return irs_T_Out_Tape_Irs_T_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_T_Out_Tape = newGroupInRecord("irs_T_Out_Tape", "IRS-T-OUT-TAPE");
        irs_T_Out_Tape_Irs_T_Record_Type = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Record_Type", "IRS-T-RECORD-TYPE", FieldType.STRING, 1);
        irs_T_Out_Tape_Irs_T_Payment_Year = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Payment_Year", "IRS-T-PAYMENT-YEAR", FieldType.STRING, 
            4);
        irs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Prior_Yr_Data_Ind", "IRS-T-PRIOR-YR-DATA-IND", FieldType.STRING, 
            1);
        irs_T_Out_Tape_Irs_T_Transmitters_Tin = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Transmitters_Tin", "IRS-T-TRANSMITTERS-TIN", FieldType.STRING, 
            9);
        irs_T_Out_Tape_Irs_T_Transmitter_Control_Code = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Transmitter_Control_Code", "IRS-T-TRANSMITTER-CONTROL-CODE", 
            FieldType.STRING, 5);
        irs_T_Out_Tape_Irs_T_Filler_1 = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Filler_1", "IRS-T-FILLER-1", FieldType.STRING, 7);
        irs_T_Out_Tape_Irs_T_Test_File_Indicator = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Test_File_Indicator", "IRS-T-TEST-FILE-INDICATOR", 
            FieldType.STRING, 1);
        irs_T_Out_Tape_Irs_T_Foreign_Entity_Ind = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Foreign_Entity_Ind", "IRS-T-FOREIGN-ENTITY-IND", 
            FieldType.STRING, 1);
        irs_T_Out_Tape_Irs_T_Transmitter_Name = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Transmitter_Name", "IRS-T-TRANSMITTER-NAME", FieldType.STRING, 
            40);
        irs_T_Out_Tape_Irs_T_Transmitter_Name_Continued = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Transmitter_Name_Continued", "IRS-T-TRANSMITTER-NAME-CONTINUED", 
            FieldType.STRING, 40);
        irs_T_Out_Tape_Irs_T_Company_Name = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_Name", "IRS-T-COMPANY-NAME", FieldType.STRING, 
            40);
        irs_T_Out_Tape_Irs_T_Company_Name_Continued = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_Name_Continued", "IRS-T-COMPANY-NAME-CONTINUED", 
            FieldType.STRING, 40);
        irs_T_Out_Tape_Irs_T_Company_Mailing_Address = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_Mailing_Address", "IRS-T-COMPANY-MAILING-ADDRESS", 
            FieldType.STRING, 40);
        irs_T_Out_Tape_Irs_T_Company_City = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_City", "IRS-T-COMPANY-CITY", FieldType.STRING, 
            40);
        irs_T_Out_Tape_Irs_T_Company_State = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_State", "IRS-T-COMPANY-STATE", FieldType.STRING, 
            2);
        irs_T_Out_Tape_Irs_T_Company_Zip_Code = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Company_Zip_Code", "IRS-T-COMPANY-ZIP-CODE", FieldType.STRING, 
            9);
        irs_T_Out_Tape_Irs_T_Filler_2 = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Filler_2", "IRS-T-FILLER-2", FieldType.STRING, 15);
        irs_T_Out_Tape_Irs_T_Total_Number_Of_Payees = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Total_Number_Of_Payees", "IRS-T-TOTAL-NUMBER-OF-PAYEES", 
            FieldType.NUMERIC, 8);
        irs_T_Out_Tape_Irs_T_Contact_Name = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Contact_Name", "IRS-T-CONTACT-NAME", FieldType.STRING, 
            40);
        irs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension", "IRS-T-CONTACT-PHONE-N-EXTENSION", 
            FieldType.STRING, 15);
        irs_T_Out_Tape_Irs_T_Contact_Email_Address = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Contact_Email_Address", "IRS-T-CONTACT-EMAIL-ADDRESS", 
            FieldType.STRING, 50);
        irs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Magnetic_Tape_File_Ind", "IRS-T-MAGNETIC-TAPE-FILE-IND", 
            FieldType.STRING, 2);
        irs_T_Out_Tape_Irs_T_Media_Number = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Media_Number", "IRS-T-MEDIA-NUMBER", FieldType.STRING, 
            6);
        irs_T_Out_Tape_Irs_T_Filler_2a = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Filler_2a", "IRS-T-FILLER-2A", FieldType.STRING, 83);
        irs_T_Out_Tape_Irs_T_Record_Sequence_No = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Record_Sequence_No", "IRS-T-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_T_Out_Tape_Irs_T_Filler_2b = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Filler_2b", "IRS-T-FILLER-2B", FieldType.STRING, 10);
        irs_T_Out_Tape_Irs_T_Vendor_Ind = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Vendor_Ind", "IRS-T-VENDOR-IND", FieldType.STRING, 1);
        irs_T_Out_Tape_Irs_T_Vendor_Information = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Vendor_Information", "IRS-T-VENDOR-INFORMATION", 
            FieldType.STRING, 230);
        irs_T_Out_Tape_Irs_T_Blank_Or_Cr_Lf = irs_T_Out_Tape.newFieldInGroup("irs_T_Out_Tape_Irs_T_Blank_Or_Cr_Lf", "IRS-T-BLANK-OR-CR-LF", FieldType.STRING, 
            2);
        irs_T_Out_TapeRedef1 = newGroupInRecord("irs_T_Out_TapeRedef1", "Redefines", irs_T_Out_Tape);
        irs_T_Out_Tape_Irs_T_Move_1 = irs_T_Out_TapeRedef1.newFieldInGroup("irs_T_Out_Tape_Irs_T_Move_1", "IRS-T-MOVE-1", FieldType.STRING, 250);
        irs_T_Out_Tape_Irs_T_Move_2 = irs_T_Out_TapeRedef1.newFieldInGroup("irs_T_Out_Tape_Irs_T_Move_2", "IRS-T-MOVE-2", FieldType.STRING, 250);
        irs_T_Out_Tape_Irs_T_Move_3 = irs_T_Out_TapeRedef1.newFieldInGroup("irs_T_Out_Tape_Irs_T_Move_3", "IRS-T-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl3326");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_T_Out_Tape_Irs_T_Record_Type.setInitialValue("T");
        irs_T_Out_Tape_Irs_T_Transmitters_Tin.setInitialValue("131624203");
        irs_T_Out_Tape_Irs_T_Transmitter_Control_Code.setInitialValue("19044");
        irs_T_Out_Tape_Irs_T_Transmitter_Name.setInitialValue("TEACHERS INSURANCE AND");
        irs_T_Out_Tape_Irs_T_Transmitter_Name_Continued.setInitialValue("ANNUITY ASSOCIATION");
        irs_T_Out_Tape_Irs_T_Company_Name.setInitialValue("TEACHERS INSURANCE AND");
        irs_T_Out_Tape_Irs_T_Company_Name_Continued.setInitialValue("ANNUITY ASSOCIATION");
        irs_T_Out_Tape_Irs_T_Company_Mailing_Address.setInitialValue("730 THIRD AVENUE");
        irs_T_Out_Tape_Irs_T_Company_City.setInitialValue("NEW YORK");
        irs_T_Out_Tape_Irs_T_Company_State.setInitialValue("NY");
        irs_T_Out_Tape_Irs_T_Company_Zip_Code.setInitialValue("100173206");
        irs_T_Out_Tape_Irs_T_Contact_Name.setInitialValue("LEROY FOSTER");
        irs_T_Out_Tape_Irs_T_Contact_Phone_N_Extension.setInitialValue("-2147483648");
    }

    // Constructor
    public LdaTwrl3326() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
