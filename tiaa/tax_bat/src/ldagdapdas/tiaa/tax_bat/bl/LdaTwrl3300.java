/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:29 PM
**        * FROM NATURAL LDA     : TWRL3300
************************************************************
**        * FILE NAME            : LdaTwrl3300.java
**        * CLASS NAME           : LdaTwrl3300
**        * INSTANCE NAME        : LdaTwrl3300
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3300 extends DbsRecord
{
    // Properties
    private DbsGroup extract_Npd_File_0;
    private DbsField extract_Npd_File_0_Extract_Id_Nmbr;
    private DbsField extract_Npd_File_0_Extract_Rcprcl_Tme;
    private DbsField extract_Npd_File_0_Extract_Add_Dte;
    private DbsField extract_Npd_File_0_Extract_Actvty_Cde;
    private DbsField extract_Npd_File_0_Extract_Cntrct_Py_Nmbr;
    private DbsGroup extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1;
    private DbsField extract_Npd_File_0_Extract_Contract_1st;
    private DbsField extract_Npd_File_0_Extract_Contract_Fill;
    private DbsField extract_Npd_File_0_Extract_Payee;
    private DbsField extract_Npd_File_0_Extract_Cntrl_Ctgry_Cde;
    private DbsField extract_Npd_File_0_Extract_Fed_St_Cde;
    private DbsField extract_Npd_File_0_Extract_Prdct_Cde;
    private DbsField extract_Npd_File_0_Extract_Type_Cde;
    private DbsField extract_Npd_File_0_Extract_Ivc_Ind;
    private DbsField extract_Npd_File_0_Extract_Stat_Cde;
    private DbsField extract_Npd_File_0_Extract_Irs_Rpt_Stat_Cde;
    private DbsField extract_Npd_File_0_Extract_Prtcpnt_Chnge_Cde;
    private DbsField extract_Npd_File_0_Extract_State_Cde;
    private DbsField extract_Npd_File_0_Extract_F_Srce_Cde;
    private DbsField extract_Npd_File_0_Extract_Pymnt_Type_Ind;
    private DbsField extract_Npd_File_0_Extract_Sttlmnt_Type_Ind;
    private DbsField extract_Npd_File_0_Extract_Fed_Gross_Amt;
    private DbsField extract_Npd_File_0_Extract_Fed_Ivc_Amt;
    private DbsField extract_Npd_File_0_Extract_Fed_Wthhld_Amt;
    private DbsField extract_Npd_File_0_Extract_St_Gross_Amt;
    private DbsField extract_Npd_File_0_Extract_St_Ivc_Amt;
    private DbsField extract_Npd_File_0_Extract_St_Wthhld_Amt;
    private DbsField extract_Npd_File_0_Extract_Frms_User_Cde;
    private DbsField extract_Npd_File_0_Extract_Frms_Cmmnt_Text;
    private DbsGroup extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2;
    private DbsField extract_Npd_File_0_Extract_Frms_Cmmnt_Txt;
    private DbsField extract_Npd_File_0_Extract_Cntr_Sys_Err;
    private DbsField extract_Npd_File_0_Extract_Sys_Err;
    private DbsField extract_Npd_File_0_Extract_Prod_Ind;
    private DbsField extract_Npd_File_0_Extract_Corr_Ind;
    private DbsField extract_Npd_File_0_Extract_Irs_Reject_Ind;
    private DbsField extract_Npd_File_0_Extract_Taxable_Amt;
    private DbsField extract_Npd_File_0_Extract_State_Hardcopy_Ind;
    private DbsField extract_Npd_File_0_Extract_Other_Id;
    private DbsField extract_Npd_File_0_Extract_Id_Ind;
    private DbsField extract_Npd_File_0_Extract_Hold_Cde;
    private DbsField extract_Npd_File_0_Extract_P_Srce_Cde;
    private DbsField extract_Npd_File_0_Extract_Name;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_1;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_2;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_3;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_4;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_5;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_6;
    private DbsGroup extract_Npd_File_0_Extract_Addrss_Txt_6Redef3;
    private DbsField extract_Npd_File_0_Extract_Geo_Cde;
    private DbsField extract_Npd_File_0_Extract_Apo_Geo_Cde;
    private DbsField extract_Npd_File_0_Extract_Country_Name;
    private DbsField extract_Npd_File_0_Extract_Province_Code;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_6_Filler;
    private DbsField extract_Npd_File_0_Extract_Addrss_Txt_7;
    private DbsGroup extract_Npd_File_0_Extract_Addrss_Txt_7Redef4;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Zip;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Plus_4;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Walk;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Geo_Cd;
    private DbsGroup extract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Geo_Nn;
    private DbsField extract_Npd_File_0_Extract_Addr_7_Filler;
    private DbsField extract_Npd_File_0_Extract_Foreign_Addr;
    private DbsField extract_Npd_File_0_Extract_Combine_Ind;
    private DbsField extract_Npd_File_0_Extract_Cmmnt_Txt;
    private DbsField extract_Npd_File_0_Extract_State_Taxid;
    private DbsField extract_Npd_File_0_Extract_Ira_Sep_Ind;
    private DbsField extract_Npd_File_0_Extract_Percent_Distrib;
    private DbsField extract_Npd_File_0_Extract_Roth_Year;
    private DbsField extract_Npd_File_0_Extract_Total_Distrib_Ind;
    private DbsField extract_Npd_File_0_Extract_Moore_Test_Ind;
    private DbsField extract_Npd_File_0_Extract_User_Cde;
    private DbsField extract_Npd_File_0_Extract_Lcl_Gross_Amt;
    private DbsField extract_Npd_File_0_Extract_Lcl_Ivc_Amt;
    private DbsField extract_Npd_File_0_Extract_Lcl_Wthhld_Amt;
    private DbsField extract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt;
    private DbsField extract_Npd_File_0_Extract_Irs_B_Contract_Payee;
    private DbsField extract_Npd_File_0_Extract_Res_Irr_Amt;
    private DbsField extract_Npd_File_0_Filler_1;

    public DbsGroup getExtract_Npd_File_0() { return extract_Npd_File_0; }

    public DbsField getExtract_Npd_File_0_Extract_Id_Nmbr() { return extract_Npd_File_0_Extract_Id_Nmbr; }

    public DbsField getExtract_Npd_File_0_Extract_Rcprcl_Tme() { return extract_Npd_File_0_Extract_Rcprcl_Tme; }

    public DbsField getExtract_Npd_File_0_Extract_Add_Dte() { return extract_Npd_File_0_Extract_Add_Dte; }

    public DbsField getExtract_Npd_File_0_Extract_Actvty_Cde() { return extract_Npd_File_0_Extract_Actvty_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Cntrct_Py_Nmbr() { return extract_Npd_File_0_Extract_Cntrct_Py_Nmbr; }

    public DbsGroup getExtract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1() { return extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1; }

    public DbsField getExtract_Npd_File_0_Extract_Contract_1st() { return extract_Npd_File_0_Extract_Contract_1st; }

    public DbsField getExtract_Npd_File_0_Extract_Contract_Fill() { return extract_Npd_File_0_Extract_Contract_Fill; }

    public DbsField getExtract_Npd_File_0_Extract_Payee() { return extract_Npd_File_0_Extract_Payee; }

    public DbsField getExtract_Npd_File_0_Extract_Cntrl_Ctgry_Cde() { return extract_Npd_File_0_Extract_Cntrl_Ctgry_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Fed_St_Cde() { return extract_Npd_File_0_Extract_Fed_St_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Prdct_Cde() { return extract_Npd_File_0_Extract_Prdct_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Type_Cde() { return extract_Npd_File_0_Extract_Type_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Ivc_Ind() { return extract_Npd_File_0_Extract_Ivc_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Stat_Cde() { return extract_Npd_File_0_Extract_Stat_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Irs_Rpt_Stat_Cde() { return extract_Npd_File_0_Extract_Irs_Rpt_Stat_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Prtcpnt_Chnge_Cde() { return extract_Npd_File_0_Extract_Prtcpnt_Chnge_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_State_Cde() { return extract_Npd_File_0_Extract_State_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_F_Srce_Cde() { return extract_Npd_File_0_Extract_F_Srce_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Pymnt_Type_Ind() { return extract_Npd_File_0_Extract_Pymnt_Type_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Sttlmnt_Type_Ind() { return extract_Npd_File_0_Extract_Sttlmnt_Type_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Fed_Gross_Amt() { return extract_Npd_File_0_Extract_Fed_Gross_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Fed_Ivc_Amt() { return extract_Npd_File_0_Extract_Fed_Ivc_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Fed_Wthhld_Amt() { return extract_Npd_File_0_Extract_Fed_Wthhld_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_St_Gross_Amt() { return extract_Npd_File_0_Extract_St_Gross_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_St_Ivc_Amt() { return extract_Npd_File_0_Extract_St_Ivc_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_St_Wthhld_Amt() { return extract_Npd_File_0_Extract_St_Wthhld_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Frms_User_Cde() { return extract_Npd_File_0_Extract_Frms_User_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Frms_Cmmnt_Text() { return extract_Npd_File_0_Extract_Frms_Cmmnt_Text; }

    public DbsGroup getExtract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2() { return extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2; }

    public DbsField getExtract_Npd_File_0_Extract_Frms_Cmmnt_Txt() { return extract_Npd_File_0_Extract_Frms_Cmmnt_Txt; }

    public DbsField getExtract_Npd_File_0_Extract_Cntr_Sys_Err() { return extract_Npd_File_0_Extract_Cntr_Sys_Err; }

    public DbsField getExtract_Npd_File_0_Extract_Sys_Err() { return extract_Npd_File_0_Extract_Sys_Err; }

    public DbsField getExtract_Npd_File_0_Extract_Prod_Ind() { return extract_Npd_File_0_Extract_Prod_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Corr_Ind() { return extract_Npd_File_0_Extract_Corr_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Irs_Reject_Ind() { return extract_Npd_File_0_Extract_Irs_Reject_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Taxable_Amt() { return extract_Npd_File_0_Extract_Taxable_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_State_Hardcopy_Ind() { return extract_Npd_File_0_Extract_State_Hardcopy_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Other_Id() { return extract_Npd_File_0_Extract_Other_Id; }

    public DbsField getExtract_Npd_File_0_Extract_Id_Ind() { return extract_Npd_File_0_Extract_Id_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Hold_Cde() { return extract_Npd_File_0_Extract_Hold_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_P_Srce_Cde() { return extract_Npd_File_0_Extract_P_Srce_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Name() { return extract_Npd_File_0_Extract_Name; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_1() { return extract_Npd_File_0_Extract_Addrss_Txt_1; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_2() { return extract_Npd_File_0_Extract_Addrss_Txt_2; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_3() { return extract_Npd_File_0_Extract_Addrss_Txt_3; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_4() { return extract_Npd_File_0_Extract_Addrss_Txt_4; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_5() { return extract_Npd_File_0_Extract_Addrss_Txt_5; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_6() { return extract_Npd_File_0_Extract_Addrss_Txt_6; }

    public DbsGroup getExtract_Npd_File_0_Extract_Addrss_Txt_6Redef3() { return extract_Npd_File_0_Extract_Addrss_Txt_6Redef3; }

    public DbsField getExtract_Npd_File_0_Extract_Geo_Cde() { return extract_Npd_File_0_Extract_Geo_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Apo_Geo_Cde() { return extract_Npd_File_0_Extract_Apo_Geo_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Country_Name() { return extract_Npd_File_0_Extract_Country_Name; }

    public DbsField getExtract_Npd_File_0_Extract_Province_Code() { return extract_Npd_File_0_Extract_Province_Code; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_6_Filler() { return extract_Npd_File_0_Extract_Addrss_Txt_6_Filler; }

    public DbsField getExtract_Npd_File_0_Extract_Addrss_Txt_7() { return extract_Npd_File_0_Extract_Addrss_Txt_7; }

    public DbsGroup getExtract_Npd_File_0_Extract_Addrss_Txt_7Redef4() { return extract_Npd_File_0_Extract_Addrss_Txt_7Redef4; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Zip() { return extract_Npd_File_0_Extract_Addr_7_Zip; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Plus_4() { return extract_Npd_File_0_Extract_Addr_7_Plus_4; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Walk() { return extract_Npd_File_0_Extract_Addr_7_Walk; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Geo_Cd() { return extract_Npd_File_0_Extract_Addr_7_Geo_Cd; }

    public DbsGroup getExtract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5() { return extract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Geo_Nn() { return extract_Npd_File_0_Extract_Addr_7_Geo_Nn; }

    public DbsField getExtract_Npd_File_0_Extract_Addr_7_Filler() { return extract_Npd_File_0_Extract_Addr_7_Filler; }

    public DbsField getExtract_Npd_File_0_Extract_Foreign_Addr() { return extract_Npd_File_0_Extract_Foreign_Addr; }

    public DbsField getExtract_Npd_File_0_Extract_Combine_Ind() { return extract_Npd_File_0_Extract_Combine_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Cmmnt_Txt() { return extract_Npd_File_0_Extract_Cmmnt_Txt; }

    public DbsField getExtract_Npd_File_0_Extract_State_Taxid() { return extract_Npd_File_0_Extract_State_Taxid; }

    public DbsField getExtract_Npd_File_0_Extract_Ira_Sep_Ind() { return extract_Npd_File_0_Extract_Ira_Sep_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Percent_Distrib() { return extract_Npd_File_0_Extract_Percent_Distrib; }

    public DbsField getExtract_Npd_File_0_Extract_Roth_Year() { return extract_Npd_File_0_Extract_Roth_Year; }

    public DbsField getExtract_Npd_File_0_Extract_Total_Distrib_Ind() { return extract_Npd_File_0_Extract_Total_Distrib_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_Moore_Test_Ind() { return extract_Npd_File_0_Extract_Moore_Test_Ind; }

    public DbsField getExtract_Npd_File_0_Extract_User_Cde() { return extract_Npd_File_0_Extract_User_Cde; }

    public DbsField getExtract_Npd_File_0_Extract_Lcl_Gross_Amt() { return extract_Npd_File_0_Extract_Lcl_Gross_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Lcl_Ivc_Amt() { return extract_Npd_File_0_Extract_Lcl_Ivc_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Lcl_Wthhld_Amt() { return extract_Npd_File_0_Extract_Lcl_Wthhld_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt() { return extract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt; }

    public DbsField getExtract_Npd_File_0_Extract_Irs_B_Contract_Payee() { return extract_Npd_File_0_Extract_Irs_B_Contract_Payee; }

    public DbsField getExtract_Npd_File_0_Extract_Res_Irr_Amt() { return extract_Npd_File_0_Extract_Res_Irr_Amt; }

    public DbsField getExtract_Npd_File_0_Filler_1() { return extract_Npd_File_0_Filler_1; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        extract_Npd_File_0 = newGroupInRecord("extract_Npd_File_0", "EXTRACT-NPD-FILE-0");
        extract_Npd_File_0_Extract_Id_Nmbr = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Id_Nmbr", "EXTRACT-ID-NMBR", FieldType.STRING, 
            10);
        extract_Npd_File_0_Extract_Rcprcl_Tme = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Rcprcl_Tme", "EXTRACT-RCPRCL-TME", FieldType.NUMERIC, 
            14);
        extract_Npd_File_0_Extract_Add_Dte = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Add_Dte", "EXTRACT-ADD-DTE", FieldType.NUMERIC, 
            8);
        extract_Npd_File_0_Extract_Actvty_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Actvty_Cde", "EXTRACT-ACTVTY-CDE", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Cntrct_Py_Nmbr = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Cntrct_Py_Nmbr", "EXTRACT-CNTRCT-PY-NMBR", 
            FieldType.STRING, 10);
        extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1 = extract_Npd_File_0.newGroupInGroup("extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1", "Redefines", 
            extract_Npd_File_0_Extract_Cntrct_Py_Nmbr);
        extract_Npd_File_0_Extract_Contract_1st = extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1.newFieldInGroup("extract_Npd_File_0_Extract_Contract_1st", 
            "EXTRACT-CONTRACT-1ST", FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Contract_Fill = extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1.newFieldInGroup("extract_Npd_File_0_Extract_Contract_Fill", 
            "EXTRACT-CONTRACT-FILL", FieldType.STRING, 6);
        extract_Npd_File_0_Extract_Payee = extract_Npd_File_0_Extract_Cntrct_Py_NmbrRedef1.newFieldInGroup("extract_Npd_File_0_Extract_Payee", "EXTRACT-PAYEE", 
            FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Cntrl_Ctgry_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Cntrl_Ctgry_Cde", "EXTRACT-CNTRL-CTGRY-CDE", 
            FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Fed_St_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Fed_St_Cde", "EXTRACT-FED-ST-CDE", FieldType.STRING, 
            2);
        extract_Npd_File_0_Extract_Prdct_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Prdct_Cde", "EXTRACT-PRDCT-CDE", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Type_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Type_Cde", "EXTRACT-TYPE-CDE", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Ivc_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Ivc_Ind", "EXTRACT-IVC-IND", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Stat_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Stat_Cde", "EXTRACT-STAT-CDE", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Irs_Rpt_Stat_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Irs_Rpt_Stat_Cde", "EXTRACT-IRS-RPT-STAT-CDE", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Prtcpnt_Chnge_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Prtcpnt_Chnge_Cde", "EXTRACT-PRTCPNT-CHNGE-CDE", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_State_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_State_Cde", "EXTRACT-STATE-CDE", FieldType.STRING, 
            2);
        extract_Npd_File_0_Extract_F_Srce_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_F_Srce_Cde", "EXTRACT-F-SRCE-CDE", FieldType.STRING, 
            2);
        extract_Npd_File_0_Extract_Pymnt_Type_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Pymnt_Type_Ind", "EXTRACT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Sttlmnt_Type_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Sttlmnt_Type_Ind", "EXTRACT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Fed_Gross_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Fed_Gross_Amt", "EXTRACT-FED-GROSS-AMT", 
            FieldType.DECIMAL, 10,2);
        extract_Npd_File_0_Extract_Fed_Ivc_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Fed_Ivc_Amt", "EXTRACT-FED-IVC-AMT", FieldType.DECIMAL, 
            10,2);
        extract_Npd_File_0_Extract_Fed_Wthhld_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Fed_Wthhld_Amt", "EXTRACT-FED-WTHHLD-AMT", 
            FieldType.DECIMAL, 10,2);
        extract_Npd_File_0_Extract_St_Gross_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_St_Gross_Amt", "EXTRACT-ST-GROSS-AMT", 
            FieldType.DECIMAL, 10,2);
        extract_Npd_File_0_Extract_St_Ivc_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_St_Ivc_Amt", "EXTRACT-ST-IVC-AMT", FieldType.DECIMAL, 
            10,2);
        extract_Npd_File_0_Extract_St_Wthhld_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_St_Wthhld_Amt", "EXTRACT-ST-WTHHLD-AMT", 
            FieldType.DECIMAL, 10,2);
        extract_Npd_File_0_Extract_Frms_User_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Frms_User_Cde", "EXTRACT-FRMS-USER-CDE", 
            FieldType.STRING, 3);
        extract_Npd_File_0_Extract_Frms_Cmmnt_Text = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Frms_Cmmnt_Text", "EXTRACT-FRMS-CMMNT-TEXT", 
            FieldType.STRING, 50);
        extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2 = extract_Npd_File_0.newGroupInGroup("extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2", "Redefines", 
            extract_Npd_File_0_Extract_Frms_Cmmnt_Text);
        extract_Npd_File_0_Extract_Frms_Cmmnt_Txt = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldInGroup("extract_Npd_File_0_Extract_Frms_Cmmnt_Txt", 
            "EXTRACT-FRMS-CMMNT-TXT", FieldType.STRING, 17);
        extract_Npd_File_0_Extract_Cntr_Sys_Err = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldInGroup("extract_Npd_File_0_Extract_Cntr_Sys_Err", 
            "EXTRACT-CNTR-SYS-ERR", FieldType.PACKED_DECIMAL, 3);
        extract_Npd_File_0_Extract_Sys_Err = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldArrayInGroup("extract_Npd_File_0_Extract_Sys_Err", 
            "EXTRACT-SYS-ERR", FieldType.NUMERIC, 2, new DbsArrayController(1,14));
        extract_Npd_File_0_Extract_Prod_Ind = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldInGroup("extract_Npd_File_0_Extract_Prod_Ind", 
            "EXTRACT-PROD-IND", FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Corr_Ind = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldInGroup("extract_Npd_File_0_Extract_Corr_Ind", 
            "EXTRACT-CORR-IND", FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Irs_Reject_Ind = extract_Npd_File_0_Extract_Frms_Cmmnt_TextRedef2.newFieldInGroup("extract_Npd_File_0_Extract_Irs_Reject_Ind", 
            "EXTRACT-IRS-REJECT-IND", FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Taxable_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Taxable_Amt", "EXTRACT-TAXABLE-AMT", FieldType.DECIMAL, 
            11,2);
        extract_Npd_File_0_Extract_State_Hardcopy_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_State_Hardcopy_Ind", "EXTRACT-STATE-HARDCOPY-IND", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Other_Id = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Other_Id", "EXTRACT-OTHER-ID", FieldType.STRING, 
            10);
        extract_Npd_File_0_Extract_Id_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Id_Ind", "EXTRACT-ID-IND", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Hold_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Hold_Cde", "EXTRACT-HOLD-CDE", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_P_Srce_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_P_Srce_Cde", "EXTRACT-P-SRCE-CDE", FieldType.STRING, 
            2);
        extract_Npd_File_0_Extract_Name = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Name", "EXTRACT-NAME", FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_1 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_1", "EXTRACT-ADDRSS-TXT-1", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_2 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_2", "EXTRACT-ADDRSS-TXT-2", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_3 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_3", "EXTRACT-ADDRSS-TXT-3", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_4 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_4", "EXTRACT-ADDRSS-TXT-4", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_5 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_5", "EXTRACT-ADDRSS-TXT-5", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_6 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_6", "EXTRACT-ADDRSS-TXT-6", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_6Redef3 = extract_Npd_File_0.newGroupInGroup("extract_Npd_File_0_Extract_Addrss_Txt_6Redef3", "Redefines", 
            extract_Npd_File_0_Extract_Addrss_Txt_6);
        extract_Npd_File_0_Extract_Geo_Cde = extract_Npd_File_0_Extract_Addrss_Txt_6Redef3.newFieldInGroup("extract_Npd_File_0_Extract_Geo_Cde", "EXTRACT-GEO-CDE", 
            FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Apo_Geo_Cde = extract_Npd_File_0_Extract_Addrss_Txt_6Redef3.newFieldInGroup("extract_Npd_File_0_Extract_Apo_Geo_Cde", 
            "EXTRACT-APO-GEO-CDE", FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Country_Name = extract_Npd_File_0_Extract_Addrss_Txt_6Redef3.newFieldInGroup("extract_Npd_File_0_Extract_Country_Name", 
            "EXTRACT-COUNTRY-NAME", FieldType.STRING, 20);
        extract_Npd_File_0_Extract_Province_Code = extract_Npd_File_0_Extract_Addrss_Txt_6Redef3.newFieldInGroup("extract_Npd_File_0_Extract_Province_Code", 
            "EXTRACT-PROVINCE-CODE", FieldType.STRING, 3);
        extract_Npd_File_0_Extract_Addrss_Txt_6_Filler = extract_Npd_File_0_Extract_Addrss_Txt_6Redef3.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_6_Filler", 
            "EXTRACT-ADDRSS-TXT-6-FILLER", FieldType.STRING, 8);
        extract_Npd_File_0_Extract_Addrss_Txt_7 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Addrss_Txt_7", "EXTRACT-ADDRSS-TXT-7", 
            FieldType.STRING, 35);
        extract_Npd_File_0_Extract_Addrss_Txt_7Redef4 = extract_Npd_File_0.newGroupInGroup("extract_Npd_File_0_Extract_Addrss_Txt_7Redef4", "Redefines", 
            extract_Npd_File_0_Extract_Addrss_Txt_7);
        extract_Npd_File_0_Extract_Addr_7_Zip = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Zip", 
            "EXTRACT-ADDR-7-ZIP", FieldType.STRING, 5);
        extract_Npd_File_0_Extract_Addr_7_Plus_4 = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Plus_4", 
            "EXTRACT-ADDR-7-PLUS-4", FieldType.STRING, 4);
        extract_Npd_File_0_Extract_Addr_7_Walk = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Walk", 
            "EXTRACT-ADDR-7-WALK", FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Addr_7_Geo_Cd = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Geo_Cd", 
            "EXTRACT-ADDR-7-GEO-CD", FieldType.STRING, 2);
        extract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5 = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newGroupInGroup("extract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5", 
            "Redefines", extract_Npd_File_0_Extract_Addr_7_Geo_Cd);
        extract_Npd_File_0_Extract_Addr_7_Geo_Nn = extract_Npd_File_0_Extract_Addr_7_Geo_CdRedef5.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Geo_Nn", 
            "EXTRACT-ADDR-7-GEO-NN", FieldType.NUMERIC, 2);
        extract_Npd_File_0_Extract_Addr_7_Filler = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Addr_7_Filler", 
            "EXTRACT-ADDR-7-FILLER", FieldType.STRING, 21);
        extract_Npd_File_0_Extract_Foreign_Addr = extract_Npd_File_0_Extract_Addrss_Txt_7Redef4.newFieldInGroup("extract_Npd_File_0_Extract_Foreign_Addr", 
            "EXTRACT-FOREIGN-ADDR", FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Combine_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Combine_Ind", "EXTRACT-COMBINE-IND", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Cmmnt_Txt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Cmmnt_Txt", "EXTRACT-CMMNT-TXT", FieldType.STRING, 
            40);
        extract_Npd_File_0_Extract_State_Taxid = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_State_Taxid", "EXTRACT-STATE-TAXID", FieldType.STRING, 
            14);
        extract_Npd_File_0_Extract_Ira_Sep_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Ira_Sep_Ind", "EXTRACT-IRA-SEP-IND", FieldType.STRING, 
            1);
        extract_Npd_File_0_Extract_Percent_Distrib = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Percent_Distrib", "EXTRACT-PERCENT-DISTRIB", 
            FieldType.PACKED_DECIMAL, 3);
        extract_Npd_File_0_Extract_Roth_Year = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Roth_Year", "EXTRACT-ROTH-YEAR", FieldType.NUMERIC, 
            8);
        extract_Npd_File_0_Extract_Total_Distrib_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Total_Distrib_Ind", "EXTRACT-TOTAL-DISTRIB-IND", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_Moore_Test_Ind = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Moore_Test_Ind", "EXTRACT-MOORE-TEST-IND", 
            FieldType.STRING, 1);
        extract_Npd_File_0_Extract_User_Cde = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_User_Cde", "EXTRACT-USER-CDE", FieldType.STRING, 
            3);
        extract_Npd_File_0_Extract_Lcl_Gross_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Lcl_Gross_Amt", "EXTRACT-LCL-GROSS-AMT", 
            FieldType.DECIMAL, 9,2);
        extract_Npd_File_0_Extract_Lcl_Ivc_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Lcl_Ivc_Amt", "EXTRACT-LCL-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        extract_Npd_File_0_Extract_Lcl_Wthhld_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Lcl_Wthhld_Amt", "EXTRACT-LCL-WTHHLD-AMT", 
            FieldType.DECIMAL, 9,2);
        extract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Tot_Ivc_Cntrt_Amt", "EXTRACT-TOT-IVC-CNTRT-AMT", 
            FieldType.DECIMAL, 9,2);
        extract_Npd_File_0_Extract_Irs_B_Contract_Payee = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Irs_B_Contract_Payee", "EXTRACT-IRS-B-CONTRACT-PAYEE", 
            FieldType.STRING, 20);
        extract_Npd_File_0_Extract_Res_Irr_Amt = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Extract_Res_Irr_Amt", "EXTRACT-RES-IRR-AMT", FieldType.DECIMAL, 
            10,2);
        extract_Npd_File_0_Filler_1 = extract_Npd_File_0.newFieldInGroup("extract_Npd_File_0_Filler_1", "FILLER-1", FieldType.STRING, 2);

        this.setRecordName("LdaTwrl3300");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3300() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
