/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:13 PM
**        * FROM NATURAL LDA     : TWRL6395
************************************************************
**        * FILE NAME            : LdaTwrl6395.java
**        * CLASS NAME           : LdaTwrl6395
**        * INSTANCE NAME        : LdaTwrl6395
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl6395 extends DbsRecord
{
    // Properties
    private DbsGroup sc_Cover_Letter_Data;
    private DbsField sc_Cover_Letter_Data_Sc_Cover_Data_Array;
    private DbsGroup sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1;
    private DbsGroup sc_Cover_Letter_Data_Letter_Data;
    private DbsField sc_Cover_Letter_Data_Cover_Rec_Id_Aa;
    private DbsField sc_Cover_Letter_Data_Year;
    private DbsField sc_Cover_Letter_Data_Letter_Id;
    private DbsField sc_Cover_Letter_Data_Institution_Ind;
    private DbsField sc_Cover_Letter_Data_Multi_Ia_Income_Ind;
    private DbsField sc_Cover_Letter_Data_Ira_Rollover_Ind;
    private DbsField sc_Cover_Letter_Data_Name_Addr_Lines;
    private DbsGroup sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2;
    private DbsGroup sc_Cover_Letter_Data_Suny_Cuny_Contract_Header;
    private DbsField sc_Cover_Letter_Data_Cover_Rec_Id_Bb;
    private DbsField sc_Cover_Letter_Data_Sc_Cont_Type;
    private DbsField sc_Cover_Letter_Data_Sc_Cont_Nbr;
    private DbsField sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt;
    private DbsField sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct;
    private DbsGroup sc_Cover_Letter_Data_Sc_Cont_Detail_Data;
    private DbsField sc_Cover_Letter_Data_Sc_Inst_Type;
    private DbsField sc_Cover_Letter_Data_Sc_Orig_Cont_Type;
    private DbsField sc_Cover_Letter_Data_Sc_Orig_Cont_Nbr;
    private DbsField sc_Cover_Letter_Data_Sc_Plan_Nbr;
    private DbsField sc_Cover_Letter_Data_Sc_Plan_Name;
    private DbsField sc_Cover_Letter_Data_Sc_Plan_Income;
    private DbsField sc_Cover_Letter_Data_Sc_Plan_Income_Pct;
    private DbsGroup sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3;
    private DbsField sc_Cover_Letter_Data_Write_Seq_Number;

    public DbsGroup getSc_Cover_Letter_Data() { return sc_Cover_Letter_Data; }

    public DbsField getSc_Cover_Letter_Data_Sc_Cover_Data_Array() { return sc_Cover_Letter_Data_Sc_Cover_Data_Array; }

    public DbsGroup getSc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1() { return sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1; }

    public DbsGroup getSc_Cover_Letter_Data_Letter_Data() { return sc_Cover_Letter_Data_Letter_Data; }

    public DbsField getSc_Cover_Letter_Data_Cover_Rec_Id_Aa() { return sc_Cover_Letter_Data_Cover_Rec_Id_Aa; }

    public DbsField getSc_Cover_Letter_Data_Year() { return sc_Cover_Letter_Data_Year; }

    public DbsField getSc_Cover_Letter_Data_Letter_Id() { return sc_Cover_Letter_Data_Letter_Id; }

    public DbsField getSc_Cover_Letter_Data_Institution_Ind() { return sc_Cover_Letter_Data_Institution_Ind; }

    public DbsField getSc_Cover_Letter_Data_Multi_Ia_Income_Ind() { return sc_Cover_Letter_Data_Multi_Ia_Income_Ind; }

    public DbsField getSc_Cover_Letter_Data_Ira_Rollover_Ind() { return sc_Cover_Letter_Data_Ira_Rollover_Ind; }

    public DbsField getSc_Cover_Letter_Data_Name_Addr_Lines() { return sc_Cover_Letter_Data_Name_Addr_Lines; }

    public DbsGroup getSc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2() { return sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2; }

    public DbsGroup getSc_Cover_Letter_Data_Suny_Cuny_Contract_Header() { return sc_Cover_Letter_Data_Suny_Cuny_Contract_Header; }

    public DbsField getSc_Cover_Letter_Data_Cover_Rec_Id_Bb() { return sc_Cover_Letter_Data_Cover_Rec_Id_Bb; }

    public DbsField getSc_Cover_Letter_Data_Sc_Cont_Type() { return sc_Cover_Letter_Data_Sc_Cont_Type; }

    public DbsField getSc_Cover_Letter_Data_Sc_Cont_Nbr() { return sc_Cover_Letter_Data_Sc_Cont_Nbr; }

    public DbsField getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt() { return sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt; }

    public DbsField getSc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct() { return sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct; }

    public DbsGroup getSc_Cover_Letter_Data_Sc_Cont_Detail_Data() { return sc_Cover_Letter_Data_Sc_Cont_Detail_Data; }

    public DbsField getSc_Cover_Letter_Data_Sc_Inst_Type() { return sc_Cover_Letter_Data_Sc_Inst_Type; }

    public DbsField getSc_Cover_Letter_Data_Sc_Orig_Cont_Type() { return sc_Cover_Letter_Data_Sc_Orig_Cont_Type; }

    public DbsField getSc_Cover_Letter_Data_Sc_Orig_Cont_Nbr() { return sc_Cover_Letter_Data_Sc_Orig_Cont_Nbr; }

    public DbsField getSc_Cover_Letter_Data_Sc_Plan_Nbr() { return sc_Cover_Letter_Data_Sc_Plan_Nbr; }

    public DbsField getSc_Cover_Letter_Data_Sc_Plan_Name() { return sc_Cover_Letter_Data_Sc_Plan_Name; }

    public DbsField getSc_Cover_Letter_Data_Sc_Plan_Income() { return sc_Cover_Letter_Data_Sc_Plan_Income; }

    public DbsField getSc_Cover_Letter_Data_Sc_Plan_Income_Pct() { return sc_Cover_Letter_Data_Sc_Plan_Income_Pct; }

    public DbsGroup getSc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3() { return sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3; }

    public DbsField getSc_Cover_Letter_Data_Write_Seq_Number() { return sc_Cover_Letter_Data_Write_Seq_Number; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sc_Cover_Letter_Data = newGroupInRecord("sc_Cover_Letter_Data", "SC-COVER-LETTER-DATA");
        sc_Cover_Letter_Data_Sc_Cover_Data_Array = sc_Cover_Letter_Data.newFieldArrayInGroup("sc_Cover_Letter_Data_Sc_Cover_Data_Array", "SC-COVER-DATA-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1,4000));
        sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1 = sc_Cover_Letter_Data.newGroupInGroup("sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1", "Redefines", 
            sc_Cover_Letter_Data_Sc_Cover_Data_Array);
        sc_Cover_Letter_Data_Letter_Data = sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef1.newGroupInGroup("sc_Cover_Letter_Data_Letter_Data", "LETTER-DATA");
        sc_Cover_Letter_Data_Cover_Rec_Id_Aa = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Cover_Rec_Id_Aa", "COVER-REC-ID-AA", 
            FieldType.STRING, 2);
        sc_Cover_Letter_Data_Year = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Year", "YEAR", FieldType.STRING, 4);
        sc_Cover_Letter_Data_Letter_Id = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Letter_Id", "LETTER-ID", FieldType.STRING, 
            4);
        sc_Cover_Letter_Data_Institution_Ind = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Institution_Ind", "INSTITUTION-IND", 
            FieldType.STRING, 1);
        sc_Cover_Letter_Data_Multi_Ia_Income_Ind = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Multi_Ia_Income_Ind", "MULTI-IA-INCOME-IND", 
            FieldType.STRING, 1);
        sc_Cover_Letter_Data_Ira_Rollover_Ind = sc_Cover_Letter_Data_Letter_Data.newFieldInGroup("sc_Cover_Letter_Data_Ira_Rollover_Ind", "IRA-ROLLOVER-IND", 
            FieldType.STRING, 1);
        sc_Cover_Letter_Data_Name_Addr_Lines = sc_Cover_Letter_Data_Letter_Data.newFieldArrayInGroup("sc_Cover_Letter_Data_Name_Addr_Lines", "NAME-ADDR-LINES", 
            FieldType.STRING, 40, new DbsArrayController(1,7));
        sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2 = sc_Cover_Letter_Data.newGroupInGroup("sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2", "Redefines", 
            sc_Cover_Letter_Data_Sc_Cover_Data_Array);
        sc_Cover_Letter_Data_Suny_Cuny_Contract_Header = sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef2.newGroupInGroup("sc_Cover_Letter_Data_Suny_Cuny_Contract_Header", 
            "SUNY-CUNY-CONTRACT-HEADER");
        sc_Cover_Letter_Data_Cover_Rec_Id_Bb = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newFieldInGroup("sc_Cover_Letter_Data_Cover_Rec_Id_Bb", 
            "COVER-REC-ID-BB", FieldType.STRING, 2);
        sc_Cover_Letter_Data_Sc_Cont_Type = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newFieldInGroup("sc_Cover_Letter_Data_Sc_Cont_Type", "SC-CONT-TYPE", 
            FieldType.STRING, 4);
        sc_Cover_Letter_Data_Sc_Cont_Nbr = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newFieldInGroup("sc_Cover_Letter_Data_Sc_Cont_Nbr", "SC-CONT-NBR", 
            FieldType.STRING, 10);
        sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newFieldInGroup("sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Amt", 
            "SC-CONT-TOT-INCOME-AMT", FieldType.STRING, 14);
        sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newFieldInGroup("sc_Cover_Letter_Data_Sc_Cont_Tot_Income_Pct", 
            "SC-CONT-TOT-INCOME-PCT", FieldType.STRING, 8);
        sc_Cover_Letter_Data_Sc_Cont_Detail_Data = sc_Cover_Letter_Data_Suny_Cuny_Contract_Header.newGroupArrayInGroup("sc_Cover_Letter_Data_Sc_Cont_Detail_Data", 
            "SC-CONT-DETAIL-DATA", new DbsArrayController(1,20));
        sc_Cover_Letter_Data_Sc_Inst_Type = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Inst_Type", "SC-INST-TYPE", 
            FieldType.STRING, 10);
        sc_Cover_Letter_Data_Sc_Orig_Cont_Type = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Orig_Cont_Type", "SC-ORIG-CONT-TYPE", 
            FieldType.STRING, 4);
        sc_Cover_Letter_Data_Sc_Orig_Cont_Nbr = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Orig_Cont_Nbr", "SC-ORIG-CONT-NBR", 
            FieldType.STRING, 10);
        sc_Cover_Letter_Data_Sc_Plan_Nbr = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Plan_Nbr", "SC-PLAN-NBR", 
            FieldType.STRING, 6);
        sc_Cover_Letter_Data_Sc_Plan_Name = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Plan_Name", "SC-PLAN-NAME", 
            FieldType.STRING, 65);
        sc_Cover_Letter_Data_Sc_Plan_Income = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Plan_Income", "SC-PLAN-INCOME", 
            FieldType.STRING, 14);
        sc_Cover_Letter_Data_Sc_Plan_Income_Pct = sc_Cover_Letter_Data_Sc_Cont_Detail_Data.newFieldInGroup("sc_Cover_Letter_Data_Sc_Plan_Income_Pct", 
            "SC-PLAN-INCOME-PCT", FieldType.STRING, 8);
        sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3 = sc_Cover_Letter_Data.newGroupInGroup("sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3", "Redefines", 
            sc_Cover_Letter_Data_Sc_Cover_Data_Array);
        sc_Cover_Letter_Data_Write_Seq_Number = sc_Cover_Letter_Data_Sc_Cover_Data_ArrayRedef3.newFieldInGroup("sc_Cover_Letter_Data_Write_Seq_Number", 
            "WRITE-SEQ-NUMBER", FieldType.NUMERIC, 7);

        this.setRecordName("LdaTwrl6395");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl6395() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
