/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:44 PM
**        * FROM NATURAL PDA     : TWRAPLSC
************************************************************
**        * FILE NAME            : PdaTwraplsc.java
**        * CLASS NAME           : PdaTwraplsc
**        * INSTANCE NAME        : PdaTwraplsc
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwraplsc extends PdaBase
{
    // Properties
    private DbsGroup twraplsc;
    private DbsGroup twraplsc_Input_Parms;
    private DbsField twraplsc_Pnd_Abend_Ind;
    private DbsField twraplsc_Pnd_Display_Ind;
    private DbsGroup twraplsc_Output_Data;
    private DbsField twraplsc_Pnd_Return_Code;
    private DbsField twraplsc_Pnd_Ret_Msg;
    private DbsField twraplsc_Rt_Plan;
    private DbsField twraplsc_Rt_Plan_Cnt;

    public DbsGroup getTwraplsc() { return twraplsc; }

    public DbsGroup getTwraplsc_Input_Parms() { return twraplsc_Input_Parms; }

    public DbsField getTwraplsc_Pnd_Abend_Ind() { return twraplsc_Pnd_Abend_Ind; }

    public DbsField getTwraplsc_Pnd_Display_Ind() { return twraplsc_Pnd_Display_Ind; }

    public DbsGroup getTwraplsc_Output_Data() { return twraplsc_Output_Data; }

    public DbsField getTwraplsc_Pnd_Return_Code() { return twraplsc_Pnd_Return_Code; }

    public DbsField getTwraplsc_Pnd_Ret_Msg() { return twraplsc_Pnd_Ret_Msg; }

    public DbsField getTwraplsc_Rt_Plan() { return twraplsc_Rt_Plan; }

    public DbsField getTwraplsc_Rt_Plan_Cnt() { return twraplsc_Rt_Plan_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        twraplsc = dbsRecord.newGroupInRecord("twraplsc", "TWRAPLSC");
        twraplsc.setParameterOption(ParameterOption.ByReference);
        twraplsc_Input_Parms = twraplsc.newGroupInGroup("twraplsc_Input_Parms", "INPUT-PARMS");
        twraplsc_Pnd_Abend_Ind = twraplsc_Input_Parms.newFieldInGroup("twraplsc_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN);
        twraplsc_Pnd_Display_Ind = twraplsc_Input_Parms.newFieldInGroup("twraplsc_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN);
        twraplsc_Output_Data = twraplsc.newGroupInGroup("twraplsc_Output_Data", "OUTPUT-DATA");
        twraplsc_Pnd_Return_Code = twraplsc_Output_Data.newFieldInGroup("twraplsc_Pnd_Return_Code", "#RETURN-CODE", FieldType.BOOLEAN);
        twraplsc_Pnd_Ret_Msg = twraplsc_Output_Data.newFieldInGroup("twraplsc_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);
        twraplsc_Rt_Plan = twraplsc_Output_Data.newFieldArrayInGroup("twraplsc_Rt_Plan", "RT-PLAN", FieldType.STRING, 6, new DbsArrayController(1,200));
        twraplsc_Rt_Plan_Cnt = twraplsc_Output_Data.newFieldInGroup("twraplsc_Rt_Plan_Cnt", "RT-PLAN-CNT", FieldType.PACKED_DECIMAL, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwraplsc(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

