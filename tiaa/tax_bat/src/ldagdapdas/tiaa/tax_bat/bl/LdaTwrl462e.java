/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:51 PM
**        * FROM NATURAL LDA     : TWRL462E
************************************************************
**        * FILE NAME            : LdaTwrl462e.java
**        * CLASS NAME           : LdaTwrl462e
**        * INSTANCE NAME        : LdaTwrl462e
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl462e extends DbsRecord
{
    // Properties
    private DbsGroup rd_Ira_Rec;
    private DbsField rd_Ira_Rec_Rd_Ira_Tax_Year;
    private DbsField rd_Ira_Rec_Rd_Ira_Company_Code;
    private DbsField rd_Ira_Rec_Rd_Ira_Simulation_Date;
    private DbsField rd_Ira_Rec_Rd_Ira_Tax_Id;
    private DbsField rd_Ira_Rec_Rd_Ira_Tax_Id_Type;
    private DbsField rd_Ira_Rec_Rd_Ira_Contract;
    private DbsField rd_Ira_Rec_Rd_Ira_Payee;
    private DbsField rd_Ira_Rec_Rd_Ira_Contract_Xref;
    private DbsField rd_Ira_Rec_Rd_Ira_Payee_Xref;
    private DbsField rd_Ira_Rec_Rd_Ira_Pin;
    private DbsField rd_Ira_Rec_Rd_Ira_Citizenship_Code;
    private DbsField rd_Ira_Rec_Rd_Ira_Residency_Code;
    private DbsField rd_Ira_Rec_Rd_Ira_Residency_Type;
    private DbsField rd_Ira_Rec_Rd_Ira_Locality_Code;
    private DbsField rd_Ira_Rec_Rd_Ira_Dob;
    private DbsField rd_Ira_Rec_Rd_Ira_Dod;
    private DbsField rd_Ira_Rec_Rd_Ira_Form;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_Type;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_Ocv;
    private DbsField rd_Ira_Rec_Rd_Ira_Rollover;
    private DbsField rd_Ira_Rec_Rd_Ira_Recharacter;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_Period;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_1078;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_1001;
    private DbsField rd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date;
    private DbsField rd_Ira_Rec_Rd_Ira_Classic_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Roth_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Rollover_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Recharacter_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Sep_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Fmv_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Roth_Conv_Amt;
    private DbsField rd_Ira_Rec_Rd_Ira_Name;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_1;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_2;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_3;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_4;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_5;
    private DbsField rd_Ira_Rec_Rd_Ira_Addr_6;
    private DbsField rd_Ira_Rec_Rd_Ira_City;
    private DbsField rd_Ira_Rec_Rd_Ira_State;
    private DbsField rd_Ira_Rec_Rd_Ira_Zip_Code;
    private DbsField rd_Ira_Rec_Rd_Ira_Foreign_Address;
    private DbsField rd_Ira_Rec_Rd_Ira_Foreign_Country;
    private DbsField rd_Ira_Rec_Rd_Ira_Rmd_Required_Flag;
    private DbsField rd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name;
    private DbsField rd_Ira_Rec_Rd_Ira_Unique_Id;
    private DbsField svt_Trans_Count;
    private DbsField svt_Classic_Amt;
    private DbsField svt_Roth_Amt;
    private DbsField svt_Rollover_Amt;
    private DbsField svt_Rechar_Amt;
    private DbsField svt_Sep_Amt;
    private DbsField svt_Fmv_Amt;
    private DbsField svt_Roth_Conv_Amt;
    private DbsField svc_Trans_Count;
    private DbsField svc_Classic_Amt;
    private DbsField svc_Roth_Amt;
    private DbsField svc_Rollover_Amt;
    private DbsField svc_Rechar_Amt;
    private DbsField svc_Sep_Amt;
    private DbsField svc_Fmv_Amt;
    private DbsField svc_Roth_Conv_Amt;
    private DbsField svs_Trans_Count;
    private DbsField svs_Classic_Amt;
    private DbsField svs_Roth_Amt;
    private DbsField svs_Rollover_Amt;
    private DbsField svs_Rechar_Amt;
    private DbsField svs_Sep_Amt;
    private DbsField svs_Fmv_Amt;
    private DbsField svs_Roth_Conv_Amt;
    private DbsField cmb_Trans_Count;
    private DbsField cmb_Classic_Amt;
    private DbsField cmb_Roth_Amt;
    private DbsField cmb_Rollover_Amt;
    private DbsField cmb_Rechar_Amt;
    private DbsField cmb_Sep_Amt;
    private DbsField cmb_Fmv_Amt;
    private DbsField cmb_Roth_Conv_Amt;

    public DbsGroup getRd_Ira_Rec() { return rd_Ira_Rec; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Tax_Year() { return rd_Ira_Rec_Rd_Ira_Tax_Year; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Company_Code() { return rd_Ira_Rec_Rd_Ira_Company_Code; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Simulation_Date() { return rd_Ira_Rec_Rd_Ira_Simulation_Date; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Tax_Id() { return rd_Ira_Rec_Rd_Ira_Tax_Id; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Tax_Id_Type() { return rd_Ira_Rec_Rd_Ira_Tax_Id_Type; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Contract() { return rd_Ira_Rec_Rd_Ira_Contract; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Payee() { return rd_Ira_Rec_Rd_Ira_Payee; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Contract_Xref() { return rd_Ira_Rec_Rd_Ira_Contract_Xref; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Payee_Xref() { return rd_Ira_Rec_Rd_Ira_Payee_Xref; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Pin() { return rd_Ira_Rec_Rd_Ira_Pin; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Citizenship_Code() { return rd_Ira_Rec_Rd_Ira_Citizenship_Code; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Residency_Code() { return rd_Ira_Rec_Rd_Ira_Residency_Code; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Residency_Type() { return rd_Ira_Rec_Rd_Ira_Residency_Type; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Locality_Code() { return rd_Ira_Rec_Rd_Ira_Locality_Code; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Dob() { return rd_Ira_Rec_Rd_Ira_Dob; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Dod() { return rd_Ira_Rec_Rd_Ira_Dod; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form() { return rd_Ira_Rec_Rd_Ira_Form; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_Type() { return rd_Ira_Rec_Rd_Ira_Form_Type; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_Ocv() { return rd_Ira_Rec_Rd_Ira_Form_Ocv; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Rollover() { return rd_Ira_Rec_Rd_Ira_Rollover; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Recharacter() { return rd_Ira_Rec_Rd_Ira_Recharacter; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_Period() { return rd_Ira_Rec_Rd_Ira_Form_Period; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_1078() { return rd_Ira_Rec_Rd_Ira_Form_1078; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date() { return rd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_1001() { return rd_Ira_Rec_Rd_Ira_Form_1001; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date() { return rd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Classic_Amt() { return rd_Ira_Rec_Rd_Ira_Classic_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Roth_Amt() { return rd_Ira_Rec_Rd_Ira_Roth_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Rollover_Amt() { return rd_Ira_Rec_Rd_Ira_Rollover_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Recharacter_Amt() { return rd_Ira_Rec_Rd_Ira_Recharacter_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Sep_Amt() { return rd_Ira_Rec_Rd_Ira_Sep_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Fmv_Amt() { return rd_Ira_Rec_Rd_Ira_Fmv_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Roth_Conv_Amt() { return rd_Ira_Rec_Rd_Ira_Roth_Conv_Amt; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Name() { return rd_Ira_Rec_Rd_Ira_Name; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_1() { return rd_Ira_Rec_Rd_Ira_Addr_1; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_2() { return rd_Ira_Rec_Rd_Ira_Addr_2; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_3() { return rd_Ira_Rec_Rd_Ira_Addr_3; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_4() { return rd_Ira_Rec_Rd_Ira_Addr_4; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_5() { return rd_Ira_Rec_Rd_Ira_Addr_5; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Addr_6() { return rd_Ira_Rec_Rd_Ira_Addr_6; }

    public DbsField getRd_Ira_Rec_Rd_Ira_City() { return rd_Ira_Rec_Rd_Ira_City; }

    public DbsField getRd_Ira_Rec_Rd_Ira_State() { return rd_Ira_Rec_Rd_Ira_State; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Zip_Code() { return rd_Ira_Rec_Rd_Ira_Zip_Code; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Foreign_Address() { return rd_Ira_Rec_Rd_Ira_Foreign_Address; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Foreign_Country() { return rd_Ira_Rec_Rd_Ira_Foreign_Country; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Rmd_Required_Flag() { return rd_Ira_Rec_Rd_Ira_Rmd_Required_Flag; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name() { return rd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name; }

    public DbsField getRd_Ira_Rec_Rd_Ira_Unique_Id() { return rd_Ira_Rec_Rd_Ira_Unique_Id; }

    public DbsField getSvt_Trans_Count() { return svt_Trans_Count; }

    public DbsField getSvt_Classic_Amt() { return svt_Classic_Amt; }

    public DbsField getSvt_Roth_Amt() { return svt_Roth_Amt; }

    public DbsField getSvt_Rollover_Amt() { return svt_Rollover_Amt; }

    public DbsField getSvt_Rechar_Amt() { return svt_Rechar_Amt; }

    public DbsField getSvt_Sep_Amt() { return svt_Sep_Amt; }

    public DbsField getSvt_Fmv_Amt() { return svt_Fmv_Amt; }

    public DbsField getSvt_Roth_Conv_Amt() { return svt_Roth_Conv_Amt; }

    public DbsField getSvc_Trans_Count() { return svc_Trans_Count; }

    public DbsField getSvc_Classic_Amt() { return svc_Classic_Amt; }

    public DbsField getSvc_Roth_Amt() { return svc_Roth_Amt; }

    public DbsField getSvc_Rollover_Amt() { return svc_Rollover_Amt; }

    public DbsField getSvc_Rechar_Amt() { return svc_Rechar_Amt; }

    public DbsField getSvc_Sep_Amt() { return svc_Sep_Amt; }

    public DbsField getSvc_Fmv_Amt() { return svc_Fmv_Amt; }

    public DbsField getSvc_Roth_Conv_Amt() { return svc_Roth_Conv_Amt; }

    public DbsField getSvs_Trans_Count() { return svs_Trans_Count; }

    public DbsField getSvs_Classic_Amt() { return svs_Classic_Amt; }

    public DbsField getSvs_Roth_Amt() { return svs_Roth_Amt; }

    public DbsField getSvs_Rollover_Amt() { return svs_Rollover_Amt; }

    public DbsField getSvs_Rechar_Amt() { return svs_Rechar_Amt; }

    public DbsField getSvs_Sep_Amt() { return svs_Sep_Amt; }

    public DbsField getSvs_Fmv_Amt() { return svs_Fmv_Amt; }

    public DbsField getSvs_Roth_Conv_Amt() { return svs_Roth_Conv_Amt; }

    public DbsField getCmb_Trans_Count() { return cmb_Trans_Count; }

    public DbsField getCmb_Classic_Amt() { return cmb_Classic_Amt; }

    public DbsField getCmb_Roth_Amt() { return cmb_Roth_Amt; }

    public DbsField getCmb_Rollover_Amt() { return cmb_Rollover_Amt; }

    public DbsField getCmb_Rechar_Amt() { return cmb_Rechar_Amt; }

    public DbsField getCmb_Sep_Amt() { return cmb_Sep_Amt; }

    public DbsField getCmb_Fmv_Amt() { return cmb_Fmv_Amt; }

    public DbsField getCmb_Roth_Conv_Amt() { return cmb_Roth_Conv_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        rd_Ira_Rec = newGroupInRecord("rd_Ira_Rec", "RD-IRA-REC");
        rd_Ira_Rec_Rd_Ira_Tax_Year = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Tax_Year", "RD-IRA-TAX-YEAR", FieldType.STRING, 4);
        rd_Ira_Rec_Rd_Ira_Company_Code = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Company_Code", "RD-IRA-COMPANY-CODE", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Simulation_Date = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Simulation_Date", "RD-IRA-SIMULATION-DATE", FieldType.STRING, 
            8);
        rd_Ira_Rec_Rd_Ira_Tax_Id = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Tax_Id", "RD-IRA-TAX-ID", FieldType.STRING, 10);
        rd_Ira_Rec_Rd_Ira_Tax_Id_Type = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Tax_Id_Type", "RD-IRA-TAX-ID-TYPE", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Contract = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Contract", "RD-IRA-CONTRACT", FieldType.STRING, 8);
        rd_Ira_Rec_Rd_Ira_Payee = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Payee", "RD-IRA-PAYEE", FieldType.STRING, 2);
        rd_Ira_Rec_Rd_Ira_Contract_Xref = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Contract_Xref", "RD-IRA-CONTRACT-XREF", FieldType.STRING, 8);
        rd_Ira_Rec_Rd_Ira_Payee_Xref = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Payee_Xref", "RD-IRA-PAYEE-XREF", FieldType.STRING, 2);
        rd_Ira_Rec_Rd_Ira_Pin = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Pin", "RD-IRA-PIN", FieldType.STRING, 12);
        rd_Ira_Rec_Rd_Ira_Citizenship_Code = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Citizenship_Code", "RD-IRA-CITIZENSHIP-CODE", FieldType.STRING, 
            2);
        rd_Ira_Rec_Rd_Ira_Residency_Code = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Residency_Code", "RD-IRA-RESIDENCY-CODE", FieldType.STRING, 2);
        rd_Ira_Rec_Rd_Ira_Residency_Type = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Residency_Type", "RD-IRA-RESIDENCY-TYPE", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Locality_Code = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Locality_Code", "RD-IRA-LOCALITY-CODE", FieldType.STRING, 3);
        rd_Ira_Rec_Rd_Ira_Dob = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Dob", "RD-IRA-DOB", FieldType.STRING, 8);
        rd_Ira_Rec_Rd_Ira_Dod = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Dod", "RD-IRA-DOD", FieldType.STRING, 8);
        rd_Ira_Rec_Rd_Ira_Form = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form", "RD-IRA-FORM", FieldType.STRING, 10);
        rd_Ira_Rec_Rd_Ira_Form_Type = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_Type", "RD-IRA-FORM-TYPE", FieldType.STRING, 2);
        rd_Ira_Rec_Rd_Ira_Form_Ocv = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_Ocv", "RD-IRA-FORM-OCV", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Rollover = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Rollover", "RD-IRA-ROLLOVER", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Recharacter = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Recharacter", "RD-IRA-RECHARACTER", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Form_Period = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_Period", "RD-IRA-FORM-PERIOD", FieldType.STRING, 3);
        rd_Ira_Rec_Rd_Ira_Form_1078 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_1078", "RD-IRA-FORM-1078", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_1078_Eff_Date", "RD-IRA-FORM-1078-EFF-DATE", FieldType.STRING, 
            8);
        rd_Ira_Rec_Rd_Ira_Form_1001 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_1001", "RD-IRA-FORM-1001", FieldType.STRING, 1);
        rd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Form_1001_Eff_Date", "RD-IRA-FORM-1001-EFF-DATE", FieldType.STRING, 
            8);
        rd_Ira_Rec_Rd_Ira_Classic_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Classic_Amt", "RD-IRA-CLASSIC-AMT", FieldType.DECIMAL, 11,2);
        rd_Ira_Rec_Rd_Ira_Roth_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Roth_Amt", "RD-IRA-ROTH-AMT", FieldType.DECIMAL, 11,2);
        rd_Ira_Rec_Rd_Ira_Rollover_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Rollover_Amt", "RD-IRA-ROLLOVER-AMT", FieldType.DECIMAL, 11,2);
        rd_Ira_Rec_Rd_Ira_Recharacter_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Recharacter_Amt", "RD-IRA-RECHARACTER-AMT", FieldType.DECIMAL, 
            11,2);
        rd_Ira_Rec_Rd_Ira_Sep_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Sep_Amt", "RD-IRA-SEP-AMT", FieldType.DECIMAL, 11,2);
        rd_Ira_Rec_Rd_Ira_Fmv_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Fmv_Amt", "RD-IRA-FMV-AMT", FieldType.DECIMAL, 11,2);
        rd_Ira_Rec_Rd_Ira_Roth_Conv_Amt = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Roth_Conv_Amt", "RD-IRA-ROTH-CONV-AMT", FieldType.DECIMAL, 11,
            2);
        rd_Ira_Rec_Rd_Ira_Name = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Name", "RD-IRA-NAME", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_1 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_1", "RD-IRA-ADDR-1", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_2 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_2", "RD-IRA-ADDR-2", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_3 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_3", "RD-IRA-ADDR-3", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_4 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_4", "RD-IRA-ADDR-4", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_5 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_5", "RD-IRA-ADDR-5", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_Addr_6 = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Addr_6", "RD-IRA-ADDR-6", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_City = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_City", "RD-IRA-CITY", FieldType.STRING, 35);
        rd_Ira_Rec_Rd_Ira_State = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_State", "RD-IRA-STATE", FieldType.STRING, 2);
        rd_Ira_Rec_Rd_Ira_Zip_Code = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Zip_Code", "RD-IRA-ZIP-CODE", FieldType.STRING, 9);
        rd_Ira_Rec_Rd_Ira_Foreign_Address = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Foreign_Address", "RD-IRA-FOREIGN-ADDRESS", FieldType.STRING, 
            1);
        rd_Ira_Rec_Rd_Ira_Foreign_Country = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Foreign_Country", "RD-IRA-FOREIGN-COUNTRY", FieldType.STRING, 
            35);
        rd_Ira_Rec_Rd_Ira_Rmd_Required_Flag = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Rmd_Required_Flag", "RD-IRA-RMD-REQUIRED-FLAG", FieldType.STRING, 
            1);
        rd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Rmd_Decedent_Name", "RD-IRA-RMD-DECEDENT-NAME", FieldType.STRING, 
            35);
        rd_Ira_Rec_Rd_Ira_Unique_Id = rd_Ira_Rec.newFieldInGroup("rd_Ira_Rec_Rd_Ira_Unique_Id", "RD-IRA-UNIQUE-ID", FieldType.STRING, 15);

        svt_Trans_Count = newFieldInRecord("svt_Trans_Count", "SVT-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7);

        svt_Classic_Amt = newFieldInRecord("svt_Classic_Amt", "SVT-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svt_Roth_Amt = newFieldInRecord("svt_Roth_Amt", "SVT-ROTH-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svt_Rollover_Amt = newFieldInRecord("svt_Rollover_Amt", "SVT-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svt_Rechar_Amt = newFieldInRecord("svt_Rechar_Amt", "SVT-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svt_Sep_Amt = newFieldInRecord("svt_Sep_Amt", "SVT-SEP-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svt_Fmv_Amt = newFieldInRecord("svt_Fmv_Amt", "SVT-FMV-AMT", FieldType.PACKED_DECIMAL, 13,2);

        svt_Roth_Conv_Amt = newFieldInRecord("svt_Roth_Conv_Amt", "SVT-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Trans_Count = newFieldInRecord("svc_Trans_Count", "SVC-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7);

        svc_Classic_Amt = newFieldInRecord("svc_Classic_Amt", "SVC-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Roth_Amt = newFieldInRecord("svc_Roth_Amt", "SVC-ROTH-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Rollover_Amt = newFieldInRecord("svc_Rollover_Amt", "SVC-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Rechar_Amt = newFieldInRecord("svc_Rechar_Amt", "SVC-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Sep_Amt = newFieldInRecord("svc_Sep_Amt", "SVC-SEP-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svc_Fmv_Amt = newFieldInRecord("svc_Fmv_Amt", "SVC-FMV-AMT", FieldType.PACKED_DECIMAL, 13,2);

        svc_Roth_Conv_Amt = newFieldInRecord("svc_Roth_Conv_Amt", "SVC-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Trans_Count = newFieldInRecord("svs_Trans_Count", "SVS-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7);

        svs_Classic_Amt = newFieldInRecord("svs_Classic_Amt", "SVS-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Roth_Amt = newFieldInRecord("svs_Roth_Amt", "SVS-ROTH-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Rollover_Amt = newFieldInRecord("svs_Rollover_Amt", "SVS-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Rechar_Amt = newFieldInRecord("svs_Rechar_Amt", "SVS-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Sep_Amt = newFieldInRecord("svs_Sep_Amt", "SVS-SEP-AMT", FieldType.PACKED_DECIMAL, 12,2);

        svs_Fmv_Amt = newFieldInRecord("svs_Fmv_Amt", "SVS-FMV-AMT", FieldType.PACKED_DECIMAL, 13,2);

        svs_Roth_Conv_Amt = newFieldInRecord("svs_Roth_Conv_Amt", "SVS-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        cmb_Trans_Count = newFieldArrayInRecord("cmb_Trans_Count", "CMB-TRANS-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1,4));

        cmb_Classic_Amt = newFieldArrayInRecord("cmb_Classic_Amt", "CMB-CLASSIC-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        cmb_Roth_Amt = newFieldArrayInRecord("cmb_Roth_Amt", "CMB-ROTH-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        cmb_Rollover_Amt = newFieldArrayInRecord("cmb_Rollover_Amt", "CMB-ROLLOVER-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        cmb_Rechar_Amt = newFieldArrayInRecord("cmb_Rechar_Amt", "CMB-RECHAR-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        cmb_Sep_Amt = newFieldArrayInRecord("cmb_Sep_Amt", "CMB-SEP-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        cmb_Fmv_Amt = newFieldArrayInRecord("cmb_Fmv_Amt", "CMB-FMV-AMT", FieldType.PACKED_DECIMAL, 13,2, new DbsArrayController(1,4));

        cmb_Roth_Conv_Amt = newFieldArrayInRecord("cmb_Roth_Conv_Amt", "CMB-ROTH-CONV-AMT", FieldType.PACKED_DECIMAL, 12,2, new DbsArrayController(1,4));

        this.setRecordName("LdaTwrl462e");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl462e() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
