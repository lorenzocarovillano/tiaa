/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:29 PM
**        * FROM NATURAL LDA     : TWRL3321
************************************************************
**        * FILE NAME            : LdaTwrl3321.java
**        * CLASS NAME           : LdaTwrl3321
**        * INSTANCE NAME        : LdaTwrl3321
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3321 extends DbsRecord
{
    // Properties
    private DbsGroup irs_A_Out_Tape;
    private DbsField irs_A_Out_Tape_Irs_A_Record_Type;
    private DbsField irs_A_Out_Tape_Irs_A_Payment_Year;
    private DbsField irs_A_Out_Tape_Irs_A_Combined_Filer;
    private DbsField irs_A_Out_Tape_Irs_A_Filler_1;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Ein;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Name_Control;
    private DbsField irs_A_Out_Tape_Irs_A_Last_Filing_Ind;
    private DbsField irs_A_Out_Tape_Irs_A_Type_Of_Return;
    private DbsField irs_A_Out_Tape_Irs_A_Amount_Indicators;
    private DbsField irs_A_Out_Tape_Irs_A_Irs_Filler_2;
    private DbsField irs_A_Out_Tape_Irs_A_Foreign_Corp_Indicator;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Name_1;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Name_2;
    private DbsField irs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Shipping_Address;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_City;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_State;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Zip;
    private DbsField irs_A_Out_Tape_Irs_A_Payer_Phone_No;
    private DbsField irs_A_Out_Tape_Irs_A_Filler_4;
    private DbsField irs_A_Out_Tape_Irs_A_Record_Sequence_No;
    private DbsField irs_A_Out_Tape_Irs_A_Filler_6;
    private DbsGroup irs_A_Out_TapeRedef1;
    private DbsField irs_A_Out_Tape_Irs_A_Move_1;
    private DbsField irs_A_Out_Tape_Irs_A_Move_2;
    private DbsField irs_A_Out_Tape_Irs_A_Move_3;

    public DbsGroup getIrs_A_Out_Tape() { return irs_A_Out_Tape; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Record_Type() { return irs_A_Out_Tape_Irs_A_Record_Type; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payment_Year() { return irs_A_Out_Tape_Irs_A_Payment_Year; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Combined_Filer() { return irs_A_Out_Tape_Irs_A_Combined_Filer; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Filler_1() { return irs_A_Out_Tape_Irs_A_Filler_1; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Ein() { return irs_A_Out_Tape_Irs_A_Payer_Ein; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Name_Control() { return irs_A_Out_Tape_Irs_A_Payer_Name_Control; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Last_Filing_Ind() { return irs_A_Out_Tape_Irs_A_Last_Filing_Ind; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Type_Of_Return() { return irs_A_Out_Tape_Irs_A_Type_Of_Return; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Amount_Indicators() { return irs_A_Out_Tape_Irs_A_Amount_Indicators; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Irs_Filler_2() { return irs_A_Out_Tape_Irs_A_Irs_Filler_2; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Foreign_Corp_Indicator() { return irs_A_Out_Tape_Irs_A_Foreign_Corp_Indicator; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Name_1() { return irs_A_Out_Tape_Irs_A_Payer_Name_1; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Name_2() { return irs_A_Out_Tape_Irs_A_Payer_Name_2; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator() { return irs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Shipping_Address() { return irs_A_Out_Tape_Irs_A_Payer_Shipping_Address; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_City() { return irs_A_Out_Tape_Irs_A_Payer_City; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_State() { return irs_A_Out_Tape_Irs_A_Payer_State; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Zip() { return irs_A_Out_Tape_Irs_A_Payer_Zip; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Payer_Phone_No() { return irs_A_Out_Tape_Irs_A_Payer_Phone_No; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Filler_4() { return irs_A_Out_Tape_Irs_A_Filler_4; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Record_Sequence_No() { return irs_A_Out_Tape_Irs_A_Record_Sequence_No; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Filler_6() { return irs_A_Out_Tape_Irs_A_Filler_6; }

    public DbsGroup getIrs_A_Out_TapeRedef1() { return irs_A_Out_TapeRedef1; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Move_1() { return irs_A_Out_Tape_Irs_A_Move_1; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Move_2() { return irs_A_Out_Tape_Irs_A_Move_2; }

    public DbsField getIrs_A_Out_Tape_Irs_A_Move_3() { return irs_A_Out_Tape_Irs_A_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_A_Out_Tape = newGroupInRecord("irs_A_Out_Tape", "IRS-A-OUT-TAPE");
        irs_A_Out_Tape_Irs_A_Record_Type = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Record_Type", "IRS-A-RECORD-TYPE", FieldType.STRING, 1);
        irs_A_Out_Tape_Irs_A_Payment_Year = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payment_Year", "IRS-A-PAYMENT-YEAR", FieldType.STRING, 
            4);
        irs_A_Out_Tape_Irs_A_Combined_Filer = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Combined_Filer", "IRS-A-COMBINED-FILER", FieldType.STRING, 
            1);
        irs_A_Out_Tape_Irs_A_Filler_1 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Filler_1", "IRS-A-FILLER-1", FieldType.STRING, 5);
        irs_A_Out_Tape_Irs_A_Payer_Ein = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Ein", "IRS-A-PAYER-EIN", FieldType.STRING, 9);
        irs_A_Out_Tape_Irs_A_Payer_Name_Control = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Name_Control", "IRS-A-PAYER-NAME-CONTROL", 
            FieldType.STRING, 4);
        irs_A_Out_Tape_Irs_A_Last_Filing_Ind = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Last_Filing_Ind", "IRS-A-LAST-FILING-IND", FieldType.STRING, 
            1);
        irs_A_Out_Tape_Irs_A_Type_Of_Return = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Type_Of_Return", "IRS-A-TYPE-OF-RETURN", FieldType.STRING, 
            2);
        irs_A_Out_Tape_Irs_A_Amount_Indicators = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Amount_Indicators", "IRS-A-AMOUNT-INDICATORS", FieldType.STRING, 
            16);
        irs_A_Out_Tape_Irs_A_Irs_Filler_2 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Irs_Filler_2", "IRS-A-IRS-FILLER-2", FieldType.STRING, 
            8);
        irs_A_Out_Tape_Irs_A_Foreign_Corp_Indicator = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Foreign_Corp_Indicator", "IRS-A-FOREIGN-CORP-INDICATOR", 
            FieldType.STRING, 1);
        irs_A_Out_Tape_Irs_A_Payer_Name_1 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Name_1", "IRS-A-PAYER-NAME-1", FieldType.STRING, 
            40);
        irs_A_Out_Tape_Irs_A_Payer_Name_2 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Name_2", "IRS-A-PAYER-NAME-2", FieldType.STRING, 
            40);
        irs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator", "IRS-A-TRANSFER-AGENT-INDICATOR", 
            FieldType.STRING, 1);
        irs_A_Out_Tape_Irs_A_Payer_Shipping_Address = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Shipping_Address", "IRS-A-PAYER-SHIPPING-ADDRESS", 
            FieldType.STRING, 40);
        irs_A_Out_Tape_Irs_A_Payer_City = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_City", "IRS-A-PAYER-CITY", FieldType.STRING, 40);
        irs_A_Out_Tape_Irs_A_Payer_State = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_State", "IRS-A-PAYER-STATE", FieldType.STRING, 2);
        irs_A_Out_Tape_Irs_A_Payer_Zip = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Zip", "IRS-A-PAYER-ZIP", FieldType.STRING, 9);
        irs_A_Out_Tape_Irs_A_Payer_Phone_No = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Payer_Phone_No", "IRS-A-PAYER-PHONE-NO", FieldType.STRING, 
            15);
        irs_A_Out_Tape_Irs_A_Filler_4 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Filler_4", "IRS-A-FILLER-4", FieldType.STRING, 260);
        irs_A_Out_Tape_Irs_A_Record_Sequence_No = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Record_Sequence_No", "IRS-A-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_A_Out_Tape_Irs_A_Filler_6 = irs_A_Out_Tape.newFieldInGroup("irs_A_Out_Tape_Irs_A_Filler_6", "IRS-A-FILLER-6", FieldType.STRING, 243);
        irs_A_Out_TapeRedef1 = newGroupInRecord("irs_A_Out_TapeRedef1", "Redefines", irs_A_Out_Tape);
        irs_A_Out_Tape_Irs_A_Move_1 = irs_A_Out_TapeRedef1.newFieldInGroup("irs_A_Out_Tape_Irs_A_Move_1", "IRS-A-MOVE-1", FieldType.STRING, 250);
        irs_A_Out_Tape_Irs_A_Move_2 = irs_A_Out_TapeRedef1.newFieldInGroup("irs_A_Out_Tape_Irs_A_Move_2", "IRS-A-MOVE-2", FieldType.STRING, 250);
        irs_A_Out_Tape_Irs_A_Move_3 = irs_A_Out_TapeRedef1.newFieldInGroup("irs_A_Out_Tape_Irs_A_Move_3", "IRS-A-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl3321");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_A_Out_Tape_Irs_A_Record_Type.setInitialValue("A");
        irs_A_Out_Tape_Irs_A_Combined_Filer.setInitialValue("1");
        irs_A_Out_Tape_Irs_A_Payer_Name_Control.setInitialValue("TIAA");
        irs_A_Out_Tape_Irs_A_Type_Of_Return.setInitialValue("9 ");
        irs_A_Out_Tape_Irs_A_Transfer_Agent_Indicator.setInitialValue("0");
        irs_A_Out_Tape_Irs_A_Payer_Shipping_Address.setInitialValue("730 THIRD AVENUE");
        irs_A_Out_Tape_Irs_A_Payer_City.setInitialValue("NEW YORK");
        irs_A_Out_Tape_Irs_A_Payer_State.setInitialValue("NY");
        irs_A_Out_Tape_Irs_A_Payer_Zip.setInitialValue("100173206");
        irs_A_Out_Tape_Irs_A_Payer_Phone_No.setInitialValue("180084227332068");
    }

    // Constructor
    public LdaTwrl3321() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
