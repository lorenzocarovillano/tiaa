/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:29 PM
**        * FROM NATURAL LDA     : TWRL3001
************************************************************
**        * FILE NAME            : LdaTwrl3001.java
**        * CLASS NAME           : LdaTwrl3001
**        * INSTANCE NAME        : LdaTwrl3001
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3001 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Info_Ff1;
    private DbsField pnd_Info_Ff1_Pnd_Ff1_Tax_Yr;
    private DbsField pnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr;
    private DbsField pnd_Info_Ff1_Pnd_Ff1_Intf_Date_To;
    private DbsField pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr;
    private DbsField pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To;

    public DbsGroup getPnd_Info_Ff1() { return pnd_Info_Ff1; }

    public DbsField getPnd_Info_Ff1_Pnd_Ff1_Tax_Yr() { return pnd_Info_Ff1_Pnd_Ff1_Tax_Yr; }

    public DbsField getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr() { return pnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr; }

    public DbsField getPnd_Info_Ff1_Pnd_Ff1_Intf_Date_To() { return pnd_Info_Ff1_Pnd_Ff1_Intf_Date_To; }

    public DbsField getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr() { return pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr; }

    public DbsField getPnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To() { return pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Info_Ff1 = newGroupInRecord("pnd_Info_Ff1", "#INFO-FF1");
        pnd_Info_Ff1_Pnd_Ff1_Tax_Yr = pnd_Info_Ff1.newFieldInGroup("pnd_Info_Ff1_Pnd_Ff1_Tax_Yr", "#FF1-TAX-YR", FieldType.NUMERIC, 4);
        pnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr = pnd_Info_Ff1.newFieldInGroup("pnd_Info_Ff1_Pnd_Ff1_Intf_Date_Fr", "#FF1-INTF-DATE-FR", FieldType.STRING, 8);
        pnd_Info_Ff1_Pnd_Ff1_Intf_Date_To = pnd_Info_Ff1.newFieldInGroup("pnd_Info_Ff1_Pnd_Ff1_Intf_Date_To", "#FF1-INTF-DATE-TO", FieldType.STRING, 8);
        pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr = pnd_Info_Ff1.newFieldInGroup("pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_Fr", "#FF1-PYMT-DATE-FR", FieldType.STRING, 8);
        pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To = pnd_Info_Ff1.newFieldInGroup("pnd_Info_Ff1_Pnd_Ff1_Pymt_Date_To", "#FF1-PYMT-DATE-TO", FieldType.STRING, 8);

        this.setRecordName("LdaTwrl3001");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3001() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
