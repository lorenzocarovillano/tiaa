/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:42 PM
**        * FROM NATURAL PDA     : TWRAFRM2
************************************************************
**        * FILE NAME            : PdaTwrafrm2.java
**        * CLASS NAME           : PdaTwrafrm2
**        * INSTANCE NAME        : PdaTwrafrm2
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwrafrm2 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Twrafrmn;
    private DbsGroup pnd_Twrafrmn_Pnd_Input_Parms;
    private DbsField pnd_Twrafrmn_Pnd_Tax_Year;
    private DbsGroup pnd_Twrafrmn_Pnd_Tax_YearRedef1;
    private DbsField pnd_Twrafrmn_Pnd_Tax_Year_A;
    private DbsGroup pnd_Twrafrmn_Pnd_Output_Data;
    private DbsField pnd_Twrafrmn_Pnd_Form_Name;
    private DbsField pnd_Twrafrmn_Pnd_Cntl_Form_Name;

    public DbsGroup getPnd_Twrafrmn() { return pnd_Twrafrmn; }

    public DbsGroup getPnd_Twrafrmn_Pnd_Input_Parms() { return pnd_Twrafrmn_Pnd_Input_Parms; }

    public DbsField getPnd_Twrafrmn_Pnd_Tax_Year() { return pnd_Twrafrmn_Pnd_Tax_Year; }

    public DbsGroup getPnd_Twrafrmn_Pnd_Tax_YearRedef1() { return pnd_Twrafrmn_Pnd_Tax_YearRedef1; }

    public DbsField getPnd_Twrafrmn_Pnd_Tax_Year_A() { return pnd_Twrafrmn_Pnd_Tax_Year_A; }

    public DbsGroup getPnd_Twrafrmn_Pnd_Output_Data() { return pnd_Twrafrmn_Pnd_Output_Data; }

    public DbsField getPnd_Twrafrmn_Pnd_Form_Name() { return pnd_Twrafrmn_Pnd_Form_Name; }

    public DbsField getPnd_Twrafrmn_Pnd_Cntl_Form_Name() { return pnd_Twrafrmn_Pnd_Cntl_Form_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrafrmn = dbsRecord.newGroupInRecord("pnd_Twrafrmn", "#TWRAFRMN");
        pnd_Twrafrmn.setParameterOption(ParameterOption.ByReference);
        pnd_Twrafrmn_Pnd_Input_Parms = pnd_Twrafrmn.newGroupInGroup("pnd_Twrafrmn_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Twrafrmn_Pnd_Tax_Year = pnd_Twrafrmn_Pnd_Input_Parms.newFieldInGroup("pnd_Twrafrmn_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Twrafrmn_Pnd_Tax_YearRedef1 = pnd_Twrafrmn_Pnd_Input_Parms.newGroupInGroup("pnd_Twrafrmn_Pnd_Tax_YearRedef1", "Redefines", pnd_Twrafrmn_Pnd_Tax_Year);
        pnd_Twrafrmn_Pnd_Tax_Year_A = pnd_Twrafrmn_Pnd_Tax_YearRedef1.newFieldInGroup("pnd_Twrafrmn_Pnd_Tax_Year_A", "#TAX-YEAR-A", FieldType.STRING, 
            4);
        pnd_Twrafrmn_Pnd_Output_Data = pnd_Twrafrmn.newGroupInGroup("pnd_Twrafrmn_Pnd_Output_Data", "#OUTPUT-DATA");
        pnd_Twrafrmn_Pnd_Form_Name = pnd_Twrafrmn_Pnd_Output_Data.newFieldArrayInGroup("pnd_Twrafrmn_Pnd_Form_Name", "#FORM-NAME", FieldType.STRING, 8, 
            new DbsArrayController(0,10));
        pnd_Twrafrmn_Pnd_Cntl_Form_Name = pnd_Twrafrmn_Pnd_Output_Data.newFieldArrayInGroup("pnd_Twrafrmn_Pnd_Cntl_Form_Name", "#CNTL-FORM-NAME", FieldType.STRING, 
            7, new DbsArrayController(0,10));

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwrafrm2(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

