/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:38 PM
**        * FROM NATURAL PDA     : TWRA5052
************************************************************
**        * FILE NAME            : PdaTwra5052.java
**        * CLASS NAME           : PdaTwra5052
**        * INSTANCE NAME        : PdaTwra5052
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwra5052 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Twra5052;
    private DbsGroup pnd_Twra5052_Pnd_Input_Parms;
    private DbsField pnd_Twra5052_Pnd_Lname;
    private DbsField pnd_Twra5052_Pnd_Abend_Ind;
    private DbsField pnd_Twra5052_Pnd_Display_Ind;
    private DbsField pnd_Twra5052_Pnd_Debug_Ind;
    private DbsGroup pnd_Twra5052_Pnd_Output_Data;
    private DbsField pnd_Twra5052_Pnd_Ret_Code;
    private DbsField pnd_Twra5052_Pnd_Ret_Msg;
    private DbsField pnd_Twra5052_Pnd_Lname_Control;

    public DbsGroup getPnd_Twra5052() { return pnd_Twra5052; }

    public DbsGroup getPnd_Twra5052_Pnd_Input_Parms() { return pnd_Twra5052_Pnd_Input_Parms; }

    public DbsField getPnd_Twra5052_Pnd_Lname() { return pnd_Twra5052_Pnd_Lname; }

    public DbsField getPnd_Twra5052_Pnd_Abend_Ind() { return pnd_Twra5052_Pnd_Abend_Ind; }

    public DbsField getPnd_Twra5052_Pnd_Display_Ind() { return pnd_Twra5052_Pnd_Display_Ind; }

    public DbsField getPnd_Twra5052_Pnd_Debug_Ind() { return pnd_Twra5052_Pnd_Debug_Ind; }

    public DbsGroup getPnd_Twra5052_Pnd_Output_Data() { return pnd_Twra5052_Pnd_Output_Data; }

    public DbsField getPnd_Twra5052_Pnd_Ret_Code() { return pnd_Twra5052_Pnd_Ret_Code; }

    public DbsField getPnd_Twra5052_Pnd_Ret_Msg() { return pnd_Twra5052_Pnd_Ret_Msg; }

    public DbsField getPnd_Twra5052_Pnd_Lname_Control() { return pnd_Twra5052_Pnd_Lname_Control; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twra5052 = dbsRecord.newGroupInRecord("pnd_Twra5052", "#TWRA5052");
        pnd_Twra5052.setParameterOption(ParameterOption.ByReference);
        pnd_Twra5052_Pnd_Input_Parms = pnd_Twra5052.newGroupInGroup("pnd_Twra5052_Pnd_Input_Parms", "#INPUT-PARMS");
        pnd_Twra5052_Pnd_Lname = pnd_Twra5052_Pnd_Input_Parms.newFieldInGroup("pnd_Twra5052_Pnd_Lname", "#LNAME", FieldType.STRING, 30);
        pnd_Twra5052_Pnd_Abend_Ind = pnd_Twra5052_Pnd_Input_Parms.newFieldInGroup("pnd_Twra5052_Pnd_Abend_Ind", "#ABEND-IND", FieldType.BOOLEAN);
        pnd_Twra5052_Pnd_Display_Ind = pnd_Twra5052_Pnd_Input_Parms.newFieldInGroup("pnd_Twra5052_Pnd_Display_Ind", "#DISPLAY-IND", FieldType.BOOLEAN);
        pnd_Twra5052_Pnd_Debug_Ind = pnd_Twra5052_Pnd_Input_Parms.newFieldInGroup("pnd_Twra5052_Pnd_Debug_Ind", "#DEBUG-IND", FieldType.BOOLEAN);
        pnd_Twra5052_Pnd_Output_Data = pnd_Twra5052.newGroupInGroup("pnd_Twra5052_Pnd_Output_Data", "#OUTPUT-DATA");
        pnd_Twra5052_Pnd_Ret_Code = pnd_Twra5052_Pnd_Output_Data.newFieldInGroup("pnd_Twra5052_Pnd_Ret_Code", "#RET-CODE", FieldType.BOOLEAN);
        pnd_Twra5052_Pnd_Ret_Msg = pnd_Twra5052_Pnd_Output_Data.newFieldInGroup("pnd_Twra5052_Pnd_Ret_Msg", "#RET-MSG", FieldType.STRING, 35);
        pnd_Twra5052_Pnd_Lname_Control = pnd_Twra5052_Pnd_Output_Data.newFieldInGroup("pnd_Twra5052_Pnd_Lname_Control", "#LNAME-CONTROL", FieldType.STRING, 
            4);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwra5052(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

