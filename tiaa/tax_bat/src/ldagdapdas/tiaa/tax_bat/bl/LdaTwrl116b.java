/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:20 PM
**        * FROM NATURAL LDA     : TWRL116B
************************************************************
**        * FILE NAME            : LdaTwrl116b.java
**        * CLASS NAME           : LdaTwrl116b
**        * INSTANCE NAME        : LdaTwrl116b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl116b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl116b;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_City;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Province;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Payer_Zip;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Contact_Name;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Tax_Year;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Total_Nr4;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Rpt_Type;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Gross_Income;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Filler;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_Rec_Type;
    private DbsGroup pnd_Twrl116bRedef1;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_1;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_2;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_3;
    private DbsField pnd_Twrl116b_Pnd_Twrl116b_4;

    public DbsGroup getPnd_Twrl116b() { return pnd_Twrl116b; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct() { return pnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_City() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_City; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Province() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Province; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Payer_Zip() { return pnd_Twrl116b_Pnd_Twrl116b_Payer_Zip; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Contact_Name() { return pnd_Twrl116b_Pnd_Twrl116b_Contact_Name; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code() { return pnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Contact_Phone() { return pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext() { return pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Tax_Year() { return pnd_Twrl116b_Pnd_Twrl116b_Tax_Year; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Total_Nr4() { return pnd_Twrl116b_Pnd_Twrl116b_Total_Nr4; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Rpt_Type() { return pnd_Twrl116b_Pnd_Twrl116b_Rpt_Type; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Gross_Income() { return pnd_Twrl116b_Pnd_Twrl116b_Gross_Income; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld() { return pnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26() { return pnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27() { return pnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported() { return pnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt() { return pnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Filler() { return pnd_Twrl116b_Pnd_Twrl116b_Filler; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_Rec_Type() { return pnd_Twrl116b_Pnd_Twrl116b_Rec_Type; }

    public DbsGroup getPnd_Twrl116bRedef1() { return pnd_Twrl116bRedef1; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_1() { return pnd_Twrl116b_Pnd_Twrl116b_1; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_2() { return pnd_Twrl116b_Pnd_Twrl116b_2; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_3() { return pnd_Twrl116b_Pnd_Twrl116b_3; }

    public DbsField getPnd_Twrl116b_Pnd_Twrl116b_4() { return pnd_Twrl116b_Pnd_Twrl116b_4; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl116b = newGroupInRecord("pnd_Twrl116b", "#TWRL116B");
        pnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Non_Resident_Acct", "#TWRL116B-NON-RESIDENT-ACCT", 
            FieldType.STRING, 9);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_1", "#TWRL116B-PAYER-NAME-1", FieldType.STRING, 
            30);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_2", "#TWRL116B-PAYER-NAME-2", FieldType.STRING, 
            30);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Name_3", "#TWRL116B-PAYER-NAME-3", FieldType.STRING, 
            30);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_1", "#TWRL116B-PAYER-ADDRESS-1", 
            FieldType.STRING, 30);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Address_2", "#TWRL116B-PAYER-ADDRESS-2", 
            FieldType.STRING, 30);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_City = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_City", "#TWRL116B-PAYER-CITY", FieldType.STRING, 
            28);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Province = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Province", "#TWRL116B-PAYER-PROVINCE", 
            FieldType.STRING, 2);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Country_Code", "#TWRL116B-PAYER-COUNTRY-CODE", 
            FieldType.STRING, 3);
        pnd_Twrl116b_Pnd_Twrl116b_Payer_Zip = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Payer_Zip", "#TWRL116B-PAYER-ZIP", FieldType.STRING, 
            10);
        pnd_Twrl116b_Pnd_Twrl116b_Contact_Name = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Contact_Name", "#TWRL116B-CONTACT-NAME", FieldType.STRING, 
            22);
        pnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Contact_Area_Code", "#TWRL116B-CONTACT-AREA-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone", "#TWRL116B-CONTACT-PHONE", FieldType.STRING, 
            8);
        pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Contact_Phone_Ext", "#TWRL116B-CONTACT-PHONE-EXT", 
            FieldType.NUMERIC, 5);
        pnd_Twrl116b_Pnd_Twrl116b_Tax_Year = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Tax_Year", "#TWRL116B-TAX-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Twrl116b_Pnd_Twrl116b_Total_Nr4 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Total_Nr4", "#TWRL116B-TOTAL-NR4", FieldType.NUMERIC, 
            7);
        pnd_Twrl116b_Pnd_Twrl116b_Rpt_Type = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Rpt_Type", "#TWRL116B-RPT-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl116b_Pnd_Twrl116b_Gross_Income = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Gross_Income", "#TWRL116B-GROSS-INCOME", FieldType.DECIMAL, 
            13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Non_Res_Hld", "#TWRL116B-NON-RES-HLD", FieldType.DECIMAL, 
            13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Tot_Gross_Box_26", "#TWRL116B-TOT-GROSS-BOX-26", 
            FieldType.DECIMAL, 13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27 = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Tot_Nrh_Box_27", "#TWRL116B-TOT-NRH-BOX-27", 
            FieldType.DECIMAL, 13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Tot_Not_Reported", "#TWRL116B-TOT-NOT-REPORTED", 
            FieldType.DECIMAL, 13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Tot_Held_Not_Rpt", "#TWRL116B-TOT-HELD-NOT-RPT", 
            FieldType.DECIMAL, 13,2);
        pnd_Twrl116b_Pnd_Twrl116b_Filler = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Filler", "#TWRL116B-FILLER", FieldType.STRING, 21);
        pnd_Twrl116b_Pnd_Twrl116b_Rec_Type = pnd_Twrl116b.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_Rec_Type", "#TWRL116B-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl116bRedef1 = newGroupInRecord("pnd_Twrl116bRedef1", "Redefines", pnd_Twrl116b);
        pnd_Twrl116b_Pnd_Twrl116b_1 = pnd_Twrl116bRedef1.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_1", "#TWRL116B-1", FieldType.STRING, 100);
        pnd_Twrl116b_Pnd_Twrl116b_2 = pnd_Twrl116bRedef1.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_2", "#TWRL116B-2", FieldType.STRING, 100);
        pnd_Twrl116b_Pnd_Twrl116b_3 = pnd_Twrl116bRedef1.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_3", "#TWRL116B-3", FieldType.STRING, 100);
        pnd_Twrl116b_Pnd_Twrl116b_4 = pnd_Twrl116bRedef1.newFieldInGroup("pnd_Twrl116b_Pnd_Twrl116b_4", "#TWRL116B-4", FieldType.STRING, 52);

        this.setRecordName("LdaTwrl116b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl116b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
