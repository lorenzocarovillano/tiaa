/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:16:14 PM
**        * FROM NATURAL LDA     : TXWL5050
************************************************************
**        * FILE NAME            : LdaTxwl5050.java
**        * CLASS NAME           : LdaTxwl5050
**        * INSTANCE NAME        : LdaTxwl5050
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTxwl5050 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_txwl5050;
    private DbsField txwl5050_Tin_Type;
    private DbsField txwl5050_Tin;
    private DbsField txwl5050_Contract_Nbr;
    private DbsField txwl5050_Payee_Cde;
    private DbsField txwl5050_Lu_User;
    private DbsField txwl5050_Effective_Ts;
    private DbsField txwl5050_Active_Ind;
    private DbsGroup txwl5050_Election_Group;
    private DbsField txwl5050_Elc_Cde;
    private DbsField txwl5050_Elc_Type;
    private DbsField txwl5050_Elc_Res;
    private DbsField txwl5050_Use_Dte;
    private DbsField txwl5050_Lu_Ts;

    public DataAccessProgramView getVw_txwl5050() { return vw_txwl5050; }

    public DbsField getTxwl5050_Tin_Type() { return txwl5050_Tin_Type; }

    public DbsField getTxwl5050_Tin() { return txwl5050_Tin; }

    public DbsField getTxwl5050_Contract_Nbr() { return txwl5050_Contract_Nbr; }

    public DbsField getTxwl5050_Payee_Cde() { return txwl5050_Payee_Cde; }

    public DbsField getTxwl5050_Lu_User() { return txwl5050_Lu_User; }

    public DbsField getTxwl5050_Effective_Ts() { return txwl5050_Effective_Ts; }

    public DbsField getTxwl5050_Active_Ind() { return txwl5050_Active_Ind; }

    public DbsGroup getTxwl5050_Election_Group() { return txwl5050_Election_Group; }

    public DbsField getTxwl5050_Elc_Cde() { return txwl5050_Elc_Cde; }

    public DbsField getTxwl5050_Elc_Type() { return txwl5050_Elc_Type; }

    public DbsField getTxwl5050_Elc_Res() { return txwl5050_Elc_Res; }

    public DbsField getTxwl5050_Use_Dte() { return txwl5050_Use_Dte; }

    public DbsField getTxwl5050_Lu_Ts() { return txwl5050_Lu_Ts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_txwl5050 = new DataAccessProgramView(new NameInfo("vw_txwl5050", "TXWL5050"), "TWR_ELECTION", "TWR_ELECTION");
        txwl5050_Tin_Type = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Tin_Type", "TIN-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TIN_TYPE");
        txwl5050_Tin_Type.setDdmHeader("TIN/TYPE");
        txwl5050_Tin = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Tin", "TIN", FieldType.STRING, 10, RepeatingFieldStrategy.None, "TIN");
        txwl5050_Tin.setDdmHeader("TAX/ID/NUMBER");
        txwl5050_Contract_Nbr = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Contract_Nbr", "CONTRACT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CONTRACT_NBR");
        txwl5050_Contract_Nbr.setDdmHeader("CONTRACT/NUMBER");
        txwl5050_Payee_Cde = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PAYEE_CDE");
        txwl5050_Payee_Cde.setDdmHeader("PAYEE/CODE");
        txwl5050_Lu_User = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Lu_User", "LU-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "LU_USER");
        txwl5050_Lu_User.setDdmHeader("LAST/UPDATE/USER");
        txwl5050_Effective_Ts = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Effective_Ts", "EFFECTIVE-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "EFFECTIVE_TS");
        txwl5050_Effective_Ts.setDdmHeader("EFFECTIVE/TIMESTAMP");
        txwl5050_Active_Ind = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ACTIVE_IND");
        txwl5050_Active_Ind.setDdmHeader("ACT/IND");
        txwl5050_Election_Group = vw_txwl5050.getRecord().newGroupInGroup("txwl5050_Election_Group", "ELECTION-GROUP");
        txwl5050_Elc_Cde = txwl5050_Election_Group.newFieldInGroup("txwl5050_Elc_Cde", "ELC-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ELC_CDE");
        txwl5050_Elc_Cde.setDdmHeader("ELC/CDE");
        txwl5050_Elc_Type = txwl5050_Election_Group.newFieldInGroup("txwl5050_Elc_Type", "ELC-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ELC_TYPE");
        txwl5050_Elc_Type.setDdmHeader("ELC/TYP");
        txwl5050_Elc_Res = txwl5050_Election_Group.newFieldInGroup("txwl5050_Elc_Res", "ELC-RES", FieldType.STRING, 3, RepeatingFieldStrategy.None, "ELC_RES");
        txwl5050_Elc_Res.setDdmHeader("ELC/RES");
        txwl5050_Use_Dte = txwl5050_Election_Group.newFieldInGroup("txwl5050_Use_Dte", "USE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "USE_DTE");
        txwl5050_Use_Dte.setDdmHeader("1ST/USE/DATE");
        txwl5050_Lu_Ts = vw_txwl5050.getRecord().newFieldInGroup("txwl5050_Lu_Ts", "LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, "LU_TS");
        txwl5050_Lu_Ts.setDdmHeader("LAST UPDATE/TIME/STAMP");

        this.setRecordName("LdaTxwl5050");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_txwl5050.reset();
    }

    // Constructor
    public LdaTxwl5050() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
