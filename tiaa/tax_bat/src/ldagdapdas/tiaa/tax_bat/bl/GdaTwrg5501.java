/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:08 PM
**        * FROM NATURAL GDA     : TWRG5501
************************************************************
**        * FILE NAME            : GdaTwrg5501.java
**        * CLASS NAME           : GdaTwrg5501
**        * INSTANCE NAME        : GdaTwrg5501
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaTwrg5501 extends DbsRecord
{
    private static ThreadLocal<List<GdaTwrg5501>> _instance = new ThreadLocal<List<GdaTwrg5501>>();

    // Properties
    private DbsField pnd_St_Tot_Max;
    private DbsGroup pnd_St_Cntl;
    private DbsField pnd_St_Cntl_Pnd_State_Desc;
    private DbsField pnd_St_Cntl_Pnd_State_Rule;
    private DbsField pnd_St_Cntl_Pnd_State_Cmb;
    private DbsField pnd_St_Cntl_Pnd_State_Med;
    private DbsField pnd_St_Cntl_Pnd_State_Gross_Rpt;
    private DbsGroup pnd_St_Tot;
    private DbsField pnd_St_Tot_Pnd_Tran_Cnt;
    private DbsField pnd_St_Tot_Pnd_Gross_Amt;
    private DbsField pnd_St_Tot_Pnd_Pymnt_Ivc;
    private DbsField pnd_St_Tot_Pnd_Ivc_Adj;
    private DbsField pnd_St_Tot_Pnd_Form_Ivc;
    private DbsField pnd_St_Tot_Pnd_Sta_Tax;
    private DbsField pnd_St_Tot_Pnd_Loc_Tax;

    public DbsField getPnd_St_Tot_Max() { return pnd_St_Tot_Max; }

    public DbsGroup getPnd_St_Cntl() { return pnd_St_Cntl; }

    public DbsField getPnd_St_Cntl_Pnd_State_Desc() { return pnd_St_Cntl_Pnd_State_Desc; }

    public DbsField getPnd_St_Cntl_Pnd_State_Rule() { return pnd_St_Cntl_Pnd_State_Rule; }

    public DbsField getPnd_St_Cntl_Pnd_State_Cmb() { return pnd_St_Cntl_Pnd_State_Cmb; }

    public DbsField getPnd_St_Cntl_Pnd_State_Med() { return pnd_St_Cntl_Pnd_State_Med; }

    public DbsField getPnd_St_Cntl_Pnd_State_Gross_Rpt() { return pnd_St_Cntl_Pnd_State_Gross_Rpt; }

    public DbsGroup getPnd_St_Tot() { return pnd_St_Tot; }

    public DbsField getPnd_St_Tot_Pnd_Tran_Cnt() { return pnd_St_Tot_Pnd_Tran_Cnt; }

    public DbsField getPnd_St_Tot_Pnd_Gross_Amt() { return pnd_St_Tot_Pnd_Gross_Amt; }

    public DbsField getPnd_St_Tot_Pnd_Pymnt_Ivc() { return pnd_St_Tot_Pnd_Pymnt_Ivc; }

    public DbsField getPnd_St_Tot_Pnd_Ivc_Adj() { return pnd_St_Tot_Pnd_Ivc_Adj; }

    public DbsField getPnd_St_Tot_Pnd_Form_Ivc() { return pnd_St_Tot_Pnd_Form_Ivc; }

    public DbsField getPnd_St_Tot_Pnd_Sta_Tax() { return pnd_St_Tot_Pnd_Sta_Tax; }

    public DbsField getPnd_St_Tot_Pnd_Loc_Tax() { return pnd_St_Tot_Pnd_Loc_Tax; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        int int_pnd_St_Tot_Max = 62;

        pnd_St_Tot_Max = newFieldInRecord("pnd_St_Tot_Max", "#ST-TOT-MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_St_Cntl = newGroupArrayInRecord("pnd_St_Cntl", "#ST-CNTL", new DbsArrayController(0,int_pnd_St_Tot_Max));
        pnd_St_Cntl_Pnd_State_Desc = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Desc", "#STATE-DESC", FieldType.STRING, 18);
        pnd_St_Cntl_Pnd_State_Rule = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Rule", "#STATE-RULE", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Cmb = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Cmb", "#STATE-CMB", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Med = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Med", "#STATE-MED", FieldType.STRING, 1);
        pnd_St_Cntl_Pnd_State_Gross_Rpt = pnd_St_Cntl.newFieldInGroup("pnd_St_Cntl_Pnd_State_Gross_Rpt", "#STATE-GROSS-RPT", FieldType.BOOLEAN);

        pnd_St_Tot = newGroupArrayInRecord("pnd_St_Tot", "#ST-TOT", new DbsArrayController(0,int_pnd_St_Tot_Max,1,2,1,5));
        pnd_St_Tot_Pnd_Tran_Cnt = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Tran_Cnt", "#TRAN-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_St_Tot_Pnd_Gross_Amt = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 13,2);
        pnd_St_Tot_Pnd_Pymnt_Ivc = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Pymnt_Ivc", "#PYMNT-IVC", FieldType.PACKED_DECIMAL, 11,2);
        pnd_St_Tot_Pnd_Ivc_Adj = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Ivc_Adj", "#IVC-ADJ", FieldType.PACKED_DECIMAL, 11,2);
        pnd_St_Tot_Pnd_Form_Ivc = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Form_Ivc", "#FORM-IVC", FieldType.PACKED_DECIMAL, 11,2);
        pnd_St_Tot_Pnd_Sta_Tax = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Sta_Tax", "#STA-TAX", FieldType.PACKED_DECIMAL, 11,2);
        pnd_St_Tot_Pnd_Loc_Tax = pnd_St_Tot.newFieldInGroup("pnd_St_Tot_Pnd_Loc_Tax", "#LOC-TAX", FieldType.PACKED_DECIMAL, 11,2);

        this.setRecordName("GdaTwrg5501");
    }
    public void initializeValues() throws Exception
    {
        reset();

        pnd_St_Tot_Max.setInitialValue(62);
        pnd_St_Cntl_Pnd_State_Desc.getValue(59).setInitialValue("Other US residency");
        pnd_St_Cntl_Pnd_State_Desc.getValue(60).setInitialValue("Form Total");
        pnd_St_Cntl_Pnd_State_Desc.getValue(61).setInitialValue("Non US residency");
        pnd_St_Cntl_Pnd_State_Desc.getValue(62).setInitialValue("Grand Total");
    }

    // Constructor
    private GdaTwrg5501() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaTwrg5501 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaTwrg5501>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaTwrg5501());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

