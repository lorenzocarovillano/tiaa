/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:35 PM
**        * FROM NATURAL LDA     : TWRL3620
************************************************************
**        * FILE NAME            : LdaTwrl3620.java
**        * CLASS NAME           : LdaTwrl3620
**        * INSTANCE NAME        : LdaTwrl3620
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3620 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Out_Ny_Record;
    private DbsField pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank;
    private DbsGroup pnd_Out_Ny_RecordRedef1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter1;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Name;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter2;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter4;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter5;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter6;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter7;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter8;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1;
    private DbsGroup pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2;
    private DbsField pnd_Out_Ny_Record_Pnd_Name1_A;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2;
    private DbsGroup pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3;
    private DbsField pnd_Out_Ny_Record_Pnd_Name2_A;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter9;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter10;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Tin_Gross;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter3;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter11;
    private DbsField pnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot;
    private DbsField pnd_Out_Ny_Record_Pnd_Delimiter12;

    public DbsGroup getPnd_Out_Ny_Record() { return pnd_Out_Ny_Record; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outa_Ny_Blank() { return pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank; }

    public DbsGroup getPnd_Out_Ny_RecordRedef1() { return pnd_Out_Ny_RecordRedef1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Tin() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter1() { return pnd_Out_Ny_Record_Pnd_Delimiter1; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Name() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Name; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter2() { return pnd_Out_Ny_Record_Pnd_Delimiter2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter4() { return pnd_Out_Ny_Record_Pnd_Delimiter4; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter5() { return pnd_Out_Ny_Record_Pnd_Delimiter5; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter6() { return pnd_Out_Ny_Record_Pnd_Delimiter6; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter7() { return pnd_Out_Ny_Record_Pnd_Delimiter7; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter8() { return pnd_Out_Ny_Record_Pnd_Delimiter8; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1; }

    public DbsGroup getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Name1_A() { return pnd_Out_Ny_Record_Pnd_Name1_A; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2; }

    public DbsGroup getPnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Name2_A() { return pnd_Out_Ny_Record_Pnd_Name2_A; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter9() { return pnd_Out_Ny_Record_Pnd_Delimiter9; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter10() { return pnd_Out_Ny_Record_Pnd_Delimiter10; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Tin_Gross() { return pnd_Out_Ny_Record_Pnd_Outw_Tin_Gross; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter3() { return pnd_Out_Ny_Record_Pnd_Delimiter3; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter11() { return pnd_Out_Ny_Record_Pnd_Delimiter11; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot() { return pnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot; }

    public DbsField getPnd_Out_Ny_Record_Pnd_Delimiter12() { return pnd_Out_Ny_Record_Pnd_Delimiter12; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out_Ny_Record = newGroupInRecord("pnd_Out_Ny_Record", "#OUT-NY-RECORD");
        pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank = pnd_Out_Ny_Record.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outa_Ny_Blank", "#OUTA-NY-BLANK", FieldType.STRING, 
            300);
        pnd_Out_Ny_RecordRedef1 = newGroupInRecord("pnd_Out_Ny_RecordRedef1", "Redefines", pnd_Out_Ny_Record);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Tin", "#OUTW-NY-TIN", FieldType.STRING, 
            9);
        pnd_Out_Ny_Record_Pnd_Delimiter1 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter1", "#DELIMITER1", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Name = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Name", "#OUTW-NY-NAME", FieldType.STRING, 
            30);
        pnd_Out_Ny_Record_Pnd_Delimiter2 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter2", "#DELIMITER2", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract", "#OUTW-NY-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Out_Ny_Record_Pnd_Delimiter4 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter4", "#DELIMITER4", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Contract_Type", "#OUTW-NY-CONTRACT-TYPE", 
            FieldType.STRING, 6);
        pnd_Out_Ny_Record_Pnd_Delimiter5 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter5", "#DELIMITER5", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Originating_Contract", 
            "#OUTW-NY-ORIGINATING-CONTRACT", FieldType.STRING, 10);
        pnd_Out_Ny_Record_Pnd_Delimiter6 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter6", "#DELIMITER6", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Orig_Contract_Type", 
            "#OUTW-NY-ORIG-CONTRACT-TYPE", FieldType.STRING, 6);
        pnd_Out_Ny_Record_Pnd_Delimiter7 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter7", "#DELIMITER7", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan", "#OUTW-NY-PLAN", FieldType.STRING, 
            6);
        pnd_Out_Ny_Record_Pnd_Delimiter8 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter8", "#DELIMITER8", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1", "#OUTW-NY-PLAN-NAME1", 
            FieldType.STRING, 32);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2 = pnd_Out_Ny_RecordRedef1.newGroupInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2", "Redefines", 
            pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1);
        pnd_Out_Ny_Record_Pnd_Name1_A = pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name1Redef2.newFieldArrayInGroup("pnd_Out_Ny_Record_Pnd_Name1_A", "#NAME1-A", 
            FieldType.STRING, 1, new DbsArrayController(1,32));
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2", "#OUTW-NY-PLAN-NAME2", 
            FieldType.STRING, 32);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3 = pnd_Out_Ny_RecordRedef1.newGroupInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3", "Redefines", 
            pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2);
        pnd_Out_Ny_Record_Pnd_Name2_A = pnd_Out_Ny_Record_Pnd_Outw_Ny_Plan_Name2Redef3.newFieldArrayInGroup("pnd_Out_Ny_Record_Pnd_Name2_A", "#NAME2-A", 
            FieldType.STRING, 1, new DbsArrayController(1,32));
        pnd_Out_Ny_Record_Pnd_Delimiter9 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter9", "#DELIMITER9", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Empl_Type", "#OUTW-NY-EMPL-TYPE", 
            FieldType.STRING, 13);
        pnd_Out_Ny_Record_Pnd_Delimiter10 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter10", "#DELIMITER10", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Tin_Gross = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Tin_Gross", "#OUTW-TIN-GROSS", FieldType.STRING, 
            20);
        pnd_Out_Ny_Record_Pnd_Delimiter3 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter3", "#DELIMITER3", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Gross_Wage", "#OUTW-NY-GROSS-WAGE", 
            FieldType.STRING, 20);
        pnd_Out_Ny_Record_Pnd_Delimiter11 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter11", "#DELIMITER11", FieldType.STRING, 
            1);
        pnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Outw_Ny_Perc_Contract_Tot", "#OUTW-NY-PERC-CONTRACT-TOT", 
            FieldType.STRING, 8);
        pnd_Out_Ny_Record_Pnd_Delimiter12 = pnd_Out_Ny_RecordRedef1.newFieldInGroup("pnd_Out_Ny_Record_Pnd_Delimiter12", "#DELIMITER12", FieldType.STRING, 
            1);

        this.setRecordName("LdaTwrl3620");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3620() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
