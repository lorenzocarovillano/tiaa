/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:20 PM
**        * FROM NATURAL LDA     : TWRL116C
************************************************************
**        * FILE NAME            : LdaTwrl116c.java
**        * CLASS NAME           : LdaTwrl116c
**        * INSTANCE NAME        : LdaTwrl116c
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl116c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Twrl116c;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Number;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Type;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Lang;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_City;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Province;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Country;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Zip;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Filler;
    private DbsField pnd_Twrl116c_Pnd_Twrl116c_Rec_Type;

    public DbsGroup getPnd_Twrl116c() { return pnd_Twrl116c; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id() { return pnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code() { return pnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Number() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Number; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Type() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Type; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary() { return pnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Lang() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Lang; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_City() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_City; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Province() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Province; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Country() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Country; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Zip() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Zip; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email() { return pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Filler() { return pnd_Twrl116c_Pnd_Twrl116c_Filler; }

    public DbsField getPnd_Twrl116c_Pnd_Twrl116c_Rec_Type() { return pnd_Twrl116c_Pnd_Twrl116c_Rec_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl116c = newGroupInRecord("pnd_Twrl116c", "#TWRL116C");
        pnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Sub_Ref_Id", "#TWRL116C-SUB-REF-ID", FieldType.STRING, 
            8);
        pnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Rpt_Type_Code", "#TWRL116C-RPT-TYPE-CODE", FieldType.STRING, 
            1);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Number = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Number", "#TWRL116C-TRANS-NUMBER", FieldType.STRING, 
            8);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Type = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Type", "#TWRL116C-TRANS-TYPE", FieldType.NUMERIC, 
            1);
        pnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Num_Of_Summary", "#TWRL116C-NUM-OF-SUMMARY", 
            FieldType.NUMERIC, 6);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Lang = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Lang", "#TWRL116C-TRANS-LANG", FieldType.STRING, 
            1);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1 = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_1", "#TWRL116C-TRANS-NAME-1", FieldType.STRING, 
            30);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2 = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Name_2", "#TWRL116C-TRANS-NAME-2", FieldType.STRING, 
            30);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1 = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_1", "#TWRL116C-TRANS-ADDRESS-1", 
            FieldType.STRING, 30);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2 = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Address_2", "#TWRL116C-TRANS-ADDRESS-2", 
            FieldType.STRING, 30);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_City = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_City", "#TWRL116C-TRANS-CITY", FieldType.STRING, 
            28);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Province = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Province", "#TWRL116C-TRANS-PROVINCE", 
            FieldType.STRING, 2);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Country = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Country", "#TWRL116C-TRANS-COUNTRY", FieldType.STRING, 
            3);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Zip = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Zip", "#TWRL116C-TRANS-ZIP", FieldType.STRING, 
            10);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Contact_Name", "#TWRL116C-TRANS-CONTACT-NAME", 
            FieldType.STRING, 22);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Area_Code", "#TWRL116C-TRANS-CONT-AREA-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Phone", "#TWRL116C-TRANS-CONT-PHONE", 
            FieldType.STRING, 8);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Ext", "#TWRL116C-TRANS-CONT-EXT", 
            FieldType.NUMERIC, 5);
        pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Trans_Cont_Email", "#TWRL116C-TRANS-CONT-EMAIL", 
            FieldType.STRING, 60);
        pnd_Twrl116c_Pnd_Twrl116c_Filler = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Filler", "#TWRL116C-FILLER", FieldType.STRING, 65);
        pnd_Twrl116c_Pnd_Twrl116c_Rec_Type = pnd_Twrl116c.newFieldInGroup("pnd_Twrl116c_Pnd_Twrl116c_Rec_Type", "#TWRL116C-REC-TYPE", FieldType.STRING, 
            1);

        this.setRecordName("LdaTwrl116c");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl116c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
