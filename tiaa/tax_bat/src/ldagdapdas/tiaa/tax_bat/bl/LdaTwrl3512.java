/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:33 PM
**        * FROM NATURAL LDA     : TWRL3512
************************************************************
**        * FILE NAME            : LdaTwrl3512.java
**        * CLASS NAME           : LdaTwrl3512
**        * INSTANCE NAME        : LdaTwrl3512
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3512 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Out1_Irs_Record;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Combine;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Tin;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Control_Code;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Company_Address;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Company_City;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Company_State;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Phone;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank2b;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_State_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Blank5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1a_Crlf;
    private DbsGroup pnd_Out1_Irs_RecordRedef1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Filler_1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Filler_2;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2;
    private DbsField filler01;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Ben;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr;
    private DbsField filler02;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Filler_4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_State_Code;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1k_Blank_Cr_Lf;
    private DbsGroup pnd_Out1_Irs_RecordRedef5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Year;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Corr_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Name_Control;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Tin_Type;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Tin;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Acct;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Office_Cde;
    private DbsField filler03;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt2;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt6;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt7;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt8;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt9;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amta;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtb;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtc;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtd;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amte;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtf;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtg;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Foreign_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_First_Payee_Name;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Sec_Payee_Name;
    private DbsField filler04;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Address;
    private DbsField filler05;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Payee_City;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Payee_State;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Zip;
    private DbsField filler06;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Seq_Num;
    private DbsField filler07;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Dist_Code;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Taxable_Not_Det;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Ira_Sep_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Total_Dist;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Per_Dist;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_1st_Year_Roth_Contrib;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Fatca_Filing_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Date_Of_Payment;
    private DbsField filler08;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7a;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7b;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Tax_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Distrib;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Vt_State_Tax_Id;
    private DbsGroup pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1_Alt;
    private DbsField filler09;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax_Withheld;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data_Entries;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Local_Tax;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1b_Cmb_State_Code;
    private DbsField filler10;
    private DbsGroup pnd_Out1_Irs_RecordRedef9;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer;
    private DbsField filler11;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg;
    private DbsField filler12;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num;
    private DbsField filler13;
    private DbsField filler14;
    private DbsGroup pnd_Out1_Irs_RecordRedef10;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1f_Zero;
    private DbsField filler15;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees;
    private DbsField filler16;
    private DbsField filler17;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num;
    private DbsField filler18;
    private DbsField filler19;
    private DbsGroup pnd_Out1_Irs_RecordRedef11;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Tin;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Control_Code;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Replacement;
    private DbsField filler20;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Test_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Foreign_Ind;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_Address;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_City;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_State;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip;
    private DbsField filler21;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email;
    private DbsField filler22;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num;
    private DbsField filler23;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind;
    private DbsField filler24;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1t_Crlf;
    private DbsGroup pnd_Out1_Irs_RecordRedef12;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7;
    private DbsField pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8;

    public DbsGroup getPnd_Out1_Irs_Record() { return pnd_Out1_Irs_Record; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year() { return pnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Combine() { return pnd_Out1_Irs_Record_Pnd_Out1a_Combine; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Tin() { return pnd_Out1_Irs_Record_Pnd_Out1a_Tin; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Control_Code() { return pnd_Out1_Irs_Record_Pnd_Out1a_Control_Code; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type() { return pnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code() { return pnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank1() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name() { return pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1() { return pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Company_Address() { return pnd_Out1_Irs_Record_Pnd_Out1a_Company_Address; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Company_City() { return pnd_Out1_Irs_Record_Pnd_Out1a_Company_City; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Company_State() { return pnd_Out1_Irs_Record_Pnd_Out1a_Company_State; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip() { return pnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Phone() { return pnd_Out1_Irs_Record_Pnd_Out1a_Phone; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank2b() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank2b; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_State_Id() { return pnd_Out1_Irs_Record_Pnd_Out1a_State_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank3() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank4() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num() { return pnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Blank5() { return pnd_Out1_Irs_Record_Pnd_Out1a_Blank5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1a_Crlf() { return pnd_Out1_Irs_Record_Pnd_Out1a_Crlf; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef1() { return pnd_Out1_Irs_RecordRedef1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees() { return pnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Filler_1() { return pnd_Out1_Irs_Record_Pnd_Out1k_Filler_1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G() { return pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Filler_2() { return pnd_Out1_Irs_Record_Pnd_Out1k_Filler_2; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No() { return pnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Filler_3() { return pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2() { return pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Ia_Ben() { return pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Ben; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr() { return pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A() { return pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3() { return pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld() { return pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A() { return pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4() { return pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld() { return pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Filler_4() { return pnd_Out1_Irs_Record_Pnd_Out1k_Filler_4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_State_Code() { return pnd_Out1_Irs_Record_Pnd_Out1k_State_Code; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1k_Blank_Cr_Lf() { return pnd_Out1_Irs_Record_Pnd_Out1k_Blank_Cr_Lf; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef5() { return pnd_Out1_Irs_RecordRedef5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1b_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Year() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Year; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Corr_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1b_Corr_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Name_Control() { return pnd_Out1_Irs_Record_Pnd_Out1b_Name_Control; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Tin_Type() { return pnd_Out1_Irs_Record_Pnd_Out1b_Tin_Type; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Tin() { return pnd_Out1_Irs_Record_Pnd_Out1b_Tin; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Acct() { return pnd_Out1_Irs_Record_Pnd_Out1b_Acct; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Office_Cde() { return pnd_Out1_Irs_Record_Pnd_Out1b_Office_Cde; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt2() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt2; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt3() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt4() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt5() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt6() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt6; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt7() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt7; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt8() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt8; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt9() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt9; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amta() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amta; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtb() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtb; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtc() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtc; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtd() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtd; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amte() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amte; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtf() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtf; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtg() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtg; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Foreign_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1b_Foreign_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_First_Payee_Name() { return pnd_Out1_Irs_Record_Pnd_Out1b_First_Payee_Name; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Sec_Payee_Name() { return pnd_Out1_Irs_Record_Pnd_Out1b_Sec_Payee_Name; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Payee_Address() { return pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Address; }

    public DbsField getFiller05() { return filler05; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Payee_City() { return pnd_Out1_Irs_Record_Pnd_Out1b_Payee_City; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Payee_State() { return pnd_Out1_Irs_Record_Pnd_Out1b_Payee_State; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Payee_Zip() { return pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Zip; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Seq_Num() { return pnd_Out1_Irs_Record_Pnd_Out1b_Seq_Num; }

    public DbsField getFiller07() { return filler07; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Dist_Code() { return pnd_Out1_Irs_Record_Pnd_Out1b_Dist_Code; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Taxable_Not_Det() { return pnd_Out1_Irs_Record_Pnd_Out1b_Taxable_Not_Det; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Ira_Sep_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1b_Ira_Sep_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Total_Dist() { return pnd_Out1_Irs_Record_Pnd_Out1b_Total_Dist; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Per_Dist() { return pnd_Out1_Irs_Record_Pnd_Out1b_Per_Dist; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_1st_Year_Roth_Contrib() { return pnd_Out1_Irs_Record_Pnd_Out1b_1st_Year_Roth_Contrib; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Fatca_Filing_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1b_Fatca_Filing_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Date_Of_Payment() { return pnd_Out1_Irs_Record_Pnd_Out1b_Date_Of_Payment; }

    public DbsField getFiller08() { return filler08; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Filler_7a() { return pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7a; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Filler_7b() { return pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7b; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Special_Data() { return pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6() { return pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Tax_Id() { return pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Tax_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Distrib() { return pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Distrib; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7() { return pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Vt_State_Tax_Id() { return pnd_Out1_Irs_Record_Pnd_Out1b_Vt_State_Tax_Id; }

    public DbsGroup getPnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8() { return pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1_Alt() { return pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1_Alt; }

    public DbsField getFiller09() { return filler09; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_State_Tax_Withheld() { return pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax_Withheld; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Special_Data_Entries() { return pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data_Entries; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_State_Tax() { return pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Local_Tax() { return pnd_Out1_Irs_Record_Pnd_Out1b_Local_Tax; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1b_Cmb_State_Code() { return pnd_Out1_Irs_Record_Pnd_Out1b_Cmb_State_Code; }

    public DbsField getFiller10() { return filler10; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef9() { return pnd_Out1_Irs_RecordRedef9; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer() { return pnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer; }

    public DbsField getFiller11() { return filler11; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg() { return pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg; }

    public DbsField getFiller12() { return filler12; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num() { return pnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num; }

    public DbsField getFiller13() { return filler13; }

    public DbsField getFiller14() { return filler14; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef10() { return pnd_Out1_Irs_RecordRedef10; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A() { return pnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1f_Zero() { return pnd_Out1_Irs_Record_Pnd_Out1f_Zero; }

    public DbsField getFiller15() { return filler15; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees() { return pnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees; }

    public DbsField getFiller16() { return filler16; }

    public DbsField getFiller17() { return filler17; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num() { return pnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num; }

    public DbsField getFiller18() { return filler18; }

    public DbsField getFiller19() { return filler19; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef11() { return pnd_Out1_Irs_RecordRedef11; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id() { return pnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year() { return pnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Tin() { return pnd_Out1_Irs_Record_Pnd_Out1t_Tin; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Control_Code() { return pnd_Out1_Irs_Record_Pnd_Out1t_Control_Code; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Replacement() { return pnd_Out1_Irs_Record_Pnd_Out1t_Replacement; }

    public DbsField getFiller20() { return filler20; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Test_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1t_Test_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Foreign_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1t_Foreign_Ind; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name() { return pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1() { return pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Name() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Address() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_Address; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_City() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_City; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_State() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_State; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip() { return pnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip; }

    public DbsField getFiller21() { return filler21; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer() { return pnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name() { return pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone() { return pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email() { return pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email; }

    public DbsField getFiller22() { return filler22; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num() { return pnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num; }

    public DbsField getFiller23() { return filler23; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind() { return pnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind; }

    public DbsField getFiller24() { return filler24; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1t_Crlf() { return pnd_Out1_Irs_Record_Pnd_Out1t_Crlf; }

    public DbsGroup getPnd_Out1_Irs_RecordRedef12() { return pnd_Out1_Irs_RecordRedef12; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7; }

    public DbsField getPnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8() { return pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Out1_Irs_Record = newGroupInRecord("pnd_Out1_Irs_Record", "#OUT1-IRS-RECORD");
        pnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Rec_Id", "#OUT1A-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Pay_Year", "#OUT1A-PAY-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Out1_Irs_Record_Pnd_Out1a_Combine = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Combine", "#OUT1A-COMBINE", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank", "#OUT1A-BLANK", FieldType.STRING, 
            5);
        pnd_Out1_Irs_Record_Pnd_Out1a_Tin = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Tin", "#OUT1A-TIN", FieldType.STRING, 9);
        pnd_Out1_Irs_Record_Pnd_Out1a_Control_Code = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Control_Code", "#OUT1A-CONTROL-CODE", 
            FieldType.STRING, 4);
        pnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Filing_Ind", "#OUT1A-FILING-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Ret_Type", "#OUT1A-RET-TYPE", FieldType.STRING, 
            2);
        pnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Amount_Code", "#OUT1A-AMOUNT-CODE", 
            FieldType.STRING, 14);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank1 = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank1", "#OUT1A-BLANK1", FieldType.STRING, 
            10);
        pnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Foreign_Ind", "#OUT1A-FOREIGN-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name", "#OUT1A-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1 = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Name1", "#OUT1A-TRANS-NAME1", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Trans_Ind", "#OUT1A-TRANS-IND", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1a_Company_Address = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Company_Address", "#OUT1A-COMPANY-ADDRESS", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1a_Company_City = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Company_City", "#OUT1A-COMPANY-CITY", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1a_Company_State = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Company_State", "#OUT1A-COMPANY-STATE", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Company_Zip", "#OUT1A-COMPANY-ZIP", 
            FieldType.STRING, 9);
        pnd_Out1_Irs_Record_Pnd_Out1a_Phone = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Phone", "#OUT1A-PHONE", FieldType.STRING, 
            15);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank2b = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank2b", "#OUT1A-BLANK2B", FieldType.STRING, 
            131);
        pnd_Out1_Irs_Record_Pnd_Out1a_State_Id = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_State_Id", "#OUT1A-STATE-ID", FieldType.STRING, 
            10);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank3 = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank3", "#OUT1A-BLANK3", FieldType.STRING, 
            109);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank4 = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank4", "#OUT1A-BLANK4", FieldType.STRING, 
            10);
        pnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Seq_Num", "#OUT1A-SEQ-NUM", FieldType.NUMERIC, 
            8);
        pnd_Out1_Irs_Record_Pnd_Out1a_Blank5 = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Blank5", "#OUT1A-BLANK5", FieldType.STRING, 
            241);
        pnd_Out1_Irs_Record_Pnd_Out1a_Crlf = pnd_Out1_Irs_Record.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1a_Crlf", "#OUT1A-CRLF", FieldType.STRING, 
            2);
        pnd_Out1_Irs_RecordRedef1 = newGroupInRecord("pnd_Out1_Irs_RecordRedef1", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Rec_Id", "#OUT1K-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Number_Of_Payees", "#OUT1K-NUMBER-OF-PAYEES", 
            FieldType.NUMERIC, 8);
        pnd_Out1_Irs_Record_Pnd_Out1k_Filler_1 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Filler_1", "#OUT1K-FILLER-1", 
            FieldType.STRING, 6);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_1", "#OUT1K-CONTROL-TOTAL-1", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_2", "#OUT1K-CONTROL-TOTAL-2", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_3", "#OUT1K-CONTROL-TOTAL-3", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_4", "#OUT1K-CONTROL-TOTAL-4", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_5", "#OUT1K-CONTROL-TOTAL-5", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_6", "#OUT1K-CONTROL-TOTAL-6", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_7", "#OUT1K-CONTROL-TOTAL-7", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_8", "#OUT1K-CONTROL-TOTAL-8", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_9", "#OUT1K-CONTROL-TOTAL-9", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_A", "#OUT1K-CONTROL-TOTAL-A", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_B", "#OUT1K-CONTROL-TOTAL-B", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_C", "#OUT1K-CONTROL-TOTAL-C", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_D", "#OUT1K-CONTROL-TOTAL-D", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_E", "#OUT1K-CONTROL-TOTAL-E", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_F", "#OUT1K-CONTROL-TOTAL-F", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Control_Total_G", "#OUT1K-CONTROL-TOTAL-G", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Filler_2 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Filler_2", "#OUT1K-FILLER-2", 
            FieldType.STRING, 196);
        pnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Record_Sequence_No", 
            "#OUT1K-RECORD-SEQUENCE-NO", FieldType.NUMERIC, 8);
        pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3", "#OUT1K-FILLER-3", 
            FieldType.STRING, 199);
        pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2 = pnd_Out1_Irs_RecordRedef1.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2", "Redefines", 
            pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3);
        filler01 = pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 174);
        pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Ben = pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Ben", "#OUT1K-IA-BEN", 
            FieldType.STRING, 8);
        pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr = pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Ia_Cnf_Nbr", 
            "#OUT1K-IA-CNF-NBR", FieldType.STRING, 10);
        filler02 = pnd_Out1_Irs_Record_Pnd_Out1k_Filler_3Redef2.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 7);
        pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A", 
            "#OUT1K-ST-TAX-WITHHELD-A", FieldType.STRING, 18);
        pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3 = pnd_Out1_Irs_RecordRedef1.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3", 
            "Redefines", pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_A);
        pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld = pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld_ARedef3.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_St_Tax_Withheld", 
            "#OUT1K-ST-TAX-WITHHELD", FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A", 
            "#OUT1K-LC-TAX-WITHHELD-A", FieldType.STRING, 18);
        pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4 = pnd_Out1_Irs_RecordRedef1.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4", 
            "Redefines", pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_A);
        pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld = pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld_ARedef4.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Lc_Tax_Withheld", 
            "#OUT1K-LC-TAX-WITHHELD", FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Filler_4 = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Filler_4", "#OUT1K-FILLER-4", 
            FieldType.STRING, 4);
        pnd_Out1_Irs_Record_Pnd_Out1k_State_Code = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_State_Code", "#OUT1K-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1k_Blank_Cr_Lf = pnd_Out1_Irs_RecordRedef1.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1k_Blank_Cr_Lf", "#OUT1K-BLANK-CR-LF", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_RecordRedef5 = newGroupInRecord("pnd_Out1_Irs_RecordRedef5", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1b_Rec_Id = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Rec_Id", "#OUT1B-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Year = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Year", "#OUT1B-PAY-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Out1_Irs_Record_Pnd_Out1b_Corr_Ind = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Corr_Ind", "#OUT1B-CORR-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Name_Control = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Name_Control", "#OUT1B-NAME-CONTROL", 
            FieldType.STRING, 4);
        pnd_Out1_Irs_Record_Pnd_Out1b_Tin_Type = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Tin_Type", "#OUT1B-TIN-TYPE", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Tin = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Tin", "#OUT1B-TIN", FieldType.STRING, 
            9);
        pnd_Out1_Irs_Record_Pnd_Out1b_Acct = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Acct", "#OUT1B-ACCT", FieldType.STRING, 
            20);
        pnd_Out1_Irs_Record_Pnd_Out1b_Office_Cde = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Office_Cde", "#OUT1B-OFFICE-CDE", 
            FieldType.STRING, 4);
        filler03 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 10);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1", "#OUT1B-PAY-AMT1", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt2 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt2", "#OUT1B-PAY-AMT2", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt3 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt3", "#OUT1B-PAY-AMT3", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt4 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt4", "#OUT1B-PAY-AMT4", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt5 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt5", "#OUT1B-PAY-AMT5", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt6 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt6", "#OUT1B-PAY-AMT6", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt7 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt7", "#OUT1B-PAY-AMT7", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt8 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt8", "#OUT1B-PAY-AMT8", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt9 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt9", "#OUT1B-PAY-AMT9", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amta = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amta", "#OUT1B-PAY-AMTA", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtb = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtb", "#OUT1B-PAY-AMTB", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtc = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtc", "#OUT1B-PAY-AMTC", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtd = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtd", "#OUT1B-PAY-AMTD", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amte = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amte", "#OUT1B-PAY-AMTE", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtf = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtf", "#OUT1B-PAY-AMTF", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtg = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amtg", "#OUT1B-PAY-AMTG", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Foreign_Ind = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Foreign_Ind", "#OUT1B-FOREIGN-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_First_Payee_Name = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_First_Payee_Name", "#OUT1B-FIRST-PAYEE-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1b_Sec_Payee_Name = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Sec_Payee_Name", "#OUT1B-SEC-PAYEE-NAME", 
            FieldType.STRING, 40);
        filler04 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Address = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Address", "#OUT1B-PAYEE-ADDRESS", 
            FieldType.STRING, 40);
        filler05 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1b_Payee_City = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Payee_City", "#OUT1B-PAYEE-CITY", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1b_Payee_State = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Payee_State", "#OUT1B-PAYEE-STATE", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Zip = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Payee_Zip", "#OUT1B-PAYEE-ZIP", 
            FieldType.STRING, 9);
        filler06 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler06", "FILLER", FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Seq_Num = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Seq_Num", "#OUT1B-SEQ-NUM", FieldType.NUMERIC, 
            8);
        filler07 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler07", "FILLER", FieldType.STRING, 37);
        pnd_Out1_Irs_Record_Pnd_Out1b_Dist_Code = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Dist_Code", "#OUT1B-DIST-CODE", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Taxable_Not_Det = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Taxable_Not_Det", "#OUT1B-TAXABLE-NOT-DET", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Ira_Sep_Ind = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Ira_Sep_Ind", "#OUT1B-IRA-SEP-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Total_Dist = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Total_Dist", "#OUT1B-TOTAL-DIST", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Per_Dist = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Per_Dist", "#OUT1B-PER-DIST", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1b_1st_Year_Roth_Contrib = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_1st_Year_Roth_Contrib", 
            "#OUT1B-1ST-YEAR-ROTH-CONTRIB", FieldType.STRING, 4);
        pnd_Out1_Irs_Record_Pnd_Out1b_Fatca_Filing_Ind = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Fatca_Filing_Ind", "#OUT1B-FATCA-FILING-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1b_Date_Of_Payment = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Date_Of_Payment", "#OUT1B-DATE-OF-PAYMENT", 
            FieldType.STRING, 8);
        filler08 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler08", "FILLER", FieldType.STRING, 22);
        pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7a = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7a", "#OUT1B-FILLER-7A", 
            FieldType.STRING, 13);
        pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7b = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Filler_7b", "#OUT1B-FILLER-7B", 
            FieldType.STRING, 63);
        pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data", "#OUT1B-SPECIAL-DATA", 
            FieldType.STRING, 60);
        pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6 = pnd_Out1_Irs_RecordRedef5.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6", 
            "Redefines", pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data);
        pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Ind = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Ind", 
            "#OUT1B-UT-STATE-IND", FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Tax_Id = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Tax_Id", 
            "#OUT1B-UT-STATE-TAX-ID", FieldType.STRING, 14);
        pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Distrib = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef6.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Ut_State_Distrib", 
            "#OUT1B-UT-STATE-DISTRIB", FieldType.DECIMAL, 13,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7 = pnd_Out1_Irs_RecordRedef5.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7", 
            "Redefines", pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data);
        pnd_Out1_Irs_Record_Pnd_Out1b_Vt_State_Tax_Id = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef7.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Vt_State_Tax_Id", 
            "#OUT1B-VT-STATE-TAX-ID", FieldType.STRING, 15);
        pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8 = pnd_Out1_Irs_RecordRedef5.newGroupInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8", 
            "Redefines", pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data);
        pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1_Alt = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Pay_Amt1_Alt", 
            "#OUT1B-PAY-AMT1-ALT", FieldType.DECIMAL, 10,2);
        filler09 = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8.newFieldInGroup("filler09", "FILLER", FieldType.STRING, 10);
        pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax_Withheld = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax_Withheld", 
            "#OUT1B-STATE-TAX-WITHHELD", FieldType.DECIMAL, 10,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data_Entries = pnd_Out1_Irs_Record_Pnd_Out1b_Special_DataRedef8.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Special_Data_Entries", 
            "#OUT1B-SPECIAL-DATA-ENTRIES", FieldType.STRING, 30);
        pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_State_Tax", "#OUT1B-STATE-TAX", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Local_Tax = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Local_Tax", "#OUT1B-LOCAL-TAX", 
            FieldType.DECIMAL, 12,2);
        pnd_Out1_Irs_Record_Pnd_Out1b_Cmb_State_Code = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1b_Cmb_State_Code", "#OUT1B-CMB-STATE-CODE", 
            FieldType.STRING, 2);
        filler10 = pnd_Out1_Irs_RecordRedef5.newFieldInGroup("filler10", "FILLER", FieldType.STRING, 2);
        pnd_Out1_Irs_RecordRedef9 = newGroupInRecord("pnd_Out1_Irs_RecordRedef9", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Rec_Id", "#OUT1C-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_No_Of_Payer", "#OUT1C-NO-OF-PAYER", 
            FieldType.NUMERIC, 8);
        filler11 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("filler11", "FILLER", FieldType.STRING, 6);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt1", "#OUT1C-PAY-AMT1", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt2", "#OUT1C-PAY-AMT2", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt3", "#OUT1C-PAY-AMT3", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt4", "#OUT1C-PAY-AMT4", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt5", "#OUT1C-PAY-AMT5", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt6", "#OUT1C-PAY-AMT6", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt7", "#OUT1C-PAY-AMT7", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt8", "#OUT1C-PAY-AMT8", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amt9", "#OUT1C-PAY-AMT9", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amta", "#OUT1C-PAY-AMTA", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtb", "#OUT1C-PAY-AMTB", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtc", "#OUT1C-PAY-AMTC", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtd", "#OUT1C-PAY-AMTD", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amte", "#OUT1C-PAY-AMTE", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtf", "#OUT1C-PAY-AMTF", 
            FieldType.DECIMAL, 18,2);
        pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Pay_Amtg", "#OUT1C-PAY-AMTG", 
            FieldType.DECIMAL, 18,2);
        filler12 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("filler12", "FILLER", FieldType.STRING, 196);
        pnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1c_Seq_Num", "#OUT1C-SEQ-NUM", FieldType.NUMERIC, 
            8);
        filler13 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("filler13", "FILLER", FieldType.STRING, 241);
        filler14 = pnd_Out1_Irs_RecordRedef9.newFieldInGroup("filler14", "FILLER", FieldType.STRING, 2);
        pnd_Out1_Irs_RecordRedef10 = newGroupInRecord("pnd_Out1_Irs_RecordRedef10", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1f_Rec_Id", "#OUT1F-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1f_Num_Of_A", "#OUT1F-NUM-OF-A", 
            FieldType.NUMERIC, 8);
        pnd_Out1_Irs_Record_Pnd_Out1f_Zero = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1f_Zero", "#OUT1F-ZERO", FieldType.STRING, 
            21);
        filler15 = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("filler15", "FILLER", FieldType.STRING, 19);
        pnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1f_Tot_Payees", "#OUT1F-TOT-PAYEES", 
            FieldType.NUMERIC, 8);
        filler16 = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("filler16", "FILLER", FieldType.STRING, 200);
        filler17 = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("filler17", "FILLER", FieldType.STRING, 242);
        pnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1f_Seq_Num", "#OUT1F-SEQ-NUM", 
            FieldType.NUMERIC, 8);
        filler18 = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("filler18", "FILLER", FieldType.STRING, 241);
        filler19 = pnd_Out1_Irs_RecordRedef10.newFieldInGroup("filler19", "FILLER", FieldType.STRING, 2);
        pnd_Out1_Irs_RecordRedef11 = newGroupInRecord("pnd_Out1_Irs_RecordRedef11", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Rec_Id", "#OUT1T-REC-ID", FieldType.STRING, 
            1);
        pnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Pay_Year", "#OUT1T-PAY-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Prior_Year_Ind", "#OUT1T-PRIOR-YEAR-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1t_Tin = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Tin", "#OUT1T-TIN", FieldType.STRING, 
            9);
        pnd_Out1_Irs_Record_Pnd_Out1t_Control_Code = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Control_Code", "#OUT1T-CONTROL-CODE", 
            FieldType.STRING, 5);
        pnd_Out1_Irs_Record_Pnd_Out1t_Replacement = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Replacement", "#OUT1T-REPLACEMENT", 
            FieldType.STRING, 2);
        filler20 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("filler20", "FILLER", FieldType.STRING, 5);
        pnd_Out1_Irs_Record_Pnd_Out1t_Test_Ind = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Test_Ind", "#OUT1T-TEST-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1t_Foreign_Ind = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Foreign_Ind", "#OUT1T-FOREIGN-IND", 
            FieldType.STRING, 1);
        pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name", "#OUT1T-TRANS-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Trans_Name1", "#OUT1T-TRANS-NAME1", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name", "#OUT1T-COMPANY-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_Name1", "#OUT1T-COMPANY-NAME1", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_Address = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_Address", "#OUT1T-COMPANY-ADDRESS", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_City = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_City", "#OUT1T-COMPANY-CITY", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_State = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_State", "#OUT1T-COMPANY-STATE", 
            FieldType.STRING, 2);
        pnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Company_Zip", "#OUT1T-COMPANY-ZIP", 
            FieldType.STRING, 9);
        filler21 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("filler21", "FILLER", FieldType.STRING, 15);
        pnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Tot_Payer", "#OUT1T-TOT-PAYER", 
            FieldType.NUMERIC, 8);
        pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Name", "#OUT1T-CONTACT-NAME", 
            FieldType.STRING, 40);
        pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Phone", "#OUT1T-CONTACT-PHONE", 
            FieldType.STRING, 15);
        pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Contact_Email", "#OUT1T-CONTACT-EMAIL", 
            FieldType.STRING, 50);
        filler22 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("filler22", "FILLER", FieldType.STRING, 91);
        pnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Seq_Num", "#OUT1T-SEQ-NUM", 
            FieldType.NUMERIC, 8);
        filler23 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("filler23", "FILLER", FieldType.STRING, 10);
        pnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Vendor_Ind", "#OUT1T-VENDOR-IND", 
            FieldType.STRING, 1);
        filler24 = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("filler24", "FILLER", FieldType.STRING, 230);
        pnd_Out1_Irs_Record_Pnd_Out1t_Crlf = pnd_Out1_Irs_RecordRedef11.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1t_Crlf", "#OUT1T-CRLF", FieldType.STRING, 
            2);
        pnd_Out1_Irs_RecordRedef12 = newGroupInRecord("pnd_Out1_Irs_RecordRedef12", "Redefines", pnd_Out1_Irs_Record);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec1", "#OUT1-IRS-REC1", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec2", "#OUT1-IRS-REC2", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec3", "#OUT1-IRS-REC3", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec4", "#OUT1-IRS-REC4", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec5", "#OUT1-IRS-REC5", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec6", "#OUT1-IRS-REC6", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec7", "#OUT1-IRS-REC7", 
            FieldType.STRING, 100);
        pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8 = pnd_Out1_Irs_RecordRedef12.newFieldInGroup("pnd_Out1_Irs_Record_Pnd_Out1_Irs_Rec8", "#OUT1-IRS-REC8", 
            FieldType.STRING, 50);

        this.setRecordName("LdaTwrl3512");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3512() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
