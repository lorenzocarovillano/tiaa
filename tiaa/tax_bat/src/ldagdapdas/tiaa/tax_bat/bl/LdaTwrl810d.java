/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:22 PM
**        * FROM NATURAL LDA     : TWRL810D
************************************************************
**        * FILE NAME            : LdaTwrl810d.java
**        * CLASS NAME           : LdaTwrl810d
**        * INSTANCE NAME        : LdaTwrl810d
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl810d extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_updpar;
    private DbsField updpar_Twrparti_Status;
    private DbsField updpar_Twrparti_Updt_User;
    private DbsField updpar_Twrparti_Updt_Dte;
    private DbsField updpar_Twrparti_Updt_Time;
    private DbsField updpar_Twrparti_Lu_Ts;
    private DbsField updpar_Twrparti_Addr_Source;
    private DbsField updpar_Twrparti_Origin;
    private DbsField updpar_Twrparti_Pin;
    private DbsField updpar_Twrparti_Participant_Name;
    private DbsField updpar_Twrparti_Addr_Ln1;
    private DbsField updpar_Twrparti_Addr_Ln2;
    private DbsField updpar_Twrparti_Addr_Ln3;
    private DbsField updpar_Twrparti_Addr_Ln4;
    private DbsField updpar_Twrparti_Addr_Ln5;
    private DbsField updpar_Twrparti_Addr_Ln6;
    private DbsField updpar_Twrparti_Participant_Dob;
    private DbsField updpar_Twrparti_Participant_Dod;

    public DataAccessProgramView getVw_updpar() { return vw_updpar; }

    public DbsField getUpdpar_Twrparti_Status() { return updpar_Twrparti_Status; }

    public DbsField getUpdpar_Twrparti_Updt_User() { return updpar_Twrparti_Updt_User; }

    public DbsField getUpdpar_Twrparti_Updt_Dte() { return updpar_Twrparti_Updt_Dte; }

    public DbsField getUpdpar_Twrparti_Updt_Time() { return updpar_Twrparti_Updt_Time; }

    public DbsField getUpdpar_Twrparti_Lu_Ts() { return updpar_Twrparti_Lu_Ts; }

    public DbsField getUpdpar_Twrparti_Addr_Source() { return updpar_Twrparti_Addr_Source; }

    public DbsField getUpdpar_Twrparti_Origin() { return updpar_Twrparti_Origin; }

    public DbsField getUpdpar_Twrparti_Pin() { return updpar_Twrparti_Pin; }

    public DbsField getUpdpar_Twrparti_Participant_Name() { return updpar_Twrparti_Participant_Name; }

    public DbsField getUpdpar_Twrparti_Addr_Ln1() { return updpar_Twrparti_Addr_Ln1; }

    public DbsField getUpdpar_Twrparti_Addr_Ln2() { return updpar_Twrparti_Addr_Ln2; }

    public DbsField getUpdpar_Twrparti_Addr_Ln3() { return updpar_Twrparti_Addr_Ln3; }

    public DbsField getUpdpar_Twrparti_Addr_Ln4() { return updpar_Twrparti_Addr_Ln4; }

    public DbsField getUpdpar_Twrparti_Addr_Ln5() { return updpar_Twrparti_Addr_Ln5; }

    public DbsField getUpdpar_Twrparti_Addr_Ln6() { return updpar_Twrparti_Addr_Ln6; }

    public DbsField getUpdpar_Twrparti_Participant_Dob() { return updpar_Twrparti_Participant_Dob; }

    public DbsField getUpdpar_Twrparti_Participant_Dod() { return updpar_Twrparti_Participant_Dod; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_updpar = new DataAccessProgramView(new NameInfo("vw_updpar", "UPDPAR"), "TWRPARTI_PARTICIPANT_FILE", "TWR_PARTICIPANT");
        updpar_Twrparti_Status = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Status", "TWRPARTI-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TWRPARTI_STATUS");
        updpar_Twrparti_Updt_User = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Updt_User", "TWRPARTI-UPDT-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_USER");
        updpar_Twrparti_Updt_Dte = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Updt_Dte", "TWRPARTI-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_DTE");
        updpar_Twrparti_Updt_Time = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Updt_Time", "TWRPARTI-UPDT-TIME", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "TWRPARTI_UPDT_TIME");
        updpar_Twrparti_Lu_Ts = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Lu_Ts", "TWRPARTI-LU-TS", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TWRPARTI_LU_TS");
        updpar_Twrparti_Addr_Source = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Source", "TWRPARTI-ADDR-SOURCE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TWRPARTI_ADDR_SOURCE");
        updpar_Twrparti_Origin = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Origin", "TWRPARTI-ORIGIN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "TWRPARTI_ORIGIN");
        updpar_Twrparti_Pin = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Pin", "TWRPARTI-PIN", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "TWRPARTI_PIN");
        updpar_Twrparti_Participant_Name = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Participant_Name", "TWRPARTI-PARTICIPANT-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_NAME");
        updpar_Twrparti_Addr_Ln1 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln1", "TWRPARTI-ADDR-LN1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN1");
        updpar_Twrparti_Addr_Ln2 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln2", "TWRPARTI-ADDR-LN2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN2");
        updpar_Twrparti_Addr_Ln3 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln3", "TWRPARTI-ADDR-LN3", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN3");
        updpar_Twrparti_Addr_Ln4 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln4", "TWRPARTI-ADDR-LN4", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN4");
        updpar_Twrparti_Addr_Ln5 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln5", "TWRPARTI-ADDR-LN5", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN5");
        updpar_Twrparti_Addr_Ln6 = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Addr_Ln6", "TWRPARTI-ADDR-LN6", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "TWRPARTI_ADDR_LN6");
        updpar_Twrparti_Participant_Dob = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Participant_Dob", "TWRPARTI-PARTICIPANT-DOB", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOB");
        updpar_Twrparti_Participant_Dod = vw_updpar.getRecord().newFieldInGroup("updpar_Twrparti_Participant_Dod", "TWRPARTI-PARTICIPANT-DOD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TWRPARTI_PARTICIPANT_DOD");

        this.setRecordName("LdaTwrl810d");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_updpar.reset();
    }

    // Constructor
    public LdaTwrl810d() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
