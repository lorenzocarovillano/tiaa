/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:15:50 PM
**        * FROM NATURAL LDA     : TXWL2900
************************************************************
**        * FILE NAME            : LdaTxwl2900.java
**        * CLASS NAME           : LdaTxwl2900
**        * INSTANCE NAME        : LdaTxwl2900
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTxwl2900 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ref_Tab;
    private DbsGroup ref_Tab_Rt_Record;
    private DbsField ref_Tab_Rt_A_I_Ind;
    private DbsField ref_Tab_Rt_Table_Id;
    private DbsField ref_Tab_Rt_Short_Key;
    private DbsField ref_Tab_Rt_Long_Key;
    private DbsGroup ref_Tab_Rt_Long_KeyRedef1;
    private DbsField ref_Tab_Rt_Inverse_Lu_Ts;
    private DbsField ref_Tab_Rt_Desc5;
    private DbsGroup ref_Tab_Rt_Desc5Redef2;
    private DbsField ref_Tab_Rt_Inverse_Effective_Ts;
    private DbsField ref_Tab_Rt_Effective_Ts;
    private DbsField ref_Tab_Rt_Create_User_Id;
    private DbsField ref_Tab_Rt_Create_Ts;
    private DbsField ref_Tab_Rt_Lu_User_Id;
    private DbsField ref_Tab_Rt_Lu_Ts;
    private DbsField ref_Tab_Rt_Ia_Cutoff2_Ts;

    public DataAccessProgramView getVw_ref_Tab() { return vw_ref_Tab; }

    public DbsGroup getRef_Tab_Rt_Record() { return ref_Tab_Rt_Record; }

    public DbsField getRef_Tab_Rt_A_I_Ind() { return ref_Tab_Rt_A_I_Ind; }

    public DbsField getRef_Tab_Rt_Table_Id() { return ref_Tab_Rt_Table_Id; }

    public DbsField getRef_Tab_Rt_Short_Key() { return ref_Tab_Rt_Short_Key; }

    public DbsField getRef_Tab_Rt_Long_Key() { return ref_Tab_Rt_Long_Key; }

    public DbsGroup getRef_Tab_Rt_Long_KeyRedef1() { return ref_Tab_Rt_Long_KeyRedef1; }

    public DbsField getRef_Tab_Rt_Inverse_Lu_Ts() { return ref_Tab_Rt_Inverse_Lu_Ts; }

    public DbsField getRef_Tab_Rt_Desc5() { return ref_Tab_Rt_Desc5; }

    public DbsGroup getRef_Tab_Rt_Desc5Redef2() { return ref_Tab_Rt_Desc5Redef2; }

    public DbsField getRef_Tab_Rt_Inverse_Effective_Ts() { return ref_Tab_Rt_Inverse_Effective_Ts; }

    public DbsField getRef_Tab_Rt_Effective_Ts() { return ref_Tab_Rt_Effective_Ts; }

    public DbsField getRef_Tab_Rt_Create_User_Id() { return ref_Tab_Rt_Create_User_Id; }

    public DbsField getRef_Tab_Rt_Create_Ts() { return ref_Tab_Rt_Create_Ts; }

    public DbsField getRef_Tab_Rt_Lu_User_Id() { return ref_Tab_Rt_Lu_User_Id; }

    public DbsField getRef_Tab_Rt_Lu_Ts() { return ref_Tab_Rt_Lu_Ts; }

    public DbsField getRef_Tab_Rt_Ia_Cutoff2_Ts() { return ref_Tab_Rt_Ia_Cutoff2_Ts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ref_Tab = new DataAccessProgramView(new NameInfo("vw_ref_Tab", "REF-TAB"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ref_Tab_Rt_Record = vw_ref_Tab.getRecord().newGroupInGroup("ref_Tab_Rt_Record", "RT-RECORD");
        ref_Tab_Rt_A_I_Ind = ref_Tab_Rt_Record.newFieldInGroup("ref_Tab_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Tab_Rt_Table_Id = ref_Tab_Rt_Record.newFieldInGroup("ref_Tab_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        ref_Tab_Rt_Short_Key = ref_Tab_Rt_Record.newFieldInGroup("ref_Tab_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        ref_Tab_Rt_Long_Key = ref_Tab_Rt_Record.newFieldInGroup("ref_Tab_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        ref_Tab_Rt_Long_KeyRedef1 = vw_ref_Tab.getRecord().newGroupInGroup("ref_Tab_Rt_Long_KeyRedef1", "Redefines", ref_Tab_Rt_Long_Key);
        ref_Tab_Rt_Inverse_Lu_Ts = ref_Tab_Rt_Long_KeyRedef1.newFieldInGroup("ref_Tab_Rt_Inverse_Lu_Ts", "RT-INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 
            13);
        ref_Tab_Rt_Desc5 = ref_Tab_Rt_Record.newFieldInGroup("ref_Tab_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ref_Tab_Rt_Desc5Redef2 = vw_ref_Tab.getRecord().newGroupInGroup("ref_Tab_Rt_Desc5Redef2", "Redefines", ref_Tab_Rt_Desc5);
        ref_Tab_Rt_Inverse_Effective_Ts = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Inverse_Effective_Ts", "RT-INVERSE-EFFECTIVE-TS", FieldType.PACKED_DECIMAL, 
            13);
        ref_Tab_Rt_Effective_Ts = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Effective_Ts", "RT-EFFECTIVE-TS", FieldType.TIME);
        ref_Tab_Rt_Create_User_Id = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Create_User_Id", "RT-CREATE-USER-ID", FieldType.STRING, 8);
        ref_Tab_Rt_Create_Ts = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Create_Ts", "RT-CREATE-TS", FieldType.TIME);
        ref_Tab_Rt_Lu_User_Id = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Lu_User_Id", "RT-LU-USER-ID", FieldType.STRING, 8);
        ref_Tab_Rt_Lu_Ts = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Lu_Ts", "RT-LU-TS", FieldType.TIME);
        ref_Tab_Rt_Ia_Cutoff2_Ts = ref_Tab_Rt_Desc5Redef2.newFieldInGroup("ref_Tab_Rt_Ia_Cutoff2_Ts", "RT-IA-CUTOFF2-TS", FieldType.TIME);

        this.setRecordName("LdaTxwl2900");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ref_Tab.reset();
    }

    // Constructor
    public LdaTxwl2900() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
