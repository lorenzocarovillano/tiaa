/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:30 PM
**        * FROM NATURAL PDA     : TWRA0214
************************************************************
**        * FILE NAME            : PdaTwra0214.java
**        * CLASS NAME           : PdaTwra0214
**        * INSTANCE NAME        : PdaTwra0214
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwra0214 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Twra0214_Link_Area;
    private DbsGroup pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Pin_Nbr;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Systm_Id_Cde;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Pckge_Cde;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Pckge_Dlvry_Typ;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Prntr_Id_Cde;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Dont_Use_Finalist;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Lines_Rule;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Full_Nme;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Last_Nme;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Typ_Cde;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Letter_Dte;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Next_Bsnss_Day_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Lttr_Slttn_Txt;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Empl_Sgntry_Nme;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Empl_Unit_Work_Nme;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Print_Fclty_Cde;
    private DbsField pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Mit_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Efm_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Form_Lbrry_Id;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_1;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_2;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_3;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_4;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_5;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Line_6;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Addrss_Zp9_Nbr;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Rqst_Log_Dte_Tme;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Mit_Status;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Pckge_Image_Ind;
    private DbsGroup pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Isn;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Payer_Info;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Recipien_Info;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Print_Inactive;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Counter;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_User_Name;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Ati;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Tin;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key;
    private DbsField pnd_Twra0214_Link_Area_Pnd_Twra0214_Reporting_Type;

    public DbsGroup getPnd_Twra0214_Link_Area() { return pnd_Twra0214_Link_Area; }

    public DbsGroup getPnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind() { return pnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Pin_Nbr() { return pnd_Twra0214_Link_Area_Pnd_Pin_Nbr; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ() { return pnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Systm_Id_Cde() { return pnd_Twra0214_Link_Area_Pnd_Systm_Id_Cde; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Pckge_Cde() { return pnd_Twra0214_Link_Area_Pnd_Pckge_Cde; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Pckge_Dlvry_Typ() { return pnd_Twra0214_Link_Area_Pnd_Pckge_Dlvry_Typ; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Prntr_Id_Cde() { return pnd_Twra0214_Link_Area_Pnd_Prntr_Id_Cde; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Dont_Use_Finalist() { return pnd_Twra0214_Link_Area_Pnd_Dont_Use_Finalist; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Lines_Rule() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Lines_Rule; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Full_Nme() { return pnd_Twra0214_Link_Area_Pnd_Full_Nme; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Last_Nme() { return pnd_Twra0214_Link_Area_Pnd_Last_Nme; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Typ_Cde() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Typ_Cde; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Letter_Dte() { return pnd_Twra0214_Link_Area_Pnd_Letter_Dte; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Next_Bsnss_Day_Ind() { return pnd_Twra0214_Link_Area_Pnd_Next_Bsnss_Day_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Lttr_Slttn_Txt() { return pnd_Twra0214_Link_Area_Pnd_Lttr_Slttn_Txt; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Empl_Sgntry_Nme() { return pnd_Twra0214_Link_Area_Pnd_Empl_Sgntry_Nme; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Empl_Unit_Work_Nme() { return pnd_Twra0214_Link_Area_Pnd_Empl_Unit_Work_Nme; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Print_Fclty_Cde() { return pnd_Twra0214_Link_Area_Pnd_Print_Fclty_Cde; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_No_Updte_To_Mit_Ind() { return pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Mit_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_No_Updte_To_Efm_Ind() { return pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Efm_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Form_Lbrry_Id() { return pnd_Twra0214_Link_Area_Pnd_Form_Lbrry_Id; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_1() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_1; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_2() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_2; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_3() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_3; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_4() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_4; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_5() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_5; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Line_6() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Line_6; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Addrss_Zp9_Nbr() { return pnd_Twra0214_Link_Area_Pnd_Addrss_Zp9_Nbr; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Ind() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Rqst_Log_Dte_Tme() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mit_Status() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Mit_Status; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Pckge_Image_Ind() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Pckge_Image_Ind; }

    public DbsGroup getPnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Isn() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Isn; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Payer_Info() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Payer_Info; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Recipien_Info() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Recipien_Info; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Print_Inactive() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Print_Inactive; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Counter() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Counter; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_User_Name() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_User_Name; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Ati() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Ati; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Tin() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Tin; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key; }

    public DbsField getPnd_Twra0214_Link_Area_Pnd_Twra0214_Reporting_Type() { return pnd_Twra0214_Link_Area_Pnd_Twra0214_Reporting_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twra0214_Link_Area = dbsRecord.newGroupInRecord("pnd_Twra0214_Link_Area", "#TWRA0214-LINK-AREA");
        pnd_Twra0214_Link_Area.setParameterOption(ParameterOption.ByReference);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area = pnd_Twra0214_Link_Area.newGroupInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area", 
            "#TWRA0214-OPEN-CLOSE-AREA");
        pnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Pckge_Immdte_Prnt_Ind", 
            "#PCKGE-IMMDTE-PRNT-IND", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Pin_Nbr = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Pin_Nbr", 
            "#PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Univ_Id_Typ", 
            "#UNIV-ID-TYP", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Systm_Id_Cde = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Systm_Id_Cde", 
            "#SYSTM-ID-CDE", FieldType.STRING, 8);
        pnd_Twra0214_Link_Area_Pnd_Pckge_Cde = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Pckge_Cde", 
            "#PCKGE-CDE", FieldType.STRING, 8);
        pnd_Twra0214_Link_Area_Pnd_Pckge_Dlvry_Typ = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Pckge_Dlvry_Typ", 
            "#PCKGE-DLVRY-TYP", FieldType.STRING, 3);
        pnd_Twra0214_Link_Area_Pnd_Prntr_Id_Cde = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Prntr_Id_Cde", 
            "#PRNTR-ID-CDE", FieldType.STRING, 8);
        pnd_Twra0214_Link_Area_Pnd_Dont_Use_Finalist = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Dont_Use_Finalist", 
            "#DONT-USE-FINALIST", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Lines_Rule = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Lines_Rule", 
            "#ADDRSS-LINES-RULE", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_Full_Nme = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Full_Nme", 
            "#FULL-NME", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Last_Nme = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Last_Nme", 
            "#LAST-NME", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Typ_Cde = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Typ_Cde", 
            "#ADDRSS-TYP-CDE", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Letter_Dte = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Letter_Dte", 
            "#LETTER-DTE", FieldType.DATE);
        pnd_Twra0214_Link_Area_Pnd_Next_Bsnss_Day_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Next_Bsnss_Day_Ind", 
            "#NEXT-BSNSS-DAY-IND", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_Lttr_Slttn_Txt = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Lttr_Slttn_Txt", 
            "#LTTR-SLTTN-TXT", FieldType.STRING, 40);
        pnd_Twra0214_Link_Area_Pnd_Empl_Sgntry_Nme = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Empl_Sgntry_Nme", 
            "#EMPL-SGNTRY-NME", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Empl_Unit_Work_Nme = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Empl_Unit_Work_Nme", 
            "#EMPL-UNIT-WORK-NME", FieldType.STRING, 45);
        pnd_Twra0214_Link_Area_Pnd_Print_Fclty_Cde = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Print_Fclty_Cde", 
            "#PRINT-FCLTY-CDE", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Mit_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Mit_Ind", 
            "#NO-UPDTE-TO-MIT-IND", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Efm_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_No_Updte_To_Efm_Ind", 
            "#NO-UPDTE-TO-EFM-IND", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_Form_Lbrry_Id = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Form_Lbrry_Id", 
            "#FORM-LBRRY-ID", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_1 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_1", 
            "#ADDRSS-LINE-1", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_2 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_2", 
            "#ADDRSS-LINE-2", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_3 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_3", 
            "#ADDRSS-LINE-3", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_4 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_4", 
            "#ADDRSS-LINE-4", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_5 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_5", 
            "#ADDRSS-LINE-5", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Line_6 = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Line_6", 
            "#ADDRSS-LINE-6", FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Addrss_Zp9_Nbr = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Addrss_Zp9_Nbr", 
            "#ADDRSS-ZP9-NBR", FieldType.STRING, 9);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Ind", 
            "#TWRA0214-OPEN-CLOSE-IND", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Rqst_Log_Dte_Tme = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Rqst_Log_Dte_Tme", 
            "#TWRA0214-RQST-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Mit_Status = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Mit_Status", 
            "#TWRA0214-MIT-STATUS", FieldType.STRING, 4);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Pckge_Image_Ind = pnd_Twra0214_Link_Area_Pnd_Twra0214_Open_Close_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Pckge_Image_Ind", 
            "#TWRA0214-PCKGE-IMAGE-IND", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area = pnd_Twra0214_Link_Area.newGroupInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area", "#TWRA0214-DATA-AREA");
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Isn = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Isn", 
            "#TWRA0214-ISN", FieldType.NUMERIC, 8);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Payer_Info = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldArrayInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Payer_Info", 
            "#TWRA0214-PAYER-INFO", FieldType.STRING, 35, new DbsArrayController(1,6));
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Recipien_Info = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldArrayInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Recipien_Info", 
            "#TWRA0214-RECIPIEN-INFO", FieldType.STRING, 35, new DbsArrayController(1,6));
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Print_Inactive = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Print_Inactive", 
            "#TWRA0214-PRINT-INACTIVE", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Hard_Copy", 
            "#TWRA0214-HARD-COPY", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Counter = pnd_Twra0214_Link_Area_Pnd_Twra0214_Data_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Counter", 
            "#TWRA0214-FORM-COUNTER", FieldType.PACKED_DECIMAL, 3);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Tax_Year", "#TWRA0214-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Online_Batch", 
            "#TWRA0214-ONLINE-BATCH", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Accept_Inactive", 
            "#TWRA0214-ACCEPT-INACTIVE", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Code", "#TWRA0214-RET-CODE", 
            FieldType.NUMERIC, 2);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_User_Name = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_User_Name", "#TWRA0214-USER-NAME", 
            FieldType.STRING, 30);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Ret_Msg", "#TWRA0214-RET-MSG", 
            FieldType.STRING, 70);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Ati = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Ati", "#TWRA0214-ATI", FieldType.BOOLEAN);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Tin = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Tin", "#TWRA0214-TIN", FieldType.STRING, 
            10);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Form_Type", "#TWRA0214-FORM-TYPE", 
            FieldType.NUMERIC, 2);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Migr_Ind", 
            "#TWRA0214-MOBIUS-MIGR-IND", FieldType.STRING, 1);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Mobius_Key", "#TWRA0214-MOBIUS-KEY", 
            FieldType.STRING, 35);
        pnd_Twra0214_Link_Area_Pnd_Twra0214_Reporting_Type = pnd_Twra0214_Link_Area.newFieldInGroup("pnd_Twra0214_Link_Area_Pnd_Twra0214_Reporting_Type", 
            "#TWRA0214-REPORTING-TYPE", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwra0214(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

