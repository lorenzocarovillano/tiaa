/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:32 PM
**        * FROM NATURAL PDA     : TWRA105
************************************************************
**        * FILE NAME            : PdaTwra105.java
**        * CLASS NAME           : PdaTwra105
**        * INSTANCE NAME        : PdaTwra105
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaTwra105 extends PdaBase
{
    // Properties
    private DbsGroup pnd_P_Vars;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Payee_Cde;
    private DbsGroup pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N;
    private DbsField pnd_P_Vars_Pnd_Pin;
    private DbsField pnd_P_Vars_Pnd_Ph_Data_Rcd_Type;
    private DbsField pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type;
    private DbsField pnd_P_Vars_Pnd_Ssn;
    private DbsField pnd_P_Vars_Pnd_Dob_Ccyymmdd;
    private DbsField pnd_P_Vars_Pnd_Dod_Ccyymmdd;
    private DbsField pnd_P_Vars_Pnd_Get_Pin;
    private DbsField pnd_P_Vars_Pnd_Get_Ssn_Level_Info;

    public DbsGroup getPnd_P_Vars() { return pnd_P_Vars; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr() { return pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Payee_Cde() { return pnd_P_Vars_Pnd_Cntrct_Payee_Cde; }

    public DbsGroup getPnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1() { return pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Payee_Cde_N() { return pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N; }

    public DbsField getPnd_P_Vars_Pnd_Pin() { return pnd_P_Vars_Pnd_Pin; }

    public DbsField getPnd_P_Vars_Pnd_Ph_Data_Rcd_Type() { return pnd_P_Vars_Pnd_Ph_Data_Rcd_Type; }

    public DbsField getPnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type() { return pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type; }

    public DbsField getPnd_P_Vars_Pnd_Ssn() { return pnd_P_Vars_Pnd_Ssn; }

    public DbsField getPnd_P_Vars_Pnd_Dob_Ccyymmdd() { return pnd_P_Vars_Pnd_Dob_Ccyymmdd; }

    public DbsField getPnd_P_Vars_Pnd_Dod_Ccyymmdd() { return pnd_P_Vars_Pnd_Dod_Ccyymmdd; }

    public DbsField getPnd_P_Vars_Pnd_Get_Pin() { return pnd_P_Vars_Pnd_Get_Pin; }

    public DbsField getPnd_P_Vars_Pnd_Get_Ssn_Level_Info() { return pnd_P_Vars_Pnd_Get_Ssn_Level_Info; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_P_Vars = dbsRecord.newGroupInRecord("pnd_P_Vars", "#P-VARS");
        pnd_P_Vars.setParameterOption(ParameterOption.ByReference);
        pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_P_Vars_Pnd_Cntrct_Payee_Cde = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1 = pnd_P_Vars.newGroupInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1", "Redefines", pnd_P_Vars_Pnd_Cntrct_Payee_Cde);
        pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N = pnd_P_Vars_Pnd_Cntrct_Payee_CdeRedef1.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Payee_Cde_N", "#CNTRCT-PAYEE-CDE-N", 
            FieldType.NUMERIC, 2);
        pnd_P_Vars_Pnd_Pin = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_P_Vars_Pnd_Ph_Data_Rcd_Type = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Ph_Data_Rcd_Type", "#PH-DATA-RCD-TYPE", FieldType.NUMERIC, 2);
        pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Cntrct_Data_Rcd_Type", "#CNTRCT-DATA-RCD-TYPE", FieldType.NUMERIC, 
            2);
        pnd_P_Vars_Pnd_Ssn = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        pnd_P_Vars_Pnd_Dob_Ccyymmdd = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Dob_Ccyymmdd", "#DOB-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_P_Vars_Pnd_Dod_Ccyymmdd = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Dod_Ccyymmdd", "#DOD-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_P_Vars_Pnd_Get_Pin = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Get_Pin", "#GET-PIN", FieldType.BOOLEAN);
        pnd_P_Vars_Pnd_Get_Ssn_Level_Info = pnd_P_Vars.newFieldInGroup("pnd_P_Vars_Pnd_Get_Ssn_Level_Info", "#GET-SSN-LEVEL-INFO", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaTwra105(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

