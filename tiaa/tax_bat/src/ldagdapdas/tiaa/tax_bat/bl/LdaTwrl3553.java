/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:34 PM
**        * FROM NATURAL LDA     : TWRL3553
************************************************************
**        * FILE NAME            : LdaTwrl3553.java
**        * CLASS NAME           : LdaTwrl3553
**        * INSTANCE NAME        : LdaTwrl3553
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3553 extends DbsRecord
{
    // Properties
    private DbsField pnd_Twrl3553c;
    private DbsGroup pnd_Twrl3553cRedef1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_City;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_City2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank3;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank4;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank5;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Fax;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank6;
    private DbsGroup pnd_Twrl3553cRedef2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_City;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Fgn_Stat_Pvc;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Postl_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntry_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank3;
    private DbsGroup pnd_Twrl3553cRedef3;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler3;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler4;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler5;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler6;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler7;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler8;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Tax_Wthld_Non_Qa_Plan;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wthld_Ann;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date;
    private DbsGroup pnd_Twrl3553cRedef5;
    private DbsField filler01;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr;
    private DbsField filler02;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Form_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Rec_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Doc_Type;
    private DbsField filler03;
    private DbsField filler04;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Tax_Year;
    private DbsField filler05;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Indentification_No;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Busins_Name;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Town;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Ext;
    private DbsField filler06;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_A_Town;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_A_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_A_Zip;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Ext;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Agent_Email;
    private DbsGroup pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Total;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Total_Forms;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Fill1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Fillr1a;
    private DbsGroup pnd_Twrl3553cRedef9;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler3;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler4;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_2;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Town;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_State;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler5;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6;
    private DbsField pnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date;

    public DbsField getPnd_Twrl3553c() { return pnd_Twrl3553c; }

    public DbsGroup getPnd_Twrl3553cRedef1() { return pnd_Twrl3553cRedef1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_City() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_City; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Blank2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_City2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_City2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Blank3() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank3; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde2() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Blank4() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank4; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Blank5() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank5; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Fax() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Fax; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Su_Blank6() { return pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank6; }

    public DbsGroup getPnd_Twrl3553cRedef2() { return pnd_Twrl3553cRedef2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_City() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_City; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank2() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Fgn_Stat_Pvc() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Fgn_Stat_Pvc; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Postl_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Postl_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntry_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntry_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank3() { return pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank3; }

    public DbsGroup getPnd_Twrl3553cRedef3() { return pnd_Twrl3553cRedef3; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler2() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler3() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler3; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler4() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler4; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler5() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler5; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler6() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler6; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler7() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler7; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler8() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler8; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Tax_Wthld_Non_Qa_Plan() { return pnd_Twrl3553c_Pnd_Twrl3553_Tax_Wthld_Non_Qa_Plan; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wthld_Ann() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wthld_Ann; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler10() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn() { return pnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date() { return pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date; }

    public DbsGroup getPnd_Twrl3553cRedef5() { return pnd_Twrl3553cRedef5; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr() { return pnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Form_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Form_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Rec_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Rec_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Doc_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Doc_Type; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Tax_Year() { return pnd_Twrl3553c_Pnd_Twrl3553_Tax_Year; }

    public DbsField getFiller05() { return filler05; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm() { return pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins() { return pnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Indentification_No() { return pnd_Twrl3553c_Pnd_Twrl3553_Indentification_No; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Busins_Name() { return pnd_Twrl3553c_Pnd_Twrl3553_Busins_Name; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme() { return pnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Town() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Town; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Ext() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Ext; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1() { return pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2() { return pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_A_Town() { return pnd_Twrl3553c_Pnd_Twrl3553_A_Town; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_A_State() { return pnd_Twrl3553c_Pnd_Twrl3553_A_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full() { return pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8() { return pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_A_Zip() { return pnd_Twrl3553c_Pnd_Twrl3553_A_Zip; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Ext() { return pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Ext; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address() { return pnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Agent_Email() { return pnd_Twrl3553c_Pnd_Twrl3553_Agent_Email; }

    public DbsGroup getPnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld() { return pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov() { return pnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20() { return pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10() { return pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified() { return pnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10() { return pnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost() { return pnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira() { return pnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10() { return pnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10() { return pnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri() { return pnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution() { return pnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria() { return pnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Total() { return pnd_Twrl3553c_Pnd_Twrl3553_Total; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Total_Forms() { return pnd_Twrl3553c_Pnd_Twrl3553_Total_Forms; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Fill1() { return pnd_Twrl3553c_Pnd_Twrl3553_Fill1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg() { return pnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Fillr1a() { return pnd_Twrl3553c_Pnd_Twrl3553_Fillr1a; }

    public DbsGroup getPnd_Twrl3553cRedef9() { return pnd_Twrl3553cRedef9; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler2() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler3() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler3; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler4() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler4; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_2() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_2; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Town() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Town; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_State() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_State; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler5() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler5; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6; }

    public DbsField getPnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date() { return pnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Twrl3553c = newFieldInRecord("pnd_Twrl3553c", "#TWRL3553C", FieldType.STRING, 2500);
        pnd_Twrl3553cRedef1 = newGroupInRecord("pnd_Twrl3553cRedef1", "Redefines", pnd_Twrl3553c);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Rec_In", "#TWRL3553-SU-REC-IN", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Ein", "#TWRL3553-SU-SUB-EIN", FieldType.NUMERIC, 
            9);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Resub_Ind", "#TWRL3553-SU-RESUB-IND", 
            FieldType.NUMERIC, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Sw_Cde", "#TWRL3553-SU-SW-CDE", FieldType.NUMERIC, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cmp_Nme", "#TWRL3553-SU-CMP-NME", FieldType.STRING, 
            57);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Add", "#TWRL3553-SU-LOC-ADD", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Add", "#TWRL3553-SU-DEL-ADD", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_City = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_City", "#TWRL3553-SU-CITY", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_St_Abbr", "#TWRL3553-SU-ST-ABBR", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip", "#TWRL3553-SU-ZIP", FieldType.NUMERIC, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn", "#TWRL3553-SU-ZIP-EXTN", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank2", "#TWRL3553-SU-BLANK2", FieldType.STRING, 
            17);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc", "#TWRL3553-SU-STATE-FGN-PRVC", 
            FieldType.STRING, 23);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde", "#TWRL3553-SU-FGN-PSTL-CDE", 
            FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde", "#TWRL3553-SU-CNTRY-CDE", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Sub_Nme", "#TWRL3553-SU-SUB-NME", FieldType.STRING, 
            57);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Loc_Addr", "#TWRL3553-SU-LOC-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Del_Addr", "#TWRL3553-SU-DEL-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_City2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_City2", "#TWRL3553-SU-CITY2", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_State = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_State", "#TWRL3553-SU-STATE", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip2", "#TWRL3553-SU-ZIP2", FieldType.NUMERIC, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Zip_Extn2", "#TWRL3553-SU-ZIP-EXTN2", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank3 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank3", "#TWRL3553-SU-BLANK3", FieldType.STRING, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_State_Fgn_Prvc2", "#TWRL3553-SU-STATE-FGN-PRVC2", 
            FieldType.STRING, 23);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Fgn_Pstl_Cde2", "#TWRL3553-SU-FGN-PSTL-CDE2", 
            FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde2 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntry_Cde2", "#TWRL3553-SU-CNTRY-CDE2", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Nme", "#TWRL3553-SU-CNTCT-NME", 
            FieldType.STRING, 27);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Num", "#TWRL3553-SU-CNTCT-PH-NUM", 
            FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Extn = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Ph_Extn", "#TWRL3553-SU-CNTCT-PH-EXTN", 
            FieldType.STRING, 5);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank4 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank4", "#TWRL3553-SU-BLANK4", FieldType.STRING, 
            3);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Email", "#TWRL3553-SU-CNTCT-EMAIL", 
            FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank5 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank5", "#TWRL3553-SU-BLANK5", FieldType.STRING, 
            3);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Fax = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Cntct_Fax", "#TWRL3553-SU-CNTCT-FAX", 
            FieldType.STRING, 10);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Prf_Prb_Ntf_Cde", "#TWRL3553-SU-PRF-PRB-NTF-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Prep_Cde", "#TWRL3553-SU-PREP-CDE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Submitr_Ident_Typ", "#TWRL3553-SU-SUBMITR-IDENT-TYP", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank6 = pnd_Twrl3553cRedef1.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Su_Blank6", "#TWRL3553-SU-BLANK6", FieldType.STRING, 
            2010);
        pnd_Twrl3553cRedef2 = newGroupInRecord("pnd_Twrl3553cRedef2", "Redefines", pnd_Twrl3553c);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Rec_Id", "#TWRL3553-PA-REC-ID", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Tax_Year", "#TWRL3553-PA-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agt_Ind_Cde", "#TWRL3553-PA-AGT-IND-CDE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Ein", "#TWRL3553-PA-EIN", FieldType.NUMERIC, 
            9);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Frm_Type", "#TWRL3553-PA-FRM-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1 = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank1", "#TWRL3553-PA-BLANK1", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Emp_Name", "#TWRL3553-PA-EMP-NAME", 
            FieldType.STRING, 57);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Loc_Addr", "#TWRL3553-PA-LOC-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Del_Addr", "#TWRL3553-PA-DEL-ADDR", 
            FieldType.STRING, 22);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_City = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_City", "#TWRL3553-PA-CITY", FieldType.STRING, 
            22);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_State = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_State", "#TWRL3553-PA-STATE", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip", "#TWRL3553-PA-ZIP", FieldType.NUMERIC, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Zip_Cde_Extn", "#TWRL3553-PA-ZIP-CDE-EXTN", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank2 = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank2", "#TWRL3553-PA-BLANK2", FieldType.STRING, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Fgn_Stat_Pvc = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Fgn_Stat_Pvc", "#TWRL3553-PA-FGN-STAT-PVC", 
            FieldType.STRING, 23);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Postl_Cde = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Postl_Cde", "#TWRL3553-PA-POSTL-CDE", 
            FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntry_Cde = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntry_Cde", "#TWRL3553-PA-CNTRY-CDE", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Cntct_Email", "#TWRL3553-PA-CNTCT-EMAIL", 
            FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Agent_Typ_Id", "#TWRL3553-PA-AGENT-TYP-ID", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank3 = pnd_Twrl3553cRedef2.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Pa_Blank3", "#TWRL3553-PA-BLANK3", FieldType.STRING, 
            2241);
        pnd_Twrl3553cRedef3 = newGroupInRecord("pnd_Twrl3553cRedef3", "Redefines", pnd_Twrl3553c);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler", "#TWRL3553-DTL-FILLER", FieldType.STRING, 
            1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Orig", "#TWRL3553-DTL-CNTRL-ORIG", 
            FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler1", "#TWRL3553-DTL-FILLER1", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Form_Type", "#TWRL3553-DTL-FORM-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rec_Type", "#TWRL3553-DTL-REC-TYPE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Doc_Type", "#TWRL3553-DTL-DOC-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler2 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler2", "#TWRL3553-DTL-FILLER2", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Year", "#TWRL3553-DTL-TAX-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler3 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler3", "#TWRL3553-DTL-FILLER3", 
            FieldType.STRING, 8);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler4 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler4", "#TWRL3553-DTL-FILLER4", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payer_Id_Type", "#TWRL3553-DTL-PAYER-ID-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Id_Nbr", "#TWRL3553-DTL-ID-NBR", FieldType.NUMERIC, 
            9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Name", "#TWRL3553-DTL-NAME", FieldType.STRING, 
            30);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line1", "#TWRL3553-DTL-ADDR-LINE1", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr_Line2", "#TWRL3553-DTL-ADDR-LINE2", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Town", "#TWRL3553-DTL-TOWN", FieldType.STRING, 
            13);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_State = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_State", "#TWRL3553-DTL-STATE", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip", "#TWRL3553-DTL-ZIP", FieldType.NUMERIC, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Zip_Extn", "#TWRL3553-DTL-ZIP-EXTN", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler5 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler5", "#TWRL3553-DTL-FILLER5", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ssn", "#TWRL3553-DTL-SSN", FieldType.NUMERIC, 
            9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Act_Nbr", "#TWRL3553-DTL-ACT-NBR", 
            FieldType.STRING, 20);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Name", "#TWRL3553-DTL-PT-NAME", 
            FieldType.STRING, 30);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line1", "#TWRL3553-DTL-ADDR2-LINE1", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Addr2_Line2", "#TWRL3553-DTL-ADDR2-LINE2", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Twn", "#TWRL3553-DTL-PT-TWN", FieldType.STRING, 
            13);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_State", "#TWRL3553-DTL-PT-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip", "#TWRL3553-DTL-PT-ZIP", FieldType.NUMERIC, 
            5);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pt_Zip_Extn", "#TWRL3553-DTL-PT-ZIP-EXTN", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler6 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler6", "#TWRL3553-DTL-FILLER6", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Form", "#TWRL3553-DTL-DIS-FORM", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pl_Type", "#TWRL3553-DTL-PL-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Cntrib", "#TWRL3553-DTL-ROLL-CNTRIB", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Roll_Distrib", "#TWRL3553-DTL-ROLL-DISTRIB", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ann_Cost", "#TWRL3553-DTL-ANN-COST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_20", "#TWRL3553-DTL-LS-TAX-WTHLD-20", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_10", "#TWRL3553-DTL-LS-TAX-WTHLD-10", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dr_Tax_Wthld_10", "#TWRL3553-DTL-DR-TAX-WTHLD-10", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rr_Tax_Wthld_10", "#TWRL3553-DTL-RR-TAX-WTHLD-10", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nr_Tax_Wthld_Dis", "#TWRL3553-DTL-NR-TAX-WTHLD-DIS", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amt_Dis", "#TWRL3553-DTL-AMT-DIS", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pre_Pay", "#TWRL3553-DTL-PRE-PAY", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Taxable_Amt", "#TWRL3553-DTL-TAXABLE-AMT", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler7 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler7", "#TWRL3553-DTL-FILLER7", 
            FieldType.STRING, 12);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler8 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler8", "#TWRL3553-DTL-FILLER8", 
            FieldType.STRING, 12);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Def_Cntrib", "#TWRL3553-DTL-DEF-CNTRIB", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Aft_Tax_Cntrib", "#TWRL3553-DTL-AFT-TAX-CNTRIB", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Inc_Accretion", "#TWRL3553-DTL-INC-ACCRETION", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Total", "#TWRL3553-DTL-TOTAL", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dis_Cde", "#TWRL3553-DTL-DIS-CDE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Qp_Tax_Wthld", "#TWRL3553-DTL-QP-TAX-WTHLD", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Tax_Wthld", "#TWRL3553-DTL-OD-TAX-WTHLD", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Others", "#TWRL3553-DTL-OTHERS", FieldType.DECIMAL, 
            12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Od_Qp_Tax_Wthld", "#TWRL3553-DTL-OD-QP-TAX-WTHLD", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9 = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9", "#TWRL3553-DTL-FILLER9", 
            FieldType.STRING, 1870);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4 = pnd_Twrl3553cRedef3.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4", "Redefines", 
            pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08 = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ls_Tax_Wthld_08", 
            "#TWRL3553-DTL-LS-TAX-WTHLD-08", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Nq_Tax_Wthld_Dis", 
            "#TWRL3553-DTL-NQ-TAX-WTHLD-DIS", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Othr_Distr_Cde", 
            "#TWRL3553-DTL-OTHR-DISTR-CDE", FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6 = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Fill6", "#TWRL3553-DTL-FILL6", 
            FieldType.STRING, 161);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_First_Name", 
            "#TWRL3553-DTL-FIRST-NAME", FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Mid_Name", 
            "#TWRL3553-DTL-PAYEEE-MID-NAME", FieldType.STRING, 15);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Payeee_Last_Name", 
            "#TWRL3553-DTL-PAYEEE-LAST-NAME", FieldType.STRING, 20);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Pye_Mm_Last_Name", 
            "#TWRL3553-DTL-PYE-MM-LAST-NAME", FieldType.STRING, 20);
        pnd_Twrl3553c_Pnd_Twrl3553_Tax_Wthld_Non_Qa_Plan = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Tax_Wthld_Non_Qa_Plan", 
            "#TWRL3553-TAX-WTHLD-NON-QA-PLAN", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wthld_Ann = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wthld_Ann", 
            "#TWRL3553-DTL-TAX-WTHLD-ANN", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ein", "#TWRL3553-DTL-EIN", 
            FieldType.STRING, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Nm", 
            "#TWRL3553-DTL-PLAN-NM", FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Plan_Spnsr", 
            "#TWRL3553-DTL-PLAN-SPNSR", FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_A_Exempt", 
            "#TWRL3553-DTL-A-EXEMPT", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_B_Taxable", 
            "#TWRL3553-DTL-B-TAXABLE", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_C_Amt_Prep", 
            "#TWRL3553-DTL-C-AMT-PREP", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_D_Aft_Tax_Cntr", 
            "#TWRL3553-DTL-D-AFT-TAX-CNTR", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_E_Total", 
            "#TWRL3553-DTL-E-TOTAL", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Tax_Wth_Elg_Dist", 
            "#TWRL3553-DTL-TAX-WTH-ELG-DIST", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Dist_Exempt_Incm", 
            "#TWRL3553-DTL-DIST-EXEMPT-INCM", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler10 = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler10", 
            "#TWRL3553-DTL-FILLER10", FieldType.STRING, 1385);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Gov_Rt_Fnd", 
            "#TWRL3553-DTL-GOV-RT-FND", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Ap_Tax_Wthld", 
            "#TWRL3553-DTL-AP-TAX-WTHLD", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn = pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Filler9Redef4.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dte_Receive_Pnsn", 
            "#TWRL3553-DTE-RECEIVE-PNSN", FieldType.STRING, 8);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Cntrl_Amnd", "#TWRL3553-DTL-CNTRL-AMND", 
            FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Rsn_For_Chg", "#TWRL3553-DTL-RSN-FOR-CHG", 
            FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date = pnd_Twrl3553cRedef3.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Dtl_Amnd_Date", "#TWRL3553-DTL-AMND-DATE", 
            FieldType.NUMERIC, 6);
        pnd_Twrl3553cRedef5 = newGroupInRecord("pnd_Twrl3553cRedef5", "Redefines", pnd_Twrl3553c);
        filler01 = pnd_Twrl3553cRedef5.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr = pnd_Twrl3553cRedef5.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Control_Nbr", "#TWRL3553-CONTROL-NBR", 
            FieldType.NUMERIC, 9);
        filler02 = pnd_Twrl3553cRedef5.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Form_Type = pnd_Twrl3553cRedef5.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Form_Type", "#TWRL3553-FORM-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl3553c_Pnd_Twrl3553_Rec_Type = pnd_Twrl3553cRedef5.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Rec_Type", "#TWRL3553-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Twrl3553c_Pnd_Twrl3553_Doc_Type = pnd_Twrl3553cRedef5.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Doc_Type", "#TWRL3553-DOC-TYPE", FieldType.STRING, 
            1);
        filler03 = pnd_Twrl3553cRedef5.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 1);
        filler04 = pnd_Twrl3553cRedef5.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Tax_Year = pnd_Twrl3553cRedef5.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Tax_Year", "#TWRL3553-TAX-YEAR", FieldType.NUMERIC, 
            4);
        filler05 = pnd_Twrl3553cRedef5.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 5);
        pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm = pnd_Twrl3553cRedef5.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm", "#TWRL3553-WITHHLDNG-AGNT-INFRM");
        pnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Payers_Id_Type", 
            "#TWRL3553-PAYERS-ID-TYPE", FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Typ_Industry_Busins", 
            "#TWRL3553-TYP-INDUSTRY-BUSINS", FieldType.STRING, 20);
        pnd_Twrl3553c_Pnd_Twrl3553_Indentification_No = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Indentification_No", 
            "#TWRL3553-INDENTIFICATION-NO", FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Busins_Name = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Busins_Name", 
            "#TWRL3553-BUSINS-NAME", FieldType.STRING, 30);
        pnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Withhld_Agent_Nme", 
            "#TWRL3553-WITHHLD-AGENT-NME", FieldType.STRING, 30);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A", 
            "#TWRL3553-AGENT-TELEPHONE-A", FieldType.STRING, 10);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6", 
            "Redefines", pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_A);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone = pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone_ARedef6.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Telephone", 
            "#TWRL3553-AGENT-TELEPHONE", FieldType.NUMERIC, 10);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_1", 
            "#TWRL3553-AGENT-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Address_2", 
            "#TWRL3553-AGENT-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Town = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Town", 
            "#TWRL3553-AGENT-TOWN", FieldType.STRING, 13);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_State = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_State", 
            "#TWRL3553-AGENT-STATE", FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full", 
            "#TWRL3553-AGENT-ZIP-FULL", FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7", 
            "Redefines", pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Full);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip = pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip", 
            "#TWRL3553-AGENT-ZIP", FieldType.NUMERIC, 5);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Ext = pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_FullRedef7.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Zip_Ext", 
            "#TWRL3553-AGENT-ZIP-EXT", FieldType.NUMERIC, 4);
        filler06 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("filler06", "FILLER", FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_1", 
            "#TWRL3553-PHYSICAL-ADDRESS-1", FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Physical_Address_2", 
            "#TWRL3553-PHYSICAL-ADDRESS-2", FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_A_Town = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_Town", "#TWRL3553-A-TOWN", 
            FieldType.STRING, 13);
        pnd_Twrl3553c_Pnd_Twrl3553_A_State = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_State", "#TWRL3553-A-STATE", 
            FieldType.STRING, 2);
        pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full", 
            "#TWRL3553-A-ZIP-FULL", FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8 = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8", 
            "Redefines", pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Full);
        pnd_Twrl3553c_Pnd_Twrl3553_A_Zip = pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_Zip", "#TWRL3553-A-ZIP", 
            FieldType.NUMERIC, 5);
        pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Ext = pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_FullRedef8.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_A_Zip_Ext", "#TWRL3553-A-ZIP-EXT", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Chng_Of_Address", 
            "#TWRL3553-CHNG-OF-ADDRESS", FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Agent_Email = pnd_Twrl3553c_Pnd_Twrl3553_Withhldng_Agnt_Infrm.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Agent_Email", 
            "#TWRL3553-AGENT-EMAIL", FieldType.STRING, 50);
        pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld = pnd_Twrl3553cRedef5.newGroupInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld", "#TWRL3553-TAX-WITHHELD");
        pnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Prdc_Pymnt_Quali_Gov", 
            "#TWRL3553-PRDC-PYMNT-QUALI-GOV", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri20", 
            "#TWRL3553-LUMPSUM-DISTRI20", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Lumpsum_Distri10", 
            "#TWRL3553-LUMPSUM-DISTRI10", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Distri_Nonqualified", 
            "#TWRL3553-DISTRI-NONQUALIFIED", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Othr_Incm_Qualifd10", 
            "#TWRL3553-OTHR-INCM-QUALIFD10", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Annuity_Cost", "#TWRL3553-ANNUITY-COST", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Rolovr_Qual_Nondtira", 
            "#TWRL3553-ROLOVR-QUAL-NONDTIRA", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Distri_Rtrmnt_Sav10", 
            "#TWRL3553-DISTRI-RTRMNT-SAV10", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Rlvr_Non_Dtira10", 
            "#TWRL3553-RLVR-NON-DTIRA10", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Nonres_Dsitri", 
            "#TWRL3553-NONRES-DSITRI", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Other_Distribution", 
            "#TWRL3553-OTHER-DISTRIBUTION", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Econo_Ergncy_Maria", 
            "#TWRL3553-ECONO-ERGNCY-MARIA", FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Total = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Total", "#TWRL3553-TOTAL", 
            FieldType.DECIMAL, 12,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Total_Forms = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Total_Forms", "#TWRL3553-TOTAL-FORMS", 
            FieldType.NUMERIC, 10);
        pnd_Twrl3553c_Pnd_Twrl3553_Fill1 = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Fill1", "#TWRL3553-FILL1", 
            FieldType.STRING, 1921);
        pnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Reason_For_Chg", 
            "#TWRL3553-REASON-FOR-CHG", FieldType.STRING, 40);
        pnd_Twrl3553c_Pnd_Twrl3553_Fillr1a = pnd_Twrl3553c_Pnd_Twrl3553_Tax_Withheld.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Fillr1a", "#TWRL3553-FILLR1A", 
            FieldType.NUMERIC, 6);
        pnd_Twrl3553cRedef9 = newGroupInRecord("pnd_Twrl3553cRedef9", "Redefines", pnd_Twrl3553c);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler", "#TWRL3553-SM-FILLER", FieldType.STRING, 
            1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Control_Number", "#TWRL3553-SM-CONTROL-NUMBER", 
            FieldType.NUMERIC, 9);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler2 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler2", "#TWRL3553-SM-FILLER2", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Form_Type", "#TWRL3553-SM-FORM-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Record_Type", "#TWRL3553-SM-RECORD-TYPE", 
            FieldType.NUMERIC, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Document_Type", "#TWRL3553-SM-DOCUMENT-TYPE", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler3 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler3", "#TWRL3553-SM-FILLER3", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Taxable_Year", "#TWRL3553-SM-TAXABLE-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler4 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler4", "#TWRL3553-SM-FILLER4", FieldType.STRING, 
            1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Payer_Id_Typ", "#TWRL3553-SM-PAYER-ID-TYP", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Idn_Nbr", "#TWRL3553-SM-IDN-NBR", FieldType.NUMERIC, 
            9);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nameer3", "#TWRL3553-SM-NAMEER3", FieldType.STRING, 
            30);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_1", "#TWRL3553-SM-ADD-LN-1", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_2 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Add_Ln_2", "#TWRL3553-SM-ADD-LN-2", 
            FieldType.STRING, 35);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Town = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Town", "#TWRL3553-SM-TOWN", FieldType.STRING, 
            13);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_State = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_State", "#TWRL3553-SM-STATE", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code", "#TWRL3553-SM-ZIP-CODE", 
            FieldType.NUMERIC, 5);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Zip_Code_Extn", "#TWRL3553-SM-ZIP-CODE-EXTN", 
            FieldType.NUMERIC, 4);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler5 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler5", "#TWRL3553-SM-FILLER5", FieldType.STRING, 
            2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Nbr_Of_Docs", "#TWRL3553-SM-NBR-OF-DOCS", 
            FieldType.NUMERIC, 10);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Wthld", "#TWRL3553-SM-TOT-AMT-WTHLD", 
            FieldType.DECIMAL, 15,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Tot_Amt_Paid", "#TWRL3553-SM-TOT-AMT-PAID", 
            FieldType.DECIMAL, 15,2);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Type_Of_Taxpayer", "#TWRL3553-SM-TYPE-OF-TAXPAYER", 
            FieldType.STRING, 1);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6 = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Filler6", "#TWRL3553-SM-FILLER6", FieldType.STRING, 
            2295);
        pnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date = pnd_Twrl3553cRedef9.newFieldInGroup("pnd_Twrl3553c_Pnd_Twrl3553_Sm_Amnd_Date", "#TWRL3553-SM-AMND-DATE", 
            FieldType.NUMERIC, 6);

        this.setRecordName("LdaTwrl3553");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl3553() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
