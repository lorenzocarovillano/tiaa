/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:13:11 PM
**        * FROM NATURAL LDA     : TWRL5950
************************************************************
**        * FILE NAME            : LdaTwrl5950.java
**        * CLASS NAME           : LdaTwrl5950
**        * INSTANCE NAME        : LdaTwrl5950
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl5950 extends DbsRecord
{
    // Properties
    private DbsGroup twrl5950;
    private DbsField twrl5950_Pnd_State_Ind_Desc;

    public DbsGroup getTwrl5950() { return twrl5950; }

    public DbsField getTwrl5950_Pnd_State_Ind_Desc() { return twrl5950_Pnd_State_Ind_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        twrl5950 = newGroupInRecord("twrl5950", "TWRL5950");
        twrl5950_Pnd_State_Ind_Desc = twrl5950.newFieldArrayInGroup("twrl5950_Pnd_State_Ind_Desc", "#STATE-IND-DESC", FieldType.STRING, 15, new DbsArrayController(1,
            9));

        this.setRecordName("LdaTwrl5950");
    }

    public void initializeValues() throws Exception
    {
        reset();
        twrl5950_Pnd_State_Ind_Desc.getValue(1).setInitialValue("V TAXABLE ALL");
        twrl5950_Pnd_State_Ind_Desc.getValue(2).setInitialValue("V TAXABLE GT 0");
        twrl5950_Pnd_State_Ind_Desc.getValue(3).setInitialValue("V GROSS ALL");
        twrl5950_Pnd_State_Ind_Desc.getValue(4).setInitialValue("V GROSS GT 0");
        twrl5950_Pnd_State_Ind_Desc.getValue(5).setInitialValue("M TAXABLE ALL");
        twrl5950_Pnd_State_Ind_Desc.getValue(6).setInitialValue("M TAXABLE GT 0");
        twrl5950_Pnd_State_Ind_Desc.getValue(7).setInitialValue("M GROSS ALL");
        twrl5950_Pnd_State_Ind_Desc.getValue(8).setInitialValue("M GROSS GT 0");
    }

    // Constructor
    public LdaTwrl5950() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
