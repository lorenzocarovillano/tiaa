/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:15:18 PM
**        * FROM NATURAL LDA     : TWRLPLSC
************************************************************
**        * FILE NAME            : LdaTwrlplsc.java
**        * CLASS NAME           : LdaTwrlplsc
**        * INSTANCE NAME        : LdaTwrlplsc
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrlplsc extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_twrlplsc;
    private DbsGroup twrlplsc_Rt_Record;
    private DbsField twrlplsc_Rt_A_I_Ind;
    private DbsField twrlplsc_Rt_Table_Id;
    private DbsField twrlplsc_Rt_Short_Key;
    private DbsGroup twrlplsc_Rt_Short_KeyRedef1;
    private DbsField twrlplsc_Rt_Plan;
    private DbsField twrlplsc_Rt_Long_Key;
    private DbsField twrlplsc_Rt_Desc5;
    private DbsGroup twrlplsc_Rt_Desc5Redef2;
    private DbsField twrlplsc_Rt_Pl_Inverse_Lu_Ts;
    private DbsField twrlplsc_Rt_Pl_Cr_User_Id;
    private DbsField twrlplsc_Rt_Pl_Cr_Ts;
    private DbsField twrlplsc_Rt_Pl_Lu_User_Id;
    private DbsField twrlplsc_Rt_Pl_Lu_Ts;
    private DbsField twrlplsc_Rt_Pl_Srce;
    private DbsField twrlplsc_Rt_Pl_Type;
    private DbsField twrlplsc_Rt_Pl_Res;
    private DbsField twrlplsc_Rt_Pl_Comm_Ind;
    private DbsField twrlplsc_Rt_Super1;

    public DataAccessProgramView getVw_twrlplsc() { return vw_twrlplsc; }

    public DbsGroup getTwrlplsc_Rt_Record() { return twrlplsc_Rt_Record; }

    public DbsField getTwrlplsc_Rt_A_I_Ind() { return twrlplsc_Rt_A_I_Ind; }

    public DbsField getTwrlplsc_Rt_Table_Id() { return twrlplsc_Rt_Table_Id; }

    public DbsField getTwrlplsc_Rt_Short_Key() { return twrlplsc_Rt_Short_Key; }

    public DbsGroup getTwrlplsc_Rt_Short_KeyRedef1() { return twrlplsc_Rt_Short_KeyRedef1; }

    public DbsField getTwrlplsc_Rt_Plan() { return twrlplsc_Rt_Plan; }

    public DbsField getTwrlplsc_Rt_Long_Key() { return twrlplsc_Rt_Long_Key; }

    public DbsField getTwrlplsc_Rt_Desc5() { return twrlplsc_Rt_Desc5; }

    public DbsGroup getTwrlplsc_Rt_Desc5Redef2() { return twrlplsc_Rt_Desc5Redef2; }

    public DbsField getTwrlplsc_Rt_Pl_Inverse_Lu_Ts() { return twrlplsc_Rt_Pl_Inverse_Lu_Ts; }

    public DbsField getTwrlplsc_Rt_Pl_Cr_User_Id() { return twrlplsc_Rt_Pl_Cr_User_Id; }

    public DbsField getTwrlplsc_Rt_Pl_Cr_Ts() { return twrlplsc_Rt_Pl_Cr_Ts; }

    public DbsField getTwrlplsc_Rt_Pl_Lu_User_Id() { return twrlplsc_Rt_Pl_Lu_User_Id; }

    public DbsField getTwrlplsc_Rt_Pl_Lu_Ts() { return twrlplsc_Rt_Pl_Lu_Ts; }

    public DbsField getTwrlplsc_Rt_Pl_Srce() { return twrlplsc_Rt_Pl_Srce; }

    public DbsField getTwrlplsc_Rt_Pl_Type() { return twrlplsc_Rt_Pl_Type; }

    public DbsField getTwrlplsc_Rt_Pl_Res() { return twrlplsc_Rt_Pl_Res; }

    public DbsField getTwrlplsc_Rt_Pl_Comm_Ind() { return twrlplsc_Rt_Pl_Comm_Ind; }

    public DbsField getTwrlplsc_Rt_Super1() { return twrlplsc_Rt_Super1; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_twrlplsc = new DataAccessProgramView(new NameInfo("vw_twrlplsc", "TWRLPLSC"), "TWR_PLAN_TABLE", "REFERNCE_TABLE");
        twrlplsc_Rt_Record = vw_twrlplsc.getRecord().newGroupInGroup("twrlplsc_Rt_Record", "RT-RECORD");
        twrlplsc_Rt_A_I_Ind = twrlplsc_Rt_Record.newFieldInGroup("twrlplsc_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RT_A_I_IND");
        twrlplsc_Rt_A_I_Ind.setDdmHeader("ACT/IND");
        twrlplsc_Rt_Table_Id = twrlplsc_Rt_Record.newFieldInGroup("twrlplsc_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "RT_TABLE_ID");
        twrlplsc_Rt_Table_Id.setDdmHeader("TABLE/NAME");
        twrlplsc_Rt_Short_Key = twrlplsc_Rt_Record.newFieldInGroup("twrlplsc_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "RT_SHORT_KEY");
        twrlplsc_Rt_Short_KeyRedef1 = vw_twrlplsc.getRecord().newGroupInGroup("twrlplsc_Rt_Short_KeyRedef1", "Redefines", twrlplsc_Rt_Short_Key);
        twrlplsc_Rt_Plan = twrlplsc_Rt_Short_KeyRedef1.newFieldInGroup("twrlplsc_Rt_Plan", "RT-PLAN", FieldType.STRING, 6);
        twrlplsc_Rt_Long_Key = twrlplsc_Rt_Record.newFieldInGroup("twrlplsc_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "RT_LONG_KEY");
        twrlplsc_Rt_Desc5 = twrlplsc_Rt_Record.newFieldInGroup("twrlplsc_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        twrlplsc_Rt_Desc5Redef2 = vw_twrlplsc.getRecord().newGroupInGroup("twrlplsc_Rt_Desc5Redef2", "Redefines", twrlplsc_Rt_Desc5);
        twrlplsc_Rt_Pl_Inverse_Lu_Ts = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Inverse_Lu_Ts", "RT-PL-INVERSE-LU-TS", FieldType.PACKED_DECIMAL, 
            13);
        twrlplsc_Rt_Pl_Cr_User_Id = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Cr_User_Id", "RT-PL-CR-USER-ID", FieldType.STRING, 8);
        twrlplsc_Rt_Pl_Cr_Ts = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Cr_Ts", "RT-PL-CR-TS", FieldType.TIME);
        twrlplsc_Rt_Pl_Lu_User_Id = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Lu_User_Id", "RT-PL-LU-USER-ID", FieldType.STRING, 8);
        twrlplsc_Rt_Pl_Lu_Ts = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Lu_Ts", "RT-PL-LU-TS", FieldType.TIME);
        twrlplsc_Rt_Pl_Srce = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Srce", "RT-PL-SRCE", FieldType.STRING, 1);
        twrlplsc_Rt_Pl_Type = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Type", "RT-PL-TYPE", FieldType.STRING, 2);
        twrlplsc_Rt_Pl_Res = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Res", "RT-PL-RES", FieldType.STRING, 3);
        twrlplsc_Rt_Pl_Comm_Ind = twrlplsc_Rt_Desc5Redef2.newFieldInGroup("twrlplsc_Rt_Pl_Comm_Ind", "RT-PL-COMM-IND", FieldType.BOOLEAN);
        twrlplsc_Rt_Super1 = vw_twrlplsc.getRecord().newFieldInGroup("twrlplsc_Rt_Super1", "RT-SUPER1", FieldType.STRING, 66, RepeatingFieldStrategy.None, 
            "RT_SUPER1");

        this.setRecordName("LdaTwrlplsc");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_twrlplsc.reset();
    }

    // Constructor
    public LdaTwrlplsc() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
