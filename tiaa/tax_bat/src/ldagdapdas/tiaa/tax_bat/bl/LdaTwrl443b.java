/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:39 PM
**        * FROM NATURAL LDA     : TWRL443B
************************************************************
**        * FILE NAME            : LdaTwrl443b.java
**        * CLASS NAME           : LdaTwrl443b
**        * INSTANCE NAME        : LdaTwrl443b
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl443b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_A_Tape;
    private DbsField pnd_A_Tape_Pnd_A_Record_Type;
    private DbsField pnd_A_Tape_Pnd_A_Payment_Year;
    private DbsField pnd_A_Tape_Pnd_A_Combined_Fed_State_Filer;
    private DbsField pnd_A_Tape_Pnd_A_Filler_1;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Ein;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Name_Control;
    private DbsField pnd_A_Tape_Pnd_A_Last_Filing_Ind;
    private DbsField pnd_A_Tape_Pnd_A_Type_Of_Return;
    private DbsField pnd_A_Tape_Pnd_A_Amount_Indicators;
    private DbsField pnd_A_Tape_Pnd_A_Filler_2;
    private DbsField pnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Name_1;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Name_2;
    private DbsField pnd_A_Tape_Pnd_A_Transfer_Agent_Indicator;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Shipping_Address;
    private DbsField pnd_A_Tape_Pnd_A_Payer_City;
    private DbsField pnd_A_Tape_Pnd_A_Payer_State;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Zip;
    private DbsField pnd_A_Tape_Pnd_A_Payer_Phone_No;
    private DbsField pnd_A_Tape_Pnd_A_Filler_4;
    private DbsField pnd_A_Tape_Pnd_A_Filler_5;
    private DbsField pnd_A_Tape_Pnd_A_Sequence_Number;
    private DbsField pnd_A_Tape_Pnd_A_Filler_6;
    private DbsGroup pnd_A_TapeRedef1;
    private DbsField pnd_A_Tape_Pnd_A_Move_1;
    private DbsField pnd_A_Tape_Pnd_A_Move_2;
    private DbsField pnd_A_Tape_Pnd_A_Move_3;

    public DbsGroup getPnd_A_Tape() { return pnd_A_Tape; }

    public DbsField getPnd_A_Tape_Pnd_A_Record_Type() { return pnd_A_Tape_Pnd_A_Record_Type; }

    public DbsField getPnd_A_Tape_Pnd_A_Payment_Year() { return pnd_A_Tape_Pnd_A_Payment_Year; }

    public DbsField getPnd_A_Tape_Pnd_A_Combined_Fed_State_Filer() { return pnd_A_Tape_Pnd_A_Combined_Fed_State_Filer; }

    public DbsField getPnd_A_Tape_Pnd_A_Filler_1() { return pnd_A_Tape_Pnd_A_Filler_1; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Ein() { return pnd_A_Tape_Pnd_A_Payer_Ein; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Name_Control() { return pnd_A_Tape_Pnd_A_Payer_Name_Control; }

    public DbsField getPnd_A_Tape_Pnd_A_Last_Filing_Ind() { return pnd_A_Tape_Pnd_A_Last_Filing_Ind; }

    public DbsField getPnd_A_Tape_Pnd_A_Type_Of_Return() { return pnd_A_Tape_Pnd_A_Type_Of_Return; }

    public DbsField getPnd_A_Tape_Pnd_A_Amount_Indicators() { return pnd_A_Tape_Pnd_A_Amount_Indicators; }

    public DbsField getPnd_A_Tape_Pnd_A_Filler_2() { return pnd_A_Tape_Pnd_A_Filler_2; }

    public DbsField getPnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator() { return pnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Name_1() { return pnd_A_Tape_Pnd_A_Payer_Name_1; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Name_2() { return pnd_A_Tape_Pnd_A_Payer_Name_2; }

    public DbsField getPnd_A_Tape_Pnd_A_Transfer_Agent_Indicator() { return pnd_A_Tape_Pnd_A_Transfer_Agent_Indicator; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Shipping_Address() { return pnd_A_Tape_Pnd_A_Payer_Shipping_Address; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_City() { return pnd_A_Tape_Pnd_A_Payer_City; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_State() { return pnd_A_Tape_Pnd_A_Payer_State; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Zip() { return pnd_A_Tape_Pnd_A_Payer_Zip; }

    public DbsField getPnd_A_Tape_Pnd_A_Payer_Phone_No() { return pnd_A_Tape_Pnd_A_Payer_Phone_No; }

    public DbsField getPnd_A_Tape_Pnd_A_Filler_4() { return pnd_A_Tape_Pnd_A_Filler_4; }

    public DbsField getPnd_A_Tape_Pnd_A_Filler_5() { return pnd_A_Tape_Pnd_A_Filler_5; }

    public DbsField getPnd_A_Tape_Pnd_A_Sequence_Number() { return pnd_A_Tape_Pnd_A_Sequence_Number; }

    public DbsField getPnd_A_Tape_Pnd_A_Filler_6() { return pnd_A_Tape_Pnd_A_Filler_6; }

    public DbsGroup getPnd_A_TapeRedef1() { return pnd_A_TapeRedef1; }

    public DbsField getPnd_A_Tape_Pnd_A_Move_1() { return pnd_A_Tape_Pnd_A_Move_1; }

    public DbsField getPnd_A_Tape_Pnd_A_Move_2() { return pnd_A_Tape_Pnd_A_Move_2; }

    public DbsField getPnd_A_Tape_Pnd_A_Move_3() { return pnd_A_Tape_Pnd_A_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_A_Tape = newGroupInRecord("pnd_A_Tape", "#A-TAPE");
        pnd_A_Tape_Pnd_A_Record_Type = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Record_Type", "#A-RECORD-TYPE", FieldType.STRING, 1);
        pnd_A_Tape_Pnd_A_Payment_Year = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payment_Year", "#A-PAYMENT-YEAR", FieldType.STRING, 4);
        pnd_A_Tape_Pnd_A_Combined_Fed_State_Filer = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Combined_Fed_State_Filer", "#A-COMBINED-FED-STATE-FILER", 
            FieldType.NUMERIC, 1);
        pnd_A_Tape_Pnd_A_Filler_1 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Filler_1", "#A-FILLER-1", FieldType.STRING, 5);
        pnd_A_Tape_Pnd_A_Payer_Ein = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Ein", "#A-PAYER-EIN", FieldType.STRING, 9);
        pnd_A_Tape_Pnd_A_Payer_Name_Control = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Name_Control", "#A-PAYER-NAME-CONTROL", FieldType.STRING, 
            4);
        pnd_A_Tape_Pnd_A_Last_Filing_Ind = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Last_Filing_Ind", "#A-LAST-FILING-IND", FieldType.STRING, 1);
        pnd_A_Tape_Pnd_A_Type_Of_Return = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Type_Of_Return", "#A-TYPE-OF-RETURN", FieldType.STRING, 2);
        pnd_A_Tape_Pnd_A_Amount_Indicators = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Amount_Indicators", "#A-AMOUNT-INDICATORS", FieldType.STRING, 
            16);
        pnd_A_Tape_Pnd_A_Filler_2 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Filler_2", "#A-FILLER-2", FieldType.STRING, 8);
        pnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator", "#A-FORIEGN-CORP-INDICATOR", FieldType.STRING, 
            1);
        pnd_A_Tape_Pnd_A_Payer_Name_1 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Name_1", "#A-PAYER-NAME-1", FieldType.STRING, 40);
        pnd_A_Tape_Pnd_A_Payer_Name_2 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Name_2", "#A-PAYER-NAME-2", FieldType.STRING, 40);
        pnd_A_Tape_Pnd_A_Transfer_Agent_Indicator = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Transfer_Agent_Indicator", "#A-TRANSFER-AGENT-INDICATOR", 
            FieldType.STRING, 1);
        pnd_A_Tape_Pnd_A_Payer_Shipping_Address = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Shipping_Address", "#A-PAYER-SHIPPING-ADDRESS", FieldType.STRING, 
            40);
        pnd_A_Tape_Pnd_A_Payer_City = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_City", "#A-PAYER-CITY", FieldType.STRING, 40);
        pnd_A_Tape_Pnd_A_Payer_State = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_State", "#A-PAYER-STATE", FieldType.STRING, 2);
        pnd_A_Tape_Pnd_A_Payer_Zip = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Zip", "#A-PAYER-ZIP", FieldType.STRING, 9);
        pnd_A_Tape_Pnd_A_Payer_Phone_No = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Payer_Phone_No", "#A-PAYER-PHONE-NO", FieldType.STRING, 15);
        pnd_A_Tape_Pnd_A_Filler_4 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Filler_4", "#A-FILLER-4", FieldType.STRING, 250);
        pnd_A_Tape_Pnd_A_Filler_5 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Filler_5", "#A-FILLER-5", FieldType.STRING, 10);
        pnd_A_Tape_Pnd_A_Sequence_Number = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Sequence_Number", "#A-SEQUENCE-NUMBER", FieldType.NUMERIC, 8);
        pnd_A_Tape_Pnd_A_Filler_6 = pnd_A_Tape.newFieldInGroup("pnd_A_Tape_Pnd_A_Filler_6", "#A-FILLER-6", FieldType.STRING, 243);
        pnd_A_TapeRedef1 = newGroupInRecord("pnd_A_TapeRedef1", "Redefines", pnd_A_Tape);
        pnd_A_Tape_Pnd_A_Move_1 = pnd_A_TapeRedef1.newFieldInGroup("pnd_A_Tape_Pnd_A_Move_1", "#A-MOVE-1", FieldType.STRING, 250);
        pnd_A_Tape_Pnd_A_Move_2 = pnd_A_TapeRedef1.newFieldInGroup("pnd_A_Tape_Pnd_A_Move_2", "#A-MOVE-2", FieldType.STRING, 250);
        pnd_A_Tape_Pnd_A_Move_3 = pnd_A_TapeRedef1.newFieldInGroup("pnd_A_Tape_Pnd_A_Move_3", "#A-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl443b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_A_Tape_Pnd_A_Record_Type.setInitialValue("A");
        pnd_A_Tape_Pnd_A_Combined_Fed_State_Filer.setInitialValue(1);
        pnd_A_Tape_Pnd_A_Filler_1.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Payer_Name_Control.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Last_Filing_Ind.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Type_Of_Return.setInitialValue("L ");
        pnd_A_Tape_Pnd_A_Filler_2.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Foriegn_Corp_Indicator.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Transfer_Agent_Indicator.setInitialValue("0");
        pnd_A_Tape_Pnd_A_Payer_Shipping_Address.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Payer_City.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Payer_State.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Payer_Zip.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Payer_Phone_No.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Filler_4.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Filler_5.setInitialValue(" ");
        pnd_A_Tape_Pnd_A_Sequence_Number.setInitialValue(0);
        pnd_A_Tape_Pnd_A_Filler_6.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl443b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
