/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:29 PM
**        * FROM NATURAL LDA     : TWRL3322
************************************************************
**        * FILE NAME            : LdaTwrl3322.java
**        * CLASS NAME           : LdaTwrl3322
**        * INSTANCE NAME        : LdaTwrl3322
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl3322 extends DbsRecord
{
    // Properties
    private DbsGroup irs_B_Out_Tape;
    private DbsField irs_B_Out_Tape_Irs_B_Record_Type;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Year;
    private DbsField irs_B_Out_Tape_Irs_B_Return_Indicator;
    private DbsField irs_B_Out_Tape_Irs_B_Name_Control;
    private DbsField irs_B_Out_Tape_Irs_B_Type_Of_Tin;
    private DbsField irs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb;
    private DbsField irs_B_Out_Tape_Irs_B_Contract_Payee;
    private DbsGroup irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1;
    private DbsField irs_B_Out_Tape_Irs_B_Contract_First_2b;
    private DbsField irs_B_Out_Tape_Irs_B_Contract_Filler;
    private DbsField irs_B_Out_Tape_Irs_B_Payers_Office_Code;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_1;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_1;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_2;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_3;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_4;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_5;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_6;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_7;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_8;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_9;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_A;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_B;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_C;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_D;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_E;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_F;
    private DbsField irs_B_Out_Tape_Irs_B_Payment_Amount_G;
    private DbsField irs_B_Out_Tape_Irs_B_Foreign_Indicator;
    private DbsField irs_B_Out_Tape_Irs_B_Name_Line_1;
    private DbsField irs_B_Out_Tape_Irs_B_Name_Line_2;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_3;
    private DbsField irs_B_Out_Tape_Irs_B_Address_Line;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_4;
    private DbsField irs_B_Out_Tape_Irs_B_City_State_Zip;
    private DbsGroup irs_B_Out_Tape_Irs_B_City_State_ZipRedef2;
    private DbsField irs_B_Out_Tape_Irs_B_City;
    private DbsField irs_B_Out_Tape_Irs_B_State;
    private DbsField irs_B_Out_Tape_Irs_B_Zip;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_5a;
    private DbsField irs_B_Out_Tape_Irs_B_Record_Sequence_No;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_5c;
    private DbsField irs_B_Out_Tape_Irs_B_1099i_Or_1099r;
    private DbsGroup irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3;
    private DbsField irs_B_Out_Tape_Irs_B_1099i_2nd_Tin_Notice;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_6;
    private DbsField irs_B_Out_Tape_Irs_B_1099i_Foreign_Or_Us_Place;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_7a;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_7b;
    private DbsGroup irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_8;
    private DbsField irs_B_Out_Tape_Irs_B_Document_Code;
    private DbsGroup irs_B_Out_Tape_Irs_B_Document_CodeRedef5;
    private DbsField irs_B_Out_Tape_Irs_B_Distribution_Code;
    private DbsField irs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind;
    private DbsField irs_B_Out_Tape_Irs_B_Ira_Sep_Indicator;
    private DbsField irs_B_Out_Tape_Irs_B_Total_Dist_Ind;
    private DbsField irs_B_Out_Tape_Irs_B_Percent_Of_Tot_Distrib;
    private DbsField irs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy;
    private DbsGroup irs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6;
    private DbsField irs_B_Out_Tape_Irs_B_Roth_Contrib_Dte;
    private DbsField irs_B_Out_Tape_Irs_B_Fatca_Filing_Ind;
    private DbsField irs_B_Out_Tape_Irs_B_Date_Of_Payment;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_9;
    private DbsField irs_B_Out_Tape_Irs_B_Special_Data_Entries;
    private DbsGroup irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7;
    private DbsField irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A;
    private DbsGroup irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8;
    private DbsField irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_10;
    private DbsGroup irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9;
    private DbsField filler01;
    private DbsField irs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No;
    private DbsGroup irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10;
    private DbsField filler02;
    private DbsField irs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No;
    private DbsGroup irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Withholding_Acct_No;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Suffix_Code;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A;
    private DbsGroup irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A;
    private DbsGroup irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13;
    private DbsField irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_11;
    private DbsGroup irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14;
    private DbsField irs_B_Out_Tape_Irs_B_Ok_State_Code;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_12;
    private DbsField irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A;
    private DbsGroup irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15;
    private DbsField irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld;
    private DbsField irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A;
    private DbsGroup irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16;
    private DbsField irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld;
    private DbsField irs_B_Out_Tape_Irs_B_Federal_State_Code;
    private DbsField irs_B_Out_Tape_Irs_B_Filler_13;
    private DbsGroup irs_B_Out_TapeRedef17;
    private DbsField irs_B_Out_Tape_Irs_B_Move_1;
    private DbsField irs_B_Out_Tape_Irs_B_Move_2;
    private DbsField irs_B_Out_Tape_Irs_B_Move_3;

    public DbsGroup getIrs_B_Out_Tape() { return irs_B_Out_Tape; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Record_Type() { return irs_B_Out_Tape_Irs_B_Record_Type; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Year() { return irs_B_Out_Tape_Irs_B_Payment_Year; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Return_Indicator() { return irs_B_Out_Tape_Irs_B_Return_Indicator; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Name_Control() { return irs_B_Out_Tape_Irs_B_Name_Control; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Type_Of_Tin() { return irs_B_Out_Tape_Irs_B_Type_Of_Tin; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb() { return irs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Contract_Payee() { return irs_B_Out_Tape_Irs_B_Contract_Payee; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Contract_PayeeRedef1() { return irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Contract_First_2b() { return irs_B_Out_Tape_Irs_B_Contract_First_2b; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Contract_Filler() { return irs_B_Out_Tape_Irs_B_Contract_Filler; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payers_Office_Code() { return irs_B_Out_Tape_Irs_B_Payers_Office_Code; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_1() { return irs_B_Out_Tape_Irs_B_Filler_1; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_1() { return irs_B_Out_Tape_Irs_B_Payment_Amount_1; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_2() { return irs_B_Out_Tape_Irs_B_Payment_Amount_2; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_3() { return irs_B_Out_Tape_Irs_B_Payment_Amount_3; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_4() { return irs_B_Out_Tape_Irs_B_Payment_Amount_4; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_5() { return irs_B_Out_Tape_Irs_B_Payment_Amount_5; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_6() { return irs_B_Out_Tape_Irs_B_Payment_Amount_6; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_7() { return irs_B_Out_Tape_Irs_B_Payment_Amount_7; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_8() { return irs_B_Out_Tape_Irs_B_Payment_Amount_8; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_9() { return irs_B_Out_Tape_Irs_B_Payment_Amount_9; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_A() { return irs_B_Out_Tape_Irs_B_Payment_Amount_A; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_B() { return irs_B_Out_Tape_Irs_B_Payment_Amount_B; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_C() { return irs_B_Out_Tape_Irs_B_Payment_Amount_C; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_D() { return irs_B_Out_Tape_Irs_B_Payment_Amount_D; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_E() { return irs_B_Out_Tape_Irs_B_Payment_Amount_E; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_F() { return irs_B_Out_Tape_Irs_B_Payment_Amount_F; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Payment_Amount_G() { return irs_B_Out_Tape_Irs_B_Payment_Amount_G; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Foreign_Indicator() { return irs_B_Out_Tape_Irs_B_Foreign_Indicator; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Name_Line_1() { return irs_B_Out_Tape_Irs_B_Name_Line_1; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Name_Line_2() { return irs_B_Out_Tape_Irs_B_Name_Line_2; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_3() { return irs_B_Out_Tape_Irs_B_Filler_3; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Address_Line() { return irs_B_Out_Tape_Irs_B_Address_Line; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_4() { return irs_B_Out_Tape_Irs_B_Filler_4; }

    public DbsField getIrs_B_Out_Tape_Irs_B_City_State_Zip() { return irs_B_Out_Tape_Irs_B_City_State_Zip; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_City_State_ZipRedef2() { return irs_B_Out_Tape_Irs_B_City_State_ZipRedef2; }

    public DbsField getIrs_B_Out_Tape_Irs_B_City() { return irs_B_Out_Tape_Irs_B_City; }

    public DbsField getIrs_B_Out_Tape_Irs_B_State() { return irs_B_Out_Tape_Irs_B_State; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Zip() { return irs_B_Out_Tape_Irs_B_Zip; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_5a() { return irs_B_Out_Tape_Irs_B_Filler_5a; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Record_Sequence_No() { return irs_B_Out_Tape_Irs_B_Record_Sequence_No; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_5c() { return irs_B_Out_Tape_Irs_B_Filler_5c; }

    public DbsField getIrs_B_Out_Tape_Irs_B_1099i_Or_1099r() { return irs_B_Out_Tape_Irs_B_1099i_Or_1099r; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3() { return irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3; }

    public DbsField getIrs_B_Out_Tape_Irs_B_1099i_2nd_Tin_Notice() { return irs_B_Out_Tape_Irs_B_1099i_2nd_Tin_Notice; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_6() { return irs_B_Out_Tape_Irs_B_Filler_6; }

    public DbsField getIrs_B_Out_Tape_Irs_B_1099i_Foreign_Or_Us_Place() { return irs_B_Out_Tape_Irs_B_1099i_Foreign_Or_Us_Place; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_7a() { return irs_B_Out_Tape_Irs_B_Filler_7a; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_7b() { return irs_B_Out_Tape_Irs_B_Filler_7b; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4() { return irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_8() { return irs_B_Out_Tape_Irs_B_Filler_8; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Document_Code() { return irs_B_Out_Tape_Irs_B_Document_Code; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Document_CodeRedef5() { return irs_B_Out_Tape_Irs_B_Document_CodeRedef5; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Distribution_Code() { return irs_B_Out_Tape_Irs_B_Distribution_Code; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind() { return irs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ira_Sep_Indicator() { return irs_B_Out_Tape_Irs_B_Ira_Sep_Indicator; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Total_Dist_Ind() { return irs_B_Out_Tape_Irs_B_Total_Dist_Ind; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Percent_Of_Tot_Distrib() { return irs_B_Out_Tape_Irs_B_Percent_Of_Tot_Distrib; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy() { return irs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6() { return irs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Roth_Contrib_Dte() { return irs_B_Out_Tape_Irs_B_Roth_Contrib_Dte; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Fatca_Filing_Ind() { return irs_B_Out_Tape_Irs_B_Fatca_Filing_Ind; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Date_Of_Payment() { return irs_B_Out_Tape_Irs_B_Date_Of_Payment; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_9() { return irs_B_Out_Tape_Irs_B_Filler_9; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Special_Data_Entries() { return irs_B_Out_Tape_Irs_B_Special_Data_Entries; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7() { return irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A() { return irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8() { return irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_St_Taxable_Income() { return irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_10() { return irs_B_Out_Tape_Irs_B_Filler_10; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9() { return irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No() { return irs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10() { return irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No() { return irs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11() { return irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Withholding_Acct_No() { return irs_B_Out_Tape_Irs_B_Ga_Withholding_Acct_No; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Suffix_Code() { return irs_B_Out_Tape_Irs_B_Ga_Suffix_Code; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A() { return irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12() { return irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Taxable_Amount() { return irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A() { return irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13() { return irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld() { return irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_11() { return irs_B_Out_Tape_Irs_B_Filler_11; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14() { return irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Ok_State_Code() { return irs_B_Out_Tape_Irs_B_Ok_State_Code; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_12() { return irs_B_Out_Tape_Irs_B_Filler_12; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A() { return irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15() { return irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld() { return irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A() { return irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A; }

    public DbsGroup getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16() { return irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld() { return irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Federal_State_Code() { return irs_B_Out_Tape_Irs_B_Federal_State_Code; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Filler_13() { return irs_B_Out_Tape_Irs_B_Filler_13; }

    public DbsGroup getIrs_B_Out_TapeRedef17() { return irs_B_Out_TapeRedef17; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Move_1() { return irs_B_Out_Tape_Irs_B_Move_1; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Move_2() { return irs_B_Out_Tape_Irs_B_Move_2; }

    public DbsField getIrs_B_Out_Tape_Irs_B_Move_3() { return irs_B_Out_Tape_Irs_B_Move_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        irs_B_Out_Tape = newGroupInRecord("irs_B_Out_Tape", "IRS-B-OUT-TAPE");
        irs_B_Out_Tape_Irs_B_Record_Type = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Record_Type", "IRS-B-RECORD-TYPE", FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Payment_Year = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Year", "IRS-B-PAYMENT-YEAR", FieldType.STRING, 
            4);
        irs_B_Out_Tape_Irs_B_Return_Indicator = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Return_Indicator", "IRS-B-RETURN-INDICATOR", FieldType.STRING, 
            1);
        irs_B_Out_Tape_Irs_B_Name_Control = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Name_Control", "IRS-B-NAME-CONTROL", FieldType.STRING, 
            4);
        irs_B_Out_Tape_Irs_B_Type_Of_Tin = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Type_Of_Tin", "IRS-B-TYPE-OF-TIN", FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Taxpayer_Id_Numb", "IRS-B-TAXPAYER-ID-NUMB", FieldType.STRING, 
            9);
        irs_B_Out_Tape_Irs_B_Contract_Payee = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Contract_Payee", "IRS-B-CONTRACT-PAYEE", FieldType.STRING, 
            20);
        irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1", "Redefines", irs_B_Out_Tape_Irs_B_Contract_Payee);
        irs_B_Out_Tape_Irs_B_Contract_First_2b = irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1.newFieldInGroup("irs_B_Out_Tape_Irs_B_Contract_First_2b", "IRS-B-CONTRACT-FIRST-2B", 
            FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Contract_Filler = irs_B_Out_Tape_Irs_B_Contract_PayeeRedef1.newFieldInGroup("irs_B_Out_Tape_Irs_B_Contract_Filler", "IRS-B-CONTRACT-FILLER", 
            FieldType.STRING, 18);
        irs_B_Out_Tape_Irs_B_Payers_Office_Code = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payers_Office_Code", "IRS-B-PAYERS-OFFICE-CODE", 
            FieldType.STRING, 4);
        irs_B_Out_Tape_Irs_B_Filler_1 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_1", "IRS-B-FILLER-1", FieldType.STRING, 10);
        irs_B_Out_Tape_Irs_B_Payment_Amount_1 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_1", "IRS-B-PAYMENT-AMOUNT-1", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_2 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_2", "IRS-B-PAYMENT-AMOUNT-2", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_3 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_3", "IRS-B-PAYMENT-AMOUNT-3", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_4 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_4", "IRS-B-PAYMENT-AMOUNT-4", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_5 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_5", "IRS-B-PAYMENT-AMOUNT-5", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_6 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_6", "IRS-B-PAYMENT-AMOUNT-6", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_7 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_7", "IRS-B-PAYMENT-AMOUNT-7", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_8 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_8", "IRS-B-PAYMENT-AMOUNT-8", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_9 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_9", "IRS-B-PAYMENT-AMOUNT-9", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_A = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_A", "IRS-B-PAYMENT-AMOUNT-A", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_B = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_B", "IRS-B-PAYMENT-AMOUNT-B", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_C = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_C", "IRS-B-PAYMENT-AMOUNT-C", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_D = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_D", "IRS-B-PAYMENT-AMOUNT-D", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_E = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_E", "IRS-B-PAYMENT-AMOUNT-E", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_F = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_F", "IRS-B-PAYMENT-AMOUNT-F", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Payment_Amount_G = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Payment_Amount_G", "IRS-B-PAYMENT-AMOUNT-G", FieldType.DECIMAL, 
            12,2);
        irs_B_Out_Tape_Irs_B_Foreign_Indicator = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Foreign_Indicator", "IRS-B-FOREIGN-INDICATOR", FieldType.STRING, 
            1);
        irs_B_Out_Tape_Irs_B_Name_Line_1 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Name_Line_1", "IRS-B-NAME-LINE-1", FieldType.STRING, 40);
        irs_B_Out_Tape_Irs_B_Name_Line_2 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Name_Line_2", "IRS-B-NAME-LINE-2", FieldType.STRING, 40);
        irs_B_Out_Tape_Irs_B_Filler_3 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_3", "IRS-B-FILLER-3", FieldType.STRING, 40);
        irs_B_Out_Tape_Irs_B_Address_Line = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Address_Line", "IRS-B-ADDRESS-LINE", FieldType.STRING, 
            40);
        irs_B_Out_Tape_Irs_B_Filler_4 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_4", "IRS-B-FILLER-4", FieldType.STRING, 40);
        irs_B_Out_Tape_Irs_B_City_State_Zip = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_City_State_Zip", "IRS-B-CITY-STATE-ZIP", FieldType.STRING, 
            51);
        irs_B_Out_Tape_Irs_B_City_State_ZipRedef2 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_City_State_ZipRedef2", "Redefines", irs_B_Out_Tape_Irs_B_City_State_Zip);
        irs_B_Out_Tape_Irs_B_City = irs_B_Out_Tape_Irs_B_City_State_ZipRedef2.newFieldInGroup("irs_B_Out_Tape_Irs_B_City", "IRS-B-CITY", FieldType.STRING, 
            40);
        irs_B_Out_Tape_Irs_B_State = irs_B_Out_Tape_Irs_B_City_State_ZipRedef2.newFieldInGroup("irs_B_Out_Tape_Irs_B_State", "IRS-B-STATE", FieldType.STRING, 
            2);
        irs_B_Out_Tape_Irs_B_Zip = irs_B_Out_Tape_Irs_B_City_State_ZipRedef2.newFieldInGroup("irs_B_Out_Tape_Irs_B_Zip", "IRS-B-ZIP", FieldType.STRING, 
            9);
        irs_B_Out_Tape_Irs_B_Filler_5a = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_5a", "IRS-B-FILLER-5A", FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Record_Sequence_No = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Record_Sequence_No", "IRS-B-RECORD-SEQUENCE-NO", 
            FieldType.NUMERIC, 8);
        irs_B_Out_Tape_Irs_B_Filler_5c = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_5c", "IRS-B-FILLER-5C", FieldType.STRING, 36);
        irs_B_Out_Tape_Irs_B_1099i_Or_1099r = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_1099i_Or_1099r", "IRS-B-1099I-OR-1099R", FieldType.STRING, 
            119);
        irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3", "Redefines", irs_B_Out_Tape_Irs_B_1099i_Or_1099r);
        irs_B_Out_Tape_Irs_B_1099i_2nd_Tin_Notice = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3.newFieldInGroup("irs_B_Out_Tape_Irs_B_1099i_2nd_Tin_Notice", 
            "IRS-B-1099I-2ND-TIN-NOTICE", FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Filler_6 = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_6", "IRS-B-FILLER-6", FieldType.STRING, 
            2);
        irs_B_Out_Tape_Irs_B_1099i_Foreign_Or_Us_Place = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3.newFieldInGroup("irs_B_Out_Tape_Irs_B_1099i_Foreign_Or_Us_Place", 
            "IRS-B-1099I-FOREIGN-OR-US-PLACE", FieldType.STRING, 40);
        irs_B_Out_Tape_Irs_B_Filler_7a = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_7a", "IRS-B-FILLER-7A", 
            FieldType.STRING, 13);
        irs_B_Out_Tape_Irs_B_Filler_7b = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef3.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_7b", "IRS-B-FILLER-7B", 
            FieldType.STRING, 63);
        irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4", "Redefines", irs_B_Out_Tape_Irs_B_1099i_Or_1099r);
        irs_B_Out_Tape_Irs_B_Filler_8 = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_8", "IRS-B-FILLER-8", FieldType.STRING, 
            1);
        irs_B_Out_Tape_Irs_B_Document_Code = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Document_Code", "IRS-B-DOCUMENT-CODE", 
            FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Document_CodeRedef5 = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newGroupInGroup("irs_B_Out_Tape_Irs_B_Document_CodeRedef5", 
            "Redefines", irs_B_Out_Tape_Irs_B_Document_Code);
        irs_B_Out_Tape_Irs_B_Distribution_Code = irs_B_Out_Tape_Irs_B_Document_CodeRedef5.newFieldInGroup("irs_B_Out_Tape_Irs_B_Distribution_Code", "IRS-B-DISTRIBUTION-CODE", 
            FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Txble_Not_Det_Ind", "IRS-B-TXBLE-NOT-DET-IND", 
            FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Ira_Sep_Indicator = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ira_Sep_Indicator", "IRS-B-IRA-SEP-INDICATOR", 
            FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Total_Dist_Ind = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Total_Dist_Ind", "IRS-B-TOTAL-DIST-IND", 
            FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Percent_Of_Tot_Distrib = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Percent_Of_Tot_Distrib", 
            "IRS-B-PERCENT-OF-TOT-DISTRIB", FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy", "IRS-B-ROTH-CONTRIB-YYYY", 
            FieldType.STRING, 4);
        irs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6 = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newGroupInGroup("irs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6", 
            "Redefines", irs_B_Out_Tape_Irs_B_Roth_Contrib_Yyyy);
        irs_B_Out_Tape_Irs_B_Roth_Contrib_Dte = irs_B_Out_Tape_Irs_B_Roth_Contrib_YyyyRedef6.newFieldInGroup("irs_B_Out_Tape_Irs_B_Roth_Contrib_Dte", 
            "IRS-B-ROTH-CONTRIB-DTE", FieldType.NUMERIC, 4);
        irs_B_Out_Tape_Irs_B_Fatca_Filing_Ind = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Fatca_Filing_Ind", "IRS-B-FATCA-FILING-IND", 
            FieldType.STRING, 1);
        irs_B_Out_Tape_Irs_B_Date_Of_Payment = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Date_Of_Payment", "IRS-B-DATE-OF-PAYMENT", 
            FieldType.STRING, 8);
        irs_B_Out_Tape_Irs_B_Filler_9 = irs_B_Out_Tape_Irs_B_1099i_Or_1099rRedef4.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_9", "IRS-B-FILLER-9", FieldType.STRING, 
            98);
        irs_B_Out_Tape_Irs_B_Special_Data_Entries = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Special_Data_Entries", "IRS-B-SPECIAL-DATA-ENTRIES", 
            FieldType.STRING, 60);
        irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7", "Redefines", 
            irs_B_Out_Tape_Irs_B_Special_Data_Entries);
        irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A", 
            "IRS-B-ME-ST-TAXABLE-INCOME-A", FieldType.STRING, 9);
        irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7.newGroupInGroup("irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8", 
            "Redefines", irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_A);
        irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income = irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income_ARedef8.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_St_Taxable_Income", 
            "IRS-B-ME-ST-TAXABLE-INCOME", FieldType.DECIMAL, 9,2);
        irs_B_Out_Tape_Irs_B_Filler_10 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef7.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_10", "IRS-B-FILLER-10", 
            FieldType.STRING, 51);
        irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9", "Redefines", 
            irs_B_Out_Tape_Irs_B_Special_Data_Entries);
        filler01 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 52);
        irs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef9.newFieldInGroup("irs_B_Out_Tape_Irs_B_Md_Cntral_Registratn_No", 
            "IRS-B-MD-CNTRAL-REGISTRATN-NO", FieldType.STRING, 8);
        irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10", "Redefines", 
            irs_B_Out_Tape_Irs_B_Special_Data_Entries);
        filler02 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 49);
        irs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef10.newFieldInGroup("irs_B_Out_Tape_Irs_B_Nm_Cntral_Registratn_No", 
            "IRS-B-NM-CNTRAL-REGISTRATN-NO", FieldType.STRING, 11);
        irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11", "Redefines", 
            irs_B_Out_Tape_Irs_B_Special_Data_Entries);
        irs_B_Out_Tape_Irs_B_Ga_Withholding_Acct_No = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Withholding_Acct_No", 
            "IRS-B-GA-WITHHOLDING-ACCT-NO", FieldType.NUMERIC, 11);
        irs_B_Out_Tape_Irs_B_Ga_Suffix_Code = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Suffix_Code", 
            "IRS-B-GA-SUFFIX-CODE", FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A", 
            "IRS-B-GA-TAXABLE-AMOUNT-A", FieldType.STRING, 15);
        irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newGroupInGroup("irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12", 
            "Redefines", irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_A);
        irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount = irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount_ARedef12.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Taxable_Amount", 
            "IRS-B-GA-TAXABLE-AMOUNT", FieldType.DECIMAL, 15,2);
        irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A", 
            "IRS-B-GA-INCOME-TAX-WITHHELD-A", FieldType.STRING, 15);
        irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newGroupInGroup("irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13", 
            "Redefines", irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_A);
        irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld = irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld_ARedef13.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ga_Income_Tax_Withheld", 
            "IRS-B-GA-INCOME-TAX-WITHHELD", FieldType.DECIMAL, 15,2);
        irs_B_Out_Tape_Irs_B_Filler_11 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef11.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_11", "IRS-B-FILLER-11", 
            FieldType.STRING, 17);
        irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14", "Redefines", 
            irs_B_Out_Tape_Irs_B_Special_Data_Entries);
        irs_B_Out_Tape_Irs_B_Ok_State_Code = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14.newFieldInGroup("irs_B_Out_Tape_Irs_B_Ok_State_Code", "IRS-B-OK-STATE-CODE", 
            FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Filler_12 = irs_B_Out_Tape_Irs_B_Special_Data_EntriesRedef14.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_12", "IRS-B-FILLER-12", 
            FieldType.STRING, 58);
        irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A", "IRS-B-ME-ST-TAX-WITHHELD-A", 
            FieldType.STRING, 12);
        irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15", "Redefines", 
            irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_A);
        irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld = irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld_ARedef15.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_St_Tax_Withheld", 
            "IRS-B-ME-ST-TAX-WITHHELD", FieldType.DECIMAL, 12,2);
        irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A", "IRS-B-ME-LC-TAX-WITHHELD-A", 
            FieldType.STRING, 12);
        irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16 = irs_B_Out_Tape.newGroupInGroup("irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16", "Redefines", 
            irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_A);
        irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld = irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld_ARedef16.newFieldInGroup("irs_B_Out_Tape_Irs_B_Me_Lc_Tax_Withheld", 
            "IRS-B-ME-LC-TAX-WITHHELD", FieldType.DECIMAL, 12,2);
        irs_B_Out_Tape_Irs_B_Federal_State_Code = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Federal_State_Code", "IRS-B-FEDERAL-STATE-CODE", 
            FieldType.STRING, 2);
        irs_B_Out_Tape_Irs_B_Filler_13 = irs_B_Out_Tape.newFieldInGroup("irs_B_Out_Tape_Irs_B_Filler_13", "IRS-B-FILLER-13", FieldType.STRING, 2);
        irs_B_Out_TapeRedef17 = newGroupInRecord("irs_B_Out_TapeRedef17", "Redefines", irs_B_Out_Tape);
        irs_B_Out_Tape_Irs_B_Move_1 = irs_B_Out_TapeRedef17.newFieldInGroup("irs_B_Out_Tape_Irs_B_Move_1", "IRS-B-MOVE-1", FieldType.STRING, 250);
        irs_B_Out_Tape_Irs_B_Move_2 = irs_B_Out_TapeRedef17.newFieldInGroup("irs_B_Out_Tape_Irs_B_Move_2", "IRS-B-MOVE-2", FieldType.STRING, 250);
        irs_B_Out_Tape_Irs_B_Move_3 = irs_B_Out_TapeRedef17.newFieldInGroup("irs_B_Out_Tape_Irs_B_Move_3", "IRS-B-MOVE-3", FieldType.STRING, 250);

        this.setRecordName("LdaTwrl3322");
    }

    public void initializeValues() throws Exception
    {
        reset();
        irs_B_Out_Tape_Irs_B_Record_Type.setInitialValue("B");
        irs_B_Out_Tape_Irs_B_Return_Indicator.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Name_Control.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Payers_Office_Code.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Filler_1.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Payment_Amount_A.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_B.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_C.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_D.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_E.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_F.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Payment_Amount_G.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Filler_3.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Filler_4.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Filler_5a.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Record_Sequence_No.setInitialValue(0);
        irs_B_Out_Tape_Irs_B_Filler_5c.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_1099i_Or_1099r.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Special_Data_Entries.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Federal_State_Code.setInitialValue(" ");
        irs_B_Out_Tape_Irs_B_Filler_13.setInitialValue(" ");
    }

    // Constructor
    public LdaTwrl3322() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
