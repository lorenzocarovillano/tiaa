/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:15:16 PM
**        * FROM NATURAL LDA     : TWRLIRAA
************************************************************
**        * FILE NAME            : LdaTwrliraa.java
**        * CLASS NAME           : LdaTwrliraa
**        * INSTANCE NAME        : LdaTwrliraa
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrliraa extends DbsRecord
{
    // Properties
    private DbsField pnd_Twrliraa_Pnd_Max;
    private DbsGroup pnd_Twrliraa;
    private DbsGroup pnd_Twrliraa_Pnd_Ira_Acct_Type_Table;
    private DbsField pnd_Twrliraa_Pnd_Iraa_Short;
    private DbsField pnd_Twrliraa_Pnd_Iraa_Desc;

    public DbsField getPnd_Twrliraa_Pnd_Max() { return pnd_Twrliraa_Pnd_Max; }

    public DbsGroup getPnd_Twrliraa() { return pnd_Twrliraa; }

    public DbsGroup getPnd_Twrliraa_Pnd_Ira_Acct_Type_Table() { return pnd_Twrliraa_Pnd_Ira_Acct_Type_Table; }

    public DbsField getPnd_Twrliraa_Pnd_Iraa_Short() { return pnd_Twrliraa_Pnd_Iraa_Short; }

    public DbsField getPnd_Twrliraa_Pnd_Iraa_Desc() { return pnd_Twrliraa_Pnd_Iraa_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        int int_pnd_Twrliraa_Pnd_Max = 5;

        pnd_Twrliraa_Pnd_Max = newFieldInRecord("pnd_Twrliraa_Pnd_Max", "#TWRLIRAA-#MAX", FieldType.PACKED_DECIMAL, 3);

        pnd_Twrliraa = newGroupInRecord("pnd_Twrliraa", "#TWRLIRAA");
        pnd_Twrliraa_Pnd_Ira_Acct_Type_Table = pnd_Twrliraa.newGroupArrayInGroup("pnd_Twrliraa_Pnd_Ira_Acct_Type_Table", "#IRA-ACCT-TYPE-TABLE", new DbsArrayController(0,
            int_pnd_Twrliraa_Pnd_Max));
        pnd_Twrliraa_Pnd_Iraa_Short = pnd_Twrliraa_Pnd_Ira_Acct_Type_Table.newFieldInGroup("pnd_Twrliraa_Pnd_Iraa_Short", "#IRAA-SHORT", FieldType.STRING, 
            4);
        pnd_Twrliraa_Pnd_Iraa_Desc = pnd_Twrliraa_Pnd_Ira_Acct_Type_Table.newFieldInGroup("pnd_Twrliraa_Pnd_Iraa_Desc", "#IRAA-DESC", FieldType.STRING, 
            11);

        this.setRecordName("LdaTwrliraa");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Twrliraa_Pnd_Max.setInitialValue(5);
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(0).setInitialValue("????");
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(1).setInitialValue("Clas");
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(2).setInitialValue("Roth");
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(3).setInitialValue("Trad");
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(4).setInitialValue("Educ");
        pnd_Twrliraa_Pnd_Iraa_Short.getValue(5).setInitialValue("SEP");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(0).setInitialValue("Unknown");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(1).setInitialValue("Classic");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(2).setInitialValue("Roth");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(3).setInitialValue("Traditional");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(4).setInitialValue("Educational");
        pnd_Twrliraa_Pnd_Iraa_Desc.getValue(5).setInitialValue("SEP");
    }

    // Constructor
    public LdaTwrliraa() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
