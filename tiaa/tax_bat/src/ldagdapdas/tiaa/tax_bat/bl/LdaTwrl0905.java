/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:12:14 PM
**        * FROM NATURAL LDA     : TWRL0905
************************************************************
**        * FILE NAME            : LdaTwrl0905.java
**        * CLASS NAME           : LdaTwrl0905
**        * INSTANCE NAME        : LdaTwrl0905
************************************************************ */

package tiaa.tax_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaTwrl0905 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Flat_File4;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Residency_Type;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Residency_Code;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Distribution_Cde;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Company_Code;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Ivc_Ind;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Contract_Nbr;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Tax_Citizenship;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Payset_Type;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Trans_Cnt;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld;
    private DbsField pnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld;

    public DbsGroup getPnd_Flat_File4() { return pnd_Flat_File4; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Residency_Type() { return pnd_Flat_File4_Pnd_Ff4_Residency_Type; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Residency_Code() { return pnd_Flat_File4_Pnd_Ff4_Residency_Code; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Distribution_Cde() { return pnd_Flat_File4_Pnd_Ff4_Distribution_Cde; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Company_Code() { return pnd_Flat_File4_Pnd_Ff4_Company_Code; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Ivc_Ind() { return pnd_Flat_File4_Pnd_Ff4_Ivc_Ind; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Contract_Nbr() { return pnd_Flat_File4_Pnd_Ff4_Contract_Nbr; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Tax_Citizenship() { return pnd_Flat_File4_Pnd_Ff4_Tax_Citizenship; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Payset_Type() { return pnd_Flat_File4_Pnd_Ff4_Payset_Type; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Trans_Cnt() { return pnd_Flat_File4_Pnd_Ff4_Trans_Cnt; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt() { return pnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt() { return pnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt() { return pnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld() { return pnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld() { return pnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld() { return pnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld() { return pnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld; }

    public DbsField getPnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld() { return pnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Flat_File4 = newGroupInRecord("pnd_Flat_File4", "#FLAT-FILE4");
        pnd_Flat_File4_Pnd_Ff4_Residency_Type = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Residency_Type", "#FF4-RESIDENCY-TYPE", FieldType.STRING, 
            1);
        pnd_Flat_File4_Pnd_Ff4_Residency_Code = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Residency_Code", "#FF4-RESIDENCY-CODE", FieldType.STRING, 
            2);
        pnd_Flat_File4_Pnd_Ff4_Distribution_Cde = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Distribution_Cde", "#FF4-DISTRIBUTION-CDE", FieldType.STRING, 
            2);
        pnd_Flat_File4_Pnd_Ff4_Company_Code = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Company_Code", "#FF4-COMPANY-CODE", FieldType.STRING, 
            1);
        pnd_Flat_File4_Pnd_Ff4_Ivc_Ind = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Ivc_Ind", "#FF4-IVC-IND", FieldType.STRING, 1);
        pnd_Flat_File4_Pnd_Ff4_Contract_Nbr = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Contract_Nbr", "#FF4-CONTRACT-NBR", FieldType.STRING, 
            8);
        pnd_Flat_File4_Pnd_Ff4_Tax_Citizenship = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Tax_Citizenship", "#FF4-TAX-CITIZENSHIP", FieldType.STRING, 
            1);
        pnd_Flat_File4_Pnd_Ff4_Payset_Type = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Payset_Type", "#FF4-PAYSET-TYPE", FieldType.STRING, 
            2);
        pnd_Flat_File4_Pnd_Ff4_Trans_Cnt = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Trans_Cnt", "#FF4-TRANS-CNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Gross_Amt", "#FF4-SUM-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Ivc_Amt", "#FF4-SUM-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Int_Amt", "#FF4-SUM-INT-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Fed_Wthld", "#FF4-SUM-FED-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Nra_Wthld", "#FF4-SUM-NRA-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Can_Wthld", "#FF4-SUM-CAN-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_State_Wthld", "#FF4-SUM-STATE-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld = pnd_Flat_File4.newFieldInGroup("pnd_Flat_File4_Pnd_Ff4_Sum_Local_Wthld", "#FF4-SUM-LOCAL-WTHLD", FieldType.PACKED_DECIMAL, 
            9,2);

        this.setRecordName("LdaTwrl0905");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaTwrl0905() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
